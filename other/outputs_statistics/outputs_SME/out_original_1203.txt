
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dušan Hudík
                                        &gt;
                Súkromné
                     
                 Exotické ovocie. 

        
            
                                    4.2.2009
            o
            21:14
                        |
            Karma článku:
                10.72
            |
            Prečítané 
            13929-krát
                    
         
     
         
             

                 
                    Keď ma poslali pred Vianocami na nákup tropického ovocia, tak ma napadlo že by nebolo zlé vybočiť zo stereotypu (mandarinky,banány a pod.) a ochutnať aj niečo čo som ešte ani nevidel, nie to ešte jedol. Nadšený a zvedavý som sa pustil do výberu "kanditátov" a minul okolo 300sk - však sú Vianoce. Keďže nie som odborník na ovocie, tak prišli aj tienisté stránky tohoto nápadu, ale to sa dalo čakať. Aspoň to bude dobrým poučením pre Vás, a pri výbere sa vyvarujete mojich chýb :-)
                 

                Na nákup som si vybral Kaufland. Možno so mnou niektorí nebudú súhlasiť, ale majú tam najlepší výber ovocia a aj v najvyššej kvalite. Aspoň v tom v Leviciach. Z pestrej ponuky "exotiky" som vybral tieto druhy : Granadila, Karambola, Physalis, Pitahaja, Kaki, Tamarillo, Kivano.      Prvým adeptom bola : Sladká Granadila (Mučenka jazyková) Angl.n.Sweet Granadilla. V prírode rastie ako liana. Keď je zrelá, tak by mala mať farbu žltooranžovú, tak pozor na to čo kupujete. Tá moja mala správnu -chvalabohu Možno so mnou nebudú niektorí súhlasiť, ale na výber ovocia sa mi najviac osvedčil Kaufland. Aspoň:-) . Dužina je sladká, rôsolovitá a plná semien. Šťava sa z nej zvykne riediť s vodou, alebo sa pridáva do iných nápojov. My sme vnútro po prekrojení vyjedli lyžičkou.            Druhá bola Karambola (Egrešovec oblý), po Angl. tiež Karambola. Je to asi 12m strom s krátkym kmeňom-aby ste mali predstavu. Plody majú krásny hviezdicový prierez, takže je to pekné na ozdobenie pohárov s drinkom, alebo ovocné misy. Zrelé je to keď je to žlté, tak pozor. My sme mali zelené, a keďže sme boli netrpezliví, tak sa nám ušlo kyslej chuti. :-) V Ázii konzumujú aj zelenú-nakladajú ju, alebo korenia či robia želé. Robia ju proste aj na slano. Zrelé sa používajú na šťavu, kandizujú a super to je do ovocných šalátov-keď to na tie hviezdy nakrájate, iste mi dáte za pravdu.           Tamarilo - (Rajčiakovec repový),Angl.Tree-tomato je na rozdiel od predchodcov poloker. Náš exemplár bol zrelý, tak uvidíte ko to má vypadať na fotke. Podľa popisu sa to vraj konzumuje ako ovocie, ale ja by som to fakt takto jedol až po ocukrení-má to korenistú chuť. Rozhodne nečakajte čosi sladké. Zvykne sa pridávať do polievok a jedál-kde by som ho uprednostnil. Ale na pohľad vypadá celkom lákavo.     Kaki, Hurmikaki - (Ebenovník rajčiakový), Angl.Kaki plum, patril medzi to najlepšie čo sme skúšali. Plody toho stromu mi z diaľky pripomínali marhule, a podobne aj chutili. Toto je prvé ovocie ktoré bez obáv odporúčam. Vraj obsahujú Tanín kým nie sú zrelé, ktorý sa stráca varením...my sme mali zrelé a chutili dobre, hoci sa pravdepodobne zberali nedozreté...Každopádne by mali byť oranžové a mäkké. Majú široké využitie- spomeniem aspoň marmelády a ako prísada do zmrzliny musí mať svoje čaro. Kaki je na fotke s Tamarilom v hornej časti taniera.         Potom došlo Kivano - Angl. Horned cucumber. Tak toto si kupujte len na ozdobu ! :-) Patrí do čeľade tekvicovitých a tak aj chutí - ako prestarnutá uhorka ! :-) Po odfotení a ochutnaní putovalo milé Kivano na kraj stola k odpadkom.         Pitahaja - (Hylocereus zvlnený), Angl. Strawberry pear je popínavý kaktus s nádhernými plodmi !Toto ovocie suverénne vyhralo súťaž krásy u všetkých. Neodfotil som to dobre, tak bohužiaľ uvidíte len vnútro..Rôsolovitá dužina je sladká a dobre sa je pomocou lyžičky, ktorou sa pekne odkrajuje. Domáci ju zvyknú pripravovať tak, že ju roztlačia, zriedia s vodou a ľadom a osladením vznikne osviežujúci nápoj.            Zlatý klinec na koniec : Physalis - (Machovka peruánska), Angl. Cape gooseberry je asi 2m vysoká bylina, na ktorej sa tvoria bobule obalené kalichom. Chutilo nám to asi najviac, a pritom to nebolo až tak strašne drahé. Bolo tam snáď okolo 10ks zabalených v krabičke. Má do dlhú trvanlivosť, čo je praktické ak ovládate svoje vášne a nechcete to zmlsnúť ihneď. Naše mali kalichy sfarbené do žltozelena a boli dobrou chuťou. Jedzte len bobule-kalich vyhodiť ! :-) Mne to chuťovo pripomínalo jahody..Obsahujú veľa vitamínu C, a domáci ich zvyknú variť dokopy s chilli papričkami z čoho vznikne koreniaca omáčka k mäsu. Obrázok nemám, preto Vám ponúkam aspoň odkaz na stránku kde je odfotená :     http://en.wikipedia.org/wiki/Physalis_peruviana         Ceny ovocia boli v priemere okolo 35 sk, ale za Pitahaju a Kivano to bolo okolo 70sk ! Pri pomyslení na Kivano ma až srdce bolí..  :-)    Tak to bolo naše malé kulinárske dobrodružstvo. Snáď sa z neho poučíte, aby ste neurobili tie isté chyby - aj keď sa hovorí, že vlastná skúsenosť je k nezaplateniu ! :-)    P.S. Viete o tom, že čierne a biele korenie je to isté ? Biele je len jadrom zbaveným dužiny, ktoré sa pomelie a trochu odlišné chuťové vlastnosti. Aj podrobnosti možno nabudúce. Ahojte.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan Hudík 
                                        
                                            4xs
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan Hudík 
                                        
                                            V našom lese
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan Hudík 
                                        
                                            Vesmírna kolonizácia a vyriešenie preľudnenia Zeme.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan Hudík 
                                        
                                            Exotické ovocie 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan Hudík 
                                        
                                            Veľké Beethovenovské klamstvo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dušan Hudík
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dušan Hudík
            
         
        hudik.blog.sme.sk (rss)
         
                                     
     
        Som pútnik na okrúhliaku zvanom Zem, a držím sa svojho hesla : "Život je ako šíp. Môžeš určiť jeho smer, ale nevieš kde nakoniec dopadne".
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3381
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Outdoor
                        
                     
                                     
                        
                            Wildlife
                        
                     
                                     
                        
                            Príhody a príbehy
                        
                     
                                     
                        
                            Filozofia a iné
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Knihy
                        
                     
                                     
                        
                            Jedlo
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Zem a popol - Atik Rahimi
                                     
                                                                             
                                            Kto kráča v temnotách - Marianne Friedriksson
                                     
                                                                             
                                            Král Krysa - James Clavell
                                     
                                                                             
                                            O návšteve Černobylu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Manuál Nikon D700  ;-)
                                     
                                                                             
                                            Lord Jim - Joseph Conrad
                                     
                                                                             
                                            Zošity Humanistov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Elena Rodriguez
                                     
                                                                             
                                            Alexander Ač
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Spoločnosť Prometeus
                                     
                                                                             
                                            Frans Lanting photography
                                     
                                                                             
                                            Hory.sk
                                     
                                                                             
                                            National Geographic
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zo života na rovníku: Zázrak pod Mt. Kenya!?!
                     
                                                         
                       Ako učíme „lenivých černochov“ chytať ryby
                     
                                                         
                       Aupark, prehanadlo, oral a šok!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




