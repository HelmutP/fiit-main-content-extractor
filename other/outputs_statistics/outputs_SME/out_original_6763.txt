
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magdaléna Stýblová
                                        &gt;
                Nezaradené
                     
                 Opäť kvalita 

        
            
                                    18.5.2010
            o
            16:33
                        (upravené
                18.5.2010
                o
                21:34)
                        |
            Karma článku:
                2.86
            |
            Prečítané 
            510-krát
                    
         
     
         
             

                 
                    Kto nebol, kultúrne upadol.
                 

                  Kto sa zastavil v Nedeľu večer v Evanjelickom kostole Sv. Ducha v Nitre, neoľutoval. Uznávam, počasie bolo hrôzostrašné. Zima, dážd' a vietor. Účasť bola aj napriek tomuto faktu celkom vysoká. Predstavil sa nám vynikajúci organista, Marek Štrbák.    Veľmi príjemný, skromný a charizmatický umelec dokázal vyčarovať na organe možné i nemožné.  Začal, ako inak, J.S. Bachom a jeho Fantáziou a fúgou g mol BWV 542. Interpretácia nás vtiahla do  koncertnej atmosféry a v duchu Baroka sme sa preniesli k d'alšiemu skladateľovi. Dietrich Buxtehude, Passacaglia in d. Po týchto dielach, nás doznievajúce tóny nútili vnútorne sa stíšiť a zamyslieť. Avšak  duch pookrial pri nasledujúcej Fantázii f mol (KV 594) od W.A.Mozarta aj napriek molovej tónine. Mozart je jednoducho balzam na dušu. Koncert plynul veľmi ľahko a interpretácia diel bola kultivovaná a výborne zvládnutá. Okrem iných diel, zaznela skladba Mozart Changes od Zsolta Gárdonyiho. Tu sme mali možnosť počuť organ v inom rozmere. Mozart v kombinácii s ľahkým vplyvom jazzu (pre tých ktorý si potrpia, čítaj džezu) bol  čistý, lesklý a aj vtipný (v tom najlepšom slovazmysle). Ani sme sa nenazdali a už bol koniec koncertu. Poslednou skladbou, Toccatou jazzicou od J.Matthiasa Michela nám opäť interpret dokázal nie len kvalitu hry, ale aj jemný cit pre jazz.    Koncert ubehol ako voda. Ked' sa ponoríte do zvuku klavitnej hudby, čas prestáva existovať, Prestávajú existovať aj problémy, ba dokonca aj fakt hrozného počasia. No, musím  smutne konšatovať, že čas letí aj bez vplyvu hudby. Preto chcem pripomenúť Vám všetkým, že festival Ars Organi síce pokračuje, no blíži sa ku koncu. Čaká nás posledný koncert českého organistu Pavla Kohouta v Piaristickom kostole sv. Ladislava v Nitre o 19.00 hod.(23.mája). Verím, že zážitok je zaručený a kto nestihol prísť, má tak šancu na "kultúrny reparát". Dovidenia v Nedeľu.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Naivných. Koľko nás v skutočnosti je?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Kultúrne Slovensko časť 2. alebo ako sa mi rozum zastavil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            "Kultúrne Slovensko" časť 1.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Dovolenka inak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Zdravotníctvo šokuje
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magdaléna Stýblová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magdaléna Stýblová
            
         
        styblova.blog.sme.sk (rss)
         
                                     
     
        ponáhľajme sa milovať ľudí, pretože rýchlo odchádzajú...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    810
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na malomeštiakovej svadbe v SND
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




