

 Český režisér, herec, scénarista, spolutvorca nepochopeného a nedoceného génia Járy Cimrmana došiel na koniec svojej životnej cesty, keď prehral svoj boj s chorobou. Je zbytočené vymenovávať všetky postavy, filmy, či divadelné hry, ktoré vytvoril, či pomohol vytvoriť.  Bola by to záplava skvelých scén, scénok, či len takých, ako tomu vravia odborníci - štekov. Z celého toho množstva mi utkvel v pamäti jeho hlas v pozadí, ktorý sa ozval vo filme "Vrchní prchni", keď sa spoločnosť spomínajúcich bývalých maturantov rozchádza a zistia, že niečí Velorex hadrový bráni výjazdu. Bol to hlas frflavého dudravého českého človíčka. 
 "Co je to tady, kto to sem dal".  
 Podobný motív by sa dal nájsť vo viacerých filmoch, napríklad Jára Cimrma ležící spíci (dvaja policajti na koňoch nabádajú ľudí aby sa rozišli krátko po tom, čo sa skončilo predstavenie českého mora, ktoré im na chvíľu povolilo skostnatelé mocnárstvo). 
 Z tých, aspoň u nás menej známych postáv, je nezabudnuteľná postava trochu natvrdlého režiséra z ďalšieho kultového filmu Trhák.  
 






 
 Ladislav Smoljak tvoril nezabudnuteľnú dvojich so Zdeňkom Svěrákom. Z českého dua S+S ostalo už len jedno. A mne je trochu smutno. 

