
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Javurek
                                        &gt;
                EÚ
                     
                 Taormina 

        
            
                                    25.11.2008
            o
            18:18
                        |
            Karma článku:
                10.97
            |
            Prečítané 
            4354-krát
                    
         
     
         
             

                 
                    Perfektne situované mestečko je pevne spojené s históriou a zároveň je otvoreným pohľadom Sicílie do súčasnosti. 
                 

                  Taormina ponúka turistom všetko, na čo si spomenú. Je tu množstvo reštaurácií a hotelov, mesto je rozložené vysoko nad morom, akoby na ťavích hrboch, nad piesočnými plážami. Najznámejšou historickou pamiatkou v Taormine je divadlo "Teatro antico", ktoré začali budovať Gréci v treťom storočí pred Kristom, neskôr ho architektonicky dotvorili Rimania.  Taormina má 10 tisíc obyvateľov, administratívne patrí pod Messinu.     Toľko historických faktov na úvod hádam stačí. Pre nás bola Taormina lákadlom na dohľad z Giardini Naxos, kde sme boli ubytovaní.            Diaľnica z Messiny je položená nad mestom. Pripomenulo mi to našu Považskú Bystricu v neďalekej budúcnosti...           Cesty v meste sú v niekoľkých výškových vrstvách, chvíľu na viaduktoch, potom zase v tuneloch...           Aj miestna dopravná sieť je značne zložitá...           Pohľad z terasy v meste.           Nasledujúce fotografie sú zdvojené. Kostol na námestí s fontánou * Detail maľby * Pohľad do vnútra          * Zátišie vo výklenku * Detail kostolného okna.           * Vstup do iného sveta * Schody, bez ktorých sa v Taormine nezaobídu.           * Hotelová kaviareň * Obchodovanie na schodoch.          * Romantická "ulička" * Obchod s keramikou.           * Obchodné "centrum" vo vedľajšej uličke * Stretnutie domácich.           * Stará zvonica * Kostolné námestie.           Výhľad na more z "Teatro antico"           Časť pláží, ktoré pokračujú do Giardini Naxos.            Detail pláže a hukot vĺn, ktorý sa nikdy nezunuje...           V Taormine sú postavené domy na každom kúsku vyčnievajúcej skaly...           Takáto scéna sa núkala už starým Grékom, v pozadí za hradbou mrakov je skrytá Etna...        Iné pohľady na Taorminu nájdete aj tu:     Taormina pýcha Sicílie     Na juh od Neapola začína Afrika      Ponuka, ktorá sa neodmieta        Miestopisné informácie čerpané z  DK Spoločník cestovateľa, Itália vydavateľstvo IKAR, vydané v roku 2001

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Vieme prví, len či pravdu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            November
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Moja prvá fantastika
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Papierové mestá
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Spisovatelia v Smoleniciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Javurek
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Javurek
            
         
        jozefjavurek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Neoznačené fotografie na blogu výlučne z vlastnej tvorby (c). Inak je všetko vo hviezdach...
 
"štandardný už tradičný bloger, s istou mierou poctivosti" (jeden čitateľ)
 
 


Click for "Your New Digest 1".
Powered by RSS Feed Informer

počet návštev:







 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    650
                
                
                    Celková karma
                    
                                                4.59
                    
                
                
                    Priemerná čítanosť
                    2950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Český raj
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Myšlienky, názory
                        
                     
                                     
                        
                            Blogovanie
                        
                     
                                     
                        
                            Technika a technológia
                        
                     
                                     
                        
                            Domácnosť
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            EÚ
                        
                     
                                     
                        
                            Hory, lesy
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




