

  
 
 
   
    
 Po tom čo som si ja, Alexander JÁRAY, v dostatočnej miere (na vysokoškolskej úrovni) naštudoval všetky zákony matematiky a fyziky a to takým spôsobom, že ja som si obsah týchto zákonov počas ich štúdii podrobil dôkladnej a kritickej, no pritom nekompromisnej analýze, pričom obsah niektorých matematických a fyzikálnych zákonov som kategoricky odmietol a to až po tom, čo ja som si ich pravdivosť aj experimentálne overil, začal z ničoho nič v mojom podvedomí fungovať mechanizmus prirodzenej selekcie prírodovedeckých materialistických bludov od materialistických právd. 
 Prvé dve veci, na ktoré ma ten môj podvedomí mechanizmus selekcie pravdy od heréz upozornil, boli pochybný obsah tretieho Newtonovho zákona „Akcie a reakcie“ vo fyzike, ako aj pochybný obsah a zmysel „limity postupnosti“ v matematike.  
 Teraz sa budem zaoberať iba rozborom heréz „tretieho Newtonovho zákona pohybu „Zákona akcie a reakcie“. 
 Obsah „Tretieho Newtonovho zákona pohybu“ „Zákona akcie a reakcie“ znie nasledovne: 
 (Citát je z učebnice fyziky pod názvom, Všeobecná fyzika 1) 
 „Sily ktorými na seba pôsobia dva hmotné body, (staticky, či dynamicky) sú rovnako veľké a majú opačný smer“. 
 + F   =  - F 
 Druhý citát je z knihy nositeľa Nobelovej ceny za fyziku, R.P. Feynmana, "Prednášky z fyziky", ktorý sa nachádza na strane 148.) 
 „Predpokladajme, že máme dve malé telieska napríklad častice, z ktorých jedna pôsobí na druhú tak, že ju odtláča. Potom podľa tretieho Newtonovho zákona, druhá častica pôsobí na prvú rovnakou silou v opačnom smere; navyše tieto sily budú pôsobiť pozdĺž tej istej priamky.  
  
 Táto hypotéza (alebo zákon vyslovený Newtonom) ukazuje sa byť správnym. 
 Matematický zápis tretieho Nevtonového zákona pohybu, „Zákona akcie a reakcie“ je nasledovný: 
 + F   =   - F 
 Tento tretí Newtonov zákon pohybu hmoty, je pre ľudstvo natoľko jasný a pravdivý, že ani jeden človek na svete, čo i len v náznakoch nevznáša proti jeho obsahu žiadne námietky, ba čo viac, nejeden fyzikálny laik obsah tohto zákona aj oduševnene obhajuje, argumentujúc tým, že keď on platí už viac ako 300 rokov, tak prečo by nemal platiť ďalších 300 rokov. 
 Iba jediný človek na tomto svete, (aj to až po experimentálnom preverení pravdivosti obsahu "Zákona akcie a reakcie") považuje tretí Newtonov zákon pohybu za blud a to nie za hocaký blud, ale za blud kolosálny, za blud ľudskú dôstojnosť ponižujúci, ako aj za blud Božie dielo zosmiešňujúci, no a tým jediným človekom na Zemi je:   
 občan SR, menom Alexander JÁRAY.          
                                                                                                                                                        
 Vzorec časového účinku statickej silovej interakcie, vyučovaný na školách a univerzitách celého sveta  je nasledovný: 
 F.dt  =  m.a.dt ; F.0t  =  m .a.0t; 
 (Ide o vzorec opisujúci staticky zrýchlený pohyb, teda  pohyb konaný na jednom mieste, na nulovej dráhe a za nulový čas, čiže na dráhe ds, za čas dt.) 
 Počuli sme slová o realite spojenej so slepým  akceptovaním pravdivosti obsahu tretieho Newtonovho zákona pohybu ľudstvom. 
  Amen. 
  
 Akými argumentmi spochybňuje pravdivosť tretieho Newtonoho zákona pohybu,  Alexander JÁRAY ???. 
 Prvým, do oči bijúcim nezmyslom pri treťom Newtonovom zákone pohybu, "Zákona akcie a reakcie" bol pre mňa jeho matematický zápis v tvare: 
 + F   =  - F 
 Lebo keď túto rovnicu vydelíme číslom F, čiže: 
 (+F = -F) / F, 
 dostaneme super matematický nezmysel a to rovnicu: 
 +1 = -1, 
 ktorej bludnosť sa prejaví až vtedy keď po dovolenej matematickej úprave dostanem rovnicu: 
 +1 +1 = 0, 
 Čiže: +2 = 0. 
 Už samotný matematický zápis tretieho Newtonovho zákona pohybu je horibilný nezmysel. No ale na tento argument, krčmoví i baroví, či akademickí fyzici obhajujú tým, že v tomto prípade ide o vektory a nie o skalárne veličiny. 
 Ibaže takýto argument ja ešte horší, ešte väčší blud ako ten predošlý, pretože medzi ukazovateľ smeru dopredu a ukazovateľ smeru dozadu, nie je možné (je prísne zakázané) dávať znak rovnosti, (=) lebo v tom momente by nastal chaos nielen v matematike a fyzike, ale aj na všetkých cestách našej premilej vlasti. 
 Z reálneho pohybu oceľových gúľ vyplýva, že na impulz guli akcie, guľa rekcie nereaguje opačne orientovanou silou, opačne orientovaným zrýchlením, lebo sa nepohybuje v smere opačnom ako guľa akcie, ale v smere guli akcie.  
 A sila reakcie nikdy ne je rovnako veľká sile reakcie, lebo v tom prípade gúľa akcie by sa od guli reakcie odrazila a sama by sa pohybovala v opačnom smere. 
 Pokračovanie zajtra. Témou bude: ignorovanie zákona Kauzality, "Zákonom akcie a reakcie".   
   

  
  

