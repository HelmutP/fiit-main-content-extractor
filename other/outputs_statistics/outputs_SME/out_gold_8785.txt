

 Po skúsenostiach sme sa do Sieny vybrali po dlhšej, 124-kilometrovej európskej ceste s využitím diaľničného obchvatu Florencie (diaľnica do Sieny nevedie). Po poldruha hodine sme boli v tomto starobylom meste, ale trvalo nám vyše polhodiny, kým sa nám podarilo v jednej uličke zaparkovať. Nejaké väčšie záchytné parkovisko sme v meste nenašli, všetky menšie parkoviská boli preplnené a vo väčšine ulíc a uličiek sú všetky parkovacie miesta vyhradené pre domácich. Aj sme dvakrát cvične zaparkovali na voľnom mieste, po dodatočnej kontrole značiek na oboch koncoch uličky sme však radšej odtiahli. Aj sme videli odťahovú službu v akcii, tak sme boli radšej opatrní. Povedal by som, že nás Taliani a mesto Siena zvlášť tak trochu diskriminujú. Napokon sme našli solidárnu uličku bez diskriminačných značiek a dokonca blízko historického centra. Pár metrov od auta sa nám odkryl tento pohľad na najstarší Siensky kostol San Francesko. 
  
 Bolo treba len prekonať po schodoch maličké údolie a boli sme pri hradbách významného stredovekého mesta, ktoré bojovalo s Florenciou o vedúce postavenie v Toskánsku. Siena je jedinečná tým, že jej centrum včítane mestských hradieb sa zachovalo v podobe, v akej bolo v dobe rozkvetu v 13.-15. storočí. 
  
  
  
 Od hradieb sme vystúpili extrémne strmou rovnou uličkou a o chvíľu sme boli pri katedrále San Francesko. 
  
  
  
 Stredoveká katedrála má pôdorys tvaru T, drevený strop, v interiéri svetlošedé a tmavé pruhy, pôsobí moderne. 
  
  
 Siena sa rozprestiera na troch pahorkoch, uprostred ktorých leží Piazza del Campo, hlavné námestie mesta. Má tvar veľkej mušle, je vydláždené červenými tehlami a zvažuje sa smerom k radnici. Od radnice vedie červenou dlažbou osem bielych pásov, ktoré delia centrálnu časť námestia na 9 častí, symbolizujúcich radu deviatich, ktorá sa skladala z 9 miestnych obchodníkov a ktorá pred 700 rokmi mestu vládla. 
  
  
  
  
  
 Radnica je najmonumentálnejšou a najkrajšou budovou na námestí. 
  
  
  
  
  
 Natrafili sme aj na nejaké sobáše. 
  
  
  
 Veža Torre del Mangia, týčiaca sa 102 metrov nad radnicou, dominuje stredu mesta. 
  
  
  
 Výstup na vežu bol z technických príčin zatvorený, tak sme sa aspoň išli pozrieť do mestského múzea, ale tam sa zase nesmello fotiť. Fotiť sa ale na moje prekvapenie mohlo v slávnom sienskom Dóme, iba sa nesmel používať blesk. Siensky Dóm (Duomo di Santa Maria) sa týči na najvyššom pahorku. Stačí vystúpiť z najvyššieho bodu námestia uličkou o pár desiatok metrov vyššie a ste pri zadných vchodoch do chrámu. 
  
  
 Siensky Dóm bol postavený v 13. a 14. storočí, je 90 m dlhý a 24 m široký. Obišli sme ho z ľavej strany a uzreli sme prekrásne priečelie celé z bieleho, čierneho a červeného mramoru. 
  
  
  
  
  
 Vystáli sme si rad na lístky, vstupné do Dómu, Krypty, Baptistéria a troch múzeí stojí 12 eur, my sme sa rozhodli pre osobitné vstupné len do Dómu za 3 eura. Za pozornosť stojí mramorová dlažba Dómu s mnohými intarziami a obrazmi. Podlahu vykladali mramorom 200 rokov a vystriedalo sa pri tom 40 majstrov. 
  
  
  
  
  
  
 Celý interér Dómu je úchvatný. 
  
  
  
  
  
  
  
 Pred Dómom je námesie Piazza del Duomo. Na námestie naväzuje Piaza di Jacopo della Quercia. Toto námestie malo byť podľa pôvodných plánov hlavnou loďou obrovského kostola, ktorý nikdy nebol dokončený. Ukončená bola len tá časť (90x24m), ktorú ste doteraz spolu so mnou obdivovali. Z nerealizovanej hlavnej lode kostola sa zachovalal múr priečelia a v týchto priestoroch je dnes aj Dómske múzeum. 
  
  
  
 Sienu sme opúšťali plní dojmov z veľmi dobre zachovanej originálnej stredovekej architektúry. Bola sobota 5. júna, posledný deň nášho pobytu v Toskánsku. Do večera sme ešte chceli stihnúť niečo z Toskánskeho vidieka, ale o tom už nabudúce. 

