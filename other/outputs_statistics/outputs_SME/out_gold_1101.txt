

 bizovňe še čuduješ, že po teľo rokoch še ozývam. Ale obecala som dzifčeťu, že ci napíšem. Keľo je to už rokov, čo ce ochabila? Das pejc? Možno i vecej budze. 
 Jak še hutorí, čas najlepší dochtor, šicke rany zahojí. I mne še už noha zahojela, čo si ma raz ožartý zdrilil z garadyčoch. 
 Teraz už znám, že to veľká chyba bula, kedz som še k vám do domu nascehovala. Do šickoho som še starala a ľem mojej Ruženke som stranu trimala. Musel si poriadne cerpec. 
 Nečudo, že si si občas čerknul betehy a neraz pozdno do večera v karčme šedzel. Vedz každý poriadny chlop sebe vypije, no ňe? Dakto ľem dakus, iný vecej, no a ty si vypil vecej. Ale ani še ci nedzivím. Ta vedz tá moja Ružena poriadna ľompa bula. Vidzela mi, jak še o tebe stará! Na frištik chľeba so šmaľcom, na obid si harčok trepanky musel vylygac, dobre si hvarel, že chucí jako koňské šiky. Na večir ci uvarela kendericu, alebo ľem postné bandurky si mal. A kedz še jej zochcelo na poľo zajsť, tak i hryžacu dziňu si dostal. Vedz ani poriadny kavej ňeznala uvaric, chucil jako krochmaľ. Veru nerada še okolo šporheltu obracala. Ľem by še parádzila a hodiny pred džveredlom vystávala. A ani čistotná bárs nebula. Brudny budar vyčuchac? Ta dze! Prac ani pigľovac še jej nechcelo, aj gráty si musel sám umyvac. No, piľná pčola to nebula, do roboty še bárs nešarpala. 
 Ale ani ja ci nebula bárs dobrou švekrou. Právom si každému vo valale hvarel, jak ci tá stará rapucha ničí život. Šľepé sme buli, nevidzeli sme jak cerpíš. Keľo razy som sebe želala, aby me guta ulapela za to, že som še k vám nascehovala. Verabože, šaľena ja bula. Šicko mohlo buc inak. 
 Ale vidzíš, drahý žecu, že ani po teľo rokoch sme na tebe nazapomnuli. Ruženka hutorí, že si ce predstavuje s veľkym bembeľom, určite ti už kosci nečerkajú. A že by še rada stala nadraguľou. Už še jej i snívalo o malym kandratym bauľe. 
 Ňemala ce ochabic, teraz to už zná i ona. 
 Jak v tebe zostalo choľem falatočok ľubosci, friško odpíš, budzeme ci blahoslavic. Vedz stará láska nehardzavie, jak še hutorí. Naša kapura budze pre tebe stále otvorená. 
 
 Tvoja švekra Amála 
 Dodanok: 
 Skoro by mi zapomnula. K tomu džegpotu, co ši ulapel v lote, ti zo šerca blahoželáme. 
 
 

