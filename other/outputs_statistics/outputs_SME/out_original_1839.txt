
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roland Cagáň
                                        &gt;
                Nezaradené
                     
                 Milujem ťa, lebo sa na teba hnevám (šlabikár emócií) 

        
            
                                    5.11.2009
            o
            15:40
                        |
            Karma článku:
                12.43
            |
            Prečítané 
            2819-krát
                    
         
     
         
             

                 
                    V praxi sa stretávam s dvoma extrémami vzťahu k cíteniu – sú ľudia, ktorí by boli radšej, keby prežívanie neexistovalo a na druhej strane sú ľudia, ktorí by boli bez emócii úplne stratení. Väčšina z nás sa situačne pohybuje medzi týmito dvoma pólmi. Ale týkaju sa nás všetkých. Načo sú nám city?
                 

                     Sme smutní, lebo plačeme?   Bez ohľadu na to, či chceme emócie naplno prežívať, alebo úplne potlačiť, jedno je isté - musíme sa s nimi zaoberať. Nemusíme vedieť, či je prvé myslenie alebo cítenie, v podstate sa vedci ešte nedohodli, či plačeme preto, že sme smutní, alebo sme smutní, pretože plačeme. Napríklad: idete v noci po tmavej ulici a za sebou počujete kroky - čo je prvé? Myšlienka, že to môže byť násilník, alebo tlkot srdca? Je myšlienka z tlkotu srdca, alebo je zrýchlené dýchanie z myšlienky?   Musíme však, a to považujú skoro všetci za základ emocionálnej inteligencie, emócie poznať a uvedomiť si ich. Pri tom mi párkrát pomohlo rozšírenie pohľadu na rozdelenie a význam emócií.   Psík je v tom možno ďalej   Pár šikovných psychológov si na pojme emocionálna inteligencia vybudovalo svetovú slávu, stovky vzdelávacích firiem to úspešne predávajú firmám a lídrom, počujeme to ako novú mantru úspechu a šťastia v živote. Jednotná teória, napriek mnohým snahám nejestvuje a tak si vyberiem jednoduchú teóriu, evolučnú. Budeme predpokladať, že emócie tu boli skôr ako hlava, (určite boli a stále sú pri nižších druhoch života – psíčkari odpusťte) a vystačíme si zo štyrmi základnými emóciami. Každá z nich z niečoho vznikla a niečo prirodzene signalizuje, ale rád by som ponúkol na zamyslenie aj iné významy, ktoré by ste možno v emočnom šlabikári nenašli.   Strach – mobilizácia energie na zmenu   Pravdepodobne evolučne prvá emócia. Vznikol z ohrozenia života a to je stále pozorovateľné u batoliat, dospelé strachy sú skôr naučené. V súčasnosti strach z ohrozenia nášho status quo, či zmeny celkovo (pozorovateľné u ľudí, ktorí preferujú hoci aj nevyhovujúci stav, lebo je známy). Za veľkou časťou takzvanej lenivosti môže byť strach z neznáma a práce na zmene. Mobilizuje organizmus, pripravuje ho na akciu (často býva výsledkom strachu agresia), respektíve redukuje smútok za  možnými stratami.   Smútok – ochrana energie do budúcnosti   Liečiaca reakcia organizmu na stratu, prispôsobuje novému stavu. Našťastie sa mu nedá  vyhnúť. V prípade, že ho po hocijakej strate (hoci aj ideálu alebo sna) necítime, tak sme ešte len nezačali smútiť. Uvádza organizmus do mierne spomaleného stavu, chvalabohu aj nižšej odolnosti na podnety, aby mu umožnil zbieranie energie do budúcnosti. Až depresia niekedy prinúti ľudí, aby sa na chvíľku zastavili.   Hnev – ohmatávanie hraníc a zbližovanie   Pôvodný význam slova agresia je „a gredi“ – pristúpiť k niekomu. Evolučne - správanie, keď nám niekto prekročil hranice (napríklad pomliaždenie hranice nosu alebo zmenšenie hraníc nášho políčka pred jaskyňou). V súčasnosti ide skôr o hranice psychické, respektíve reakcia na frustráciu z nesplnenia našich túžob a potrieb – a teda jasnejšie a silnejšie vyjadrenie toho, čo potrebujem. Veľmi zanedbávaná emócia vo vzťahoch, všeobecne označovaná za negatívnu, ale podľa Karla Frielingsdorfa (Agrese vytváří vztahy, 2000) dokonca vitálna sila, ktorá vzťahy buduje. (odbočka pre nefreudovcov: na prienik penisu do vagíny je tiež agresia nevyhnutná, existuje výskum, ktorý potvrdzuje pozitívnu koreláciu medzi vášňou a hádkami.)   Radosť – odbúravanie   Túto netreba rozoberať nikomu, že? Štvrtá základná emócia, ale jej nedostatočné prežitie môže byť tiež na škodu. Nie je samoúčelné, že sa pri extrémnej radosti chvejeme alebo plačeme.   Ak šlabikár – dá sa toto učiť?   Keď si predstavíme modelovú hodinu výučby emócií na ZŠ, možno by mi stačilo, aby sme na začiatok zmenili pár stereotypov. Napríklad:   Chlapci plačú a môžu byť slabí.   Dievčatá sa môžu pekne nasrať.   Smútok nie je nutné potlačiť, ale prežívať.   Radosť nie je prednejšia a jediná, čo by sme mali sledovať v živote.   Hnev môže pomôcť zblížiť, nielen ublížiť.   Strach nás môže viesť a nie je zahanbujúci.   A emócie nemusíte vedieť hlavne ovládnuť, ale spoznať a žiť s nimi.   Ďakujem ľuďom, ktorí mi pomohli spoznať moje predsudky a málinko ľutujem, že to nebolo o tridsať rokov skôr.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Magická kuchyňa intímnych vzťahov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            1000 druhov plaču
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Samota, hnev, smrť a egoizmus. Vivat!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Scenáre nešťastných párov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Ani ten komplex menejcennosti nemám poriadny!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roland Cagáň
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roland Cagáň
            
         
        rolandcagan.blog.sme.sk (rss)
         
                        VIP
                             
     
        Psychológ, psychoterapeut, Modrý anjel na polovičný úväzok, zaujíma ma človečina z akejkoľvek stránky. Chcem sa podeliť o myšlienky, ktoré mi víria hlavou, keď sa s mojimi klientmi učím byť človekom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3107
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




