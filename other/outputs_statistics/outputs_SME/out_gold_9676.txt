

 Deň pred voľbami (11. júna 2010) minister životného prostredia zverejnil výzvu na predkladanie projektov protipovodňovej ochrany (http://www.opzp.sk/downloadfile/vyzvy/opzp-po2-10-1/00-vyzva-opzp-po2-10-1.pdf), ktorá síce hovorí o prevencii pred povodňami, ale uchádzačmi môžu byť len správcovia vodných tokov. V praxi to znamená, že sa budú naďalej regulovať potoky, ktoré síce v danej obci dokážu zabrániť vyliatiu povodňovej vlny, ale regulácia zhoršuje povodňové stavy na celom úseku pod reguláciou. 
 Ak sa nebudú spolupracovať na protipovodňovej ochrane aj majitelia či správcovia lesných, poľnohospodárskych i urbannych  pozemkov, obce v údoliach sa budú musieť pripraviť na ďalšie katastrofálne povodne. Je nenormálne, aby vedenie štátu ohrozovalo svojich ľudí! Do protipovodňovej ochrany musia byť zapojení všetci, aj lesníci, aj poľnohospodári, aj majitelia či správcovia akýchkoľvek plôch, aby na svojich pozemkoch zadržiavali dažďovú vodu, či už hradením bystrín, budovaním odrážok na cestách v lesnej a poľnohospodárskej krajine, budovaním malých vodných plôch, rybníkov, protipožiarnych a závlahových nádrží, zberných nádrží na zbieranie dažďovej vody zo spevnených plôch miest a obcí. Všetky tieto opatrenia významným spôsobom prispievajú k znižovaniu povodňových rizík. 
 Ponúkam ďalší smutný príbeh odumierajúceho lesa na Kysuciach, kde zahryznutý lykožrút do dreva kántri smrekové porasty, po ktorých ostávajú holé pláne pripravené na samovývoj povodní. Okrem ťažby lesa sa v danej lokality nerobí nič. V údolí pod vyholeným lesom sa nachádza najväčšia dedina na Slovensku Oščadnica, ktorá už pár krát sa pokúšala získať finančné zdroje na komplexnú integrovanú ochranu vôd, no neúspešné, lebo nespĺňa kritériá. V tom istom čase bolo na protipovodňovú ochranu bolo zrealizovaných 13 projektov a všetky na reguláciu potoku, ktoré prispievajú k rizikám povodní na rieke Kysuce i Váh. 
  
  
  
  
  
  
  
  
 PS: 
 Keď Nemecké Bavorsko postihla v roku 1988 veterná smršť, padlo viac ako 70 mil. kubíkov dreva. Vtedy nemeckí lesníci, ochranári aj politici spolupracovali a vymysleli program podpory ekologizácie lesov v celom Nemecku a federálna vláda financovala zo štátneho rozpočtu 10.000 Nemeckých mariek na jeden hektár. A tak sa od roku 1990 vysádzali poškodené lesy ekologický a dokonca aj monokultúrne dospelé smrekové porasty po celom Nemecku sa jeden deň vyťažili a na druhý deň ekologický vysádzali. Tým predišli lykožrútovej katastrofe. U nás sa ochranári s lesníkmi hádali, kto má silnejšie lakte, a politici ako truhlici nenabrali odvahu zásadne sa rozhodnúť, ako predísť umieraniu lesov. 
   

