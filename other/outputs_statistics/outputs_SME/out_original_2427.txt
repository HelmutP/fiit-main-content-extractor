
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Beáta Blaženiaková
                                        &gt;
                Svet okolo mňa
                     
                 Robíš hanbu tomuto národu a tomuto štátu!!!! 

        
            
                                    7.3.2010
            o
            0:58
                        (upravené
                10.2.2011
                o
                13:28)
                        |
            Karma článku:
                24.71
            |
            Prečítané 
            6597-krát
                    
         
     
         
             

                 
                    A prečo rovno nezastavíme v pondelok o 12.00 celú republiku, nezastavíme uprostred ulice, uprostred lesa v momente, keď slnce máme nad hlavou a prečo všetci nahlas nespievame hymnu?
                 

                 Jano,   robíš mojej krajine hanbu. Hovorím mojej, pretože sa ako Slovák cítim a som na to hrdá. Napriek tomu, že si nastavil nulový štandard morálky, hodnôt a etiky, mi nezabrániš, aby som túto krajinu menej milovala. Cítim vlastenectvo a cítim aj národné povedomie. A vieš prečo, vieš prečo ho vôbec cítim?   Lebo táto krajina, nech to v súčasnosti tak vôbec nevyzerá, je bohatá duchom a kultúrou, je bohatá na ľudí, ktorí robia dobré meno tejto krajine doma a v zahraničí. Ty a tebe podobní "hejslováci" ste však opakom všetkého, čo na tejto krajine milujem. Ste tým, čím moje deti nikdy nebudú, i keď ty robíš všetko pre to, aby táto krajina bola na tvoj obraz.   Ľudia musia mať impulz k tomu, aby niečo tak silné ako je vlastenectvo cítili. Musia na to mať dôvod. A keď ja sa musím pozerať, ako ty nadávaš žene slovom najhorším, ako štveš ľudí v tejto krajine proti sebe, ako ráno chodíš na mol ožratý z parlamentu a vládna limuzína, kúpená našimi peniazmi, je využívana na tieto účely, keď vidím ako kradneš a už to ani neskrývaš, tak sa to snažím prekusnúť. Zo slova národný si spravil slovo nacionálny a z národa slovenského si spravil národ buranov.   Urážaš ma, keď reprezentuješ moju krajinu, urážaš ma, keď sa tváriš, že robíš niečo pozitívne pre Slovensko. Mám právo ti to povedať, nie preto, že mám slovenskú národnosť, mám právo ti to povedať ako každý jeden občan tejto republiky.   Nemáš rád Maďarov a nemáš rád ani Cigánov. Cigáni sú podľa teba ľudia, ktorí kradnú a nepracujú. No potom si Cigánom aj ty. Tiež nepracuješ a tiež kradneš a kradneš viac než ktokoľvek iný bez ohľadu na svoj pôvod a kradneš mne a mojim deťom nádej, že v tejto krajine sa raz bude slušne a dobre žiť a že hymnu budú počúvať s rukou na srdci.   Je mi nadovšetko ľúto, že prvý raz v živote sa musím hanbiť za svoju krajinu, ktorú takto chceš reprezentovať ty. Hanbím sa za každého človeka, ktorý ti dá svoj voličský hlas (a teraz použijem slová jedného známeho z Facebooku, ktorý už po tom výroku nie je ani "známym"), "lebo Maďar pre Slovensko nikdy nič nespravil". No ja tvrdím, že každý Maďar žijúci na Slovensku, ktorý tu žije rád a slušne, bude vždy plnohodnotným občanom tohto štátu, bez ohľadu na to, či mu chceš nanútiť slovenskú hymnu alebo nie. Bez ohľadu na to, že si mal ešte tú absolútnu drzosť, chcieť donútiť niekoho, komu si nedal ani jeden jediný dôvod cítiť sa Slovákom, aby skladal sľub vernosti štátu!       A vieš čo ešte? Ja na srdci ruku mám pri slovenskej hymne a to mi žiadny zákon nebude prikazovať, či ju mám počúvať a kedy ju mám počúvať. Vlastenectvo sa prikázať nedá, to musí človek cítiť, ale to ty nikdy nepochopíš. Ty a ani tvoji voliči.   Ja som za to, aby v triedach boli štátne vlajky a štátny symbol, ale to všetko sa stane až v momente, keď si budú ľudia vedomí toho, prečo to tam chcú mať a keď peniaze určené nám, občanom, nebudeš kradnúť ty a tvoji spolukoaliční zlodeji, ale namiesto toho naše deti dostanú učebnice, aby sa mali z čoho v škole učiť o našej krajine a svete, v ktorom žijú.   Vidíme sa pri voľbách, ale hlas slušných ľudí ty nedostaneš. Ja volím v júni v tomto štáte zmenu, využijem to jedinečné právo a teda  si nárokujem aj na to, nahlas ti povedať, že sa za teba hanbím, keď ty  na to sebareflexiu nemáš. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (92)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Blaženiaková 
                                        
                                            Neodchádzam, naopak, vraciam sa na Slovensko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Blaženiaková 
                                        
                                            SMER, ďakujem. Vážení učitelia a študenti - Vám ďakujem dvakrát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Blaženiaková 
                                        
                                            Telekom.....ja Vás prosím, veľmi, veľmi prosím......
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Blaženiaková 
                                        
                                            Slovenská vláda sa chystá vyvolávať dážď
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Blaženiaková 
                                        
                                            Normalizácia pod rúškom demokracie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Beáta Blaženiaková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Beáta Blaženiaková
            
         
        blazeniakova.blog.sme.sk (rss)
         
                                     
     
        Som človekom, ktorý veľa počúva a chce hovoriť, ak má čo povedať, ktorý pozná svoje hranice a vie ich prekročiť, ak je to nutné a ktorý sa neustále učí. Veď škola je život. Alebo život je škola?


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    36
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2647
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Každodenný život
                        
                     
                                     
                        
                            Svet okolo mňa
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štadión prežil vojny, ale asi neprežije developerov
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Alfréd Wetzler - Čo Dante nevidel
                                     
                                                                             
                                            Petržalka - Engerau - Ligetfalu
                                     
                                                                             
                                            Slovenské národné povstanie 1944
                                     
                                                                             
                                            Rudolf Vrba - Utekl jsem z Osvětimi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            tie roky 90-te
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bodaj by si páni z EU vytiahli konečne hlavy odtiaľ, kde ich majú
                     
                                                         
                       Hlavná stanica ako výsledok bezradnosti magistrátu
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Pozrimeže, kto sa to objavil v pozadí „veľkej daňovej lúpeže“
                     
                                                         
                       Neodchádzam, naopak, vraciam sa na Slovensko
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       TA3 vystrihla nepríjemné otázky novinárov na Fica
                     
                                                         
                       Slovenská hanba, nielen v Bruseli
                     
                                                         
                       Keď netečú peniaze správnym Smerom
                     
                                                         
                       Keď Fico opustí svoje jahniatka
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




