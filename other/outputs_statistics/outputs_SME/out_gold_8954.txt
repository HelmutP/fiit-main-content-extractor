

 
 „Chcel by som byť milionár, no keďže som z chudobnej rodiny, určite nemám šancu“. A tak sa ani nepokúsim uvažovať o tom, ako by sa to dalo. A tak sa ním nestanem. 
 „Chcel by som krásnu partnerku, ale mám krivý nos a žiadna pekná žena by ma nechcela“. Tak sa na ne ani nepozriem. A strácam šancu. 
 
 „Rád by som spravil kariéru, ale pri mojom šéfovi asi nemám žiadnu šancu“. Kašlem na to. Neskúsim to. 
 Takto myslíme často, lebo to na prvý pohľad vyzerá logicky. Niečo chcem, ale je tu prekážka. Keby nebolo prekážky, tak by som to mal. Vzdám sa však pokusov prekonať prekážku, pretože neverím, že to môžem zvládnuť. 
 Žijeme tak, ako chceme žiť 
 To, čo v živote mám, som si spôsobil ja sám. Prijať tento fakt je pre mnohých ľudí ťažké, hlavne, ak nie sú spokojní s tým, čo „v živote majú“. Ak totiž nie som spokojný s tým čo mám, je nepríjemné priznať si, že sa tak stalo mojím pričinením. Pohodlnejšie je obviniť niekoho iného.  
 
 Príklad. Jozef je už roky zamestnaný na zle platenom pracovnom mieste. Má pocit, že nízky plat a veľa práce sú okolnosti, ktoré on sám už nemôže nijako ovplyvniť. Ale v skutočnosti ich nielenže ovplyvňuje, ale priamo spoluvytvára. On nastúpil na zle platené miesto. On súhlasil s tým, že svoj životný čas bude predávať za málo peňazí. On súhlasil s tým, že po skončení školy sa už prestal vzdelávať. On nie je obeťou okolností. On tú situáciu akceptoval a potom celkom prestal hľadať inú možnosť. On prestal hľadať východisko zo situácie, s ktorou je nespokojný. On zostáva na zle platenom mieste a tým akceptuje nízky plat pravdepodobne aj do budúcnosti. To je jeho podpis ako autora vlastného života. 
 Príklad. Viera má 120 kíl a stredný vek na krku. Tvrdí, že nikdy nechcela byť tučná. Ale roky sa pozerala do zrkadla a videla, ako priberá. Súhlasila s tým, pretože keby jej naozaj záležalo na tom, aby bola štíhla, hľadala by účinný spôsob, ako priberanie zastaviť. To ona jedla koláče, klobásy a iné nevhodné jedlá. Vedela, že sú nevhodné. Je to všeobecne známe. Každým jedným sústom tak vydávala súhlas s tým, že v budúcnosti bude o niečo tučnejšia ako práve teraz. 
 
 Komplexy menejcennosti ako príčina neúspechu 
 Komplex menejcennosti je súbor pocitov ovplyvňujúcich správanie človeka. 
 To, ako žijeme, závisí od toho, ako sami o sebe zmýšľame. Stávame sa takými, za akých sa sami považujeme. My sme súčasťou aktuálneho diania, reality. Spoluvytvárame ju spolu s ostatnými ľuďmi a vecami. Ak sa považujeme za slabých, budeme ako slabí aj konať. Slabý neočakáva od seba veľa. A tak dostane málo, pretože sa uspokojí s málom. 
 Človek, ktorý má komplexy menejcennosti a chcel by napríklad schudnúť, uvažuje takto: „Chcel by som schudnúť, ale to ja určite nedokážem. Som nedisciplinovaný, ne­odo­lám čokoláde, a vôbec… cvičenie ma nebaví. Nemám silnú vôľu“. Asi takto uvažuje človek, ktorý si neverí. Podľa tohto sebahodnotenia sa neskôr aj zariadi. Bude sa podľa toho aj správať. 
 Ak už vopred neverím v možný úspech, tak načo sa potom vôbec snažiť? 
 Keď si dovolíte myslieť o sebe, že ste neschopní, tým si sami sebe vystavujete potvrdenie, že budete konať, ako si zvyčajne počínajú neschopní. Opakovane sa tým len programujete na neschopnosť. 
 Iste aj vy poznáte ľudí, ktorí si každú chvíľu povedia výrok typu: 
 
 Nič sa mi nedarí. 
 Nemám pevnú vôľu. 
 Nikdy nedosiahnem to, čo chcem. 
 Som smoliar. 
 Často som chorý. 
 Neviem si nájsť dobrú prácu. 
 
 Takéto výroky sú celkom zrozumiteľné. Neraz s nimi začali už rodičia, alebo vychovávatelia, keď opakovane tvrdili: 
 
 Ty si ale nešikovný. 
 Z teba nič dobrého nebude. 
 Tak toto určite nedokážeš. 
 Kto by bol na teba zvedavý? 
 
 Je jedno, či takéto krátke výroky povieme nahlas, alebo si ich hovoríme len v duchu sami pre seba. Dokonca nie je dôležité ani to, či sú pravdivé. Všetky takéto hodnotiace výroky však programujú naše sebahodnotenie. 
 
 Ak sa správate podľa predstavy „som chudobný“, dostanete viac chudoby, dostanete viac neschopnosti. 
 Ak si často hovoríte, že ste človek, čo má stále málo peňazí, budete mať stále menej peňazí. Nepokúsite sa naučiť získať viac, alebo sporiť. 
 
 Ak máte pocit, že ste slabí, nebudete sa púšťať do ťažkých úloh. Potom si na život s malými nárokmi na seba zvyknete a vaše schopnosti ešte viac ochabnú. 
 A tak komplexy menejcennosti, alebo pochybnosti o sebe a svojich schopnostiach trvale regulujú vaše každodenné konanie. To sa neskôr zhmotní do menej kvalitného životného štýlu. Je to podobné, ako prírodný zákon. Platí trvale. 
 Nabudúce sa pozrieme na to, ako vykrmujeme svoj mozog. 

