
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniela Nemčoková
                                        &gt;
                postrehy
                     
                 Moja kríza má krízu 

        
            
                                    9.4.2010
            o
            7:53
                        |
            Karma článku:
                10.92
            |
            Prečítané 
            1466-krát
                    
         
     
         
             

                 
                    Všade naokolo „straší" nejaká kríza. Ale keď počujem pojem „kríza stredného veku", rozmýšľam, čo to môže byť a pre istotu sa opýtam detí, sú študovanejšie, možno mi to vysvetlia...
                 

                 Vysvetľujú ... Ale ja aj tak neviem, o čom hovoria. Možno majú na mysli tých ušomraných štyridsiatnikov, čo majú vraj zbabraný život, neveria už ničomu a nikomu a celý svet je „naruby". Úprimne - aj ja som si mnohé veci vo svojom živote trochu inak predstavovala. Áno, život je ťažký, ale je aj krásny. Najlepšia vec, ktorá sa mi mohla stať sú moje deti... Tak dobre, nezískala som tituly, nevybudovala som si kariéru, nemám vplyvných známych a ani tučný účet v banke... Jediným titulom, ktorým sa môžem chváliť je titul „MAMA", namiesto kariéry mám množstvo životných skúseností; namiesto vplyvných známych niekoľko veľmi dobrých priateľov, na ktorých sa môžem spoľahnúť. A peniaze? Mám ich alebo nemám. Kúpim si za ne vari pokoj, lásku, zdravie alebo šťastie?   Viete, čo sa mi na mojom veku páči? Nie som ani príliš mladá ani príliš stará, taká akurát. Keď chatujem, preberám jazyk mladých. Rada sa učím novým veciam a objavujem nepoznané. Vtedy sa cítim mlado. Alebo keď sa rozprávam a smejem  so svojimi veľkými deťmi... Moje veľké dcéry rady hodnotia a vylepšujú môj imidž.... Taktiež keď sedím na pieskovisku so svojimi malými deťmi medzi mladými mamičkami. No áno, cítim sa o kúsok skúsenejšia od nich, ale vek - ten nech mi radšej nik neháda...   Keď zabúdam, alebo už tak nevládzem ako niekedy, poviem - „starnem". Je to pravda a pomáha to aj ako výhovorka. Deti si už zvykli, že keď niekde ideme, musím sa ešte dva-trikrát po niečo vrátiť. Stáva sa mi tiež (ale priznajte sa, že aj vám sa to už stalo), že idem do obchodu pre jednu vec, kúpim ďalších päť a tu jednu zabudnem...   Nedávno som predviedla doma „ekonomické pranie". Nevšimla som si totiž, že mám práčku nadstavenú na plákanie a oprala som takto dve dávky prádla. Keď už všetka bielizeň visela na balkóne a ja som dala do práčky ďalšiu, zrazu mi to došlo, prečo práčka tak rýchlo doprala a bielizeň nevonia ako zvyčajne... No čo, tak aspoň tá posledná dávka bude trojnásobne čistejšia (pretože zakaždým som dala nový prášok na pranie). Potom som už len sledovala práčku, či nadmerne nepení a krútila som hlavou sama nad sebou...   Podaria sa mi aj iné kúsky. Napríklad pri varení... Neposolená polievka sa dá ľahko napraviť, ale niekedy svojou zábudlivosťou vytváram nové jedlá a recepty. Naposledy som zabudla dať do plnenej papriky ryžu. Robila som to v časovej tiesni a ani ochutnať som to nestihla. Až keď som prišla z práce a najedla som sa, rozmýšľam, čo s tým je... Keď som na to prišla, opýtala som sa detí, či im obed chutil. Dcéra si pochvaľovala, že lepšie ako inokedy. Syn skonštatoval, že je to akési iné, ale dalo sa to zjesť. Keď som sa im priznala, akú chybu som urobila, tak sa len smiali... Takže aj o zábavu máme postarané...   Je výhodou byť raz mladá a inokedy stará. Veď aj deti počujete hovoriť, že sú malé alebo veľké - presne tak ako to potrebujú...   Krízu stredného veku vraj spôsobuje aj odchod detí z domu. Ale to predsa patrí k životu. Nemôžem ich stále vodiť za ručičku. Prišiel čas ukázať, či som ich niečo naučila. Ak zvládnu dospelácke veci, som na ne pyšná. A keď ma už nepotrebujú? Nehovorím, že to občas nezabolí, ale viem, že si musia budovať vlastný život. A keď ich vidím, že sú šťastné, tak som aj ja.   Veď keď boli malé, nemala som na nič čas. Od rána do večera len deti. Teraz sa mi začínajú otvárať nové možnosti a môžem myslieť aj na seba. Plním si svoje sny... Niekedy sa pýtam seba samej, či nie som sebecká, ale zdá sa, že deťom moje aktivity neprekážajú, práve naopak. Sú radi, keď ma vidia usmiatu.   Čas plynie veľmi rýchlo a neviem, či som ešte v tej prvej, ale asi skôr v tej druhej polovici života. Viem si vážiť hodnotu, ktorou je čas. A preto chcem žiť naplno využívajúc každú minútu...    No povedzte, nemá tento vek osobité čaro? Alebo už ku vám zavítala tiež voľajaká kríza? Neviem... ale dúfam, že mňa obíde... A že tá moja kríza má krízu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Oplatí sa bojovať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Prípad rozviazaných šnúrok a poľského kamionistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Kráska z internetu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Bolesť - nepríjemná spoločníčka, dobrá učiteľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            "Dnes slnko vyšlo aj pre mňa"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniela Nemčoková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniela Nemčoková
            
         
        nemcokova.blog.sme.sk (rss)
         
                                     
     
        Som obyčajná žena-matka veľkej rodiny.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    117
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            deti
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            osobné príbehy
                        
                     
                                     
                        
                            príbehy iných
                        
                     
                                     
                        
                            socializmus
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Huljak
                                     
                                                                             
                                            Adriana Bagová
                                     
                                                                             
                                            Pavol Biely
                                     
                                                                             
                                            Ivana O Deen
                                     
                                                                             
                                            Marcela Bagínová
                                     
                                                                             
                                            Marek Ševčík
                                     
                                                                             
                                            Mária Kohutiarová
                                     
                                                                             
                                            Emília Katriňáková
                                     
                                                                             
                                            Peter Zákutný
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Oplatí sa bojovať
                     
                                                         
                       Babka, neseď stále za tým Facebookom
                     
                                                         
                       Prípad rozviazaných šnúrok a poľského kamionistu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




