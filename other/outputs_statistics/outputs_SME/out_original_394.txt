
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Vek na získanie vodičského oprávnenia - komentár k návrhu zákona 

        
            
                                    12.1.2008
            o
            17:52
                        |
            Karma článku:
                10.47
            |
            Prečítané 
            7452-krát
                    
         
     
         
             

                 
                    Principiálne sa nemení podmienka veku na získanie vodičského oprávnenia i keď zmenou zaradenia jednotlivých kategórií vozidiel dochádza k určitým posunom.
                 

                 
  
   Nastávajú dve zmeny v definovaní kategórií.  1, U motoriek kde sa vo veku 18 rokov dá získať oprávnenie až do výkonu motora 25kW alebo 0,16kW/kg.  2, V kategórii AM návrh obmedzuje maximálnu hmotnosť na 350 kg, čo doteraz nebolo.  3, V kategórii B1 návrh nešťastným spôsobom zavádza dvojaký strop hmotnosti. Doteraz bolo 550 kg a návrh zavádza 400 kg a v prípade prepravy tovaru 550 kg.   Osobne sa mi nepáči práve ta definícia "tovaru". Lebo je nejednoznačná. Zoberte si príklad, že 17 ročný brigádnik bude na takomto vozidle dohliadať nad čistotou v areáli. Pokiaľ bude voziť tovar, napríklad čistiace prostriedky tak môže mať hmotnosť do 550 kg, ale ale budú tieto čistiace prostriedky pre neho naradím a nie tovarom, tak môže mať hmotnosť len do 400 kg.   Nerozdeľoval by som kategóriu na 400 kg a 550 kg, ale nechal len 550 kg, tak ako je to väčšinou v zahraničí i keď sú štáty kde to rozdeľujú. Ak je to z nejakého neznámeho dôvodu nutné rozdeliť, tak by som zmenil slovo "tovar" na "náklad". Ak by vozidlo mohlo okrem osôb prevážať aj náklad, napríklad golfové palice, alebo brašnu s náradím ap., tak by mohlo mať pohotovostnú hmotnosť až 550 kg   Doteraz je vek na získanie oprávnenia na vedenie motoriek a ľahkých vozidiel nasledovný:     
  Kategória Popis Vek   AM jedno a dvoj stope vozidlo do 45 km/hod 15 rokov   B1 iné ako motorka do 550 kg 16 rokov   A1 motorka do 125 cm3 a do 11kW 16 rokov   T traktor a pod. 17 rokov   B iné ako motorka a traktor, nad 550 kg  18 rokov   A motorka nad 125 cm3 21 rokov, alebo 2 roky od A1     Návrh zákona uvádza:    § 78 Vek na udelenie vodičského oprávnenia a vedenie motorových vozidiel      (1) Minimálny vek na udelenie vodičského oprávnenia je  a) 15 rokov pre žiadateľa o udelenie vodičského oprávnenia skupiny AM,  b) 16 rokov pre žiadateľa o udelenie vodičského oprávnenia podskupiny A1 a B1,  c) 17 rokov pre žiadateľa o udelenie vodičského oprávnenia skupiny T,  d) 18 rokov pre žiadateľa o udelenie vodičského oprávnenia skupiny A na vedenie motorového vozidla s výkonom motora nepresahujúcim 25 kW alebo s pomerom výkon/ najväčšia prípustná celková hmotnosť nepresahujúcim 0,16 kW/kg alebo motocykla s postranným vozíkom s pomerom výkon/ najväčšia prípustná celková hmotnosť nepresahujúcim 0,16 kW/kg, skupiny B, B+E, C, C+E a podskupiny C1 a C1+E,   e) 21 rokov pre žiadateľa o udelenie vodičského oprávnenia skupiny A bez obmedzení podľa písm. d), D, D+E a podskupiny D1 a D1+E,   (2) Osobe ťažko zdravotne postihnutej možno udeliť vodičské oprávnenie skupiny B a skupiny B+E, ak dovŕšila vek 17 rokov.      § 75 Rozsah a členenie skupín a podskupín motorových vozidiel    (2) Do podskupiny motorových vozidiel AM patria  a) dvojkolesové vozidlá alebo trojkolesové vozidlá s najväčšou konštrukčnou rýchlosťou 25 až 45 km.h-1, ktoré sú charakterizované  1. v prípade dvojkolesového vozidla motorom, ktorého zdvihový objem valcov nepresahuje 50 cm3 v prípade spaľovacieho motora alebo ktorého najväčší trvalý menovitý výkon nie je väčší ako 4 kW v prípade elektrického motora a  2. v prípade trojkolesového vozidla motorom, ktorého zdvihový objem valcov nepresahuje 50 cm3 v prípade zážihového motora, najväčší čistý výkon nie je väčší ako 4 kW v prípade spaľovacieho motora alebo ktorého najväčší trvalý menovitý výkon nie je väčší ako 4 kW v prípade elektrického motora a  b) štvorkolesové motorové vozidlá, ktorých prevádzková hmotnosť je menšia ako 350 kg bez hmotnosti batérií v prípade elektrických vozidiel, ktorých najväčšia konštrukčná rýchlosť nepresahuje 45 km.h-1, a ktorých  1. zdvihový objem motora nepresahuje 50 cm3 v prípade zážihových motorov,  2. najväčší čistý výkon nie je väčší ako 4 kW v prípade iných spaľovacích motorov, alebo  3. najväčší trvalý menovitý výkon nie je väčší ako 4 kW v prípade elektrického motora.  (4) Do podskupiny motorových vozidiel B1 patria  a) motorové vozidlá s tromi symetricky usporiadanými kolesami, ktorých zdvihový objem valcov motora je väčší ako 50 cm3 v prípade spaľovacieho motora alebo ktorých najväčšia konštrukčná rýchlosť je vyššia ako 45 km.h-1 a  b) motorové vozidlá so štyrmi kolesami, ktorých   1. prevádzková hmotnosť je 350 kg až 400 kg bez hmotnosti batérií v prípade elektrických vozidiel; u vozidiel na prepravu tovaru 350 kg až 550 kg,  2. najväčšia konštrukčná rýchlosť presahuje 45 km.h-1,   3. ktorých zdvihový objem valcov presahuje 50 cm3 v prípade zážihových motorov, najväčší čistý výkon je väčší ako 4 kW v prípade iných spaľovacích motorov, alebo ktorých najväčší trvalý menovitý výkon je 4 kW až 15 kW v prípade elektrického motora.      Doteraz platí vyhláška 225/2004 Z. z.  § 15 Skupiny a podskupiny motorových vozidiel  (5) Podskupina B1  trojkolesové motorové vozidlá, štvorkolesové motorové vozidlá a pásové motorové vozidlá skupiny B, ktorých najvyššia povolená rýchlosť prevyšuje 45 km.h-1, s ktorýmkoľvek druhom pohonu a s pohonom spaľovacím motorom s objemom valcov presahujúcim 50 cm3, alebo sú poháňané akýmkoľvek iným zariadením dosahujúcim rovnaké technické údaje. Pohotovostná hmotnosť týchto vozidiel nesmie presahovať 550 kg; do pohotovostnej hmotnosti vozidla poháňaného elektromotorom sa nezapočítava hmotnosť akumulátorov.  (15) Skupina AM  dvojkolesové motorové vozidlá, trojkolesové motorové vozidlá, štvorkolesové motorové vozidlá a pásové motorové vozidlá s najvyššou povolenou rýchlosťou neprevyšujúcou 45 km.h-1, s akýmkoľvek druhom pohonu a s pohonom spaľovacím motorom s objemom valcov nepresahujúcim 50 cm3 okrem bicyklov dodatočne vybavených pomocným motorčekom s pohonom spaľovacím motorom s objemom valcov nepresahujúcim 50 cm3 a s najvyššou povolenou rýchlosťou neprevyšujúcou 30 km.h-1.    Z vyššie uvedeného dôvodu navrhujem zmenu textu návrhu zákona  z:   § 75 Rozsah a členenie skupín a podskupín motorových vozidiel  4) Do podskupiny motorových vozidiel B1 patria  a) motorové vozidlá s tromi symetricky usporiadanými kolesami, ktorých zdvihový objem valcov motora je väčší ako 50 cm3 v prípade spaľovacieho motora alebo ktorých najväčšia konštrukčná rýchlosť je vyššia ako 45 km.h-1 a  b) motorové vozidlá so štyrmi kolesami, ktorých   1. prevádzková hmotnosť je 350 kg až 400 kg bez hmotnosti batérií v prípade elektrických vozidiel; u vozidiel na prepravu tovaru 350 kg až 550 kg,  2. najväčšia konštrukčná rýchlosť presahuje 45 km.h-1,   3. ktorých zdvihový objem valcov presahuje 50 cm3 v prípade zážihových motorov, najväčší čistý výkon je väčší ako 4 kW v prípade iných spaľovacích motorov, alebo ktorých najväčší trvalý menovitý výkon je 4 kW až 15 kW v prípade elektrického motora.   na:   § 75 Rozsah a členenie skupín a podskupín motorových vozidiel  4) Do podskupiny motorových vozidiel B1 patria  a) motorové vozidlá s tromi symetricky usporiadanými kolesami, ktorých zdvihový objem valcov motora je väčší ako 50 cm3 v prípade spaľovacieho motora alebo ktorých najväčšia konštrukčná rýchlosť je vyššia ako 45 km.h-1 a  b) motorové vozidlá so štyrmi kolesami, ktorých   1. prevádzková hmotnosť je 350 kg až 550 kg bez hmotnosti batérií v prípade elektrických vozidiel.  2. najväčšia konštrukčná rýchlosť presahuje 45 km.h-1,   3. ktorých zdvihový objem valcov presahuje 50 cm3 v prípade zážihových motorov, najväčší čistý výkon je väčší ako 4 kW v prípade iných spaľovacích motorov, alebo ktorých najväčší trvalý menovitý výkon je 4 kW až 15 kW v prípade elektrického motora. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




