

 O čo ide: 
Národný systém dopravných informácii - má byť systém, 
ktorý bude spracovávať a poskytovať aktuálne informácie najmä vodičom a správcom komunikácii, také informácie aby doprava bola plynulá, bezpečná a efektívna pri pohľade najrôznejších hľadísk. Napríklad jeden z jeho výstupov má byť aj posielanie informácií cez TMC do navigačných prístrojov vodičov.  

  Ako znie zadanie: 
2.1 Názov predmetu súťaže návrhov: „Dokumentácia pre realizáciu projektu Národného systému dopravných informácií“ (ďalej len „NSDI“) 
2.2 Podrobný opis predmetu súťaže návrhov, vrátane príloh, 
tvorí časť súťažných podmienok B.1 - „Opis predmetu súťaže návrhov“. 
2.3 Nomenklatúra - Spoločný slovník obstarávania (CPV) 
Hlavný predmet:  79421200-3 Vypracovanie projektov iných 
ako pre stavebné práce 
Doplňujúce predmety: 48614000-5 Systémy zberu údajov, 60100000-9 Služby cestnej dopravy, 72316000-3 Analýza údajov 


  Čo je v hre?: 
3.1 Verejná súťaž podľa zákona o verejnom obstarávaní 
3.2 Súťaž návrhov je súčasťou postupu vedúceho k zadaniu 
zákazky na poskytnutie služby tomu účastníkovi, ktorého porota vyberie ako 
víťazného, t. j. účastníkovi, ktorého návrh bude na 1. mieste. S víťazným 
účastníkom bude uzavretá zmluva rokovacím konaním bez zverejnenia v súlade s § 
58 ods. 1 písm. h) a § 59 zákona o verejnom obstarávaní. 


  Aká je výhra súťaže? 
23.1 Vyhlasovateľ udelí za víťazný návrh účastníkovi, ktorý 
tento návrh predložil cenu v hodnote 500,- €. 
23.2 Vyhlasovateľ udelí odmenu v hodnote 100,- € každému 
účastníkovi, ktorý splnil súťažné podmienky, ale jeho návrh porota nevybrala ako víťazný. 
23.3 Ceny a odmeny účastníkom, ktorí spĺňajú náležitosti 
podľa bodu 23.1 a 23.2 vyplatí Vyhlasovateľ bezhotovostným prevodom do 2 mesiacov odo dňa oznámenia 
výsledku účastníkom. 

  Termíny: 
4.1 Lehota na získanie súťažných podmienok 12. 03. 2010 do 14.00 hod. 
4.2 Lehota na požiadanie vysvetlení 23. 03. 2010 
4.3 Lehota na predkladanie návrhov 06. 04. 2010 do 10.00 hod 
4.4 Predpokladaná lehota na vyhodnotenie súťaže návrhov 03. 05. 2010 
4.5 Lehota na vyplatenie cien a odmien do 2 mesiacov odo dňa zaslania oznámenia 
výsledku účastníkom 

  Čo som si o tom povodne myslel? 
Rozumný krok MDPT, že najprv vyhlási súťaž návrhov, kde 
každý môže predložiť svoj návrh, vrátane výskumných pracovísk, vysokých škôl, 
študentov a podobne a tak systém dopravných informácii môže byť oveľa 
dokonalejší než v susedných krajinách, lebo môže byť pred pripravený na 
technológie, ktoré sú dnes len v štádiu vývoja a experimentálneho testovania. 

  V čom je problém? 
Problém je v tom, že do súťaže návrhov (nie dodávky 
systému) sa nemôže zapojiť každý. Je tam podmienka, ktorá diskvalifikuje väčšinu 
záujemcov o podanie návrhu: 
2.1.1 referenciu k dvom úspešne zrealizovaným zmluvám v období predchádzajúcich troch rokov (2007 – 2009) s rovnakým alebo obdobným predmetom zmluvy ako je predmet súťaže návrhov v minimálnej hodnote zmluvnej ceny 1 500 000,- € bez DPH pre každú zmluvu osobitne; obdobným predmetom zmluvy sa rozumejú informačné systémy v oblasti dopravy, systémy na riadenie dopravy, systémy na zber, distribúciu a zobrazovanie informácií v oblasti dopravy. 

  Záver: 
Týmito podmienkami sa prakticky zúžil okruh pre podanie návrhu len na spoločnosti, ktoré dodali dva takéto systémy a zrátate ich na prstoch jednej ruky. 

