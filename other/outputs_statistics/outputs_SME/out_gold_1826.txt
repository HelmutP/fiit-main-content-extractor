

 Najprv metrológia. Každý merací pristroj má určitú chybu merania. Niektoré chyby merania sú fixne niektoré závisle na nameranej hodnote. Polícia používa rôzne merače rýchlosti. Najčastejšie majú maximálnu chybu merania +/- 3 km/hod do nameranej rýchlosti 100 km/hod a od nej smerom hore sú to  +/- 3% z nameranej hodnoty. Samozrejme, že každé úradné meradlo musí mať platný certifikát. 


 Ak je u úseku maximálna povolená rýchlosti 50 km/hod a policajný radar a vám namerá rýchlosť 53 km/hod a polícia vás chce pokutovať za rýchlosť, tak má význam požadovať odpočítanie chyby meradla. 

 To isté platí ak na diaľnici, kde je minimálna rýchlosť 80 km/hod. 
Ak pôjdete za dobrého počasia ako prvé vozidlo v kolóne, policajný radar vám namerá rýchlosť 77 km/hod a polícia vás chce pokutovať na nedodržanie minimálnej rýchlosti, tak má význam požadovať pripočítanie chyby meradla. 

 Správne sa to robí tak, že nameraný udaj sa koriguje o odchýlku do plusu alebo mínusu o chybu meradla, podľa toho či sa garantuje minimálna alebo 
maximálne rýchlosť, výsledný údaj sa matematicky zaokrúhli na rovnaký rád ako 
je predpis a porovná s predpisom. 


 Pozrime sa ako je stanovená výška pokút. 

 Zákon 8/2009 Z. z. zadefinoval akékoľvek prekročenie rýchlosti ako závažný dopravný priestupok z čoho pramení aj vysoká pokuta, až 650 € v hotovosti a 800 € v správnom konaní. 
Čiže pokutu môžete dostať pri preukázane vyššej alebo nižšej rýchlosti ako je dovolená už od 1 km/hod, s tým, že matematické zaokrúhlenie hrá v prospech vás. Lenže v praxi dopravní policajti nepripočítavajú a neodpočítavajú odchýlku ale zaujímajú sa len o vozidlá ktorých 
rýchlosť je zaručene za hranicou maximálnej chyby meracieho prístroja. 

 Na jemnejšie kategorizáciu výšky pokuty existuje sadzobník pokút, ktorý je ale len informatívny materiál MVSR. V ňom je uvedené: 





 v obci 


 1. od 6 – 10 km/h 10 		€/301,26 Sk 


 2. od 11 – 20 km/h od 		30 €/903,78 Sk do 50 €/1.506,30 Sk 


 3. od 21 - 30 km/h od 		50 €/1.506,30 Sk do 100 €/3.012,60 Sk 


 4. od 31 - 40 km/h od 		100 €/3.012,60 Sk do 200 €/6.025,20 Sk 


 5. od 41 - 50 km/h od 		200 €/6.025,20 Sk do 300 €/9.037,80 Sk 


 6. od 51 - 60 km/h od 		300 €/9.037,80 Sk do 400 €/12.050,40 Sk 


 7. od 61 - 70 km/h od 		400 €/12.050,40 Sk do 500 €/15.063,00 Sk 


 8. o viac ako 71 km/h 		od 500 €/15.063,00 Sk do 650 €/19.581,90 Sk 



 mimo obec 


 1. od 11 – 20 km/h od 		20 €/602,52 Sk do 30 €/903,78 Sk 


 2. od 21 - 30 km/h od 		50 €/1.506,30 Sk do 100 €/3.012,60 Sk 


 3. od 31 - 40 km/h od 		100 €/3.012,60 Sk do 200 €/6.025,20 Sk 


 4. od 41 - 50 km/h od 		200 €/6.025,20 Sk do 300 €/9.037,80 Sk 


 5. od 51 - 60 km/h od 		300 €/9.037,80 Sk do 400 €/12.050,40 Sk 


 6. od 61 - 70 km/h od 		400 €/12.050,40 Sk do 500 €/15.063,00 Sk 


 7. o viac ako 71 km/h 		od 500 €/15.063,00 Sk do 650 €/19.581,90 Sk 




 Ak si pozorne pozriete ako je definovaný sadzobní pokút, 
zistíte, že tam nie je prekročenie od 1 km/hod ale až od 6 km/hod v obci a 11 
km/hod mimo obce. Z tohto je možné usúdiť, že sadzobník bol navrhnutý tak aby sa 
nemusela odpočítavať chyba meradla a údaje sa vzťahujú na nameranú rýchlosť bez 
odpočítania chyby meradla. 

 Ak v obci kde je mpr. 50 km/hod vám namerajú  56 km/hod tak 
nemá význam hádať sa na chybe meradla, lebo v skutočnosti vaša skutočná rýchlosť 
bola od 53 km/hod do 59 km/hod, čiže ide o jednoznačné porušenie predpisu. 

 To isté platí  mimo obce ak na mpr. 130 km/hod vám namerajú 
141 km/hod, tak vaša skutočná rýchlosť bola od 136,77 km/hod do 145,23 km/hod, 
čiže tiež ide o jednoznačné porušenie predpisu. 

 Záver: 
Žiadať odpočítanie/pripočítanie chyby meradla má význam len pri nedodržaní 
rýchlosti nie väčšej ako je maximálna odchýlka meradla. Treba mať na pamäti 
aj to, že sadzobník pokút je len orientačný a navyše je v ňom už zahrnutá chyba 
meradla.  
 Čiže treba si hľadať iné viac či menej akceptovateľné zdôvodnenie nedodržanie rýchlosti jazdy, kreativite sa medze nekladú. 

