

 Zimná olympiáda priniesla na Slovensko viac radosti, ako asi očakával každý, aj ten najoptimistickejší športový fanúšik. Kompletná medailová zbierka a pre hokejových fanúšikov hlavne fantastické vystúpenie našej reprezentácie vliali radosť do našich sŕdc. Olympiáda sa však skončila a pálčivá otázka hokejovej budúcnosti maličkého, ale vždy silného Slovenska je späť. Tak ako sa jedno oko tešilo z krásneho predstavenia našich reprezentantov, druhé, nielen odborné, ale aj fanúšikovské muselo utratiť nejednu slzu. Ani nie preto, že sme nezískali medailu - štvrté miesto na najlepšie obsadenom turnaji na svete je úžasný výsledok - ale skôr preto, že to bolo naposledy, čo sme videli naše národné mužstvo v takomto zložení. Odchody Švehlu, Bondru, či Cígera do hokejového dôchodku preboleli, pretože tu bola kvalitatívna náhrada. Lenže odchody Pálffyho, Stümpela, Šatana, Demitru, Štrbáka a ďalších už asi tak ľahko nestrávime. Jedna silná generácia sa lúči s dresom s dvojkrížom na prsiach. Či už teraz, vo Vancouvri, alebo o rok na domácej pôde. 
 Budúcnosť našej reprezentácie je veľmi otázna. Talenty sú, ale práca s mládežou je nedostatočná. Aj keď badať postupné zlepšenia, kompetentných čaká ešte veľa práce. O výchove hráčov v dorasteneckom, či juniorskom veku je nezainteresovanému ťažko sa vyjadrovať. Určite je treba zapracovať na systéme mládežníckeho hokeja, lákať deti do hokejových prípravok, podporovať talentované deti z nemajetných rodín... To všetko je úloha nielen zväzu, ale aj štátu, ktorý sa vo vyspelých hokejových krajinách podieľa na financovaní mládeže oveľa väčšou mierou. 
 Kontinualita by sa však mala sledovať aj medzi našimi reprezentačnými mužstvami vyššej vekovej kategórie - dvadsiatky a „A" mužstva. Účasť reprezentácie do 20 rokov v extralige má - napriek mnohým kritickým hlasom - opodstatnenie. Nielen minuloročné štvrté miesto na majstrovstvách sveta, ale aj čoraz väčší počet odchovancom púchovského mužstva, ktorí sa presadzujú vo svojich materských kluboch. Je otázkou, či by sa Štajnoch, Hudáček, či Gašparovič presadzovali tak, ako teraz aj bez skorého účinkovania medzi mužmi v drese HK Orange. Z toho dôvodu by mal byť tento projekt zachovaný aj pre nasledujúce ročníky. 
 Ďalším krokom k udržaniu nášho „A" mužstva vo svetovej sedmičke by mohla byť reprezentácia do 23 rokov, ktorá by slúžila hlavne ako prechodový stupeň pre najtalentovanejších hokejistov z dvadsiatky. Mali by možnosť konfrontovať svoje schopnosti na medzinárodnom fóre a zápasy so súpermi ako Maďarsko, Taliansko, Francúzsko či Slovinsko by im mohli v ich hokejovom raste veľmi pomôcť. Mladí hráči, ktorí ešte nedosiahli kvality „A" mužstva by tak nestratili kontakt s medzinárodným hokejom a mohli sa skôr dostať do povedomia manažérov z kvalitnejších zahraničných súťaží - KHL či Elitserien. 
 Vrcholom tohto trojuholníka by bol „A" team, v ktorom by sa počas sezóny vykryštalizovala základná kostra našej reprezentácie na majstrovstvá sveta. „A" Team by mal hrať zhruba v rovnakom zložení z najkvalitnejších slovenských hokejistov pôsobiacich v Európe. Hráči by boli oveľa zohratejší, čím by mužstvá  ako Švajčiarsko, Bielorusko či Lotyšsko stratili veľkú časť svojej výhody, ktorá spočíva v klubovej či dlhodobej reprezentačnej zohratosti. Takto zostavený team by sa pred majstrovstvami sveta doplnil o kvalitatívne najlepších hokejistov zo zámoria. Počas sezóny by sa naopak mohol dopĺňať „odrastenými" hráčmi z reprezentácie do 23 rokov. 
 Takýto model určite nie je ideálny, možno z finančného a časového hľadiska ani nie je uskutočniteľný, ale určite stojí za to zamyslieť sa nad tým. Myslím si, že v súčasnej situácii v najúspešnejšom kolektívnom športe na Slovensku nie je žiadna myšlienka, ktorá vychádza z dobrého úmyslu, dopredu odpísaná. 
 A takto by mohli vyzerať naše reprezentácie: 
 „A" mužstvo: 
 Brankári: 
 Lašák, Staňa 
 Formácie: 
 Obšut, Baranka - Radivojevič, Cibák, Ružička 
 Lintner, Starosta - Zedník, Stümpel, Nagy 
 Podhradský, Sersen - Kolník, Bulík, Hossa 
 Graňák, Stehlík - Bartečko, Kukumberg, Bartovič 
 Reprezentácia do 23 rokov: 
 Brankári: 
 Konrád, Hudáček 
 Formácie: 
 Štajnoch, Valach - Tybor, Záborský, Gašparovič 
 Bohunický, Rais - Skokan, Hudáček, Bartánus 
 Gründling, Biro - Chovan, Slovák, Čaládi 
 Trefný, Deyl - Buc, Marcel Haščák, Lapšanský 
   

