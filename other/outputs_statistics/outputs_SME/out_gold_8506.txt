

 
  Myslím si, že to bol práve „kompakt“, ktorý niekedy v sedemdesiatych rokoch položil základy boomu dnešnej digitálnej fotografie, kedy sa fotí všetko a akosi už bez rozmyslu. Áno, fotiť vždy a všade bolo aj motto aj legendárnej firmy KODAK, ktorá prvá prišla so známym perforovaným filmom, alebo aj predvojnovej Leicy, ktorá dala základ kompaktným hľadáčikovým fotoaparátom. Najmä osemdesiate roky zažili tisíce rôznych lepších, či horších kompaktov a desiatky miliónov lepších, či horších fotografií. 
 
 Súčasný „digitálny spotrebiteľ“ sa môže s úškrnom spýtať: „A ako to vlastne fotilo?“ Nuž z vlastnej skúsenosti musím povedať, že dobre. Teda, aspoň niektoré z nich. Lepšie, ako mnohé dnešné kompakty digitálne. Jedným z mojich najobľúbenejších fotoaparátov, ktorý ma sprevádzal od alpských priesmykov až po čínsky múr, bol Canon Canonet 28. Jeden z posledných z generácie robustných kovových kompaktov, ktorého prvá verzia bola uvedená na trh v roku 1971. Hovorilo sa mu aj „Leica chudobných“.   
  
   
   
 Fotil som ním asi pätnásť rokov a až nedávno ho dcére niekde ukradli. Mal celkom dobrý a dostatočne sveteľný objektív, a napriek obmedzeniam, ktoré jeho „zvláštna“ expozičná automatika mala, dali sa s ním urobiť pekné fotografie: 
 
 
 Maličká dcéra Erika bola odfotená v roku 1978 pri svetle jedinej sviečky na film Ilford HP5, ručne vyvolaná a po 32 rokoch naskenovaná z pozitívu 13x18 cm.  
   
  
  

 V roku októbri 1981 som Canonetom vo Vrátnej nafotil tieto ovečky. Tiež na môj obľúbený Ilford a tiež som ich po rokoch naskenoval z ručne vyvolanej fotky až nedávno. Tu je možno fotku otvoriť na väčší rozmer: http://www.fotky.sme.sk/fotka/126706/stara-pohladnica-10 . Starý Plicka by mal radosť, nie? 
 
 
 Odvtedy prešla éra kinofilmových kompaktov a vypukla digitalizácia. Aj keď temer výlučne fotografujem klasickou analógovou zrkadlovkou, kompaktov som mal vtedy vždy „plné vrecká“. Bola to priam povinnosť, pretože so pracoval v jednej známej spoločnosti, ktorá filmy aj kompakty vyrábala.  Tak ako napredoval vývoj nových typov filmov, tak sa aj zlepšovali výsledky fotenia kompaktmi a zmenšovali ich rozmery. Snáď vrcholom techniky bolo obdobie APS systému, ktoré prinieslo prvú digitalizáciu záznamom informácií o fotografii a nastavení fotoaparátu na magnetickú stopu na filme. Málokto vie, že tieto informácie slúžili najmä na automatické nastavenie vyvolávacích strojov vo veľkovýrobe fotografií. Cieľom bolo vyťažiť z nafoteného filmu čo najviac dobrých fotiek a tým zvýšiť tržby. 

 Obdobie klasických kompaktov dávno skončilo, keď ma niekde na ebay upútala maličká APS kamera od Leicy. Jediný APS model, ktorý táto slávna značka iba krátke obdobie po roku 2000  vyrábala. Leica C11.  
  
   
 Myslím, že som chybu neurobil, keď som do nej investoval. Jej výborná optika dokáže vyťažiť z APS filmu maximum a príjemná kresba nezaprie kvalitu značky. Navyše nie je o veľa väčšia ako krabička cigariet a dá sa ľahko nosiť vždy a stále vo vrecku. Škoda iba, že APS filmy sa pomaly vytrácajú aj zo špecializovaných obchodov. Fotí naozaj pekne: 
 
 
 Táto skorá jesenná scenéria bola nafotená na APS film Kodak Advantix 400 a po vyvolaní naskenovaná priamo z filmu. Obrázok nie je nijako počítačovo upravený. Vo väčsom si ho môžete pozrieť tu: http://www.fotky.sme.sk/fotka/128129/starym-fotakom-4 . Pekne to kreslí, že? 

  
   
 Keď už sme pri kreslení, musím spomenúť kreatívnu fotografiu. Úmyselne nepoviem „fine art“, lebo ani po vyše štyridsiatich rokoch fotenia sa na takéto pomenovanie mojich fotiek necítim. Ale priznám sa, že mojou záľubou je impresionistická fotografia. Tak ma zaujímalo, čo na to APS kompakt? Odpoveď v podaní maličkej Leicy C11 je excelentná: 
 
 
 Opäť to bolo na Kodak Advantix 400. Fotené na dlhý čas, vtedy treba ten foťáčik trocha klamať, a pohnuté vertikálne počas expozície. Jedna z mojich najlepších impresionistických náladoviek teda vznikla starým kompaktom. Posúďte: http://www.fotky.sme.sk/fotka/128128/starym-fotakom-3 
 Čo záverom dodať, keď niet čo dodať? Niektoré staré kompakty neboli na zahodenie. A fotiť sa s nimi dalo, len bolo treba chcieť. A okrem dobrého svetla aj trocha vedieť. 
   
   
   
   

   
 
 foto: (C) Tibor Javor (Okrem obrázkov fotoaparátov, ktoré sú z internetu) 

