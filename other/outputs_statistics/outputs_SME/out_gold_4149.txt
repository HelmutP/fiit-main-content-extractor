

 Solidarita 
 
Pokým hluchý naťahuje uši 
za zvukmi nemého, čo kýcha,  
a zakrýva si rukou oči 
proti svetlu, ktoré si 
nedokáže predstaviť slepý, 
my sa snažíme ich nevnímať. 
Predčasne ukončená bytosť 
bez končatín kynie aspoň hlavou 
oblakom, z ktorých sa k nám 
znáša dážď a všetkých 
nás presviedča o tom, 
že sme ešte nestratili 
posledný kúsok citu. 
 
 
Chlad nás strasie 
z piedestálov a dáva 
nám na výber. 
Skryjeme sa sami,  
pred všetkými 
alebo pomôžeme 
aspoň niektorým 
zo zjavených 
neviditeľných. 
 
 
A možno schytíme 
studenú pištoľ, 
ktorá páli v dlani 
a rovno ich postrieľame. 
 
 
Pre ich vlastné dobro. 
 
 
A aj pre naše, 
čo nám majú čo 
kriviť zrak, 
drhnúť sluch, 
zavadzať... 
 
 
Ak ste, milí čitatelia, 
pri čítaní 
necítili nič, 
vaša ľahostajnosť 
predstavuje 
strašnejšiu zbraň, 
než je zmienená pištoľ. 
 
 
Ak ste však pocítili 
kvapky dažďa, 
máte šancu stať sa dáždnikom 
pre tých, ktorých bičujú 
viac ako Vás. 
 
 
Alebo strieľajte, ale  
sa už konečne prejavte! 
 
 
Nech vieme, na čom sme. 
 

