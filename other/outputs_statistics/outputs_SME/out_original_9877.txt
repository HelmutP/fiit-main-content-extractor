
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Burger
                                        &gt;
                Nakrátko
                     
                 Krátky článok o našom milom zdravotníctve 

        
            
                                    30.6.2010
            o
            10:58
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            2190-krát
                    
         
     
         
             

                 
                    Včera som sa v HN dočítal, že v USA stojí "chirurgický zákrok" vytiahnutie kliešťa 1200 eur (informácie pre turistov). Tá informácia ma šokovala. Aj touto cestou ďakujem pani pohotovostnej doktorke z nemocnice na ......... v Bratislave, ktorá mi ho vytiahla z pre mňa nedostupného miesta zadarmo :). Tak sa mi vidí, že naše zdravotníctvo nie je naozaj zaplatené a že je dokonca niekedy hlboko podhodnotené. Nehovorím, že 1200 eur je správna cesta. To nie. Ale jedno som si trochu uvedomil: berieme úplne ako samozrejmé, veci, ktoré samozrejmé vo svete nie sú. Zaplatíme poplatok a starajte sa... Neviem, koľko sa za zákrok u nás účtuje. Ale predpokladám, že podobne ako v mojom prípade, lekár ho ani nezapíše, ani možno nevypýta poisteneckú kartičku. Neviem ako Vy, ale mňa tá naša ľudskosť stále fascinuje... a nečudujem sa, že vo svete sú lekári naozaj zámožnými ľuďmi.
                 

                

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (58)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Svet Stefana Zweiga (tip na dve knihy)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Istanbul nie je iba Taksim (a ten nie je Gezi Park)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Hüzün
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Čo mi vlastne je (vie ľudový liečiteľ)?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Ako som mal strach (Kuala Lumpur a Singapur 1)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Burger
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Burger
            
         
        burger.blog.sme.sk (rss)
         
                                     
     
          
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    212
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3407
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje kecy
                        
                     
                                     
                        
                            Nakrátko
                        
                     
                                     
                        
                            Pokus o poéziu
                        
                     
                                     
                        
                            Rozprávanie z ciest
                        
                     
                                     
                        
                            Listy Štefana Bohunického
                        
                     
                                     
                        
                            Veselé povzdychy
                        
                     
                                     
                        
                            Nadlho
                        
                     
                                     
                        
                            Príbeh bývalého miništranta
                        
                     
                                     
                        
                            poviedka na nedeľu
                        
                     
                                     
                        
                            Fotografie-súkromné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Reč S. Krčméryho na súde
                                     
                                                                             
                                            Spomienka na Čiernu labuť
                                     
                                                                             
                                            Nikdy by som nebol veril
                                     
                                                                             
                                            Ako som dusil blogerky a piekol
                                     
                                                                             
                                            Môj New York III.
                                     
                                                                             
                                            Mamino lono
                                     
                                                                             
                                            Niečo o Coca Cole, hoteli Palace a o...
                                     
                                                                             
                                            Hitlera by trafil šľak
                                     
                                                                             
                                            Rozhovory Karola Sudora sú to najstrhujúcejšie na sme.sk
                                     
                                                                             
                                            RDB: Pôjdeš tam, neviem kam
                                     
                                                                             
                                            Alenka Technovská: Ľadové abstrakcie
                                     
                                                                             
                                            Moja videovizitka
                                     
                                                                             
                                            Barborka Bzdušková: Možno (ale dostala ma naozaj)
                                     
                                                                             
                                            Viktor DAHOME Holman: Kto som?
                                     
                                                                             
                                            Chen Lidka Liang: Možno mi nežiť v živote, nemožno mi ťa neľúbiť
                                     
                                                                             
                                            Miriam Novanská: Premeny ženy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Orhan Pamuk: Sníh
                                     
                                                                             
                                            Haruki Murakami: Sputnik, má láska
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Johnny Cash - God's Gonna Cut You Down
                                     
                                                                             
                                            Kill Bill - Strawberry Fields Forever
                                     
                                                                             
                                            The Beatles
                                     
                                                                             
                                            Happiness Is A Warm Gun z Across The Universe
                                     
                                                                             
                                            Underworld
                                     
                                                                             
                                            Portishead
                                     
                                                                             
                                            Bat For Lashes
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Beáta Grünmannová
                                     
                                                                             
                                            Jana Shemesh
                                     
                                                                             
                                            Helenka Fábryová
                                     
                                                                             
                                            Kika Kováčiková
                                     
                                                                             
                                            Adriana Didi Markovičová
                                     
                                                                             
                                            Miriam Reid
                                     
                                                                             
                                            Magduška Kotulová
                                     
                                                                             
                                            Katka Bagoči
                                     
                                                                             
                                            Samo Marec
                                     
                                                                             
                                            Mika (Mirkaa!) Polohová
                                     
                                                                             
                                            Janko K. Myšľanov+
                                     
                                                                             
                                            Kamila
                                     
                                                                             
                                            Juro a Apollo
                                     
                                                                             
                                            Katka Maliková
                                     
                                                                             
                                            Natalka Blahová
                                     
                                                                             
                                            Jarka Mikušová
                                     
                                                                             
                                            Miro Šedivý
                                     
                                                                             
                                            Andy Sekanová
                                     
                                                                             
                                            Bett Mateová
                                     
                                                                             
                                            Chen Lidka Liang &amp;#26753;&amp;#26216;
                                     
                                                                             
                                            Nataška Holinová
                                     
                                                                             
                                            Gabina Weissová
                                     
                                                                             
                                            Roman Daniel Baránek
                                     
                                                                             
                                            Danka Sudorová
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Hlas od sporáka
                                     
                                                                             
                                            O všetkom aj o ničom
                                     
                                                                             
                                            Sieťovka
                                     
                                                                             
                                            MHD
                                     
                                                                             
                                            Weather report
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                     
                                                         
                       Ktorý November by radšej oslavovali komunisti?
                     
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Nesto a jeho štyri manželky
                     
                                                         
                       Toto má byť akože umenie?
                     
                                                         
                       Chceli sme sa vrátiť domov, ale…
                     
                                                         
                       V knižnici
                     
                                                         
                       Ako zarobiť na onkologickom pacientovi
                     
                                                         
                       Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                     
                                                         
                       Bratislavská žumpa blafuje
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




