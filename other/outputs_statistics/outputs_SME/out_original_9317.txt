
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Romana Dériková
                                        &gt;
                Isn´t this ironic?
                     
                 Usmej sa na svojho cudzinca! Okamžite! 

        
            
                                    21.6.2010
            o
            12:43
                        (upravené
                21.6.2010
                o
                12:49)
                        |
            Karma článku:
                8.56
            |
            Prečítané 
            733-krát
                    
         
     
         
             

                 
                    Zo začiatku som sa to všetko snažila brať s nadhľadom. Veď si len nadávajte, búchajte sa do hrude a frflite na všetko, čo nie je naše a vraj nám chce byť vnútené a kradnúť nám tým tú našu slovenskosť. Však sa to nikto nedozvie, sme malí, v celosvetovom merítku bezvýznamní a vlastne o nás aj tak nikto nevie. Však ono to je celkom aj zábavná podívaná, keď v roku 2010 (v období európskej integrácie a búraní hraníc) ľudia v krčme s hranatými ksichtami a pivom v ruke vykrikujú, že nás treba brániť a rozčuľujú sa, že reku ako, Slovensko si nedáme
                 

                 Ja som si cestovala svetom, usmievala sa na ľudí a počúvala vety o tom, že som síce jediná Slovenka, ktorú zatiaľ stretli, ale usudzujú teda, že Slováci musia byť isto milí, otvorení, zvedaví, nápomocní a tak sa na Slovensko prídu pozrieť.  Pravdu vám do očí potom chrstnú až vaši bratia.  Nie je žiadnym tajomstvom, že bývam v Česku. Študujem, pracujem, vyskytujem sa, poskakujem, vybavujem, tlačím sa do projektov a tak.  "Nelíbi se mi myšlenka spoplatnění studia, ale když už, tak prostě Slovákům okamžitě, vždyť kolik je jich tady? Si to vem, ne, prostě, kolik Čechů studuje na Slovensku a kolik Slováků je tady? Za naše peníze."  Kozička moja drahá, vieš, medzi Českom a Slovenskom existuje určitá bilaterálna dohoda o tom, že Slováci sú v Česku za peniaze slovenských daňových poplatníkov a naopak. Jednoducho, že študujeme ako keby za tých istých podmienok, čo by sme mali doma. Preto môžem aj po slovensky, vieš? Ale nejdem sa rozčuľovať, už som si zvykla, budem držať hubu a nepoviem ani slovo. Nech si pofrfle. (áno, viem, že ak by sa štúdium spoplatnilo, tak to budem platiť ja. to len, ak by mal niekto chuť mi to začať vysvetľovať)  "A prostě, jak se k nám chovaj... Chápeš, přijedeš na Slovensko, deš jim tam dělat kšefty, zajedeš si do podniku, do ňákyho rekreačního střediska a tak dále a oni se k tebe chovaj naprosto hnusně, jenom proto, že seš Čech..."  Klopím hlavu dole a hanbím sa... toto je pravda.  "A ani se neusmějou, nic. Fandí nám oni vůbec, když hrajem hokej, tak, jak fandíme my jim?"  Nie, všetci sa škodoradostne bavia na tom, keď prehráte a tešia sa z toho, že sú lepší ako vy. Hanbím sa ešte viac za svojich tam doma na Slovensku.  "A měla sem spolubydlu Slovenu a ona snad schválně mluvila s přízvukem, kerý neznám, jenom abych nerozuměla nebo co..."  A bolo toho viac. A počúvala som to dosť dlho. A musím poznamenať, že úplne chápem, prečo sa z nás tak rozčuľujú.  A ešte to diskusné fórum, čo založil jeden doktor na stránke našej univerzity... Že keď my máme na Slovensku jazykový zákon, tak by sme snáď principiálne mohli pochopiť, ak oni budú za zrušenie používania slovenského jazyka na českých univerzitách. Veď je to založené na tom istom princípe. Jednoducho český jazyk je predsa štátnym jazykom, tak a basta fidli.  Vyzeráme smiešno. Doma sa búchame do hrude, všetko cudzie odhadzujeme, zakladáme si na tom "našom", ktoré mnohí Slováci ani poriadne nepoznajú. Že čo je to vlastne to naše. V zahraničí nám robia ústupky, dávajú nám možnosti a my si doma lebedíme zadky, mastíme bruchá a nevieme pochopiť, že to s tým naším prístupom asi nebude trvať večne.  Prosím každého jedného z vás. Buďte milí na cudzincov na Slovensku, ak nechcete aby nás čoskoro začali odvšadiaľ vyháňať a škaredo na nás zazerať a nedávať nám možnosti len preto, že sme zatrpknutí Slováci, ktorí si cudzie domov nepustia, ale chcú, aby všade inde boli uznávaní a prijímaní.  Je to smutnosmiešne. Celé. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Tekvicové kadečo v období tekvicových orgií.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Raňajky sú to najdôležitejšie!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Zápal močových ciest a štátnice? Och, áno.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            "Mám džuge.": Ako ma deti učia cigánčine.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Milí rodičia!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Romana Dériková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Romana Dériková
            
         
        derikova.blog.sme.sk (rss)
         
                        VIP
                             
     
         moja mama hovorí, že sa lesknem ako koňovi gule. 

 sedlák v meste. študent. barista. čašníčka. učiteľka angličtiny v nízkoprahu armády spásy. dobrovoľník v sociálnych službách. nehorázne sarkastický trúd. náhodný cestovateľ, stopovateľ. a sem tam o tom všetkom rada niečo zdelím. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    69
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1291
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            rmn the cestovateľ
                        
                     
                                     
                        
                            Isn´t this ironic?
                        
                     
                                     
                        
                            Romanka kuchtí
                        
                     
                                     
                        
                            Stretla som...
                        
                     
                                     
                        
                            Zápisky z frontov
                        
                     
                                     
                        
                            Cítim
                        
                     
                                     
                        
                            London
                        
                     
                                     
                        
                            This is music
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            F. M. Dostojevskij - Uražení a ponížení
                                     
                                                                             
                                            Ingmar Bergmann - Filmové povídky
                                     
                                                                             
                                            Sjón - Syn stínu
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            last.fm štatistiky, ak by to niekoho zajímalo
                                     
                                                                             
                                            Swans
                                     
                                                                             
                                            Connan Mockasin
                                     
                                                                             
                                            Kid Koala
                                     
                                                                             
                                            Roots Manuva
                                     
                                                                             
                                            Nine Inch Nails
                                     
                                                                             
                                            Chaozz
                                     
                                                                             
                                            The Horrors
                                     
                                                                             
                                            The Good, The Bad &amp; The Queen
                                     
                                                                             
                                            Feist
                                     
                                                                             
                                            Fionna Apple
                                     
                                                                             
                                            Erik Truffaz
                                     
                                                                             
                                            Bonobo
                                     
                                                                             
                                            Alice In Chains
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            žoviálny VKTR
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tekvicové kadečo v období tekvicových orgií.
                     
                                                         
                       Bratislavské momentky - fotografie
                     
                                                         
                       Raňajky sú to najdôležitejšie!
                     
                                                         
                       Zápal močových ciest a štátnice? Och, áno.
                     
                                                         
                       O falošných tónoch na maturitnom koncerte
                     
                                                         
                       Je Čaplovič blbý, alebo nie?
                     
                                                         
                       mala som štyridsať, bolo dobre. teraz mám dvadsať a pubertu
                     
                                                         
                       "Mám džuge.": Ako ma deti učia cigánčine.
                     
                                                         
                       Sarajevo (fotografie)
                     
                                                         
                       Tallinn (fotografie)
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




