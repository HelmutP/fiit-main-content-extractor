

 Nie každého to baví, ale tentoraz je to zaujímavé. Teda čítať blogy samotné a diskusie pod nimi. Pokiaľ sa týka blogov, tak pomer textov zdôvodňujúcich akýmkoľvek spôsobom boj za práva homosexuálov oproti tým príspevkom, ktoré sa snažia či už racionálne, doktrinálne, alebo emocionálne odhaliť a pomenovať skutočné i vzdialenejšie ciele homosexuálnej komunity a polemizovať s nimi, tak doteraz mi vychádza pomer tak asi 1:10. V prospech počtu príspevkov podporujúcich aktivity homosexuálnej komunity.  
   
   
 V diskusiách je to ešte zaujímavejšie. Ktokoľvek, keď čo len jediným písmenkom vyjadrí názor inklinujúci k uprednostňovaniu klasického  (heterosexuálneho) modelu usporiadania spoločnosti je okamžite kameňovaný spŕškou reakcií  diskutérov z prevažne homosexuálnej komunity, ktorým statočne sekundujú tí, čo si hovoria liberáli. Temer akoby sa jednalo o volebnú kampaň a administrátori majú čoraz viac práce s mazaním nekorektných príspevkov. Ak v rovnakom duchu dopadne aj plánovaný happening, či pochod, tak sa homosexuálna komunita zdiskredituje úplne. 
   
 Ako som vo svojom predchádzajúcom blogu napísal, osobne nemám s homosexuálmi žiaden problém. S čím mám však problém je exhibicionizmus, ktorého sa často táto komunita dopúšťa. Avšak aj to sa dá pochopiť. A mali by to pochopiť a uvedomiť si aj oni sami. Homosexuálna komunita, teda aspoň podľa toho, čo je navonok vidno, sa stáva čoraz viac a viac „heterofóbnou“. Pociťujú, zdá sa, akýsi neidentifikovateľný pocit ohrozenia zo strany väčšiny štandardne orientovaných. (Nepoviem „normálne orientovaných“, aby som nebol politicky nekorektný.) Pociťujú, že ich identita je, napriek „comming-outom“ mnohých z nich, minoritná.  Začínajú cítiť čoraz väčšiu averziu voči tomu, čo nie je „inaké“. Homosexuálna komunita začína byť čoraz viac „heterofóbna“. Carl Gustav Jung by iste prijal toto potvrdenie svojich teórií v priamom prenose.  
   
 Homosexuálna komunita, alebo prinajmenšom niektorí z nich, začína v normálne orientovaných ľuďoch nenávidieť samých seba. Nenávidia v nich svoju „inakosť“a dávajú heterosexuálom akoby za vinu, že sú takí, akí sú a „zákonnou“ cestou si chcú sami sebe dokázať, že ich minorita je predsalen normálna. Veru, Jung by sa tešil. Mne je ich úprimne ľúto...   
   
   
   
   
 Pravdepodobne pre môj predchádzajúci blog ma administrátori vyradili z výberu. Nečudo...  
 
 
 

 foto: (C) Tibor Javor 

