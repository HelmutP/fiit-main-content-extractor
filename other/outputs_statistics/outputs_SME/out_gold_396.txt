

 Vážení čitatelia, 
 na stránkach kultúry ste sa stretávali s príspevkami zahraničného autora Edwina Grassmeiera. Edwin Grassmeier bol pseudonym. Šlo o človeka, ktorý sa ponúkol, že nám bude posielať príspevky, ale len pod pseudonymom, pretože, ako povedal, je spolupracovníkom agentúry DPA. Išlo o správy, zriedkavejšie recenzie, a najčastejšie rozhovory. Pri rozhovoroch vždy uvádzal, že nie sú exkluzívne, ale že sú to záznamy z tlačových besied alebo online diskusií. Šlo spravidla o rozhovory z festivalov alebo z turné, preto nám nenapadlo overovať si ich autenticitu. Pred niekoľkými dňami autor webovej stránky Slovak Press Watch upozornil na to, že niektoré články, publikované pod menom Grassmeier, sa stotožňujú alebo takmer stotožňujú s niektorými článkami, ktoré vyšli v Spiegel Online. 
 Je nám úprimne ľúto, že sa to stalo, a chceli by sme sa verejne ospravedlniť čitateľom i autorom kultúrnej redakcie Spiegel Online. 
 ANDREA PUKOVÁ, vedúca oddelenia kultúry MARTA FRIŠOVÁ 
 Stanovisko šéfredaktora 
 Kopírovanie textov z iných médií bez uverejnenia zdroja je vždy vážnym porušením etických noriem. Bez ohľadu na moje presvedčenie, že išlo z našej strany o neúmyselné zlyhanie, denník SME tieto normy porušil. Je mi to veľmi ľúto a čitateľom sa za to ospravedlňujem. 
 Považujem to zároveň za poučnú skúsenosť, ktorá bude do budúcnosti zárukou, že podobné zlyhanie sa už nebude opakovať. 
 Redakciu Spiegel Online v Hamburgu sme osobne informovali o tom, čo sa stalo, a naše ospravedlnenie prijala bez výhrad. 
 Andrea Puková ma požiadala o uvoľnenie z funkcie vedúcej oddelenia kultúry. Jej žiadosti som vyhovel. 
 Martin M. Šimečka, šéfredaktor denníka SME 

