
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Mihalik
                                        &gt;
                Nezaradené
                     
                 Nesmeli ani plakať... 

        
            
                                    23.9.2009
            o
            22:37
                        |
            Karma článku:
                12.45
            |
            Prečítané 
            2732-krát
                    
         
     
         
             

                 
                    Jurij 8 r., Nataška 6 r. , Jelka 8., Oľa 7 r., Oleg 9 r.
                 

                 
no commentwikipedia
   Krčili sa pod stolom. Báli sa a vlastne ani nevedeli čoho. Nepoznali v rukách ľudí, ktorí na nich kričali nič, čo by im mohlo ublížiť. "Ako sú srandovne oblečení", pomyslel si Oleg a pozeral na čudne zamaskovaného muža v čiernej kombinéze, ktorý mal okolo tela akési valčeky a v ruke držal pušku. Tú Oleg už videl. Avšak to bolo v televízii, no teraz mal strach. Tak ako všetci v miestnosti. Nataška kľačala vedľa neho a chcela mu čosi pošepnúť, chcela sa ho opýtať, kedy pôjdu už domov. Bolo jej zima a mama ju určite bude už hľadať. Postavila sa a chcela odísť. Muž, na ktorého hľadeľ Oleg k nej pristúpil a pažbou samopalu AK-47 jej jediným pohybom rozsekol čeľusť a Nataška sa skĺzla k zemi. Tichý plač počuli všetci v miestnosti... Trvalo celých 5 minút, kým z rán na tvári malého dievčatka vytiekla takmer všetka krv a ona upadla do nenávratna... Oleg chcel strašne plakať, ale bál sa čo i len nahlas dýchať. Vonku začalo čosi strašne búchať a v miestnosti nastal zmätok. Muž v čiernej kombinéze ťahal telo Natašky za nohu po dlážke k východu, nechávajúc za ňou dvadsaťcentimetrov hrubú šmuhu od krvi. Jelke prišlo zle. Sedela na stoličke v kraji miestnosti a bola tam úplne sama. Vedela, že sa nesmie ani len pohnúť, ale keď vonku začal rachot, tak sa rýchlo presunula pod stôl a tam ostala najbližšiu minútu. Po tých šesťdesiatich sekundách dopadol na vrch stola valček, ktorý tam odhodil muž v čiernej kombinéze. Čosi nezrozumiteľne zakričal a Oleg pochopil, že to nie je valček, ale granát, ktorý v nasledujúcej sekunde roztrhal Jelku na kusy. V tej chvíli už Oľa, ani Jurij, ktorí tiež kľačali pod stolíkom, na ktorom si ešte pre chvíľou kreslili nevydržali a vstali. Neboli však jediný. Do miestnosti vtrhli ľudia. Ďalší ľudia s puškami. "Snáď nás zachránia," pomysleli si deti. Áno, tí ľudia ich prišli zachrániť. No muž v čiernej kombinéze i ďalší dvaja jeho "kamaráti" mali iné úmysly. Keď sa postavili prvé deti, začali po nich strieľať. Samopalu AK-47, ktorého rýchlosť guľky po vypálení je viac než 700 m/s, neunikol Oleg, ani za ním stojaci Jurij. Jediná guľka im prerazila hrudný kôš a Oli zlomila ľavú nohu. Oleg zomrel okamžite, Jurij ležal na zemi a plakal. Nevládal nahlas vzlykať, roztrhané pľúca mu nedávali ani dúšok vzduchu, iba plakal a triasol sa. Bál sa smrti, ale vlastne túžil, aby prišla čo najskôr. Prišla ešte skôr, ako sa k nemu priplazila Oľa a pohladila ho. Vlastne to ešte cítil.... Oľu strašne bolela noha, no nechcela zomrieť a tak sa tak aspoň tvárila. Ležala nehybne na zakrvavenom Jurjivom tele, kým jediným výstrelom do zátylku neukončil život muž v čiernej kombinéze... Na dlážke ležalo 5 mŕtvych detí, ktoré nedostali šancu na život, ani na dôstojnú smrť. Keď statný ruský vojak vynášal telo Ole von, plakal. Celú cestu si ju tútil k hrudi, akoby dúfal, že ju dokáže oživiť. Dokázal iba plakať...       Severné Osetsko, Beslan, september 2004       Česť pamiatke všetkých zabitých detí! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (58)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Až mi smutno prišlo...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Vietor fúka na Montmartri
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Všetko najlepšie Majstre :-)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Nie domov...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Eros Ramazzotti v Košiciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Mihalik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Mihalik
            
         
        mihalik.blog.sme.sk (rss)
         
                                     
     
        Bývalý novinár so záujmom o všetko užitočné.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1434
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




