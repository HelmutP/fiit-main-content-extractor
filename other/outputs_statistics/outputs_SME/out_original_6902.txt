
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Furmaník
                                        &gt;
                Naokolo naživo
                     
                 Pastieri, paste 

        
            
                                    20.5.2010
            o
            16:02
                        (upravené
                20.5.2010
                o
                22:40)
                        |
            Karma článku:
                7.25
            |
            Prečítané 
            1279-krát
                    
         
     
         
             

                 
                    Majoritné náboženstvo zdôrazňuje často a predovšetkým hodnoty, úctu k nim a o tom, že by sme im mali podriadiť svoje správanie. Pravda, láska, čestnosť, neklamať, nekradnúť a podobne. Reprezentovať by ich mali najmä jej predstavitelia, a nielen reprezentovať, ale sa za nich biť, podľa nich žiť, ozvať sa, keď sa to kdesi zvrháva, keď to niekto porušuje, a tak. Skrátka donebatrčiace vzory.
                 

                 Človek by čakal, že po desaťročiach ničenia hodnôt využijú slobodu, ktorú dostali.   Ale kdeže, houby.   V priebehu posledného volebného obdobia bohato „využili príležitosť mlčať", ako hovorí klasik.   Nevadí im, že ľudia, ktorí nám vládnu, sú priamymi nasledovníkmi tých, ktorí ich mučili, väznili, nútili kopať urán či „iba" vyradili na niekoľko desaťročí z profesie a zo života vôbec.   Nevadí im, že nám vládnu ľudia, ktorí rozoštvávajú národy a rasy a nakopávajú tak mier.   Nevadí im, ako Fico podnecuje a živí v ľuďoch závisť voči všetkému úspešnému - nenásytným bankárom, nenažratým majiteľom a šéfom DSSiek, skutočným podnikateľom i zahraničným investorom, ktorí nám okrem iného dávajú prácu a trošku inú ekonomickú situáciu, aká tu dlho nebola.   Nevadí im, že v ľudoch pestuje materializmus a rozdávaním štátnych dávok (vianočné dôchodky, šrotovné, rodinné príspevky) ich korumpuje. Nie, priamo najvyšší arcisokol hovorí, že „blahobyt" Slovenského štátu bol viac než pár miliónov ľudí v plyne.   Neprekáža im masové kradnutie, ktoré sa skrýva za sociálny štát, národ a politiku „pre ľudí".   Nemajú problém s tým, ako Fico a jeho kumpáni majú v úcte súkromné vlastníctvo ich „ľudí"- postupné vyvlastňovanie našich vlastných úspor v druhom pilieri, zákonom posvätené vyvlastňovanie pozemkov či znárodňovanie zisku súkromných zdravotných poisťovní - nie, tento priamy útok na to, čo je naše, je asi v súlade s ich učením, či čo.   Nechcú vidieť masívne zadlžovanie nás všetkých, čo je vlastne prejedanie budúcnosti nás, našich detí, vnukov (bohviekoho ešte), čo je demolácia pojmu "zodpovednosť".   Nie, keby im to prekážalo, čosi z toho by sa nalepilo i na ovečky. Ale houby, 45% voličskej podpory v priebehu volebného obdobia pre jednu stranu, ktorá väčšinu z toho zosobňuje, to je absolútny rekord, čosi čo nedokázal ani Mečiar v časoch svojej najväčšej slávy, čosi, čo nemal ani Hitler.   Nie, len potichu plaťte dane a buďte poslušní. Kritika a vlastné slobodné myslenie nie sú žiadúce.   A je už jedno, či už sa s Ficom dohodli alebo nie. Zlyhali a je jedno prečo. Ozvali sa len dvakrát. Raz pri Národnom programe sexuálneho zdravia, druhýkrát pri Metropolise - výnimky reprezentujúce úzkoprsosť a neschopnosť dovidieť si ďalej od nosa.   Predstavitelia náboženstiev sa rátajú medzi elity. Elitou je niekto, kto povie vždy to, čo považuje za správne a to, čo treba, bez ohľadu na to, či to niekto počuť chce alebo nie; bez ohľadu na to, či sa to hodí. Niekto, čo ľudí niekam vedie, je vzorom a otcom. Niekto, komu jeho súčasné postavenie či pohodlie nestojí za to, aby sklopil uši a prikyvoval.   Čo také povedali a kam vedú? Kto z nich vystúpil v médiách a povedal, tak toto teda nie páni, toto ľuďom nerobte! Či: nenechajte sa klamať, korumpovať a zavádzať, ovečky naše?   Hej, viem, ťažko sa kritizuje niekto, kto nás platí.   Správanie predstaviteľov majoritnej cirkvi tak pripomína správanie umelcov, ktorí nemajú problém povedať, že sú kurvy, ktoré zahrajú každému, kto zaplatí.   Preto mi je celkom sympatické, že to ktosi povedal nahlas, i keď mu to percentá veľmi nepridá, skôr naopak.   Sulíkova cirkevná daň je vec síce možno tiež sympatická, ale nie celkom pravicovo kóšer. Buďme totiž poctiví, nejde len o registrované duchovné hnutia. Mám rád kvalitnú literatúru, divadlo i classical music, ale prečo by mal mne a mne podobným na to prispievať ktosi, kto ani nevie čo je to filharmónia?   Mám dojem, že ak sa to niekto za niečo spomínané hodnoty bije, tak je to Hlina a niekoľko ďalších občianskych aktivistov, ktorých však majorita považuje za bláznov. Oni však za to peniaze neberú. Tí, čo ich za to berú a nie fiktívne, ale skutočné z našich daní, mlčia.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Peter Furmaník 
                                        
                                            4 marketingové tipy (nielen) pre začínajúcich politikov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Furmaník 
                                        
                                            "Voloďa, kde si tak dlho?" alebo pozývací list 2.0
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Furmaník 
                                        
                                            Šikovný úradník nežobre, ale tvorí - inšpiratívny príklad pre slovenských starostov/primátorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Furmaník 
                                        
                                            Mizerného pianistu pokojne zastreľte alebo Nie je profík ako profík
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Furmaník 
                                        
                                            Na trase KE - ZA - BA - Mexiko a späť: medzigeneračné debaty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Furmaník
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Furmaník
            
         
        furmanik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Milovník umenia, literatúry a slobody, tulák, provokatér, libertarián; inak firemný ekonóm &amp; konzultant + profi autor a bloger. O financiách a podnikaní píšem predovšetkým na Ľudskourečou.sk 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    113
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1415
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Naokolo naživo
                        
                     
                                     
                        
                            we are the culture
                        
                     
                                     
                        
                            Pohľady a pocity
                        
                     
                                     
                        
                            Moje najmilovanejšie mesto
                        
                     
                                     
                        
                            Biznis - základ nášho bytia
                        
                     
                                     
                        
                            Smrteľníci
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prečo sú na Slovensku nízke mzdy?
                                     
                                                                             
                                            Kresťanský mýtus o tradičnej rodine
                                     
                                                                             
                                            Pravidlo 80 : 20 : Pracujte menej a užívajte si viac
                                     
                                                                             
                                            Nemýľme si infláciu s rastom cien
                                     
                                                                             
                                            Nevera je mýtus duševne chorých ľudí - je?
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Susan Cain: Ticho (moja recenzia)
                                     
                                                                             
                                            Umberto Eco: Ostrov včerajšieho dňa
                                     
                                                                             
                                            Tim Ferriss: Štvorhodinový pracovný týždeň (moja recenzia)
                                     
                                                                             
                                            Javier Marías: Srdce tak bílé (literárny masaker)
                                     
                                                                             
                                            Austin Kleon: Steel like an artist
                                     
                                                                             
                                            Chris Guillebeau: Startup za pakatel
                                     
                                                                             
                                            Haruki Murakami: 1Q84
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            O živote tak naozajstne "životne" a krásne - Katka Džunková
                                     
                                                                             
                                            Ľudskou rečou o financiách (môj web)
                                     
                                                                             
                                            Tomáš Hajzler a sloboda v práci
                                     
                                                                             
                                            Menej štátu ... viac slobody
                                     
                                                                             
                                            Follow me on Twitter
                                     
                                                                             
                                            Môj blog o daniach, odvodoch, paragrafoch a podnikaní (eTrend)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Investujeme.sk - odborný magazín s mojou účasťou
                                     
                                                                             
                                            Sloboda v práci (SK)
                                     
                                                                             
                                            FINMAG (najmä ostrý Michal Kašpárek)
                                     
                                                                             
                                            Mít vše hotovo
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Protipovodňová ochrana Košíc? Len likvidácia obľúbenej krčmy
                     
                                                         
                       Krymské reminiscencie
                     
                                                         
                       Šikovný úradník nežobre, ale tvorí - inšpiratívny príklad pre slovenských starostov/primátorov
                     
                                                         
                       10 otázok pre Roberta Fica na TA3
                     
                                                         
                       Fico na konci s dychom
                     
                                                         
                       Podnikajú bez úplatkov a sú úspešní. Na Slovensku !
                     
                                                         
                       Ako sa veľké svetové médiá starajú o svoj Facebook a Twitter?
                     
                                                         
                       Viete, čo majú spoločné Igor Rattaj (J&amp;T) a Miroslav Trnka (Eset)?
                     
                                                         
                       Zmysel ženského orgazmu z pohľadu evolučných vied
                     
                                                         
                       Mizerného pianistu pokojne zastreľte alebo Nie je profík ako profík
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




