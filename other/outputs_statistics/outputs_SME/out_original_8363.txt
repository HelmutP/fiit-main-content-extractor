
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Ježek
                                        &gt;
                Staré Mesto
                     
                 Bratislavské Staré Mesto - už len realitná kancelária? 

        
            
                                    8.6.2010
            o
            18:10
                        |
            Karma článku:
                11.73
            |
            Prečítané 
            1166-krát
                    
         
     
         
             

                 
                    Keď som nastupoval na miesto poslanca za Staré Mesto, bol som plný ideálov ako správne rozhodovať a ako ľudom pomáhať. Na sklonku poslaneckého mandátu už ide len o jedno. Ako zabrániť výpredaju nehnuteľností v tejto lokalite.
                 

                 Mašinéria predajov sa začala cca pred pol rokom a odvtedy sa valí čoraz strmším kopcom a s čoraz väčšou razanciou. Bohužiaľ, my poslanci ktorí sa snažíme zabrániť výpredaju našeho spoločného majetku sme v žalostnej menšine. Priznám sa že je to až niekedy na zúfanie. Môžeme dôvodiť, môžeme hlasovať proti, prehlasujú nás stále tí istí. A ak sa ich v prestávke rokovania už naozaj naštvaný občan pýtal prečo tak konajú, zrozumiteľnej odpovede sa nedočkal.   Ďalšia vec je cena za ktorú predávame. Vo väčšine prípadov ide o cenu kúsok nad znaleckým posudkom. Ale myslím že predávať za cenu určenú znaleckým posudkom nie je dobrý predaj. Je to vlastne predaj za najnižšiu cenu, keďže pod znalecký posudok sa ísť nedá. Pripadá mi to ako predaj auta v nemenovanom autobazáre. Tiež vám nejakú cenu navrhnú ale tá v drvivej väčšine vašim predstavám nezodpovedá.   Vždy som si myslel že zvolený zástupca sa má chovať tak, aby majetok obce zveľaďoval. Za posledného pol roka už ten dojem nemám ak sa pozriem ako sa práve tento majetok rozpredáva. Či to už boli pozemky vedľa amfiteátra, bývalý Mliečny bar alebo teraz zrejme najšandalóznejší predaj - spoločnosť RBI čo voľne preložené je päť domov priamo na Hlavnom námestí. Áno, je pravda že je tam pôžička. Áno, je pravda že je kríza a priestory sa prenajímajú ťažšie. Dobrý hospodár však pri prvých problémoch hneď nepredáva, ale snaží sa odstrániť príčiny zlej situácie. Mám si vari myslieť že ten kto objekty kúpil, ich kúpil s tým že na nich bude prerábať? Alebo že je to nebodaj charita ktorá sa nad nami zľutovala a zbavila nás tohoto bremena? Nie, obávam sa že proste len vie nehnuteľnosti zhodnotiť a zjavne to vie lepšie ako vedenie RBI. Takže pýtam sa bolo treba predávať? Nestačilo by dosadiť do vedenia RBI ľudí ktorí to vedia?  Toto sú však len najvypuklejšie príklady, v poslednom čase sa s predajmi naozaj roztrhlo vrece. Posúďte sami:  26. zasadnutie Miestneho zastupiteľstva 9. 2. 2010 - predložených 15 návrhov na predaj (našťastie všetky stiahnuté) 29. zasadnutie Miestneho zastupiteľstva 20. 4. 2010 - predložených 21 návrhov na predaj (z toho 6 stiahnutých) 30. zasadnutie Miestneho zastupiteľstva 8. 6. 2010 - predložených 41 návrhov na predaj... vždy drvivá väčšina prešla a prešla len vďaka stále tým istým hlasom  Ja sa už naozaj obávam každého ďalšieho zastupiteľstva. Ak to takto pôjde ďalej, budeme už naozaj hlasovať len o predajoch. A zasa sa môžeme hádať, môžeme nesúhlasiť ale v konečnom dôsledku vďaka rozloženiu síl v zastupiteľstve znova boj o zachovanie majetku nás všetkých prehráme.   Nakoniec, všetky hlasovania o predajoch si môžete pozrieť aj s menovitými výpismi na stránke miestneho úradu Staré Mesto. Bude to určite poučné čítanie. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Ježek 
                                        
                                            Chcete nový byt? Staňte sa neprispôsobivým.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Ježek 
                                        
                                            Ako ďalej s nájomcami reštituovaných bytov?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Ježek 
                                        
                                            Páchnuci bezdomovec a zmiešané pocity
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Ježek 
                                        
                                            Ja lietam! Alebo ako málo dokáže potešiť chlapa v rokoch.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Ježek 
                                        
                                            Prevrat v Starom Meste, alebo kto má prsty v puči.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Ježek
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Ježek
            
         
        jaroslavjezek.blog.sme.sk (rss)
         
                                     
     
        Poslanec MČ Staré Mesto, fotograf a pedagóg
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2958
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Staré Mesto
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




