
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viktor Wurm
                                        &gt;
                Šport
                     
                 Futbalový ofsajd - známe neznáme pravidlo. 

        
            
                                    26.1.2009
            o
            22:14
                        |
            Karma článku:
                10.51
            |
            Prečítané 
            7401-krát
                    
         
     
         
             

                 
                    Načrtnime si jedno z najdôležitejších pravidiel futbalu, s ktorým prichádzajú neustále kontroverzie. Herná situácia nazývaná laicky "ofsajd" má z pohľadu pravidiel niektoré úskalia, ktoré množstvo bežných fanúšikov futbalu nepozná.
                 

                  Pravidlá futbalu aktualizované 1.7.2006 sú dokumentom, ktorým sa riadi moderný futbal, futbalové stretnutie by malo prebiehať v súlade s nimi. Ofsajdom nazývame situáciu, ktorá vyplýva z hry no je nedovolená. Túto situáciu podrobne popisuje pravidlo XI.: HRÁČ MIMO HRY.     Je dôležité si na začiatok uvedomiť, že hráč v postavení mimo hry ešte nie je hráčom mimo hry.           Znenie pravidla:   "1. Hráč je v postavení mimo hry, ak je bližšie k súperovej bránkovej čiare ako lopta,   s výnimkou:     a) ak je na vlastnej polovici hracej plochy alebo     b) ak nie je bližšie k súperovej bránkovej čiare ako najmenej dvaja hráči súperovho družstva.           Z uvedeného vyplýva:     - hráč nemôže byť mimo hry, ak v momente prihrávky stojí na vlastnej polovici.     - hráč nemôže byť mimo hry, ak sú z jeho pohľadu smerom na súperovu bránu, bližšie k bránkovej čiare minimálne dvaja protihráči, resp. jeho spoluhráč s loptou. Pojem "dvaja protihráči" pritom zásadne neurčuje, že jeden z nich musí byť brankár. Hlavne pri rohových kopoch nastáva situácia, pri obsadení oboch žrdí brániacimi hráčmi, že brankár je až pred-predposledným brániacim hráčom.          Ďalej pravidlo hovorí:   2. Hráča, ktorý je v postavení mimo hry, považuje rozhodca za hráča mimo hry, ak v okamihu,   keď sa niektorý z jeho spoluhráčov dotkne lopty alebo ju zahrá, podľa názoru rozhodcu:     a) ovplyvňuje hru,    b) ovplyvňuje súpera,    c) pokúša sa z toho postavenia získať výhodu.           Pravidlá teda zásadne určujú rozdiel medzi hráčom v POSTAVENÍ MIMO HRY a HRÁČOM MIMO HRY. Hráč v postavení mimo hry môže byť vzdialený desiatky metrov od ťažiska hry, preto takéhoto hráča nemôžeme považovať za hráča mimo hry. Ak však na tohto hráča smeruje prihrávka, resp. zo svojho postavenia získava výhodu či ovplyvňuje hru súpera, musíme ho považovať za hráča mimo hry.     Pritom rozhodca nepovažuje hráča za hráča mimo hry, ak je v takomto postavení pri vykonávaní kopu z rohu, kopu od brány a vhadzovania lopty.     Dôležitým pojmom je tiež takzvané "zbehnutie si z postavenia mimo hry". Mnoho divákov pobúrene nesúhlasí s verdiktami rozhodcov, ak vidia, že hráč si preberá prihrávku ešte pred ofsajdovou líniou, či dokonca na vlastnej polovici. V tomto prípade je potrebné povedať a zvýrazniť predošlé odstavce, teda, ak sa hráč nachádza v postavení mimo hry v momente prihrávky, a následne sa aktívne zapojí do hry, stáva sa hráčom mimo hry. Nezáleží pritom, v akom mieste sa nachádza pri preberaní prihrávky.     Ďalším dôležitým faktom je ten, že rozhodca má prihliadať aj na plynulý ráz hry, teda, ak by prerušením došlo k jej zbytočnému kúskovaniu, má od prerušenia upustiť. Preto nastáva situácia, kedy rozhodca ponecháva výhodu v hre (napríklad, hráč v postavení mimo hry zjavne nestíha dobehnúť nakopnutú loptu, naopak brankár ju s istotou pokrýva), čo sa tiež stretáva s častým nesúhlasom fanúšikov.      Zhrnúc dané informácie, je najdôležitejšie uvedomiť si základné informácie:     1) Hráč je v postavení mimo hry, ak sa v momente, keď mu loptu prihráva spoluhráč, nachádza bližšie k súperovej bránkovej čiare ako lopta alebo predposledný hráč brániaceho družstva. To neplatí, ak je v momente prihrávky na svojej polovici, ak loptu dostáva priamo z kopu z rohu, kopu od brány a vhadzovania lopty.      Tento hráč sa však stáva hráčom mimo hry až v momente, keď sa do hry aktívne zapojí, čiže ju ovplyvňuje, prípadne ovplyvňuje súpera, alebo sa zo svojho postavenia snaží akýkmkoľvek spôsobom získať výhodu.     2) Rozhodca hru preruší až vtedy, keď sa z hráča v postavení mimo hry stane hráč mimo hry s výnimkou už uvedeného ponechávania plynulosti hre. Podľa pravidiel je v tom prípade rozhodca "povinný upustiť od potrestania previnivšieho sa mužstva, ak sa domnieva, že by mu týmto potrestaním poskytol výhodu".     3) Hráč sa môže pri snahe vyhnúť sa prerušeniu hry, respektíve vyhnúť sa ofsajdu zachovať rôznymi spôsobmi. Najčastejšie je to zastavenie sa, čím prejaví nezáujem o zapojenie sa do hry, tým pádom môže prihrávku prebrať iný spoluhráč, ktorý neporušil pravidlo XI. Pravidlá však poskytujú hráčom ešte jednu možnosť. Hráč môže opustiť hraciu plochu, ak sa chce vyhnúť tejto pozícii, v tomto prípade ho rozhodca netrestá udelením žltej karty za "svojvoľné opustenie hracej plochy".      Je hlavne dôležité uvedomiť si tieto reguly, ktoré jednoznačne definujú pojem "Hráč mimo hry". Mnohokrát počujeme na adresu rozhodcov kritiku, dokonca vulgarizmy na základe mylného domnievania sa o tomto pravidle. Po zvážení týchto skutočností však môžeme zistiť, že táto kritika je neoprávnená, pohnútky, ktoré vedú divákov k agresívnemu správaniu v súvislosti s ofsajdom sú neodôvodnené. Toto je však problém "krčmových pravidiel" a takzvaných futbalových odborníkov, ktorých je na Slovensku primnoho ( a taktiež alkoholu:D ).        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Wurm 
                                        
                                            Fanúšikovia úspechu? No, tak sorry..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Wurm 
                                        
                                            Mezenská a Fischer hneď na úvod "mimo hry"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Wurm 
                                        
                                            Ako funguje naša krajina (vtip)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Wurm 
                                        
                                            Najnovšie informácie o Európskom hlavnom meste kultúry 2013!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Wurm 
                                        
                                            Kortešačky po košicky...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viktor Wurm
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viktor Wurm
            
         
        wurm.blog.sme.sk (rss)
         
                                     
     
        Narodený a žijúci v Košiciach...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2522
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zuzanka Hanzelová
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Fanúšikovia úspechu? No, tak sorry..
                     
                                                         
                       Mezenská a Fischer hneď na úvod "mimo hry"
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




