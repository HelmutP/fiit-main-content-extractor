
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomír Kopáček
                                        &gt;
                Lajf stajl
                     
                 Thor Steinar = hon na čarodejnice 

        
            
                                    29.10.2007
            o
            7:35
                        |
            Karma článku:
                12.37
            |
            Prečítané 
            18130-krát
                    
         
     
         
             

                 
                    Niekde som čítal, počul alebo to snáď je aj nejaká z hlavných vlastností zriadenia, ktoré je paródiou na demokraciu, že cenzúra je neprípustná a existuje niečo ako sloboda prejavu.
                 

                 
Svätá inkvizícia, takto skončí každý kto sa nezaradí do davu. 
    Ak si to vysvetľujem správne, tak za prejav možno považovať aj prejav neverbálny – obliekanie. V modernej Európe je veľmi v kurze ľavicový extrémizmus a "antifa" náboženstvo, tento zhubný fenomén prerástol aj do vysokej politiky. Niemandi z antifa hnutí dokonca fungujú ako "experti" na zostavovanie policajných manuálov pre klasifikáciu čarodejníc. Do manuálov nakreslia rôzne znaky, ktoré podľa nich nesú jasné známky pravicového extrémizmu a stádo blbečkov sa manuálmi riadi.  Medzi typických expertov, ktorý všade bol a všetko vie najlepšie, patrí aj Laco Ďurkovič z TV JOJ.     Jedna z obetí chorobného bujnenia antifa rakoviny je nemecká odevná značka Thor Steinar, ktorá doplatila na to, že používa symboliku nordickej mytológie – runové písmo. Logo Thor Steinar bolo pôvodne zostavené z rún "Tyr" (písmeno T) a "Sol" (písmeno S) – skratka Thor Steinar – TS.  Runa Tyr je runou šťastia, duchovného víťazstva, plodenia a večnej premeny. Runa Sol je runou víťazstva, slnečnej sily, je runou bojovníka – víťaza.     Antifasom runové písmo pripomína znak nacistickej organizácie SS, takže podľa ich zvrátenej logiky všetko, kde sa vyskytujú runy, je nacistické, neonacistické, pravicovo-extrémne atď., fantázii sa medze nekladú.  Pri Thor Steinar zašli asi najďalej ako sa len dalo a povymýšľali rôzne konšpiračné teórie a tajné odkazy zašifrované v logu značky a na všetkých jej produktoch.     Pobavte sa, čo dokázali vymyslieť a predložiť berlínskemu súdu ako dôkaz:          Berlínsky súd, ktorý asi nechcel na seba zbytočne pútať pozornosť a zahrávať sa s fanatikmi, aby nebol označený za pro-nacistický, pôvodné logo Thor Steinar, zložené z rún, svojím rozhodnutím v roku 2005 zakázal. Nezakázal však činnosť firmy MediaTex GmbH, ktorá značku Thor Steinar vyrába a predáva aj naďalej.     Zdá sa, že chaos v zákonoch a nestrannosť nemajú len slovenské súdy, ale aj tie nemecké. Spomínaný berlínsky súd v roku 2006 odsúdil chlapíka za nosenie pôvodného loga na 7 mesiacov a 150 hodín verejných prác. Paradoxom je, že rovnaký súd, ale iný sudca, o niekoľko mesiacov skôr oslobodil iného chlapíka obvineného z toho istého ťažkého zločinu.  O čo tu vlastne ide?     Je desivé, že banda dobre organizovaných paneurópskych antifa zaslepencov, jednoducho dokáže zničiť alebo aspoň poškodiť ľubovolnú firmu, ktorá sa im znepáči. Stačí vykonštruovať nacistické spojenectvo vo forme nezmyselných šifier a firma môže uvažovať o bankrote.  Toto celé je svedectvom ich neúcty k hodnotám demokratickej spoločnosti a pohŕdaním súkromným majetkom.     Možno je len otázkou času, kedy sa vznesú žaloby na Mercedes – Adolf Hitler a jeho generalita sa vozila predsa na Mercedesoch. Pokojne môžu zaútočiť aj na Volkswagen a Porsche, veď tieto firmy začali, podľa návrhov Adolfa Hitlera, vyrábať "ľudové vozidlo", ktoré v podobe New Beatle prežilo dodnes.  Mediálne najznámejšia neonacistka na Slovensku, musí teda podľa logiky antifasov byť Zuzka Belohorcová, jeden taký Beatle jej hovorí pani :-)     Hugo Boss je tiež dobrý cieľ, uniformy SS šila práve táto firma. Je to škandál, jednoducho identifikovateľný nacista je už vlastne každý dobre oblečený manager, ktorý dnes oblieka SS uniformu nenápadne maskovanú za kvalitný oblek.     A čo len firma IG Farben, ktorá sa podieľala na výrobe Zyklon B – plynu, ktorý údajne zlikvidoval v koncentračných táboroch tisíce Židov. IG Farben, ktorá vo forme známejších dcérskych firem ako AGFA, BASF alebo Bayer, žije stále.  Tu všade by podľa ich logiky mali smrdieť "nacistické" peniaze, firmy by určite mali byť zakázane a zákazníci kupujúci ich produkty by mali dostať trest smrti, veď podporujú firmy, ktoré prežili dodnes aj z nacistických peňazí.     Čudujem sa, že fanatikom nevadí značka Helly Hansen, ktorá má v logu natvrdo HH, čo podľa spomínanej logiky musí byť skratka Heil Hitler.  Bože chráň, aby oblečenie HH začali nosiť nakrátko ostrihaní chalani, hneď by bol problém na svete.     Skutočnosť, že Európska únia je vlastne "Tretia ríša s ľudskou tvárou" nemá asi veľmi význam ani spomínať. Všetci by sme mali spáchať hromadnú samovraždu, lebo sme toho súčasťou.     Svojím idiotským lovením čarodejníc dosiahli to, že značka Thor Steinar je symbolom rebélie a odporu voči nekonečnej ľudskej blbosti. V skutočnosti by sa mali šéfovia MediaTexu antifa chamradi poďakovať, lebo podľa mojich informácií dosahuje značka rekordné predaje.  Ja kupujem Thor Steinar od jeho vzniku a kupovať ho budem aj ďalej, nič nelegálne na tejto značke nie je a vykonštruované blbosti ma jednoducho nezaujímajú.     Označíte ma za neonacistu? Nech sa páči, ale upracte si najprv pred vlastným prahom a spomeňte, kedy naposledy ste si šlehli nejakú tú tabletku od firmy Bayer napríklad... Ja ani neviem čo slovo neonacista znamená, ale viem, že dnes frčí a používa sa ako univerzálne označenie každého, kto má na určité témy odlišné názory od majoritnej spoločnosti.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (108)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Mestská polícia - my sme zákon! Ty drž hubu a plať! Časť druhá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Cola moja milovaná, kde v prdeli si?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Mestská polícia - my sme zákon! Ty drž hubu a plať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            V pazúroch mafie a výpalníkov release 2008.1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Kopáček 
                                        
                                            Na žranici u Rozina
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomír Kopáček
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomír Kopáček
            
         
        kopacek.blog.sme.sk (rss)
         
                                     
     
        Je to blázen, hoďte na něj síť!
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5497
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezmysly
                        
                     
                                     
                        
                            Lajf stajl
                        
                     
                                     
                        
                            Blacklist
                        
                     
                                     
                        
                            Motorizmus
                        
                     
                                     
                        
                            Staré články
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nepriateľ ma jednoducho miluje :-)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Indy &amp; Wich
                                     
                                                                             
                                            Pure.FM Radio Trance
                                     
                                                                             
                                            podcast D&amp;B Arena
                                     
                                                                             
                                            iPod
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ďalší šťastlivý milionár rekonštruoval svoje sídlo :-)
                                     
                                                                             
                                            Cica Mica
                                     
                                                                             
                                            Dagmar Kopáčková (moja pokrvná príbuzná a jazykový korektor)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Hálova 7 - informačný web našej samosprávy
                                     
                                                                             
                                            Beo.sk – spravodajstvo, spoločnosť, história
                                     
                                                                             
                                            Metapedia
                                     
                                                                             
                                            Thor Steinar
                                     
                                                                             
                                            POLAR Personal Trainer
                                     
                                                                             
                                            Muscle &amp; Fitness
                                     
                                                                             
                                            BIOMag
                                     
                                                                             
                                            Campagnolo
                                     
                                                                             
                                            Root
                                     
                                                                             
                                            Google Maps
                                     
                                                                             
                                            Amateri.cz :-)
                                     
                                                                             
                                            Apple
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




