
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Epigramiáda
                     
                 Ideme na hojdačku 

        
            
                                    9.6.2010
            o
            7:30
                        (upravené
                8.6.2010
                o
                13:06)
                        |
            Karma článku:
                5.97
            |
            Prečítané 
            1297-krát
                    
         
     
         
             

                 
                    Tak som si povedal, že možno bude dobre hovoriť aj o tom, ako sa moje dni rozmieňajú na drobné okamihy. S Leou sme veľa kreslili a rozhodol som sa tie pastelové "výtvory", ktoré ešte Lea neroztrhala zviazať do jedného zošita. Bude to kniha toho, čo sme videli v parku. Nie síce odborná o akej snívam, no pre vyťaženosť sa "nekoná", no dobrá. Ja som sa pritom učil kresliť a Lea rozprávať. A potom tieto krátke epizódy. Fotky sú fajn, kamera tiež, no takto aspoň priemerné publikum uvidí, čo prežíva otec na "materskej" dovolenke. Snáď to niekoho inšpiruje. Pre niekoho to môže byť neúčinná úsmevná výstraha.
                 

                 
... na hojdačke...4ever.sk
   „Leuška ide na hojdačku, na svoju", povedala Lea, keď sme dorazili na ihrisko „nášho" rodinného parku. Na hojdačky pribehli v čase obedňajšej pauzy dve tínejdžerky. Obsadili naše  hojdačky. Obidve.    Lea z obďaleč pozoruje zo svojej(!) odrážacej motorky ako jej obsadili hojdačku a zvolá so zachmúrenou tvárou: „Nie. To je Leuškina" a vystrie ruku so zamietavým a zakazujúcim gestom smerom k obsadenej hojdačke. Tínejdžerky nás nepočuli, no našťastie to na hojdačkách dlho nevydržali. Došli sme posledných dvadsať metrov k hojdačke a Lea si natešene sadla.    Bacuľatými ručičkami zviera reťaze, na ktorých hojdačky visia. Spokojne hovorí: „Nie je studená hojdačka". Dnes je po dlhom čase opäť predvídateľné počasie, taká malá reklama na leto. Okolo hojdačiek sa pomaly začínajú zbiehať ďalšie deti. Čakajú, či ich niekto na hojdačku pustí. My to nebudeme. Zatiaľ určite.    Leuška volá: „Vysoko tatino, takto. Za bruško, vieš?" naviguje ma ako ju mám hojdať a šťastne sa nahlas smeje.    Motorka stojí vedľa hojdačky. Malý neznámy chlapček na ňu zaujate pozerá, skúša sa na ňu posadiť. Je však na ňu príliš malý. Má o pár mesiacov a centimetrov menej ako Lea. Teda... je veľmi malý. Motorka sa mu vykĺzne spomedzi nôh a mladý budúci muž spadne. Leuška všetko hojdavo pozoruje. Z vysoka ako z kyvadla. Konštatuje: „Chlapček spadol z motorky leuškinej, takto baaac".    Deti čakajú. Smutne pozerajú sem-tam na žiariacu Leuškinu tvár, ktorá sa kúpe v prvom júnovom slnečnom kúpeli. Zasa som nenašiel opaľovací krém. Najhoršie je vždy balenie a hľadanie vecí pred vychádzkou. Zvlášť ak ste ich pár týždňov či mesiacov nepotrebovali. Tu končia hranice mojich nekonečných schopností. Dúfam, že ju to slnko nespáli.    Lea náhle preruší moje opaľovacie uvažovanie: „Leuška sa hojdá, vieš tatino. Vysoko, takto...."  Prešlo sedemásť minút. Leuška sa stále hojdá. Čakateľky a čakatelia na hojdačku sú netrpezliví. Odchádzajú a prichádzajú.    Hojdáme sa. Lea náhle povie: „Pustíme dievčatko." Odchádzame na pieskovisko a Lea hovorí: „Tatino má formičky v backpacku, vieš? Veľa, veľa formičiek. Dobre. Aj lopatku." Tak skoro spať ešte nepôjdeme. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




