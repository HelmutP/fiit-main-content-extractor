
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Šimčík
                                        &gt;
                Hudba
                     
                 Libertínci sú späť 

        
            
                                    1.4.2010
            o
            18:59
                        (upravené
                1.4.2010
                o
                19:45)
                        |
            Karma článku:
                3.93
            |
            Prečítané 
            942-krát
                    
         
     
         
             

                 
                    Po 6 rokoch sa na pódia vracia pravdepodobne najdôležitejšia britská skupina posledného desaťročia.
                 

                 
  
   The Libertines založili niekedy okolo roku 1997 v Londýne jej dvaja frontmani a najlepší priatelia Carl Barat a Pete Doherty. Inšpirovaní The Beatles, The Kinks, The Clash, The Jam, Oasis a inými sa rozhodli na konci nového milénia vrátiť rock'n'roll k jeho špinavým koreňom. Namiesto komplikovaných melódií a abstraktných textoch dvojica stavila na melodické jednoduché piesne s často sociálnymi textami a londýnskym cockney slangom z ulice. V roku 2002 vydávajú za pomoci producenta Micka Jonesa (The Clash) debutový album "Up The Bracket", ktorý o 2 roky nasleduje "The Libertines". Oba albumy boli vysoko hodnotené a zožali  navyše aj komerčný úspech   Slávu a peniaze však nezvládol Pete Doherty. Ten sa posledných desať rokov pravidelne objavoval na stránkach bulváru nezvládajúc svoju drogovú závislosť. Jeho finančná situácia došla až tak ďaleko, že vykradol byt Carla Barata a ten ho zato  vyhodil z kapely. Obaja hudobníci po rozpade The Libertines pokračovali v hudobnej kariére vo svojich nových projektoch - Carl založil Dirty Pretty Thing a Pete úspešnejších  Babyshambles (zahrali aj u nás na Topfeste) .      Pred necelým týždňom britské média ohlásili, že po 6 rokoch sa kapela dáva opäť dokopy aby zahrala na festivaloch v Reading a Leeds, za čo by mali zinkasovať £ 1,5 mil za vystúpenie. Situácia sa však môže ešte zmeniť keďže Doherty bol minulý týždeň uväznený z dodávania drog vedúcej k smrti.                         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Ako Maťo Ďurinda reggae hral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O retre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O hmle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O nechcených vianočných darčekoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Keď jeden chýba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Šimčík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Šimčík
            
         
        simcik.blog.sme.sk (rss)
         
                        VIP
                             
     
        Agropunk, vraj aj Mod, stratený ako working class hero na východoslovenskom vidieku.  Rád chodím na futbal, koncerty, do pubu, kostola a z času na čas aj na disco :)

"Myslím, teda slon"


  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    186
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názory
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Subkultúry
                        
                     
                                     
                        
                            Británia
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Čo sa domov nedostalo
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Roger Krowiak
                                     
                                                                             
                                            Rudolf Sloboda - Do tohto domu sa vchádzalo širokou bránou
                                     
                                                                             
                                            Subculture - The Meaning Of Style
                                     
                                                                             
                                            F.M. Dostojevskij - Idiot
                                     
                                                                             
                                            NME
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Arctic Monkeys
                                     
                                                                             
                                            The Gaslight Anthem
                                     
                                                                             
                                            Slovensko 1
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            BBC Radio 2
                                     
                                                                             
                                            BBC Mike Davies Punk Show
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tatran Prešov
                                     
                                                                             
                                            Bezkonkurenčný Dilbert
                                     
                                                                             
                                            flickr
                                     
                                                                             
                                            discogs
                                     
                                                                             
                                            ebay.co.uk
                                     
                                                                             
                                            Banksy
                                     
                                                                             
                                            Kids And Heroes
                                     
                                                                             
                                            Roots Archive
                                     
                                                                             
                                            West Ham United
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            denník Guardian
                                     
                                                                             
                                            denník Pravda
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




