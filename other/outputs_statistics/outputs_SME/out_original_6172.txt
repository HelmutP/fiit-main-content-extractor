
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslava Sčensná - Harvanová
                                        &gt;
                Len tak.....
                     
                 Deň matiek 2009 

        
            
                                    9.5.2010
            o
            14:10
                        (upravené
                9.5.2010
                o
                14:21)
                        |
            Karma článku:
                5.83
            |
            Prečítané 
            631-krát
                    
         
     
         
             

                 
                    Presne pred rokom, sme si uvedomili, čo je to mať mamku, a aké „jednoduché“, je o ňu prísť... Za pár sekúnd si uvedomiť, čo všetko mamka v našom živote znamená, čo predstavuje, čo nám dáva, ako nás usmerňuje a bojí sa o nás. A potom sa to celé obráti, a my všetci sa bojíme o ňu.
                 

                 
  
   Telefonát od brata, že mamku odviezli do nemocnice. Šok, lebo bola úplne zdravá. Sadáme do auta a štyri hodiny je v aute ticho. Ani rádio, nič. Len strach a nevedomosť, čo sa vlastne stalo. Domov prichádzame v noci, k nej môžeme ísť až ráno. O siedmej sme už v nemocnici, máme šťastie na milého a ochotného ošetrujúceho lekára, ktorý nám trpezlivo vysvetlí, čo sa stalo. Mamku máme na JIS-ke. Najprv návštevu nepovolí, potom ho uprosíme, a môžeme na desať minút za ňou. Vlastne , nie všetci, len ja. Inak nik. Dávam sestričke veci pre mamku, ktoré bude potrebovať. Vydržím neplakať asi dve sekundy, potom už je mi to jedno.... Vystískam, spýtam sa, či ju niečo bolí, či niečo nepotrebuje. Odpoveď, že v celku  dobre, bolí ju hlava a nevidí dobre. Personál je však úžasný, milý a ochotný. Sestrička príde, poprosí ma, aby som šla. Pekne, s citom, chápavo. Odchádzam, ale sľubujem mamke, že zajtra nás už k nej pustia všetkých, primár to sľúbil.....       Doma je pusto, smutno. Vybalíme z tašky hrozno, ktoré mamka tak miluje,  ktoré sme je v nemocnici nemohli nechať. Rovnako ako knihy, a krížovky, lebo mamka si nesmie namáhať oči. Tak len niečo drobné zjeme. V izbe visia moje svadobné šaty, pod nimi vybalené svadobné topánky. Kúpené len pred týždňom, s mamkou. Vravela, že som v nich krásna. Ako vari každá mamka svojej dcére, ktorá sa chystá na najkrajší deň svojho života. V tej chvíli som po nich len prebehla pohľadom, a nič. Žiadna radosť , len strach, čo bude ďalej.....       Večer prichádza domov aj ocko. Po jeho vete, ako je tu bez mamičky smutno, sa nielen jemu kotúľajú z očí slzy. Ráno je však už lepšie, pustia nás k nej všetkých, mamka už vyzerá lepšie, pomáha jej naša prítomnosť. Dokonca si už aj zažartuje. Vraj, dala sa hospitalizovať, aby nemusela pomáhať pri svadobných prípravách :O) Beriem dovolenku, ostávam doma, a chodíme za ňou každý deň. Keď ju po dvoch týždňoch púšťajú domov, žiarime všetci šťastím.        Mamka sa zotavila, svadobné prípravy už prebiehali pod jej dozorom, a hoci veľmi nevládala, s ockom si na svadbe spolu zatancovali.        Život nebeží stále tak, ako by sme túžili a chceli. Zo dňa na deň sa všetko, čo ste mali a na čo spoliehali, môže zmeniť. Zo života sa vám môžu vytratiť ľudia, ktorí sú pre vás najdôležitejší a najvzácnejší. A preto nielen dnes, ale každý deň, a nie preto, lebo sa to musí, ale preto, lebo tak chceme, si treba mamky vážiť a milovať. A dávať to najavo, aj keď možno trošku ostýchavo, a s červenou tvárou, lebo nie každý vie prejaviť svoje city. Stačí pohladenie, úsmev, alebo hoci aj telefonát domov. Aby vedeli, že ich ľúbime stále, celý rok, nielen prvú májovú nedeľu........... Lebo mamku máme len jednu...........     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Sčensná - Harvanová 
                                        
                                            My, prvého novembra narodení
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Sčensná - Harvanová 
                                        
                                            Nie, nejdem pre ten kvet!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Sčensná - Harvanová 
                                        
                                            Ja tie ženy nechápem!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Sčensná - Harvanová 
                                        
                                            Odišla mi kamarátka z detstva.....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslava Sčensná - Harvanová 
                                        
                                            Prvá nádielka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslava Sčensná - Harvanová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslava Sčensná - Harvanová
            
         
        scensna.blog.sme.sk (rss)
         
                                     
     
        ...zo škôlky prestúpila rovno na základnú školu O)) 

Milujem slovenskú a českú hudbu a mojím snom je naučiť sa hrať na gitare :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2066
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Len tak.....
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Mezinárodní bezpečnost v době globalizace - J. Eichler
                                     
                                                                             
                                            Základy práva EÚ - Mazák, Jánošíková
                                     
                                                                             
                                            Geopolitika stredo-európskeho priestoru - O. Krejčí
                                     
                                                                             
                                            Wm.Paul Young - Chatrč
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio HEY
                                     
                                                                             
                                            Výber slovenské a české hity
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Protestujem a žalujem.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




