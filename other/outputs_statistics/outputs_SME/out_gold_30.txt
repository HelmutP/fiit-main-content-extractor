

 
Kupujete lekváre, džemy a marmelády? Neviem, aký je v tom rozdiel, možno iba tuším. 
 
 
Napríklad taký slivkový lekvár... 
 
 

Vraj bol najlepší z bystričiek (druh sliviek) a ten sa vraj najlepšie varil v záhrade v kotlíku, pod ktorým sa prikurovalo brezovým (?) drevom, aby pekne voňal. Nešiel tam žiadny cukor, nič. Slivky sa varili dovtedy, kým sa nepremenili na hmotu podobnú mäkkému asfaltu. (Slivky sa rozvarili tak, že po šupke z nich nebolo ani stopy.) Bol sladučký a vraj v tmavej kameninovej nádobe dokázal vydržať roky. Aj lyžica sa v ňom dala zlomiť. Večer sa z neho odobralo, rozvarilo s vodou a na druhý deň šups s ním do buchiet alebo do prekladanca (najúžasnejší koláč môjho detstva, ktorý sa skladal z lekvárovej, makovej a orechovej vrstvy preloženej kysnutým cestom...)... Taký nekúpite nikde.
 
 
A čo taký malinový jam? (džem)...
 
 

Píšem to schválne tak, ako kedysi moja stará mama. Nemal ani jadierko a dal sa priam krájať nožom. Bol pevnejší ako huspenina a varil sa výhradne z prepasírovaných malín (šťavy) a asi aj cukru. Mimochodom, malinová šťava (teda sirup), keď sme už pri nej - taká domáca... Keď sa otvorila fľaška, v zime sme mali kuchyňu plnú malín. A čo potom, keď sa ňou polial taký čokoládový puding... No, kúpite niekde? Aj keď by som za pol litra chcela zaplatiť pár stovák, jednoducho niet... Malinový lekvár som mala najradšej na čerstvom chlebe s maslom. Už len pohľad naň bol úžasný. Lekvár sa od "jamu" líšil tým, že sa zaváral tak, že na 1 kg ovocia sa pridal 1 kg cukru. Najviac mi chutili tie jadierka. Boli všade...
 
 
Jahodový lekvár - jedna báseň...
 
 
Asi sa tiež robil 1:1 s cukrom, no krátko pred dokončením sa medzi tie úplne rozvarené jahody vhodilo zopár ďalších tak, aby sme pri vyjedaní najprv vyjedli jahody a potom zvyšok. Geniálne, nie?
 
 
Marmeláda a ostatné zaváraniny z obchodu 

 
 
To bola hustá zmes zo zavareného ovocia - stála okolo 2 Kčs a bola dobrá - i tá čerešňová, marhuľová a miešaná... Vlastne za "socíku" sa predával aj kvalitný slivkový lekvár - bol skoro čierny a ťahavý, aj ten marhuľový lekvár s kúskami marhúľ, ktorý potrebujem, bol dosť kvalitný... 
 
 
Čo obchod ponúka dnes?
 
 
Pozreli ste si niekedy zloženie zaváranín? Niekedy len zahusťujúca jablková hmota (pektín), ktorú obsahuje aj želírovací cukor a ostatné ovocie len akoby z rýchlika. Ak hľadáte vo vyššej cenovej kategórii, tak je to rozdelené na nejaké tie tretiny. Ja nechcem želírovaciu hmotu a príchuť a možno nejaké to ovocie. Mojím lakmusovým papierikom je marhuľový lekvár.  Pár týždňov ho nedokážem kúpiť. Ja som na taký doteraz nenatrafila. Neviem, či je chyba vo výrobe, alebo v obchode. Ak máte opačné skúsenosti, poradíte, kde? (Odpovede zo Slovenska ma zorientujú, odpovede zo zahraničia poslúžia na porovnanie :-) )
 
 
Gazdinky, ako pečiete?
 
 
Ak nemáte poruke domácu zaváraninu, darí sa Vám pri starých a kvalitných receptoch s novými obchodnými zaváraninami? Moja skúsenosť s receptom, ktorý chcem čoskoro zverejniť je katastrofálna. Je totiž založený na kvalitnom marhuľovom lekvári - takom, kde sa rozvarila aj šupka. Inakšie to nebude ono (už som to odskúšavala a nešlo to, hmota, ktorá sa mala vyšľahať ako hustý sneh, spadla a roztiekla sa - asi kvoôi tej želírovacej "srande".
 
 
Začnem opäť zavárať?
 
 
Len kvôli niektorým sviatočným receptom. Mať zopár fľaštičiek poruke a robiť frajerku. Pretože, ak na Slovensku nemáme výrobcu kvalitných zaváranín a k nemu predajcu, alebo nemáme ani dovozcu, hoci vo "fajnšmekerovských"množstvách a cenách, nemám inú možnosť. Len mi potom, prosím, poraďte, kam v paneláku schovám kvalitný a dobrý lekvár pred nosmi mojich "sarančiat"? Keď som naposledy pred 15 rokmi uvarila taký černicový lekvár, do týždňa ho zvyšná časť rodiny našla a vyjedla. Všetkých 10 fľašiek... A dnes to bude ešte horšie... 
 
 
 
 
 
 
 
 
 
 
 
 
 

