

 Pravdu povediac, neoľutovala som.
 
 Najťažšou úlohou bolo totiž to isté: obliecť mančaft a dostať ho za dvere. A to ma čaká za každých podmienok. 
 Zaujímavé, deti ani neprotestovali. Poskakovali, rečnili, komentovali, ba povedala by som, že som u nich nachádzala stopy svedčiace o neuveriteľnom úsilí ich slušne vychovať. 
 A koľko ľudí sme to len stretli! 
 Najprv našu pediatričku. To nič, že sme u nej už dnes doobeda boli. Teraz poobede to bola naša kamoška. Potom Kubovu pani učiteľku. Potom spústu iných ľudí. Boli to stretnutia síce krátke, ale urobili nám všetkým náladičku - a dúfam, že aj druhej strane. 
 Cestou späť z výtvarky sme šli do obchodu, na ihrisko a poslednú časť trasy, keď už špuntov začínali bolieť nohy, nás zachránila fantázia: v parku si našli dlhé konáre a cestu domov si všetci štyria "vykosačkovali".  
 Pravdou je, že som sa neprejavila ako najvytrvalejší člen výpravy - lebo po návrate domov som päť minút predýchavala ten pochod. Ale pravdou je aj to, že "riadiť" štyri malé formuly mi napriek láske k šoférovaniu v čase poobedňajšej špičky prišlo podstatne jednoduchšie.Včera sme boli celý deň bez auta. A hlásim, že sme prežili v dobrom zdraví.  

