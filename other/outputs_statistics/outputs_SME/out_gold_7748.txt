

   MATERIÁL:  - bavlnené tričko  - 3D farby v tube  - vytlačený text (ja som použila typ písma Comic Sans, bold, veľkosť  120)  - kartón - priesvitnú lepiacu pásku a nožnice  - fén                            
 
    POSTUP:  
 
 Vytlačený  text prilepte na kartón priesvitnou lepiacou páskou. Kartón aj s textom vsuňte pod tričko.  Nielenže text uvidíte ale vďaka kartónu sa vám farba nepretlačí na zadnú  časť trička.                               3D farby sú v tube a dobre sa držia v ruke. Sú ukončené šikovným aplikátorom, pomocou ktorého sa nanáša farba na tričko. Treba naniesť dostatok farby kľudne aj v hrúbke 2 milimetrov. Farba po vysušení a pôsobení tepla zväčší svoj objem, nabotná a vytvorí plastický efekt.                               Po ukončení nanášania nechajte farbu  uschnúť. Trvá to tak 5-6 hodín. Kartón z trička nevyberajte.                                Po uschnutí je potrebné farbu zafixovať teplom. A to okolo 100 - 150 stupňov Celzia. Najľahším spôsobom je použitie fénu na vlasy. Budete totiž vidieť ako farba rastie.  Samozrejme sa dá na dosiahnutie 3D efektu použiť aj žehlička. Vtedy je potrebné prežehliť text z rubovej strany a na lícnu stranu pod text vložiť bavlnenú látku aby sa farba ak náhodou nie je dokonale vyschnutá neotlačila.                               A tu je ukážka písma, ktoré nám vytvorilo plastickú štruktúru. Po zafixovaním teplom je možné tričko prať bežným spôsobom. Farba sa nezlúpe a ani nijako inak nepoškodí. Taktiež nezafarbí iné oblečenia ak použijete pranie maximálne do 60 stupňov.                        
   
 Priebežne budem pridávať ďalšie tipy na darčeky z kratívnej dielne U šikovných rúk. 
 Do tvorenia!   

