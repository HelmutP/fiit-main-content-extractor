

   
 Cirkev, vážení športoví priatelia, je téma, o ktorej Poliaci medzi sebou nediskutujú, lebo nie je o čom, zato ju však veľmi radi vysvetľujú cudzincom. Pozdravujem Kazimierza a jeho pravidelné polhodinové vsuvky na seminároch. S láskou spomínam. 
 No a teraz, keď máme za sebou vtipný úvod, prejdime k vážnejšiemu jadru, lebo ja vlastne, ak ste si nevšimli, píšem výlučne o vážnych veciach. Len som ešte dostatočne mladý a naivný nato, aby som si z nich robil srandu. 
 Prednášok o histórii a úlohe cirkvi v živote poľskej spoločnosti som absolvoval na tucty a to fakt ešte nerátam toho Kazimierza. Stále nemám dosť. Aj včera som bol a zas to bolo zaujímavé. 
 Že v období neexistencie kráľovstva cirkev udržiavala poľské povedomie a v období komunizmu bola duševným azylom pred režimom všetci vieme. Pochopiteľne najmä vďaka tomu, že som o tom už jeden veľmi pútavý článok napísal. Včera sme sa rozprávali o návšteve Jána Pavla II. v Poľsku v lete roku 1979. Necelý rok predtým bol zvolený za pápeža, čo samé o sebe bolo symbolom, s ktorým si režim nevedel rady. So symbolmi sa celkovo veľmi zle bojuje. Múdra veta, Samo. 
   
 Grzegorz nám s pohnutím v hlase rozprával ako sa v tej chvíli celkom maličkí ale zato metálmi ovešaní stranícki pohlavári stretli s dôstojným, pokojným a v bielom oblečeným pápežom. Triasli sa pritom tak, že reportáž v správach odvysielali len raz, ale aj len raz bolo o jedenkrát viac ako bolo treba. Už to tak proste chodí, že niektoré symboly sa symbolmi stávajú preto, lebo sa za ne vyhlásia a ovešajú sa metálmi a iné preto, lebo ich za ne vyhlásia ľudia a ani metále nepotrebujú. Cipana, jaka múdra veta zas. 
   
 Svätá omša sa slúžila na Štadióne Desaťročia, vlajkovej to lodi toho režimu, v ktorom každému všetko patrilo ale špeciálne v Poľsku fakt nikto nič nemal. Tiež symbolické. Varšava mala vtedy zhruba 1,7 milióna obyvateľov a omše sa zúčastnili asi tri milióny ľudí. 
 To, že bol človek súčasťou toho davu, mu pripomínalo, aký je maličký, ale zároveň vypovedalo o sile a vôli národa. Pri tom štadióne tečie Visla a priamo oproti na jej druhej brehu bola vysoká budova ústredia strany. Brehy spájal neďaleký most, ktorý strážilo asi dvesto vojakov. Ak by vtedy, opäť slovami Grzegorza, pápež čo i len náznakom spomenul pochod na druhú stranu toho mosta, režim by sa utopil v krvi a padol. On namiesto toho odslúžil omšu, ktorá nám dodala silu. Keď sa skončila, nič už nebolo ako predtým, lebo v našich hlavách sme ten most prekročili. Tak povedal Grzegorz. 
 Onedlho sa zrodila Solidarita a zvyšok je už známa história. 
   

