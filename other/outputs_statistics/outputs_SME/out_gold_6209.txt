

 Zdá sa, že podľa posledných prieskumov zrejme aj vznikom nezvratného trendu vznikla naša lenivosť písať si perom na papier nové recepty. Televízne kanály s online živým varením a internetové stránky ponúkajúce s "cooking shows" súvisiace recepty sú naozaj silnou kombináciou. 
 Naše internetové správanie prezrádza veľa o stravovacích preferenciách a keď sa pozrieme na prieskumy internetových vyhľadávačov - tak v rámci denného jedálnička na večeru takmer vždy volíme kurča:). Prekvapení? Kurča?! ...že by bolo také výnimočné? 
  Je to aj o pohodli...a dostupnosti. 
  
 Ak sa pozriete na prieskumy internetových serverov spravujúcich najpopulárnejšie svetové stránky o jedle, varení či gastronómii vôbec, najvyhľadávanejšou surovinou je práve kurča. A je jednoduché zistiť aj v akom období počas roka je kurča "hitom internetu". Letu vládne hľadaný výraz "kurací šalát" a v zime ho pomaly nahrádza "kuracia polievka" alebo" kurča na iné spôsoby". 
 Vyhľadávané termíny prezrádzajú o nás aj to, ako sme ovplyvení miestom, kde žijeme. Obyvatelia mesta väčšinou vyhľadávajú zoznamy a recenzie známych reštaurácií, detailne pátrajú po informáciách o ponúkaných jedlách a radi diskutujú v internetových fórach o jedle. Obyvatelia žijúci na okraji veľkých miest či dedinkách preferujú stránky venované medzinárodným reťazcom reštaurácií a pod. (neskoré príchody domov?:))  
 Hľadané trendové výrazy pre "organic" jedlo sú síce na vzostupe (za posledný rok + 30%), no vyzerá to tak, že ich ešte brzdia práve chute vyhľadávať niečo príjemnejšie a známe pre naše kulinárske pohodlie... 
 Zdroj: Hitwise, Allrecipes.com 

