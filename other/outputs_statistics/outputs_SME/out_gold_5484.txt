

 Kostými a stany zbalené a auto nabalené tak, že sa tam už ani mucha nezmestí. Môžeme vyraziť! S civilizáciou sa lúčime v českom diaľničnom fastfoode, ukorisťujeme červeného draka, ktorý sa stáva maskotom celej výpravy. V pražskej zápche stretávame prvých spolubojovníkov a po krátkom blúdení vďaka navigácii podľa mapy zo začiatku deväťdesiatych rokov konečne dorazíme do dedinky Libušín. Rozložíme sa na našom mieste v templárskom tábore a čakáme na zvyšok výpravy, ktorej cesta vďaka GPSku trvala skoro o dve hodiny viac. Slnko už zapadá a my len začíname stavať stany, nakoniec nám to ide lepšie ako sme očakávali a náš prístrešok na najbližšie dva dni je hotový ešte pred úplným zotmením. 
  
 Prezlečieme sa do kostýmov a vydáme sa na obhliadku tábora. Nachádzame krčmu, vodu a toitoiky, okrem toho objavíme miesto, kde sa opeká prasiatko, takže o večeru je postarané. Stretávame bratov johanitov a prijímame pozvanie k ich ohňu. Teplota sa začína blížiť k nule, tak sa zababušíme do kožušín z líšky, prisunieme bližšie k ohňu a postupne ochutnávame rôzne ovocné výrobky z páleníc z celého Československa. Zima začína byť neznesiteľná a keďže nechcem riskovať chorobu pred mojou ďalšou výpravou, vzdávam to a spím v aute. 
  
 Zobudím sa na pocit, že som v saune a pomaly nemám čo dýchať, zisťujem, že namiesto raňajok môžem chystať obed. Po dvoch prebdených nociach som sa konečne dobre vyspala, delím sa o zvyšky jedla a pozorujem, ako sa celý tábor chystá na boj. Pofotím prípravy a idem si obsadiť čo najlepšie miesto na kopec, nech sa mi dobre fotí. Davy sú už nedočkavé a bažia po krvi. Naozaj sme len zvieratká... 
  
 Nakoniec bitka trvá skoro hodinu a pol, súboje bojovníkov v rôznych formáciách sa striedajú s rozprávaním príbehu, ktorý nás vťahuje do boja. Chvíľami mám pocit, že chalani bojujú na život a na smrť... Keď bitka skončí, zvyšok našej partie nenachádzam a namiesto toho sa dozvedám, že zachraňujú ranených na pohotovosti v Kladne. Konečná bilancia je niekoľko rozseknutých obočí a prstov, dva zhorené stany (njn, variť v stane nie je práve najlepší nápad) a jeden rozdrtený malíček. 
  
 Pred stanom si užívam jarné slniečko, prírodu a zisťujem, ako sa cítia zvieratká v zoo, keď si ich chodia obzerať davy. Niektorí sa zastavia aj na kus reči, pochvália kostými a obdivujú zbroj. Proste krásna jarná pohoda. Keď sa chalani vrátia, vydáme sa na nákupy, doplníme výbavu, kŕmime sa pečeným býkom a zapíjame ho čučoriedkovým pivom. Pri ohni si užívame posledný večer, plánujeme, ako bude prebiehať ranný odchod, dopíjame zásoby, vymýšľame ďalšie výpravy a ja si dohadujem škôlku pre moje (budúce) deti :) 
 Living history... 
 Viac fotiek nájdete na mojej stránke www.petraadame.com, konkrétne tu. 
   

