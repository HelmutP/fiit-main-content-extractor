
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Hofbeck
                                        &gt;
                Nezaradené
                     
                 Legalizácia Marihuany - nikdy!!! 

        
            
                                    16.4.2010
            o
            9:30
                        (upravené
                16.4.2010
                o
                15:13)
                        |
            Karma článku:
                6.19
            |
            Prečítané 
            673-krát
                    
         
     
         
             

                 
                    Stále častejšie o tom počúvame a čítame v médiách. Dookola sa opakujúca otázka či legalizovať marihuanu alebo nie. Drvivá väčšina z nás nemá s ňou žiadne skúsenosti a tak si veľakrát ani nedokážeme vytvoriť názor na túto rastlinu. Rôzne články a publikácie sa nám snažia nahovoriť, že marihuana nie je škodlivá a že dokonca lieči. Zo životných skúseností vám však môžem dosvedčiť pravý opak. Marihuana je nielenže škodlivá, ona je doslova prekliata.
                 

                 Našťastie som sám na toto rýchlo prišiel a tak som s touto rastlinou prestal experimentovať už pred rokmi. Naneštastie, osud môjho kamaráta bol iný. Bývali sme v Bratislave na jednom byte, keď sme spolu študovali.  Mal som možnosť sledovať denno denne človeka, ktorý sa nechtiac stal pravidelným konzumentom marihuany. Každá minca má dve strany, samozrejme že sa človekcíti lepšie a všetko sa zdá také zábavné a pestré - to je ale klamanie mysle. Každý ďeň si to musel dať 5 krát, niekedy aj viec, aby sa cítil v pohode. Pred tým to bol ambiciózny a rozumný chalan, ktorý sa pred mojimi očami menil na lenivca bez ambícii sediaceho celý deň pred počítačovými hrami. Niekedy už po sebe ani neumyl riad, bolo mu všetko jedno a nedalo sa s ním vôbec rozprávať. Keď sa jeho frajerka chcela s ním rozprávať na skype, on to zapol a hral pri tom hry. Bola smutná z toho a nechápala, čo sa s ním stalo. Jeho osobnosť sa úplne zmenila a ja som mu zakázal to fajčiť v izbe, povedal som mu nech s tým ide na balkón. Klamal ma, že v izbe nefajčí, no ja som to cítil všade a vyústilo do do konfliktu pri ktorom som mu vykričal, že sa mení na nulu a trosku. Jedného dňa keď sme boli na víkend doma mi celý nadšený volal že prestal. Bol som teda šťastný, lebo mi to bol kamarát. Bol som ale aj asi naivný, lebo po týždni som ten smrad cítil zasa a keď sme sa rozprávali, neustále sa ma pýtal "O čom sme sa bavili?" Podľa neovplyvnených psychológov má marihuana negatívny vplyv na osobnosť a duševné zdravie a zasahuje do toho, čím si vytvárame svoje osudy - do nášho myslenia. Marihuana má moc meniť kongitívne procesy v mozgu a mení vnímanie a posudzovanie originality myšlienky. Čo ma ale zaráža najviac je fakt, že na slovenskej politickej scéne sa objavila strana, ktorá chce tohto démona legalizovať. Zachovajme zdravie našej mysle a nestaňme sa otrokmi. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Slepé Srdce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            PREHLAD
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Pripravme sa na Duel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Prebudený a osvietený mesiac pred voľbami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            REAKCIA
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Hofbeck
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Hofbeck
            
         
        hofbeck.blog.sme.sk (rss)
         
                                     
     
        Obdivovateľ života, kreatívna bytosť, človek, ktorý po sebe chce niečo zanechať.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    734
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




