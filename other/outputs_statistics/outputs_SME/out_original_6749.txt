
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Galbavý
                                        &gt;
                Moje návrhy
                     
                 Tornádo vláda. 

        
            
                                    18.5.2010
            o
            11:53
                        (upravené
                18.5.2010
                o
                14:12)
                        |
            Karma článku:
                9.26
            |
            Prečítané 
            1617-krát
                    
         
     
         
             

                 
                    Megapôžičkou Grécku si eurozóna možno (podľa nemeckej kancelárky Merkelovej) “kúpila čas“. Zároveň sa však vytvorili podmienky na to, aby sa kríza, ktorá teraz vznikla v súvislosti s Gréckom, stala permanentnou hrozbou. Lebo aké z nej vyplýva poučenie? To, že sa netreba báť žiť nad pomery. Pretože toho, kto si takto žil, zachránia na účet tých, ktorí sa správali rozumne a zodpovedne.
                 

                 
  
   Hovorí sa, že malé dlhy sú problémom dlžníkov, veľké dlhy sú problémy veriteľov. A veriteľom sú hlavne nemecké a francúzske banky. Tie roky Grécku požičiavali, kupovali jeho dlhopisy – a mali z toho dobrý biznis. Manžéri dostávali vysoké platy a akcionári hojné dividendy. Teraz sa tieto zlaté časy skončili, naopak, banky sa dostali do problémov. Záchranným balíkom sa problémy týchto súkromných podnikateľských subjektov preniesli na štátnu úroveň a dokonca sa internacionalizujú. Zasahujú aj nás, hoci naše banky a finančné inštitúcie sa na biznise s gréckymi štátnymi cennými papiermi nepodieľali významnou mierou.        Paradoxne dopady na obyvateľstvo budú v našom prípade relatívne tvrdšie, ako napríklad v samotnom Nemecko. Prečo? Pretože objem pomoci na jedného obyvateľa Nemecka je 270 euro a na jedného obyvateľa Slovenska je 150 euro. Ale s ohľadom na rozdielnu úroveň miezd bude jeden Nemec na túto čiastku pracovať asi dva dni, kým priemerný Slovák vyše týždňa.        Máme právo toto od našich občanov vyžadovať? Ako príde priemerný slovenský penzista k tomu, že musí poslať približne polovicu svojej mesačnej penzie na pomoc štátu, v ktorom bude mať aj po prijatí „tvrdých a reštriktívnych opatrení“ penzista dvojnásobný dôchodok? Ako občanom vysvetlíme, že si musíme požičať, aby sme mohli požičať Grécku – a pretože sa na nás bude tlačiť, aby sme dodržali náš rozpočtový deficit, budú si naši občania musieť utiahnuť opasok nie o jednu, ale rovno o tri dierky? Pritom je jasné, že nepôjde o pôžičku, ale o nevratný dar. Pretože podľa mnohých analytikov Grécko aj tak skrachuje. Potom sa jeho dlhy odpustia, ale od nás sa budú vymáhať do posledného centu. A odporučia nám, aby sme si nejako pomohli sami. Asi ako keď sa barón Prášil zachránil z močiara tým, že sa silno ťahal za vlastný vrkoč.        Aby sa zakryli nepopulárne stránky navrhovaného riešenia, začalo sa tvrdiť, že už ani nejde o pomoc Grécku, ale ide o záchranu eura. Lenže ako sa ukazuje, tadiaľto cesta nevedie. Tým zásadným problémom totiž je fakt, že všetky krajiny eurozóny (a nielen tam) žijú nad pomery, všetky sa naučili považovať trojpercentný ročný deficit ako niečo normálne. Všetky sa postupne zadlžujú stále viac. A ani my nie sme výnimkou, naopak. Naše zadlženie síce formálne ešte nie je nad povolenú hranicu, ale narastá enormnou rýchlosťou, a keď sa zrátajú všetky tie záväzky z PPP projektov, zo zadlžujúceho sa zdravotníctva atď., je jasné, že vyhliadky ružové nie sú a že sme hrdinsky vykročili do Atén.        To, čo potrebuje euro na svoju záchranu, teda nie sú miliardové balíky, ktoré v prípade potreby zagarantujú štáty eurozóny. Napokon, behom jedného dňa sa ukázalo, že eufória zo správy o ich vytvorení vyprchala veľmi rýchlo. To, čo Európa a euro potrebujú, je rozpočtová disciplína.       Krokom správnym smerom by mohla byť nemecká iniciatíva – zakotviť pravidlo, aby krajiny eurozóny museli hospodáriť s vyrovnaným štátnym rozpočtom. Teda zásada, že každý sa bude prikrývať len takou perinou, na akú má. Otázne ale je, či sa s touto brzdou na zadlžovanie ostatné krajiny zmieria. To sa týka najmä tých, ktoré doteraz svoje populistické rozhadzovanie a bezstarostné zadlžovanie obhajujú tvrdením, že „ešte stále máme najnižší dlh z krajín eurozóny“, prípadne „ešte stále naše zadlženie nedosahuje povolanú hranicu.“       Ja nie som proti tomu, aby sme boli solidárni s ostatnými štátmi Európskej únie. Ak by bolo Grécko, Taliansko, Španielsko alebo iný štát postihnutý živelnou pohromou – teda zemetrasením, povodňami, vlnou sucha, potom by som s pokojným svedomím v parlamente hlasoval za pomoc. Dokonca aj v prípade, že by sme si na ňu museli požičať. Ale je toto ten prípad?        Pomoc Grécku sa ospravedlňuje bezprecedentným výkladom odstavca 2 článku 122 Lisabonskej zmluvy. Podľa neho sa môže pomôcť členskému štátu eurozóny v prípade, ak má alebo mu hrozia „závažné ťažkosti spôsobené prírodnými katastrofami alebo výnimočnými udalosťami, ktoré sú mimo jeho kontroly.“        Nuž, nezodpovední a populistickí politici sú pohromou a ak ešte zostavia nekompetentnú vládu, je to katastrofa. Z našej domácej skúsenosti ale musím povedať, že nie živelná.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Galbavý 
                                        
                                            Nedajme si nahovoriť, že veci verejné nemôžeme ovplyvňovať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Galbavý 
                                        
                                            Neberte nám Modré z neba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Galbavý 
                                        
                                            Tomáš Galbavý - slušnosť musí zvíťaziť.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Galbavý 
                                        
                                            Modrý alebo červený?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Galbavý 
                                        
                                            Okrádanie pokračuje – dokedy?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Galbavý
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Galbavý
            
         
        tomasgalbavy.blog.sme.sk (rss)
         
                                     
     
        Bývalý poslanec NR SR. V súčasnosti bez politickej príslušnosti.
 
 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2564
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Tlačový zákon a Lisabonská zml
                        
                     
                                     
                        
                            Moje návrhy
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo volíme nacionalizmom?
                     
                                                         
                       Ešte druhý pokus a potom si ju môžeme strčiť ...
                     
                                                         
                       Poctivo pracovať je v tejto krajine jednoducho ekonomický nezmysel
                     
                                                         
                       Ficovina v Komárne
                     
                                                         
                       Prečo sa zúčastním volieb do VÚC
                     
                                                         
                       Ľahostajnosť sa v krajoch nevypláca
                     
                                                         
                       Nedajme si nahovoriť, že veci verejné nemôžeme ovplyvňovať
                     
                                                         
                       Neberte nám Modré z neba
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




