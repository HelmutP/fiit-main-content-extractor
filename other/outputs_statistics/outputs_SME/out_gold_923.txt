

 Pre neznalých problematiky, uvádzam že bežné automobily ako Oktávia, Fábia, 
Roomster, dodávky od rôznych výrobcov a podobne existujú v dvoch kategóriách a to M1 a N1. Kategória M1 je určená v prvom rade na prevoz osôb. Kategória N1 je určená v prvom rade na prevoz nákladu. Pritom vzhľad vozidla, konštrukčné a jazdné parametre vozidla, počet sedadiel a aj veľkosť priestoru pre náklad je rovnaký. Rozdiel okrem zápisu M1 alebo N1 v technickom preukaze je len ten, že vozidlá N1 majú medzi zadnými sedadlami a priestorom pre náklad bezpečnostnú mriežku, ktorá chráni prepravované osoby pred kinetickou energiou nákladu v prípade dopravnej nehody. Na vedenie vozidla M1 a aj N1 je potrebne vodičské oprávnenie B. 

 Z predpisov vyberám: 
Kategórie vozidiel M,  motorové vozidlá s najmenej štyrmi kolesami 
projektované a konštruované na prepravu cestujúcich sa členia na: 
kategória M1 - vozidlá projektované a konštruované na prepravu cestujúcich, 
najviac s ôsmimi sedadlami okrem sedadla pre vodiča, 
kategória M2 - vozidlá projektované a konštruované na prepravu cestujúcich, s viac ako ôsmimi sedadlami okrem sedadla pre vodiča, s najväčšou prípustnou celkovou hmotnosťou neprevyšujúcou 5 000 kg, 
kategória M3 - vozidlá projektované a konštruované na prepravu cestujúcich, s viac ako ôsmimi sedadlami okrem sedadla pre vodiča, s najväčšou prípustnou celkovou hmotnosťou vyššou ako 5 000 kg.
  
Kategórie vozidiel N, motorové vozidlá s najmenej štyrmi kolesami projektované a 
konštruované na prepravu tovaru sa členia na: 
kategória N1 - vozidlá projektované a konštruované na prepravu tovaru s 
najväčšou prípustnou celkovou hmotnosťou neprevyšujúcou 3 500 kg, 
kategória N2 - vozidlá projektované a konštruované na prepravu tovaru s najväčšou prípustnou celkovou hmotnosťou vyššou ako 3 500 kg, ale neprevyšujúcou 12 000 kg, 
kategória N3 - vozidlá projektované a konštruované na prepravu tovaru s najväčšou prípustnou celkovou hmotnosťou vyššou ako 12 000 kg. 

 Výber z emailov čitateľov: 
Chcem Vás upozorniť na nechcené porušovanie dopravných predpisov, konkrétne 
značky dopravnej značky B5 - zákaz vjazdu nákladných vozidiel. Je to z dôvodu, 
že nikto nerozmýšľal, nad tým, že dodávkové auta do 3,5t majú v technickom 
preukaze napísané N1 - nákladné vozidlo. Následkom čoho časť policajtov pokutuje 
vodičov s vozidlami N1 ako (pošta, upc, dhl, nay, atd....) z nerešpektovania 
symbolu nákladného auta na značke. 


Na objasnenie problému si dovolím napísať tri veci: 
1, Vyberanie pokút je dôležitý finančný nástroj a uplatňuje sa za každú 
cenu, bez ohľadu na rozum a logiku.

 2, V pripravovanej vyhláške k novému zákonu je pri značke zákaz vjazdu 
nákladných automobilov taxatívne uvedené, že ide vo vozidlá na prepravu 
nákladov nad 3,5 tony.  

 3, Aj v terajšej právnej úprave je symbol osobného a nákladného vodidla viazaný na tvar vozidla, respektíve jeho vonkajšie rozmery a hmotnosť 3,5 tony a nie na to či je, alebo nie je, v technickom preukaze napísané N1 alebo M1.  


 Myslím, že z nasledujúcich obrázkoch to bude jasnejšie. 

  
Obr. 1. Vozidla N1 majú vjazd zakázaný značkou B4 a nie B5. 

  
Obr. 2. Vozidla N1 majú zakázané predbiehať pri značke B25a. Pri značka B26a 
majú zakázané predbiehať len nákladne vozidlá nad 3,5 tony. 

  
Obr. 3. Najvyššie dovolené rýchlosti pre vozidlá N1 platia tie vľavo. Tie vpravo 
sú pre nákladné vozidlá nad 3,5 tony. 

  
Obr. 4. Takýto zákaz jazdy v ľavom pruhu platí pre nákladné vozidlá nad 3,5 tony 
a nie N1. Taktiež maximálna dovolená rýchlosť platí len pre nákladné vozidlá nad 
3,5 tony. 

  
Obr. 5. Rozdielne parkoviská pre vozidlá vyznačené na dodatkových tabuľkách 
E9-druh vozidla. Ak si stále myslíte, že ak máte vozidlo N1 tak na musíte ísť na 
parkovisko za čerpacou, ktoré je označené symbolom nákladného auta, tak sa pozrite na zem, ako je parkovisko organizované. Zistíte, že parkovacie miesto pre vozidlo 
vpravo je zhruba 3x dlhšie ako parkovisko pre vozidlo vľavo, kam teba ísť 
parkovať aj z vozidlom N1. 

