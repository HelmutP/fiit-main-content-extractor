

 Chladiaci ohriaty vzduch sa ventilátorom dopravuje opäť von, čím sa všetka, vzduchu odovzdaná energia, odvádza nevyužitá. 
 Stúpajúce náklady na energiu nútia výrobcov a aj prevádzkovateľov kompresorov vyvíjať a používať výkonné zariadenia na spätné získanie tepla, ktoré sa dá využiť na vyhrievanie priestorov a prípravu kurenárskej alebo úžitkovej vody. Aby sme vedeli odhadnúť možnosti spätného získavania tepla pri kompresoroch, musíme poznať základy termodynamiky, t.j. ako sa premieňa v kompresore prijatá elektrická energia na teplo. Energia, ktorú dopravuje ohriaty chladiaci vzduch alebo voda, obsahuje asi 95 % elektrickej energie, ktorú sme priviedli do kompresora. 
 Len asi 4 % energie zostávajú ako zvyškové teplo v stlačenom vzduchu a sú odvádzané do tlakovej siete. Zvyšné 1 % sú straty energie sálaním do okolitého prostredia. Aj motor premieňa elektrickú energiu na teplo, takže treba brať do úvahy aj príkon motora. Pomer príkonu a výkonu je jeho účinnosť, ktorá sa pohybuje v hraniciach 80-92 %. Takže napríklad pri 45 kW motore tvorí dodatočná strata energie ďaľších cca. 5 kW. 
   
 Vykurovanie priestoru (miestností, hál). 
 Aby sme mohli využiť teplo, ktoré obsahuje chladiaci vzduch, vyžaduje si to stavebné úpravy, ktoré sa môžu líšiť v závislosti od miestnych podmienok. V prvom rade treba prúd chladiaceho vzduchu viesť priamo do kompresora, opäť ho zachytiť a odviesť. K tomu je prispôsobený protihlukový kryt skrutkového kompresora, ku ktorému je možné napojiť vzduchotechnické kanály na vstup a výstup vzduchu, aby sa dal teplý vzduch privádzať v zime do vykurovanej miestnosti a v lete odvádzať do okolia – mimo budovu. Pri tomto vykurovaní by sa však chladiaci vzduch nemal používať na vykurovanie tých miestností, ktoré sú príliš vzdialené od kompresorovne. Dlhým transportom sa totiž väčšina tepla vzduchu o teplote 50 – 60 OC stratí alebo treba používať tepelne izolované potrubia alebo kanály. 
   
 Ohrev úžitkovej a kurenárskej vody. 
 Ak chceme efektívnejšie využiť odpadové teplo ako v predchádzajúcom prípade, alebo ho dopravovať na väčšie vzdialenosti, sú pre skrutkové kompresory so vstrekom oleja najvhodnejšie doskové výmenníky tepla na ohrev kurenárskej vody alebo bezpečnostné výmenníky tepla na ohrev úžitkovej vody – zväzok rúr s dvojitými stenami. 

  



   





 Uvedené výmenníky pracujú nezávisle od typu chladiaceho systému, nakoľko vodnému alebo vzdušnému chladeniu oleja sa predradí rekuperačný predchladič a tým sa vlastne dostaneme do chladiaceho okruhu skrutkového kompresora. Zariadenia sú okrem toho pripravené aj na priame vykurovanie miestností. Po inštalácii zodpovedajúceho potrubného systému a regulačných prvkov (napr. termostaty, ventily, atď.), je umožnené prepínanie na ohrev vody alebo teplovzdušné vykurovanie. 
 U vodou chladených bezolejových kompresorov je horeuvedená rekuperácia taktiež možná, treba ju ale riešiť na základe miestnych potrieb a špecifík. 
   
 Koľko energie možno ušetriť? 
 Pri zisťovaní nákladov, ktoré je možné ušetriť, musíme brať do úvahy, že využívanie odpadového tepla je závislé od doby prevádzky kompresora pri plnom výkone, nakoľko len v takomto prípade sa produkuje využiteľné odpadové teplo. 
 Množstvo tepla, ktoré je obsiahnuté v chladiacom médiu, je možné vypočítať podľa základných zákonov termodynamiky alebo podľa tabuliek – viď tabuľky (obr.6 a 7). 

     



 Pohon 
   
 [kW] 


 Využiteľný 			výkon 
   
 [kW] 


 Množstvo 			tepla 
   
 [kJ/h] 


 Množstvo 			teplého vzduchu 
 [m3/h] 


 Ekvivalent 			vyk. olej 
 [l/h] 




 5,5 
 7,5 
 11 
 15 


 6,47 
 8,72 
 12,29 
 16,29 


 23292 
 31392 
 44259 
 58629 


 1350 
 1350 
 1700 
 2000 


 0,65 
 0,87 
 1,23 
 1,63 




 18,5 
 22 
 30 
 37 


 19,86 
 23,35 
 31,84 
 39,27 


 71492 
 84067 
 114637 
 141385 


 3100 
 3450 
 4500 
 5500 


 1,99 
 2,34 
 3,18 
 3,93 




 45 
 55 
 65 


 47,77 
 57,73 
 68,23 


 171955 
 207845 
 245635 


 6500 
 8000 
 8600 


 4,78 
 5,77 
 6,82 




 75 			+ 4 
 90 			+ 4 
 110 			+ 4 


 86,69 
 102,78 
 125,91 


 312093 
 370006 
 453291 


 9200 
 11500 
 15000 


 8,67 
 10,28 
 12,59 




 132 			+ 5,5 
 160 			+ 11 


 151,44 
 180,04 


 544184 
 648144 


 20000 
 34000 


 15,14 
 18,0 




 200 			+ 15 
 250 			+ 18,5 


 226,09 
 280,75 


 813924 
 1010700 


 38500 
 45000 


 22,6 
 28,1 




 Tab. č.6 

       



 Pohon 
   
   
 [kW] 


 Odvádzaný 				výkon 
   
 [kW/h] 


 Množstvo 				tepla 
   
 [MJ/h] 


 Množstvo 				vody pri 


 Ušetrenie 				nákladov pri 1000 Bh 
 [DM] 






 Δt 				25 K 
 313→338 				K 
 [m3/h] 


 Δt 				35 K 
 293→328 				K 
 [m3/h] 


 Δt 				50 K 
 293→343 				K 
 [m3/h] 






 11,0 


 8,9 


 32,0 


 0,305 


 0,217 


 0,152 


 449,- 




 15,0 


 12,3 


 44,2 


 0,420 


 0,300 


 0,210 


 620,- 




 18,5 


 14,8 


 53,2 


 0,509 


 0,363 


 0,255 


 746,- 




 22,0 


 17,7 


 63,7 


 0,609 


 0,435 


 0,305 


 894,- 




 30,0 


 24,4 


 87,8 


 0,835 


 0,596 


 0,417 


 1232,- 




 37,0 


 30,3 


 109,0 


 1,040 


 0,743 


 0,520 


 1530,- 




 45,0 


 37,7 


 135,7 


 1,295 


 0,925 


 0,647 


 1905,- 




 55,0 


 45,5 


 163,8 


 1,565 


 1,118 


 0,782 


 2300,- 




 65,0 


 54,9 


 197,6 


 1,885 


 1,346 


 0,942 


 2770,- 




 75,0 


 63,1 


 227,1 


 2,170 


 1,550 


 1,085 


 3187,- 




 90,0 


 74,0 


 266,4 


 2,545 


 1,818 


 1,272 


 3740,- 




 110,0 


 90,0 


 324,0 


 3,095 


 2,210 


 1,547 


 4547,- 




 132,0 


 110,5 


 397,0 


 3,800 


 2,714 


 1,900 


 5570,- 




 160,0 


 133,5 


 480,6 


 4,590 


 3,278 


 2,295 


 6745,- 




 200,0 


 168,3 


 605,8 


 5,790 


 4,136 


 2,895 


 8500,- 




 250,0 


 208,9 


 752,0 


 7,180 


 5,128 


 3,590 


 10550,- 




 Tab. č. 7 
   
 Hodnoty uvedené v tabuľke sú množstvá využiteľného výkonu, ktoré vznikajú pri prevádzkovaní nami dodávaných skrutkových kompresorov (BOGE, HAFI) za plného zaťaženia. 
   
 Záverom môžeme konštatovať, že rozhodnutie, či využívať odpadové teplo vzniknuté pri výrobe stlačeného vzduchu, príp. akým efektívnym spôsobom, je podmienené miestnymi podmienkami, t.j. veľkosťou inštalovaných kompresorov, času amortizácie zariadení a času využitia kompresorov v plnej záťaži.  
 zdroj. Ulrich Bierbaum (Boge Kompressoren GmbH): Druckluft - Kompendium 
   
   
   

