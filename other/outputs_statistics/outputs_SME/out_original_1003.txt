
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Fülöp
                                        &gt;
                svet okolo mňa
                     
                 Vojna pokračuje 

        
            
                                    15.11.2008
            o
            15:11
                        |
            Karma článku:
                11.68
            |
            Prečítané 
            2138-krát
                    
         
     
         
             

                 
                    Pred piatimi mesiacmi som si myslela, že som zvíťazila. Áno, bolo to víťazstvo, no nevyhrala som vojnu, iba jednu bitku. Teraz začína druhé kolo a ja doňho vstupujem s vedomím, že to musí byť bitka konečná. Musím rakovinu knokoutovať, aby jej bolo jasné – u mňa si už neškrtne!
                 

                  Niektorí z vás čítali moje predchádzajúce články a viete, v akej som bola situácii ani nie pred rokom. Moje telo mi tentoraz múdrejšie naznačilo, že niečo nie je v poriadku a išla som hneď k lekárovi. Rakovina sa snažila rovnako ticho zakrádať, aby sme si ju nevšimli a mohla sa rozrastať, ale tentokrát jej to nevyšlo. Chytila som ju popod krk ešte predtým, ako sa mohla plne rozvinúť. Stúpajúci marker je toho svedkom. Tempo stúpania klesá. Bojujem momentálne na všetkých frontoch, takže straty zaznamenáva jedine ona.       Som znova v plnej sile a momentálne nezoslabnem kvôli operácii, lebo tá nie je nutná. Aj chemoterapia bude miernejšia, bez takých vážnych vedľajších účinkov, ako predtým. Nebude mi zle a nevypadajú mi vlasy. Už aj táto správa niekoho poteší. Samozrejme, chemoterapia nestačí. Nestačí ani zmena stravovania, lebo to som spravila hneď po prvej diagnóze. Treba niečo viac a ja už viem čo.       Určite sa teraz pýtate, ako sa toto celé dá zvládnuť. Keď som sedela v čakárni a mali mi oznámiť výsledky vyšetrení, prosila som v duchu Boha, aby mi dal silu zniesť všetko, čo mi povedia s dôstojnosťou a aby mi dal dosť síl bojovať. Moje želanie bolo vyslišané. Aj keď sa mi rozbúchalo srdce a na chvíľu som sa strašne ľutovala, prešlo to pomerne rýchlo. Občas ma napadne, keď vidím, ako sa na ulici ľudia ponáhľajú, že ako im je dobre, majú iné starosti, nemusia bojovať s rakovinou, môžu zarábať peniaze, vybavovať, starať sa... Ale väčšmi ako tieto myšlienky mám v sebe veľké presvedčenie, že im všetkým, všetkým týmto ľudom na ulici dokážem, že sa to dá. Že sa človek aj s takouto vážnou diagnózou, aj s recidívou v takom krátkom čase môže vyliečiť a zostať zdravý ešte ďalších päťdesiat rokov. Dokážem to aj vám, ktorí čítate tieto riadky. Ešte o mne budete počuť...  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Skutočný príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Prečo sa mi oplatí žiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Rakovina moja každodenná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            (Skoro) Perfektný večer v Aréne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Fülöp 
                                        
                                            Ľudia na onkológii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Fülöp
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Fülöp
            
         
        evafulop.blog.sme.sk (rss)
         
                                     
     
        Som matka, manželka, dcéra, priateľka, kamarátka, známa aj neznáma a som aj onkologický pacient
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3253
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Mám sa stále lepšie
                        
                     
                                     
                        
                            svet okolo mňa
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Silvia Sabova
                                     
                                                                             
                                            Miriamin blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Internetový časopis Priateľka
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




