

 Mal som sen...o krajine, kde liekom na ekonomické krízy neboli len predražené štátne projekty a  projekty verejno-súkromného partnerstva (PPP), a kde vláda nezadlžovala zbytočne ďalšie generácie. 
 Mal som sen...o krajine, kde sa za zvyšovanie zamestnanosti nepovažovalo zriaďovanie pochybných sociálnych podnikov, ale skutočná sociálna a zamestnanecká politika a podpora tvorcov pracovných miest. 
 Mal som sen...o krajine, ktorá efektívne čerpala finančné prostriedky ponúkané ostatnými partnermi veľkej európskej rodiny, o krajine, ktorá používala tieto financie v súlade s dohodami a pravidlami, a konečne sa zbavila nálepky  provinčnej zaostalosti. 
 Mal som sen ...o krajine, kde sa dostalo každému primeranej zdravotnej starostlivosti , kde sa neorganizovali verejné a pouličné zbierky na zakúpenie potrebných medicínskych prístrojov, o krajine, kde sa ľudia dožívali zaslúženého dôchodku, finančne posilnení a aj o slušné výnosy z druhého piliéra dôchodkového sporenia. 
 Mal som sen...o krajine, v ktorej sa mladé rodiny dostali k vlastnému bývaniu aj iným, štandardnejším spôsobom, než sú nedosiahnuteľné mladomanželské pôžičky, a kde sa deti rodili nielen z dôvodu jednorázového štátneho príspevku pri ich narodení. 
 Mal som sen...o krajine, kde sa sudcovia už neospravedlňovali za svoje zlyhania slepotou bohyne spravodlivosti, nestýkali sa s podsvetím a na ich čele stáli skutočné morálne a erudované autority, ktoré boli príkladom aj pre poslaneckú snemovňu, ministerstvá a ostatné silové zložky tejto krajiny. 
 Mal som sen...o krajine, kde sa ľuďom nevnucoval cit k vlasti zákonom, kde politici neperzekvovali novinárov a ctili si hlas a vôľu občana. 
 Mal som sen...o krajine, kde úcta k prírode bola viac ako záujmy bohatých,a kde sa v národných parkoch nestavali lunaparky. 
 Mal som sen..., že sa do parlamentu nedostala tá najnárodnejšia slovenská strana a zostala tak za bránami parlamentu, hašteriac a trhajúc sa na pravú a starú, nechávajúc si tak zájsť chuť na všetky tie ďalšie predaje emisií a všetky tie verejné obstarávania a nástenkové tendre. 
 Mal som sen..., že 12. júna tohto roku, slnečné lúče nasmerovali ľudí namiesto do záhradiek a na slovenské končiare, do volených miestností, k volebným urnám a účasť na voľbách bola najvyššia za celé slobodné ponovembrové obdobie. 
 Mal som sen..., že konečne zvíťazil rozum. 

