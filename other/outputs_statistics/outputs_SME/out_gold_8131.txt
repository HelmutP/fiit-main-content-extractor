

 
 
 Bola treskúca zima, normálny človek by ani psa nevyhodil von. Na lavičke na slávnom „Václaváku“ sedel muž. Na sebe mal obnosenú hnedú bundu, ktorá sa na niektorých miestach prederavila. Skrehnuté nohy si masíroval rukami, aby mu nezamrzli na kus ľadu. Hlavu mal sklopenú a svojim pohľadom prechádzal po ľadom pokrytej dlažbe. 
 Muž sa zrazu strhol. Zľakol sa detského kriku za jeho chrbtom. Dve rozradostené deti sa guľovali asi dva metre za ním. Niečo si zamrmlal popod nos a siahol po poháriku vareného vína, čo mal vedľa seba. 
 Kdesi z diaľky sa ozvali kostolné hodiny. Bolo päť hodín podvečer a z metra sa postupne začali rojiť ľudia. Boli to obyčajní ustarostení robotníci a zamestnanci rôznych firiem, ktorí sa ako každý deň ponáhľali domov. 
 Muž bol už dávno zvyknutý na nepozornosť týchto ľudí. Mali prácu, rodinu, byty... všetko čo potrebovali ku životu. Nikto sa nepotreboval zahadzovať s bezdomovcom. Hlavne takýmto. 
 Muž teda opäť sklonil hlavu, a tentoraz skúmal dno prázdneho plastového pohárika od vareného vína. 
 -„Pane, vy byste si měl najít ubytování! Nemůžete přeci takhle mrznout tady venku! Co se vám vlastně stalo?“Ozvalo sa spoza mužovho chrbta. Ten chvíľu zostal ako obarený, no nakoniec sa zmohol na odpoveď. 
 -„Vy viete čo sa mi stalo. Veľmi dobre to viete!“ 
 -„Jo aha, vy jste Slovák! To je mi hned jasný! Taky jste museli utéct před bankrotem?“ 
 -„Musel som, tak ako veľa iných ľudí. Cítim sa zodpovedný za to čo sa stalo.“ 
 -„Hele vy jste Fico? Nebo kdokoliv kdo dostal vaši ekonomiku na úplné dno?“ 
 -„Nie ja som niečo horšie. 12. Júna 2010 som bol lenivý vstať z postele a rozhodnúť o našej budúcnosti…“ 
 -„Jo aha, vám tedy už není pomoci pane! Promarnili jste šanci na změnu! My jsme ji využili, vy ne.A teď vidíme, jak to dopadlo! No komu není rady, tomu není ani pomoci!“ 

