
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Simon Štefanka
                                        &gt;
                Účesy - tipy a triky
                     
                 Účesy: jemné vlasy a melír 

        
            
                                    9.2.2009
            o
            9:13
                        |
            Karma článku:
                10.65
            |
            Prečítané 
            8642-krát
                    
         
     
         
             

                 
                    Devätnasťročná študentka Ivana z Bratislavy: "Mám predstavu o slušivom účese, ktorý by nevyžadoval veľa času. Fúkanie a úprava sú samozrejmosťou, ale mám jemne vlasy, takže žehlenie v žiadnom prípade. Účes by mal hlavne byť postupný, aby vlasy vyzerali hustejšie. Rada si vlasy rôzne upravujem keď idem von, takže asi nieco dlhšie. Farba by podľa mna akurát nemala byť čierna a nie som veľmi ani za červené/bordové odtiene. Mám radšej dlhšie alebo polodlhé vlasy, proste hlavne nie krátke, nech si ich viem dať do gumičky... Ale ak sľúbiš, že to bude vyzerať dobre, som skoro za všetko..."
                 

                  Stav vlasov: Ivana má veľmi jemné a suché vlasy, v ktorých bol aplikovaný blond melír. Štruktúrou sú porézne, tmavý blond bez lesku a jemne sa vlnia. Majú tendenciu "motať" sa do seba.    Stav strihu: Polodlhý neudržiavaný strih bez objemu.     Typ tváre: "Srdce" s nižším čelom a výraznejšími lícnymi kosťami, ktoré pridávajú tvári neželanú šírku.    Výsledok premeny a doporučení:      
 

     Strih, farba a styling: Simon Štefanka  Líčenie: Lenká Orságová  Foto: Petit Press, a.s. 2008   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simon Štefanka 
                                        
                                            Návšteva kaderníka? 6 tipov ako to zvládnuť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simon Štefanka 
                                        
                                            Zmena účesu pre Lucku: totálna, radikálna, absolútna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simon Štefanka 
                                        
                                            Chcete byť blond? Zosvetľujte postupne. Časť prvá...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simon Štefanka 
                                        
                                            Keď je ofina nutnosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simon Štefanka 
                                        
                                            Dlhé vlasy za každú cenu? Nie, veru nie...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Simon Štefanka
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Simon Štefanka
            
         
        stefanka.blog.sme.sk (rss)
         
                                     
     
         Pracujem ako kaderník v Bratislave. že  ma práca baví je dúfam zrejme z tohto blogu... :) Viac napovie GOOGLE... :) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    30
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3978
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Účesy - tipy a triky
                        
                     
                                     
                        
                            Starostlivosť o vlasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Kaku - Hyperprostor
                                     
                                                                             
                                            Young - Chatrč
                                     
                                                                             
                                            Smoley - Zakázaná viera
                                     
                                                                             
                                            Szántó - Dejiny Anglicka
                                     
                                                                             
                                            Hirax - Raz aj v pekle vyjde slnko
                                     
                                                                             
                                            Raeper, Smith - Myslenie západnej civilizácie
                                     
                                                                             
                                            Kung - Katolícka cirkev - stručné dejiny
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Sinéad O Connor - Fire of Babylon, Troy
                                     
                                                                             
                                            Verdi - Nabucco, Aida
                                     
                                                                             
                                            Grigorov - Balady
                                     
                                                                             
                                            Vivaldi - The Four Seasons
                                     
                                                                             
                                            Muller - 2001
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Návšteva kaderníka? 6 tipov ako to zvládnuť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




