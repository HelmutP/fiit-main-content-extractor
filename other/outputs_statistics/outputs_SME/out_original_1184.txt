
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Koiš
                                        &gt;
                Médiá @ komunikácia
                     
                 Smutný koniec RTI, jediného slovensko-anglického rádia 

        
            
                                    26.1.2009
            o
            13:01
                        |
            Karma článku:
                10.45
            |
            Prečítané 
            3535-krát
                    
         
     
         
             

                 
                    Mnohí ho možno ani nepoznali. Pre mnohých môže byť neznámym pojmom. Prieskum počúvanosti ho prakticky nezaznamenal, no napriek tomu neskončilo kvôli finančným problémom. Projekt prvého nadnárodného rádia pôsobiaceho zo Slovenska včera ukončil svoje pôsobenie. Rádio RTI z Popradu vchádza do histórie. Dôvod? Nevysielalo len po slovensky. A to jazykový zákon neodpúšťa.
                 

                 
Zdroj: Facebook 
   Slovenská mediálna legislatíva je v mnohom neštandardná. Presvedčil sa o tom aj tím popradského nadregionálneho rádia RTI, ktoré šírilo dobré meno Slovenska za hranicami. Ojedinelý a v mnohom priekopnícky projekt slovensko-anglického vysielania sa zrodil pred necelými štyrmi rokmi (9. apríla 2005), keď na troskách vtedajšieho Rádia Tatry vzniklo nadregionálne rádio RTI. Pôvodné Rádio Tatry patrilo medzi najstaršie rozhlasové stanice na Slovensku. Jeho pôvodnú licenciu však odkúpila skupina okolo silnejšieho Fun rádia a jeho vysielanie v roku 2001 zastavila. Pôvodná myšlienka Rádia Tatry sa mala vrátiť o niekoľko rokov neskôr, takmer však skončila ešte skôr ako sa stihla rozbehnúť.    Tesne pred prepadnutím licencie obnoveného Rádia Tatry sa do projektu vložil investor, českobritský podnikateľ Jan Telenský, o.i. majiteľ popradského areálu Aqua City. Z jeho prostriedkov vzniklo Rádio Tatry – RTI, ktoré začalo vysielať na lukratívnej podtatranskej frekvencii 94.2 MHz a pokrývalo významnú časť severovýchodného Slovenska, časť Poľska, a čiastočne presahovalo aj do Maďarska. Bolo to zároveň prvé slovenské regionálne rádio, ktoré sa objavilo aj na satelite. Na pozícii 28.5 stupňov východne na družici Eurobird začalo vysielať pre britské domácnosti v rámci satelitnej televízie SKY Digital.    Projekt to bol výrazne nadčasový a dá sa povedať, že aj megalománsky, ale dokázal prilákať pozornosť. Vo Veľkej Británii bolo RTI dostupné pre milióny domácností, ktoré prijímali SKY Digital, a jeho význam ako média prinášajúceho zmienky o Slovensku vzrastal. Program rádia sa vyznačoval slovenským vysielaním počas dňa a anglickým vysielaním vo večerných a nočných hodinách. Časť programu vznikala v Poprade, časť v Londýne. Tak to fungovalo niekoľko rokov. Podobne ako kedysi na našom území vysielala pobočka BBC World Service v angličtine a slovenčine.    Čo ale nikomu nenapadlo bol fakt, že existencia tohto rádia bola v slovenskom legislatívnom prostredí prakticky odsúdená na zánik. Vysielanie v angličtine bolo totiž od začiatku v rozpore so Zákonom o štátnom jazyku. Konkrétne ustanovenie zákona neumožňuje vysielať cudzojazyčné informácie bez toho aby boli pretlmočené do slovenčiny. Napriek tomu, že regulačný orgán niekoľkohodinové nočné vysielanie v angličtine pár rokov toleroval, v posledných mesiacoch na RTI pritlačil a začal striktne vyžadovať plnenie zákonných podmienok.    Manažéri rozhlasovej stanice mali dve možnosti. Buď sa pokúsia pretlmočiť celú anglickú časť programu do slovenčiny, alebo budú vedome porušovať zákon a vystavia sa sankciám. Napokon si zvolili tretiu cestu – paradoxne – a ukončili vysielanie. Nebolo to síce úplne bez boja, bolo to však asi najhoršie riešenie zo všetkých zlých. Nepomohol ani lobing zástupcov britského veľvyslanectva v Bratislave ani množstvo poslucháčskych dopisov a ohlasov, ktoré sa zástupcom rádia podarilo zaktivizovať.    Končí projekt, ktorý na Slovensku predbehol dobu. Končí jediné cudzojazyčné komerčné rádio v našich končinách, hoci v zahraničí sú podobné projekty úplne bežné. Neďaleko – v Prahe vysiela v FM pásme francúzske rádio RFI, a české Expresrádio pravidelne ponúka moderované bloky v angličtine. Viaceré tamojšie stanice vysielajú aj po slovensky – zákony to umožňujú. V Rakúsku dokonca verejnoprávny rozhlas ORF prevádzkuje jeden takmer čisto anglický okruh – tiež je všetko v poriadku. Problém s cudzími jazykmi ostatne nemajú takmer nikde v Európskej únii – snáď okrem Slovenska.    Na Slovensku stále platí zvláštny zákon o ochrane štátneho jazyka, ktorý prikazuje médiám hovoriť „na Slovensku, po slovensky“. Bez výnimky. Iba čeština je sem-tam povolená, a maďarčinu a ďalšie menšinové jazyky si môže dovoliť len Slovenský rozhlas, ktorý má na to oporu v zákone. Súkromné médiá však musia kultivovať len štátny jazyk. Ak chcete vysielať v inom jazyku, na Slovensku o licenciu nežiadajte. Ibaže si zabezpečíte simultánny preklad.    Rádio RTI nebodovalo v prieskumoch počúvanosti, mimo Popradu bolo prakticky málo známe. Mnoho Slovákov o ňom zrejme počuje prvý raz. Bolo však ukážkou toho, ako môže zaujímavá myšlienka v otvorenej spoločnosti rezonovať aj ďaleko za našimi hranicami. Malo však smolu. Nevysielalo len po slovensky ale snažilo sa podporovať aj iné jazykové schopnosti Slovákov, zatiaľ čo šírilo dobré meno našej krajine v Európe. A kvôli tomu ho bude škoda. Legislatíva sa zo dňa na deň nezmení. Štátny jazyk si môžeme naďalej chrániť bez ohľadu na to, že nebudeme ovládať tie cudzie. Mrzí ma, že Slovensko v určitých aspektoch sa ešte stále nechová ako moderná krajina v modernej Európe. A je otázne, či sme ochotní sa jej dostatočne otvoriť.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Malé desatoro SaS: Koncesionárske poplatky (už) nezrušíme!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Referendum podľa Fica: občianska povinnosť len keď sa mi to hodí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Pán Kmotrík postaví štadión, prispejeme mu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Do tankoch a na živnostníkov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Omeškal som sa so splátkou úveru. O mínus jeden deň (aktualizácia)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Koiš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Koiš
            
         
        kois.blog.sme.sk (rss)
         
                                     
     
        Rád píšem o všetkom, čo ma zaujme. A rád dávam najavo aj pozitívne skúsenosti, ktoré nevnímam ako samozrejmé.


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    91
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Letecká doprava
                        
                     
                                     
                        
                            Cestovanie vlakom
                        
                     
                                     
                        
                            Spoločnosť a politika
                        
                     
                                     
                        
                            Médiá @ komunikácia
                        
                     
                                     
                        
                            Hokej a futbal
                        
                     
                                     
                        
                            Život v Bratislave
                        
                     
                                     
                        
                            Život v Prahe
                        
                     
                                     
                        
                            Ostatné články
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Gabin - La Maison
                                     
                                                                             
                                            Tom Waits - What's he building in there?
                                     
                                                                             
                                            Kings of Convenience - I'd rather dance with you
                                     
                                                                             
                                            One Night Only - Just for tonight
                                     
                                                                             
                                            Counting Crows - A Murder of One
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Ščobák
                                     
                                                                             
                                            Jozef Havrilla
                                     
                                                                             
                                            Jan Potůček
                                     
                                                                             
                                            Sergej Danilov
                                     
                                                                             
                                            Lukáš Polák
                                     
                                                                             
                                            Zdeno Jašek
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Richard Sulík
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Digitálně.tv
                                     
                                                                             
                                            Digizone.cz
                                     
                                                                             
                                            RadioTV.sk
                                     
                                                                             
                                            RadioTV.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chrániť ľudí pred vrahom (a naopak)
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




