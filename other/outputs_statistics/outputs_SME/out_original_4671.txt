
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Černák
                                        &gt;
                Chvála jedla
                     
                 O Hríbovi, lampe, Filcovi, deťoch a hokeji... 

        
            
                                    16.4.2010
            o
            11:50
                        |
            Karma článku:
                13.17
            |
            Prečítané 
            3404-krát
                    
         
     
         
             

                 
                    Keď som uvidel včera večer, kto bude hosťom v lampe, tak som dúfal, že Hríb s Filcom a Miklánkom sa budú baviť o mládeži, o tom ako pomôcť hokeju a podobne. Bohužiaľ, názory prezentované spomenutými pánmi boli zmesou klišé, poloprávd, vlastných protirečení a ja neviem ako to ešte nazvať. BTW, som ten "hokejový tatko", ktorého mail bol citovaný v relácii a skúsim sa vyjadriť z pohľadu rodiča
                 

                 Na úvod by som si dovolil poznamenať že si pána Filca vážim, a za jeho úspechy či už s juniormi, alebo seniormi mu patrí jedno veľké ďakujem. A čo sa týka mojej motivácie, tak nemienim zo svojich chlapcov vychovať ďaľších Gáboríkov.Jednoducho som rád, že si našli nejaký koníček (naozaj si ho našli sami) a dokiaľ ich to bude baviť budem ich v tom maximálne podporovať.   Myslím, že vo včerajšej debate všetkým unikol jeden podstatný bod. Podpora športu ako prevencia pred "inou" neželanou činnosťou detí  a mládeže. Osobne si myslím, že čím viac detí bude športovať tým menej budeme musieť v budúcnosti riešiť zdravotné a iné problémy a je úplne jedno, či sa bude jednať o hokej, futbal, tenis alebo bárs aj baseball.   Rovnako si nemyslím že dnešné deti sú lenivšie, tak ako to povedal Miklánek. Je síce pravdou, že možnosti zábavy sú rozmanitejšie (najmä PC-ka, PSP-ka, X-boxy sú veľmi lákavé a čo si budeme navrávať aj ja som nad nimi strávil zopár hodín...), ale tie decká dajú okamžite prednosť aktívnej hre niekde na ľade. Tie deti nie sú lenivšie, len nie sú aktívne vedené k pohybu. A dokiaľ sa na takéto ativity bude spoločnosť pozerať ako na zábavku bohatých tak sa veľa nezmení ( v tejto súvisloti vyzerá úplne smiešne  a provokačne dotácia Ministerstva školstva na golfový turnaj pána Sotáka)   Ďaľším včera spomenutým bodom bol zánik/rozklad hokejových tried. Tu je dôvod pomerne jednoduchý. Rodičia (aspoň väčšina) si začala vážiť vzdelanie a nechcú vychovať zo svojich detí (len to prosím nebrať osobne) hokejových idiotov. Ak hokejová trieda, tak potom na škole, kde bude rovnaký dôraz dávaný na vzdelanie, kde tie deti sa budú skutočne vzdelávať a nie len popri hokeji chodiť do školy.   Najväčším problémom malých detí nie sú ani tak peniaze (mesačné poplatky za dieťa sú od 20 do 60 Eur, výstroj  - aj vďaka nadácii ktorú voľakedy viedol Filc  -väčšina klubov dokáže poskytnúť, výstroj sa dá zohnať aj z druhej ruky, kto má rodinu, alebo priateľov v USA tak má vyhrané, pretože ceny sú tam o 50 % nižšie ako v Európe.), ale čas. Ak chcete stíhať tréningy, tak musíte utekať z práce, skoro ráno vstávať, víkendy trávite na turnajoch, vozíte vlastné a cudzie deti, pomáhajú vám starí rodičia a v podstate celá rodina. A celé sa to odvíja od množstva ľadu (rozumej krytých štadiónov) Vyslovene ma nadvihla poznámka Miklánka aj Filca, že nové haly by nepomohli... Veď si to len spočitajte, Bratislava má 400 000 obyvateľov, detí do 10 rokov nech je cca 20 000 (čo je 5%), chlapcov z toho polovica teda 10 000. A teraz Bratislava má 3 ľadové plochy (Dúbravka, Ružinov má dve , ale kvôli Slovanu je tam obrovský problém s ľadom, plus malá ľadová plocha v nákupnom centre v Avione. Okrem toho ešte súkromníci postavili zo súkromných peňazí zimák v Hamuliakove, podobný projekt v Petržalke bol zastavený). Koľko detí má reálne šancu sa dostať na ľad??? 200 ? A není to málo Antone Pavloviči? Ja rozumiem argumentu, že prevádzka zimného štadióna (oproti napr. toľko pertraktovaným malým futbalovým ihriskám, alebo aj tenisovým kurtom) je určite podstatne drahšia, ale ak to dokážu v malých dedinkách  ako sú Tŕemošná, Rosice, alebo Veľká Bíteš, tak prečo sa to nedá v okresných mestách na Slovensku???? To štát a zväz naozaj radšej dá miliardy eur na nezmyselnú prestavbu starého zimmného štadióna  ( a na provízie, úplatky, a pod) ako by podporil prevádzku zimných štadiónov. Gáborík dokázal že sa to dá, tak prečo to nejde aj inde? Prečo napr. v Prahe Letňanoch súčasťou výstavby veľkého nákupného centra bola podmienka developerovi, že tam musí postaviť aquapark a zimný štadión? A už úplne ma dorazilo protirečenie oboch pánov, keď na jednej strane tvrdia, že nové štadióny by nepomohli, ale na druhej tvrdili, že nech sa pozrieme na Maďarsko, koľko tam sa investuje do hokeja, koľko štadiónov postavili... Sakra páni, tak ako to je vlastne...   Úplne ma dorazilo tvrdenia Miklánka, že hokej je len pre deti bohatých rodičov. Nie je to pravda, je to obrovské klišé, ktoré hokeju strašne ubližuje. Ešte raz, nie je to najlacnejšie, ale najväčším problém je čas. A ak by to aj pravda bola, tak zväz musí hľadať alternatívne riešenia pre deti zo sociálne slabších rodín. A v tomto zväz úplne rezignoval na svoju funkciu, spolieha sa na rodičov a potom sa veľmi diví súkromným iniciatívam. Svojou nepodporou deťom, zväz doslova kradne budúcnosť hokeja.   A čo sa týka SZĽH. Budem trvať na svojom vyjadrení, že zväz deťom a ľuďom okolo nich nepomáha, ale presne naopak im hádže polená pod nohy. Ešte raz ten príklad, Košice usporiadali turnaj pre štvrtákov, kde sa hralo na celú plochu. Pretože podľa názoru zväzu, je toto neprípustné (napr. v Kanade sa s týmto nejako nepárajú a odmalička sa tam rieši celá plocha...), tak Košiciam ako usporiadateľom udelil pokutu. Rozumiete??? Súkromný turnaj pod hlavičkou súkromnej firmy bol dôvodom na udelenie pokuty. A to už ani nevravím o tom, že tie decká do 5 mesiacov budú aj podľa SZĽH hrať na veľkom ihrisku. Čo také zásadné sa udeje s tými deckami, že v marci je to dôvod na udelenie pokuty a v septembri tak budú hraž oficiálne??? Prečo ma nutne napadá ono povestné, že každá iniciatíva bude po zásluhe potrestaná ? Inak je zaujímavé, že zväz organizuje pre piatakov Považský pohár, kde tie decká už musia chápať zakázané uvoľnenie, ofsajd a podobne, ale kde sa to majú naučiť??? (BTW, tie pravidlá sú tam úplne strašné, ešte aj siedmaci hrajú na nemenné päťky, to jest, tréner pred zápasom, označí jednotlivé päťky farebnými páskami, hráčov počas zápasu nesmie medzi päťkami prestriedať alebo nechať sedieť keď sa chalanovi nedarí, alebo nepočúva pokyny. Rovnako nesmie nasadzovať lajny ľubovoľne, ale vždy len červenú proti červenej súpera. Strieda sa po minúte, na pokyn rozhodcu sa preruší hra, strieda povinne celá päťka...) Inak včera Filc brojil proti tomu turnaju v Košiciach aj s argumentom, že celý svet ide formou malého hokeja. A ako to teda vyzerá na Slovensku? Pre štvrtákov organizuje SZĽH spolu s Orange "Orange minihokej tour".  Je to 6 turnajov po 6 mužstiev, víťaz každej skupiny postúpi do finále do KE. Hrá sa na v tretine na šírku 4 na 4. Ak mužstvo nevyhrá turnaj, tak to znamená že za sezónu odohrá 3 zápasy v minihokeji a na budúci rok môže hrať hokej na veľké ihrisko.... pripadá vám to ako koncepčná práca? Skutočne ma prekvapilo vyjadrenie Filca (úplne presne vedel o akom turnaji som vo svojom maili vravel), o nejakom smiešnom pobiehaní detí na ľade.   Prečo rovnako staré deti môžu  - sj so zvolením ich zväzov  -  "smiešne" pobehovať po tenisových kurtoch, alebo futbalových trávnikoch??? Prečo títo môžu a malí hokejisti nie? Prečo všetci zodpovední tvrdia, že malí deti do štvrtej triedy nemajú čo hrať hokej, len a len trénovať? Prečo deťom berú radosť z hry??? Ktoré decko vydrží otročiť na tréningoch (úroveň trénerov je kapitola sama o sebe) bez toho aby dostalo odmenu v podobe zápasu??? Hokej je hra, tak prečo nedovolíme deťom hrať sa???   Úprimne, včera som niektorými vyjadreniami bol veľmi sklamaný a obávam, že olympiáda bola naozaj nadlho posledným úspechom             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Černák 
                                        
                                            O Bratislave a o "tiežbratislavčanoch"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Černák 
                                        
                                            O detskom hokeji, už zasa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Černák 
                                        
                                            O mojej generácii a včerajšku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Černák 
                                        
                                            O dvadsiatke, osemnástke a hokeji celkovo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Černák 
                                        
                                            Milá Tatrabanka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Černák
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Černák
            
         
        cernak.blog.sme.sk (rss)
         
                                     
     
         Som (skoro) 45-ročný (Preboha, to už naozaj???) normálny chlap, s normálne vyvinutým egom (t.j. myslím si o sebe, že som inteligentnejší,krajší a príťažlivejší ako zbytok tohto sveta) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3180
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Chvála jedla
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Er ist wieder da
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Dire Straits - všetko
                                     
                                                                             
                                            Peter Gabriel - SO
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List pre UEFA ad. Taliansko - Slovensko
                     
                                                         
                       O Bratislave a o "tiežbratislavčanoch"
                     
                                                         
                       Prihlásenie vozidla na dopravnom inšpektoráte? Katastrofa
                     
                                                         
                       Rado, čo si to vyviedol?
                     
                                                         
                       Akčná opatrovateľka, alebo Batman versus Spiderman
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Ako sa Bobo rozhodol byť s mačkami
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       V Slovenskej sporiteľni alebo v Kocúrkove?
                     
                                                         
                       Medzitým na ostrove
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




