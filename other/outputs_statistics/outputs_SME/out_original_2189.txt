
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Steven Nagy
                                        &gt;
                Informačné
                     
                 Slovenské predavačky - úprimný nezáujem 

        
            
                                    3.2.2010
            o
            5:05
                        (upravené
                28.3.2011
                o
                1:58)
                        |
            Karma článku:
                15.63
            |
            Prečítané 
            4367-krát
                    
         
     
         
             

                 
                    Diskusie na SME sú veľmi príťžlivé, návykové a často poučné.  Pred pár dňami som našiel jeden príspevok od kolegu diskutéra s nickom "zrnko" .    Tak sa mi to zapáčilo, lebo  som presne ako tá osoba v jeho komente zažil nevraživosť, neochotu a úprimný nezáujem predavačiek  na Slovensku.  Milý "zrnko" , dúfam, že nemáš “copyright”  na svoj koment :-)    Tvoje riadky sa ma tak dotkli (súhlasne a na správnom mieste), že som ich po úprave akcentu preposlal emailom mojim početným slovenským a českým kontaktom.  Koment “zrnka”:
                 

                       Istý Japonec raz do SME napísal članok, že nerozumie, prečo sú predavačky na Slovensku take zamračené.   U nich v Japonsku majitelia obchodov od predavačiek vyžadujú, aby sa na každého zakáznika usmievali.  Dodal, že Slováci mu, vysvetľovali, že tie japonské predavačky sa neusmievajú úprimne.   Japonec im na to odvetil, že jemu osobne je príjemnejší neuprimný úsmev, ako úprimny nezáujem.           A ozvena na ten môj mail bol skoro okamžitý a vybral som z nich jednu, od  priateľa, ktorý to mohol porovnať, nakoľko strávili tu u mňa v Austrálii so svojim bratom istý čas a dobre hovorí anglicky:    Email priateľa:    :o))  Svätá pravda! Netreba ani do Japonska chodiť, aj v Austrálii je to brutalny rozdiel, predavači sú milí a zhovorčiví.  Keď som sa raz zakecal na pokladni a za mnou bola fronta, tak po chvílke som mal sám z toho zlý pocit, že to tam celé zdržujem, no ľudia za mnou boli v pohode, tí čo boli hneď za mnou sa dokonca pridali do debaty a tí opodial trpezlivo vyčkávali bez akejkoľvek nervozity.    Keď sme prišli na Slovensko (hneď ako sme išli zo Schwechatu), tak nám mama volala aby sme v BA kúpili prach na pranie, že je nejaká akcia. Tak sme šli do Carrefouru a keď sme platili, tak pokladníčka dala za nas ceduľku že pokladňa je uz zavretá. V tom prišla pani asi 50-55 ročná a pýtala sa či môže zobrať ešte aj ju. Pokladníčka sa ospravedlnila, že žiaľ nie, lebo už je tam po pracovnej dobe a že zmešká autobus. Nato sa tá pani začala strašne rozčuľovať a kričať, že čo sú to za móresy, že ona nebude čakať polhodinu na inej pokladni… My sme sa s bratom len na seba pozreli a súhlasne potvrdili, že toto sa v Austrálii stať nemôze   ...žial je to fakt a len tak sa to nezmení…           A úplne náhodou v tom istom  Carrefoure (na Tehelnom poli?)  som aj ja mal incident:  Mal som  v nákupnom košíku jedinú vec a postavil som sa do express fronty, (žiadna nebola, iba jeden zákazník predomnou) kde bolo dovolené mať  pár vecí. Bolo poobede, celkove málo zákazníkov.  Predavačka bola v pohode, sa dokonca zabávala s “pistolierom” starším uniformovaným  pánom bezpečnosti  opretým o pult nahnutým k nej a dôverne sa bavili. Za mnou prišiel muž, mal košík s pár vecami a za ním široko ďaleko nikto. Ako som už platil, predavačka sa mu mrkla na košík a dosť  surovo prehlásila: “Tak dúfam, že tam nemáte viac ako päť vecí! ”. A to sa ma dosť dotklo, lebo som zvyknutý na  niečo iné...lebo viem, že by v Austrálii si toho ani nevšimli a keď predavačka  má čas, je ústretová a bez slova vybaví aj zákazníka s viac ako tými pár extra  vecami.  Ozval som sa na obranu toho chlapíka, že čo aj keby mal pár extra  vecí, aj tak nikto za ním nestojí....  Bože môj! To ste mali vidieť tú reakciu!  Tá predavačka sa do mňa pustila ako fúria aj s podporou toho “pištolníka” . Dostal som  “hubovú”  a  som odišiel dosť zahanbene s škaredým pohľadom tých dvoch - tej  fúrie a pistoliera  s veľmi zlým pocitom, že by som mal ísť za manažmentom a zahlásiť tento incident, ale som si to rozmyslel, lebo možno by som tam  bol dostal  naložené ešte viac. Tak som mlčky s pokazenou náladou odišiel s  prísahou, že už nikdy do Carrefouru nevstúpim.            A na záver  ešte môj doslov.     Tento môj príspevok neberte ako urážku. V žiadnom prípade nechcem nadnášať a vyťahovať  tú ktorú krajinu a ich  prístup k ľuďom alebo vyťahovať  negatíva. Carrefour, Hypermarket a iné obchody dosahujú výberom a kvalitou to, na čo som zvyknutý tu...horšie je to s personalom.  Rád by som skôr navrhol manažmentom podobných podnikov školiť zamestnancov, a možno zabudovať  do odmien nejaké to percento z počtu obslúžených zákazníkov, alebo predaného tovaru,  aby tie predavačky miesto nevraživosti skôr vítali extra zákazníkov....Neviem či je to praktické a neviem, či to už tak nie je.  Ale ako častý návštevník Slovenska  dobre viem a poznám pomery na oboch stranách oceánu.  A tí ktorí hodne cestujú, verím že moje  postrehy a tie toho  môjho návštevníka  povyše potvrdia.   Moje riadky píšem hlavne pre tých ktorí sú menej scestovaní a asi neuvedomujú akú škodu, zdanlivo nepatrnú  robia  krajine  ktorú mám rád.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Tajomný Milionár
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Skutočná Austrálska "Love Story"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Hanbite sa pán M.!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Lietajúce sosáky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Klokan na cestách Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Steven Nagy
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Steven Nagy
            
         
        stevennagy.blog.sme.sk (rss)
         
                        VIP
                             
     
        Don't cry for me Argentina.. I'm on my way to see you soon.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    115
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poznávacie
                        
                     
                                     
                        
                            Vtipné
                        
                     
                                     
                        
                            Smutno-vážne
                        
                     
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Informačné
                        
                     
                                     
                        
                            Existencionalistické
                        
                     
                                     
                        
                            Osobné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Fastball
                                     
                                                                             
                                            James Blunt
                                     
                                                                             
                                            Jason Mraz
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivka Vrablanska
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľudmila Schwandtnerová,
                                     
                                                                             
                                            Martin Marušic
                                     
                                                                             
                                            Jozef Klucho
                                     
                                                                             
                                            Jozef Javurek
                                     
                                                                             
                                            Boris Burger
                                     
                                                                             
                                            Ján Babarík
                                     
                                                                             
                                            Natália Bláhova
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Počítače
                                     
                                                                             
                                            Zo zahraničia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Deň na ceste okolo sveta na Novom Zélande
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Vodičov pokutujeme aj za drobnosti, ale ostatných nie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




