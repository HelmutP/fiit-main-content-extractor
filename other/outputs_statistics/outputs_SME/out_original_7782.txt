
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Bednár
                                        &gt;
                Príbehy zo života
                     
                 Joy, happiness 

        
            
                                    1.6.2010
            o
            16:09
                        |
            Karma článku:
                9.84
            |
            Prečítané 
            1250-krát
                    
         
     
         
             

                 
                    Jej mama bola sestrou mojej starej mamy. Má už 86 rokov. Je rodená Američanka. Pred pár dňami prišla opäť na Slovensko... Majo sa už niekoľko dní pred jej príchodom tešil na „tetu americkú“. „Tati, ak je ona teta americká, ja som Marek slovenský? A kde je Amerika? Za Šamorínom?“ pýtal sa ma celkom logicky takto viackrát, napriek správnemu použitiu výrazu „americká“ však trošku tápajúc, kde tá Amerika vlastne je.
                 

                Nechcem v tomto krátkom texte ale priamo o „tete“. Skôr o jej pohľade na Majka. Bol som naňho tiež zvedavý. Zatiaľ ho videla raz, v roku 2004, počas svojej poslednej cesty na Slovensko. Vtedy mal Majko 5 rokov, ledva rozprával, o autistických, ešte hlboko zakorenených rysoch ani nehovorím. Tetu sme hneď na druhý deň pobytu u nás pozvali na tanečnú súťaž, v rámci ktorej cez prestávku v rámci propagácie spoločenského tanca postihnutých detí s tými „normálnymi“, vystúpil aj Majko. Keďže to bolo už jeho štvrté vystúpenie, vystúpil už pomerne sebavedome a naplno si vychutnával pozornosť a následný potlesk  asi 200 prítomných divákov. Vrátane našej tety, ktorej nadšenie bolo veľké aj preto, lebo podľa jej slov tancovanie integrovaných párov, v ktorom je jeden tanečný partner zdravý a druhý buď telesne, alebo mentálne postihnutý, ešte v živote nevidela. Jej slová uznania na Majkovu adresu hriali pri srdiečku. A nielen preto, že sa jej páčil Marekov tanec. V synovom prípade je tanec len jedným z kamienkov, ktoré sa snažíme poskladať do čo najpevnejšej mozaiky jeho šťastného života... „Jožko, mne stačí vidieť tú radosť a šťastie na Marekovej tvári. Nielen po tanci sa jeho tvár smiala. Radosť a šťastie sú viditeľné na jeho tvári takmer stále. Ak by to necítil tak aj vnútri, len ťažko by ste ho prinútili ukazovať skoro neprestajne svoju radosť a šťastie vonkajšiemu svetu. Veď aký máme často my, kvázi normálni ľudia, problém prejaviť radosť, šťastie, dať najavo svoje city. Majko žiari takmer stále. Určite aj preto, lebo cíti, ako ho milujete, ako mu odovzdávate kopce lásky deň čo deň, ako ho neúnavne povzbudzujete. A čo je najdôležitejšie, žiari ako slniečko aj preto, lebo cíti, že ho beriete ako rovnocenného,“ vyslovila na jeden šup s uznaním, ktoré potešilo. Priznám sa, dobre nám s polovičkou tie jej slová padli. Majo si ich určite zaslúži...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bednár 
                                        
                                            Spoveď
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bednár 
                                        
                                            Rain Man a jeho voľný čas
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bednár 
                                        
                                            Dr.Jekyll, Mr.Hyde - to je náš Majo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bednár 
                                        
                                            Summer of 84
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Bednár
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Bednár
            
         
        bednar.blog.sme.sk (rss)
         
                        VIP
                             
     
         Milovník života, rodiny, dobrých ľudí. Stále na ceste - momentálne ešte raz aj hovorcom na MK SR... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    765
                
                
                    Celková karma
                    
                                                10.23
                    
                
                
                    Priemerná čítanosť
                    2120
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Dumky a úvahy
                        
                     
                                     
                        
                            Vážnejšie dumky a úvahy
                        
                     
                                     
                        
                            Autizmus
                        
                     
                                     
                        
                            Viera a ja
                        
                     
                                     
                        
                            Médiá a komunikácia
                        
                     
                                     
                        
                            Moje malé postrehy
                        
                     
                                     
                        
                            Spomienky na minulosť
                        
                     
                                     
                        
                            Príbehy zo života
                        
                     
                                     
                        
                            Ako si žijeme
                        
                     
                                     
                        
                            Na margo
                        
                     
                                     
                        
                            Bežecký tragéd
                        
                     
                                     
                        
                            Pohodička
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dan Ariely: Jak drahá je intuice
                                     
                                                                             
                                            Friedrich Nietzsche: Lidské, příliš lidské II.
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Václav Neckář
                                     
                                                                             
                                            The best of Tanita Tikaram
                                     
                                                                             
                                            Hapka a Horáček - všetko
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.medialne.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




