
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lettrich
                                        &gt;
                Politika
                     
                 Gašparko režisérom 

        
            
                                    14.6.2010
            o
            21:51
                        |
            Karma článku:
                21.29
            |
            Prečítané 
            8313-krát
                    
         
     
         
             

                 
                    Keď sa Ivana Gašparoviča pýtali, či pôjde osobne povzbudiť slovenských futbalistov na svetový šampionát odpovedal, že nemôže, pretože práve v tom čase sa bude na Slovensku odohrávať zápas, ktorého on bude arbitrom. Nuž stalo sa...
                 

                 Dnes "poveril" Róberta Fica zostavením vlády. Dal tým najavo niekoľko skutočností: 1. ukázal, že chce naťahovať čas - "poverenie" líderky najsilnejšej z ostatných strán nebude ochotný urobiť skôr ako vyprší čas Ficovi 2. ukázal, že pokiaľ je to v jeho záujme neváha ani prekročiť svoje právomoci. Kým prvý bod je len dôkazom toho, že Gašparovič je naozaj sťaby člen Smeru, druhý ukazuje stratu zábran.   Ústava SR totiž nepozná nič také ako právomoc prezidenta SR "poveriť zostavením vlády". Pozná však ustanovenie, že štátne orgány môžu konať len v rámci toho, čo im zákony výslovne dovoľujú. Niet pochýb, že prezident je štátnym orgánom par excellence.   Právomocou, ktorú Ústava prezidentovi dáva, je vymenovať predsedu vlády. Ako by teda mal konať prezident v čase rokovaní o novej vláde? Mal by čakať, kedy za ním prídu zástupcovia politických strán garantujúci parlamentnú väčšinu. Čakanie si môže skrátiť rozhovormi s predstaviteľmi politických strán, v prípade nedohody lídrov môže plniť úlohu akéhosi mediátora. A potom, samozrejme, vymenovať za predsedu vlády človeka, ktorý mu väčšinu v NR SR garantuje. Fico, a ani nikto iný, žiadne "poverenie" od prezidenta zostaviť vládu nepotrebuje. Rozhovory o zostavení vlády môže pokojne rozbehnúť aj predseda SNS Slota...   Divadielko, ktoré nám dnes prezident Ivan Gašparovič zahral nie je len reakciou urazeného chlapca, ktorému na pieskovisku zbúrali hrad, ale je aj veľmi nezrelou reakciou človeka, ktorý (aspoň do času) odmieta uznať realitu,a čo je vážnejšie odmieta rešpektovať názor voličov. Počína si presne akoby bol, s prepáčením, gašparko režisérom. Treba však povedať, že zas tak veľmi neprekvapil... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (175)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Cenzúrou za ochranu  - čoho vlastne? (príbeh denníka Sme)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Príliš falošná Solidarita
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Tak zakázal či nezakázal súd adopcie kresťanom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Prečo denník SME neuverejnil môj príspevok?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Oplatilo sa rozpučiť Orange a začať dýchať O2?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lettrich
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lettrich
            
         
        lettrich.blog.sme.sk (rss)
         
                                     
     
        Človek, kresťan a katolícky kňaz. Nie je mi ľahostajný svet okolo nás, ani hodnoty, ktoré vyznávame. 
  Od roku 2011 na blogu denníka SME nepublikujem. Budem rád, keď navštívite môj blog na tomto odkaze.



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3790
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Viera a náboženstvo
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Blog
                                     
                                                                             
                                            .týždeň
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pred druhým kolom voľby budem stáť za Róbertom Ficom
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Kto tu klame II? alebo Otvorená odpoveď pánovi Lučanovi
                     
                                                         
                       Bránime homosexuálne cítiacich
                     
                                                         
                       Keby tak Fico tušil
                     
                                                         
                       Reakcie LGBTI hnutia na ochranu manželstva v Ústave sú slabé
                     
                                                         
                       O mojom detstve.  (pre premiéra)
                     
                                                         
                       Putovanie po východnom Slovensku 3.: Belianske Tatry (videoblog)
                     
                                                         
                       Diktatúra sexuálnych menšín
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




