
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nora-Soul Slížová
                                        &gt;
                Prúd myšlienok
                     
                 Okno do duše 

        
            
                                    8.5.2010
            o
            17:07
                        (upravené
                8.5.2010
                o
                21:05)
                        |
            Karma článku:
                5.50
            |
            Prečítané 
            973-krát
                    
         
     
         
             

                 
                    Hovorí sa, že oči sú oknom do duše. Občas pohľad do oči neznámeho človeka môže byť silným zážitkom. Vtedy keď nestihneme, či nechceme sklopiť zrak, zatiahnuť žalúzie svojej duše.
                 

                 Kráčam k neďalekému obchodíku, teším sa z čerstvého vzduchu, lebo posledných 36 hodín sedím nad knihami. Vonku je príjemne a myšlienky mi blúdia po záhrade, kosačke a hlavne mačičke, ktorá čoskoro bude mať malé.   Pár krokov predo mnou prechádza pani asi 45 až 50 ročná. Naše pohľady sa na okamih stretnú, pričom každá ide svojou cestou. V jej pohľade vidím stratenosť a zúfalstvo, alkohol. Nie je opitá a je upravená, napriek tomu, prvá myšlienka, ktorá mi prišla, keď sa naše pohľady stretli bol alkohol a zúfalstvo. Pohľad strateného človeka. Neviem prečo. Niekde v mojom vnútri vzniká nepríjemný pocit. Pocit, že ten človek na ktorého myslím má asi ťažké chvíle, že by asi potreboval pomoc.  Nepríjemný pocit odsúvam nabok, vkladám do košíka mlieko a prechádzam sa medzi regálmi. Pri pokladni zaplatím a ukladám si ešte zopár drobnosti do tašky.  Vedľa mňa pri pokladni stojí táto pani, ktorú som stretla pred pár minútami na ceste do obchodu. V ruke drží fľašu vína.    Pomaly kráčam domov, rozmýšľam nad tým, kde je tá hranica mojej predstavivosti a tým, čo sa skutočne deje. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Keď stojíme nad hrobom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Milujem oboch - a to rovnako
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Transformácia pesimistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Dining Guide - Kde sa najesť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Byť lepším človekom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nora-Soul Slížová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nora-Soul Slížová
            
         
        slizova.blog.sme.sk (rss)
         
                                     
     
         Človek, ktorý si chce nájsť čas pre uvedomenie si vlastných myšlienok, postojov, názorov. Pochopiť určitú vec, teóriu, či událosť, mi prináša osobný rast a vytúžený pocit štastia.   
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    409
                
                
                    Celková karma
                    
                                                4.81
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja Rodina
                        
                     
                                     
                        
                            Dining Guide
                        
                     
                                     
                        
                            Prúd myšlienok
                        
                     
                                     
                        
                            To čo ma potešilo
                        
                     
                                     
                        
                            To čo ma zarmútilo
                        
                     
                                     
                        
                            Vedeli ste, že...?
                        
                     
                                     
                        
                            o živote
                        
                     
                                     
                        
                            z vlaku
                        
                     
                                     
                        
                            Zvieracie
                        
                     
                                     
                        
                            readers diary
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Smutné ale pravdivé
                                     
                                                                             
                                            Suicide Read This First
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            C.G.Jung
                                     
                                                                             
                                            Fyzika I
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jesus Adrian Romero
                                     
                                                                             
                                            Fly To The Sky
                                     
                                                                             
                                            Rady svojich priatelov
                                     
                                                                             
                                            Hlas svojho srdca
                                     
                                                                             
                                            Svoje myšlienky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj prvý...(blog ktorý som čítala)
                                     
                                                                             
                                            Michal Patarák-Filozof v bielom plašti
                                     
                                                                             
                                            Kamilka- Úprimná duša
                                     
                                                                             
                                            Hirax
                                     
                                                                             
                                            DiDi- proste skvela
                                     
                                                                             
                                            Veronika Bahnová - Favorite Teacher
                                     
                                                                             
                                            Rolo Cagáň
                                     
                                                                             
                                            Jozef Klucho- Doktor bacsi
                                     
                                                                             
                                            Juraj Drobny-ODF- Velky drobec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Forum - psychológia
                                     
                                                                             
                                            I Psychologia
                                     
                                                                             
                                            Macher z KE :)
                                     
                                                                             
                                            Bennett Pologe, Ph.D.
                                     
                                                                             
                                            Kokopelli forum
                                     
                                                                             
                                            Umenie na iný spôsob- Akupunktura
                                     
                                                                             
                                            Ambulancia klinického psychológa
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Blog na SME bol výnimočný, už nie je
                     
                                                         
                       Nenapísaná poviedka
                     
                                                         
                       Čestný občan Róbert Bezák
                     
                                                         
                       Ako som Jožka odviezla na psychiatriu (smutný príbeh s úsmevom )
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Obyčajná láska
                     
                                                         
                       Rande na slepo
                     
                                                         
                       Kvapka krvi
                     
                                                         
                       Bolesť.
                     
                                                         
                       Taká obyčajná autonehoda
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




