

 
Romanticko-hudobná komédia
v réžií Bořivoja Zemana  rozpráva
príbeh princa a princeznej z dvoch susedných zemí (Helena Vondráčková
a Václav Neckář). Ich otcovia sa za pomoci svojich radcov  rozhodli, že svoje deti zosobášia
a spoja tak svoju moc. Mladým ľuďom sa ale táto myšlienka neveľmi páčila
a rozhodli sa sami nájsť svoju lásku a životných partnerov.
 
 
 
 
Filmové štúdio Barandov
adresovalo v roku 1967 vtedajšiemu riaditeľovi múzea profesorovi Hodálovi
žiadosť na povolenie natáčania tohto celovečerného filmu. Okrem iného sa
v nej píše, že režisér Zeman by rád niektoré zábery filmu natočil práve
v Bojniciach. Konkrétne v priestoroch pred hlavným vchodom, pri
rybníku s vežičkou a na nádvoriach. Predpokladaný rozsah prác bol
stanovený na 3 pracovné dni. 
 
 
 
 
Šialene smutná princezná sa okrem
bojnického zámku natáčala aj na juhočeskom zámku Blatná. Práve
v spojitosti s týmito dvoma zámkami si jeden z hlavných
hereckých predstaviteľov pri svojej návšteve Bojníc, počas ktorej odohral
v priestoroch zámku aj úžasný koncert, 
spomenul na zaujímavú strihovú scénu, v ktorej stál na vŕbe
v Bojniciach, spadol do rybníka neďaleko zámku Blatná a z vody
vyšiel opäť v Bojniciach. Všetko to bol práca šikovných filmových
strihačov.  
 
 
Vo výslednej fáze mala táto hudobná rozprávka 89 minút
a jej premiéra sa uskutočnila 7.6.1968. Vidieť ste v nej mohli Helenu
Vondráčkovú, Václava Neckářa, Františka Dibarboru, Stellu Zázvorkovú, Jaroslava
Marvana, Josefa Kemra či Bohuša Záhorského.
 
 
 
 
 
 
 
 
Šialene smutná princezná bola námetom aj pre vznik jedného z ročníkov Rozprávkového zámku pod názvom Šialene veselá princezná.
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
fotografie: Filmová batabáze www.fdb.cz
 
 
 
 
 

