
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubor Perháč
                                        &gt;
                Nezaradené
                     
                 Jedna daň a jej niektoré kvality 

        
            
                                    9.5.2010
            o
            11:27
                        |
            Karma článku:
                3.33
            |
            Prečítané 
            909-krát
                    
         
     
         
             

                 
                    Už slovo jedna navodzuje náladu jednoty a preto prvotne by sme mali pristúpiť k otázke jednej dane ako zjednoteniu všetkých daní do jednej – novej dane. Keď si predstavíme, že dnešný daňový systém má 31 druhov daní, odvodov a poplatkov a že ide o nejaký systém, ktorý sa postupne vytvoril ako klenba stavby – teda niečo, čo nesie resp. je samonosný – časovo od druhej polovice 19. storočia, tak vybrať jednu daň z tých dnešných 31 daní, je ako harakiri alebo samovražda pre ekonomiku, vybratie jednej tehly zo stavby klenby.
                 

                 Prejdime od tohto filozofického pohľadu, ktorý celý problém robí jasným a jednoznačným - zjednotenie všetkých daní do jednej - pozrime sa na to aj národohospodársky. Aké kvality potrebujeme dosiahnuť, aby daň nielenže nepôsobila negatívne, ale aby oživovala celú ekonomiku?   Hlavnou úlohou národného hospodárstva je uspokojovať potreby ľudí cez ich schopnosti a teda je nanajvýš dôležité tieto potreby - dopyt na jednej strane a schopnosti - ponuku na druhej strane - poznať. Z predchádzajúceho vyplýva, že tieto „kapitalistické heslá" (dopyt - ponuka) aj socialistické (potreby a schopnosti) sú zjavne tým istým, takže obe heslá presahujú obe zriadenia ako niečo nadčasové a objektívne a je našou najvyššou snahou ich hľadať ako niečo morálne. Ak totiž nenájdeme schopnosti človeka, ktorého na dané pracovné miesto potrebujeme, tak potom ani nemôžeme vytvoriť dostatočne kvalitnú službu alebo produkt.   Ale nájsť, odhaliť skutočnú schopnosť ľudí pre zodpovedajúcu prácu a skutočnú potrebu ľudí znamená pre spotrebu, že medzi kupujúcim a predávajúcim nebudú žiadne prekážky, ktoré nám to znemožnia rozpoznať. A keďže sme sa zamerali na daňovú otázku, vezmime si hlavné dane, ktoré prinášajú najväčšie príjmy do štátneho rozpočtu a tými sú: odvody - ktoré tvoria cca ½ štátneho rozpočtu, DPH - 1/3, DP - 1/6, spotrebné dane - 1/10.   Odvody sa vzťahujú len na príjmy do 1800 EUR a celoplošne na prímy zo zamestnania,  z podnikania aj z prenájmu nehnuteľností. Nie na peniaze, kapitál, majetkové výnosy, podiely a práva. A teda zjavne zvyšujú cenu práce. Ľudia sa predsa nemôžu najímať ako pracovná sila. Technicky sa vyberajú ako poistenie, ale zjavne tu človek nie je svojprávnym subjektom ako v komerčnom poistení. Človek neplatí za službu tomu, kto mu ju poskytuje. Napr. zdravotné poistenie   DPH: Ani tu človek nie je svojprávnym subjektom, lebo keď si kupuje tovar, tak platí za tovar a nie za DPH, aj keď ju platí započítanú v cene. Navyše sa DPH v časti ako 19% z rozdielu ceny na vstupe a na výstupe správa ako DP - z jeho obchodnej marže. A ešte k tomu je nielenže odoberaná v procese tvorby ale podniky si na ňu musia brať aj úvery, aby prečkali obdobie do jej zaplatenia odberateľom alebo DÚ. Nevzťahuje sa na kapitál, peniaze a nehnuteľnosti. Nie je spotrebná - ako sa hovorí - ale produktová .   DP: Vzťahuje sa na dosiahnutý zisk, čiže nezaťažuje tvorbu, ale až dôchodok a teda spotrebu. Tu je človek čiastočne svojprávny, ale štát zjavne vyberá daň protiprávne ako trestný čin vydierania. Štát sa o to, čo zdaňuje, nepričinil a pod hrozbou prísnych trestov si ju vynucuje (čo platí aj o ostatných daniach, ale tu je to najokatejšie). DP zaťažuje peniaze aj kapitál, ale len zo  zisku. Je najťažšie dosiahnuteľná, pretože daňová optimalizácia môže byť maximálna. A tiež by sme ju mohli nazvať dnešnou formou inkvizície, lebo neuznať náklady a započítať výnosy je pre DÚ najľahšie.   SD sú dodatočnou DPH.   Kým DP skresľuje stranu spotreby, DPH a SD skresľujú tvorbu, výrobu, čiže ponuku, schopnosti. Odvody oboje. Cez túto štruktúru daní sa nemôže stretnúť dopyt a ponuka pri skutočnej cene a to je hlavný problém dneška. Tieto dane tak zahmlievajú skutočnú tvorbu ceny, že príslušné tovary a služby sa buď majú problém dostať na trh z dôvodu vysokej ceny - nízky dopyt, alebo majú problém, že musia na trhu skončiť z dôvodu nízkej ceny výrobcu a vysokej dane, čím sa ich výrobcovi už neoplatí vyrábať aj keby dopyt bol. Vyrába sa len kvantitatívne - zisk je hnacím činiteľom, nie skutočné potreby ľudí.   Hlavným zámerom by bolo, aby medzi kupujúcim a predávajúcim nebolo žiadne skreslenie spôsobené daňami, čiže teoreticky nulové dane a s tým súvisiaca nulová administratíva.   Druhou najvážnejšou vecou je, že za posledných 150 rokov sa tak radikálne zmenila štruktúra národného hospodárstva(NH), že zdaňujeme už iba to, čo síce tvorilo kedysi 100% hodnoty NH, ale dnes to je cca 2-25% hodnoty NH. Poľnohospodárstvo (dnes 1-5% hodnoty NH) a priemysel (5-25%) sa postupne od II. sv. vojny rozšírili o služby (20-30%) a posledných 25 rokov špeciálne o bankové služby a peňažníctvo (30-70%). Ekonomické teórie nemajú tento peňažný priemysel vôbec vo výkonoch a stále s ním počítajú ako s niečím samostatným, odtrhnutým od NH, čo sa očividne javí ako problém. Objem medzibankového, finančného a kapitálového trhu bol v r. 2008 2700 mld. EUR , objem hrubého domáceho produktu HDP (ako ho udáva NBS) bol za rok 2008 67 mld.. Problém je, že bankovníctvo nemáme vo výkonoch NH, ktoré sú zdaňované presne tak, ako kedysi boli zdaňované iné výkony: daň z pozemku (zavedená  v r. 1870) - poľnohospodárstvo, daň z príjmu  (1900) - priemysel, DPH (1970) - služby.   Súčasná snaha politikov troch najväčších krajín EU - Merkelová, Brown, Sarkozi i Svetovej banky o „Robin Hood Tax" - daň z medzinárodných transakcií ukazuje uvedomenie si tejto trhliny. Ale pokiaľ to bude len ďalšia, iná, 32. daň v poradí, tak nám to neodhalí skutočné potreby NH, bude viesť k prehĺbeniu krízy a môže to vyústiť do mocenského boja politikov proti korporáciám.    Skutočnosť je taká, že  v NH sa používajú, spotrebúvajú výkony peňažného sektora, ale do hrubého domáceho produktu sa nezapočítavajú, a to znamená,  že nepodliehajú zdaňovaniu. Takže sa zjavne tento sektor od NH odtrhol a „zdivočel" a treba ho „umravniť". „Umravniť" tým, že sa zdaní každý pohyb peňazí a kapitálu, ako objektívnej zložky vyjadrujúcej všetky výkony NH. Keďže v klasickej ekonomike (zahrňujúcej poľnohospodárstvo, priemysel a služby) obrátkovosť peňazí klesá, v bankovníctve zjavne objemy rastú. Tento rast môžeme vyjadriť ako túžbu ľudí po hodnotách, veciach nemateriálnej povahy, po svete ideálov a mnoho iných pojmov by sme mohli použiť.   Kapitál ľudí sa ale napriek tomu preskupil nezdravým spôsobom: 90% bohatstva je v rukách 10% ľudí a sociálna priepasť sa stále viac zväčšuje.   Jediná daň ako zjednotenie všetkých ostatných daní musí byť tak nízka, aby neskresľovala hospodárske výkony; okamžitá - on line, aby neskresľovala časové rozpätie použitia výkonu; celoplošná, aby neskresľovala priestorové použitie výkonu; technicky jednoduchá bez administratívy a ďalších pracovných výkonov potrebných na jej výber a hlavne aplikovateľná iba na jednom mieste, čo sa javí ako bezvýznamné, ale keď si pomyslíme na 50-100%-nú DP, alebo 50-100%-nú DPH ako jedinú daň - keby sme ju zaviedli iba v jednej krajine, tak okamžite dochádza ku daňovo-kurzovej turistike a odlevu kapitálu do okolitých krajín. Ak by sme mali ísť cestou DP alebo DPH, je to nemožné urobiť bez celoplošného zavedenia na veľkom území (súčasne v mnohých krajinách).   Daň 0,5% (návrh pre SR) z každého prevodu, pohybu peňazí,  vyberaná bankami pri bezhotovostnom styku. To je tá daň, ktorá je schopná čeliť inflácii, svojvôli politikov i úžere bankárov, nenásytnosti byrokratov, nenávisti ľavičiarov, namyslenosti a arogantnosti pravičiarov , pohŕdaniu ekonómov a nezáujmu laikov.   Častou námietkou bude zrejme to, že peniaze musia prúdiť a touto daňou by sa spomalili finančné toky. Ale to je len ekonomická teória. Aké by toky mali byť? Koľkokrát by sa mali peniaze otočiť? 7 krát alebo 30 krát? Koľko? Národohospodárstvo je aj praktická veda. Výška tejto dane bude odpovedať skutkovému stavu národného hospodárstva. Lebo ak sú finančné a kapitálové trhy veľké, potom daň môže byť malá. Čiže čím sú väčšie finančné toky, pri konštantnom výbere dane, môže byť daň menšia. Tovary a služby sú stále viac zabezpečované a riadené ľuďmi, nie štátom alebo korporáciami. Ľudia môžu takto stále viac riadiť spoločnosť priamo, tým čo si kúpia, tým kde pošlú peniaze. V náznaku začíname tvoriť vedomostnú spoločnosť, kde jednotlivec pracuje vedome v prospech celku. Kde sú finančné toky malé, bude musieť byť daň väčšia, lebo výber daní je menší. Tam bude snaha štátu aj bánk regulovať tento  proces.    Avšak tento pomer, výšku dane, štát a banky nebudú môcť svojvoľne zmeniť, lebo svojvoľné zvýšenie dane, ktoré by nezodpovedalo skutkovému stavu hospodárstva, by viedlo ku kolapsu finančného systému.   Kto chce moc štátu a politikov, preferuje DP. Kto chce moc korporácií a veľkokapitálu, preferuje DPH. Obe skupiny už ale pôsobia na vývoj spoločnosti retardačne. Jediným tvorcom je človek a na neho musíme orientovať svoju pozornosť. A to nie len prázdnymi heslami, ako sa to deje v súčasnej predvolebnej kampani. Táto daň odníma moc politikom i korporáciám i peniazom a dáva ju človeku.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubor Perháč 
                                        
                                            Peniaze-nástroj priamej demokracie alebo Slovensko ako(daňový) raj.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubor Perháč 
                                        
                                            Jedna Daň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubor Perháč 
                                        
                                            Človek a Spravodlivosť (alebo východisko z krízy)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubor Perháč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubor Perháč
            
         
        luborperhac.blog.sme.sk (rss)
         
                                     
     
        Snažím sa myslieť o veciach spoločných. Hlavne o peniazoch a daniach, ktoré nás denne nútia robiť veci, ktoré nechceme. Známi mi hovoria, keď im niečo čo len navrhujem: nerob na mňa nátlak. O peniazoch a daniach tak nehovoria. Nemáme vedomie spoločenského časopriestoru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1098
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




