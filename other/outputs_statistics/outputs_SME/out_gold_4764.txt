

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 Obec Hamuliakovo stojí za návštevu kvôli svojmu románskemu kostolu sv. Kríža z 13. stor. Je tehlový, čo vraj vyplývalo z neprístupnosti iných materiálov, hlavne stavebného kameňa. Vo vnútri sú mnohostoročné nástenné maľby, ktoré som pri dnešnej návšteve nevidel, lebo kostol bol zavretý, ale z predchádzajúcich výletov si ich pamätám. Sú tam určite. V dedine sú aj nejaké kúrie a sochy, kto má hlbší záujem, informácie sú na internete, tu ich radšej vynechám.  
  
 Jazda na priehradnej hrádzi, či už na bicykli alebo kolieskových korčuliach, je zážitkom sám o sebe a množstvo ľudí sa v ňom vyžíva. Mne bolo ľúto hlavne dreva, ktoré sa povaľovalo na brehoch priehrady. Možno pochádza až z Čierneho lesa v Nemecku, odkiaľ si k nám našlo cestu po Dunaji, je teda vzácne, pritom nevyužité a slúži ledaže na dotvorenie krajiny. „Keby sa nejakým zázrakom popílilo a prenieslo do mojej záhrady. To by som mal kurivo zabezpečené na ... ani neskúšam hádať dokedy,“ tak som si nejako povzdychol a spomenul na hubárske výlety z detských liet, keď som si podobne predstavoval, ako mi všetky huby lesa naskáču do košíka. Zázrak sa neudial, žiadne drevo od priehrady som po návrate domov v záhrade nenašiel. 
  
 Čilistov má 1343 odkazov na internete. To nie je málo. Pre (cyklo)turistu sú vzácne jeho zákutia a riečna loď s možnosťou občerstvenia. 
  
  
  
  
 Nedá sa nič robiť, som zaťažený na historické pamiatky a tak som sa poponáhľal do Šamorína. Samotný názov mesta má zaujímavú históriu. Vraj v dobách,  keď sme ešte mali latinu ako úradný jazyk, sa mesto volalo Sancta Maria, z čoho vraj všelijakými prešmyčkami vznikol dnešný názov Šamorín. Z budov spomeniem stavbu najvzácnejšiu, reformovaný kostol, ktorý tiež patrí do skupiny románskych kostolov Podunajska, len tento má odkryté rozsiahle nástenné maľby a tie z kostola urobili skutočný skvost. Fotky neprikladám, lebo dnes som sa dovnútra nedostal. Choďte a presvedčite sa, či preháňam, keď hovorím o skvoste.  
  
 Ešte skok do šamorínskeho cintorína, kde majú pomníky ruskí a talianski vojaci z čias prvej svetovej vojny. Vraj pri Šamoríne stál zajatecký tábor a pochovaní vojaci sú hlavne obete chorôb.  
  
  
  
  
  
 Na ceste domov som sa zastavil v Kvetoslavove kvôli fotke pomníka padlým z 1. a 2. svetovej vojny. Sú tam lavičky, na ktorých sa veľmi dobre sedí a číta. To asi kvôli tým mohutným dubom, ktoré šíria okolo seba pocit istoty. 
  
  
 To je všetko o mojom sobotňajšom výlete. Bolo pekne, však fotky to azda potvrdzujú. Len informácie, by mali byť obšírnejšie. Vraj všetko je na internete a tak si azda každý nájde to, čo ho zaujíma. Kto však chce zažiť tú správnu atmosféru a nefalšované to, čomu sa hovorí genius loci, musí sa tam vybrať osobne. To na internete nájsť nemožno. 
  
  

