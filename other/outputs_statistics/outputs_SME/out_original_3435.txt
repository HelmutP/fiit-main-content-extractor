
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Súkromné
                     
                 Dnes otáčaš novú stránku ... 

        
            
                                    27.3.2010
            o
            15:21
                        |
            Karma článku:
                9.80
            |
            Prečítané 
            1026-krát
                    
         
     
         
             

                 
                    Tak ako pred búrkou pribúdajú obláčiky na oblohe, tak pribúdajú stránky aj v našej Života knihe. Dnes otáča novú stránku aj môj manžel a ja netradične nič špeciálne nechystám, nepečiem... Operácia ma obrala o sily, ich návrat je pomalý, akoby sa niesol na chrbte slimáka. Dokonca dnes pripravoval obed oslávenec. Tí, čo moje články poznajú iste tušia, že u nás dnes rozvoniavala škvariaca sa slaninka, ktorá urobila z halušiek s bryndzou ten správny gurmánsky zážitok, mňam.
                 

                 
  
   Od rána sa musím trápne čudovať, ako čas rýchlo letí, ako mi tie  dve narodeninové  päťky mútia rozum. Kde sa vzali? Kedy sa im podarilo vkĺznuť manželovi pod kožu? Ako to, že žijem po jeho boku a nevšimla som si to?   Spoznali sme sa, keď mal dvadsaťsedem. Vtedy sa nám ľudia po päťdesiatke už zdali starcami, cha. A teraz? Mne samej už jubileum brnká po nose a sprisahanecky sa teší ako ma o rok prepadne, samozrejme - nepripravenú.     Manžel môj,   každý z nás prežíva niekoľko životov. Nie, nemám na mysli reinkarnáciu. Hovorím o kapitolách v knihe nášho Života, ktorými každý prechádza - detstvo, dospievanie, mladosť, vzdelávanie, manželstvo, zamestnanie... Počas týchto období prežívame radosť aj bolesť. Čo je podstatné, vytvárame si isté modely prežívania, postoje, priority, názory...   Tvoje narodeniny sú vhodnou príležitosťou stať sa na chvíľu detektívom a pátrať v labyrinte doterajšieho života.   Každá životná skúsenosť nám pomáha selektovať isté hodnoty, na ktorých staviame svoj život. Sú to ľudia, sú to veci, ktoré sme milovali a milujeme, sny, ktoré sme sa pokúšali naplniť, chvíle radosti, ktoré sme si pestovali, o ktoré sme bojovali a bojujeme. Sú to straty, s ktorými sme sa museli vyrovnať...   Práve tieto čriepky nášho života nám poskytujú kľúč k prežívaniu, pomáhajú nám odhaliť, čo prispieva k naplneniu nášho života. Veď práve minulosť nám pripomína všetky prekážky, ktoré sme museli prekonať, silu, ktorú sme získali v tomto boji a múdrosť, ktorú sme načerpali zo svojich skúseností. Nič sa nedeje náhodne, samoúčelne. Jestvuje priame spojenie medzi tým, čím sme boli kedysi a čím sme teraz. Schopnosť vnímať svoju minulosť ako súčasť osobnosti a chápať následky svojho rozhodovania, sú prvým krokom k vytvoreniu toho najlepšieho života, ktorý sa aj  pred Tebou otvára...   Slnko mojich očí,   želám Ti, aby si bol s pátraním vo svojom doterajšom živote spokojný. Prajem Ti, aby ďaľšie stránky Tvojej knihy Života boli prežiarené láskou, aby prekvitali zdravím,  iskrili radosťou, splnenými túžbami, prekypovali elánom a pozitívnou mysľou. Prajem Ti veľa dôvodov na úsmev, prajem Ti slniečko v duši, nech Tvoju pohodu nič neruší... To, čo Ťa na predchádzajúcich stránkach bolelo, zraňovalo, trápílo  uzavri v starých kapitolách a novú stránku popíš  iba písmenkami radosti.    Ďakujem Ti, že si...       PS: Strávila som istý čas v nemocnici. Aby ma manžel potešil, tak v čase mojej hospitalizácie vymaľoval moje kráľovstvo - kuchyňu. Nakoľko kuchyňa je samá polička, korenička, fľaštička, mažiarik, košíček, ozdôbka, tak si najprv kuchyňu pred vypratávaním pofotil, aby ju neskôr podľa fotiek mohol dať do pôvodného stavu. Bolo to príjemné prekvapko a nebolo v ten moment na svete brzdy, ktorá by zastavila slzy dojatia, keď som sa ocitla na jej prahu. Aj toto je okamih, pre ktorý Ťa milujem.   Všetko najlepšie!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




