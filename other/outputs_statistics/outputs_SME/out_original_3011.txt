
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magda Kotulová
                                        &gt;
                Nezaradené
                     
                 Tvár z ulice 

        
            
                                    21.3.2010
            o
            12:50
                        (upravené
                21.3.2010
                o
                10:53)
                        |
            Karma článku:
                8.93
            |
            Prečítané 
            742-krát
                    
         
     
         
             

                 
                    Keď deň pred marcovou sviečkovou demonštráciou cez obed okolo ich sídliska, s trúbením a rachotom prechádzali policajné autá, transportéry a vodné delo, prenikal ňou neurčitý strach. Čo toto má byť? Demonštrácia sily? Naháňanie hrôzy, aby odstrašili tých, čo by TAM išli?... A večer, keď sa v byte rozkmitala, roziskrila obrazovka televízora, a presvedčovacím tónom duchovný Záreczký „prosil“občanov, aby TAM nešli, bolo jej opäť všelijako. Však sa TAM celá rodina chystala. Deti, muž i ona. Ako to dopadne? Odhodlá sa a pustí ich? Nepôjde len sama?... Hlavou jej prebleskovali všakovaké myšlienky, ktoré jej radili raz to, raz ono. Nie, nesmie byť zbabelá, musí im povedať, že môžu ísť, že ich pustí. Nemôže im ukázať strach, obavu o nich, musí im vliať odvahu i keď... Nachystala v TEN DEŇ banány a cukríky. Každému z nich. Vopchala im ich do vreciek. Aby mali aspoň niečo pod zub, keby ich vzali, zaistili...
                 

                 Traja išli spolu. Najmladšia s ňou. Muž sám. Každý zvieral vo vrecku sviečku, zápalky.   Kráčala rozochvená s dcérkou za ruku. Električka. Chôdza od zástavky smerom k Hviezdoslavovmu námestiu. Plná obáv o muža, deti. O seba nie. Čo už ona môže stratiť? Ale deti? Muž?... Des ju zadúšal, ale odháňala ho. Tuho objímala zvlhnutou rukou pršteky svojej dcérky. V tom sa k nim pridal. Cítila ho dlhšie v pätách. Ulízaný, pretučnený, sviatočne nahodený, v bielej košeli ako na hodokvas.   „Kam idete? Na Hviezdoslavovo...?“ pridal sa k nim. Vyzvedal, s prešibaným úsmevom.   „Do toho vás nič... ale, áno, ideme tam!“ odvrkla a zrýchlila krok. Dcéra za ňou sotva stačila.   „Čo tam idete?... Viete, že je to nebezpečné...?“ nenechal sa odradiť chlap. Snažil sa žoviálne nadväzovať ďalší rozhovor, kontakt.   „Nech...!“ vyhŕkla, a dali sa s dcérou skoro do behu, aby sa od úlisníka odpútali. Cítila ho však za nimi aj naďalej.   Prišli k Národnému divadlu. Kordóny eštebákov v civile i v uniformách, množstvá áut milície, taxíkov. V blízkosti sa ozýval brechot psov. A spústa ľudí po uliciach, v patričnej úctivej diaľke od stredu, kde pred divadlom stála zhrčená do seba hŕstka tých, čo chceli čeliť násiliu, obkolesená nerovnými partnermi. Došli s dcérou k prvej skupine esenbákov, eštebákov.   „Pustite nás, chceme ísť k divadlu...!“ rozhodne povedala.   „Tam nemôžete!“ vyhŕkol jeden.   „Prečo?... My tam chceme ísť...!“   „Nesmiete!“ už skoro zrúkol.   Ulízanec práve dorazil k partii, s ktorou sa hneď zbratal a začal sa rozprávať: „Servus, Mišo,“ pozdravil známeho. Začal sa s ním baviť. Vôbec si ich už nevšímal: ju a dcérku.   „Poď, Hanka!“ potiahla dcéru.   Rýchlo sa od chlapov, čo sa hurónsky rehotali, poberali preč. Našli medzeru medzi nimi a prepchali sa dopredu za kordón alkoholom páchnucich strážcov pokoja a poriadku. Bolo päť minút pred šiestou. Zastali medzi svojimi. Vybrali sviečku, zápalky.   A vtom to začalo: zavýjanie sirén, hluk, húkanie, trúbenie áut, brechot psov, pohyb áut do ľudí, údery pelendrekov do občanov, ktorí prišli hájiť práva svoje i tých, čo zbabelo trčali doma a čakali, čo bude a ako sa to vyvinie, alebo čo nemali záujem. Prúdy špinavej vody z polievača dopadli na jej kabát. Oťažel. Bola jej zima. Padal mrholivý drobný dážď a vlhkosť zaliezala všade. Prenikla k roztrasenému telu a rozochvenej duši.   „Drž sviečku, Hanka...! Chráň plamienok dlaňou...!   „Otče náš, ktorý...,“ ozývalo sa z pier slabých na námestí, ohlušenom rachotom bezcitných mocných.   Horko-ťažko dokončili modlitbu, spev. Potom ozbrojené prilby s pelendrekmi ich vytláčali z námestia, mlátili, tisli, oblievali vodou z polievačov.  Smradľavý alkoholový dych žoldnierov ich zarážal. V skrumáži tiel, ktoré sa posunovali ťažko dozadu schmatla dcérku, aby sa v dave nestratila. Sviečky nedohorené ležali na zemi. Stúpalo sa po nich. Ozbrojenci. Aj tí, čo pred chvíľou, takou úžasnou, ktorú  nezabúda do smrti ani rozum ani srdce, držali ich rozžiarené blikajúce v hlbokej tme...       O niekoľko rokov potom. Televízna politická relácia KROKY. KROKY. Koľko ráz ich sledovala?  Všetky! Všetky, čo boli vysielané v STV. Nenechala si ich nikdy ujsť. Chcela vidieť na vlastné oči, počuť na vlastné uši, tých v koalícii i tých v opozícii. Utvárala si názor na politikov, na ich činy, na všetko okolo seba. Raz sa zarazila pri pohľade na obrazovku. Hlavná téma diskusie bola o vnútre, o verejnom poriadku a o bezpečnosti občanov v štáte. Predstavitelia polície. Zbystrila uši, zrak.   Z obrazovky sa na ňu díval „ulízanec“ spred pár rokov. O voľačo starší. Načúvala jeho reči, jeho akoby vypľúvané, šušlavé slová, vety. Akoby ich pľul, ako vtedy, keď sa jej pýtal, či idú na Hviezdoslavovo. Prečo má takú dobrú pamäť? Prečo si pamätá  tváre ľudí, ľudí, ktorých v živote stretáva?   Túto tvár videla v roku l988 na ulici, keď sa diali udalosti, ktoré začali hýbať dejinami, v roku 1996 na obrazovke, akože demokratickej STV. A dnes – kde je tá tvár?...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            U kaderníčky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Oprstienkovaná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Daniel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Keď zakape mobil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Registrácia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magda Kotulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magda Kotulová
            
         
        magdakotulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som mama štyroch detí, stará mama pätnástich vnúčat a dvoch pravnúčat. Príležitostná publicistka. Občas sa "niečo" pokúsim napísať, keď ma čosi nahnevá alebo urobí spokojnou. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    318
                
                
                    Celková karma
                    
                                                10.84
                    
                
                
                    Priemerná čítanosť
                    1470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kódex blogera na blog.sme.sk (aktualizovaný 10. 7. 2012)
                     
                                                         
                       Ocko náš
                     
                                                         
                       Príjemné výročia si treba  pripomínať
                     
                                                         
                       Lezúň
                     
                                                         
                       Garancia vrátenia peňazí. To určite!
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Vôňa ženy
                     
                                                         
                       Vodopády Slunj
                     
                                                         
                       Mladý, nervózny vodič a starká
                     
                                                         
                       Nikdy, nikdy, nikdy sa nevzdávaj
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




