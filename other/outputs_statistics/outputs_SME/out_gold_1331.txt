

 Spôsob života: Kajmanky dravé trávia väčšinu času pod vodou, ležiac na dne plytkých sladkovodných jazier, rybníkov alebo pomaly tečúcich riek. Vo vode sa pohybujú pomaly, na zemi sú ťažkopadné a neobrátne. V chladnom podnebí severnej časti územia výskytu prezimujú pod vodou, avšak aj pod zamrznutou hladinou vody zostavajú často značne aktívne. Kajmanky dravé sú na zemi veľmi agresívne, hlavne v stave ohrozenia. V nebezpečí sa prudko vrhajú na útočníka, pričom ich hákovité čeľuste sa hrozivo roztiahnú. Majú veľkú hlavu s ostrými čeľusťami, silné nohy s mohutnými pazúrmi a plávacími blanami medzi prstami. 
 Rozmnožovanie: Kajmanka dravá sa parí spravidla vo vode, a to tak, že menšie samčeky vyliezajú na samičky. Samička si potom na okraji brehu vyhrabe jamku, do ktorej od začiatku leta nakladie až dvadcaťštyri vajec. Vývin trvá dva až tri mesiace. Z vajec, ktoré boli nakladené na konci leta, sa mláďatá liahnu najčastejšie až budúcu jar za teplého počasia. Sfarbenie mláďat je oveľa svetlejšie ako u dospelých jedincov. Ihneď po vyliahnutí sa mláďatá škriabu do vody, kde sú počas prvého roka života dobre ukryté. Rýchle rastú a ich pancier vo veku jedného roka obyčajne meria pätnásť centimetrov. 
 Potrava a lov: Kajmanka dravá loví každé zviera, na ktoré si vzhľadom na jeho veľkosť trúfne. Loví ryby, malé korytnačky, žaby, mloky, vodné hady, mláďatá aligátorov a lmáďatá vtákov, tak isto ako cicavce žijúce vo vode. Ak Kajmanka zbadá vo vode živú rybu, zostane nehybná, ale pozorne sleduje každý pohyb koristi. Väčšie zvieratá zachytáva svojími čeľusťami a prednými pazúrmi ich trhá na kúsky. Menšiu korisť prehĺta celú. Kajmanky dravé sú aj zdochlinožravce, zožerú zdochlinu akejkoľvek veľkosti, ktorú stiahnu pod vodu. Mláďatá Kajmanky dravej sa živia mladými rybami, žubrienkami, kôrovcami a vodným hmyzom. 
 - Kedysi boli Kajmanky používané na vyhľadávanie tiel obetí nehôd, vrážd alebo samovrážd v jazerách. Do vody ich púšťali priviazané na vlákno a keď Kajmanka zatiahla, bol to signál, že práve našla hľadané telo. 
 - Kajmanka supia má malý červovitý výrastok na jazyku, ktorým pravidelne pohybuje. Sedí s otvorenou papuľou na dne jazera a striehne na malé rybky, ktoré na tento červovitý výrastok naláka. Sotva sú dostatočne blízko, korytnačka zaklapne papuľu a prehĺtne ich. 
 Tento druh korytnačky je pre odchov a akvateráriu zaujímavý aj svojím spôsobom života. Korytnačky chované v akvateráriu s hĺbkou vody 30 centimetrov, v lete ich môžeme umiestniť v jazierku s hĺbkou približne 40 centimetrov. Jednotlivé korytnačky chováme oddelene, len v čase párenia umiestníme pár spoločne. 
 Výskyt- v plytkých sladkovodných jazerách, rybníkoch a riekach na východnej strane Severnej Ameriky, od južnej Kanady až po Strednú Ameriku a severozápad Južnej Ameriky. 
 Ochrana druhu- Hlavným nepriateľom Kajmanky dravej je človek: Zabíja ju, aby si mohol pripraviť korytnačiu polievku, alebo ju loví len tak zo športu. Napriek tomu sú stavy početnosti konštantné. 
 Telesné rozmery - pancier až 40 cm, celková dĺžka až 80 cm, samce sú o niečo menšie. 
 Rozmnožovanie a pohlavná dospelosť- samce v 3 - 5 rokoch, samice v 4 - 6 rokoch. 
 Obdobie kladenia vajec - koniec leta. 
 Počet vajec - asi 24. 
 Inkubačná doba - 2 - 3 mesiace. 
 Spôsob života - samotár. 
 Potrava - ryby, mláďatá vtákov a cicavcov, žaby, mloky, vodné hady, mladé korytnačky. 
 Dĺžka života - v zajatí dokonca až 60 rokov. 
 Príbuzné druhy - v Severnej Amerike žije Kajmanka supia Macroclemys temminckii. 
 Trieda - Plazy 
 Rad- Korytnačky 
 Čeľaď - Kajmankovité 
 Rod a Druh - Chelydra serpentina 

