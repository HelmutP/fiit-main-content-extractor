
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Čísla v Tomanovej letáku sú haluze 

        
            
                                    28.4.2008
            o
            23:18
                        |
            Karma článku:
                15.19
            |
            Prečítané 
            30731-krát
                    
         
     
         
             

                 
                    Ministerstvo čísla buď nevie správne vyrátať, alebo ich zámerne zmanipulovalo
                 

                Leták, ktorý Ministerstvo práce vydalo a v týchto dňoch roznáša do všetkých schránok na Slovensku, obsahuje nesprávne čísla. Pritom sa nebavíme o pár korunách, či percentách, ale o podstatných rozdieloch. Napríklad je v letáku v príklade 35 ročného Petra uvedené, že suma dôchodkov pri zotrvaní len v prvom pilieri je o 921 tisíc Sk vyššia, ako suma dôchodkov z prvého a II. piliera. Podľa korektných prepočtov je rozdiel 345 tisíc  Sk a to v prospech II. piliera. Stručný prehľad dáva graf:         Ako vidieť, v sume vyplatených dôchodkoch len z prvého piliera sa moje výpočty a výpočty ministerstva až na pár sto korún zhodujú (rozdiely vznikajú zaokrúhlením). Problém vzniká pri sume dôchodkov z prvého a II. piliera.     Zoberme si prvý príklad - 40 rokov. Výnos len z prvého piliera je v obidvoch prepočtoch 5,276 mil. Sk. V tomto príklade dotyčný robil 44 rokov, z toho 22 platil aj do II. pilieru. V prípade vstupu do II. piliera je jeho nárok na dôchodok z prvého piliera 75% a to preto, že polovicu odvodov platil celých 44 rokov do prvého piliera a druhú polovicu platil 22 rokov do prvého a 22 rokov do II. piliera, to znamená, že 75% všetkých jeho odvodov išlo do prvého piliera a preto dostane z prvého piliera 75% dôchodku, na ktorý by mal nárok, keby v II. pilieri nebol. V uvedenom príklade to je 3,957 mil. Sk (75% z 5,276 mil. Sk), to znamená, že ministerstvo predpokladá už len necelých 70 tisíc Sk (4,026 - 3,957), ktoré má II. pilier vyniesť. To pri 22 rokov sporenia v II. pilieri - no skratka haluze.     Všetky prepočty ako aj samotný graf sa nachádzajú v excelovskom súbore, ktorý je možné stiahnuť tu. Moje prepočty sú robené na mesačnej báze a vychádzajú z predpokladov, ktoré sú uvedené v letáku, konkrétne:     Vek poistenca dnes:                                  40, 35 a 25 rokov     Odchod do dôchodku:                                po dovŕšení 62 rokov     Počet rokov do dôchodku:                         22, 27 a 37 rokov     Hrubá mzda poistenca dnes:                     25 000 Sk     Nárast hrubej mzdy:                                 3% ročne     Výnosnosť v II. pilieri:                              5% ročne     Nárast hrubej mzdy a výnosnosť v II. pilieri považujem za nominálne hodnoty, to znamená vrátane inflácie. Pokiaľ by tieto údaje boli považované za reálne hodnoty, bolo by to ešte viac v prospech II. pilieru.     Ďalšie predpoklady, ktoré v letáku uvedené nie sú, ale pre presné výpočty potrebné sú:     Odvod do II. piliera:                               9% (znížený o 0,5%, ktoré si necháva poisťovňa)                 Inflácia:                                                  2,5%     Aktuálna dôchodková hodnota:             249,14 Sk pre rok 2008     Priemerná mzda:                                    20 146 pre rok 2007     Poplatky za vedenie II. piliera:               0,065% za správu fondu, 1% za vedenie účtu     Technický úrok životnej poistky:             3%     Poplatok životnej poisťovni:                   2%     Očakávaná dĺžka života:                        76,8 rokov     Počul som, že ministerstvo (ktoré svoje prepočty bohužiaľ  nezverejnilo), ráta s vekom dožitia až 17 rokov. Pre prípad Petra 35 rokov je prerátaná aj táto alternatíva a výsledok je 57 tis. v prospech II. piliera (namiesto 922 tis. v prospech I. piliera). Vtedy ale nesedí celkový výnos v prvom pilieri.     Okrem toho, že výpočty na letáku nie sú korektné, nie sú ani ostatné informácie príliš objektívne. Napríklad je na zadnej strane uvedený návod, ako z II. piliera vystúpiť, ale v celom letáku nie je jedna jediná zmienka, že do II. piliera sa dá aj vstúpiť. Taktiež o možnosti dedenia v II. pilieri ani slovo. Ani slovo ani k obrovským deficitom, ktoré prvý pilier produkuje už dnes, kvôli ktorým bude nutná reforma prvého piliera a potom bude ešte nevýhodnejšie v ňom zotrvať. O nereálnom príklade (v ktorom 35 ročný Peter už 9 rokov pracuje ako manažér za 25 tisíc mesačne), úbohej grafike (to je názor profesionálnych grafikov) a hodne vyretušovanej fotke pani ministerky (žeby klamala telom?) sa rozširovať nebudem a prejdem k tomu najpodstatnejšiemu:     Podľa parametrov, ktoré zvolilo ministerstvo je výhodné vstúpiť do II. piliera.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (150)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




