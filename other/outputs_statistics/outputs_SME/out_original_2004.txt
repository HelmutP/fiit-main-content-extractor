
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Lužinský
                                        &gt;
                Viera
                     
                 Boží a náš cieľ v živote 

        
            
                                    9.1.2010
            o
            23:00
                        (upravené
                9.1.2010
                o
                23:30)
                        |
            Karma článku:
                25.99
            |
            Prečítané 
            2933-krát
                    
         
     
         
             

                 
                    Citujúc slová apoštola Pavla (Rím.8:22-23), teológ J.W.C Wand, v knihe The New Testament Letters, zanamenal tento zaujímavý komentár:
                 

                 "Fyzický svet nebol podrobený márnosti z vlastnej vôle,   ale z vôle Stvoriteľa, ktorý ho podrobil na základe nádeje,  že tvorstvo bude oslobodené z otroctva hriechu a bude mať  slávnu slobodu Božích detí."   Zatiaľ čo Božie meno Jehova vyjadruje zmysel, cieľ v živote,  väčšina ľudí dnes žijúcich na Zemi nevidí žiadny zmysel vo svojej existencii. Pozorujú celé ľudstvo klopýtajúc od jednej krízy k ďalšej i ďalšej - vojny, prírodné katastrófy spôsobené sebectvom, epidémie a choroby, chudobu a zločin, atď. Ale aj tí, ktorí sú bohatí a žijú v luxuse majú vážne pochyby o budúcnosti a o svojom zmysle života. Prvý človek, Adam, na samom začiatku bol stvorený v prekrásnej záhrade a jeho prvá úloha od svojho Stvoriteľa bolo niečo, čo by zoológ nazval jednoducho to najväčšie potešenie - pomenovať všetky zvieratá, ktoré Jehova vytvoril. Toto je len jeden spôsob, akým by ľudstvo mohlo využívať svoje vlastné  osobné talenty! A historický záznam nás informuje len o jednom prvom príkaze pre Adama a Evu (1.Mojžiš. 2:17). Tento príkaz znamenal, že  ľudstvo malo rešpektovať a tiež poslúchať svojho Stvoriteľa. Adam a všetci ľudia museli, napríklad, uznávať gravitačný zákon, ináč by si vážne ublížili! Tak prečo by potom odmietli rozumne poslúchať Stvoriteľa, Otca, napriek jeho varovaniu? Cez Mojžiša Jehova Boh informuje ľudstvo:   "Ja beriem dnes proti vám za svedkov nebesia a Zem, že  som ti predložil život a smrť, požehnanie a zlorečenie;  a vyvolíš si život, aby si zostal nažive, ty a tvoje  potomstvo." - 5. Mojžišova 30:19.   Jehova Boh stvoril inteligentné bytosti na nebi aj na Zemi, aby sa spolu so svojim Stvoriteľom tešili zo života, rešpektujúc zákony života. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (111)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Vlády ako divé zvery
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Prenasledovanie za pravdu je príjemné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Pravá božska láska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Kto pozná Božie meno?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Smrťou sa končí všetko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Lužinský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Lužinský
            
         
        luzinsky.blog.sme.sk (rss)
         
                                     
     
         Autor sa bude snažiť budovať, povzbudzovať a utešovať ľudí (vrátane samého seba) svojou prózou i poéziou.    
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1303
                
                
                    Celková karma
                    
                                                1.18
                    
                
                
                    Priemerná čítanosť
                    481
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Teológia
                        
                     
                                     
                        
                            Historia
                        
                     
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Viera
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




