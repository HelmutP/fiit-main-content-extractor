
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Drotován
                                        &gt;
                Spoločnosť
                     
                 Výročie teroristického útoku v Londýne 

        
            
                                    7.7.2010
            o
            10:30
                        (upravené
                13.8.2010
                o
                12:54)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            960-krát
                    
         
     
         
             

                 
                    Dnes je to presne päť rokov od teroristického útoku na metro a autobus v Londýne. 07.07.2005 islamskí samovražební atentátnici vyhodili do vzduchu tri súpravy metra a jeden z nich, po neúspešnom ataku ďalšieho metra (explózie mali byť na všetky štyri svetové strany), explodoval s výbušninou v autobuse číslo 30. Pred piatimi rokmi som pracoval v samom centre Londýna na Fleet Street neďaleko St. Paul´s Cathedral, preto som žiaľ zažil tieto útoky takpovediac naživo. Tento spomienkový blog je poctou všetkým 52 nevinným obetiam štyroch teroristov.
                 

                 
 Foto:autor
   V daný deň, 07. júla 2005 som ráno išiel tradične na metro, modrú Victoria line na Walthamstow Central. Na konečnej zastávke metra sa už tlačilo veľké množstvo ľudí a boli nervózni, tak ako ja, pretože sa začala šíriť správa, že pre výpadok prúdu nejde žiadne metro. Autobusy do centra boli preplnené, preto musel byť človek veľmi asertívny, aby sa do nejakého z nich dostal. Na zastávke Highbury&amp;Islington nám šofér oznámil, že musia všetci z autobusu vystúpiť a tiež to, že do centra mesta nepôjde od tejto chvíle už žiaden autobus. Nikto vrátane mňa v tých autobusoch nevedel čo sa deje.   Vybral som sa teda z Highbury&amp;Islington pešo do centra mesta, na ulici som pred jednou prevádzkou zazrel pred televízorom zhluk ľudí. Pristavil som sa a videl som vybuchnutý autobus a dole bežiaci oznam, že polícia dôrazne žiada ľudí, aby v žiadnom prípade necestovali do centra mesta. Nasledovali zábery na horiace súpravy metra. Nakoľko kamarátka Danka v tom čase pracovala neďaleko Charing Cross, rozhodol som sa ísť do centra napriek výzve polície. Prišiel som do práce (pracoval som pre reštauráciu Temples Food, ktorú vlastnil jeden Ind), mali sme otvorené ešte pár hodín (čo bolo dosť zvláštne, keďže všetci zatvárali prevádzky, kamarátka však robila v Mc Donalde a keď som poobede pre ňu prišiel, tak tam bol stále business as usual a nikto o ničom neinformoval zamestnancov, bolo to už v čase, keď po celom meste chodili kolportéri s najnovším Evening Standardom). V centre verejná doprava nepremávala, preto bolo potrebné pešo prejsť na zastávku Angel, odkiaľ sa dalo dostať už autobusom.   Na základe vyšetrovania vyplynulo, že štyria teroristi nastúpili do rôznych liniek metra na rušnej  stanici King´s Cross St. Pancras, stanici ktorou sme všetci chodili skoro denne, jeden výbuch pred stanicou Altgate East bol tesne za stanicou Liverpool Street, kde som niekedy vystupoval, keď som robil v druhej prevádzke Temples Food. Hasib Hussein (18), Germaine Lindsay (19), Shehzad Tanweer (22) a Mohammad Sidique Khan (30) boli priamo zodpovední za teroristický útok pri ktorom zahynuli.   Londýn sa z tohoto útoku spamätával ešte veľmi dlho, o dva týždne nasledoval neúspešný pokus o ďalšie štyri výbuchy, v meste vtedy zavládla panika (v žiadnom meste západného sveta neboli v nedávnej dobe v tak krátkom čase dva pokusy o teroristické útoky), metro zastavovali a evakuovali každú chvíľu (stačilo ak niekto uvidel nejakú pohodenú nákupnú tašku), zažil som studený pot na chrbte, keď do autobusu nastupovali muži s bradami oblečený v thobe. Tiež som zažil vypratanie celej ulice Fleet Street pre podozrivú tašku v autobuse. Do nášho obchodu vtedy vbehli policajti a vyzvali nás, aby sme stáli čím najďalej od okien, ak by ten autobus vybuchol. So zákazníkmi sme čakali na pyrotechnikov asi hodinu, nikto nemohol za ten čas vyjsť z okolitých prevádzok a kancelárií.   Od teroristického útoku v Londýne uplynulo dnes päť rokov, nasledujúce desiatky útokov po celom svete ukazujú, že boj s terorizmom je veľmi ťažký. Na druhej strane niektoré režimy maskujú bojom proti terorizmu štátny terorizmus (napr. Rusko alebo Čína). Obmedzovanie osobných slobôd vidíme na každom kroku. Cieľom teroristov je vyvolať všeobecný strach, je na nás, aby nás strach neovládol. Len tak zabránime tomu, aby akýkoľvek extrémisti mali nad nami moc.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Odpočet práce poslanca v Rači 2010-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Politika bez kšeftov a tajných dohôd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Otvorený list primátorovi vo veci Železnej studienky a jej záchrany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Bude primátor Ftáčnik chlap alebo handrová bábika?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Ohlásenie vstupu do prvej línie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Drotován
            
         
        drotovan.blog.sme.sk (rss)
         
                                     
     
        Blogger na SME 2005-2014, nominovaný na Novinársku cenu 2012. Aktuálne som presunul blogovanie na http://projektn.sk/autor/drotovan/  
Viac o mne na: www.drotovan.webs.com 
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    224
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4323
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Poznámka pod čiarou
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Bratislava - Rača
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Mesto Bratislava
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Môj blog na Račan.sk
                                     
                                                                             
                                            Môj blog- Hospodárske noviny
                                     
                                                                             
                                            Klasický ekonomický liberalizmus ako spojenec konzervativizmu
                                     
                                                                             
                                            Salón
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            KIosk
                                     
                                                                             
                                            Knihožrútov blog
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                             
                                            Ivo Nesrovnal
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyklodoprava
                                     
                                                                             
                                            Bratislava - Rača
                                     
                                                                             
                                            Project Syndicate
                                     
                                                                             
                                            Bjorn Lomborg
                                     
                                                                             
                                            Leblog
                                     
                                                                             
                                            Cyklokoalícia
                                     
                                                                             
                                            Anarchy.com
                                     
                                                                             
                                            EUPortal.cz
                                     
                                                                             
                                            Political humor
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




