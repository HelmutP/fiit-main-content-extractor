

 Aj teraz tomu nie je inak. V poslednom čase si stále hovorím: 
 ,,Keď sa mi podarí urobiť skúšku, potom budem šťastná." 
 ,,Keď budem už mať všetky skúšky za sebou, potom sa budem naplno venovať bakalárskej práci a príprave na štátnice a potom budem šťastná." 
 ,,Keď už budem mať úspešne napísanú bakalársku prácu, potom budem šťastná." 
 ,,Keď už budem mať úspešne po štátniciach, potom budem šťastná." 
 ,,Keď ma príjmu na vysokú školu, na ktorú sa chcem dostať, potom budem šťastná." 
 ,,Keď si nájdem najlepšiu kamarátku, potom budem šťastná." 
 ,,Keď sa mi splní môj sen, moje túžby, potom budem šťastná." 
 Takýchto ,,Keď..., potom budem šťastná." je v mojom živote veľmi veľa. Niekedy až tak rozmýšľam, ako by som ten svoj život zmenila, urýchlila, aby som bola konečne šťastná. No stále zabúdam na jednu veľmi dôležitú vec! A to je, že ja nebudem šťastná až vtedy, keď úspešne zvládnem všetky skúšky, či budem mať po štátniciach, prijatá na školu na akú chcem ísť, alebo či sa mi splnia moje túžby, ja môžem byť šťastná už TERAZ.Takýchto želaní a túžob v mojom živote bude každý deň veľa. Ony sa budú postupne meniť. Jedny sa splnia skôr a na iné si budem musieť počkať dlhšie, no akonáhle sa mi niektoré túžby vyplnia, hneď sa objavia ďalšie. A takto to pôjde celý život. 
 Dnes si uvedomujem, že skutočným šťastím nie je to, keď dosiahnem to čo chcem, ale skutočným šťastím je to, keď sa dokážem tešiť z toho, čo mám. 
 Bože odpusť mi, že som tak veľa času premrhala frflaním nad tým čo nemám, čo by som chcela mať a nevšímala si koľko šťastia, darov a milostí mi deň čo deň dávaš. Prosím Ťa, Pane, nauč ma tešiť sa z vecí, ktoré som doteraz brala ako samozrejmosť. Nauč ma vidieť všetko to šťastie, ktorým ma každý deň obdarúvaš. Nedovoľ, prosím, aby som si celý svoj život myslela, že budem šťastná až vtedy, keď sa mi splní to, alebo ono, ale práve naopak dovoľ mi, aby šťastie bolo pre mňa cestou a nie cieľom. 

