

 
 
 Zničený, z poslednými kúskami síl som nastúpil na autobus. Okružná jazda po Košiciach v takom počasí a pri vedomí straty krásneho dňa, dokáže človeka poriadne nasrdiť. Cesta plynula pomerne rýchlo. Ani som sa nenazdal a prvá polhodinka cesty bola za mnou. Na zastávke „Národné námestie“ nastupovala starenka. Nízka, na kosť vychudnutá starenka chodiaca o paličke. Nieje to až taký zvláštny úkaz. Neskôr som sa dozvedel, že tá starenka má 93 rokov :). Pekný vek a o to bolo krajšie, že chodila dosť dobre a iba o jednej paličke :) 
 Zhodou náhod šofér tohto autobusu bol nieaký zvláštny. Bál som sa v tom autobuse, pretože stále zo sebou šklbal, rozhadzoval rukami a niečo tlmeným nahnevaným hlasom kričal- aj napriek tomu, že mu nikto počas jazdy neprekážal, žiadne dopravné priestupky ...nič. Skôr to vyzeralo, že v ňom býva ešte jeden platiteľ nájmu ktorý sa tiež chce dostať na slobodu. 
 Sedel som napravo od neho v prvom sedadle a tá starenka nastupovala do predných dverí. Šofér ako keby znervóznel, no jeho dialóg na chvíľu prestal Zviezla sa iba jednu zastávku a keď vystupovala, otočila sa na šoféra a povedala "Boh Vás žehnaj". Jeho tvár akoby skamenela a ne jej tvári sa objavil úsmev. Mňa to tak nadchlo, až som sa z toho začal usmievať aj napriek tomu, že som už skoro spal. 
 Keď vystúpila, šofér zavrel dvere a až na Ťahanovce s ním ani raz nešklblo, nikto tlmene nekričal, nikto sa nedobíjal von z tela. Bol proste kľudný. 
 Tri slovíčka, ktoré dokážu podvihnúť človeka, vniesť mu nádej, energiu, istotu... Nemyslím, že to spravia len práve tieto slovíčka. Môžu byť aj iné. Ale v podaní ľudí, ktorý toho prežili naozaj veľa a podané s úsmevom na tvári- to dokáže divy. 
 Možno som sentimentálny v tejto chvíli, ale celkový zážitok sa nedá tak dobre opísať ako sa cíti... 
   
   

