
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alžbeta Iľašová
                                        &gt;
                Nezaradené
                     
                 Veľká noc na Kréte 

        
            
                                    3.4.2010
            o
            12:13
                        (upravené
                3.4.2010
                o
                15:19)
                        |
            Karma článku:
                5.09
            |
            Prečítané 
            1055-krát
                    
         
     
         
             

                 
                    Pre Grékov je Veľká noc najvýznamnejším sviatkom v roku. Ich viera mi pripadá okázalá a veľkolepá, v tomto mi možno trochu pripomínajú našich cigánov.  
                 

                 
stánok s vyšívaným obrazom znázorňujúcim Krista v hrobeBetka
        Zašla som na Zelený štvrtok do miestneho ortodoxného kostola v Ammoudare. Pre spresnenie Ammoudara je mestská časť Heraklionu tiahnuca sa  na západ od hlavného mesta. Tam sme  so spolužiačkou Martinou ubytované v apartmáne u Matiny na celé tri mesiace nášho pobytu.        Do kostola ma zviezla Matina cestou do práce. Hneď pri vchode do kostola ma prekvapilo ako je tam málo ľudí. Myslela som si totiž, že celý Svätý týždeň ako ho volajú Gréci majú vo veľkej úcte a v hojnom počte sa zúčastňujú bohoslužieb. No postupne sa kostol zapĺňal. Vtedy mi došlo, že asi v ich cirkvi nebudú také striktné pravidlá ako v tej našej a ľudia môžu do kostola prichádzať postupne ako im práve príde vhod. Do dvoch hodín bol kostol preplnený a ľudia stáli všade naokolo. Interiéry ich kostolov sú krásne vyzdobené maľovanými ikonami aj po stenách všade visia mnohé ikony. Vpredu stojí kňaz väčšinu času obrátený chrbtom k ľudu, po stranách sú dve menšie stanovištia, kde stáli dvaja speváci na jednej a dvaja na druhej strane a spievali obradné piesne počas celej omše. Pred oltárom bola na stojane položená ikona zobrazujúca poslednú večeru Krista a ľudia sa jej chodili klaňať a na znak úcty ju pobozkali. Počas slávnosti zberal jeden chlapík klasicky peniaze do košíčka a v druhej ruke mal krásnu karafu, z ktorej lial dospelým na ruky a deťom na hlavy prenikavo sladko voňajúcu  vodu. Ľudia si tou vodou potierali tvár. Neskôr mi Matina vysvetlila, že je to svätená voda zmiešaná s extraktom z ruží a ľudia veria, že im pomôže udržať si pevné zdravie. Na konci omše kňaz podával Eucharistiu, lyžičku vína priamo do úst veriacich. Keďže som deformovaná štúdiom medicíny a hneď som si predstavila všetky možné choroby, ktoré by som mohla získať, radšej som sa vína dobrovoľne vzdala a vzala som si len chlieb. Ten bol vo veľkých košoch hneď vedľa kňaza. Používajú normálny veľký chlieb pokrájaný na menšie kúsky, my máme narozdiel od nich oblátku. Veriaci si brali aj po troch kúskoch chleba, jedli ho a rozprávali sa medzi sebou. V podstate sa dosť hlasno bavili aj počas celej omše, predsa len dve hodiny je pre tak hlučný a ukecaný národ asi dosť dlhá doba.       Včera bol Veľký piatok, spomienka na Kristovu smrť na kríži. S Martinkou sme sa šli prejsť ku kostolu, kde bol už pripravený a ozdobený Kristov hrob. Pred oltárom stál drevený vyrezávaný stánok so strieškou, celý ozdobený krásnymi kvetmi a na ňom bol položený vyšívaný obraz Krista ležiaceho v hrobe. Vedľa bola ešte jedna kniha, ale neviem presne čo to bolo za knihu, možno evanjelium. Ľudia sa chodili tam chodili klaňať a bozkávať obraz aj knihu. Malé deti ešte podliezali popod stánok. Za stánkom bol kríž, tiež pekne ozdobený a pod krížom na jednej strane ikona Márie a z druhej strany sv.Ján. Niektorí bozkávali aj kríž. V zadnej časti kostola si mohli ľudia kúpiť sviečky a zapichnúť ich do špeciálnej nádobky k ostatným horiacim sviečkam.       Večer nás pozvala naša Matina na večeru spolu s jej priateľom Zachariasom a ešte jedným jeho kamošom Emanuelom. Zaujímavé je, že Gréci držia prísny  pôst, na Veľký piatok, nesmú jesť mäso, ale ani mlieko, vajíčka a žiadne mliečny výrobky. Pôst však trvá len do západu slnka a práve potom všetci vyrážajú do ulíc a zaplnia všetky taverny v meste. Je to špeciálny deň kedy spoločne s priateľmi jedia ryby a dary mora. Aj keď podľa Matiny správne by mal pôst trvať až do polnoci, tak ako to máme vo zvyku my, ale Kréťania si to prispôsobili po svojom. Naša taverna bola hneď vedľa malého kostolíka preplneného ľuďmi, ktorí spievali a klaňali sa stánku s hrobom. Lepšiu atmosféru sme si ani nemohli priať.       Naši hostitelia nám vysvetlili, že u nich sa objednáva jedlo tak, že si v jedálnom lístku označia všetko čo chcú, a to sa prinesie na stôl. Nerobia sa jednotlivé porcie pre každého, ale jedlo sa položí do stredu stola a každý si berie na svoj tanier to, na čo má chuť. Keď sa to minie, objedná sa ďalšie. My s Martinou ako pravé Čechoslovenky sme nedôverčivo zazerali na všetky tie morské príšery, ktoré pred nami ležali. Kalamáry, chobotnice, sépie v inkouste....Ochutnali sme všetko a ostali sme nadšené. Ja som žiaľ zo všetkého zjedla len kúsok, keďže som držala pôst až do polnoci. Veľká smola, ale aspoň bol môj pôst ešte o to viac intenzívnejší :-).       O desiatej začali procesie. Z každého kostola sa vybrali sprievody rôznymi smermi. Na čele kríž, lampáše, kadidlo s rolničkami, za nimi ozdobený stánok znázorňujúci hrob Krista, v sprievode kráčali veriaci so zapálenými sviečkami a spievajúci veľkonočné piesne. Z balkónov sypali ľudia na stánok lupienky ruží všade tam, kde procesia prechádzala. Asi za pol hodiny sa vrátili do kostola a pred vchodom pozdvihli stánok do výšky a všetci ľudia vchádzali do kostola popod neho.       My sme sa z našej krásnej taverny ohúrené celou atmosférou vybrali do štýlovej kaviarničky, kde sme s Martinou ochutnali typický grécky horský čaj a Martina so žiariacimi očami zjedla všetko sladké, čo sa objednalo. Ja som zase ochutnala len máličko. Najlepší bol grécky syr (asi to bola féta), obalený a zapečený v tenkom ceste a poliaty výborným medom. Úžasné!!!       Mesto bolo stále plné ľudí a my sme sa unavené, ale spokojné vracali spolu s našou Matinou domov, do Ammoudary.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Medzi kaňonmi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Hľadanie Dia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Iľašová 
                                        
                                            Grécka cesta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alžbeta Iľašová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alžbeta Iľašová
            
         
        ilasova.blog.sme.sk (rss)
         
                                     
     
        Študentka šiesteho ročníka 1.lekárskej fakulty Univerzity Karlovej v Prahe, telom aj dušou hrdá východniarka - spišiačka, momentálne sa nachádzajúca v Heraklione, v hlavnom meste Kréty.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    969
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Jozef Klucho
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




