
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Medard Slovík
                                        &gt;
                architektúra lahodiaca oku
                     
                 Čaro kresaného dreva na Orave 

        
            
                                    23.3.2009
            o
            10:01
                        |
            Karma článku:
                10.68
            |
            Prečítané 
            6304-krát
                    
         
     
         
             

                 
                    Orava bola kedysi jednou z najvýznamnejších oblastí slovenskej ľudovej kultúry. Na pomerne malom území sa sústreďoval veľký počet rázovitých dreveníc. Roľnícke obyvateľstvo nebudovalo domy, robili to remeselníci - tesári, ktorých bol na dedinách dostatok. Charakter domov ovplyvňoval pôvod majstrov, ich zaužívaný typ domu a predovšetkým želanie stavebníka, jeho majetkové pomery, prírodne a zemepisné podmienky. V 50. rokov 20. storočia sa postupne začala ľudová architektúra z dedín vytrácať.  Pričom tieto drevenice dokladujú skutočné remeselnícke majstrovstvo a vynaliezavosť našich predkov. Vybral som preto základne typy nádherných dreveníc, ktoré boli v minulosti prirodzenou súčasťou tejto krajiny.
                 

                 "Každému zaseknutiu sekery prislúcha význam a každému úderu dláta výraz. Kde môžeme nájsť väčšiu štrukturálnu jasnosť ako na starých dreveniciach? Kde inde nájdeme jednotu materiálu, konštrukciu a formy? Tu sú zhromaždené znalosti celých generácii. Aký cit pre materiál a aká sila výrazu je v týchto budovách! Aké sú hrejivé a krásne! Zdajú sa byť ohlasom starodávnych piesni!"   architekt Mies van de Rohe          obr. 1 - Veličná -  omietnutá drevenica s výškovou komorou na dolnej Orave       Vývoj ľudovej architektúry sa začínal od sporého gotického osídlenia, pokračoval neskôr valaškou kolonizáciou, ktorá vytvorila kaleidoskop foriem, ktoré boli opakom slohovej jednoty na južnom Slovensku, od konca 17. storočia sa znovu zmiešal s množstvom etnických prvkov z južného Slovenska a tak vznikla pestrá a ťažko analizovateľná architektonická štruktúra týchto stavieb. Z hľadiska územného členenia ich rozdeľuje podľa základných charakteristických znakov na typy domu: Z dolnej a hornej Oravy.           obr.2 - Párnica - drevenica s výškovou komorou a pavlačouna dolnej Orave       Hoci vývoj vychádzal spočiatku zjednotenej schémy a zrubovej techniky na slovenskom území, postupne vznikli na Orave najrozmanitejšie stavebné typy. Niektoré dolnooravské domy majú spojitosť najmä so stredným Slovenskom s regiónmi Liptova a Kysúc. Na dolnej Orave sa zrub staval z kresaného dreva, často bielený okolo okien a dverí. Dispozícia domu bola obyčajne štvorpriestorová. Štít domu bol lomený v polovici, alebo dolnej tretine. Osobitnou skupinou domov tvorili roľnícko - remeselnícke domy s poschodím a pavlačou.          obr.3 - Zázrivá - najvyššie situované drevenice dosahujú hranicu lesa. Namiesto ciest vedú k ním chodniky.       Dnes už môžeme vidieť čaro kresaného dreva až na niekoľko výnimiek, zachovaného v pôvodnom prostredí,  len v expozícii Múzea oravskej dediny v Zuberci. Tieto rázovite drevenice, ich regionálne formy s pôvodným interiérom dokladujú rozmanitosť, invenčnosť, originalitu, cit pre harmóniu s prostredím. Preto medzi najvýznamnejšie architektonické objekty kultúrneho dedičstva na Orave právom patri aj neopakovateľná miestna ľudová architektúra, s ktorou sa návštevník tohto regiónu môže zoznámiť  v expozícii Múzeu oravskej dediny.          obr. 4 - Hruštín - uličná zástavba, s typickými dvakrát lomenými štítmi na hornej Orave             obr. 5 - Oravská Jasenica - slanický typ domu "s polkoňovými štítmi" na hornej Orave           Architektúru domov na hornej Orave delíme na štyri oblasti. Domy v poriečí Oravy sú oproti dolnej Oravy so strmšími strechami a štít je lomený v hornej tretine. V Zamagurí do popredia vystupuje najmä hruštínsky typ domu, bol pre túto oblasť najvýstavnejší a vznikol výsledkom stavebného vývoja 19. a 20. storočia. No naďalej sa vyskytoval aj jedenkrát lomený štít obvyklý v zátopovej oblasti Or. priehrady, najmä Slanica a Ústie. Tento slanický typ v Hruštíne nebol veľmi zastúpený, ale vyskytoval sa na širšom území od Hruštína, v Or. Jasenici a zasahoval aj do goralskej oblasti Oravského Veselého a poriečia Oravy (Podbieľ). Tento typ domu je starší, ako hruštínsky, pričom rozdielna charakteristika sa predovšetkým viaže na konštrukciu strechy a štítu.          obr. 7 - Novoť - dvojpriestorové bývanie spolu s humnom a stajňou pod spoločnou strechou bez lomeníc, typické pre goralskú oblasť Pilska a Babej hory       Goralské obydlia v oblasti Pilska a Babej hory vonkajšou úpravou, dispozičným riešením a predovšetkým rozdielnou strechou vytvárajú samostatnú skupinu oravskej architektúry. Do obytného priestoru sa vstupovalo z humná, alebo z predsiene, alebo jaty. Vonkjaší vstup nadväzoval na dvojpriestorový systém predsiene a izby, kde sa varilo aj bývalo. Strecha týchto domov predovšetkým dedinách, ako je napríklad Novoť je s plnými štítmi bez lomeníc.  Rozptýlená zástavba v dedinách kopaničiarskeho charakteru pre ňu je typickejšie, rozsiahlejšie samostatne stajace stavby, prevažne v obdĺžnikovom pôdoryse, ktoré pod spoločnú strechu zahŕňali aj obytnú a hospodársku časť. Staršie drevenice si zachovávali z vonku guľatosť  nekresaného dreva a škáry boli vypchaté machom. Mladšie drevenice už boli z kresaných brvien na vyššej  podmurovke.          obr. 6 - Rabčice - richtárska chalupa mojich prarodičov, pripomína slanický typ domu, strecha je lomená a pod oknami je vybielené, dreváreň je súčasťou obytnej časti a hospodárske budovy stoja samostatne       V niektorých goralských obciach v údoli Polhoranky obytná časť pripomína slanický typ, kým hospodárske budovy si zachovávajú goralskú dispozíciu. Dom je už riešený štvorprietorovo ( predná, zadná izba, predsieň a kuchyňa )  s  prístavnym humnom a maštaľou murovaného z kameňa.          obr. 8 - Suchá hora - vľavo goralská podholianska riešená pavlač a veranda domu       Posledná časť v architektúre Oravy tvorí goralská oblasť pod hoľami Západných Tatier. Ide o architektúru s podhoľanskými znakmi. Zrub je viazaný klinovaním z polovičných rezaných kmeňov, škáry sú vyplnené povrieslom. Podstrešné časti sú profilované a rezbársky zdobené. Architektúra sa viaže k podhoľanským na poľskej strane.       Bibliografické odkazy:   ČAPLOVIČ, P. 1977. (eds). Čaro kresaného dreva : ľudová architektúra Oravy. Martin, Vydavateľstvo Osveta        ČAPLOVIČ, P. 1980. (eds). Ľud Oravy v minulosti. Martin Vydavateľstvo Osveta   ČAPLOVIČOVÁ, Z. (eds). 1990. Múzeum oravskej dediny : Sprievodca po expozícii. Martin, Vydavateľstvo Osveta   KRPEĽAN, I. 1996a. Pokračovanie rodu : Využitie objektov ľudovej architektúry a súvisiace úpravy. Banská Bystrica, Merkantil   Fotografie obr. 1 - 5, 7, 8 - kolektív autorov z knihy Čaploviča, P. - Čaro kresaného dreva   Fotografia obr. 6 - z rodinného albumu autora     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Poézia južných Čiech (II.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Poézia južných Čiech  (I.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Vlčie chodníčky v Novoti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Vianočná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Highlands - nebezpečné midžis na ostrove Skye (III.)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Medard Slovík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Medard Slovík
            
         
        slovik.blog.sme.sk (rss)
         
                                     
     
        Píšuci pábitel Jozef Medard Slovík                     
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    141
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            príbehy z ulice
                        
                     
                                     
                        
                            postrehy hlavy
                        
                     
                                     
                        
                            kniha do dažda
                        
                     
                                     
                        
                            cestovanie na hrášku
                        
                     
                                     
                        
                            poviedka
                        
                     
                                     
                        
                            šport a stáť na jednej nohe
                        
                     
                                     
                        
                            glosa
                        
                     
                                     
                        
                            básne s čistou, krehkou lyriko
                        
                     
                                     
                        
                            medzi knihami
                        
                     
                                     
                        
                            citáty nie len o sebe
                        
                     
                                     
                        
                            na besede
                        
                     
                                     
                        
                            vône a chute
                        
                     
                                     
                        
                            architektúra lahodiaca oku
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            ČO PRÁVE ČITÁM
                                     
                                                                             
                                            John Steinbeck - Na východ od raja
                                     
                                                                             
                                            KNIHY KTORE MÁM RÁD
                                     
                                                                             
                                            Frances Mayesová - Pod toskánskym sluncem
                                     
                                                                             
                                            Emily Dickinsonová - Monológ s nesmrteľnosťou
                                     
                                                                             
                                            Herman Hesse - Malé radosti
                                     
                                                                             
                                            Anhthony de Mello - Modlitba žaby
                                     
                                                                             
                                            Richard Bach - Dar krídiel
                                     
                                                                             
                                            Miroslav Nevrly - Karpatské hry
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Carlos Arturo Sotelo Zumarán - Všeličo o Ríši Inkov
                                     
                                                                             
                                            Minarovičová - o potulkách svetom
                                     
                                                                             
                                            Baránek - varenie s medveďom
                                     
                                                                             
                                            Šuraba - Vikingovia a Remarque to je jeho
                                     
                                                                             
                                            Ulaherová - inšpiruje svojimi cestopismi
                                     
                                                                             
                                            Smejkalová - jej poéziu mám rád
                                     
                                                                             
                                            Šedivý - napísal prvý odkaz na mojom blogu
                                     
                                                                             
                                            Urda - filatelista a jednou rukou na blogu
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Poézia južných Čiech (II.)
                     
                                                         
                       Potulky nočnou Varšavou
                     
                                                         
                       Vlčie chodníčky v Novoti
                     
                                                         
                       Moje Dánsko v pätnástich bodoch
                     
                                                         
                       Vianočná
                     
                                                         
                       Medzi Filipom Macedónskym a Disneylandom
                     
                                                         
                       Highlands - nebezpečné midžis na ostrove Skye (III.)
                     
                                                         
                       V kraji Tarasa Buľbu (UA 2013/2)
                     
                                                         
                       Agamemnón, Leonidas, Kyklopovia  a Laskarina Bubulina
                     
                                                         
                       A opäť na Ukrajinu 2/2
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




