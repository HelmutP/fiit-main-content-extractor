
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Majerová
                                        &gt;
                Come in quietly
                     
                 V dialógu dvoch tiel 

        
            
                                    9.5.2010
            o
            11:08
                        (upravené
                9.8.2011
                o
                22:16)
                        |
            Karma článku:
                3.79
            |
            Prečítané 
            316-krát
                    
         
     
         
             

                 
                    
                 

                     Túžba tlejúca v tvojej duši   predstavami vo mne búši   spaľuje ma vôňou kvetov   v dialógu dvoch tiel bez slov       Zhoreli sme vášňou do tla   láska sa nám tvárí dotkla   popol zmätie do pálivej noci   bytostí vo Fénixovej moci       Po zrnkách nás opäť skladá   DNA lásky do citov vkladá   sme tajnou Pandorinou skrinkou   bez ťarchy bremena vinníkov       Očistení putom blízkosti   vpíšeme do knihy večnosti   príbeh o bludisku a hľadaní   o tajomstve a naplnení     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Majerová 
                                        
                                            Naslúchajme svojim myšlienkam. A strážme si ich.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Majerová 
                                        
                                            Ak budeme dieťaťu hovoriť, aké je zlé, naozaj sa takým stane.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Majerová 
                                        
                                            O polarizujúcej spravodlivosti modréhoZneba. O odpustení a slobode.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Majerová 
                                        
                                            Dovoľme deťom spoznávať realitu. No tak,aby sme im nebrali detstvo.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Majerová 
                                        
                                            Panta Rhei našich duší. Alebo nebojme sa byť sami sebou.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Majerová
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Majerová
            
         
        katarinamajerova.blog.sme.sk (rss)
         
                                     
     
        Som vnímajúca bytosť. Milovaná a milujúca žena. Šťastná matka. A to stačí :-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    86
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1115
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Svet mojimi očami
                        
                     
                                     
                        
                            Svet detí mojimi očami
                        
                     
                                     
                        
                            Symbolika
                        
                     
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Túlavé myšlienky
                        
                     
                                     
                        
                            Puto spomienok
                        
                     
                                     
                        
                            Come in quietly
                        
                     
                                     
                        
                            My voice is Lisa
                        
                     
                                     
                        
                            V rozhovore s láskou
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Stieg Larsson - Millennium
                                     
                                                                             
                                            A. de Saint-Exupéry - Země lidí
                                     
                                                                             
                                            Paulo Coelho
                                     
                                                                             
                                            Miguel Ruiz - Přestaň se bát
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Johnny Cash - Hurt
                                     
                                                                             
                                            Lisa Gerrard - The Serpent And The Dove
                                     
                                                                             
                                            Lisa Gerrard - Serenity
                                     
                                                                             
                                            Secret Garden - Nocturne
                                     
                                                                             
                                            Dulce Pontes - O Infante
                                     
                                                                             
                                            Lisa Gerrard - The Sea Whisperer
                                     
                                                                             
                                            This Mortal Coil - Song to the Siren
                                     
                                                                             
                                            Il Divo - Amazing Grace
                                     
                                                                             
                                            Lisa Gerrard - Sanvean
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Samo Marec
                                     
                                                                             
                                            Miloslav Bartos
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ivana Krekáňová
                                     
                                                                             
                                            Maťo Chudík
                                     
                                                                             
                                            Peter Balko
                                     
                                                                             
                                            Igor Mikula
                                     
                                                                             
                                            Dana Janebová
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Možno že...
                     
                                                         
                       Kŕmime svet, ktorý kŕmi nás.
                     
                                                         
                       obJAvenia VIII
                     
                                                         
                       Prelet nad autistickým hniezdom
                     
                                                         
                       Jesenný striptíz stromov
                     
                                                         
                       6 dôvodov prečo ešte neviete poriadne po anglicky
                     
                                                         
                       Dovoľme deťom spoznávať realitu. No tak,aby sme im nebrali detstvo.
                     
                                                         
                       Deti sú zrkadlom svojich rodičov
                     
                                                         
                       Zdá sa že svet sa vetrá z černoty
                     
                                                         
                       Stretká so spolužiačkami
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




