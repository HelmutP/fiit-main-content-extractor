

 Ježo Ežo, zubor Gábor* &amp; vlk Olinský (Howlin' Wolf) 
 Vlk Olinský vie vyť. Vyje na hviezdy. Nie vždy - keď veje vietor, nevyje. Kým nevyje, čo si navaril, tiež nie. Vari neveríte? Vie i to, že kto vyje na hviezdy, je slávny. Hviezdne slávny. (Dnes aj pre prednes.) Iný názor má zubor Gábor*, rodom zo Záboria &amp; Zubrohlavy. Keď marha svet ho hétfő-kedd tak skvári-zmorí-chorým spraví, rajcovne ratice do hrivy zubrice Zory zaborí .... oóch, oravské bory, sorry. A kdeže väzí ježo Ežo? Zažúroval si s ostriežom a včuľ túruje svoj Peugeot. 
 *zubor Gábor © Miki Lisický (1946 - 2008) 

