
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Medard Slovík
                                        &gt;
                architektúra lahodiaca oku
                     
                 Jar v Španej Doline a dážď v Kremnici 

        
            
                                    4.6.2010
            o
            8:53
                        |
            Karma článku:
                7.25
            |
            Prečítané 
            1409-krát
                    
         
     
         
             

                 
                    Šťastne som ukončil štvrtý ročník na Univerzite Mateja Bela v Banskej Bystrici. Odovzdal som index na študijnom oddelení a hurá na prvý prázdninový výlet do Španej Doliny. Autobus z Bystrice nás tam odviezol kľukatou cestou medzi horami. Špania Dolina práve voňala jarou a potešila naše srdcia.
                 

                    Vystúpili sme z busu a museli ešte zdolať 162 schodov krytého dreveného schodiska,   aby sme sadostali ku kostolu Premenenia Pána.          Vyše kostola sme natrafili na sympatickú chalúpku.          Od nej nás viedol chodník medzi haldami sute k vyhliadkovému miestu nad dedinou.          Zhora bol nádherný výhľad na dominantu obce kostola Premenenia Pána.          Pastvou pre oči boli aj okolité hory.          Prechádzali sme sa banskými uličkami a obdivovali kvetinovú výzdobu medzi domami.          Z oblokov sa na nás usmievali červené muškáty.          Na lúkach rozvoniavala materina dúška, žlté a fialové klinčeky.          Na ďaľší deň sme sa vybrali do Kremnice, už na prvý pohľad nás zaujal mestský hrad.          Morový stĺp Najsvätejšej trojice obdivujeme s dáždnikmi, pretože prší, prší a prší ...          Na námestí si pozrieme Múzeum mincí a medailí, kde si zaspomíname so slzami v očiach   aj na našu dobrú slovenskú korunu.          Najlepšie nás čakalo na záver v Múzeu Gýča v Kremnici sme sa osobne zoznámili   aj s Mojsejovcami.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Poézia južných Čiech (II.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Poézia južných Čiech  (I.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Vlčie chodníčky v Novoti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Vianočná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Medard Slovík 
                                        
                                            Highlands - nebezpečné midžis na ostrove Skye (III.)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Medard Slovík
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Medard Slovík
            
         
        slovik.blog.sme.sk (rss)
         
                                     
     
        Píšuci pábitel Jozef Medard Slovík                     
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    141
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            príbehy z ulice
                        
                     
                                     
                        
                            postrehy hlavy
                        
                     
                                     
                        
                            kniha do dažda
                        
                     
                                     
                        
                            cestovanie na hrášku
                        
                     
                                     
                        
                            poviedka
                        
                     
                                     
                        
                            šport a stáť na jednej nohe
                        
                     
                                     
                        
                            glosa
                        
                     
                                     
                        
                            básne s čistou, krehkou lyriko
                        
                     
                                     
                        
                            medzi knihami
                        
                     
                                     
                        
                            citáty nie len o sebe
                        
                     
                                     
                        
                            na besede
                        
                     
                                     
                        
                            vône a chute
                        
                     
                                     
                        
                            architektúra lahodiaca oku
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            ČO PRÁVE ČITÁM
                                     
                                                                             
                                            John Steinbeck - Na východ od raja
                                     
                                                                             
                                            KNIHY KTORE MÁM RÁD
                                     
                                                                             
                                            Frances Mayesová - Pod toskánskym sluncem
                                     
                                                                             
                                            Emily Dickinsonová - Monológ s nesmrteľnosťou
                                     
                                                                             
                                            Herman Hesse - Malé radosti
                                     
                                                                             
                                            Anhthony de Mello - Modlitba žaby
                                     
                                                                             
                                            Richard Bach - Dar krídiel
                                     
                                                                             
                                            Miroslav Nevrly - Karpatské hry
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Carlos Arturo Sotelo Zumarán - Všeličo o Ríši Inkov
                                     
                                                                             
                                            Minarovičová - o potulkách svetom
                                     
                                                                             
                                            Baránek - varenie s medveďom
                                     
                                                                             
                                            Šuraba - Vikingovia a Remarque to je jeho
                                     
                                                                             
                                            Ulaherová - inšpiruje svojimi cestopismi
                                     
                                                                             
                                            Smejkalová - jej poéziu mám rád
                                     
                                                                             
                                            Šedivý - napísal prvý odkaz na mojom blogu
                                     
                                                                             
                                            Urda - filatelista a jednou rukou na blogu
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Poézia južných Čiech (II.)
                     
                                                         
                       Potulky nočnou Varšavou
                     
                                                         
                       Vlčie chodníčky v Novoti
                     
                                                         
                       Moje Dánsko v pätnástich bodoch
                     
                                                         
                       Vianočná
                     
                                                         
                       Medzi Filipom Macedónskym a Disneylandom
                     
                                                         
                       Highlands - nebezpečné midžis na ostrove Skye (III.)
                     
                                                         
                       V kraji Tarasa Buľbu (UA 2013/2)
                     
                                                         
                       Agamemnón, Leonidas, Kyklopovia  a Laskarina Bubulina
                     
                                                         
                       A opäť na Ukrajinu 2/2
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




