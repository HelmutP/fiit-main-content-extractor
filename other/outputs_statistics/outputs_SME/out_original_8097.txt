
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kotrčka
                                        &gt;
                OpenSource
                     
                 Ako sa píše hlásenie o príjme 

        
            
                                    5.6.2010
            o
            12:06
                        |
            Karma článku:
                1.42
            |
            Prečítané 
            722-krát
                    
         
     
         
             

                 
                    Niekde som už písal od DX-ingu, poďme sa teraz pozrieť na reálny dopis reálnej stanici, ktorú som zachytil.
                 

                 Píšem do Portugalska, ale po anglicky - je to svetový jazyk, zatiaľ som s ním nemal problémy nikde. Prepáčte zníženú kvalitu angličtiny, nie som v nej práve najlepší (ale rozumejú mi celkom dobre, aspoň sa zdá). Komentáre modrou farbou a bez kurzívy.   Dear radio station RDP Internacional, - oslovujem väčšinou stanicu  it is my big pleasure to send you my reception report of your station RDP Internacional from Slovakia.  I do not know portuguese language so well and reception was not very clear, so I am sorry for small mistakes in names, etc. - úvod o tom, že je mi potešením písať im toto hlásenie o zachytení ich stanice, prípadne ešte ospravedlnenie za jazykovú neznalosť :-)   Here is the detail of my reception: - tu začína hlásenie s detailami - dátum, čas, frekvencia, programové detaily (kto čo hovoril, čo bolo témou, jingle a tak podobne)   Date: 5th of June 2010   Time: 0859 - 0915 UTC, 10:59 - 11:15 local CET, 09:59 - 10:15 local time in Portugal  Frequency: 12020 kHz, 25 metres band  Location: Rudná, Rožňava district, eastern Slovakia, coordinates: 48° 39′ 0″ N, 20° 30′ 0″ E  Receiver: Resprom A-104, vintage vacuum tube receiver - typ prijímača  Antenna: Wired, 3 metres long, indoor - anténa  Signal: average strenght, with fading, audible almost all time, no interference, good audio quality if the signal was strong, slightly better after 0910 UTC - signál (sila, rušenie, úniky, dojem z kvality)   Program details: - samotné detaily, čo najpodrobnejšie, kľudne aj s prepisom slov  0900 UTC  "Noticiados" + jingle   male voice - headlines and weather - "temperatura maxima", "bank centras", Gaza  female voice - Gaza, translated phone call (with english phone voice in background)  male voice - something about humanitarian, maybe city name Kandahar (signal was with fading this time)  female voice - music theme, name James Blunt  0907 UTC  male voice - spoken web address of radio station rtp.pt, jingle  male voice - spoken "RDP Internacional" + some information + "Portugal no mundo" (?) with background music  male voice - old footage "Viva Respublica"  0910 UTC  male voice with background music, "Club" (I found the real name on webpage - Clube de Amizade), said about plans for next hour  male + female (guest?) - dialogue   If my report is correct, please verify it with your QSL card or by verification letter on my address. - požiadanie o QSL lístok alebo list s potvrdením   I am 26 years old male, currently working with DTP and computer graphics. My interests are DX-ing (of course :-) ), tourism, cycling, photography and computers. I am also interested in knowledge of other cultures and languages. - niečo o sebe - vek, práca, záujmy   I wish you all the best and looking forward to receive your answer.   Yours   Peter Kotrčka - zdvorilostné formulky    P.S.: my address is /without accented letters in ( ) if not possible to see those on your computer/:   Peter Kotrčka (Kotrcka)  Rudná 333 (Rudna)  048 01 Rožňava (Roznava)  Slovakia - adresu píšem aj  diakritiky a až ako P.S., ale kľudne môže byť aj priamo v tele správy   P.P.S.: and a small slovak language lesson for you:   prajem pekny den - I wish you a nice day :-) - zvyknem pribaliť aj jazykovú lekciu slovenčiny, prípadne foto z okolia a tak podobne, proste originálnu drobnosť na koniec listu (mailu)   -------------------------------------------------------------   Takže takto to vyzerá po napísaní - už len odoslať a čakať na pekný QSL lístok od stanice. Príjemný DX-ing.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Správy STV/RTV:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Červená tabuľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            02 je drahé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Dlhodobý problém 02
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Prečo nie je 02 môj hlavný operátor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kotrčka
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kotrčka
            
         
        kotrcka.blog.sme.sk (rss)
         
                                     
     
        25/184/72 - narodený v deň, keď má meniny Adolf, IT, hamradio a cyklofanatik...

 
  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    63
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1383
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            OpenSource
                        
                     
                                     
                        
                            Fotografovanie
                        
                     
                                     
                        
                            Cestovanie a iné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sex na pracovisku
                     
                                                         
                       Má Ficova vláda prepojenie na scientológov?
                     
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Privatizácia Smeru
                     
                                                         
                       Cargo – obrovské dlhy, vysoké platy a pokútne spory
                     
                                                         
                       Na ceste po juhovýchodnom Poľsku
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Počiatkov pochabý nápad: zadržiavanie turistov za 30 EUR
                     
                                                         
                       "Rómsky problém"
                     
                                                         
                       Rómska otázka – aké sú vlastne fakty?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




