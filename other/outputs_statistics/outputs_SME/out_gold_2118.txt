

 „A úplne strašné je to, ak nemajú ani bratranca alebo sesternicu, či uja alebo tetu, a vlastnia iba jedného otca a mamu, jedných starých rodičov z jednej strany a z druhej strany. To je hrozná otrava!“ 
 Pravdu povediac, padla mi z jej vyjadrenia sánka. 
 Človek, to ako ja – matka, si občas zúfa, keď vidí svoje potomstvo v sústavných výbojoch a slovných frčkách, ktoré nie sú zrovna milosrdné a nežné. A čo je paradox, táto naša puberťáčka vzhľadom na svoju povahu a ostrý jazyk je na tom s oným vyjadrovaním sa o svojich milovaných súrodencoch najhoršie. 
 A tak často premýšľam: aké by to bolo, keby ich bolo menej? Keby všetci boli chlapci? /Lebo tí sú u nás síce ukecaní, ale vcelku mierumilovní a ľahšie ovládateľní, na rozdiel od našich diev./ Bili by sa menej? Bola by som ušetrená tých strašidelných dohadovačiek typu „Ja som umýval včera...“ a „Prečo on nie?“... Alebo „no jasné, ona je tvoj miláčik...“ a „Zasa ja...“. 
 Občas mám pocit, že by to bolo ešte strašidelnejšie pri menšom počte ratolestí, nakoľko by matematická pravdepodobnosť opakovania rovnakých krstných mien bola častejšia a to by mi asi pílilo uši. /A nervy, priznávam. 
 Niežeby som teraz nemala z toho záchvaty zúfalstva, čo za vzťahy to budú mať moje deti, až vyrastú a ich rodičia sa pominú – a občas im to aj poviem na rovinu, že sa zožerú aj s topánkami, ak sa budú tak neuveriteľne zaoberať pocitom nespravodlivosti voči ich maličkosti. 
 A nato... len pár minút po tom, ako Kubo atakoval funkciu požiarnej húkačky s výkrikom „OOOOOOOONa mi robíííííííííí zléééééééééééééééé!“ a jeho milovaná sestrička ho na oplátku vyprevádzala nepoeticky z dievčenských priestorov slovíčkom „Zmizni!“ ... sedia obaja svorne nad jedným /!!!!/ výtlačkom hlavolamov pre bystré hlavičky a s chichotom lúštia akési rébusy, zjavne sa netýkajúce súrodeneckých zvád. 
 V takých okamihoch nasucho pregĺgam výchovné ponaučenie so silne moralizujúcim podtónom a v duchu si pomyslím: „Asi to, Bože, nebude také zlé, ako to vidím...“ 
 Takže, ak riešite problém, koľko súrodencov dopriať svojmu prvorodenému, a máte odvahu a viete zniesť značnejšiu dávku decibelov /asi ako neprestávajúca disco od šiestej rána do času, kedy detváky zaspia/, tak neváhajte...  
 Nakoniec, naša svorka napriek všetkým nezhodám svorne tvrdí, že keby ich bolo menej, bola by to nuda. 
 My s mojim zákonitým a drahým manželom, /nakoniec, kto za ten počet môže ak nie my? / len v tichu dúfame, že staré platné porekadlá kašlú na pretechnizovanú dobu a platia aj teraz. 
 Vraj „co se škádlívá, to se rado mívá“. Neuveriteľné. 


