
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Babčan
                                        &gt;
                Nezaradené
                     
                 Posledné výdychy zimy na Skalke 

        
            
                                    18.3.2010
            o
            8:53
                        (upravené
                18.3.2010
                o
                9:05)
                        |
            Karma článku:
                6.58
            |
            Prečítané 
            870-krát
                    
         
     
         
             

                 
                    Tohtoročná zima si v polovici marca povedala, že ešte na chvíľu ostane. Ba čo viac, nadelila nám slušnú nádielku bielej pokrývky, ktorá potešila mnohých lyžiarov.
                 

                     Banská Bystrica sa zo snehu tešila len pár hodín. No sú miesta, kde sa sneh usadil na dlhšie obdobie. Pritom si stačí vybehnúť pár kilometrov od centra. Príkladom môže byť obec Králiky. Tam ešte stále panuje zima v pravom slova zmysle. Túto obec pozná azda každý bežkár v okrese Banská Bystrica. Je to hlavne z dôvodu východiskového bodu do raja bežeckého lyžovania – Skalku v Kremnických vrchoch.     
 Je viacero spôsobov ako sa dostať z obci Králiky na Skalku. Väčšina bežkárov volí takzvanú zvážnicu, ktorá je svojím stúpaním najprijateľnejšou voľbou. Zvážnicu je spravidla raz do týždňa upravovaná a preto je vhodná pre klasickú techniku aj korčuľovanie. Cez Králické mostíky vedie turistická trasa, ktorá vrcholí v Králickom sedle. Je určená pre pešiu turistiku, prípadne skialpinistov, z dôvodu veľkého prevýšenia. Prijateľnou voľbou je vybrať sa na skalku cez tunel, ktorý kedysi slúžil na prepravu medi a striebra z hút v Tajove a Banskej Bystrici do Kremnice.   Keďže cez víkend napadlo snehu viac než som čakal, znamenalo to, že trate nebudú upravené. Preto sme volili trasu cez tunel. Namazali sme fialový vosk a vybrali sme sa Tajovskou cestou smerom na tunel.         
 Vosk držal lepšie ako lepidlá z reklamy a tak sme skôr kráčali ako “bežkovali“. Po Tajovskej ceste nás to obmedzovalo, no v stúpaní na tunel to bolo na nezaplatenie. Všade navôkol vysoká vrstva snehu, len stredom koľajnička vyjazdená turistami, ktorí si privstali.             
 S pribúdajúcimi metrami boli stromy viac a viac zahalené do bieleho plášťa. Sám neviem, či som túto zimu zažil takúto nádheru.        
 Na konci stúpania nás už čakal tunel. Na jeho druhom konci šumel vietor a tak sme sa občerstvili ešte na našej, Bystrickej strane. Tak ako sme čakali a už aj počuli, kremnická strana hostila počasie iného rázu. Vietor sfukoval sneh zo stromov, tvoril záveje a miestami nebolo nič vidieť. Aj keď sme boli na to pripravení, dlho sme sa hore “neohriali“ a hor sa dole na zvážnicu.                
 Je to zaujímavé, ako sa zima snami zahráva. Už v októbri príde z obrovskou nádielkou a potom na nás určitý čas zabudne. A keď si už myslíme, že sa ukáže zas na rok, ešte príde na návštevu. Predpokladám, že do veľkej noci bude ešte snehu na skalke dostatok. Treba to využiť!      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Babčan 
                                        
                                            Aj Slovensko má čo ponúknuť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Babčan 
                                        
                                            Odložili ste lyže a neviete čo s časom? Tu je malý príklad.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Babčan 
                                        
                                            Kráľova hoľa v celej svojej kráse
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Babčan 
                                        
                                            Medailová nádielka našich biatlonistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Babčan 
                                        
                                            predstavy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Babčan
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Babčan
            
         
        babcan.blog.sme.sk (rss)
         
                                     
     
        Som človek, ktorý nemá rád stereotyp. Vo všetkom sa snažím nájsť niečo nové, aby bol môj život pestrejší.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1044
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




