
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Žiak
                                        &gt;
                Politika
                     
                 Do plynu so všetkými,čo i len raz vyskúšali marihuanu.Nepodmienečne 

        
            
                                    18.6.2010
            o
            16:14
                        (upravené
                18.6.2010
                o
                16:22)
                        |
            Karma článku:
                13.21
            |
            Prečítané 
            3164-krát
                    
         
     
         
             

                 
                    Ak máte záujem vidieť "marihuanovú" problematiku trochu viac „širokouhlo“ a nebyť manipulovaný názormi rôznych „krčmových odborníkov“ a „zaujatých hipíkov“, práve Vám patrí tento článok. A ak máte pocit, že táto téma sa Vás netýka a nie je pre Vás zaujímavou, pretože je to len banalita oproti iným, napr. ekonomickým otázkam, na ktoré treba upriamiť pozornosť, ste na veľkom omyle. Odpoveď nájdete v článku.
                 

                 
  
   Len nedávno sa na Slovensku začalo hovoriť na tému „dekriminalizácia marihuany". Prieskumy ukazujú, že konzumácia marihuany už dávno nie je v našej krajine tabu, avšak hovoriť o nej na verejnosti a rozpútavať verejnú diskusiu na túto tému, tak s tým majú mnohí problém aj v dnešnej dobe. Strana SaS si ako prvá a jediná strana, ktorá bude sedieť v parlamente a teda bude disponovať veľkou silou prednášať verejnosti nové témy, zakotvila do svojho programu dekriminalizáciu marihuany. Týmto svojím rozhodnutím zároveň chtiac-nechtiac otvorila dlho tabuizovanú tému v našej krajine a už teraz možno pozorovať líšiace sa názory ľudí. Tieto názory sa dostávajú k širokej verejnosti, ale len máloktorý z nich pochádza z úst kompetentných ľudí, ktorý majú z danej oblasti dostatočné vzdelanie a informácie.   V prvom a neposlednom rade si treba uvedomiť, čo je to marihuana. Áno, marihuana je skutočne droga. Napriek tomu, že slovenský zákon nepozná tvrdé a mäkké drogy, odborníci v danej problematike považujú a označujú marihuanu za mäkkú drogu. To, že naše právo nepozná takéto rozdiely medzi drogami, netreba vnímať ako dogmu, že nie sú rozdiely medzi jednotlivými drogami, ale skôr ako nedokonalosť slovenského zákona, ktorý nedostatočne reflektuje poznatky odborníkov. Ak chce niekto naozaj tvrdiť, že nie je rozdiel medzi heroínom a marihuanou, pravdepodobne to bude spôsobené nedostatočným preštudovaním rôznych odborných kníh, či článkov. Ale nemyslím si, že takýto ľudia tvoria zaujímavé percento v našej spoločnosti.   Čo sa týka pozitívnych účinkov marihuany na ľudský organizmus, tie sú k nám zväčša podsúvané od samotných užívateľov „trávy". Odpoveď na otázku, prečo to robia, je veľmi jednoduchá. Totižto, ak by to tak naozaj bolo, prívrženci legalizácie by mali v rukách silný tromf, prečo by sa mala táto droga zlegalizovať čo najskôr (len pripomeniem, že bod programu strany SaS nehovorí o legalizácií, ale dekriminalizácii, v čom treba vidieť rozdiel). A len ťažko by sa hľadali protiargumenty na takýto „liek". Situácia je však trochu iná. Je dnes preukázateľné, že marihuana má aj pozitívne účinky na ľudský organizmus. Otázka však stojí inak. Má marihuana naozaj také špecifické pozitívne účinky na ľudský organizmus, aké nemá žiadna iná legálna látka, rastlina, či liek? A ak by tomu bolo tak, nebolo by dostatočné, ak by sa distribuovala táto droga len vo forme lieku a na lekársky predpis len pri určitých skutočne špecifických prípadoch? Treba hovoriť o veciach pravdivo a nezaujato a to platí aj pre prívržencov dekriminalizácie, alebo legalizácie. Určite existujú aj dôvody, prečo by bolo dobré, aby bola marihuana legálna, avšak to, že marihuana lieči, to nie je argument pre jej legalizáciu. Ale kto vlastne hlása na Slovensku legalizáciu, resp. ktorý politický subjekt ju podporuje?   Z táboru, ktorý je či už proti dekriminalizácii, alebo legalizácii sa zase ozývajú hlasy, že marihuana je škodlivá pre človeka. Nepochybne, je to nevyvrátiteľný argument. Napriek tomu, že môže, resp. má aj nejaké pozitívne účinky, je zrejmé, že tie škodlivé sú v značnej prevahe. Avšak škodlivosť marihuany pokladať za dôvod, prečo byť proti dekriminalizácii je naozaj scestné. Pre človeka škodlivých vecí je dnes kvantum, počnúc rôznymi potravinárskymi výrobkami, končiac pri alkohole a cigaretách. Napriek tomu ich je možné v súčasnosti zakúpiť v každom bežnom obchode. Argument ľudí, ktorí sú proti dekriminalizácii však nie je ten, že si tým konzumenti sami ubližujú a preto ich treba ochrániť pred vlastnou hlúposťou. Ak by mal byť naozaj toto argument, tak stojí naozaj na zlých základoch. Jednak je to ignorácia existencie iných škodlivých látok človeku bežne dostupných a v neposlednom rade je to obrovský zásah do svojprávnosti človeka, do jeho práva slobodne sa rozhodnúť, čo je pre neho dobré a čo zlé. Napokon, ak niekto konzumuje marihuanu dajme tomu pár krát do roka, nebodaj pár krát za život, ako veľmi mu môže uškodiť na jeho zdraví?   Mám ale skôr pocit, že niektorým ľuďom sa nepáči fakt, že na liečenie závislosti a chorôb spojených s vedomou konzumáciou škodlivej „trávy" sa musia skladať aj zo svojich peňaženiek, prostredníctvom daní a poistného. Zdieľam s týmito ľuďmi podobný názor a tiež si veľa krát pokladám otázku, prečo by som aj ja mal platiť za to, keď si niekto vedome ničí svoje zdravie, akýmkoľvek spôsobom. Ak však nechceme, aby to tak bolo, nebolo by spravodlivejšie, aby sa na liečenie takýchto chorôb skladali len tí, ktorí sa takto „rizikovo" správajú? A ak chceme aby to tak bolo, nevyriešila by to práve legalizácia a daň, ktorú by konzument musel platiť pri kúpe takýchto škodlivých výrobkov?   Vstupná brána. Dve slová, jednoduchá fráza a pritom už asi všetci vedia o čom je reč. Naozaj nemožno vyvrátiť fakt, že väčšina užívateľov tvrdých drog začala s marihuanou. Nikto ma nepresvedčí o tom, že to tak nie je. Ale rovnako tak ma nikto nepresvedčí o tom, že drvivá väčšina tých, ktorí začali s marihuanou, nevyskúšali pred tým alkohol. Nie je potom namieste hovoriť o tom, že vstupnou bránou do sveta drog je alkohol? Poznám veľa ľudí, ktorý konzumujú marihuanu pravidelne, napriek tomu však neholdujú iným nelegálnym drogám. Možno teda vôbec hovoriť o akejsi bezprostrednej súvislosti medzi marihuanou a tvrdými drogami? Ja by som za tým skôr hľadal iné súvislosti, ako napr. sociálne prostredie, výchova, nedostatočná kontrola, vzdelanie...  Ono naozaj, to že sa ukázalo, že prevažná väčšina „závislákov" na tvrdých drogách začala s trávou nie je o nič väčšia pravda ako to, že prevažná väčšina z nich začala s alkoholom. A taktiež všetci závislí na tvrdých drogách začali chodiť v šiestich, alebo siedmich rokoch do školy. Nebude to náhodou tým?   Obavy ľudí z dekriminalizácie pochádzajú určite aj z toho, že sa domnievajú, že to bude mať zlý vplyv na mládež, zvýši sa jej konzumácia, pretože bude ľahko dostupná a z mnohých mladých perspektívnych ľudí sa stanú trosky. Opäť treba voči týmto názorom a presvedčeniam postaviť do kontrastu pár faktov. Dekriminalizácia neznamená, že sa droga bude ľahšie dostupná a následne sa zvýši jej konzumácia u mladých. S tým súvisí do určitej miery legalizácia, nie však dekriminalizácia. No a dekriminalizácia marihuany podľa návrhu strany SaS už von koncom nie. To, že sú mnohí ľudia zaujatí proti dekriminalizácii nie na základe faktov, ale skôr akýchsi osobných pocitov a presvedčení a nepochopenia problému je zrejmé aj z toho, že títo ľudia nepodporujú myšlienku SaS, ale práve naopak, bojujú proti nej. Prečo? Pretože Sulíková SaS nehovorí len o tom, že konzumenti marihuany by nemali byť trestne stíhaní, ale aj o tom, že díleri, ktorí distribuujú marihuanu medzi ľudí by mali mať väčšie tresty. Práve to je cesta, ako odstrániť, resp. znížiť konzumáciu „trávy" na Slovensku. Chytanie drobných užívateľov neodstráni drogy z ulíc, pretože vždy sa nájdu noví užívatelia, ktorí podľahnú pokušeniu. Chytanie drobných užívateľov je nie len že neefektívne z hľadiska znižovania výskytu drog na uliciach, ale je to aj neefektívne narábanie s potenciálom policajných zložiek, súdov a tým pádom aj peňazí nás všetkých. Mimochodom, Sulíkova forma dekriminalizácie tiež ráta s trestami pre tých, ktorí budú prichytení s danou látkou. Nie však basa, ale finančná pokuta, ktorá môže byť investovaná jednak do prevencie spoločnosti napríklad väčšou mierou informovanosti, alebo do prevencie sekundárnej, čiže do rôznych inštitúcii zaoberajúcich sa liečbou závislých. Konzumenti by teda neokrádali štát napr. aj tým, že by boli v base (pretože každý človek v base nás stojí nemalé peniaze), ale prispievali by na neho (napr. na liečenie závislostí) prostredníctvom mastných pokút. Proti zástancom názoru, že droga bude po dekriminalizácii dostupnejšie stojí teda fakt, že dekriminalizácia prináša so sebou aj vyššie tresty pre dílerov (čo práve znižuje dostupnosť drogy), na čom majú záujem práve odporcovia dekriminalizácie. To, že bude marihuana dostupnejšia po jej dekriminalizácii možno spochybniť aj iným spôsobom. Totižto, dnes v našej republike, ak by som mal záujem kúpiť si „jointa", môžem tak urobiť kedykoľvek a verte mi, do desiatich minút by som ho držal v ruke. A takto to nie je len v Bratislave. Myslím, že „tráva" je dnes maximálne dostupná a neviem si moc dobre predstaviť ešte vyššiu dostupnosť, ako je dnes. Napriek tomu, že je v súčasnosti nelegálna.   Ak si stále myslíte, že sa Vás táto téma netýka, je to nerozvážne zmýšľanie. Ak aj napr. neholdujete tomuto spôsobu zábavy, resp. odreagovania sa, možno ním budú holdovať Vaše deti. A možno nie. Možno len raz v živote skúsia vyfajčiť jedného jediného „jointa" a možno práve vtedy bude pri tom polícia, ktorá spraví z mladého človeka zločinca, ktorý skončí v base. A možno sa tak nestane, ale každopádne majte na pamäti, že podľa prieskumov (z roku 2006) marihuanu vyskúšalo aspoň raz v živote 31,3 percenta opýtaných ľudí vo veku 15-24 rokov. Inými slovami každý tretí mladý človek. Možno ani neviete, že svojimi názormi chcete svojho vyštudovaného vnuka, ktorý pracuje a zarába a prispieva tak aj na iných, čo sú v núdzi, poslať do basy. Naozaj si myslíte, že sa Vás to netýka?   Zaujímali by ma tiež Vaše daľšie názory, prečo by „mariška" mala, resp. nemala byť dekriminalizovaná, takže ak máte k tejto téme niečo zaujímavé, sem s tým.   P.S.:  Mal som možnosť vidieť toho starého pána na námestí, ako bojkotuje zhromaždenie za dekriminalizáciu marihuany a ako by najradšej videl všetkých narkomanov v plyne. Napriek tomu, že starý pán si zmýlil pojmy, pretože dnes aj „základoškolák" vie, že pod pojmom narkoman sa rozumie niečo iné ako konzument marihuany, napriek tomu mu neprajem, aby skončil v plyne. Ani on, ani narkomani a ani užívatelia marihuany. Spomínaný starý človek patrí do školy, narkomani patria na liečenie a konzumenti marihuany patria na slobodu. Pretože ubližujú iba sebe a tak podať na nich trestné oznámenie by mali mať právo len oni sami.   Autor je vyštudovaný sociálny pracovník na Univerzite Komenského.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (137)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Žiak 
                                        
                                            Iphone 4S za 569 € cez TopSales.sk, alebo pozor na vianočné podvody
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Žiak 
                                        
                                            1. slovenský zábavný park otvorený!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Žiak 
                                        
                                            Fico začne písať pre SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Žiak 
                                        
                                            Dajte facku spravodlivosti!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Žiak
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Žiak
            
         
        jurajziak.blog.sme.sk (rss)
         
                                     
     
        Vyštudoval som sociálnu prácu na pedagogickej fakulte UK a pracujem ako IT technik vo firme so sídlom v Bratislave. Okrem iného sa zaoberám tvorbou web stránok.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3563
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Internet
                        
                     
                                     
                        
                            Reflexie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




