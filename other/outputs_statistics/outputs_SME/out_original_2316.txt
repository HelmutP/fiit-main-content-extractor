
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarina Pisutova
                                        &gt;
                africký život
                     
                 Nadváha sa nosí a roky pridávajú na vážnosti... 

        
            
                                    19.2.2010
            o
            11:14
                        (upravené
                19.2.2010
                o
                11:26)
                        |
            Karma článku:
                16.29
            |
            Prečítané 
            4463-krát
                    
         
     
         
             

                 
                    V Ugande je nadváha znakom blahobytu a objektom závisti a úcta k starším vštepovaná odmala a poskytovaná na každom kroku.
                 

                 V našej rurálnej oblasti je obéznych Uganďanov poriedko. Jedla je málo, prevažne sa je varená zelenina a väčšina ľudí pracuje na poli od rána do večera. To sa potom nepriberá. Obézni sú naozaj iba tí, ktorí si to môžu dovoliť. Nielenže patria medzi tých vyvolených 20%, ktorí majú prácu, ale ešte k tomu to nie je práca manuálna a môžu si dovoliť jesť viac, než ich organizmus nevyhnutne potrebuje. Takže priberajú bankári, právnici, farári, úradníci, manažéri či biznismeni. A páni sú primerane hrdí na svoj pivný sval a dámy na impozantné pozadie.   Takto je potom spoločensky nielen že prípustné, ale ba priam vhodné zložiť kompliment človeku, ktorému sa pošťastilo pribrať. A vyjadriť sústrasť ak niekto schudol. Moja americká kamarátka, ktorá tu žije už vyše desať rokov sa celkom dobre bavila na tom, ako dostávala komplimenty v čase, kedy sa jej po pôrode nedarilo zbaviť nadobudnutých kíl. A ako ju niekoľko miestnych známych poľutovalo potom, keď sa jej tvrdou diétou a cvičením podarilo dostať postavu do rozmerov, ktoré ona sama považovala za akceptovateľné.   Starých ľudí v rurálnej Ugande tiež vidíte málo. Podľa štatistík je ešte stále priemerná dĺžka života 47 rokov a aj keď AIDS je na miernom ústupe, iné choroby a nedostatok zdravotnej starostlivosti stále hrajú úlohu. Ale tých pár „muzee" a „makekuru", ktorých stretnete požívajú jednoznačnú úctu. Videla som ako sa divoká party na svadbe rozostúpila ako more pred prichádzajúcou babičkou, ktorej dvaja pomáhali kráčať a ďalší traja skočili prisunúť a pridržať stoličku. Okolo mudrujúceho starčeka sa sedí a počúva, aj keď mu vypovedať každé slovo trvá desať sekúnd. A samozrejme, v krajine, kde neexistuje dôchodkový systém sú to mladí ľudia, ktorí sa musia postarať o svojich starých rodičov či príbuzných.   Keď som pred pár dňami kráčala s deťmi po ulici v Kabale, zbehnuvší sa kŕdeľ detí mi skladal komplimenty v snahe čosi vyžobrať. Dozvedela som sa takto, že moja dcéra má krásne vlasy, že ja mám pekné tričko a druhá dcéra má pekné topánky. A potom ma malé dievčatko dorazilo otázkou tiež zjavne myslenou ako kompliment: „A ty si ich stará mama?" Jasné, vyzerám na svoj vek a štyridsiatničky v Ugande väčšinou už starými mamami sú... Moje našské ego to aj tak nieslo ťažko..   Moj manžel tvrdí, že až budeme na dôchodku, deti budú odrastené a preč z domu, mali by sme sa do Ugandy odsťahovať natrvalo. Budeme mať úcty a úcty...        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Od akého veku učiť deti pracovať s počítačmi?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Na Slovensku je skvele. Čo tam po Amerike…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            V Amerike dobre, doma najlepšie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Ako sme vianočný papier recyklovali
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarina Pisutova 
                                        
                                            Super Bowl
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarina Pisutova
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarina Pisutova
            
         
        pisutova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Komunitný projekt v Ugande (www.lakebunyonyi.net), americký manžel, dve dcéry, online vzdelávanie a chuť cestovať a objavovať nové veci...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    127
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4325
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            africký život
                        
                     
                                     
                        
                            americký život
                        
                     
                                     
                        
                            len tak kecam
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Martin Meredith - The State of Africa
                                     
                                                                             
                                            Ryszard Kapucsinski - Shadow of the Sun
                                     
                                                                             
                                            Jon Krakauer - Into The Wild
                                     
                                                                             
                                            Tim Robbins - Even Cowgirls Get the Blues
                                     
                                                                             
                                            Giles Foden - The Last King of Scotland
                                     
                                                                             
                                            Thor Hansen - The Impenetrable Forest
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sorry Bože, sorry ľudia alebo novoročné čistenie duše
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                                                         
                       Národ zlodejov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




