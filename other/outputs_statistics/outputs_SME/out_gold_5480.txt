

 Objatie. 
 To je najväčšia dôvera. To je obrovská blízkosť. 
 To je, keď môžem prestať riešiť a premýšľať. Stačí len byť. Keď si neuvedomujem zem, zimu ani hnev. Keď môžem len cítiť a nič viac. 
 Také objatie je vzácne. 
 Objatie, ktoré sa nikam neponáhľa. Ktoré tu je len pre mňa. Také, ktoré sa nešetrí, lebo tak ako ja potrebujem dostať, ono chce dať. 
 Veľmi také objatia potrebujem. Veľmi ich potrebujeme. 
 Ďakujem za také objatia. 

