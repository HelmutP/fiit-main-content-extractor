
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Makatúra
                                        &gt;
                Nezaradené
                     
                 Kamikadze a moslimskí samovražední útočníci 

        
            
                                    2.4.2010
            o
            9:50
                        (upravené
                2.4.2010
                o
                9:42)
                        |
            Karma článku:
                6.27
            |
            Prečítané 
            976-krát
                    
         
     
         
             

                 
                    Sebazáchova je jedným z najsilnejších ľudských pudov. Len v extrémnych situáciách dokáže ľudská bytosť obetovať život pre iný prospech. Európska civilizácia berie život ako najcennejšiu hodnotu. Zdá sa nám absurdné, aby sa normálny človek opásal bombou a išiel vyhodiť do vzduchu iných ľudí...
                 

                 História nám však pripomína, že s takýmto fenoménom sme sa už v modernom svete stretli. Nie je to tak dávno, keď túto taktiku používala japonská armáda. Beznádejne sfanatizovaní „dobrovoľníci" sa vrhali na bojových lietadlách na vojnové lode nepriateľa. Má to okrem smrti nejaké podobné črty s dnešnými moslimskými útočníkmi?   Japonskí kamikadze boli väčšinou z radov vojenských pilotov. Železná armádna disciplína a náboženské presvedčenie, ako aj historická „tradícia" Japonska je črta, ktorá ich spája s moslimskými krajinami žijúcimi v rovnakých schémach dnes. Ku koncu vojny, keď sa zásoba cvičených pilotov vyčerpala, boli Hirohitovi dôstojníci schopní posielať na smrť aj „rýchlovýkrm" kamikadze. Boli to študenti a ich „pilotný výcvik" pozostával len z niekoľkohodinovej inštruktáži v drevenej lavici s provizórnymi klapkami, ku ktorému prehrávali zvuk motora lietadla... Sľubovaná večná sláva a miesto medzi bohmi je rovnaká odmena, ako pre moslimských teroristov... Ich rodinám patrila spoločenská úcta.. Sebevražední piloti nemali v podstate spoločensky prijateľnú alternatívu s vecou nesúhlasiť. Všetko to silne pripomína svet terajších teroristov. Až na niekoľko odlišností   Dialo sa to vo vojne. Kamikadze boli vojakmi a útočili na vojenské ciele. Dnešní teroristi sú verbovaní z radov civilistov a ich masakrovanie nevinných obetí sa deje mimo vojenský priestor. Otázne je, či majú títo ľudia vo svojej komunite možnosť inej voľby...?   Zmenu môže priniesť len zrušenie schémy. Ponuka iných možností. Prienik do priorít.   Japonci museli po vojne svoj svet prebudovať od základu. Otvorili sa svetu a stavili na technický pokrok. Napriek odlišnostiam sú schopní kooperovať s Európou a Amerikou v spoločnom hodnotovom rebríčku.    Aký je bod zlomu moslimského priestoru? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (52)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            So sestrami sa hrá hra na hlúpych
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Kolaps parlamentnej demokracie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Slovensko neuživí toľko politikov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Krajina kde práca znamená prežívanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Surová doba alebo ako speňažiť objekt domova sociálnych služieb
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Makatúra
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Makatúra
            
         
        makatura.blog.sme.sk (rss)
         
                                     
     
        Elév-idealista

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    98
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1538
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Krajina kde práca znamená prežívanie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




