

 Niekde sa rozleteli noty. 
  
 Niekde sa rozfúkal vietor. 
  
 Niekde sa niečo hrá. 
  
 Niekde sa už dohralo. 
  
 Niekde vlak už nepríde. 
  
 Niekde prišiel a odchádza pomaly. 
  
 Niekde takmer nikto. 
  
 Niekde iba niekto. 
  
 Niekde sa čaká na jar. 
  
 Niekde je smutno za snehom. 
  

