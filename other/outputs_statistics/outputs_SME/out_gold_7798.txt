

 Pršalo, nakúpil som drobnosti v obchode za cestou. Z ovocia dva kusy. Dve hrušky, dve jablká. A okrúhle piškóty, dve balenia. Aj ponožky, hoci nemali také bez gumy. 
 Jej druhý syn postavil pre svoju rodinu dom, čakajú ďalšie dieťa. Pani vyrátala počet a mená vnúčeniec na ruke, každému prstu dala meno. Privretím očí si každé dieťa predstavila. 
 Vyložil som nákup pre dve ženy, v nemocničnej izbe. Proviant rozdelil na stolíky vedľa postelí. Nebolo toho veľa, plastové fľaše som položil na zem. Pani rozprávala zasnene o vnúčikovi. O tom, ktorý ešte nemá meno. Ešte nevedia čo to bude, či chlapec alebo dievča. Potom zopla ruky. 
 Najmladší, tretí syn si kúpil nové auto. Obrovské, vraj tmavej farby, no nevie akej značky, nevyzná sa. Pred seba na paplón, mi vykladala peniaze za nákup. Pár mincí štrngalo o seba, kým som jedny ponožky rozbaľoval. Mama si ich chcela vyskúšať. 
 Prehrýzol som zubami niť aj na druhých ponožkách, pre tú pani. Dotkla sa mojej ruky, otočil som sa smerom k nej. Do dlane mi vsypala mince a zavrela. Z druhej mi zobrala ponožky a vložila lístok. Zoznam vecí, napísaný veľkým písmom slabých očí. 
 „Poprosím vás. Vraj prídete aj vo štvrtok..."  Povedala smutne, v očiach sivý odtieň oblohy za oknom. 
   

