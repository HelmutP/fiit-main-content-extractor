

   
 Ponaučenie je pre nás všetkých iba jedno. Dávajte si veľký pozor, na kom necháte oči. Ak sa aj stane, že vám padne do oka sličná slečna, v prvom rade je potrebné zlustrovať jej rodokmeň. Ak bol nejaký jej blízky príbuzný niekedy v živote trestaný, treba ňou verejne opovrhnúť. Ak sa napriek tomu ujmete tejto zblúdilej duše, rozhodne by ste si to mali písať do všetkých životopisov. Je to dôležitý fakt a hovorí veľa o vašom morálnom kredite a vašich schopnostiach. 
 Je úplne jedno, že ste nemali možnosť ovplyvniť minulosť, niekomu sa tieto fakty o vašej osobe budú zdať relevantné. Teda pod podmienkou, že žijete na Slovensku. Lebo tu sa neschopáci môžu brániť jedine podpásovými ťahmi. Myslia, že svoju neschopnosť tým zamaskujú. Bohužiaľ pre nás všetkých, táto ich neschopnosť sa nedá zamaskovať ničím. Kričí z každého kúta tejto republiky, nech pracujete v ktoromkoľvek odvetví. 
 Zdochýňajúca kobyla kope. Kope primitívne, lebo došli argumenty. Tak ako včera pani ministerke práce za okrúhlym stolom. Mala som za hulvátov iba poniektorých mužov v politike, ale keď som ju počula preafektovaným tónom „argumentovať" dvom vysoko vzdelaným a dôstojným dámam formou: „Ste hlúpe! Ste neskutočne hlúpe! ....odpustite mi, ale je to tak!" ,  vtedy som sa absolútne presvedčila o tom, že tento klan už nemá na viac, iba na to, aby osočoval a intrigoval. 
 Preč s nimi. A čo najskôr. 
   

