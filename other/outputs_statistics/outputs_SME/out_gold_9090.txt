

   
 Nechcem nikoho sklamať, ale ja som doučovala už na základnej škole slabších spolužiakov. Dokonca boli aj takí, čo dávali súkromné lekcie, a to ešte za komunistov. Štatút súkromných učiteľov je o dosť starší, ako sa na prvý pohľad zdá. Kto dnes vlastne doučovanie vyhľadáva? 
   
 O doučovanie majú záujem dospelí aj deti, lepšie povedané rodičia detí. 
   
 Deti by sa dali rozdeliť do dvoch základných skupín: tie, ktorým to z nejakého dôvodu nejde, a tie, ktoré sú jednoducho prehnane lenivé. Niekedy je to kombinácia obidvoch faktorov. Ak miera lenivosti presahuje u dieťaťa určitú hranicu, je dobré prekonzultovať tento úkaz s rodičmi, aby nečakali zázraky. Motivovať prirodzene lenivého človeka k výkonom je úloha náročná, ak nie priam nemožná. Ak sa sám nerozhodne niečo so sebou robiť a tým aj zmeniť svoj prístup, neporadíte si. 
   
 Ak dieťa v škole neprospieva, môže to mať niekoľko príčin. Často ňou býva učiteľ, čo neznamená, že teraz musíte prekladať dieťa zo školy do školy, lebo za jeho známky môžu učitelia.  Jednoducho si nepadli so žiakom do oka, nerozumejú si, dieťa si začne vytvárať negatívny postoj k predmetu,  na čo sa môže nabaliť množstvo ďalších nedorozumení, ak sa veci neriešia včas. 
   
 Druhým dôvodom býva nezvládanie učiva, pretože dieťa jednoducho na to nemá predpoklady.  V takom prípade je cieľom doučovania zistiť, prečo to dieťaťu nejde, a zvoliť postup ako mu pomôcť zlepšiť sa. Deti s poruchami učenia potrebujú dokonca individuálny prístup a študijný plán, aby v škole zvládli, čo sa od nich očakáva, a aby svoje vedomosti aj vedeli vôbec použiť v praxi. 
   
 Dospeláci majú tiež svoje problémy. Študujú diaľkovo a aktívna znalosť angličtiny sa veľmi ťažko získava z materálov natlačených v škole pre tento účel. Viacerí absolvovali už niekoľko kurzov, ale očakávaný výsledok sa doteraz nedostavil, niektorí po viacerých rokoch zotrvávajú na pozícii začiatočníkov.  Prípadne si hľadajú zaujímavejšiu prácu, kde je nutná znalosť cudzieho jazyka, a potrebujú sa dostať do formy, pretože angličtinu dlhšie nepoužívali. 
   
 A možno sa budú niektorí diviť, ale do vzdelávania seba a svojich detí investujú často ľudia, ktorých zárobky nepatria práve k vysokým. 

