
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stela Berecova
                                        &gt;
                Nezaradené
                     
                 vo "svetle" reflektorov 

        
            
                                    10.4.2010
            o
            18:31
                        (upravené
                10.4.2010
                o
                18:59)
                        |
            Karma článku:
                5.14
            |
            Prečítané 
            632-krát
                    
         
     
         
             

                 
                    čítam, čítaš, čítame...
                 

                 Pohodený  magazín na gauči. Sadám si. Vyložím nohy na stôl. Zaujmem tú najpohodlnejšiu polohu, okolo hŕba rôzne farebných vankúšov, vonku mračná zahalujú oblohu, sused kričí na svoje nepodarené deti. Opäť. Na titulnej strane čítam - magazín pre ženy. Obzrem sa za seba, premeriam sa v zrkadle. Žena - podľa všetkého som to aj ja. Pokochám sa sexi dievčinou na titulke, podobne ako to zvyknú praktikovať muži. Roky sledovania môjho brata, kamarátov alebo aj neznámych mužov v kaviarni. Nespúšťam z nej zrak. Po piatich minútach sa preberám z tranzu. Keby si odmyslím tú tonu šminiek, lak na vlasy, pózu, ktorú fotograf musel hľadať minimálne hodinu a následné hodiny strávené upravovaním fotky, napadne ma iba jedna vec. Na jej mieste mohla byť ktorákóľvek z nás, dokonca i dievča od susedov.  * Dnes už neplatí skutočnosť, že šaty robia človeka. * * " Make-up robí človeka,"motto dnešných  lifestylových magazínov. *  Mierne znechutená listujem o stranu ďalej. Potom o ďalších desať. Všadé samé reklamy na kozmetiku.  * Šampón, ktorý i z tých najriedkejších vlasov vykúzli dokonalú lesklú hrivu. * Super  rúž, ktorý je celosvetovo uznávaný. A podľa reklamy ho nosí v kabelke nejedna topmodelka            alebo herečka. Pri zazrení ceny sa cítim ako po údere bejzbalovou pálkou do hlavy. Pretrem si oči. Nie, nie je to sen. Skutočnosť, ktorá navádza mladé slečny a dámy k zakúpenie tejto maličkosti. Budete s ním okúzlujúca a chlapi na vás budú čakať v kilometrovom zástupe. Prosím, ukážte mi aspoň jedného chlapa, ktorý  chce  cítiť na svojich perách počas bozkávanie niečo tak lepkavé ? *  Nájdete tu reklamy na všetko  možné. Od laku na vlasy, parfúmu cez intímky až po špirály alebo briliantový prsteň, pri ktorom máte pocit, že sa jeho lesk odráža vo vašich očiach.  Prečítam si zopár článkov. Prečo pravidelne cvičiť. Pár trikov ako predísť jarnej únave. Tipy ako stráviť piatkový večer v pohodlí. Takýmto článkom je venovaná jedna maximálne dve strany a nehovoriac o tom, že polovičku zaberajú obrázky, aby nám vysvetlili ako na to. Isto si myslia, že bez ich názornej ukážky, by sme nepochopili princíp, ako to celé funguje. Ale veď nevadí. :) Polovička stránok za mňou . Stále som neprišla na to, v čom spočíva ich posolstvo. Tajomstvo, ktoré sa dozviem, až po prevrátení  poslednej stránky ?  Trpezlivosť nepatrí k mojim silným stránkam. Dovolím si nalistovať poslednú  dvojstránku. Jediné, čo vidím, je zoznam výhercov a obsah  ďalšieho vydania. Samozrejme, že napravo sa vyníma ďalšia reklama. Jasnačka, "peňážky" sa  zídu. :) Rozhodnutá, že na dnes už toho bolo dosť, zablúdim nečakane na stranu s veľkým nápisom. Čítam, čítam, čítam. Od toľkého zanietenia a zvedavosti mi až horia oči. :) Zopár rád ako sa (ne)strápniť na prvom rade. Hlavne buďte prirodzená. Tvrdí autor tohto článku. Súhlasím. Ďalej som sa dočítala a hlavne ponaučila, ako mám sedieť, kam majú smerovať ruky alebo nohy, zopárkrát odbehnúť na toaletu a to len z toho dôvodu, aby si dotyčný všimol moje nohy.  Nečakane zaboriť svoju paprčku do vlasov, troška sa s nimi pohrať, pohadzovať z jednej strany na druhú, aby  muž vašich túžob  ucítil okúzlujúcu vôňu parfému, za ktorý ste utratili svinsky veľa  prachov. Áno, určite to zabere. Chlapa napadne iba to, či ste si pred rande hrkli za jeden alebo či trpíte závažnou poruchou osobnosti.  Hnus. Vraciam časák na pôvodné miesto. O niekoľko minút neskôr ho do ruky chytí mamina. Prelistuje ho rýchlosťou, ako keby frčala po dialnici. Minimálne sto kilometrov za hodinu. A to je koniec jej čítania. Vytiahne z poličky rozčítanu knihu a veselo sa ponorí do príbehu. Správne rozhodnutie.  Ak pre niekoho znamená celý život týchto zopár desiatok strán o povrchnostiach, myslím, že potom máte problém. Záver je jednoznačný. Diagnóza: egoista. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            O  nás
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            Polnočné čakanie ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            (Ne)hrám
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            zopár (ne)správnych dôvodov...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stela Berecova 
                                        
                                            Ad Revidendum 2015
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stela Berecova
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stela Berecova
            
         
        stelaberecova.blog.sme.sk (rss)
         
                                     
     
        Život nie je o tom, aby sme vždy získali len tie najlepšie veci, či dosiahli pocit víťazstva. Naučiť sa byť šťastný s tým, čo nám život ponúka, i keď toho možno na pohľad nie je veľa, je omnoho ťažšie, ako zapáčiť sa niekomu alebo pridať sa na stranu silnejšieho. :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    803
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Puzzle môjho života
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jackie Collins - Hollywoodske deti
                                     
                                                                             
                                            Paulo Coelho - Láska ( citáty )
                                     
                                                                             
                                            Terry Pratchett
                                     
                                                                             
                                            John Grishman-Klient
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Sister Hazel
                                     
                                                                             
                                            The Grease ( platňa )
                                     
                                                                             
                                            The Corrs
                                     
                                                                             
                                            Paramore
                                     
                                                                             
                                            Blue October
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Michal Hlavaty
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Kníhkupectvo Martinus
                                     
                                                                             
                                            Labello
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




