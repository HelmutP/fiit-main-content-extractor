

 „Chcela by si ísť so mnou, Brunom, jeho frajerkou a ešte Lennym na chatu? Len na víkend," spýta sa ma Rišo keď cestujeme v autobuse smer ku mne domov. 
 „Kam?" hovorím a jedným okom pozorujem chalana, ktorý sa nenormálne podobá na môjho ex. 
 „Do Stupavy, to je tu kúsok od Blavy." 
 „Viem." Do riti tak je to on, či nie? 
 „Len na víkend," zopakuje. 
 „To si už povedal." Tetka, ktorá mi zacláňa vo výhľade sa nakloní dopredu, aby si zo zeme zdvihla dve igelitky. Vtom zazriem jeho tvár, ústa, oči, všetko. Je to on!!! Kriste! 
 „Šli by sme mojim autom." 
 „Mhm," pozriem na Riša. Ľavou rukou je zavesený na autobusovej tyči a tvári sa, že tak blízko pri mne musí byť preto, lebo inak by sa sklátil k zemi. 
 „Pobozkaj ma," vravím mu zrazu. 
 „Čo?" kuká na mňa ako na zjavenie. 
 „Toto," poviem, pritiahnem si ho bližšie a pobozkám ho. Chvíľu akoby nevedel čo sa deje. Potom sa zapojí, dokonca ucítim jeho jazyk ... odtiahnem sa. Znova pozriem na môjho ex. Rišo je potom asi dvadsať sekúnd ticho a potom sa spýta s úsmevom na tvári: „Čo to malo znamenať?" 
 Kútiky pier dvihnem trochu vyššie a lišiacky mu poviem: „Len som chcela čosi skúsiť." 
 „Aha," hovorí. Jeho ďalšiu vetu nevnímam, pretože môj bývalý sa náhle pozberá k dverám, prejde okolo mňa, hodí na mňa pohľad a zmizne v tme vonku. 
 Hmm, tak predsa to nebol on. 
   
 Štyri dni na to sa spolu vezieme do Stupavy na chatu, na ktorej som v živote nebola, s ľuďmi, ktorých poriadne ani nepoznám, ale keďže som nemala nič lepšie na programe, považujem tento fakt za celkom pozitívny. 
 „Lenny je trochu divný," konštatuje Rišo sledujúc červenú na semafóre. 
 „Akože v čom?" 
 „Má oplzlé poznámky, takže sa pokús nevšímať si to," hovorí mi akoby bol môj veľký brat. 
 „To je v pohode. S takými si dokážem poradiť." Vyzlečiem si sveter a hodím ho na zadné sedadlo. 
 „Je ti teplo?" pýta sa a prstami okamžite tlmí vyhrievanie. 
 „V pohode, nechaj to tak." 
 „Oukej," vyhrievanie znova púšťa. 
 Naskočí zelená. 
   
 Chata je to pomerne malá. Zvonku odhadujem, že sú tam tak dve izby, kuchyňa a dúfam, že aj kúpeľňa. Rišove auto zaflekuje vedľa nej. Natiahnem sa dozadu po sveter, no keď vidím, že on sa akosi nemá k činu, zahľadím sa na neho. 
 „Táni?" začína opatrne. To neveští nič dobré. 
 „Uhuh?" 
 „Viem, že si o tom našom ... bozku ... nechcela hovoriť...." 
 Zjavne chcel pokračovať, ale preruším ho. „Hej, nechcela." 
 Prikývne. Akože chápajúco. „Len som chcel vedieť, či ..." 
 „Prosím ťa, Riško, nechajme to teraz tak, dobre?" Zľahka sa mu dotknem brady. „Posledné čo teraz potrebujem je niekam sa ponáhľať." 
 Nič nehovorí. 
 „Dobre?" pýtam sa. V tvári ľútosť. 
 „Dobre." 
 Uff. 
   
 „Čo ste tam vonku robili? Som myslel, že sa ani nevytrepete z toho auta," rehlí sa zrejme Lenny. Zatiaľ mi nebol predstavený. 
 „Súložili sme," odpoviem. 
 Rehot. 
 Rišo sa pritmolí ku mne. „No tak toto je Lenny." 
 „Ty sa nezdáš, chlape," potľapká Riša po pleci a do ruky mu strčí fľašu borovičky. Rišo ju prijme, no neodpije si. Chvíľu na nás zíza. 
 „Neprejdeme dnu?" pozriem na neho. 
 „A ty sa mi ani nepredstavíš?" vraví mi Lenny. 
 „Táňa." 
 „Hmm," poškrabe sa vo vlasoch. „Raz som mal jednu Táňu," skúmavo na mňa hľadí. „Ale to si nebola ty," rehoce sa. 
 „Bola malá, tlstá a škaredá?" pýtam sa nemastne-neslane. 
 Svoj ukazovák namieri na mňa. „Ty musíš byť riadna dračica." 
 Rišo tam pri nás bez slova stojí sledujúc našu scénku. Chytím ho za ruku a poviem „Poď." 
 Prejdeme do veľkej izby s dvomi gaučami, jedným stolom, telkou a pár skriňami. Na stene visí obraz, ktorému celkom nerozumiem. V rohu na podlahe je rádio, repárky sú namierené do priestoru. 
 „Čaute, ľudkovia," pristúpi k nám Bruno. V rukách drží taniere s chlebíčkami. Položí ich na stolík. Obaja odzdravíme. Do miestnosti za potom vrúti drobná babenka s dvomi pitlami pukancov, ktoré končia pri chlebíčkoch. Bruno ju okamžite chytá okolo pliec. „Zlatko, toto je Rišo a jeho ...," zasekne sa. 
 „Kamoška," poviem. 
 „Kamoška. Táňa, že?" 
 Prikývnem: „Správne." 
 Podáme si ruky. 
 „Valika," predstaví sa drobné žieňa. „Sadnite si u nás. Čo budete piť?" 
 „Čo je v ponuke?" 
 „Asi desať litrov alkoholu," zakričí spoza nás Lenny, potom sa k nám prirúti, zvalí sa na gauč vedľa mňa a zapne telku. 
 „Je tam víno, vodka, borovička, potom kola a džús," vymenuje mi Valika. 
 „Tak ja si dám víno s kolou." 
 „A ty, Rišo?" 
 „Ja nič." 
 „Jak, že nič?" 
 „Čosi si daj," vravím mu. 
 „Tak tú borovičku." 
 „Vypni to," zvolá Valika na Lennyho. 
 „Fááájn," vzdáva sa. 
   
 Ten večer to bola nuda. Chvíľu sme kecali, potom nám došli témy, tak sme si pustili nejaký film. Vypila som toho dosť veľa na to, aby som bola v nálade a ešte aj čosi naviac. To naviac ma však skôr utlmilo ako prebralo. Valika bola zlatá a stále sa snažila, aby sme sa zabávali a dokonca jej to zo začiatku aj šlo, ale potom ju začal viac zaujímať Bruno ako my. Asi o pol dvanástej sa vyhovorili na to, že sú unavení a zmizli v izbe na prvom poschodí. Ani som sa nečudovala. Po toľkom alkohole by som sa na svojho frajera vrhla aj keby to nechcel. 
 „Je ti dobre?" spýtal sa ma po polnoci Rišo. „Nechceš už spať?" 
 Boli sme zvalení na gauči. Vnímala som všetko tak na polovicu. „Je mi fajn," usmiala som sa. 
 Lenny chrápal na koberci pri stolíku. 
 „To som rád," povedal mi a hľadel na mňa ako na svätý obrázok. Dostalo ma to. Zrazu som mala strašnú chuť byť pri ňom bližšie. 
 „Riško?" nadvihnem jedno obočie a pomaly sa k nemu posúvam. 
 „Mhm," zamrmle. 
 „Prečo si na mňa taký dobrý?" pýtam sa opito. 
 Ticho. 
 Pritúlim sa k nemu. 
 Objíme ma. 
 Silno. 
 A potom mi zašepká do vlasov: „Lebo ťa mám rád." 
   
 „Povedz mi čosi o sebe." S Rišom sedíme v malom zafajčenom podniku kdesi na okraji mesta. Týždeň po našej chate. 
 „Načo?" 
 „Len tak," pokrčí ramenami. „Zaujíma ma to." 
 „Aha." 
 „Minule som rozmýšľal o tom, že sa poznáme už takmer pol roka a skoro nič o sebe nevieme." 
 „Tak sa pýtaj čo chceš vedieť," poviem mu a z úst vydýchnem cigaretový dym. 
 „Prečo o sebe nerada rozprávaš?" usmeje sa. Zrejme aby to trochu odľahčil. 
 „Nie je o čom." 
 „Ja myslím, že je," žmurkne na mňa. 
 „Tak fajn," znova si potiahnem. Po tom ako vydýchnem, začnem: „Narodila som sa jeden krásny letný deň, slniečko svietilo, voda žblnkotala, všetko vyzeralo úžasne. Žila som si svoj nádherný život ..." Zasmejem sa. 
 „A ďalej?" 
 „Potom som dostala prvú facku a pochopila som, že nič nie je také úžasné ako vyzerá." Do pľúc si znova vpúšťam nikotínový opar. 
 „Čo sa stalo?" 
 „Opustil nás foter." 
 So súcitom na mňa hľadí. 
 „Ale to je jedno. To už bolo dávno," vravím a cigaretu zadusím v popolníku. Potom ho sledujem ako sa nervózne pohráva s lyžičkou v jeho čaji. 
 „Mala si niekedy vážny vzťah?" spýta sa zrazu. 
 Zasmejem sa. „Mala som veľa vzťahov." 
 Pohľadom prejde zo stola na mňa a zabodne sa mi priamo do očí. „Čo to znamená?" 
 „Presne to čo som povedala." 
 „A boli vážne?" 
 „Dva z nich hej." 
 „Mhm," prikývne. Rozmýšľam, či sa ma spýta na podrobnosti alebo to radšej nechá tak. 
 „Prečo ani jeden nevyšiel?" 
 „Lebo som si vždy nakoniec uvedomila, že nechcem byť niekoho handra. Jednoducho nemám na chlapov šťastie." 
 „To to bolo až také zlé?" Znova ten súcit. 
 „Horšie," zarehlím sa. 
 Chvíľu je ticho akoby zvažoval či sa v tom chce rýpať ďalej. 
 „A veríš ešte na lásku?" 
 Rehlím sa. „To čo je za otázka?" 
 Pomrví sa na stoličke. „Neviem, niekedy mám dojem, že ... že cúvaš alebo čo." 
 Ústa skrivím do čohosi čudného: „Hmm, nemám ten pocit." 
 Odpije si koly. 
 „Jednoducho už nemienim znova trpieť, toť vše." 
 „Nie všetci chlapi sú takí, že by si pri nich trpela." 
 „Tak takých si ja už nezaslúžim." 
 Nechápavo na mňa pozrie. 
 „Možno kedysi ... ale teraz už nie," dodám ticho. 
   
 „Riško?" šveholím do telefónu. 
 „Áno?" 
 „Zlatíčko, nechce sa ti ísť dnes na diskotéku?" pýtam sa ho sediac pri počítači. Už ma to doma nebaví. Matka chytila nejaký hysterický záchvat. Je tu na mňa moc dusno. 
 „Ja neviem," počujem ho z druhej strany linky. 
 „Ale no tááák, čo budeš robiť doma? Je piatok večer, treba vyraziť do ulíc." Do kelu, tak poď!!! Nudím sa!! 
 „Tak fajn, ale asi nebudeme moc dlho, dobre?" 
 „Okej, ako myslíš." Haha, tak to by sme mali. 
   
 „Si si istá, že tu chceš ostať?" oči má vypleštené. Neviem či z toho, že je taký prekvapený, že sa mi páči v takom pajzli alebo preto, lebo na mňa cez ten dym nevidí. 
 „Hej, hej, je tu fajn. Dobre tu hrajú," vravím čakajúc v rade do šatne. 
 „Objednám ti zatiaľ čosi piť, hej?" 
 „Fajn, daj mi bundu, odložím ti ju sem." 
 „Okej." 
 Rad sa posúva čím ďalej, tým pomalšie. Zozadu sa na mňa lepí nejaký týpek. 
 „Daj si pohov," otočím sa na neho. Lenny! 
 „Aha ho, naša dračica," zvolá na mňa. 
 Kriste, už len tento mi tu chýbal. 
 „Čo ty tu?" pýta sa ma. 
 „Prišla som sa zabaviť." 
 „A si tu sama?" obzrie sa okolo mňa. 
 „Nie, Rišo mi šiel kúpiť čosi na pitie." 
 „Aha, fajn, tak potom dojdite za nami. Som tu s takou partou." 
 „Jasné." Už ma vidíš. 
 Dostávam sa k babenke šatniarke. 
 „Tak sa zatim zabav," zakričí mi Lenny do ucha a zmizne v dave. 
   
 Asi o jednej sa rozprúdi tá pravá zábava. Rišo sa tvári, že sa baví, ale myslím, že by najradšej zdúchol. Je mi to jedno. Spokojne si tancujem obklopená tromi chalanmi. Jeden z nich vyzerá celkom schopne. Dokonca sa vie aj hýbať. Potom to trochu preženie a keď sa mi snaží vopchať ruku pod tričko, rázne mu dám najavo, nech ide do prdele. 
 „Čo tu tak stojíš?" pritancujem k Rišovi pri stole a odpijem si vodky. 
 „Nemám dnes náladu." 
 „Ale no tááák," zošpúlim pery a ťahám ho za obe ruky na parket. 
 Celkom sa chytí. Krútim sa okolo neho, nechám ho prepliesť nám naše prsty, chytať ma za boky. Smejem sa. O chvíľu sa však tempo hudby spomalí. DJ zakričí čosi, že tu máme pár, ktorý sa práve zasnúbil, takže pustí nejaký slaďák. Rišo sa ku mne bez slova priblíži, chytí ma za pás a privinie si ma k sebe akoby bol liana. 
 „Je skvelé mať ťa tak blízko," povie ticho. Ledva som mu rozumela. 
 Nič nehovorím. Nič schopné mi totiž nenapadá. 
 „Som rád, že si ma nakoniec vytiahla von." 
 „Aj ja," hovorím a hlavu si opriem o jeho rameno. 
   
 „Už musím ísť," pozrie na mňa asi o pol druhej s previnilým výrazom. 
 „Ale mne sa ešte nechce," namietam a znova si hltnem z vodky. Svet okolo mňa sa začína točiť. 
 „Zajtra ráno musím skoro vstať, pretože musím odviezť segru na tréning." 
 „Ale veď to zvládaš, ešte takú hodinku," nalepím sa na neho. 
 „Fakt nie, Táni. Musím." 
 Odstrčím ho. Naštvane. „Tak fajn, ja tu zostávam." 
 Vyvalí na mňa oči. „Čo? A s kým?" 
 „S nikým, len tak." 
 „A ako pôjdeš domov?" 
 „Taxíkom." 
 „Nechcem ťa tu nechať samú." 
 „Tak zostaň," znova sa na neho usmievam. 
 „Nemôžem." 
 „Okej, tak čau," odvrknem a dám sa na odchod. Ešte ma však stihne zastaviť. 
 „Prepáč. Zajtra sa ti ozvem, dobre?" 
 „Ako chceš." 
 Do riti, čo teraz? Poobzerám sa okolo seba. Pohľad mi padne na chalana v čiernej košeli. 
 „Čauko, tak som za tebou došla," usmievam sa od ucha k uchu na Lennyho. 
 „A kde máš Richarda?" spýta sa ma. 
 „Odišiel. Nechal ma tu samu samučičkú." 
 „No nie je to kokot?" žmurkne na mňa. „Chceš čosi na pitie?" 
 Myknem plecom. „Vodku s džúsom." 
 „Fajn." 
 Prejdeme k baru a kým on objednáva, ja si uvedomím, že ak dám do seba ešte jeden pohárik vodky prestanem sa ovládať. 
 „Prečo tá prezývka? Že Lenny?" pýtam sa ho. 
 „Podľa Kravitza." 
 „Tvoj obľúbený spevák?" 
 „Tak tak." 
 Zaplatí barmanovi za dva drinky, jeden mi podá. 
 „Tak na túto noc," zvolá. 
 „Na nás," štrngnem si s ním a vodku vlejem do hrdla. 
   
 „Čo robí kočka ako si ty s takým trapkom ako je Rišo?" pýta sa ma asi po hodke tancovania, vlnenia, krútenia, flirtovania a neviem čoho všetkého, keď stojíme vonku a dýchame čerstvý vzduch. 
 Zasmejem sa. „Rišo je celkom fajn." 
 „Som si na sto percent istý, že to s babami vôbec nevie." 
 „A ty to akože vieš?" kuknem naňho. Opito. Po tých piatich vodkách vyzerá fakt k svetu. 
 Podíde ku mne bližšie. „Ja som v tom majster." 
 Zarehlím sa. „Neveríííím." 
 Tentokrát sa zasmeje aj on, chytí ma za bradu a takmer sa mi dotkne pier. „Dokážem ti," zašepká. Potom ma pobozká, najprv bez jazyka, čo vcelku oceňujem, potom mi struhne riadny francuzák. Rukou mi šmátra pod tričkom, oprie ma o stenu. Keď ma pobozká na krk, zavzdychám. 
 „Ale, ale," kukne na mňa a začne mi rozopínať gombíky na rifliach. 
 „To to chceš robiť tu?" kuknem naňho so smiechom. 
 „A chceš ísť inde?" na chvíľu so všetkým prestáva. 
 „Kde bývaš?" 
 „Dosť ďaleko," zarehlí sa. 
 „Shit." 
 „Ty musíš byť fakt nadržaná," kukne na mňa. 
 „No lebo ty nie si," poviem podráždene. 
 „Tak poďme k tebe," znova sa ku mne nakloní. Odvrátim tvár. 
 „Už si si to rozmyslela?" prižmúri oči. 
 Neviem, či to bolo tou vodkou, či ním, alebo tým, že som sa už fakt dlho s nikým nevyspala, dostala som zo seba len: „Poďme aspoň trochu ďalej." A tak sme si vystačili s lavičkou zastrčenou za pár stromami asi 50 metrov od hlavného vchodu do klubu. 
   
 „Táni?" 
 „Áno?" 
 „Je mi s tebou dobre," povie mi Rišo, pozrie na mňa a chytí moju ruku do svojej dlane. 
 „Mhm," prikývnem. 
 Sme na jeho izbe na intráku, sediac pri sebe na jeho posteli, presne tak ako toľkokrát predtým. Pomaly sa ku mne nakloní a v jeho očiach zbadám úmysel pobozkať ma. Odtiahnem sa. Hľadím do vankúša. 
 „Čo sa stalo?" pýta sa. 
 „Nič," poviem neprítomne. 
 „Aha, tak prepáč." Moja ruka je zrazu sama. 
 „Nemám čo." 
 Pokrúti hlavou, hrabne si do vlasov. „Ja ti nerozumiem Táni." 
 „Mhm." 
 „Myslel som, že je nám spolu fajn." 
 „No veď je," hovorím. 
 „Tak v čom je problém? Alebo ty nechceš viac? Včera keď sme boli vonku sa mi zdalo, že hej." 
 „Ja neviem." 
 „Čo nevieš?" 
 „Či chcem viac." 
 Včera som sa vyspala s Lennym! 
 Vstane a spraví pár neurčitých pohybov akoby nevedel čo so sebou. Potom sa na mňa zasa obráti. „A to je všetko?" 
 „Nemám k tomu čo dodať." 
 Včera som sa vyspala s Lennym!!!!!! 
 Znova si sadne. „Tak načo sa správaš tak, akoby si odo mňa čosi chcela? Píšeš mi smsky, voláš ma von. Potom sa mi týždeň neozveš. Kto sa v tom má vyznať?" 
 „Neviem." 
 „Sa počúvaš?" 
 Prosím ťa, daj mi pokoj. 
 „Kde je teda problém?" 
 „Jednoducho ja už neverím vzťahom." 
 Vo mne je problém. 
 Na čele sa mu zjaví vráska. „Čo?" 
 „Že neverím vzťahom." 
 „Prečo?" 
 „Neviem." 
 „Kravina. Musíš vedieť." 
 „Moje srdce bolo zlomené už toľkokrát, že by ho nedal dokopy ani ten najlepší chirurg," vravím s výrazom podobným úsmevu. 
 Ticho. 
 „Takže teraz čo?" 
 „Asi nič." 
 Ticho. Premýšľa. Vidím to na ňom. 
 „Ja som sa do teba asi zamiloval." 
 Zasmejem sa. To som ale zrejme nemala robiť. 
 „Tebe je to smiešne?" 
 „Celá táto situácia je mi smiešna," vravím. 
 „Ako môžeš byť taká chladná?" opýta sa, ale myslím, že je to rečnícka otázka. 
 „Mala by som asi ísť," vstanem. 
 „Tak mi vysvetli prečo si sa na mňa včera na tej diskotéke tak lepila?" pýta sa zrazu. 
 „Čo ja viem. Bola som opitá, ja ani neviem čo som robila." 
 „Aha," hovorí s výrazom raneného srnca. 
 „Takže ty si nikdy ani len nepomyslela na to, že by sme mohli byť spolu?" 
 „Asi ani nie." 
 „Asi? Ty si fakt dobrá." Irónia by sa dala krájať. 
 Nemám čo povedať. Beriem si veci zo stoličky. 
 „A minulý týždeň v piatok si mi prečo vravela, že som úžasný chalan a poklad a že si rada, že sme sa spoznali a podobné kraviny?" zrejme to nechce tak rýchlo vzdať. 
 „Tak lebo je to pravda. Ja som naozaj rada, že sme sa spoznali." 
 „A prečo si myslíš, že aj náš vzťah by skrachoval?" 
 „Proste na vzťahy teraz nemám náladu. To je jedno či s tebou, či s niekým iným." 
 „Aha, tak si mi nemala dávať nádej." 
 Znova sa zasmejem. Znova nevhodná chvíľa. „Ak som ti ju dávala, tak sorry." 
 Zrejme ho to vytočilo. „Tak ja neviem akoby si nazvala to túlenie sa ku mne, keď sme boli na chate." 
 „Ježiš, toto nikam nevedie," už som vytočená aj ja. 
 „Asi nie," povie smutne. Zrazu mi je ho ľúto. 
 „Pozri, človek rád vidí veci tam kde nie sú." 
 „Fajn, to si radšej zapamätám, aby som už zo seba nabudúce nerobil debila." 
 „Okej." 
 „Takže to je fakt všetko?" 
 „Asi hej." 
 Vidím ako preglgne. 
 Asi slza. 
 Smrknem. 
 Asi nádcha. 
 „Tebe je to jedno, že sa už zrejme nikdy neuvidíme?" 
 „Asi to tak bude lepšie." 
 Ja už ani neviem, či je mi to jedno, či nie je. 
 „Fajn." 
 Odídem. Prejdem asi dvadsať metrov, keď mi pípne smska. 
 „Ak si sa vo vzťahoch vždy správala takto, tak sa ani nečudujem, že ti všetky skrachovali." 
 Prečítam si to dvakrát, potom stlačím VYMAZAŤ. 
   
 A keď prídem domov, uvedomím si presne dve veci: že Rišo si toto nezaslúžil a že zo mňa sa stala kreatúra podobná mojim bývalým. 
   
 A potom sa zo seba prvýkrát v živote povraciam. 
   
   

