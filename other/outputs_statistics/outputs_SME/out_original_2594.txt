
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Leopold Böttcher
                                        &gt;
                Súkromné
                     
                 Keď dávate peniaze žobrákovi, dávajte ich jemu 

        
            
                                    11.3.2010
            o
            17:20
                        (upravené
                16.3.2010
                o
                16:28)
                        |
            Karma článku:
                4.01
            |
            Prečítané 
            933-krát
                    
         
     
         
             

                 
                    Spomenul som si na text piesne, ktorú spieva jeden pražský český Žid po rusky: - ...neverju nikamu, toľka Bogu adnamu.
                 

                   
 Ja som raz dal asi päťdesiatročnému žobrákovi 500 Sk v bývalej mene. Totiž, jedol som langoš so smotanou a strúhanými syrom, ktorý som si kúpil v bufete na Mlynských Nivách a nechcel som byť vyrušovaný.    V očiach mal slzy a rozprával fistulickým hlasom o sebe, ako mu je ťažko. Vyzeral ako muž, ktorý len teraz nedávno, možno pred týždňom, prišiel o strechu nad hlavou. Mal som pri sebe len túto bankovku, tak som mu ju podal so slovami: - Nech sa páči. V srdci som nepociťoval takmer nič, jednoducho som vytiahol bankovku z peňaženky a odovzdal mu ju.   Mal som veľký hlad a na langoš som sa tešil už pred dvomi hodinami, ako si ho preložím na polovicu, akú chuť budem citiť, a ako hladko sa to dobré jedlo bude presúvať do môjho brucha. V podstate intimita, ktorú ste plánovali a nastavili ste sa na ňu.     
 Začal mi ďakovať a vtedy som videl, že som ho dojal a plače. Hrudník sa mu chvel, aj pery. A stále ďakoval, akoby sa pritom začal meniť na akýsi lepkavý mazľavý hmyz.   Vzdialil som sa, ale hlavou mi vírila neodbytná myšlienka, že som toho človeka ponížil.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Leopold Böttcher 
                                        
                                            Juliánovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Leopold Böttcher 
                                        
                                            Spokojnosť so sebou samým
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Leopold Böttcher 
                                        
                                            Nádej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Leopold Böttcher 
                                        
                                            Samouspokojovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Leopold Böttcher 
                                        
                                            Mal by som ešte raz preskúmať tie pretŕčajúce veci z tej zmiešaniny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Leopold Böttcher
                        
                     
            
                     
                         Ďalšie články z rubriky próza 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        David Stojaspal 
                                        
                                            Dukelské zápisky 1944-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Igor Čonka 
                                        
                                            Ellis Parker Butler - Prasce sú prasce I
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Kto nezažil, neuverí. . . (46)
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Oni
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Odrazový mostík
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky próza
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Leopold Böttcher
            
         
        leopoldbottcher.blog.sme.sk (rss)
         
                                     
     
        Kto sa náhle postaví, pretože je zvedavý, čo sa odohráva za oknom v záhrade.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    419
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Teraz
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Julian Barnes: Žádný duvod k obavám
                                     
                                                                             
                                            Haruki Murakami: Sputnik, má láska
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




