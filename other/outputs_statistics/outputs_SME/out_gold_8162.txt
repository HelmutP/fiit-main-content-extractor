

 Znel v žiare krásnych príbehov 
 čo hral v tiaži rúch a líčidiel. 
 Tichu čiernych rád, tým tváram skrytým v prítmí, 
 za dotyky rúk seba odovzdal.     
   
 Sám hriešnym tieňom nároží, 
 mačkám, lúčom hviezd a čierni striech, 
 potom príbeh svoj vždy cestou domov hrával 
 za bzučanie múch v teple nočných lámp. 
   
 Bdel, dúfal v ráno v páperí. 
 Vravel smútku kníh a vôni stien, 
 drevu, záclonám, že hráva dvakrát príbeh  
 za bzučanie múch v teple nočných lámp. 

