

     
 Netreba ju teda inštalovať. Táto aplikácia prekonvertuje video súbor na zvukový súbor vo formáte mp3, a umožní vám tento súbor stiahnuť. Všetko čo potrebujete je link na video (URL). 
 Ako príklad stiahneme záznam koncertu skupiny Hudba z Marsu. Rovnako si však možete stiahnuť aj prejav Roberta Fica, a púšťať si ho na dobrú noc. 
 Takže si skopírujte link z aplikácie YouTube  a vložte ho na stránku 
 http://www.video2mp3.net 
 V  prípade, že sťahujete marťanskú hudbu, to bude vyzerať takto: 
  
  
   
 Potom už len stačí stlačiť “Convert”. Na obrazovke sa percentuálne zobrazuje priebeh konvertovania. Po úspešnom skonvertovaní, sa vám zobrazí odkaz “Download mp3” a zvukový súbor (v tomto prípade pesničku) si môžete stiahnuť do počítača. 
 Priame sťahovanie v prehliadači Mozilla 
 Na konvertovanie a sťahovanie video súborov z Youtube vo formáte mp3 môžete použiť aj prídavný modul (addon) pre prehliadač Mozilla. V tomto prípade, vám link na stiahnutie zobrazí prehliadač priamo: 
   
  
   
 Pomocou tohto modulu môžete sťahovať zvuk zo stránok: Youtube, MyVideo, Clipfish, Sevenload, Dailymotion, MySpace. 
 Sťahovania videa z Youtube v Mozille 
 Na sťahovanie videí z YouTube jedným klikom vo vybranom formáte (FLV, 3GP, MP3, MP4) odporúčam použiť prídavný modul Easiest Youtube Video Downloader 3.1. Po nainštalovaní sa vám priamo na stránke YouTube  zobrazí link na stiahnutie videa: 
   
  

