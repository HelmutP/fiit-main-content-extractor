

 Uff. No teda. Dočerta. Šľak aby to trafil tisíc hrmených. Kde začať. Dobre. Od vajíčka. 

 
 Film LÁSKA V ČASE CHOLERY vznikol podľa románu „Láska v čase cholery“. Napísal ho cenený Gabriel García Márquez, ktorý získal Nobelovu cenu za literatúru a ktorého knižkami sú preplnené i slovenské knižnice. Našťastie. Zarážajúce je, že hoci sú to často skôr novelky, z nich niektoré nemajú snáď ani dvesto strán, čítajú sa mi hrozne ťažko až náročne a s niektorými som to dokonca vzdal po tridsiatich stranách. U iných som pretrpel prvých 150 strán a posledných 50 už „išlo“. Moje skúsenosti s tvorbou tohto ceneného „juhoamerického“ spisovateľa sú, ako sami vidíte, komplikované. Nedovolím si tvrdiť, že zlé a dokonca si nedovolím tvrdiť ani že je nudný. To by bolo hlúpe, krátkozraké, detinské, zjednodušujúce a trápne. 

 Hlavne by to ale bolo neférové, pretože nech sa mi tie knižky čítajú akokoľvek ťažko, rozhodne uznávam, že MAJÚ jedinečnú atmosféru „magického realizmu“, že dialógy MAJÚ hlavu a pätu a že medzi postavami často vládnu POZORUHODNE ORIGINÁLNE vzťahy. Márquez je krutý a zároveň rozprávkový umelec. Dokáže v jednej vete šokovať a v druhej dojať. Nič to ale nemení na tom, že ak sa mám začítať do niektorej z jeho knižiek, nie je mi väčšinou ľahko pri srdci a musím sa pred začítaním sám so sebou zmieriť s tým, že dvestostranovú knižku budem čítať dva mesiace. „Lásku v čase cholery“ som však poctivo pretrpel (nehnevajte sa ale inak sa to napísať nedá - ročne prečítam niekoľko desiatok knižiek, ale na Márqueza som jednoducho prikrátky, rád to priznávam) a preto som sa, až zvrátene, tešil na jej filmovú adaptáciu. 

 Bohužiaľ sa jej v roku 2007 chopili Američania. Milujem americkú kinematografiu a určite si nemyslím, že stomiliónový americký veľkofilm musí byť hlúpy, že horor je podradný žáner a že americký národ by mal vymrieť. Cítim potrebu tieto veci povedať nahlas, aby ma nebodaj niekto nevinil z arogancie, namyslenosti, škrobenosti a vyžívaní sa v klubových filmoch (nič proti nim, niektoré mám ozaj rád, ale žiadny z nich mi neurobí takú radosť, ako keď dvanásťmetrový krokodíl v newyorskej kanalizácii niekomu odhryzne hlavu)... ale tento film mi pripadal „typicky americký“ v tom najhoršom zmysle slova. Zjednodušuje, psychológiu ktosi nešikovne vymenil za opulentnú vizuálnu stránku a snímku nabúchal presne tými typmi scén, dialógov a monológov, ktoré sa snažia prízemne dojať skôr menej náročného diváka. To, čo malo a podľa mňa za určitých okolností (ktoré však v snímke nie sú) MOHLO byť strhujúce, skončilo horšie ako MANDOLÍNA KAPITÁNA CORELLIHO, kde sa fanúšikovia knižnej predlohy dodnes strhávajú z ťaživých nočných snov, v ktorých ich prenasleduje Nicolas Cage s nastrelenými vlasmi a mandolínou. 

 Pritom Brit Mike Newell nie je žiadny hlúpy režisér. Má na konte pôvabné ŠTYRI SVADBY A JEDEN POHREB (80%), napínavé KRYCIE MENO DONNIE BRASCO (80%) a magického HARRYHO POTTERA A OHNIVÚ ČAŠU (100%). Rozpočet bol evidentne obrovský a padol na prekrásny vzhľad (monumentálna výprava, naberané kostýmy, detailné dekorácie). Po vizuálnej stránke je to naozaj miestami až obdivu hodná práca stoviek skúsených remeselníkov a profesionálov, ktorá na vás najviac doľahne zrejme pri takmer fascinujúcej scéne „úteku cez hory“ (ten ma zaujal už v knižnej predlohe). Bohužiaľ, kým v detailoch sa film „dá“, ako celok nefunguje, samého ma to mrzí. Neviem, koho je to vina, či režisérova (asi nie), scenáristova (ktovie... ale získal Oscara za PIANISTU) alebo producentov (ak áno, tak najmä preto, že takú knižku vôbec chceli sfilmovať). Možno kebyže z knižky vznikne televízny seriál nakrútený „ľudom Južnej Ameriky“ bez hollywoodskeho pričinenia, by naň Márquez bol hrdý. Takto ale ťažko povedať a nad rozliatym mliekom sa plakať vraj nemá. 

 “Až“ 60% dávam pre „celkovú svojráznosť“, cez výpravu po občasné náznaky atmosféry až po bizarné (ale aspoň nenudí!) herecké obsadenie (Bardem, Bratt) s ešte kurióznejšími maskami, ktoré boli oveľa bizarnejšie, ako v obidvoch HELLBOYOCH dohromady (!!) a to sa nevyjadrujem k záverečnej erotickej scéne, ktorá v predstavách tvorcov zrejme mala byť revolučná (aspoň ja som sex v tak pokročilom veku v nepornografickom filme nevidel), ale je smiešna a dokonca nedôstojná a to hovorí človek, ktorý berie sexualitu ako neoddeliteľnú súčasť ľudského života, má sexuálne úchylky ako každý človek na Zemi a vie, že sexualita neumiera po tridsiatom roku života. Niektorí boli z filmu znechutení a písali o ňom dosť škaredé a dosť neslušné veci. Tým, ktorí knižku dobre poznali, bolo ľúto, že film dopadol tak ako dopadol a hovorili slušnejšie to, čo neslušne hovorili tí, ktorí knižku nečítali. 

 Réžia: Mike Newell. Scenár: Ronald Harwood (podľa románu G.G. Márqueza). Kamera: Alfonso Beato. Hudba: Antonio Pinto. Strih: Mick Audsley. Výprava: Wolf Kroeger. Kostýmy: Marit Allen. Hrajú: Javier Bardem, Benjamin Bratt, Gina Bernard Forbesová, Giovanna Mezzogiornoová, Marcela Marová, John Leguizamo, Hector Elizondo, Liev Schreiber a ďalší 

 








 



&lt;a href="http://blueboard.cz/anketa_0.php?id=839725"&gt;Anketa&lt;/a&gt; od &lt;a href="http://blueboard.cz/"&gt;BlueBoard.cz&lt;/a&gt;




 
Prosím, v prípade záujmu využite anketu na hodnotenie filmu, nie recenzie. Ďakujem za pochopenie a prajem príjemne hlasovanie.

