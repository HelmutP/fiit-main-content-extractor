
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Popaďaková
                                        &gt;
                Zaujalo ma
                     
                 Pankrác, Servác, Bonifác... 

        
            
                                    15.5.2008
            o
            8:53
                        |
            Karma článku:
                3.68
            |
            Prečítané 
            1345-krát
                    
         
     
         
             

                 
                    Tohto roku sa predstavenie v podaní týchto troch ľadových mužíčkov nekonalo. Niekto si povie aká škoda, iní sú radi. Pravdupovediac, také oteplenie nečakal takmer nikto z nás. O to príjemnejšie prekvapenie.
                 

                 
  
    Dni pred týmito troma vôbec nenaznačovali, že počasie bude také krásne. V sobotu a v nedeľu sme si ešte museli vziať so sebou nejaký ten svetrík alebo bundu, ale od pondelka... Teploty vysoko nad 20 stupňov. Výnimkou boli samozrejme vyššie položené oblasti.     Meteorológovia tiež nepredpokladali oteplenie skôr ako pred 10. májom. Táto predpoveď im dokonale vyšla. No, ale čo s našim Pankrácom, Servácom a Bonifácom? Tak tohto roku sa ochladenie buď posunie na neskôr a prípadný koniec mesiaca alebo sme si toho ochladenia užili dosť počas predchádzajúcich mesiacov.     A tieto dni? Bolo slnečno, pofukoval jemný vánok, sem-tam nejaká búrka... Miestami sa teploty všplhali nad 25 stupňov. Počasie ako v lete. Žeby už prichádzalo? Tak otvorme mu dvere, nech je svet krajší aj vďaka počasiu.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Popaďaková 
                                        
                                            Takmer po roku, opäť...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Popaďaková 
                                        
                                            "Adrenalín bez konca" s Ježišom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Popaďaková 
                                        
                                            Kam na vysokú... alebo kam ma vezmú...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Popaďaková 
                                        
                                            Sila slova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Popaďaková 
                                        
                                            Tri dôležité dni v mojom živote
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Popaďaková
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Popaďaková
            
         
        popadakova.blog.sme.sk (rss)
         
                                     
     
        Vysokoškoláčka študujúca žurnalistiku, ktorá je atypickou žurnalistkou... V inom prípade, normálne nenormálne dievča, ktoré chce žiť svoje sny a cestu životom by neprešla bez najbližších jej srdcu...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    974
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Škola
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Zaujalo ma
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Biblia
                                     
                                                                             
                                            R. Jašík: Námestie sv. Alžbety
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio Expres :)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zuzana Vongrejová
                                     
                                                                             
                                            Jozef červeň
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




