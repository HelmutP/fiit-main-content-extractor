

 Návrh zákona nadobudne platnosť až 1. Apríla, preto chceme  v stredu 10. Marca vyzvať prezidenta, aby ho nepodpísal, a to prostredníctvom protestu na Hodžovom námestí a otvoreným listom, ktorý bude už od dnes možné podporiť i elektronicky, každým jedným občanom SR. Od dnes taktiež spúšťame  petičnú kampaň za zrušenie tohto zákona. V prípade, že by prezident napokon zákon podpísal vyzývame študentov, k druhému protestu 1. Apríla kedy nadobudne právoplatnosť a následnému štrajku študentov na celom území SR! 
 Celý deň nám chodia desiatky správ od študentov a riaditeľov škôl, ktorí sa do protestov a štrajku chcú s určitosťou zapojiť. Iba druhý deň po vzniku podporuje iniciatívu proti vlasteneckému zákonu už vyše 3000 ľuďí! Vyzývame preto i Vás, všetkých slušných a inteligentných ľudí aby tento protest v stredu na Hodžovom námestí prišli podporiť! 
 Online petícia za zrušenie zákona o vlastenectve ako i otvorený list adresovaný prezidentovi nájdete na stránkach: http://www.changenet.sk/?section=kampane 
 Robert Mihály - Iniciatíva za transparentnú demokraciu 

