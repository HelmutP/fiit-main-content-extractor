
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Fořt
                                        &gt;
                Nezaradené
                     
                 Portugalsko: zelená manželstvám párov rovnakého pohlavia 

        
            
                                    18.5.2010
            o
            12:22
                        (upravené
                18.5.2010
                o
                13:08)
                        |
            Karma článku:
                7.97
            |
            Prečítané 
            1939-krát
                    
         
     
         
             

                 
                    Po Rakúsku, ktoré zaviedlo takzvané registrované partnerstvá od 1.1.2010 je Portugalsko druhou západoeurópskou krajinou v tomto roku, ktorá schválením parlamentnou väčšinou a následným podpisom prezidenta zavádza možnosť životného partnerstva aj pre páry rovnakého pohlavia práve v týchto dňoch.
                 

                  Portugalsko má so Slovenskom mnoho spoločného. Patrí skôr medzi menšie členské štáty EÚ, so silnou katolíckou tradíciou a porovnateľnou ekonomickou úrovňou ako Slovensko. Podobne ako Slovensko  v Česku, má aj Portugalsko v Španielsku väčšieho kultúrne a jazykovo blízkeho suseda, ktorý už má uzákonenie životných partnerstiev za sebou.   V Západnej Európe sú posledné krajiny, ktoré sa na tento posun v spoločnosti zatiaľ neodhodlali: Írsko, Malta a Taliansko. Spoločný menovateľ je zrejmý a má mnoho spoločného so Slovenskom či Poľskom. O to je dôležitejšie, že tradičné katolícke krajiny ako Rakúsko, Španielsko či Portugalsko sa rozhodli už ďalej neodďaľovať prijatie tohto pre vývoj spoločnosti dôležitého medzníka porovnateľného so zrušením otroctva či priznaním volebného práva ženám v minulosti. Mám za to, že pre spoločnosť založenej na princípe slobody a tolerancie ide o historickú nevyhnutnosť. A to ma napĺňa optimizmom.   Chcem všetkým Portugalčanom pogratulovať! Ukázali, že už na to dozreli. Môžu byť na seba a spoločnosť, v ktorej žijú, právom hrdí. Pre mňa, Slováka s českým priezviskom je to povzbudením, že nie je ďaleko deň, keď sa budem o tejto možnosti môcť rozhodovať aj ja s mojim dlhoročným životným partnerom. Rozhodol som sa, že k tomu, aby sa tak stalo aj u nás na Slovensku prispejem aktívne. Nik to za nás nespraví.    3 komentáre, ktoré vystihujú aj moje pocity:    André Pinto, Porto, Portugalsko:   "It's not everyday I can say I am proud of being Portuguese but today is definitely one of those days! I am truly happy for our society, well done us!"    Oscar V. Pinta, Soenderborg, Dánsko:   "Congratulations Portugal! It's a huge step forward and let's hope that soon Eastern Europe is going to understand the essence of the equality!"    Warson Correa, Belo Horizonte, Brazília:   "Congratulations. It was a big surprise for me to read it here. Hope we as ex colony follow the same step and do something about human rights. But a bit dificult when you think we are the largest catholic population. But fingers crossed and who knows ??? Things change!!!!"     Priatelia, homosexuali aj gay friendly heterosexuali, vidíme sa na prvej slovenskej gay pride, ktorá sa uskutoční už túto sobotu 22.5.2010 o 13:00 hod. v Bratislave na Hviezdoslavovom námestí. Možno nad ňou po dlhých dňoch zamračeného neba vyjde slniečko. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (125)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Preniesť sa ponad seba a zažiť kus večnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Aky je odkaz bilbordu Aliancie za rodinu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Couchsurfing, moja vlastná 6 ročná skúsenosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Drobné prekvapenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Kabaret: milujem cesty vlakom 2. triedy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Fořt
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Fořt
            
         
        fort.blog.sme.sk (rss)
         
                                     
     
        Mám rád slobodu, je to moja životná filozofia a môj životný kompas. Som pozorovateľ sveta. Fascinujú ma neviditeľné veci, ktoré hýbu dušou človeka. Zaujímam sa o happyológiu a dlhovekosť. Som couchsurfer. Obohacujú ma stretnutia. 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2977
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Karol Kaliský
                                     
                                                                             
                                            Samuel Genzor
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       6 tipov pri výbere osobného trénera
                     
                                                         
                       Gladiátor HD tréning
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




