

 
 
 
 
Obyčajný deň. Teplo, slniečko. Za domom mám potok. Syn si rád hádže kamienky do vody a keď máme šťastie, tak vidíme kačičky. 
 
   
 
Aj tentoraz sme vyšli, ani bráničku nezatváram, veď je to 5,25 metra. 
 
   
 
Zjavil sa nenazdajky. Zohol som sa, aby mu syn nezavadzal, keď som uvidel pred očami jeho päsť. Hoci som reflexívne uhol, dostal som plný úder. 
 
   
 
Isteže zostal som šokovaný, ale času veľa nebolo. Dostával som ďaľšie rany. No už som sa bránil. Keďže som počul ako syn plače, chytil som bežca okolo pása , bol oveľa vyšší ako ja, vyšportovaný, ale podarilo sa mi nohou od zadu ho zvaliť a väčšou váhou som ho pritlačil k zemi. Bránil sa a keďže syn stál vedľa, plakal čoraz hlasnejšie, zodvihol som ruky, sediac mu na hrudi. Myslel som si, že sa ukľudnil, no dostal som ranu do brady. Verte, hoci som humorista, prežil som detstvo v športovej triede ako hokejista, a toto ten športovec nemohol vedieť... 
 
   
 
Keď som už videl, že dlhšiu dobu neuvidí, vstal som z neho a pomohol mu na nohy... Obaja sme lapali po dychu. Bál som sa obrátiť k synovi, nevedel som, či má  ten šialenec dosť alebo ešte chce, či na mňa nezaútočí opäť.... 
 
   
 
Ale keď sa ma vyčítavo spýtal, že prečo sa bijem pred synom, zodvihol som ruku so zovretou pesťou, na čo len povedal „Aha", že pochopil že sám začal. Keď som ho poprosil aby zmizol od môjho pozemku, zbadal som, že spoza plota sledoval náš boj sused. Pozval ma bližšie a povedal mi, že ten športovec je miestny magor. Nejaký Motyka (ja tam bývam nedávno takže nepoznám všetky náradia v okolí). Keď som mu oznámil že idem na políciu, či bude svedčiť, tak mi povedal že nie. Pretože nechce s magorom nič mať. Už vraj zmlátil jeho neter, keď sa ozvala, že týra svoju dogu.... 
 
   
 
Tak som šiel na tú políciu sám, čo mi poradia. Keďže som nemal svedkov a Motyka podľa môjho názoru dopadla horšie, tak som to nechal. Bolel ma však krk a hlava, v noci som zašiel s manželkou na pohotovosť. 
 
   
 
Bývame trochu nad mestom a k nám vedie tmavá úzka cesta pomedzi jablone, vedľa stajní s koňmi. Pri ceste z pohotovosti som ho našťastie zbadal a reflexívne zabrzdil. Kôň vybehol na cestu, podľa neho bola tráva na druhej strane oveľa šťavnatejšia, hoci neviem ako to vie v tej tme rozoznať.. 
 
   
 
Zapol som blikačky a začal vyvolávať. Keďže nemám predstavu ako sa volá polícia, spojovateľka ma prepla na nejakú centrálu. Policajt mi oznámil, že mám volať mestských a že on sedí v Košiciach. Poprosil som si jeho meno, vysvetlil mu, že idem z pohotovosti, bolí ma hlava, ale s radosťou oznámim zajtra novinárom jeho priezvisko, keď sa niečo stane, pretože odchádzam. To som už mal zvýšený hlas... Našťastie pochopil, že nemá zmysel sa so mnou ťahať. Že vraj ihneď niekoho posiela. 
 
   
 
Neoklamal. Prišli onedlho mestský policajti a pomohol som im nájsť toho koňa a zahnať ho do deravej ohrady. 
 
   
 
Ráno keď som šiel na neurológiu, už som videl, že plot je poplátaný. 
 
   
 
Neurológa som pobavil včerajšou historkou, ale pre istotu ma preklepal, reagoval som správne, tak ma prepustil. 
 
   
 
Domov z Polikliniky  som šiel na úplne novom aute, ktoré dostala moja manželka za pôrod môjho syna. Dokonca som auto nemal ani poistené. Jednoducho ešte voňal novotou. Šiel som pomaličky po tichej uličke (ak poznáte v SP.N.Vsi rajón za Šestnástkou, tak tam, za zimným štadiónom) keď zrazu vidím, že sa zem zdvíha a točí sa okolo auta. Poznáte to zo spomalených filmov o pretekárskych autách. Bolo to naozaj fantastické... len ten koniec bol tvrdý. Nechápal som prečo sa Zem nevrátila pod auto, ale ju mám nad hlavou.... Veľmi tesne nad hlavou... 
 
   
 
Veľa jazdím, takže občas som toto už videl. Lenže vždy z vonku. Teraz som nevedel čo mám robiť. Keďže ma nič nebolelo, nebol som si istý či ešte žijem - to som mal z toho filmu Duch s Patrikom a Demi. . 
 
   
 
Pre istotu som sa pozrel, či neuvidím seba samého sa niekde povaľovať v kaluži krvy s prepichnutou hruďou, otvorenou hlavou, odseknutou nohou, ale nevidel som žiadnu povaľujúcu sa končaťinu bez tela a ani krv. 

Takže asi tieto ruky sú moje ruky. Tak som nimi začal hľadať tlačítko na páse. Ani som si neuvedomil, že som dole hlavou a to čo ma drží je práve ten pás. Že žijem som pocítil, keď som sa zvalil na hlavu. Je sranda, koľko málo priestoru máte, keď ste v prevrátenom aute. A že vraj koľko užitočného (alebo úžitkového?) priestoru - tvrdila reklama. 
 
   
 
To sa už niekto pokúšal otvoriť dvere, zo strany spolujazdca. Z mojej strany to bolo nemožné. 
 
   
 
Prekvapila ma húkačka požiarnikov, na mieste boli ešte ani prach hádam neopadol. Ja som videl len ruky, ktoré ma vyťahovali.... 
 
   
 
A vtedy sa to stalo. Poranil som sa, totiž, jednu ruku som musel dať na vozovku, na ktorej bolo roztrúsené sklo. 
 
   
 
Keď ma postavili, spýtali sa, či mi niečo je, tak som mal našťastie aspoň tú ruku poškriabanú. A to by my nikto neveril, že som z toho auta vyliezol práve ja. 
 
   
 
Poznáte to o tých supoch. Zažil som to. Je neuveriteľné, koľko národa sa zišlo v tichej uličke v priebehu minút... 
 
   
 
Hľadal som príčinu svojho letu a uvidel som ju zopár metrov odo mňa. Ku mne sa blížila tmavovlasá blondínka, plakala a nohami robila pohyby akoby jej bolo treba cikať. Bolo mi jasné, že ten let bola jej príčina. Veď človek, keď mu treba tak neberie ohľad na nejaké mŕtvoly a ide najprv na ten záchod, aby si mŕtvoly mohol v kľude užiť. Trochu za ňou som zbadal v polo zbúranom plote polo hrdzavého Favorita. 
 
   
 
Bolo mi všetko jasné, keď došla sanitka. Ja som bol v pohode, ale tmavovlasá blondínka - tú potrebovali zachrániť - v takom bola šoku. Keď som z tej sanitky vyliezol, policajt si ma vzal na miesto stretu. Ilustroval cestu sprejom a vysvetľoval mi, čo sa stalo. Šiel som po hlavnej a tmavovlasá blondínka stlačila namiesto brzdy plyn. A trafila ma do zadného kolesa tak „šťastne" že som si mohol skúsiť bezváhový stav. 
 
   
 
Poistku (nevyplatenú) som mal ešte stále v saku, takže som mu mohol kľudne nadiktovať číslo zmluvy. 
 
   
 
Potom som už len tak postával v dave. Keď auto prevracali nazad, aby ho vytiahli na odťahovacie auto, potiahol ma za rukáv kamarát, ktorý práve dobehol. Roky som ho nevidel, ale teraz viem kde ho nájsť. Pohľadám nejakú sviežu havárku ... 
 
   
 
Nazdar Miro. Čo sa tu stalo sa ma pýta. Tak som mu vysvetlil to o tej tmavovlasej blondínke a saltách ... keď ma prerušil otázkou: A čo vodič, prežil? ... 
 
   
 
Bolo mi ľúto tej tmavovlasej blondínky, zjavne to bolo jediné, čo v živote mala, myslím jej auto a do mesta zo svojích Kurimian zablúdila náhodou alebo omylom. Zjavne je pre ňu naše malé mestečko nad jej psychycké možnosti. ...
 
   
 
Tak som ju na polícii zabával, zašiel s ňou do poisťovne, sám si zohnal odťahovačku, sám sa sral s poisťovňou, a v princípe zaplatila oveľaoveľa menej, keby som bol väčšia sviňa a chcel sa na nej povoziť.
 
 
 Neverte tmavovlasým blondínkam, ktoré vás vytočia na rovnom mieste. Neoplatí sa. STiahnite z nich koľko sa dá. Možno sa poučia a budú aspoň ... 
 
   
 
Ona už jazdila na novom aute a ja sa potil v aute z požičovne bez klímy. A keď som si mal už prísť po auto, začala ešte robiť problémy v autoservise. Že vraj som šiel rýchlo, keby som šiel pomalšie (podľa jej prepočtov asi 15,60 km/h) a ona ma trafila, tak by som to salto asi nespravil... 
 
   
 
Ale vrátim sa k Môjmu dňu. Musel som predsa domov. Oznámiť manželke čo sa stalo a opäť zájsť k dochtorovy nech má opäť preklepe... Vážne.  
 
   
 
Očakával som na sebe niečo podobné, ako sa mi stalo raz v Moskve. Keď na mňa zaútočila v zime svorka dobre organizovaných túlavých psov. Cúval som pred vlčiakmi, ktorých z boku chránili  dogy a v ich strede na mňa brechali miešanci... Keď som spadol do snehu a trhali mi nohavice, zbadal som pri stene prilepené dievča. Keď psi začali cúvať (najprv stred, potom kraje a nakoniec vlčiaci) a dovolili mi vstať, musel som chladnokrvne osobne odlepovať tú dievčinu od steny - v takom bola šoku. Až keď som prišiel domov, začalo  triasť aj mnou... 
 
   
 
Takže, aj teraz som niečo podobné očakával, preto som zavolal sestre, nech ma domov odvezie. Ale nič podobné sa nestalo. 
 
   
 
Manželka sa dala do reči so sestrou, sadli sme si piť ruský čaj a ja som ich ešte chvýľu zabával a v kľude manželke oznámil, že je bez auta... 
 
   
 
V kľude. 
 
   
 
Asi to je tými zážitkami. Už ma nevie vytočiť ani tmavovlasá blondínka. 
 
   
 
Som zvedavý, čo mi prinesie zajtrajšok. Budú opäť Mirove dni? 
 
   
 
 
PS: A naozaj. Mal som problém, keď som v ten istý deň zašiel opäť k tomu istému doktorovi, aby ma preklepal, v zmysle či sa mu od smiechu niečo nestane, tak sa rehotal...keď som mu rozpovedal ďalší zážitok...
 

