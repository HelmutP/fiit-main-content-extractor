
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Kačáni
                                        &gt;
                Politika
                     
                 Grécko je o nás 

        
            
                                    11.5.2010
            o
            10:00
                        (upravené
                10.5.2010
                o
                17:50)
                        |
            Karma článku:
                24.26
            |
            Prečítané 
            7299-krát
                    
         
     
         
             

                 
                    Vyhadzuje sa vám koprivka pri pomyslení, že ideme požičiavať Grécku 24 miliárd korún, ktoré pravdepodobne nikdy neuvidíme? Ide vás roztrhnúť od jedu? Hm, pýtam sa, prečo sa zlostíte? Táto otázka patrí hlavne voličom vládnych strán. Lebo to Grécko je vo veľkej miere o nás.... Je to naše zrkadlo...
                 

                 Aký živočíšny odpor sa šíri našou verejnosťou voči Grékom. Aké emócie planú v „rozhorčenej" verejnosti. Viete čo je na tom komické? Protestujeme proti tomu, po čom väčšina z nás túži.  
 Ja som samozrejme tiež rozhorčený nad tým, čo sa v Grécku deje. Pozerajúc na tie besniace davy, ako v tranze devastujú všetko, čo im príde pod ruku, napádajú ma také prívlastky na adresu Grékov, aké tu nemôžem ani napísať. Išiel ma šľak trafiť, keď som videl titulky dennej tlače v Grécku (ukazovali ich v nemeckej televízii), na ktorých sa vyskytujú karikatúry Merkelovej spotvorené na Hitlera, upíra, zločinca a podobne. Tej ženy, ktorá nakoniec súhlasila, aby Gréci dostali tie obrovské peniaze a ktorá im dá (oficiálne „požičia") úplne najviac zo všetkých. Normálne by som ich všetkých prefackal a vyšľahal mokrým uterákom po papuliach. Ale to len moja osobná emócia.   Žasnem nad tým množstvom článkov o Grécku. Či médiá, či blogy, enem sa to tak hemží insitnými ekonómami, ktorí ako Truhlíci tliachajú čo ich napadne. Prepáčte priatelia, ale to sú tak neskutočne komplikované veci, že do podstaty vidí možno pár ľudí. Ale u nás do toho vidí každý chruňo a každý má riešenie po ruke. Neuveriteľné. To len na okraj.   Neskutočne smiešne je na celom prípade to, že my sme presne takí istí ako Gréci. Na chlp. Minimálne voliči Smeru. A tých je skoro polovica, ak veríme „nezmanipulovaným" prieskumom verejnej mienky.   Veď si predstavte, že by naša „sociálna" vláda zrazu zvýšila dôchodky o 400%, znížila by dôchodkový vek, dôchodok by bol vo výške 100% posledného platu, rozdávala by 13, 14, 15 plat a dôchodok, odstupné by bolo až do 100 platov a polovica národa by pracovala v štátnej správe. Myslíte si, že by niekto počúval nejakého ekonóma, ktorý by varoval pred krachom? Fico by mal nie 35%-nú podporu, ale 80%-nú!   Celé tie húfy nímandov a parazitov by mľaskalo rozkošou, oči by im zahalilo beľmo erekciou z takýchto „sociálnych" výhod. Pretože volič Smeru toto chce, toto očakáva. Veľa peňazí, za čo možno najmenšiu robotu, prípadne veľa peňazí za žiadnu robotu. Hlavne, že je dnes trochu dobre, zajtrajšok ma nezaujíma. Keby to však aspoň takto bolo!   Naši „socialisti" sú podstatne prefíkanejší, ako tí grécki. Kým tí grécki korumpovali širokú verejnosť a rozhadzovali tak, že parazitickým spôsobom života mohla existovať väčšina obyvateľstva, tak u nás pre voličov socialistov zostali len smiešne omrvinky, celý koláč vyrabovalo niekoľko „chytrákov". Dúfam, že nikto nie je taký slepý, že by vážne tvrdil, že sa mu za súčasnej vlády nejako seriózne polepšilo.... teda okrem sponzorov....   Naproti tomu, naši voliči sú podstatne sprostejší ako tí grécki. Kým v Grécku reálne nastolili na pár rokov socializmus a národ si ho mohol aspoň chvíľu vychutnať, tak u nás vláda o socializme len tliachala a reálne si mohlo vychutnať socializmus len zopár sponzorov a kumpánov tej chamrade. Tí si ho však vychutnali plnými priehrštiami. Ostatným stačilo počúvať drísty a napodiv ich to nasýtilo.   Navyše je tu ešte jeden moment, ktorý potvrdzuje moje slová. Kým v Grécku o katastrofálnej situácii vedelo len zopár ľudí, lebo roky klamali a upravovali čísla bulharskými konštantami, tak u nás katastrofálne čísla vývoja ekonomiky sú známe (možno ich tiež falšujú a sú ešte horšie). Zlostíme sa na Grékov, že roky žili v ilúzii sociálneho štátu (čo samozrejme veľmi radi robili), ale na Slovensku nemáme ani tú ilúziu, čísla sú jasné, napriek tomu najviac voličov (aby som vylúčil akúkoľvek možnosť interpretácie, tak poznamenávam, že myslím ficovoličov) túži dostať krajinu do gréckej situácie. Sme podstatne blbší, ako Gréci.   Obludné a perverzné je to, že o socializme tára ten, kto sa správa ako zamestnanec finančných skupín a oligarchov. Gréci si to aspoň užili. Nás naši „socialisti" len ponížili, vysmiali sa nám do ksichtu, ukradli milión krát viac ako sa vôbec dalo a nechávajú tu po sebe zjazvenú, rozbombardovanú krajinu. Nielen ekonomicky, ale aj morálne. Kriminálny štýl politiky sa stal štátnou doktrínou. Na jednej strane boľševická luza pustila pár omrviniek do pľacu, na druhej strane vycucala chudákov na priamych platbách v zdravotníctve a cez energie omnoho viac, ako im z toho balíka peňazí, ku ktorému sa dostala ako slepé kura k zrnu a o ktorý sa nijako nezaslúžila, „pustila". A to sme len začali. Zatiaľ budujeme „socializmus" len štyri roky. Ale vyzerá to tu, ako keby sme ho budovali už celé desaťročia.   Na jednej strane zúrivosť, lebo sa vyskytol štát, kde korupcia, infantilnosť a neschopnosť politikov, spolu s nehoráznymi „sociálnymi" výhodami doviedli krajinu do bankrotu a dnes sa musíme aj my podieľať na záchrane toho chorého systému, na druhej strane túžba po presne takom istom systéme. Sme kompletní?   Je to neuveriteľné a neskutočné. Máme tu rukolapný príklad, ako to dopadne, ak sa socialisti so svojimi demagogickými drístami utrhnú z reťaze, ale u nás doma veselo a bez mihnutia oka si zvolíme presne takých istých, čo takých istých, podstatne horších! A keby len zvolíme. Ono to vážne smrdí tým, že sa to zopakuje ešte raz! Po štyroch rokoch dokázalo naše lúpežné komando naškodiť a vyrabovať toľko, že pokiaľ Gréci išli do krachu pokojnou a dlhou cestou, my tam máme už vybudovanú diaľnicu. Stačí si len pozrieť čísla deficitu verejných financií, zadĺženosť štátu a korupciu približujúcu sa k dokonalosti.   Ficovoliči, nezlostite sa na Grékov. Ste rovnakí!   Preto pekne čušte a poskladajte sa na vyčíňanie gréckych socialistov (žiaľ budeme sa musieť poskladať aj my ostatní). Poskladajte sa na „pôžičku" a nepindajte, nepindali ste ani na štvormiliardovú„pôžičku" nášmu Cargu, ktorá bola presne taká istá „pôžička", ktorá bude „určite" splatená, tak ako bude „určite" splatená aj „pôžička" Grécku. A kde tie peniaze nakoniec skončia, to je tiež podobné. Napchajú si ich do vrecák súkromné banky, u nás si ich napchali do vrecák súkromní sponzori „najsocialistickejšej" strany vo Vesmíre. Veď je to skoro to isté. Aj v jednom, aj v druhom prípade nikto z vás neuvidí ani jeden cent. A predsa solidárnosť, to je jedno z bojových hesiel vašej politickej superstar, však že? Tak buďte solidárni s gréckymi súdruhmi.   P.S. Môj osobný tip je, že Grécko vyrazia z eurozóny. Myslím si, že pragmatickí politici to len tak jednoducho nenechajú. Tento záchranný balík slúži na to, aby sa získal čas na pokojné a rozumné vyhodenie Grékov z eurozóny. Ale to je len môj osobný názor insitného ekonóma. Možno to čaká niekedy v budúcnosti aj nás. Smer máme dobrý.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (146)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Mozog je plastický
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            List jeho excelencii od organizátora protestov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Tušenie budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Najväčšia európska lúpež
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Reinkarnácia Štefánika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Kačáni
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Kačáni
            
         
        kacani.blog.sme.sk (rss)
         
                                     
     
        Potrím k tým, ktorí nikdy neparazitovali na štáte, ktorí nekradli a nekorumpovali. Teda k menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    103
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5713
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            môj iný blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            vysúšanie muriva
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




