

   Medzi zúčastnené strany irackej vojny môžeme zaradiť irackú armádu, ktorej hlavným veliteľom bol Saddám Husajn. Iracká armáda bola podporovaná hnutiami Al-Káida, stranou Baas a zahraničnou podporou boli štáty Sýria, Irán a Severná Kórea. Na strane USA bojovali Kurdistan, Veľká Británia, Turecko a tzv. "koalícia ochotných".  Vojna skončila porážkou hroznej diktatúry Saddáma Husajna. V Iraku sa uskutočnili „spravodlivé" voľby, Obama hlása že vojská sa z Iraku stiahnu... Ale prichádzajú otázky, ktoré sa týkajú tejto vojny - je USA taký ochranca ako by sa malo zdať? 
   
 Casus belli - Ropa 
   
   Keby sme mali hľadať reálny dôvod na začatie vojny v Iraku, určíte by sa nezhodoval s dôvodom, ktorý USA prezentovala vo svete. Dôvod založený na tvrdení, že Irak vlastní zbrane hromadného ničenia, a tože v štáte vládne tvrdá diktatúra nie je v žiadnom prípade, podľa medzinárodného práva, dôvod na napadnutie druhého suverenného štátu. 
   Definícia casus belli - čiže postačujúceho dôvodu na začatie a vyhlásenie vojny v medzinárodnom práve sa vymedzuje na napadnutie od iného štátu, napadnutie spojeneckého štátu, resp. Rozhodnutie Rady bezpečnosti OSN o vojenských akciách proti útočníkovi, ale ako je v novodobej histórií zvykom, USA šliape po medzinárodnom práve a nerešpektuje ho, čo je v tejto dobe zlým mementom pre medzinárodný mier. 
   Reálnym a jediným dôvodom na začatie vojny v Iraku boli nerastné suroviny - USA sa nikdy nezaujímala o blaho irackého ľudu o to, aby v Iraku existoval právny štát, aby prebehli spravodlivé voľby, aby padol krutý tyran Saddám Husajn. Jediný dôvod na inváziu do Iraku boli iracké ropné polia. Už teraz sú známe zmluvy a licencie uzatvorené medzi irackou vládou a americkými ropnými spoločnostiami. Pre ropu a biznis zomrelo 53000 vojakov a civilistov. 
   
 Saddám Husajn - mocná figúrka 
   
   Samostatnou kapitolou je diktátor Saddám Husajn. Narodil sa v pastierskej rodine, otca nikdy nepoznal, keď sa jeho matka znova vydala, mal problémy s nevlastným otcom. Svoje styky s politikou v Iraku získal cez strýka, ktorý bol členom strany Baas - Arabská strana socialistickej obrody. Pri prevrate v roku 1968 sa stal viceprezidentom. K tomuto mu pomohla americká CIA, ktorá ho dosadila ako figúrku, no Saddám postupne začal naberať moc a prestal byť pod vplyvom USA. Irak mal za jeho vlády predpoklad stať sa kvôli geografickej polohe a ropným poliam jedným z najrozvinutejších krajín Blízkeho východu. Tak sa postupne zo Saddáma Husajna stal nevyhovujúci človek. 
   USA uvalilo od roku 1991 na Irak hospodárske embargo, ktoré krajinu hospodársky a ekonomicky vyčerpalo. Po definitívnom rozhodnutí USA o vpáde do Iraku bolo jedným z hlavných cieľov chytiť a potrestať diktátora. Medzi zaujímavosti určite patrí to, že Saddám Husajn je označovaný za najväčšieho zlodeja všetkých čias. V marci 2003 odcudzil z irackej centrálnej banky jednu miliardu eur (1 000 000 000 €). 
   Saddámovi sa dlho darilo unikať. Po zrade blízkych spolupracovníkov bol 24. decembra 2003 zajatý americkými vojskami. Následne sa na súde zodpovedal z vojnových zločinov genocídy, vrážd a týrania. Na súde sa obhajoval sám a kategoricky odmietal všetky obvinenia - súd považoval za zmanipulovaný. 5. novembra 2006 bol súdom uznaný vinným a súd ho odsúdil na trest smrti, ktorý bol vykonaný 26. decembra 2006. Saddám Husajn - mocná figúrka, ktorú na scénu dosadil a aj zo scény stiahol veľký „spravodlivý" gigant - USA. 
   
 Vojna ako memento do budúcnosti 
   
   Iracká vojna je "asi" na konci. Tvrdím, asi, lebo vojská ešte neodišli. V krajine sa stále opakujú nepokoje, útoky, no USA má pocit, že by mohlo vojakov už stiahnuť - iracká vláda je pro-americky naklonená, ropné polia sú pod vplyvom amerických korporácií a iracký ľud...? Zbavil sa diktátora, no na jeho miesto prídu ďalší - nie jedna osoba, ale mnoho - korupcia porastie a Irak bude tam, kde bol pred vojnou. 
   Teraz tá najdôležitejšia otázka. Kto bude po Afganistane a Iraku ďalší? Irán, Sýria...??? 
   

