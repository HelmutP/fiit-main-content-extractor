
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Múdry
                                        &gt;
                Domáce úlohy
                     
                 Sipí sivý had, nik ho nemá rád 

        
            
                                    10.1.2008
            o
            17:00
                        |
            Karma článku:
                6.59
            |
            Prečítané 
            2193-krát
                    
         
     
         
             

                 
                    Ľudí je veľa ako hadov. Nie som teda žiaden strachopud. Keď už mám pred niečím rešpekt a strach, tak je to had. Ešte aj taký obyčajný neškodný slepúch si na mne dokáže vyliečiť svoje prízemné komplexy a zvyčajne ma so stopercentnou zárukou vydesí na smrť. Fóbia z hadov je vo mne hlboko zakorenená. Nie a nie sa toho zbaviť. Do čerta tam.
                 

                 
  
    Byť hadom, to prináša určité výhody. Uľahčuje sa transport – ľahneš a ideš, nemôže ti natiecť do topánok a prezliekaš kabáty kedy sa to hodí. Na druhej strane ale nemôže zobrať nohy na plecia a zdúchnuť keď je zle. Lenže bez hada by sme boli nahratí ako lokše. Pravdepodobne by sme vôbec neboli. Myslíte si, že by ju to trklo samú od seba aby si vzala to jablko? Ako poznám Evu, tak je to málo pravdepodobné. Našťastie bola Eva blondínka, Adam nebol gay a to čertisko nepodarené tam poslalo to vyššie uvedené hadisko jedovaté.      Počúvajte ma. Jula ste poznali, nie?  Ale istotne ste ho poznali. No, ten s tým bachoriskom a fúziskami. Čo do výzoru, tak celý ja. Vždy mi pripadal iba ako taký nahrávač na smeč a krovie pre toho druhého vysokého elegána. Viem o čom hovorím. Ešte ako starší žiak a mladší dorastenec som hrával volejbal. Jasné že som bol nahrávačom. Moja „výška“ ma pre tento post predurčovala. Stmeľoval som kolektív. Vždy, keď sme vyfasovali, tak sa tento stmelil a vinu zvalil na toho najmenšieho, teda na mňa. Malí ľudia to majú na tomto svete ťažké. Vždy „vytŕčajú“ z radu. Potom musia vyvádzať somariny aby si ich niekto všimol. Uvediem niekoľko príkladov: páni Napoleon, Stalin, Hitler a čo ja viem kto ešte. Aha na Múdreho som zabudol. Skromnosť nadovšetko. Julovu životnú filozofiu, názory a spôsob humoru chápem až teraz. Mám ho rád in memoriam.       Aj druhý Julo je kabrňák. Kam ten len chodil na tie svoje nápady? Bol to vôbec človek? Neviem. Vizionár bol určite. Všade dookola zúrilo devätnáste storočie, Ľudo s Miloslavom si hádzali mincou či budeme Paprikáši alebo Šnicláci (padla na hranu, tak sme zostali Haluškári) a on (Julo) zatiaľ vymyslí takú geniálnu vec ako sú dva roky prázdnin. Tento prevratný objav však zapadol prachom. Ľudstvo je nepoučiteľné. Zapodieva sa výletmi na Mesiac, lietačkami v balónoch, ementálovaním až skoro do stredu Zeme a podobnými nezmyslami, čo si Julo z prsta vycucal. Uznávam, že sú to užitočné a ľudstvu prospešné činnosti. Prečo sa ale nezačalo  dvojročnými prázdninami? Ešte že sme tu mali komunistov. Tí vymysleli aspoň uhoľné prázdniny. Keď už netečie, tak aspoň kvapká. Ostatné čoho sa chytili stálo za prd. Dúfam iba, že sa nenájde nejaký idiot, ktorý by vymyslel atómové prázdniny. Bum a je to tu prázdne. Tie by boli sakramentsky dlhé, tiché a pravdepodobne na večnosť.      Julka. Tak tú ste najskôr nepoznali. Zato ja áno. Miestami aj dosť detailne. Už vás vidím ako slintáte že čo bude ďalej? Aká prasačinka nám to tu spestrí? To musíte stále nato myslieť? Nič také nebude, lebo nič také ani nebolo. Keby aj bolo, aj tak by som sa nepriznal. Som predsa rodený gentleman.      Ježiš. Ježiša jeho, tak na toho by som skoro zabudol. Fakt, že Mária zostala nepoškvrnená je pre mňa tabu a vonkoncom ho nespochybňujem. Prečo nás ale zbytočne zavádzajú nehmotným Duchom svätým? Každý predsa už od malička vie, že detičky nosia bociany. Bocian je spojenec. Mimo inej fauny žerie aj hady. Ešte stále mám z nich strach. Pritom mi žiaden had v živote neublížil. Na rozdiel od niektorých ľudí. Nemal by som mať teda strach zo zlých, sebeckých a zákerných ľudí? Neviem. Ľudí mám rád. Budem sa radšej báť bocianov.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Bobuľka a gaštanko
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Uhrovec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Jankov vŕšok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Kolo, kolo mlynské
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Vodná šlapačka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Múdry
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Múdry
            
         
        igormudry.blog.sme.sk (rss)
         
                        VIP
                             
     
          

 Ešte stále strojár. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    176
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1888
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko moje rodné
                        
                     
                                     
                        
                            Nemecko moje prechodné
                        
                     
                                     
                        
                            Pokusy o veselosti
                        
                     
                                     
                        
                            Domáce úlohy
                        
                     
                                     
                        
                            Receptárium
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rolling Stones
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Je z čoho vyberať.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotky
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




