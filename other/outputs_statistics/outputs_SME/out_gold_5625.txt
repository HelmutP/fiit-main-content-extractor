

  
 Za týchto 7 mesiacov, kedy sa nemusím nikomu hlásiť a prať chlapské smradľavé ponožky som prišla k niekoľkým prekvapivým záverom a životným zisteniam. Určite na ne vo svojom živote príde každý, kto žije aspoň chvíľu sám, no ja si ich neviem nechať pre seba. Možno preto, že niektoré sa mi zdajú tak prekvapivé a tak prevratné, že ich radšej ani nečítajte. Možno by ste po nich zatúžili... 
 Niektoré sú stručnejšie, k niektorým pripájam aj dovysvetľovanie. 
   
 Uvarená bio-ryža nesplesnivie v chladničke v hrnci ani po troch týždňoch! To je naozaj veľmi prekvapivé, nakoľko mám v pamäti paniku, s ktorou som pred Vianocami otvárala chladničku. Po dvoch mesiacoch úporného smradu z jej útrob som sa naozaj začala báť, že to grilované kura ma v noci znásilní... Čiže: bio-ryža nehnije! 
   
 Ani 7 mesiacov po tom, ako vašu domácnosť opustí pes s čiernymi chlapmi, nemáte vyhraté. Aj napriek tomu, že žiadne zviera neprekročilo prah môjho bytu už viac ako pol roka, na gauči sú stále čierne psie chlpy a spoza práčky takisto občas vyskočí chumáč psích čiernych chlpov. Čiže: spomienok na čierneho psa sa zbavuje ťažko. 
   
 Sťahovanie nábytku mi už vôbec nerobí takú radosť ako kedysi. Vyrastala som v rodine, v ktorej sa pri každej zlomovej situácii sťahoval nábytok, izby a menili sa funkcie solitéru. Pohreb/svadbu/maturitu/narodenie teda vždy striedal víkend plný colštoku, sádry a nachádzania vianočných darčekov. Aj keď sme sa vrátili z pohrebu v strede augusta. 
 Keď som pred troma mesiacmi uletela „feng-šuej" smerom a kompletne prehádzala byt, zistila som, že už to nie je, čo to bývalo. Keď mi rodičia ako decku prehádzali nábytok v izbe a zrazu som sa z postele nepozerala na ružinovský Milex ale na filmové ateliéry na Kolibe, fakt som to prežívala. Tešila som sa a cítila som sa v izbe ako v novom priestore. Po „feng-šuej" nájazde mám iba tak permanentnú modrinu na lýtku, lebo ešte stále nemám vypočítanú vzdialenosť postele od dverí. Čiže tretie ponaučenie znie: sťahovanie už nie je také vzrúšo ako v detstve. 
   
 Všetky škatule, ktoré sú v byte treba skontrolovať hneď po rozchode. Hneď ako začnete žiť single, tak prejdite všetky škatule, ktoré v byte vidíte. Ja som to podcenila... Môžete v nich nájsť ešte akékoľvek zvyšky spomienok na svoju bývalú lásku či na tú ešte pred tým, ale môže sa vám stať aj to, čo mne. 
 Už od apríla 2009 som registrovala škatuľu v rohu, hore na kuchynskej linke. Nikto sa k nej nemal a keď sa môj ex v polovici septembra odsťahoval, mala som dosť iných vecí na práci ako kontrolovať nejaké zastrčené škatule. Hodne som žúrovala a kuchyňu som používala len ako miesto na varenie kávy. Na tom sa vlastne nič nezmenilo, si teraz uvedomujem. Anyway! 
 Na škatuľu utešene sadal prach. Pred Vianocami 2009 som si naozaj začala dávať otázku „čo tam asi tak je?". Keďže sa z rohu kuchynskej linky nešíril smrad ani nič podobné, tak som bola v klídku. A potom prišiel 25. december, kedy som levitujúc po byte rozchádzala vianočnú opicu a povedala som si „keď nie teraz, tak už nikdy". 
 25. 12. 2009 som teda v rohu na vrchu kuchynskej linky našla vianočné pečivo z vianoc 2008. 
 Čiže poučenie: skontrolujte po rozchode VŠETKY škatule. 
   
 Riad umývam raz/dvakrát do týždňa. Nikdy ma nebavilo varenie. Už ako puberťáčka som to považovala za stratu času. Odkedy som single, vôbec zo mňa nie je dievča na vydaj... 
 Keď moje kamošky, tiež so statusom single, začnú hovoriť o tom, čo si navarili v nedeľu predpoludním a potom to až do stredy dojedali, obchádzajú ma mdloby. Panebože! V LaMamme za rohom je šalát za dve eurá! Mäso nejem a kávu mi uvaria všade. Takže doma ne-va-rím nič, okrem vody na kávu. 
 A vďaka tomu, že som si za svoj krátky život nazbierala dosť šálok (či už ako darčeky, úplatky, spomienky na miesta...) a doma pijem jednu kávu ráno a jednu večer ( cez víkendy 4 ), prišla som na geniálny spôsob stohovania šálok v dreze... Čiže: riad mi stačí umyť raz za týždeň.  
   
 Tu je ešte zopár dôvodov, pre ktoré si JA OSOBNE vážim svoj single status: 
 -       polička v kúpeľni je zaprataná patlaninami od výmyslu sveta. A vôbec nikto nenadáva! 
 -       môžem si na záchode čítať knihu aj dve hodiny. 
 -       žiadne povinné jazdy po návštevách rodinných príslušníkov. 
 -       svoj víkendový komatický spánok štartujem v piatok a končím v nedeľu. A nemusím medzi tým vstávať a variť. 
 -       môžem ochotne pomáhať couchsurferom. 
 -       nemusím cvakať kartu, ktorú by mi niekto kontroloval. 
 -       nikomu nemusím NIČ vysvetľovať. A že by sa toho našlo dosť... 
   
 Je možné, že po prečítaní týchto riadkov niektoré ženy zatúžia po single živote. Nie je to až taká sranda. Nemá vám kto vyniesť čerstvo kúpené oblečenie z auta. Minule som sa trepala s taškami z centra a kým som ich vyniesla, tak som skoro vypľula dušu. 
   
 A v zmysle vety „každá správna single žena má mať doma kladivo, vibrátor a vŕtačku" odchádzam do Hornbachu. Nový model Black and Decker vyzerá dosť zaujímavo. Želám vám ešte pekný deň. 
   

   

