
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Počai
                                        &gt;
                Viera
                     
                 Že Kristus je jediná cesta k Bohu? Aká to netolerancia! 

        
            
                                    31.3.2010
            o
            11:12
                        (upravené
                31.3.2010
                o
                13:29)
                        |
            Karma článku:
                14.76
            |
            Prečítané 
            4491-krát
                    
         
     
         
             

                 
                    „Ja som cesta, pravda i život. Nik nepríde k Otcovi, ak nejde cezo mňa." Takýto výrok Ježiša Krista máme zaznamenaný v Evanjeliu Jána 14:6. To teda prestrelil! Že mu to nedošlo? To je taký nerozhľadený a netolerantný? Alebo nie?
                 

                 Často sa stretávam s názormi, že kresťanstvo je veľmi netolerantné. Myslí si, že ono jediné má pravdu. Ľudia sa pýtajú: Prečo nemôžete akceptovať druhých ľudí a ich pravdu?   Slovník slovenského jazyka definuje toleranciu ako „znášanlivosť voči presvedčeniu alebo spôsobom iných". Slovo tolerovať je definované ako „znášať, pripúšťať," (napr. chyby alebo omyly). Akademický slovník cudzích slov toto slovo vysvetľuje ako „znášať, trpieť, dovoľovať, pripúšťať". Vo všetkých týchto významoch je jasná konotácia, že ide o znášanie niečoho, čo sa nám príliš nepáči alebo s tým nesúhlasíme. Apoštol Pavol to vyjadril takto: „Láska všetko pretrpí" (1. Korinťanom 13:7).   Dnes sa však poza bučky postupne zavádza a pretláča nová definícia tolerancie. Táto definícia hovorí, že presvedčenie, názory, životný štýl a vnímanie pravdy každého jednotlivca sú rovnocenné. Inak povedané, tvoje aj moje presvedčenie je rovnaké a každá pravda je relatívna.   Skúsme sa ale zamyslieť nad výrokom: „Každá pravda je relatívna." Počkať, a nie je potom aj tento výrok relatívny? A nie je presadzovanie takéhoto názoru netolerantné voči presvedčeniu tých, ktorí veria, že je tu predsa len nejaká absolútna pravda?   Všimnime si rozdiel medzi otázkou tolerancie a otázkou pravdy. Tolerancia neznamená automaticky všetko schváliť ako pravdu. Môžem mať svoje presvedčenie a zároveň tolerovať druhého s iným presvedčením, teda nebudem mu nadávať, prenasledovať ho, páliť ho na hranici. A on zas môže tolerovať mňa, hoci spolu nesúhlasíme. To je pluralitná spoločnosť, ktorej nevyhnutným predpokladom je, že môžem slobodne zastávať a šíriť svoje presvedčenie prostriedkami, ktoré sú nenásilné. Ale to neznamená, že každý má pravdu. Raz uvidíme, kto ju mal.   Osobne som vo svojom živote spoznal Ježiša Krista ako živého, vzkrieseného Božieho Syna, ktorý zmenil môj život. Dospel som k presvedčeniu, že jeho výrok, ktorý som uviedol na začiatku, je pravdivý. V pluralitnej spoločnosti mám slobodu toto presvedčenie zastávať a šíriť nenásilnými prostriedkami. Druhí majú slobodu oponovať mi takisto nenásilnými prostriedkami. Ak to urobia, ja nemám právo obviňovať ich z neznášanlivosti a netolerancie. Niekomu sa moje svedectvo nemusí páčiť. Niekoho naopak môže osloviť a môže prežiť podobnú skúsenosť. A raz uvidíme, kto mal pravdu.   Niekto môže namietnuť, že cirkev v minulosti používala tvrdé metódy proti „neprispôsobivým" a keby mohla, využívala by ich aj dnes. Možno vás šokujem, ale úplne s tým súhlasím. Mám na to veľmi dobrý dôvod: s mojím presvedčením by som v stredoveku pravdepodobne tiež zhorel na hranici, podobne ako Ján Hus.   Keď chceli dvaja z Ježišových učeníkov zvolať oheň z neba na mesto, ktoré Ježiša neprijalo, Ježiš ich rázne zastavil: „Neviete, akého ste ducha. Syn človeka neprišiel ľudí zahubiť, ale zachrániť" (Evanjelium Lukáša 9:55). Kto sa snaží násilným spôsobom vytrestať ľudí, ktorí nechcú uveriť v Krista, nie je Kristovým nasledovníkom, nech sa volá hoci aj pápežom.   Apoštol Ján píše: „Milovaný, nenasleduj zlé, ale dobré. Kto robí dobre, je z Boha; kto robí zle, nevidel Boha" (3. list Jána 11). Poznám veľa fantastických ľudí, ktorí žijú podľa tohto textu. A viete čo, som rád, že nie vždy mi všetko schválili. Som rád, že ma upozorňovali na večne platné pravdy, ktoré nám Boh dal v Biblii. Po čase som mohol okúsiť sladké ovocie poslušnosti.   Môžete sa rozhodnúť, že takého exkluzívneho Ježiša nechcete. Máte na to plné právo. Ja to budem tolerovať. Ale kým budem žiť, budem hovoriť o tom, čo urobil pre mňa. Nemôžem inak.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (287)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Počai 
                                        
                                            Vzkriesenie: mýtus alebo realita?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Počai 
                                        
                                            Ježiš na kríži a Face in Hole
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Počai 
                                        
                                            Vari Boh každému neodpustí? A to prečo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Počai 
                                        
                                            Teplo domova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Počai 
                                        
                                            Hanba Ahmadinedžádovi!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Počai
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Počai
            
         
        pocai.blog.sme.sk (rss)
         
                                     
     
        Som teológ a prekladateľ, a predovšetkým normálny človek ako vy. Naučil som sa niečo zhora a považujem to za príliš drahé, než aby som si to nechal iba pre seba...
 Moja osobná stránka
 
 Moje prekladateľské služby
        
      
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    30
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1736
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Viera
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            J.C. Maxwell: 17 zákonov tímovej práce
                                     
                                                                             
                                            Ulf Ekman: Prayer Changes Nations
                                     
                                                                             
                                            Kauza Stvoriteľ
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Hillsong
                                     
                                                                             
                                            DecembeRadio
                                     
                                                                             
                                            Paul Wilbur
                                     
                                                                             
                                            John Schlitt
                                     
                                                                             
                                            Project Damage Control
                                     
                                                                             
                                            Petra
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Slovo života Banská Štiavnica
                                     
                                                                             
                                            CHRIST.SK
                                     
                                                                             
                                            Kapelka, kde som chvíľu hrával
                                     
                                                                             
                                            Hudobný portál Firestream.net
                                     
                                                                             
                                            Ulf Ekman Ministries
                                     
                                                                             
                                            Kresťanská televízia TV7
                                     
                                                                             
                                            Slovo života international
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




