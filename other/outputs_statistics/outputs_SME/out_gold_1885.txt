

 V utorok 3. 11. 2009 skončil štvrtý ročník súťaže Špongia. Súťažné tímy prezentovali svoje diela v telocvični Školy pre mimoriadne nadané deti na Skalickej 1 v Bratislave. Na prezentáciu mal každý tím asi 20 minút, ktoré sa dali využiť na rozprávanie o tímovej spolupráci, opis hry, ukážky z hry alebo premietnutie pripraveného reklamného videa. Potom sa k hre vyjadrili porotcovia – učitelia troch bratislavských gymnázií (ŠMND, Grösslingová, GJH) a zástupca firmy Cauldron. Aj body získané za prezentáciu boli súčasťou celkového skóre. O tretej popoludní sa slávnostne vyhlásili výsledky súťaže. 

 Ešte pripomeniem, že tento rok bolo úlohou vytvoriť hru, ktorú musia hrať viacerí hráči na jednom počítači. Na jej vytvorenie mali súťažné tímy 18 dní. V jednom tíme mohlo byť 1 až 6 súťažiacich. Hodnotí sa nielen program, ale aj grafika, hudba a celková hrateľnosť. 

 

  

 Tento rok bola súťaž otvorená aj pre tých, čo už nie sú študenti. Mohli sa zúčastniť ako nesúťažný tím, čo znamená rovnaké zadanie a rovnaké podmienky, ale bez uvedenia vo výsledkovej listine. Prihlásil som sa aj s kamarátom ako nesúťažný tím K2, ale z rôznych dôvodov sa nám v danom časovom limite nepodarilo hru vytvoriť, preto sme ani nič neprezentovali. Ponaučenie – keď už sa prihlásite do tejto súťaže, vyhraďte si na ňu počas jej trvania dostatok času a triezvo odhadnite svoje schopnosti. 

 Jedného dňa snáď túto hru dokončíme a keď sa tak stane, zrejme sa o nej zmienim aj na tomto blogu. Ale možno to nebude tak rýchlo, takže zatiaľ je tu aspoň ilustračný obrázok – hra má pracovný názov „Klik2“ a bude to niečo ako pexeso pre neobmedzený počet hráčov. Viac neprezradím, lebo ešte sám presne neviem. 

 

   

 Jedenáste miesto patrí tímu Metal Militia a hre „Stratení Mayovia“. Zaujímavá grafika; paličkoví panáci pri chôdzi prepletajú nohami a môžu si navzájom pomáhať pri skákaní. Hru sa však nepodarilo dokončiť, takže keď sa dostanete do cieľa miestnosti, nič sa nestane. Navyše program obsahuje mnoho chýb. (Skoro každá hra v tejto súťaži obsahuje nejaké chyby; v danom časovom limite sa väčšinou nepodarí všetky vychytať. Ale je fajn, keď sa ich aspoň počas prezentácie neobjaví priveľa.) Preto sa táto hra spomedzi súťažných príspevkov umiestnila na poslednom mieste. 

 Myslím si však, že aj s takýmto výsledkom môžu byť autori spokojní. V minulom ročníku súťaže sa posledné dve hry nedali ani spustiť, takže už samotné zhotovenie spustiteľného programu je prvým krokom k úspechu. Autori sú študentmi kvarty (vekovo zodpovedá 8. ročníku ZŠ), majú teda pred sebou ešte štyri ročníky Špongie, počas ktorých nás môžu príjemne prekvapiť. Ešte spomeniem, že táto hra bola naprogramovaná v jazyku Python. 

 

   

 Desiate miesto obsadil tím JACKY Studio a hra „Euromisia“. Téma hry – lietadlo nesie na Slovensko balíky eurobankoviek, ktoré z neho vypadnú niekde nad Indickým oceánom. Hráči ovládajú dva eurobalíky a ich úlohou je preskákať cez 16 štátov, z Bangladéša až na Slovensko. Balíky skáču cez plošiny, musia sa vyhýbať pichliačom a mínam, niekedy je potrebná spolupráca – jeden balík musí skákať z druhého, aby dočiahol na cieľ alebo jeden balík gombíkom aktivuje trampolínu, z ktorej vyskočí druhý. V jednotlivých miestnostiach sa na pozadí striedajú vlajky rôznych štátov a po celý čas hrá slovenská hymna (dá sa aj vypnúť). Prezentácia spočívala prevažne v tom, že Jacky veľkým krikom povzbudzoval členov svojho tímu, aby prešli čo najviac miestností... a oni stále padali na pichliače. ;-) 

 Tento tím si zaslúži veľký rešpekt – hoci sú autori študentmi kvarty, je to už tretí ročník Špongie, na ktorom sa zúčastnili! Minulý rok boli síce medzi tými, ktorým sa nepodarilo spustiť program, ale ešte rok predtým sa im to podarilo a umiestnili sa na peknom šiestom mieste z jedenástich. Nielenže majú ešte štyri súťažné roky pred sebou, ale od budúceho roka budú vlastne tímom s najdlhšou históriou. Na druhej strane, vzhľadom na to, že už tri roky robia hry pomocou nástroja Game Maker, očakával by som trochu viac herných prvkov a pestrejšie miestnosti. Možno nabudúce. 

 

   

 Deviate miesto získal tím Basha a Rado a hra „3v1 pre viac hráčov“. Autori vytvorili tri samostatné hry v programovacom jazyku Imagine Logo. V prvej hre sa z kôpky odoberajú 1 až 3 zápalky a kto vezme poslednú, prehráva. Druhá hra je ruleta. Tretia je mankala. 

 Všetky tri zatiaľ spomenuté súťažné tímy sú z našej kvarty – zdá sa, že to bude v Špongii veľmi silný ročník. :-) Budeme ich potrebovať, lebo tohtoročná oktáva, ktorá takisto postavila tri súťažné tímy, sa už v ďalších ročníkoch nezúčastní. 

 

   

 Na ôsmom mieste sa umiestnil tím VMSD a hra „Imperiosustres“. Hra má krásnu grafiku a vyzerá veľmi lákavo, s ovládaním je to žiaľ podstatne horšie, takže celkovo nie je príliš hrateľná. 

 Každopádne, na žiakov sekundy (vekovo zodpovedá 6. ročníku ZŠ) je to veľmi dobrý výsledok. Keďže aj táto hra je vytvorená pomocou nástroja Game Maker, je zaujímavé porovnať ju s Euromisiou -  Imperiosustres má zaujímavý príbeh, skvelú grafiku a dobre vyzerajúce miestnosti; Euromisia má zase vtipný nápad a dosť dobré ovládanie – keby sa tieto vlastnosti podarilo spojiť v jednej hre... 

 

   

 Siedme miesto patrí tímu PWNED z GJH a ich hre „Hell-o“. Po grafickej stránke sa mi táto hra páčila najviac; má profesionálnu úroveň. Aj nápad je zaujímavý – smrtka omylom odnesie do pekla duše štyroch dobrých bojovníkov a tí sa asertívne idú prebojovať von. Hru môžu hrať 1 až 4 hráči. Na webovej stránke si môžete stiahnuť obrázky na pozadie obrazovky. Doma mi táto hra žiaľ nešla nainštalovať, chýbala jej nejaká knižnica DLL. 

 Podobne ako tím Basha a Rado aj PWNED sa rozhodli namiesto jednej veľkej hry urobiť niekoľko menších. To je podľa mňa strategická chyba – za 18 dní sa to nedá rozumne stihnúť. Basha a Rado to stihli vďaka tomu, že ich minihry boli veľmi jednoduché a Imagine Logo urobilo 90% práce za nich. PWNED však kódovali v Delphi a tak hoci mali v tíme troch programátorov, zo štyroch plánovaných minihier stihli vytvoriť tuším iba dve. Veľká škoda, lebo z hľadiska grafiky a zvuku rozhodne mali na lepšie umiestnenie. Keďže sú tretiaci, majú na to ešte jeden rok. 

 

   

 Šieste miesto obsadil tím Neviem a hra „World of Console“. Hra má 3D grafiku a je to RPG z pohľadu hráča. Keďže túto hru hrajú dvaja hráči naraz, každý má svoju polovicu obrazovky. Rád by som napísal niečo viac, ale nevedel som prežiť ani pol minúty, hneď ma tam nejakí panáci rozsekali. Nuž, čo sa týka náročnosti hry, menej je niekedy viac. 

 

   

 Piate miesto získal tím Deep Hole In The Road z gymnázia  Grösslingová (známeho aj ako GAMČA) a hra „Sponge Snake“. Klasika – každý hráč ovláda hada, ktorý musí jesť potravu, po ktorej sa predlžuje a nesmie naraziť do steny ani do iného hráča. Nejde o žiadnu novú tému, ale veľmi sa mi páči jej technické spracovanie – množstvo nastavení hry (spôsob ovládania, rýchlosť hry, množstvo potravy, pohyblivé steny, počítačový súper), možnosť jednoducho vytvárať vlastné miestnosti a celkovo príjemná grafika a hudba. Aj webová stránka hry urobí silný dojem, hoci so samotnou hrou súvisí len málo. 

 Opäť by som sa vrátil k hre Hell-o, pretože jedna z jej štyroch (plánovaných) minihier boli zhodou okolností tiež takéto hady. Keby sa autori sústredili iba na jednu hru, mohli vytvoriť niečo podobné, ba vzhľadom na ich skvelú grafiku možno aj lepšie. 

 

   

 Na štvrtom mieste sa umiestnil tím Faisceau de Lumière a hra „Dodgem“. Hra je vyrobená vo Flashi, takže si ju môžete zahrať aj bez sťahovania a inštalácie. Dve autíčka sa zrážajú a snažia sa navzájom vystrčiť z obrazovky, na obrazovke sa náhodne zjavujú pomocné predmety. Škoda slov, keď si to po jednom kliknutí môžete sami zahrať. Táto hra je podľa mňa z hľadiska hrateľnosti porovnateľná s prvým miestom. 

 Ešte dodám, že tento tím má iba jedného člena, ktorý má na starosti programovanie, grafiku aj hudbu – a zvláda to všetko veľmi dobre. V predchádzajúcich troch ročníkoch spolupracoval na úspešných hrách – minulý rok na hre „Espero“, ktorá získala prvé miesto, pred dvoma rokmi druhé miesto s hrou „Guľkáčik“ a pred troma rokmi druhé miesto s hrou „Gulf of Death“. A to je náš prvý tím z oktávy, s ktorým sa tento rok v súťaži Špongia rozlúčime. 

 

   

 Tretie miesto patrí tímu Burning Marek z gymnázia  Grösslingová (GAMČA) a ich hre „The Art of Stealing The Big Cyan Ball“. Hra je určená pre 2 až 4 hráčov, ktorí sa naháňajú autíčkami v aréne a snažia sa ukradnúť veľkú azúrovú loptu (ako naznačuje názov hry). Čím dlhšie sa hráčovi podarí udržať u seba loptu, čiže vyhýbať sa zrážkam s ostatnými hráčmi, tým viac bodov získa. Hra je fajn, akurát netuším, načo mali v tíme troch grafikov. 

 

 


 

     

 Druhé miesto obsadil tím Posledná Konštitúcia a hra „Yarivan“. Každý hráč má polovicu obrazovky, chodí po bludisku a strieľa potvory. (Ak ho potvory dostanú, nič vážne sa nedeje... o 20 sekúnd hráč ožije a môže pokračovať v hre.) Hráči musia niekedy spolupracovať tak, že jeden stojí na tlačidle, ktorým sa v inej časti bludiska otvárajú dvere a druhý tými dverami prejde. 

 To je náš druhý tím z oktávy, s ktorým sa tento rok v Špongii lúčime. Pripomeniem, že minulý rok sa tento tím takisto umiestnil na druhom mieste s hrou „Scion“ a podobne ako tento rok, rozdiel medzi prvými dvoma miestami bol dosť tesný. Niektorí členovia sa úspešne zúčastňovali aj predchádzajúce roky. 

 

   

 Prvé miesto získal tím DL Games a hra „Pigsty“. V tejto podivnej strieľačke prasiatko likviduje nepriateľov samopalom a pomáha mu pritom oblak, ktorý na nich zosiela blesky, dážď a sneh. Oblak je nezraniteľný, ale rôzne útoky ho môžu pripraviť o energiu a vtedy nemôže pomáhať prasiatku. Ak zomrie prasiatko, hra končí – a vaše spoločné skóre sa zverejní na internetovej stránke hry. To skrátka treba vidieť! 

 Toto je tretí tím z našej oktávy. Obaja „D. L.“ spolu robia Špongiu už štyri roky, mali pritom rôznych spolupracovníkov – minulý rok tiež skončili na prvom mieste s hrou „Espero“, pred dvoma rokmi na druhom mieste s hrou „Guľkáčik“ a pred troma rokmi to bolo štvrté miesto s hrou „Hawaii“. Programovací jazyk C/C++. 

 

 Tento ročník sme teda ukončili. Piaty ročník Špongie bude zrejme v októbri 2010. Naša škola príde o tri najsilnejšie tímy a je veľké riziko, že prvé miesto môže skončiť mimo našej školy. Ak ste teda študenti gymnázia a láka vás možnosť zúčastniť sa na tejto súťaži, budúci rok je ideálny. Poznačte si to do kalendára a sledujte webové stránky Špongie! 

