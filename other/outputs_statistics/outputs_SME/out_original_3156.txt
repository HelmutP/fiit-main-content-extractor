
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Makatúra
                                        &gt;
                Nezaradené
                     
                 Smrť štátnej správy pre životné prostredie 

        
            
                                    23.3.2010
            o
            15:45
                        (upravené
                23.3.2010
                o
                17:14)
                        |
            Karma článku:
                5.73
            |
            Prečítané 
            827-krát
                    
         
     
         
             

                 
                    V každej modernej Európskej krajine patrí životné prostredie k preferovaným odborom. Pre politikov je priam otázkou cti, aby sa ním zaoberali a svoje rozhodnutia filtrovali merítkom environmentálne ústretového postupu. Nedotlačila ich k tomu len verejná mienka, ale dlhodobý vplyv výchovy, morálky a priamej konfrontácie modernej spoločnosti s ekologickými problémami.
                 

                 Ako je to u nás? Štátna správa pre životné prostredie sa od svojho vzniku zmieta v neustálych reformách. Úradníci boli triedení v politicky motivovaných čistkách. Niektoré kauzy svedčia o masívnom ústupe záujmovým skupinám. V desiatkach príkladov je zrejmé, že naprieč celým systémom sa rúca chrbtová kosť nezávislosti a odbornosti. Napriek tomu, sektor životného prostredia stále predstavuje možnú morálnu opozíciu a tak zákonite prichádza snaha o jeho úplné paralyzovanie v rámci inej štruktúry verejnej správy. Stačí už len preorganizovať polopotentné úrady a dosadené kamarátske figúrky budú vydávať povolenia podľa pokynov mocných.   Odhliadnuc od zvrátenosti takejto predstavy, ktorá sa mohla narodiť iba v hlavách stále fungujúcich v inom čase myslím si, že je tu hlboké nepochopenie situácie. Aj keď- každý deň v prevrátenom režime je dobrý...   Dnes sa už nie je možné tváriť, že životné prostredie je len doménou územia nášho štátu. Okrem vážnych záväzkov podpísaných s Európskou úniou v prístupových zmluvách a iných medzinárodných dohodách parafovaných Slovenskom sme pod neustálou kontrolou nezávislých odborníkov z európskych štruktúr. Nie je možné počítať s tým, že si pod Tatrami nastavíme nižšie kritériá na ekológiu ako ostatní, že by bolo tolerované vylúčenie verejnosti z administratívnych procesov, nie je možné, že by túto krajinu bez zosmiešnenia reprezentovali úradníci bez orientácie v obore. V tejto chvíli sme ešte období popieraniania skutočného stavu, ale rezonuje to stále hlasnejšie. Diletantské a nenažrané obchodovanie s emisiami, pokus o zmenu povinností pre biodegradabilné odpady, preferovanie skládok, pokusy o vyblokovanie verejnosti z povoľovacích konaní úradov, rôzne metre na investorov, zhumpľovanie národného parku... a stovky ďalších kauzičiek na lokálnych úrovniach.   Nie zrušenie a programovaný rozklad verejnej správy v životnom prostredí, ale jej plná podpora, technické vybavenie, dostupnosť odborného vzdelávania, dostatočný počet výkonných pracovníkov a civilizovaný kariérny systém by pomohol zmeniť stav. Pomôže tiež len politický rešpekt k názoru odborne spôsobilého úradníka, ktorý je dostatočne zaplatený a fundovaný, aby si stál za vecou.   Na slovenských vysokých školách už vyrástla celá generácia ekológov. Nie sú deformovaní starými zvykmi, majú aktuálne vedomosti, ovládajú jazyky. Dokážu komunikovať s mimovládnymi skupinami aj zahraničnými expertmi. Je čas im dať šancu, nakoľko všetky pokusy riadiť sektor životného prostredia dinosarím spôsobom zlyhali. Domácej scéne to ešte veľmi nevadí, ale zo zahraničia už chodia veľmi výrazné signály a žiadosti o modernejší prístup.   Postupná smrť štátnej správy pre životné prostredie nebude znamenať voľné pole v prostredí, kde doteraz prekážala. Bude hlasným signálom, že Slovensko neplní mnohé environmentálne záväzky a drancovanie prírodných zdrojov stavia nad lásku ku krajine a k zdraviu ľudí. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            So sestrami sa hrá hra na hlúpych
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Kolaps parlamentnej demokracie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Slovensko neuživí toľko politikov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Krajina kde práca znamená prežívanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Makatúra 
                                        
                                            Surová doba alebo ako speňažiť objekt domova sociálnych služieb
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Makatúra
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Makatúra
            
         
        makatura.blog.sme.sk (rss)
         
                                     
     
        Elév-idealista

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    98
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1538
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Krajina kde práca znamená prežívanie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




