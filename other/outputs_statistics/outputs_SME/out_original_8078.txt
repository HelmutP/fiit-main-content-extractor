
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniel Dluhý
                                        &gt;
                Napísané udalosťami
                     
                 Záplavy pod Tatrami - Kežmarok 

        
            
                                    5.6.2010
            o
            2:10
                        (upravené
                29.1.2011
                o
                21:41)
                        |
            Karma článku:
                14.35
            |
            Prečítané 
            12026-krát
                    
         
     
         
             

                 
                    4.6. 2010 - Po nočnej búrke sa zobúdzam do nie menej búrlivého rána. Z okien kežmarských bytoviek vykúkajú ľudia, mnohí sa ponáhľajú preparkovať svoje autá. Bývam na sídlisku Juh. Ľubický potok sa tu ešte o 6:30 drží vo svojom 3,5 metra vysokom koryte. Nechýba však veľa, aby sa prelial. Nakoniec sa tak stalo.
                 

                 Prúdom odvlečené kontajnery a autá, zaplavené ulice. Voda ohrozuje  aj hrad v severnej časti mesta. Najhoršie sú na tom sídliská Juh,  Sever a priľahlá triapoltisícová obec Ľubica, ktorá je momentálne od Kežmarku takmer úplne odrezaná. Rieka strhla niekoľko mostov. Veľká časť mesta je pod vodou. Nefunguje elektrická sieť, televízia ani  internet. Stovky zvedavcov v pršiplášťoch postávajú na brehoch ulíc a niekoľkohektárových jazier. Požiarnici, mestskí aj štátni policajti po celý deň evakuovali obyvateľstvo z najviac postihnutých oblastí. Kežmarok nezažil záplavy najmenej 50 rokov. Od rána sa brodím vo vode, aby som túto mimoriadnu udalosť zdokumentoval.   7:08 - Hladina Ľubického potoka nebezpečne stúpa. Ľudia si medzi sebou dohadujú krízové scenáre.      7:10 - Voda dosahuje úroveň lávky, naráža do mosta a vylieva sa z koryta.         7:25 - Spodná časť sídliska Juh je pod vodou. Deti sa nemôžu dostať do školy, ani naspäť domov.      7:39 - Ľudia, ktorí sa potrebujú dostať do práce alebo len preparkovať auto tak robia s vypätím všetkých síl.         7:42 - Chlapec so smútkom na tvári tlačí svoje vozidlo po kolesá vo vode. Odkladám fotoaparát a pomáham mu. Auto sa nám podarí odtlačiť na plytčinu a naštartovať. Otvárame dvere a von sa lejú litre vody. Zostáva jej tam ešte po členky.         7:45 - Z hypermarketu Tesco sa stáva ostrov. Brodím sa vodou až ku vchodu. Strážnik odmieta vpustiť ma dovnútra.         7:48 - Najväčšia pekáreň v Kežmarku je zatopená a vďaka strhnutému mostu odrezaná od zbytku mesta. Zvyčajné piatkové dodávky chlaba sa do obchodov nedostávajú. Dvaja občania ešte stihli nakúpiť zemiaky.               8:00 - Voda mi siahá nad kolená. Dostávam sa k českým kolotočom, ktoré do Kežmarku pricestovali len pred niekoľkými dňami. Majitelia zachraňujú vybavenie maringotiek. Rozložili ich na nížine pri starom futbalovom štadióne. Tam dole hladina siaha až po hruď.      8:23 - Rozvodnený potok sa valí z Ľubice smerom na hrad a sídlisko Sever. V pozadí Nový evanjelický kostol, ktorý je mimo ohrozenia.         8:25 - Jedno z miest, kde sa Ľubický potok z koryta vylieva v najväčšej miere. Mestská časť Kamenná baňa je odrezaná od zvyšku Kežmarku. Požiarnici onedlho nasadia motorové člny a začnú s evakuáciou.      Na tomto mieste ma pod vplyvom vlhkosti zrádza baterka fotoaparátu. Ďalej pokračujem len s objektívom telefónu. Bohužiaľ, doteraz sa mi z neho nepodarilo vytiahnuť ani jednu fotografiu. Prší a ja prechádzam mestom pozdĺž zatopenej ulice Trhovište. Míňam zaplavený futbalový štadión. Práve na tejto ulici sa nachádza aj rekonštruovaný hokejový štadión, na Slovensku známy vďaka kauze zrušených Deaflypijských hier. Už aj tak ohrozenej rekonštrukcii záplavy nerobia najlepšiu službu. O niekoľko metrov ďalej sa dostávam k budove okresného súdu. Z jednej strany ho obteká prúd asi meter vysokej vody. Na druhej strane trojmetrová rieka. Pracovníci sa nemôžu dostať z budovy von, požiarnici s nimi preto komunikujú zo striech protiľahlých garáží. Dážď na chvíľu ustal. Pravdepodobne najhoršia situácia je na sídlisku Sever a v blízkosti hradu, kde sa novovytvorená rieka z ulice Trhovište opäť zlieva s Ľubickým potokom. V tomto čase malo po hrade návštevníkov sprevádzať ľubické divadlo ExTeatro - herci však pomáhajú so záchrannými prácami. Jedno z hradných nádvorí je zatopené. Všetci, niektorí s tvárami namaľovanými a prichystanými na predstavenie, vynášajú materiály z  knižnice na prízemí, ktorej rovnako hrozí záplava. Znovu začína pršať. Pokúšam sa dostať priamo na sídlisko Sever, no nejde to. Prúd je natoľko silný, že otvára brány záhrad a vynáša strechu altánku. Dovidím však na bytovky. V diaľke, len niekoľko centimetrov nad hladinou vytŕčajú strechy áut. Medzi ľuďmi sa rozniesla správa, že pri evakuácii prišiel o život záchranár.  Prechádzam cez park za hradom, asi sto metrov nižšie. V susednom parčíku sa nachádza pamätník - tank z druhej svetovej vojny. Z vody trčí len on a malá časť asi 1,6 metra vysokého podstavca. Blízka čerpacia stanica je až do polovice okien vo vode. Na diktafón nahrávam rozhovory s vyľakanými obyvateľmi - zvesť o mŕtvom záchranárovi sa ukazuje ako nepravdivá. O nič lepšia nie je ani situácia pri Tatraľane, na konci mesta. Neďaleko sa Ľubický potok vlieva do rieky Poprad. Našťastie, až na nižšom toku - možno preto sa "Popradka" v meste ešte stále drží svojho koryta. Blízke kasárne sú aj napriek tomu celé pod vodu. Rovnako zaplavený je aj dvor Základnej školy Nižná brána, odkiaľ niekoľko detí previezli na člnoch. Šialený Jožo Kubáni sa snaží dostať na jeden z nich, požiarnici mu v tom bránia. Preto sa neskôr, aby to malo grády, ponorí s kamerou po hruď do vody. Asi trinásťročný chlapec mi hovorí, že onedlho má od Popradu prísť prívalová vlna. Našťastie sa nedostavila. Je krátko predpoludním. Začínam svoju púť domov - na druhý koniec mesta, na sídlisko Juh. Okolo mňa húkajú sirény sanitiek a požiarnych áut. Atmosféra pripomína katastrofický film. Na krátko sa zastavujem v jednych z mála otvorených potravín. Takmer všetko pečivo je vypredané. Ľudia si robia zásoby. Na Juhu voda mierne klesla, hladina je však ešte stále pomerne vysoká. Od ľudí postávajúcich v strede cesty sa dozvedám, že ak chcem, môžem sa ku svojej bytovke dostať celkom jednoducho. Obyvateľov totiž rozváža bager. Žltý - ako taxík. Spolu s ďalšími piatimi ľuďmi celí premočení stúpame na radlicu. Tá sa dvíha do vzduchu. Medzipristátia máme na uliciach Košická a Lanškrounská. Cestujúci vystupujú a ja zostávam sám. Meter a pol vo vzduchu, podomnou parkovisko zmenené na rieku. Vodič bagra pridáva plyn. Kvapky dažďa mi narážajú do tváre. Jednou rukou sa pevne držím radlice, druhou kývem pani vrátničke kežmarského gymnázia stojacej na brehu. Asi dostala voľno. Niekoľko ľudí kýva mne. Mám namierené na Bardejovskú ulicu. Domov. Vodičovi kývnem, nech odbočí doprava. Vysadí ma priamo pred vchodom. Ďakujem mu zdvihnutou rukou a stroj sa vzápätí otáča, aby doviezol ďalších ľudí. Je päť hodín večer. Hľadím von cez otvorené okno. Voda opadla. Na parkoviskách stojí niekoľko áut omotaných trávou. Nejaký muž sa snaží chytiť rybku uväznenú v mláke. Chodníky a cesty, teda to, čo z nich zostalo, takmer nevidno. Prekrýva ich hrubá vrstva naneseného blata. Po chvíli nabieha elektrický prúd, no káblová televízia a internet ešte stále nefungujú. Rovnako ani teplá voda. Využívam situáciu a zapínam rádio. Správy sú zaplavené informáciami o povodniach na celom Slovensku. Všade prišli rýchlo a možno aj nečakane. Už len vydržať.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (28)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Dluhý 
                                        
                                            Točíme!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Dluhý 
                                        
                                            Mám pod nohami Bratislavu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Dluhý 
                                        
                                            Na slovenskej samote
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Dluhý 
                                        
                                            Jeseň na dvore
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniel Dluhý
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniel Dluhý
            
         
        dluhy.blog.sme.sk (rss)
         
                                     
     
        Kežmarčan, študent réžie dokumentárnych filmov na Filmovej a televíznej fakulte VŠMU.
 

        
      
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3677
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje fotenie
                        
                     
                                     
                        
                            Napísané udalosťami
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




