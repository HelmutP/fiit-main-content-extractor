

 Prievidza 10. marca (TASR) - Hnutie za demokratické Slovensko
- ľudová strana (HZDS) podľa jej podpredsedu Jozefa Božika nepovažuje
prezidentom navrhované vyššie územné celky (VÚC) 3+1 za optimálne
a nebude o takom variante rokovať.  

 Dialóg o reforme verejnej správy však HZDS chce viesť so
všetkými politickými stranami, ktoré participujú na správe vecí
verejných. Bude však presadzovať vznik ôsmich VÚC. Božik to dnes
uviedol v Prievidzi, kde sa zúčastnil na prvých regionálnych oslavách
10. výročia vzniku HZDS. 

 Ako zdôraznil, model osem VÚC je z hľadiska efektívnosti
vynakladania finančných prostriedkov i dostupnosti štátnej správy
a samosprávy občanom najvýhodnejší. HZDS považuje konsenzus o reforme
verejnej správy medzi koalíciou a opozíciou za veľmi dôležitý,
pretože reforma ovplyvní život občanov na mnoho rokov. Božik verí, že
vládne strany konečne prekročia svoj tieň a sadnú si s opozičnými
stranami za rokovací stôl. 

