

 Uplynulo 31 rokov úžasnej práce s deťmi, práce, na ktorú som sa denne tešila a nebolo dňa, kedy by som nešla s radosťou do práce. 
 Deti mi dávali veľa energie a ja som ich zbožňovala. V čase, keď som „riaditeľovala" v našej materskej škole som si povedala, že mám dosť tých papierovačiek (vôbec nie detí) a dala som výpoveď. Keď mi nejako osudovo nevyšla práca, na ktorú som sa chcela dať, napadlo ma vzápätí, že sa  musím stať opatrovateľkou a robila som preto všetko, aby som sa ňou stala. Prišiel čas, kedy som si povedala, že lásku a cit od detí odovzdám tým, čo ju budú potrebovať. Urobila som si opatrovateľský a masérsky kurz a učila som sa nemčinu. 
 Nie že by som nenašla prácu v nejakom domove dôchodcov, o to som sa ja neusilovala nijako zvlášť, pretože ja považujem prácu s človekom na dôstojnej úrovni len individuálnu, rozhodla som sa pre prácu v zahraničí, kde je lepšie finančne ohodnotená. 
 Prešiel takmer rok a ja som nepociťovala za celý ten čas strach, že sa nezamestnám. Moja rodina bola z toho nervózna, každý ma chcel nejako finančne podporiť, ale ja som nechcela a nechcela som byť ľutovaná. Zato som si nekonečne snívala o mojej babke, či dedkovi, o ktorého sa budem starať. Moje sny začali fungovať v nemčine, ale presne takej ako som ja ovládala, teda v základoch nemčiny. 
 Zaregistrovala som sa do niekoľkých agentúr a až tá posledná mi raz zavolala a povedala, že si ma vybrala rodina v Berlíne. Opýtali sa ma, či som ochotná starať sa o pána ležiaceho a na vozíku. Súhlasila som a dobre som urobila. 
 Prišla som do Berlína a zistila som zvláštne veci. Sú len náhodou? 
 Prišla som do domu, ktorý má rovnaké číslo ako je na našom dome. Pani má rovnako detí - dve dcéry a jedného syna presne tak ako my. Ich syn sa narodil presne v ten istý deň aj rok ako ten môj. Ich posledná dcéra je rovnako nadaná na výtvarné umenie ako naša posledná. 
 Prijali ma napriek tomu, že moja nemčina nie je na jednotku, ani dvojku. 
 Pán, o ktorého sa starám je úžasný. Napriek rečovému handicapu, je po porážke 4 roky, ma učí po nemecky. Dokonca, ak urobím vo vete chybu, ma aj opraví. Úžasná vec. Hráme sa so slovíčkami opačného významu a vymýšľame spoločne vety. Volám ho môj pán učiteľ a ja som jeho žiačka. 
 No a ja za to, že mi takto nekonečne pomáha, s láskou sa o neho postarám a odovzdám mu všetko, čo som získala od mojich detí z materskej školy. Aj ony boli pre mňa takými učiteľmi. Učiteľmi vnímania citov a pocitov, učiteľmi lásky. Lásky takej normálnej, ľudskej, spontánnej. Oni ma naučili, že pohladenie a objatie sú dve veľmi dôležité veci v živote každého človeka. 
 Prvé objatie v Berlíne som dostala za to, že som pani domu požehlila prádlo. Okrem neho aj nekonečnú vďačnosť. Počula som, že Nemci sú odjakživa povýšeneckí k slovanským národom. Nie je to pravda. Ľudia sú všade rovnakí - dobrí aj zlí, citliví aj necitliví, úprimní aj neúprimní... 
 Ja som verila, že prídem do rodiny, kde je veľa lásky a nikdy som si ani na chvíľku nedovolila zapochybovať, že to bude ináč. 
 Ak mi niekto povedal, že prácu len tak nenájdem, povedala som mu: „Boh ma má rád, on sa o mňa postará, avšak je nutné preto niečo urobiť." A ja som aj urobila. Presne to, čo som mala... 
 Ak žijete práve v strachu, že sa nezamestnáte, skúste snívať, nikdy nespochybňujte svoje sny a spýtajte sa svojho vnútra, čo preto treba urobiť. Ono vám odpovie, ak ho budete dobre počúvať. A potom stačí len riadiť sa vnútrom. 
 Ja som tak do roka získala prácu, ktorá bude znamenať  pre mňa zas VŠETKO... 
   

