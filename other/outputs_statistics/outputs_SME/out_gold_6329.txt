

 Vo všeobecnosti sa dajú darčeky pre ženu zhrnúť do štyroch základných kategórií:  Prvá kategória je stáročiami overená a osvedčená klasika. Sem patria šperky, kozmetika, kvety... V tomto prípade je úspech zaručený, veď už ste niekedy počuli o žene, ktorá by povedala, preboha , načo sú mi ďalšie náušnice, veď ja už nejaké doma mám. Alebo počuli ste niekedy o žene, ktorá by povedala, že nechce nový parfum, pleťový krém či šminky? O kvetoch ani nehovoriac, takže tu niet čo pokaziť. 
 V podstate bezproblémová sa dá nazvať aj kategória druhá a to sú darčeky, ktoré by sme mohlo označiť ako užitočné. Je síce pravda, že ak žena rozbalí darček a nájde tam kľučky na dvere do vášho spoločného práve prerobeného bytu asi veľmi od radosti skákať nebude, ale to ju prejde, keď  budú kľučky namontované a ona už nebude musieť otvárať dvere na záchode šraubovákom. Do tejto kategórie by sme mohli zaradiť napríklad aj taký nový výfuk na auto, hlavne ak ten starý odtrhla ona, keď si chcela skrátiť cestu z parkoviska cez obrubník. 
 Opatrnejší by som však bol pri tretej kategórií a to sú darčeky, ktoré kupujeme ženám, lebo ich chceme my. Tu si to už vyžaduje nejakú prípravu a určité komunikačné schopnosti. Neslobodno to zanedbať, lebo ak je vaša žena technický analfabet a dostane najnovšiu digitálnu zrkadlovku od svetoznámeho výrobcu fotoaparátov a k tomu zopár objektívov, polarizačné filtere a statív, je dobré mať celú záležitosť dôkladne premyslenú a naplánované nejaké odôvodnenie.  Aj na takú vŕtačku s príklepom a novotou sa lesknúce vrtáky môže pozerať s vypleštenými očami, preto je dobré mať po ruke pripravené nejaké vysvetlenie. Nikdy, opakujem nikdy prípravu neodfláknite s tým, že vás hádam niečo napadne priamo na mieste. Nenapadne, verte mi. Vtedy je už neskoro. A okrem toho neistý, koktajúci muž s kvapkami potu na čele nevyzerá veľmi dôveryhodne. 
 Najnebezpečnejšou kategóriou, a tu by som bol obzvlášť opatrný, je štvrtá kategória. Sem patria darčeky, ktoré sme zvolili len tak, na poslednú chvíľu, bez rozmyslenia a v rukách sklamanej ženy môžu  poslúžiť ako zbraň. Je pravda, že ako zbraň môže poslúžiť snáď každý predmet, ale pravdepodobnosť, že vás rozrušená žene trafí s pamätným tanierom k osemstopäťdesiatému výročiu prvej zmienky o vašom meste, ktorý ste jej dali k narodeninám je dosť malá. Uznajte. A o  rane reklamným vankúšikom s logom vašej firmy, ktorý jej zabalíte ako darček k výročiu svadby ani nehovoriac. V strehu však treba byť v prípade takej sady kutáčov na stojane ku krbu obzvlášť, ak žiadny krb nemáte, v prípade elektrického predlžovacieho káblu, či valčeka na cesto, lebo z takýchto darčekov sa môže vykľuť ten povestný bič, ktorý plieska na konci, tak bacha, aby  neplieskal na vašom chrbte. 
   
   
   
 ...a keď tak premýšľam nad tou štvrtou kategóriou, v hlave sa mi preháňajú myšlienky, vety sa prevaľujú jedna cez druhú, slová sa samé ukladajú na seba ako kocky lega a ja som si spomenul na môjho kamaráta Jana... 
   
  Jana vám raz napadlo  kúpiť Anči švihadlo. Cez švihadlo skoky zoštíhľujú boky, tak sa hádam poteší figúru si vylepší. 
   
 Švihadlu sa potešila, že po takom vždy túžila a teraz keď Jano pije Anča ho švihadlom bije a z ruky ho nepustí kým ju zlosť neopustí. 
   
   
   

