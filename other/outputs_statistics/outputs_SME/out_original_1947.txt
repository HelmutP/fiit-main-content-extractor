
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marcel Burkert
                                        &gt;
                Zahraničná politika
                     
                 Nadávať na USA je "in" 

        
            
                                    8.4.2010
            o
            7:44
                        (upravené
                30.6.2014
                o
                22:39)
                        |
            Karma článku:
                12.13
            |
            Prečítané 
            2051-krát
                    
         
     
         
             

                 
                    Medzi tvrdých kritikov USA patrí aj môj dobrý kamarát, ktorý tam po revolúcii odišiel za lepším životom. Nedbal na všetky tie varovania, ktoré v časoch neobmedzenej komunistickej moci často počúval, konkrétne, že ide o prehnitý kapitalizmus, krajinu imperialistov, vykorisťovateľov a žobrákov. Dnes je obhajcom štátu, ktorý nie len nám na desaťročia implantoval totalitný režim a rozhodujúcou mierou stál za tankistickým potlačením pokusu o zmenu. Dokonca popiera, že by úroveň dodržiavania základných ľudských práv bola v Rusku iná než v USA... 
                 

                     Nuž je zaujímavé, že nikto neuteká, ani nikdy neutekal za lepším životom do Ruska, Bieloruska aj keď tieto sú bližšie ako USA. Bližšie sú nám aj kultúrne aj jazykovo. Navyše Rusko, minimálne čo sa týka surovín, je veľmi bohatá krajina. Odhadujem, že 99 ľudí zo sto, ak by si malo vybrať, uprednostnilo by zrejme žitie v USA pred Ruskom, alebo pred Čínou, ďalšou "demokratickou a ohľaduplnou" veľmocou. A to nie len z ekonomických dôvodov, ale aj pre rozdielnu úroveň dodržiavania základných ľudských práv, vymáhania práva, korupcie, byrokracie a možností uplatnenia sa.   Napriek tomu USA jasne vedie v rebríčku najnepopulárnejších krajín.   Ľuďom na Američanoch asi najviac prekáža ich zahraničná politika. Nechcem ju obhajovať, len si myslím, že ak by takúto moc a vplyv, akú majú dnes USA mali Čína, Rusko, alebo Irán, je veľmi otázne, či by túto moc nezneužívali ešte viac. Môžme si napr. spomenúť na pomery v povojnovom Západnom a vo Východnom Nemecku. Ako je známe, v NSR bola demokracia a prosperita, aj keď bola v područí  nenávidených USA, ako jednej z víťazných mocností. Na strane druhej, v NDR, ktorá bola v područí Ruska, resp. ZSSR, nielen že životná úroveň bola oveľa nižšia, ale strieľali tam zo strachu na hraniciach tých, čo chceli újsť do "prehnitého kapitalizmu", a to len preto, aby nemohli svedčiť, že tento nie je až tak prehnitý ako ho chceli vidieť predstavitelia komunizmu. Že sme zaťažení, čo sa týka mocností, iba, alebo hlavne na Američanov, tomu nasvedčuje napr. aj to, že vpád Číny do Tibetu a následné čistky na demonštrantoch neboli našimi politikmi hádam vôbec odsúdené, ale zásahy NATO na Balkáne alebo zásah USA v Iraku boli mnohými z nich kritizované alebo aj odsúdené. Pritom Tibet nebol podozrivý ani z držby jadrových zbraní ani tam nedochádzalo k vraždeniu civilistov. Išlo čisto o zištné obsadenie krajiny. Nehovoriac o tom, že pokútne dovážaný čínsky tovar už dopomohol k bankrotu nejednej našej odevnej firmy. Stále nám však v žalúdku ležia Američania.   Slovensko nikdy svoju moc navonok nezneužívalo, ale nemalo na to ani možnosti. Vo vnútri štátu sa však moc, čo pamätám, vždy zneužívala a využíva ako sa len dá. Preto si len ťažko predstaviť čo by ľudia našich kvalít vystrájali ak by sedeli v americkej vláde...   Môj synovec mi raz v súvislosti s výlučne negatívnymi vyjadreniami Jozefa Ráža na adresu Američanov, povedal, že nemá pocit, že ak sa niekde objaví ropa tak tam Američania napochodujú a jednoducho si ju vezmú, ako to tvrdil Ráž. Má pocit, že za ňu riadne platia. Irak zďaleka nebol nevinnou krajinou, aj keď sa tam jadrové zbrane nenašli. Takéto intervencie by si žiadalo urobiť aj v iných krajinách. Oficiálne a skutočné dôvody intervencií sú naozaj rozdielne veci, ale postup USA väčšinou podporila napr. aj naša krajina. Američania ťahajú Európu z kaluže, do ktorej sa stále Európa dostála vlastnou vinou. Prvá a druhá svetová vojna sú toho príkladom. Studená vojna je ďalším príkladom. Ak Jozef Ráž sympatizuje s Rusmi, lebo sú inteligentní, resp. každý z nich dokáže niečo hodnotné zarecitovať, pričom žiadneho blbca tam údajne nestretol, treba sa pozrieť aj na to, čo národ, ktorý na počkanie dokáže recitovať Puškina urobil s východnou Európou za posledných 50 rokov. A čo urobil zo seba za posledných 90 rokov napriek možnostiam a bohatstvu ktoré má?   "Títo hlúpi a tlstí Američania, za Európanov položili nemálo životov. Museli to robiť? A aj keby v tom bola určitá vypočítavosť, životy jednoducho položili, a položili ich zväčša jednoduchí ľudia." Rusi neboli vypočítaví, keď po 2. svetovej vojne nastolili v mnohých okolitých krajinách svoj totalitný režim? To zas nebolo až tak dávno. Navyše k zmene ich donútili pomery. Neurobili to na základe vlastného osvietenia.   "Títo hlúpi Američania majú v zozname 500 univerzít sveta v prvej desiatke 6 univerzít. Žiadna slovenská univerzita sa do toho zoznamu ani len nedostala. V tej prvej desiatke sú len dve európske univerzity. A tieto dve univerzity sú univerzity z Veľkej Británie, krajiny ktorá je kultúrne podobná USA asi najviac. Z tejto krajiny obéznych ľudí je možno úplne najviac držiteľov olympijských medailí. Z tejto krajiny hlupákov vychádza väčšina držiteľov Nobelových cien. V tejto krajine prebieha najviac vedeckých výskumov na svete. Že Rusi majú podľa Ráža najlepšie zbrane na svete? No to je mi prvenstvo... Popravde, ktorých zbraní  v rukách sa mám viac báť? Zo skúseností tu ani nie je o čom rozmýšľať. Nehovoriac o tom, že v časoch keď v Rusku treli bežní občania biedu, ich "slobodne" zvolená vláda vrážala miliardy do zbraní. Údajne preto, aby ich nemohlo ohroziť NATO, do ktorého patrí dnes už aj SR a mnohé ďalšie krajiny z područia bývalého ZSSR."   "Možno tá kritika Američanov pramení aj zo závisti ktorá je v našom národe akoby zakódovaná. Najprv sme nadávali na Čechov, keď to bolo aktuálne, teraz nadávame na Maďarov, Česi nadávajú na Nemcov a spoločne všetci nadávame na Američanov. Pritom z 90%  pozeráme americké filmy, počúvame americkú muziku, využívame predmety, ktoré boli vynájdené Američanmi. Najpredávanejšie nealkoholické nápoje a občerstvenie sú americké. Nikto nás nenúti tieto veci užívať a spotrebovávať." Je síce pravdou, že USA veľmi veľa spotrebuje a vyprodukuje veľké množstvo odpadov, ale položme si otázku, či by sme boli iní my, ak by sme boli na takej istej životnej úrovni, ak by sme si mohli dovoliť to, čo si môžu dovoliť USA. Čo sa týka odpadov, určite nejeden z Vás si všimol tie tony odpadkov, plastových fliaš a pod. okolo našich ciest, nespočet čiernych skládok dokonca pri našich riekach atď.   Je zaujímavé, že optimizmus Američanov nezlomili ani strádajúce automobilky, ktoré rok za rokom vytláčajú z vlastnej krajiny draví japonskí výrobcovia, ani pád energetického giganta Enron, krach bánk a iné. Navzdory týmto a aj iným problémom (vysoké zadĺženie štátu...) americká ekonomika stále patrí k svetovej špičke a to najmä vďaka vojenskému priemyslu, oblasti technológií, počítačových softwérov a strategicky cenných procesorov, kde u posledných dvoch „položiek" americké firmy údajne ovládajú až 90% svetového obchodu. Tvrdí sa, že mladý americký národ vďačí za svoje výkony tomu, že odmietli veľkú časť zväzujúcich európskych tradícií. Američania nikdy nepoznali silu socializmu a teda ani lákavé sľuby o štáte - opatrovateľovi, ktorý sa o všetkých spravodlivo postará. Namiesto čakania na zázrak nastúpilo krédo tvrdej práce, spoliehanie sa na vlastné schopnosti a rovnosť príležitostí. Celá stovka z terajších cca 490 amerických miliardárov údajne vyrástla z úplnej nuly. Vie si to niekto predstaviť u nás, že by to išlo bez známostí a pod.? Američania vedia, že vďaka niekoľkým ťahúňom (po našom „bezohľadných nenažratých kapitalistov"...) bohatne i zbytok krajiny.  Lebo kto vie zarábať, nenechá ležať peniaze ľadom. Investuje, vyrába, zamestnáva a prepúšťa.   Samozrejme, vidím aj negatíva USA, ale tie sú vo všeobecnosti tak známe, že písať o nich by nemalo význam; zvlášť keď to vedia oveľa lepšie mnohí iní. Záverom: Zaujímalo by ma, či by naši určití politici boli takí lojálni a servilní Rusku,  ak by ropa a plyn neprúdili z východu, ale z USA a ako by sa v takom prípade vyjadrovali alebo nevyjadrovali o tejto krajine v súvislosti s hospodárskou krízou.   Použitá lit. pri predposlednom odseku: USA západ, Radek ADAMEC, vydavatel: FREYTAG &amp; BERNDT.   Súvisiaci článok: http://burkert.blog.sme.sk/c/225381/ucit-sa-mozme-dokonca-aj-od-americanov.html 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (192)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Východ najzápadnejšieho Orientu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Komu hlavne slúži mestská polícia. Napr. v bratislavskej Dúbravke
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            B. Dúbravka a krásy D. Kobyly a okolia XV - XVI. Plus retro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Aspoň na bilbordoch som "nezávislým"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Kto chce občanom slúžiť, musí si ich najprv kúpiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marcel Burkert
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marcel Burkert
            
         
        burkert.blog.sme.sk (rss)
         
                        VIP
                             
     
         Spoluzakladateľ OZ Stop alibizmu, učiteľ, príležitostný turistický sprievodca po horách a krajinách a od 15.12.2014 poslanec miestneho zastupiteľstva BA-Dúbravka za stredopravú koalíciu. Prvé články na blogu SME zverejnil v júni 2007: "Mýty a zvrátenosti vo vzdelávaní" a "Matka prírody - Boh materialistov". Od r. 2010 prispieva aj na blog spomínaného OZ.  
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    276
                
                
                    Celková karma
                    
                                                7.07
                    
                
                
                    Priemerná čítanosť
                    2342
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Výroky na zamyslenie 11/2014
                        
                     
                                     
                        
                            Rómska otázka
                        
                     
                                     
                        
                            Duchovné otázky
                        
                     
                                     
                        
                            Polícia s.r.o.
                        
                     
                                     
                        
                            Marenie výkonu cestnýh kontrol
                        
                     
                                     
                        
                            Školstvo a výchova
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Muž a žena
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Politika, spoločnosť, mediácia
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Ochrana a bytosti prírody
                        
                     
                                     
                        
                            Environmentálna kriminalita
                        
                     
                                     
                        
                            Cesty z bludného kruhu
                        
                     
                                     
                        
                            Dúbravka, Bratislavsko
                        
                     
                                     
                        
                            Krásy Devínskej Kobyly
                        
                     
                                     
                        
                            Potulky Slovenskom a ČR
                        
                     
                                     
                        
                            Potulky po Európe a okolí
                        
                     
                                     
                        
                            Potulky po USA
                        
                     
                                     
                        
                            Potulky maratónske
                        
                     
                                     
                        
                            Šport netradične
                        
                     
                                     
                        
                            Zvrátenosť vrcholového športu
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ako eliminovať vlády s.r.o.
                                     
                                                                             
                                            Ako prichádza polícia o produktívnych policajtov
                                     
                                                                             
                                            Manželské sexuálne povinnosti??? Duchovný pohľad
                                     
                                                                             
                                            Hodnotová slepota - Rusko vs. USA
                                     
                                                                             
                                            Manuál mladého konšpirátora
                                     
                                                                             
                                            Rodinné prídavky len na 2 deti=1. krok k riešeniu rómskej otázky
                                     
                                                                             
                                            21 zvrátenosti v Policajnom zbore
                                     
                                                                             
                                            Sonda do duše politika
                                     
                                                                             
                                            Museli sme zabiť Božieho Syna, aby sme mohli byť spasení...
                                     
                                                                             
                                            Čím neškodnejšia polícia, tým väčšia korupcia
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vo svetle Pravdy
                                     
                                                                             
                                            Zaviate doby sa prebúdzajú
                                     
                                                                             
                                            Kráľ Lear
                                     
                                                                             
                                            Sokrates (od D. Millmana)
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Rudolf Pado
                                     
                                                                             
                                            Martin Škopec Antal
                                     
                                                                             
                                            Ján Macek
                                     
                                                                             
                                            Radovan Bránik
                                     
                                                                             
                                            Miroslav Kocúr
                                     
                                                                             
                                            Marek Strapko
                                     
                                                                             
                                            Ján Galan
                                     
                                                                             
                                            Ján Žatko
                                     
                                                                             
                                            Peter Konček
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Slavomír Repka
                                     
                                                                             
                                            Jozef Kamenský
                                     
                                                                             
                                            Michal Legelli
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                             
                                            Boris Kačáni
                                     
                                                                             
                                            Vlkáč Juraj Lukáč
                                     
                                                                             
                                            Smädný pútnik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Svet Grálu
                                     
                                                                             
                                            O cigánoch. Jozef Varga
                                     
                                                                             
                                            Môj iný blog
                                     
                                                                             
                                            Dúbravčan
                                     
                                                                             
                                            Šachový klub Dúbravan
                                     
                                                                             
                                            Na skládky nie sme krátki
                                     
                                                                             
                                            Lesoochranárske združenie VLK
                                     
                                                                             
                                            JUDr. Ing. Ivan Šimko
                                     
                                                                             
                                            Roman Kučera - cestovateľ
                                     
                                                                             
                                            Toulky
                                     
                                                                             
                                            Bežecké dojáky
                                     
                                                                             
                                            Vitariánska reštaurácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hrozba vojnového konfliktu nie je zahranično-politickou otázkou! Váš minister zahraničia
                     
                                                         
                       Zákaz bilbordovej kampane - prvý krok k zrovnoprávneniu šancí kandidátov
                     
                                                         
                       Nechci zmeniť celý svet. Zmeň len svoje okolie
                     
                                                         
                       Dear John, odchádzam z blogu SME
                     
                                                         
                       111 krokov, ako vrátiť Bratislavu Bratislavčanom
                     
                                                         
                       Páni Milanovia, mňa nezastavíte
                     
                                                         
                       Tak oblečme si dresy, ale ...
                     
                                                         
                       Sonda do duše politikov a voličov
                     
                                                         
                       Ficov minister posadil svojho šéfa do bytu Goríl a on neprotestuje!
                     
                                                         
                       Je potrebné deštruovať ľudskú blbosť a nie volať po rekonštrukcii štátu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




