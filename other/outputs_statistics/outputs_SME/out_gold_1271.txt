

 — Aká je to pesnička? — spytujú sa rad—radom.
 — Aká? Neviete? — diví sa mamka. — Nuž volebná. Veď budú voľby! 
 — A načo? — naťahujú chlapci krky. 
 — Načo — načo! Nuž na ablegáta. 
 — Hej, ja už viem, — hlási sa Maroš. — Taký pán pôjde potom do Pešti. 
 — A čo tam bude robiť? — zvedavé sú deti ďalej. 
 — Bojovať za nás, Slovákov, na sneme! — vykladá chlapec. — I za naše pesničky, aby sa všade mohli spievať. 
 — A sa nemôžu? — divia sa detváky. 
 — Nie veru! Len maďarské… 
 — Kde si to ty nabral? — pozrie mať na syna. — Kto ti to všetko povravel? 
 — Kto? Otcovi prišli novinky. Vyčítal som — ja to viem, — vypne sa hrdo, — naším ablegátom nebude Kapušányi, ale pán Rubík. On je advokátom v meste, ten bude! 
 — Hej, ale ho najprv musíme vyvoliť, — ohlási sa mamka. — Vyvoliť, Maroš. 
 — Veď ho my vyvolíme! 
 — Vy? Ha—ha—ha! — nevie sa zdržať smiechu. — Čo by ste vy koho vyvolili? 
 — Vyvolíme! 
 — Na to sú starí, čo majú vóta. 
 .... 
 — Ujec, a kde idete? — volajú na chlapa, keď vidia, že má namierené do Glückovie krčmy, kde sa čosi kutí, o čom nikto nesmie vedieť, zato každý vie. — Kde idete, ujec? Nechoďte ta! 
 — Veď ja len tak — skúsiť! — odkašle si. — Len tak! 
 — Len tak — hej, len tak! — kričia deti. — Len aby to nebolo naozaj! 
 ............ 
 I v Mosticiach sa nájdu ľudia, o ktorých sa rozchýri, že sa sľúbili za pálenku. A to vraj i niekoľkí dobrí gazdovia a pltníci, keď pán Glück má i obchod s drevom a pltníči. Lalovie ujec, i Mrázikovie sú vraj tiež medzi nimi, ba i ujec Stračko z Nižného konca. 
 Chlapci vidia, ako to búri mysle. Maroš doma nemôže sa skoro mamke priznať, najmä odkedy sa spomína i starý otec Stračkovie medzi obchodníkmi. ...Uvedomí si, že i oni, chlapci, musia zasiahnuť do toho. To sa aj ich týka, keď i nemajú vóta! 
 ................................ 
 Ľudia prídu a idú hlasovať. Všíma si, ako sú všelijako oblečení, i hovoria jedni tak, druhí trochu inakšie. A jednako ich čosi spája. 
 — Nech žije Miško Rubík! 
 — Nech žije! 
 — A ja chcem pálenky! Dajte mi ešte pálenky! — stavia sa ženám akýsi chlap z horných dedín, trochu už drgnutý. 
 — Máte dosť, ujec! Máte dosť! — chlácholí ho mamka. 
 — No, keď nedáte, idem ta, kde dajú! — odsotí ju od seba. 
 — Počujte, ujec, a sa nehanbíte? — volá Maroš za ním. 
 A príde mu na myseľ jeho vlastná pesnička o tej starej svini. — Nehanbíte sa? 
 — No dobre, dobre! — zmotá sa chlap a ide medzi svojich. 
 Takto sa hlasuje do večera — hlasuje do pozdnej noci. 
 Úryvok v plnom znení nájdete tu: Martin Rázus Maroško 
 ................................................................  
 P.S. Jeden z obrazov dnešných dní... Príde chlap v dedine do krčmy, objedná pivo a frťana a dá sa do debaty. Po viacerých pohárikoch a vášnivej debate, buchne do stola a zakričí:
 
 — To by som sa pozrel na to, aby nám tu nejaká sliepka  rozkazovala! Sliepky patria na dvor, nech tam hrabú a nech tam nerobia zmätok medzi normálnymi ľuďmi! - a za pritakávania ostatných ešte dodá:  
 — Určité dievky neukojené majú určité snahy sa realizovať, pretože sa nevedeli ani poriadne vydať, ani porodiť nejakého poriadneho slovenského chlapca... 
 Pridajú sa ďalší, poháriky štrngajú a ozýva sa hurónsky rehot. 
 — Kto sa obetuje, zrelí muži?! Lebo, ak sa nikto neobetuje, ona s tým neprestane. Niekam tú nadbytočnú energiu vraziť musí... Opatruj nás pre takými Pane, či ktokoľvek môžeš!" 

