
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Gríger
                                        &gt;
                Malinovou cestou
                     
                 Lanzarote - na ceste do pekla 

        
            
                                    19.3.2010
            o
            23:31
                        (upravené
                20.3.2010
                o
                18:08)
                        |
            Karma článku:
                11.52
            |
            Prečítané 
            3038-krát
                    
         
     
         
             

                 
                    Vybrať sa len tak s batohom na niekoľko dní do pustatiny. S hmliste vygooglenou predstavou toho, čo nás asi čaká, plný energie, na počiatku nevyhnuteľného dobrodružstva. To sú moje obľúbené časy. Tentokrát som nemal žiadne obavy, ostrov Lanzarote nie je veľký, niet kam zablúdiť, počasie je i v zime príjemne letné a pobrežie sa hemží plážovými dovolenkármi a ich hotelmi, proste pravý Kanársky ostrov. A napriek tomu bolo v tomto raji na Zemi mojím cieľom miesto, ktoré mohol vytvoriť len sám diabol. Pustý svet bez života, poznačený skazou obludných rozmerov, svet tak hrozivý, až je tým krásny... sopky, krátery a lávové polia ostrova Lanzarote.
                 

                 Len som sa zvítal s príjemným januárovým letom, nakúpil zásoby vody, štrngol si so skupinkou dovolenkujúcich dôchodcov a načerpal energiu na dlhú cestu...      ...kde ukazujú šípky doľava a doprava, vybral som sa rovno...      ...a čoskoro som sa ocitol mimo rýchleho tempa civilizácie...      ...aká to divná zem, ako škvára z ihriska u nás na základnej škole...      ...ale dobre nás tam naučili, že sopečná pôda je úrodná...      ...hore hore... hodiny šľapania do kopca, dobre že teraz v zime nebýva nad 30°C...      ...podvečer som sa úspešne vyškriabal na jednu z najvyšších sopiek...      ...môj cieľ bol ešte ďaleko, stmievalo sa, tak som si v závetrí nad kráterom ustlal v machu pelech...      ...a v dokonalom tichu pod gýčovo červeným svetlom sa uložil na noc.          Noc nad kráterom bola príjemná. V diaľke podo mnou sa trblietali občasné svetlá dedín a nado mnou milióny hviezd. Kým som ich sledoval, zo desať sa ich zrútilo na zem. Tak som sa radšej zachuchlal do spacáka, nech ich nepopadá viac.   Ráno ma čakal výhľad aký sa nevidí z hocijakej postele...       ...keď sa rozplynul ranný opar, odhalil na horizonte môj cieľ, čiernu krajinu z pekiel...      ...podo mnou sa ťahali nekonečné polia z popola...      ...a v diaľke sa dvíhali pusté hory bez života..      ...snehobiele domky akoby z lega roztrúsené po pekelných poliach...      ...bez vody, bez dažďov, ako len v tomto čiernom prachu môže niečo rásť?...      ...dômyselné a pracné, každá rastlinka má svoju jamu obklopenú kamením na zachytávanie vlahy...      ...keď som sa pokochal ranným výhľadom...      ...a dal si od nemeckého turistu spraviť jednu narcisovku...      ...vybral som sa kamenným chodníkom naspäť do dolín...       ...cez kamenné polia...       ...cestou spočiatku celkom bežnou...      ...neskôr trochu fľakatou...       ...až nakoniec celkom čiernou...       ...predo mnou sa objavila mŕtva krajina, pravé peklo na zemi...       ...v poslednej dedinke z lega som dotankoval zásobu vody...      ...dal si malú siestu v tieni palmy...      ...a vybral sa na dlhý pochod sopečným krajom...      ...ovšem, svojou cestou...      ...po dlhých hodinách šľapania ma stále obklopovali len skaly a prach...      ...cesta sa začínala zdať nekonečná...      ...5 rokov neustálej sopečnej aktivity spôsobili jediné...      ...absolútnu deštrukciu nepredstaviteľných rozmerov...      ...po ťavom chodníku som sa nakoniec dostal až k národnému parku Timanfaya...      ...z diaľky sa na mňa škeril jeho symbol, El Diablo...      ...bol som v priamom srdci zdanlivo nekonečnej sopečnej pustatiny...      ...krajina sa od výbuchov spred tristo rokov veľmi nezmenila...      ...okrem lišajníkov a pár pichľavých kríkov tu niet veľa života...      ...ak nerátame návštevníkov v klimatizovaných autobusoch...      ...a občasných cyklistov na výlete okolo ostrova...      ...sopečný povrch je zradný, preto som sa aj ja musel odkloniť na bezpečie asfaltu...      ...ďalšie hodiny chôdze, aj mi už začínala chýbať Malina...      ...cesta bola pustá a okolité skaly strašidelné...      ...kde kedysi stáli dediny a polia, dnes ostala len kamenná púšť...      ...za každou zákrutou nová a tá istá ničota...      ...v podvečer som pred sebou uvidel cieľ dňa, bielu horu, obrovský kráter Caldera Blanca      ...odbočil som z bezpečia asfaltu na opustenú cestu skrz lávové polia...      ...no slnko rýchlo klesalo, a pár nepresností v mape a chyba v mojich výpočtoch spôsobili...      ...že kdesi uprostred hrôzostrašnej lávovej krajiny som sa odrazu ocitol v úplnej tme...      ...uhľovo čierna zem a bezmesačná obloha sa spojili v jeden dokonale čierny celok. Spočiatku mi to nevadilo a vo veselom svetle lampáša za zvuku španielskej hudby z rádia som pokračoval v ceste ešte dve hodiny. Z veľkej diaľky som počul špliechanie vĺn ako sa trieštili na ostrých skaliskách, čo sa mi vôbec nepáčilo, pretože podľa vzdialeností na mape som už pri mori dávno mal byť, dať si nočný kúpeľ a dobrú večeru na terase s výhľadom. V tom sa mi z ničoho nič umlčala hudba z rádia a ako som sa zastavil že skontrolujem baterku, všimol som si na samom konci svetla lampáša zvláštne pravidelný obrys. Prišiel som bližšie a potvrdil si svoje podozrenie... bol to dom. Alebo skôr to čo z neho zostalo. Kamenné pováľané steny, prázdné okná a za ním akási travnatá lúčka, akoby záhrada. Nechcelo sa mi veriť, že uprostred tejto nekonečnej deštrukcie prežil nejaký dom. Bol som už veľmi unavený, a tak som sa rozhodol, že noc strávim tu a až ráno keď sa rozvidnie a budem podľa okolitých kopcov vedieť určit svoju polohu, vyberiem sa ďalej. Rozložil som si spacák v mäkkej tráve pod jedným z pichľavých kríkov a sledoval ako rýchloletiace strapce oblakov zakrývali hviezdy. Myslel som na to, že podľa príručiek tu v lávovom poli nie je žiadny život a okrem niekoľkých druhov vtákov a jednej malej jašterice tu nestretneme nič. Spokojný, že ma tu teda spiaceho na zemi nič nezje som sa uložil spať...   ...pokračovanie tu: "Lanzarote - na ceste z pekla". 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Tento blog pokračuje na www.viamalina.com
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (3) - Lake District
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (2) - Yorkshirskými údoliami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (1) - Z mesta do sveta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Dorset
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Gríger
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Gríger
            
         
        griger.blog.sme.sk (rss)
         
                        VIP
                             
     
        Autor foto-cestopisov na: "www.viamalina.com"
   
  
    
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3237
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Malinovou cestou
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Chill on the sun
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Severné Arménsko. Tajuplnými kláštormi
                     
                                                         
                       Papierové modely na Tété model 2013
                     
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  




     






            
    
        
    
  
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 


 


