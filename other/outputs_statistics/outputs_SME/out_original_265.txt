
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Tropko
                                        &gt;
                Via Russia
                     
                 10 100 metrov nad Sibírou: Oci, a toto môžem krúťiť? 

        
            
                                    8.11.2007
            o
            11:11
                        |
            Karma článku:
                12.30
            |
            Prečítané 
            13798-krát
                    
         
     
         
             

                 
                    Veľa krát som už písal, ale zopakujem. Niektoré veci sa na tejto planéte Zem vymyslieť nedajú, iba v Rusku zažiť. Ak by som sám niečo podobné nezažil (moja 2-ročná dcéra sedela pilotovi počas letu na kolenách a držala sa „šturvalu“) bral by som to ako nejaký výmysel.  Posúďte sami: Prepis rozhovoru posledných minút z lietadla linky 593 pred padnutím pri  Meždurečensku, alebo ruská vzdušná ruleta v priamom prenose.
                 

                 
Meždurečensk. Let SU 593http://hectop.livejournal.com/309517.html
   Meždurečensk poznám osobne(mapa pádu lietalda). Prežil som tam, v možno najbohatšej časti našej planéty na suroviny časť svojho života. Mestečko sa nachádza na hranici Kuzbasu a prekrásneho Altaja.     Letecká katastrofa letu  SU593 sa stala  23 marca 1994. Zahynulo 75 ľudí, všetci ktorí sa na palube lietadla nachádzali.     Linka  593 lietala na lietadlách  Airbus A310 (F-OGQS) z Moskovského Šeremeťeva do Honkongu. Kapitán posádky Jaroslav Kudrinský, zobral na palubu dve svoje deti 15 ročného Eľdara a dcéru Janku.   V čase, keď lietadlo prelietavalo nad mestom Novokuzneck (hrávalo tam zopár našich a českých hokejistov za Metalurg) , Kudrinský napriek prísnym predpisom dovolil dcére a následne synovi  sadnúť si  do kapitánskeho kresla v kokpite lietadla. Táto udalosť bola ostatnou posádkou odignorovaná.     Ešte predtým, ako kapitán povolil deťom sadnúť si na svoje miesto, zapol autopilota. Dcéra si v sedadle len posedela, na rozdiel od syna, ktorý otočil riadiacu páku (šturval) na pravo, pričom zatlačil dostatočnou silou (8-10 kg) na to aby autopilot vypol a previedol ho do ručného režimu.   Napriek tomu, že sa zapla svetelná signalizácia, posádka si ju nevšimla, pretože „zvonček" bol vypnutý.     Lietadlo  sa natáčalo napravo po pozdĺžnej osi s rýchlosťou 1,5°/sek, a zanedlho uhol náklonu dosiahol 45°,  čo je vyššie povolenej hranice. Toto spôsobilo značné pozitívne preťaženie v kokpite (4,8g).   Keď členovia posádky zbadali, že je autopilot vypnutý, snažili sa zaujať svoje miesta. Druhý pilot s týmto problém nemal, pretože jeho miesto je vpravo v kabíne a lietadlo bolo nahnuté na pravobok.  Kapitán sa dlho na svoje miesto nevedel dostať , „vďaka" synovi, preťaženiu a veľkému uhlu náklonu lietadla.   Medzičasom tento uhol dosiahol 90°, a lietadlo začalo strácať výšku. Aby lietadlo naďalej nestrácalo výšku, autopilot ( vypnutý bol len autopilot náklonu) zvýšil uhol tangáže do takej úrovne, že lietadlo začalo rýchlo strácať rýchlosť a začal padať.   Druhý pilot úplne vypol autopilot a dokázal lietadlo dostať z pádu spôsobom zníženia nosa.  Záťaž sa znížila a kapitán sa dostal na svoje miesto. Lietadlo začalo rýchlo naberať rýchlosť  no taktiež rýchlo strácalo výšku.    Ekipáž zapol „malý plyn" ( nominálny režim práce motorov) a piloti potiahli riadiace páky na seba do krajnej polohy, no boli prinízko.   O 2 minúty a 6 sekúnd po tom čo sa dostali do vývrtky, lietadlo narazilo do lesa, len niekoľko kilometrov juhozápadne od Meždurečenska.     Ale vrátim sa k čiernej skrinke. Tu je preklad originálu :     Do tragédie je pol hodiny. Lietadlo letí na autopilote. V kabíne sa nachádza kapitán, druhý pilot a dve osoby nepovolané.     Druhý pilot: Novosibirsk, „Aeroflot" ,  593-tí prechádzame nad Vami vo výške  10 100.     Kapitán: No Jana, budeš pilotovať?      Jana sedí v kresle kapitána lietadla: Nie!      Kapitán: Na gombíky netlač. A tejto červenej sa nedotýkaj!     Jana: Oci, a toto môžem krútiť?     Kapitán: Novokuzneck zľava vidíš?      Jana: My tak nízko letíme?     Kapitán: Desať tisíc sto metrov.     Jana: To je veľa, áno?     Kapitán: Veľa...     Jana sa pokúša dostať z kresla.     Kapitán: Počkaj, neponáhľaj sa...     Jana: Ja aj tak opatrne...     Objavuje sa syn kapitána a sadá si na miesto prvého pilota.      Eľdar: Toto točiť môžem?     Kapitán: Áno! Ak potočíš doľava, kam lietadlo pôjde?      Eľdar: Vľavo!      Kapitán: Otoč. Vľavo otoč!     Eľdar: Supééér!     Kapitán: Lietadlo ide doľava?      Eľdar:  Ide.     Prešlo niekoľko minút..     Eľdar:  A čože sa on otáča?     Kapitán: Sám sa otáča?     Kapitán:  Drž riadenie!     Pilot: Rýchlosť!     Kapitán: Toč doľava! Vľavo!  Vpravo! Vpravo! Vľavo! Aha zem! Eľdar, vyjdi! Vylez! Eľdar! Vyjdi! Vyjdi! Vyjdi! Vyjdi! Vyjdi, hovorím! Plný plyn!      Pilot: Dal som plyn!     Kapitán: Plný plyn!     Pilot: Dal som!     Kapitán: Plyn, naplno!     Pilot: Rýchlosť, príliš veľká!      Kapitán: Tak! Vychádzame! Vpravo! Vpravo nôžku!     Kapitán: Uber plyn!     Pilot: Ubral!     Kapitán: Potich-ú-ú-čky!     Pilot: K...,opäť!     Kapitán: O chvíľu vyjdeme! Všetko bude v norme!    Kapitán: Potichučky, hovorím!     Koniec zápisu! .       Zdroje: Internet (aj Wikipedia). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (64)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako bolo zostrelené Malajzijské lietadlo MH17
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako Sibiriaci „profesionálne“ rozohnali demonštrantov „amatérov“.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Internát podľa Sťopu Kinga alebo čo v Mlynskej doline nezažijete.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Ako sme vysadili Bielorusa na Marse.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Tropko 
                                        
                                            Nechcem byť Bratislavčan! No a čo je na tom.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Tropko
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Tropko
            
         
        tropko.blog.sme.sk (rss)
         
                                     
     
         Všetko je v blogoch. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    157
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6146
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sibiriáda
                        
                     
                                     
                        
                            Moje univerzity
                        
                     
                                     
                        
                            Moskovia
                        
                     
                                     
                        
                            Via Russia
                        
                     
                                     
                        
                            Milicejské bájky
                        
                     
                                     
                        
                            Svokrine moskovské bájky
                        
                     
                                     
                        
                            Pohľad z iného brehu
                        
                     
                                     
                        
                            Christiánia
                        
                     
                                     
                        
                            Slovakia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            O Čečencoch, vodke a ako to bolo
                                     
                                                                             
                                            Ruské knihy na Slovensku
                                     
                                                                             
                                            Procházková zo SME - ľudská hyena?
                                     
                                                                             
                                            Toto nás čaka?
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            My iz buduščevo
                                     
                                                                             
                                            Ja zdelan v CCCP
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vysocky mal kamaráta Slováka
                                     
                                                                             
                                            Jediné východisko pre Spiš
                                     
                                                                             
                                            Svedectvo
                                     
                                                                             
                                            O slobodnej demokratickej žurnalistike
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Úžasní ľudia
                                     
                                                                             
                                            Hlasuj!
                                     
                                                                             
                                            Ruské Preklady
                                     
                                                                             
                                            krestan.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pápež ignoruje pápeža, katolicizmus v troskách!
                     
                                                         
                       Spáľme kostoly?
                     
                                                         
                       Skoro som prišiel o dcéru
                     
                                                         
                       Nenechajme si ukradnúť deti
                     
                                                         
                       Informácie (nielen o Rusku) z druhej strany?
                     
                                                         
                       Ako bolo zostrelené Malajzijské lietadlo MH17
                     
                                                         
                       Štyri a pol zemiaka
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Chcete dať svoje deti k skautom? Prečítajte si najprv toto.
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




