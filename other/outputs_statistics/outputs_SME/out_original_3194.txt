
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Patrícia Holíková
                                        &gt;
                PRÓZY. Príbehy bez veršov.
                     
                 more 

        
            
                                    24.3.2010
            o
            11:18
                        |
            Karma článku:
                4.85
            |
            Prečítané 
            873-krát
                    
         
     
         
             

                 
                    ...venované môjmu priateľovi antikvariátov, pretože vzdialenosť vyjadrená v tisíckach kilometrov je v porovnaní s váhou ľudského súzvuku a porozumenia iba smiešna fyzikálna kategória...
                 

                 _ _ _   „Mám tu priateľov, a pokoj a vyrovnanosť, po ktorých tak dlho túžim, nájdem skôr, keď sa na nejaký ten čas usadím na jednom mieste. A je tu more."        Znovu a znovu si čítam týchto pár slov. Hoci je to iba nepatrný, smiešny zlomok dennej dávky všetkých slov, chrliacich sa nepríčetne z človeka a do neho...cítim prudký pohyb existencie. Ozajstný slastný zážitok.       Von pučí jar. Svet sa rozjasňuje, ľudia v ľahkých kabátoch kúzlia úprimné úsmevy, na uliciach stretávam zaľúbené dvojice...       „Ale ja stále chodím s nepokrytou hlavou,   plnou trápenia."           „Prečo? Pozri na nádheru mesta. Nebuď smutná."       ...smutná nie som. Hoci, každý má na to právo.       Nie, nie som  smutná. Realita je subjektívna krása. Krása je realita. A ešte aká nádherná! Som jedným z miliárd ľudí.  Ako každý mám svoje spomienky. Svoju „psychologickú anamnézu", „úzkostné myšlienky", „negatívne jadrové schémy" , „škodlivé podmiené pravidlá", „maladaptívne reakcie", ...       A - ako každý - mám i toľko schopností, vzácnych spomienok, výnimočných ľudí v srdci, ušľachtilých pocitov, plánov a radostí. Preto sa veľmi snažím zabudnúť, myslieť, skúmať, pýtať sa, hľadať alternatívy, cestu von z bludných prstencov, tešiť sa, ľúbiť .... písať! Žiť!       „....no tak sa teda usmej! Pristane ti to!"       Ďakujem. Vo svojich básňach sa usmievam najkrajšie ako viem.       „...píšeme, píšeme,   posledná spodná sukňa noci je dávno popísaná   a nikto nevie čo je poézia"       ...a predsa to skúsim naznačiť, hoci na veľké veci definície nestačia.       Nemyslím si, že ozajstnej poézii ide o slová. Verše sú nepodstatné. Poznám veľa básnikov. Niektorí z nich napísali len pár basní a to zrejme ani jednu nikto okrem nich nečítal. Ba niektorí poéziu objektívne ani nevyznávajú...a predsa - sú to básnici. Velikáni poézie! Vidíte im to na očiach. Keď...       „...poézia je bolesť aj krása.   Len ľahostajnosť nepotrebuje poéziu.   Ale ľahostajnosť je choroba, na ktorú sa zomiera už za živa."       Môj drahý priateľ má more. Liek na ľahostajnosť. Má svoje myšlienky. Krásne lienky. Mám ich aj ja. Presne také, aké mi predkreslievala mamina, keď som bola malá. A ja som ich vždy nakreslila také veľké a škaredé. Tie jej boli útlunké a ... slobodné. Dnes viem podobné kresliť sama. Je to jednoduché, skúste to. Najlepšie sa kreslí do vzduchu (oproti slnku a rozihraným oblakom) a na ľudské líca. Lienky - myšlienky. Cestičky k sebe.       „Tak chodím ráno zbierať mušle po pláži   v záhyboch vĺn a ticha   hľadať zmienku o sebe"               (utorok 23.3.2010, Praha, Břevnov)         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Patrícia Holíková 
                                        
                                            Love without Metaphor
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Patrícia Holíková 
                                        
                                            poppy seeds
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Patrícia Holíková 
                                        
                                            naspäť I
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrícia Holíková 
                                        
                                            len také ako
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrícia Holíková 
                                        
                                            prosincová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Patrícia Holíková
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Patrícia Holíková
            
         
        holikova.blog.sme.sk (rss)
         
                                     
     
        "A viděl jsem jak se rodí nepostižitelné / Vysněná noc." Paul Eluard


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    429
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            POÉZIA ku káve. Žolík v rukáve
                        
                     
                                     
                        
                            Temperky a terapie.
                        
                     
                                     
                        
                            Manifesty.
                        
                     
                                     
                        
                            Horkosladko.
                        
                     
                                     
                        
                            ESEJE. Kto múdrosť zaseje.
                        
                     
                                     
                        
                            PRÓZY. Príbehy bez veršov.
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            o krásnych ľuďoch
                                     
                                                                             
                                            o písaní na Slovensku
                                     
                                                                             
                                            o čistej vode a o nás
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Carol Ann Duffy: Rapture
                                     
                                                                             
                                            všelijaké básničky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            &amp;#9834; La Vuelta al Mundo
                                     
                                                                             
                                            &amp;#9834; Nepokoj
                                     
                                                                             
                                            &amp;#9834; cat power
                                     
                                                                             
                                            &amp;#9834; always spring
                                     
                                                                             
                                            &amp;#9834; babylonian
                                     
                                                                             
                                            &amp;#9834; mystical and witchcrafty
                                     
                                                                             
                                            &amp;#9834; something you never seen
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Juraj Bocian. Poznanie a čas.
                                     
                                                                             
                                            Michal Badín. Boutique de poésie.
                                     
                                                                             
                                            Helena Fábryová. Fragmenty zrenia.
                                     
                                                                             
                                            Jakub. Spoločník do slnka, aj do dažďa.
                                     
                                                                             
                                            Anna Strachan. Svet slovanstva.
                                     
                                                                             
                                            Uršula Šilleová. Malá veľká básničkárka.
                                     
                                                                             
                                            Peter Balko. Noc tekutá a Goethe.
                                     
                                                                             
                                            Pán Mika. Naše - vaše muchy.
                                     
                                                                             
                                            SAŠA Grodecká. Kronikárka života.
                                     
                                                                             
                                            Stanko Lukáč. Playas perdidas del Sur.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Delighted
                                     
                                                                             
                                            Poésie
                                     
                                                                             
                                            Another Street
                                     
                                                                             
                                            Joy of Books
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




