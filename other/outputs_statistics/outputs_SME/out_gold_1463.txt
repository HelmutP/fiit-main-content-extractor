

 Ako je to s celoročným svietením. V aute (Citroen c4, 2008) mím originál svetelný senzor. Pri naštartovaní sa rozsvietia iba predne stretávacie svetla, zadne svetla nie. Zadne sa automaticky zasvietia iba pri zotmení. Stačí cez deň svietiť iba prednými svetlami? Zatiaľ to riešim manuálnym zapínaním svetiel, ale prečo nevyužiť senzor, keď tam už je :-) . Ako je to vyriešene napríklad u AUDI (LED pásiky na denne svietenie), svieti im vtedy aj niečo vzadu?  Vďaka za odpoveď, pekný deň, Peter 
 

 Predpisy v tomto ohlase sú trochu komplikovane. Zákon 8/2009 Z. z. predpisuje svietenie aj počas dňa bez zníženej viditeľnosti a súčasne dovoľuje používať svetlá pre denne svietenie. Samotné svetlá musia byť homologizované podľa zákona 
725/2004 Z. z. a ďalších predpisov. 


 Na Slovensku je stav taký, že k predným stretávacím svetlám musia svietiť aj 
zadné červené svetlá. Zákon navyše umožňuje aby za nezníženej viditeľnosti sa 
namiesto predných stretávacích svetiel používali aj účelové svetlá pre denné 
svietenie, ale pri tejto možnosti neruší povinnosť svietiť aj zadnými červenými 
svetlami. 

 Zašiel som aj za Importérom AUDI a výsledok rozhovoru je takýto: 

 Každé vozidlo určené pre slovenský trh programujú aby fungovalo nasledovne: 

 Pri naštartovaní sa rozsvietia predné svetla pre denné svietenie, ktoré sú známe ako LED pásiky pod alebo okolo reflektorov, súčasne sa rozsvietia aj zadné 
červené svetlá. 

 Ak svetelný senzor detekuje nedostatok svetla - tmu, tak sa rozsvietia 
stretávacie reflektory. Zadné svetlá zostanú svietiť naďalej. Vodič má možnosť 
prepínania medzi stretávacími a diaľkovými reflektormi. 

 V prípade, že vozidlo má aj asistenta diaľkových svetiel, čo je detektor vozidiel v protismere, tak prepínanie z diaľkových svetiel na stretávacie je zautomatizované. 

 Na cestách existujú aj staršie modely, ktoré ešte nemajú preprogramovanú 
elektroniku podľa nového zákona na celoročné svietenie a môže sa stať, že svietia len prednými svetlami, ale AUDI ich preprogramováva priebežne pri pravidelných prehliadkach, dovtedy je to na vodičovi aby zabezpečil svietenie prednými aj zadnými svetlami manuálne. 


 Záver: 
Treba svietiť celý deň prednými aj zadným svetlami. Svetelný senzor je v tomto 
prípade dobrá vec na prepínanie medzi dennými a stretávacími svetlami. Pokiaľ 
všetko nefunguje v súlade s aktuálnymi predpismi treba sa obrátiť na výrobcu 
vozidla. 


 Doplnené 22.6.2009 Pokračovanie témy a vysvetlenia aktuálneho právneho stavu je v nasledujúcom článku:

Svetlá pre denné svietenie - je to komplikovanejšie ako sa zdá.
 

