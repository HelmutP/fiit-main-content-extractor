
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Prvý pokus
                                        &gt;
                Súkromné
                     
                 „Ladies“ day 

        
            
                                    13.4.2010
            o
            9:09
                        |
            Karma článku:
                12.67
            |
            Prečítané 
            2672-krát
                    
         
     
         
             

                 
                    Dnešné Anglicko človeka veľmi rýchlo vylieči z ilúzií o rozprávkovom živote v zahraničí. Napríklad v meste ako Stoke-on-Trent nezaberie veľa času odhaliť, že sa tu dajú robiť približne tri veci: pracovať, nakupovať a opíjať sa. Ak sa teda v okolí náhodou vyskytne akákoľvek iná spoločenská udalosť, my neaklimatizovaní Poliaci, máme sklon za každú cenu sa jej zúčastniť.
                 

                 Minulý týždeň sa neďaleko vyskytli konské dostihy. Udalosť kvetnato nazývajú Grand National a najnavštevovanejším dňom dostihov je tradične tzv. Ladies day. Reklama láka návštevníkov obrázkami elegantných dám v klobúkoch, ktoré sa opierajú o ešte elegantnejších pánov s cigarami v ústach a spoločne so záujmom sledujú, ktorýžeto koník doklusá do cieľa ako prvý.   Ráno to spočiatku skutočne vyzerá tak, že sa jedná o udalosť hodnú názvu Grand National, ba dokonca i Ladies day. Cez vstupnú bránu sa presúvajú „Ladies" vymaľované ako kraslice z ÚĽUV-u, a predvádzajú jedny starostlivo zvolené šaty za druhými. „Ladies" prichádzajú vo všetkých farbách, tvaroch a veľkostiach. Pôsobí to celé ako kampaň na zvýšenie sebavedomia (pod)priemerných žien. V pozadí znie výzva: „Deti v Afrike mrznú, neplytvajte textilom." „Ladies" si to berú k srdcu, a čím by daná „Lady" poterbovala viac látky na decentne zahaľujúci odev, tým spokojnejšie vyrazí v mini. Ja a ďalšie Poľky žasneme nad tým, aké veľké môže byť ľudské stehno. Žasneme tam vo svojich absolútne netrendy kabátoch aj nad tým, či je husia koža najnovšým módnym hitom priamo z Paríža. Napovedajú tomu totiž všetky „Ladies", ktoré napriek teplote 13 stupňov Celzia vytrvalo ignorujú nepopulárne kusy odevu, ako pančuchy, či už spomínané kabáty.   Po pompéznom presune „Ladies" vstupnou bránou v podstate nasleduje už len mnoho ďalších presunov. Presúva sa z hľadiska k stánkom s pivom, od piva k záchodom, a späť do hľadiska. Sedem pretekov, ktoré medzitým kone odbehnú, v podstate len udáva tempo presunov : hľadisko - pivo - WC - hľadisko. S klesajúcim množstvom prechodov, ktoré zostáva absolvovať stúpajú nasledovné hladiny:   1, hladina znečistenia záchodov označených „Ladies" - okolité krovie po skončení festivalu Pohoda je v porovnaní s nimi vhodné ako priestor pre chirurgické úkony   2, hladina znčistenia „Ladies" samotných - po dlhodobom presúvaní sa, nasleduje zvyčajne zosúvanie sa. Úbohé „Ladies" nemajú na zosunutie k dispozícii vhodnejšiu plochu ako priestor pod nohami tých, ktorých zosúvanie ešte len čaká,  a tak sa približne po dvoch odbehnutých dostihoch na zemi vyrovná počet plastových pohárov a tiel obalených kvetovanou látkou. Približne po štyroch dostihoch je značne komplikované rozlíšiť na telách kvetinové vzory od zvratkov.   3, hladina tolerancie zúčastnených - napríklad, ak niektorý z prítomných gentlemanov usúdi, že už nie je spôsobilý na presun k toaletám, s distingvovaným úsmevom na tvári sa vymočí uprostred davu, zatiaľčo ho jeho priateľ striedavo pochavlne potľapkáva po pleci a po riťke. Ako prejav vrcholnej ústretovosti polovicu obsahu mechúra nevyprázdni na zem, ale do pohára s pivom - veď predsa navôkol je mnoho zosunutých „Ladies".   4, hladina povoľnosti - zatiaľčo v predpoludňajších hodinách budia „Ladies" dojem vznešených korábov plaviacich sa v nedostupných diaľavách, s pribúdajúcim časom sa menia na nafukovacie člnky z Tesca a ochotne Vás upozornia i na aktuálnu akciu „Buy one, get one free".   Keď utíchnu dupotajúce kopytá koní, neaklimatizovaným cudzincom ako my sa hlavami preháňajú nezodpovedané otázky. Začínajú banalitami („Ako to, že má žena čas naniesť si na lícne kosti flitre, ale nemá čas ostrihať si nechty na nohách?"), pokračujú filozofickými otázkami („Načo vlastne Angličanky venujú toľkú energiu svojmu zovňajšku, keď Angličanov vôbec, ale skutočne vôbec nazaujímajú ak im práve nenesú pivo?") a končia otázkami rečníckymi („Všimol by si tu vôbec niekto, keby namiesto koní vybehli na trať fretky?").   autorka: Katarína Rafajová 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (50)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Stačí tak málo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Moje skúsenosti s poistením liečebných nákladov v jednej poistovni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Demonstrace zahájená objetím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Referendum o rodine z pohľadu kresťanského libertariána
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Prvý pokus 
                                        
                                            Kam zmizli z Malého Ríma všetci kresťania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Prvý pokus
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Prvý pokus
            
         
        pokus.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ak chcete zverejniť svoj text na tomto blogu, pošlite nám ho e-mailom vo worde na adresu matus.paculik(at)smeonline.sk. Nezabudnite na predmet PRVÝ POKUS. 
 
Výber textov je v právomoci redakcie, ktorá má právo zmeniť autorom navrhnutý titulok. Väčšinu článkov zverejňujeme, ale vyhradzujeme si právo ktorýkoľvek odmietnuť aj bez udania dôvodu, najmä ak sa autor nepodpíše celým menom. 
 
Vždy uveďte aj vašu poštovú adresu a telefónne číslo. Tieto dva údaje nezverejníme, ale musíme ich mať k dispozícii kvôli overeniu identity alebo pre prípad, že sa váš článok rozhodneme honorovať. 
 
Ak meno a priezvisko priamo v článku nechcete uviesť, prosíme, napíšte nám dôvod. Za istých dôležitých okolností tieto údaje nepublikujeme, redakcia ich však vždy musí poznať. 
 
Zaradenie textu zväčša trvá niekoľko dní. Na tomto blogu nezverejňujeme poéziu ani prózu. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    713
                
                
                    Celková karma
                    
                                                6.49
                    
                
                
                    Priemerná čítanosť
                    5054
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovenské zdravotníctvo
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       1.5 metra na bratislavských chodníkoch
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Prečo ma Kyjev rozplakal..
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




