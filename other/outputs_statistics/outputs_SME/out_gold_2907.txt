

   
 Najprv nebola 
 akože kríza, 
 len nedostatok 
 peňazí. 
 Dnes bieda ľuďom 
 do okien zíza, 
 nekorektnosť 
 vo vzťahoch víťazí. 
 Prestali platiť 
 dohody a zmluvy, 
 desím sa ďaľšej 
 predstavy. 
 Nevraživosť nahlas 
 v uliciach zúri 
 a zasa ryba ,smrdí 
 od hlavy. 
 Národ umiera, 
 chrbát hrbí, 
 znáša svoj údel 
 hlúposti. 
 Vyplače hymnu 
 do bútľavej vŕby, 
 s pocitom vlasteneckej 
 hrdosti. 
   

