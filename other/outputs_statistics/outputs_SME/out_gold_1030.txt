

 
Predpokladám, že nie som sám, komu sa dnes do mailu dostala pozvánka na úžasnú mega hyper ultra Mikulášsku párty pod záštitou tetušky Martinákovej a jej slobodného fóra. Ako to tak vidím, tak ich strana si asi rada robí fóry z ľudí, keď už to majú v názve. Pozvali ma na Mikulášsku párty, ktorá sa nekoná 6teho decembra, nikde nenapísali koľko ľudí môžem so sebou zobrať na tú žranicu a ani ma neinformovali, na ktorom mieste z tej prvej stovky som sa umiestnil. Toto má pôsobiť dôveryhodne? Však keď nie som v prvej dvadsiatke tak sa mi ani neoplatí chodiť. Čo sa tam budem predierať medzi toľkými ľudmi, ktorých slobodné fórum považuje za lepších než som ja sám.  
 
Pani Martinákova, prosím Vás keď už dávate takúto akciu niekomu organizovať, tak to dajte do ruky niekomu schopnému, kto z toho nespraví pozvánku na párty pre žiakov tretieho ročníka ZŠ.  
 
"Vážime si Vašu občiansku aktivitu a prejavovanie neľahostajnosti k veciam verejným, ako aj kreativitu, ktorú v našich podmienkach považujeme len za málo cenenú."  
 
Fráza hodná siedmaka pri písaní slohovej práce na tému Mamička je v politike a ja jej chcem pomôcť. Takých fráz tam je ale viac. Text obsahom skôr pobaví akoby mal vážne zaujať. Najviac ma ale dorazil komentár ku koncu. To si myslíte, že koľko ľudí Vám skočí na motanie tých medových motúzov popod fúzy? Ešteže som si fúziky oholil a nemám sa kde zagebriť. 
 
"Možno budete v tejto chvíli pociťovať rozpaky, prijať toto pozvanie, ktoré možno budú niektorí považovať za aktivitu smerujúcu k propagácii politickej strany, prípadne Vás zaväzujúce písať v takýchto tendenciách. No my veríme vo Váš vyzretý názor a nebojácnosť konania, ktorú ste inak už dávno prejavili aj vo Vašich blogoch. Nedovolili by sme si na Vás vyvíjať hociaký druh nátlaku, pretože vieme, že by ste ho mohli silou Vašej prestíže použiť nakoniec proti nám." 
 
Rozpaky nepociťujem. Jediné čo cítim je trhanie kútikov úst od smiechu za tieto vety. Áno, považujem to za veľmi slaboduchú a otravnú propagáciu politickej strany, hlavne to považujem za nevyžiadaný mail. Škoda, že gmail sa ešte stále občas pomýli a nevyfiltruje 100%ne všetok spam. Neberiem to ako nátlak, ale musíte očakávať, že keď takýto blábol pošlete niekomu, kto rád píše články nasraté a sarkastické, že aj vy môžte byť cieľom.  
 
Ďakujem, že ma už nikdy nebudete otravovať pozvánkami na akcie, kde sa budem musieť pozerať na to, ako sa niekto snaží tlačiť mi politiku do žranice. Úspešne som si Váš mail archivoval a zároveň pridal do spam listu. Ak by sa mi ešte náhodou nejaký dostal do očí, mám aj iné riešenia ako spam filter. 
 
PS: No koľkatý som to teda skončil v tej 100ke? Máte ten rebríček aj niečim podložený, či si ho mám podložiť sám vašimi volebnými lístkami? S pozdravom Váš stopercentne odradený nevolič!!
 

