

  Často sa zvykne hovoriť, že študovanie medicíny a povolanie lekára sa dedí. Bolo to tak aj vo  vašom prípade alebo vás podnietilo vydať sa na ťažkú cestu k lekárskemu titulu niečo úplne iné? 
   
 Rasťo: Otec je síce lekár, no nepovažoval by som to za dôkaz pokrvného dedičstva. Naopak. Už teraz zisťujem, že to, čo ho v mojom veku do medicíny hnalo a spôsob, akým školu vnímal on, je úplne iného rázu, ako ten môj. 
   
 Juraj: Je pravda, že aj v našej rodine sa vyskytlo pár doktorov. Ale nemyslím si, že to bol hlavný dôvod, prečo som sa rozhodol práve pre túto vysokú školu. Na gymnáziu som mal bližšie k prírodovedným predmetom akými sú chémia a biológia. A z toho aj vyplynulo moje rozhodnutie. 
   
  Rozhodovali ste sa aj medzi viacerými školami a miestami, kde by ste medicínu mohli študovať? 
   
 Juraj: Som z Bratislavy, takže prioritou v rámci Slovenska bola jednoznačne Bratislava. Prihlášky som si však podal aj do Martina a Košíc. Posielal som prihlášky aj na lekárske fakulty v Českej republike. Ale tam je prioritou okrem biológie a chémie aj fyzika. A tá nemala na strednej škole práve najlepšiu úroveň, takže nakoniec som sa o štúdium v Česku ani nepokúšal. 
   
 Rasťo: Ja som sa rozhodoval medzi medicínou a psychológiou. ,,Medina" vyhrala kvôli väčšej perspektíve, svojej hĺbke, zdanlivo menším vplyvom na moju vlastnú zdravú psychiku. Chcel som mať prácu, ktorou by sa dala uživiť rodina (smiech). 
   
  Juraj, ty si momentálne v poslednom ročníku. Na ktorý rok máš najkrajšie spomienky? 
   
 Ťažko povedať. Prvý ročník bol zaujímavý tým prechodom zo strednej školy na vysokú. Potom asi štvrtý a piaty ročník, ktoré sú, oproti tým predchádzajúcim, o niečo ľahšie. A ďalej, samozrejme, tento posledný, keď sa už konečne blíži koniec. (smiech) 
   
 Čo by ste obaja ako študenti zmenili na samotnej výučbe budúcich lekárov?  
   
 Rasťo: Masarykova univerzita je úžasné miesto a vskutku tu netreba veľa meniť. Z niektorého predmetu by som ubral, do iného zas pridal, no sú to drobnosti. 
 Škola robí každý semester masovú anketu pre všetkých študentov, kde sa majú vyjadriť ku každému predmetu a učiteľovi, takže názory študentov nasmerujú školu tou správnou cestou. Ja sa o ňu vôbec nebojím. 
   
 Juraj: Povedal by som, že štúdium medicíny je špecifické tým, že do istej miery je naviazané na úroveň zdravotníckych zariadení. Vo vyšších ročníkoch totiž dominantná časť výučby prebieha práve v nemocničnom prostredí. Za najväčší nedostatok považujem prevahu teoretickej prípravy nad praktickou. Určite by som uvítal väčší priestor pre prax. 
   
 A čo váš aktuálny pohľad na medicínu a prácu lekárov?  
   
 Rasťo: Stále menej mám rád lieky a médiá, ktoré svojími múdrosťami o stravovaní a diétach narúšajú telesnú rovnováhu ľuďom, ktorých mám rád. Práve na tejto škole zistíte, že ženské časopisy sú skutočným nebezpečenstvom. A to nezveličujem! 
   
 Juraj: Určite áno. Človek spozná prácu lekárov z druhej strany, než ako ju mal možnosť poznať doteraz. A zrazu sa stratia všetky ideály. I keď ťažko povedať, či som nejaké vôbec mal (smiech). 
   
 Majú ťažko študujúci medici vôbec čas aj na zábavu?  
   
 Juraj: Ale určite, ten čas sa nájde. Záleží aj na samotnom študentovi, ako pristupuje k štúdiu. Človek nemusí byť stále zatvorený medzi štyrmi stenami a sedieť nad skriptami. Ja osobne už dlhé roky hrávam aktívne stolný tenis a to mi ostalo aj počas vysokej školy. Okrem toho mam rád hudbu, takže nemôžu chýbať nejaké koncerty a posedenie pri pivku. 
   
 Rasťo: Ja popri učení športujem, chodím na tanec, kreslím, navštevujem medické (!) akcie, koncerty, chodím na prechádzky. Samozrejme, nedá sa robiť všetko naraz, treba si vedieť vybrať. Dá sa nájsť čas, no jeho kvantita sa, podľa mňa, na rozdiel od iných škôl, trošku líši. 
   
 Aké sú tvoje vyhliadky do budúcnosti Juraj? 
   
 Ja by som sa chcel venovať rádiológii. 
   
 Rasťo, ty si spomínal, že by si chcel byť jedným z Lekárov bez hraníc. Nebojíš sa podmienok, v akých musia títo lekári pracovať? Prečo sa mladý človek ako ty rozhodne práve pre túto možnosť uplatnenia sa? 
   
 Bojím sa. Pre mňa je pomoc ľuďom jeden zo základných pilierov môjho šťastia a keď zmiešate túto vášeň s chuťou spoznávať extrémne zákutia sveta, vyjdú vám Lekári bez hraníc. Áno, samozrejme vám musí aj trochu ,,šibať". (smiech) 
   
   

