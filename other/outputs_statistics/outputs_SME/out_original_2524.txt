
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek H. Eliáš
                                        &gt;
                KlokaNoviny
                     
                 A von! 

        
            
                                    9.3.2010
            o
            17:10
                        |
            Karma článku:
                15.83
            |
            Prečítané 
            2883-krát
                    
         
     
         
             

                 
                    Dám vám hádanku. Čo má spoločné sedemdesiatročný Srílančan a ja? Našepkám vám: obaja sme boli v Austrálii a už tam nie sme. Správna odpoveď: obaja sme museli tú krajinu opustiť dobrovoľne nasilu, aby nás odtiaľ nevyhodili.
                 

                 
 Stock.xchng
   Edward Joseph bol jediným opatrovníkom svojej deväťdesiatročnej matky, ktorá inak takto mala v Austrálii trvalý pobyt. Možno ho tam ešte stále má, ale už jej je na dve veci. Jej syn totiž podľa ministerstva pre imigráciu a občianstvo nemá právny nárok na zotrvanie v Austrálii. A tak, menej ako 24 hodín pred tým, ako by ho vyhostili, si zbalil kufor, deväťdesiatročnú mamičku a odišli naspäť na Srí Lanku.   Marek Eliáš je tridsaťročný zdravotný brat. Školu skončil s veľmi slušnými výsledkami, stal sa členom austrálskej komory sestier a začal si hľadať prácu. Až po pár týždňoch nevysvetliteľného neúspechu mu jedna dobrá duša z nemocnice, kde ho odmietli zamestnať, prezradila príčinu: podľa zákona totiž gastarbeiteri musia dostať zákonom stanovené minimum - ktoré je však vyššie ako plat absolventa. Dôsledok? Nezamestnateľnosť. Bez práce, bez školy, bez peňazí - opäť ktosi balil kufre a išiel domov, aj keď nechcel.   Austrálska vláda ohlásila, že prehodnotí zoznam žiadaných povolaní a posvieti si na školy, ktoré boli továrňami pre Indov na získanie trvalého pobytu. Však aj v Austrálii sa blížia voľľby. A zdá sa, že keď sa pustia do cudzincov, zbiera im to body. Rasizmus a xenofóbia majú v Austrálii dobrú tradíciu - nebolo to tak dávno, čo imigračná politika mala oficiálne motto "Za bielu Austráliu" a ešte sa nezabudlo na slávny test diktátom. Záujemcovi o prisťahovanie mohli úradníci dať diktát z ľubovoľného európskeho jazyka. Takže Číňania sa potili nad diktátom zo švédčiny. Samozrejme, že ho nezvládli. O to predsa išlo.   Národ, ktorý vznikol z imigrantov, si zrazu zmyslel, že imigrantov bolo dosť. Čo na tom, že rozbíjajú rodiny, že posielajú preč zdravotné sestry, ktoré údajne zúfalo potrebujú. Dôležité je zbaviť sa cudzincov.   Doma v Komárne ma vítal Slota loziaci po streche. V novinách reči o vlasteneckom zákone. Díval som sa na to a už som si len povzdychol. Neviete o nejakej krajine, kde ľudí viac zaujíma, čo dokážete a aký ste človek? Lebo krajín, kde najdôležitejšia je národnosť či občianstvo, mám už po krk. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (54)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek H. Eliáš 
                                        
                                            Najdôležitejšia vec: stop radikálom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek H. Eliáš 
                                        
                                            Povedzme si to rovno
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek H. Eliáš 
                                        
                                            V obchodoch majú čokoládové vajíčka!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek H. Eliáš 
                                        
                                            "Penis," povedalo rádio
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek H. Eliáš 
                                        
                                            Bludičky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek H. Eliáš
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek H. Eliáš
            
         
        marekelias.blog.sme.sk (rss)
         
                        VIP
                             
     
        Hyena je hyena je hyena.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    88
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            KlokaNoviny
                        
                     
                                     
                        
                            Špitálske zážitky
                        
                     
                                     
                        
                            Prírodopis
                        
                     
                                     
                        
                            Panic Room
                        
                     
                                     
                        
                            Aktuálne
                        
                     
                                     
                        
                            Divnosprávy
                        
                     
                                     
                        
                            (Ne)Kultúra
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Elisabeth má postreh
                                     
                                                                             
                                            Aničkina kaviareň
                                     
                                                                             
                                            Jakože cicuška
                                     
                                                                             
                                            Ľubka Romanová
                                     
                                                                             
                                            Jozef Klucho
                                     
                                                                             
                                            Medveď
                                     
                                                                             
                                            Riddick na blogu
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dilbert
                                     
                                                                             
                                            Môj pravý domov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pochod za právo na rozhodnutie
                     
                                                         
                       Mnoho systému, žiadna človečina
                     
                                                         
                       Ak si nepomôžeme  sami, kto potom?
                     
                                                         
                       Prospievajú deti homosexuálov v živote lepšie?
                     
                                                         
                       Tri klamstvá miliardárskych developerov
                     
                                                         
                       Stehno veľryby
                     
                                                         
                       Základné pravidlá predpovedania budúcnosti vo volebnej kampani
                     
                                                         
                       10 vecí, ktoré by sme zrejme nemali robiť hendikepovanému človeku.
                     
                                                         
                       Prečo by na nás mimozemšťania zaútočili
                     
                                                         
                       Medveď mudruje - Tri stručné odkazy
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




