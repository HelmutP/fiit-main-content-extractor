
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katy Suchovská
                                        &gt;
                Kynológia
                     
                 Náš aktívny agilitný život 

        
            
                                    23.6.2010
            o
            2:56
                        (upravené
                23.6.2010
                o
                3:22)
                        |
            Karma článku:
                3.73
            |
            Prečítané 
            234-krát
                    
         
     
         
             

                 
                    Ako by som zhrnula naše aktívne žitie v kynológii? Už to sú tri roky mojho života, ktorej súčasťou sú moji psíkovia, kedy sme spoločne pojazdili preteky na Morave, kde všade a ako to vidím po tých troch rokoch...
                 

                 
Sponzorské cenyRadka Konečná
     
 Začalo to, keď som si priviezla Charlieho. (viz článok Predstavujeme) Začala som sa venovať kynologickému športu menom agility. Ako by som tieto tri roky aktívneho života zhrnula v praxi?   V máji v roku 2007 som začala trénovať s mojím prvým psíkom Charliem. Pravidelne sme navštevovali cvičisko, kde sme podstivo trénovali tento šport, ktorý mňa a myslím, že aj mojho psíka chytol za srdce. Po piatich mesiacoch trénovania sme sa vydali na našu premiéru v „závodení“. Naša kamarátka usporiadala v útulku vo Bzenci deň venovaný psíkom z názvom „Happy Dog Day 2007“. Súčasťou tohto dňa boli ukážky agility, závody, „výstava psíkov“, ukážka trikov. Popri tomto dni bola verejná zbierka pre zvieratká v tomto útulku. Pre nás to bola prvá skúsenosť, kde sme s Charliem predviedli čo umíme: 5-mesačná snaha nám priniesla naše 1.miesto. Ďalšia skúsenosť pre nás boli neoficiálne závody v Blansku, klubové závody v Hodoníne a Mikulášsky sranda závod. Stále sme sa len učili, takže sme aj tieto nové skúsenosti brali ako pozitívum v našom živote.   Než sa začnem podrobnejšie rozpisovať o všetkých závodoch, ktoré sme absolvovali, nesmiem zabudnúť na ďalšieho hlavného aktéra, okrem štvornohých, ktorých sa to týka najviac. Neodmysliteľnou súčasťou všetkých pretekov, ktorý to so mnou "trpí" :) je dedo! Všade kam potrebujem ma zavezie, odvezie, počká na mňa, urobí si nejaký program, aj keď je to v mieste, kde sa nejaký program urobiť nedá.. Vymyslí si prechádzku po prírode, zoberie si časopisy a celý deň so mnou "pretrpí". Samozrejme moje závody sponzoruje, pretože ja chudobný študent... :))) Samozrejme, že je to pre mňa najvačšia podpora, keď viem, že je tam pri nás. Obaja štvornohý sú z neho unesený..  :)     Rok 2008 bol podobný. Okrem toho, že na konci februára nám do rodiny pribudlo to malé čiernobiele šteniatko sme s Charliem a teraz už aj s Crashom podstivo trénovali. S Charliem sme sa vrhli po hlave do závodnej kariéry. Behali sme na klubových závodoch v Brne, neoficiálnych závodoch v Šlapaniciach. Rok 2008 sa zapísal do Charlieho agilitnej premiéry, kedy sa stal s Charlieka „dospelák“. Navštívil oficiálne závody, na ktorých už nejde len o nejaký „trénink“ s ocenením pred ľuďmi. Už sme behali oficiálne. Naším prvým miestom, kde sa Charlie stal „dospelým“ boli Hrušky u Vyškova. Charlie si odtiaľ priviezol prvú medailu za 3.miesto v skúške agility.   Pokračovali sme ďalej na klubových závodoch v Třebíči, neoficiálnych závodoch v Blansku, oficiálnych závodoch s názvom Letná hodonínska tlapka v Hodoníne, oficiálnych závodoch s názvom O Zlínsky ranec I. v Zlíne, špeciálom kategórie A1 (začiatočnícky závodníci) v Brne, neoficiálnymi závodmi v Hodoníne, oficiálnymi závodmi v Zlechove, oficiálnymi závodmi v Hruškách u Vyškova, klubovými závodmi Mistr Zero v Hodoníne a záver roku 2008 sme absolvovali oficiálnymi halovými závodmi v Dubňanoch, pri ktorých sa spoločne robili majstrovstvá ČR IX. Skupiny FCI (ktorej je súčasťou aj náš Charliek).     Rok 2008 bol pre nás prínosný, priniesli sme si nejaké umiestnenia, ale tiež nové skúsenosti v množstve nových prostredí. Pre psíka začiatočníka je veľmi náročné behať v novom prostredí a tiež pre psovoda, ktorý musí bojovať.     V roku 2009 sme neoficiálne závody brali už len ako tréningy a našou náplňou boli hlavne oficiálne závody. Charlie už bol zabehnutý, behal ako ten najvačší profík (aspoň pre mňa) a tak sme sa do toho v tomto roku pustili na plno. Okrem toho, že som behala s Charliem sa pre mňa stalo behanie dvojnásobné aspoň na tréningoch, kde som pečlivo pripravovala Crashíka na život dospeláka.    Absolvovali sme Třebíčske halové závody – Zimný přebor, neoficiálne závody v Hodoníne – Předvelikonočný výprask I., Velikonočního beránka v Třebíči, jarné oficiálne závody v Hruškách, Jumpingový piatok v Zlíne, Skúškovú nedelu v Zlíne, Mistra Veselí vo Veselí nad Moravou, Zlínský ranec II. v Zlíne, Letnú hodonínskú tlapku v Hodoníne, neoficiálne závody Lážo plážo v Brne, klubové závody – Prázdninový V.I.P zásah v Hodoníne, Letnú hodonínskú tlapku II. v Hodoníne, Holešovské hopsání v Holešove, jesenné oficiálne závody v Hruškách a sezónu sme zakončili neoficiálnymi závodami v Hodoníne – Vzhůru na Olymp aneb Na křídlech Pegasů.   V tomto roku sa Crashík stal dospelákom na jesenných závodoch v Hruškách. Do tej doby na troch závodoch robil tzv. predskokana, kde si može psík zo svojim psovodom vyskúšať behať v novom prostredí bez ohodnotenia. Všetko zvládol na výbornú.        Suma sumárum u Charlieho a Crashíka? Priniesli sme si z niektorých závodov ocenenia, či už v podobe pohárov, medajlí, plaketiek, krmiva, hračiek... Pre mňa je vždy najvačšia výhra, že daný beh dobehneme a máme z toho obaja veľkú radosť.   Nikdy mi nešlo o to vyhrať, ale zabehať si, zmerať si rýchlosť a presnosť s ostatnými pretekármi, z toho, že nám to obom v našom „tíme Charlie a ja, alebo Crashík a ja“ urobí radosť a že nám to spolu klape.   Je to náročný šport a málokto to ocení. Smutné je vidieť, ako niektorí ľudia po dobehu, keď sa im náhodou niečo nepodarí skritizujú psíka, že sa to nepodarilo a vyžadujú od štvornohého veľké nároky. Veď ako som písala v niektorom článku, že je to tímový šport a záleží od psovoda, ako psíka vedie, veď štvornohý len kopíruje náš pohyb a počúva naše povely.   Je to súhra nás dvoch. Bežím na plno len preto, aby som štvornohému urobila radosť, z toho že beží a štvornohý beží, aby urobil radosť mne.               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            European Open, medzinárodný závod agility
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Muffiny s Marsu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Keď kynológ, tak poriadny..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Veľká voda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Agility
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katy Suchovská
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katy Suchovská
            
         
        suchovska.blog.sme.sk (rss)
         
                                     
     
        Študentka VŠ, ktorá venuje všetok svoj voľný čas svojim dvom chlpatým miláčikom..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    27
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    663
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Kuchyňské pokusy
                        
                     
                                     
                        
                            Kynológia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            My chemical romance
                                     
                                                                             
                                            Evanescence
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




