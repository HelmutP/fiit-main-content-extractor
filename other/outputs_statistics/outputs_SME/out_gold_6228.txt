
 Cesty Bratislavy boli už rozoberané viackrát, ale mám dojem, že viac ako po 20 rokoch od revolúcie sú stále v horšom a horšom stave. Niekto bude namietať, že je viac áut, čo je pravda. V priamej úmere by to malo znamenať aj kvalitnejšie vozovky, ktoré vydržia oveľa viac.

Lenže to čo platí už v na hranici s Rakúskom, ako keby k nám ešte nedorazilo. V súčasnosti, teda ledva mesiac po naozaj tuhej zime, sa opravujú výtlky na celom Slovensku. Zima opäť dala zabrať našim cestám ešte viac, ako po minulé roky. 

Uvediem jednoduchý prípad cesty medzi Bratislavou, Svätým Jurom a Pezinkom. Pred dvoma rokmi sme sa tešili ako malé deti, pretože cestári pokladali na tieto úseky nový koberec. Konečne sa nebudeme v autobusoch natriasať, v autách na rovnej ceste kľučkovať. Dnes? Mimoriadne nekvalitný asfaltový materiál nevydržal nápor dopravy, „ neočakávane“ silných mrazov a výtlky sú poriadne veľké a hlboké. Opäť sa raz potvrdilo, že do vyspelej Európy nepatríme.

O pezinských uliciach ani nehovoriac. V zime sa návaly snehu museli obchádzať, pretože chodníky a ulice neboli vôbec odpratané. To ako vyzerajú jednotlivé ulice dnes je na zaplakanie. Mozajka výtlkov, obchádzok, pretože sa teraz lepia novým( určite nekvalitným) asfaltom, je nespočetná. Apelujem týmto na mestské zastupiteľstvo, aby už konečne niekto z nich predložil návrh na vybetónovanie všetkých ciest a ulíc v tomto meste!

Rovnako by som sa pozastavil nad tým, že zopár chorých mozgov rozhodlo o zjednosmernení niektorých úsekov v našom okresnom meste. Beriem to ako vcelku dobrý nápad( sám som chorý?), ktorý ako taký, zrýchly dopravu. Ale to, že sa zvýši hluk na neúmernú mieru pôvodne tichých oblastí( Starý dvor, sídlisko Juh, Dubový vŕšok) sa určite nikto nezamýšľal.
 
Vodiči v žiadnom prípade nedodržujú predpísanú rýchlosť v meste na žiadnom z týchto úsekov. Kde je polícia, ktorá by mala vyberať pokuty za neprimeranú rýchlosť? Na sídlisku Starý Dvor, a na komunikácii Dubový vŕšok si vodiči mýlia cestu s rýchlostnou komunikáciou. Často sa stáva, že vás obieha vodič v 80 km rýchlosti a viac. Stáva sa to každý deň, a nikto, rozumej Polícii je to jedno. Ako je to možné?

Kompetentní by sa mali naozaj zamyslieť, ako s týmto stavom naložiť. Určite nie som jediný, ktorému tento stav v žiadnom prípade nevyhovuje. Očakávam od kompetentných mestských orgánov, a štátnych orgánov príslušnú nápravu.

 
