
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kamenský
                                        &gt;
                umenie
                     
                 Maliar Michaloviec - J. T. Mousson 

        
            
                                    29.11.2009
            o
            19:16
                        (upravené
                21.8.2012
                o
                12:08)
                        |
            Karma článku:
                10.68
            |
            Prečítané 
            4131-krát
                    
         
     
         
             

                 
                    "Keď som vystúpil na stanici v Michalovciach z vlaku, bol som očarený krojmi žien žiariacimi množstvom farieb." Takto komentoval svoj príchod na "Hornú zem" učiteľ z Dolniakov. Maďar - Uhor v ktorom kolovala aj Francúzska krv, ale  hlavne obdivuhodný impresionista.
                 

                         Teodor Jozef Mousson (1887 Högyész – 1946 Trenčín), žil a tvoril v Michalovciach v rokoch 1911 – 1944. Do Michaloviec ho odsunula školská správa Rakúsko-Uhorskej monarchie čo sa mu v budúcnosti zmenilo na utrpenie. V roku 1918 totiž vzniklo Československo a jeho ako Maďarského učiteľa už nepotrebovali. Bol prepustený a preto často musel chytať do ruky štetec a farby.   Za druhej svetovej že vraj po krčmách predával obrazy po 50 korún. Dnes sa ich cena vyšplhá aj na stonásobok pôvodnej ceny.   Mousson napriek tomu aký mal život, býval pre Michalovčana na ozaj romantickom mieste. Nazývame to  miesto "Hrádok". Na mieste terajšej hvezdárne mal svoj dom. Vyššie je kaplnka - mauzóleum rodiny Stárayovcov - grófov z Michaloviec. Vrch oddávna nazývaný Hrádkom je opísaný už v encyklopédií o Rakúsko- Uhorsku svojho času ako vojensky strategické miesto. Z jednej strany že vraj nedobytné  a z druhej s dobrým prístupom. Jozef T. Mousson mal teda strategické bývanie. Videl na mesto i na okolie. Každé ročné obdobie skúmal pôsobenie slnečných lúčov, ich hru a snažil sa zachytiť realitu z akej vyžaruje samo svetlo. Svetlo, ktoré konzervuje prítomný okamžik, svetlo ktoré je v prítomnej chvíli chvíli tu, a zároveň niekde inde. Vitajte vo svete impresionizmu!             Michalovce J. T. Moussona  vtedy a dnes              Jeho dcéry? Kam kráčajú? V pozadí je Hrádok s kaplnkou na vrchu. Za ňou mal svoj dom.          Toto nie je určite miesto odkiaľ majster Mousson maľoval. Je to jediné ktoré sa približuje originálu na obraze. To pravé miesto je inde. Tak prečo som neodfotografoval ohyb Laborca s pozadím "Hrádku" z toho správneho miesta? Pretože sa nedalo. Mousson si postavil svoj maliarsky stojan zrejme tam kde je dnes hrádza pre reguláciu toku. Možno za jeho čias  sa tam dalo kúpať. Z tohto bodu hrádze by to sedelo, lenže pán Mousson nemal foťák a maľoval bez ohľadu na panoramatické videnie. Teda mne sa do foťáka nezmestil ohyb Laborca a zároveň hrádok v pozadí. Majster Mousson to dokázal. Možno vedome, možno nevedome ...   A prečo je tam dnes tak málo vody? No práve preto sa budovala pri Michalovciach vodná priehrada Zemplínska Šírava, aby sa Laborec nevylieval. A preto je dnes v Laborci miestami tak málo vody. Ale opravte ma, tiež mám pocit že som fotil nejaký kanál:-)          Trh s grécko-katolíckym kostolom v pozadí. A dzivky še chvaľa co novoho kupiľi:-)      No tak toto len hádam. Všimnite si ten oblúk pod strechou veže. To sa nepodobá. Lenže rímsko-katolícka veža o pár desiatok metrov ďalej je už úplne iná. Takže možno to ani Michalovce nie sú, alebo majster improvizoval ...                 dnes je to budova ktorá patrí Zemplínskemu múzeu. Je  to dnes galéria, ale aby bol obraz úplný, patrí k tomu aj fotka nižšie:      Táto budova patrila miestnym grófom a slúžila ako sýpka. K tomu spomeniem že za druhej svetovej vojny po akciách partizánov v okolí a po zabití vinníkov okupantmi zobrali fašisti násilým niekoľko chlapov z vypálenej obce Vinné a okrem iného  mali vybrať práve túto sýpku. Dá sa do nej dostaňť cez okno od parku.:-)          Škola, židovská škola, za Slovenské štátu škola pre Nemecké deti. Myslím že hitlerjugend. Dnes (alebo včera?:-) Hostinec Tatra. V podstate je v tej budove krčma a predaj mäsa.          No nemám pravdu?:-)             V pravo kašteľ nevidno, nezmestil sa do záberu. Pán majster Mousson to dokázal.:-)            A ešte niektoré z obrazov J. T. Moussona                                                              Miesto kde J. T. býval       Dnešná hvezdáreň na mieste majstrovho domu.          Tento múrik s plotom zámerne zachovali ako pozostatok z domu J. T. Moussona          Takúto pekne gotickú kaplnku mal hneď za domom ...        Vždy som chcel napodobňovať Mussonov impresionizmus. Jeho obrazy sú tak živé ... Nie je tam fotografická presnosť v zmysle precíznosti, ale je tam presnosť vnímania farieb mimo čas a priestor. A predsa sa mi fotografia reality zobrazí kdesi v mozgu ak sa dívam na obrazy majstra Moussona. Ale to len duch nahliadol pocit, ktorý inak nezachytíme, len ak cítime túžbu umelca po zobrazení krásy.     Nikdy som jeho impresie nedosiahol, vždy zostane pre mňa neprekonaný. Alebo možno raz? ....                                                                                                                                                                                      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Oneskorene k 17. Novembru ....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Milujeme tých, ktorých ľutujeme?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Liberalizmus trhu, ale otroctvo výroby?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Naozaj ide o komunizmus?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            O liberalizme a pôvode slova idiot
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kamenský
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kamenský
            
         
        jozefkamensky.blog.sme.sk (rss)
         
                                     
     
         Zaváži len to, čo neumiera a pre nás neumiera to, čo umiera s nami. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    285
                
                
                    Celková karma
                    
                                                3.85
                    
                
                
                    Priemerná čítanosť
                    1019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            filozofické zamyslenia
                        
                     
                                     
                        
                            poézia
                        
                     
                                     
                        
                            duchovno
                        
                     
                                     
                        
                            umenie
                        
                     
                                     
                        
                            portréty blogerov
                        
                     
                                     
                        
                            Duch pôsobiaci v čase
                        
                     
                                     
                        
                            alchýmia varenia
                        
                     
                                     
                        
                            angelológia
                        
                     
                                     
                        
                            história
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Otvorený list prezidentovi SR A. Kiskovi.
                     
                                                         
                       Demografia a druhý pilier
                     
                                                         
                       Andy Warhol za 105 miliónov dolárov
                     
                                                         
                       Maňka-Kotleba...0:1
                     
                                                         
                       Mužskosť a ženskosť ako sociálne implantáty?
                     
                                                         
                       Volím život. Aj pre počaté deti
                     
                                                         
                       Lotyšské obrázky
                     
                                                         
                       Štyri generácie
                     
                                                         
                       Slovensko treba spájať
                     
                                                         
                       Uličská dolina
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




