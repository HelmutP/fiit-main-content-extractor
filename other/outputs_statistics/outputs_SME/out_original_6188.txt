
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Varga
                                        &gt;
                Nezaradené
                     
                 Iveta Radičová, dostane opätovne môj hlas 

        
            
                                    20.5.2010
            o
            8:11
                        |
            Karma článku:
                7.53
            |
            Prečítané 
            314-krát
                    
         
     
         
             

                 
                    Žena v politike a žena v slovenskej politike na  vysokom straníckom, politickom poste je skoro zvláštnosťou, i keď boli pokusy - Pani Keltošová, Brigita Schmognerová, Katarína Tóthová, Zuzana Martináková, Monika Flašíková - Beňová  a samozrejme  Iveta Radičová, ako predstaviteľka nového SDKÚ DS, ktorému aj mienim dať svoj volebný hlas, teda konkrétne aj pani Radičovej asi tak, ako to bolo naposledy v prezidentských voľbách, kde mala zvíťaziť slušnosť nad neslušnosťou, ale ukázalo sa, že u nás je akosi všetko naruby .
                 

                 
  
   Slovenská politika v sukni ? Prečo nie ? Vždy lepšie, ako politika, ktorej sme svedkami včera a dnes . V podstate som aj rád, že Pani Radičová neuspela na funkciu prezidenta, i keď akosi nejde si zvyknúť, že líder mojej strany, ktorú mienim voliť a ktorú volím od jej vzniku, je žena a pravdu povediac celkom pohľadný politik v sukni.   S l u š n o s ť .    Najskôr sa tak ukazuje, i keď je to predčasné, že SMER SD nezíska viac ako 33% podporu voličskej základne, ako aj že HZDS ĽS sa s veľkou pravdepodobnosťou do parlamentu nedostane / možno je to len moje prianie /, pričom SNS bude možno kopírovať 6,5- 7 % ný volebný zisk, čo nijako nedáva predpoklad vzniku staronovej väčšinovej vládnej koalície, i keď socialistom  ako víťazom volieb najskôr prezident umožní zostaviť vládu, ale ak sa stredopravé strany dohodnú a ani jedná z nich sa neulakomí, čo najskôr jedna bude málo, tak socialisti na Slovensku dovládli, čoby ako tak ukazovalo občanovi našej vlasti na lepšie časy . Už len z dôvodu, slušnosť nadovšetko, slušnosť v našej vlasti nevymrela, slušnosť patrí aj do politiky a hlavne slušnosť v celom spektre ľudského bytia včítane podnikania dáva aj predpoklad slušného života všetkým občanom Slovenska bez rozdielu .    Z m e n y   v  dobrom pre všetkých    V podstate je načase urobiť po štyroch rokoch také zmeny v politike, ktoré prinesú konečné ovocie odriekania dlhých rokov reforiem a  celosvetovej krízy, v plnej miere začneme využívať voľné finančné zdroje uložené v európslym fondoch, ktoré celé štyri roky využívali najmä mecenáši vládnych strán, privrženci a vlezmidozadku ľudia blízki socanom, HZDS, národniarom, ale našťastie v svojej malosti a najmä byrokracii pre nás ostatných nestačili minúť veľa, teda skôr málo, keď v čerpaní fondov sa Slovensko moce na chvoste EÚ  a je ako taký predpoklad, že po vzniku stredopravej vlády na čele s SDKÚ DS alebo KDH sa veci ohľadom čerpania financií  z fondov dostanú do správnych koľají a nie podľa toho nad ktorou dedinou, firmou, aká politická vlajka plápola .   T r p e z l i v o s ť   ... ruže prináša, a tak trpezlivo 4 roky, i keď nie bez následkov, mnohí čakáme na lepšie časy, ktoré počas vládnutia SMERU SD neprišli, teda lepšie povedané väčšina sa ich nedočkala.   Sľuby slovenských politikov sa nedajú brať vážne, až na uťahovanie opaskov, ktoré doposiaľ fungovalo perfektne, ale zo slov lídra SDKÚ DS vyznieva : Už bolo dosť skromnosti pre bežného občana, pri dobrom vládnom managmente vieme ušetriť, ale nie na tebe obyčajný volič, ty si už svoje odtrpel .   Nuž  chce sa mi veriť pani Radičovej, pretože to sú jej slová, že občan má už nárok na kvalitatívne lepší život aj bez zbytočného byrokratického preháňania úradmi, bez zlých zákonov, s maximálnou ústretovosťou vo všetkom, čo život človeku prináša.   Podľa všetkého sociálni demokrati voľby vyhrajú, ale nebude im umožnené vytvoriť staronovú koalíciu, je nutné povedať : Chvála Bohu  Slovensko ,    ... i keď  vládnuť v stredopravej zostave aj s novým členom Sas tiež nebude ľahké .                                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Varga 
                                        
                                            Kiska alebo Fico ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Varga 
                                        
                                            Vybrané slová alebo ako uspieť na SMEblog
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Varga 
                                        
                                            Konečne máme vládu bez účasti bolševika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Varga 
                                        
                                            Keď vás dopravný policajt označí za sprostého
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Varga 
                                        
                                            Keď bohatí sú každým dňom bohatší i na úkor tých najchudobnejších..
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Varga
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Varga
            
         
        jozefvarga.blog.sme.sk (rss)
         
                                     
     
        Iba obyčajný človek,ktorý celý život prežil hlavne medzi obyčajnými ľuďmi,ale mnohokrát so zaujímavými životnými príbehmi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    503
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




