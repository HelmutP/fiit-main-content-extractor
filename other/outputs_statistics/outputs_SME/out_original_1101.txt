
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Rias
                                        &gt;
                Zábava
                     
                 List od švekry 

        
            
                                    8.8.2005
            o
            7:08
                        |
            Karma článku:
                12.64
            |
            Prečítané 
            59005-krát
                    
         
     
         
             

                 
                    Drahý žecu,
                 

                 bizovňe še čuduješ, že po teľo rokoch še ozývam. Ale obecala som dzifčeťu, že ci napíšem. Keľo je to už rokov, čo ce ochabila? Das pejc? Možno i vecej budze. 
 Jak še hutorí, čas najlepší dochtor, šicke rany zahojí. I mne še už noha zahojela, čo si ma raz ožartý zdrilil z garadyčoch. 
 Teraz už znám, že to veľká chyba bula, kedz som še k vám do domu nascehovala. Do šickoho som še starala a ľem mojej Ruženke som stranu trimala. Musel si poriadne cerpec. 
 Nečudo, že si si občas čerknul betehy a neraz pozdno do večera v karčme šedzel. Vedz každý poriadny chlop sebe vypije, no ňe? Dakto ľem dakus, iný vecej, no a ty si vypil vecej. Ale ani še ci nedzivím. Ta vedz tá moja Ružena poriadna ľompa bula. Vidzela mi, jak še o tebe stará! Na frištik chľeba so šmaľcom, na obid si harčok trepanky musel vylygac, dobre si hvarel, že chucí jako koňské šiky. Na večir ci uvarela kendericu, alebo ľem postné bandurky si mal. A kedz še jej zochcelo na poľo zajsť, tak i hryžacu dziňu si dostal. Vedz ani poriadny kavej ňeznala uvaric, chucil jako krochmaľ. Veru nerada še okolo šporheltu obracala. Ľem by še parádzila a hodiny pred džveredlom vystávala. A ani čistotná bárs nebula. Brudny budar vyčuchac? Ta dze! Prac ani pigľovac še jej nechcelo, aj gráty si musel sám umyvac. No, piľná pčola to nebula, do roboty še bárs nešarpala. 
 Ale ani ja ci nebula bárs dobrou švekrou. Právom si každému vo valale hvarel, jak ci tá stará rapucha ničí život. Šľepé sme buli, nevidzeli sme jak cerpíš. Keľo razy som sebe želala, aby me guta ulapela za to, že som še k vám nascehovala. Verabože, šaľena ja bula. Šicko mohlo buc inak. 
 Ale vidzíš, drahý žecu, že ani po teľo rokoch sme na tebe nazapomnuli. Ruženka hutorí, že si ce predstavuje s veľkym bembeľom, určite ti už kosci nečerkajú. A že by še rada stala nadraguľou. Už še jej i snívalo o malym kandratym bauľe. 
 Ňemala ce ochabic, teraz to už zná i ona. 
 Jak v tebe zostalo choľem falatočok ľubosci, friško odpíš, budzeme ci blahoslavic. Vedz stará láska nehardzavie, jak še hutorí. Naša kapura budze pre tebe stále otvorená. 
 
 Tvoja švekra Amála 
 Dodanok: 
 Skoro by mi zapomnula. K tomu džegpotu, co ši ulapel v lote, ti zo šerca blahoželáme. 
 
 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Rias 
                                        
                                            Toto má byť akože umenie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Rias 
                                        
                                            Prečo sú vínkari dobrí ľudia alebo Ako Lajči na vysávač zanevrel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Rias 
                                        
                                            Markíza opäť boduje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Rias 
                                        
                                            Rada blogerom - držte sa témy!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Rias 
                                        
                                            Intímnosti politikov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Rias
                        
                     
            
                     
                         Ďalšie články z rubriky próza 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        David Stojaspal 
                                        
                                            Dukelské zápisky 1944-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Igor Čonka 
                                        
                                            Ellis Parker Butler - Prasce sú prasce I
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Kto nezažil, neuverí. . . (46)
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Oni
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Odrazový mostík
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky próza
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Rias
            
         
        rias.blog.sme.sk (rss)
         
                        VIP
                             
     
         Spýtajte sa Ondra... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    262
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3929
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Aforizmy
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Môj názor
                        
                     
                                     
                        
                            Epigramy
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Literárne maličkosti
                        
                     
                                     
                        
                            Politikárčenie
                        
                     
                                     
                        
                            Odpozorované
                        
                     
                                     
                        
                            Ondro
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Moje básne
                                     
                                                                             
                                            Neverte vlastným ušiam
                                     
                                                                             
                                            Aké bude počasie?
                                     
                                                                             
                                            V čom je rozdiel
                                     
                                                                             
                                            Výlet
                                     
                                                                             
                                            Hviezdy a kráska
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Je ich niekoľko...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Sieťovka
                                     
                                                                             
                                            weby
                                     
                                                                             
                                            online sudoku
                                     
                                                                             
                                            Krížovky online
                                     
                                                                             
                                            Slovenský zväz hádankárov a krížovkárov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O Saint-Exupérym, Malom princovi a novej knihe Petra Sísa.
                     
                                                         
                       Fico o emisnej "nevine"
                     
                                                         
                       Kto chce ešte po ústach?
                     
                                                         
                       Nože a nožiarska výroba na Slovensku - pohľad do histórie
                     
                                                         
                       Socialistické vetry - kolobeh života
                     
                                                         
                       SMERácki nímandi ničia životy detí
                     
                                                         
                       Farebný kút Viedne
                     
                                                         
                       Niekomu hrozí basa
                     
                                                         
                       Centrálny vestník - vyťahovanie peňazí z vreciek neinformovaných
                     
                                                         
                       Dávajte si pozor! Môžete prísť o dieťa.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




