

 Dva roky pre vás sprístupňujeme literatúru na internete, pomôžte nám jej prístupniť ešte viac. Tak znie výzva, s ktorou sa na verejnosť obracia dobrovoľnícky projekt Zlatý fond denníka SME. 
 Stovka dobrovoľníkov z celého Slovenska preniesla na internet za posledné dva roky tisíc najvýznamnejších diel slovenskej literatúry. Medzi najpopulárnejšie patria napríklad Mor ho, Marína, román Demokrati, či Dobšinského rozprávky. 
 Okrem diel staršej literatúry však chce zdigitalizovať aj autorov 20. storočia. Na to však potrebuje súhlas ich dedičov. Teraz ich chce skúsiť získať. 
  
 "Chceli by sme na internet okrem štúrovcov dostať napríklad aj autorov medzivojnovej slovenskej literatúry. Autorský zákon nám to však dovolí urobiť až okolo roku 2050," hovorí Tomáš Ulej, koordinátor projektu. 
 "Pritom mnohí nie sú už vydávaní, uverejnenie na internete by pomohlo dostať ich opäť bližšie k ľuďom. Ak nám dajú súhlas ich dediči a potomkovia, môžeme to urobiť už dnes," hovorí. 
 Údaje o tom, kto je dedičom konkrétneho autora, nie sú verejne dostupné. Jediný spôsob je ich vyhľadať. Digitalizátori preto vytvorili stránku "Celoštátne literárne pátranie", ktorá má pomôcť dedičov nájsť. "Veríme v silu internetu a dobrého slova - že sa vďaka nim správa o našej výzve dostane k tým správnym ľuďom a tí nás napíšu alebo zavolajú," hovorí Ulej. 
 "Diela, ktoré nevychádzajú celé desaťročia sa stanú opäť dostupnými - každému bez rozdielu," dodáva. 
 Čo môžete urobiť vy? 
 
 Pozrite sa na stránku zlatyfond.sme.sk/patranie. Poznáte niektorého z dedičov významných slovenských autorov, ktorých tu uvádzame? Skúste ho osloviť a napíšte nám. 
 Šírte podľa inštrukcií informáciu o našej výzve a stránke medzi vašimi priateľmi - či už pomocou sociálnych sieti, e-mailu alebo hoci aj na na pive. Neexistuje efektívnejší spôsob šírenia informácií ako klebeta. 
 Uverejnite našu výzvu aj na vašej webovej stránke.  Viac informácii
 
 

