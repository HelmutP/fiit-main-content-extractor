
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Kubuš
                                        &gt;
                Južný Kaukaz 2010
                     
                 Južný Kaukaz 2010 

        
            
                                    30.6.2010
            o
            9:14
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            907-krát
                    
         
     
         
             

                 
                    Akosi rýchlo beží ten čas. Už to bude viac než pol roka čo ma napadlo, že by som sa rád pozrel na Kaukaz. Letenka do Baku leží na poličke vyše dvoch mesiacov, lístok na vlak do Kyjeva dva, tri týždne a dnes sa to celé začne...
                 

                 Je to zvláštny pocit, keď sa začiatok cesty nepočíta na mesiace, týždne ba ani dni, ale na hodiny. Človek má zbalený batoh a každú chvíľku si predstavuje iné miesto, inú krajinu, snaží sa tam v myšlienkach dostať ešte skôr ako sa tam fyzicky ocitne. Veď v poslednom čase sa už aj tak po každom z nich prešiel na stránkach cestopisov či kníh.    Plány máme v hlave, plány máme na papieri, ale aké to nakoniec bude sa dozvieme priamo na mieste. Začneme v azerbajdžanskom Baku, prejdeme touto zaujímavou krajinou až do Gruzínska, kde by sme si chceli pozrieť Tbilisi s okolím, Gori, spraviť si výlet pod Kazbeg, ísť do Arménska a hľadať staré arménske kostolíky, pozerať na hladinu jazera Sevan, túlať sa Jerevanom, hľadieť na majestátny Ararat, objavovať čím žije Karabach, preliezť skalnatou gruzínskou Vardziou, navštíviť gruzínske Svaneti s mestečkami s kamennými vežami, okúpať sa v Batumi, či v neposlednom rade sa poflakovať východným Tureckom, aby sme mesačnú cestu mohli zakončiť v milovanom Istanbule...           Posledné roky som zvykol z ciest písať správy priamo na svoju stránku. Ak Vás to zaujíma, môžete ich sledovať a vždy keď bude čas či možnosť napísať pár riadkov, ozveme sa.       Stačí prísť na moju stránku: www.tom182cesty.wz.cz a tam ma nájdete.       Majte sa pekne :) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Libanon 2014 (foto)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Buzkaši na brehu kirgizského Issyk Kul
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Sýrska Resafa. Prekvapenie v púšti
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Tatev. Najkrajší kláštor Arménska
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Severný Cyprus 2013 (foto)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Kubuš
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Kubuš
            
         
        kubus.blog.sme.sk (rss)
         
                        VIP
                             
     
         Milujem cestovanie, cudzie krajiny, jedlá, čaj, Turecko, Blízky či Stredný východ, Indiu, fotografovanie, písanie..no jednoducho CESTOVANIE!!! mám sen precestovať celý svet, zdvihnúť sa a ísť, zastaviť sa na miestach po ktorých túžim a pokračovať až na jeho koniec, potom sa vrátiť a ísť opäť, ale inou cestou...   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    292
                
                
                    Celková karma
                    
                                                8.28
                    
                
                
                    Priemerná čítanosť
                    3047
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Grécko s batohom 2007
                        
                     
                                     
                        
                            Káhira - Istanbul 2008
                        
                     
                                     
                        
                            Čarovná Perzia 2008
                        
                     
                                     
                        
                            Central Asia, Iran 2009
                        
                     
                                     
                        
                            Južný Kaukaz 2010
                        
                     
                                     
                        
                            Turecko, Irak 2010
                        
                     
                                     
                        
                            Turecko
                        
                     
                                     
                        
                            Severná Európa
                        
                     
                                     
                        
                            Maroko
                        
                     
                                     
                        
                            Monako
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Blízky Východ - Stredný Východ
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Francúzsko
                        
                     
                                     
                        
                            Anglicko
                        
                     
                                     
                        
                            Benelux
                        
                     
                                     
                        
                            Blízkovýchodné dobrodružstvo
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Grécko
                        
                     
                                     
                        
                            Ukrajina
                        
                     
                                     
                        
                            Rusko
                        
                     
                                     
                        
                            Irak
                        
                     
                                     
                        
                            Nemecko - Rakúsko
                        
                     
                                     
                        
                            Malta
                        
                     
                                     
                        
                            Tunisko
                        
                     
                                     
                        
                            Čechy a Morava
                        
                     
                                     
                        
                            Cyprus
                        
                     
                                     
                        
                            India, Nepál 2013
                        
                     
                                     
                        
                            Juhovýchodná Ázia
                        
                     
                                     
                        
                            Arábia
                        
                     
                                     
                        
                            Balkán
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Za zvoncami karaván
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Vilček
                                     
                                                                             
                                            Renáta Kuljovská
                                     
                                                                             
                                            Braňo Skokan
                                     
                                                                             
                                            Frenky Hříbal
                                     
                                                                             
                                            Zdenko Somorovský
                                     
                                                                             
                                            Peter Dzurinda
                                     
                                                                             
                                            Marek Pšenák
                                     
                                                                             
                                            Peter Gregor
                                     
                                                                             
                                            Andrej Hajdušek
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dobrodruh
                                     
                                                                             
                                            CestovanieSvetom
                                     
                                                                             
                                            Blízky Východ
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo navštíviť Thajsko?
                     
                                                         
                       Beslan desať rokov po tragédii
                     
                                                         
                       Skopje - mesto, kde sa snúbi nové so starým
                     
                                                         
                       Krížom čarovnou, ale chudobnou Indiou
                     
                                                         
                       Tartu a Čudské jazero
                     
                                                         
                       Škandinávsky road trip: Bergen
                     
                                                         
                       Cestoval som v Afganistane. Blázon alebo fajnšmeker?
                     
                                                         
                       Lahemaa - v krajine zátok, lesov a močiarov
                     
                                                         
                       V horskom pohraničí Čečenska a Dagestanu
                     
                                                         
                       Cez Turkmenistan a Uzbekistan do centra Hodvábnej cesty
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




