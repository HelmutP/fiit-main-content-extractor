
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomír Dobrovodský
                                        &gt;
                Fikcia, recesia
                     
                 Modrákov sprievodca politickými presvedčeniami 

        
            
                                    7.6.2010
            o
            0:00
                        (upravené
                7.6.2010
                o
                1:07)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            425-krát
                    
         
     
         
             

                 
                    Toto je krátky popis presvedčení pre Modrákovho sprievodcu politickou vojnou na diskusiách SME.
                 

                 Zákonné dobro: Postava koná v hre len absolútne a nezištné dobro. Snaží sa pomôcť ostaním aj keby za to mala obetovať svoj život. Verí, že usporiadaná, silná spoločnosť s dobre organizovanou vládou môže dosiahnuť lepšieho života pre väčšinu ľudí. Verí, že na vytvorenie kvalitného života musia byť vytvorené a dodržiavané zákony. Vie, že keď ľudia rešpektujú zákon a snažia sa pomáhať ostatným, celá spoločnosť prekvitá. Usiluje sa o tie veci, ktoré prinesú úžitok väčšine ľudí a spôsobia malú škodu.  Neutrálne dobro: Tieto postavy veria, že rovnováha síl je dôležitá, ale že vzťah poriadku a chaosu nezmierňuje potrebu dobra.  Zmätené dobro: Zmätene dobré postavy sú silní individualisti. Verí vo všetky cnosti dobroty a správnosti, ale nemajú pochopenie pre zákony a obmedzenia. Ich činy sú vedené ich vlastným morálnym kompasom, ktorý (aj keď je dobrý) nemusí vždy byť v úplnej zhode so zvyškom spoločnosti.  Neutrál: Naozaj neutrálne postavy veria v absolútnu rovnováhu síl a odmietajú vidieť činy, ako len zlé alebo dobré. Skutočne neutrálne postavy sú niekedy prinútené k celkom divným spojenectvám.  Zákonné zlo: Tieto postavy veria v zneužívanie spoločnosti a ich zákonov k vlastnému prospechu. Štruktúra a organizácia povyšuje tých, ktorí si zaslúžia vládnuť, a taktiež zaisťuje jasne určenú hierarchiu medzi pánom a sluhom.  Neutrálne zlo: Neutrálny zlé postavy sú v prvom rade zamerané na seba a svoj postup. Nemajú žiadny osobitný záujem na prácu s inými alebo, v tomto prípade, rokovania na vlastnú päsť. Ich jediným záujmom je prebíjať sa vpred. Ak existuje rýchly a jednoduchý spôsob, ako mať úžitok, či už je to legálne, sporné alebo jasne zlé, radi toho využijú.  Zmätené zlo: Tieto postavy sú tí, ktorí by chceli zničiť všetko, čo je dobré a organizované. Zmätene zlé postavy sú motivované túžbou po osobnom úžitku a potešení. Nevidia vôbec nič zlé na tom, že si vezmú čo chcú, akýmkoľvek spôsobom. Zákony a vláda sú podľa nich nástroje slabochov, ktorí sa o seba nedokážu postarať sami. Silní majú právo vziať si, čo chcú, a slabí sú tu od toho, aby boli vykorisťovaní. Keď sa spoja zmätene zlé postavy, nie sú motivované túžbou spolupracovať, ale skôr postaviť sa mocným nepriateľom. Takáto skupina môže byť udržovaná pohromade silným vodcom, schopným donútiť svojich podriadených k poslušnosti. Keďže je vodcovstvo založené na hrubej sile, vodca býva pri prvom náznaku slabosti vymenený kýmkoľvek, kto mu dokáže odobrať jeho postavenie akýmkoľvek spôsobom.       Preložené zo stránky: Sprievodca hry Baldur's Gate 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Dobrovodský 
                                        
                                            Intuícia z pohľadu neurovedy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Dobrovodský 
                                        
                                            Evolúcia Satana: Ako sa z božieho služobníka stal nepriateľ Boha
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Dobrovodský 
                                        
                                            Nový zákon: Ježišovo vzkriesenie bolo asi inšpirované iným príbehom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Dobrovodský 
                                        
                                            Stručné dejiny prešľapov Katolíckej cirkvi a kresťanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Dobrovodský 
                                        
                                            Prečo pôjdem na protest Gorila 27.1.2012
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomír Dobrovodský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomír Dobrovodský
            
         
        dobrovodsky.blog.sme.sk (rss)
         
                                     
     
         Fajn chlapík. IT developer, 8 rokov v branži. Mám záľubu v literatúre faktu - religionistika, neuroveda, psychológia, mytológia, fyzika, dejiny atď. Rád čítam aj historické romány, fantasy, mýty a legendy. Vo voľnom čase sa zaujímam o beh, turistiku a bicyklovanie. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    79
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2009
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Biblia, viera a dejiny
                        
                     
                                     
                        
                            Systém
                        
                     
                                     
                        
                            Myšlienky
                        
                     
                                     
                        
                            Dobrovoľníctvo
                        
                     
                                     
                        
                            Fikcia, recesia
                        
                     
                                     
                        
                            Krátke príbehy
                        
                     
                                     
                        
                            Cirkev svätého Svetového Mieru
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Som reprezentatívna vzorka Slovenska
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Subliminal: How Your Unconscious Mind Rules Your Behavior
                                     
                                                                             
                                            Myšlení rychlé a pomalé
                                     
                                                                             
                                            Hledání Spinozy (Antonio Damasio)
                                     
                                                                             
                                            Inkognito
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Hilary Hahn
                                     
                                                                             
                                            Enya
                                     
                                                                             
                                            Lindsey Stirling
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Greenpeace
                                     
                                                                             
                                            Aliancia Fair-play
                                     
                                                                             
                                            Zelená hliadka
                                     
                                                                             
                                            Ľubomír Dobrovodský
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Krátke príbehy
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




