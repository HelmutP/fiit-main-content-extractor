
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Ryšavý záletník 

        
            
                                    24.4.2010
            o
            8:32
                        (upravené
                25.4.2010
                o
                14:58)
                        |
            Karma článku:
                7.42
            |
            Prečítané 
            1327-krát
                    
         
     
         
             

                 
                    Včera som si mailovala s priateľkou Bibkou. Spomenula mi aktuálny príbeh o kocúrikovi bratovej rodiny. Páčil sa mi. Rozhodla som sa,  že ho sprostredkujem aj vám...
                 

                 
  
   Ide o Oscara, presne povedané o kocúra Oscara. Krásneho ryšavého, prítulného, "rodinne založeného" miláčika. Trvalé bydlisko nemenil dva roky a za ten čas sa stal ozajstným rodinným kamarátom. Často si  aj od cudzích vyslúžil obdiv, lebo takých krásnych  ryšavých kocúrov zase až tak veľa nie je. Dlho si mysleli, že v tejto farbe je v dedine jediný.   Občas sa stalo, že Oscar dostal túlavé topánky, vlastne labky, cha... a vybral sa na výlet. Občas bol preč aj celú noc, no vždy sa domov vrátil a tváril sa, že o nič nešlo...   Minulý týždeň sa Oscar stratil. Zmizol v priebehu dňa, nevrátil sa ani na noc, ani ráno, ani na druhý deň, ani na ďaľší. To už vznikla v rodine nervozita, pretože taký dlhý výlet ešte neabsolvoval. Už dostali strach a veru sa vybrali aj na obchôdzku, či ho niekde na záletoch "nevymáknu...". Nie, nenašli žiadnu stopu, ani susedia o ňom nič nevedeli.   Stále sa však nevzdávali a  milovaného kocúrika hľadali ďalej. Dostali sa k hrádzi vzdialenej kúsok od domu. Dcéra skríkla, pretože zbadala nehybne ležiacu chlpatú  ryšavú kôpku. Pribehol k nej otec a musel skonštatovať, že ide o Oscara. Ryšavý, síce dosť zdevastovaný, no jednoznačne kocúr. Prihliadnuc na okolnosti, v presvedčení, že našli  ich záletníka, vzali ho  so sebou. Jeho pozostatky pochovali so všetkými poctami vedľa hrobčeka ich psíka. Padli aj slzičky, za Oscarom bolo všetkým veľmi ľúto. Nakoľko majú psíka, zajačika, povedali si, že nové mača si už  ale nezaobstarajú a Oscar  aj tak bude naďalej žiť ich v spomienkach.   Prišiel štvrtok tohto týždňa a mladšia  dcéra, hľadiac  z okna, zaregistrovala na záhrade pohyb akéhosi chlpatého štvrornohého stvorenia. Vybehla na dvor a zostala stáť v nemom úžase. Po chvíľke, keď sa zo šoku a prívalu radosti spamätala, začala z plných pľúc vykrikovať: "Poďte všetci sem, Oscar vstal z mŕtvych!". Postupne vybehli na dvor všetci členova rodiny, nechápajúc čo to dievča vykrikuje. Každý však ihneď pochopil. Oscar stál pred nimi. Síce trochu špinavý, trochu pochudnutý, ale bolo nad slnko jasné, že je to ich  ryšavý, krásny, milovaný, s huncútskym kukučom na nich upretý Oscar!   "Vstal z mŕtvych, vstal z mŕtvych", vykrikovala do zbláznenia mladšia dcéra a už si ho túlila k hrudi. Kocúrik akoby cítil, že je stredobodom pozornosti, hrdo vztýčil hlavu a doprial rodine, aby sa vytešovala  z jeho tajomného návratu. Oscara vykúpali, nakŕmili, rozmaznávali   pamlskami, hladkali a dohovárali mu, ale len tak, aby sa nepovedalo, cha... Oscar ich počúval, ale premáhal ho spánok a tak sa odobral  na svoje obľúbené miestočko, oddychovať po dlhom výlete.   Vtedy si však rodina  spomenula na pohreb, ktorý tak decentne, so všetkými poctami,  vystrojili pred pár dňami. Koho teda pochovali? Komu tá ryšavá kôpka teda patrila? Doteraz nevedia, no snažia sa po dedine rozchýriť, čo sa udialo, aby potencionálny majiteľ pochovaného kocúra sa dozvedel, čo sa stalo...   Takže predsa nebol v dedine Oscar jediným ryšavým kocúrom!   Oscar si už pár dní užíva teplo  domáceho kozuba a rodinnej priazne, no doteraz svojim drahým neprezradil, kde sa celé dni túlal... Vyzerá to tak, že jeho výlet zostane navždy ukrytý pod rúškom tajomstva!   Za tento kúsok si ryšavý Oscar zaslúži "Oscara", čo poviete?       Foto: zdroj internet   (foto pravého Oscara, žiaľ, k dispozícii nemám)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (46)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




