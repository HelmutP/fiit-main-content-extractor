

 V prvom rade treba vylúčiť úvahy o solidarite s Gréckom. Treba sa sebecky pýtať, čo najviac pomôže nám, na Slovensku. Ak pomocou Grécku zabránime ešte hlbšiemu úpadku eura, Eurozóny a teda aj slovenskej ekonomiky, tak potom pomôcť treba, lebo pomocou Grécku pomáhame predovšetkým sebe. Ak budúcnosti eura a EU viac pomôže ponechanie Grécka a všetkých ostatných kandidátov na štátny bankrot (Portugalsko, Španielsko, ...), tak potom treba nechať Grécko na seba a vylúčiť ho z Eurozóny. Samozrejme treba pritom dbať na to, aby nás nestiahlo so sebou, lebo tuším presne toto už robí. Možno sú aj ďalšie alternatívy, len ja sa necítim byť odborníkom v tejto oblasti. Zostáva mi len dúfať, že dobre platení odborníci u nás a v iných štátoch, či centrálach EU, sa konečne poučia a urobia správne rozhodnutie. Len si netreba robiť ilúzie. Doplácame na to a budeme doplácať tak či tak. 
 . 
 . 
  
  
  
  
  
  
 Veľmi som podporoval vstup Slovenska do Eurozóny, pričom mojím argumentom bola predovšetkým snaha zabrániť dobrodruhom, ktorí sa u nás pravidelne dostávajú do pozície vplyvu, aby podmínovávali našu ekonomiku, ako to urobili v Grécku, Maďarsku a ďalších už menovaných i nemenovaných štátoch. Viac som dôveroval bruselským, než domácim hlavám. Moja dôvera k Bruselu sa síce naštrbila, ale i tak si myslím, že Slovensko by v tom lietalo tak či tak, aj keby malo vlastnú menu. Nakoniec sa budeme musieť prispôsobiť rozhodnutiu najsilnejších hráčov Eurozóny, neodvažujem sa hádať, komu sa budú prispôsobovať oni, ale utešujem sa, že i to je lepšie, než sa prispôsobovať prevoditeľnému rubľu alebo čomukoľvek inému. Momentálne vraj americký dolár i japonský jen posilňuje, ale priskočiť k týmto menám sa Slovensku tuším nedá. Ani moje úspory nie sú dostatočne veľké na to, aby sa mi vyplatilo presúvať ich z euro-účtu na dolárový, jenový, či iný. A tak mi iné nezostáva len počúvať správy a hlavne si robiť svoju robotu. Moja práca je to najlepšie, čo môžem urobiť pre ľudstvo celého sveta, Európu, vlasť a samozrejme a predovšetkým, pre seba. K tomu vyzývam aj ostatných! 
 . 
  
  
  
  
  
  

