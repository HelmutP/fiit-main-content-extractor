
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondro Mikláš
                                        &gt;
                Tech
                     
                 Geolokačné hry – načo? 

        
            
                                    15.6.2010
            o
            17:34
                        |
            Karma článku:
                4.13
            |
            Prečítané 
            974-krát
                    
         
     
         
             

                 
                    Po sociálnych sieťach zaznamenávajú obrovský boom aj služby, ktoré pracujú s geolokáciou. Foursquare a Gowalla sú asi dva najznámejšie príklady týchto služieb. Ja si však kladiem otázku: Na čo sú dobré tieto služby?
                 

                 Foursquare, alebo Gowalla sú vlastne obrovské hry. Hra spočíva v tom, že niekde ste, tak sa prihlásite pomocou mobilu, alebo iného mobilného zariadenia a oznámite svetu, že tam ste. Prevádzkovatelia týchto "sránd" to urobili perfektne. Neprihlasujete sa len tak, ale dostávate za to všelijaké virtuálne odmeny vo forme medailí či iných vyznamenaní. Toto všetko stačí ľuďom na to, aby dávali každý deň každému vedieť, kde si boli práve uľaviť. Samozrejme nech si každý robí čo uzná za vhodné, a ak má niekto potrebu dávať celému svetu na známosť, že si v New Yorku sadol na tri a pol minúty v preplnenom Starbuckse na zadok tak nech to vytrúbi do celého sveta. Čo mňa zaráža, že na takúto triviálnu činnosť ľudstvo potrebovalo extra služby, pričom jednu - na to úplne stvorenú - už má: Twitter. Dobre priznávam, že na profilovej stránke Twitteru si neviete tak prehľadne prezrieť, kde sa človek kedy nachádza, ale to už narážam na moju neschopnosť pochopiť význam niečoho takého, ako je oznamovanie ľudstvu, kde pijem pivo. Ak chcem, aby moji priatelia vedeli kde sa nachádzam - prečo to nenapísať jednoducho na Twitter? Ten má takisto geolokačnú funkciu (a už dosť dlho) a pomocou hocijakého klienta (napr. TweetDeck) viem veľmi jednudocho ukázať svoju polohu na mape. A zaiste nechodím po podnikoch len kvôli tomu, aby som sa tým mohol pochváliť na nejakej stránke. Ďalší problém vidím v zbytočnom zrkadlení zbytočného obsahu. Veľa ľudí, ktorí sú mojimi followermi na Twitteri zahlcuje priestor správami, ktoré sú iba odrazom zo služby Foursquare. Ak je teda ten Foursquare tak skvelou službou, prečo si to nenechajú len pre seba a pre používateľov tej služby? Ale updatovať takto aj Twitter? Znovu si kladiem otázku: Prečo to nestačí dať ľuďom vedieť iba pomocou Twitteru? Musím to následne nájsť aj vo svojom Google Buzz streame a poslednú dobu aj na Facebooku? A pri tom sú to tak zbytočné informácie.....zbytočné ako pre koho.   Teraz sa zahrám na paranoidného konšpirátora a napíšem o jednom veľkom riziku. Predpokladajme, že sme sa zaregistrovali na Foursquare a sme poctivý človek, ktorý nijak nepodvádza. Každý boží deň chodíme s mobilom či tabletom v ruke a pilne sa prihlasujeme z každej riti na svete v ktorej sme, aby moji kamaráti vedeli v ktorej riti na svete som. Začnú sa nám množiť úspechy, sme pánmi svojho mesta (aspoň virtuálne na Foursquare), všade sme boli najviackrát, dokonca už máme v miestnej pizzerii aj zľavu, keďže sa odtiaľ vždy prihlásime. Máme desiatky priateľov, ktorí sa tiež hlásia zo svojích ríť na svete a lačno pozerajú na naše úspechy. Ale medzi takýmito priateľmi sa nájde jeden lapaj - výmyselník, ktorý tiež lačno sleduje, kde sme a čo tam robíme. Až raz prídeme veselo domov, chtiac si nabiť svoj smartfón, lebo sa nám od toľkého logovania sa na foursquare vybil. Bác - ani len nabíjačka doma nezostala. Byt komplet vybielený. Zlodeji museli ísť na istotu, ale odkiaľ preboha mohli vedieť, že nebudem doma?? Sú tri možnosti: a) mali šťastie b) pracne čakali mesiace v aute a zapisovali si moje odchody a príchody a počkali na vhodnú chvíľu a potom c) stačilo im pár týždňom sledovať náš pulz veľkomesta, ktorý nám prúdi v žilách a zistili, že vtedy a vtedy sme stále tam, a že namiesto toho, aby sme boli v robote sme na pive. Je jasné, že nás nešli nabonzovať zamestnávateľovi, išli nás rovno vybieliť. Toľko paranoidná ukážka - ktorá však nemusí byť ďaleko od reality. Stopovať takto ľudí nie je vôbec ťažké. Kritici sociálneho webu (s ktorými ja nesúhlasím) hovoria neustále o strate súkromia. Súdny človek na web nedáva to, čo nechce, to čo by ho mohlo nejako poškodiť. Lenže služby typu Foursquare z ľudí vytiahnu niečo lepšie. Ich stereotypy, ich chute bez toho, aby sa na to nejakým spôsobom opýtali. Ak 25 z 30 najviac navštevovaných miest u vás je piváreň, tak je jasné, že asi nebudete milovníkom "božolé". Ak sa hovorí o sociálnom inžinierstve, tak geolokačné služby o vás prezradia všetko bez toho, aby ste o tom vlastne vy napísali. Jedine, ak podvádzate a v skutočnosti sedíte doma. Ale potom na čo sa hrať geolokačnú hru?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Mikláš 
                                        
                                            Opera 10.50 - svetlá budúcnosť surfovania
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Mikláš 
                                        
                                            5 rozšírení, ktoré robia z Firefoxu bezkonkurenčný prehliadač
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Mikláš 
                                        
                                            FireFox je najlepší (s veľkým otáznikom)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Mikláš 
                                        
                                            Nepotrebujem si pamätať heslá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Mikláš 
                                        
                                            Mojich "osemtoro" čo sa Google Chrome týka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondro Mikláš
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondro Mikláš
            
         
        miklas.blog.sme.sk (rss)
         
                                     
     
        Som obyčajný chlapík s foťákom a klávesnicou, ktorý rád fotí to čo vidí a opisuje veci, ktoré má rád a ktoré ho zaujímajú.Všetci ste tu vítaní a ak sa Vám tu páči, tak sa nebojte pridať komentáre či zdieľať články na sociálnych sieťach. Za to budete mať u mňa veľké pivo.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2275
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zážitky
                        
                     
                                     
                        
                            Tech
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Prázdne slová
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Stařec, který četl milostné romány
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Electric Wizard
                                     
                                                                             
                                            Dmitrij Dmitrievič Šostakovič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo k nám nechodia tisíce bežkárov ako k susedom do Rakúska
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




