

 Prešla polhodina. Pokojná pohoda plynie pomaly. 
 Prezerá plazivé plamienky plápolajúce pred pozláteným pomníčkom. Pohľadom prevŕta podarené pestré prestieranie. 
 Poblúdil? Premýšľa. Podozrivé. 
 Príde? Pánubohu poručeno... 
 „Poslúžim, pani?", pánko príjemne ponúka pomoc. „Príde priateľ", poznamená pokojne. „Potom. Popolník, poprosím." 
 Posedáva, pozoruje povedomé prichádzajúce páry. Preklenie pochybnosti. 
   
 Prichádza. 
   
 Pri pleci princeznú. Prvoplánovú. Plavovlasú, plachý pohľad, plavné pohyby. Pôsobivé pootvorené pery, poľahky pláva priestorom. Protivná pyšná potvora! Provokujúca pokušiteľka. Panebože... 
 Pobúrene pozerá, popletená, prekvapená, plná poníženia. Parádny preplesk! 
   
 Plakať? Prskať? Preklínať? 
   
 „Pekná", poznamená. Prácne prekrýva podlomenú pózu. 
 „Pochop, prosím. Prepáč." 
   
 Primitív! Panák! Podrazák. Podlý pako... 
 Padá. Prebytočná, potupená. Predýchaj, pokoj. Prehryzni porážku. Prezatiaľ. 
 Pokorenie páli. Posilnená? Poučená? Polámaná. 
 Prebolí? Pánbohvie. 
 Poničil, pokazil, porozbíjal, pošliapal, poranil. 
 Peklo. 
   
 Posratý piatok... 

