

 
Nikto vtedy ešte netušil, že sa z nej raz stane rakúska cisárovná.
Jej teta Žofia, ktorá bola veľmi panovačná, vybrala pre svojho syna inú
nevestu. Jej sestru Helenu, ktorá síce nebola taká krásna ako Alžbeta, no
mravmi sa viac podobala cisárovnej. Priebeh zoznámenia rakúskeho cisára
Františka Jozefa I. a vtedy iba pätnásť ročnej Sissi je každému známy z nádherného
romantického filmu. Stretli sa celkom náhodou a cisár očarený krásou a
prirodzenosťou mladého dievčaťa, sa prvý krát postavil proti matkinej vôli. Nezasnúbil
sa s krásnou Helenou, ale jej mladšou sestrou. 
 
 
 
 
 
Sissi bola vrúcne vítaná všade, kam prichádzala. Keď sa v roku
1853 celá jej rodina vypravila loďou na cestu do Viedne, Alžbetu so sprievodom
vítali rany z diel, hlučné pochody vojenských kapiel, hlahol zvonov a
ohlušujúci jasot. Mladý cisár ju láskyplne privítal nežným objatím a bozkom.
Dojatá snúbenica si vreckovkou utierala slzy šťastia. V záplave bielych ruží,
ktoré zdobili prístav, sa v ružových atlasových šatách s diadémom, s hustými
gaštanovými vlasmi vynímala ako rozprávkovo krásny, takmer nadpozemský zjav.
Alžbeta sa spolu s Františkom Jozefom viezla v pozlátenom koči, obklopenom
vojakmi a sluhami. 
 
 
Svadba sa konala 24. apríla 1854 o siedmej hodine večer vo
viedenskom Kostole sv. Augustína, ožiarenom desať tisícami sviečok. Pred
oltárom sa zdvíhal biely, zlatom pretkávaný baldachýn. A všade nádherné
uniformy gardistov, dôstojníkov, generálov, rázovité odevy zástupcov uhorskej a
poľskej šľachty, orientálnych predstaviteľov v turbanoch. Pestrá paleta farieb
a krojov a ľud omráčený nádherou. Tej sa práve končila bezstarostná mladosť a
začínalo sa ťažké obdobie plné sklamaní. 
 
 
 
 
 
Sissi, doposiaľ zvyknutá na voľnosť, hory a detstvo, sa ocitla v
zlatej klietke, kde ju na každom kroku pozorovali desiatky očí dvorných a
apartmánových dám, a kde ju zviazovala španielska dvorná etiketa. Žofia ju
karhala a poučovala, vyjadrovala sa o nej ako o pojašenom dieťati a netaktne sa
hovorila o všetkých jej nedostatkoch pred cudzími ľuďmi. Denne bola poučovaná,
čo nesmie. Toho, čo mohla, bolo zúfalo málo. Keď si pri slávnostnom obede
stiahla z rúk nepohodlné rukavičky, týždeň sa o ničom inom nehovorilo, len
akého hrozného priestupku sa cisárovná dopustila.
 
 
 
 
 
Prvoradou
úlohou cisárovnej bolo dať monarchii dediča. 5. marca 1855 porodila svoje prvé
dieťa – dievčatko, ktoré dostalo meno po starej matke Žofia. Sisi nedostala
šancu venovať sa dcérkinej výchove, rovnako ako i výchove mladšej Gizely,
narodenej 15. júna 1856, detská izba bola umiestnená v blízkosti apartmánov
arcivojvodkyne Žofie, v opačnom krídle Hofburgu. Keď Sisi vyšliapala nekonečne
dlhé schody, aby svoje deti uvidela, izba bola väčšinou plná dvorných dám.  
Nečudo teda, že s radosťou prijala návrh svojho manžela uniknúť z dusnej
viedenskej atmosféry a vydať sa 2. septembra spolu s ním na cestu po Korutánsku.
O tom, že spoločný výlet ich vzťahu nesmierne prospel, svedčí fakt, že už štyri
dni po návrate do Viedne, cisár napísal matke, ktorej sa obyčajne zvykol vo
všetkom podriadiť, list, v ktorom ju dôrazne žiadal o premiestnenie detí do
centrálnej časti zámku, tzv. Radetzkého izieb. Tak sa Sisi podarilo vybojovať
prvé malé víťazstvo nad nemilovanou svokrou. Radosť ale netrvala dlho, lebo už
v roku 1857 stratila svoju milovanú dcéru Gizelu. O rok neskôr prišiel na svet syn Rudolf, cisárovná mala vtedy 22 rokov. Konečne sa skončili
nekonečné narážky a urážky zo strany Žofie na to, že nie je schopná porodiť
následníka, ktorý jej mal roku 1889 zlomiť srdce. 
 
 
 
 
 
Alžbeta bola naozaj krásna, ale aj posadnutá svojím výzorom. Nenávidela
svoje tehotenstvá, lebo ničili jej „osí“ driek, svoje deti milovala a opúšťala
podľa nálady. Aj keď sa klebetilo o ich vzťahu, je isté, že Alžbetin vzťah k
sexu bol skôr negatívny. František Jozef sa na ňu vraj vždy „vrhal“ s
nesmiernou túžbou, ale jej potreby boli asi iné. Milovala inotaje v poézii a
rozhovoroch, preto bol pre ňu dôležitý vzťah s Ľudovítom II. Bavorským. Šialený
Ľudovít, obdivovateľ krásy a staviteľ rozprávkových zámkov, jej povahe
vyhovoval. Realita ani jedného z nich nezaujímala. Nevideli ju, alebo nechceli
vidieť. 
 
 
Alžbetina choroba, ktorá jej umožňovala utekať z Viedne a zdržovať
sa, kde chcela. Choroba, ktorou trpeli, by sa dnes liečila na psychiatrii.
Pravdepodobne trpela anorexiou. Choroba jej umožnila cisára aj vydierať. Listy
cisárovi sa začínali: „Ďalej si prajem,“ a pokračovali požiadavkami. Cisár ich
plnil, len ju prosil, aby myslela na „úbohé deti“.
 
 
Po synovej smrti sa manželstvo cisárskeho páru rozpadlo. Každý si
naďalej žil svojím životom. Alžbeta často cestovala do cudziny. Jej obľúbeným
miestom bola vila Miramare v Terste. Plavila sa po mori v luxusnej jachte, podnikla
cestu do Afriky, športovala, vo svojom apartmáne vo Viedni, ale aj kdekoľvek
inde mala vždy poruke sadu telocvičného náradia. Často jazdila na koni. Tejto
vášne sa vzdala, až keď jej lekári konské sedlo zakázali pre gynekologické
problémy. Mala rada aj knihy. Jej obľúbeným básnikom bol Heinrich Heine, milovala
Shakespeara, u Franza Liszta sa učila hrať na klavíri. Do Anglicka vraj
chodievala za istým generálom, ktorý bol jej intímnym priateľom. Vo Viedni sa
zdržiavala len cez zimu. Cisár sa v čase rozvratu ich manželstva zoznámil s
mladou herečkou Dvorského divadla Katarínou Schrattovou. Vyvinulo sa z toho
priateľstvo, o ktorého erotickom pozadí napriek cisárovmu veku nie je dôvod
pochybovať. Pozoruhodné je, že Alžbete cisárova dôverná známosť so Schrattovou
vôbec neprekážala. Akoby sa naopak tešila, že Schrattová poskytovala jej mužovi
niektoré potešenia namiesto nej. Dokonca sa s priateľkou svojho muža zoznámila
a sama s ňou nadviazala dôverný priateľský vzťah. 
 
 
Sama Sissi sa veľmi dôverne zblížila s kniežaťom Júliusom Andrášim
(Andrássy), účastníkom povstania, ktorý bol odsúdený na trest smrti a neskôr
omilostený. Alžbeta bola podozrievaná, že je jeho milenkou, dokonca že Andráši
je otcom jej dcéry Valérie. Nie je to nemožné, pri cisárovej citovej absencii
by sa nebolo čomu čudovať. Hovorilo sa tiež, že má ľúbostný pomer s krásnym
dôstojníkom, ktorý sa istý čas často vyskytoval v jej blízkosti. Nikdy však nebola
pri ničom prichytená. 
 
 
 
 
 
V závere svojho života sa Alžbeta stránila ľudí a upadala stále do
väčších depresií. Obdobia, kedy sa prejedala sa striedali s obdobiami, kedy
nejedla vôbec. Veľmi pribrala a nikde sa necítila dobre. Hluk mesta ju dráždil
a ticho lesa ju napĺňalo samotou. Osud sa s ňou znovu raz kruto zahral. 
 
 
V období, keď sa už cítila lepšie, odcestovala do Švajčiarska. 10.
septembra 1898 sa chystala plaviť parníkom. Rýchlym krokom kráčala parkom. Z
lavičky vstal muž s hranatou tvárou, Talian Luigi Lucheni. Špeciálne upraveným
pilníkom ju bodol do hrude. Dvorná dáma, ktorá sprevádzala Alžbetu si myslela,
že jej prišlo zle od srdca a pomohla jej nastúpiť na parník, ktorým sa chystali
plaviť. Až keď cisárovná upadla do bezvedomia a kňažná sa ju pokúšala prebrať,
zistilo sa, že cisárovná krváca. Ťažko ranenú Sissi odviezli do Hotela, kde
lekár konštatoval smrť. 
 
 
Cisára správa zastihla vo Viedni. Napriek všetkým problémom, ktoré
v ich vzťahu boli František Jozef nikdy neprestal milovať svoju manželku. Traduje
sa, že jeho posledné slova boli: Nikto aj tak nevie, ako veľmi som ju miloval. 
 

