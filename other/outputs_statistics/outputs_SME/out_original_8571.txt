
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alexander JÁRAY
                                        &gt;
                Kvantová matematika
                     
                 Štvrtý dialóg s kvantovým chemikom. 

        
            
                                    11.6.2010
            o
            9:21
                        (upravené
                27.2.2014
                o
                10:53)
                        |
            Karma článku:
                1.00
            |
            Prečítané 
            559-krát
                    
         
     
         
             

                 
                    No a toto je moja odpoveď a jeho otázku: Súčasná matematika má jeden zásadný problém, nezaujímajú  ju žiadne zákony fyziky a to ani Zákon zachovania hmoty a energie. Súčasná matematika je matematikou o ničom. V prípade, že do matematických rovníc o ničom dosadíme materiálne hodnoty, v tom momente prejaví sa harlekýnsky charakter matematiky. Problém predmetnej otázky  je zhutnený v rovnosti  1 . 2 = 2. Ale my sa opýtajme, že čo tie čísla prezentujú, čoho sú násobkami. Odpoveď je jednoduchá, tieto čísla sú násobkami ničoho teda hodnoty x0.
                 

                           
   
                      Po dlhšej odmlke mi kvantový fyzik položil nasledovnú otázku:      Vo svojich príspevkoch na blogu ste rozviedli myšlienky tzv. Járayovej  kvantovej matematiky a kauzálnej fyziky, od ktorých by som sa odpichol.  Sedliackym rozumom je pochopiteľné, že jedna krava plus jedna krava  dajú dve kravy, ktoré môžu spolu bučať v kravíne. Materiálne objekty  sú zachované na oboch stranách rovnice.  Zoberme ale, že ideme násobiť tú jednu kravičku dvomi. Na ľavej strane  máme stále jednu kravičku, no pravej strane rovnice sme dostali  kravičky dve. Z ničoho sa objavila hmota.  Nebolo by treba pre takéto prípady zaviesť nejakú fyzikálnu korekciu ?   Diracova relativistická rovnica predpovedá existenciu pozitrónov, čo  je antihmota voči elektrónom. Či by sa nemalo vo Vašej matematike  uvažovať aj o vynáraní sa telies z antipriestoru (spomínaná kravička).  Ďakujem,  M.I.      Podotýkam, že autor tejto otázky je kvantový chemik a  pôsobí na jednej Slovenskej univerzite. Aj jemu matematický zápis násobenia jednej kravy číslom dve robí problém a chce ho riešiť pomocou fyzikálnej korekcie, čiže aparátom kvantovej fyziky a to vynárania sa kravy z antipriestoru, z čiernej hmoty, alebo pomocou pozitrónov, ktoré vznikli iba pomocou aparátu súčasnej matematiky..   Na  tomto pánovi je sympatické to, že na vec ktorej ani on, ako kvantový chemik vyzbrojený okrem iného aj Diracovou relativistickou rovnicou nerozumie, nehanbí sa opýtať. Že on sa za svoju neznalosť objektívnej reality nehanbí.       No a toto je moja odpoveď a jeho otázku:   Súčasná matematika má jeden zásadný problém, nezaujímajú  ju žiadne zákony fyziky a to ani Zákon zachovania hmoty a energie. Súčasná matematika je matematikou o ničom. V prípade, že do matematických rovníc o ničom dosadíme materiálne hodnoty, v tom momente prejaví sa harlekýnsky charakter matematiky.   Problém predmetnéj otázky  je zhutnený v rovnosti  1 . 2 = 2   Ale my sa opýtajme, že čo tie čísla prezentujú, čoho sú násobkami.   Odpoveď je jednoduchá, tieto čísla sú násobkami ničoho teda hodnoty x0.   Potom uvedenú rovnicu môžeme zapísať ako:   (1x0. 2x0 = 2 x0)     =     (1.2 = 2).   A iba v takejto podobe platí výsledok matematikou predpísaného súčinu.   Ale nás asi takýto postup počítania súčinov nula rozmerných veličín nemôže zaujímať a preto za členy x0 dosadíme reálne materiálne rozmery.   Po zmaterializovaní táto rovnica by vyzerala nasledovne:   1m . 2m = 2.m;   (1m.  2m = 2m2)   Čo ale už nie je rovnica ale matematický blud, ktorý človek žijúci v materiálnom priestore nemôže na nič použiť.      Nezmyselnosť rovnice 1. 2 = 2, ktorú každý jeden človek na zemi považuje za svätú pravdu, dokážem inverzným spôsobom a to na výsledku podielu čísel 1: 2 = ?   Podľa matematikov, ale aj podľa kvantových fyzikov a chemikov výsledok tohto podielu je nasledovný:   1) 1:2 = 0   2) 10:2 = 0,5   Ide o nasledovnú úvahu matematikov: Keď v prvom kroku predmetného podielu sa väčšie číslo 2 v menšom čísle nachádza iba nula krát, tak potom v druhom kroku číslo 1 premeníme na číslo desať, 10 (matematici rozdelia číslo  1 na desať desatín 10. 0,1) a potom sa už v tých desiatych desatinách  podľa matematiky, číslo 2 nachádza sa päťkrát.   Takáto slabomyseľná úvaha matematikov platí iba dovtedy, kým sa nebudeme zaujímať o materiálnu podstatu uvedených čísel podielu, kým si nepoložíme otázku ako tento matematický model podielu môžeme použiť v materiálnej praxi.   Keď predmetný podiel zmaterializujeme napríklad hodnotou m1 (meter na jednu) keď predmetný podiel upravíme nasledovne:   1m1: 2 m1 = 0   tak s takým podielom i keď urobíme hoci čo, výsledok 0,5 nikdy nedostaneme, lebo ak hodnotu 1m1 rozdelíme na hoci koľko častí, ani v  rozdelenom metri,  dva metre sa nebudú nachádzať ináč ako nula krát.   Podotýkam, že ako kvantová fyzika, tak aj kvantová chémie stojí na výsledku podielu menšieho čísla 1 väčším číslom 2  o hodnote 0,5.   A tým by som odpoveď na uvedenú otázku považoval za vyčerpanú.      Prípadný diskutér mal by odpovedať na nasledovné otázky:      1. Akým spôsobom sa matematika dopracovala v podieli 1:2 k číslu 5 ?      2. Prečo sa k takému výsledku nemôže dopracovať matematika pri podieli 1m1: 2 m1 ?      3. Ktorý z výsledkov uvedeného podielu je podľa vašich osobných dojmov správny a ktorý mylný ?      4. Prečo sa vyučovanie hodnoty podielu 1:2 = 0,5 nepovažuje za trestný čin podvodu ?      5. Prečo sa učitelia matematiky vyučujúci hodnotu podielu 1:2 = 0,5 nepovažujú za duševne chorých jedincov ?      6. Prečo vám osobne nevadí vyučovanie matematického bludu v tvare 1: 2 = 0,5 ?      7. Môžu sa akceptovať výsledky fyziky a chémie, ktoré akceptujú pravdivosť výsledku podielu:  1:2 = 0,5 ?       A to by bol aj koniec mojej odpovede na otázku kvantového chemika.                

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Overte si, či ste múdrym človekom s IQ 201 bodov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Ženský genetický kód!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Experimentálny dôkaz neplatnosti „Všeobecnej teórie relativity“ pohybu matérie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Veritas vincit (Pravda víťazí.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Čas sem, čas tam, nám je to všetko jedno.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alexander JÁRAY
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alexander JÁRAY
            
         
        jaray.blog.sme.sk (rss)
         
                                     
     
        Pravde žijem, krivdu bijem, verne národ svoj milujem. To jediná moja vina a okrem tej žiadna iná.
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    255
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1326
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kvantová matematika
                        
                     
                                     
                        
                            O zločinoch vedcov
                        
                     
                                     
                        
                            Kde neplatia zákony fyziky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Overte si, či ste múdrym človekom s IQ 201 bodov.
                     
                                                         
                       Ženský genetický kód!
                     
                                                         
                       Veritas vincit (Pravda víťazí.)
                     
                                                         
                       Čas sem, čas tam, nám je to všetko jedno.
                     
                                                         
                       Kvíkanie divých svíň.
                     
                                                         
                       Ako nemohol vzniknúť priestor a hmota.
                     
                                                         
                       Odpoveď na otázku z Facebooku.
                     
                                                         
                       Moja dnešná elktornická pošta (26. 2. 2014).
                     
                                                         
                       Kde bolo tam bolo, bola raz jedna „Nežná revolúcia“.
                     
                                                         
                       Zákon zachovania hmoty.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




