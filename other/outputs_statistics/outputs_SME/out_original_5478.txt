
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Jusko
                                        &gt;
                Školstvo
                     
                 Z ihriska parkovisko, zo športovca feťák 

        
            
                                    29.4.2010
            o
            10:30
                        (upravené
                29.4.2010
                o
                10:49)
                        |
            Karma článku:
                9.66
            |
            Prečítané 
            1189-krát
                    
         
     
         
             

                 
                    Slovenská spoločnosť nepodoporuje šport. Šport u nás nieje fenomén ako v iných krajinách. O šport sa ľudia zaujímajú iba počas majstrovstiev sveta v hokeji, futbale, finále davis cupu a iných podobných akcií. Ale čo táto spoločnosť robí, aby sme mali dobrých športovcov? Aké morálne právo ma niekto kritizovať úspechy či neuspéchy keď pre dobro veci nič nespravil?
                 

                 Za všetko ponúkam jeden príklad zo života. Vyrastal som na sídlisku, kde bolo pred strednou školou basketbalové ihrisko. Hrávali sme tam 7 mesiacov v roku denno-denne. Po skončení strednej školy som odišiel mimo svojho rodiska a s ihriskom som už tak často do styku neprichádzal. Pri mojej poslednej návšteve som sa ale nestačil čudovať - z ihriska sa stalo parkovisko ŠKOLY! Namiesto deciek čo tam hravajú basket sú tam teraz nastavané autá. A nikomu z tej školy to neprišlo divné.   Viem sa na to pozrieť z pohľadu školy - darí sa jej, má veľa študentov na doobedných aj poobedných kurzoch, učí aj platiacich študentov, čo prináša škole peniaze. Pre týchto študentov je treba vytvoriť parkovacie miesta. Najľahšie riešenie - zmena priľahlého ihriska na parkovisko. A práve tu je ten problém. Toto je logika, ktorou škola uvažuje. Neuvažuje o tom, že keď decká nebudú mať čo robiť vo volnom čase, začnú piť a fetovať. Sám som tým prešiel a verte mi, je to tak. Basketbal je pouličný šport, na sídliskách veľmi obľúbený. Namiesto toho aby sme sa snažili tam tie deti za každu cenu dostať, my im to ihrisko berieme.   Noviny a televízie sú plné toho ako je šport finančne poddimenzovaný. Toto ale nieje o štáte, je to o ľuďoch a ich prioritách. Šport na Slovensku momentálne prioritou nieje. A to je veľká chyba nás všetkých. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Jusko 
                                        
                                            Nesystémová finančná podpora cestovnému ruchu na východe Slovenska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Jusko 
                                        
                                            Nová politická strana - strana nevoličov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Jusko 
                                        
                                            Zákon o turizme - ďalší dôkaz o ignorácií cestovného ruchu na Slove
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Jusko
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Jusko
            
         
        marekjusko.blog.sme.sk (rss)
         
                                     
     
        Mám rád dobré víno a užívam si života. Ľudia ako Slota, Rafaj, Fico, Mečiar, Kotleba mi ten život znechucujú. Ale bojujem stým...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1312
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Cestovný ruch
                        
                     
                                     
                        
                            Školstvo
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




