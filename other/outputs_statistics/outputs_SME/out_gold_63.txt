

 
 FOTO – ARCHÍV 
 

 Sovietsky zväz bojoval v Afganistane takmer 10 rokov. Neuspel a s hanbou musel odísť. Svoje vojská začal sťahovať pred 15 rokmi - 15. mája 1988. Začiatok odsunu sovietskych vojsk z Afganistanu nebol poslednou operáciou rusko-afganskej vojny. Afganistan je dodnes pre Rusov čiernou morou. 
 Nejde ani tak o drogy, ktoré cez Rusko prúdia do Európy, či o talibov, ktorí sa v poslednom čase zase aktivizujú. Najväčšie starosti robia Moskve vlastní vojaci, ktorí prešli afganskou vojnou a vrátili sa nakoniec domov. 
 Tu sa opäť dali do boja. Tentoraz nie proti mudžahedínom, ale proti vlastnému, nenávidenému štátu. Vojnoví veteráni z Afganistanu sú jednou z najobávanejších skupín ruského podsvetia. 
 Pre Rusko sa preto afganská vojna neskončila v momente, keď vojaci 40. armády začali baliť svoje torby a otočili pásy tankov smerom na severozápad. Zabudnutá bude až vtedy, keď zomrie posledný účastník bojov. 
 Afganistan zostáva pre Rusov traumou, čiernou škvrnou na vojenskej histórii krajiny. Čiernejšou, než bola okupácia Československa či invázia do Maďarska. Afganistan totiž trval príliš dlho a k úplnej okupácii nikdy nedošlo. 
 Desať rokov tu zúrili boje, v ktorých zahynulo podľa rôznych údajov od 800 000 do 1 500 000 Afgancov a podľa oficiálnych odhadov 15-tisíc ruských vojakov a dôstojníkov. Vojna stála Moskvu 6 miliárd rubľov a dodnes sa hľadajú stovky nezvestných. 
 Rusi sa nikdy nezmierili s porážkou, ktorú v Afganistane utrpeli. Na rozdiel od Američanov nedokázali svoju prehru reflektovať ani v umeleckej forme. Nenakrútili jediný celovečerný film na tému afganská vojna, len náznakom podobný americkým filmom o traumatickej skúsenosti z Vietnamu. Ani jeden film, z ktorého by bolo patrné zúfalstvo nad tým, čo to tí naši vodcovia predviedli. Všetky naopak opisujú ruských vojakov ako hrdinských záchrancov a afganských partizánov, ako bytosti podobné divej zveri. 
 Dodnes sa ruskí historici snažia ospravedlniť vojnu najrôznejšími výmyslami, dodnes je v Rusku mnoho vojenských expertov i politikov, ktorí by najradšej vrátili čas naspäť a ujali sa velenia celej operácie s väčšou rozhodnosťou. I keď z archívov je zrejmé, že už niekoľko mesiacov po vstupe sovietskych vojsk do Afganistanu v Moskve pochopili, že urobili zlé rozhodnutie. 
 Od prvého útoku až po podpísanie ženevských dohôd v apríli 1988, podľa ktorých sa mali sovietski vojaci 15. mája 1988 dať na pochod domov, uplynulo priveľa času. A tak sa výročie začatia odsunu nijako neoslavuje. Radšej. 
 Celá akcia trvala 9 mesiacov do 15. februára 1989. Stále žijúci očití svedkovia na ňu spomínajú skôr s hrôzou v očiach, než ako na koniec vojnového ťaženia. 
 Veliteľ operácie generál Boris Gromov na väčšinu otázok o udalostiach 15 rokov starých odpovedá: „Východ, to je tenký ľad.“ Ako jeden z mála je schopný priznať, že na zúfalej situácii, v ktorej sa dnes Afganistan nachádza, má Moskva leví podiel. 
 Nerád hovorí o podrobnostiach, napríklad tých, ktorých následky je dodnes možné v Afganistane vidieť. V snahe ochrániť vracajúcich sa sovietskych vojakov pred jasajúcimi mudžahedínmi dal Gromov vypáliť a vybombardovať pás ústupovej cesty smerom k hranici Afganistanu s Uzbekistanom. 
 Odchod sovietskej armády a americká vojenská i finančná podpora susedného Pakistanu, kde sa ako nová sila proti Rusom zrodil Taliban, začal ďalšiu tragédiu nástup extrémneho islamu. 
 Rusi sú dnes radi, že sa pod vplyvom nových vojen a nových dramatických udalostí na trápne výročie, akým určite vstup i odchod sovietskych vojsk z Afganistanu v očiach ruských občanov je, zabudlo. 
 Zo spomienok na Kábul dnes v Rusku žijú ešte vyvezené vzácne koberce v bytoch generálov, historické exponáty ako telefónny prístroj Rusmi zavraždeného afganského vodcu Chafizulla Amina, tajné dokumenty ukradnuté zo štátnych afganských archívov a veteráni. Tí, ktorí budú svojimi činmi do konca svojich životov pripomínať, koľko stojí jedno chybné rozhodnutie.	 

