
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boba Baluchová
                                        &gt;
                Kinematografia
                     
                 Šulíkov kľúč k určeniu života (nielen Juráčkovho) za normalizácie 

        
            
                                    19.5.2010
            o
            15:50
                        (upravené
                19.5.2010
                o
                16:17)
                        |
            Karma článku:
                3.51
            |
            Prečítané 
            613-krát
                    
         
     
         
             

                 
                    Napísať rozbor filmu Martina Šulíka o Pavlovi Juráčkovi je veľmi ťažké, až nemožné. Keď súčasný talentovaný režisér natočí film o osobnom až intímnom vnútre a rozorvaných myšlienkových pochodoch talentovaného scenáristu/režiséra predchádzajúcej generácie, filmový kritik/čka by mal vycítiť podstatu výsledku a zvolať len: „bravó!“ No pre tých, ktorí jednu alebo druhú osobnosť, a teda ani ich filmárske zmýšľanie a vplyv na rovesníkov (ako aj vývin česko-slovenského filmu) nepoznajú, treba napísať predsa len viac. Dokument „Kľúč k určovaniu trpaslíkov“ si to zaslúži.
                 

                 
Juráčkove denníkové zápisky dokonalo trefne a smutne dešifrovali situáciu umelca v období normalizácie – odsúdenie za výnimočnosť/protiprúdovosť.Palo Markovič
   Tento text by mohol mať toľko stovák strán, ako knižne vydané denníky azda najdôležitejšej osobnosti šesťdesiatych rokov minulého storočia - predstaviteľa československej Novej vlny - Pavla Juráčka, ktorý zomrel krátko pred pádom režimu, čo mu neumožnil slobodne tvoriť a žiť. Ale aby sa to celé neminulo účinku, budem omnoho stručnejšia.   Najprv sa priznám, že som film „Kľúč k určovaniu trpaslíkov“  (a je jedno, či ho väčšinovo priradíme k hranému či dokumentárnemu) doteraz nevidela. A keby som nevedela vopred, že ho natočil Martin Šulík v roku 2002, podľa atmosféry a všetkých výrazových prostriedkov by som bezpochyby tipovala rok výroby na prelom šesťdesiatych a sedemdesiatych rokov. Tak verná je pomalá čiernobiela kamera Martina Štrbu a zámer režiséra Šulíka - sprítomniť obdobie normalizacie, hustnúcu prepolitizovanú náladu na všetkých frontoch: prostredníctvom rodinných fotografií rodiny Juráčkovcov, archívnych záberov z Barandovského štúdia, záberov na dobové električky v centre Prahy za dňa či smutné svetelné tabule na pražských obchodoch a baroch v noci. Kde bolo potrebné sprítomniť vzťah Pavla Juráčka s dcérou Juditou či manželkou Verou alebo neskoršou partnerkou Hankou - tam nastúpila dokonalá a dôveryhodná rekonštrukcia. V úlohe Pavla Juráčka sa predstavil jeho syn Marek. Pozeráte sa na hru dcéry s otcom na brehu Vltavy, či ozdobovanie vianočného stromčeka, stavanie domčeka z karát a cítite sa plným vedomím v bývalom režime, plnom príkazov a zákazov, nezmyselností a zbytočností. Tam skrátka tvorivý duch tvoriť nemohol, slobodný človek neexistoval, nebol pochopený, dokonca za svoju výnimočnosť/protiprúdovosť bol odsudzovaný. Takže opäť sa priznávam: tak, ako som cítila tvorivú neistotu, nepochopenie, neužitočnosť, riziko predčasného vyhorenia počas sledovania „Kristových rokov" Jura Jakubiska, podobne ma zasiahol aj tento film „Kľúč k určovaniu trpaslíkov" Martina Šulíka. Lenže v tomto dokumente nejde len o osobnú tragédiu jedného umelca (ktorý netúži byť umelcom vo vtedajšej krajine, dokáže tvoriť najviac do tridsaťpäťky). Juráčkove denníkové zápisky dokonale trefne dešifrujú vtedajšiu situáciu: „žijeme karikatúru života"; „vymysleli sme si obraz sveta v sebe, aby sme mu porozumeli a zvykli si naň"; „zle chápeme demokraciu - rozpad spoločnosti zvnútra"...   Snímka má tri tragické roviny, zlúčené a vypovedané cez postavu obetného baránka, ktorého síce rovesníci/kolegovia tajne obdivujú ako frajera, no verejne sa zaň nik nepostaví. Preto po kádrových posudkoch a zamestnaneckých previerkach, odporúčaniach komunistickej strany ako jediný musí odísť zo zamestnania. Prvou tragédiou je rozpad Juráčkovho manželstva (nevera manželky Very a únos ich dcéry Judity do NDR), druhou je zánik filmovej tvorby na Barandove (nástup režimom ovládaných tvorcov na scénu, previerky scenárov aj pracovníkov, a prepúšťanie), treťou je smutný chod Československa ako krajiny - vplyv normalizácie na každodenný život občanov/ok. Rodinný život, pracovný i ten celonárodný máme vďaka podrobným denníkovým záznamom Juráčka z rokov 1964 až 1973 prerozprávaný v tejto emocionálne-nabitej snímke.   Sledujeme Juráčka, ako sa mu striedavo darí aj nedarí písať scenár o Guliverovi. Ako z neho dcéra robí človeka, a pre ňu je ochotný tento scenár aj zničiť. Prežívame spolu s ním dokončenie filmu „Prípad pre začínajúceho kata"- splnený dlhoročný sen. No v zápätí vidíme príchod tankov varšavských vojsk na územie Prahy a stratu dcéry, teda katastrofu celospoločenskú i osobnú. Z hlasu rozprávača, teda z myšlienok stvoriteľa Gulivera, cítime únavu a náznaky pomyslenia na samovraždu. Stiesnenú atmosféru dotvárajú čiernobiele obrazy, ilustrujúce bezútešné monológy hlavnej postavy, a predovšetkým komponovaná klavírna hudba. Tá náladou síce kopíruje občasné radostné chvíle z Juráčkovho života v obraze, no viac vystupuje do popredia práve v smutných obdobiach tejto osobnosti (Vianoce bez dcéry, nútený odchod zo zamestnania, dokončovanie filmu s budhistickou pokorou, mesiace tvorivej krízy atď.).   Dokument sa končí zmienkou o tom, ako dosadený pracovník Barandova - akýsi režimu poplatný Toman nakrútil na príkaz zhora strihový film zo štrnástich konkrétnych „nevyhovujúcich" filmov - o mladých predstaviteľoch československej Novej vlny. Rozprávač tlmočí divákovi/čke ironickú poznámku z Juráčkovho zápisu, že v spomínanom filme je, samozrejme, pre výstrahu citovaný aj jeho film o Guliverovi „Prípad pre začínajúceho kata". Vraj ide o rozšafný - na klapky početný a štátne financie náročný film. Na zjazde KSČ bol film zle ohodnotený, no v kinách mal dlho vysokú návštevnosť, a až neskôr bol pre nesúlad s ideológiou strany zakázaný. Juráček sa zmieňuje úsmevne aj o tom, že natočiť film o skupine tridsaťročných tvorcov - to tu ešte nebolo. Spytuje sa sám seba, či strane raz za túto námahu bude vďačný. Paradoxne, my sme týmto komunistickým pohlavárom vďační za „zmapovanie" dôležitej etapy a generácie československého filmu... Škoda, že sa tohto obratu nedožili aj všetci zmieňovaní protagonisti. Predovšetkým Pavel Juráček, ktorý si svoju satisfakciu a rehabilitáciu už prežiť nestihol.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Neseďte na korytnačkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Dobrý projekt jedna krádež nezastaví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Oslepnúť na celý život pre nevedomosť dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Dojčiť – radšej všade, ako nikdy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boba Baluchová 
                                        
                                            Pytliakmi proti svojej vôli
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boba Baluchová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boba Baluchová
            
         
        bobabaluchova.blog.sme.sk (rss)
         
                        VIP
                             
     
        nekončiaca študentka a pedagogička, novinárka aj filmárka, pracujúca v roku 2013 ako terénna rozvojová pracovníčka v Keni - snažiaca sa hovoriť o témach, čo sa dejú, čo nás zaujímajú i trápia... http://twitter.com/bobinkha
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    36
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestopis
                        
                     
                                     
                        
                            Kinematografia
                        
                     
                                     
                        
                            Reportáže
                        
                     
                                     
                        
                            Lekcie rozvoja
                        
                     
                                     
                        
                            Globálne témy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            O médiách
                                     
                                                                             
                                            Týždeň v nových médiách
                                     
                                                                             
                                            SPW
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




