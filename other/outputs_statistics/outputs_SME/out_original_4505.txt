
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Blažeková
                                        &gt;
                Súkromné
                     
                 Zlato v hrdle. Spievala aj v perinke 

        
            
                                    13.4.2010
            o
            21:14
                        (upravené
                13.4.2010
                o
                21:24)
                        |
            Karma článku:
                3.08
            |
            Prečítané 
            541-krát
                    
         
     
         
             

                 
                    Malé, usmievavé, dobre naladené dievčatko s červenými lícami... Tak presne to je Valika Jenešíkova z Dlhej nad Kysucou. Tretiačka, ktorá na prvý pohľad ničím nevyniká, skrýva vo svojom hrdle zlato. Spievať začala už v perinke. Vždy, keď niečo potrebovala, melodicky si to vypýtala. „Nemala ani tri roky, začala rozprávať a piesne sa už len – tak hrnuli,“ prezrádza na Valiku jej mamina.
                 

                 
Valika JanešíkováMartina Blažeková
     
  O tom, že jej spevácky talent nemá hranice niet pochýb. Mimoriadny nadanie si ako prvá všimla pani vychovávateľka Maďariová z folklórneho súboru Bukovinka, ktorý Valika navštevuje.     Aby sa jej spevácke schopnosti rozvíjali ujala sa  jej pani učiteľka Emília Kobolková. Pod jej vedením nacvičila dve piesne: „Neprš, daždik neprš“ a „Zahrajce hudaci“, s ktorými sa zúčastnila speváckej súťaže Slávik Slovenska, ktorá sa konala tohto roku v júni. Spomedzi 60 tisíc detí sa Valika prebojovala do celoslovenského finále a stala sa tak členkou finálovej 24.      A ako bolo na súťaži? „Každý si vyžreboval číslo. Ja som mala číslo 6. Išli sme po poradí. Jeden po druhom sme vystúpili so svojimi piesňami. Každý povedal niečo o sebe. Pri speve som myslela na pani učiteľku,“ s úsmevom prezrádza svoje skúsenosti malá speváčka. Na súťaži sa Valika stretla s pani Zahradníkovou a s pánom Dvorským, pod ktorého záštitou sa súťaž konala.     Valika sa okrem spevu začala venovať i hre na harmonike. „Takto si bude môcť robiť sprievod k spevu sama. Doposiaľ sme vždy museli niekoho hľadať,“ vysvetľuje prvotnú myšlienku hry na harmonike Valikina mamina.     Prázdniny sú síce v plnom prúde, ale mladá hviezdička si nedovolí zaháľať. Neustále trénuje – spieva ráno, spieva na obed, spieva i večer, ba niekedy sa jej i sníva, že spieva. Koniec koncov tréning je veľmi dôležitý. Veď 1. augusta sa spoločne s heligonkármi zúčastní súťaže v Čiernom pri Čadci a samozrejme, že sa so svojim spevom predstaví i počas Beskydských slávností v Turzovke.     A čo je snom malej speváčky? „Keď budem veľká chcela by som byť pani učiteľka na konzervatóriu. Zamýšľam sa nad tým, že by som učila spev. Preto sa musím naučiť hrať i na klavíri.“    Malej Valike držíme palce a želáme ešte veľa úspechov v speve i v škole.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Keď diabetes (cukrovka) "žerie" zaživa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Dobrá známka sa nerovná dobrá vedomosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Originalny pozdrav k Vianociam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Môj prvý kúpeľný pobyt a hra na hrocha
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Blažeková 
                                        
                                            Uhni kripel!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Blažeková
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Blažeková
            
         
        martinablazekova.blog.sme.sk (rss)
         
                                     
     
        Neviem kto som, čo som, kam kráčam, čí som posol, čaká ma smrť či spása, ale aj tak usmievam sa.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    48
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    513
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            študáci :o)
                        
                     
                                     
                        
                            mňa nevymyslíš, ja som
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď diabetes (cukrovka) "žerie" zaživa
                     
                                                         
                       Dobrá známka sa nerovná dobrá vedomosť
                     
                                                         
                       Môj prvý kúpeľný pobyt a hra na hrocha
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




