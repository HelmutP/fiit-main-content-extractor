
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Sama Mareca príhody a skúsenos
                     
                 Ako Samo vo fabrike na mäso pracoval 

        
            
                                    7.4.2010
            o
            19:41
                        (upravené
                1.6.2010
                o
                13:40)
                        |
            Karma článku:
                21.97
            |
            Prečítané 
            12131-krát
                    
         
     
         
             

                 
                    Keď som sa asi pred piatimi rokmi vybral na leto do Edinburghu, celý čas som sa cítil veľmi odľahčene a nebol to pocit náhodný, lebo hneď tretí deň ma niekto odľahčil o peniaze. A pas. To sa potom človeku hneď ľahšie dýcha, keď nemá čo stratiť. Doslova.
                 

                 Nie je to na škodu, ak chcete vedieť, zakúsiť si takú neriedenú chudobu. Nemať čo na večeru, nemať kde bývať a nemať prácu. Mnohé som vtedy pochopil a schudol asi desať kíl. V polovici júla som oslavoval narodeniny a nejako sme naškrabali na fľašu vodky, lenže potom prišlo ráno a my sme zase nemali peniaze, raňajky ani prácu, len opicu. Nechcel by som si to zopakovať, hoci mám pocit, že aj tak bolo dobre. Niekedy je možno najlepšie, keď je najhoršie. Typický Slovák sa vo mne ozval.   Zásadnou chybou, alebo teda jednou z mnohých zásadných chýb toho leta bolo, že nikto nepadal na prdel predo mnou, mladým slovenským študentom, ktorý si prišiel nájsť robotu. Býval som prevažne kadejako, v emočne najvypätejších časoch aj v byte s ďalšími desiatimi ľuďmi. Spával som na karimatke v kuchyni prikrytý závesom. Neskôr som postúpil na rozkladacie kreslo a núkali mi dokonca aj spacák, vraj mi v ňom bude teplejšie, lenže ja som sa už so svojim závesom natoľko stotožnil, že som sa ho odmietol vzdať.   A pracoval som. Za poldruha mesiaca celé tri dni. Vo fabrike na mäso. Vstával som o piatej ráno a o pol siedmej som už stál nastúpený spolu s Poliakmi, Litovcami a ďalším obvyklým prisťahovaleckým výkvetom. Aj jeden Bask sa tam priplietol, chudák. Práca bola, ako inak, naplňujúca a s fascinujúcimi možnosťami kariérneho postupu. Po roku by som sa možno bol stal aj šoférom vysokozdvižného vozíka, ak by som bol vydržal.   Zatiaľ som sa musel uspokojiť s jednoduchšími činnosťami, menovite hádzaním pomletého mäsa na pás, kde ho potom taká gilotínovitá vec sekala na porcie. Keď ich nasekala dosť, prešlo sa na druhú stranu tej veci a beztvaré kusy, ehm, mäsa, sa takou železnou pečiatkou upravovali do toho tvaru, v akom ho kupujete úhľadne zabalené a pekne nasvietené v nemenovanom britskom supermarkete. Lenže ani svetlá a obal nič nezmenia na tom, že je to stále to isté, ehm, mäso.   Rád som pracoval v mrazničke veľkej asi ako naša aula, hlavne preto, že tam neboli kamery a tým mäsom sa tam teda dalo ohadzovať. Čo som samozrejme nerobil, to robili len Poliaci. Istú dobu som potom bol vegetarián, len tak, pre istotu. Tiež sme napichovali vecí na paličky a ukladali ich na plastové misky. Predávalo sa to ako ražniči, hoci to vtedy pripomínalo čokoľvek iné. A zvyšné paličky sa potom občas zvykli hodiť do toho stroja na mäso. Vždy sa tak pekne pomleli.   Pre ktorý supermarket sme to mäso chystali vám nepoviem, nech v Sainsbury´s nakupujete aj naďalej. Ja ostatne nepochybujem, že sa to v tej fabrike už všetko zmenilo, všetko je na poriadku, všetky safety aj iné measures sa dodržujú. Občas mám pocit, že Británia stojí práve na safety measures a ak by neexistovali, všetci by sa tam nožíkmi dosekali. Čo mimochodom robia aj tak. Inak som chcel pôvodne napísať vtipný článok.   Po troch dňoch, keď som už konečne začal vychytávať drobné finesy ničnerobenia, ma prepustili. Bol som tam vtedy cez agentúru. Nadával som, veď tri dni práce ma nenakŕmili a ani nepoplatili moje dlhy, ale úprimne, veľkú kariéru som si tam predstaviť nevedel. Niektorí si vedeli a možno tam pracujú doteraz.   O pár dní neskôr sa mi v živote začalo prudko dariť. Našiel som si aspoň trochu poriadnu prácu a aj z karimatky som sa presunul na to rozkladacie kreslo. Vidíte, keď človek nemá nič, aj málo vie byť veľa. To je jedna z vecí, ktoré som vtedy pochopil. Aj Maorka z Nového Zélandu sa ma vytrvalo snažila zbaliť, ale až potom, ako nezbalila môjho kamoša a ja som síce peniaze nemal, ale hrdosť som si zachoval, takže z toho nič nebolo. Teda párkrát som sa za ňou zastavil v práci. Pracovala v reštaurácii a vždy mi dala najesť.   A viete, čo je najzvláštnejšie? Hoci to bolo veľmi ťažké obdobie, spomínam naň skoro s láskou. Nikdy potom som sa už necítil taký pripravený čeliť každému ďalšiemu problému, ktorý si život pre mňa pripravil. Asi som prišiel na to, že sa o seba dokážem vždy postarať a na takéto veci človek prichádza rád. Chodil som po svete s úsmevom, dvadsiatimi pencami vo vrecku a bolo mi dobre. Niekedy je proste najlepšie, keď je najhoršie.   Yozeefkovi, ktorý to mäso pečiatkoval so mnou. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




