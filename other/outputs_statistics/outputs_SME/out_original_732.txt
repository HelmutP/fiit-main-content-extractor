
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Olga Pietruchová
                                        &gt;
                Cestopisy
                     
                 Happy Birthday, NASA! Washington DC V. 

        
            
                                    28.7.2008
            o
            10:10
                        |
            Karma článku:
                10.89
            |
            Prečítané 
            6648-krát
                    
         
     
         
             

                 
                    "Je súčasťou ľudského charakteru ísť dopredu, vidieť, porozumieť. Vesmírny výskum nie je jednou z možností, je to nutná potreba". Michael Collins, astronaut, člen posádky Apolla 11 spolu s Amstrongom a Aldrinom, ktorá ako prvá pristala na Mesiaci.
                 

                  1. októbra 1958 začala fungovať agentúra National Aeronautics and Space Administration, známa pod skratkou NASA. Jej vytvorenie schválil prezident Einsenhower podpisom 29.júla 1958. NASA teda oslavuje svoju päťdesiatku, čo je dobrý vek na prípitok i malú rekapituláciu. Hoci jej história  nebola len plná úspechov, dôvodov na oslavu je viac ako dosť.     Oslavy a festival      Na tradičnom festivale na Washingtonskom móle mala pri príležitosti narodenín NASA výstavu pod holým nebom, kde predvádzala nielen úspechy svojej histórie, ale i víziu do budúcnosti.           Festival bol zaujímavý hlavne pre deti, ktoré mali možnosť zúčastniť sa malého kozmického výskumu či potľapkať si odev kozmonauta. NASA predstavila svoje úspešné projekty, ale i budúce plány ako rakety Ares a vesmírny ďalekohľad Jamesa Webba. K programu patrila aj diskusia s tromi americkými astronautmi (mená ktorých som si bohužiaľ nezapamätala).          National Air and Space Museum     Samozrejme, oslavuje sa aj v múzeu vesmírnych letov. Hneď vo vestibule múzea víta návštevníkov výstava, pripomínajúca 50. výročie vesmírneho veku. Skutočné výročie bolo vlastne 4. októbra 2007, teda 50 rokov od štartu vesmírnej družice Sputnik 1 v kazašskom Bajkonure (na obrázku model vpravo vedľa plagátu, vľavo americká sonda Explorer). Sputnik s priemerom 58 centimetrov a hmotnosťou 84 kilogramov začal novú éru vedeckých výdobytkov ako sú satelitná televízna a komunikačná technika.            Vesmírne preteky     Vypustenie Sputnika znamenalo aj začiatok vesmírnych pretekov medzi ZSSR a USA. Vtedajší sovietsky líder Nikita Chruščov si uvedomil propagandistickú hodnotu vesmírnych letov a Rusi 3. novembra 1957 vypustili Sputnik 2, čím oslávili 40. výročie októbrovej revolúcie. Satelit už vážil 500 kilogramov a na jeho palube bola "posádka" - pes menom Lajka. Až niekoľko mesiacov po nich, začiatkom roku 1958, sa Američanom konečne podarilo dostať na obežnú dráhu satelit Explorer 1. 1     Na fotografii modely rôznych rakiet a striel, v pozadí vľavo vesmírne laboratórium Skylab.            Lety na Mesiac     V múzeu je samostatná expozícia venovaná letom Apollo na mesiac (dole na obrázku štart Apollo 11). Niet sa čo čudovať, je to jediné prvenstvo, ktoré sa Američanom v kozmických pretekoch počas studenej vojny podarilo reálne dosiahnuť.     Medzi rokmi 1969 a 1972 chodilo po povrchu mesiaca 12 Američanov. Pre USA to bol výrazný úspech potom, ako im Sovietsky zväz "vyfúkol" prvenstvo vo všetkých iných misiách: vypustenie prvej družice Sputnik 1 (4.10.1957), let prvého zvieraťa (Sputnik 2 s Lajkou, 3.11.1957) a nakoniec let prvého človeka Jurija Gagarina (1961). Ako odpoveď na to v máji 1961 vytýčil prezident J.F.Kennedy jasný cieľ: prvý človek na Mesiaci musí byť Američan!     Tieto vesmírne preteky (Space Race) sú zaujímavým spôsobom zdokumentované vo filme, ktorý sa premieta v múzeu. Milé je, že si v ňom svetová veľmoc tak trochu robí srandu zo seba - čo v USA nie je zďaleka prirodzené a bežné. Zaujímavé články o pretekoch a príčinách počiatočného amerického fiaska nájdete tu.          Expozícia múzea sa samozrejme podrobne venuje letu Apollo 11, ktorý bol piatym pilotovaným letom programu a prvým, ktorého posádka pristála na Mesiaci. Podľa odhadu viac ako 600 miliónov televíznych divákov na celom svete sledovalo tento historický let. Milo ma prekvapilo, že na slovenskej Wikipédii je podrobne zdokumentovaný.     Pripomeňme si túto historickú udalosť pár obrázkami. Nasledujúce fotografie dokumentujúce úspešnú misiu Apolla 11 sú z archívu sprístupneného NASA.2 Na obrázkoch Aldrin s Amstrongom na povrchu Mesiaca a fascinujúci pohľad na modrú planétu Zem.                         V múzeu sú vystavené aj prepravné moduly - identické modely alebo originály - používané pri letoch Apolla. Tento slúžil na prepravu kozmonautov k vesmírnemu laboratóriu Skylab.           Ak pohľady sa asi naskytovali z tohto ležadla?          Detail jedného z piatich motorov F-1 používaných v raketách Apollo. Pre predstavu skutočnej veľkosti: dĺžka 5,6 m, maximálny priemer 3,7 m.          Jeden z 12 mesačných modulov, postavených pre projekt Apollo. Je identický s tým, ktorý bol nakoniec na Mesiaci skutočne použitý.          Osobne ma trochu zarazila kvalita prevedenia - omotaný fóliami, akoby pozliepaný, pripadal mi ako vyrobený na kolene. Nakoniec, každý, kto pracoval napr. ako ja v laboratóriu s americkými a nemeckými prístrojmi a mal to šťastie ako ja nahliadnuť do ich vnútra, vie porovnať kvalitu spracovania. Made in USA nie je práve synonymom pre precíznosť prevedenia (platí to aj o autách).          Modul Apolla 11, ktorý cestovala posádka Neil Amstrong, Michael Collins a Edwin Aldrin na Mesiac a späť.          Na paneli dokumentujúcom všetky vesmírne lety má svoje miesto aj Sojuz 28 s prvou medzinárodnou posádkou v histórii. Aj keď je jasné, že sa jednalo o politické gesto, Vladimír Remek z Československa zostáva prvým kozmonautom mimo Američanov a občanov ZSSR, ktorý letel do vesmíru.     Vo vestibule múzea je vystavený model Apollo-Sojuz, na ktorom sa skúšalo pripojenie oboch systémov. Táto misia mala aj vysokú symbolickú hodnotu, pretože znamenala definitívne ukončenie vesmírnej studenej vojny. Dnes je už spolupráca oboch vesmírnych veľmocí bežná a takmer neodmysliteľná. Samozrejme, naskytá sa otázka, nakoľko studená vojna a vesmírne preteky posunuli dopredu celý výskum...alebo by spolupráca od počiatku bola priniesla viac? To sa už asi nedozvieme.          Ako chodia kozmonauti na záchod? To je vraj najčastejšia otázka detí (dospelí sa ju väčšinou zdráhajú vysloviť). V múzeu je vystavený aj "záchodový vesmírny modul", ruský vynález.           Raketoplány - space shuttle - Columbia, Challenger, Atlantis, Discovery a Endeavour boli úspešným modelom pre vesmírne lety. Pre úplnosť spomeniem ešte Enterprise vybudovaný na testovacie účely. Od predstavenia Columbie v roku 1981 vyviezli viac ako 200 členov posádky a 1,36 miliónov kg materiálu (údaj z panela v múzeu, dnes už možno prekonaný).  Space Transportation System (STS), ako sa nazýva program raketoplánov, znamenal novú éru vesmírnych dopravných prostriedkov, ktoré boli znovu použiteľné.      Dnes už dosluhujú a do roku 2010 majú byť všetky vyradené z prevádzky. Dole model Discovery: skladá sa z externej palivovej nádrže, dvoch raketových pohonov a samotného raketoplánu - orbitra.          Samozrejme, nechýba ani spomienka na nešťastia a ich obete. Prvou haváriou bol požiar na palube Apollo 1, ktorý znamenal smrť troch kozmonautov. Posádka raketoplánu Challengeru zahynula v explózii 73 sekúnd po štarte 28. januára 1986. Podobnou katastrofou bolo stroskotanie Columbie krátko pred návratom na Zem 1.2.2003. V oboch prípadoch sa jednalo o sedemčlenné posádky vrátane dvoch žien.     Štart úspešnejšieho raketoplánu Atlantis z web stránky NASA:          Misia na Mars     Úspešný program vesmírnych letov je momentálne na vážkach. Raketoplány dosluhujú a podľa vízie G.W. Busha je budúcnosť amerického vesmírneho programu v misii na Mars. Samozrejme, mnohí to berú ako typický politický populizmus...najmä pri škrtení prostriedkov, ktoré idú namiesto vedy na zbrojenie.     Je ale pravda, že Mars bol vždy stredobodom ľudskej zvedavosti a výskum na ňom úspešne pokračuje. Na obrázku dole je sonda Viking. Kým Viking 1 a 2 pristáli na Marse pred viac ako 30 rokmi, vystavený model bol používaný na simuláciu ich činnosti a testovanie zaťaženia.          Od pristátia Vikingov postúpil výskum Marsu už hodný kus. Aj dnes vysiela z Marsu sonda Phonix - viac informácií nájdete na stránke NASA, kde je aj galéria fotografií z Marsu. Jedna z fotografií zobrazujúca kráter na povrchu červenej planéty.          Kus kameňa z Marsu, vystavený v múzeu. Zvláštny pocit dotýkať sa niečoho, čo vzniklo tak ďaleko od nás...           Sonda Pioneer bola prvá, ktorá letela "za obzor". V roku 1973 preletela popri Jupiteri a zo vzdialenosti 130 tisíc km poslala množstvo obrázkov. V roku 1983 prekonala hranicu Slnečnej sústavy. Pre prípad, že sa dostane do "rúk" mimozemskej civilizácie, sú na nej uložené informácie planéte Zem a ľuďoch.          Pohľad na vesmírne diaľavy     Úspešným projektom NASA je Hubblov vesmírny ďalekohľad (HST) na obežnej dráhe okolo Zeme. V roku 1990 bol vynesený raketoplánom Discovery. Od svojho vypustenia sa stal jedným z najdôležitejších ďalekohľadov v dejinách astronómie. HST má hmotnosť 12 ton a obieha vo výške 600 km nad zemským povrchom. Dole na obrázku jeho model.          Prínos HST bol tak veľký, že v astronómii možno hovoriť o ére pred Hubblovým teleskopom a ére HST. Pred jeho vypustením sme iba odhadovali vzdialenosť iných galaxií, otázky ako rýchlo sa vesmír zväčšuje a na ako dlho, vyvolávali veľké polemické debaty. Dnes na mnohé otázky vďaka HST poznáme odpovede. (Model HST vzadu za Skylab)          Hoci to pred nedávnom vyzeralo, že teleskop končí, nakoniec sa peniaze na jeho opravu našli. Práve sa pripravuje štvrtá opravná misia Servicing Mission 4. Má odštartovať v októbri 2008 pod vedením Scotta D.Altmana. Misia má predĺžiť životnosť teleskopu aspoň o 5 rokov a o.i. nainštalovať nový spektrograf, ktorý desaťnásobne zvýši citlivosť HST. Nový Space Telescope Imaging Spectrograph (STIS) rozoznáva svetlo z oblasti ultra-fialového, viditeľného a infra-červeného spektra a používa sa napr. na skúmanie čiernych dier a kvazarov. Ostáva len dúfať, že vylepšený Hubblov teleskop nás bude zásobovať ďalšími fascinujúcimi obrázkami (ako sú tie dole) a dávať odpovede na doteraz nezodpovedané otázky.           Zaujímavý článok o HST nájdete tu. Dolu je pár fascinujúcich obrázkov, urobených Hubblovým teleskopom z hmloviny Nebula a zverejnených na stránke venovanej HST. 3                               Nasledovníkom Hubblovho teleskopu by mal byť vesmírny ďalekohľad Jamesa Webba, ktorý nemá obiehať po orbite, ale má byť umiestnený vo vzdialenosti 1,5 milióna km od Zeme smerom od Slnka (pre porovnanie, vzdialenosť Zeme od Slnka je 150 miliónov km).  Jeho predpokladaný termín vypustenia je rok 2012.     Model JWST vystavený na móle počas festivalu.          Ako ďalej? Budúcnosť vesmírnych letov     Na obrázku sú modely dvoch budúcich rakiet Ares I. a Ares V., ktoré plánuje NASA nasadiť ako náhradu za STS program. Ďalší astronauti by na nich mali letieť do kozmu v roku 2014 a na Mesiac v roku 2020.          Doplnkom rakiet Ares bude dopravný modul Orion pre lety na Mesiac a orbitálnu stanicu, ktorý má byť využiteľný aj na lety hlbšie do Slnečnej sústavy. Jeho štart je predpokladaný na rok 2015.          Vesmírny výskum a lety sa stali bežnou súčasťou života ľudstva. Na orbitálnej stanicu sú takmer neustále ľudia, sondy vysielajú obrázky z blízkych i vzdialených končín vesmíru. Začína sa hovoriť o komerčných letoch na Mesiac....uvidíme. V každom prípade bolo tých predchádzajúcich 50 rokov fascinujúcich a hodných pripomenutia nielen medzi odbornou verejnosťou, ale i laikmi ako je väčšina z nás....čo bolo aj cieľom tohto článku.      Zdroje:   Fotografie z festivalu a múzea - autorka   Doplnené fotografiami z archívu NASA, www.nasa.gov, ktoré nepodliehajú copyrightu         1 Podľa článku http://magazin.atlas.sk/technologie/technika/132657/sputnik-1-oslavuje-50-vyrocie-(video)     2 Fotografie z pristátia na mesiaci: http://www.apolloarchive.com/apollo_gallery.html      3 Fotografie zo stránky http://www.hubblesite.com/   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová ideológia? Nie, iba spravodlivosť a fair-play pre naše dcéry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová rovnosť ako kultúra života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Odpoveď na otvorený list Gabriele Kuby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Ženskosť a mužskosť alebo komu slúžia stereotypy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Od Harryho Pottera ku gender ideológii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Olga Pietruchová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Olga Pietruchová
            
         
        pietruchova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Viac mojich článkov nájdete na www.olga.sk


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    180
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7252
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Práva žien a rodová ne/rovnosť
                        
                     
                                     
                        
                            Ateizmus a veda
                        
                     
                                     
                        
                            Politika a spoločnosť
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Fejtóny:-)
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Sexualita, reprodukcia a práva
                        
                     
                                     
                        
                            The God Delusion
                        
                     
                                     
                        
                            Zelené témy
                        
                     
                                     
                        
                            Cirkev a náboženstvo
                        
                     
                                     
                        
                            O čom sa ne/hovorí...
                        
                     
                                     
                        
                            Jej príbeh
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ženy v politike, politika pre ženy
                                     
                                                                             
                                            Môj rozhovor v Sme Víkend
                                     
                                                                             
                                            Na výsluchu: Hierarchia katolíckej cirkvi mi leží v žalúdku
                                     
                                                                             
                                            Môj rozhovor na www.sme.sk
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Steven Pinker: The Blank Slate
                                     
                                                                             
                                            Jared Diamond: Collapse
                                     
                                                                             
                                            Martin Uhlír: Jak sme se stali lidmi
                                     
                                                                             
                                            Lynn Abrams: Zrození moderní ženy
                                     
                                                                             
                                            Daniel Dennett: Breaking the Spell
                                     
                                                                             
                                            Richard Dawkins: God Delusion
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moja osobná stránka
                                     
                                                                             
                                            Zošity humanistov
                                     
                                                                             
                                            Spoločnosť pre plánované rodičovstvo
                                     
                                                                             
                                            Edge -  The Third Culture
                                     
                                                                             
                                            JeToTak
                                     
                                                                             
                                            Changenet
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




