

 Vravel si  
  
 že planéta má svoje kúzlo 
  
 a ďalekohľad ti neumožní 
  
 uzrieť moju tvár 
   
 svoje pocity  
  
 si schoval pod kabát 
  
 a symfónia modrých očí 
  
 sa mi do srdca zavŕtala 
   
 cítim 
  
 že tam v diaľke 
  
 naša láska je 
  
 ale bojím sa ako ty 
   
 bojím sa  
  
 vyznať lásku do prázdnoty 
   
 chýba nám kúzlo včerajška 
  
 zatiaľ čo Ja 
  
 som na vrchole blaha 
  
 Ty vzdycháš 
  
 a Moliéra cituješ 
   
 - 
 „Bláznivá túžba  
  
 má k múdrosti ďaleko 
  
 a tvoj lásky plný cit  
  
 si ma úplne podmanil“ 
  
  

