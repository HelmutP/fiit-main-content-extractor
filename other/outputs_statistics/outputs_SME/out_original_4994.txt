
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                Názory
                     
                 Počkám si na inšpiráciu 

        
            
                                    21.4.2010
            o
            18:32
                        (upravené
                21.4.2010
                o
                19:51)
                        |
            Karma článku:
                6.75
            |
            Prečítané 
            1084-krát
                    
         
     
         
             

                 
                    Aj dnes mám chuť napísať blog, áno som asi závislý na ich písaní. Možno je to kvôli tomu, že ich píšem ani nie pol roka, a ak nenapíšem jeden aspoň každý druhý deň, už ma ovládne nepokoj. Koľkokrát, však ani nie je o čom, ale ako asi každý závislý bloger si tému nájdem, aj napriek tomu, že mi hrozí pranierovanie, že načo tomu vôbec venovať čas, keď nie je o čom. No ale, keď musím, tak musím. Je to neovládateľné.
                 

                 Zatiaľ odolávam, ale keď sledujem pribúdajúce nové blogy, schyľuje sa k môjmu blogerskému abstinenčnému príznaku. Prichádza znova známy nepokoj, v rýchlosti spomínam na dnešné zážitky, na myšlienky, ktoré sa mi od rána prehnali hlavou. Pýtam sa sám seba: „ Tak, o čom dáš dnes?". Odpovedám : „ Neviem, počkám si".   Aha toto by bolo dobré, sledujúc diskusiu o sexuálnych škandáloch v rímskokatolíckej cirkvi. Volím čistý papier v textovom editore a už mám pripravených pár viet o zdravej sexualite , teda o potrebe ľudskej sexuality, podfarbené  aj  spomienkami na pár pravoslávnych a gréckokatolíckych, ešte nevysvätených kňazov, ktorí mali možnosť výberu medzi celibátom a ženbou. Všetci boli rozhodnutí pre manželstvo a pre deti. Mal som s nimi tú česť na vojenčine. Neviem, či sú dnes dobrými kňazmi, ale udivovali ma ich rozhovory o ženách  a o ich pripravenosti na manželstvo. O nežnom pohlaví rozprávali tak, že aj ja už ženáč, som od údivu mlčal a počúval ich milé pohľady na ženské pokolenie. Bolo veľmi sympatické, ako slušnými slovami ospevovali dievčenskú a ženskú krásu. Spievali chválospevy na jej nositeľky, ktoré sa prechádzali  po chodníkoch za plotom kasární. Medzi vojakmi základnej služby to bolo naozaj nezvyčajné, pretože ostatní obyvatelia kasární po dlhšej sexuálnej abstinencii , používali predsa len iný opis daných skutočností. No potom volím radšej krížik v pravom hornom rohu textového editora a na otázku, či uložiť, odpovedám nie. Nie, naozaj nechcem ísť do diskusného ringu so všetkými tými, ktorí nevidia chyby v nimi uznávanej inštitúcii. Už som to raz urobil, ešte niekde na začiatku môjho krátkeho blogovania a odniesol som si za svoj otvorený názor a malú verejnú spoveď o stave mojej viery, len kritiku dokonca aj od kňazov tiež blogerov, od ktorých som to skutočne nečakal.   Ale dlho nevydržím a pýtam si nový list. Tak o čom teda dám, skúsim nejaký politický? Politickou realitou motivované blogy majú úspech, to ľudia čítajú. Mydlím si ruky a chystám sa na prvé údery do klávesnice. Len čo už vymyslíš v dnešnej dobe politického marazmu, komentár na to, čo už komentovali iní a bude to opäť len nejaký paškvil, snažiaci sa podobať na politickú analýzu alebo glosu. Inšpirácia neprichádza a ja kŕčovito sťahujem späť svoje pazúry  zamerané na klávesnicu počítača. No veď o čom mám písať, na žiadnej tlačovke som nebol, nesledoval som v poslednom čase žiadnu politickú reláciu, politikov osobne nepoznám, netykám si s nimi a čo už takého odhalíš v slovenskej politike, keď aj profesionálni novinári sa o veľa veciach len domnievajú. Všetko je len o dohadoch. Veď nedokázateľné je aj to, že firmy blízke politickým predstaviteľom vládnych strán sú zaangažované v zákazkách štátu. Správy o rozkrádaní verejných financií sú vždy len v štádiu pravdepodobnosti, ktorá len zriedka hraničí s istotou. Prieskumy verejnej mienky venované najbližším voľbám ma nezaujímajú, považujem ich za neobjektívne, to už viac verím športovému stávkovaniu. A to, že sa premiér obáva pravice je už dávno známa vec, aj keď slovenská pravica je nebezpečná  iba  ak naplní slová kniežaťa Svätopluka, alebo ak si osvojí heslo EÚ „Zjednotení v rozmanitosti" . Ináč povedané je nebezpečná iba ako zlepenec.   Tak o čom dám? O poľskej leteckej katastrofe a islandskej sopke, by bolo už naozaj ako s krížikom po pohrebe, či ako by sa dal doslovne preložiť, od bratov Čechov požičaný okrídlený výraz.   Dám nejaký zo života, napríklad o tom, ako som bol pre dcérku v škôlke o trocha skôr, ako zvyčajne, s úmyslom urobiť jej radosť. A ona sa mi odvďačila plačom, pretože ju pani učiteľka zobudila z poobedného spánku skôr ako ostatné deti.  Aký je záver? Deťom a ženám nikdy nevyhovieš. To je blbosť, nie som žiadny macho. Mažem.   Nie, dám radšej  o nešťastnej láske, s ktorej prejavmi som sa dnes stretol pri tej spomínanej ceste zo škôlky. Kráčajúc a držiac dcérku za rúčku, sa oproti nám za bieleho dňa tackal mladý muž, pristavil sa a plakal. Podal mi trocha, asi po páde zranenú ruku a so slzami v očiach mi oznámil, že ho nechala frajerka. Z jeho dosť  neartikulovaného prednesu som sa  dozvedel, že vzťah trval dva roky a on sa musel po včerajšom rozchode opiť a zaliať tak žiaľ. Opýtal som sa ho na jeho vek a on bľabotal niečo  o štvrťstoročí, tak som mu odpovedal, že ešte má pred sebou dlhú cestu, že stretne ešte veľa lások, že srdce sa teraz zahojí a ešte schytá niekoľko ďalších jazvičiek. Ale on si húdol len svoje, tak som sa s ním zdvorilo a trocha aj so strachu (bolo so mnou malé dieťa) rozlúčil. Veď, čo ja môžem kázať niekomu o láske, na to nie som oprávnený ( aj keď môžem povedať, že mám lásky plný byt, nesťažujem sa). A tak radšej zatlačím na klávesu delete. Inšpirácie sú týmto vyčerpané. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4080
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




