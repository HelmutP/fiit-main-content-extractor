

 Začať môžeme tým, že podľa posledných meraní globálnych teplôt NASA aj NOAA je obdobie január-apríl 2010 zatiaľ najteplejším v histórii inštrumentálnych meraní teplôt – tj. za posledných 131 rokov (od r. 1879): 
   
   
   
 Graf odchýlok teplôt od priemeru v rokoch 1951-1980, podľa agentúry NASA za obdobie január-apríl 2010.  Všimnite si, že napríklad v strednej Európe je odchylka teploty blízka nule, teda u nás bolo približne rovnako teplo ako v rokoch 1951-1980. Zdroj: NASA 
   
  
   
 Teplotné ochchylky za rovnaké obdobie podľa analýz NOAA. Odchýlky sa v tomto prípade vzťahujú k priemeru za obdobie 1961-1990, čo samozrejme neovplyvňuje absolútne, ale iba relatívne zmeny v teplote. Podľa NOAA je obdobie január-apríl o 0,01 °C teplejšie, ako bolo v doteraz rekordnom roku 2002. Zdroj: NOAA  
   
 Zároveň aj prudko klesá objem arktického ľadu, určený na základe modelu PIOMAS (Pan-Arctic Ice Ocean Modeling and Assimilation System). Pokles objemu ľadu je presnejší indikátor topenia ako plocha, hoci aj tá sa rýchlo zmenšuje: 
   
   
   
 Graf odchýlok objemu arktického ľadu. Podľa týchto údajov ubúda arktický ľad rýchlosťou 3 400 km3 za desaťročie. Všimnite si, že v grafe je možné vidieť 14-ročný cyklus (lokálne minimá boli dosiahnuté v rokoch 1982, 1996, a 2010), ktorý pravdepodobne súvisí s prirodzenými zmenami podnebia (napr. tzv. Arktická oscilácia a ďalšie). Zdroj: Polar Science Center  
   
 Zároveň narastá množstvo tepla absorbovaného oceánmi (ocean heat content), najmä jeho hornými vrstvami. V rokoch 1993-2008 oceány pohltili 80 až 90 % z celkového nadbytočného tepla. Štúdia bola publikovaná v časopise Nature (Lyman a kol., 2010): 
   
  
   
 Graf upravený podľa štúdie Lymana a kol. Oceány sú oveľa lepším a spoľahlivejším indikátorom tepelnej nerovnováhy Zeme, pretože až 80-90 % tepla sa dostane sem. Oceány sú tiež zdrojom klimatickej "zotrvačnosti", to znamená, že ak by sme aj znížili emisie skleníkových plynov na nulu, hladiny oceánov až budú narastať niekoľko storočí, a teplota atmosféry sa budu zvyšovať ešte niekoľko desaťročí (cca. 30 rokov). Červená krivka zobrazuje výbuchy veľkých sopiek, ktoré dočasne znížili množstvo tepla absorbovaného oceánmi. Zdroj: Desdemona Despair, pozri aj RealClimate   
   
 A topenie Grónska je možné pozorovať nielen v negatívnej bilancii množstva ľadu, ale aj v zrýchľujúcom sa „zdvíhaní“ jeho pevniny, na niektorých miestach až rýchlosťou 2,5 cm/rok. Štúdia bola publikovaná v časopise Nature Geoscience (Jiang a kol., 2010).  
   
   
  
 Satelitný snímok Západného Grónska senzoru MODIS (NASA). Tmavšie oblasti sú miesta, kde sa ľad topí a zároveň tu dochádza k zdvíhaniu pevniny. Šípka ukazuje miesto v okolí jedného z najväčších a najrýchlejších odtokových ľadovcov - ľadovca Jacobshavn, ktorý sa takisto rýchlo stenčuje a významne prispieva k nárastu hladín svetových oceánov. Zdroj: ScienceDaily 
   
   
 Grónsko tak bude jedna z mála pevnín, ktoré nebudú ohrozené nárastom hladiny oceánov. Dá sa ale pochybovať o tom, či Grónsko dokáže uživiť aspoň zlomok dnešnej svetovej populácie.  
   
 Takže ak vám niekto bude tvrdiť, že sa neotepľuje, alebo že sa otepľovanie zastavilo, skúste sa nad tým trochu zamyslieť. Ak vám bude tvrdiť, že sa otepľuje, ale CO2 a ostatné skleníkové plyny s tým nemajú takmer nič spoločné, budete sa musieť zamyslieť ešte o trochu viac.  
   

