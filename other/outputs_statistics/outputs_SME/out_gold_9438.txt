

 S triumfálnym výrazom v tvári a s istotou v hlase pri tom vyhlási to  isté, čo ako pokazený verklík omielal dookola už od prvej tlačovky po  voľbách. Ukončí sa tým ďalší nepotrebný Gašparogýč. Gýč, ktorý si jeho  hlavní aktéri mohli odpustiť. Mohli i nemuseli. Voliči populizmu, nacionalizmu a oportunizmu s arómou nostalgie socializmu im to dnes radi odpustia. No a ďalej už očividne rozširovať svoje rady tak či onak nemajú kam. 
 Nuž tak... 
 Načim si nám ponaučenie pre príduvšie štyri roky vziať prichodí. Vari najtrefnejšie ho vyjadril vzácne dobrý diskutér bodliak: "Aj s malým Gašparkom sa dá robiť veľké divadlo." 

