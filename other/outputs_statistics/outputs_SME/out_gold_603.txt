

 
Na rokovanie parlamentu sa zakrátko dostane novela interrupčného zákona, ktorá vyplynula z rohodnutia Ústavného súdu SR. Tá má zaviesť lehotu pre umelé prerušenie tehotenstva v prípade genetického poškodenia plodu - 24 týždňov - do zákona.  Keby táto novela neprešla, príslušný paragraf vyhlášky prestane o pár týždňov platiť a v praxi by to znamenali, že výkon UPT z genetických dôvodov by nebol časovo regulovaný. Akékoľvek protesty proti tejto novele sú teda kontraproduktívne a gólom do vlastnej bránky. Našim teoretickým odborníkom a odborníčkam to však viditeľne nevadí. Hlavne, že sa o nich zase bude v médiách hovoriť a budú môcť ukázať, že práve oni sú lepší a morálnejší ako zvyšok spoločnosti. Čím tvrdšie argumenty predhodia, tým sa o nich bude samozrejme hovoriť viac. Krvavé billboardy asi nebudú, preto treba obúchať protivníkom o hlavu aspoň eugeniku a Hitlera, veď médiá krváky milujú. 
 
 
Interrupcia, eugenika a Hitler 
 
 
Eugenika je snaha o "cielené kríženie ľudského druhu na posilnenie určitých vlastností". Presadzuje prístup, že „spoločnosť by mala od plodenia detí odradiť tých svojich členov, ktorí sú „nespôsobilí" buď fyzicky, mentálne, alebo sociálne"1. Do absurda boli eugenické praktiky dotiahnuté počas fašistického Nemecka, kedy sa trestalo umelé prerušenie tehotenstva árijskej ženy, kým pre židovské alebo postihnuté ženy bola praktizovaná nútená interrupcia alebo sterilizácia. Po roku 1939 mohlo byť vykonanie UPT árijskej žene trestané aj trestom smrti. Manželia, ktorí po piatich rokoch nemali deti, museli platiť vyššiu daň ako formu pokuty. Fašisti rovnako aplikovali „pozitívnu eugeniku", keď mnohodetným árijským matkám udeľovali ceny, pretože pomáhali zachraňovať a rozširovať „čistú rasu". (Podobnosť s rečami o potrebe rodenia kresťanských detí, aby nás „neprevalcovali" moslimovia čiste náhodná?) 
 
 
Eugenika je spojená so štrukturálnymi zásahmi zvonka, s vnucovaním jedného riešenia v mene "spoločného blaha alebo vyššieho princípu" a je vždy spojená s obmedzovaním slobody voľby jednotlivca. Toto slovenská legislatíva ani predkladané novela predsa nerobí. Naopak. Podstatný rozdiel k fašistickej eugenickej praxi je v tom, že nikto nenúti ženu, resp. rodičov aby sa rozhodli pre niektoré z riešení a ani ich k tomu nijako nemotivuje. Skôr by sa za eugenickú prax dal považovať zákaz interrupcií s cieľom "zachrániť slovenský národ alebo prestížnu kresťansko-židovskú civilizáciu". Paradoxne zaň  najhorlivejšie verbujú tí, ktorí presadzujú aj neohraničené právo na výhradu svedomia. Svedomie je predsa súčasťou slobody človeka...iste...tak prečo to zrazu neplatí pri rodičoch? Prečo by žena musela donosiť ťažko poškodený plod, hoci to odporuje jej svedomiu?  
 
 
To, čo dnes v zápale verejnej diskusie niektorí hanlivo označujú eugenikou a porovnávajú s fašizmom - prenatálna genetická diagnostika - je bežnou súčasťou moderných zdravotníckych služieb poskytovaných tehotným ženám. Podporujú ju nielen lekári a ženy samotné, ale i prestížni genetici a nositelia Nobelovej ceny ako John Sulston: "Nemyslím, že niekto túži priviesť na svet ťažko postihnuté dieťa," alebo James D. Watson, prvý riaditeľ Human Genome projektu: "Keď raz máme cestu, vďaka ktorej môžeme zlepšiť zdravie našich detí, nikto to nedokáže zastavit".2 Prenatálna diagnostika je odrazom medicínskeho pokroku a v normálnych krajinách o jej oprávnenosti a legitímnosti nikto nepochybuje, pretože by ho verejnosť jednoducho vysmiala. 
 
 
Prenatálna diagnostika nezabíja 
 
 
Čo zabíja, sú ťažké vývojové chyby plodu. Práve vďaka dnešnej úrovne prenatálnej a genetickej diagnostiky kombinovanej s ultrazvukovými prístrojov vrátane 3D je diagnóza poškodenia plodu stanovená s pravdepodobnosťou hraničiacou so 100% istotou. Tvrdenia o tom, že ženy dnes podstupujú interrupciu kvôli 10% podozreniu na genetické poškodenie plodu sú nezmyselné a jednoducho klamstvo. Rovnako prípady, ktoré sa stávali pred rokmi, kedy lekári predpovedali poškodenie plodu a nakoniec sa narodilo zdravé dieťa, sú pri použití moderných metód a overení diagnózy viacerými spôsobmi takmer nemožné. Je to skôr utešovanie zo strany laikov ako reálna možnosť a nádej pre rodičov. Osočovanie lekárov, že „vyhľadávajú poškodené plody za účelom ich likvidácie" a že nútia ženu podstúpiť interrupciu, hraničia podľa mňa s trestným činom hanobenia dobrého mena či šírenia poplašnej správy. 
 
 
Prenatálna diagnostika, z nej vyplývajúce poznanie stavu a možnosť rozhodnúť sa pre interrupciu poškodeného plodu nie je nič nehumánne a odsúdeniahodné. Je prirodzenou túžbou rodičov mať zdravé dieťa a nikto si neželá, aby sa jeho dieťa narodilo ťažko postihnuté a jeho život znamenal iba utrpenie. Ak je aj pre niektorých veriacich utrpenie dar Boží a vedia ho prijať s pokorou, nie všetci sa s tým stotožníme. Pre mnohých je jednoducho humánnejšie minimalizovať utrpenie ešte predtým, než ho vôbec dieťa začne pociťovať.  
 
 
Hoci prípadov ťažkého poškodenia plodu nie je extrémne veľa, podľa informácií zahraničných gynekológov sa vyskytuje zhruba v každom 135 tehotenstve. Na Slovensku sa medzi 13. a 24. týždňom vykoná ročne okolo 150 až 200 interrupcií (v r.2005 to bolo 1743). Oproti tomu okolo 400 novorodencov zomiera do veku jedného roka, štvrtina (v r.2005 to bolo 109) na vrodené vývojové chyby, najčastejšie chyby srdca a obehovej sústavy4. Z prípadov novorodeneckého úmrtia je polovica na východnom Slovensku, kde je úroveň navštevovania tehotenských poradní najnižšia (hlavne u rómskych žien žijúcich v marginalizovaných komunitách). 
 
 
Z údajov nie je zrejmé, koľko z tých prípadov bolo prenatálne diagnostikovaných. Zrejmé je ale to, že aj keď sa žena pri ťažkom poškodení plodu nerozhodne pre UPT, dieťa ešte zďaleka „nemá vyhraté" a svoj boj o život často prehrávam, akurát s väčšou mierou utrpenia pre seba i rodičov. 
 
 
Keď rozhodovanie znamená peklo 
 
 
Žena, ktorá sa v druhom trimestri želaného tehotenstva náhle dozvie, že jej dieťa bude ťažko postihnuté alebo dokonca je jeho stav nezlučiteľný so životom, prechádza svojim súkromným peklom. Takáto situácia je určite jednou z najťažších skúšok v živote. Je to rozhodovanie, ktoré navždy zanechá v trinástej komnate podvedomia trpkú pachuť a bude sa ešte dlho objavovať v desivých snoch. Práve preto nikto - opakujem, NIKTO - nemá právo jej a jej partnerovi pri rozhodovaní niečo vnucovať. V konečnom efekte zostáva zodpovednosť vždy na nich samotných, dokonca niekedy iba na žene. Nie sú zriedkavé prípady, kedy muž spoločnú výchovu postihnutého dieťaťa nezvládne a rodinu opustí.  Rovnako som presvedčená, že nikto nemá právo súdiť, moralizovať a odsudzovať, pretože nemôže situáciu týchto ľudí precítiť a pochopiť. 
 
 
Niekedy v rozhodovaní pomáhajú aj vonkajšie okolnosti. Zrejme to má hlboko veriaca žena jednoduchšie, pretože ju možnosť voľby neťaží. Viera a cirkev jej v tomto prípade vlastne žiadnu neponecháva. Tie ostatné ženy sa musia nejako rozhodnúť a akokoľvek to dopadne, musí to byť ich vlastné rozhodnutie, s ktorým budú vedieť žiť a vyrovnať sa. V našej spoločnosti nie sú vytvorené žiadne mechanizmy ani štruktúry, ktoré by im pomohli alebo ich aspoň podporili vo vedomí, že nie je vo svojom ťažkom osude sama. Jediné, čo sa im ujde, je morálne odsúdenie zo strany našich "teoretických odborníkov".  Čím menej skúseností títo morálni inkvizítori s podobnými situáciami majú, tým plamennejšie a tvrdšie sú ich hodnotenia, tým lepšie vedia, čo by daná žena urobiť mala a čo nesmie. 
 
 
Iste, sú prípady, kedy aj ťažko postihnuté dieťa dokáže obohatiť život a to často o úplne nové dimenzie. Dnešné medicínske možnosti poskytujú nádej pre deti s vrodenými chybami v ľahšej forme ako hydrocefalus, rázštep chrbtice alebo brušnej dutiny, ktoré by pred pár desaťročiami takmer nemali reálnu šancu na život. Takéto prípady sú skôr výnimkou a je neprípustné, aby sa na ich základe podsudzovali všetky ostatné v duchu "pozrite sa, zabili ste dieťa, ktoré mohlo žiť". Každý prípad je iný a existuje mnoho vývojových chýb, ktorých korekcia alebo aspoň zlučiteľnosť s prijateľnou formou života ešte nie je možná a pravdepodobne ani nikdy nebude. Vysoko si vážim matky/rodičov, ktorí sa o také dieťa starajú a obetujú mu svoj osobný život. Musí to byť ale ich vlastné rozhodnutie. Neverím však, že si také dieťa niekto vopred želá alebo že sa matka vo svojom ťažkom údele aspoň občas nezamyslí nad tým, "prečo práve ja?" Je to predsa úplne ľudská reakcia na ťažkú situáciu. 
 
 
Rovnako tvrdenia o tom, že treba poškodené dieťa za každú cenu priviesť na svet a dať na adopciu sú z roviny utopických. Detské domovy sú plné detí, ktoré sa len máličko odlošujú farbou pleti a už o ne nie je záujem. Akú šancu na adopciu by mal potom ťažko postihnuté dieťa?    
 
 
Pretože raz vidieť je lepšie ako stokrát počuť 
 
 
Áno, súhlasím, takéto deti resp. plody sú v určitom zmysle "diskriminované". Nie však zo strany zákona alebo rodičov, ale zo strany prírody (alebo Boha, ako chcete). Mali jednoducho smolu, keď v priebehu embryonálneho vývoja niečo zlyhalo. Ide totiž o úžasne komplexný postup a je vlastne malý zázrak, že sa väčšina detí rodí úplne zdravých.   
 
 
A aby bol jasné, o čom sa tu vlastne bavíme: čo môže znamenať genetické poškodenie plodu a vrodené vývojové chyby v praxi. Banalitu? Maličkosť? Chybičku krásy? 
 
 
Pre tých a tie, ktorí majú potrebu podpisovať petície proti interrupčnému zákonu a diktovať všetkým svoje presvedčenie prikladám pár ilustračných záberov vývojových porúch. Dúfam, že sa aspoň zamyslia nad tým, či by oni sami chceli mať takéto deti, či by oni chceli žiť takýmto životom, pripútaní na hadičky bez možnosti a schopnosti preciťovať a vnímať to, čo robí človeka človekom. Či by oni sami v takejto situácii tak urputne stáli o to svoje "právo na život". 
 
 

	
		
			 

			 

		
		
			
			 
			Anencefalia: Je to vývojová anomália centrálneho nervového systému. Jej znakom je chýbanie lebečnej klenby a veľkej časti mozgu. Vývojová anomália sa zakladá na poruche uzatvorenia neurálnej trubice počas 3. až 4. embryonálneho týždňa a patrí, tak ako spina bifida, k defektom neurálnej rúry. 
			 
			
			
			 
			Anencefália sa dá počas tehotenstva zistiť ultrazvukovým vyšetrením. Ďalšou možnosťou včasného zistenia je krvné vyšetrenie tehotných žien v 16. týždni tehotenstva (Triple-test). Neexistujú žiadne možnosti liečby. Deti sa narodia väčšinou mŕtve alebo zomierajú krátko po pôrode. V strednej Európe je týmto defektom postihnutých asi jedno z 1 000 tehotenstiev, čo by znamenal na Slovensku asi 50 prípadov ročne. 
			 
			
		
		
			 

			  

		
		
			
			 
			 Exencefalia, rovnako ako anencefalia vrodená chyba uzatvorenia neurálnej trubice. Mozog sa nachádza mimo lebky. Zisťuje sa rovnako ako iné defekty neurálnej trubice. Vyskytuje sa raz medzi 2 000 tehotenstvami.  
			 
			
			
			 
			 Cyclopia - zriedkavá vývojová porucha, kedy sa pri vývoji tváre nevytvoria dve oči. vyskytuje sa zruba raz na 16 000 tehotenstiev. 
			 
			
		
		
			 

			 

		
		
			 Siamske jednovaječné dvojičky

			 V takýchto prípadoch nie je ani teoretická šanca na život.
		
		
			
			 
			   
			 
			
		
		
			
			 

		
		
			
			 
			Acrania  - chýbajúca lebečná kosť a masívna porucha uzatvorenia neurálnej trubice.  Ľahšie prípady rázštepu chrbtice (spina bifida occulta) dovoľujú viesť pomerne normálny život, ťažšie sú si životom nezlučiteľné. 
			 
			
			
			 
			 Hydrocefalus -vodnatieľka mozgu- v podobe nezlučiteľnej so životom. Najčastejšie vzniká ako následok obštrukcie prechodu likvorových ciest - tzv. obštrukčný hydrocefalus (ca.94%), menej často vzniká ako následok nadprodukcie likvoru alebo nádoru. Ľahšie formy je možné dostať pod kontrolu odvádzaním tekutiny z mozgu, liečiť sa ale nedá. Vyskytuje sa zhruba raz medzi 500 tehotenstvami. 
			 
			
		
		
			 
		
	

 
 
1 Podľa http://sk.wikipedia.org/wiki/Eugenika  
2 Podľa http://en.wikipedia.org/wiki/Eugenics#Ethical_re-assessment  
3 Zdravotnícka ročenka SR za rok 2005, Potraty podľa druhu vo vekových skupinách, Tab. 1.7.1  
 4 Zdravotnícka ročenka SR za rok 2005, Úmrtia a úmrtnosť do 1 roku podľa príčin smrti, Tab. 1.5 
 
 
 
Zdroj fotografií a informácií:  
http://www.neuropathologyweb.org/chapter11/chapter11bNTD.html http://www.pathology.vcu.edu/WirSelfInst/devdis.html http://isc.temple.edu/neuroanatomy/lab/embryo_new/index.htm  
http://www.primar.sk/ 
 
 

 

