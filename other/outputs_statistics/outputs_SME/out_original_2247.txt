
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Líška
                                        &gt;
                Čo čítam ?
                     
                 Manipulácia histórie 

        
            
                                    10.2.2010
            o
            15:44
                        (upravené
                23.2.2010
                o
                15:20)
                        |
            Karma článku:
                5.81
            |
            Prečítané 
            1608-krát
                    
         
     
         
             

                 
                    Žijeme v období manipulácií. Sme vo veľkom ovládaní a manipulovaní demokraticky politikmi, médiami, farmaceutickými spoločnosťami, ezoterickými hnutiami a cirkvami, bankami, novinármi, mimovládkami ... etc....a mnohé z nich ťahajú za jeden povraz. Keby som mal charakterizovať jedným slovom dobu, v ktorej žijeme, povedal by som - manipulácia...
                 

                 
 V. Líška
      Nie je to až také výrazné, ako v totalitných réžimoch 20. storočia, v súčasnosti je to ,,demokratický,, skrytý podvedomý legálny vplyv. Stále sme figúrky, ktoré žijú vo svojej ilúzii slobodnej voľby, avšak stále otrokmi spoločnosti a systému. Dejiny píšu víťazi, históriu píše archeológia, presnejšie archeológovia, pre ktorých je skutočnosť, že mohli existovať civilizácie na omnoho vyššej úrovni nemysliteľná. Odchovaní na ateistickom darwinizme, resp. na viere vo vínimočnosť a jedinečnosť božieho stvorenia (človek - vrchol tvorstva) ignorujú všetky nepohodlné dôkazy, aby tak čo najdlhšie udržali ľudstvo v nevedomosti... Ale raz výjde pravda na povrch a mýty starých sa ukážu vo svetle pravdy ako skutočnosť, kým naše ilúzie o historii len ako nedokonalý klam. ...    Ludia si to pomaly začínajú uvedomovať, že      veci, ktoré sa dejú napríklad okolo očkovania sú minimálne čudné... Bude hromadné povinné očkovanie prvým      náznakom totalitnej svetovlády ?Je zdravotníctvo a lekárska veda      ovládané a deformované ziskuchtivými záujmami farmaceutického priemyslu? Slúži      OSN, ktorá by mala riešiť otázky svetového mieru, zdravia a prosperity,      záujmom lobistických skupín? Bol vírus HIV, Eboly, vtáčej a prasacej      chrípky vyvinutý genetickými manipuláciami v laboratóriách? Plánuje      svetovládna elita takto znížiť o dve tretiny populáciu ľudstva biologickými a      geofyzikálnymi zbraňami (umelé zemetrasenia)?    Môžme brať stále vážne archeológiu, skostnatenú extremnekonzervatívnu vedu, ktorá sa rozplýva nad jedným kúskom črepiny, spriada od neho celú históriu a príbehy, zatial čo iné dôkazy, ktoré im do ich vlastného vymysleného príbehu nepasujú hádže pod koberec ?.... Uvádzam zopár príkladov:         Príkladov sú tisíce, píše o nich von Daniken, Hausdorf, Butler, Wojnar, Souček, Bergier, Brennan a iní, uvediem tie, o ktorých som čítal naposledy, ktoré sú verejnosti menej známe. Apropo asi pred rokom vyšli knihy: Zakázaná archeologie (Klaus Dona, Habeck Reinhard) a zakázaná egyptologie (Erdogan Ercivan), spomínané knihy som zatiaľ nečítal.    Presne opracovanú skalnú formáciu Hayu Marca v peruánskych Andách pokladajú indiáni za hviezdnu bránu, ktorou prichádzali a odchádzali ich bohovia. Bohovia mizli vraj v modrom prenikavom svetle a takouto bránou vraj ušiel pred španielmi aj incký kňaz Amaru Maru. V 4000 m výške sa nachádzaju mesta Tiahuanaku a Puma Punku. V mimoriadne tvrdej andezitovej hornine postavili indiáni (?) grandiociózne chrámové komplexy, podľa archeológov mokrými klinami a kamennými nástrojmi...Chrám Kalasasaya ohraničujú až 100 tonové (!) stavebné prvky z kameňa...Podobný komplex bez laseru, presných fréz by nebolo možné postaviť.. Brána sa otvárala zlatým kotúčom - kľúčom bohov siedmych lúčov ...ten sa vkladal do guľatej priehlbni v skale.. Indiáni veria, že v blízkej dobe sa majú bohovia vrátiť a nastoliť zlatý vek... H. Hausdorf správne navrhuje, aby páni archeológovia demonštrovali svoje teórie aj v praxi s pílkami a  kamennými pästnými klinmi. Cestovateľ Thor Heyerdahl robil podobné pokusy... Chcel vytesať sochy primitívnym spôsobom, také aké sú na veľkonočnom ostrove. No za jeden deň celodennej driny sa mu podarilo urobiť s dvoma tuctami ľudi akurát ...pár milimetrov širokú čiaru...do skaly....ale archeológovia musia mať skrátka pravdu ! ...   Pod pyramídou v Sakkáre sa našlo 30 000 váz s labutími krkmi z ruženínu, dioritu, bazaltu. Fakt je ten, že vyrobiť takéto vázy s úzkymi krkmi, s tak tvrdých hornín až doteraz nebolo možné ! Až v súčasnosti existujú ultrazvukové vrtáky schopné niečo také vyrobiť. (Brennan)       Na nálezisku Paj-kung v Číne sa vyskytuje množstvo zvláštnych kovových rúriek v kameni, ktoré už dokonca aj komunistická vláda v Číne vyhlásila, že sú mimozemského pôvodu a veľmi staré - min. 300 000 rokov. V okolitých jazerách sa hojne vyskytuje lithium... Ide o 80 m vysokú horu-stavbu-pyramídu, v ktorej je zabudovaný systém trúbiek nevysvetliteľného pôvodu. Prirodzený vznik je vylúčený. Miestne povesti hovoria o mimozemských návštevníkoch. V Európe sa však podobné teórie neberú vážne a témy okolo ufo sa presúvajú skôr do ezoterickej oblasti. Tu sa však vyskytujú učenia, ktoré veciam okolo UFO len zhoršujú image. Pri pojme - Vesmírni lidé - Milujeme Vás a pomáhame Vám :)) sa každý len ironicky pousmeje... Zaujímavejší je ďalší prípad.    V minulosti sa našlo 716 kamenných diskov s podivným písmom v hroboch malých bytostí, podla archeológov – opíc :) ...V Číne v horách Bajan Kara Ula sa nachádza pre cudzincov uzavretá oblasť. Podla legiend tu žili bytosti malého vzrastu, ktoré prišli z nebies. V roku 1962 sa podarilo tieto nápisy rozlúštiť. Pred 12 000 rokmi došlo k nehode, pri ktorej boli návštevníci nútení ostať na Zemi. Vedecký svet odbil príbeh ako totálny nezmysel a profesor, ktorý tento príbeh skúmal musel odísť z komunistickej Číny do svojej vlasti Japonska. V múzeu v Bajan Kara bolo pár exemplárov kotúčov, tie však zmizli a s nimi  aj sprievodkyňa v múzeu. V Číne to je celkom bežná prax, mnoho nepohodlných ľudí - odporcov réžimu, najmä členov Falun Gong sa stratí bez stopy.  V oblasti bolo nedávno objavených 120 živých miniľudí vysokých 1,15 m. Ide o veľkú hádanku ľudstva, oblasť je pre cudzincov uzatvorená...Ide o akési getto, v ktorom sú  pravdepodobne potomci dávno stroskotanej kozmickej lodi...   ..Príbehy o potope, o stvorení boli známe 6 000 rokov pred tým ako bola napísaná biblia (sumerský epos o stvorení). Sumerské kresby rakiet, postáv v kozmických skafandroch, kyslíkových maskách sú jasným dôkazovým materiálom, že už v dávnych dobách existovali civilizácie na vysokej technickej úrovni...    ...Majster Ribchu zostrojil lietajuci bojový voz bez koňa, lietal po cestách vtákov. Voz sa pohyboval rýchlejšie ako myšlienka a na zem sa spúšťal so silným revom. (indické Rigvédy - vek min. 5000 r.)      Máme predátora, který přichádzí z hlubin vesmíru a přebíra pravidla našeho života. Lidé jsou vězni. Predátor je náš búh a mistr. Odevzdávame se mu bezmocně. Jestliže chceme protestovať, potlačí náš protest. Jesliže chceme být nezávislý, vymúže si abychom to nedělali. Vskutku jsme drženi jako vězni. (Castenada)                                                                                                           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Líška 
                                        
                                            The Secret - skutočná tvár neskutočného oblbováku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Líška 
                                        
                                            Veštice a veštenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Líška 
                                        
                                            Metalový sviatok - koncert Cradle Of Filth, Moonspell, Turisas
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Líška 
                                        
                                            Smrť (košický cintorín, karma a osud, eutanázia...)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Líška 
                                        
                                            Historický park v Lednických Rovniach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Líška
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Tatiana Lajšová 
                                        
                                            Recenzia: Fantázia 2014
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Líška
            
         
        vladimirliska.blog.sme.sk (rss)
         
                                     
     
        zaujíma ma príroda, taoizmus a čínska filozofia, čikung, joga, hranie na hudobných nástrojoch, japonský šerm, muzika, psy, knihy, filmy, písanie haiku, staré stromy etc...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3450
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Čo čítam ?
                        
                     
                                     
                        
                            Čo počúvam ?
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Su Ming-tchang - Čchi-kung
                                     
                                                                             
                                            T.E.Rostas - Mlčanie
                                     
                                                                             
                                            H. Hausdorf - Když si bohové hrají na boha
                                     
                                                                             
                                            Viktor Farkas - Nemilosrdná moc
                                     
                                                                             
                                            Roši Kaisen - Budo dharma, poučení samuraje
                                     
                                                                             
                                            H. Hausdorf - Biela pyramída
                                     
                                                                             
                                            H. Hausdorf - Ne z tohoto světa
                                     
                                                                             
                                            E. von Daniken - Bohové byli astronauti
                                     
                                                                             
                                            Lovecraft, POE - všetko
                                     
                                                                             
                                            Picknett - Spiknutí hvězdná brána
                                     
                                                                             
                                            Dr.Ing. Zillmer - EVOLUCE podvod století
                                     
                                                                             
                                            Maclellan - Agartha
                                     
                                                                             
                                            Florek - Kniha o runách
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            http://www.carovnezrcadlo.cz
                                     
                                                                             
                                            http://www.osud.cz
                                     
                                                                             
                                            http://www.zvedavec.org/
                                     
                                                                             
                                            http://www.youtube.com/watch?v=oTKvYWsQGjc
                                     
                                                                             
                                            www.facebook.com
                                     
                                                                             
                                            www.myspace.com
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




