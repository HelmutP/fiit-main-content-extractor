
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ronald Ižip
                                        &gt;
                Nezaradené
                     
                 Ako ekonómovia šíria mor 

        
            
                                    11.3.2010
            o
            13:13
                        (upravené
                11.3.2010
                o
                13:18)
                        |
            Karma článku:
                9.62
            |
            Prečítané 
            2383-krát
                    
         
     
         
             

                 
                    Švajčiarska banka UBS prišla s veľmi pekným príbehom, ktorý v jednoduchosti poukazuje na obmedzenia, ktoré naše konanie predstavuje. Dovoľte mi ho preložiť a pridať k nemu krátku poznámku.
                 

                 Čierny mor  10. marca roku 1349, v čase kulminácie epidémie neslávneho čierneho moru, spojili svoje sily vládne vedecké a akademické kruhy, aby našli plán ako zachrániť ľudstvo. Ako pravdepodobne viete, mor sa šíril z oblastí Stredomoria cez väčšinu Európy a za predchádzajúce 3 roky zabil milióny ľudí. Ľudia sa snažili všemožne objaviť zdroj moru.... nebeské prekliatie, dar imigrantov alebo dôsledok korenín v jedle. Avšak bolo ťažké tento zdroj objaviť, pretože vždy, keď sa konala konferencia, tak usporiadateľské miesto bolo zasiahnuté morom alebo ho priniesli návštevníci.   6 mesiacov predtým sa zdalo, že nárast chorobnosti sa zastavil. To dodalo šancu zozbierať najväčšie mozgy danej doby, aby zistili pôvod tejto choroby. A dostali sa celkom blízko. Kolektívna múdrosť vlád a akademikov označila za zdroj čierneho moru blchy (čo bolo absolútne správne). Začalo sa tak šíriť povedomie, že na zastavenie moru treba vykynožiť blchy. Najviac bĺch mali psy a začal tak masaker psov.   Podobne ako to dopadlo aj s inými vládno-akademickými myšlienkami, táto idea sa netrafila svojim účinkom a mala neželané neočakávané následky. Príčinou boli blchy, to bolo v poriadku, avšak nie psie, ale blchy potkanie. A práve v 14. storočí boli psy tým najúčinnejším spôsobom znižovania populácie potkanov. Preto elimináciou psov vlády neúmyselne umožnili výrazný nárast populácie potkanov a čoskoro sa objavili nové obete moru a ten sa začal prudko rozširovať. O 3 roky neskôr, keď morová epidémia skončila, moru podľahol každý tretí človek.   Záver  UBS článok uzatvára s tým, že hoci vlády a akademici tvrdo pracujú na riešení súčasnej krízy, ich riešenie zďaleka nemusí byť tým najefektívnejším a možno často naopak kontraproduktívnym riešením.   Ja by som si dovolil zájsť ešte oveľa ďalej. Bibliológia a medicína sú exaktné vedy. Pri nich je jednoznačné určiť, ktoré príčiny spôsobujú aké následky. Medicína sa vyvíja a vie sa učiť na predchádzajúcich chybách. Ekonómia ako sociálna veda je však iného rangu. Ekonomika je živým systémom, ktorý tvoria miliardy samostatných a samostatne zmýšľajúcich jednotiek. Predpokladať ako sa zachová jedinec je omnoho ťažšie ako predpokladať ako sa zachová fyzikálna častica. Nehovoriac o spoločnosti, čo je súhrnom často iracionálne rozmýšľajúcich bytostí.  Moderná ekonómia dospela veľmi ďaleko. Bohužiaľ dospela ďaleko iba v tvorbe ekonomických modelov založených na nereálnych podmienkach. Dovolím si tvrdiť, že väčšina nositeľov Nobelovej ceny za ekonómiu bola ocenená za sofistikovanú, avšak absolútne nepoužiteľnú teóriu. Aplikácia teórie zjednodušujúcej realitu alebo založenej na nesprávnych premisách môže spôsobiť absolútnu katastrofu. Jej implementácia v praxi znamená, že spontánny trhový systém, ktorý funguje na vzájomnom konsenze miliónov účastníkov trhu sa začne rúcať. A iba zbesilé novátorstvo vo forme kvantitatívneho uvoľňovania a inej manipulácie trhu dokáže nakrátko systém stabilizovať.   Momentálne obľúbený hon na hedge-fondy a špekulantov mi pripomína hon na psov z konca 14-teho storočia. Správanie špekulantov je barometrom zdravia ekonomiky, oni nie sú tí, ktorí spôsobili obrovské nesplatiteľné deficity. Oni sú tí, ktorí chcú na klamstvách vlád a vytváraní trhových absurdít profitovať. Tak ako by to mal robiť každý z nás. Zabitím špekulantov zabijeme trhy; vieme aké malo zabitie psov následky pre ľudí 14. storočia. Túto chybu by sme nemali zopakovať...  Prestaňme preto veriť ekonomickým teóriám, ktoré nefungujú. Ekonómovia sú výborní v rekapitulácií udalostí, nie však v ich predvídaní. Vráťme sa naspäť k ekonomike, ktorá je založená na skutočných hodnotách a skutočnom bohatstve. Vráťme sa k skutočnému sedliackemu rozumu a počúvajme, čo nám hovorí... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ronald Ižip 
                                        
                                            Ak padne Rím, padne svet
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ronald Ižip 
                                        
                                            Sulíkov Matrix
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ronald Ižip 
                                        
                                            Nemecká expanzia bude drahá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ronald Ižip 
                                        
                                            Keď Grécko skrachuje...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ronald Ižip 
                                        
                                            Štáty siahajú na peniaze občanov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ronald Ižip
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ronald Ižip
            
         
        izip.blog.sme.sk (rss)
         
                                     
     
        Ekonóm, finančný analytik a trochu filozof..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    24
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1277
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 vystrihla nepríjemné otázky novinárov na Fica
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




