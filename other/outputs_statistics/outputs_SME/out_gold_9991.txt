

 Medzi množstvom vychutnávačov tepla, vody, vzduchu, sedela mladá žena. Schúlená, objímala si rukami nohy. Hľadela smutno pred seba. Dievčatko v bodôčkavých plavočkách čľapotalo sa pred jej zrakom. Búrlivo sa tešilo vlnám, ktoré drankali do malých kolienok a zaplavovali drobučký zadoček. 
 „Mamiíí, mamiííí, pozri sa...! Mamiíí, hrajú sa so mnou... Vlnka, vlnka, žblnk... vlnka, vlnka, žblnk...“ mongoloidná tvárička žiarila šťastím. Dievčatko skackalo, trepalo rúčkami, norilo si tvár do morských vĺn. 
 Po brehu sa uberal chlapček. V ruke držal kýblik a lopatku. Prišiel až k dievčatku. Chvíľu sa naň díval. Potom k nej prikročil a chytil ju za ruku. 
 „Poď, poď sa hrať! Postavím ti hrad...“ 
 Dievčatko pozabudlo na morský príboj. Kvapôčky vody sa mu perlili na prekvapenom obličaji. Skúmavo si prezeralo chlapčeka pred sebou. 
 „Poď!“ chlapček ju schytil tuho za rúčku a ťahal vyššie na breh. 
 Dievčatko sa podvolilo. Vyšli spolu na piesočnatý zlatistý breh. Chlapček začal kopať vo vlhkom tmavom piesku. Dievčatko si pri ňom čuplo. Útlymi dlaničkami mu posúvalo piesok k stavbe. Chlapček staval. Staval. O chvíľku bol hrad vybudovaný. S pýchou v očkách zazrel na dievčatko. To sa rozbujdošene rozosmialo, pokývalo hlávkou a ani divé začalo drepčiť po práve postavenom hrade. Tešilo sa, skákalo, smialo sa. 
 Chlapča onemelo. Udivene sa dívalo na ničivú radosť vrstovníčky. Dievčatko prestalo skackať, pristúpilo k nemu a zadívalo sa mu do tváre, akoby zachmúrenej. Už sa neusmievalo. Nadvihlo rúčku a pohladilo chlapčeka po tváre, zašpinenej od piesku. 
 Chlapček sa usmial. Povedal: 
 „Poď, to nič, že si to zbúrala, poď, ideme chytať vlny...“ 
 Obidvaja opäť rozosmiati, rozšantení vrhli sa do vody. Čľapkali, penili ešte viac spenenú, sperlenú vodu. Fŕkali si mokrotu do tváre, padali do nej, vstávali, diveli. 
 Žena na brehu sa šťastne vystrela. 
   

