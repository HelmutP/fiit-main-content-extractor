

 
 FOTO SME – ĽUBOŠ PILC 
 

 V týchto dňoch vrcholí zber broskýň. V sade Poľnohospodárskeho družstva Vajnory ich denne oberá a triedi asi šeťdesiat ľudí, z toho najmenej 40 brigádnikov. A denne na ne v sade od rána stoja rady kupujúcich. 
 „Broskyne sme začali predávať od začiatku júla. Prvé boli len na jedenie. Asi tak od polovice júla predávame aj broskyne na zaváranie pre drobných záujemcov i veľké firmy,“ hovorí Ivan Kyselica, vedúci predaja družstva, ktoré ako jediné v hlavnom meste nezlikvidovalo svoje ovocné sady a úrodu z nich dodnes vo veľkom predáva. 
 Sad Poľnohospodárskeho družstva Vajnory má okolo 52 hektárov, broskyne tu pestujú na ploche 16 – 17 hektárov. Na zvyšnej ploche sú najmä jablone, malú časť tvoria aj marhule. 
 „Marhúľ bolo málo, ale úrodu broskýň máme tohto roku výrazne lepšiu ako minulý rok, je ich asi dvakrát viac. Ale aj tak musíme odmietať veľké firmy napríklad od Žiliny a Topoľčian, lebo by sme kapacitne nestačili,“ hovorí Kyselica. Šancu nakúpiť pre veľkosklady, obchody a trhoviská majú preto len firmy z Bratislavy a okolia. 
 S uspokojovaním drobných záujemcov, ktorí kupujú broskyne pre vlastnú potrebu na zaváranie, však nemajú problém. Len si na ne musia zvyčajne počkať. „Záujem je naozaj veľký. Už trištvrte hodiny pred začiatkom predaja je tu aj 10 – 15 ľudí a počas dňa sa ich tu niekedy nazbiera toľko, že čakajú aj dve hodiny,“ hovorí Kyselica. 
 Každý tiež nemusí dostať kategóriu broskýň, o akú má záujem. Vopred totiž nie je možné vedieť, aké ovocie zo sadu privezú. Objednávky od drobných spotrebiteľov však vedúci predaja neprijíma. Ako povedal, „bolo by to  nezvládnuteľné“. Drobných záujemcov, ktorí berú zvyčajne len jednu (12 – 13 kg) až sedem prepraviek broskýň, je totiž viac ako firiem, a tie berú oveľa väčšie množstvá. 
 Družstvo zatiaľ predalo deväť až desať vagónov broskýň (čo je 9 – 10 ton), zo štyri ešte má. Broskyne by sa preto orientačne mohli predávať do 10. až 12. augusta. Ich predaj však bude závisieť od počasia. 
 JANA MARTINKOVÁ 
 Predaj broskýň v sade vo Vajnoroch funguje denne od pondelka do soboty. Pondelok až piatok sa broskyne predávajú v čase od 7.30 do 12.00 a od 12.30 do 18.00. V sobotu od 7.30 do 12.00. Ivan Kyselica tvrdí, že tých, čo čakajú v rade, určite nepošlú domov naprázdno, aj keď je po šiestej hodine. Minulý týždeň sa v piatok a v sobotu už nepredávalo, pretože všetky zrelé broskyne ľudia do štvrtka vykúpili.Ceny broskýň 
 výber	25 – 30 Sk/kg 
 I. trieda	18 – 24 Sk/kg 
 II. trieda	13 –  17 Sk/kg 
 III. trieda	8 – 12 Sk/kg 
 (na džem a lekvár) 
 Kategóriu určujú normy podľa veľkosti a kvality broskýň. Aj veľké broskyne môžu byť zaradené do III. triedy, ak sú trochu chrastavé alebo majú nejakú estetickú chybu. Na broskyne si treba priniesť vlastné prepravky, tašky alebo iné nádoby, pretože družstvo svoje prepravky nepredáva ani nezapožičiava. 	(jm) 

