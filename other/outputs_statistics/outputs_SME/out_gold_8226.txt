

   
 Dnes by veľa Sloveniek a Slovákov napriek tomu, ako sa majú, kľudne tvrdilo, že sa za komunizmu mali lepšie. Do kostola chodili síce len tajne ale inak sa mali dobre. Minule som to dokonca počul od nejakej tety zo Záhoria na televíznej stanici ARTE. K Morave vraj nemohla, lebo tam boli pohraničníci a ostnatý drôt. Ale deti vyštudovali vysokú školu a postavili si aj dom. Dobre bolo vtedy. Dnes aha: „Nemáám any robotu, a vela je nás tu takých.." Nejeden našinec povie, že je to tak, ako to „prorokoval" dnešný premiér v roku 2002. Sme v Európe, no s holými zadkami. Bežný Slovák vraj z toho nič nemá, že môže cestovať, keď na to nemá ani peniaze, ani jazykovú výbavu. Boli sme síce do krachu Grécka čistými prijímateľmi európskych peňazí, no tie sa zvlášť posledné štyri roky zasekli kdesi „hore" a dole padali riadne preosiate.  A teraz ešte aj tie povodne a rezerva sa už minula. Dnešný Slovák má káblovú alebo satelitnú televíziu, počúva desiatky rozhlasových staníc, kupuje si DVD a CD nosiče so svetovou súčasnou tvorbou. Jeho deti cestujú a študujú v zahraničí. Možno sa tam aj udomácnia a nikto ich za to nebude stíhať. Ani ich rodičov a súrodencov, čo ostali doma. Pôjdu pokojne na návštevu. Do Ameriky, do Austrálie, na Nový Zéland. Nemecko, Švajčiarsko či Španielsko už prestali byť aj atraktívne.  
 Do loga strany SMER za tie roky - ani nevedno kedy - pribudla akási štylizovaná ruža. Šípka sa zmenila na štylizovanú stonku. Monika Beňová nepozná súčasné logo, Robert Fico zapiera pôvodné. Ktovie ako to celé bolo. Bohumil? Logo očervenelo a zakvitlo. O tom niet pochýb.  
 
 







 




 Dajme im ružu. Nech sa na ňu zakladajúci členovia zjednotenej sociálnej demokracie pozerajú. Tí ktorí si ju nevšimli, aj tí, ktorí ju zapierajú. Generálny prokurátor pán Trnka by mohol síce konať pružnejšie ako koná, aby sa všeličo vyjasnilo, no možno po voľbách sa do toho pustí.  
 Bude dosť ruží aj pre neho a napr. pre pána Harabína či pani Petríkovú? Bol to ostatne pán generálny prokurátor, čo pred časom povedal, že naša spravodlivosť nie je len slepá, ako ju predstavuje klasická ikonografia. Podľa slov pána Trnku, slovenskej spravodlivosti okrem zaviazaného zraku vtedy chýbalo aj všeličo iné. Naozaj sa nezdá, že by on k zlepšeniu kondície či aspoň zotaveniu slovenskej „spravodlivosti" niečím prispel. Ja by som si na jeho mieste určite dal záležať na očistení „dobrého (ťažko sa mi to píše) mena" pána premiéra. Alebo aspoň vlastného. Ešte pred voľbami.  
 Zdá sa, že scénu ako si Robert Fico namiesto do premiérskeho kresla sadá pri vyšetrovaní na lavicu podozrivých vo veci podozrenia zo sľubovania neprimeraných výhod za finančné dary pre rodiacu sa stranu Smer si okrem Bohumila Hanzela vie predstaviť len málokto. Ruža pre premiéra a jeho najvernejších súputníkov však nemôže nič pokaziť. V stroho zariadenej miestnosti môže taká ruža neskôr naozaj aj potešiť. Len tak. Videli ste film Pay It Forward? Tu je adresa: Úrad vlády Slovenskej republiky, Námestie slobody 1 ,813 70 Bratislava. Virtuálne ruže pre pána premiéra: urad@vlada.gov.sk. Za to, čo pre túto krajinu spolu so svojimi koaličnými partnermi pán premiér spravil si to určite zaslúži. Keď už za nič iné, tak za neobyčajené zefektívnenie komunikácie pomocou softvéru LenIDIOT Bejzik. Čoskoro nám na dorozumievanie bude stačiť zopár vulgárnych slov a nasrdený výraz tváre. Preto navrhujem: Ružou na nich.  
 Aj tí príslušníci VB v roku 1989 sa na nás tak trochu vyjavene dívali, keď dostali „trikolóru". Nevedeli totiž, aké to všetko naberie smer. Nečakali, že slobodné voľby raz vôbec budú. 
  

