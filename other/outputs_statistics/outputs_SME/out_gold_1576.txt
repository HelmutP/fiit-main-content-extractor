

  
 Farský kostol Nanebovzatia Panny Márie bol postavený v gotickom štýle v 14. storočí 
  
 Papierový model kostola vyšiel v tohtoročnom aprílovom čísle časopisu Fifík. Aj keď tento časopis je určený mladšej generácii, model je relatívne dosť prepracovaný a zložitý. 
  
 87-metrová kostolná veža bola dokončená až v roku 1893 
  
 Asi 500-krát zmenšený model má vežu vysokú 17 centimetrov 
  
 Aby sa drobné detaily dali zhotoviť čo najlepšie, prekopíroval som originálnu 2-stranovú vystrihovačku na tenký papier. 
  
  
  
   

