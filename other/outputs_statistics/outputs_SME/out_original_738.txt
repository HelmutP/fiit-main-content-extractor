
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nataša Holinová
                                        &gt;
                Ňa nebudete nasírat furt
                     
                 Kde udělali soudruzi z T-Comu chybu? 

        
            
                                    9.7.2008
            o
            10:04
                        |
            Karma článku:
                12.86
            |
            Prečítané 
            4300-krát
                    
         
     
         
             

                 
                    Pred pár dňami som si cez telefón objednala 4-géčko namiesto doterajšieho neviemakého turbo internetu. Dohoda znela, že najprv mi príde poštou nové prihlasovacie meno a heslo, potom jedného dňa zistím, že starým spôsobom sa pripojiť nedá, a prihlásim sa novým.  Trt a tri orechy, hovorieval Satinský.
                 

                  Včera na súmraku sa idem pripojiť a onô tvrdí, že chyba 691: Prístup bol odmietnutý, pretože meno používateľa alebo heslo bolo na doméne neplatné. Pomyslím si, že sú to somári, lebo poštou zatiaľ nič nedošlo, ale nebolo by to prvé zlyhanie pošty v našich krajoch.     Volám 0800123456. Zvolila som „služby bytovým zákazníkom", kde som vybrala možnosť hovoriť so živým človekom. Ihneď to oznámilo, že živí sú zaneprázdnení a mám zavolať neskôr alebo nechať odkaz. Potom mi vypadla pevná linka a niekoľko minút nešla. Volala som na ňu mobilom, potom naskočila.     Druhý telefonát na 0800123456: nechávam prebehnúť všetkých iks možností, lebo mám dojem, že nakoniec sa aj tak dočkám živého človeka. Stalo sa. Živý mi po istých peripetiách poskytol nové meno aj heslo a zaželal príjemný večer.     A onô furt hlási chybu 691.     Tretí telefonát na 0800123456. Zdvihla Diana. Intímne sa jej zverujem. Diana je toho názoru, že najlepšie mi pomôže centrum internetovej podpory 0800123777, voľba 4. Diana praje príjemný večer.     Okamžite volám 0800123777 a volím 4. Studený hlas mi oznámi, že som sa, bohužiaľ, dovolala mimo ordinačných hodín, sú tam pre mňa od 8.00 do 22.00 a môžem nechať odkaz. V stave akútnej zlosti zapínam po štvrťroku televízor, aby som zistila absolútne presný čas. Je 22:00:40.     Štvrtý telefonát na 0800123456. Veľmi dúfam, že to zdvihne Diana. Pevná linka opäť na chvíľu vypadáva. Vytáčam piaty raz. A prekvapko. Naozaj som si myslela, že táto služba funguje nonstop ako t-mobilovská 12345. Figu drevenú. Studený hlas mi oznámi, že sú tam pre mňa od 8.00 do 22.00.     Zúrim. Diana nevedela, že o minútu má spolu s kolegami z centra internetovej podpory fajront? V tom prípade T-Com pokojne posadil k telefónu pre zákazníkov retardovaného človeka. No v spomienke na Gaussovu krivku sa obávam, že nepôjde vyslovene o debila, len o šedý priemer, ten najrozšírenejší, ktorý sa opiera o príslovečnú lopatu a má na háku. Kam si môže strčiť svoj príjemný večer, nemusím dodávať.     Vďaka T-Comu som bez internetu. Tento text odnesiem do práce na USB kľúči, ale čo keby šlo o večer (v mojom pracovnom živote typický), keď som sľúbila nejakú robotu? Veď na to si platím internet bez časového či dátového obmedzenia. Preto sme sa s T-Comom, ktorý odo mňa za všetko berie nejaké mizerné dve tisícky mesačne, dohodli, že nebudem bez internetu ani sekundu.     Dnes zruším celú akciu, budem sa sťažovať a absolútne odmietnem platiť za ten čas, keď mi internet neposkytovali. Hneď po uplynutí viazanosti sa rozhliadnem, čo iné je na trhu. Len neviem, kedy poskytovatelia všetkých možných služieb v tomto postkomunistickom raji darebákov pochopia, že ich vlastná existencia závisí od toho, ako často budú nasierať svojich zákazníkov.           Aktualizácia (11:02): Pred niekoľkými minútami mi zavolala na mobil madam z T-Comu, ktorá sa v reakcii na tento blog ospravedlnila za vzniknuté ťažkosti a prisľúbila, že problém bude dnes vyriešený. Síce nikto nevie, ako ťažkosti vznikli, ale prístup vraj funguje. Nemôžem si to teraz overiť, ale uvidíme. V každom prípade som sa úprimne poďakovala za ústretovosť a dám sa prekvapiť.     Aktualizácia (14:20): Firma T-Com mi ako cez telefón, tak prostredníctvom svojho pracovníka v reakciách pod článkom celkom vážne tvrdí, že som zakaždým zadala na koniec hesla medzeru. Má firma T-Com svojho zákazníka skutočne za úplného kreténa? No hesla, ktoré mi sľúbili zaslať esemeskou, som sa zatiaľ nedočkala.     Aktualizácia (polnoc): Rozlúsknutie bolo prostučké ako dve mačiatka v košíku. Šlo o zamenenú spoluhlásku v hesle. Stačilo by teda, keby Diana nebola hnilá heslo zopakovať.      Za väčší problém pokladám, že T-Com sprosto klamal, keď mi všetkými prostriedkami tvrdil, že som pridávala do hesla medzeru - zrejme sa nazdával, že si ho píšem rovno do počítača, a nie starosvetsky na papier, takže nebudem mať možnosť si obe heslá porovnať. Moje pokusy pripojiť sa naozaj prešetril - veľmi dobre vedel, že posledne som to skúšala o 04.31 ráno. Len si k tomu spôsobom hodným prváčika primyslel, že je to moja chyba. Hehe!  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (60)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nataša Holinová 
                                        
                                            Veselý manuál pre trúchliacich pozostalých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nataša Holinová 
                                        
                                            Nemáme kauzu? Tak si ju ukradneme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nataša Holinová 
                                        
                                            Ukážky okopírovaných textov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nataša Holinová 
                                        
                                            Darinka pod stolom: štát konal v jej najlepšom záujme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nataša Holinová 
                                        
                                            Ako vyrobiť týrané dieťa (praktický návod)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nataša Holinová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nataša Holinová
            
         
        holinova.blog.sme.sk (rss)
         
                        VIP
                             
     
         jazyčnica -e -níc ž. hovor. pletkárka pejor. jazyčnica, teraz aj na ešte lepšom blogu 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6078
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ňa nebudete nasírat furt
                        
                     
                                     
                        
                            Články publikované inde
                        
                     
                                     
                        
                            Jazykové oknoviny
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            V dôchodku nad krémešmi
                        
                     
                                     
                        
                            Pitominky furt
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ondřejíček: Homo asapiens
                                     
                                                                             
                                            Strasti anonymného admina
                                     
                                                                             
                                            Prečo Slovák nemá Nobelovku za literatúru
                                     
                                                                             
                                            Skvelý Földvári o jazykách a ľuďách
                                     
                                                                             
                                            O jazyku v médiách... pche
                                     
                                                                             
                                            Fakultná nemocnica roku Pána
                                     
                                                                             
                                            Nová jazyková poradňa
                                     
                                                                             
                                            Dialóg o jazykovom zákone
                                     
                                                                             
                                            Hlbší ponor do svinskej problematiky
                                     
                                                                             
                                            Respekt: Je načase ... nechat jazyk jeho přirozenému vývoji
                                     
                                                                             
                                            Marián Leško o jazykovom zákone
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            internety
                                     
                                                                             
                                            knihy
                                     
                                                                             
                                            časopisy
                                     
                                                                             
                                            noviny
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Doma najlepšie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Právo veta pre zriaďovateľa je zlý nápad
                     
                                                         
                       Polícia nechráni občanov ale podvodníkov.
                     
                                                         
                       Darinka pod stolom: štát konal v jej najlepšom záujme
                     
                                                         
                       Prokuratúra dala za pravdu „nepríčetnej“ ....
                     
                                                         
                       Podozrenia sa menia na dôkaz: Takto sa kšeftuje v Starom Meste
                     
                                                         
                       Priznanie na Súdnej rade - ide o naše deti!
                     
                                                         
                       TA3 vystrihla nepríjemné otázky novinárov na Fica
                     
                                                         
                       Žilinský boss Salinger: Ničomník, obeť či oboje? (1. časť)
                     
                                                         
                       Rumunskí podvodníci sú späť!
                     
                                                         
                       Poznámky na okraj k prekladom detskej literatúry (z pohľadu mamy)
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




