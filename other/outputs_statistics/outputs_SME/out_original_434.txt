




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 M?sli 
 
 Vydan? 23. 3. 2004 o 0:00 Autor: MIRIAM ZSILLEOV?
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 Maximili?n Bircher-Benner. 
 FOTO - ARCH?V 
 
   
 
 Pre niekoho je to len krmivo pre vt?ky, pre in?ho ra?ajky, bez ktor?ch sa nem??e za?a? de?. A bolo aj mnoho tak?ch, ktor? ich jedli na protest proti konzumnej spolo?nosti, odchovanej na m?se a tukoch. Jedol ich Thomas Mann a sympatie im preukazovali aj Mussolini ?i Hitler. Z jabl?n?ho di?tneho jedla je dnes svetov? pojem zdrav?ho stravovania a recept na mnoh? neduhy. ?vaj?iarsky objav sa vol? m?sli a v marci osl?vi svoju storo?nicu. 
 A mo?no s? m?sli ove?a star?ie. Ka?u z obilia, surovej stravy a mlieka jedli alpsk? pastieri d?vno predt?m, ne? in?pirovali mlad?ho ?vaj?iarskeho lek?ra Maximili?na Birchera-Bennera. A? on urobil z ka?e revolu?n? pokrm. 
 Bircher-Benner bol z?stancom surovej stravy. P? rokov sk??al poznatky na vlastnom tele, k?m si v roku 1891 ako 24-ro?n? otvoril prax v Z?richu. S novou di?tou mal prekvapuj?ce ?spechy. 
 Ke? sa mlad? lek?r v janu?ri 1900 postavil pred z?ri?sk? konz?lium lek?rov, aby im predstavil nov? stravovaciu terapiu, mal u? na Z?richbergu mal? kliniku. Pre u?encov to bola revolu?n? my?lienka. Civiliza?n? choroby sp?soben? aj nadmern?m po??van?m m?sa chcel Bircher-Benner lie?i? rastlinnou stravou a varen? jedl? nahradi? surov?mi. Na vtedaj?ie obdobie, ?o hygienu e?te len objavovali, to bola provok?cia. Prv? polen? hodili ?vaj?iarskemu nad?encovi zn?mi lek?ri Robert Koch a Louis Pasteur. Pre nich bolo dobr? jedlo len uvaren?. Medic?na vtedy ?ivotne d?le?it? funkcie vitam?nov, ?i stopov?ch prvkov nepoznala. 
 
 
 

 Bircher-Benner sa v?ak nevzdal. O ?tyri roky u? viedol ve?k? sanat?rium na Z?richbergu, kde sa zdravo jedlo, pri proced?rach sa vyu??vala studen? a tepl? voda, mas?e, lie?ebn? gymnastika. A ve?k? pozornos? sa venovala du?evn?mu rozpolo?eniu pacientov. 
 Z?kladom jeho te?rie bolo obmedzi? m?so, cukry a tuky a nahradi? ich zdravou surovou stravou. 
 Tak sa zrodilo jabl?n? di?tne jedlo. Recept bol jednoduch?: jedno a? dve nastr?han? jablk?, polievkov? ly?ica ovsen?ch vlo?iek, tri ly?ice vody, ??ava z polovice citr?na, a sladk? kondenzovan? mlieko. Nakoniec pridal Bircher-Benner ly?icu postr?han?ch orechov a ka?a bola hotov?. Zmes dostala n?zov m?sli o nie?o nesk?r. Bola to zdrobnenina od ?vaj?iarsko-nemeck?ho slova Mus a mala nahradi? ide?lnu stravu - matersk? mlieko. N?zov vraj vymysleli Bircherove deti. 
 Dnes u? je jasn?, ?e hodnota surovej stravy nie je, ako si to myslel Bircher, v jej energetickom obsahu, ale v mno?stve vitam?nov ?i stopov?ch prvkov, ktor? obsahuje. 
 To vtedy nevedel ani ich tvorca, ani jeho milovn?ci, hoci m?sli na Bircherovej klinike jedli aj pacienti tak?ho kalibru ako Thomas Mann. Revolu?n? sp?sob lie?by ho nadchol a Z?richberg pohotovo premenoval na Z?zra?n? vrch. Fascinovan? vegetari?nskou stravou boli v 30. rokoch aj taliansky vodca Mussolini ?i Adolf Hitler. Striedma, zdrav? strava pasovala do novej nemeckej lek?rskej vedy a k silnej nemeckej nadrase. A ke? Bircher-Benner oboch verejne pochv?lil za ich stravovacie n?vyky, dostal l?kav? ponuku. Mal sa sta? riadite?om Nemocnice Rudolfa Hessa. Odmietol a zostal v Z?richu. Nebol vraj pr?vr?encom fa?izmu, hoci jeho sympatie mu nesk?r na?trbili poves?. 
 Treba doda?, ?e nie v?etko bolo ide?lne a nie v?etci Bircherovi pacienti boli nad?en? z vtedaj??ch lie?ebn?ch met?d. Na klinike bol toti? zak?zan? alkohol, pri jedle sa muselo spr?vne pre??va?, predp?san? bol pohyb na ?erstvom vzduchu. Na mnoh?ch bolo toho zdravia pr?li? a z kliniky zdupkali. 
 M?sli pre?ili a mali mnoho hviezdnych obdob?. ?udia po nich ?asto siahali nielen ako po ?ahkom a zdravom pokrme. V kr?zov?ch spolo?ensk?ch ?asoch sa stala ka?a symbolom protestu proti konzumnej spolo?nosti. V revolu?nom roku 1968 ich ?as? spolo?nosti jedla, aby tak demon?trovali svoj n?vrat k pr?rode a prirodzen?mu. Pre in?ch boli m?sli v rovnakom ?ase synonymom nad?vky pre ekologick? hnutia a nedbanlivo oble?en?ch ?ud? s dlh?mi vlasmi a sand?lmi. 
 Dnes s? na trhu najrozli?nej?ie produkty podobn? m?sli, hoci s t?mi prap?vodn?mi nemaj? u? ve?a spolo?n?. M?sli ty?inky, hotov? m?sli a r?zne in? produkty s? dnes receptami na n?dorov? ochorenia, na zn?enie cholesterolu, zlep?enie tr?venia, ?i ako s??as? skrᚾovacej di?ty po viano?n?ch a ve?kono?n?ch sviatkoch. 
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 21:30  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v utorok st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
KOMENT?R PETRA SCHUTZA  
Len korup?n? ?kand?ly preferencie Smeru nepotopia
 
 Ak chce opoz?cia vo vo?b?ch zvrhn?? Smer, bez roz??renia reperto?ru si neporad?. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 23:50  
Taliban zabil v ?kole v Pakistane 132 det?
 
 Pakistan a? teraz poriadne zatla?il na islamistov. Odplatou je ?tok na deti vojakov. 
 
 
 
 
 




 
 

 
ZAHRANI?IE  
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi
 
 Na demon?tr?cii proti Orb?novej vl?de bolo v utorok u? len nieko?ko tis?c ?ud?. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






