

 
Amerika je obrovská krajina a v časoch Divokého západu chýbali školení policajti a tak vláda prehliadala prešlapy amatérskych šerifov, hlavne, že vládne aspoň aký - taký zákon. Do každého väčšieho mesta vymenovala vláda marshalla, ktorý dohliadal na všetko, čo by bolo namierené proti záujmom štátu. Záujmy občanov mal hájiť šerif. Keď som písal o Divokom Billovi alebo Calamity Jane ako rozporuplných osobnostiach, o Earpovi to potom platí desaťnásobne. Jeden deň spacifikuje niekoľko desperádov, neohrozene a priamo kráča v boji proti zlu, aby na druhý deň vyhlásil platenie miestnej dane a poplatky občanov si strčil do vlastného vrecka. 
Hollywood dodnes natáča úspešné westerny o Earpovi, stále je hrdinom románov a príbehov. A tam sa vždy objavuje na strane zákona, ako ochranca práva, pričom neváha nasadiť vlastný život v prospech ostatných. Filmové plátno či stránky kníh mávajú často ďaleko od reality a nie je tomu inak ani v prípade Wyatta Earpa či jeho pravej ruky Doca Hollidaya. Aké boli tieto dve legendy v skutočnosti?
 
 
 
 
 
Wyatt Earp sa narodil 19. marca 1848 v Illinois, kde sa usadili jeho rodičia. Už ako mladý sa túžil stať šerifom. Vo svojom živote bol síce len zástupcom šerifa, ale zastával mnoho iných funkcií. Bol pomocný maršál vo Wichite, Tombstone, Ellsworthe a Dodge City. Všetko to boli dobytkárske mestá, plné užívajúcich si kovbojov a banditov, kde bolo treba ochraňovať zákon. Nie vždy bol však Wyatt v policajných službách, napríklad v roku 1876 zachvátila aj jeho zlatá horúčka a vydal sa do Deadwoodu v Dakote skúsiť šťastie. V tejto tlačenici ale neuspel. A tak vyskúšal karty, kde sa mu darilo striedavo oblačno, no opäť si pripol hviezdu Maršála a aspoň naoko stál na strane zákona. V týchto aktivitách mu výdatne asistovali aj bratia Virgil a Morgan.
 
 

 
 

 
 
Často chodil v službe neozbrojený, strieľal len v prípade nutnosti a ak, tak natoľko bleskovo, že jeho protivníci nemali najmenšiu šancu. Väčšinou ich spacifikoval ručne a vyhnal z mesta. Tak napríklad zatočil s obávaným Clay Allisonom, ktorému vrazil revolver medzi rebrá a prinútil ho zdvihnúť ruky nad hlavu. Toto bola pre gangstra Allisonovho typu obrovská potupa a tak sa ešte tej noci nebadane odplížil z mesta. Earp počas svojho života zabil možno len niekoľko chlapov, mnoho však poslal k zemi tzv. „zbyvolovaciou technikou". Nechal protivníka - ktorý keď Earpa videl bez zbrane v ruke, tak ho podcenil - aby sa k nemu priblížil a potom ho poslal k zemi úderom koltu po hlave. Earp síce takto zlikvidoval mnoho zločincov, často však nerozlišoval zrno od pliev a mozoľ na hlave navrel nejednému nevinnému, no na druhej strane si nemohol dovoliť zaváhať a dostať guľku ako prvý. Preto jednal ako jednal. 
Earp ako zástupca šerifa vydal zoznam pravidel správania sa, ako napr. 
- je zakázané nosiť po meste zbraň 
- je zakázané vojsť na koni do obchodu 
- zvieratám sa zakazuje vstup na chodník 
Zástupca šerifa mal v tom čase žold 250 dolárov mesačne plus 2,50 dolára za chyteného zločinca. Wyatt sa preslávil hlavne bitkou pri O.K. Corrale (trvala 30 sekúnd a padlo pri nej 34 výstrelov), kde spolu s Hollidayom a bratmi Virgilom a Morganom zlikvidovali bandu zločincov pod vedením Ike Clantona, s ktorými spolupracoval aj šerif Behan. Šuškalo sa, že s Clantonovcami sa pobratal aj Holliday a teraz sa rozhodol zahladiť stopy. Ktovie.
 
 
   
 
 
Do života Wyatta Earpa veľmi negatívne zasiahla smrť jeho tehotnej manželky. Túto udalosť takmer nezvládol, opíjal sa, túlal a žobral. Na sklonku života sa Earp stal najiteľom naftových polí a veľmi sa stránil publicity, pretože rôzni bulvárni autori jeho životný príbeh značne prekrucovali. 
Napriek tomu, že Wyatt Earp si v živote narobil veľa nepriateľov, neumrel rukou ani jedného z nich. Zomrel na úsvite 13. januára 1929, vo svojom byte v Los Angeles, vo veku 81 rokov. Earp však nemal pokoj ani po smrti, v roku 1957 fanatický zberateľ vykradol jeho hrob v San Franciscu. Miesto jeho večného odpočinku tak zostalo utajené necelých 30 rokov.
 
 
  
 
 
Pozrime sa teraz v skratke na jeho nerozlučného priateľa. John Henry (Doc) Holliday sa narodil 14. augusta 1851 v Griffine v Georgii v rodine zámožného plantážnika. Vyštudol za zubára a v tejto profesii bol zručný a vyhľadávaný odborník, oveľa častejšie však ordinoval s revolverom v ruke alebo s kartami v herni. 
 
 
   
 
 
Doc hral v podstate so životom vabank, bol nevyliečiteľne chorý na tuberkulózu a tak častokrát s až príliš chladnou hlavou vyhľadával nebezpečenstvo. Ešte ako mladík, ktorý utiekol z domova (Doc bol Južan a zranil niekoľko černochov), na ceste do nového domova (Texas, kde si otvoril aj zubársku ordináciu) skúsil šťastie v kartách. Tam sa stretol s falošným hráčom, nechal ho tasiť ako prvého a nakoľko bol Doc rýchlejší, pribudol mu na revolver ďalší zárez. Samozrejme, ako každý „správny gunman" tej doby do tejto štatistiky nezahŕňal Indiánov, černochov a mexičanov. Holliday bol chorobne nedotklivý a táto vlastnosť sa mu takmer stala osudnou. Jedného dňa sa pri kartách urazil a zabil vojaka. Do deja zasiahla Docova milenka Kate Elderová, zvaná aj Big Nose Kate. Prezývku, tú mala ako každá správna „speváčka" tej doby, pričom zvykla nezriedka poskytovať služby v najstaršom remesle. Nosatá Katka kryla Docovi chrbát, zapálila hotel a vo vzniknutom všeobecnom zmatku hodila Docovi revolver, ktorý už vedel, čo s ním urobiť a komu prevŕtať hlavu.
 
 
   
 
 
Po tejto udalosti musel znovu utiecť a zastavil sa až v Denveri v Colorade. Doc sa neskôr pravdepodobne zmenil, vedel byť veselý a vtipný spoločník, bránil vraj slabších a zachránil Earpovi život. Ten mu to nikdy nezabudol a možno mu to vrátil aj s úrokmi. Týmito činmi sa pomaly ale isto život Doca Hollidaya chýlil ku svojmu očakávanému koncu. 8. novembra 1887 podľahol tuberkulóze vo veku 36 rokov. 
 

