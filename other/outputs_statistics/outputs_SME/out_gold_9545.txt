
  
  Myslím si, že aj preto môžeme pri tých krásnych chvíľach, aká sa nám naskytla aj dnes, prežiť krásnu, povedal by som až čistú radosť. Z vypočítavosti takú radosť nie je možné vydolovať. Všetci sme dnes opäť videli, ako tam naši nechali srdce, že dali do toho všetko a ešte niečo naviac. Že naše víťazstvo (ako si ho ľahko pripisujeme) je zaslúžené. Nemožno ich pochváliť za taktizovanie, veď práve zo zbytočného naháňania sa dopredu dostali prvý a možno aj druhý gól. Ale treba ich pochváliť za športového bojovného ducha, vďaka ktorému strelili aj tretí gól, ktorý sa ukázal byť víťazný. A to mi je omnoho sympatickejšie a nielen to. Je to tak správne. 
 Máme Filca a Weissa, ktorý ukázali státisícom mladým, že sa oplatí byť poctivým a oplatí sa popasovať zo súperom a nevzdávať sa. A to je cesta, ktorá vedie k úspechu nielen v športe. Slovensko do toho. 
  
