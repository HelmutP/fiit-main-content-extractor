
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Už trošku v inom garde, ale... 

        
            
                                    27.6.2010
            o
            15:15
                        |
            Karma článku:
                11.73
            |
            Prečítané 
            798-krát
                    
         
     
         
             

                 
                      súboj to nie je o nič menej dramatickejší a očakávanejší, lebo muž v pozícii premiéra krajiny a oproti nemu žena s proklamovaným odmietavým stanoviskom k jeho názorom, postupom a riešeniam vo forme minimálne nariadení, to je jednoducho grandiózny ťahák! Konštatovanie vo forme podmienení je tu akurát preto, že toto sme v premiére zažili už pred parlamentnými voľbami, malo to nielen očakávanie, ale aj publicitu a vyvolávalo úprimný záujem väčšiny, no nielen to, boli tu pokusy zo stretnutia Róberta Fica a Ivety Radičovej - lebo jedná sa akurát o tieto osobnosti – vyrobiť voľajaký hodnotený súboj o priazeň či o kvalitu argumentácie. Aj teraz tu boli také tie prekáračky, s kým sa bude Iveta Radičová vlastne rozprávať a sporiť a keď som sa včera podvečer dozvedel, že predsa len s Ficom, očakávanie sa zmenilo na nedočkavosť. Som si istý, že premiér už nikomu nezvestuje, ako sa na tento rozhovor nepripravuje, naopak, celkom iste sa sústredí, lebo intelekt jeho súperky, program a argumenty s ktorými disponuje ho varuje, aby ani náhodou na nič nezabudol!
                 

                     Len škoda, že priestorom stretnutia je zasa STV, ten prívlastok verejnoprávna televízia už vopred signalizuje kto je v tomto priestore doma a kto hosťom a aj keď je to prevzatá športová terminológia, skúsenosti hovoria, že aj tak reálna! Pre premiéra a „jeho kruhy“ všetko, to už predviedli toľkokrát a v priamom prenose, že s tým sa nedá robiť vôbec nič. Pochopte však všetci, že na iné „ihrisko“ premiér nevstúpi, lebo novinári, redaktori a zodpovední za mediálnu politiku vôbec sú...., spomínajte, tých prívlastkov už povedal toľko, že sú doslova encyklopedické a tak sa musela prispôsobiť zasa žena, čo naozaj nie je gentlemanské! No nič...   Radovan Slobodník svojich hostí uviedol ako odchádzajúceho premiéra a dezignovanú premiérku budúceho času a na počudovanie, úvodné slovo dostala žena. Poďakovala všetkým voličom, poblahoželala Ficovi k volebným výsledkom a pridala sľub, že sa pousilujú byť vládou pre občana a všetkých prosí o pochopenie, že niektoré dané sľuby sa nenaplnia hneď. Takmer rovnakým štýlom chcel začať svoje repliky aj premiér Fico, no nedarilo samu zotrvať v línii korektnosti. Potreba dokazovať to svoje, čo aj tak nie je reálne sa prejavila nielen v tom, že pripomenul individuálne víťazstvo strany SMER a okamžite pripomínal kauzy predvolebné i povolebné, ktoré sú namierené proti nim. Falšovanie jeho podpisu na materiáloch SDĽ a potom kauzu Matovič! Ale ako najväčší generálny podraz na voličoch uviedol fakt, že programové tézy nastupujúcej vlády neobsahujú nič z toho, čo strany pred voľbami sľubovali! Veď uvidíte, dodal, teraz sa o tom bude rokovať v parlamente a pre vás sa počty poslancov pri hlasovaní stanú poriadnym mementom. Odmietli ste s nami rokovať, hoci čo len formálne a volali sme vás, takže nič ústretovejšie od nás nečakajte ani vy!   Krátka reakcia Ivety Radičovej pripomenula premiérovi i auditóriu v tejto republike, že nikto nič neodmietol, lebo ona sama a osobne odovzdala strane SMER písomné stanovisko, že dohoda stredopravicových strán je nemenná a vládu v zmysle dosiahnutých volebných výsledkov sa dohodli vytvoriť vo vlastnej réžii, čo je korektné a jednoznačné i kultúrne riešenia kauzy. Pre premiéra slabý argument a oznámil Radičovej, že on teda výsledky volieb akceptuje, je mu jasné že príde k zmene moci a príde pravica, ale sa zaručuje, že premiérsky post jej on odovzdá osobne a nie upratovačka, ako sa to stalo jemu. A bude tam aj s pohárom šampanského!   A celkom prirodzene sa akurát v tejto chvíli rozhovoril o dobrom stave Slovenska, o dobrej kondícii, druhom najmenšom dlhu o pohode vo financiách štátu, ale to už Radičová nevydržala a zareagovala celkom razantne. Pán premiér, ale nezavádzajte! Počiatek to síce dlho tajil, až po voľbách našiel odvahu hovoriť o tých takmer 7% deficitu rozpočtu, ale aspoň dodal, že sa musí veľmi šetriť a to je jeho odkaz nástupcom! Tak ako je to vlastne?   Odpoveď svedčila o tom, že tentoraz sa Fico pripravil. My sme presne toto očakávali, aj tak je to dobré v porovnaní s inými a nech pravicová vláda ukáže, čo vie a čo dokáže! A my si ako víťazi volieb nárokujeme miesto predsedu parlamentu, čo je celkom normálne a ak od nás niečo v smere podpory voľačo očakávate... nuž viac premiér nepovedal, všetko doznelo tak akosi v trápnej pomlčke.   Tentoraz ho však pred kamerami nesprevádzala jemná útlocitná osôbka, ale razantná rétorka a pekne mu vystavila účet aj s percentami „dépéhá“, ako sa hovorí, keď má byť komplexný. Dozvedel sa, ako je to s kondíciou Slovenska naozaj, uviedla, aká je nezamestnanosť a klasifikovala odvodové nedostatky v daňovníctve ako hlavný problém aj v samosprávnej sfére, kde je už väčšina miest a obcí pred kolapsom a Počiatek jednoducho veci priznal. A nesúhlasila ani s tvrdením, že strany čo zostavujú vládu nemajú žiadnu zhodu v programe, že zavádzali svojich voličov a povedal by som, že ju to rozčertilo. Reagovala prudko, zopakovala, že zhoda v tom najhlavnejšom je pevná ako skala, svojim voličom sa vo všetkom zodpovedne vyznávajú a vedia, že boj proti korupcii, proti okrádaniu štátu a klientelizmu, za poriadok v súdnictve a za zvýšenie pracovných príležitostí je prvoradou povinnosťou a na nič sa nezabúda. Všetko sa bude riešiť postupne a tak ako sa otvoria možnosti sa vláda ku všetkému vráti.   A Fico vyrukoval s novým argumentom – podpora Lisabonskej zmluvy, prečo ste sa zachovali akurát takto? A nereagovali ste ani na môj návrh, aby sme rokovali o zostavovaní vlády, to je poriadna chyba...Odmietli ste moju ponuku a... A ešte niečo! Je tu ultimátum EÚ ohľadne nášho podpisu pod euroval, ako nazývame projekt na záchranu eura a ak ho do 1. júla Počiatek nepodpíše, tak nám odoprú akúkoľvek solidárnosť a to už je nielen hanba, ale akútne nebezpečie. Jasné, to už zaznelo ako vyhrážka, hoci Radičová už celú diskusiu jasne ovládala! Pokojne zopakovala svoje stanovisko o dohode štyroch strán a pridala informáciu, ktorá tak trošku nielen Ficovi vzala dych!   S pánom prezidentom Barosom som mala dlhý telefonický rozhovor, je informovaný, pozná všetky podrobnosti v akých sa Slovensko nachádza a ubezpečil ma, že všetkému rozumie. Za seba sľubujem, že v novom parlamente to príde na program okamžite a pripomínam, že to už dávno mohol vyriešiť aj ten starý, s vašou vládou pán premiér, to stačilo prijať náš návrh, aby sa o tom rokovalo! Keď ste nepotrebovali náš súhlas a ten parlamentný už vôbec nie na dokument o riešení gréckeho dlhu, mohli ste sa pokojne zaobísť bez neho aj v kauze záchrana eura! To už však bolo príliš veľké sústo a hoci ste stále šéfom našej vlády, už by vám k tomu pasoval aj náš súhlas. Pravda?! Ale nebojte sa, dohoda s EÚ sa vyrieši právoplatne v najbližšom možnom termíne našej moci, vládnej a verím aj zákonodarnej, lebo verím, že budete hlasovať v súlade so svojim presvedčením!   Evidentne zneistený premiér sa zmohol iba na opakovanie výhrad, vraj ak nie je súhlas budúcej vlády, Počiatek nič nepodpíše a dostal odpoveď v poriadku, my si to dohodneme vo vlastnej réžii, Brusel všetko vie a veciam rozumie. A znova to pokojné, pán premiér nezavádzajte, neklamte, vy ste tie podmienky nikdy nikomu nevysvetlil, nikto o ich obsahu nič netušil, no v tejto chvíli už EÚ naše stanovisko pozná!   A potom už to stratilo hodnotu názorovej rôznosti, reč sa zvrtla na problematiku zástrčkového zákona a teda zrušenia koncesionárskych poplatkov, nároky na post predsedu parlamentu, zasa reč o vládnom programovom vyhlásení a zavádzaní voličov s rovnako razantným odmietnutím a tak trošku do Radičovej, zrejme ako pomoc dosť znepokojenému premiérovi – zabŕdol aj moderátor. A čo vláda, v pondelok bude!? Dočkal sa, my skladáme vládu od stredy, pán premiér skoro tri týždne, tak sa niet kam ponáhľať, ale bude to v bude to v zhode. A vtedy sa stala udalosť všetky informácie o duševnej pohode aktérov dosvedčujúca. Pán premiér pripomenul, že on tie voľby aj tak vyhral!   No a predstavte si nasledovalo finále so širokorozchodnou, ktorú premiér obhajoval, Radičová odmietala a bodka za všetkým. Pani Radičová, sľub voličom neplníte, váš program je prázdny, nie je tam nič, čo ste sľúbili. A keď Radičová zareagovala obratom že nesúhlasí, ale pripomína to jeho o dani milionárov, o dividendách o náhrade škôd spôsobených.... Slobodník zakročil! Ďakujem za účasť a bol koniec! To, čo prišlo potom, ako glosa k povedanému a videnému, v prevedení Petra Horvátha, sociológa a Juraja Marušiaka z Ústavu politiky SAV už bolo celkom nepodstatné. Kto videl a počul, toho už žiadne glosovanie ovplyvniť aj tak nedokáže a v tomto prípade všetko bolo celkom jasné. Tie voľby dopadli naozaj dobre a nech nám to vydrží!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




