

 Výsledok volieb 
 Ak vychádzame z prieskumov verejnej mienky, môžeme predpokladať bezkonkurenčné víťazstvo politickej strany Smer - sociálna demokracia, ktorá dlhodobo dominuje v týchto prieskumoch, pričom nemá relevantného súpera, ktorý by sa k nemu len trochu mohol priblížiť. Preto je možné očakávať, že to bude práve Smer, resp. jeho predseda, ktorý bude poverený rokovaniami o zostavení vlády. 
 Ďalej sa do NR SR dostanú asi dve pravicové politické strany SDKÚ - DS, KDH, ktorých rozdiel sa stále zmenšuje v neprospech SDKÚ - DS. Je možné počítať s SNS, ktorá má stabilný elektorát, ktorý jej zabezpečí pohodlný vstup do NR SR. 
 Otázne však je pre ĽS - HZDS prekročenie 5-percentnej hranice. Niektoré prieskumy avizujú, že sa tak nestane, čo môže súvisieť s tým, že respondenti sa nepriznávajú, že ju budú túto stranu voliť. Preto pravdepodobne získa ďalšie hlasy, s ktorými tieto prieskumy nepočítajú a tesne prekĺzne na hranicu zvoliteľnosti. 
 Podstatný je vplyv týchto prieskumov na voliča, ktorý môže byť dvojaký. Buď odradia voliča, ktorý sa tak rozhodne pre inú stranu, pretože by to mohol byť v jeho očiach stratený hlas, alebo to ešte viac mobilizuje týchto voličov, aby ich strana dosiahla potrebnú hranicu zvoliteľnosti. 
 Situácia okolo SMK a Mostu je ešte zložitejšia. Treba si uvedomiť, že volebný základňa zjednotenej SMK nebola natoľko naviazaná na osobu svojho lídra Bélu Bugára, ako je to napr. pri Smere alebo ĽS - HZDS. Konzervatívny maďarský volič preto zostane asi tradične verný značke SMK, ktorá sa mu osvedčila a nevyskúša neistý experiment zvaný Most. 
 Sloboda a solidarita je mediálna strana, ktorá ako nový subjekt na politickej scéne ešte nemá stabilizovaný elektorát, preto sú jej výsledky v prieskumoch skôr preexponované ako reálne a jej vstup do NR SR sa nepredpokladá. 
   
 Povolebné perspektívy 
 Ak vychádzame z predchádzajúcich odhadov, dá sa očakávať, že predseda Smeru Robert Fico bude poverený rokovaniami o zostavení vlády. Otázka však je, či si bude mať z koho vyberať, resp. kto s ním pôjde vo vlády. Predchádzajúci odhad predpokladá účasť šiestich politických strán v NR SR: 
 -         dominantný Smer - sociálna demokracia, 
 -         3 až 4 strednej veľké strany: SDKÚ - DS, KDH, SNS, (SMK), 
 -         1 až 2 malé strany: ĽS - HZDS, (SMK). 
 V prípade SMK jej zaradenie medzi druhú alebo tretiu skupiny závisí od množstva hlasov, o ktoré ju pripraví Most. 
 Pri hľadaní koalícií je možné postupovať vylučovacou metódou pri tých dvojiciach politických strán, u ktorých sa ich spolupráca vo vládnej koalícii javí ako málo pravdepodobná. Medzi ne patrí dvojica Smer - SD a SDKÚ - DS vzhľadom k nezmieriteľnému antagonizmu a zásadným programovým a ideologickým disproporciám medzi nimi. Analogicky je možné uvažovať o spolupráci SNS a SMK, ktorých účasť vo vláde je ešte menej pravdepodobná ako u predchádzajúcej dvojice. 
 Smer - SD, ĽS - HZDS sa javí ako jedna z pravdepodobných variantov. Otázne však je či takáto možnosť by získala väčšinu v parlamente. Hoci by to bolo pre Smer - SD optimálne riešenie, pochádza skôr z ríše snov ako z politickej praxe, pretože preferencie Smeru - SD dosahujúce hodnoty okolo 40 % sú pravdepodobne nadsadené. Preto si treba položiť otázku, s ktorou stranou by išli do koalície. Pokračovanie súčasnej koalície s SNS sa zdá byť vzhľadom na signály z médií, ktoré svedčia o vzájomných vzťahoch týchto politických strán, ako menej pravdepodobné. KDH sa takisto nejaví ako nevesta, ktorý by mala záujem o takúto konšteláciu a SMK už vôbec nie. 
 Inou možnosťou je Smer - SD v spojení s KDH, ktorým by mohlo sekundovať SMK. 
 Inou alternatívou je menšinová vláda Smeru - SD s niekým. Ak by sa nepodarila, tak prichádza v úvahu menšinová vláda pravicových strán SDKÚ-DS, KDH a SMK. Obe možnosti menšinových vlád sú však málo pravdepodobné. 
   
 Implikácie volebných koalícií 
 V prípade účasti politickej strany Smer - SD ako dominantného hráča vo vládnej koalícii bude takáto vláda fungovať bez vážnejších problémov, kde Smer obsadí kľúčové posty a bude môcť realizovať svoje zámery vo zvýšenej miere ako zvyšní menší alebo malí koaliční partneri, ale zároveň preberie aj najväčší podiel zodpovednosti za vládnutie. 
 Účasť SMK v takejto vládnej koalícii by vylepšilo kredit Smeru v zahraniční. Spojenie s KDH by viedlo k ďalšiemu „zakonzervovaniu" niektorých kontroverzných tém, ktoré sú príznačné pre ľavicové strany v západnej Európe. Navyše by sa tým akcentovala národná orientácia vlády. 
 Spomínané menšinové koalície alebo široké vládne koalície by znamenali veľké kompromisy za cenu ústupkov a nestabilitu vládnej koalície s možnosťou vyústenia do predčasných volieb. 
   

