
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Bogric
                                        &gt;
                História
                     
                 17. rok vojny - 13. časť - Návrat Aténčanov do Katany a 

        
            
                                    18.6.2010
            o
            9:30
                        (upravené
                18.6.2010
                o
                9:14)
                        |
            Karma článku:
                4.08
            |
            Prečítané 
            387-krát
                    
         
     
         
             

                 
                    rozhodnutie Syrakúzanov. V predchádzajúcej časti som vám opísal ako dopadol 1. pozemský konflikt medzi Aténčanmi a Syrakúzanmi. Aténčania zvíťazili, ale bolo to víťazstvo na vážkach. Ak ste pozorne čítali, tak viete, čo bolo príčinou porážky syrakúzskeho vojska. Nedostatok spánku, nedisciplinovanosť, slabé bojové skúsenosti a ešte jeden aspekt, na ktorý neskôr poukáže Hermokrates- priveľa veliteľov. Zatiaľ čo na strane aténskeho vojska boli tieto aspekty- odpočinutí vojaci, disciplinované a skúsené vojsko i skúsení dvaja velitelia. Na druhej strane Syrakúzanov od úplnej porážky zachránilo to, že bojovali s plným nasadením a veľmi im pomohla ich jazda, zatiaľ čo Aténčania kvôli nedostatku vlastnej jazdy nedosiahli úplné víťazstvo, aké by mohli dosiahnúť. Túto stať Tukydides vo svojej knihe ukončil tým, že Syrakúzania poslali oddiel na ochranu Olympeie... a tam aj dnes budeme pokračovať.
                 

                     "Aténčania však nešli do svätyne, ale pozbierali mŕtvoly svojich padlých, pokládli ich na hranicu a strávili noc na bojovom poli. Na druhý deň, podľa dohody o prímerí, dovolili Syrakúzanom, aby pozbierali mŕtvoly svojich padlých. Na strane Syrakúzanov padlo asi 250 vojakov, straty Aténčanov boli asi 50 mužov. Aténčania zobrali výstroj a výzbroj z padlých nepriateľov a odplávali do Katany. Keďže bola zima a nemohli v týchto miestach viesť vojnu, poslali do Atén po jazdu a zhromaždili všetkých spojencov z oblasti Sicílie, aby si s nimi syrakúzska jazda len tak ľahko neporadila. Chceli aj nazhromaždiť peniaze zo Sicílie a čakali ich aj z Atén, aby si na svoju stranu získali niektoré sicílske mestá, o ktorých boli presvedčení, že sa im takto ochotnejšie podriadia. Nakoniec si zhromažďovali aj potraviny, aby ich mali dosť, keby na jar opäť začali robiť výpady proti Syrakúzanom.   Z týchto príčin sa Aténčania vrátili do Naxu a do Katany, aby tam prezimovali. Syrakúzania pochovali svojich padlých a potom zvolali zhromaždenie ľudu. Na zhromaždení vystúpil Hermokrates, muž ktorý bol práve taký rozumný, aké mal vojnové skúsenosti a schopnosti statočného veliteľa. Sám začal povzbudzovať ľud, aby nepodliehal beznádeji, že boli porazení. Veď ich bojové odhodlanie nebolo zlomené, ich nešťastie vyplynulo z nedostatočnej bojovej disciplíny. Napokon, povedal, ich porážka nebola až taká hrozná a musia brať do úvahy, že podľahli pre svoju neskúsenosť v boji, ktorí sa museli stretnúť v boji s najskúsenejšími Grékmi. Druhá príčina ich porážky bola v tom, že mali veľa veliteľov ( až 15 stratégov), čo pri nejednotnom velení viedlo k neporiadku a k anarchii. Keby si zvolili menej stratégov, ale skúsených a keby ešte cez túto zimu cvičili vojsko zložené z hoplitov, keby zadovážili zbrane vojakom, ktorí ich nemajú, vtedy- ako povedal- by pravdepodobne porazili nepriateľov, lebo k chrabrosti, ktorá je pre nich typická sa môže pridať aj zmysel pre vojenskú disciplínu a výcvik. Tieto prednosti sa budú navzájom upevňovať, lebo disciplína sa zväčšuje vo vojnových nebezpečenstvách a statočnosť, ktorá je prirodzene daná, bude sa znásobovať súčasne s dôverou v nadobudnuté skúsenosti získané výcvikom. Preto treba zvoliť iba niekoľko veliteľov s neobmedzenou právomocou a vojakov zaviazať prísahou, že sa nechajú viesť podľa najlepšieho svedomia stratégov. Takto budú môcť utajiť všetky vojenské tajomstvá a všetko ostatné si pripravia v poriadku a v slušnosti.   Keď si to Syrakúzania vypočuli, rozhodli sa prijať všetky jeho návrhy a zvolili si troch stratégov: samého Hermokrata, Herakleida, Lysimachovho syna a Sikana, syna Exekestovho. Vyslali aj poslov do Korintu a Lacedemonu žiadať spojencov o pomoc, aby s ich pomocou začali rozhodne vyhlásenú vojnu proti Aténčanom a tak ich donútili odísť zo Sicílie, alebo aby im aspoň zabránili posielať ďalšie posily na Sicíliu.   Aténske loďstvo medzi tým odplávalo z Katany do Messeny, ktorej sa chceli zmocniť zradou. Ale tento zámer im nevyšiel, lebo len čo Alkibiades dostal rozkaz vrátiť sa do Atén, vedel, že bude odsúdený na smrť a preto oznámil Messenčanom, priateľom Syrakúz, všetko, čo sa malo udiať. Messenčania bez prieťahov uväznili a usmrtili sprisahancov, ktorí ich chceli zradiť a keď sa Aténčania priblížili k mestu, Messenčania sa chopili zbraní, aby nedovolili Aténčanom vstúpiť do mesta. Aténčania tam čakali 13 dní, ale keď ich začalo trápiť nepriaznivé počasie a nedostatok potravín, bez akéhokoľvek úspechu odtiahli do Naxu, kde sa utáborili v opevnenom tábore, aby tam prezimovali. Zároveň do Atén poslali loď, aby im do jari poslali jazdu a peniaze."   Syrakúzy sa z tejto porážky veľmi rýchlo spamätajú a ešte ak na dôvažok, príde zo Sparty Gyllipos a zaúraduje tam Alkibiades, tak Aténčania budú na tom neveľmi dobre. Ešte vyhrajú niekoľko potyčiek ako na súši, tak aj na mori, ale budú to iba také strety, v ktorých sa zocelí vojsko Syrakúzanov a zdokonalia a zladia sa velitelia. V ďalších častiach vám vďaka Tukydidovi a jeho Historiai opíšem, ako sa budú snažiť Syrakúzania a Aténčania dostať každý na svoju stranu mesto Kamarinu.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            23. rok vojny- 1. časť- Alkibiadove vojenské výpravy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Xenofón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            22. rok vojny- Úspechy a porážky aténskeho vojska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            21. rok vojny- kapitola č. 2- Politika Farnabaza, osud Syrakúzanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Kapitola č. 1 – Pokračovanie Tukydida
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Bogric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Bogric
            
         
        bogric.blog.sme.sk (rss)
         
                                     
     
        Kto nepozná minulosť, nepochopí súčastnosť a nebude vedieť, čo ho môže čakať v budúcnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    138
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    722
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vyhoďme do vzduchu parlament!
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Starosta je vinný a čo tí druhí
                     
                                                         
                       Nehoráznosť a zvrátenosť
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Naši zákonodarcovia
                     
                                                         
                       Karel Kryl – nekompromisný demokrat aj po revolúcii
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Závisť aj po dvoch rokoch
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




