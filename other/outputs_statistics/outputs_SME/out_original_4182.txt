
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marian Baran
                                        &gt;
                Odpočuté
                     
                 Môžeš ma, ja budem teba 

        
            
                                    8.4.2010
            o
            18:59
                        |
            Karma článku:
                7.55
            |
            Prečítané 
            1181-krát
                    
         
     
         
             

                 
                    Konáre stromu, ktoré na zemi tieňom kreslili čiary, už zhrubli. Listy sa už cez puky predrali nasvetlo. Staršia pani v zástere naprávala na strome vtáčiu búdku. Pozrela dnu, či tam nemusí dosypať zrno z vrecúška, ktoré držala v ruke.
                 

                 Vtáky mali do čoho zobnúť. Preto pozrela na kvety, ktoré mala posadené v úhľadných riadkoch, hneď vedľa okrasných kríkov. Postavila sa vedľa kríku ruží, prižmúrila oko, aby tú líniu skontrolovala. Jar a ona má opäť toľko práce vo svojej záhradke. Pre niečo popínavé, čo ešte len začína klíčiť v zemi napichala palice. Fľaše z číreho plastu prikrývali malé výrastky rastlín, chránili a zároveň zvyšovali teplotu. Lahodilo to oku, všetko, i to čo v jej kráľovstve sa ešte len chystalo vzklíčiť, zapučať, preraziť hlinu a drať sa za svetlom do neba. Priestor plný tajomstva, ktoré poschovávala do zeme. Na strome zaspieval vták. Zafúkal vánok a prekrížil tiene na zemi do nových ornamentov. Pred očami jej preletel celofán z cigariet. Zdvihla ho a pozrela hore na okná dvanásť poschodového paneláku.   Vídavam ju tam často, hlavne teraz, keď sa oteplilo. Keď skončí, zoberie metlu a pozametá z chodníka neviditeľné smeti, a hlinu. V tej zástere tam potom bude stáť a každého, kto prejde okolo pozdraví. Tak trochu pyšná, dokonale skromne.   Nedokážem odhadnúť, ako dlho trvá, kým niečo také človek vytvorí. No je v tom veľa slnka. A ešte niečo. Okolo celej záhrady jej pribudli kamene, veľké ako futbalové lopty, vzdialené od seba s matematickou presnosťou. A ja premýšľam, ako ich tam tá útla žena dostala.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Baran 
                                        
                                            Kým si spala
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Baran 
                                        
                                            Taška
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Baran 
                                        
                                            Kabelka obyčajného rána
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Baran 
                                        
                                            Odtiahnuť sporák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Baran 
                                        
                                            Ginko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marian Baran
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marian Baran
            
         
        marian.blog.sme.sk (rss)
         
                        VIP
                             
     
        mám rád červenú farbu a na raňajky pohár mlieka 
















        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    487
                
                
                    Celková karma
                    
                                                15.82
                    
                
                
                    Priemerná čítanosť
                    2246
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Osobné
                        
                     
                                     
                        
                            Odpočuté
                        
                     
                                     
                        
                            Odkazy
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            ODPORÚČAM
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




