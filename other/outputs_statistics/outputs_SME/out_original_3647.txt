
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Bastl
                                        &gt;
                nezaradené
                     
                 Detstvo na dedine 

        
            
                                    31.3.2010
            o
            17:00
                        (upravené
                31.3.2010
                o
                9:12)
                        |
            Karma článku:
                6.97
            |
            Prečítané 
            1047-krát
                    
         
     
         
             

                 
                    S rodičmi a so súrodencami sme bývali v rodinnom domčeku v malej dedinke v stredných Čechách. Vždy na Veľkonočné sviatky, na Zelený štvrtok, Veľký piatok a Bielu sobotu sme chodievali s väčšou partiou chlapcov rozdelenou na dve skupiny rapkať po dedine. Používali sme rôzne, väčšinou po domácky vyrobené, rapkáče. Kto mal hlasnejší rapkáč, bol najlepší a väčšinou robil šéfa partie.
                 

                 
  
   Aby sme sa ráno načas zišli, nespávali sme doma, ale u gazdu v stodole na sene. Gazda nás často chodil kontrolovať, či nerobíme zle, či sa napríklad nehráme so zápalkami, čo sme mali vyslovene zakázané. Medzi rapkaním sme chodievali k potoku rezať vŕbové prútiky. Z nich sme sa spoločne jeden od druhého učili spletať korbáče, aby sme nimi mohli na Veľ­konočný pondelok šibať dievčatá. Kto vedel upliesť korbáč z väčšieho množstva prútikov, vy­slú­žil si obdiv ostatných. Na Bielu sobotu sme chodievali po domoch po výslužku za rap­ka­nie, pričom sme spievali náboženské piesne. Potom sme si v stodole spravodlivo po­de­lili výslužku, či už to boli peniaze, varené vajíčka alebo iné dobroty, ktoré sme dostali od ľudí.   Počas letných prázdnin tiež bývalo veselo. Chodievali sme sa kúpať do veľkého rybníka. Spomínam si, ako som sa učil plávať. Moja staršia sestra ma držala za ruky a pomaly sme postupovali do hlbokej vody. Poskakoval som vo vode po špičkách nôh, aby sa mi nenabralo do úst. Stávalo sa ale, že som stúpil do jamy a zrazu som sa ocitol pod vodou. Väčšinou to skončilo tak, že som sa dobre napil a veľakrát aj dusil.      Moji starší kamaráti sa rozhodli pre radikálnejší spôsob plaveckého výcviku. Zavolali ma k stavidlu, lebo tam bola voda najhlbšia. Chytili ma za ruky a za nohy a hodili do vody čo naj­ďa­lej od brehu. Nezostávalo mi nič iné, ako robiť rukami nohami pohyby, ktorými som sa udržal na vode. Keď zbadali, že už nevládzem, skočili do vody a vytiahli ma na breh. Po viacerých takýchto pokusoch som prišiel na to, ako sa udržať na vode a potom som sa už len zdokonaľoval v plávaní. Dnes si myslím, že to bol v tom čase najlacnejší a najúčinnejší spôsob, ako sa naučiť plávať, ale najmä, ako sa nebáť vody.      Mal som veľa kamarátov, s ktorými sme chodili vždy na jeseň k rybníku chytať ryby. Boli to ryby, ktoré po vypustení a výlove rybníka uviazli v mlákach plných bahna. Väčšinou boli ďalej od brehu a bezmocne sa trepali na hladine mláky. Ryby sme lovili tak, že sme si doniesli dlhšiu a širšiu dosku, položili sme ju na bahno a po nej sme prešli k mláke s rybami. Na koniec palice sme uviazali slučku z tenkého drôtu, do ktorej sme rybu chytili, pritiahli k sebe a vyhodili kamarátom, ktorí boli na brehu      Bol som spomedzi kamarátov najmladší, najmenší a najľahší, takže pri takomto spôsobe lo­vu rýb som bol veľmi platným členom skupiny. Kamaráti totiž predpokladali, že keď sa zaborím do bahna, tak ma skôr a ľahšie vytiahnu. Často som mal problém udržať rovnováhu na labilnej doske. Raz sa mi stalo, že som spadol do bahna, ktoré bolo hlboké po moje ramena. V hustom bahne sa nedalo plávať, cítil som, ako klesám, ale dna som nohami nedo­sia­hol.   Tak som spanikáril, že som klesal ku dnu skôr po hlave ako nohami, pričom som mal bahno všade, ešte aj v ústach. Našťastie jeden kamarát mal so sebou šnúru na vešanie bielizne. Hodil ju smerom na mňa a ako som mlátil rukami okolo seba, zachytil som sa o ňu. Potom ma pomaly pritiahli po šmykľavom bahne k brehu. V ten deň sa pre mňa rybačka skončila.  Premočený a špinavý som sa vrátil domov. Pochvalu od rodičov som, prirodzene, nečakal. V tom lepšom prípade na mňa len nakričali. Napriek tomu rád na tieto príhody zo svojho detstva spomínam. Boli to dni plné zábavy a skutočného kamarátstva.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Cestný pirát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Radosť z úrody
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Aj seniori to dokážu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Moje prvé pristátie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Bastl 
                                        
                                            Návrat do detstva
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Bastl
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Bastl
            
         
        bastl.blog.sme.sk (rss)
         
                                     
     
         Vyrastal som vo viacčlennej rodine v malej dedinke neďaleko Mladej Boleslavy. Vyučil som sa najskôr za baníka, neskôr za automechanika. Vojenskú základnú službu som vykonával neďaleko Bratislavy. Tu som sa aj oženil a založil svoju rodinu. Pracoval som na viacerých postoch vrátane verejnej funkcie. Najdlhšie, takmer tridsať rokov, som pracoval v Československom neskôr v Slovenskom rozhlase ako vedúci dopravný referent. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1404
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            súkromná
                        
                     
                                     
                        
                            nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       3. Na pohraničnej rote
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




