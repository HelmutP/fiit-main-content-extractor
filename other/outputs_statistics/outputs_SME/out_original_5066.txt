
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Vrabec
                                        &gt;
                Láska
                     
                 Boj 

        
            
                                    22.4.2010
            o
            20:50
                        (upravené
                26.3.2011
                o
                10:01)
                        |
            Karma článku:
                3.35
            |
            Prečítané 
            419-krát
                    
         
     
         
             

                 
                    Keď ju zazrel prvýkrát, všetko naokolo prestalo pre neho existovať. Myslel, že vidí prelud, bál sa dýchať, nie - on prestal dýchať, len aby ju svojim hlasným dychom neodplašil, aby sa nerozplynula. Pri jeho prekliatej plachosti sa mu stalo, ako už veľakrát, že nedokázal hľadieť priamo na ňu, bočil pohľadom, no nikdy dlho nevydržal a neustále sa vracal k objektu svojho záujmu. Uff! - pozrela na neho! Rýchlo odvrátil zrak a zahanbene pocítil, že aj za ten krátky okamih mu videla až na dno duše. Nesmie sa tým smerom už pozrieť! Ale ako silný magnet pritiahne kov, tak aj jej prítomnosť zmagnetizovala i tie posledné myšlienky, ktoré si pri tých život vracajúcich obrazoch dokázali v jeho v hlave zachovať akú - takú triezvosť. Musí sa na ňu znova pozrieť! No tentoraz jeho oči začreli do prázdna .... odišla .... pocítil beznádej, akoby ho prigniavilo lúčenie, rozchod, strata ... všetko zliate do jednej hrče, ktorá mu nie a nie prejsť dolu hrdlom.
                 

                 Postupom času mu v podstate aj odľahlo, najmä pri predstave, čo by spravil, keby na neho nedajbože prehovorila? On, nespoločenský, nezáživný, neatraktívny ... asi by mal pocit, že sa mu celý svet vysmieva. Kde by sa jej on mohol rovnať! Nemal na mysli fyzickú krásu, nie, nebola vyslovene krásavica, ani to nebol ten modelkovsko-čajočkový typ .... niekto by povedal - tuctová žena. Pre neho však mal jej vzhľad aj tak druhoradý význam. Od detstva bol veľmi empatický, cítil to, čo iní ľudia už dávno cítiť zabudli. Za tú chvíľu, kým sa stratila, stihol aj tými občasno-blúdivými pohľadmi uzrieť veľmi veľa. Prechádzajúc od vonkajšieho výzoru, od hlbokých, uhrančivo tmavých očí, cez havranie, v kaskádovitých vlnách poniže pliec padajúce vlasy, dolu úzkym hrdlom, naisto vyludzujúcim tie najjemnejšie zvuky, vnikol kĺzavým pohľadom do jej vnútra. A to čo videl, predčilo jeho najtúžobnejšie očakávania: ,Anjel, hotový anjel!‘ Cítil, ako sa v nej prelieva čistá a svetlá energia, cítil, ako okolo nej všetko ožíva, cítil zárodok Zeme, života ... a nech bol akokoľvek hrdý na svoj spôsob života, vnímal, že táto bytosť stojí vysoko nad ním, nad jeho doterajšími skutkami. Práve toto, na prvý pohľad pozitívne zistenie, mu zväzovalo ruky a k jeho vrodenej plachosti sa pridal pocit zúfalej nedosiahnuteľnosti. Prečo ju len musel stretnúť? Či ona jeho? Bol to osud? Či Boh, ktorý im spojil cesty?   Nech TO bolo čokoľvek, nevzdalo sa len tak ľahko. Neprešiel ani týždeň a znova ju uvidel. Na tom istom mieste, skoro na blší centimeter presne. Akoby na niečo, niekoho čakala. Keď ju zbadal, srdce mu zamrelo ..... presnejšie, najprv prestalo biť, potom sa roztĺklo ako zvon, až videl, ako mu zvonením a bitím rozdrapí hrudný kôš, vyletí z tela a poskakuje pred ním po zemi. Stála s privretými očami a odovzdávala sa živej, nežne pohládzajúcej slnečnej energii. Kúpala sa v žiarivých lúčoch, ktoré jej pozlátili čierne vlasy. Presvitajúce letné šaty mu poskytovali oslepujúci pohľad, znásobený ostrým svetlom. Nebol schopný pohybu, nebol schopný hlesnúť, len tam stál ako kôl v plote. Akoby videl ôsmy div sveta. Myšlienky mu splašene poletovali hlavou, sácali sa, chaoticky po sebe vyvreskovali, množili sa, zrazu ich bolo päťkrát viac, ako môže priemerný človek mať a zniesť. A jedna z nich hulákala najhlasnejšie: ‚Ide k Tebe!!!‘ Precitol, chcel zutekať, no nohy ho neposlúchali. Snažil sa upokojiť, dosť zúfalo a úplne bezvýsledne. Možno si len namýšľa, možno len prejde popri ňom, možno sa mu len zdá, že sa na neho usmieva, možno niekto stojí za ním, ako to býva vo filmoch - človek si myslí, že krásna deva máva na neho, ponáhľa sa ho objať a napokon prejde popri ňom k indivíduu, stojacemu mimo zorného poľa. Lenže ona kráčala rovno k nemu a dívala sa mu priamo do očí  Sklopil zrak. ,To nie je možné, to nie je možné.‘, krútila sa v hlave plachá myšlienka stále dokola. ,Prečo by to nebolo možné?‘ dobiedzala krikľúnska myšlienka. ,Lebo nie som pripravený na zázrak.‘ Pozrel hore. Bola tu. Pohľad mu zavisol na jej perách, z ktorých sa šírili nejaké tóny, akordy, melódie. Nemýlil sa v „prvostupňovom" hodnotení, zvuky to boli jemné, ako si predstavoval, ale ani zamak im nerozumel. Nevnímal slová, videl len jahodové polmesiace, vsal sa do nich pohľadom, do pier, ktoré okrem nej určite patrili nejakému šťastlivcovi. Videla, že jej civí na ústa, milo sa usmiala a dlhými prstami mu jemne zdvihla bradu, aby sa im stretli oči.  „Čakám tu na teba." zreteľne sa ozvalo.  „Na mňa? Prečo na mňa?" panika sa zväčšovala.  „Lebo ti dôverujem, lebo si milý, lebo sa mi páčiš."  Neveril svojim ušiam, čakal, že každú chvíľu zazvoní budík a on sa poberie do práce a toto bude len sen, síce krásny a škoda, že ukončený, ale len sen.  „Prečo?" neuvedomil si absurdnosť otázky.  Usmiala sa: „Veď som povedala - si milý ... aaa tak .... nedávno si sa na mňa díval spôsobom, akoby si mi hľadel do duše. Uvedomujem si, asi to nevyzerá dobre, keď sa žena prihovorí mužovi, ktovie, čo si o mne teraz myslíš, ale mne sa vidí, akoby som Ťa poznala už večnosť." „Prečo?" nezmohol sa na iné, ako na donekonečna opakovanú detskú otázku.  Vyprskla smiechom: „No dobre, chceš nejaký rozumný dôvod. V meste som dva týždne a ešte sa tu dobre nevyznám. Potrebujem niekoho, kto by ma ním trochu previedol. A Ty si ten pravý." ,Si ten pravý.' Tieto tri krátke slová, tri obyčajné slová mu rezonovali v hlave, nechápal, ako to môže vysloviť s takou ľahkosťou v hlase, ako sa môže tak bezprostredne správať. Akoby mu povedala, že v pekárni na rohu majú čerstvé rožky s makovou posýpkou.  Silou vôle sa snažil spamätať, otriasť a pôsobiť prirodzeným dojmom, Nie veľmi sa mu to darilo, cítil to z jej  pobavených pohľadov. Neboli výsmešné, to nie, len láskavo chápajúce. Zato on stále nechápal. Neveril, že sa to stalo, neveril sebe.  „A ty si ma minule videla?" skúsil konečne nadviazať rozhovor - neohrabane, ale predsa. Rozhodla sa, že zahrá menšie divadielko: „Áno, už som Ti povedala, že som si všimla, ako sa na mňa dívaš, aj keď si stále bočil pohľadom. Myslela som, že by bolo pekné, keby si sa mi prihovoril, no ty si neustále odvracal zrak. Vôbec si si ma nevšímal," zamračila sa s hranou mrzutosťou. „Iste som sa Ti nezdala dosť zaujímavá," vychrstla priamo na neho a skúmavo sa mu zahľadela do očí. Jej bezprostrednosť ho znovu dostala do kolien. Kde sa vzala? Odkiaľ prišla? Čo vlastne od neho chce?  ,Šalieš?!‘ chcelo sa mu skríknuť, - ‚Veď ty si tá najkrajšia, najlepšia, najúžasnejšia, najcennejšia, najnenahraditeľnejšia bytosť, akú som kedy videl! Pre Teba som až doteraz žil ako askéta, na túto chvíľu som čakal celý život, v tento moment som veril a modlil sa, aby ma, až nastane, ovalil kladivom po hlave a nedovolil ho prepásť.‘ Miesto toho len mlčky hľadel do zeme, špicou topánky posúval kamienok a rozmýšľal, prečo je taký chmuľo. ‚Je možné, že je taký nechápavý?!‘ preletelo jej hlavou. ‚Veď som videla, že je zo mňa hotový ... alebo sa mi to naozaj len zdalo? ..... nie, intuícia ma nikdy nesklamala, vyznám sa v ľuďoch.‘  Okolo prešlo polievacie auto, z ktorého striekali na zaprášené cesty šuštiace prúdy priezračnej vody. Na tvári pocítil vánkom priviatu studenú rosu. Konečne precitol.  „To nie je pravda, všímal som si Ťa až priveľmi dobre. Nevidela si, nemohla si vidieť moje trasúce sa nohy, nepočula, ako mi bilo srdce." Držal sa statočne. ‘A je to vonku. Nech sa deje, čo chce.‘  „Prepáč, žartovala som, viem veľmi dobre, čo si zažíval, bolo to na tebe vidieť. Videla som, že po mne túžiš a musím sa priznať, že mi to vôbec nebolo nepríjemné. Páčilo sa mi, ako sa na mňa dívaš. Toto priznanie ber ako odškodnenie za to, že som ťa potrápila, Ako som povedala, si milý a viem, že aj dobrý človek. Ver, že mám na to nos." Usmiala sa. Bol stále ticho. ‘Teda ten chalan mi to vôbec ale vôbec neuľahčuje.' No nemienila sa vzdať tak ľahko. „Tak čo, dohodneme sa? Budeš mi robiť sprievodcu? Ak sa cítiš trápne, môžeme sa dohodnúť aj na Dohode o vykonaní práce. Ty mi ukážeš mesto a ja Ti to vrátim dobrou náladou. Uvidíš, neobanuješ." Vidiac jeho zamyslenú tvár rýchlo dodala: „Teda, ak máš čas. Nebudem Ťa veľmi zdržiavať..... občas by sme sa stretli a ...." „Samozrejme, bude mi potešením," skočil jej do reči, cítiac, že ju asi svojim správaním príliš nenadchol. „Hocikedy, stačí mi zavolať, alebo mi napíš na mail."  „Hm, ale to by som najprv musela mať tvoje číslo a mailovú adresu," uškrnula sa. „Prepáč, som trochu mimo, ešte si budeš myslieť, že sa ťa chcem zbaviť. Počkaj, napíšem ti ich," začal po vreckách hľadať kus papiera ... „Eeee, mám tu cestovný lístok, nevadí, to bude stačiť .... ešte pero .... " Začala sa strašne smiať. S údivom na ňu pozeral, nechápal, čo je na tom také smiešne. Z kabelky vytiahla telefón. „Ty nemáš mobil?" spýtala sa, ešte sa zadúšajúc úprimným smiechom z jeho popletenosti.  Pochopil a ústa mu tiež roztiahol úsmev. „Vidíš, aký som. Ešte sa chceš so mnou stretnúť, aby som ti ukázal mesto? Pri tebe strácam niť, pri vlakovej stanici ti možno poviem, že je to významný kultúrny stánok, kde sa odohrala významná bitka pri príležitosti pivných slávností...."  ‚Tak predsa som sa nemýlila, dokonca je aj vtipný,‘ odľahlo jej. „Tak takýto výklad si náhodou rada vypočujem, mám rada smiech .... a veselých ľudí." zadúšala sa smiechom. Začervenal sa, pochopil, že sa ukázal v dobrom svetle.  Videla jeho zaváhanie, poponáhľala sa, aby znova neupadol do rozpakov: „Tak, nadiktuj mi mail a ja si ho zapíšem do telefónu. A potom ti dám svoje číslo, prezvoníš ma a budeme mať obaja číslo toho druhého. Vidíš, žiaden papierik nepotrebujeme." „Ja viem, prepáč, že som bol taký nepraktický. Ale to preto, lebo si ma od začiatku zaskočila." Nadiktoval jej svoju adresu, po prezvonení a uložení čísel sa na ňu s ľútosťou zadíval: „Veľmi ma to mrzí, ale budem už musieť ísť. Nehneváš sa?" „Neblázni, jasné, že nie. Kľudne choď, už som ti dnes narobila dosť šokov. Ešte chvíľu a možno ti vyvediem niečo, čo by si nerozchodil," zasmiala sa.  Rozlúčili sa a vydali každý iným smerom. Počas chôdze sa niekoľkokrát obzrel, aby uvidel, že aj ona otáča hlavu za ním, až sa skoro potkla. Po chvíli zašiel za roh domu a spadla na neho obrovská pochybnosť, či to všetko, čo sa mu prihodilo, bola pravda alebo len príjemný, vysnívaný zážitok. Zastal, vrátil sa späť k rohu, pomaly sa vyklonil a poslal zrak k miestu, kde pred chvíľou stáli. Nebola tam. Myšlienky sa mu znova roztancovali, prinášali pochybnosti o zdravom rozume. Iné ich zavracali a sľubovali nevšednú budúcnosť.  V tej istej chvíli rovnaký myšlienkový boj zažívala aj ona. Vedela, že je to ten pravý, cítila to. Videla do neho, vnímala ho, znela s ním na rovnakej vlnovej aj tónovej dĺžke .... problémom bola len jeho utiahnutá povaha, nesebavedomá a ničomu neveriaca, najviac samému sebe. To by mohlo byť povestné poleno pod nohami, ktoré by zatarasilo cestu jedného k druhému. Zľahka potriasla bohatou hrivou a odohnala dotieravé a neprajnícke myšlienky. To ona nedovolí! Zo všetkých síl sa bude snažiť, aby patrili jeden druhému. Nie v majetníckej, ale duševnej rovine. Aby cítil aj on, že sú si súdení, tak, ako to prežíva ona. Vie, že to dokážu, cíti to, verí v to. A čo je dôležitejšie ako viera?   Ozval sa prvý a hneď v ten istý večer. Nemohol zaspať, musel sa presvedčiť, že sa všetko odohralo tak, ako si pamätal. S úľavou počúval jej jemný, zvonivý hlas a čím dlhšie spolu rozprávali, tým väčšiu pohodu pociťoval. Prekecali skoro hodinu, počas ktorej prebrali množstvo tém, uťahovali si jeden z druhého, skákali si do reči, smiali sa .... spoznávali sa. Nikdy necítil taký strašne silný súzvuk s dušou ženy. Je snáď pravdivá stará čínska povesť o pôvodnej „dvojjedinej" bytosti, v ktorej prebývali obidve duše - mužská i ženská - vo vzájomnej láske a harmónii? A ktorú pre tieto časom sa hašteriace polovičky nahnevaný Boh rozorval a obe nehodné duše zahodil na opačné kúty sveta? Odvtedy sa vraj hľadajú. Môže to byť pravda? Nemal ďaleko od toho, aby tomu uveril. Neodvážil sa viesť rozhovor do mobilu týmto smerom, nechcel, aby sa vyplašila. Bál sa, že je len priveľmi priateľská, aj keď srdce mu vravelo niečo iné. Prisahal, že bude veriť svojmu srdcu, ktoré vraví, že ten anjelský hlas to myslí vážne. Obaja, srdce i on by ho dokázali počúvať až do rána. Nakoniec ho musela brzdiť a presviedčať, aby už išli spať, lebo ráno musí skoro vstávať. Pripadal si ako malý chlapec, ktorého z okna volá mama večer domov a on zjednáva: „Ešte chvíľku....." Pomohla až jej žartom vyslovená vyhrážka, že zruší jeho sprievodcovské služby. To zabralo. Dohodli si stretnutie na druhý deň a rozlúčili sa ako dlhoroční kamaráti. Po zrušení spojenia si ešte dlho premietal vyslovené slová a obrazy z nich vyplývajúce. Po hodnej chvíli spokojný zaspal, v očakávaní nasledujúceho dňa. To jej trvalo zaspať podstatne dlhšie, už vedela, že sa našli, už vedela, že svet bude ešte krajší ako bol doteraz. Telom jej jemne elektrizovalo, cítila také zvláštne nedefinovateľné šteklenie, vo vnútri bola ako nedočkavé dieťa, ktoré chce preskočiť štedrovečernú večeru, už by chcelo rozbaľovať vianočné darčeky, už by chcelo zistiť, čo za prekvapenia ho čakajú.   Na druhý aj v ďalšie dni, ktorých čísla sa striedali závratným tempom, zisťovali, že jednoducho patria k sebe. Nič si k sebe nedovoľovali, ale to ani nebolo treba, aj bez slov či dotykov bolo všetko jasné. Samozrejme, že z prehliadky mesta nebolo nič, teda skoro nič. Spolu spoznali rôzne romantické zákutia, hlavne rozkvitnuté parky, prímestské lesné cestičky, úzke uličky historického centra, také úzke, že sa pri chôdzi dotýkali bokmi. V takých chvíľach ho drvila zimnica rozkoše z jej prítomnosti. Pri týchto prechádzkach prebrali nespočetné množstvo tém, vyslovili tisícky viet, desaťtisíce slov, stotisíc slabík, milióny písmen. S údivom sa navzájom počúvali a mali už dopredu tušenie, čo chce ten druhý povedať. Skoro telepaticky striedavo dopĺňali začaté vety vychádzajúce spomedzi pier, ktoré boli pre oboch sladkým pokušením. No ani po dlhotrvajúcom strávenom čase v symbióze vzájomného duševného spojenia nemal odvahu pre rozhodnutie spojiť svoje pery s tými, po ktorých tak túžil. Pomyslenie na toto spojenie bolo pre neho vzdialené ako Venuša od Marsu. A to, čo by mohlo nasledovať po prvom bozku, tak to si radšej predstavoval len hmlisto, tak ďaleko ešte nebol. Zatiaľ mu stačila jej čistá prítomnosť, jej evidentná slabosť pre neho, už vedel, že sú si súdení. Nechcel nič unáhliť, niečo pokaziť, nechcel sa zobudiť. Ako inak, znova to bola ona, kto musel vziať opraty do svojich rúk. Nie, žeby jej vadilo jeho gavalierske správania, žeby bola nejaká hrrr, no videla, že ju miluje a nevie jej to dať najavo. To síce ani nebolo treba, sršalo to z každej jeho bunky, ale videla aj to, že sa  trápi a že po nej nesmierne túži. Napokon, nebol sám, kto túžil po tom druhom. Nechcela sa už ráno budiť sama, nechcela sa tešiť len na tie chvíle, ktoré trávili spolu na svojich schôdzkach. Chcela ho mať pri sebe navždy, chcela ho mať pre seba celého a to doslovne.   Keď ju v ten večer ako vždy odprevadil pred dom, v ktorom mala prenajatý maličký byt, zaželal jej dobrú noc a automaticky sa chystal na odchod. Chytila ho za ruku a pozrela mu do očí. Pomaly priblížila svoju tvár k jeho a zľahka ho pobozkala na ústa. Rozbúšilo sa mu srdce, zatočila hlava, podlomili nohy. Bolo to tak silno nečakané, že ostal zaskočený. Samozrejme, ostal by zaskočený, aj keby to očakával. Znova sa dotkla ústami jeho pier, tentoraz však neostala pri jednom letmom bozku. Objal ju okolo pása a pritlačil sa na jej hebké telo. Bolo to také nádherné, také plné lásky, že nevedel prestať. Nevedel, koľko stáli takto spojení, nechcel to vedieť, žil pre túto chvíľu, pre tento moment. Keď sa konečne od seba odtrhli, nemohli polapiť dych. Srdcia im zúfalo trepali a snažili sa zvládnuť ten silný a prudký nápor krvi, valiaci sa v ich vzrušených telách. Ostali v objatí, mlčky si pozerali do očí, len ruky hmatom jemne skúmali naproti stojacu milovanú bytosť.  „Nechoď preč," prosebne sa ozvala. „Buď so mnou, potrebujem ťa. Potrebujem tvoje oči, tvoje ústa, potrebujem cítiť dotyk tvojej duše, potrebujem cítiť tvoju lásku."  Jej slová mu zneli ako najľúbeznejšia pieseň. Miloval ju viac ako hocičo na svete. Mohol takejto prosbe odolať? Keď vstúpil do jej života, nebude sa predsa báť vstúpiť do jej bytu.  Garsónku mala zariadenú skromne, ale útulne. Veľa si však toho nestihol všimnúť, hneď ho objala a začala bozkávať. Zaboril ruky do jej vlasov a s potešením sa v nich prehrabával.  „Čakal som na teba celú večnosť, už som ani nedúfal. Kľudne by som sa pre teba upísal aj diablovi, kľudne by som zomrel. Ale nie, to by som zas s tebou nemohol byť, nemohol by som ťa hladiť po vlasoch, nemohol by som sa ťa dotýkať, nemohol by som ťa cítiť, nemohol by som cítiť dotyk tvojej duše." zašepkal, keď im v bozkoch došiel dych. Z oka jej dolu lícom stekala slza šťastia. Na mape pokožky kreslila tenkú linku cesty nádeje a očakávania. Šepla: „Ľúbim ťa."  Jemne sa jej dotýkal, pohrával sa s jej pokožkou, akoby ju ovieval vánok. So zvedavosťou malého chlapca spoznával miesta, ktorých sa doteraz neodvážil dotýkať. Nechala ho plaviť sa po objavnej trase a vracala mu nežnosti. Pomaly ho začala zľahka navigovať ku gauču v rohu izby. Vrhli sa na seba ako dvaja zápasníci, ktorí si navzájom nič nedarujú. Dotyky, predtým len letmé a opatrné, dotyky, predtým také jemné a nežné, sa zmenili na neohrabané chvaty, drsné hmaty, ktoré spôsobili, že šaty strácali svoj tvar a pôvodný zmysel. Na rozdiel od súboja na žinenke, kde si aktéri boja v pravidelných intervaloch naprávajú svoje kimoná, oni si prestávku na úpravu nedopriali. Ani to nemali v úmysle, postupne strácali jednotlivé kusy svojich odevov, až ostali obnažení na dušu. Za zvukov bojových stonov a fučania sa prevaľovali jeden cez druhého, raz bol hore jeden, raz druhý. Spočiatku bol zápas vyrovnaný, v rámci športových pravidiel, obaja si šetrili sily na ďalší vývoj a jeho vyvrcholenie. Ani jeden to ale nechcel dlho odkladať a začali používať rôzne nešportové praktiky - ona nechty vrývajúce sa do súperovho chrbta, on opatrné hryzenie uší. Vo vidine víťazstva sa čím ďalej, tým viac utiekali k nedovoleným chmatom pod pás. Postupne cítil, že sa mu poddáva, až so zovretým hrdlom šepla: „Doraz ma!" Viac mu nebolo treba - vyrazil ako hrdý kráľ na čele vojska, ktorého predvoj, tvrdo si raziac cestu, prenikol až do tyla nepriateľa. Za zvukov rinčiacich zbraní, hlaholu poľníc, bubnovania bubnov sa opätovne vrhal do boja,. tep srdca mu vrážal do spánkov, až mal pocit, že mu z nich vytryskne krv. Nedbal na to, znova a znova vrážal svoju zbraň do mäkkého tela a získaval ďalšie a ďalšie úspechy. Kopijou prebodával pomyselných nepriateľov, koril ich jedného po druhom, až nakoniec ostal posledný - ona. Ešte mu ako-tak chabo odolávala. Z posledných síl vyrazil do konečného útoku. Zrazu mu telom prebehol životodarný výboj, ktorým ju načisto pohltil. Strašne dlhú chvíľu sa obaja zmietali v energetických kŕčoch, prostredníctvom ktorých sa ich duše spojili s nekonečným vesmírom.   Ležali unavení vedľa seba, dvaja šťastní, k sebe pritisnutí víťazi, navzájom sa odmeňujúc  dlhým bozkom, po ktorom si celú večnosť láskyplne a vďačne hľadeli do očí. ‚Tak takto vyzerá smrť?' pýtala sa sama seba. ‚Potom nech som mŕtva navždy.' Postupne sa jej zrak zahmlieval, až odišla do sladkej ničoty. Hľadel na ňu a duša sa mu pomaly vracala na svoje miesto. No už nikdy nemusí hľadať jej druhú časť, schováva sa schúlená v žene ležiacej v jeho náručí, s ktorou práve dobojoval urputný boj. Vedel, že sú spojení v jednu bytosť. Na spánku jej tepala čiara života a on prisahal, že odteraz je jeho jedinou starosťou, aby tepala navždy spolu rytmom jeho srdca. ‘Takúto úlohu a boj som ochotný znášať do konca sveta.' 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Nešťastie ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Mňam a mľask ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Dokonalý svet ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Podpultový tovar?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavel Vrabec 
                                        
                                            Pravda
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Vrabec
                        
                     
            
                     
                         Ďalšie články z rubriky próza 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        David Stojaspal 
                                        
                                            Dukelské zápisky 1944-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Igor Čonka 
                                        
                                            Ellis Parker Butler - Prasce sú prasce I
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Kto nezažil, neuverí. . . (46)
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Oni
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Odrazový mostík
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky próza
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Vrabec
            
         
        vrabec.blog.sme.sk (rss)
         
                                     
     
         Som tu na návšteve ... asi ako dieťa - pozorovateľ, aspoň sa o to stále snažím. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    450
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisník "športovca" ... :o)
                        
                     
                                     
                        
                            V MHD
                        
                     
                                     
                        
                            Láska
                        
                     
                                     
                        
                            Oplatí sa vidieť
                        
                     
                                     
                        
                            Čo ma teší ...
                        
                     
                                     
                        
                            Medzi ľuďmi
                        
                     
                                     
                        
                            Čo ma se... srdí ....
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ebola a iné vírusové ochorenia, ktoré strašia našu civilizáciu.
                     
                                                         
                       Prečo milujem svoju ženu?
                     
                                                         
                       Dobré časy, časť prvá: Kanab
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




