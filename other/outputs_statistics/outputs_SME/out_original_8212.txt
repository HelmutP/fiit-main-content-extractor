
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alžbeta Sersenová
                                        &gt;
                Nezaradené
                     
                 Náš záchod, váš zážitok 

        
            
                                    6.6.2010
            o
            22:21
                        (upravené
                6.6.2010
                o
                22:30)
                        |
            Karma článku:
                8.02
            |
            Prečítané 
            501-krát
                    
         
     
         
             

                 
                    alebo ako neuveriteľne ma dokážu niektoré veci nahnevať, a ako človeku zobrať posledný kúsok úcty za dvadsať sekúnd.
                 

                 
Zdroj obrázkov: internet 
     
   Čakanie u lekára je aktivita, ktorá zrejme nie je nikomu príjemná. Nepoznám človeka, ktorý by si to užíval. Lebo život naberá úplne iný rozmer, ak človeka niečo bolí a trápi. Zvlášť ak čakáte na výsledky, ktoré môžu zmeniť celý váš život. Vtedy túžite byť niekde celkom inde. Na mieste, kde je príjemne. Kde nie je bolesť, strach a studená dlaždičková chodba. Prestupujete z nohy na nohu, obhrýzate si nechty a každých päť minút vám treba na záchod.    Ha! Ale trnavská poliklinika, tá má svoj patent na to, aby ste sa s týmto posledným zmieneným  zlozvykom veľmi rýchlo rozlúčili.    Rozpoviem vám svoj príbeh. I keď každý Trnavčan má určite vo svojom repertoári podobný. Zo dva dni ma pobolieval zub.V preklade mi išlo roztrhnúť hlavu od bolesti.  Medzi zubárskym a elektrickým kreslom rozdiel nevidím, nech by bol pán doktor akokoľvek príjemný. ( A že sa po  každé  snaží byť!)  Stála som teda pred ordináciou a čakala. Nepotili sa mi len ruky, ale snáď všetko, čo sa spotiť môže  a pomaly mi začala stúpať teplota. Uchlipkávala som z fľaše až my v istom momente mechúr vyslal správu, že stačilo.   „Ešte si odskočím,“ povedala som, dúfajúc v úľavu. Prerátala som sa.   Na prvom poschodí, záchod zamknutý, druhý upchaný.   Na druhom poschodí som hneď medzi dverami dostala takú facku, že som nevedela či skôr vrchom alebo spodkom.   Poschodie tretie síce rovnako ako tie dve predošlé nemalo kľučky, ani splachovače, ale postupom času človek prišiel na princíp ako spláchnuť. Len bolo treba nedýchať , všetko vybaviť v čo najkratšom časovom limite a nepozerať sa okolo seba.      Tak som okrem bolesti zubu mala ešte aj pocit, že pacient, teda aj ja,  je to posledné, na čo tam ktokoľvek myslel. A to mňa bolel len zub. Neviem si predstaviť ako sa cíti naozaj chorý človek, ktorý musí obehnúť tri poschodia, aby našiel vyslovene dieru v zemi, do ktorej si môže uľaviť. Prečo ten človek musí podstúpiť ešte aj procedúru, ktorá z neho spraví  zviera? Pre to, aby nezabudol, že je na Slovensku? Ale aj tu, v Trnave, sú iné zdravotnícke zariadenie (nehovoriac o súkromných), kde je tá úroveň celkom iná. Pritom ide o takú banálnu a prirodzenú potrebu. Potrebu, ktorá keď sa raz ozve, nejde len tak ľahko utíšiť. A namiesto toho, aby sme urobili všetko pre to, aby sa chorý ľudia cítili čo najlepšie, ešte ich aj zdehonestujeme tým, že musia konať potrebu pri polootvorených   dverách, ktoré, kvôli absencií kľučiek, nejdú zavrieť. Ale zas na druhej strane, milovníci hororov si zgustnú...       Až po takomto zážitku si človek uvedomí dôležitosť niektorých vecí.   Oddnes na poliklinike nepijem.   Oddnes si začnem zrejme na polikliniku nosiť vrecúška na psie exkrementy. Ešte stále to bude hygienickejšie ako tamtie toalety.   Oddnes si náš záchod doma cením omnoho viac!       Napadá ma len otázka:  Nemusí toto zariadenie spĺňať nejaký hygienický štandard?     Alebo sa mám cítiť ako Alica v krajine zázrakov ak očakávam kľučky,    (keď už nie kľúče), splachovače, nejaký ten papier a úplne neutrálnu    arómu?       Zostáva nám len dúfať, že tvrdenie kultúru národa spoznáš podľa záchoda neberie toľko ľudí až tak vážne. Lebo v tom prípade, sme my zostali niekde v štádiu ramapithecusa.   Pomodliť, vycikať a na polikliniku....      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Sersenová 
                                        
                                            Opäť za 5?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Sersenová 
                                        
                                            Pod jedličkou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Sersenová 
                                        
                                            Virtuálne hladkanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Sersenová 
                                        
                                            Nechcené
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Sersenová 
                                        
                                            O bohatstve s Benjaminom Franklinom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alžbeta Sersenová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alžbeta Sersenová
            
         
        sersenova.blog.sme.sk (rss)
         
                                     
     
        Ad astra per aspera. Utrpením ku hviezdam....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    49
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dimitrij Merezkovskij
                                     
                                                                             
                                            Peasovci
                                     
                                                                             
                                            John Irving
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            MIrusko
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




