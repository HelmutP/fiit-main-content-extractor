
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                jedlo, bylinky
                     
                 Maličké ľanové semienko - obrovská zbraň proti neduhom 

        
            
                                    24.5.2010
            o
            7:33
                        (upravené
                24.5.2010
                o
                9:18)
                        |
            Karma článku:
                11.18
            |
            Prečítané 
            15770-krát
                    
         
     
         
             

                 
                    Verím, že všetci poznáte drobné, lesklé, hnedé semienka, ktorým sa hovorí ľanové. Ľan je pôvodom z Blízkeho východu a v Európe sa pestuje asi 6000 rokov. Už v dávnej minulosti sa používal na výrobu plátna, ako potrava aj liek. V ostatných rokoch toto nenápadné semienko aj z farmaceutického hľadiska naberá na svojom význame, priam raketovou rýchlosťou.
                 

                 
  
   Semienko odstraňuje zápchu, zastavuje hnačku a prospeje aj  srdcu. Upokojuje sliznicu žalúdka, lieči žaludočné vredy, zvonku priložené zastavuje bolesť.   V ponuke lekární nájdeme ľanový olej, ktorý sa môže používať  vnútorne aj na vonkajšie účely. Vnútorne znižuje cholesterol, tlak krvi, lieči reumatické zápaly, spevňuje nechty a vlasy. Zvonku lieči popáleniny a má regeneračné účinky na pokožku, je súčasťou mnohých kozmetických prípravkov. Odporúčam siahnuť po oleji, ktorý je lisovaný za studena, zväčša je označený ako "bio".   Čomu vďačí ľanové semienko za svoje liečivé účinky? Ide predovšetkým o vysoký obsah vzácnych omega-3 mastných kyselín, lignanov, fytoestrogénu a vlákniny.   Poradca pre naturálnu medicínu Ján Dedík získal z nemeckého zdroja recept na  protirakovinovú kúru, ktorej súčasťou je práve ľanové semienko.  Nakoľko ani farmaceutický priemysel nepodceňuje protirakovinový účinok, myslím, že túto kúru netreba podceňovať.   Trojtýždňová protirakovinová kúra:   necelú lyžičku ľanového semienka pomelieme (pomôže aj kávomlynček) a posypepeme ním kocku tvarohu, nie nízkotučného, je potrebné, aby mal minimálne 10% tuku. Toto množstvo predstavuje dennú dávku, ktorú si treba rozdeliť do niekoľkých menších porcií. Upozornenie: nemelieme do zásoby, semienko má byť vždy čerstvo zomleté! Zmes môžeme konzumovať prípadne aj s banánom, s ryžou natural, zjemníme jogurtom...   Aby sa uvoľnené škodliviny dostávali z tela von je potrebné piť dostatočné množstvo urologických čajov, cviklovú šťavu. Vhodným doplnením je strava v zmysle protiplesňovej diety.       Semienka zbavujú bolestí vnútorných orgánov. Obklad si pripravíme jednoducho:   ľanové semienka, pomleté či celé vložíme do pláteného vrecúška, ktoré na desať minút ponoríme do horúcej (ale nie vriacej) vody. Necháme odkvapkať a čo najteplejšie prikladáme na bolestivé miesto.   Výskumy potvrdzujú, že nedostatok omega-3 mastných kyselín sa spája aj s Alzheimerovou chorobou a s problémami pamäti. Semienko je ich bohatým zdrojom.   Semienka celé, alebo mleté môžeme používať aj v našej kuchyni. Môžeme ich pridať (trebárs jemne opečené) do šalátu, polievky, do vločiek, do cesta na slané pečivo, prípadne ho ním posypeme. Aj na pultoch predajní nájdeme chlieb,  pečivo, či tyčinky ktoré ho obsahujú.   Myslím, že toto malinké semienko dá ešte o sebe počuť. Určite ho budeme čoraz častejšie tasiť ako zázračnú zbraň  na mnohé neduhy. Verím, že jeho popularita bude stúpať a jeho účinky budú naďalej prekvapovať. Som presvedčená, že je a bude účinnou zbraňou v prevencii, ale aj pomocníkom v chorobe.   Kúpiť si vrecúško so semiačkom už dnes nie je problém. Tak prečo tak neurobiť?   ****   Dovolím si pripojiť aj vlastnú skúsenosť so semienkom, čo sa týka liečby kašľa:   4 polievkové lyžice semienka zalejeme cca 2 deci horúcej vody. Necháme postáť 2-3 hodiny. Precedíme a osladíme medom. Dávka pre deti je jedna čajová lyžička a pre dospelých 1 polievková lyžica 4-5 krát denne. Môžeme pridať aj mlieko v pomere 1:1.   Na podporu hnisania (napr.na vredy): uvaríme ho vo vode na hustú kašu, necháme trochu vychladnúť a zabalíme do bavlnenej látky, prikladáme na boľavé miesto.           Foto aj zdroj informácií: internet 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (57)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




