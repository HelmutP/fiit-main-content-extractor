
 Pamätáš si...
keď pod závojom tisícich hviezd 
moja tvár 
žiarila ako Botticelliho anjel...? 
Boli sme k sebe nežne drzí 
a hrejivá záplava chvenia
nám k perám stiekla
Pritiahli sme sa ako dva meteority 

Chceli sme to   
Ach áno - tak veľmi sme to chceli 
Francúzske bozky 
nám vtedy pery dočervena sfarbili 

Pamätám sa... 
ako naše srdcia v mokrom tričku 
na morskom pobreží burácali  
Šepkal si mi – 
"Chcem byť hromozvod tvojej nálady
a aj keď budem 
o stareckú paličku opretý 
aj tak ti francúzske bozky 
z lásky oplatím"


* 
