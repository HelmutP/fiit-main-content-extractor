
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nora-Soul Slížová
                                        &gt;
                o živote
                     
                 Syn, ktorý chcel vidieť ako vyzerá jeho otec 

        
            
                                    23.4.2010
            o
            20:22
                        |
            Karma článku:
                9.11
            |
            Prečítané 
            1901-krát
                    
         
     
         
             

                 
                    Život má plno prekvapení. Predstavte si, že zdvihnete telefón, a na druhom konci zaznie veta. „Dobrý deň, som váš syn, vaša dcéra. Nemali by ste chuť na kávu ? Pozývam vás.  Čo tak, povedzme zajtra? Vyhovuje vám to? „ Ale takéto veci sa stávajú. Nie len v lacných príbehoch pre nenáročných čitateľov, ale aj mimo strán kníh...
                 

                 Aron, je mladý muž, ktorého veľmi dobre poznám.   Vlak uháňal zasneženou krajinou, obklopený monotónnymi zvukmi prebúdzajúceho sa zimného rána. Bolo to také obyčajné všedné ráno, ponorené ešte do ticha predošlej noci.    Aron sedel vo vlaku, jeho vnútro bolo v hlbokom kontraste oproti pokojnému tichému ránu. Bol nervózny, ruky mal spotené z toho ako kŕčovito zvieral mobil. V duchu si opakoval, že predsa o nič nejde, že nemá čo stratiť, že nič neočakáva, chce  urobiť len tento krok, na ktorý sa pripravoval viac než dvadsať rokov. Napriek tomu, že bol nervózny, vedel, že teraz je pripravený. Cítil sa dosť silný a vyrovnaný nato, aby prijal ďalšie odmietnutie, keď bude treba.   Chystal sa zavolať svojmu otcovi, chcel ho požiadať o stretnutie. Aron svojho otca nikdy nepoznal, nevidel ani jeho fotku, len vedel , že existuje a poznal jeho meno. Jeho matka mu povedala, že sa naňho podobá, ale nikdy mu o ňom viac neprezradila. Aron  ešte ako malé dieťa vycítil, že pre matku sú jeho otázky ohľadne otca nepríjemné, tak sa už na nič nepýtal, nechcel aby sa naňho hnevala ešte viac. Na svojho otca myslel často, predstavoval si, ako asi vyzerá, či sa smeje a či je vysoký. Dávno predtým než dospel, pochopil, že jeho otec už naňho dávno zabudol. Pre jeho otca neexistoval žiadny Aron, s jeho matkou sa rozišiel ešte pred jeho narodením. Pre Arona však existoval, a on vedel, že dospel do bodu, keď chce túto záležitosť uzavrieť. Potreboval urobiť tento krok, aby si mohol vo svojom príbehu túto kapitolu uzatvoriť.   Pred pár dňami sa Aronovi podarilo, získať kontaktné údaje na svojho otca,  jeho adresu a telefóne číslo. Keď na toto číslo volal vydával sa za novinára, ktorý ma snahu napísať článok o firme, v ktorej jeho otec dlhé roky pracoval. Za žiadnu cenu, nechcel spôsobiť nikomu nepríjemnosti.  Telefón zdvihla jeho manželka, ktorá mu ochotne poradila kedy má zavolať aby zastihol manžela doma. Bol rád, že to išlo takto hladko. Pred dvoma dňami hovoril so svojím otcom, súhlasil, že sa sním stretne na krátke interview, nech zavolá o dva dni a potvrdí mu presný čas stretnutia. Aronovi znel jeho hlas strašne staro. Prekvapilo ho to, sám nevedel prečo. Na druhom konci počul hlas starého unaveného muža, bolo to prvýkrát čo počul hlas svojho otca.   Aron vstal a pomaly vyšiel na chodbu, v ruke ešte stále zvieral mobil. Niekoľko krát sa z hlboka nadýchol a vytočil číslo svojho otca a potom si priložil  mobil k uchu.   Jeho otec zodvihol telefón na tretie zvonenie. Jeho hlas znel unavene a podráždene. Aron sa  predstavil - opäť ako novinár a v duchu sa modlil, nech to stretnutie neodriekne. Jeho modlitba tento krát nebola vyslyšaná.    Jeho otec sa pomaly vykrúcal z rozhovoru s novinárom. Argumentoval so zaneprázdnenosťou a vraj už mnohé roky v tej firme nepracuje. Aron si bol vedomý, že jeho plán zlyhal, v kútiku duše zapochyboval, či bolo rozumné sa do tohto celého púšťať. Či by nebolo rozumnejšie nechať to celé tak, ale uvedomil si, ako veľmi chce uzavrieť túto kapitolu svojho života. Rozhodol sa pre posledný  pokus, vyslovil meno svojej matky, spýtal sa ho, či mu to meno niečo hovorí. Spojenie sa prerušilo, jeho otec položil telefón.   Aron sa pozeral von oknom na bielu krajinu. Mlčky pozoroval ako ho unáša vlak. Jeho telo sa chvelo, pocítil smútok, ale bol na toto pripravený. Nič si nesľuboval ani neočakával. Len dúfal, že muž na druhom konci mu dá šancu, že pochopí, že nič od neho nepotrebuje a nechce. Vytočil opäť číslo, na druhom konci sa okamžite ozval starý ale teraz rozrušený hlas jeho otca. Aron si pomyslel, že podľa rýchlosti s akou zdvihol telefón možno čakal že zazvoní. Jeho otec začal rozprávať ako prvý.  Spýtal sa ho priamo, či je syn, tej ženy, ktorej meno vyslovil. Priznal sa. Tento krát neklamal, už sa nič nehral. Vysvetlil mu dôvod prečo ho kontaktoval. Prečo by ho rád stretol, videl. Nič nechce, len potrebuje uzavrieť túto kapitolu života. Nastalo ticho. Starší muž váhal, zbieral myšlienky, a hľadal tie správne slová. Vraj má už syna a manželku, ktorý vedia, že v mladosti raz dávno tiež splodil syna. Ale, on nie je pripravený ešte teraz vidieť svojho syna. Možno raz niekedy, nabudúce, teraz ešte nemôže, vraj  teraz je to príliš silné.   Aron mu povedal, že rozumie, poďakoval sa a rozlúčili sa. Skutočne porozumel.   Teraz bol pokojný, jeho ruky sa prestali triasť, vo svojom vnútri vedel, že urobil dôležitý krok. Podarilo sa mu ukončiť ďalšiu kapitolu svoju života. Pousmial sa pri predstave, že možno prešli vedľa sebe ako okoloidúci, veď obaja žijú v rovnakom meste.     Aron je občas smutný z toho, že  život mu rozdal karty v ktorých môže vidieť ďaleké miesta, slávnych a dôležitých ľudí ,ale to obyčajné sa tak trocha zamotalo.   A niekedy nám to obyčajné a všedné chýba najviac. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Keď stojíme nad hrobom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Milujem oboch - a to rovnako
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Transformácia pesimistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Dining Guide - Kde sa najesť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Byť lepším človekom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nora-Soul Slížová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nora-Soul Slížová
            
         
        slizova.blog.sme.sk (rss)
         
                                     
     
         Človek, ktorý si chce nájsť čas pre uvedomenie si vlastných myšlienok, postojov, názorov. Pochopiť určitú vec, teóriu, či událosť, mi prináša osobný rast a vytúžený pocit štastia.   
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    409
                
                
                    Celková karma
                    
                                                4.81
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja Rodina
                        
                     
                                     
                        
                            Dining Guide
                        
                     
                                     
                        
                            Prúd myšlienok
                        
                     
                                     
                        
                            To čo ma potešilo
                        
                     
                                     
                        
                            To čo ma zarmútilo
                        
                     
                                     
                        
                            Vedeli ste, že...?
                        
                     
                                     
                        
                            o živote
                        
                     
                                     
                        
                            z vlaku
                        
                     
                                     
                        
                            Zvieracie
                        
                     
                                     
                        
                            readers diary
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Smutné ale pravdivé
                                     
                                                                             
                                            Suicide Read This First
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            C.G.Jung
                                     
                                                                             
                                            Fyzika I
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jesus Adrian Romero
                                     
                                                                             
                                            Fly To The Sky
                                     
                                                                             
                                            Rady svojich priatelov
                                     
                                                                             
                                            Hlas svojho srdca
                                     
                                                                             
                                            Svoje myšlienky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj prvý...(blog ktorý som čítala)
                                     
                                                                             
                                            Michal Patarák-Filozof v bielom plašti
                                     
                                                                             
                                            Kamilka- Úprimná duša
                                     
                                                                             
                                            Hirax
                                     
                                                                             
                                            DiDi- proste skvela
                                     
                                                                             
                                            Veronika Bahnová - Favorite Teacher
                                     
                                                                             
                                            Rolo Cagáň
                                     
                                                                             
                                            Jozef Klucho- Doktor bacsi
                                     
                                                                             
                                            Juraj Drobny-ODF- Velky drobec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Forum - psychológia
                                     
                                                                             
                                            I Psychologia
                                     
                                                                             
                                            Macher z KE :)
                                     
                                                                             
                                            Bennett Pologe, Ph.D.
                                     
                                                                             
                                            Kokopelli forum
                                     
                                                                             
                                            Umenie na iný spôsob- Akupunktura
                                     
                                                                             
                                            Ambulancia klinického psychológa
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Blog na SME bol výnimočný, už nie je
                     
                                                         
                       Nenapísaná poviedka
                     
                                                         
                       Čestný občan Róbert Bezák
                     
                                                         
                       Ako som Jožka odviezla na psychiatriu (smutný príbeh s úsmevom )
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Obyčajná láska
                     
                                                         
                       Rande na slepo
                     
                                                         
                       Kvapka krvi
                     
                                                         
                       Bolesť.
                     
                                                         
                       Taká obyčajná autonehoda
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




