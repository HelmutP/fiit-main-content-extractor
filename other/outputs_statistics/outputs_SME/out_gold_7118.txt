

   
 Nadväzujem na udalosti spred roka a pol. Lebedím si pred TV a sledujem jednu z mojich obľúbených relácií na nemeckej RTL, ktorá podáva každý boží deň novinky zo života celebrít. To, že Pamela Anderson je asi najznámejšou záchranárkou planéty, sa stáva už pomaly ošúchaným klišé. Keď práve nezachraňuje v poradí už asi tretie rozpadajúce manželstvo, ako to už v Hollywoode býva trendom a ‚nezarába' Kalifornii ďalší polmiliónový dlh, urputne sa angažuje za práva zvierat. 
   
 A práve s týmto zámerom prichádza do nemenovanej krajiny na opačnej pologule, aby tu odprezentovala svoj dokumentárny film o neľudskom zaobchádzaní s kuraťami v krmníkoch na chovateľských statkoch a následne sa podrobila interview. Na Pam, cudno odetú a perfektne nastylovanú, tak ako sa na zanietenú bojovníčku za slobodu zvierat sluší a patrí, padne spŕška bleskov. Tróni za veľkým stolom na vyvýšenom piedestáli. Pod ňou sa hemžia novinári, reportéri, kameramani, ktorých plecia sa len tak prehýbajú na stranu pod ťarchou kamery. A všetci sú dychtiví po senzáciách, ostražití, aby ich pozornosti neunikla čo i len jedna neustrážená reakcia, neukontrolované gesto. Zdá sa, že Pam si tento stredobod pozornosti pred objektívmi foťákov a kamier ani veľmi neužíva.  Účelovo sa snaží potlačiť napätie, vyvolať dojem uvoľnenosti, vyčarovať prirodzený výraz tváre, a pritom všetko sa ešte aj sústrediť na myšlienky, ktoré musí starostlivo a pohotovo ukladať do viet. 
   
 Po poslednej zodpovedanej otázke sa prihlási mladý muž. Na kukuč veľmi pohľadný. A žiada od Pamely autogram. Ona polichotená, skrýva rozpaky za širokým strojeným úsmevom, keď vidí, ako jej mladý sympaťák vypučí pevný zadoček a stiahne si jeansy s trenírkami do polky svojich poliek. Pam zažehná rozpaky a opatrí signatúrou sympaťákovu zadnicu. Tento si svojvoľne vytiahne hore svoje gate, flegmaticky vykročí na svoje miesto a ...siahne si provokatívne do nohavíc na to najchúlostivejšie miesto, kde si chlapi schovávajú svoj verzajch a niečo ružovo-hnedej farby stadiaľ vyťahuje. Každý čumí jak puk. A ja za dvoch. 
 Dvíha ma z fotelky. Už-už idem vykríknuť od zhrozenia nad týmto odsúdenia hodným exhibicionistickým prejavom, keď však mladý provokatér si priloží to „čosi" k ústam a z chuti do toho zahryzne. S ohromením zisťujem, že týpek si pochutnáva na stehne vysmážaného kuraťa. 
   
 Po tom všetkom sa Pamelin agent zmohol iba na strohé: „Pam was shocked." 
 K tomu zostáva dodať už len jedno: Pam, chytaaaaaaj! 
   

