

 
Takže... na začiatku si vypočujeme telefónny hovor medzi nejakou urevanou ženskou, ktorá volá na políciu a ohlasuje nejaký zločin. Síce neviem aký, ale vraj je tam plno krvi, atď. veď to poznáte. 
 
 
Potom uvidíme záber na párik (už neviem, ako sa volali, hovorme im napríklad John a Jane), ktorí, ako sa neskôr dozvedáme, idú zo svadby Johnovho kamaráta, kde John požiadal Jane o ruku a ona odmietla. Čo v takom prípade robiť? No jasné, treba ísť depkovať do chaty na samote v lese, ktorá patrí Johnovmu otcovi. Chvíľu sledujeme nejaké bla-bla-bla medzi nimi, potom Jane pošle Johna kúpiť niečo do mesta (už si nepamätám čo). Zrazu niekto zabúcha na dvere, John otvorí a tam stojí nejaká ženská, ktorej nie je vidieť do tváre a pýta sa: - Je doma Tamara?- John odpovedá: -Nie, tu žiadna Tamara nebýva.- Žena: -Ste si istý?- John: -Áno, to ste si asi pomýlili dom.- Žena teda odíde, John odíde do obchodu a Jane zostane sama. Niekto búcha na dvere. A zase žena, čo hľadá Tamaru. "To už je nejaké divné", pomyslí si asi Jane a zamkne dvere a chystá sa zavolať Johnovi. Ale telefón je čo? Vybitý. Tak ho dá do nabíjačky a ide sa rozhliadnuť po dome. (poznámka: v amerických filmoch telefón pripojený k nabíjačke nefunguje) 
 
 
No a tu začína hororový dej. V dome začne pípať požiarny hlásič, Jane ho rozbije a nechá na zemi. Po dome sa pohybuje nejaká divná postava v maske, niekto rozbije okno, Jane začína byť hysterická, atď. Medzitým sa vráti John a Jane mu hovorí, že sa bojí a zrazu zistia, že im mobil niekto z nabíjačky vybral a hodil do ohňa a rozmlátený požiarny hlásič zobral zo zeme a položil na stôl. Bububu, no naozaj desivé. Teda, toto bola v podstate najlepšia časť filmu. Pokiaľ som nevedel, kto to vlastne na dvojicu útočí a prečo, bolo to celkom napínavé a keby si toto film udržal po celú dobu, tak by sa to aj dalo pozerať. 
 
 
Ale poďme ďalej. O chvíľu J&amp;J niekto začne rozbíjať dvere sekerou, a to už im dôjde, že to nie je sranda. Tak dvere zavalia a buchot prestane. To už J&amp;J začínajú mať taký nejasný pocit, že by im mohol chcieť snáď niekto ublížiť, alebo čo, tak hľadajú telefón, ale ten už horí v kozube. Aj tak nefungoval, veď bol v nabíjačke. Ale čo to? John nájde v dome brokovnicu. Niekto by si mohol povedať, že chvalabohu, ale John a náhoda sú blbci, takže John odbachne svojho kamoša zo svadby, ktorý sa tam medzitým dovalil. John je síce blb, ale strieľa dobre, takže kamoša trafil rovno do ksichtu. Po tejto srande už začínajú J&amp;J panikáriť a pobehovať a dostanú geniálny nápad, že by mohli napríklad ujsť na kamošovom aute (to ich predtým niekto rozbil, to som zabudol povedať, ale Johnovi to neprišlo nejako divné). Nasadnú preto do auta, ale 50 metrov pred nimi sa objaví postava v maske, čo J&amp;J tak konsternuje, že John zabudne ako sa šoféruje a potom do nich zozadu napáli pickup. Útočníkov je viac!!!! No ty koki, to som nečakal. 
 
 
Ok, už to skrátim, nebojte sa. Bežia naspäť do domu, John beží do kôlničky k vysielačke, ale niekto ho ovalí po hlave papekom. Veď čo je pumpovacia brokovnica proti takému konáru z brezy. Jane zatiaľ pobehuje z domu, do domu a z domu ako taký Osmijanko. Potom sa zavrie do šatníka. Takého toho so žalúziami, ktoré sú priehľadné, ale iba zvnútra. Odtiaľ vidí, že po dome chodia nejakí týpkovia v maskách, ale oni o nej akože nevedia. Potom sa jej ale začnú dobíjať dovnútra, ale tieto šatníky vydržia viac ako dvere protiatómovom bunkri, takže sa na to vykašlú. Takže Jane asi o 5 sekúnd vyjde von, lebo si myslí, že vrahovia išli otravovať nejakú inú dvojicu na inú samotu do iného lesa. Strih. 
 
 
J&amp;J sedia zviazaní na stoličkách, oproti nim stoja tri postavy v maskách - muž (ten má celkom dobrú masku, vyrobenú akoby z plesnivého vechťa, podobne ako Scarecrow v Batmanovi) a dve asi-ženy. Tie už nemajú také super masky, ale skôr také, aké si môžete vystreliť vzduchovkou od kolotočára. J&amp;J sa pýtajú: -Prečo my, fňuk fňuk?- Asi-žena č. 1: -Lebo ste boli doma.- Takáto logická odpoveď by zobrala vietor z plachiet asi každému, takže bez dlhých rečí Johna a Jane nožmi dobodajú a odídu na aute. Medzitým si dajú dole masky, ale aj tak im nie je vidieť do tváre, takže nič. Ešte je tam jedna scéna, ktorá indikuje pokračovanie tohto supa-dupa filmu a to je koniec. 
 
 
********** 
 
 
Napriek tomu, že film má cca. 70 minút, zdalo sa mi, že som pri tom strávil pol dňa. Už som dlho nevidel toľko hororových klišé pokope, zároveň s absenciou deja a najmä akejkoľvek pointy. "Prišli-nastrašili-zabili-odišli" za dej nepovažujem. Navyše vrahovia disponujú tými klasickými slasherovými nadprirodzenými schopnosťami, ako je napríklad teleportácia, neviditeľnosť, jasnovidectvo, nočné videnie a podobne. A že vraj je to podľa skutočnej udalosti. Pche... Keď si chcete pozrieť podarený horor podľa skutočnej udalosti, pozrite si radšej ILs (po anglicky tuším "Them"). Američania už totiž originálne horory nakrúcať nevedia, iba ak remakeovať Francúzov a Aziatov. Škoda. 
 

