

   
 Ja za to nemôžem, že som taká prchká, hádavá nátura, ja sa pohádam aj s ministrom, keď treba. Druhá by ma možno už dávno otrávila, v lepšom prípade by odo mňa hneď po svadbe zutekala. A moja žena? Ona je pravý opak - mierna, tichá, trpezlivá. Ako mohla taká vzácna duša doteraz vydržať s takou hašterivou povahou, akou som ja, há? 
 Vieš, koľko som sa jej narobil hanby, problémov, príkoria a ona to všetko anjelsky znášala - chcel by som mať také nervy, aké má ona. Predstav si, že my spolu žijeme už vyše tridsať rokov! Veril by si tomu? Dvoch synov sme spolu vychovali - hlavne moja žena! 
 Ja viem, že si takú manželku nezaslúžim, druhý chlap by takú ženu nosil na rukách, a ja, frfloš, prídem domov, šomrem, hundrem, furt sa nájde dačo, s čím nie som spokojný. Videl si už dakedy takého somára? Vysvetli mi, prečo som ja mal také šťastie v živote, prečo si moja žena vybrala práve mňa? 
 Vieš, ona je inžinierka, číta knižky, cvičí, dbá o seba a pozri na mňa - čert je oproti mne fešák. Poznáš ma dobre, ja nie som náročný človek, ja v živote toho veľa nepotrebujem! Je pravda, viem zarobiť peniaze, ale viem ich aj minúť. Každý mesiac jej položím celú výplatu na stôl a zvyšok, čo zarobím bokom, to všetko miniem na život. Však všetko treba v živote vyskúšať, nie? A moja žena vie, že mne je zbytočne niečo zakazovať, ja si vždy budem žiť podľa svojho! 
 No, ona tiež dobre zarába, bez môjho súhlasu by nezobrala z mojich úspor ani cent. Nemusím pred ňou nič schovávať, nebojím sa, že ma okradne, oklame. Ona má takú poctivú povahu jak jej nebohá matka - varí mi, šatí ma, perie na mňa. 
 Vieš čo, priznám sa ti, ja ju ľúbim a ona ľúbi mňa. Už sme si na seba zvykli, už to spolu doklepeme - ja do pekla a ona do raja!" 
   

