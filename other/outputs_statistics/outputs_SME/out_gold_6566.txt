

    
 Na potulkách rovinkou holandskej metropoly sme sa pokochali s priateľom na vydarených kúskoch pouličných umelcov, talentovaných hudobníkov a nadchýňali sa veteránskymi bicyklami z roku päť. Povymetali sme  zastrčené coffee shopy. Poblúdili v úzkych uličkách. Povadili sa kvôli našej do neba volajúcej dezorientácii. Bolo už neskoré popoludnie, keď sme sa opäť ‚našli', udobrili, pomojkali a urobili zopár spoločných romantických fotiek na klenbových tehlových mostíkoch. Vtedy sme už išli  odpadnúť od únavy a zložili sme sa preto na najbližšom brehu kanála. 
   
 Pred nami sa rozprestieral asi ten najluxusnejší Grand Hotel, ktorého noblesa zaváňala až k nám. Lemoval ho kanál, ktorého hladinu kedy-tedy zvírila brodiaca sa loďka. Medzitým sa zvečerilo. Terasy hotela sa vysvietili. Prichádzali hostia, zväčša spoločenská elita, ktorej večerné spoločenské šaty boli práve tak nápadné ako uhladené rovnošaty čašníkov. Dozaista si pochutnávali na kaviári a upíjali z namiešaných kokteilov. Spokojne si hoveli pri vkusne prestretých stoloch nevšímajúc si okolie. Jediní, ktorí toto všetko registrovali, sme boli my, dvaja outsideri pregĺgajúci na sucho na opačnej strane kanála. 
   
 Večer pokročil. Nastalo príjemné ticho. A keď sa už zdalo, že nás nič nemôže z tohto pokoja vytrhnúť, vynoril sa z ničoho nič z tmy motorový čln s mužskou posádkou. Na palube bolo asi pätnásť mladých mužov. Vyzerali ako prvoligové mužstvo, ležérne sa vyvaľujúce na doskách. Keď sa čln začal približovať k hotelu, statní fešáci sa ako na povel postavali jeden vedľa druhého do rovnej línie tvárou k nám a obyvateľom hotela chrbtom. A v tom sa to stalo. Chlapíci si znenazdajky všetci do jedného stiahli nohavice a otrčili VIP pohľadom holé zadky. 
 Na drzovku! A namiesto toho, aby hotelové ladies pohrúžené do exkluzívnych rozhovorov sa rozhorčili a ohrnuli svoje nosy nad takou nehoráznosťou, vyskočili zo svojich mäkkých čalúnených stoličiek a plné rozčarovania a nemého úžasu začali aplaudovať! 
   
 Trvalo to presne jeden a pol sekundy, kým mne a môjmu priateľovi neurón preniesol onen nervový vzruch do šedej mozgovej kôry, a my sme si uvedomili, akému divadielku sme boli svedkami. Pozreli sme sa na seba a prepukli do zadosťučiňujúceho smiechu. Po tom, čo sa čln odrachotil preč a hostia opäť klesli na svoje stoličky, zavládol pokoj. Len my dvaja sme sa roztržito hniezdili na vyhriatom asfaltovom obrubníku a vykúkali ďalšie rozptýlenie. 
   
   

