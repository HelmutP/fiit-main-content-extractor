
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Šátek
                                        &gt;
                Súkromné
                     
                 Účelová politická manipulácia s „praním špinavých peňazí“. 

        
            
                                    1.2.2010
            o
            17:21
                        (upravené
                1.2.2010
                o
                18:04)
                        |
            Karma článku:
                13.84
            |
            Prečítané 
            2933-krát
                    
         
     
         
             

                 
                    Vopred treba zločincovi preukázať, že určitým zločinom získal majetkový prospech  -financie alebo vec - a až potom ho možno obviniť, že tento majetok rôznym spôsobom  „prepral“ čiže po „zlegalizovaní“ ho používal na rôzne súkromné účely. Spôsoby získania príjmov z trestnej činnosti môžu byť rôzne - podvod, sprenevera, korupcia, poskytovanie dôverných informácií, vydieranie, daňové úniky ... Tvrdiť však najprv, že niekto „perie špinavé peniaze“ a až potom veľmi mlhavo hovoriť odkiaľ a akým spôsobom  ich získal je právny nezmysel alebo účelová politická manipulácia.
                 

                Z právneho hľadiska je prakticky bezvýznamné pre účely trestného konania tvrdenie premiéra R. Fica, že, citujem : „Existuje vážne podozrenie, že SDKÚ vyviezla do zahraničia provízie z privatizácie strategických podnikov, prikryla ich fiktívnymi firmami a dnes ich používa na financovanie svojich aktivít.“ Toto jeho tvrdenie by malo dokazovať „zdroj“ nelegálnych majetkových príjmov ? Ako docent trestného práva musí vedieť, že na základe tejto informácie nie je možné ani začať trestné stíhanie vo veci podozrenia niektorého z trestných činov uvedených v osobitnej časti Trestného zákona. Okrem toho, že si to R. Fico myslí, nepredložil žiadne konkrétnejšie dôkazy, o ktoré svoje podozrenie opiera, ale ani základné informácie o skutku k získaniu "provízií" - kto, kde, kedy, akým spôsobom, z akých dôvodov, s akou škodou. Tendenčnosť „investigatívne“ poskytovaných  informácií predsedom vlády sa prejavila ihneď potom, čo na protiotázku položenú redaktorom k podvodne predaným emisiám stroho a bez akýchkoľvek bližších vysvetlení tvrdil, že v tomto prípade sa nejedná o pranie špinavých peňazí. Už na základe predchádzajúceho vysvetlenia je zrejmé, že práve s kauzou predaja emisií sú verejnosti známe všetky potrebné informácie pre úspešné vedenie vyšetrovania – základným zdrojom nelegálne získaného majetkového prospechu sú podozrenia z konkrétnych zločinov – založenie, zosnovanie a podpora zločineckej skupiny a sprenevery. Majetkový prospech vygenerovala zločinecká skupina podvodným znížením kúpnej ceny pod jej reálnu hodnotu a následne takto získaný rozdiel (zatiaľ škoda na štátnom majetku cca 66 mil. EUR - 2 miliardy Sk) si členovia tejto zločineckej skupiny práve „legalizáciou príjmov z trestnej činnosti“ preprali do svojho osobného vlastníctva. Treba sa niekedy vrátiť aj do minulosti a to nie až tak dávnej. Začiatkom roka 2004 Úrad boja proti korupcii obdržal trestné oznámenie poslanca NR SR doc. JUDr. R. Fica, CSc. Po svojej tlačovej konferenciii podal oznámenie pre podozrenie z korupcie a sprenevery na presne neurčené osoby a vedenie stredoslovenskej energetickej spoločnosti, ktoré mali na základe rôznych zmlúv dcérskym spoločnostiam patriacim strategickému vlastníkovi ročne uhrádzať sumu presahujúcu niekoľko desiatok miliónov korún za plnenie „triviálnych úloh a inú pomoc“.  Oznamovateľ ani pri podanom vysvetlení nepodložil svoje podozrenie - oznámenie žiadnymi konkrétnymi dôkazmi. Po vykonanom vyšetrovaní trestné stíhanie bolo zastavené, pretože skutok nebol trestným činom.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Šátek 
                                        
                                            Politické policajné manažmenty.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Šátek 
                                        
                                            Takto chcú "vyšetrovať" Gorilu ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Šátek 
                                        
                                            Gorila žije a bude žiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Šátek 
                                        
                                            Gorila ako King-Kong II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Šátek 
                                        
                                            Gorila ako King-Kong I.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Šátek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Šátek
            
         
        satek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Celý svoj produktívny vek som venoval policajnej práci a túto som bral ako životné poslanie. Počas 27 rokov vyšetrovateľskej praxe mi prešlo cez ruky tisícky vyšetrovaných prípadov, najmä tých najzložitejších. Dvakrát som za nekompromisné postoje narazil na tvrdý politický odpor. Prvý krát v roku 1995, kedy ako šéf úradu vyšetrujúci "zavlečenie prezidentovho syna" som bol jedným z mnohých, ktorí museli počas lexizmu z polície dobrovoľne "odísť". Druhý krát už po opätovnom návrate, keď ako riaditeľ Úradu boja proti korupcii som "nevyhovoval" vtedajšiemu ministrovi Palkovi. A opäť to bolo z mojej strany "nepochopením" trestnoprávnej nedotknuteľnosti určitých ľudí. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    102
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7134
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako Zuza v Nemecku bola u lekára
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




