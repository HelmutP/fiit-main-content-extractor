

 The Black Balloon je austrálska dráma z roku 2008, ktorá opisuje príbehy jednej štvorčlennej rodiny. Otec, profesionálny vojak, matka, tehotná žena v domácnosti, pubertiak a autista. Skutočne nevšedná kombinácia ľudských charakterov. 
 Hlavnou postavou je 15-ročný Thomas Mollison. Kvôli otcovej profesii sa rodina neustále sťahuje a Thomas musí opäť raz začínať v novej škole od nuly. Nepomáha mu v tom ani jeho brat Charlie, ktorý je autista. Postupne sa v škole zbližuje s krásnou Jackie, ktorá mu pomáha prekonať bariéru medzi ním a jeho bratom. 
 Musím povedať, že tento film bol výborným vykreslením ľudských osudov a hereckým koncertom všetkých predstaviteľov na čele s Toni Collette. Tá v úlohe tehotnej matky, ktorá sa starala o postihnutého syna doslova excelovala. 
 Biela vrana ma donútila premýšľať o živote a o životných hodnotách, rodine... Myslím si, že na titulkách všetkých novín a časopisov by sa nemali objavovať všetky pekné tváričky, ktoré sa mihnú na obrazovke. Mali by tam byť ľudia, ktorý si to zaslúžia. Zaslúžia si pozornosť, obdiv, úctu a ich príbehy dokážu viesť ľudí k pomoci iným. Hovorím o ľuďoch, ktorí sa starajú o chorých, postihnutých, autistov... 
 Priznám sa, že som nemal ani potuchy o tom, koľko starostlivosti a lásky taký človek potrebuje. Neviem, ako by som reagoval na mieste Thomasa, možno by som si aj ja stále želal, aby bol Charlie normálny, ale táto dráma poukázala na to, čo je podstatné. Zmieriť sa so skutočnosťou a žiť podľa nej. Obetovať sa pre tých, čo to naozaj potrebujú. 
 Vrelo tento film odporúčam. Myslím si, že by ho malo zhliadnuť čo najviac ľudí, aby sme si uvedomili, že aj takíto ľudia žijú medzi nami. A sú to taký istý ľudia ako my, len žijú vo svojom vlastnom svete. Biela vrana mi doňho dovolila nazrieť... A zmenila môj pohľad na svet... 
 LoBo 
   

