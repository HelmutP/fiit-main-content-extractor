

 Deň po voľbách, kedy sa SaS celkom slušne nedostala do Europarlamentu, nás bývalý premiér Mikuláš Dzurinda obvinil, že oslabujeme pravicu. Napriek tomu, že to nebola pravda (aj keby všetky hlasy SaS získala SDKÚ, mala by pravica naďalej šesť poslancov v EP), nemáme záujem pravicu rozbíjať. Práve naopak, všetci, ktorí len trochu ctia slušnosť a zdravý rozum, sa musia spojiť, lebo dnešná takzvaná ľavica, je pre Slovensko hotová pohroma. 
 Preto sme prejavili záujem ísť v najbližších voľbách - to je voľbách do VÚC - spolu s SDKÚ a prípadnými inými stranami v koalícii. Konkrétne v Bratislave sme SDKÚ navrhli, že sa pridáme k existujúcej koalícii SDKÚ, KDH, SMK a OKS. Bratislavská VÚC bude mať 44 poslancov, tieto strany teda postavia 44 kandidátov a rozdelia si ich takto: 
 
  SDKÚ (18 kandidátov) 
  KDH (15 kandidátov) 
  SMK (6 kandidátov) 
  OKS (5 kandidátov) 
 
   
 SaS bola v bratislavskom kraji v posledných voľbách (v júnových Eurovoľbách) s výsledkom 10,24% treťou najsilnejšou stranou, KDH získala 8,51% a SMK spolu s OKS/KDS získali 8,19% hlasov, čiže tiež menej ako my. 
 Samozrejme že si uvedomujeme, že máme za sebou len jedny voľby a preto sme mali snahu prezentovať triezve postoje, čo v tomto prípade znamenalo, že sme požadovali šesť miest na kandidátke a nové rozdelenie mohlo byť: 
 
  SDKÚ (16 kandidátov) 
  KDH (13 kandidátov) 
  SaS (6 kandidátov) 
  SMK (5 kandidátov) 
  OKS (4 kandidátov) 
 
 Mali by sme teda menej než polovicu mandátov ako KDH a to napriek lepšiemu volebnému výsledku v posledných voľbách a mali by sme o tretinu menej mandátov ako SMK a OKS/KDS, tiež pri lepšom výsledku ako tieto tri strany spolu. 
 Sme presvedčení, že požadovať 6 zo 44 mandátov je pre stranu s tretím najlepším výsledkom primerané, no bohužiaľ to takto nevnímajú naši takmer koaliční partneri a návrh odmietli. Nie sme z toho zúfalí, pre nás by to bol dosť veľký kompromis a tento článok píšem len z jedného jediného dôvodu: 
 Keďže sa pri voľbách do VÚC jedná o väčšinový volebný systém, môže sa stať, že ostatné pravicové strany stratia mandáty a už teraz dôrazne odmietame akékoľvek obvinenia z nedostatočnej snahy o spoluprácu. 
 Čakal som veru viac politickej zrelosti. Tiež som čakal viac zásadovosti. Mikuláš Dzurinda totiž opätovne vyhlásil, že SDKÚ so SMERom vládnuť nebude, lebo by to bol mačkopes. To, čo platí možno pre Slovensko, neplatí pre Nitriansky kraj, lebo ten zjavne nie je na Slovensku, ale asi na Marse. V Nitrianskom kraji ide SDKÚ do koalície práve so SMERom a tam už nikomu nevadí, že "na Slovensku panuje veľká politická nekultúra namiesto slušnosti, je tu veľa arogancie, násilia, namiesto dialógu, vládne populizmus namiesto zodpovednosti, nacionalizmus namiesto vlastenectva, korupcia a netransparentnosť, lobizmus oproti transparentnému správaniu sa" (citát M. Dzurinda). 
 Ako dôvod koalície so SMERom uvádza SDKÚ nutnosť slovenskej koalície proti SMK, nemá ale najmenší problém s tou istou SMK ísť do koalície v susednej župe. Naopak, kúsok na sever, v Trenčíne, je všetko inak. Tam bude SDKÚ koalovať s HZDS a tak ďalej. Nuž, zásadovosť vyzerá inak a preto som ja osobne celkom rád, že SaS možno ako jediná strana bude na poslancov VÚC vo všetkých krajoch dôsledne kandidovať sama. 
 To sa však netýka županov. Keďže za župana bude zvolený len kandidát s najväčším počtom hlasov,  nemá zmysel, aby sme stavali vlastných kandidátov na županov a preto podporíme toho, kto nám je blízky. V Bratislave je to Pavol Frešo, v Bystrici Jozef Mikuš a v Košiciach Ján Süli, všetci SDKÚ. V ostatných krajoch uvidíme. 
   

