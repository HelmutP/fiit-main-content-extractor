
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Menyhertova
                                        &gt;
                Nezaradené
                     
                 Už si môžete sadnúť! 

        
            
                                    16.6.2010
            o
            19:30
                        (upravené
                16.6.2010
                o
                20:52)
                        |
            Karma článku:
                9.66
            |
            Prečítané 
            789-krát
                    
         
     
         
             

                 
                    Zistila som ze som taky motýľový typ. Vždy, deď sa niečo udeje, čo mi mierne vyrazí dych, tak mám motýle v žalúdku.
                 

                 Z práce som cestovala prostriedkom MHD. Bola to fakticky džungľa. Bohužial. Vydýchaný vzdruch, mokré a spotené telá, veď prší.  Kedže sa obvykle veziem iba niekoľko málo zastávok, snažím sa takúto cestu prečkať nečujne, bez dychu a zväčsa aj na jednej nohe. Sťažovať sa nebudem, nieje komu, všetci sú na tom rovnako. Veď sťažovateľ sa zpravidla vždy nájde, tak to ja už nekomplikujem.   Ako inak , našiel sa. Veľmi distingvovaná pani, nie staršia ako 50 rokov sa už asi 2 minúty ako som nastúpila ja, osopovala na mladého chalana, aby sa postavil, a pustil ju sadnúť. On aj tak celý deň len vysedával v škole. Na počudovanie, chlapec, ktorého sa tieto hlasné komentáre týkaly ani okom nemihol, a pozeral sa opačným smerom cez okno.   Pridali sa ďalší "dobrý" ľudia a suché komentáre vystriedala veľmi živá diskusia. "Tá mládež, čo sú to za spôsoby, nevychovanci drzí jedny atď.  Hovorím si, pravda, nič by sa nestalo, ak by mladý uhol a tetušku pustil sadnúť. Slová ako nech sa páči a ďakujem sa v poslednom čase akoby prestali nosiť. Nuž nedá sa nič robiť, je to asi aj výchovou a aj povahou.   Zastavili sme na zastávke. Chlapec vystúpil. Autobus stál asi ešte niekoľko sekúnd na zastávke, aby pustil chodcov na prechode a vtedy sa to stalo. Po tej vrave, čo rozpútala tá milá tetuška ani stopy. Aj ona stála a bola biela ako krieda.   Všetci sme videli toho mladého chalana, ako kráča po druhej strane ulice a na nohe mal protézu. Bolo to zjavné, bol fyzicky postihnutý. Nič nepovedal, neoháňal sa svojím nešťastím na každom kroku, aby všetci videli, že mu majú dať prednosť, alebo uvolniť miesto. Len sa tichučko viezol tým MHD, bez slova. Moje žalúdkové motýle sa vlnili ostošesť.   Hanbila som sa za svoje myšlienky, došlo mi, že nie všetci ľudia aj keď sú mladí, tak sú aj zdraví. A niekedy to nieje ani v prvej chvíli poznať, kde je pes zakopaný. Myslím, že aj tá tetuška sa hanbila, lebo vzdorovito stisla pery a čušala.   Iba taká mladá slečna asi vo veku toho chalana sa na ňu s odporom dívala a potom hlesla: Tak, a už teraz ste spokojná?! Môžete si sadnúť, veď vám uvolnil miesto!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (63)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Mužolapka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Rytmus a jeho kecy....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Závisť, alebo prehnané ego?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O  cintoríne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O delení ministerstiev...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Menyhertova
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Menyhertova
            
         
        menyhertova.blog.sme.sk (rss)
         
                                     
     
        som človek s chybami.....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    971
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Výnimočná
                                     
                                                                             
                                            Timea Keresztényiová :Sama mama
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lara Fabian
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




