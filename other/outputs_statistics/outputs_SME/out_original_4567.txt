
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kolísek
                                        &gt;
                Súkromné
                     
                 Zázračné byliny – VI. 

        
            
                                    14.4.2010
            o
            19:38
                        |
            Karma článku:
                1.59
            |
            Prečítané 
            388-krát
                    
         
     
         
             

                 
                    Pokračování v popisu účinků byliny NONI - Morinda citrifolia. Poklepete-li 2x na moje foto otevře se můj soukromý blog a  zde jsou všechny články které jsem zatím vyprodukoval.
                 

                   
 To je skvělé pro rostlinu, ve které jsou tyto alkaloidy produkovány. Přivedeme-li však tyto alkaloidy do našeho těla, dostaneme se k zajímavým otázkám. Tyto alkaloidy jsou v rostlině zcela neaktivní. Jelikož nám jsou ale naprosto cizí a jejich struktura je podobná struktuře xeroninu, jsou našimi proteiny jako xeronin vnímány a také akceptovány. Jelikož člověk např. kouří cigaretu, dostane se do těla velké množství volného nikotinu. Ačkoliv je nikotin v tabákové rostlině neaktivní, podobá se struktuře xeroninu natolik, že se mu podaří oklamat proteiny v našem těle a ty ho pak akceptují spíše,než xeronin jako takový, který tělo přirozeně potřebuje. Pokud nikotin převezme v proteinu pozici xeroninu, zaktivuje ho, a ačkoliv je to nežádoucí, tento protein se, stejně jako xeronin, stává málo efektivním. Jelikož však má stále ještě podobnou strukturu jako základní struktura xeroninu, je nadále s to aktivovat protein, avšak činí to o mnoho hůře, a to z důvodu molekulárního odpadu, který tomu brání. Pokud člověk pokračuje v kouření, a tím i v příjmu nikotinu do svého těla, může se tělo nikotinu eventuelně přizpůsobit. A to tím, že mírně pozmění formu proteinů, aby pasovala spíše i nikotinu než ke xeroninu, je molekulární báze jakékoliv závislosti. Touha po cigaretě se pak neustále zvětšuje, neboť' spousta proteinů v těle doslova vyžaduje molekuly nikotinu, tak jak kdysi vyžadovala molekuly xeroninu, aby vůbec mohly fungovat. Čím víc člověk kouří, tím více proteinů je přeměněno z xeroninových proteinů na proteiny nikotinové. Tím je také stále těžší se této závislosti zbavit. Když se rozhodne přestat kouřit, je to velice bolestivé a těžké, neboť' sousta proteinů v těle, kterým nikotin schází, nyní není schopna své funkce. To je důvodem projevů abstinence po kouření. Je možné, že se vaše proteiny, pokud přerušíte tělu přísun nikotinu, opět přizpůsobí na xeronin, který je v těle k dispozici. Vy se pak vrátíte do původního normálu a nebudete více psychicky závislí na nikotinu. Stejný proces platí také pro cizí alkaloidy, které svému tělu přivádíme, včetně kofeinů, kokainu, heroinu, morfinu, atd. Pokud tyto alkaloidy přijmeme, naše proteiny se jim přizpůsobí a my přejdeme od přírodní potřeby xerninu k umělé potřebě cizích alkaloidů. Důvod, proč jsme po přijetí těchto alkaloidů "vitální" je jednoduchý. Jelikož tyto cizí alkaloidy jsou našim xeroninům natolik podobné, že nás oklamou, dovolují proteinům dále provádět jejich práci. Pokud své tělo zaplavíme těmito cizími alkaloidy (aplikací nějaké drogy), vybudí tyto u našich proteinů více aktivity, než je běžné. Tím je také vyvolán pocit euforie. Rozličné drogy se chovají různorodým způsobem díky rozdílnému podílu molekulárního odpadu, kterým jim byl dodán. Kokain na tělo působí jinak, než morfin, díky malému rozdílu jejich struktury, což omezuje nebo zesiluje určité aspekty přirozených funkcí xeroninu. Klíč k tomu spočívá v tom, že jednoduše imitují všechny přirozené funkce xeroninu. Spousta alkaloidů je běžně užívána jako drogy ve farmaceutickém průmyslu, avšak veškerý farmaceutický účinek těchto cizích alkaloidů je roven přirozeným funkcím xeroninu. Kdybychom znali skutečnou povahu těchto alkaloidů a proces jak se dostaneme do jejich závislosti, mohli bychom lidem lépe pomoci překonat jejich drogovou závislost. Mohli by jste se snad zeptat, jak to, že je tak snadné stát se na něčem závislým, a přesto tak těžké se takové závislosti zbavit? Odpověď' je jednoduchá, pakliže pochopíme pravou podstatu takové závislosti. Když si poprvé vezmeme nějakou drogu jako např. heroin, zaplavíme svůj organizmus těmito cizími alkaloidy. Obrovský příval heroinu do krve přemůže malé množství přírodního xeroninu a přemění tím velmi rychle mnoho proteinů. K překonání těchto potíží a k doslovnému "vyléčení" závislosti, musíme učinit to, že zaplavíme svůj organizmus xeroninem, stejně jako jste to učinili zprvu se zaplavením vašeho organizmu cizími alkaloidy. Pokud to učiníte "vrátíte se" opět na xeronin, přičemž se můžete vyvarovat všech problémů s abstinencí. Podle této teorie by mělo být možno překonat drogovou závislost stejně rychle, jako vznikla, a to bez jakýchkoliv projevů abstinence  ( snad již dokonce během 1-3dnů), pokud je tento proces proveden korektně. Pro zbavení se závislosti pomocí proxeroninu, by jste tento proxeronin museli nějakým způsobem dostat do krve dříve, než do zažívacího traktu. Jedna z možností, jak byste toto mohli provést, spočívá v tom, že by jste museli každou hodinu kapátkem pod jazyk nakapat několik kapek prostředku Tím se proxeronin dostane z jemné tkáně pod jazykem přímo do krve, a to dříve, než do zažívacího traktu, kde je jeho další dávkováni přísně regulováno játry. Spolu s účinností coby léčivý prostředek při závislostech, nám znalost úlohy xeroninu v těle přináší také spoustu možností dalšího využití. Jedna z možností využití Noni je jeho potenciál v kosmetických produktech. Jak již bylo vysvětleno, tvoří játra podstatnou zásobárnu proxeroninu v těle. Druhou velkou zásobárnou proxeroninu je pokožka. Proxeronin je na xeronin přetvářen v celém těle a pokožka netvoří žádnou výjimku. Abychom si pokožku zachovali zdravou a hladkou, musí obsahovat dostatečné množství proxeroninu. Nedostatek proxeroninu v pokožce může mít za následek nezdravou pokožku a spoustu dalších kožních problémů. Proxeronin je také zapotřebí k udržení zdravých vlasů a pokožky hlavy. Dostatečným přísunem proxeroninu do pokožky hlavy a do vlasů docílíte viditelného zlepšení, pokud zde existují nějaké nedostatky. Další velmi zajímavou oblastí využití xeroninu je jeho použití při tišení bolesti a jako anestetika. Silnými prostředky, které tiší bolest, jsou v současnosti cizí alkaloidy jako morfin a kodein. Již jsme se dozvěděli, že cizí alkaloidy nejsou nic jiného, než imitace xeroninů, které jsou vytvářeny rostlinami za účelem jejich uskladnění. Proto xeronin dokáže všechno to, co dokáží tyto cizí alkaloidy, a to lépe a přírodní cestou. Xeronin působí v těle jako nejefektivnější tlumič bolestí, neboť' v těle pracuje s endorfiny, aby společně omámily bolesti a vyvolaly bezbolestné pocity. Endorfiny jsou hormony, které jsou v těle zodpovědné za příjemné pocity. Vážou se na určité proteiny, stejně jako to dělá xeronin. Když se na protein naváže xeronin a endorfin, převede xeronin do endorfínu energii z vody a endorfin způsobí to, že se člověk cítí dobře. Věda stále ještě zkoumá, jak endorfín tento dobrý pocit vyvolává. Avšak víme již, že to jsou právě ony, kdož jsou za to zodpovědné. Ať je to jakkoliv, bez xeroninu jsou endorfiny neprospěšné. Xeronin díky svým vlastnostem tiší bolesti. Zároveň působí také jako stimulátor.  Stimulační efekt xeroninu může vyvinout neuvěřitelný potenciál. Může zřetelně zvýšit výkon atletů, stejně jako schopnost vaší koncentrace a jasného myšlení. Vyjmenovali jsme zde pouze několik oblastí využití z obrovského potenciálu, který proxeronin a xeronin skýtá. Plod NONI pro svoje úžasné účinky které má, tedy doplnění proxeroninu, čímž ovlivní kladně imunitní systém, ale i jiné oblasti těla a  přestože byla uvedena celá řada pozitivních účinků, možnost využití NONI nejsou zdaleka vyčerpány.   Konec  popisu účinků NONI. Další zázračná bylina - Klanopraška čínská - Schizandra chinensis.   Kolísek /senior/.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Neobvyklá příležitost.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Povinné svícení automobilů a ekologie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Na kolik si ceníte svoje zdraví ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Vytvoření vlastní podprahové audio nahrávky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Zázračné byliny – VII.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kolísek
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kolísek
            
         
        kolisek.blog.sme.sk (rss)
         
                                     
     
        Můj zájem je především zdraví a to jak fyzické tak i psychické. Sestavil jsem unikátní regenerační zařízení a i z vlastní zkušenosti tvrdím, že prokazatelně uzdravuje lidi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    451
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




