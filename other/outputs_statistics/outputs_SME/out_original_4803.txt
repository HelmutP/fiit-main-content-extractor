
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Jakubec
                                        &gt;
                Nezaradené
                     
                 Žilinská cenotvorba 

        
            
                                    2.9.2010
            o
            8:55
                        |
            Karma článku:
                9.68
            |
            Prečítané 
            2695-krát
                    
         
     
         
             

                 
                    Rozpútala sa hystéria okolo cien lístkov na zápas ligy majstrov proti Chelsea FC. Vlna povrchnej kritiky podľa mňa dosiahla neúnosnú mieru.
                 

                 Na prvý pohľad je to jasné, nenažraný klub, chudáci faúšikovia. Situácia má ale oveľa viac aspektov ako sa môže zdať.   Na úvod vyberám percento, ktoré v zahraničných (rozumej vyspelých) kluboch predstavuje podiel príjmov z predaja vstupeniek na celkových príjmoch klubu - 30.   Na Slovensku futbal nie je biznis a prijmy prakticky neexistujú, vstupné je nastavené aby krylo možno variabilné náklady spojené s organizáciou zápasu. Percento z príjmov by som odhadol menej ako 5.   Ďalším problémom resp. odlišnosťou je percento vstupeniek predaných dopredu - permanentiek na celú sezónu. V zahraničí sa ich na niektoré kluby bežne predá aj 75 percent kapacity štadióna, niekde sa dokonca dedia (Newcastle). U nás ide o mizivé percento.   Finančné dopady prvých dvoch bodov:   Nízky podiel príjmov z predaja vstupeniek je jeden z faktorov prečo musia slovenské kluby dotovať majitelia a je nutný predaj hráčov.   Nízke percento predaja celosezónnych vstupeniek má za následok nekalkulovateľné príjmy.   Dopady na realitu:   Dotácie majiteľa a predaj hráčov sa dajú hodnotiť vyslovene ako nutné zlo, klub (firma) by si mal na seba zarobiť v ideálnom prípade sám a nebyť závislý od neustáleho predaja aktív a vkladov akcionára. Nepredvídateľné príjmy majú veľmi zlý efekt, na druhej strane k nim má totiž klub až príliš predvídateľné náklady. Platy hráčov, zamestnancov, energie toto všetko je splatné v očakávanej výške a čase. Disproporcia je veľmi zlý jav a musí byť opäť vykrytá majiteľom. Bankové financovanie prevádzkových potrieb klubov je z rovnakého dôvodu komplikované, nie je možné preukázať budúce príjmy.   Dopady na prípadné financovanie výstavby štadióna bankovými zdrojmi:   Štadión je špecifické aktívum, jednoúčelová stavba, využívaná v prípade ligy raz za dva týždne. Jeho kofinancovanie je možné len na základe generovania pravidelného predvídateľného príjmu v prijateľnej výške. Keďže ten neexistuje, táto cesta je zarúbaná.   Naspäť k Žiline.   Klub stanovil ceny pre fanúšikov, ktorí vlastnia permanentky a klubové karty na 50 EUR príslušné tribúny. Ostatní si zaplatia, záujem je. Čo chce Žilina dosiahnuť? Vyšší podiel predikovateľných príjmov z permanentiek alebo z členského. Chcú pravidelného diváka, nie diváka oportunistu. Tých 300 EUR by som nazval výchovnou lekciou žilinskému faúšikovi, aby si na budúcu sezónu kúpil permanentku. Popri tom trh za 300 EUR aj tak vypredá štadión a Žilina bude mať príjem, ktorý jej nahradí absenciu dopredu predaných celosezónnych vstupeniek.   Trošku konkrétnych čísiel:   5000 ľudí vlastní permanentku alebo klubovú kartu   1000 lístkov pre UEFA   1000 lístkov pre hostí   1000 lístkov pre potreby hráčov a klubu   okolo 2500 lístkov pôjde do predaja v režime "300 EUR+"   Podľa klubu na 2500 lístkov existuje 20 násobný dopyt, ich predaj za nižšie sumy by evidentne spustil čierny trh, ktorému chceli predísť.   Z uvedeného evidentne narážame na problém - kapacita štadióna. Tu sa kruh uzatvára, na financovanie zvýšenia jeho kapacity sú potrebné bankové zdroje a tie chcú predikovateľný príjmem.   Záver: Žilina urobila najlepšie ako mohla a potvrdila prvé miesto v koncepčnosti medzi klubmi na Slovensku. V krátkodobom horizonte maximalizujú zisk z predaja vstupeniek segmentu "300 EUR+", ktorý aj tak pravidelne na zápasy Žiliny chodiť nebude. Zdroje môžu byť ideálne použité ako vlastný kapitál na rozšírenie štadióna. V dlhodobom horizonte vykonali lekciu na kúpu celosezónnych vstupeniek, prípadne režim člena klubu a tým posilnil svoje šance na získanie čiastočného externého financovania rozšírenia štadióna bankami.   Nedá mi nespomenúť úryvok z doslova rodinnej tragédie, ktorá sa odohrala po zverejnení cenovej politiky na spomínaný zápas.   Jedna dievčina chcela dopriať svojmu otcovi zážitok kúpou lístka na tento zápas. Po zistení ceny sa takmer rozplakala v diskusii. Rada: radšej mu kúp permanentku na budúci rok   Objavilo sa množstvo kalkulácií cien zahraničnými klubmi na ligu majstrov, žilinské občianske zruženie spísalo list tu. Odporúčam im zaslať ho ešte do Štrasburgu, organizácii human rights watch, organizácii za oslobodenie Tibetu a moskovskej pobočke organizácie Greenpeace.   Nezabudnite "blue is the colour chelsea is the name" a aj keď to nikto nechce priznať, takmer všetci sa idú pozrieť na nich.   Ad médiá: prestaňte podporovať prostú kritiku a viac ten problém vstupeniek analyzujte, všetko čo som čítal bolo naozal len trápne prisluhovačstvo väčšinovému názoru.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (64)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Jakubec 
                                        
                                            GM future
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Jakubec 
                                        
                                            Slovensko - San Marino - test záujmu o reprezentáciu SR
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Jakubec 
                                        
                                            Kam lieta Skyeurope z Bratislavy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Jakubec 
                                        
                                            Rivalita alebo nuda?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Jakubec 
                                        
                                            Gratulujem Hnonline!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Jakubec
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Jakubec
            
         
        jakubec.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1824
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




