
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jiří Ščobák
                                        &gt;
                Nezaradené
                     
                 Robo! 

        
            
                                    13.6.2010
            o
            6:12
                        (upravené
                13.6.2010
                o
                9:54)
                        |
            Karma článku:
                18.26
            |
            Prečítané 
            4483-krát
                    
         
     
         
             

                 
                    Je skoro ráno, já poslouchám Tvoji tlačovku a musím se vyjádřit: Přiznávám, že bych si doopravdy nevsadil, že Smer získá v parlamentních volbách skoro 35%. Odhadoval jsem 27%. Doopravdy je pravdou, že to je téměř o šest procent víc, než před čtyřmi lety. Jsem překvapen. Musím Ti však oponovat. Není to výrazný úspěch. Stejně, jako bych si nevsadil, že Smer získá téměř 35%, nenapadlo by mě, že SNS získá jen 5,1% a do parlamentu proleze doslova s odřenýma ušima. Šest procent, které Smer získal, SNS ztratilo. Vzhledem k faktu, že jsi jim před volbami "kanibaloval" jejich nosnou filozofii, se ...
                 

                 ... určitě nejedná o náhodu. Ty víš (a oni ví), žes je připravil o část voličů. Stejně tak HZDS. Ztratili tolik, že nejsou v parlamentu. Dohromady Tvoji současní koaliční partneři ztratili přes deset procent a Ty jsi z toho dokázal pro sebe získat jen necelých šest. Podívej se na to touto logikou a zjistíš, že záře Tvého úspěchu bledne.  Přesně opačná situace nastala u SDKÚ. Upozornil jsi, že získali o 3% méně, než v roce 2006 a jemně naznačil, že už nejsou tak úspěšní, jako dřív. Je pravda, že mají o tři procenta méně. Jenže zatímco Smer strany SNS a HZDS "kanibaloval" a dnes je bez jednoho partnera (a s druhým silně oslabeným), dokázala SDKÚ "uživit" solidní část voličů SaS a možná i Mostu. Dobře, v roce 2006 měla SDKÚ o tři procenta víc. Jenže dnes nestojí s KDH sama. Je tu SaS s víc než 12% a Most s více než 8%. Pravice je po těchto volbách mnohem silnější, než byla. Má partnery, se kterými si může bez zásadních obav potřást rukou. A to, jaksi, nemůžeš popřít ani Ty.  P.S. Mám na tebo prosbu. Prosím, nebraň se zuby nehty vzniku vlády bez Tebe. Potřebujeme, aby si vyhrnula (ta vláda) rukávy a pustila se do napravování toho, co jsi po sobě zanechal. P.P.S. Oceňuji, že jsi dnes ráno nedštil kolem sebe (a na novináře) oheň a síru. Drž se tohoto trendu dál. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jiří Ščobák 
                                        
                                            Úkol: Udržet F*ca v současném stavu, až do roku 2016
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jiří Ščobák 
                                        
                                            Hypotéza: Pro úspěšné předání genetické informace může být drobná výhoda u žen větší ...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jiří Ščobák 
                                        
                                            Nejpsychologičtější film roku je 196-minutová záležitost a určitě není pro každého
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jiří Ščobák 
                                        
                                            Hypotéza, jak se lidstvo k monogamii dopracovalo
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jiří Ščobák 
                                        
                                            Fotografie z otevřené taneční hodiny Karolíny v Mlynskej doline
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jiří Ščobák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jiří Ščobák
            
         
        scobak.blog.sme.sk (rss)
         
                                     
     
         Zájmy: square dancing (jsem caller), akvaristika, zoologie, archeologie, cestování, fotografování. Doma mám 20 menších akvárií, osmáky degu, ženu a tři dcery. Moje firma organizuje na Slovensku soutěž finančních produktů s názvem Zlatá minca. 
 
Moje nejčtenější blogy: 
■ Jak může vypadat akvárium 
■ Loro Parque - nejkrásnější delfinárium a zoo v Evropě 
■ Boj se sinicí v akváriích 
■ Pomazánka z avokáda 
■ Trocha prachu při bourání Zimního stadiónu O. Nepelu 
Je zajímavé, že uvedené blogy nemají současně nejvyšší karmu - tu obvykle dosáhnu, když napíšu o politice :(. 
 
Víc, než "nejkarmovitější", stojí za pozornost moje nejdiskutovanější články: 
■ Gen homosexuality? Vsadil bych se, že ano. 
■ Bloger zo Súmračnej? 
■ Vadí mi, když o evoluci a Darwinovi píší diletanti 
■ Důležitost předmanželského sexu 
■ Svatopluk - kráľ starých Slovákov? 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    512
                
                
                    Celková karma
                    
                                                9.15
                    
                
                
                    Priemerná čítanosť
                    2611
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Akvaristika
                        
                     
                                     
                        
                            Alexandra &amp; Danielka
                        
                     
                                     
                        
                            Cestování
                        
                     
                                     
                        
                            Filmy, které mě zaujaly
                        
                     
                                     
                        
                            Knihy, které mě zaujaly
                        
                     
                                     
                        
                            Hudba, která se mi líbí
                        
                     
                                     
                        
                            Společenské hry
                        
                     
                                     
                        
                            Filatelie
                        
                     
                                     
                        
                            SQUARE DANCE
                        
                     
                                     
                        
                            Historie, archeologie, evoluce
                        
                     
                                     
                        
                            Příroda
                        
                     
                                     
                        
                            Zoologie
                        
                     
                                     
                        
                            Zuzka
                        
                     
                                     
                        
                            Zlatá minca
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Moje komentáře na ČeskoSlovenské filmové databázi
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Robin Dunbar: Příběh rodu Homo
                                     
                                                                             
                                            Zdeněk Veselovský: Etologie
                                     
                                                                             
                                            Matt Ridley: Červená královna
                                     
                                                                             
                                            Samo Šaling: Slovník cudzích slov (záchodová četba)
                                     
                                                                             
                                            Nejnovější číslo National Geografic
                                     
                                                                             
                                            Jaroslav Flegr: Úvod do evoluční biologie (po kouskách)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            YUI
                                     
                                                                             
                                            miss A
                                     
                                                                             
                                            ... bublání vody v akváriu ... :)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zuzka Ščobáková - blog mé dcery
                                     
                                                                             
                                            Andrea Galicová - za "Prosbu"
                                     
                                                                             
                                            Laco Tabak - pouze "zírám" na jeho blog
                                     
                                                                             
                                            Andrej Hajdusek - závidím mu jeho cesty
                                     
                                                                             
                                            Lenka Jíleková - "poriadne naštvaná" baba, která výborně píše
                                     
                                                                             
                                            Pavol Satko - líbí se mi jeho názory na historii člověka
                                     
                                                                             
                                            Mirka Grófová - její první dva blogy zaujaly
                                     
                                                                             
                                            David Králik - značně sympatický "cvok"
                                     
                                                                             
                                            Katarína Pišutová - žije na jednom jezeře v Ugandě
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje půlnoční video z Vídně: calleruji já, pomáhá Slamák, máj 08
                                     
                                                                             
                                            Karolína SDC Bratislava
                                     
                                                                             
                                            Zlatá minca - súťaž finančných produktov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chlapec z trolejbusu
                     
                                                         
                       Smer - strana papalášov
                     
                                                         
                       Záhadný Azerbajdžan? Omyl. Krajina budúcnosti.
                     
                                                         
                       Komunálne byty v Číne
                     
                                                         
                       Varíme s medveďom - Rok v kuchyni
                     
                                                         
                       Hypotéza: Pro úspěšné předání genetické informace může být drobná výhoda u žen větší ...
                     
                                                         
                       Ako sme (ne)navštívili Južné Osetsko
                     
                                                         
                       Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                     
                                                         
                       Ktoré médiá o kauze CT mlčali?
                     
                                                         
                       Maldivy na vlastnú päsť - strava
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




