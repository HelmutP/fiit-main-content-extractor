

 
   
 ...po zhliadnutí filmu, ktorého názov by bol azda výstižnejší „Kráva jordánská“.. Ale po predstave plagátu s týmto názvom, by vo mne evokoval bláznivú českú komédiu, ktorú by som začala brať s rezervou ešte predtým, než by som si šla tento film pozrieť - ak by som si ho vôbec išla pozrieť.. a, vidíte?? S tajomným názvom „Kawasakiho rúže“ ma načisto namotali ako križiak muchu v rohu našej záhrady a bola som jasná. :) tí Česi to proste fakt „uměj“! 
 
  
 
 
  
  O tom, čo znamená kráva jordánská, akú bezstarostnú pointu tento pojem nesie odvíjajúcim sa príbehom, a keď sa už už zdá, že to bola len jedna z tých bezvýznamných vtipných českých poznámok, opäť sa tento pojem nenápadne pripomenie ako hladné mača..  
 .. o teórii sústredenia sa na úzky okruh záujmu v živote (vo filme je to obyčajná "koule" a jej variácie..) ma filmový Borek - Čech žijúci vo Švédsku svojimi zeleno-tirkisovými očami a bohémskym, miestami až nehereckým spôsobom uplne dostal!.. :)  
  
 Kráva- a predsa posvätná, dobro- a predsa nie bezchybné, hrdina - ale kto je ním vlastne? A je vôbec niekto hrdina, alebo zbabelec?.. Asi niet človeka, kto by nebol aj jedným aj druhým v konkrétnych okamihoch svojho bytia.. v tom sme si všetci rovní:) Pre svoje netradičné stvárnenie kompexného deja s prvkami toho kontroverzného, čo v nás ako v človekoch je, sa mi tento film zapáčil natoľko, že o ňom píšem..  
  ...o tom všetkom, i o mnoho inom (čo môže divákovi na prvé zhliadnutie filmu uniknúť a čo si všimne zas niekto druhý) vypovedá táto česká vážna "groteska" s "heppy"?.. ale hlavne endom, ktorému dá myslím každý za pravdu..  
 Ak máte chuť na niečo smutné i veselé, duchaplné, ale zas nie príliš, zaujímavé, šokujúce, ľahkovážne, trpko-vtipné s čiastočnou pointou, tak je tento film pre vás šitý na mieru.. :) 

 Odporúčam, nenechajte si ho ujsť. A hlavne urobte všetko preto, aby ste sa naň v kine nemuseli pozerať z prvej rady!.. :P.. :)  
  
  
  
      

