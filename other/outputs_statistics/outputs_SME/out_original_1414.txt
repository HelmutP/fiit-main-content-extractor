
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Olga Pietruchová
                                        &gt;
                O čom sa ne/hovorí...
                     
                 Pedofilní Františkáni alebo inkvizícia v priamom prenose? 

        
            
                                    21.5.2009
            o
            8:35
                        (upravené
                19.8.2010
                o
                16:48)
                        |
            Karma článku:
                13.56
            |
            Prečítané 
            13538-krát
                    
         
     
         
             

                 
                    Slovensko má prvý verejný škandál s údajne pedofilnými duchovnými. Prvý verejný píšem preto, lebo podľa zahraničných skúseností sa oprávnene môžeme domnievať, že sa sexuálne zneužívanie duchovnými dialo aj na Slovensku. Som ten posledný človek, ktorý by sa zastával sexuálnych zločincov v radoch cirkvi. Rešeršovala som na moje články množstvo prípadov, ale pri tomto sa nemôžem zbaviť pocitu, že ide skôr o inkvizičné ťaženie ako o vyšetrovanie zločinu.
                 

                 Čo vieme?    Polícia vtrhla nad ránom do kláštora, zaistila dôkazný materiál a vzniesla obvinenia. Stránka TV Joj píše: „Policajti tam zaistili viaceré dôkazové materiály, medzi ktorými nechýbajú ani biologické stopy odobraté z miesta, kde sa podľa tvrdení chlapcov zneužívanie odohralo." Neskôr však spolu s jedným  z bulvárnych denníkov stupňuje: „Muži zákona namiesto modlitebných knižiek a svätých obrázkov údajne našli porno časopisy s gay tematikou a pornografické DVD. Zaujímavý pohľad sa im vraj naskytol aj pri zasvietení UV lampy v miestnosti, ktorá mala byť celá postriekaná mužským ejakulátom". (Prečo mi to pripomína legendárne šaty Moniky Lewinskej s biologickými stopami Billa Clintona?)   Podľa vyjadrenia polície sa dôkazy stále vyhodnocujú. Je teda zaujímavé, odkiaľ čerpá bulvár informácie o údajných porno materiáloch. Okrem toho vlastnenie pornografických časopisov alebo DVD - či už s gej tematikou alebo hetero-, ani masturbácia nie sú trestné činy, dokonca ani u duchovných. (Tu mi zas napadá Anna Polívková vo filme Účestníci zájezdu: kto masturbuje, ať zdvihne ruku...). Je len prirodzené, že mladí muži sú sexuálne bytosti, či už za stenami kláštora alebo mimo neho. Kto tvrdí niečo iné a núti ich k celibátu, je pokrytec a mal by byť obvinený zo spolupáchateľstva.   Najzaujímavejšie na celej veci je fakt, že obvinení študenti už viac ako pol roka v danom kláštore nežijú. Napriek tomu urobila polícia podrobnú domovú prehliadku v izbách všetkých rehoľníkov a v celom objekte. Vyšetrovala teda polícia konkrétne obvinenia konkrétnych ľudí, alebo to bolo inkvizičné ťaženie proti celému nepohodlnému rádu?   Štandardný postup - ututlať a upratať   Samozrejme, sexuálne zneužívanie mladistvých je veľmi vážne obvinenie - ale aj tu platí prezumpcia neviny. Minimálne do času, kým polícia vydá oficálne stanovisko k zaisteným dôkazovým materiálom sú špekulácie typu údajne prinajmenšom nekorektné.   Celý prípad sa mi ale nepozdáva... najmä keď ho porovnám s iným prípadom katolíckeho kňaza, ktorý bol obvinený zo zneužívania maloletých a dokonca sa aj priznal. Pred väzením ho zachránila záruka, ktorú za neho poskytol biskup Rožňavskej diecézy Eduard Kojnok. (Slovenský inštitút spoločenskej záruky je skutočne rarita...) Všetko však prebehlo v najväčšej tichosti a hriešnik (resp. zločinec) bol vraj preložený „priamo do Vatikánu a neskôr do duchovného strediska v obci Osušie, kde sa mal prevychovať a podľa všetkého aj zbaviť závislosti od alkoholu. V roku 2006 sa vrátil k svojmu povolaniu a poslaniu farára v obci Hodejov, kúsok od Šuríc. Po pár mesiacoch, pravdepodobne pod ťarchou zlého svedomia, tu ako 36-ročný spáchal samovraždu. Otrávil sa liekmi.", píše sa v článku o prípade.   Žiadny verejný škandál, žiadna inkvizícia v priamom prenose, naopak. Štandarný postup známy zo zahraničných scenárov - podozrenie, obvinenie, upratanie na nejaký čas mimo dohľadu, preloženie na iné miesto...   Neštandardný postup - inkvizícia v priamom prenose   Osobne si prajem, aby polícia v každom prípade sexuálneho zločinu konala tak razantne. Obete znásilnení však vedia o tom svoje... spochybňovanie, bagatelizovanie a laxný prístup k vyšetrovaniu je bežnou praxou.   Pri Františkánoch je zrazu polícia tvrdá a nekompromisná... kiež by to bolo stále tak. Nežijeme však v utopickom svete a každý človek, čo trochu sleduje dianie na politickej scéne vie o brataní sa vládnych strán s katolíckou cirkvou. Všetci vieme o dohode, že sa vláda nebude púšťať do kontroverzných vecí (čítaj práv žien a neheterosexuálnej menšiny), ktoré sa niekto snaží vložiť ako klin medzi otca štát a matku cirkev, a vieme aj o tichej podpore toho správneho prezidentského kandidáta.   Preto si myslím, že za normálnych slovenských okolností  by otec štát zodvihol telefón a potichu upozornil matku cirkev na to, že hrozí „pruser".  Myslím si, že takáto kontroverzná a pre cirkev ťažko stráviteľná aféra by sa len tak nerozniesla v médiách a ak by polícia konala, konala by veľmi potichu. Keďže je opak pravdou, myslím si, že je za tým niečo iné ...Cui bono?   Biskupi sa nevyjadrujú   Myslím si to aj preto, že Konferencia biskupov Slovenska vyhlásila, že „Rehoľa menších bratov Františkánov funguje ako samostatný subjekt. Obvinenie padlo do jej radov, preto nám neprináleží ho komentovať."   Dovoľte mi zasmiať sa - biskupi sa nevyjadrujú? Nevyjadrujú? Biskupi? Oni, ktorí majú inokedy univerzálny patent na všetko z oblasti ľudskej sexuality? Oni, ktorí tu pomaly určujú, ako má vyzerať vládny program starostlivosti o reprodukčné zdravie a sexuálna výchova?   Scénár je jasný: ak sa podozrenia potvrdia, bude to čiste záležitosť Františkánov a KBS si umyje ruky (hoci Františkáni sú katolícky rád). Ak sa ukážu ako nevinní, KBS to vyhlási za útok voči celej katolíckej cirkvi a určite z toho vyťaží nejaké to odškodné.   Môžem si len želať, aby si Konferencia biskupov Slovenska osvojila výraz neprináleží nám komentovať aj v prípadoch, keď sa jedná o iné témy súvisiace so sexualitou.  To im totiž v sekulárnom štáte naozaj neprináleží. Začať môžu hneď na budúci týždeň v prípade Národného programu starostlivosti o ženy, bezpečné tehotenstvo a reprodukčné zdravie.   Rovnako si želám, aby polícia konala tak razantne a rýchlo vo všetkých prípadoch sexuálneho zneužitia a vôbec všetkých iných prípadoch. Mám však pocit, že sa nám rysuje ďalší prípad a´la Malinová, keď sa skôr hovorilo ako konalo...   Želám si však aj to, aby sa v právnom štáte nesúdilo v priamom prenose a aby sa nepripisovala kolektívna vina všetkým Františkánom za prípadné zlyhanie jednotlivcov. Pripisovanie kolektívnej viny je cestou do pekla pre každú spoločnosť. Ak sa preukáže vina mladých mužov, bude dosť času verejne ich pranierovať. Ale ak sú nevinní, kto očistí ich a celý rád od škaredých obvinení? Nehovoriac o tom, že bulvarizácia tejto vážnej problematiky môže prípadné obete podobných zločinov viac odstrašiť ako povzbudiť. Preto by si slovenské médiá mali zobrať príklad z Boston Globe, ako sa robí investigatívna žurnalistika v tak citlivých veciach. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (477)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová ideológia? Nie, iba spravodlivosť a fair-play pre naše dcéry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Rodová rovnosť ako kultúra života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Odpoveď na otvorený list Gabriele Kuby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Ženskosť a mužskosť alebo komu slúžia stereotypy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Olga Pietruchová 
                                        
                                            Od Harryho Pottera ku gender ideológii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Olga Pietruchová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Olga Pietruchová
            
         
        pietruchova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Viac mojich článkov nájdete na www.olga.sk


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    180
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7252
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Práva žien a rodová ne/rovnosť
                        
                     
                                     
                        
                            Ateizmus a veda
                        
                     
                                     
                        
                            Politika a spoločnosť
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Fejtóny:-)
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Sexualita, reprodukcia a práva
                        
                     
                                     
                        
                            The God Delusion
                        
                     
                                     
                        
                            Zelené témy
                        
                     
                                     
                        
                            Cirkev a náboženstvo
                        
                     
                                     
                        
                            O čom sa ne/hovorí...
                        
                     
                                     
                        
                            Jej príbeh
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ženy v politike, politika pre ženy
                                     
                                                                             
                                            Môj rozhovor v Sme Víkend
                                     
                                                                             
                                            Na výsluchu: Hierarchia katolíckej cirkvi mi leží v žalúdku
                                     
                                                                             
                                            Môj rozhovor na www.sme.sk
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Steven Pinker: The Blank Slate
                                     
                                                                             
                                            Jared Diamond: Collapse
                                     
                                                                             
                                            Martin Uhlír: Jak sme se stali lidmi
                                     
                                                                             
                                            Lynn Abrams: Zrození moderní ženy
                                     
                                                                             
                                            Daniel Dennett: Breaking the Spell
                                     
                                                                             
                                            Richard Dawkins: God Delusion
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moja osobná stránka
                                     
                                                                             
                                            Zošity humanistov
                                     
                                                                             
                                            Spoločnosť pre plánované rodičovstvo
                                     
                                                                             
                                            Edge -  The Third Culture
                                     
                                                                             
                                            JeToTak
                                     
                                                                             
                                            Changenet
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




