
 
  Okná mám otvorené 
  
 Dokorán 
  
 A čerstvý vzduch sa vypredal 
  
 Tak hľadám spásu inde. 
  
   
  
 Je to zlé ale bolo horšie 
  
 Nezvesím hlavu 
  
 Na to mám malú guráž 
  
 Odvahu som kúpil psovi 
  
 Aby sa v svojej epoche 
  
 Mal k komu modliť. 
  
   
  
 Sám si vymýšľam 
  
 Ostrie hrán 
  
 A dotváram krehkú myseľ 
  
 Nepýtam sa neba 
  
 Kvôli čomu som vlastne 
  
 Prišiel. 
  
   
  
 Hľadám vzduch vo dne 
  
 V noci všade 
  
 A neviem sa premôcť 
  
 Všetko mi príde drahé 
  
   
  
 Teraz ma chytia Za úsvitu 
  
 A hodia ku rieke veľkých 
  
 Sĺz a uvidím nádej od psa 
  
 Zbitú. 
  
   
  
 A tu to končí 
  
 A možno začína 
  
 Znova 
  
 To čo sa opakuje 
  
 Cyklicky dookola. 
  
   
  
 To nie je  nič a predsa všetko 
  
 Život sa to volá.  
 
