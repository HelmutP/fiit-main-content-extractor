
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Junkers
                                        &gt;
                Irsko
                     
                 Nezúčastniť sa neznamená ignorovať - Južná Afrika v Írsku 

        
            
                                    18.6.2010
            o
            11:20
                        (upravené
                18.6.2010
                o
                12:14)
                        |
            Karma článku:
                3.76
            |
            Prečítané 
            843-krát
                    
         
     
         
             

                 
                    Je jún, lásky čas. Nie, nepomýlila som si mesiace a neposunula romantiku rozkvinutých čerešní (aj u nás kvitnú začiatkom mája) o 30 dní neskôr. Je čas lásky k poskakujúcej lopte, čas rande s telkou a bdelo sledovaného programu majstrovstiev, aby nám nič neušlo. Naše malé bejby síce ešte nemá veľké pochopenie pre krásu futbalu, ale základy sú tiež dôležité a od malička vie, že vo februári sa pozerajú Six Nations a každé štyri roky v júni bude pozerať, ako sa 22 chlapíkov naháňa na tráve za loptou. A že je to takmer tak dôležité, ako povedať ďakujem a dobrý deň.
                 

                     Naša multikulti domácnosť má hneď tri želiezka v ohni. Slováci, tí žiaľ v prvom zápase nepresvedčili, ale dúfajme, že sa ešte polepšia. Nedeľa, Paraguay, treba zamakať! Druhý tím je Nemecko a to sú už pozitívnejšie správy. Klinsmann s nimi dokázal malý zázrak, premeniť nudu, vyčkávaciu taktiku a síce efektívny ale neskutočne nezáživný štýl na hru hodnú pozerania. Po jeho odchode sa zdalo, že sa Löw vráti naspäť k starým praktikám, Európa 2008 bola skôr sklamaním. Zápas s Austráliou však ukázal mančaft v zabudnutom svetle : tí chlapi utekali! útočili! neopaľovali sa niekde vzadu v obrannom pásme a nečakali na to, že znechutený súper zaspí od nudy, aby mohli streliť gól! Potom, čo sa mi podarilo dať do jedného odstavca viac výkričníkov ako Nový Čas môžem len povedať : skvelé, len tak ďalej. Nemecký futbal lahodný pre oči sa často nestáva, ale o to viac poteší.   Tretí tím, ktorý u nás sledujeme, sú Francúzi. A teraz je všetko naopak. Slováci, Nemci, do toho! Francúzi – tiež do toho, rýchleho balenia si kufrov. Áno, aj my sme sa nakazili írskou frankofóbiou, ktorá začala 18 novembra 2009, keď Thierry Henry magickou rukou nahral na víťazný gól Francúzska a dostal ich na majstrovstvá sveta v Južnej Afrike.   Odozva bola neskutočná, od obyvateľstva protestujúceho pred francúzskou ambasádou, až po oficiálne vyhlásenia vlády na čele s ministerským predsedom Brianom Cowenom žiadajúcich anuláciu výsledku a opakovanie zápasu. FIFA (vedená Platinim, ako sa nikdy nezabudlo zdôrazniť) vláde odkázala, že sa nič anulovať nebude a nemajú čo strkať nos do futbalových záležitostí. FIA (Írska futbalová asociácia) tiež nebola v proteste úspešná a tak si írski futbalisti berú dovolenky, organizujú veľkolepé svadby a írski fanúšici držia všetkým tímom, ktoré hrajú proti Francúzsku.   Pravdu povediac, Francúzi to Írom vôbec nesťažujú. Akoby sa so Zidanom odobralo do dôchodku aj srdce francúzskych futbalistov a hrdosť na to, že hrajú v národnom mančafte. Kto ich videl včera hrať s Mexikom vedel, že Mexičania vyhrali zaslúžene – respektíve Francúzov ubehali. Tí chlapi boli schopní poskakovať ako jojo ešte v 90 minúte hry, kým Francúzi vyzerali, akoby mali namiesto nôh uvarené žabie stehienka a bezprizorne sa motali po trávniku ako sirota Podhradských. A tak v štúdiu RTE Eamonn Dunphy, ikona a dinosaurus írskeho športového žurnalizmu pri analýze priebehu zápasu žiaril ako supernova, škodoradostne komentoval každý kiks francúzskeho tímu a mal presne vyrátané, za akých podmienok sa splní írske želanie a Francúzi neprejdú zo základnej skupiny.   Nepostup má samozrejme aj niekoľko negatívnych stránok : prestanú zábavné reklamy ponúkajúce rady, ako sa vyhnúť francúzskym výrobkom a už nikdy sa nedozvieme, komu by držali Íri palce pri zápase Anglicka proti Francúzsku. Priznám sa, aj ja by som to rada vedela, ale ako to tak vyzerá, ostane to tajomstvom.   Ostáva nám len tešiť sa na ďalšie turnaje, keď sa tam objavia aj Íri. V roku 2002 tu bola fantastická atmosféra, všetky domy vyzdobené írskymi vlajkami, všetci boli plní nadšenia a elánu a popri celkom dobrých výsledkoch národného tímu mal vtedajší turnaj aj pikantné spestrenie v podobu konfliktu najlepšieho írskeho hráča Roya Keana a vtedajšieho trénera Micka McCarthyho.   Keď Roy Keane otvorí ústa, vyzerá pri ňom ešte aj Golonka ako slušný malý školáčik s ulízanými vláskami a povahou malého medvedíka. Ak má niekto záujem o prečítanie si, čo povedal Roy Keane trénerovi predtým, než sa na protest voči zlej pripravenosti írskeho tímu na majstrovstvá odišiel, nech si v Google zadá Roy Keane. Ja sa obávam, že by ma už len anglický citát vyhviezdičkoval.   Lúčim sa a idem rýchlo pozrieť, čo ešte stihnem pred 12.30, keď sa moja ratolesť bude znova diviť, prečo mamka tak strašne kvičí, kričí a rozčuľuje sa pri telke. Raz, čoskoro, to pochopí.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Dúhový pochod v Dubline
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Čo nám hovoria kosti z Tuam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Výlet do Antrim v Severnom Írsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            Trampoty so zvieratami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Junkers 
                                        
                                            O mačkách a zelenine
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Junkers
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Junkers
            
         
        junkers.blog.sme.sk (rss)
         
                                     
     
         Jeden manžel, jedno bejby, jedna mačka. To všetko na zelenom ostrove. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    73
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Recepty
                        
                     
                                     
                        
                            Irsko
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            The Girl Who Kicked the Hornet's Nest
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            RTE Lyrics - vynikajuce radio
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zirafa na koni - aj take sa stava
                                     
                                                                             
                                            Nadherne ludske pribehy bez zbytocneho sentimentu
                                     
                                                                             
                                            A vraj knihovnici nie su sexy
                                     
                                                                             
                                            Medvede - co este k tomu dodat?
                                     
                                                                             
                                            O vsetkom moznom a aj vareni
                                     
                                                                             
                                            Fascinujuce fakty o slovenskej literature
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo spíme s mužmi s ktorými by sme nechodili?
                     
                                                         
                       Óda na neveru
                     
                                                         
                       Šťastné a veselé ...
                     
                                                         
                       Silvestrovské "mecheche" v Bratislave
                     
                                                         
                       obžerstvo na vianoce a rok 2013
                     
                                                         
                       Cesta stopom: Žilina-Bratislava. Offroad mercedes a čas na zmenu.
                     
                                                         
                       Ako som sa stala neplatičom respektíve obeťou exekúcie...
                     
                                                         
                       Whisky race
                     
                                                         
                       Pozrite si výber najkurióznejších správ a titulkov roka 2012
                     
                                                         
                       Dve americké školy - dva názory
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




