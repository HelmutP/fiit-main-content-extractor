

 My sme na tom taky tak. 
 Ale asi je to jedno, koľko tých pubertiakov sa premelie životom. 
 I keď vraj, niektorí sú celkom neškodní, a oní rodičia si zväčša ten efekt pripisujú sebe. Aj mne by sa seba sa tak dobre škrabkalo, ale - nedá sa. 
 Zádrapka včera boli - klasické riady. Večierka, predĺžená podľa želania našich gymnazistov, ubiehala veselo, až tak, že ich ani netrklo - hlavne slečnu dcéru - že ono to má v dohode aj písmeno b - komplet poctivo spratanú kuchyňu. 
 A keď došlo na lámanie chleba a zistenie skutkovej podstaty, to vyrastené dieťa inokedy s veľmi vážnymi problémami zo zaspávaním neuveriteľne rýchlo zmizlo do hajan. 
 Meine nerven! 
 Sedeli sme na posteli ako dvaja pritvrdlí truhlíci, môj drahý soptil spravodlivým otcovským hnevom a ja som zúfalo vedľa neho čušala ako zmoknutá slepica. V hlave tisíc variácií,kombinácií, rovníc a nerovníc, prečo???? 
 Nic naplat, z naplánovaného romantického večera v náručí bola figa rozmrzelá, a nadôvažok som si v pracovnej pošte našla - neuveriteľná súhra náhod - citát od Marka Ebena o puberťákoch, ktorí jednoducho experimentujú s nemožnosťou svojich rodičov, aby sa raz mohlo experimentovať na nich... 
 V tej chvíli by to Marek Eben schytal, aj keď ho celkom rada vidím a uznávam. 
 Keď si človek pripadá ako blb neschopný výchovy, komunikácie, dohody a rešpektu, to je na odpis. Aspoň v tom okamihu. 
 Ako to krásne okomentuje vždy naša pani doktorka, keď sa bavíme o deťoch: "Tak dlho ich človek piple, aby mal s nimi dobrý vzťah, aby všetko klapalo, dáva sa... a príde puberta a všetko je v keli." K tomu patrí patričný povzdych. Môj i jej. 
 Nejak sa utešujem, že to prežijeme, lebo to prežili všetci, a počítam roky, koľko ešte to potrvá... a dúfam, že tak, ako ju to skoro chytilo, tak ju to skoro prejde... a dúfam, že dovtedy budeme bez infartku a ostane nám napriek našej trápnosti z jej pohľadu a jej nezodpovednosti z nášho pohľadu dobrý vzťah. 
 Nuž teda - puberta u nás skúša všelijaké výbušniny, (a je k podiveniu, na čo všetko zle reaguje), ale inak sa máme radi. 
 Len keď náhodou budete mať voľné víza pre tetu Bertu do nejakej exotickej destinácie bez možnosti návratu, hlásime sa... 


