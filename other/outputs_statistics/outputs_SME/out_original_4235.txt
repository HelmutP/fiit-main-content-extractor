
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Illovský
                                        &gt;
                Jazykoveda
                     
                 Pôvod slovenských čísloviek II. 

        
            
                                    9.4.2010
            o
            16:29
                        (upravené
                9.4.2010
                o
                16:41)
                        |
            Karma článku:
                4.83
            |
            Prečítané 
            1202-krát
                    
         
     
         
             

                 
                    Etymológie čísloviek (6-8).
                 

                 
  
   Etymológie (6-8)   šesť - v praslovančine vzniklo spodstatnením číslovky na substantívum. Derivačná morféma -ti- je rovnaká ako v slovách vlasť, kosť, hosť a pod. V psl. *šestь &lt; *šes-ti-s. Latinské sex, starogrécke hex a germánske jazyky pripúšťajú počiatočné nahé [s], ale zo slovanských a indo-iránskych dokladov vyplýva, že na mieste iniciály koreňa musel byť spoluhláskový kluster [ks]. K tomu pristupuje ešte keltské suex, arménske vech a wex z gréckych dialektov mykénskeho a dórskeho [Jagodzińsky, 2008]. Predpokladá sa preto zhluk spoluhlások v tvare ksw-. Radikála koreňa, vo väčšine jazykov „x", poukazuje na kluster gurutály K a sibilantu: -Ks-, pričom satemová vetva indoeurópskych jazykov umožňuje konkretizovať spoluhlásku radu K na palatalizovanú veláru [k^j]. Ide. koreň pre číslovku šesť je tak rekonštruovaný v tvare *kswek^js-.   sedem - druhotvar odvodený od ordinália siedmy &lt; psl. *sedm-ъ-jь. Tvar koreňa sed- je pravidelným výstupom z praslovanských procesov regresívneho spodobenia (-pt- &gt; -tt-) a znelostnej asimilácie (-tm- &gt; -dm-). To druhé vidno aj v starogréckom hebdomos &lt; *septm-o-s, kde je znelostnou asimiláciou zasiahnuté aj [p]. Základná číslovka sedem mala v ide. tvar *sep-t-m (lat. septem; gr. hepta), kde -t- je podľa Shieldsa tzv. „redundant non-singularity marker" [K. Shields, 1996], pripájaný mimovoľne k číslovke s vlastným, inherentným významom počtu siedmych, ale hypercharakterizovanej ako množné číslo. Čo sa týka pôvodu samotného koreňa *sep-, predpokladá sa, že je to zrejme stará výpožička zo semitského prajazka - číslovka sedem sa v arabčine a hebrejčine aj dnes podobá na súčasné indoeurópske tvary. Očakávané psl. †sedъ (s reflexom †sed v súčasnej slovenčine) sa nikde v slovanských nezachovalo, a tak sa predpokladá, že popísaná zmena v psl. je prastarého dáta.   osem - vznik analogicky podľa sedem. Pôvodný je iba koreň os- &lt; ide. *h3ek^j-. Staré indoeurópske jazyky pripájajú ku koreňu nesingulárový formant -t- a skloňujú to ako duál - teda podvojné číslo (lat. oktō; gr. oktō; nem. acht; angl. eight; ind. astau a keltské ocht). To je zrejme prejav toho, že číselný význam osem bol vnímaný párovo - t.j. ako dve čosi. Keď táto skutočnosť vyšla najavo, viedlo to niektorých k prehodnoteniu koreňa. Erhart analyzuje *h3o-k^wet- ako zloženinu dvoch ide. významovo duálových lexém (slov, ktoré pomenúvajú veci a javy vyskytujúce sa zásadne v dvojici) s disimiláciou prvej spoluhlásky druhého z nich k prvej spoluhláske prvého z nich - t.j. h3/k^w &gt; h3/k^j. Súčasná jazykovedná obec sa však prikláňa väčšmi k originálnemu, klasickému výkladu. Zdá sa totiž, že číslovka osem má síce duálovú flexiu (je skloňovaná s pádovými koncovkami prehistorického podvojného čísla), ale interpretácia koreňa ako zloženiny samostatných lexém, nesúcich význam párovosti, je príliš ťažkopádna. Okrem toho existuje dôvodné podozrenie, že slovanské slovo čata, ktoré zmienená hypotéza potrebuje na preukázanie prítomnosti k^wet- v koreni číslovky, je výpožička z uralskej jazykovej veľrodiny (maď. ket, kettő „dva").     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Illovský 
                                        
                                            Prijatý dámsky gambit
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Illovský 
                                        
                                            Čo hovoria matematické vety? III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Illovský 
                                        
                                            Čo je charakteristické pre...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Illovský 
                                        
                                            Čo hovoria matematické vety? II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Illovský 
                                        
                                            Čo hovoria matematické vety?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Illovský
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Illovský
            
         
        illovsky.blog.sme.sk (rss)
         
                                     
     
        Pochádzam z Bratislavy, kde aj žijem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    41
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1728
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Umenie
                        
                     
                                     
                        
                            Šach
                        
                     
                                     
                        
                            Jazykoveda
                        
                     
                                     
                        
                            Analytické vedy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




