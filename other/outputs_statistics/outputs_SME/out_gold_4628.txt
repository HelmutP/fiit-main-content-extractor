

 
 
 Na úvod by som uviedla pár trikov/ tipov , ktorými sa dá zaujať. 
 1, Úprimnosť! 
 Myslíte si, že zaujmete , keď príkrášlite isté skutočnosti ? Áno, je to pravda , ale neoplatí sa to . Prečo ? Pri prvom kontakte s dotyčným, by ste mali byť úprimné a povedať všetko, čo je dôležité. Nikdy nevynechávajte detaily , ktoré podľa vás nie sú potrebné. Vždy sú , ale o si uvedomíte až po tom ako sa vám chlap už na druhý deň neozve. Čakáte dni ba i celé týždne, no chlap sa neozve. Dôvod ? Niekde ste spravili chybu , ale kde ? Možno to pramení z neúprimnosti, z niečoho čo ste zatajili a on sa to dozvedel. radšej nech to počuje od vás ako od vašich kamarátov , spolužiakov, alebo kolegov , či náhodných "známych". Uvediem príklad - : ak ste náhodou ešte nikoho nemali , prečo by to ten druhý nemal vedieť ? Celý večer sa trápite, trémujete a potom tak aj celé rande dopadne. Skúste byť otvorené a všetko pôjde ľahšie. Prvé rande sa tak môže vďaka úprimnosti stať príjemným stretnutím, plným vzájomného porozumenia a opory. Stačí povedať nahlas o svojich pravdivých pocitoch. Prvé rande končí pocitom vzájomného uznania. Máte vyhrané. Obaja! 
 2,Vyberte miesto! 
 Skúste vziať iniciatívu do vlastných rúk. Čo tak vybrať miesto , kde sa cítite dobre? Nie je to skvelý nápad ? Budete sa cítiť príjemné a ako vo svojej koži. Chlap to bude mať ťažšie , ale prisôsobí sa. Dôležité je aby po čase nadobudol istotu , že miesto ste nevybrali pre to, že tam chodia vaše kamarátky a tie tam budú v tú dobu tiež. Alebo , že barman z podniku, kde ste si sadli , je váš bývalý. V každom prípade, treba si vybrať miesto, kde sa nebduete cítiť strémovane. 
 3,Nemeškajte!  
 Aj keď sa vravieva, že má žena chvíľu meškať , aby vyvolala napätie u nápadníka, v dnešnej dobe to už nie je "in". Nemeškajte , narobíte si tým len zbytočný trapas. Ak chcete u chlapa vašich snov obstáť na výbornú , prídite načas a ak sa vám bude dať, aj o minútku či dve skôr. 
 4,Skladajte komplimenty! 
 Prečo nie ? Chlapom vždy ulahodí príjemná poznámka na jeho adresu , ako skvele vyzerá , alebo že na fotkách je nespozanteľný a že v skutočnosti vyzerá sto krát lepšie. Hovorte sladko , no nie až príliš, aby to nevyzeralo , že sa vtierate. Ak vám bude komplimenty oplácať, všetko je v poriadku. Vždy sa však snažte byť milá aj v prípade , keď zistíte, že ste si s dotyčným nesadli príliš dobre: Prečo by ste mali byť tá o ktorej bude šíriť ktovie aké klebety ? 
 5, Nehovorte len o sebe! 
 Hovorte s ním o tom , čo má rád , čo sa mu páči , alebo čo by chcel vyskúšať. Isto sa nájde tisíc tém , ktoré by ho zaujímali.  V žiadnom prípade nehovorte len o sebe , budete pôsobiť sebeckym, povýšenecky a namyslene. Ak už nenájdete žiadnu vhodnú tému , rozprávajte sa o filmoch , ktoré práve bežia v kinách , alebo o hudbe, čo je "in". To zaberie vždy. ;) 
 6, Nezhadzujte sa ! 
 Ak si myslíte , že muži milujú ženy , ktoré sa vedia dokonale podceňovať , ste na omyle. Odrádza ich to , boja sa , že ak si takúto ženu dotiahnu do bytu , už sa jej nezbavia...Stále sa sťažujúca žena , to sú len problémy. Preto na rande zabudnite na svoje starosti a poriadne si ho užívajte. Môže sa stať , že je to prvé aj posledné rande s dotyčným chlapom. 
 Tohto sa samozrejme budem držať i ja , ale aj ďalších tipov a trikov , ktoré poznám. Dúfam , že stihnem zbaliť piatich chlapcov a že nebudú príliš sklamaní , keď zistia , že to bola len hra :) 
   

