
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Sendecka
                                        &gt;
                Nezaradené
                     
                 Láska a vášeň v priamom prenose (výber videí uplynulého týždňa) 

        
            
                                    23.5.2010
            o
            17:26
                        (upravené
                23.5.2010
                o
                17:53)
                        |
            Karma článku:
                3.70
            |
            Prečítané 
            1394-krát
                    
         
     
         
             

                 
                    Na záver krásneho jarno-letného víkendu vám opäť prinášam výber videí, ktoré ma počas uplynulého týžďňa najviac zaujali. Názov tohto blogu bol inšpirovaný citátom o práci, ktorého autorom je  poet  Khalil Gibran: „Práca je zviditeľnená láska". Čo konkrétne mám na mysli sa dozviete, ak si nájdete pár desiatok minút voľného času na môj dar pre vás.
                 

                 Havajský hráč na ukulele predviedol dych vyrážajúce predstavenie na TEDxTokyo.  Jake Shimabukuro, určite napĺňa Gibranovu definíciu práce. Vďaka za zdieľanie patrí Garrovi Reynoldos ovi, ktorý mal tú česť zažiť to naživo ako divák v publiku.             (kliknite tu a pozerajte video priamo na Youtube)       Po vzhliadnutí havajského ukulelistu, som sa ďalej preklikávala TEDxTokyo „streamom" na  Youtube a som vďačná za deväť minútové stretnutie s talianskym karikaturistom, ktorému trvalo 4 roky, kým prerazil v Japonsku. Matteov príbeh o tom ako sa nevzdal svojho sna má pre mňa nasledujúce prívlastky: Úžasné, autentické, úprimné, plné inšpirácie, veď  posúďte sami.  Dámy a páni:  Matteo Ceccarini                 (Youtube link je ,tu)       Ak niečomu zasvätíte 15 rokov svojho života, tak to už musíte mať naozaj jasno vo svojich pocitoch, že? Tím ľudí okolo Craiga Ventera bez pochýb demonštrovali svoju lásku pri vývoji syntetickej bunky. Cesta za týmto revolučným objavom bola plná omylov a zlyhaní, ale práve o tom to je: Len tí, čo prekonávajú prekážky a po každom páde vstanú a pokračujú v bádaní „neznámych území",  menia a píšu históriu. Bravó!                     (link na túto TED „speech" je, tu)   Objavitelia syntetickej bunky, by určite súhlasili so slovami tohto inovatívneho rapu. „PERFORMANCE-FEEDBACK-REVISION." Tomuto ja hovorím, evolúcia, innovatívnosť a odvaha v jednom.  Go Baba!             (vďaka za originálny post patrí Beth a Neillovi)           Na odľahčenie od informačného pretlaku si môžete pustiť ako „podmaz"  nasledújucu chicken- monkey-duck pieseň, keď budete zalamovať rukami nad opakom Gibranovho citátu o práci. Pre tvorcov tohto olympijskéo loga za 1 000 000 $ bola láska určite neznámy pojem.  Je to nový módny trend,že za prepádakovú kvalitu sa platí veľa?  Maskot „vajčiak" , pardón „Gooly" je oproti londýnskemu logu priam práca Michelangela...                     Ďakujem za sledovanie a ak máte chuť, tak viacej "potravy pre dušu" sa podáva na mojom posterous blogu, ktorý je dedikovaný  videám, ktoré stoja za zverejnenie a pozretie si.   Možno aj vás zaujme blog s príbehom o tom ako pár očí zmenil môj život, ktorý zaznamenal  čitateľský úspech a komentovala naň aj americká spisovateľka Christine Ranck, ktorá je okrem iného blízkou rodinnou priateľkou Sira Kena Robinsona.   Želám vám úspešný pracovný týždeň plný momentov, ktoré urobia svet okolo vás lepším.   Tvorte a rozdávajte svoje umenie.   i.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Sendecka 
                                        
                                            [Manifesto] o vzdelávaní. Spravíme to realitou na školách?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Sendecka 
                                        
                                            Naozaj potrebujú slovenskí žiaci TV reality šou Trieda 9.A?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Sendecka 
                                        
                                            Update k Planéte vedomostí 2.0 + Dotazník: Pomôžte nám s dizajnom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Sendecka 
                                        
                                            [Výzva] Slovensko vytvorme si Planétu vedomostí 2.0
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Sendecka 
                                        
                                            Nastupujúca Generácia Lídrov Slovenska sa  o 3 dni stretne v Modre
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Sendecka
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Sendecka
            
         
        sendecka.blog.sme.sk (rss)
         
                                     
     
        Konzultantka nielen na digitálne stratégie. S vášňou pre celoživotné vzdelávanie.Tweetuje ako @IvanaSendecka. Zakladateľka NGLS.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2305
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




