
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Patarák
                                        &gt;
                Ťažká hlava psychiatrova
                     
                 Nie som žiadny cvokár 

        
            
                                    18.4.2010
            o
            16:28
                        (upravené
                9.11.2013
                o
                4:38)
                        |
            Karma článku:
                16.47
            |
            Prečítané 
            3376-krát
                    
         
     
         
             

                 
                    Nie som žiadny cvokár. Ani som nikdy nevidel žiadneho blázna. Robím síce na psychiatrii, no blázincom by som to s čistým svedomím nikdy nenazval.
                 

                 
  
   Bláznenie sa mi odmala asociovalo s chaosom. My však máme oddelenie krásne usporiadané. Pred budovou je parčík, ktorý si moja žena chutne pochvaľuje a hovorí, že sa jej v žiadnom nekočíkuje tak dobre, ako práve v ňom. Práve teraz tam kvitnú stromy a vyspevujú drozdy. Telocvičňa je na športové hry, v jedálničke sa zas schádzame k stolu ako taká väčšia juhotalianska rodinka (veď dobre dobre, niektorí z nás sú trocha temperamentnejší a čo?). A vlastne, v istom zmysle rodina aj sme. Mnohí ľudia mi o sebe dôverne povedia viacej, ako o nich najbližší rodinní príslušníci čo i len tušia.   Tak teda, nenazvať to skutočne famíliou?   Ako som povedal. Bláznenie sa mi odmala asociovalo s chaosom. Keď zajtra po nočnej službe vyjdem z oddelenia, až vtedy ma však pohltí chaos. Protichodné politické bilboardy, ruch dopravnej zápchy, namosúrené ksichty mladých faganov s mp3 prehrávačmi s čudesnou muzikou. Bľakot, rehot, pomixovaný džavot a to nekonečné náhlenie, so slepotou k tomu, čo je v skutočnosti naozaj dôležité.   Len tak mimochodom, jednou zo známok duševnej poruchy je strata kontaktu s vlastnou skutočnosťou. Tak, ako aj to, čo nazývame nozoagnóziou. Skrátka, neschopnosť uvedomiť si, že je so mnou, či s mojím životom niečo v neporiadku. V širšom zmysle tým ale trpí väčšina tých, ktorí sú pohotoví ihneď ukázať na blázna.   Nie je teda práve ten svet vonku akýsi chorý? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (85)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Patarák 
                                        
                                            Omnoho viac, než ponovembrová sloboda
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Patarák 
                                        
                                            Ukrutné obrázky a odpoveď pánovi Bánovi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Patarák 
                                        
                                            Ambivalencia medzi SME a Projektom-N
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Patarák 
                                        
                                            Moje blogy o smrti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Patarák 
                                        
                                            Žijeme a dýchame v SME
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Patarák
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Patarák
            
         
        patarak.blog.sme.sk (rss)
         
                        VIP
                             
     
         Odmalička sú mojim skutočným svetom najmä knihy a ľudská psyché. Zrejme sa nadmieru vŕtam do vecí, ktoré máme tendenciu považovať za ustálené a nemenné. Možno sa až príliš hrabem v špine a v čiernom bahne. Z toho bahna mi však napokon vždycky svietia hviezdy... 

 (Jungiánsky orientovaný psychiater s fókusom na neuropsychiatriu, psychologicko-psychiatrické aspekty sexuality, duchovný život a symbolicko-meditačnú psychoterapiu. V osobnom živote schopný donekonečna uvažovať nad interpretáciami Nietzscheovej filozofie, milujúci poéziu, prudké stúpanie do kopcov a prvé tri hlty z fľaškového piva) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    156
                
                
                    Celková karma
                    
                                                10.11
                    
                
                
                    Priemerná čítanosť
                    4726
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ach tá naša sexu-ach-lita
                        
                     
                                     
                        
                            Marihuana kazí chuana
                        
                     
                                     
                        
                            Bebida alcohólica
                        
                     
                                     
                        
                            Zo sveta psychiatrie
                        
                     
                                     
                        
                            Muž a žena
                        
                     
                                     
                        
                            Patologické hráčstvo
                        
                     
                                     
                        
                            Ťažká hlava psychiatrova
                        
                     
                                     
                        
                            (Neuro)psychiatria a veda
                        
                     
                                     
                        
                            Vám, nedosiahnuteľným
                        
                     
                                     
                        
                            Z hlbín vyviera viera
                        
                     
                                     
                        
                            Hľadanie pôvodu zla
                        
                     
                                     
                        
                            Zápisky z pavučiny
                        
                     
                                     
                        
                            Homoparanoja
                        
                     
                                     
                        
                            Premýšľam o svete
                        
                     
                                     
                        
                            Meditationes
                        
                     
                                     
                        
                            Politické tvory
                        
                     
                                     
                        
                            Recepisy
                        
                     
                                     
                        
                            Reflexie a reakcie
                        
                     
                                     
                        
                            Kauza Bezák
                        
                     
                                     
                        
                            Kauza Hedviga
                        
                     
                                     
                        
                            Kríza zdravotníctva
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prechádzky čínskymi rozprávkami
                     
                                                         
                       Rakúsky potravinový fenomén vyráža dych. Bratislavčania už zareagovali.
                     
                                                         
                       Prečo sú tí LGBTI takí agresívni, alebo môžeme si za nepochopenie sami?
                     
                                                         
                       Pohľad Slováka študujúceho v Škótsku na referendum o nezávislosti
                     
                                                         
                       Všetci majú sny!
                     
                                                         
                       Som rehoľná sestra. A nehanbím sa za to.
                     
                                                         
                       Môj Deň bez umenia
                     
                                                         
                       Kto vyhnal moderátorku zo Slovenska?
                     
                                                         
                       Ako získať 80 tisíc predplatiteľov a písať len jeden článok denne?
                     
                                                         
                       Ak chce mať RTVS talkshow, tak určite nie s Marcinom
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




