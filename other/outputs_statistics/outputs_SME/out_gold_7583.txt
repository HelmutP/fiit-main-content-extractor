

  
 Najdôležitejšou podmienkou toho, aby sa v dieťati vyvinula láska k životu, je pre neho to, aby bolo s ľuďmi, ktorí milujú život. (Erich Fromm) 
  
  
 Láska je ako dieťa, túži po všetkom, čo má na očiach. (William Shakespeare) 
  
  
 Láska je dieťaťom ilúzie a matkou dezilúzie. (Miguel de Unamuno) 
  
   

