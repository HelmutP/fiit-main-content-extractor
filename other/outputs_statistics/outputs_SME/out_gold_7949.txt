

 Riešenie ktoré vyhovuje úplne všetkým nikdy neexistuje a tak inžinieri často musia voliť riešenie, kde celkové benefity prevážia nad stratami a dielo prinesie úžitok spoločnosti ako celku, aj keď možno za cenu niekoľkých "obetí". V blogu prinášam zjednodušený (laický) popis výstavby diela, ktorý je prebratý (pretlmočený) z niekoľkých odborných publikácií spomenutých na konci blogu. 
 Mestká estakáda v Považskej Bystrici je prvým diaľničným mostom na území SR, ktorého nosná konštrukcia nesie celý diaľničný profil a nosnú konštrukciu v priečnom smere podopiera iba jedna podpera. Toto neobvyklé riešenie vyplynulo z požiadaviek čo najmenej zasahovať do areálu mesta. Svojou dĺžkou 969 metrov je to tiež najdlhší mostný dilatačný celok na území SR. 
  
 Samotný spôsob výstavby mosta bol už od začiatku projektovania limitovaný na minimálne zásahy do fungovania mestkých častí, ako aj požadovanou rýchlosťou výstavby. Z tohto dôvodu sa v prvých fázach projektovania zvolila na výstavbu technológia vysúvania, ktorej sa prispôsoboval celkový koncept mosta. Výsledný dizajn zvládli mostári naozaj bravúrne a po architektonickej stránke by to pravdepodobne krajšie nenavrhol ani žiadny študovaný architekt. Polohy podpier boli limitované už existujúcou zástavbou, polohou železnice, tokom Váhu, ale aj s novobudovanou križovatkou.  Táto skutočnosť vyústila do rozpätí polí až 122 metrov. 
  
 Počas hľadania najvhodnejšieho riešenia sa testovali rôzne varianty mosta ako napr. oceľová alebo tvz  spriahnutá konštrukcia (oceľová konštrukcia komory mosta s nadbetónovanou hornou doskou). Výsledný želozobetónový variant zvíťazil vďaka nižšej cene, ale aj vďaka možnosti rýchlejšej výstavby. 
 Ako najvhodnejšia technológia, pomocou ktorej sa dali splniť všetky stanovené kritériá, sa nakoniec ukázala technológia letmej betonáže. Pri tejto technológii sa most postupne buduje rovnomerne od pilierov na obe strany. Vytvárajú sa tvz. vahadlá, ktoré sa  nakoniec vzájomne prepoja  a vznikne spojitá konštrukcia. 
  
 Na urýchlenie výstavby sa muselo nasadiť až 14 tvz. betonárskych vozíkov naraz, pričom každý z nich bol schopný zhotoviť cca 5 metrov mosta za cca 10 dní. 
  
 Betonáž musela byť veľmi disciplonovaná aby nevznikali navyrovnané sily na vahadle. Vahadlo sa muselo betónovať naraz a symetricky z oboch strán tak, aby nedošlo k prekročeniu povoleného limitu ani na jednej strane (napr. ak jedna strana pri betonáži lamely už zabetónovala o tých povolených X kubíkov viac ako druhá, musela čakať kým ju druhá strana dobehne a až potom mohli pokračovať v ďalšej betonáži). Stabilizácia vahadla sa zabezpečila zvoleným tvarom pilierov a teda nemuseli sa budovať žiadne dočasné podpery. Tvarovanie pilierov sa teda logicky prispôsobilo technológii výstavby a toku vnútorných síl. 
  
  
 Výška pylónov a počet závesov sa niekoľkokrát menil a bola aj istá varianta v ktorej sa s pylónom a závesmi vôbec nepočítalo. Výsledný počet závesov a výška pylóna sa nakoniec  zvolila najmä z estetického hľadiska tak, aby tieto závesy boli viditeľné z väčšiny pohľadov zdola z mesta. Vyššia výška pylóna taktiež zvýšila ich účinnosť. 
 Prierez mosta je komorový výšky 4.7 až 6 metrov. Samotný prenos síl v rámci priečneho rezu sa dá veľmi zjednodušene zobraziť takýmto obrázkom. 
  
 Keďže je betón v ťahu pomerne slabý, všetky ťahané časti sú predopnuté t.j. je do nich pomocou napnutých lán, prípadne tyčí vopred vnesená tlaková sila z ktorej sa vplyvom zaťažení tento tlak "odčerpáva" (teda reálne od vonkajších zaťažení v nich ťahové napätia nevzniknú). 
 Pohľad do komory mosta: 
  
 Betonáž priečneho rezu sa pred začiatkom prác odskúšala na segmente v merítku 1:1 na ktorom sa súčastne sledoval vývoj tvz. hydratačného tepla vznikajúceho pri reakciách vody a cementu (teplota sa pri masívnych prvkoch nesmie dostať nad určenú kritickú hodnotu). Na zhotovenej experimentálnej lamele sa tiaž odskúšala jej únosnosť v priečnom smere a porovnala sa s vypočítanými hodnotami, ktoré nakoniec prevýšila približne 1,5 násobne. 
  
 Akonáhle sa doriešili všetky technologické problémy a stavbári sa zabehli v prácach, výstavba hornej stavby začala napredovať závratným tempom. Výstavba celej mestkej estakády vrátane pilierov trvala necelých 22 mesiacov. V istých fázach výstavby na moste pracovalo naraz až vyše 270 pracovníkov a technikov. 
  
   
  
  
  
 zaťažovacia skúška 
  
 Pohľad z mesta: 
  
 Aby sa dal takýto most nakoniec postaviť je za tým množstvo hodín kreslenia a statických výpočtov rôznych fáz výstavby a finálnej prevádzky mosta. Navrhnúť a postaviť takýto unikátny most je veľkým inžinierskym úspechom a gratulujem všetkým ktorý sa na jeho projekcii či realizácii nejako podieľali. 
 No a keďže sa nám nezadržateľne blížia voľby, úplne na záver si dovolím ešte krátku politickú úvahu :-). Mosty už od dávnych čias boli symbolom technologického pokroku daného regiónu alebo krajiny a odzrkadlovali úspech hospodárstva a rozkvetu. Ich výstavba bola podmienená práve úspechmi regiónov a tiež ďalší rozkvet hospodárstva bol možný len vďaka vyspelej infraštruktúre, ktorá je bez mostných stavieb nemysliteľná.  Z tohto dôvodu mosty vždy boli oslavované aj politickou elitou, ktorá nakoniec prestrihla pásku a slávnostne odovzdala most do užívania obyvateľom. Takto to bolo už v časoch Rímskej ríše a platí to dodnes. Samozrejme politici nesú podstatnú zásluhu pri výstavbe mosta, nakoľko oni rozhodujú o jeho financovaní a takto majú akési prednostné právo prestrihávať pásky a hrdiť sa novootvoreným mostom. Otázne však ostáva nakoľko môže politika zasahovať do samotnej výstavby mosta a kde je prijateľná hranica politických cieľov. Stavba Považskej estakády bola vo veľkej miere ovplyvnená práve rôznymi politickými zámermi, čo platí aj pre čas jej výstavby, ktorý bol tlačený k jej otvoreniu ešte pred termínom parlamentných volieb. Možno by bolo zaujímavé zanalyzovať nakoľko táto politická angažovanosť ovplyvnila výslednú cenu tohto veľkolepého diela,  teda nakoľko boli hospodárske benefity rýchlejšieho otvorenia v rovnováhe s cenou za mimoriadne rýchlu výstavbu. 
 Mostári vždy boli a aj budú v rukách politických hier a ostáva len na ich inžinierskej morálke nakoľko sa nechajú politickou scénou ovplyvniť. Most predsa nieje predvolebný bilboard, ktorý zmizne pár dní po voľbách, ale je to dielo, ktoré ostáva aj pre budúce generácie slúžiť cieľu na ktorý bol postavený. Most sa postupom času stáva aj symbolom svojej doby a je mi trochu ľúto, že práve tento unikátny most bude mať na sebe popri reprezentatívnej nálepke pokroku našej malej krajiny aj štítok politickej propagandy. Ale aj o tom sú mosty. 
   
 Literatúra: 
 M. Maťaščík, K. Táborská: Mestská estakáda v Považskej Bystrici, in zborník konferencie: Betón na Slovensku 2006-2010 
 M. Chandoga, J. Sedlák, A. Prítula, J. Kucharík: Zaťažovacia skúška skúšobnej lamely extradosového mosta v Považskej Bystrici, Inžinierske stavby 2/2010 
 G. Tevec: Betonárske vozíky pri letmej betonáži mosta v Považskej Bystrici, Inžinierske stavby 2/2010 
 M. Račanský: Dálniční estakády nad Považskou Bystricou, Silnice a železnice 2/2010 

