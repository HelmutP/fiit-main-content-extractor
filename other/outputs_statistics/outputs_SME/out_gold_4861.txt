

   
 Žila s ním, žila pre neho. 
 Videla ako túži po starostlivosti, precíznosti, ako lapá po dychu a vyhadzuje každý jeden neživý balvan. 
 Videla ako ho rozpustili kvapky neba, ako mu slnečné lúče vytvárali nový odtieň pleti a vysušovali pokožku. 
 Videla ako mu zo semienok vyrastali prvé zelené páperia, ako sa miestami snažila prebiť nechcená ozdoba. Ako sa mu výhonky ťahali k nebu, ako sa mu pravidelne robil letný strih. 
 Prezentuje sa jednoduchou krásou a milovníčke sa to páči. Na boku sa pýši veľkou hrudou so skalnými ružami. Boky mu lemujú otvorené kvádre s malými stromami. 
 Tvorba jeho takmernej dokonalosti je u konca,ale to už nevidí. Keď prichádza posledný raz, zhliadne jeho finálnu podobu a žasne. V ťažkej chvíli dokáže iba on vyžiariť úsmev na slaných perách. 
 Lúči sa s ním, myslí, že naveky. Iba ak, čo zbadá ho z diaľky a zdvihne ruku na pozdrav, venuje myšlienku. 
   

