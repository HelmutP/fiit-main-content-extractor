

   
 Tváre histórie 
 Do Tallinnu prichádzajú cestujúci zvyčajne po mori, trajektom z Helsínk, alebo zo Štokholmu. Strávia tu pár hodín, nanajvýš víkend, aby si pozreli pamiatky, nakúpili a ochutnali niečo domáce. Prichádzajú, aby „objavili Tallinn“, ako znie reklama v škandinávskych denníkoch, aby znovuobjavili chuť dotyku so susedom, s ktorým prežili kus spoločnej minulosti. Odjakživa ich spája more, na pár desaťročí ich však oddelila politika veľkého východného suseda, ktorá Estóncom nikdy nebola pochuti. 
 Dnešné Estónsko ukazuje príchodzím dve tváre – a obe sú napodiv skutočné. Zanedbané zátišia ako vystrihnuté z niekdajších sovietskych filmov sú fragmentom spoločného života v nedobrovoľnom zväzku s Ruskom. Tallinnčania chcú túto tvár prehlušiť zrenovovanou stredovekou architektúrou a panorámou presklených mrakodrapov. UNESCO zaradilo historické jadro mesta na listinu svetového kultúrneho dedičstva. 
 Vďaka polohe na pobreží Fínskeho zálivu sa v Tallinne spájali obchodné záujmy Východu a Západu, čím sa prirodzene stal dôležitým mestom vtedajšej hanzy. Od stredoveku tu mali vplyv striedavo Dáni, Švédi, Nemci a Rusi. Po skončení 1. svetovej vojny získalo Estónsko samostatnosť, no dohoda Hitlera so Stalinom na sklonku 40-tych rokov minulého storočia umožnila Rusom opäť siahnuť na túto krajinu. Nasledovali deportácie nepohodlných do sibírskych gulagov a potom štyri desaťročia sovietskeho režimu. Estónci sa začali proti systému verejne búriť v roku 1987. Slobodu, ktorú zažívajú dodnes, si vydobili v januári 1991. Euro teda zavedú na 20. výročie získania nezávislosti. Zaujímavosť – v juhoestónskom mestečku Otepää napísal Alexander Solženicyn väčšiu časť románu Súostrovie Gulag. 
  
 Panoráma Tallinnu pri vstupe od mora 
   
  
 Tallinn, ktorý možno veľkosťou prirovnať k Bratislave, je druhý najväčší prístav v Baltickom mori 
   
   
 Optimizmus nových Estóncov 
 Ľudia, ktorí Estónsko navštívili v ostatných rokoch viackrát, hovoria, že krajina sa zmenila na nepoznanie. Slobodný trh vyrobil nových, históriou nezaťažených Estóncov. Učia sa jazyky, tykajú si s informačnými technológiami. Sú ako naši mladí – chcú veľa zarábať a užívať si. 
 Stredná a staršia generácia nevychádza z údivu ako sa dnes treba obracať a ponosuje sa na zhoršenie kvality života. „Starí nestíhajú“, povedal mi 26-ročný Kaarel pri pohári najlepšieho domáceho piva Saku. Rodičia päťdesiatnici ťažko chápu zmeny. Majú nepoužiteľné vzdelanie z bývalého režimu. Do školy chodili pred tridsiatimi rokmi a orientovať sa v trhovej ekonomike im spôsobuje problémy. Mladí veria, že budúcnosť bude fajn, aj keď ich staré mamy ledva dokážu vyžiť z dôchodkov. 
 Estónci rýchlo zistili, že fínsky ťah na informačné technológie je bránou do budúcnosti. Severný sused je silnou inšpiráciou a Estónsko by rado sebe i svetu dokázalo, že vie rovnako dobre narábať s informačnými sieťami ako s rybárskymi. Estónci patria k najnadšenejším prívržencom moderných internetových aplikácií a množstvo úradných úkonov môžu vybaviť elektronicky, dokonca tak môžu aj voliť. V tom sú ďaleko pred nami... 
 Jazykovo si Estónci rozumejú jedine s Fínmi. Do spoločnej ugrofínskej jazykovej vetvy patrí i maďarčina, tá je však v týchto končinách nepoužiteľná. Napríklad „do you speak English?“ sa v estónčine povie „kas te räägite inglise keelt?“ Kto má tomu rozumieť... našťastie – čoraz viac mladých na túto otázku odpovie – „yes!“ 
   
  
 Muž v zapadnutej uličke starého mesta relaxuje pri novinách 
   
  
 Mladí zarábajú a starí sa snažia prežiť ako vedia. Hoci aj prilepšením si almužničkou pri kostole. 
   
   
 Tallinn 
 Imidž mesta má občas podobu guláša. Vedľa seba tu žijú škandinávske, estónske i ruské prvky, no aj päťstoročné domy vedľa mrakodrapov. Ambíciou Tallinnu je stať sa prirodzenou metropolou Pobaltia. Lotyšská Riga je v regióne najľudnatejším mestom, výhodou Tallinnu sú ambiciózne reformy a blízkosť vyspelej Škandinávie. Litovský Vilnius neleží na pobreží, ako ostatné metropoly Pobaltia a Škandinávie. 
   
   
 Historická metropola dostáva znaky moderného, "proeurópskeho" mesta 
   
  
 Niektoré scény pôsobia ako zo starých sovietskych dokumentárnych filmov 
   
  
 Ruská menšina v Estónsku sa snaží zviditeľniť svojou kultúrou. Napríklad v uznávanej reštaurácii Troika 
   
  
 Údajne najstaršia lekáreň na svete z roku 1422 v tallinnskom centre 
   
   
 Vôľa nepoddať sa 
 Estónsko je prerastené prírodou, je všade na dotyk. Pobrežie krajiny lemuje 1 521 ostrovov, husté lesy sú plné zveri. Súčasníci zdedili po svojich pohanských predkoch úctu k prírode, severskú nepoddajnosť a vieru vo vlastné sily. Svoj sklon potláčať emócie chápu ako daň zádumčivosti a zároveň vôli nepoddať sa osudu. K mentálnej výbave Estóncov patrí istá rezervovanosť, podobne ako v iných severských krajinách. Faktom je, že táto vôľa im umožnila prežiť. Nie je ich veľa, pretože v 1,5-miliónovovej krajine tvoria polmiliónovú menšinu Rusi. Hoci ruština je tu druhým najrozšírenejším jazykom, „noví“ Estónci dávajú prednosť angličtine. 
 Identitu si Estónci zachovávajú aj v cudzine (najmä vo Fínsku a vo Švédsku), kde majú svoje školy a jazyk udržiavajú pri živote už po mnohé generácie. Vzorom húževnatosti malého národa je atlét Erki Nool, ktorý na letnej olympiáde v Sydney získal zlato v desaťboji. Ďalším „veľkým medzi malými“ je majster sveta a olympijský víťaz v behu na lyžiach Andrus Veerpalu. Znalcom klasickej hudby je známe meno Arvo Pärt (skladateľ). 
 Psychológ Anti Liiv vysvetľuje „ľadové tváre“ svojich krajanov historickou skúsenosťou, podľa ktorej sa im lepšie oplatilo mlčať a ticho vzdorovať. Európe je však sympatická iná vlastnosť – zmysel pre racionalitu a vedomie, že aj keď reformy prinášajú ťažkosti, treba ich uviesť do života. Estónsko patrí k najmenej zadlženým krajinám. Aj vďaka tomu Európa nepochybuje, že Estónci si euro zaslúžia. 
   

