

 ... Taktiež je prameňovi známe, že objekt v spoločnosti riaditeľa xxx sa zúčastnil konferencie asi v októbri vo Vysokých Tatrách, celú konferenciu podľa prameňa mal viesť riaditeľ xxx. Tejto konferencie sa zúčastnili aj osoby z kapitalistického zahraničia a to z USA, Japonska, Holandska a iných socialistických štátov. Riaditeľ xxx si objekta so sebou bral z toho dôvodu, aby mu robil tlmočníka, rozprával údajne s Američanom, avšak podľa jeho vyjadrenia iba služobne. 
   
 V závere schôdzky prameň uviedol, že ich riaditeľ xxx dostal pozvánku na rok 1986 do Japonska a prameň usudzoval z toho, že určite bude tento chcieť zobrať so sebou aj objekt „FILOZOF“, nakoľko tento tam určitú dobu pôsobil a pozná tamojšie pomery. Iné poznatky k objektovi zistené neboli... 
 Poznačil: ppor. yyy 
 Výlohy na schôdzke vznikli 32,50 Kčs. 
   
 Na konferenciu v Tatrách si ma riaditeľ zobral, aby si overil moje schopnosti tlmočiť v jazyku anglickom a pohybovať sa v kruhu zahraničných osôb. Podľa všetkého som sa mu pozdával. V náväznosti na konferenciu v Tatrách si ma chcel krátko na to zobrať aj na konferenciu do Budapešti. Pozval si ma do kancelárie, zadal mi hŕbu roboty, aby som sa pripravil. 
   
 „V poriadku, rád pôjdem, lenže mňa Krajská správa pasov a víz nechce pustiť ani na dvojdňový zájazd s ROH do Užhorodu,“ vravím ja. 
   
 „A aké majú námietky,“ opýtal sa. 
   
 „Vraj to nie je v štátnom záujme,“ citoval som mu príslušný paragraf zákona, na ktorý sa Krajská správa pasov a víz odvolala, keď mi zamietla cestu do Užhorodu. (http://vratny.blog.sme.sk/c/220313/Nepriatelska-osoba-6-Jemne-vydieranie-zajazd-s-ROH-do-Uzhorodu.html)  
   
 „Čo? Všetko ti vybavím.“ A v mojej prítomnosti zatelefonoval priamo náčelníkovi na Krajskú správu pasov a víz. „Jeho cesta je v štátnom a podnikovom záujme,“ zdôrazňoval do telefónu.  
   
 „Priprav sa, ideme“, dodal mne a prepustil ma. 
   
 Na druhý deň ráno mi zavolal jeho asistent, že veru moja cesta do Budapešti bola zamietnutá. 
   
 „Vás to asi mrzí viac, ako mňa,“ odpovedal som. „Ja som to očakával.“ 
   
 Vecou som sa viac nezaoberal, ale dopočul som sa od kolegov, že riaditeľ mal v tejto veci veľmi podráždený rozhovor s náčelníkom Krajskej správy verejnej bezpečnosti. “Ak je špión, tak ho zavrite. Ak nie je špión, tak ho neobmedzujte“, zdôrazňoval. Bol to rozumný argument, lenže eštebáci a rozum nešli dokopy. 
   
 Jednalo sa o moje druhé zamestnanie po návrate do Československa. Pred prijatím si ma riaditeľ osobne pozval do kancelárie na rozhovor. „Ste špión?“ bola jeho prvá otázka potom, ako som sa usadil.  
   
 „Azda neočakávate, že by som sa vám priznal, keby som ním bol,“ znela moja odpoveď. 
   
 „No vy nie ste špión,“ zamrmlal pre seba. 
   
 Podľa všetkého na moje prijatie do zamestnania mal významný vplyv rozhovor riaditeľa s mojim vedúcim z predchádzajúceho zamestnania. Riaditeľ vo veľmi dôvernom rozhovore chcel odpoveď na tri otázky: či som pracant, či viem po anglicky a či spolupracujem s ŠtB. Pozitívna odpoveď na prvé dve otázky a neistá, skôr negatívna na tretiu otázku rozhodla o mojom prijatí. 
   
 Do toho Japonska riaditeľ nakoniec šiel, samozrejme bez mňa, hoci som mu vybavoval všetku anglickú korešpondenciu, aj víza na japonskom veľvyslanectve v Prahe. V tých dobách iní by za možnosť dostať sa hoci len na týždeň do Japonska azda neváhali sa ísť „poradiť“, však už každý vie kam, a podpísali by hocijaký súhlas k spolupráci. Ja som na také príležitosti nereflektoval. A vôbec nie je pravda, že by som sa v Tatrách s Američanom bavil len služobne, ako tvrdil prameň. Bavili sme sa o všetkom možnom, aj o politike, a Američan sa výborne bavil na politických žartoch „o zlatú mrežu“, na ktorých sa v tých dobách potajme smiali občania celej socialistickej vlasti. Keď som mu ich chrlil, dbal som samozrejme na to, aby nebol prítomný nik, čo by neskôr mohol zahrať eštebákom úlohu prameňa. 
   
 Na pracovisku sa ma snažili zapojiť do práce s cudzincami, lebo veď sme mali dovozy i vývozy, zahraničné stroje a zariadenia a cudzie jazyky na pracovnej úrovni ovládal vtedy málokto. Lenže to bola oblasť, ktorú mali pod palcom eštebáci, túto právomoc si žiarlivo strážili a prísne sledovali. Svedčí o tom aj nasledovný záznam z môjho spisu, vyhotovený na základe rozhovoru s mojim kolegom v práci. 
   
 Čo sa týka zamestnania prameň uviedol, že svoju prácu sa objekt „FILOZOF“ snaží vykonávať k spokojnosti svojich nadriadených. Vzhľadom k tomu, že jeho nadriadení vedia, o akú osobu sa jedná, ktorá veľmi dobre ovláda nemecký a anglický jazyk, z toho dôvodu ho využívajú k rôznym prekladom a zvlášť riaditeľ, ktorý často cestuje po kapitalistických štátoch a pokiaľ donesie nejaké prospekty, alebo knihy, tieto dávajú objektovi „FILOZOF“ prekladať. Za to, že ovláda cudzie jazyky objekt „FILOZOF“ nedostáva žiadne finančné prostriedky, ale revanšujú sa mu formou odmien. Prameň sa domnieva, že objekt „FILOZOF“ môže prekladať aj také skutočnosti, ktoré tvoria predmet utajovaných vecí, ku ktorým majú prístup iba osoby, ktoré sú na to kompetentné.  
   
 Teraz jedna skúsenosť. Vedúci ma zavolal, že v podniku máme nejakých cudzincov a či by som mu nešiel tlmočiť. Samozrejme som šiel, ja s tým nemám problém. Jednalo sa o dvojicu z nejakého holandského alebo dánskeho podniku, dodávateľa materiálov potrebných pre našu výrobu. Obchodný rozhovor ubehol štandardne, ako hocikde inde vo svete a po skončení si ma vedúci znovu zavolal.  
   
 „Musíme o rokovaní napísať správu do vedenia podniku,“ povedal.  
   
 „Samozrejme“, súhlasil som a napísali sme správu o obchodnom rokovaní a čo z neho vyplývalo pre obchodný úsek, výrobu, technikov a podobne. Proste štandardné veci, ktoré sa píšu o obchodných rokovaniach v podnikoch po celom svete.  
   
 Potom vedúci vybral ešte jeden formulár. „Túto správu musíme tiež napísať,“ vraví.  
   
 Jednalo sa o rozmnožený formulár s otázkami typu „hovorili ste o vojenských záležitostiach, ak áno o čom, hovorili ste o politických záležitostiach, ak áno, o čom, hovorili ste o československých emigrantoch žijúcich v zahraničí, ak áno o čom“. Tento formulár už očividne nebol pre účely podniku a ja som sa nemal k činu. Nechal som ho ležať na stole pred sebou, ani som sa ho nedotkol. Bral by som to ako pošpinenie rúk. 
   
 „Dobre,“ vraví veľkoryso vedúci, „ja formulár vyplním aj za teba.“ 
   
 A všade vpísal nie, nie, nie, lebo sme skutočne viedli rozhovor iba o technických záležitostiach a politika, vojenské záležitosti, či emigranti, by mi pri tej príležitosti ani len neprišli na um.  
   
 „No ale treba to podpísať“, vraví vedúci. A keď videl, že sa stále nemám k činu, povedal. „Však to môžem podpísať aj za teba“.  
   
 Podpísal a k veci sme sa už nikdy nevracali. Ani mi nikto nikdy nevyčítal, že som formulár nevyplnil a nepodpísal. Len ma už viac na rokovania s cudzincami nepozvali. Nie že by v podniku až tak fandili eštebáckym spôsobom, len vedeli, že ak ma budú posielať na obchodné rokovania s cudzincami a ja zamietnem písať aj takéto správy, ja síce referentom zásobovania zostanem, ale vedúci prestane byť vedúcim, námestník námestníkom, riaditeľ riaditeľom. Totalita fungovala aj v tomto. A ktokoľvek by ich nahradil, by sa už údajným štátnobezpečnostným záujmom bez výhrad podriadil. Medzičasom som kľúčovým osobám podniku dal vedieť, že som ochotný byť podniku užitočný v plnom rozsahu svojich schopností a síl, len nesmie si to vyžadovať priamu osobnú súčinnosť s ŠtB. Rešpektovali mi to. 
   
 Prekladať a riešiť korešpondenciu a rôzne písomnosti v cudzích jazykoch ma nechali, ale obchodné rokovania s cudzincami a cesty do zahraničia zostali pre mňa tabu. Spôsobil som si to samozrejme sám svojou neprispôsobivosťou. Zahraničný obchod socialistického Československa ako-tak fungoval, čo znamená, že tisíckam, možno desiatkam tisícok občanov takéto formuláre nevadili. Dnes, ako živnostník, robím poskromne aj zahraničný obchod, aj s cudzincami rokujem a žiadnej kontrarozviedke nemusím skladať účty. Je to moja vec. Som rád, že som sa dožil dnešnej doby. 
   
 Ešte späť k poznámke, že „prameň sa domnieva, že objekt „FILOZOF“ môže prekladať aj také skutočnosti, ktoré tvoria predmet utajovaných vecí, ku ktorým majú prístup iba osoby, ktoré sú na to kompetentné.“ Jednalo sa o rôzne odborné články a publikácie organizácií a podnikov, v zahraničí bežne dostupné pre každého, kto o ne prejavil záujem. Riaditeľ ich prinášal z podnikov, konferencií, veľtrhov a výstav, ktoré hojne navštevoval. 
   
  
  

