
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Komžík
                                        &gt;
                Politika
                     
                 Keď je niekto ..., nemôže byť lastovička 

        
            
                                    26.10.2008
            o
            10:10
                        |
            Karma článku:
                9.26
            |
            Prečítané 
            2674-krát
                    
         
     
         
             

                 
                    V biznise, v ktorom pracujem, je práve u mňa obdobie plnej sezóny. V preklade to znamená toľko, že padám na hubu, prechádzam choroby z vyčerpania a oslabenia imunitného systému, hádžem do seba tabletky, nič súkromné ani osobné nestíham a to všetko s cieľom zarobiť nejakú tú korunu, vďaka ktorej dokážem ja i moja rodina vyžiť, zabezpečiť si základné životné potreby, prípadne si i občas čosi „dopriať“. Je logické, že počas tohto obdobia popri 18-20 hodinách intenzívnej práce denne nemám veľa času na podrobné sledovanie vývoja našej politickej scény a už vôbec nemám čas zaujať k tomu svoje stanovisko. V poslednej dobe však sledujem také nakopenie zaujímavých (bodaj by to bolo len o tej zaujímavosti!) momentov na našej politickej scéne, že mi nedá nereagovať.
                 

                 
 www.antifico.sk
    Paradoxné na mojej práci je, že až po skončení sezóny viem, či sa mi nejakú tú korunu podarilo zarobiť alebo nie. To, či skončím v pluse alebo nie závisí od mojich schopností a šikovnosti. To, či a kedy mi odberatelia zaplatia závisí od ďalších rozličných faktorov. Isté je len to, že štát si na svoje jednoznačne príde. Či už vo forme daní, odvodov, DPH, či iných mechanizmov určených na vyberanie poplatkov (nechcem to nazvať priam výpalným, i keď občas mi to tak pripadá) od občanov a firiem, slúžiacich na zabezpečenie chodu a rozvoja štátu. My ako občania, ktorí sa na tento balík spoločne skladáme by sme mali mať logicky možnosť k téme hospodárenia s tými financiami sa vyjadriť. A to nielen vo voľbách podporou toho „nášho“ kandidáta, ktorý to podľa nás bude robiť dobre, ale aj v priebehu samotného funkčného obdobia. Zvlášť, ak sme na neschopnosť tej súčasnej garnitúry poukazovali už pred jej nástupom do preudofunkcií alebo keď cítime, že spätnou väzbou nášho kŕmenia štátu je namiesto pomoci zo strany štátu iba produkt trávenia..      Reagovať na všetko sa nedá. Nakoniec, stostranový blog by sa asi nikomu nechcelo čítať. Pár reakcií si však neodpustím. V prvom rade sa chcem pozastaviť nad fenomenálnym vyriešením Vladenkového splatenia pôžičky za Elektru. Pán Mečiar, dovoľte mi poblahoželať vám. Nemý úžas! Že som sa JA tak dobre neoženil!!! Normálne zvažujem možnosť prehodnotenia môjho manželstva a následnú výmenu manželky. Veď sa ich predsa len musí nájsť viac na tomto svete s alchimistickými vlastnosťami, ktoré mávnutím čarovného prútika urobia biznis jak hovädo a moje úspory z večera do rána narastú o niekoľko tisíc percent! Ak budem dobre hľadať, snáď sa mi to podarí a aj ja si kúpim nejaký ten skromný penziónik, kde budem robiť zabíjačky s mojimi kamarátmi.      K návrhu štátneho rozpočtu mi neprináleží sa vyjadrovať. Na to sú tu iní profíci, ktorí to vedia posúdiť ďaleko lepšie ako ja. Ale z toho čo som mal možnosť doteraz postrehnúť mi je úplne jasné, že nepotrebujem na to logaritmické pravítko, aby som pochopil, že daný návrh je nereálny blud založený na pseudočíslach a podsúvaný verejnosti s jediným cieľom – zavádzať občanov a manipulovať s verejnou mienkou. Ono to pán minister síce navonok vyzerá všetko pekne, ale reálne čísla sú predsa len niečo iné ako tie fiktívne a verte mi, že sa nájde kopec ľudí ktorí sa tomu rozumejú a vytrúbia to aj medzi pospolitý ľud.      Samostatnou kapitolou na našej politickej scéne je pán Slota, neustále mútiaci vodu. Som si vedomý faktu, že otázka slovensko-maďarských vzťahov je veľmi citlivá téma a, žiaľ, balansovanie na veľmi tenkom ľade. „Žiaľ“ a „balansovanie“ preto, lebo osobne si myslím, že táto bublina by vôbec neexistovala, nebyť neustáleho prikladania polienok do ohňa zo strany našich popredných národniarov na čele s pánom Slotom a, ruku na srdce, aj zo strany jeho zrkadlového obrazu – pána Duraya a pár jeho verných kamarátov. Udržovanie tejto témy v neustále živej a aktuálnej rovine je živnou pôdou pre zvyšovanie nenávisti určitej skupiny voličov, čoho dôsledkom je udržovanie si preferencií skupiny ľudí sústredených v SNS. Voličská základňa SNS zrejme potrebuje stále počúvať takéto „príbehy“, ktoré ich očividne rajcujú do niečoho, čo my ostatní zjavne nechápeme a ani nikdy nepochopíme, pretože nenávisť založená na preudopredsudkoch sa normálnym ľuďom ani pochopiť nedá. Kúsok mi to príde celé postavené na hlavu, keď si pomyslím, aké je to celé v reálnom živote. Poznám množstvo Maďarov v južného Slovenska, množstvo Slovákov z rovnakého regiónu, ale nepoznám nikoho z nich, kto by mal so spolunažívaním s tou druhou národnosťou problém. Nakoniec, za stáročia spolunažívania sa už tieto národnosti tak rodinne aj krvne premiešali, že dnes sa už v podstate u väčšiny z nich nedá ani určiť kam ich zaradiť. Sú to bežní ľudia žijúci si svoje životy, starajúci sa o seba, svoju rodinu, platiaci rovnako ako ja dane tomuto štátu a vyžadujúci vďaka tomu rovnaké podmienky ako má trebárs Žilina alebo Banská Bystrica. A paranoidné predstavy alkoholom vymytého mozgu o obrnených transportéroch útočiacich na Slovensko ich nejako zvlášť nemôžu osloviť ani chytiť za srdce..      Jahôdkou na torte je opäť náš, s prepáčením, premiér. Jeho červené srdce už v minulosti viac krát útočilo na naše životné hodnoty, na princípy slobody, na základy ľudského práva a často i na naše bránice. Veľakrát sme si už po jeho výrokoch ťukali na čelo a vraveli sme si, že toto snáď nemôže byť pravda, že toto si nemôže dovoliť. A rovnako veľa krát sme sa presvedčili, že môže. Nakoniec, s takými prisluhovačmi akých má okolo seba a s tými tonami cieleného populizmu ktorými nás každodenne zavaľuje to zas nie je až taký problém. Jeho najnovšie výroky však smerujú k počinu, ktorý môže byť šľapnutím do osieho hniezda. Pretože tento krát nechce siahnuť na naše práva a slobody, ktoré si stále zrejme prostý ľud ktorý prežil celé desaťročia v totalite zväčša ani neuvedomuje, ale na naše peniaze. Siahnutie na druhý pilier je totiž posvätný a nedotknuteľný krok, je to dotknutie sa súkromných úspor občanov. Jedným slovom znárodnenie. A to je už teda silné kafe !!! Na margo šikovnosti pána premiéra treba však uviesť zásadný fakt, že tento krok šikovne schoval pod hlavičku globálnej ekonomickej krízy, vďaka čomu ho bude tvrdošijne obhajovať ako službu pre občanov. A väčšina z nich mu to štandardne zožerie. Elementárny princíp je však jednoduchý – siahnuť niekomu na jeho súkromné peniaze je bezbrehá krádež a je jedno či tak urobí zlodej alebo štát.     Úsmevné je sledovať, ako sa k tomu šafáreniu, neetickému správaniu či šliapaniu základných ľudských práv a slobôd zo strany našich politických hláv stavajú bežní občania. Ten úsmev je však smiechom cez slzy. Jediný, kto sa tu smeje naozaj, sú totiž práve tí hore. S plným vedomím, že zase z toho vyjdú bez ujmy a že pospolitý ľud im to zase zožerie aj s navijakom. V tejto súvislosti som si spomenul na jeden obľúbený citát môjho známeho – „Keď je niekto ko..t, nemôže byť lastovička“. Ja som zvykol k tomu pridať ešte môj osobný dodatok – „A keď o tom nevie, tak mu to treba povedať“. Nastoľuje sa mi tu však otázka, že kto vlastne teraz tí ko..ti sú? Tí hore, ktorí to celé vymyslia či spôsobia alebo tí dole, ktorí to celé dopustia? A potom v krčme búchajú päsťou po stole, že ako im je všetkým na hovno...   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (37)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Komžík 
                                        
                                            Konečne !!!!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Komžík 
                                        
                                            Skutoční hrdinovia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Komžík 
                                        
                                            Prehnitá koalícia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Komžík 
                                        
                                            Pán Fico, opäť šafárite s naším majetkom alebo ste taký bohatý?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Komžík 
                                        
                                            Pán Gašparovič, ste plytký.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Komžík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Komžík
            
         
        komzik.blog.sme.sk (rss)
         
                                     
     
        Som človek milujúci slobodu vo všetkých jej formách. Možno trochu kontroverzný, každopádne však s vlastným názorom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1276
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Song na podporu Falun gong
                                     
                                                                             
                                            No comment. Proste podoba..
                                     
                                                                             
                                            Superstar - večná téma...
                                     
                                                                             
                                            Peking 2008
                                     
                                                                             
                                            Norika a Braňo...
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sady a vinice č.2/2008
                                     
                                                                             
                                            .týždeň
                                     
                                                                             
                                            Z.Grzyb, E.Rozpara - Intensywny sad sliwowy
                                     
                                                                             
                                            Ian McEwan - Betónová záhrada
                                     
                                                                             
                                            Sad nowoczesny
                                     
                                                                             
                                            Michal Hvorecký - Eskorta
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            DJ Cheeba (RIP) - sets
                                     
                                                                             
                                            HT - Absinth
                                     
                                                                             
                                            PiiJem - Live at Pohoda (Radio_FM)
                                     
                                                                             
                                            Náhodný Wískit - Nová
                                     
                                                                             
                                            Chór vážskych muzikantov - Pank
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Richard Sulík
                                     
                                                                             
                                            Jakub Lorenz
                                     
                                                                             
                                            Tomáš Holetz
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Hnutie za pokojnejších stánkárov
                                     
                                                                             
                                            HT
                                     
                                                                             
                                            Kohaplant
                                     
                                                                             
                                            Pohoda
                                     
                                                                             
                                            Hodokvas
                                     
                                                                             
                                            Karpatské chrbáty
                                     
                                                                             
                                            Chór vážskych muzikantov
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




