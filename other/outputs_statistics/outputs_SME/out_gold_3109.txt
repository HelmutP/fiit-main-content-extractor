

   
 Kultúrny šok je  psychický, čiastočne aj fyzický (napríklad zmena telesnej hmotnosti) prerod spôsobený zmenou životného a kultúrneho zázemia na určitú dobu. Najlepšie pozorovateľný je pri dlhodobom pobyte mimo domova. Prejavy kultúrneho šoku sú veľmi individuálne. Závisia od osobnostného nastavenia človeka, od situácie, v ktorej sa ocitol, od času, ako dlho v nej bude, od perspektívy jeho/jej najbližších týždňov, od spoločenstva, v ktorom sa vyskytuje, od úloh, ktoré plní, od jeho/jej sociálnych i komunikačných kompetencií a podobne. Pri dlhodobom pobyte sa delí na  4-5 základných častí. 
   
 1.časť – Nadšenie – Nastáva hneď po príchode do nového prostredia. Nadšenie vyvoláva takmer všetko. Odlišené správanie ľudí, odlišné návyky, jedlo, životný štýl, počasie, objavovanie všetkého nového a doposiaľ nepoznaného. Zvyčajne sa jedná o najkratšiu z častí (trvá približne 3-4 týždne). 
 2.časť – Otrávenie – Jedná sa o najhoršiu fázu zo všetkých. Práve v tejto nastáva prerod, ktorý sa v ďalších fázach už iba kryštalizuje. Všetky veci, ktoré spočiatku spôsobovali nadšenie, začnú teraz neskutočne liezť na nervy. Od prejavov ľudí, cez televízne programy až po cudzí jazyk, v podstate všetko, čo je iné ako doma. Ide o jednoznačne najdlhšiu časť, ktorá sa môže pretiahnuť až na štyri mesiace. 
 3.časť – Zvykanie si – Dĺžka je iba o niečo dlhšia ako pri prvej časti. Pravdepodobne ide o časť najpríjemnejšiu. Postupne sa s vecami, ktorá nás ešte nedávno žrali vyrovnávame, zžívame sa v kultúrou, prijímame zvyky a stávame sa časťou miestnej kultúry. 
 4.časť – Multikultúrny človek – Nastáva kompletný prerod s neobmedzenou dĺžkou. Stal sa z nás človek, ktorý sa zžil a pochopil cudziu kultúru a popri tom má stále v sebe zakódovanú tú vlastnú. Kedykoľvek sa vrátime do danej kultúry, sme schopní prepnúť a žiť s miestnymi obyvateľmi. 
   
 5. časť (v niektorých prípadoch k nej nemusí dôjsť) – Obrátený kultúrny šok- Nastane po návrate domov, do našej pôvodnej kultúry. Trvá dohromady cca. 1 mesiac. Počas tohto mesiaca prebehnú všetky vyššie menované fázy, v kratšom čase a menšej intenzite. Až po dovŕšení tejto piatej časti sa človek stane skutočne multikultúrny. Oficiálne sa hovorí iba o štyroch častiach, pretože piata už nemusí nikdy nastať, človek totižto už novú krajinu opustiť nemusí a tým pádom zabúda postupom času na svoju pôvodnú kultúru a po návrate späť si musí kultúrny šok zopakovať. 
   
 Z denníka Tomáša Turanského. 
 1.časť – Nadšenie. 
 07.08.2009 – piatok 
 Už tu budem pomaly týždeň a musím zaklopať, ale všetko je zatiaľ skvelé. Ľudia sú veľmi príjemní, starajú sa naozaj prvotriedne. Práve som doobedoval, mal som skvelý steak, fazuľu, kukuricu a polentu. Presne na jedlo tohto typu som sa tak tešil. Maximálna spokojnosť. 
 19.08.2009 – streda 
 Bol som druhý deň v škole. Včera tam bola naozaj perfektná atmosféra, všetci ma zdravili,všeličo sa ma vypytovali , niektorí sa dokonca sa so mnou fotili. Aspoň sa nenudím. 
 2.časť – Otrávenie. 
 1.10.2009 – štvrtok 
 Tak som tu už pomaličky dva mesiace, ani sa mi to nezdá. Smútok za domovom sa za posledný mesiac poriadne vystupňoval. Najradšej by som si na celý čas pustil do uší Sex Pistols, lebo už sa mi v škole z tých afektovaných ľudí hovoriacich portugalsky otvára nožík vo vrecku. Mám plné zuby ich nacvičených úsmevov, priateľského potľapkávania, fazule, ničnerobenia, dažďa...Nejdem sa ďalej sťažovať lebo mi šibne. 
 27.10.2009 – utorok 
 Boha, tak strašne sa nudím. Pokazil sa mi VAIO, dnes by som ho mal ísť zaniesť do opravy, som skutočne zvedavý, čo s tým bude. Bolí ma hrdlo a včera som mal horúčku. Paráda ! 
 28.10.2009 – streda (písané hrubou červenou fixkou) 
 Je tu v tomto diári vidieť moje postupné výkyvy nálad. Pripadám si ako blázon !!! 
 17.11.2009 – utorok 
 Čím dlhšie som sám, tým samozrejmejšie mi to pripadá. Prestávam postrádať, neprestávam však spomínať, čo nie je dobré, lebo za každou spomienku si predstavím, ako dlho to ešte mám k návratu. Musím sa toho vyvarovať. Mimochodom štve ma slnko, stále tu svieti. 
 23.12.2009 – streda 
 Môj najsmutnejší okamih. Pozeral som Mrázika a Tri oriešky pre Popolušku ,ale náladu mi to rozhodne nezlepšilo. Rozhodol som sa, že týmto sviatočným obdobím iba preplávam a nebudem ho komentovať, akurát by som si to zhoršil. Veselé Vianoce. 
 3. časť – Zvykanie si. 
 11.01.2010 – pondelok 
 Zajtra na mesiac odchádzam. Konečne nejaká vzpruha. Uvidím, čo to bude ale každopádne sa teším a dúfam, že po týchto hovno-mesiacoch to bude stáť za to. 
 15.02.2010 – pondelok 
 Cestovanie po Brazílii bolo naozaj skvelé, niektoré miesta vo mne zanechali hlboký dojem. Za pár dní začína škola, posledná etapa môjho pobytu. 
 03.03.2010- streda 
 Ako som si teraz dôkladne tento diár preštudoval, zistil som, že takmer vôbec som nepísal, keď som sa cítil viac menej dobre. Zvyčajne som vyjadroval iba hnev, smútok, sklamanie. Ku každému zápisku som si navyše poznačil hudbu, literatúru a filmy, ktoré som v tej chvíli pozerával a zistil som, že jedno s druhým vzájomne súvisí (hudba od nálady, následne nálada od hudby...). Odo dnes ma čaká ešte 64 dní,sám som zvedavý, akým smerom sa budú moje myšlienky uberať. Všimol som si, že postupom času som dané veci prestal konkrétne pozorovať, prestával som ich vnímať ako niečo iné. Zvykol som si. Vlastne ešte stále si zvykám. Či to dotiahnem do úspešného konca sa uvidí...ale dám o tom na blogu vedieť J. 
 (Nasledujúci text som vytvoril výhradne pre pobyt 10-12 mesiacov po rozhovoroch s viacerými ľudmi vo veku 16-19 rokov, moje názory sú viac menej subjektívne). 
  
   
   

