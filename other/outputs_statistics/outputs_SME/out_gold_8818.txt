

   
 keď plynie. trvalo veľmi dlho kým moja škrupina praskla. dovtedy vápenatela a priliepala sa k telu. odcudzovala som sa pravde. 
 najprv to bola pýcha čo ma vzdialila od podstaty, potom zlosť a bezmocnosť v nemocnici, a nakoniec nezmierenie. so stratou dieťaťa. s tým, že som sa  ako vydatá zaľúbila do iného muža. po roku sa odsťahovala, prechádzala katarziou, dnom, sústavou ponížení, obvinení, bola som na seba prísna, zavrela som sama seba do samotky, bez svetla, konfrontácia. 
 s mužom zákonitým aj novoobjaveným som sa stretávala. pri stretnutiach som sa ako slimák priplazila, zhodila ulitku, nechala na seba útočiť pocity, vina, znechutenie, ľútosť. odchádzala som zdrvená. preplazila som  sa novembrom, vianocami, jarou, občas mi bliklo, a potom sa maják stratil. 
 dala som výpoveď. začala sa vzďaľovať novoobjavenému mužovi a viac počúvať zákonitého, toho čo mi bol blízky ešte dávno pred tým, než som ho stretla. možno prišiel nejaký čas, ktorý mal lámať, drviť, odslimačiť ma. 
 ráno som sa zobudila na povracanie znechutená z mojho plynutia, a začala písať zaťažená vlastným egom blízkemu mužovi esemesky. neozval sa. neodpísal. neprišiel do práce. spotená, s vytreštenými očami, s vnútornou horúčkou aj triaškou, utekala som za ním do ofisu. nedvíhanie telefónu sa mi potvrdilo. jeho úrad, stolička bola prázdna. za výkladom tma. 
 s usedavým plačom som sa s ním v duchu rozlúčila. 
   
 večer zavolal. 
   
 vytiahol ma na horský park. bola som čistá. pravdivá. odrazu som vnímala. kroky, cestu. vzduch aj zvuky. ozveny túžob z minulosti. že pred desiatmi rokmi som kráčala tou cestou dolu, s plným ruksakom ako študentka a tušila som ten moment, ktorý som práve v tej chvíli žila. pravdu. absolútno. 
   
 chcela som už len žiť zlomky momentov. nepovedať, nenapísať. nekomentovať. poďakovať mu za jeho mlčanie. za jeho paralelný príbeh, vďaka ktorému sa mi rozletela ulita. 
   
 takto to je, a chcem aby ste to vedeli, že akékoľvek trápenie svojej duše treba prežiť naplno, že je to cesta. ako tá cesta dolu z horského parku. že takto sa preseká duša k podstate. zoči voči. 
   
 mám rada ľudí. a konečne ich stretávam. a vidím ich smútky a potešenia. 
   
 predvčerom som stretla po trnavskom mýte blúdiacu dievčinu s igelitkou billa, išla z neúspešného pohovoru, nevedela trafiť domov, vysokoškolsky vzdelaný človek, prišla do bratislavy, zarobiť, jej plat je do tristo euro. povedala som jej o svojom blúdení, a hľadaní svojho domova v sebe. vypýtala som si od nej telefónne číslo, nech má pocit, že tu nie je sama. že kedykoľvek zablúdi, je tu niekto, kto jej pomôže nájsť domov. do autobusu nastupovala a usmievala sa. vtedy to bolo pre moju dušu to najviac čo som mohla z vďaky za život urobiť. to najviac. 
   

