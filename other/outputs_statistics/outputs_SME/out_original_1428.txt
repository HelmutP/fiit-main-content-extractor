
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Kačáni
                                        &gt;
                Politika
                     
                 Normálna katastrofa. 

        
            
                                    26.5.2009
            o
            10:00
                        |
            Karma článku:
                17.95
            |
            Prečítané 
            9371-krát
                    
         
     
         
             

                 
                    Kšeft s teplým vzduchom bol jasný úplne od začiatku. Je tu však niekoľko momentov, ktoré by som rád vypichol, nech je téma na zamyslenie. Okolnosti, fakty sú známe, výsledkom je lúpež a pôjde o papačkové peniaze, súdiac podľa fanatickej obrany tohto kšeftu.
                 

                 Osobne ma asi najviac vytáčajú podobné kšefty. Podnikám už asi 16 rokov a viem, ako ťažko sa zarábajú peniaze, koľko musí človek robiť, ak si chce normálnym spôsobom niečo zarobiť. O to viac ma dostávajú do vývrtky práve takýto hajzli, ktorý bez roboty, šmahom ruky zarobia miliardy, ukradnuté nám všetkým.  
 Niet lepšieho biznisu, ako kšeft s teplým vzduchom. S takmer nulovými nákladmi, bez kapitálu, zarobiť rozprávkovú sumu. Založiť firmu v Amerike je smiešna záležitosť. Spomínam si, ako kedysi aj za mnou prišli dôležito sa tváriaci pandrláci, ktorí mi ponúkali založenie firmy v Delaware (USA). Nebolo treba pohnúť zadkom, všetko by spravil nejaký právnik, za približne 300 USD. Pri takýchto firmách (sú to väčšinou P.O. boxové firmy) ide o daňový únik, presnejšie o spôsob, ako vybabrať s daňami. Aby som vylúčil akékoľvek špekulácie, nikdy som to nespravil a nemám žiadnu firmu nikde inde. To len na ilustráciu, aké „ťažké", „zložité" a „nákladné" je založiť firmu v Amerike.   Na tento kšeft teda nebolo treba žiadne peniaze. Ani ľudí. Takýto biznis spraví ľavou zadnou jeden človek. Osobne tipujem, že celá firma bude mať jedného, maximálne dvoch zamestnancov. Majiteľov, hlavne tichých, oveľa viac. Uvedomte si priatelia, že takýto kšeft by dokázal zrealizovať každý z vás, úplne jedno, kto ste a koľko máte peňazí!!! Ak je splatnosť 60 dní, nie je problém! Teplý vzduch sa strelí za cenu bežnú, kúpna cena sa vyplatí a pár miliardičiek zostáva doma. A čo je doma, to sa počíta.   Len drobná poznámka. Čo keby tá firma nezaplatila vôbec? Uvedomuje si niekto tento hardcore? Veď také riziko reálne hrozí! Čo by robili naši volení lupiči? Zatĺkli by to? Požiadali by o exekúciu dvoch garáží? TOTO je podvod ako vyšitý, učebnicový a už si ani nedokážem predstaviť drzejší a nechutnejší spôsob!   Keď si zoberieme nástenkový kšeft, tak pri tom by aspoň niečo Janové firmy museli spraviť, nejakú, aj keď smiešnu robotu (smiešnu za tie sumy) by spravili. V kšefte s teplým vzduchom nič. Len čistý, nehorázne veľký zisk.   A čo na to môj najdrahší a najmilší premiér? Je spokojný, lebo zmluva sa zverejnila. Čo na tom, že sa niekoľko krát flagrantne porušil zákon? Ale má pravdu, nikdy nepovedal, že ak bude zmluva nechutne škandalózna, bude konať. On len chcel zmluvu zverejniť. To sa stalo, nevadí, že nevieme ceny, kto je za garážovou firmou, nevadí, že si tento biznis zabezpečili na 4 roky dopredu, podľa neho je to v poriadku. Zase a znova zakryl zlodejinu Divného Janka!!!   Viete čo je komické priatelia ficovoliči? Nie ja vám nadávam, nie ja vás ponižujem, keď som o vás niekedy písal že ste dementi. To ON sám vás zatriedil do kategórie imbecilov! A robí to neustále. Kryje tak evidentné podvody, zvíja sa ako posolený červík, klame vám do tváre a ponižuje vás tak, ako to asi nikto doteraz nedokázal. Nechápem, ako to môžete zniesť.   Zle bude, pokiaľ si nedokážu voliči pofidérnych strán uvedomiť do dôsledkov, že kradnutie peňazí daňových poplatníkov nie je nejaký virtuálny gavaliersky prečin. Že to sú reálne peniaze, ktoré nám všetkým bastardi ukradli, ktoré budú chýbať na iné veci, za ktoré si lúpežníci financujú svoje úchylné chúťky. Beztrestne!!!   Tisíce podnikateľov drie dennodenne, chorá a perverzná vláda zložená z komunistov, eštebákov, zlodejov a psychopatov sa snaží zobrať im čo najviac peňazí, hádže im polená pod nohy, za sto korunové nedoplatky sa na nich vrhajú exekútori ako besní, aby mohli tí primitívi kradnúť miliardy! Dennodenne bojujú s úradníkmi, byrokraciou, pochybnou morálkou spoločnosti, zlou platobnou disciplínou, často sú zruinovaní nie vlastnou vinou, pričom ich musí šľak triafať, keď vidia, akým spôsobom možno rozprávkovo zbohatnúť zo dňa na deň, bez roboty, bez kapitálu, bez zodpovednosti. Často majú samovražedné nálady, ak vidia, ako si Hara Bin Ládin spravil na Slovensku súkromné súdnictvo. Pokiaľ nie ste nejaký mafián napichnutí na politické špičky, ťažko sa dovoláte práva.   Osobne nie som extra prekvapený, že sa dejú veci ako kšeft s teplým vzduchom. Dalo sa to čakať, ja som to čakal. Oveľa horšie ako ten kšeft je to, čo vystrája nervák. Ak sa ide do lúpeže, tak vždy je riziko, že to praskne, že zlodeja niekto vymákne. Tá perverznosť toho celého spočíva v tom, že ak to prasklo, tak sám najväčší strážca „pokladu" kryje zlodejov. Aj keď dôkazy sú také evidentné a jasné, že jasnejšie už ani nemôžu byť. Toto je oveľa desivejšie, ako samotná lúpež, ktorá ako všetky ostatné zákonite musela prísť nástupom dnešnej hávede do vlády. Lebo to je výsmech nás všetkých, je to poníženie a degradácia celého národa. Súdruh premiér nás má za infantilných a dementných idiotov. Najmä svojich voličov. A ak sa ani po tomto nezobudia, tak ja s ním súhlasím.   Tento článok som napísal už v piatok, teraz sa objavili ešte ďalšie informácie, ktoré perverzitu celého kšeftu násobia. Vyzerá to tak, že lúpežníci kšeftli viac ton céodva, ako mali a cena za tonu bola ešte nižšia, než tá škandalózna, pôvodná.   Na záver úplne najpodstatnejšia otázka. PREČO PREMIÉR TAK VÁŠNIVO KRYJE TEN TUNEL? Nejaké dôvody musia byť! Bojí sa Divného Janka, alebo je v tom namočený aj nejaký jeho sponzor, kamarát, či nebodaj aj on sám? Myslím si totiž, že takýmto postojom si plusové body nenazbiera ani u svojich obdivovateľov, marketingovo by spravil lepšie, keby celý kšeft s teplým vzduchom zrušil. Keďže celý život sa riadil marketingom svojich preferencií, niečo za tým bude, prečo tak nečiní aj teraz. Vyzerá to tak, že kšeft prejde a my všetci zase raz utrieme hubu. A pár hajzlíkov sa bude spokojne uškŕňať.   Milí ficovoliči, váš mesiáš spolu s Divným Jankom vám ukradli miliardy korún, priamo pred očami, primitívnym spôsobom, drzo a verejne. Namiesto ospravedlnenia, vám oznámil váš idol, že sere na to (presnejšie na vás), nezaujíma ho, čo si myslíte, lúpež je dokonaná. Blahoželám.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (39)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Mozog je plastický
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            List jeho excelencii od organizátora protestov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Tušenie budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Najväčšia európska lúpež
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Reinkarnácia Štefánika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Kačáni
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Kačáni
            
         
        kacani.blog.sme.sk (rss)
         
                                     
     
        Potrím k tým, ktorí nikdy neparazitovali na štáte, ktorí nekradli a nekorumpovali. Teda k menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    103
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5713
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            môj iný blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            vysúšanie muriva
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




