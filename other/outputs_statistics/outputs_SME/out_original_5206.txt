
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Cykloturistika
                     
                 Moja dnešná rozcvička 

        
            
                                    25.4.2010
            o
            10:21
                        (upravené
                25.4.2010
                o
                15:36)
                        |
            Karma článku:
                8.62
            |
            Prečítané 
            1905-krát
                    
         
     
         
             

                 
                    Keď je také pekné ráno, ako dnes, idem sa prebehnúť na bicykli ešte pred raňajkami. Dlhé roky som v rámci rannej rozcvičky chodil do Jovsy pre dobrú pitnú vodu. Odkedy máme vodovod s chutnou vodou z Vihorlatských vrchov, občas chodím pre čerstvé mlieko do Poruby pod Vihorlatom. Keď mlieko nepotrebujeme, tak sa len tak prebehnem po jednom z cca 20 kilometrových okruhov. Dnes som sa pustil smerom na juh cez Veľké a Blatné Revištia, do jedinej obce okresu Sobrance s väčšinovým cigánskym obyvateľstvom, do Blatných Remiet.
                 

                 Táto dedina ma fascinuje tým, že aj keď v nej žije podľa odhadov domácich až 70% obyvateľov tmavšieho etnika, je tam poriadok a čistota, ako v máloktorej "bielej" dedine. Žijú v pekných domoch a pri tunajších problémoch s nezamestnanosťou si takmer 30 cigánskych rodín privyrába opatrovateľstvom detí z detských domovov.            Keďže sa nerád vraciam tou istou cestou, ktorou som prišiel, vrátil som sa cez Bunkovce, v ktorých tiež vídam tmavších obyvateľov na dvoroch pekných domčekov, napríklad tohto.      Opúšťam Bunkovce, ako vidíte, cesty 3. triedy tu máme kvalitné a počas poldruhahodinovej bicyklovačky som teraz, v nedeľu ráno, stretol asi 5 áut.      Za Bunkovcami som stretol kamarátovho syna, agronóma neďalekej poľnohospodárskej firmy, čerpali vodu z kanála, postrekovali polia.      Poľnohospodári niekedy musia pracovať aj v nedeľu, aj pri Nižnej Rybnici pastieri dohliadali na pasúce sa kravy.      A o chvíľu som doma, manželka ma čaká s raňajkami. Po raňajkách spracúvam fotky, píšem tento článok. Stihnem aj kostol, menšiu rodinnú oslavu a podvečerný futbalový zápas.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.49
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




