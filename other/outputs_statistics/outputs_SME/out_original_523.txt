
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kron
                                        &gt;
                Nezaradené
                     
                 Príbeh jednej karikatúry (Fico a tí druhí) 

        
            
                                    18.3.2008
            o
            22:06
                        |
            Karma článku:
                11.37
            |
            Prečítané 
            6637-krát
                    
         
     
         
             

                 
                    Aj karikatúra môže mať svoj príbeh. Začal som ju tvoriť v roku 1990, no stále ju akosi nemôžem dokončiť, aj keď by som veľmi chcel.
                 

                  Písal sa rok 1990 a ja som pod vplyvom eufórie tých čias nakreslil nižšie uvedenú karikatúru. Veril som, že prvé slobodné voľby v roku 1990 pošlú týchto pánov: Ladislav Adamec (vtedy predseda vlády a predseda KSČ) a Peter Weiss (vtedy predseda KSS) na smetisko dejín. Veril som, že červený komunistický rak stiahne svoje posledné obete do priepasti, z ktorej sa už nevyhrabú. Pán Adamec po niekoľkých rokoch odišiel do dôchodku, no Peter Weiss ešte dlho zamoroval politický vzduch svojimi pochybnými ideálmi a taktikou priskočstrany, ktorá dovoľovala vyniknúť a dostať sa k moci takým indivíduám ako Vladimír Mečiar, Ján Slota či Ján Ľupták.     Chcel som túto karikatúru uverejniť v nejakom periodiku, vtedy populárnom Roháči či Kocúrkove, no akosi z toho zišlo a karikatúra putovala do stolíka.                                                      V roku 2002 som túto kresbu opäť „vyhrabal“, lebo som zacítil, že by znova mohla byť - po istom vynovení - aktuálna. Vyzeralo to totiž, že SDĽ, nástupkyňa KSS, definitívne končí, pretože z nej odišla „mladá krv“: Ftáčnik, zakladateľ Weiss či populárna Šmegi (Brigita Schmögnerová) a kormidla sa ujali neškodní (a našťastie neschopní) páni v rokoch: Jozef Migaš a Pavel Koncoš.                                                             SDĽ skutočne skončila, no spolu s odídencami z SDA ju pohltil ďalší červený moloch SMER-SD. Jeho líder Róbert Fico sa spojil s oživenými mŕtvolami: Vladimírom Mečiarom a Jánom Slotom a vládne nám spôsobom, ktorý každý deň musím predýchavať, aby som sa z toho nepototo. Povedal som si, že prerobím karikatúru ešte raz a teraz ju aj zverejním, s prianím, aby som ju už nikdy nemusel vyťahovať. Viem, že v nadchádzajúcich voľbách to možno ešte nebude, ale skôr či neskôr sa s nimi história vysporiada, verím, že červený rak sa s nimi odoberie do svojej skrýše a nielenže ICH nepustí, ale že si už nepríde po žiadnych iných takto postihnutých, lebo už žiadni nebudú; aspoň nie pri moci.                                   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kron 
                                        
                                            Keď o dieťati rozhodne až súd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kron 
                                        
                                            Falšovanie obrazov na Slovensku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kron 
                                        
                                            Čo Nemec, to fašista?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kron 
                                        
                                            Viete, že sa už falšujú aj vŕtacie kladivá HILTI a pracie prášky?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kron 
                                        
                                            Škoda, mohol som mať pekné Vianoce!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kron
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kron
            
         
        kron.blog.sme.sk (rss)
         
                                     
     
         Píšem, prežívam, uvažujem, tvorím... Až sa mi nazbieralo na román - nech sa páči:   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2681
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Fikcia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Krátke
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Peter Pišťanek - Traktoristi a buzeranti
                                     
                                                                             
                                            Albert Camus - Pád
                                     
                                                                             
                                            Bohumil Hrabal - Tři novely
                                     
                                                                             
                                            Orhan Pamuk - Nový život
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Vangelis  Reprise 1990 - 1999
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jozef Mikloško
                                     
                                                                             
                                            Ivana Pajanková
                                     
                                                                             
                                            Milka Katriňáková
                                     
                                                                             
                                            Vladimír Schwandtner
                                     
                                                                             
                                            Boris Burger
                                     
                                                                             
                                            Anna Moravčíková
                                     
                                                                             
                                            Katarína Domanská
                                     
                                                                             
                                            mária lazárová
                                     
                                                                             
                                            Marek Sopko
                                     
                                                                             
                                            Marek Ondra
                                     
                                                                             
                                            Viktor Štrba
                                     
                                                                             
                                            Nataša Sallaiová
                                     
                                                                             
                                            Zuzana Dubcová *zuz*
                                     
                                                                             
                                            Ladislav Šebák
                                     
                                                                             
                                            Miroslav Babič
                                     
                                                                             
                                            Martina Verešová
                                     
                                                                             
                                            Adriána Fehérová
                                     
                                                                             
                                            Juraj Drobný
                                     
                                                                             
                                            Tamara Kadnárová
                                     
                                                                             
                                            Marcela Bagínová
                                     
                                                                             
                                            Júlia Hubeňáková - Bianka
                                     
                                                                             
                                            Marian Baran
                                     
                                                                             
                                            a mnohí ďalší
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




