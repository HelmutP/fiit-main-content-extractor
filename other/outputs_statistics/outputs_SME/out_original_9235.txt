
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Bogric
                                        &gt;
                História
                     
                 17. rok vojny - 14. časť - Snahy o získanie Kamariny. 

        
            
                                    20.6.2010
            o
            9:00
                        (upravené
                20.6.2010
                o
                8:45)
                        |
            Karma článku:
                2.09
            |
            Prečítané 
            466-krát
                    
         
     
         
             

                 
                      V tejto časti vám odporúčam sa vrátiť o pekných pár rôčkov vojny dozadu a pripomenúť si ako aj snahy Lachesa, ale aj 1. reč Hermokrata, ktorý apeloval na spojenectvo sicílskych miest, pre tú istú príčinu, kvôli ktorej musí teraz brániť svoje mesto. Myslel som si, že výborní politici a súčasne vojvodcovia boli iba v Aténach. Som rád, že, čo sa toho môjho myslenia týka, som sa v osobe Hermokrata pomýlil. Túto časť rozdelím minimálne do dvoch častí, lebo v druhej časti budem chcieť uviesť 1. Periklovu reč, ktorú predniesol v sneme, keď Lacedemončania posielali posolstvo za posolstvom, aby sa Aténčania neangažovali vo veci Korint- Kerkyra ohľadom mesta Megary a Poteidai.  
                 

                     Tieto reči od Hermokrata a Eufemea, ktoré teraz opíšem, nemusia byť fiktívne, lebo, ako som spomenul, Tukydides sa tam nablízku mohol nachádzať. Usudzujem to z toho, že čas od jeho vyhostenia do tejto výpravy bol takmer 10 rokov a to už mohol mať určenú ucelenú koncepciu a skladbu svojej knihy. Možno sa mýlim, ale vychádzam z toho, že prvotné konflikty, kde nebol priamy účastník a konflikty bezprostredne po vyhostení neopisuje tak precízne, ako je táto výprava a udalosti na nej, ako zo strany Aténčanov, tak aj zo strany Syrakúzanov. Všimol som si, že niektoré bitky a udalosti opísal pomerne povrchne (hlavne v spomínanom začiatku peloponézskej vojny), ale potom opis nadobúda konkrétnejšiu podobu a precíznosť. Už bitka pri Mantinei (či skôr Tegei) bola opísaná veľmi spoľahlivo, aj s chybami, ktoré urobili spartskí lochagovia. Ak by tam nebol, tak nemohol tú bitku opísať tak, ako ju opísal. Lebo neverím tomu, žeby mu Sparťania (alebo priamo kráľ Agis) čokoľvek o priebehu bitky povedali a Aténčania, či Argejčania by ho boli zabili. Takisto výpravu na Sicíliu a jej priebeh musel priamo sledovať, alebo byť niekde nablízku, lebo nám podáva také svedectvá, ktoré by z druhej ruky, aj keď niekoľkokrát preverené, nemuseli byť také precízne. Takže všetky tri reči (dve v Kamarine plus prvú Periklovu), ktoré opíšem, môžu byť, ako som spomenul, autentické. Samozrejme musíme brať do úvahy, že vtedy neboli žiadne nahrávacie zariadenia a mohol sa spoľahnúť len na svoju pamäť, ktorú ale musel mať perfektnú, keď v rečnení dokázal poraziť aj Perikla.   "Tej zimy začali Syrakúzania budovať okolo mesta hradby a uzavreli nimi Temenites po celej dĺžke obrátenej smerom k Epipolám, aby v prípade porážky ich vojska nemohli Aténčania urobiť tak ľahko násyp v bezprostrednej blízkosti mesta. Potom opevnili Megary a vybudovali aj hradbu v Olympeiu. Kolovú hradbu urobili všade pri mori, kde bolo možné pristáť. Syrakúzania vedeli, že Aténčania zimujú v Naxe a tak podnikli veľkú výpravu proti Katane, spustošili jej územie a vrátili sa domov. Keď sa dozvedeli, že Aténčania na základe spojeneckej zmluvy poslali poslov do Kamariny (tú uzavreli s Kamarinčanmi, keď bol veliteľom Laches), aby si jej obyvateľov získali na svoju stranu, poslali ta aj oni svojich poslov, lebo sa domnievali, že aj pomoc, ktorú im Kamarinčania poslali, nebola prejavom ochoty z ich strany, na čo mal vplyv aj úspech Aténčanov v bitke a potom sa báli, že sa proti ním spoja s Aténčanmi. Do Kamariny za Syrakúzy išiel Hermokrates a za Atény Eufemes so sprievodom. Keď sa obyvatelia Kamariny zišli do zhromaždenia, Hermokrates s úmyslom vytvoriť nepriateľskú náladu voči Aténčanom povedal toto:-   " Kamarinčania! Neprišli sme sem preto, lebo sme mali obavy, že vás prekvapí prítomnosť aténskeho loďstva. Väčšmi sa obávame toho, že sa im podarí presvedčiť vás skôr, než sa nám podarí vás vypočuť. Ako viete, Aténčania prišli na Sicíliu pod zámienkou, ale ich skutočný cieľ sme všetci vybadali. Myslím, že neprišli preto, aby pomohli Leontinčanom vrátiť sa domov, ale aby nás vyhnali z našich miest. Ktože verí, že ľudia, čo ničili mestá v Grécku, chcú stavať mestá tu, alebo že zotročovatelia a podmanitelia Chalkiďanov a Eubojčanov môžu mať nejaké city k Leontinčanom na základe ich príbuznosti s Chalkiďanmi? Veď ako si Aténčania dobýjali moc vo vlastnej krajine, takým spôsobom sa pokúšajú nastoliť ju tu, na Sicílii. Pod zámienkou, že potrestajú Peržanov, stali sa veliteľmi Iónov a ich spojencov, potom však zotročili iných, lebo vraj im neposkytli podľa dohody vojakov a ďalších, lebo mali medzi sebou spory, čiže pre každý prípad si našli vždy nejaké presvedčivé obvinenie. Preto Aténčania nebojovali proti perzskému kráľovi za slobodu Grékov a ani Gréci za svoju slobodu. Aténčania bojovali skôr za to, aby urobili z kráľových poplatníkov svojich poplatníkov a platcov daní, alebo aby priamo Gréci otročili im a ostatní Gréci bojovali za to, aby si vymenili pána za iného a ešte horšieho a chytráckejšieho.   Ale neprišli sme sem preto, aby sme dokazovali, že aténsky štát má na rováši mnohé prečiny, ktoré dobre poznáte. Skôr sme prišli kvôli tomu, aby sme obviňovali samých seba, lebo máme pred očami varovné príklady tamojších Grékov, ako boli zotročení, keď sa nedokázali brániť. Teraz opäť počúvame lživé reči o návrate našich kmeňových príbuzných a o pomoci spojencom z Egesty, nuž prečo sa nespojíme, aby sme im ukázali, že tu nebývajú Ióni, ani Helesponťania, či ostrovania, ktorí vždy iba menia pánov, raz Peržana, inokedy voľakoho iného, ale slobodní Dórovia, pochádzajúci zo samotného Peloponézu a žijúci na Sicílii. Alebo budeme čakať, že ovládnu jedno mesto po druhom, hoci vieme, že len takto si nás môžu podrobiť? Vidíme, že Aténčania vedú takú politiku, keď rozdvojujú nás niektorých z nás svojimi rečami, iných podnecujú do vojny proti svojim susedom a sľubujú im spojenectvo, ďalším škodia, ak môžu, obracajúc sa na jednotlivé mestá s lákavými sľubmi. Alebo si myslíte, že ak zahynie náš ďaleký krajan, nebezpečenstvo nás nezasiahne a nešťastie sa dotkne len toho, koho zasiahlo prvého?   Vari sa niekto z vás, Kamarinčanov, nazdáva, že nepriateľom Aténčanov sú Syrakúzania a nie on sám a myslí si, že je strašné vystavovať sa nebezpečenstvu za vlastné mesto? Nech si teda nepredstavuje, že bude bojovať skôr za moju vlasť, ale bude bojovať aj v mojej vlasti rovnako i za svoju vlasť a o to bezpečnejšie, keď si uvedomí, že som nezahynul skôr ako on, lebo takto bude mať vo mne spojenca. Nech si uvedomí, že Aténčania sem neprišli potrestať nepriateľstvo Syrakúzanov, ale využívajú nepriateľstvo Syrakúzanov ako zámienku, lebo si chcú zabezpečiť priateľstvo s inými sicílskymi Grékmi. Ale ak niekto zo závisti, alebo zo strachu pred nami - lebo veľmi silné mestá ako Syrakúzy musia rátať aj s jedným a aj s druhým- chce oslabiť Syrakúzy (nám na príučku) a kvôli vlastnej bezpečnosti, aby to všetci prežili, živí nádeje, ktoré presahujú medze ľudských možností, nech sa na to nespolieha. Tak, ako sa nemôže spoliehať na to, že ak nás Aténčania pokoria, tak prestanú s vojnou na Sicílii. Preto treba, Kamarinčania, aby ste predovšetkým vy, susedia, ktorí ste ohrození hneď po nás, toto predvídali a aby ste si svoje vojnové povinnosti neplnili tak neochotne, ako doteraz. Mali by ste sa hneď dobrovoľne postaviť na našu stranu. Namiesto toho, aby sme prišli so žiadosťou o pomoc k vám, vy ste sa mali obrátiť so žiadosťou o pomoc na nás. A keby prišli Aténčania skôr do Kamariny, mali ste nás nabádať k odvahe, aby sme sa statočne držali. No žiaľ, doteraz ste ani vy, ani iní Sicílčania  neprejavili takú ochotu.   Iste z nedostatku odvahy sa chcete pred nami i pred útočníkmi spravodlivými pod zámienkou, že ste spojencami Aténčanov. Ale toto spojenectvo s nimi ste neuzavreli proti svojim priateľom, uzavreli ste ho proti nepriateľom, ak by vás niekto napadol a aby ste pomáhali Aténčanom, ak by im iní spôsobili škody a nie ako teraz, keď sami spôsobujú škody iným. Veď ani Regijčania (hoci sú ich príbuzní) nie sú ochotní Aténčanom pomáhať pri návrate svojich krajanov Leontinčanov do ich mesta. Bolo by hrozné, ak by Regijčania zapochybovali o cieli tohto premysleného činu Aténčanov, aby sa ukázali ako rozumní bez dôvodu, kým vy by ste sa chceli skrývať za rečnícky odôvodnenú zámienku a chceli pomáhať svojim ozajstným nepriateľom a spolu s nimi ničiť svojich najbližších príbuzných ako svojich nepriateľov. Toto pravdaže nie je spravodlivé. Naopak, vašou úlohou je pomôcť nám a nebáť sa moci Aténčanov. Veď ak sa my všetci Sicílčania zjednotíme, nemusíme sa báť sily nepriateľa. Chyba je v tom, že sme rozdrobení. Aj keď nad nami vyhrali, no po víťazstve boli nútení ihneď ustúpiť.   Preto nesmieme strácať odvahu, ak budeme spojení, ale musíme si plniť svoje spojenecké povinnosti o to ochotnejšie, keď nám idú na pomoc Peloponézania, ktorí sú vojensky pomerne silnejší ako Aténčania. Nech si nikto z vás nemyslí, že vaša obozretná politika, nepomáhať ani jednej ani druhej strane, lebo ste spojencami oboch, je spravodlivá voči vám. No je bezohľadná voči druhým. Táto politika nie je vonkoncom správna ani v teórii a ani v praxi, lebo ak nám odmietnete vojenskú pomoc, budeme zničení a Aténčania vyhrajú. Čo iné dokážete svojou neutrálnou politikou, ak nie to, že jedni sa bez vašej pomoci nezachránia a druhým umožníte konať bezprávie. Prosíme vás a zároveň, ak sa nám nepodarí vás presvedčiť, voláme vás za svedkov, že nás ohrozujú naši dávni nepriatelia- Iónovia, ale zároveň nás zrádzajú aj Dórovia, hoci aj my sme Dórovia. Ak si nás oni podrobia, zvíťazia vďaka vašej nerozhodnosti, ale sláva pripadne im. No ak sa stane naopak to, že zvíťazíme my, vy budete potrestaní za to, že ste zapríčinili tie nebezpečenstvá, z ktorých sme vyviazli. Zamyslíte sa teda a zvoľte si teraz, alebo zotročenie, alebo nádej, že zvíťazíte s nami a uniknete pred hanebným ujarmením Aténčanmi a našim nepriateľstvom, ktoré nie je bezvýznamné, ako som už spomínal."   Toto povedal Hermokrates."   Pred rečou Eufemena vám ešte pripomeniem Perikla, štátnika, ktorý dal Aténam smerovanie také, aké mali. Perikles bol okrem štátnika aj celkom schopný vojvodca a spolu s Tukydidom, Formionom a Hagnonom tvorili zbor stratégov, ktorým vtedy nik nemohol odolať. Vo vzájomnej súhre si niekoľko rokov pred začatím tejto vojny ( cca v r. 440 p.n.l.) podrobili ostrov Samos a tak si zaviazali Miléťanov, ktorým pomohli. No preceňovanie svojej sily a podceňovanie protivníka je vlastné ako Periklovi, tak aj jeho nástupcom. Možnože jediný Nikias bol trochu súdny a pochopil nebezpečenstvo spojenia všetkých Dórov proti Aténčanom, ale ako to nepochopil Perikles, tak to nepochopil ani Eufemes.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            23. rok vojny- 1. časť- Alkibiadove vojenské výpravy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Xenofón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            22. rok vojny- Úspechy a porážky aténskeho vojska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            21. rok vojny- kapitola č. 2- Politika Farnabaza, osud Syrakúzanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Kapitola č. 1 – Pokračovanie Tukydida
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Bogric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Bogric
            
         
        bogric.blog.sme.sk (rss)
         
                                     
     
        Kto nepozná minulosť, nepochopí súčastnosť a nebude vedieť, čo ho môže čakať v budúcnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    138
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    722
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vyhoďme do vzduchu parlament!
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Starosta je vinný a čo tí druhí
                     
                                                         
                       Nehoráznosť a zvrátenosť
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Naši zákonodarcovia
                     
                                                         
                       Karel Kryl – nekompromisný demokrat aj po revolúcii
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Závisť aj po dvoch rokoch
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




