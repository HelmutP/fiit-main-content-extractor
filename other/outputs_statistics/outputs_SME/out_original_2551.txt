
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Zápisky medveďa
                     
                 „Buch...buch...nič, buch...buch...nič, buch...buch...nič... 

        
            
                                    9.3.2010
            o
            22:00
                        (upravené
                22.3.2010
                o
                9:35)
                        |
            Karma článku:
                15.79
            |
            Prečítané 
            4435-krát
                    
         
     
         
             

                 
                    Takto. Takto mi bije. Buch...buch...a nič. Nepravidelne. Mám nepravidelný pulz!"   Diagnóza vyrieknutá. Scéna dôverne známa, scéna pre niekoho humorná, pre zainteresovaných skôr  tragikomická... Otec sedí v kresle a meria si tep. Niekedy celé hodiny. Skleslý pohľad zabodnutý do televízora, pohľad svedčiaci o úplnom ignorovaní socialistického vysielania jednej z dvoch existujúcich televíznych staníc. Pohľad, v ktorom čítať utrpenie, ale i rezignáciu a zmierenie sa so skutočnosťou, že lepšie už nebude.
                 

                    „Veď ho máš pravidelný...", chce sa mi podotknúť s pubertálnou uštipačnostou. Som však ticho. Škoda dráždiť osie hniezdo. „Radšej čuš!", hovorím si.   Situácie podobné jedna druhej a skoro periodicky sa opakujúce už od čias, keď sa otcovi ešte ani neusadil štvrtý krížik na chrbte. Dobrých dvadsať rokov i viac. Kto by nerezignoval. Rezignovali sme všetci. On aj my. On na naše pochopenie svojich problémov a trápení  a my na jeho sťažnosti.   V mysli mi utkvela spomienka. Prichádzam zo školy. Otec ma víta vo dverách.   „Vieš čo sa stalo?", dramatická pauza... Tušil som, že sa to bude týkať 'jeho'.   „Čo?", opýtal som  sa s jasným očakávaním 'hrozivej' pravdy.   „Prekonal som infarkt!"   „A kedy?", reagujem popravde neveľmi vzrušene.   „Bol to skrytý infarkt. Prišiel som od doktora. Zistil nejaké extrasystoly. Hovoril, že by to mohol byť aj následok prekonania slabého infarktu."   „Márnosť, to ti ten doktor mal čo povedať.", mrmlem si pre seba. Pochopil som, že náhle zhoršenie zdravotného stavu predbežne nehrozí.   A od tohto času sa k radu otcových zdravotných problémov (žalúdok, ľadviny, kĺby...) pridalo aj srdce. Srdce o ktorého existencii dovtedy ani poriadne nevedel, ho rozbolelo s nevídanou nástojčivosťou. Vždy keď sa rozčúlil, čo bolo skoro denne, ostatne aj v tom rozčuľovaní som po ňom, svoje rozhorčenie dal najavo rukou pritisnutou k ľavej strane hrudníka v teatrálnom geste a s poznámkou: „Až ma srdce rozbolelo..."   Z otcovho života boli postupne vylúčené účasti na pohreboch aj najbližšej rodiny. „Ja si ho (ju) chcem zapamätať takého, aký bol." To boli jeho zdôvodňujúce tvrdenia, ktorým však nebolo možné veriť.   Otcov strach z rakoviny bol trvalou súčasťou jeho života, zalepená obálka s poznámkou, „Otvoriť, len keď sa so mnou niečo stane" a večné narážky typu „Veď sa ma už konečne zbavíte." vypĺňali prázdno v otcovom živote.   Lieky. Samé lieky. Precízne zoradené, vždy veľmi pedantne pripravené. Užívané pravidelne a vždy nachystané vopred. A čím bol otcov vek vyšší a pribúdalo aj prirodzených zdravotných problémov spojených s vekom, priamo úmerne rástlo ich množstvo. Len otcov výraz sa nemenil. Stále rovnaký trpiteľský...   Nikdy sme sa nedozvedeli, čo ho tak vnútorne trápilo, nikdy sme nezistili pred čím sa tak veľmi utiekal do bolestí a trápení rôznych častí svojho zmučeného tela. Akosi nemal kto zisťovať, kde je príčina jeho chorobných strachov. A hlavne nemali sme ani žiadne povedomie o tomto probléme. Otec by dobrovoľne svoju hypochondriu nepriznal.   Koľkokrát som si povedal: „Takto nesmieš dopadnúť. Takto nie. Ty taký byť nesmieš!" Za chvíľu budem mať 42. Dívam sa na seba a vidím jeho. Či chcem, či nechcem, nezapriem ho. Hold, čo si nedokázal ty, otec, musím ja.   Ďalšia spomienka o mnoho rokov neskôr od prvej...   „Mate ľadvinu so zvyškovou funkčnosťou. To nie je dobre. Treba ju dať von. V tele nemá čo hľadať orgán, bez ktorého sa zaobídete a ktorý nie je funkčný."   Takto ho niekdy po 55. roku života prehováral lekár. Ale kdeže. Strach z operačného zákroku bol prisilný. Bol by to prvý vážnejší chirurgický zákrok v jeho živote. O zhruba tri roky neskôr ho podstúpiť musel. Bolo však  neskoro. Karcinóm sa už rozšíril...   Neviem či ešte niekedy vo svojom živote uvidím človeka s takým hlbokým paralyzujúcim strachom, aký prežíval môj otec.   „Ja ešte nechcem zomrieť. Ja si chcem ešte požiť." Opakoval stále dokola. To opakovanie sa už stávalo mechanickým, akousi mantrou, ktorou sa snažil prehlušiť všetko. Nevidel, nepočul, nevnímal nič. A ja už som ani nič nečakal. A možno práve vtedy, keď už sme nič nečakali, otec sa rozhodol dokázať nám, že tam niekde v jeho hlbokom vnútri sa nachádza tá vzácna a vyrovnaná krásna bytosť, ktorú sme v ňom neodtušili ani v najtajnejšom sne.   Naraz sa upokojil. Už len skonštatoval, že celý život sa bál rakoviny, tak je prirodzené, že ju just dostal. Od istej chvíle z neho začal vyžarovať nesmierny pokoj a pohoda. Nadhľad, ktorý by som nikdy neočakával. Vnútorná vyrovnanosť, ktorá je vzácna, úzkoprofilový  produkt aj pre oveľa silnejšie osobnosti. Nadhľad človeka, ktorý sa nemá čoho báť, lebo vie, že nemá čo stratiť.   Niekde to tam bolo, otec. Niekde to tam v tebe driemalo. A škoda tých mnohých rokov, ktoré si strávil v sledovaní svojich neexistujúcich, či zanedbateľných bolestí, škoda všetkých tých hodín a dní, strávených v zbytočnom strachu...       Milí moji, máte doma príbuzného, ktorý svoju lásku k vám a pozornosť k okoliu zamieňa za sledovanie symptómov svojich nejestvujúcich či menej podstatných až malicherných chorôb? Pamätajte na to, že v prvom rade nesmierne trpí. Že v jeho utrápenom vnútri už možno nie je miesto k plnohodnotnému vnímaniu vašich starostí, ba ani radostí, prípadne len tých, ktoré súvisia s vašim zdravotným stavom. Želám vám veľa trpezlivosti k naslúchaniu, odvahy k nájdeniu vnútornej schopnosti motivovať a inšpirovať ho k zmene. Želám veľa sily k vyberaniu kamienka po kamienku z múru, ktorý váš milovaný okolo seba postavil. Popravde neviem, kedy príde vaša odmena a ani či bude dostatočná. Viem však, že aspoň znížite možnosť rizika, že budete musieť primnoho času tráviť úvahami o tom, čo ste urobiť mohli a neurobili...       tenjeho         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (87)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




