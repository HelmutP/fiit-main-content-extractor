

 Dážď bubnuje ako beatlesácky Ringo Starr. 
 Sedím a spomínam  na rok tisíc deväťsto osemdesiat, 
 keď zabili Johna Lennona. 
   
 Napnutý ako struna na gitare som prijal túto správu 
 a pýtal sa Boha na jeho spravodlivosť. 
   
 On iba chytil do ruky gitaru a spieval do noci 
 nesmrteľné Yesterday. 
   

