

 Koľkokrát le tak z vytrvačnosti počúvame slová, vety bez toho aby sme vnímali čo sa nám niekto snaží riecť. Všetky tie možné a nemožné informácie sa valia do nášho mozgového priestoru z televízora, rádia, priateľov a JPP. 
 Je to ťažké, to vám vravím. Ja to málokedy dokážem. Občas fakt len počúvam bez toho aby som aj vnímala čo sa za slovami skrýva. Raz pri náhlom osvietení som však prišla na to, že počúvanie a nevnímanie nás niekedy chráni pred informáciami  bez ktorých sa dá kľudne žiť. 
 Naučila som sa kedy počúvať aj vnímať: syn, manžel, suseda, priateľka, spolusediaca v autobuse majú obrovský problém, ktorý treba vyrozprávať. 
 Naučila som sa kedy stačí len počúvať a menej vnímať: syn, manžel, suseda, priateľka, spolusediaca v autobuse majú obrovský problém, ktorý treba vyrozprávať. 
 Kedy stačí len mlčky vypočuť, kedy sa očakáva súhlas, súcit či nesúhlas. 
 Dnes sa mi podarilo fajne aj počúvať aj vnímať, ale jednalo sa o dvoch informátorov. Stojím si v bránke a čumím do neznáma, občas sa mi to stáva. Pristavila sa pri mne susedka s trojmesačným synom. Vyškieral sa na mňa z kočíka a batoliačtinou mi vysvetľoval že sa dokáže s mojou pomocou posadiť. Chytal mi prst a ťahal sa hore. Suseda mi medzitým vysvetľovala že to a že ono a že tamto. Ja a malý vzpierač sa vyškierame na seba opreteky. Dôležito mu vysvetľujem že ťuťuli a muťuli. Chichoce sa. Mamka zmlkne, tak na ňu zaškúlim a vravím: ja ťa počúvam, len rozprávaj. Pokračuje. Aj ja. Vynikajúca konverzácia, mamka sa vyrozprávala a my s malým sme si odovzdali dôležité informácie. 

