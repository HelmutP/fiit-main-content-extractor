

 
Je hlúpe a tupé neustále neustále dookola opakovať politickú mantru o globálnych klimatických zmenách a zároveň trojnásobne zvyšovať ťažbu v lesoch severovýchodného Slovenska, kde je les jediným účinným faktorom, ktorý dokáže na flyšových pohoriach účinne zachytiť mimoriadne zrážky a tak výrazne zmierniť účinky povodňovej vlny dole v údolí. 
 
 
Čergov je typické pohorie, tvorené flyšom, ktorý sám o sebe vodu zadržiava málo a jeho nádherné jedľovobukové lesy v minulosti magdalénske dažde nasali do svojej pôdy a bránili tak vylievaniu horských riečok, ktoré v ňom pramenia.  
 
 
 
 
 
Dá sa samozrejme ukázať, že dažde minulosti a dažde súčasnosti sú dažde podobné alebo rovnaké a že jediné čo sa mení je intenzita ľudských zásahov do krajiny. Je ale veľmi nepohodolné a pre isté skupiny obyvateľov aj finančne stratové, meniť spôsob a intenzitu lesného hospodárenia tak, aby les robil to, čo vo flyšových pohoriach robiť má. Zadržiaval extrémne zrážky a bránil tak ľudský majetok a životy. 
 
 
Preto vznikajú rôzne teórie o povodniach spôsobených nepokosenou trávou na lúkach, neupravených vodných tokoch, či riekach upchatých neporiadkom splaveným z podhorských obcí. 
 
 
Mojim záujmovým územím, kde sa dá veľmi pekne ukázať vzťah medzi povodňovými prietokmi a lesným hospodárením je povodie riečky Ľutinka, ktorá sa vlieva nad Sabinovom do Torysy.  Včera v podvečer som pozeral na sabinovský most a žiaden vzpriečený neporiadok som tam nevidel. Videl som len veľkú, veľmi veľkú vodu, zafarbenú do hneda hlinou z čergovských vyrúbaných svahov. 
 
 
 
 
 
Ľutinka pred vtokom do Torysy bola široká a veľká, ako horská rieka, ktorej nič nebráni v toku. 
 
 
 
 
 
Ani pod mostom v Olejníkov som nevidel žiadne vzpriečené brvná, len veľa, veľmi veľa vody, holorubnej vody. 
 
 
  
 
 
Cesta nad Olejníkovom, v miestach kde Ľutinka ešte len začína svoj tok, je v mojich článkoch o povodniach pravidelne zobrazovaná ako ukážka zbytočnej sizyfovskej práce cestárov, ktorí nemajú šancu dovtedy, dokiaľ sa situácia nebude riešiť komplexne a hore, v čergovských lesoch a medzi politickou elitou. 
 
 
 
 
 
Ono to možno aj niekomu vyhovuje, povodne, keď sa pojmu kreatívne, sú perpetum mobile na peniaze. V máji upravíme vodný tok: 
 
 
 
 
 
   
 
 
v júli nám všetko odnesie voda: 
 
 
 
 
 
A na druhý rok v máji môžeme zase investovať verejné financie.  
 
 
Keby neboli mŕtvi, tak by to možno bolo aj smiešne. 
 
 
 
 
 
Post scriptum:
 
 
STV pred mesiacom:
 
 
 
 Hrozba povodní 
 
streda 18.6. 2008
 
 
 
 
 
 
Východnému Slovensku hrozia v júli veľké záplavy. Za touto prognózou stojí náčelník Lesoochranárskeho zoskupenia VLK, ktorý už predpovedal povodne v roku 1998. Vtedy vzala voda život 42 ľuďom. Tentoraz by sa mala jej ničivá sila prejaviť hlavne v pohorí Čergov či v Levočských a Bukovských vrchoch. O príčinách i riešení záplav budeme  rozprávať s našimi ďalšími hosťami. Desiatky zničených domov, miliónové škody a strach miestnych o svoje životy. Takúto podobu mala voda pod Čergovom už tri roky po sebe.  
 
 
Eva Bartošová,starostka obce Olejnikov, má na povodne takýto názor: 
 
 
Je to sila, je to živel a hotovo, to sa nedá nič proti tomu robiť..... 
 
 
Podľa lesoochranára Juraja Lukáča sa takýto scenár môže o mesiac opakovať. " Na severovýchodnom Slovensku sú zásadne flyšové pohoria, ktoré veľmi zle zadržiavajú vodu ....".  Príčinou ničivých záplav je zvyšujúca sa ťažba v lesoch, pokračuje Lukáč: "Konkrétne v oblasti Čergova nad obcami Olejnikov a Ľutina sa zvyšuje ťažba takmer trojnásobne ...  Lesy SR nadmernú ťažbu popierajú.  Anita Fáková – hovorkyňa Lesov SR tvrdí toto: " V oblasti čergovských vrchov sa v tomto roku dokonca ani neťaží toľko dreva, koľko sa pôvodne plánovalo...  "
 
 
Klimatológovia blížiace predpovede ochranárov nepotvrdili. Možnosť povodní však vo všeobecnosti nevylučujú. Hovorí Ján Smékal - klimatológ 
 
 
" Vplyvom klimatických zmien stúpa počet extrémnych javov, to znamená že pribúda počet búrok...."  Starostovia navrhujú ako ochranu pred záplavami vybudovanie suchých nádrží.  Tvrdí to aj 
 
 
Vladimír Lelovský – starosta obce Ľutina.  " Tam by sa to malo uchytiť tá voda." Protipovodňové opatrenia v podobe suchých nádrží budú pod Čergovským pohorím hotové pravdepodobne až o 2 roky. Dovtedy si musia tamojší obyvatelia poradiť  s veľkou vodou sami. 
 
 
 
 
 
 
 
 

