
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Mikloško
                                        &gt;
                Nezaradené
                     
                 Spomienka na najväčšiu drámu v dejinách ľudstva 

        
            
                                    11.4.2009
            o
            11:28
                        |
            Karma článku:
                8.65
            |
            Prečítané 
            1946-krát
                    
         
     
         
             

                 
                    Jarná príroda v nás prebúdza staré spomienky a nové túžby. Zlatým dažďom opekneli lesy plné nádeje, zrazu sa objavili košaté nevesty v bielom a ružovom, slávičí spev zaznieva aj v noci, potôčik ticho a neúnavne žblnkoce, smutné vŕby pletú žlté korbáčiky a mesiac, rastúci  po jarnej rovnodennosti nám prináša dar hlavného sviatku kresťanov – Veľkú Noc, spomienku na najväčšiu drámu ľudstva, ktorá sa kedy stala.
                 

                 
  
  Vo štvrtok úzkosť a krvavá modlitba, zrada za tridsať strieborných, trojnásobné zapretie budúceho nasledovníka, zbabelosť priateľov, ľahostajnosť davu, falošné svedectvá,  surové vyšetrovanie a bičovanie, tŕňová koruna a výsmech, násilné ľudové referendum, zmanipulovaný súd a nespravodlivý rozsudok. Na konci piatkovej cesty je Kalvária, pomoc Afričana Šimona, nebojácny súcit žien, barbarské ukrižovanie, bolesť, beznádej, osamelosť, plačúca matka a krutá smrť. Očami ľudí sa všetko skončilo veľkým fiaskom. Dezorientovaní priatelia rezignovali pred triumfom zla. Temnota noci nakrátko zvíťazila. Smrť však bola iba prelúdiom vzkriesenia, rozohnali ju zore veľkonočného rána. Mária tú sobotnú noc isto nespala, čakala a dočkala sa. Ranné lúče slnka pozlátili prázdny hrob, v ktorom zaznelo: „Nebojte sa! Vstal z mŕtvych, nie je tu!“ Prázdny hrob je hlavným argumentom našej viery, je prísľubom aj nášho zmŕtvychvstania, ním sa začala nová éra ľudstva. Vzkriesený Kristus už nespomína tých, čo ho zradili a ukrižovali, díva sa len dopredu a hore, kde má o Otca veľa príbytkov pre verných. Len cez Krista možno pochopiť zmysel utrpenia. Jeho život, smrť a vzkriesenie je radostným posolstvom, ktoré nikoho nemôže nechať ľahostajným. Svojou smrťou nás  oslobodil od hriechov a zmŕtvychvstaním nám otvoril vstup do nového života. Ukázal nám, že smrťou sa život nekončí, iba sa mení. Aj nás pozýva do Galilei, aby sme ho tam videli, povzbudzuje nás k prekonaniu strachu a starostí a vyzýva nás vziať svoj kríž, zaprieť sa a nasledovať ho. Lebo len on je cesta, pravda a život. Kríž je symbolom kresťanstva. Nevyhnutne patrí životu, ako povedala sv. Rozália z Limy v 16. storočí: „Neexistuje žiadny iný rebrík k výstupu do neba, iba kríž.“ Ideme životom, trápime sa, bojujeme, padáme, vstávame a zasa padáme a na konci nás čaká kríž  smrti, ale ak chceme aj veľkonočné ráno. K nemu sa však dá prísť len cez obetu, sebazaprenie a násilie na sebe. Len semeno, ktoré zahynie dáva stonásobnú úrodu, len cez utrpenie sa dá prísť k hviezdam. Využime sviatočné chvíle, zamyslime sa sami, aj v kruhu rodiny nad svojim životom a snažme sa v ňom niečo zmeniť k lepšiemu. Veľká Noc nám zasa dáva túto šancu, nikto z nás nevie, koľko ich ešte dostane. Prajem Vám  spokojné prežitie veľkonočných  sviatkov. Nech sa vám podarí precítiť opravdivú veľkonočnú radosť, ktorú nám, napriek všetkému, nik nemôže vziať.  Pocítil som ju aj včera pri prenose Krížovej cesty s Benediktom XVI., s rozkvitnutými rímskymi kvetmi a stromami pri splne mesiaca nad Koloseom za prítomnosti desaťtisícou veriacich z celého sveta. Tentoraz sa konala na pozadí smutného pohrebu obetí zemetrasenia v L´Aquile. Autorom meditácií na tému zla vo svete bol indický arcibiskup T. Menamparampilo. Zastavenia krížovej cesty sa líšili od tradičných, úvahy boli aktuálne, hovorili o dnešných problémoch ľudstva, siahali na hlbinu. Pápež v závere zdôraznil, že Kristus zmenil svet nezabíjajúc iných, tým, že sám sa nechal zabiť. Cez tisícročia očaril zástupy ľudí, ktorí ho verne nasledovali. Aj keď sa ľudia po smrti Krista ponáhľali domov, aby nezmeškali veľkonočnú večeru, v surovom pohanskom rímskom stotníkovi, ktorý asistoval pri ukrižovaní, sa prekvapujúco zrodila viera, keď povedal: „On bol naozaj Boží Syn!“ A na úsvite červené zore pozlátili prázdny hrob...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (84)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Mladí, príďte do Prahy, na stretnutie Taizé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Ďalší škandál vo verejnom obstarávaní
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Spomienka na tých, čo odišli do večných lovísk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Interpelácia na premiéra Fica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            11. Kalvárske olympijské hry - fotoreportáž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Mikloško
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Mikloško
            
         
        jozefmiklosko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Mám dve na tretiu krát tri na druhú plus dva rokov (bŕŕŕ), 49-ty rok ženatý, štyri deti, jedenásť vnukov. Do r.1989 som bol vedec - matematik, potom politik, poslanec, prorektor, publicista, veľvyslanec v Taliansku (2000-2005), spisovateľ. Od r.2005 som dôchodca, vydavateľ najmä vlastných kníh (7), hudobno-dramatického CD-čka Ona, Ono a On, predseda Združenia kresťanských seniorov Slovenska. Pravidelne športujem, počúvam klasiku a rád sa smejem. Zabudol som, že od 11.3.2012 som sa stal poslancom NR SR, takže som zasa politik. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    216
                
                
                    Celková karma
                    
                                                7.77
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ďalší škandál vo verejnom obstarávaní
                     
                                                         
                       Tento november si už Paška zapamätá
                     
                                                         
                       Spomienka na tých, čo odišli do večných lovísk
                     
                                                         
                       Interpelácia na premiéra Fica
                     
                                                         
                       Obchod s ľudskými orgánmi - zbraň najťažšieho kalibru v informačnej vojne o Ukrajinu
                     
                                                         
                       Písať o tom, čo je tabu
                     
                                                         
                       Rozvod a cirkev
                     
                                                         
                       11. Kalvárske olympijské hry - fotoreportáž
                     
                                                         
                       Holokaust kresťanov v Iraku a Sýrii sa nás netýka?
                     
                                                         
                       Som katolíčka. Nejdem príkladom.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




