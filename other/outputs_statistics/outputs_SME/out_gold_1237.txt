

 Každý vie, ako sa vrátili do našich domov a bytov tapety. Nielen staromódne a gýčové, ale i moderné, farebné, so vzormi od zvieratok po geometrické retro. Aby toho nebolo málo, zmenil sa a prispôsobil modernej dobe aj ich povrch. Okrem hladkých tapiet dnes ľahko narazíte na tapety s 3D efektom, teda také, ktorých vzory sú vystúpené. Ďalej sú aj umývateľné, metalické... na čo si len spomeniete.  
  
 Mňa však v poslednej dobe zaujalo niečo celkom odlišné. Ide o tzv. wallpainting, teda maľovanie na steny. Wallpainting u nás robí ešte málo firiem v podobe nových, umeleckých a kreatívnych výtvorov. Hovorím predovšetkým o lese so zvieratkami v detskej izbe, o oblohe na strope spálne, o Elvisovom portréte nad posteľov tínedžera... Najúžasnejšie na tejto technike skrášľovania je to, že kvalitné firmy pracujú bez makiet a všetko sú prispôsobiteľné originály, ktoré vytvárajú v spolupráci s obyvateľom daného priestoru.  
   
   
 Ďalšou výnimočnou vecou v oblasti moderného bývania je tzv. wallwrap a ide o ešte novšiu technológiu, ktorá sa vyznačuje tepelným lepením tenkého lesklého papiera tak, že dokonale obkreslí hocijaký povrch, dokonca tú najdrsnejšiu fasádu. Táto medóda sa totiž využívala spočiatku na reklamné účely, predovšetkým do exteriérov. Ale keďže každá kampaň má obmedzené trvanie, tento papier sa dá rovnako jednoducho za pomoci tepla odstrániť bez toho, aby bola poškodená omietka, fasáda, či maľovka (!). 
  
 Výhodou wallpaintingu je, že je to predsalen stará dobrá klasická a originálna maľba, výhodou wallwrapu je predovšetkým jeho šetrnosť ku stenám a možnosť prenosu digitálnej fotografie na celú stenu. 
 Tak či onak, zdobenie príbytkov je každého osobná voľba, záležitosť vkusu a chute. Snáď sa vám môj tip bude páčiť, referenčné stránky mám uvedené medzi mojimi obľúbenými webmi. ;) 
   
 Zatiaľ ostávam s pozdravom 
 Vanda :)  

