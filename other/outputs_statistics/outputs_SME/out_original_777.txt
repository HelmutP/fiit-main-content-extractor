
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Lipták
                                        &gt;
                Zdravotné problémy
                     
                 Chrápanie 

        
            
                                    12.7.2005
            o
            10:24
                        (upravené
                19.2.2012
                o
                9:45)
                        |
            Karma článku:
                10.72
            |
            Prečítané 
            32820-krát
                    
         
     
         
             

                 
                    Dnes vieme, že to nie je iba "sranda", ale choroba. Môže mať vážne následky, ale dá sa liečiť.  25- ročná Nikol má problém , ktorý trápi jej priateľa a aj ju. Priateľ posledných pár mesiacov hrozne chrápe. Je jedno, kde spí a ako, či na bruchu, chrbte, alebo na boku. Chrápe tak silno, že sa pri ňom nevyspí . Čo sa dá robiť proti chrápaniu? Existujú lieky alebo nejaké prípravky, ktoré by mohli zabrať?
                 

                 
  
   27- ročný Martin má problémy so spánkom, sú však opačného charakteru ako u ľudí s nespavosťou. Má veľmi hlboký spánok. Ráno sa nevie zobudiť. Nastaví si viac budíkov, ale keď sa zobudí napríklad aj o 2-3 hodiny neskôr ako chcel vstať, nájde budíky vypnuté. Vie, že ich musel vypnúť, ale nepamätá si to. Niekedy nájde budíky vyzvonené, to zase znamená, že ich počas spánku odignoroval, napriek tomu, že budíky zvonia strašne nahlas. Nepredpodkladá, že to bude prílišnou únavou, lebo sa mu to stáva aj keď si ide ľahnúť zavčasu a na spánok má napríklad 10 hodín. V strede noci ho nedokáže zobudiť žiadny buchot. Je z toho nešťastný, má problémy aj v práci. Vie o sebe, že veľmi silno chrápe, ľudia, ktorí to zažili, tvrdia, že niečo také ešte nepočuli a vyjadrujú obavy z toho, aby sa nezadusil vlastným chrápaním. Inak netrpí žiadnou zjavnou chorobou ale je silný fajčiar.   Chrápanie je nepríjemný problém, môže ale znamenať aj vážne ohrozenie zdravia. Príčinou chrápania je vznik prekážky(obštrukcie) v horných dýchacích cestách (HDC) počas spánku. Miestami obštrukcie sú najčastejšie mäkké podnebie, koreň jazyka a hrtanová príchlopka.   Rozlišujeme niekoľko druhov chrápania 1. Jednoduché chrápanie - je to občasné chrápanie v polohe na chrbte, vyskytuje sa až u 50% populácie. 2. Habituálne chrápanie - chrápanie každú noc v každej polohe, takto chrápe 19% populácie. Chrápanie typu 1. a 2. nepríjemne ruší okolie, ale zdravie postihnutého neohrozuje.  Závažné zdravie ohrozujúce formy chrápania sú: 3. Syndrom zvýšeného odporu v horných dýchacích cestách (u 10% populácie). 4. Syndrom spánkového apnoe (u 3% populácie). Ľahšou formou je tzv. syndróm zvýšeného odporu v horných dýchacích cestách -tu chorí chrápu v noci kolísavou intenzitou. Typická je narastajúca hlasitosť chrápania až zrazu chrápanie prestane na niekoľko sekúnd alebo aj niekoľko desiatok minút. Potom sa stav opakuje. Tu je typické, že postihnutí neprestanú v spánku dýchať .Ráno sa budia nevyspatí a unavení. Behom dňa môžu mať v kľude mikrospánky. Ak sa súčasne vyskytujú zástavy dýchania( tzv. apnoické pauzy) pravdepodobne sa jedná o tzv. spánkové apnoe (zastavovanie dýchania v spánku).Spánkové apnoe je vážne, život ohrozujúce ochorenie, ktoré sa vyskytuje častejšie, než sa všeobecne predpokladá. Prvýkrát bolo popísané v roku 1965 ako porucha dýchania charakterizovaná krátkymi prerušeniami dýchania behom spánku. Vyskytujú sa dva typy - centrálne(porucha je v mozgu (regulačnom centre) a obštrukčné (keď je prekážka- obštrukcia v horných dýchacích cestách). Centrálne spánkové apnoe je vzácne, vzniká keď mozog vysiela chybné signály pre nádych k dýchacím svalom. Obštrukčné spánkové apnoe vzniká vtedy keď vzduch nemôže prúdiť dýchacími cestami, napriek tomu, že sa človek snaží nadýchnuť. V spánku, počet nedobrovoľných dýchacích prestávok, čiže apnoických páuz, môže byť aj 20-30 za hodinu. Hranicou chorobnosti je, počet zástav dýchania viac ako 10 x za hodinu , a keď prestávky trvajú viac ako 10 sekúnd. Zástavy dýchania ktoré sú kratšie ako 10 sekúnd, a ktoré sa opakujú menej než 10x za hodinu sa považujú ešte za normálne. Medzi jednotlivými apnoickými pauzami, postihnutý obvykle chrápe, hoci nie každý kto chrápe má tieto stavy. Spánkové apnoe môže byť charakterizované tiež prejavmi dusenia. Časté prerušovanie hlbokého osviežujúceho spánku vedie k ranným bolestiam hlavy a k zaspávaniu vo dne. Skoré rozpoznanie a liečba spánkového apnoe sú dôležité, pretože býva spojené s poruchami srdcového rytmu, vysokým krvným tlakom, infarktom srdca a mozgovou porážkou.  Kedy by sme sa mali obávať spánkového apnoe? U mnohých pacientov sú prvými pozorovateľmi ich partneri. Pozorujú u nich v spánku výrazné chrápanie a pauzy „stíchnutia". Behom spánku v ľahších prípadoch sa postihnutí opakovane budia a lapajú po dychu, sú vyľakaní a boja sa znova zaspať. V ťažších prípadoch si ani neuvedomujú opakované prebudenia. Ráno sa prebúdzajú neoddýchnutí a behom dňa sú nadmerne unavení a opakovane zaspávajú v kľude v nevhodnom čase a situáciách počas dňa ( svedkami sú spolupracovníci, priatelia...). Veľmi vážne chorí sa ráno po prebudení cítia horšie ako večer pred spánkom. Paradoxne sa cítia lepšie po kratšom spánku než po dlhšom spánku. Pacienti často nevedia, že majú tieto problémy a nevedia ich popísať. Je dôležité aby človek aj len pri podozrení včas navštívil lekára, ktorý zhodnotí jeho spánkový problém.  Kto je ohrozený spánkovým apnoe ? Spánkové apnoe sa vyskytuje vo všetkých vekových skupinách. V populácii stredného veku sa vyskytuje asi u 4% mužov a u 2% žien. Ľudia so silným chrápaním, nadváhou, so zvýšenou dennou únavnosťou, vysokým krvným tlakom, abnormálnym nálezom v nose, krku alebo v daľších častiach horných dýchacích ciest, sú ohrození častejšie.  Aká je príčina obštrukčného spánkového apnoe? Príčinou je vznik obštrukcie (prekážky), v horných cestách dýchacích. U niektorých ľudí vzniká apnoe v dôsledku ochabnutia svalov dutiny ústnej a jazyka, ktoré čiastočne alebo úplne uzatvoria dýchacie cesty. Keď svaloviny mäkkého podnebia a koreňa jazyka ochabujú , dýchacie cesty sa začínajú uzatvárať, dýchanie sa stáva namáhavé a hlučné až celkom prestane. Spánkové apnoe u ľudí s nadváhou vzniká tak, že stučnené, ťažké mäkké podnebie v polohe na chrbáte zapadne , zúži dýchacie cesty, vzduch nemôže volne prúdiť a výsledkom je chrápanie prerušované výpadkami dychu a častým nevedomým budením.  Stanovenie diagnózy Osoby s vyššie uvedenými prejavmi by mali vyhľadať svojho praktického lekára, ktorý by mal postihnutého odoslať do spánkového laboratória. Choroba sa potvrdzuje nočným monitorovaním v spánkovom laboratóriu( polysomnografiou )- stačí tu stráviť len jednu noc, počas ktorej sa u pacienta zaznamenáva pulzová frekvencia, množstvo kyslíka v krvi, dýchacie pohyby hrudníka a a brucha, prúdenie vdychovaného a vydychovaného vzduchu, pohyby pacientových končatín, poloha pacienta v spánku a tiež zvukový záznam „chrápania".  Polysomnografický záznam umožňuje úplne nebolestivé a jednoduché diagnostikovanie porúch spánku. Vždy je potrebné absolvovať ešte ORL (ušno-nosno- krčné) vyšetrenie a niekedy aj pneumologické a neurologické vyšetrenia.  Aká je liečba spánkového apnoe? Táto liečba je individuálna. Liečba liekmi je všeobecne neúčinná. Určitým pacientom môže pomôcť vdychovanie kyslíka, toto ale neodstráni spánkové apnoe a nezabráni záspávaniu cez deň. Úloha liečby kyslíkom je preto sporná, a je ťažko dopredu povedať, ktorému pacientovi prípadne môže pomôcť. Zmeny v životospráve sú dôležitou súčasťou liečebného programu a v ľahkých prípadoch môžu byť úpravy životosprávy postačujúce k vyliečeniu. Životospráva, pacienti by sa mali vyhýbať alkoholu, nefajčiť, neužívať lieky na spanie (po nich ľahšie dochádza k ochabnutiu dýchacích ciest). Ľudí s nadváhou často vylieči schudnutie, pretože každé zníženie hmotnosti o 10% výrazne znižuje počet apnoických prerušení.  Lekárska pomoc spočíva buď v chirurgickom, operačnom postupe alebo, v závažnejších prípadoch je doporučované počas spánku používať prístroj - ventilátor, ktorý vháňa vzduch pod určitým trvalým tlakom, zabezpečuje tak trvalý pretlak v dýchacích cestách. Pri liečbe trvalým pretlakom má pacient nasadenú masku na ústach aj nose po celú dobu spánku. Tento prístroj chráni dýchacie cesty pred vznikom obštrukcie. Pre ľahšie formy obštrukčného spánkového apnoe sú vypracované chirurgické postupy, ktoré sa realizujú hlavne na ORL oddeleniach. Je celá rada operačných metód na rozšírenie horných dýchacích ciest. ale žiadna z nich nie je 100% úspešná a úplne bez rizika. Niekedy sú potrebné aj viaceré operačné výkony, než sa objaví pozitívny efekt. Väčšinou sa robia bežné operácie ako je odstránenie nosových polypov, vyrovnanie vybočenej nosovej priehradky, odstránenie nosnej mandle u detí ( aj opakovane - môže dorastať), odstránenie podnebných mandlí u dospelých. Špeciálnejším výkonom je tzv. uvulopharyngopalatoplastika čo je plastika- chirurgická úprava - čípku (uvula), hltanu (pharynx) a mäkkého podnebia ( palatum mole)-pri tejto operácii sa odstránia podnebné mandle, odstráni sa podnebný čípok a skráti sa mäkké podnebie.Operácia je úspešná až asi u polovice pacientov so spánkovým apnoe a u jednoduchého chrápania až v 90%.Touto operáciou je možno úspešne zabrániť vzniku komplikácií (poruchy srdcového rytmu, vysoký krvný tlak, infarkt srdca, mozgová porážka ) a súčasne zlepšiť kvalitu života - odstrániť chrápanie, denné zaspávanie, mikrospánky, únavu...   Kam sa môžete obrátiť ak máte podozrenie na spánkové apnoe? Pacienti sa môžu obrátiť na nasledovné spánkové laboratóriá:   Bratislava: 1. Spánkové laboratórium - MUDr. Imrich MUCSKA, Klinika TBC a pľúcnych chorôb FN a LFUK Bratislava, Mickiewiczova 13, tel.:02/57290646, mobil: 0905 020 534, www.zdravyspanok.sk 2. Spánkové štúdio - MUDr. Mariana KUBOVČÁKOVÁ, Grösslingova 58, Bratislava, tel.: 02/52733589 , 0905283832 www.chrapanie.sk  cenník služieb 3.Pneumo-Alergo centrum, Uzbecká 16, tel.: 02/4020 2111, www.pneumoalergo.sk 4.Spánkové laboratórium Mediklinik   Košice: 1. Spánkové laboratórium - Prof. MUDr. Viliam DONIČ, CSc., Ústav patologickej fyziológie LF ÚPJŠ, tel.:055/06717157, 055/6423350, korzar.sme.sk/c/4485335/prve-spankove-laboratorium-na-slovensku-bolo-zriadene-v-kosiciach-a-nepretrzite-funguje-uz-12-rokov.html Prednášky prof. Doniča: a) Spánková medicína, syndrom spánkového apnoe a následky  b) Syndrom spánkového apnoe a diabetes mellitus     2. Ambulancia pre poruchy spánku - MUDr. Mária TORMÁŠIOVÁ, Neurologická klinika FNsP, Trieda SNP 1, Košice, tel.: 055/6403789,055/6403798  Martin: Spánkové laboratórium - Doc. MUDr. Robert VYŠEHRADSKÝ, PhD., MUDr.Dreveňáková, KTaPCH JLFU, Kolárova 2, Martin, tel.:043/4203111, 043/4224960, www.zdravyspanok.sk/sleep_medicine/Martin.htm   Anatómia chrápania a základné spôsoby liečby na www.hasanedali.com/dutch/hol_uyg_horapte.htm   A úplne jednoduchá rada, ktorá pomohla Nikolke: zašila svojmu priateľovi do goliera tenisovú loptičku.Takto je nútený mať hlavu položenú stále na boku, odstránil sa nepriaznivý vplyv gravitácie(zapadanie mäkkého podnebia a koreňa jazyka). Môže to pomôcť, ale v prípade najmenšieho podozrenia na závažné formy chrápania odporúčam absolvovať uvedené vyšetrenia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Lipták 
                                        
                                            Zdravie nie je len šťastie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Lipták 
                                        
                                            3. Brať zdravie? Skiaď to právo? Či ste ho vy dali?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Lipták 
                                        
                                            2. Úplatky ako „búrka v šerbli“
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Lipták 
                                        
                                            1. Prosba k finančno-politickej mafii Slovenska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Lipták 
                                        
                                            Výmenné lístky verzus kompetencie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Lipták
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Lipták
            
         
        liptak.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som všeobecný lekár pre dospelých. Obvodný lekár. Obvoďák. Všeobecný praktik. Pechota medicíny. Pešiak.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8024
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekonomika zdravotníctva
                        
                     
                                     
                        
                            Fotozamyslenia
                        
                     
                                     
                        
                            Bájky
                        
                     
                                     
                        
                            Zmeny
                        
                     
                                     
                        
                            Prevencia
                        
                     
                                     
                        
                            Zdravotné problémy
                        
                     
                                     
                        
                            Hrozby
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Všeobecní praktickí lekári
                                     
                                                                             
                                            WHO
                                     
                                                                             
                                            Ambulancia
                                     
                                                                             
                                            Môj učiteľ fotografie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sme ovce?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




