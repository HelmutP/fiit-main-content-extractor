
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Oplatí sa rozumieť si s prírodou... 

        
            
                                    18.4.2010
            o
            9:08
                        |
            Karma článku:
                6.57
            |
            Prečítané 
            1016-krát
                    
         
     
         
             

                 
                    Keď som v médiách zachytila prvú správu o výbuchu sopky na Islande, tak mi, ako mame s nadmerne vyvinutým počtom strachových buniek, prvé prišlo na rozum: "Uff, ešte že nevybuchla, keď tam bola v lete dcéra!".
                 

                 
  
   Sopky nevybuchujú - na šťastie - každý deň a tak sa priznám, že som správy o tej islandskej, počúvala a počúvam so záujmom. Táto sopka, doteraz svetu až tak nie moc známa, krkolomného mena Eyjafjallajokull, čupí pod islandským ľadovcom. Od Reykjavíku je vzdialená približne 150 km. Ozaj, práve dnes som počula v správach v súvislosti s týmto mestom, že aj meteorológovia prekvapene sledujú tamojšie dnešné slnečné počasie. Ale vrátim sa k sopke, ktorá posledne soptila začiatkom 19.storočia a vydržala spať svoj sopečný sen  až doteraz. V marci  tohto roku sa začala prebúdzať a dala na známosť časť svojej sily. V tom čase sa stala atrakciou a jej prebudenie chodilo obzerať množstvo zvedavcov. Nik však netušil, že o pár týžňov vybuchne v plnej sile.   Som rada, že nedošlo k stratám na životoch. Zachytila som, že  včas evakuovali asi 700 ľudí.  Čo som však vôbec netušila, je fakt, ako sopka toľké kilometre vzdialená, ovplyvní kus sveta, včítane Európy. Priznám sa, že som nikdy neuvažovala tým smerom, že čo všetko môže spôsobiť sopka oblastiam, ktoré sú od nej tak ďaleko.   Teraz už viem. Veď výbuch prilepil tisíce lietadiel k zemi a nedovolí im vzlietnuť. Sopka dala nebu prázdniny a desiatkam tisícom potencionálnych pasažierov skomplikovala život. Dala a dáva zarobiť dopravným spoločnostiam, ktoré vsadili na dopravné prostriedky s kolesami, či už po ceste, či po železničnej trati.   Sú rôzne prognózy, dokedy budú lietadlá parkovať na zemi. Niektorí odborníci sa vyjadrujú, že ak bude sopka takto intenzívne vystrájať dlhšie, tak môže ovplyviť letovú situáciu aj na pol roka. Tak to by už bola naozaj sila! Zatiaľ chrlí pár dní a čo všetko už jej prachový oblak ovplyvnil...   Opäť raz príroda ukázala svoju moc. Opäť raz nám, malým človiečikom, pohrozila prstom, aby sme si uvedomili, akí drobulinkí a nemohúci sme pred jej silou. Ľudstvo už dokáže kadečo, pokrok v každej oblasti beží svojim osvedčeným tempom ale aj ten má svoje hranice. Pred múrom pani Prírody zväčša zostane nemo stáť so založenými rukami. Jednoducho na túto dámu ešte nedorástol.   Táto myšlienka sa v posledné dni  plazí mojou mysľou. Akosi ma núti uvedomiť si, akí sme - my ľudkovia -pred jej kráľovstvom bezmocní. Živly prírody sme sa nenaučili zvládať, ovplyvňovať a asi sa nám to ani nepodarí. Čosi áno, ale je to len zrnko v kope piesku.  Pred pani Prírodou a jej zbraňami - živlami,  sme takmer nemohúcni. Napriek tomu, že si túto skutočnosť uvedomuje takmer každý človek, napriek tomu máme stále odvahu, silu a drzosť zasahovať necitlivo do prírody. Ničíme, zamorujeme, devastujeme, hubíme...   Činnosť sopky asi človek ničím neovplyvní, ale v mnohom, keby chcel, tak môže byť pani Prírode v jej diele nápomocný.  Zložme konečne masky Kazisveta a začnime sa správať k prírode tak, ako očakávame od nej, aby sa ona správala k nám. Príroda nám naše prehrešky zväčša nedá hned pocítiť, nerada sa mstí, no keď ju už rozhneváme, tak nám to potom zväčša jednorázovo spočíta...Plakať  potom nad rozliatym mliekom je neskoro a zbytočné. Naučme sa rešpektovať prírodu a ona bude rešpektovať nás! Musíme si konečne uvedomiť, že bez nej tu nebudeme ani my...       Foto: islandská sopka, zdroj internet     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




