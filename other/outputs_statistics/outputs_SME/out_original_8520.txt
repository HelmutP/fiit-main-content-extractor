
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Sýkora
                                        &gt;
                STOP ZLU
                     
                 Štátny rozpočet a dlhy naše každodenné 

        
            
                                    10.6.2010
            o
            15:47
                        (upravené
                10.6.2010
                o
                15:57)
                        |
            Karma článku:
                3.90
            |
            Prečítané 
            527-krát
                    
         
     
         
             

                 
                    Ako to vyzerá so slovenským rozpočtom? A čo je to ten štátny dlh? V čase krízy sa s touto témou šermuje kde-kade. Aj tu. :-) Kordy do rúk berú predovšetkým politici. Na Slovensku, kde je zadĺženie relatívne nízke (ale porovnateľné s krajinami bývaleho Východného bloku), nás táto téma akútne netlačí. Zatiaľ. Voda sa začína variť a my (každý z nás!) sme žaba, ktorá si zvyká. Zmysly a racionálne uvažovanie má otupené.
                 

                 V prvom rade, štátny dlh nie je niečo imaginárne, práve naopak. Slovensko dlhuje reálne peniaze niekomu, kto nám ich požičal. Či už ide o formu vydania dlhopisov, ktoré raz budú splatné a požičali nám korporátni investori či dôchodkové fondy je len technický detail. Pointa je taká istá, ako keď si požičia Jožo od Fera pár eur na pivo a cigarety – a síce, že raz ich bude musieť vrátiť.    Aktuálna výška dlhu je viac ako 26 000 000 000 € (26 miliárd), v korunách asi tak 780 000 000 000 (780 miliárd). Takýmto tempom to bude onedlho 5 000 € na každého občana. (Zdroj: Cena štátu)    Čo sa týka aktuálneho stavu, tak príjmy rozpočtu v máji medziročne vzrástli o 0,3%, výdavky však až o závratných 26,4%. (!!!) Nehovoriac o tom, že príjmy sú momentálne na hodnote 31,6% z rozpočtovanej sumy. Inak povedané, za päť mesiacov roka nie sú príjmy ani na úrovni troch! (Zdroj: Štatistický úrad)    Po očistení od inflácie (rastu cien) bol rast slovenského hrubého domáceho produktu (čo je HDP a jeho kritika) v roku 2008 10,6%, 2009 zaznamenal pokles rastu na 6,2% no a v roku 2009 prišla recesia v podobe -4,7%. (Zdroj: CIA Factbook)    Podľa Medzinárodného menového fondu nás čaká v roku 2010 rast na úrovni 4,1%. Je však potrebné si uvedomiť, čo porovnáme ako bázický rok – voči čomu počítame rast. (Čo je viac, 10% zo 100 alebo 2% z 1000?) Čarovnou nuansou zostáva, že napriek rastu HDP bude rozpočet deficitný – je však nutné dodať, že to vyjadruje dlhodobé nastavenie európskej politiky.    S témou súvisia mnohé rozmery, akými sú napríklad Euro (jednak nemožnosť devalvácie po prijatí meny, na druhej strane bohatnutie posilňovaním koruny voči Euru za posledné roky). Privatizácia štátnych podnikov je takmer dokončená, ale bankový sektor je ozdravený a v zahraničných rukách. Na stránkach OECD (Organizácia pre hospodársku spoluprácu a rozvoj) je zároveň možné ľahko porovnať stúpajúcu tendenciu ekonomického rastu v rokoch 2000-2008, či – a to je veľmi dôležité – nárast zahraničných investícií. V rokoch 1995 aj 2000 sme ešte výrazne zaostávali v porovnaní s krajinami Višegrádskej štvorky, ale postupne sa pomer vyrovnal. Snáď okrem Čiech, ktorý sú na tom stále výrazne lepšie. (Berúc do úvahy veľkosť krajín, samozrejme.)    Nemali by sme zabúdať ani na v súčasnosti najvyšší rast nezamestanosti v OECD. V roku 2008 bola na úrovni 7,7%, 2009 12,1% a kam stúpa dnes by sme radšej nemali chcieť vedieť. (Alebo si aspoň počkajme na absolventov, ktorí nenájdu prácu, nech je obraz kompletný.)    Verejný dlh ako pomer k HDP je kapitolou samou o sebe – čísla sa líšia, ale nakoľko v súčasnosti môže byť po započítaní PPP projektov až nad 50%, rast za posledné roky je viac ako slušný. Škoda, že v tomto ukazovateli by sme boli radi aspoň za stagnáciu. (Podľa OECD sme v roku 2008 boli na úrovni 30% HDP - odvtedy však kleslo HDP a stúplo zadlžovanie, takže sa niet čo čudovať.)   A tak ďalej... V každom prípade, „toto“ bude musieť „niekto“ riešiť. Dlhy sa skôr či neskôr stanú našou každodennosťou.    Sám som na pochybách, kto je riešenia schopný a hlavne, ako rýchlo. V tomto smere (v žiadnom prípade však Smere!), by sme mali ísť pozajtra voliť a vyjadriť svoj názor. Jedným z návodov ako na to, môže byť stránka Pre lepšie Slovensko.   P.S.: Viete ako vyzerá trilión dolárov?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Nosme si do supermarketov vlastné „sáčky“
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Daruj darček
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Neľudské zlo v koncentračnom tábore Sachsenhausen
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Malé múzeum Stasi, tajnej služby z čias NDR
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Odtíňať ruky fajčiarskym prasatám
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Sýkora
            
         
        matejsykora.blog.sme.sk (rss)
         
                                     
     
        Človek. Aspoň sa snažím.
 Občas trochu stručný a chladný,
 inokedy zas obšírnejší a citlivý.

 Facebook 
 LinkedIn 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    151
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1173
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Občianske združenie BERKAT
                        
                     
                                     
                        
                            Potrebujeme (r)evolúciu?
                        
                     
                                     
                        
                            Život (je krásny)
                        
                     
                                     
                        
                            Svet ľudí sa nás týka
                        
                     
                                     
                        
                            Na Slovensku je to tak...
                        
                     
                                     
                        
                            Adjumani, Uganda
                        
                     
                                     
                        
                            Írsko, Dublin
                        
                     
                                     
                        
                            Londýn, Veľká Británia
                        
                     
                                     
                        
                            Randers, Dánsko
                        
                     
                                     
                        
                            New York, USA
                        
                     
                                     
                        
                            Triedenie odpadu
                        
                     
                                     
                        
                            Business
                        
                     
                                     
                        
                            Užitočné
                        
                     
                                     
                        
                            Festival Jeden svet
                        
                     
                                     
                        
                            Festival Pohoda
                        
                     
                                     
                        
                            SOS Povodne 2010
                        
                     
                                     
                        
                            STOP ZLU
                        
                     
                                     
                        
                            Energia naša každodenná
                        
                     
                                     
                        
                            Každý iný - všetci rovní
                        
                     
                                     
                        
                            Aktualizované z archívu
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo by som nešiel do povstania ?
                     
                                                         
                       TOP 10 výrokov politikov v roku 2013
                     
                                                         
                       Dobrý projekt jedna krádež nezastaví
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




