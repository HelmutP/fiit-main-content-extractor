
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Orient  
                                        &gt;
                India
                     
                 Výprask 

        
            
                                    13.6.2010
            o
            18:41
                        (upravené
                13.6.2010
                o
                17:43)
                        |
            Karma článku:
                4.55
            |
            Prečítané 
            1462-krát
                    
         
     
         
             

                 
                    V okne kráľovského paláca v Bídari sedel mladý princ s princeznou.
                 

                 Bídar (o ňom pozri tu), hlavné mesto sultanátu Bahmanovcov (o nich tu), ktoré leží v severnom cípe moderného indického štátu Karnátaka, bol v stredoveku vychýreným strediskom obchodu a moci. Afanasij Nikitin, ruský cestovateľ a obchodník, ho navštívil začiatkom 15. storočia a vo svojich zápiskoch vraví: „Do sultánovho paláca vedie sedmoro brán a v každej z nich sedí sto strážcov a sto pohanských pisárov. Jedni zapisujú toho, kto príde, druhí toho, kto odíde. Palác je veľmi krásny. Všetko v ňom je vyrezávané a pozlátené, dokonca aj najmenší kameň....“    Mladý pár si pochutnával na banánoch a obaja sa kochali pohľadom na zvlnený priestor Dakšinskej náhornej plošiny (o nej pozri tu). Bolo práve po monzúnoch, krajina sfarbená do zelena, čas, keď mal princ s otcom opäť odísť na vojnovú výpravu. Vtom si mladý následník trónu všimol, že šupky od banánov, ktoré spolu s princeznou ľahostajne vyhadzovali z okna, zbiera starý žobrák a vyjedá z nich posledné zvyšky sladkého ovocia. Ako je to možné, že sa vôbec dostal až sem, pod okná paláca? A vôbec, čo si to dovoľuje, pozorovať kráľovský pár a čakať na to, či mu hodia nejakú tú šupku! Rozhnevaný princ okamžite zavolal stráže a rozkázal žobráka odviesť preč spod jeho okien a ako príučku za neprístojné správanie mu dať poriadny výprask. Tak sa aj stalo. Akonáhle však vojaci uštedrili žobrákovi pár slušných rán, ten, namiesto toho, aby prosil o odpustenie, začal sa od srdca smiať. Zvláštne správanie princa prekvapilo, pretože kto už sa smeje, keď dostane bitku? Prikázal teda strážam, aby ho prestali biť. Vzápätí však žobrák začal plakať. To už princa zarazilo nadobro a rozhodol sa, že sa žobráka musí na príčinu  jeho zvláštneho správania spýtať sám.    Žobrák, prečo sa smeješ, keď ťa bijú, a prečo plačeš, keď ťa biť prestanú?   Starý muž odpovedal: Akože prečo? Snažil som sa pochutnať si aspoň na zvyškoch ovocia a vďaka tomu som dostal bitku. Kto to kedy videl, aby niekoho bili za to, že líže šupky z banána? Preto sa smejem. Keď sa však pozriem na teba, je mi do plaču. Za ostatky ovocia som dostal pekných pár rán. Aký však bude trest pre teba, čo si zjedol niekoľko celých banánov?      Princ razom vytriezvel a rozhodol sa zanechať svetský život, ktorý ho dosiaľ viedol k radovánkam a pocitu moci, ale nepomáhal mu pochopiť jeho náhly smútok, ktorý sa ho zmocnil po stretnutí so žobrákom. Začal preto putovať a hľadať niekoho, kto by ho vyviedol z premenlivosti šťastia a smútku.    A ako to už v indických príbehoch býva, samozrejme, že ho našiel. Bol ním Sahadžánanda zo starobylého Kaljání (viac o tomto meste tu). Z princa sa neskôr stal svätec Mrtjundžaj – ten, ktorý zvíťazil nad smrťou, alebo, podľa iných, Šáh Muktódží Bahmaní – Oslobodený kráľ-Bahmanovec. Mahipati, autor, vďaka ktorému sa tento príbeh stal známym najmä medzi maráthsky hovoriacim obyvateľstvom Indie (o ňom pozri tu), ho nazval kráľ Šánta Bráhmaní – večne pokojný kráľ s charakterom bráhmana (v prenesenom význame s čistým charakterom). Či už je Mahipatiho príbeh pravdivý alebo nie (motív šupky a ovocia nájdeme aj v iných príbehoch), či už sa príslušník rodu Bahmanovcov naozaj stal svätcom a ľudová tradícia zmiešala pomenovania Bahman (dávny iránsky panovník) a bráhman (príslušník najvyššej hinduistickej kasty), alebo či ide len o idealizáciu lokálneho svätca neistého pôvodu, to už sa dnes s úplnou presnosťou dozvedieť nedá. Jeho hrobka však leží neďaleko Kaljání a uctievajú ju predovšetkým miestni hinduisti. Svedčí o zaujímavých spoločenských vzťahoch v stredovekej Indii, keď sultánski synovia z času na čas hľadali poznanie u miestnych guruov alebo naopak, synovia rádžov navštevovali islámskych mystikov - súfijov, a keď hľadanie duchovného poznania častokrát prekročilo hranice náboženstiev, ak tieto skutočne niekedy pre hľadačov poznania existovali. O tom však niekedy inokedy.        Dušan Deák           Príbeh je voľne prerozprávaný podľa maráthskeho diela z druhej polovice osemnásteho storočia Bhaktavidžaja od Mahipatiho.    Zaujímavé čítanie je aj Lesný, Vincenc, prel. (1951): Putování ruského kupce Afanasije Nikitina přes tři moře. Praha: Slovanské nakladatelství.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Suši sa môže jesť rukou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Civilizácie – zániky a vzniky...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Mačča – kráľ čajov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Tea tree nie je čajovník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Mozaikové mesto
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Orient  
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Orient  
            
         
        orient.blog.sme.sk (rss)
         
                                     
     
        Spája nás FascinÁzia; kultúry a krajiny Orientu sa stali súčasťou našich životov. Spolupracujeme v rámci OZ Pro Oriente (www.prooriente.sk)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1282
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Blízky východ
                        
                     
                                     
                        
                            Ďaleký východ
                        
                     
                                     
                        
                            India
                        
                     
                                     
                        
                            Juhovýchodná Ázia
                        
                     
                                     
                        
                            Všeobecné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




