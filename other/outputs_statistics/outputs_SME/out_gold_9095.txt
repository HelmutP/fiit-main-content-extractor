

  
 "Kto mi tu zacláňa výhľad ?! Čo to... veľký dvojnožec... Hm, čosi mi to hovorí. Niekde som už také čosi videla..."   
  
 "Júúúj, kto tu po mne blýska ?! Vrrr... Cha, to určite ten dvojnohý šašo !"  
  
 "Počuj, obor ! Prestaň po mne záblesky vrhať, ak ti je koža drahá ! My samičky roháčov máme hryzadlá ešte silnejšie než naši páni manželia..."  
  
 "Nežartujem ! Môj manžel je vychýrený zápasník, známy v celej našej dubovej húštine... ale beda mu, ak príde neskoro domov a má pod čapicou ! Vtedy mu pripomeniem, že som hrdá vlastníčka čierneho opasku v hryzadlovom džude !"   
  
 "Joj, si ty ale dotieravý ! Žiadna úcta k starnúcej dáme... Stará som, veru stará - mám už rovné tri mesiace !"  
  
 "Nie, žiadne interview nebude. Vari nevidíš, že som ti nastavila chrbát ?!"  
  
 "Márne sa snažíš dať mi úplatok. Ja takéto listy zásadne nekonzumujem. A to nie som žiadna fajnovka..."  
  
 "Nám roháčom chutí jedine menu z hnijúcich zvyškov dubového dreva. To je skutočná delikatesa, na to vezmi osí jed !"   
  
 "No, ale už naozaj stačilo. Zmizni, dvojnožec, chcem si ešte vychutnať trocha relaxu. A nie je to ľahký relax, musím dávať pozor na tie otravné lietajúce potvory a ich ostré zobáčiská. Ešte šťastie, že som pre väčšinu z nich príliš veľké sústo, na rozdiel od mojich drobnejších kolegov !"    
  
 "Sedíš si na ušných otvoroch ? Už toho mám po tykadlá ! Prestaň narúšať moju súkromnú zónu, dvojnožec... 
  
 ...a povedz tej bielej chlpatej oblude, nech na mňa tak uprene nehľadí !"  
  
 Ale, no ! Rafko, fuj ! Teta roháčka je možno trochu cholerická, ale do psieho žalúdka nepatrí ! Kroť svoje temné úmysly... ;-))    
   
 ---- 
 Roháč veľký (Lucanus cervus) je klasickým zástupcom čeľade roháčovitých na Slovensku a zároveň našim najväčším pôvodným druhom chrobáka. U nás sa vyskytuje prevažne v južných a východných regiónoch krajiny. Jeho prirodzeným životným prostredím sú dubové porasty, obzvlášť staré dubové lesy, v ktorých má dostatok svojej základnej potravy : Odumretých a hnijúcich zvyškov starého dreva. Dnes je pomerne vzácny a lokálne dokonca aj ohrozený. Je u nás celoročne chránený a za jeho usmrtenie je dosť vysoká pokuta (cca 5000 bývalých slovensých korún). Najikonickejšími príslušníkmi druhu sú samozrejme samce (5-7,5 cm), ktoré disponujú veľkými, jelenie parohy pripomínajúcimi hryzadlami. Hryzadlá sú ukážkou bizarnej konvergentnej evolúcie : Samce roháčov ich, podobne ako samce jeleňov, využívajú pri vzájomných súbojoch. Možnosť vidieť naživo samičku roháča - asi o polovicu menšiu (2,5-5 cm) a výzorom obyčajnejšiu - je pomerne vzácna príležitosť. Vážim si, že som ju dostal, a že vďaka nej mohol vzniknúť aj tento článok. :-)   
 Fotografie : (C) Peter Molnár - jún 2010 
   

