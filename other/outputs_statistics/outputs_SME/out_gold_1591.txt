
 
 Neskôr sme s kamarátmi popíjali Absint trochu častejšie (častejšie ako raz za 4 roky). Stále sme ho pili s istou pompéznosťou a držali sme sa rituálu pitia Absintu. Jedného dňa sa mi do vlastníctva dostali 3 pollitrovky tohto nápoja. Nedalo mi a začal som sa viac zaujímať o rituál pitia tohto nápoja. Prišiel som na zaujímavé zistenia. 
 
 Predtým ako budem pokračovať, by som rád upozornil, že sa nesnažím navádzať na pitie alkoholu mladistvých ani dospelých. Alkohol je zlý! Fuj fuj! A zabíja mozgové bunky. A robí neplechu v tele. Tvrdne z neho pečeň. Bije manželky a deti. Ako vravím - ruky preč. Bez srandy. 
 
 Tak a teraz môžeme pokračovať. 
 
 Bežný rituál z krčiem 
 
 Nalejeme Absint do pohára. Na lyžičku si dáme cukor a navlhčíme ho v pohári Absintu. Takto navlhčený a vysoko-horľavý cukor zapálime. Necháme dohorieť (cca minúta) a skaramelizovaný cukor rozmiešame v Absinte. Pijeme na-ex. 
 
 Mnou objavené rituály, ktoré som našiel na internete, sú o niečo zaujímavejšie. 
 
 Prvý rituál, ktorý sa považuje za originálny a prvotný rituál, akým sa pije Absint. 
 
 Do pohára nalejeme Absint. Na vrch pohára položíme špeciálnu Absint-lyžičku a na ňu položíme kocku cukru. Vezmeme pohár ľadovo studenej vody a prelejeme ňou cukor. Zbytok cukru rozmiešame v nápoji. Pomer vody ku Absintu má byť 5:1. Teda viac vody. Popíjame. 
 
 Druhý rituál sa viac podobá na bežný krčmový rituál. 
 
 Do pohára nalejeme 3/4 množstva Absintu, ktoré chceme vypiť. Na vrch pohára položíme Absint-lyžičku a na ňu kocku cukru. Vezmeme fľašu Absintu a dolejeme štvrtú štvrtinu Absintu. Dolievaný Absint má pretiecť cez kocku cukru, aby bola navlhčená. Cukor zapálime a necháme chvíľu horieť. Ešte počas horenia vezmeme lyžičku a premiešame ňou cukor s Absintom. Zahasíme ľadovo studenou vodou, opäť chceme dosiahnuť pomer vody k Absintu 5:1. Takto pripravený nápoj popíjame. 
 
 Oba "regulárne" spôsoby pitia Absintu spôsobia zriedenie tohto moku, avšak treba si uvedomiť, že Absint nie je na to, aby sme sa z neho opili na mol, ale aby sme ho vychutnávali - veď Absint je likér. Na poriadne opitie sa odporúčam radšej vodku, gin, rum alebo borovičku. 
 
 Apropo Absint-lyžička. Špeciálne upravená lyžička, ktorá nespadne z pohára ani do pohára - ani po niekoľkých kolách a má v sebe otvory, cez ktoré pretečie modro-zelený mok cez kocku cukru do kalicha. 
 Absintu zdar. 
 
