

 Na tanier z poľných kvetov  
 uložím hrozno a syr. 
 Nech mušky ukradnú si 
 kus vône týchto chvíľ. 
   
 Na steblá prestriem deku, 
 chrobákom spravím noc. 
 To ty si mi, moja láska, 
 dala tu božiu moc. 

