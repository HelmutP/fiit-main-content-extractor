
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gleb Chesnokov
                                        &gt;
                Spoločnosť
                     
                 Západ hynie: prestante dávať peniaze na hunanitárne programy 

        
            
                                    22.4.2008
            o
            9:08
                        |
            Karma článku:
                8.62
            |
            Prečítané 
            2761-krát
                    
         
     
         
             

                 
                    Tretí faktor, ktorý som spomínal už v predchádzajúcich článkoch, je ten, že islamské extrémistické hnutia využívali ako riešenie svojich otázok medzi veľmocami, napr. ako CIA financovala Bin Ládina. Je menej známa skutočnosť, že svojho času práve izraelská rozviedka Mossad umožnila vznik organizácie ako „Skupina mučeníkov Al-Aqsa“.
                 

                Myšlienka Massad bola taká: máme Yassera Arafata, ktorý bol v tej dobe skutočným komunistom, a k Islamu mal veľmi vzdialený vzťah – tak skúsime postaviť oproti nemu niečo islamské, veď Islam komunistov nemá rád. Postavili.        Ale to je veľmi známa a lacná historka. Mňa zas trápi jedna otázka: mnohí kritizujú nevychovaných moslimov za to, že posledné desaťročia (nie všetci) veľmi nemajú radi Západ a prejavili to najmä tým, že nabúrali do dvojičiek. Ale pozrite sa na seba, na vzdelaných. Prosím, povedzte mi, či aspoň jeden človek v spoločenskej sfére na Západe vyslovil nahlas myšlienku likvidácie tých humanitárnych programov, ktoré tú všetkú nenávisť plodia? Vy nakoniec prestanete kŕmiť Organizáciu oslobodenia Palestíny, jej nástupcov a protiklady Hamas a Fatah, ktoré teraz vládnu v Gaze, vy ich prestanete podporovať? Oni dostávajú miliardy dolárov pomoci. A čo s tým robia?        To, čo má teraz názov Palestína, je veľmi zaujímavé zoskupenie. Nie je to štát a vôbec sa nechce byť štátom, pretože je pritom potrebné brať zodpovednosť za svojich občanov. Je to zoskupenie, ktoré rozdeľuje medzi vrchnosť a svojich obyvateľov peniaze zo Západu, Saudskej Arábie a Iránu. Neviem, či viete, ale na rozdiel od mnohých západných krajín, je v Palestíne stomatologická starostlivosť zdarma.        Nepamätáte na nádhernú situáciu, kedy chudobné obyvateľstvo Palestíny pretrhlo blokádu a začalo utekať do Egypta? Vtedy som sledoval televíziu a veľmi mi udrelo do očí, že tá chudobná, nešťastná, utláčaná palestínska mládež si konečne pre seba kúpila motorky v Egypte. Tak prepáčte, ale keď chudobné obyvateľstvo pretrhlo blokádu, malo by začať vykradať, a v tomto prípade začalo kupovať motorky a autá. Sú to naozaj chudobní ľudia!        Odkiaľ majú peniaze? Najmä zo Západu, v prvom rade od rôznych humanitárnych programov. Už to konečne prestane niekto podporovať! Aspoň niekto, kto sa považuje za zodpovedného politika, začne na Západe diskusiu o tom, že netreba simulovať pre ľudí situáciu, pri ktorej nič nerobia, pri ktorej nemajú žiadne stimuly, okrem toho ako zadarmo dostať peniaze a najmä tým, že vyhodia do vzduchu seba alebo svojho syna.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gleb Chesnokov 
                                        
                                            Nové letadlo ruského prezidenta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gleb Chesnokov 
                                        
                                            Pohreb (Lk 24,13-35)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gleb Chesnokov 
                                        
                                            Povolanie a poslanie veriacich (Mk 6, 7-13)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gleb Chesnokov 
                                        
                                            Rodina to je sviatosť (Mt 19, 3-6)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gleb Chesnokov 
                                        
                                            Krst to je voľba (Mk 10, 13-16)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gleb Chesnokov
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gleb Chesnokov
            
         
        chesnokov.blog.sme.sk (rss)
         
                                     
     
        Narodil som sa a vyrástol som.

  
 

 

        
    
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1909
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Denník
                        
                     
                                     
                        
                            Rusko
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ako sa riešia lúpežné prepadnutia, alebo príbeh o imunite Rómov
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ján Szentkereszty
                                     
                                                                             
                                            sestra Hermana
                                     
                                                                             
                                            Zora Pauliniová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            brat Šavol
                                     
                                                                             
                                            NEWSRU.com
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  




     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




