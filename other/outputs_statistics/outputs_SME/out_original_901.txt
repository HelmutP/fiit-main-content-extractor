
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Stožická
                                        &gt;
                FOTÁ
                     
                 Stratená - dovolenka 

        
            
                                    29.9.2008
            o
            8:00
                        |
            Karma článku:
                10.75
            |
            Prečítané 
            6430-krát
                    
         
     
         
             

                 
                    V živote prichádzajú chvíle, keď človek cíti, že je toho naňho veľa. Má stále menej síl bojovať s problémami a hoci vie, že utekať od nich nie je to najlepšie riešenie, zatúži sa jednoducho stratiť. Aspoň na pár dní, ale poriadne. Uniknúť z dohľadu svojich večných a neľútostných veriteľov času, nenápadne splynúť s vegetáciou... Presne takéto chúťky ma ovládli po lete bez dovolenky. I vzala som manžela, nasadla na vlak a poďho niekam, kde sme ešte nikdy neboli. Náš cieľ mal veľmi príznačné meno - Stratená.
                 

                  Stratená je malá obec na juhu Slovenského raja, s históriou Csákyovských vysokých pecí a milým gotickým kostolíkom, ktorý je hrdou súčasťou Gotickej cesty.                 Je dobre schovaná, ale my sme ju našli asi po tri štvrte hodinke pochodu zo železničnej stanice Dobšinská ľadová jaskyňa, po prúde riečky Hnilec.     Cestou sme si mohli vychutnať pohľad na kľukatý Stratenský kaňon.                          Priezračne čistá riečka sa vytrvalo zahrýza do sivo-bielych vápencov. Až keď sa človek zadíva na vrcholy strmých skál, kde ten proces musel začať, uvedomí si, aká neskutočná sila spočíva v obyčajnom (ale neprestajnom) tečení.                       Bola sotva polovica septembra, no do okolia Stratenej si našla cestu jeseň.                Lúky plné jesienok nás v tomto kraji vítali už z vlaku. Ja som sa nevedela dočkať, kedy ich uvidím zblízka.                Okraje ciest zdobili modré pakosty,                dopĺňané nenápadnými očiankami...                ... a jemnými hlavičkami hlaváčov.                V tieni smrečín ťažko skúšaných polomami                  na vlhkých skalách sa darilo slezinníkom, papradiam a najrôznejším druhom machov a pečeňoviek. Milovníci výtrusných rastlín by povedali, že sa ocitli v raji.                               Husté machové pralesy skrývali početné čriedy mikroskopickej zveri.                  Čriedy zveri v makroskopických pralesoch sme iba počuli. Z Havranej skaly sme videli strašidelné zdrapy oblakov, ženúce sa prázdnotou pod našimi nohami a len kde-tu odhaliace kúsok lesa, alebo hladiny, v ktorej sme tušili vodnú nádrž Dedinky. Zhodli sme sa, že na túto vyhliadku musíme prísť ešte raz a pri lepšej viditeľnosti.                 Samotná Havrania skala je prevŕtaná úctyhodnými jaskyňami, v ktorých by sa človek jaskynný určite cítil ako doma. Kúsok pod ňou vyviera (alebo nevyviera) Občasný prameň. My sme mali šťastie - keď už nie na slnečný svit, tak aspoň na dostatok vody - a videli sme ho v činnosti.                  Ako v každých správnych horách, aj tu našli lišajníky ideálne podmienky pre svoj rast.                Horec luskáčovitý už dokvital,                 ale z času na čas sme narazili aj na takéto plne rozvité súkvetie.                Dobšiná - najbližšie sídlo s výdobytkami civilizácie typu bankomat - a krásnym výhľadom na Slovenské rudohorie. Pre moderných turistov: Keď sa chcete kvalitne stratiť, je naivné očakávať, že tam budete môcť platiť kartou... A ešte jedna rada: Žltá značka z Dobšinej vedie cez súkromný pozemok, na ktorom dobroprajný majiteľ zamaľoval značenie a prelepil priateľskými odkazmi ako "Zákaz vstupu" a "Pozor zlý pes" (naznačiť pocestným alternatívnu trasu mu asi mentálna úroveň nedovolila). Dá sa to obísť kúsok po zelenej a potom po lesnej ceste, spýtajte sa miestnych ĽUDÍ...                Na výslnných stráňach sme našli aj takéto zlaté spomienky na leto (vyzerá to krajšie ako žltá značka).                 Na opačnej strane kopca zasa kvitol vres.                  Rýchle ochladenie mnohých prekvapilo. Mŕtva a "spomalená" včela na bodiaku ukazujú smutnú stránku jesene.                Vodná nádrž Dedinky (Palcmanská maša). Zo Stratenej sem jazdí skvelý malý vláčik. Lanovka na Geravy však zaháľa - dopravu hore kopcom si človek musí zabezpečiť svojpomocne.                Cesta Zejmarskou roklinou okolo vodopádov kapitána Nálepku patrí k tým trasám na Geravy, ktoré človek neľutuje. Údajne najľahšia roklina v Slovenskom raji. Jej zdolanie nás príliš neunavilo - len zvýšilo našu chuť na rebríky. Rozchodník hore na Geravách ukazoval dve hodiny po zelenej značke k ústiu Sokolej rokliny a my sme neodolali...                A neľutovali sme. Stretli sme salamandru, zdiaľky zazreli aj stádočko laní... Cesta nám vzhľadom na stav chodníka trvala dlhšie než dve hodiny...                   Až tu sme si naplno uvedomili, aká veľká voda sa musela nedávno prehnať Slovenským rajom - vymletý chodník, odnesené mosty, koryto potoka obnažené až na biele "kosti matky Zeme".                  Technická pamiatka - systém vodných nádrží Klauzy uľahčoval našim predkom dopravu kmeňov z lesa.                Závojový vodopád v Sokolej rokline, vraj "najdivšej" v Slovenskom raji, nám vzal dych.                Vychutnávali sme si dlhočizné rebríky a skvostné výhľady, dojem z ktorých nepokazilo ani daždivé počasie.                 A keď si človek myslí, že už vyliezol úplne hore, objaví sa pred ním ešte jeden vodopád s ďalším rebríkom...     Hoci sme väčšinu prevýšenia vyšplhali v Sokolej rokline, návrat do Stratenej nás stál posledné sily. Na konci sme si uvedomili, že náš výlet Dedinky-Zejmarská-Sokolia-Stratená trval deväť hodín nepretržitého šliapania (dokonca aj jedlo sme hrýzli postojačky, lebo všade bolo mokro). Tak sme sa potľapkali po pleciach, ubezpečili navzájom, že to za to stálo a potichu rozhodli, že nabudúce budeme uvážlivejší...                Na druhý deň sme zo železničnej stanice Poprad-Tatry zamávali našim ťažko skúšaným veľhorám, ktoré sa radšej zahalili do oblakov. O niekoľko hodín sme sa našli v Bratislave...           A poučenie? Ak sa máte kam stratiť, nič nemusí byť stratené, bez ohľadu na počasie. Tento článok píšem s vedomím, že už v čase jeho zverejnenia mi skočia na krk moje staré problémy - bohužiaľ, ani jeden z nich nie je zajac a tak si na mňa trpezlivo počkali. Ale nádej, tú ešte nestrácam... A najneskôr o rok sa chcem vrátiť do Slovenského raja.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Zuzana Stožická 
                                        
                                            Dejepiscovo dobrodružstvo - recenzia románu Martina Juríka: Projekt Zenta.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Stožická 
                                        
                                            Nezvyčajný hosť - bernikla na Kuchajde
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Stožická 
                                        
                                            Vážne vášne vážok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Stožická 
                                        
                                            Quercia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Stožická 
                                        
                                            Prebudené dreviny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Stožická
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Stožická
            
         
        stozicka.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som idealistka, scifistka, milovníčka prírody. Najradšej metabolizujem písmenká a lovím svetelné odlesky svetskej krásy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1633
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            FOTÁ
                        
                     
                                     
                        
                            Nostalgická zásuvka
                        
                     
                                     
                        
                            Kňyški
                        
                     
                                     
                        
                            SF_Akcie
                        
                     
                                     
                        
                            Ideológie
                        
                     
                                     
                        
                            Svet podľa...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Juraj Smatana na učiteľskú nôtu
                                     
                                                                             
                                            Pekná recenzia na Dívku na klíček
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dotyky budúcnosti
                                     
                                                                             
                                            Kevin Hearne: Kroniky železného druida
                                     
                                                                             
                                            Connie Willisová: O psu nemluvě
                                     
                                                                             
                                            Patrick Rothfuss: Strach múdreho muža
                                     
                                                                             
                                            Dušan D. Fabian: Odbila 13. hodina
                                     
                                                                             
                                            Čas hrdinov (antológia fantasy)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Karel Plíhal: Vzduchoprázdniny
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Vtáky (Slovenská ornitologická spoločnosť)
                                     
                                                                             
                                            Jupiter (e-časopis o fantastike)
                                     
                                                                             
                                            Fandom
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Príbeh z čiernobieleho sveta, ktorý ale nie je čiernobiely
                     
                                                         
                       Kvapkanie do ľadu - fotografie
                     
                                                         
                       Česká hard scifi kvalitka - Zpráva z Hádu
                     
                                                         
                       Čítanie pre všetkých Figurov (a figúrky) dôb súčasných aj budúcich
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




