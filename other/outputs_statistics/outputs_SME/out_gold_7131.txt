

 Na začiatku bolo lístkové cesto a veľká chuť na niečo slané, čo by sme mohli chrumkať s Buttlerom pri DVDčku.. ehmm.. čo by sme mohli chrumkať pri DVD-čku s Buttlerom ;) 
 Mrkla som do chladničky a nápad bol na svete. 
   
 Na cca 20ks jednohubiek potrebujeme: 
 1 lístkové cesto (najlepšie to zrolované, ktoré už netreba vaľkať) 
 200g šunky 
 5 lyžíc kyslej smotany 
 2-3 jarné cibuľky 
 oregano, bazalka, farebné korenie, cesnak, soľ, ľanové semienka, olej a vajíčko na potretie 
   
 Na vyvaľkané cesto rozotrieme smotanu a posypeme ju ľanovými semienkami. 
  
 Oregano, bazalku, farebné korenie, cesnak, lyžičku oleja a trošku soli zmiešame. Pozor, ak máme príliš slanú šunku, soľ úplne vynecháme. 
  
 Po celej ploche cesta rozložíme šunku, potrieme ju bylinkovou zmesou a pridáme na kolieska nakrájanú  jarnú cibuľku. 
 Na začiatku som spomínala veľký priestor pre fantáziu. Jednohubky môžme  pripraviť v mnohých obmenách. S kečupom, taveným syrom, strúhaným  eidamom, salámou, na tenko nakrájaným kuracím mäsom, so sezamom..  každému podľa chuti, alebo čo chladnička ponúka. Varenie je hra :) 
  
 Opatrne zrolujeme. 
  
 Potrieme rozšľahaným vajíčkom a nakrájame jednotlivé kúsky. Ukladáme na vymastený plech. 
  
  
 Pečieme asi 20 minút pri 180°C. 
  
 Podávame s Ctihodným občanom, alebo iným filmom ;) 
 Dobrú chuť! 
  

