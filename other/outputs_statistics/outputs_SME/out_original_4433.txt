
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            sona hruzikova
                                        &gt;
                dušou
                     
                 Jedy 

        
            
                                    12.4.2010
            o
            19:11
                        |
            Karma článku:
                8.36
            |
            Prečítané 
            1764-krát
                    
         
     
         
             

                 
                    Niekedy si na hlavu dávam kapucňu. Chcem sa stratiť vo veľkej šedej mikine, chcem odísť alebo zmiznúť. Som radikálna, krutá, možno groteskná, keď túžim kričať a rozbíjať. Potom pijem alkoholický nápoj, ponáram sa do horúcej vody vo vani, rozpúšťam zlosť. A jej príčina sa tým zatiaľ len čistí, trbliece sa, vyostruje.
                 

                Keď som bola malá, mama ma posielala pozrieť sa do zrkadla. Alebo povedala, že vezme špendlík, aby som praskla. Taká som bývala odutá, škaredá. No nakoniec som sa musela smiať a smejem sa doteraz, odutá Soňa, nič smiešnejšie ste ešte nevideli.   U babky za záhradou je jarok, hore druhým brehom ďalšie záhrady a domy, ďalšia ulica. Okno mojej izby bolo na tú stranu, občas v lete som počula, ako tam niekto kričí, rozbíja taniere. Tma, svrčky, nezrozumiteľný krik, ozvena dedinského priestoru. Ráno sa však iste ospravedlňovali, potichu, so sklonenou hlavou, medzi tie črepy. Potom pár dní napätie, ale keď sa prvýkrát spolu zasmiali, bolo už dobre.   Ak na niečo verím, tak na silu myšlienok. A nielen preto, že v noci sa budím chvíľu predtým, ako mi príde esemeska. Čo to s nami musí robiť, keď ich nepúšťame von, ale starostlivo si ich kŕmime, vystielame nimi vlastný svet. (Odpovede bez otázok, duša v ich tieni.) A potom kričíme, zobúdzame susedov a čudujeme sa, že nás nikto nepočuje.   Mohla by som tvrdiť, že odkedy som ako malá stála pred zrkadlom a pozerala sa, ako mi hnev deformuje tvár, sa zmenili príčiny všetkých jedov.  Mohla by som, ale zaťaté päste sú stále len zaťaté päste.   Vidím ich všade, opakujem ich. Definujem ich pocitom, že toho treba veľa zmeniť. Možno aj všetko. Sú to príčiny, zároveň riešenia a z tohto kruhu sa mi už točí hlava. Sila myšlienok, túžba po inom, okolnosti a ich interpretácie.   Teraz svetlo a ticho.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Ortuť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Mariánska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Imágo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Posledné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Zmeny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: sona hruzikova
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                sona hruzikova
            
         
        hruzikova.blog.sme.sk (rss)
         
                        VIP
                             
     
        "Musí sa, pomyslela si a rozvážne ponorila štetec do farby, udržať na úrovni prostého zážitku, jednoducho cítiť, že toto je stolička, toto stôl, ale zároveň cítiť aj to, že je to zázrak, je to extáza." Virginia Woolfová
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    137
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1683
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            dušou
                        
                     
                                     
                        
                            úlomky
                        
                     
                                     
                        
                            spolu
                        
                     
                                     
                        
                            hrach o stenu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Oštiepok
                                     
                                                                             
                                            Behind the fence
                                     
                                                                             
                                            Respice Finem
                                     
                                                                             
                                            re-wilding
                                     
                                                                             
                                            Sami people
                                     
                                                                             
                                            Kenozero Dreams
                                     
                                                                             
                                            Rudo
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Michal Ajvaz - Lucemburská záhrada
                                     
                                                                             
                                            Olga Tokarczuk - Dom vo dne, dom v noci
                                     
                                                                             
                                            Sándor Márai - Čutora. Pes s charakterem
                                     
                                                                             
                                            Graham Swift - Krajina vôd
                                     
                                                                             
                                            Ladislav Ballek - Pomocník
                                     
                                                                             
                                            Dragan Velikić - Ruské okno
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Outliers
                                     
                                                                             
                                            Ryat
                                     
                                                                             
                                            Nils Frahm
                                     
                                                                             
                                            Julia Holter
                                     
                                                                             
                                            iamamiwhoami
                                     
                                                                             
                                            Ólafur Arnalds
                                     
                                                                             
                                            Julianna Barwick
                                     
                                                                             
                                            Jono McCleery
                                     
                                                                             
                                            Fever Ray
                                     
                                                                             
                                            Sophie Hutchings
                                     
                                                                             
                                            Soap &amp; Skin
                                     
                                                                             
                                            Hidden Orchestra
                                     
                                                                             
                                            Kraków Loves Adana
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jonathan
                                     
                                                                             
                                            Saša
                                     
                                                                             
                                            Simona
                                     
                                                                             
                                            Baša
                                     
                                                                             
                                            Samo
                                     
                                                                             
                                            Tony
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Andrej
                                     
                                                                             
                                            Tomáš
                                     
                                                                             
                                            Daniela
                                     
                                                                             
                                            Džejn
                                     
                                                                             
                                            Anna
                                     
                                                                             
                                            Alenka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            iGNANT
                                     
                                                                             
                                            Lemon
                                     
                                                                             
                                            Humno
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




