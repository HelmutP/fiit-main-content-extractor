

 Technicky sa dá napísať akýkoľvek cenník, napríklad auta s evidenčným číslom Košíc parkovanie zadarmo, ostatní euro/hodina, alebo vozidlá evidované v Petržalke za 100 euro/rok ostatní euro/hodina. 

 Odpoveď je pomerne jednoduchá, ale najprv trocha histórie. Ešte v nedávnej minulosti mali niektoré hotely publikovanú katalógovú cenu napríklad xxxx,xx Sk izba/noc, ale keď hosť predložil pas, tak mu účtovali inú cenu. Inú položku z cenníka, určenú pre zahraničného návštevníka, ktorá bola aj 3 násobne vyššia. 

 Takýmto diskriminačným cenníkom založených na štátnej 
príslušnosti spravila koniec až Slovenská obchodná inšpekcia, 
vysokými pokutami, ktorá vylúčila akúkoľvek diskrimináciu. Konkrétna služba musí byť poskytnutá pre všetkých za oficiálne publikovanú cenníkovú cenu a nie je možné ju viazať na to odkiaľ hosť pochádza. 

 To isté platí aj pri parkovaní. Prevádzkovateľ parkovania môže spraviť akýkoľvek cenník v hodinových, dnových alebo aj ročných sadzbách, ale 
služba musí byť prístupná pre všetkých bez akejkoľvek diskriminácie na pôvode vodiča alebo vlastníka vozidla. 

 Otázka: Ako postupovať keď niekto chce dať zľavu, ale len domácim.  
Dá sa aj to, ale určite nie rôznou cenníkovou cenou. Keď niekto chce, môže dotovať vybraného parkujúceho zo svojich prostriedkov. Čiže cena za parkovanie zostane jednotná, ale časť z nej zaplatí ten kto dotuje, napríklad mestská časť. V praxi to bude znamenať napríklad, že ročný parkovací lístok na jedno vozidlo bude stáť 1000 Euro a môže si ho kúpiť ktokoľvek a aj fakturovaná suma bude 1000 Euro!, ale mestská časť preplatí každému domácemu obyvateľovi, alebo na byt, dom, prípadne podľa inak odsúhlasených pravidiel 900 Euro za zakúpený lístok. Pritom je jedno či to budú dve samostatné transakcie, alebo to pokladňa zvládne v jednej faktúre. 

