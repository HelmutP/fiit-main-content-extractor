

   
 
  Ako štáty šliapu po medzinárodnom práve 
   
 Na začiatok by som veľmi rád spomenul niekoľko faktov o Kosove a jeho statuse z pohľadu medzinárodného práva. Po skončení bombardovania v roku 1999 prijala Bezpečnostná rada OSN rezolúciu č. 1244, podľa ktorej územie Kosova patrí Juhoslávii (nástupnícky štát Srbsko). Bezpečnostná rada OSN doposiaľ nezmenila svoju rezolúciu č. 1244, takže územie stále patrí Srbsku. Vyhlásením nezávislosti Kosova nastala jednostranná revízia hraníc. Podľa Záverečného aktu Helsinskej konferencie, ktorá nahradzuje mierovú zmluvu po 2. svetovej vojne sa umožnuje zmena hraníc jedine so súhlasom dotknutého štátu - čiže Srbska. Podľa medzinárodného práva je uznanie Kosova ako štátu neprípustné a tvoriace neuveriteľný precedens - spomeniem iba oblasti, ktorých sa to týka - Podnestersko, Južné Osetínsko, Abcházsko a možno v budúcnosti sa na tento precedens odvolajú Katalánci a Baskovia. Je nepochopiteľné, koľko štátov uznalo nezávislosť Kosova. 
   
 Hashim "Snake" Thaçi - cesta od zločinca k premiérovi 
   
 Hashim Thaçi je prvým "premiérom" Kosovského "štátu". Študoval filozofiu a históriu v Prištine. Po emigrácii do Švajčiarska sa stretával s albánskou "elitou" ,ktorá mala veľmi blízko k albánskej mafii. Bol predstaviteľom Nezávislej albánskej študentskej asociácie a podporovateľom marxisticko-leninovskej organizácie Národné kosovské hnutie. Ideológia ktorú Hashim presadzoval bola ideológiou nacionalizmu a tzv idey "Veľkého Albánska". V roku 1993 odišiel do Albánska a stal sa vnútorným členom UÇK - v tích rokoch považovanú za teroristickú organizáciu až do roku 1998, kedy bola vyškrtnutá z čiernej listiny najhľadanejších teroristických organizácií. 
 Prvým krokom k označeniu Hashima Thaçiho za zločinca môžeme označiť incident, ktorý spáchal 25. mája 1993 spolu s Rafetom Ramaom, Jakupom Nurim, Samim Ljustkom a Ilijazom Kadrijom, kde spolu napadli v blízkosti mesta Glovac členov srbskej polície. Dvaja policajti boli zabití a piati zranení. 
 Druhý teroristický útok pripisovaný Hashimovi Thaçimu a jeho skupine sa stal na ceste vedúcej k mestu Pec, kde bol jeden policajt zranený a druhý útok neprežil. Pod jeho velením údajne dochádzalo k páchaniu vojnových zločinov, je podozrivý z vraždy bývalého ministra obrany Ahmeta Krasniča a vytvoreniu "vyhladovacieho" tábora Lapusnik, kde zahynulo množstvo Srbov a aj Albáncov. Na Hashima bola podaná žaloba a následne vydaný zatykač Interpolu. V roku 1997 ho srbský súd uznal vinným a odsúdil ho na 10 rokov väzby. 
 Zmena v chápaní Hashima Thaςiho ako vojnového zločinca a teroristu nastala v roku 1998, keď sa dostal do milosti americkej diplomacie a z teroristickej organizácie sa stala Kosovská oslobodenecká armáda bojujúca za práva Albáncov. Hashim sa dostal do kontaktu s bývalou americkou minsterkou zahraničných vecí, Madeline Albrightovou. Ministerstvo spravodlivosti Srbska predložilo rozsiahly počet dôkazov a výpovedí týkajúcich sa zločinov spáchaných na príkaz Hashima Thaςiho Medzinárodnému súdnemu dvoru pre bývalú Juhosláviu. Bol zatknutý, ale pre svoje medzinárodné kontakty bol nakoniec prepustený a pred súd v Haagu sa ani nedostal.  V roku 2008 bol obvinený žalobkyňou Carlou del Ponteovou, že v roku 1999 osobne organizoval únos niekoľko stoviek kosovských Srbov, ktorí boli prevezení do Albánska a nakoniec boli zabití a ich orgány boli ponúknuté do Európy na predaj ich záujemcom. 
 Jeho cesta do vrcholnej politiky nebola až taká závratná - s medzinárodnými kontaktmi a svojím menom v Kosove ako predstaviteľ Demokratickej strany Kosova sa stal po voľbách v roku 2007 premiérom samozvaného "štátu" Kosova. 
   
 Prečo vlastne naozaj vznikol pseudo-štát Kosovo? 
   
 Dalo by sa odpovedať jedným slovom - Bondsteel. Je viac než jasné, že pseudo-štát Kosovo by nemohol existovať bez zahraničnej podpory, a ako je známe zahraničná podpora nie je nikdy iba jednostranná - musí mať svoje výhody. Táto výhoda pre USA je očividne vojenská základňa Bondsteel. Bondsteel je jedna z najväčších základní mimo územia USA. Jej výhodná poloha v blízkosti západnej Európy a Ruska má svoje opodstatnenie. Ale to je už na Vaše vlastné uváženie, čo znamená sloboda nového "mafiánsko-pseudo-štátu" v dnešnej svetovej politike oproti právu štátu na svoju celistvosť a čo znamená pre USA dodržiavanie medzinárodného práva. 
  
 Dokument o Kosove natočení Českou televíziou no nikdy nebol odvisielaný. Je voľne dostupný na video.google.com
  
   
 
   
   

