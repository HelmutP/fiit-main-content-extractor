

 A tak sa v mojom spise objavil PRÍSNE TAJNÝ plán agentúrno-operatívneho rozpracovania objekta „FILOZOF“ zo dňa 10. decembra 1980, v ktorom sa okrem iného uvádza nasledovné: 
   
 K ďalším zaujímavým poznatkom patrí aj to, že objekt sa vyjadril, že neudržuje, ani nemieni udržovať písomné styky na KZ (kapitalistické zahraničie). Avšak cestou prameňa „NÁKUP“ (zadržiavanie a otváranie pošty) bolo získaných niekoľko zaujímavých stykov objekta, hlavne na Angliu, Kanadu a Japonsko. Z obsahu korešpondencie, ktorú objekt dostáva je vidieť, že listy musí zasielať do KZ aj on, avšak tieto cestou TÚ „NÁKUP“ zachytené neboli, ani doposiaľ nebol zistený kanál, cesta alebo spôsob dopravy korešpondencie smerom od objekta do KZ. Je tu predpoklad, že objekt zasiela svoje listy do KZ z ostatných krajov ČSSR, alebo cez tretiu osobu z niektorého socialistického štátu, prípadne na kryciu adresu do KZ.  
   
 V tom čase existovala na Slovensku veľmi známa hudobná skupina. Nebudem ju menovať, ale v Slovenskom rozhlase jej pesničky často dávajú do dnešných dní. Ja ju poznám iba po tejto linke. A ešte tak, že nám hrala na imatrikulácii v bratislavskom Parku kultúry a oddychu na úvod do prvého ročníka vysokej školy v 1964.  
   
 Často chodili hrať do zahraničia cez Slovkoncert, teda podnik zahraničného obchodu, ktorý v tom čase mal monopol na sprostredkovávanie takýchto prác pre našich umelcov v cudzine. Znamenalo to, že boli intenzívnym predmetom záujmu štátnej bezpečnosti. A čo čert nechcel, jeden člen tejto skupiny mal blízku príbuznú, ktorá bývala v tom istom dome, dokonca v tom istom vchode ako ja a  niekoľkokrát do roka ju navštevoval. Ja som sa o ňom dozvedel až z môjho spisu, on o mne zrejme niečo skôr, lebo nepochybujem, že ho kvôli mne vyťažovali, aj keď žiadne záznamy z jeho prípadných výpovedí o našich neexistujúcich stykoch som v spise nenašiel. Zato našiel som nasledovný, samozrejme PRÍSNE TAJNÝ záznam z 18. februára 1982:  
   
 „... V osobe xxx sa jedná o bývalého člena hudobnej skupiny "yyy“, ktorý s uvedenou skupinou často pôsobil v kapitalistickom zahraničí. Tiež chodí do Košíc jeden až dvakrát do roka po návrate z kapitalistického zahraničia. V Košiciach má mimo iného navštevovať zzz, ktorá býva na adrese, kde býva aj objekt "FILOZOF"... 
   
 ... Zaslať dožiadanie na I. odbor S-ŠtB Bratislava, za účelom previerky xxx, so zameraním sa na jeho cesty do kapitalistického zahraničia, ktoré štáty navštívil a ako dlho tam pôsobil, prípadne jeho styky. Taktiež jeho politické názory, minulosť i súčasnosť a pod.... 
   
 ...Vzhľadom k tomu, že cestou STÚ NÁKUP nie je zachycovaná korešpondencia, ktorú objekt „FILOZOF“ zasiela do kapitalistického zahraničia, je tu predpoklad, že túto vynáša do kapitalistického zahraničia nejaká ďalšia osoba, ktorá často do kapitalistického zahraničia cestuje. Keby bolo zistené, že dochádza k stykom medzi xxx a objektom, tak xxx by mohol vynášať listy do kapitalistických štátov, nakoľko často chodí do kapitalistického zahraničia ako hudobník...“ (STÚ Nákup v eštebáckej terminológii znamená, ako som už spomenul, otváranie a kontrola pošty a objekt „FILOZOF“ som, prosím pekne, či prepytujem, ja.) 
   
  „Previerkou bolo zistené, že objekt „FILOZOF“ by mohol prichádzať do styku s xxx z Bratislavy, asi 35 ročným, bývalým členom hudobnej skupiny yyy, ktorý často dochádza do Košíc po návrate z kapitalistického zahraničia, do miesta trvalého bydliska objekta „FILOZOF“. Preto je tu predpoklad, že by xxx mohol byť využívaný objektom ako spojka pre spojenie nášho objekta do KZ (kapitalistického zahraničia). Preto je žiadúce uvedené okolnosti a prípadné styky objasniť. 
             Z vyššie uvádzaných dôvodov žiadam vykonať previerku xxx v tomto rozsahu: 
 -         Z akej rodiny pochádza a aké má vzdelanie. 
 -         Aké styky udržuje v ČSSR a na KZ z hľadiska spravodajského.  
 -         Jeho politická činnosť a postoje v minulosti, ako aj v súčasnej dobe. Činnosť v krízovom období 1968-69. 
 -         Vzťah menovaného k nášmu zriadeniu. 
 -         Pobyt a styky menovaného v KZ. Kedy, koľkokrát, v ktorom KŠ (kapitalistickom štáte) a akú dobu menovaný pôsobil. 
 -         Iné zaujímavé poznatky, ktoré by nadväzovali na uvedenú operatívnu situáciu. 
 Výsledok previerky nám zašli k nášmu horeuvedenému číslu. 
   
 Niektoré zahraničné kontakty z čias môjho osemročného nepovoleného pobytu mimo republiky som si ponechal do dnešných dní. Aj za socializmu som si s nimi dopisoval, dokonca som sa s niektorými z nich aj stretol v Československu ešte za totality, neviem, či eštebáci o tom vedeli, lebo časť spisu, v ktorej by o tom mohla byť zmienka, hneď po novembri 1989 zničili. Listy zo zahraničia eštebáci zachytávali, nechali si ich preložiť, vyhodnocovali ich a potom mi ich postúpili znovu zalepené. Nevedeli ale zachytiť moje listy smerom navonok, hoci som ich posielal riadnou poštou. Dokonca som mal vo zvyku na obálky lepiť veľa známok (v časoch socialistického Československa sme mali veľmi pekné poštové známky), takže moje listy boli ako výkladná skriňa pošty a museli poriadne biť do očí. Eštebáci medzičasom stihli prekonšpirovať môj prípad, hľadali čo najkomplikovanejší spôsob, ako sa dostávajú moje listy do zahraničia a tento hudobník sa im javil ako vhodný kontakt pre tento účel. Nakoniec zrejme zistili, že sa jedná o falošnú stopu a vec nechali tak. Viac sa v mojom spise k tomuto „prameňu“ nevrátili.  
   
 Nuž a tie osoby z KZ, teda z kapitalistického zahraničia, ktorých dopisy zachytávali, boli moji spolužiaci a známi, celkom normálni ľudia a mohli byť predmetom kontrarozviedneho záujmu len strešteným eštebákom, ktorí, žiaľ, dozerali nad každodenným životom všetkých občanov Československej socialistickej republiky a predovšetkým žiarlivo strážili, aby neboli ovplyvňovaní dekadenciou Západu. Sami eštebáci prišli na to, že s bdelosťou a ostražitosťou preháňajú, lebo ako som zistil z obsahu na začiatku môjho spisu, všetky preklady listov, ktoré si nechali vyhotoviť a vyhodnotiť, skartovali ešte skôr, ako zvyšok spisu uložili do archívu. Neboli pre nich podstatné. Pre mňa áno, aj ľudí, s ktorými som si ich vymieňal, si vysoko vážim. Veľmi by som sa ponížil vo vlastných očiach, keby som sa nechal zmanipulovať k tomu, aby som sa o nich gaunerom z eštébé zmieňoval. Aj keď viem, že týmto mojim zahraničným známym by to bolo jedno. 
   
   
   

