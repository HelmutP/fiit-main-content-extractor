

 
Približne pred rokom, teda presne 4.júla 2006, prišla bibliou predpovedaná apokalypsa. Nešla na to globálne, zastavila sa len u nás na Slovensku. Súdruh prezident v tento deň (aká irónia, že v USA majú v rovnaký dátum svoj "Deň nezávislosti") menoval niečo, čo si hovorí Vláda Slovenskej republiky.*
 

 
*

Prečo otváram túto starú ranu, keď už sme sa všetci vyplakali a zmierili s osudom? Je to súčasť mojej psychoterapie, lekári tvrdia, že by som nemal v sebe držať hnev a musím sa vysporiadať so všetkým, čo ma už dlho žerie.

 

 
Na párty muselo byť fasa, väčšinou neznáme ksichty sa nažrali do sýtosti a zapíjali svoj veľký kšeft. Nie, nemám na mysli vreckové vo forme ministerského platu, preboha veď nebuďme trochári, ale tie skvelé bočné kšefty, ktoré majú na minimálne 4 roky všetci pred sebou. 
 

 
Zo všetkých politických niemandov ma zaujal hlavne jeden - Ján Počiatek. Tohto nagélovaného panáčika odniekiaľ poznám, sakra ale odkiaľ??? Jasnééé, veď to je veľký šéf Telenoru Slovakia! Že minister financií? To si Fico robí už ale naozaj kozy zo všetkých?
 

 
Fakt by som chcel vidieť to rozdávanie funkcií, predstavujem si to nejak takto:
  
Fico: "boha Jano, zostala mi tu ešte jedna funkcia, nikto to nechce" 
Jano: "čo je to zač?" 
Fico: "ááále minister financií, flek po Miklošovi" 
Jano: "čo ja viem, viem niečo málo akurát tak o riadení malej firmy" 
Fico: "Jano neser ma, vieš kto je na päťtisícke?" 
Jano: "Jasné, Štefánik" 
Fico: "No vidíš, bude z teba dobrý minister financií. Kúp si, prosím ťa, jeden normálny oblek, zajtra s tým ideme k starému. Česť práci." 
Jano: "Česť Robko česť."
 

 
Ako prišla k funkcií taká Tomanová, na to je aj moja fantázia krátka...
 

 
No nič, čas plynul a ja som začal o súdruha Počiatka zakopávať na môj vkus príliš často. Cestou do práce vidím pravidelne pred budovou prístavby BBC1 (BBC = Bratislava Business Center - nie British Broadcasting Corporation, sídlo Telenor Slovakia) staré, hnilé "sedmičkové" BMW. Spomenul som si na slová exministra financií Mikloša, že on mal údajne k dispozícií ten najstarši gepel z celej vlády.
 

 
 
 Policajta, ktorý orlím zrakom striehol na Jankovu bezpečnosť, som nechcel budiť bleskom a tak som radšej volil fotenie v pohybe, zbabelo zozadu. 
 

 


 
Zvedavo nakuknem do interiéru BMW, a samozrejme žiadne prekvapenie, "holder" na vysielačku bol na svojom mieste, na podložke pod EČV stálo "ÚOÚČ". Bavorák parkoval štandardne vo vyhradenej požiarnej zóne, tak mi bolo jasné, že to nebude žiadny klaun bez špeciálnych výsad. Bol to klaun s výsadami a policajnou hodnosťou.
 

 
Spočítal som si 1+1 a na konci zložitej rovnice bolo jediné meno - jeho géličenstvo Ján Počiatek. Mozog sa mi v rámci mojich obmedzených možností rozbehol na plné obrátky a napáda mi jediné - prachy, prachy, prachy, ktoré zo mňa tá oranžová lúza ťaha a ťahať bude chcieť. Som predsa podľa ich nahúlených mozgov milionár a mám teda platiť a platiť a platiť. Kurva, tak toto ne. Čo asi tak môže exriaditeľ  (naozaj ex? nepoužijeme radšej slovo "tieňový"?), v pracovnom čase štátneho úradníka A.K.A. ministra, robiť vo svojej ex-firme???
 

 
Ani nejdem uvažovať, či a v čom s príchuťou "štátne", má svoje prsty Telenor Slovakia - nemám dôkazy, nejdem špekulovať... ale boh je mi svedkom, že toto mi straší v hlave dodnes.
 

 
OK, možno išiel Janko na kávičku, veď nechajme ho žiť - aj keď za naše, ale buďme granti. Janík je ale na kofeíne asi silne závislý, svoje ošlehané BMW nechával stáť pred budovou veľmi často. V istom období takmer denne a väčšinu dňa.
 

 
Nuž, čo asi urobí správne nasraný daňový poplatník, keď vidí túto nehoráznu drzosť? Bonzuje... A kto sa vie krajšie špárať v hovne ako bulvár ? Takže, pracne som zháňal kontakt na niekoho konkrétneho v Novom Čase, zohnal som, informoval som.
 

 
Nestalo sa nič, staré BMW sa zmenilo na fungláč Mercedes a Janko chodí "na kávičku" do Telenoru Slovakia aj ďalej. V ÚOÚČ sa z krízového vývoja tiež poučili a vytunovali podložky pod EČV, kde už nie je skratka "ÚOÚČ". Ale tie holdre na vysielačky a kvalitne prepálené policajné ksichty za volantom, všetku dobrú kamufláž pokazili.
 

 
Moje echo pomohlo, ale len výmene starého známeho bavoráku za nový, neznámy medveď. Ďakujem teda Novému Času, že promptne súdruha ministra informuje. Človeku sa hneď zdajú byť veci jasnejšie, keď Róbert Glenda Fico, vypustí z toho svojho ozubeného kanála, že Nový Čas je najobjektívnejšie médium. 
Šéfredaktor by si mal z tváre utrieť exkrementy...
 

 
Mám ešte mnoho veselých príhod, ako som Janka Počiatka a Robka Kaliňáka pravidelne stretával v Steam &amp; Coffee - ich spoločnom podniku. Nič zlé by na tom nebolo, až na ten "pracovný čas" a skupinku ich divných kamošov. Odhliadnuc od faktu, že v Steam &amp; Coffee sú zrejme nejaké špinavé komunistické prachy, varia tam dobre, to sa musí nechať a chodím tam rád. Chuť mi ale vedia dokonale pokaziť štátny úradníci, nahodení v "casual style", ktorí sa  ale vôbec netvária, že ich ešte čaká "úradovanie".
 

 
BTW "casual style"... prečo mám pocit, že Janko Počiatek je v casual nahodený prakticky stále? V obleku som ho snáď ešte ani nestretol.
 

 
Jeden deň bol fakt drsný - Janík si skočil zaúradovať do Telenoru, čo čert nechcel zas som ho stretol, a pár hodín na to mi už kazil náladu v kaviarni pod BBC3.
 

 
Proti osobe súdruha Počiatka nemám prakticky nič, je mi jasné, že do Komunistickej Strany Slovenska, pardón SMERu (ešte stále sa mi to mýli) vstúpil z vypočítavosti a nie z ideologických dôvodov. Ale ako proti ministrovi financií mám proti nemu všetko, hlavne si myslím, že je neschopný a nevie túto krajinu reprezentovať na úrovni ministra.
 

 
Jano, vyser sa na to, rob biznis - na to máš, to ti ide, nerob ale zo seba debila pod hlavičkou SMERu...
 

