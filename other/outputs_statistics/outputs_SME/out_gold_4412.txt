

 Zámerom projektu Gypsy Spirit je verejná prezentácia práce a výsledkov všetkých tých, ktorí prispievajú k zlepšeniu situácie Rómov. Dlhodobým cieľom projektu Gypsy Spirit je prispieť k vytvoreniu pozitívnejšieho obrazu rómskej komunity na Slovensku a podporiť zlepšenie jej celkovej životnej situácie. Ocenenie a prezentácia pozitívnych príkladov môže byť motiváciou aj pre iné subjekty, aby sa angažovali v rámci projektov, ktoré prinesú komplexné, efektívne a dlhodobé riešenie problémov rómskej komunity. Ocenenie Gypsy Spirit je  zameraná na podporu a informovanosť o tých projektoch a aktivitách, ktoré prinášajú skutočné, merateľné výsledky, a tak predstavujú pre  rómskych obyvateľov aj reálnu pomoc. 
 Predsedkyňou poroty kategórie Čin roka ocenenia Gypsy Spirit 2010 je „Cigánska diablica" Silvia Šarköziová. Ďalšími osobnosťami, ktoré sa podieľajú na hodnotení nominácií a výbere víťaza kategórie Čin roka, sú Andrej Bán, Jarmila Balážová, Rena Milgrom a František Godla.  
 „Nie je veľa Rómov, ktorí mali podobne ako ja obrovské šťastie v tom, že vyrastali v slušnej a sporiadanej rodine. Ja som tento dar od Boha dostala a práve vďaka tomu sa dnes môžem profesionálne venovať práci, ktorú najviac milujem, teda hudbe. Preto si myslím, že je veľmi dôležité, aby tým, ku ktorým nebol osud taký milosrdný, bola cesta k splneniu ich sna uľahčená. A projekt Gypsy Spirit túto úlohu zvláda výborne." hovorí Silvia Šarköziová, predsedkyňa poroty kategórie Čin roka ocenenia Gypsy Spirit 2010. 
 Životopisy a fotografie porotcov nájdete na webstránke GypsySpirit.eu v sekcii Komisia.  


