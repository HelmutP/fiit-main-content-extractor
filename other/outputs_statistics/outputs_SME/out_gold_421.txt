

 
	 je to mesto, v ktorom vás ráno budia kohúty ako na dedine a v záhradách sa do výšky tiahnu fazuľové úponky a  po domoch červené ruže, 
	 žijú tu pamiatkári, ktorí majú radi pamiatky a ľudia, ktorí majú radi svoje mesto,  
	 práve tu máme kamarátov, s ktorými môžeme nad pohárom červeného vína prekecať hodiny a hodiny o veciach, ktoré máme spoločné, 
	 len tu po týždni získate kondičku od chodenia hore a dole ulicami a keď sa dostanete nad mesto, môžete si ho prezerať zo všetkých strán ako krásny diamant na dlani.  
 
 
 
 
 
 

 
 
Dolná Ružová - naozaj s ružami... 
 
 
 

 
 
Kalvária, ktorá je jedným zo symbolov Štiavnice. Predjarná, jarná, letná, jesenná aj zimná. Vidno ju doďaleka - z Paradajzu aj zo Sitna... Krásna a od júna 2007 zaradená na zoznam "100 najohrozenejších pamiatok sveta". 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
Klenutá kupola Evanjelickéhom kostola dopĺňa strmý krov strechy gotického kostola svätej Kataríny; medzi nimi štíhla a elegantná Hodinová veža. 
 
 
 
 
 

 
 
Banská klopačka s príjemnou čajovňou vovnútri 
 

 
 
Starý zámok; kedysi kostol, neskôr v časoch tureckých útokov prestavaný na pevnosť 
 
 
 

 
 
Pohľad na Trojičné námestie a na štiavnické strechy zhora, z Vodárenskej ulice a zo svahov Paradajzu. 
 
 
 
 
 

 
 
Trojičné námestie s Trojičným stĺpom 
 
 
 

 
 
Mám rada Štiavnicu, lebo: 
 
 
	 tu nájdete v starom baníckom dome úžasnú škôlku Nezábudka, 
	 práve v Štiavnici objavíte na zemi namiesto zámkovej dlažby krásny červený ryolit  - a trotuár a galander sú tu naživo, nielen vo vtipoch o Náckovi, 
	 Veľká vodárenská nádrž je na jar ostro tyrkysová a a keď sa na nej
	v zime nad 10 metrami vody preháňate na kočuliach, spomínate si, ako
	vám v máji elektrizovala svaly a ako sa vám v nej za teplej augustovej
	noci krásne plávalo, 
	 len tu sa dá za pár dní stihnúť kúpanie
	aspoň v dvadsiatich tajchoch či za pár hodín obehnúť Červená
	studňa, Ottergrund a Klinger, 
	 tu nájdete čarovnú čajovňu  v Klopačke,  autentické Libresso so zatopenou štôlňou pri evanjelickom kostole či umenia milovné Art Café, 
	 Tu nemusím vedieť, stačí vnímať  a  dýchať... 
 
 
 
Pohľady dole i hore Trotuárom 
 
 
 
 
 
 
 
 
 
 
 
Veľká vodárenská nádrž - pod Paradajzom s nádherne sfarbenou tyrkysovou vodou, ktorá vynikne najmä na jar. Domček na hrádzi sa nazýva mních a jazerá sú samozrejme tajchy. 
 
 
 
 
 
Klinger, ktorého prívod vody zo štôlne sa teraz narušil; na snímku medzi lesmi Rozgrund. 
 
 
 
 
 
Mám rada Štiavnicu,  
 
	 pretože tu, keď sa vám prepadne v záhrade kus trávnika a ukáže sa
	šachta, banský úrad vám napíše, že máte vzniknuté banské dielo sanovať
	na vlastné náklady, 
	 keď 
	sa pozriem poza kostol svätej Kataríny na "dvestosedmičku", ktorú sme kedysi šindľovali, môžem si neskromne povedať, že dnes možno  stojí aj vďaka nám, 
 
 
 
Dom č. 207, tzv. Dvestosedmička, príklad citlivej obnovy pamiatky. Dnes je pracoviskom Fakulty architektúry STU, o jeho záchranu sa v začiatkoch výrazne  zaslúžili bratislavskí ochranári.  
 
 
 
 
 
 
 

Mám rada Štiavnicu, lebo či je zaviata alebo zakvitnutá, vždy tu mám čo hľadať - staré nápisy vynárajúce z hlbín času alebo spod omietky, obrazy minulého sveta, klam svetla a  tieňa, lom medzi obyčajným a ozajstným.
 
 

Mám rada Štiavnicu, lebo tu ešte duša mesta nezomrela... 
 
 

 
 
 
 
 
  
 
 

 
 

 
 
 
 
 

 
 
 
 
 
Dvaja amatérski archeológovia na sitnianskom hrade - ideoví pokračovatelia diela Jána Truchlíka.
 
 

 
 
Evanjelický kostol  
 
 

 
 
Renesančný Nový zámok  a jeho rôzne podoby...  
 
 

 
 
  
 
 

Moje smútky... 
 
 
Kedykoľvek sem prídem, hľadám a nachádzam vzťah lásky, ovplyvňovania a objavovania. Možno preto, že sme spolu s inými zanechali stopy v krajine okolo Štiavnice, so smútkom a hnevom pozerám  na veci, čo sa tu dejú: 
 
 
	 na výrub zjazdovky, ktorá vyryla v krajine jazvu, viditeľnú až  z druhej strany Hrona a teraz sa jej tvorcovia hádajú v médiách a na súdoch, 
	 
	 na arogantných investorov, ktorí si myslia, že z pamiatky stačí zachovať fasádu  a že na múroch  dominikánskeho kláštora môžu stavať hotely a garáže ;  
	 
	 na samosprávu, ktorá zabúda počúvať svojich občanov, 
	 na opakujúce sa príbehy o malom meste a veľkej moci...  
 
 
 
 
 

Hellov dom  - aj on sa už ocitol na Štiavnici dobre známom zozname najohrozenejších pamiatok sveta...
 
 
 
 

 
 
Zošroubovaný dom - takto vraj ďalší schátraný dom nazývajú niektorí Śtiavničania...  
 
 

 
 
 
 
 
 
Moja milá Štiavnica, vraciam s k tebe a  odchádzam. A stále mám pocit, že dostávam viac, oveľa viac, ako dávam...  
 
 
 
 
 
 
 
 
 
 

