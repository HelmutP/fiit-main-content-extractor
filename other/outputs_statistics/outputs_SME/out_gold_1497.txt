

 2. Martin Rausch "Pyco"Martin Rausch "Pyco" sa narodil 9. októbra 1977 v Bratislave. Vyštudoval hotelovú akadémiu, pretože ho nelákalo gymnázium a lepšiu školu nenašiel. Tu sa učil tri cudzie jazyky a navyše bolo na nej minimum matematiky a fyziky. Od pätnástich  rokov pracuje vo Fun Radiu, ale byť moderátorom ho nelákalo, spočiatku chcel byť hudobným redaktorom a vysielať hudbu, ktorá mu v rádiu chýba. K moderovaniu ho dotlačila Oľga Záblacká, aj keď to vraj spočiatku nádejne nevyzeralo. Po piatich rokoch z Fun Radia odišiel a o tri roky sa zase vrátil. Na konte má veľa hudobných relácií, pripravuje  medziiným nedeľnú trojhodinovku Music Files. V STV moderoval reláciu Menu a Cestuj s nami - tú pripravoval aj ako scenárista a režisér. Jeho záľubami sú cestovanie a hudba. Je slobodný, má dvoch bratov a sestru. Rád varí a je vegetarián. Prezývku Pyco mu dali rozhlasoví poslucháči. "Ďakujem pekne za informáciu, prekvapilo a potešilo. Akurát tento  týždeň mám viac-menej dovolenku, telefón skoro nezapínam a maily som si pozrel náhodou, takže dnes ani zajtra sa s vami určite stretnúť nemôžem," prišiel do redakcie e-mail od Martina Rauscha-Pyca. Čo sa dá robiť. O tom, že moderátor súťaže SuperStar je druhým najšarmantnejším mužom Slovenska, sme sa s ním teda bavili virtuálne,  vďaka počítačovej sieti. 
 Do poslednej chvíle ste bojovali o hlasy s finalistom súťaže, ktorú moderujete, Tomášom Bezdedom. Prečo asi vyhral? 
 
 Tiež by som mu poslal hlas, keby som bol prispievateľom do ankiet. Ja som skôr príjemne prekvapený svojím umiestnením, než by som sa mal čudovať, že Tomáško vyhral. Je to milý, večne usmiaty chlapec so silnou charizmou, počas niekoľkých týždňov si získal celé Slovensko. 
  

 Dostávali ste hlasy od žien aj mužov, ktorí nie raz ako dôvod vašej šarmantnosti napísali, že nielen dobre vyzeráte, ale ste aj inteligentný. Keby ste boli na ich mieste, podľa čoho by ste si vyberali? 
 
 Sám neviem, ankety sú obyčajne zrkadlom momentálneho názoru spoločnosti, ktorý sa o mesiac môže zmeniť. Osobne sa nerád vyjadrujem k aktuálnym hitom či favoritom v čomkoľvek, pretože o tri týždne si môžem myslieť niečo iné. Ak má niekto pocit, že pôsobím inteligentne, určite to počujem radšej, ako keby som mal o sebe počuť, že pôsobím hlúpo. Pravda bude asi niekde uprostred. 
  

 Ako to, že môžete mať teraz dovolenku? Ako ju trávite? 
 
 Potreboval som vypnúť, takže v rádiu robím len hudobné správy a na týždeň som vynechal normálne vysielanie. Každý máme svoje baterky a ja pracujem sedem dní v týždni. Potreboval som sa vyspať. Voľno mám len štyri dni. 
  

 Ako v tejto chvíli vyzeráte a ako sa prejavuje vaša inteligencia a šarm? 
 
 Momentálne? Zobudil som sa pred polhodinou, zápasím s tradičnými kruhmi pod očami, pretože ich mám na tvári vždy, keď sa dobre vyspím. Ráno na každého štekám ako pes, zle sa mi komunikuje, ledva komponujem vety. Asi preto žijem sám a ráno nezapínam telefón, pokiaľ to nie je nutné. O hodinu bude zo mňa opäť človek. 
  

 Zmenila vás súťaž? 
 
 Vo svojej podstate som ten istý človek, kardinálne zmeny nenastali žiadne. Mám dvadsaťsedem rokov a v rádiu som dvanásť rokov, SuperStar som preto vnímal ako profesionálnu skúsenosť a pracovnú príležitosť. Sláva mi až tak veľmi nechutí, navyše je premenlivá ako počasie, takže zmeny nenastali. 
  

 Chýba vám pocit slobody? 
 
 To je relatívne. Áno, chýba mi sloboda z minulého roka, keď som mohol ísť hocikam a neukazovalo sa na mňa prstom. Na druhej strane však nezalamujem rukami, pretože do SuperStar som išiel s otvorenými očami a triezvo, bol som na to pripravený. 
  

 Myslíte si niečo o ženách a dievčatách, ktoré podliehajú hystérii, alebo naopak, ktoré túto súťaž ignorujú? Môžu tak extrémne reagovať aj muži? 
 
 To by bolo veľmi hlúpe škatuľkovanie. Myslím, že SuperStar sa aj na Slovensku stala spoločenským fenoménom, rozprávame sa o nej stále a všade, aspoň zatiaľ. Rozdeľovať či posudzovať ľudí podľa toho, či ju sledujú, alebo naoko ignorujú, by bolo, ako keby som ženy či ľudí všeobecne posudzoval podľa toho, akú farbu preferujú, či ako reagovali na posledný album môjho obľúbeného interpreta. Žijem v presvedčení, že každý máme právo na svoj názor. 
  

 V akej pozícii sa cítite voči súťažiacim, porotcom a divákom? 
 
 Súťažiaci sú moji kamaráti, mám ich úprimne rád, sme na tej istej lodi. Ak sa dá, snažím sa im odľahčiť celý ten kolotoč, ktorý z nich robí šialene prenasledované mediálne hviezdy. S porotou sme jeden tím a voči divákom mám skôr pocit akéhosi sprievodcu celým tým organizovaným chaosom. 
  

 Nemáte pocit, že vás súťaž manipuluje? Bojujete napríklad o to, čo chcete či nechcete na javisku povedať, alebo ako chcete vyzerať? 
 
 Nie, nebojujem. Priznal som už viackrát, že pri podpise zmluvy som mal mierne obavy, čo všetko budem musieť robiť. Dodnes si však neviem vynachváliť spoluprácu pod hlavičkou Slovenskej televízie. Peter Ňunéz je skvelý, pokojný profesionál a režisér, u ktorého je na prvom mieste dialóg a aj keď sa nezhodneme v spoločnom názore, čo sa deje málokedy, vždy nájdeme kompromis. Čo sa týka výzoru, mám absolútne právo čokoľvek meniť, štylistka Dana Kleinert dokonale pochopila moje hranice v rámci stylingu, takže teraz už občas prídem aj sám s nápadom, ako vyzerať, a baví ma to. Jediné, čo sa mi nepodarilo presadiť, boli veľmi krátke vlasy, ale strihá má Martin Horňanský, takže sa nemám na čo sťažovať. Rád pracujem s profesionálmi. 
  

 Máte pocit, že kým ste nezačali moderovať SuperStar, ukrývali ste v sebe niečo, čo nemalo možnosť prejaviť sa? 
 
 Určite áno. Rád sa hýbem a pracujem s hudbou. Z rádia počuť len hlas, nič viac nevidno. Niektorých kolegov, s ktorými som roky pracoval na svojich rozhlasových programoch, moje účinkovanie v SuperStar nijako neprekvapuje, pretože niečo podobné som robil počas pesničiek v rádiu už celé roky. 
  

 Priviedla vás k moderátorskej práci hudba? 
 
 Len hudba. Ja som ani moderátorom nechcel byť, túžil som byť redaktorom, ktorý posúva ďalej informácie, pochopiteľne, hudobné. Aj keď som začal moderovať, tvrdil som, že som redaktor, ktorý popritom občas moderuje. Tvrdím to o sebe dodnes. 
  

 Odznela už v súťaži pesnička, ku ktorej máte citový vzťah? 
 
 Zatiaľ ešte nie, ale doposiaľ mi bola najbližšia Love of my life od skupiny Queen, ktorú spievala Petra Humeňanská. Bohužiaľ, v ten istý večer vypadla. 
  

 Máte nejaký konkrétny nápad, ktorým sa teraz nemôžete zaoberať, a po skončení SuperStar sa doň chcete hneď pustiť? 
 
 Nápad? Oddýchnuť si a niekam vycestovať, to je nápad, ktorý ma drží od septembra minulého roka, keď som cestoval naposledy. 
  

 Nie je vám ľúto, že keby nebolo súťaže, zrejme by ste teraz neboli druhým najšarmantnejším mužom? 
 
 Ak je toto showbiznis, tak to chápem doslova, show a biznis. Treba sa ukázať a robiť biznis. Keď sa človek objaví v takom veľkom, masovo atraktívnom projekte, čelí mnohým sympatiám aj kritike. Som rád, že to v tejto ankete vypálilo prvým smerom. 
  

 Čo by ste odkázali čitateľom SME, ktorí za vás nehlasovali? 
 
 Neviem. Pekný deň. 
  


 4. Pavol Bruchala: O kom sa hovorí a číta,  dostane sa aj do ankiet Moderátor Pavol Bruchala našu anketu poznal a bol z umiestnenia úprimne polichotený. "Všetkým čitateľkám SME ďakujem. Že tam bolo aj veľa mužov? Samozrejme, ďakujem aj im, ale musím úprimne priznať, že viac oceňujem ženské hlasy. Som rád, že pôsobím na ľudí ako šarmantný človek, pretože vždy  je príjemnejšie, keď niekto pôsobí sympaticky." O tom, že by svoje pôsobenie na ľudí nejako vedome zneužíval, nevie. "Je mi však jasné, že mi to prináša nejaké výhody. Ľudia sú povedzme ku mne ústretovejší, ochotnejší, milší. Snažím sa tiež správať slušne." Koho by nominoval na najšarmantnejšieho  muža on sám? "Teraz prežívame ošiaľ SuperStar, tak budem aktuálny - Tomáša Bezdedu. Nevidel som nevinnejší výraz v mužskej tvári, ako má on. Večne vyzerá, akoby sa pred minútou zobudil a nevie, kde je a čo sa okolo neho robí. Tomáš sa do prvej desiatky šarmantných určite dostane, je to aj môj súkromný tip. Je to celé o  tom, že o kom sa hovorí a číta, dostane sa aj do ankiet. Ale muži sa mi ťažko hodnotia." A čo, keby súťaž mala aj ženskú kategóriu? "Volil by som Mariannu Ďurianovú, pretože je to krásne dievča s milým úsmevom. A určite  aj Emíliu Vášáryovú.Pavel Bruchala sa narodil v roku 1977 v Brezne. Po skončení strednej školy  obchodnej pracoval v cestovnej kancelárii, neskôr rok v Štátnej opere v Banskej Bystrici ako garderobiér. V štúdiu potom pokračoval na banskobystrickej Akadémii umení, odbor herectvo a réžia. Po dvoch rokoch odišiel na VŠMU do Bratislavy, kde vyštudoval herectvo. Štyri roky pracuje ako televízny moderátor. Vo voľnom čase sa venuje hlavne športu a rád chodí do kina. Je  slobodný. 5. Peter Kotuľa: Nikto nevie, ako 
dlho sa ľudia budú o nás zaujímať"Už keď som videl v novinách, že som nominovaný za najšarmantnejšieho muža, bol som veľmi príjemne prekvapený. O mojom šarme sa začalo viac rozprávať, až keď som sa objavil v SuperStar. Tak intenzívne ako teraz som to predtým nepociťoval. Neuvedomoval som si  až tak reakcie žien z môjho okolia. Veľmi si toto ocenenie a miesto vážim. Skutočne som to nečakal. Neviem, či to takto bude aj v budúcnosti. O všetkom rozhodujú len a len ľudia. Teraz sme sa stali známymi, ale nikto nevie, ako dlho sa ľudia budú o nás zaujímať. Dúfam, že ich budem mať čím zaujať aj naďalej." 	(hab)Peter Kotuľa sa  narodil 11. marca 1982 v Bratislave. Od piatich rokov spieva a hrá na klavíri, niekoľko rokov spieval v chlapčenskom zbore. Na gymnáziu účinkoval v garážových kapelách. Študuje manažment a túžba stať sa spevákom ho stále neopúšťa. Zaujíma sa o autá a má rád dobré jedlo. Až päť hodín denne strávi komunikáciou  cez internet.Peter Kotuľa 
 Taký uhrančivý pohľad takých modrých očí sa málokedy vidí. Asi je to kombináciou s výrazným, tmavým obočím. Napriek veľkej príťažlivosti vystupoval vždy prirodzene, škoda, že mu tie 70. roky nevyšli, dobre sa naňho pozeralo. 
 6.Miroslav Šatan: Na SuperStar  
nežiarlime"Chvalabohu aspoň  nemusím zase zoširoka rozoberať, čo pre mňa znamená a čo sám robím pre šarm," zasmial sa hokejista Miroslav Šatan, keď sa dozvedel, že z vlaňajšieho druhého miesta sa zosunul o niečo nižšie. 
 "Ale vážne, veľmi si vážim každý čitateľský hlas. V minulých rokoch som mal trochu výčitky, že  väčšinu sezóny som hral v zámorí, a ľudia mi aj tak dali toľko hlasov. Nástup spevákov z televíznej relácie SuperStar beriem, nežiarlim. Ľudia sú radi na mediálnom výslní, v televízii. Viem, čo fenomén realít znamená v Amerike. Škoda, že niektoré sú tam za hranicou vkusu. Dúfam, že formáty, ako je Feel limit, sa k nám  nedostanú. Tam už ide o týranie ľudí a aj zvrátenosti." 
 Jeho manželka Ingrid vyštudovala v New Yorku spev. "Nie som smutná, že sa pred Mira dostali SuperStar. Všetkým fandím. Je to predsa silná vec. Spievajú pekne a hlavne sa všetci pritom tešia, pozitívnych emócií nikdy nie je dosť." 	(ju) 
 Miroslav Šatan - kapitán slovenskej hokejovej reprezentácie  sa narodil sa 22. októbra 1974 v Topoľčanoch. Má dvoch súrodencov - Emila a Petra. Hokejovo vyrastal v Topoľčanoch a Trenčíne. Do Ameriky odišiel v roku 1994. V zámorskej NHL odohral 744 zápasov v tímoch Edmontonu a Buffala. Slovensko doviedol k trom medailovým úspechom na majstrovstvách sveta (2000 - striebro, 2002 - zlato, 2003 - bronz). 18. júla 2004 sa oženil sa s  Ingrid Smolinskou. Medzi jeho záľuby patrí golf, šach, knihy.7. Miro Jaroš: Na vzhľade 
či pekných šatách nezáleží "Ja? A jeden z najšarmantnejších mužov na Slovensku? To som teda prekvapený," reagoval Miro Jaroš na výsledky našej ankety. Nevie, čo robí muža šarmantným. "Možno to, že je sám sebou? Že dodrží slovo?  Že sa dokáže slušne správať k žene? Nemyslím si, že je dôležitá vonkajšia krása. Azda to, aký má človek charakter a srdce, ako sa správa k iným. Sú ľudia, pri ktorých sa cítim dobre a dokážem z ich očí vyčítať úprimnosť. To sa mi ráta. V tom je pre mňa istý šarm. Na vzhľade  či pekných šatách nezáleží. Mne pripadá šarmantná aj moja mama v kuchárskej zástere, keď pečie na Vianoce medovníky."	(hab)Miro Jaroš (14. marca 1978 v Tepličke nad Váhom) - kvôli SuperStar sa vrátil z Izraela, kde si zarábal, aby mal prostriedky na kariéru speváka, čo je jeho sen od detstva. Hrá na gitare, rozpráva po anglicky a  hebrejsky, zaujíma sa o módu. Učí angličtinu. Späť do zahraničia sa nechystá, počas súťaže si na Slovensku zvykol. V najbližšom čase bude pracovať na vydaní albumu s vlastnými skladbami.Miro Jaroš - úžasne  
charizmatický, inteligentný, cieľavedomý, talentovaný. Dávam hlas Mirovi Jarošovi - jeho šarm je v skromnom  vystupovaní, trochu neistom pohľade a milom úsmeve. A keď sa rozospieva,  
ale najmä roztancuje, vyrovná sa hocijakej hviezde. 

