

 Hneď na začiatku musím spomenúť, že niektoré grafy a tabuľky nie sú na 100% presné pretože tabuľka kandidátov nemá zjednotený systém povolaní a ďalších popisov. Preto napr. nie sú v mojom prípade zohľadnené počty kandidátov, ktorí majú uvedených viac titulov a pod. Dokonalú štatistiku určite veľmi rád urobí štatistický úrad, ktorý to má v náplni práce. 
 Počty podľa obcí uvádzam len pre prvých pätnásť najpočetnejších. Pridal som k tomu aj rozdelenie podľa politických strán (hlavička stĺpcov 1-18).   
 Počty podľa vzdelania v jednotlivých stranách taktiež uvádzam len pre prvých 15 najpočetnejších.  
 Prvých pätnásť najpočetnejších podľa titulu a obce je v nasledujúcej tabuľke. Zobrazuje len obce a celkový počet kandidátov s aspoň jedným titulom.   
 Inžinierov, ktorí majú aj iné vzdelanie je 76, takže spolu ich je 771. V tomto prípade sa tiež vyskytujú v zdrojovej tabuľke nepresnosti, lebo napr. jeden kandidát má uvedený titul Ing dvakrát (samozrejme mohol skončiť dve školy, ale myslím, že rovnaký titul sa uvádza len jedenkrát). Doktorov (JUDr., MUDr., ...) je 358. V tomto prípade spresnenie výsledkov podľa konkrétneho vzdelania by bolo veľmi prácne, a preto ho ďalej neuvádzam. 
 Pomer vysokoškolsky vzdelaných v jednotlivých stranách je v stĺpcovom grafe. Vychádza z toho, že viac ako 100 vysokoškolsky vzdelaných kandidátov majú tieto strany 17, 13, 15, 5, 8, 18, 6, 10 a 7. Strana č. 4 nemá žiadneho.   
 Podľa veku v intervaloch po 10 rokov sú kandidáti členení ako je zobrazené v koláčovom grafe bez ohľadu na politickú stranu.   
 Podľa jednotlivých strán (hlavičky riadkov 1-18) a veku sú počty v tejto tabuľke.  
 Grafické znázornenie pre parlamentné strany a tie s preferenciami viac ako 5% sú znázornené graficky takto:   
 Podľa povolania sú výsledky znovu skreslené kvôli nejednotnému systému vpisovania názvov povolaní. Uvádzam prvých štyridsať najpočetnejších povolaní.  
 Po vyfiltrovaní nepresných názvov je počet podnikateľov a živnostníkov 314, riaditeľov a riadiacich pracovníkov je 137, poslancov je 110, manažérov je 106, robotníkov 62, lekárov 61, dôchodcov 61, učiteľov a pedagógov 51+42, študentov 50, konateľov 48, invalidných dôchodcov je 11, ministrov a nezamestnaných je zhodne po 8 a napr. vyslúžilý dôchodca je jeden a pod. 
 Snáď Vám tento článok priniesol aspoň základný prehľad o kandidátoch v číslach aj keď je jasné, že volebný program je dôležitejší. Dozvedel som sa čo som potreboval a zistil som aj to, že kandiduje jeden známy, ale za stranu, ktorú voliť nebudem. Tiež som si vyskúšal novo inštalovaný Office 2010 RTM na reálnych dátach, či všetko funguje ako má. :-) 

