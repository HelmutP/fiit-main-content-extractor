
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Baláž
                                        &gt;
                Všeobecne
                     
                 Levanduľa a história 

        
            
                                    28.6.2010
            o
            8:12
                        (upravené
                28.6.2010
                o
                1:45)
                        |
            Karma článku:
                5.92
            |
            Prečítané 
            1222-krát
                    
         
     
         
             

                 
                    Aromatická a liečivá rastlina známa pre svoju nezameniteľnú sviežu a nasládlu vôňu. Dnes sa používa v parfémoch, aromaterapii alebo pri poruche spánku, strese a depresii. Niekoho asi prekvapí, že v niektorých krajinách sa používa aj na varenie. Napríklad známe provensálske korenie obsahuje levanduľu.
                 

                 Antika  Egypt  V Egypte našla využitie hlavne pri balzamovaní mŕtvych, v kozmetike a výrobe parfémov. Keď archeológvia otvorili Tutanchámovu hrobku, v hlinených nádobách našli balzamovaciu masť s obsahom levandule. Tieto masti a vonné oleje používala len najvyššia vrstva - kráľovská rodina a cirkevní hodnostári.  Grécko  Grékov zaučili do tajomv parfémov a aromaterapie Egypťania. Grécky lekár Theophrastus (3. st. pred n.l.) písal o liečivých vlastnostiach aromaterapie, vo svojej knihe o vôňach. Narozdiel od Egypťanov, ktorí si natierali hlavu, grécky filozof Diogenes si radšej natieral nohy a hovoríl "Keď si natriete hlavu, vôňa letí preč a jedine vtáky majú z nej prospech, ale ak si natriete chodidlá, vôňa zahalí moje telo a vďačne stúpa až k nosu. Anakreon odporúčal natierať hruď, lebo v nej sídli srdce.   Antický Rím  Boli to Rímania, ktorí ako jedny z prvých objavili liečivé účinky levandule. Používali ju ako antiseptikum, ako aj repelent, či pri praní. Liečili ňou bolesti hlavy, stavy úzkosti, depresiu, kožné problémy, ťažkosti s trávením.  Prvá písomná zmienka o využití v liečiteľstve pochádza od gréckeho vojenského lekára Dioscoridesa (77 rokov pred Kristom). Počas služby pre rímskeho cisára Nera, zbieral liečivé byliny z celého stredozemia. Svoje poznatky o bylinách a ich liečivom využití napísal v päť zväzkovom diele "De Materia Medica". O levanduli píše "ak sa užije perorálne, uľavuje od bolesti hlavy alebo krku". Pri povrchovom užití sa môže aplikovať na popáleniny alebo čistenie otvorených rán.   Rímski vojaci si nosili pri vojnových ťaženiach levanduľu a liečili si ňou poranenia z bojov. Často sa rozsypávala po zemi, aby osviežila vzduch. Používala sa pri vydymení miestností chorých alebo v kadidle pri náboženských obradoch.   V starom Ríme bola vzácnou komoditou. Pol kila levandulového kvetu sa predavávalo za 100 denárov, čo bola výška mesačnej mzdy robotníka. Súčasťou života vtedajšieho Ríma boli verejné kúpele, kde ju používali vo vonnych olejoch a liečivých mastiach. Tiež služila k rozvoňaniu kúpeľovej vody.  Rímania, ktorí sa márnotratne voňali, používali aromatické oleje na navonanie vlasov, tela, oblečenia, kúpeľní a dokonca aj vojenských vlajok, či postelí a múrov domov. Ženy ju vešali vedľa postelí, aby podnecovali vášeň.   Stredovek  V dobe ranného stredoveku nastal všeobecný útlm vo využívaní levandule.Hlavný záujem o ňu sa presunul na mníchov. Pestovala sa hlavne v kláštorných záhradách. Mnísi prepisovali staré rukopisy a zaznamenávali liečivé účinky rôznych rastlín.  Levanduľa zažila znovuzrodenie až v dobe Tudorovcov, keď Juraj VIII. rozpustil kláštory a pestovanie levandule sa presunulo do domácich záhrad. Bolo zvýkom natierať nábytok zmesou včelieho vosku a levandule. Tradične sa pestovala v blízkosti práčovní a prádlo sa nechávalo sušiť na levandulových kríkoch. Taktiež sa používala ako repelent proti hmyzu.   Anglická kráľovna Alžbeta milovala levanduľu a varila si z nej čaj proti migréne. Používala aj levanduľový parfém.  (V Anglicku sa levanduľa používa ako jeden z najstarších parfémov vôbec.) O najvačší rozvoj levanduľových fariem sa zaslúžila práve kráľovna Alžbeta. Manželka Karola I. Henrietta Mária priniesla kozmetiku na anglický dvor, pridávala levanduľu do mydiel a vody na umývanie a kúpanie.   Francúzsky kráľ Karol VI. mal vankúš na sedenie naplnený levanduľou.  V 12. storočí nemecká mníška, lekárka a botanička Hildegarda z Bingenu zaznamenala pozitívne účinky levandule pri liečbe vší a bĺch.  Renesancia   V 16. storočí vo Francúzku bola považovaná ako účinná a spolahlivá ochrana proti infekcií.  Povráva sa, že rukavičkári napúšťali levandulovým parfémom svoje výrobky ako prevenciu proti cholere.   Viktoriánska éra  Kráľovná Viktória bola veľka oblúbenkyňa levandule. V tej dobe bola velmi v móde aj medzi dvornými dámami. Čerstvá levanduľa sa nechala vysušiť a potom sa ňou plnili mušelínové vrecúška, ktoré sa dávali do skríň, používala sa aj na umývanie stien miestností a nábytku. Slúžila ako repelent, pri liečbe vší, pridávala sa do mydiel a celkovo bola domácim všeliekom.  Prehnané používanie spôsobilo pokles jej popularity v začiatku 20. storočia, keď bola spájaná so starými dámami. Počas viktorianskej doby bolo predmestie Londýna Mitcham centrom výroby levandulového oleja.  Moderná doba  René Gattefosse, jeden zo zakladateľov modernej arómaterapie sa presvedčil o jej  liečívých a antiseptických vlastnostiach, keď si popálil ruku pri práci v laboratóriu. Natrel si popálené miesto levanduľovým olejom a čoskoro bolesť ustala, koža sa rýchlo zahojila a ostala bez jazvy. Kvôli nedostatku antiseptík počas 1. sv. vojny sa napúšťalo oblečenie levandulovým olejom.  Dnes má bohaté využitie v rôznych oblastiach v medicíne, parfumérií, kozmetike alebo gastronómií. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Baláž 
                                        
                                            Nový blog Violette's Garden
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Baláž 
                                        
                                            Levanduľa - druhy a pestovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Baláž 
                                        
                                            Levanduľa v jedle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Baláž 
                                        
                                            Levanduľovo-zázvorová limonáda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Baláž 
                                        
                                            Levanduľovo-medový dressing
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Baláž
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Baláž
            
         
        marekbalaz.blog.sme.sk (rss)
         
                                     
     
        Som vášnivý pestovateľ, nadšenec a experimentátor, ktorý prepadol vôni levandule.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5843
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Všeobecne
                        
                     
                                     
                        
                            Recepty
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Poľské potraviny a slovenská blbosť
                     
                                                         
                       Rudo „Koro“ Ulehla - potulný gitarista
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




