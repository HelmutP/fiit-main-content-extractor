
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Dostál
                                        &gt;
                Politika
                     
                 Fašistickí smradi a slizký Kaliňák 

        
            
                                    24.5.2010
            o
            14:31
                        (upravené
                24.5.2010
                o
                15:08)
                        |
            Karma článku:
                20.96
            |
            Prečítané 
            14287-krát
                    
         
     
         
             

                 
                    Slovné spojenie "slizký ako had" dostáva po včerajších vyjadreniach ministra vnútra Kaliňáka o skvelom výkone polície počas sobotňajšieho dúhového zhromaždenia silnú konkurenciu. Nemám už žiadnu pochybnosť o tom, že keby sa mladý Robert Kaliňák ocitol spolu s Harrym Potterom v čarodejníckej škole v Rockforte, triediaci klobúk by ho bez najmenšieho zaváhania zveril do láskavej starostlivosti profesora Snapa na fakulte nazvanej podľa jej zakladateľa Slizolin.
                 

                 
Všetko je v najlepšom poriadku. Ako vždy.Zdroj: TV Markíza
   Na dúhové pochody, registrované partnerstvá, dokonca aj na samotnú homosexualitu môžeme mať rôzne názory, ale sobotňajšie vyčíňanie fašistických smradov si bez ohľadu na taký alebo onaký postoj k týmto témam zaslúži iba jediné - jednoznačné odsúdenie a hlboké opovrhnutie. Vulgárne urážky a fyzické násilie do civilizovanej spoločnosti nepatria.   Fašistickí smradi a sloboda   Fašistickí smradi sú najväčšími bojovníkmi za slobodu prejavu, keď im chce niekto brániť v šírení ich rasistickej nenávisti. Homosexuálnym aktivistom rovnaké právo na slobodu prejavu upierajú a pokúsili sa im v sobotu zabrániť v prezentovaní vlastného názoru nadávkami, pľuvancami, päsťami, vajíčkami, kameňmi a slzným plynom. Fašistickí smradi sa tvária ako veľkí obhajcovia slobody zhromažďovania, keď ide o ich čierne pochody k Tisovmu hrobu. Dúhovému pochodu však v sobotu zabránili slovným a fyzickým násilím, vyhrážkami a zastrašovaním. Treba si na to spomenúť, keď sa raz zasa fašistickí smradi budú tváriť ako najväčší demokrati dovolávajúci sa svojich ústavných práv.   V sobotu sa opäť ukázala podobnosť aj rozdiely medzi ožransko-zlodejským a fašistickým prúdom slovenského nacionalizmu. Po obdive k Tisovi a protirómskom rasizme objavili ďalší styčný bod - vulgárne prejavovanú nenávisť voči homosexuálom. Avšak kým Ján Slota bol zrejme aj túto sobotu natoľko spoločensky unavený, že sa nezmohol na viac ako na slovné vyhrážanie sa na diaľku a symbolické pľuvance na homosexuálov, fašistickí smradi sú mladší a akčnejší, takže priložili ruku (a kapsule so slzným plynom) k dielu a zapľuvali si aj reálne. Dalo by sa povedať, že v tomto konkrétnom prípade si napriek všetkým ich výhradám voči Slotovi zobrali jeho slová k srdcu, akoby to bol ich vodca. Slota zavelil, že treba pľuvať, fašistickí smradi pľuvali.   Spokojný Kali    Robert zo Slizolinu bol, ako obvykle, s prácou svojich chlapcov nadmieru spokojný. Polícia si podľa neho svoju prácu urobila veľmi dobre. Veci iste nie sú čiernobiele. Polícia v zásade ochránila účastníkov dúhového stretnutia, zabránila nejakému masívnejšiemu útoku na nich a nakoniec proti fašistickým smradom pomerne tvrdo zakročila. Podľa informácií z médií však najmä zo začiatku nezabránila rušeniu riadne ohláseného zhromaždenia, viacerým fyzickým útokom zo strany fašistických smradov na účastníkov zhromaždenia, ich prenikaniu na zhromaždenie, hádzaniu vajíčok, či použitiu slzného plynu.   Viacerým z týchto výčinov sa podľa svedectiev policajti spočiatku iba nečinne prizerali. Policajti budú iste tvrdiť, že zasiahli vždy, keď niečo zaregistrovali, ale z vlastnej skúsenosti (zhromaždenie počas návštevy čínskeho prezidenta v júni 2009) viem, že policajti na zhromaždeniach disponujú niekedy až obdivuhodnou schopnosťou prehliadať fyzické útoky, hoci sú na ne napadnutými, či svedkami priamo upozorňovaní a volaní na pomoc. Takže nemám dôvod neveriť tým, ktorí tvrdia, že k podobným situáciám došlo aj na začiatku dúhového zhromaždenia.   Dvojaký meter polície   Dúhový pochod ulicami sa napokon neuskutočnil, lebo polícia vraj nevedela zabezpečiť bezpečnosť ulíc. Stojí za to si pripomenúť, že 14. marca tohto roku bola polícia dostatočne akčná a dôrazná na to, aby dokázala zabezpečiť bezpečnosť pochodu fašistických smradov spred Prezidentského paláca k údajnému Tisovmu hrobu. Aj na Hodžovom námestí vtedy dokázala bez problémov zaručiť bezpečnosť zhromaždenia fašistických smradov. A nás, účastníkov protifašistického zhromaždenia, ktoré sa na tom istom mieste konalo krátko predtým, vytlačili zo stredu námestia do bezpečnej vzdialenosti a cez policajný kordón, ktorý nás oddeľoval, by vtedy neprenikla ani myš. Na rozdiel od skínov, ktorí sa v sobotu cez policajtov spočiatku vedeli dostať pomerne ľahko. Zrejme sú antifašisti políciou vnímaní ako väčšie bezpečnostné riziko než fašisti.   Dvojaký meter polície bije do očí aj v porovnaní so spontánnym zhromaždením kritikov premiéra Fica, ktorí sa zišli necelý týždeň pred dúhovým zhromaždením pred budovou Slovenskej televízie. V čase, keď tam bol opäť raz sám so sebou "diskutovať" Robert Fico. Zástupca polície ich vyzýval, aby spred budovy STV odišli, argumentoval tým, že zhromaždenie nebolo ohlásené na miestny úrad a vyhrážal sa im, že v opačnom prípade budú predvedení na policajnú stanicu. Hoci existuje povinnosť zhromaždenie ohlásiť, ani neohlásené zhromaždenie nemožno rozpustiť len tak, ale iba v prípade, že existujú dôvody, pre ktoré by bolo možné zhromaždenie zakázať. Také dôvody však minulú nedeľu v prípade zhromaždenia Ficových kritikov pred STV neexistovali, polícia sa ich napriek tomu snažila "rozpustiť". Úplne odlišný meter uplatnila  polícia v túto sobotu na fašistických smradov, ktorí rušili dúhové zhromaždenie. Ich neohlásené kontrazhromaždenie polícia nerozpustila, hoci jeho účel evidentne smeroval k výzve "dopúšťať sa násilia a hrubej neslušnosti", čo je jeden zo zákonom vymedzených dôvodov pre zákaz zhromaždenia. Čiže neohlásené zhromaždenie Ficových odporcov polícia v jeden týždeň horlivo rozpúšťa, hoci na to nemá zákonný dôvod, a neohlásené zhromaždenie fašistických smradov o týždeň neskôr nechá dlhú dobu na pokoji, hoci existujú zákonné dôvody na jeho rozpustenie.   Hrubej neslušnosti sa včera dopustil aj minister Kaliňák, keď tvrdil, že políciu si netreba zamieňať s organizačným štábom pochodu homosexuálov a lesbičiek a že si organizátori mali najať súkromnú ochranku. Pre čo iné, ak nie pre zaistenie bezpečnosti, sa potom na políciu skladáme všetci svojimi daňami? Už ani nehovoriac o tom, že mu za dva mesiace úplne vyfučalo z hlavy, že 14. marca polícia bez reptania a veľmi efektívne plnila úlohy organizačného štábu aj súkromnej ochranky zhromaždenia a následného pochodu fašistických smradov. Slizké ako Kaliňák. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (304)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Konečne sa dozvieme mená všetkých členov Smeru
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Niekoľko viet o Táni Kratochvílovej
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Smeráčina á la Brixi: Len si tak niečo vybaviť
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Fico nám berie peniaze a dáva ich svojim kamarátom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Dostál
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Dostál
            
         
        dostal.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som predsedom OKS. V novembrových voľbách kandidujem v bratislavskom Starom Meste do miestneho i mestského zastupiteľstva. Ako jeden z členov
Staromestskej päťky. Viac na www.sm5.sk. 

  

 Ondrej Dostál on Facebook 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    202
                
                
                    Celková karma
                    
                                                15.07
                    
                
                
                    Priemerná čítanosť
                    5758
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Smrteľne vážne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                             
                                            Ivan Kuhn
                                     
                                                                             
                                            Juraj Petrovič
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Michal Novota
                                     
                                                                             
                                            Ondrej Schutz
                                     
                                                                             
                                            Tomáš Krištofóry
                                     
                                                                             
                                            Peter Spáč
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            INLAND
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Konzervatívny inštitút M.R.Štefánika
                                     
                                                                             
                                            Peter Gonda
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 robí Ficovi reklamu
                     
                                                         
                       V TA3 Fica hrýzkajú slabúčko
                     
                                                         
                       O tom, čo platí "štát"
                     
                                                         
                       Konečne sa dozvieme mená všetkých členov Smeru
                     
                                                         
                       My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                     
                                                         
                       Smeráčina á la Brixi: Len si tak niečo vybaviť
                     
                                                         
                       Potemkinov most v Bratislave
                     
                                                         
                       Fico nám berie peniaze a dáva ich svojim kamarátom
                     
                                                         
                       Výsmech občanom v priamom prenose alebo .... Mišíková v akcii ...
                     
                                                         
                       Ficov podrazík na voličoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




