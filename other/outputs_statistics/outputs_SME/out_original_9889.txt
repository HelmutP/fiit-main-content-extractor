
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Šlinská
                                        &gt;
                Nezaradené
                     
                 Festival Easthetic oživil mŕtvy východ 

        
            
                                    30.6.2010
            o
            12:52
                        (upravené
                30.6.2010
                o
                13:33)
                        |
            Karma článku:
                6.43
            |
            Prečítané 
            1295-krát
                    
         
     
         
             

                 
                    Festival Easthetic priniesol kvalitnú hudbu konečne aj na východ.
                 

                 
  
   Východ zaspal. Čo sa týka festivalov určite. Od Košíc smerom na západ môžete navštíviť Pohodu, Žákovic open, Top Fest, Barbakan, Toto Je Hip Hop, po minulé roky aj Hodokvas či Wilsonic a tento rok prvýkrát Grape festival. Ak bývate od Košíc smerom na Ukrajinu a patríte medzi takzvaného náročnejšieho poslucháča, môžete si aj vlasy vytrhať, aj tak okrem Desmodu, Smatanovej a Cmoríka neuvidíte nič.   Tento stav sa rozhodli zmeniť mladí organizátori prvého multižánrového open air festivalu na východe. Easthetic, to bolo slovo, ktoré začiatkom leta skloňovali hudobní nadšenci. Prvý ročník festivalu ponúkal skvelý line up, okrem hudby si návštevníci mohli pozrieť divadlo pre deti aj dospelých, street art, maľbu, literárny stan či výstavu fotografií. Nie málo.                 Počasie organizátorom neprialo. Skvelý priestor, stredisko Lúč na Zemplínskej Šírave, ostal prvý deň poloprázdny. Pritom, ktorý festival sa môže pochváliť menším kopcom, z ktorého vidíte hlavný stage a za ním šíru vodnú plochu? Chcela by som veriť, že hlavne vietor a dážď sa podpísali pod účasť, ktorá nebola bohvieaká. Ale čo čakať v krajine, kde je každý tretí Funrádio.   Hudobný program bol na prvý ročník viac než dokonalý. Zahraniční hostia, francúzske duo Nôze, maďarská kapela Realistic Crew a českí experimentátori WWW odohrali fantastické vystúpenia, pri ktorých zaplesalo srdce nejedného fanúšika dobrej hudby. Výber slovenských kapiel bol rôznorodý. Od rapu a hip hopu (Vec, Zverina, Hafner, Beyuz, Zetuzeta) cez alternatívny rock (Talkshow, The Swan Bride, Lavagance) elektroniku a dnb (The Autumnist a B- Complex) až po miláčikov publika Billy Barman, Chiki-liki-tu-a  a Puding Pani Elvisovej.                    Škoda, že zabudnutý ostal druhý stage- Nu Spirit tanečný stan. Aj ten priniesol výber toho najlepšieho, čo domáca scéna ponúka. Dídžeji  Milosh, Skank, Magálová (všetci z Rádia_FM) hrali pre minimum ľudí, čo považujem spolu so zrušením vystúpenia skupín The Uniques a Home Made Mutant za najväčšie mínus festivalu. Do budúcna to treba lepšie premyslieť. Zaplnený tanečný stan si však užili chlapci z Dezorzovho lútkového divadla, ktorí priniesli na východ svoju najnovšiu hru len pre dospelých Gašparkové šibalstvá alebo erotic dobrodružstvá Bob de Nira. Erotika s bábkami? Áno, páčilo sa a nielen mne.   Vydavateľ Koloman Kertész Bagala, ktorý sa po festivalovom areáli prechádzal vo vysokom klobúku, mal pod palcom Literárny stan. Návštevníci si tam mohli pozrieť besedy so spisovateľmi, diskutovať a kúpiť zaujímavé knihy. Priestor dostali aj mladí umelci (street art &amp; maľba), ktorí sa mohli realizovať pri oboch stejdžoch.   Prvý ročník festivalu je za nami. Organizátori sa nevyhli menším chybám, no chvalabohu sú ochotní prijať kritiku a veci do budúcna zlepšiť. Snáď sa Šírava dočká aj druhého ročníka najlepšieho festivalu na východe, festivalu Easthetic.                    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Diagnóza: SAShE pozitív
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Martina Slováková, talentovaná individualistka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Hip-hop bez pózy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Juraj Kušnierik: Slováci čítajú toľko, ako nikdy predtým
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Rudi Rus: Metal je výnimočné umenie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Šlinská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Šlinská
            
         
        slinska.blog.sme.sk (rss)
         
                        VIP
                             
     
        music is my religion...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2904
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Francisco X. Stork- Marcelo objavuje skutočný svet
                                     
                                                                             
                                            Peter Stamm Približná krajina
                                     
                                                                             
                                            Stefan Beuse- Strieľame gumičkami do hviezd
                                     
                                                                             
                                            Monika Kompaníková- Piata loď
                                     
                                                                             
                                            Zuzana Mojžišová- Bon voyage
                                     
                                                                             
                                            Michaela Rosová- Hlava nehlava
                                     
                                                                             
                                            Chuck Palahniuk- Deník
                                     
                                                                             
                                            Amélie Nothomb- V ohromení a strachu
                                     
                                                                             
                                            Michael Hornburg- Břečka
                                     
                                                                             
                                            Boris Vian-Pěna dní
                                     
                                                                             
                                            Paolo Giordano- Osamělost prvočísel
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Thelma &amp; Clyde
                                     
                                                                             
                                            James Blake
                                     
                                                                             
                                            stateless
                                     
                                                                             
                                            zero 7
                                     
                                                                             
                                            koop
                                     
                                                                             
                                            tori amos
                                     
                                                                             
                                            lisa papineau
                                     
                                                                             
                                            dan bárta
                                     
                                                                             
                                            cat power
                                     
                                                                             
                                            bat fot lashes
                                     
                                                                             
                                            st germain
                                     
                                                                             
                                            feist
                                     
                                                                             
                                            ane brun
                                     
                                                                             
                                            lisa papineau
                                     
                                                                             
                                            tori amos
                                     
                                                                             
                                            radiohead
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Simča
                                     
                                                                             
                                            Baška
                                     
                                                                             
                                            Saša
                                     
                                                                             
                                            Samo
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Alenka
                                     
                                                                             
                                            Tomáš
                                     
                                                                             
                                            Daniela
                                     
                                                                             
                                            Anka
                                     
                                                                             
                                            Soňa
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            lemon
                                     
                                                                             
                                            sashe
                                     
                                                                             
                                            eFeMko
                                     
                                                                             
                                            voices
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




