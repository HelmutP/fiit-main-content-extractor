

 
 FOTO SME - PAVOL MAJER 
 
   
 

 V mesiacoch júl a august zvyknú vysoké školy, ktoré nenaplnili počty plánovaných prijatí, vyhlasovať tzv. druhé, doplňujúce kolo prijímacích pohovorov. Najčastejšie ide o technické vysoké školy či  fakulty prírodovedného zamerania. 
 Vychytené školy a fakulty už spravidla dodatočné prijímacie pohovory nerobia. "Nie je to potrebné. Zákon o vysokých školách totiž umožňuje, že ak z návratok nevzíde toľko prijatých, koľko sa očakávalo, dekan môže posunúť čiaru podľa poradia úspešnosti, čím sa  vyrieši ďalšie prijímanie," povedala SME Mária Holická zo študijného oddelenia Univerzity Komenského. Informovala, že žiadna z fakúlt UK nebude robiť dodatočné prijímačky. 
 Napriek tomu však väčšina vysokých škôl ešte prijíma študentov - napríklad Žilinská univerzita robí dodatočné pohovory na  väčšine fakúlt. "Samozrejme je možné prijímať tak, že prijmeme uchádzačov, ktorí boli v prvom kole  tesne pod čiarou. Tak by sme však nedali šancu tým, ktorí si podali len jednu alebo dve prihlášky a neboli prijatí napríklad na exponované študijné programy. Pritom to môžu byť dobrí študenti," povedal prorektor pre  vzdelávanie Michal Pokorný. 
 Študentov ešte bude prijímať napríklad aj Technická univerzita v Košiciach, Univerzita Mateja Bela v Banskej Bystrici či Slovenská poľnohospodárska univerzita v Nitre. 
 Pre ďalšie prijímanie sa rozhodli preto, že počet skutočne zapísaných študentov, je menší než počet tých, ktorí úspešne prešli  prijímačkami a dostali rozhodnutie o prijatí. 
 SOŇA REBROVÁ 
 Vychytené školy a fakulty už spravidla dodatočné prijímacie pohovory nerobia. 
   

