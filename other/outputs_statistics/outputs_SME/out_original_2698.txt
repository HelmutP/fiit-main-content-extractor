
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lettrich
                                        &gt;
                Politika
                     
                 Zlatica osobnosťou TV publicistiky? Nie, ale záskok si našla dobrý. 

        
            
                                    15.3.2010
            o
            9:35
                        |
            Karma článku:
                16.05
            |
            Prečítané 
            5121-krát
                    
         
     
         
             

                 
                    Zlatica Puškárová, hoci sa od jesene na obrazovkách neobjavuje, získala počas víkendu ocenenie Osobnosť televíznej publicistiky. Ide o ďalší dôkaz, aké to v súčasnosti s televíznou publicistikou na Slovensku zlé.
                 

                 Viac ma už prekvapilo len nazvanie Profesionálov najkomediálnejší seriál roka. Nevidel som síce jediný celý diel, ale aj to, čo som videl by som spolu s upútavkami nazval akurát tak najtrápnejší seriál.  Ale späť k publicistike. Zlatica sa v uplynulom roku zapísala do povedomia verejnosti azda iba hlúpym a zlomyseľným zostrihom počas prezidentského duelu Gašparovič - Radičová. Cieľavedomo kráča tam, kde už dávno dorazil jej niekdajší kolega Daniel Krajcer - stal sa z neho cirkusový politický kabaretiér, ktorého vysielací čas sa neúprosne blíži k polnoci. Apropo, naozaj chce Daniel moderovať politické debaty pred voľbami, keďže sa len pred niekoľkými týždňami rozhodol, že do politiky (priamo) nevstúpi, ale veľmi dlho túto otázku riešil a dnes celé Slovensko vie aj to, ktorí politici mali byť súčasťou jeho partie?  O vysielací čas sa Zlatica zatiaľ báť nemusí, jej relácia dlhoročne spríjemňuje pravé nedeľné poludnie. Rizika skĺznutia do kabaretu však určite. Kto si spomenie napríklad na duel Počiatek - Mikloš, hádam mi dá za pravdu. Puškárová verejne prijala úlohu, ktorá bola mixom časomerača a diváčky v eufórii, akurát že sedela v kresle moderátorky.   Neviem komu, mne však Zlatica Puškárová vôbec nepripadá ako osobnosť televíznej publicistiky. Zato Gábor Grendel, jej moderátorský kolega, vo mne začína evokovať to, čo od moderátora politických debát očakávam (niekedy sa neubránim túžbe, ako rád by som si nejakého politika podal sám, to sa mi však asi nesplní...).   Gábor Grendel pochopil podstatnú vec: debata s politikom v televíznej relácii nie je míting, kde je moderátor štatistom. Práve naopak: moderátor je ten, kto debatu určuje, moderátor nastoľuje otázky a trvá na odpovediach, prideľuje čas... Politik sa musí cítiť ako na neúprosnom výsluchu. V slovenských televíziách, žiaľ, niečo neviditeľné. Ale mám pocit, že práve Gábor Grendel kráča správnym smerom. Ak by som mal vybrať kto bude reláciu Na telo moderovať, výber by bol jednoduchý a jednoznačný.  Skutočnú osobnosť televíznej publicistiky zatiaľ nemáme, ale možno sme objavili jedného adepta. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (68)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Cenzúrou za ochranu  - čoho vlastne? (príbeh denníka Sme)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Príliš falošná Solidarita
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Tak zakázal či nezakázal súd adopcie kresťanom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Prečo denník SME neuverejnil môj príspevok?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Oplatilo sa rozpučiť Orange a začať dýchať O2?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lettrich
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lettrich
            
         
        lettrich.blog.sme.sk (rss)
         
                                     
     
        Človek, kresťan a katolícky kňaz. Nie je mi ľahostajný svet okolo nás, ani hodnoty, ktoré vyznávame. 
  Od roku 2011 na blogu denníka SME nepublikujem. Budem rád, keď navštívite môj blog na tomto odkaze.



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3790
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Viera a náboženstvo
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Blog
                                     
                                                                             
                                            .týždeň
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pred druhým kolom voľby budem stáť za Róbertom Ficom
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Kto tu klame II? alebo Otvorená odpoveď pánovi Lučanovi
                     
                                                         
                       Bránime homosexuálne cítiacich
                     
                                                         
                       Keby tak Fico tušil
                     
                                                         
                       Reakcie LGBTI hnutia na ochranu manželstva v Ústave sú slabé
                     
                                                         
                       O mojom detstve.  (pre premiéra)
                     
                                                         
                       Putovanie po východnom Slovensku 3.: Belianske Tatry (videoblog)
                     
                                                         
                       Diktatúra sexuálnych menšín
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




