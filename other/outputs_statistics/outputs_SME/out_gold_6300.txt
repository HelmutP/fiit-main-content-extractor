

 To asi nedostatok miesta na sedenie, večne zavretá kopírovacia služba, hodiny čakania pred temnými, ktovie či správnymi dverami, príliš ostražitá informátorka, vždy nejaké neordinujúce kancelárie, a možno aj nápis: ŠMYKĽAVÁ PODLAHA - NEBEZPEČENSTVO ÚRAZU, premieňa úbohých čakajúcich na štekajúce hyeny. 
 A ani keď vám príde mdlo, radšej si sadnúť nechoďte, aby ste pri opätovnom zaradení do fronty neboli obvinení z predbiehania. 
 K užitočnej informácii o podlahe skrývajúcej alegóriu, že ak si u nás na úrade zlomíte nohu, je to len vaša chyba, žiadalo by sa vyrobiť novú tabuľku: 
 Nepýtajte sa, neotravujte, neoverujte, a hlavne, preboha - hlavne nikam neklopte! 
   
   
   

