
 Vďaka pápežovi Jánovi Pavlovi II. sú tajomstvá ruženca úplné. Mne osobne vždy chýbali tie, ktoré by niečo hovorili o Ježišovom pôsobení. Boli iba tajomstvá radosti o tom, ako prišiel Syn Boží na svet a jeho život do dvanástich rokov. Potom  podobne ako v biblii bola odmlka. Tá sa však v ruženci na rozdiel od biblie jeho verejným vystúpením neskončila. Odmlka pokračovala až do chvíle, keď začal trpieť. Boli tajomstvá utrpenia a končilo sa tajomstvami zmŕtvychvstania. Teraz je to už kompletné, aj s Ježišovým verejným životom. Lebo ruženec je vlastne modlitba, ktorá rozjíma nad Bibliou.

Ak prebieha mesiac ruženca u nás? Od začiatku mesiaca sme si rozdelili tajomstvá tak, aby sme si dvadsať tajomstiev pripomenuli na dvadsiatich svätých omšiach. Kázeň je vždy zameraná na jedno tajomstvo, ktoré vysvetlíme a hneď po vysvetlení sa všetci spoločne pomodlíme desiatok ruženca, ktorý pozostáva z modlitby Otče náš a z desiatich modlitieb Zdravas Mária. Desať Zdravasov však nerecitujeme, ale spievame. Takýto spôsob sa nám všetkým páči viac. Na konci je modlitba Sláva Otcu a modlitba Ó Ježišu, v ktorej prosíme o odpustenie hriechov o ochranu pred pekelným ohňom a v ktorej je aj prosba o duše, ktoré najviac potrebujú Božie Milosrdenstvo.

Celé sa to končí tým, že na nástenku pripevníme obrázok, ktorý symbolizuje jednotlivé tajomstvo. Tajomstvá máme rozdelené do štyroch skupín a vždy sa modlíme za jeden kontinent. Vždy si spomenieme aj na tých, ktorí nám pomáhajú a modlitbou im vyprosíme pomoc od Boha.

Okrem toho každý večer sa pravidelne modlievame desiatok ruženca, kde vždy pred modlibou pripomenieme, za koho sa modlíme. Modlíme sa aj za Vás, za všetkých, ktorí nám fandíte, myslíte na nás a pomáhate nám.

V noci pred spaním - okolo polnoci - sa zvyknem ešte poprechádzať po Luniku s ružencom v ruke. Modlím sa vtedy za celý svet aj za Vás. Neverili by ste, aký tu je vtedy pokoj... 
