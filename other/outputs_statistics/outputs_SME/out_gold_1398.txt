

 Naša cesta začína ako vždy v Čope. Je niečo po polnoci a staničný personál hrá presilovku. Vďaka redukcii nočných spojov sme jediným spestrením nočnej pasovákov, colníkov, pokladníčok a mužov s kumulovanou funkciou taxikár - vekslák. Po zrušení zmenárne vo vestibule stanice to so službou 24/7 zabalila aj neďaleká pobočka banky a tak sme nútení podporiť tieňovú ekonomiku. Skúšobne meníme 50 eur, za čo dostaneme 400 hrivien. Absolútne netušíme, ako  sa od našej poslednej návštevy vyvinul kurz, ale že sme nenakúpili najvýhodnejšie zistíme, keď za ďalšiu päťdesiatku dostaneme o 50 hrivien viac.  Neskôr si všimneme, že oficiálne je 1 € cca 10,4 hrivien. 

 Jakub ide s vekslákom k bankomatu, lebo ten nenosí so sebou vyššie sumy.  
"Maximálne za 100 eur", hovorí, "inak..." - prejde  si prstom po krku a toto gesto ma zbaví akejkoľvek chuti na nočnú prechádzku mestom. Kupujeme lístky, nasadáme do kupé moskovského vlaku a zložíme sa spať.   



 Ráno si objednáme čaj a v Ľvove zbehneme do bufetu kúpiť niečo pod zub, pretože v zmysle príslovia "čo môžeš urobiť dnes, neodkladaj na zajtra" sme dnešné raňajky zjedli včera medzi Čiernou a Čopom. 

 Zoznamujeme sa so spolucestujúcimi. Mama s maličkou Nikou cestuje do Moskvy. Boli u jej rodičov v dedinke Tjačevskej oblasti. Hovorím, že to tam trochu poznám, lebo moja korešpondenčná kamarátka Natália z ruskej Jakšangy má mamu v susednej dedine, vo Vinilove. Nika je vytúžené dieťa, na ktoré čakali 16 rokov. Pretože pani pred pôrodom nepracovala, dostáva len 100 rubľov, teda asi 3 doláre ako príspevok na dieťa. Ale nesťažuje sa, manžel zarába dosť na to, aby im nič nechýbalo. 
 



 Na poludnie sa rozlúčime a v Ternopile vystupujeme. Do príchodu ďalšieho vlaku máme dve hodiny času a tak si urobíme malú prehliadku mesta a naobedujeme sa. 

 Mesto vzniklo v 16. storočí a striedavo patrilo Rakúsko-Uhorsku, Poľsku, Rusku a Ukrajine. Aj výzorom je viac podobné naším mestám, ľudia sa ničím nelíšia od našich a malí školáčikovia idú zo školy s mobilmi pri uchu tak, ako u nás. Je tu divadlo, pešia zóna, kostoly, parky, aj oddychová zóna pri jazere. 









 Na malom trhu predávajú knihy. Viete, ktorá začína vetou "Otže, našogo Ferdynanda j ubyly"? 






 Po prehliadke mesta nasadáme do môjho obľúbeného plackartného a smerujeme rozoranými celinami ďalej. Cestu si krátim čítaním  Itineraria - cestovného denníka spred 300 rokov, kedy sa jeho autor Daniel Krman ako vyslanec  evanjelickej cirkvi vybral za švédskym kráľom Karolom XII., aby od neho vymámil morálnu a finančnú pomoc pre bratov vo viere. Jeho cesta viedla zo Žiliny na Poltavu, rovnako ako naša o tristo rokov neskôr. 

 To je však asi jediné, čo máme spoločné. Na rozdiel odo mňa bol Krman nafúkaný ješitný chlap plný predsudkov ("...Haďač, ktorý nám bol odporný už samým názvom."  str. 91) presvedčený o svojej dokonalosti (a len vrodená skromnosť mi bráni napísať, že cestopisy píšem lepšie a som krajšia.) 
Tento hlbokoveriaci Bohu oddaný človek lásku k blížnemu svojmu prejavoval na každom kroku: 

- "Mesto Sacz sa hmýri židmi ako plesnivý syr červami" (str. 13)  
- "Kalmyci, táto zberba ľudožrútska" (str. 43) 
- "Všetci rusínski kňazi sú veľmi nevzdelaní a hlúpi" (str. 83) a pod. 

Z Desatora mu asi nedopatrením vypadlo piate a siedme prikázanie, pretože sa dosť laxne staval k vraždeniu a okrádaniu miestneho obyvateľstva svojimi spoločníkmi. 
Ale dosť už o ňom, naše cesty sa aj tak na chvíľu rozdelili, pretože on cestoval do Poltavy územím súčasného  Poľska, Bieloruska a pobaltských krajín. 

 Vláčik prechádza rovinatou krajinou. Občas zastaví na nejakej stanici, kde nás čakajú miestne ženičky s  krátkou prezentáciou svojho tovaru. V Chmelnyckom ponúkali hlavne oblečenie.  



 Moja obľúbená Žmerynka ostala verná svojim tradíciam nielen v oblasti stravovania. 





 Večer si uvaríme polievku a uložíme sa spať. Pretože máme miesta na konci vagóna, každú chvíľu ma budia cestujúci, ktorí cestou na WC zavadia o moje vystrčené nohy alebo tresknú dvermi. Po celú noc ľudia nastupujú i vystupujú, niektorí si svoje lôžko užijú len pár hodín a ráno zisťujem, že sa osadenstvo značne obmenilo. Starší pán v okuliaroch si na stolíku rozložil obaly s faktúrami a ja sa cítim, ako keď prídem k mame na návštevu a ona rýchlo odkladá podobné papiere zo stola, aby som jej ich nepoprehadzovala. 



 Vlakom prejde žena s taškou, z ktorej postupne vyťahuje pískajúcich a hovoriacich papagájov, farebné svietiace lampy, chodiace bábiky a spievajúcu pseudobárbinu. Aby som ju neurazila, prejavím opatrný záujem o čokoládu a správna obchodníčka mi predá za 30 hrivien rôznych dobošiek, ktoré som vlastne ani nechcela, takže mám batožinu o pol kila ťažšiu. 

 Po desiatej vystupujeme na stanici v Charkove.  



 Ubytovanie nehľadáme dlho, hneď na stanici je hotel Ekspres. No, hotel ako hotel, teda nič luxusné už na prvý pohľad, ale poloha dobrá a cena - 220 hrivien za dvoch (22 eur) -  prijateľná. 

 "Tá bola pionierkou ešte za Brežneva", komentuje Jakub výzor pracovníčky recepcie, a hoci to od neho nie je zdvorilé, rozosmeje ma to. Pani je milá a imidžovo do hotela zapadá. Hlavne, že sa môžeme hneď ubytovať. 





 Je deviaty máj a v telke beží na piatich kanáloch priamy prenos z celoštátnych osláv Dňa peremogy - Dňa víťazstva na Majdane nezaležnosti v Kijeve.  Túto fotku venujem Jankovi Urdovi, práve spievajú Frantu Váňu. 



 Rýchlo si vybavíme lístky na ďalšiu cestu, posilníme sa blinami a mierime do metra, aby sme uvideli čo najviac. 
Eskalátor v metre sa pohybuje veľmi rýchlo, vďaka čomu hneď na prvý pokus spadnem, ale stihnem sa postaviť ešte skôr, ako dofrčíme úplne dole. Lístok stojí 1,5 hrivny (15 centov). 



 Kým sme sa dostali na námestie Víťazstva, dážď rozohnal vojenskú prehliadku a tak vidíme už len jej odchádzajúce torzo. 



 Ľudia sa pred dážďom schovávajú do metra. Mnohí majú pripnuté oranžovočierne stužky ako dôkaz nezabúdania na hrôzy vojny. Tieto veteránky si dlhý čas krátia spevom. 



 "Rasscvetali jabloni i gruši, poplyli tumany nad rekoj... 



 ... vychodila na bereg Kaťuša, na vysokij bereg na krutoj." 

 Keď prejde najväčší lejak, zájdeme si na čaj do kaviarne. Pri vedľajšom stole sa chce usadiť vojnový veterán. Prichádza k nemu muž stredného veku. 
"Deduško, neseďte tu sám, poďte k môjmu stolu." 
"Nie, ďakujem, už som sa dnes stretol so starými priateľmi, teraz si chcem len oddýchnuť. Rád by som si zatancoval, ale nemám s kým", žartuje. 
"Počkajte chvíľu, nájdem vám tanečnicu", odchádza muž. 
Medzitým dôjdu štyria tínedžeri. 
"Deduško, poďte si sadnúť k nám", prosia veterána. Ten im hovorí to isté, čo pred chvíľou mužovi. Chlapci sklamane odchádzajú.  
Hneď na to k nemu prichádza mladý muž s malým synom. Podáva veteránovi ruku a synovi vysvetľuje, že deduško bojoval za slobodu. Rozlúčia sa a už je tu prvý muž aj s tanečnicou a zakrátko na to má deduško na improvizovanom tanečnom parkete sólo.
 


 Aj keď som si pri svojich návštevách Ukrajiny aj Ruska všimla nápisy "Veteráni Veľkej vlasteneckej vojny sa obsluhujú prednostne", osobitné pokladne len pre nich na každej stanici i bezplatné vstupy na kultúrne akcie, dosť ma dojalo, že úctu k veteránom prejavujú ľudia aj takto spontánne. Snáď to nebolo len kvôli tomu, že je Deň víťazstva. 



 Urobíme si malú prehliadku. Charkov, alebo po ukrajinsky Charkiv, bol až do roku 1934 hlavným mestom Ukrajiny a dlho aj najväčším mestom. Dnes tu žije 1,5 milióna obyvateľov. Údajne najväčšiemu námestiu Európy dominuje vodca svetového proletariátu. 



 Zopár záberov z ulíc. 



 Pri tomto pohľade sa mi vybaví báseň od môjho pravoslávneho spolucestujúceho, Alexandra Sergejeviča Puškina. 



 "Ždi menja, i ja vernus, toľko očeň ždi. 
ždi, kogda navoďat grusť žoltyje doždi, 
ždi, kogda snega metut, ždi kogda žara, 
ždi, kogda drugich ne ždut, pozabyv včera..."  
("Čakaj ma a ja sa vrátim, len veľmi čakaj, 
čakaj, keď žlté dážde privodia smútok, 
čakaj, keď je snehová metelica, čakaj, keď je horúco, 
čakaj, keď druhých nečakajú, zabudnúc ich včera...") 

 Pozrieme si chrámy, ...  





 ... sochy Gogoľa a Ševčenka... 



 ... a potom na chvíľku zájdeme do ZOO. Ťažko mi byť v tejto oblasti objektívna. Po tom, čo som navštívila ZOO v Lešnej, sa mi už žiadna iná nezdá pekná, a po prehliadke belehradskej ani žiadna škaredá.  





 Ako vidieť, kráľ zvierat je už unavený, my tiež.  Čakáme na večer, lebo ako sme sa dozvedeli od rôznych ľudí, o devätnástej bude na námestí slávnostný koncert a vystúpi aj Leščenko. Nemám ani tušenie, kto je to, ani či bude spievať, recitovať alebo fidlikať, ale jeho meno miestni vyslovujú s takou úctou, že už len preto stojím tri hodiny v dave a čakám.  





 Koncert je vedený po rusky v duchu družby medzi jednotlivými národmi bývalého ZSSR a vystupujú na ňom speváci z Estónska, Litvy, Bieloruska, Gruzínska, Ruska a Ukrajiny. Bielorusov stretneme na ďalší deň vo vedľajšom vagóne cestou z Poltavy. Toto sú oni: 


 Všetci umelci pozdravujú veteránov, úžasných Charkovčanov a hovoria, ako tu radi vystupujú a že ich ľúbia, čo vyvoláva nadšenie davu. Umelci sú to všelijakí národní a zaslúžilí, t.z. staršie ročníky typu Vondráčková a Modus, ale spievajú dobre a ľuďom bez rozdielu veku sa veľmi páčia. Obecenstvo tancuje, spieva, tlieska, kričí a fotí si ich. Ja tiež. 






 Najviac sa mi páčil Rus Aleksandr Maršal. Okrem iného zaspieval aj dve piesne s vojnovou tematikou, počas ktorých na obrazovke premietali archívne vojnové zábery. Bolo to veľmi emotívne. 





 Zlatým klincom programu je samozrejme Leščenko a nadšenie davu dosahuje vrchol.  



 My sa radšej pomaly poberáme preč, aby sme sa vyhli tlačenici po koncerte. Nakoniec, nech si užijeme aj trochu pohodlia našej 220-hrivnovej hotelovej izby.  



 Zajtra nás čaká Poltava a večer presun do Odesy. 
 (Citované úryvky sú z knihy Itinerarium od Daniela Krmana, vydal Spolok slovenských spisovateľov, spol. s r.o. v roku 2008)

  
Pokračovanie nabudúce. 

