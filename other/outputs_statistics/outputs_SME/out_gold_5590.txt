

 Zloženie čakajúcich zvedavcov a možno aj nejakých Mečiarových priaznivcov bolo podľa sfarbenia pleti fifty - fifty, aj keď prví sa k nemu dostali tí tmavší. 
  
 Podobne to vyzeralo aj v sále kultúrneho strediska, pravá polovička hľadiska tmavá, ľavá svetlejšia. Nič dramatické sa na mítingu neodohralo. Na začiatku aj na konci zaspievala  kandidátka na poslankyňu Katka Rosinová pekným zvučným hlasom niekoľko ľudových aj cigánskych piesni. 
  
 Po servilnom príhovore primátora a poslanca NR Pavla Džurinu nasledoval typický suverénny Mečiarov príhovor aj odpovede na písomne položené otázky. Atmosféru mítingu narušovala akurát hlučnosť a časté presuny cigánskych spoluobčanov vrátane detí. Po mítingu dostali cigančatá po cukríku a z kufra Mečiarovho auta rozdávali nové CD Katky Rosinovej, ktoré nahrala s Cigánskymi diablami. Po CD sa len zaprášilo, niektoré Cigánky si odnášali aj po tri. Nasledovala prechádzka po jednom z najmenších okresných miest, primátor s Mečiarom potom na štvrťhodinu odišli do Mestského úradu. 
  
 Po návrate symbolicky ozdobili mája a po krátkom sprievode centrom mestečka sa zúčastnili na jeho vztýčení. 
  
  
  
 Pokračoval kultúrny program, zaspievala ešte aj Katka. 
  
  
  
  
  
  
  
 Na avízovaný predvolebný guláš s pivom som nepočkal, musel som sa presunúť do Vyšných Remiet. Tam sa stavania mája zúčastňuje folklórna spevácka skupina z našej dediny "Úbrežanka", chcem to zdokumentovať a večer vložiť na obecnú webstránku. 
  
 V Remetách som zbadal europoslanca Borisa Zalu, žeby bol starosta zo Smeru? 
  
  
  

