

 Keď som sa o druhej prebrala, zistila som, že stále mám k dispozícii celú manželskú posteľ. Vedela som, že vyzvania typu: "Poď už spať, pozri koľko je hodín...." aj tak nepadnú na úrodnú pôdu, tak som to vopred vzdala. Darmo, je volebná noc, korunovaná poriadnou búrkou. Ktovie, žeby symbolika? 
 Zaspala som asi tvrdo, lebo už ani neviem, kedy sa manžel rozhodol zmeniť miesto svojho pobytu z obývačky na spáleň. Ráno som sa prebrala pár minút po siedmej. Tiež sa na mne prejavilo ponocovanie a tak som si pospala dlhšie, ináč vstávam skôr. O manželovi ani nehovorím, takto "dlho" nespal už dávno. 
 Keďže neviem len tak polihovať, tak som opatrne, potichúčky, vyliezla z postele a čo najtichšie som za sebou zavrela dvere. Zavreté zostali len pár sekúnd. Prudkým pohybom kľučky ich už otváral manžel. Nakoľko som bola v najmenšej miestnôstke nekomentovala som, že čo sa plaší, veď šiel spať nad ránom, nech si ešte pospí, alebo  niečo v tom zmysle... Zakričala som len ranný pozdrav. 
 O chvíľu som zmenila najmenšiu miestnosť za miestnoť iba o trochu väčšiu od nej a spustila som v pohode štart rannej hygieny. Vyleziem a môj manžel s vytrešteným pohľadom, oblečený, taška s notbookom pri dverách, obúva botasky. Kašľal na všetko, len rýchlo preč, veď je už "neskoro". Pýtam sa s úškrnom čo blbne, kde ide. S vážnym hlasom s miernou iskierkou podráždenosti mi odpovedal: "No kde asi? Do roboty". Tak to som už vedela koľká bije. Ešte som ho  chvíľu naťahovala, evidentne som  však nenarazila na zmysel pre humor a keď som sa už bála, že exploduje, tak som mu zvestovala, že nech sa kľudne vyzlečie, ukľudní, predýcha, odloží tašku, notbook, lebo že je dnes predsa nedeľa! 
 Hm, to ste mali vidieť! Nevedel, či má pokračovať v podráždenom tóne, či sa smiať. Asi uhádnete, čo zvíťazilo. Smiech, smiech, smiech.... Vybuchla som už aj ja.  Jednoducho skrat! 
 Urobila som môjmu vyplašenému chlapcovi raňajky. V kľude ich zjedol, pozrel čo nového na TA3 a začal sa znovu obliekať. Tentokrát už správne. Kraťasy, tričko - záhradkársky úbor. Zamietol po sebe stopy náznaku pondelka a vydal sa s výrazom uškŕňajúcich sa uletených včiel, do záhrady, do svojho pracovného raja. Ja som o chvíľu zaparkovala  vo svojom vareškovom raji a spustila som nedeľné tango, cha. 
   
 Aj takéto môže byť povolebné ráno... 
 Peknú nedeľu všetkým! 
   
 PS: nebonznite ma, prosím, že som ho bonzla 

