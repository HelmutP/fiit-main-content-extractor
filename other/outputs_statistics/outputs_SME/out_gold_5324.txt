

   
 Vieme o tebe,  že si hral vo viacerých kluboch. Aké  boli tvoje začiatky? 
 Moje futbalové začiatky  boli v rodnej dedine Šarišské Dravce. Neskôr som prešiel do futbalového  klubu v Lipanoch, kde som hral dva roky. Hralo som mi tam dobre, no  cítil  som, že mám na viac a preto som sa rozhodol pre MFK Košice. Tu sa  mi hrá výborne. Hral som v doraste, v košickej rezerve a momentálne  reprezentujem košický „A„ tím. 
  Určite máš futbaloý  vzor. Kto je ním? A aký je tvoj vysnívaný  futbalový klub, v ktorom by si chcel pôsobiť? 
 Mojím futbalovým vzorom je  už od malička David Beckhem. A môj vysnívaný klub? Jednoznačne  Arsenal FC. Myslím si, že je snom každého futbalistu hrať v takomto  skvelom tíme. 
 Máš iba 19 rokov a  hráš v MFK Košice za Á- čko. Bolo  ťažké prebojovať sa k najlepším? 
 Ľahké to určite nebolo,  ale ak niečo miluješ tak si ideš za svojím cieľom. 
  Kde  študuješ? Zvládaš popri futbalu aj  školu?  
 Navštevujem Športové  gymnázium v Košiciach. Mám individuálny učebný plán, čo  mi umožňuje venovať sa futbalu na 100%. Jasné, škola  je pre mňa tiež veľmi dôležitá. V blízkej budúcnosti je  tu maturita a chcel by som ju zvládnuť čo najlepšie. 
  Mal si už  počas svojej futbalovej kariéry nejaké  vážne zranenia? 
 Áno, bohužiaľ som už mal  vážne zranenia. Bol som na operáciu kolena a taktiež som mal zlomenú  ruku. Našťastie sa všetko dobre zrástlo a hrám ďalej. 
  Na akom poste hráš? 
 Som defenzívny záložník  (stredný). 
  Tri roky po sebe si sa  dostal do slovenskej reprezentácie. Určite to bol pre teba  úspech. Kde všade si za slovenskú  reprezentáciu hral? 
 Bolo to skvelé reprezentovať  Slovensko. Nazbieral som veľa skúseností a mám super zážitky.  Ako slovenský preprezentant som hral v Anglicku, Slovinku, Poľsku,  Českej republike a na Ukrajine. 
 V decembri 2008 si ako  mládežnícky futbalový reprezentant zaujal na testoch v anglickom  prvoligovom klube FC Middlesbrough. Hovorilo sa,  že by si mohol byť ďalším slovenským  úlovkom Angličanov. Je to pravda? Mal si možnosť  prestúpiť? 
 Je to pravda. Anglický  klub mal o mňa záujem. Bolo to nadosah, no nezhody v klube ma vrátili  späť na Slovensko. Dúfam, že v budúcnosti odídem do európskej  futbalovej špičky a nič mi v tom nebude brániť. 
  Novým slovenským  futbalovým  kráľom za rok 2009 je Marek Hamšík. Ak by si mal možnosť  hlasovať v ankete koho by si vybral ty? 
 S futbalovým kráľom za rok  2009- Marekom Hamšíkom som spokojný. Myslím si, že si toto odcenenie  zaslúži. 
 Aký  je tvoj najvačší futbalový  úspech? 
 Som majster Slovenska v  dorasteneckej  súťaži a víťaz Slovenského pohára s košickým ,,A,, mužstvom. 
 Prezraď  nám, aké sú tvoje očakávania do budúcna? 
 V budúcnosti by som sa chcel  živiť futbalom. Futbal milujem a budem robiť všetko preto, aby som  patril k tým najlepším.   
 „Futbal milujem a budem  robiť všetko preto, aby som patril k tým najlepším." 
   

