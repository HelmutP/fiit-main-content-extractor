
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Šášky
                                        &gt;
                Z môjho nudného života
                     
                 Internetová Partička 

        
            
                                    9.5.2010
            o
            22:55
                        (upravené
                31.5.2010
                o
                12:52)
                        |
            Karma článku:
                4.58
            |
            Prečítané 
            1417-krát
                    
         
     
         
             

                 
                    Pamätám sa, keď začínal seriál Panelák a ja som bol veľmi nespokojný, ako vyzeral. Bol vážny, prakticky bez humoru. Nahnevalo ma to, lebo som bol zo Susedov zvyknutý od Andyho Krausa na trochu iný štýl. Rozhodol som sa urobiť vlastný seriál, spoločne s kamarátmi...
                 

                 Padlo to. Aj keď sme to plánovali ako internetový seriál, čo bolo na náklady oveľa jednoduchšie. Boli to pekné prípravy. Mali sme už hlavné postavy, mali sme už vymyslené z akého prostredia to bude písané, dokonca sme mali napísaný prvý diel...   Aj keď sme mladí a človek by povedal, že času máme habadej, akosi každý z nás mal iné problémy ako riešiť internetový seriál. Aj keď som si myslel, že to viacerých z nás nadchne viac. Dokonca sám som bol prekvapený, ako som zrazu stratil nápady a elán...   Dnes viem, že vytvoriť z ničoho nič vlastný seriál je len detská predstava. Treba mať do toho oveľa väčšiu vervu, ako som ja vtedy mal. Možno, že teraz by som sa tomu vedel venovať plnohodnotneje, ale nemám na to chuť, písať to. Aj tak nemám peniaze na nejakú reklamu a bez reklamy dnes nefunguje nič...   Preto som, ani sám neviem ako, opustil seriálový svet a dostal sa do sveta relácii. Alebo skôr jednej relácie. Volá sa Partička. Myslím, že nemusím tento zábavný program nejak zvlášť predstavovať, myslím, že ho pozná priam každý...   Chodievam v škole na krúžok s názvom KLiK (knižný a literárny klub), nakoľko je do neho zaradený aj novinársky krúžok, kde ja spoločne s trochu chabou štvorčlennou redakciou tvoríme školský časopis. Keď dorobíme nejakú časť a skončíme, poberiem sa ku KLiK-áčom. A pred dvoma týždňami vedúca krúžku prišla so zaujímavou a originálnou myšlienkou: „Poďme si zahrať takú hru, že každý povieme jednu vetu a budeme z toho tvoriť príbeh.” Jej sestra Kristína ju razom sfúkla, že sleduje veľa Partičky...   Ale hrali sme. Už dva týždne za sebou. Jednu vetu. Boli z toho dva veľmi zaujímavé príbehy. Ten prvý obzvlášť. Osobne si však myslím, že ešte lepšia hra je „Jedno slovo“. V nej ide o  to, že štyria hráči tvoria po jednom slove nejaký zaujímavý príbeh.  Trochu ma hnevá, že Dano Dangl k tomu v Partičke dáva nejaký začiatok. Keby herci  tvorili príbeh úplne od začiatku sami, možno by to bolo ešte lepšie.   Každopádne, ja a Kristína sme sa z dlhej chvíľky rozhodli si zahrať to „Jedno slovo“. Keďže boli tri-štyri minúty pred jedenástou večer a navyše ja som bol chorý, museli sme si na túto hru vystačiť internetom. A veru, ani na internete (v tomto prípade konkrétne na ICQ) to nebolo zlé. Aspoň sa to lepšie zachytilo.   A tento príbeh naozaj stál za to (áno, takto skromný som sa už narodil). Napriek tomu, že sme sa pre tú hru rozhodli dve minúty pred tým, ako sme ju hrať začali, bola to sranda. Presne ako v Partičke - vlastne sme improvizovali.   Rozhodol som sa Vám poskytnúť tento príbeh. V ICQ grafike po jednom slove to zaberalo sedem-osem wordových strán. Vám som sa to snažil spraviť do troľku ľahšie-čitateľnej, teda jednoduchšej grafiky. Príbeh je však samozrejme nezmenený a ja dúfam, že sa Vám bude páčiť.   Vychutnajte si teda trochu Partičky pred naozajstnou Partičkou (dnes po Let's Dance na Markíze) a predom sa ospravedlňujem všetkým starším ľuďom (aby sme nedopadli ako Facebook-skupina: Jejich revírem je Kaufland, jejich tempo je vražedné - DŮCHODCI!) a tiež aj Slobode zvierat (aby sme nedopadli podobne ako spomínaná skupina)...   K: Malá M: Miška K: šla  M: do  K: krčmy  M: kúpiť  K: babke  M: točené  K: zuby  M: ale  K: boli  M: príliš  K: drahé  M: tak  K: sa  M: na  K: to  M: z  K: ničoho  M: nič  K: vykašlala.   M: Radšej  K: vzala  M: do  K: tašky  M: babku  K: v  M: deke  K: a  M: šla  K: domov  M: kde  K: našla  M: schúleného  K: kocúra  M: ktorý  K: chrápal  M: na  K: stolčeku  M: vedľa  K: psa  M: ktorý  K: bol  M: úplne  K: zmrznutý  M: od  K: zimy  M: ktorá  K: spôsobila  M: výpadok  K:  prúdu.   M:  Babka  K:  si  M:  ťažká!  K:  vykríkla  M:  ona  K:  a  M:  hodila  K:  babku  M: za  K:  pec.   M:  Ako  K:  začalo  M: blýskať  K: pes  M: začal  K: brechať  M: Miška  K: sa  M: pokakala  K: od  M: prekvapenia  K: že  M: pes  K: žije  M: lebo  K: bol  M: na  K: plechu  M: kde  K: pripravila  M: zmzlinové  K: prekvapenie  M: pre  K: dedka  M: a  K: zavrela  M: prasesternicu  K: do  M: pece  K: za  M: stoličkou.   K: Na  M: koberci  K: sa  M: trblietali  K: Miškine  M: zuby  K: ktoré  M: jej  K: vytrhla  M: mŕtva  K: nevesta  M: na  K: svadbe  M: ktorá  K: skončila  M: tragédiou.   K: Ženích  M: kopol  K: svokra  M: medzi  K: oči  M: a  K: rozplakal  M: sa  K: pred  M: oltárom  K: kde  M: už  K: nevydržal  M: močový  K: tlak  M: a  K: utiekol  M: pričom  K: nechal  M: mokrý  K: obrys  M: na  K: koberci  M: ktorý  K: teraz  M: páchne  K: po  M: mrcine.   K: Babka  M: sa  K: už  M: zobúdzala  K: spoza  M: pece  K: a  M: myslela  K: že   M: oslepla.   K: Vtom M: zistila K: že M: je K: mŕtva M: čo K:  vyvolalo M:  otázky K:  jej  M:  posmrtného K:  stavu M:  a K:  preto M:  vyšla K:  von M:  spoza K:  dverí M:  a K: zavrela M:  Mišku K.  do M:  izby K:  spolu M: s K:  dedkom M:  a K:  zamkla M:  tam K:  aj M:  schúleného K:  kocúra M: lebo K:  začal M:  hrooozne K:  smrdieť M:  ako K:  keby M:  ich K:  už M:  začínali K:  nevidieť M:  duchovia K: zo M: záhrobia K: preto  M: babka  K: odišla  M: do  K: nebeského  M: hostinca  K: a  M: vypýtala  K: si  M: točené  K: zuby  M: ktoré  K: jej   M: objednala  K: jej  M: bezzubá  K: vnučka  M: Miška. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Pekarík potešil mladých slovenských futbalistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Prišlo na moje slová, komentovať bude Slávo Jurko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Keď aj futbalový fanúšik pociťuje hrdosť, že je Slovák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Weiss nadáva na fanúšikov, fanúšikovia na Weissa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Šášky
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Šášky
            
         
        sasky.blog.sme.sk (rss)
         
                                     
     
        Večne meškajúci maniak do športovej novinárčiny, s extrémne vyvinutým zmyslom pre ironické a sarkastické poznámky, predseda vlastnej kvázipolitickej strany SPI a človek prácu vykonávajúci vždy na 100% (pondelok 12%, utorok 23%, streda 40%, štvrtok 20% a piatok 5%...)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    139
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    980
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            MS v hokeji 2011
                        
                     
                                     
                        
                            MS v hokeji 2012
                        
                     
                                     
                        
                            Iné športy
                        
                     
                                     
                        
                            Z môjho nudného života
                        
                     
                                     
                        
                            Televízor
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mono pohľad na to čo vidím keď nevyzriem natoľko,
                     
                                                         
                       3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




