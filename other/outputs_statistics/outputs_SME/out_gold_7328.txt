

 Kde ešte môžte vidieť v dobe igelitiek klasickú sieťku, ako zo šesťdesiatych rokov (rovnakú nosila moja mama desaťročia v kabelke), v ktorej má bezradný dôchodca hlávku kapusty. Márne hľadá okuliare, aby vylúštil, čo ešte treba nakúpiť zo zoznamu na dokrčenom papieriku. 
 Kde inde  sa dá vidieť ženička v kidľoch a lajbriku, v neuveriteľnej kombinácii tyrkysovej a ružovej, ktorá v nárečí s prekrásne mäkkým š a ž, ponúka "ľem totu najsamľepšu žeľeninu a šemiačka na calym tarhu". 
 Kde inde, ratlík, ponechaný na chvíľu bez dozoru ovoniava košík plný uhoriek na zaváranie. Keď nezacíti nič zaujímavé, zdvihne zadnú nohu a... Ešteže to nebola doga. 
 Kde inde môžete stretnúť snáď storočnú babičku ako z rozprávky, ktorá sa beznádejne pokúša predať rovnako zošúverené jablká. Sú na koláč, hanblivo ospravedlňuje ich biednu kvalitu. 
 Kde inde vidíte pána riaditeľa, či profesora len tak naboso v šľapkách a pokrčenom tričku ako kupujú čučoriedky od fialovorukých Rómov, sviatočne oblečených v snehobielych košeliach a blúzkach. 
 Kde inde asi mestskí policajti "kupujú" pašované cigarety od priekupníkov, ktorí ich majú schované v igelitkách medzi odpadkami. 
 Kde inde počujete gazdu volať za starenou, ktorú citiť naftalínom a močou: "a nabudúce si aspoň vypýtajte", keď so sklonenou hlavou, šuchtavo odchádza od pultu bez zaplatenia s veľkým zemiakom v každej ruke. 
 Kde inde kúpite u posledného bulharského zeleninára skôr slaninu a klobásky, ako jeho zeleninu alebo ovocie. 
 Kde inde tetky predávajúce domáce vajíčka, či syry, sa snažia kvôli nezmyselnému zákonu dostať čím skôr preč zo záberu kamerového systému, keď si nájdu zákazníka.  
 Kde inde z vás predavač papriky právom urobí hlupáka, keď si pýtate štipľavú papriku a on vám podá feferónky: "Ja som dobre dal, vy zle pýtal. Kceli ste čiplavú, máte čiplavú. Mali ste pýtať pikánš, keď ste kceli, čo ste kceli". 
 Kde inde vám kúpa prvej tohoročnej tekvice urobí viacnásobnú radosť. Prvýkrát,  keď si ju kúpite, druhýkrát pri predstave chutného perkeltu s tekvicovým prívarkom, a tretíkrát, keď stretnete svojho najvyššieho šéfa a vybaví sa vám vaša výplatná páska. Pri predstave, ako by ste  za ňu, ho tou tekvicou najradšej tresol po hlave, sa nahlas rozosmejete. 
 A úplne najlepšie by bolo, dať tou tekvicou po hlave všetkým našim radným, ktorí odklepli výstavbu Auparku v centre mesta. Nie Aupark, ale krytú tržnicu - brucho mesta, treba Košiciam! 

