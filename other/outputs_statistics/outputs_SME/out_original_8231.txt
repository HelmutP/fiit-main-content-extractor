
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Dunaj
                                        &gt;
                Nezaradené
                     
                 Legislatívna ochrana je ako vzduch....dôležitá až vtedy, keď nie je 

        
            
                                    7.6.2010
            o
            1:22
                        (upravené
                7.6.2010
                o
                10:03)
                        |
            Karma článku:
                3.51
            |
            Prečítané 
            549-krát
                    
         
     
         
             

                 
                    Väčšinu z nás – a povedzme si pravdu, skôr ženy, než mužov, vychovali vo viere, že jedného dňa stretneme tú pravú/toho pravého a že to bude osoba, s ktorou budeme navždy (pre štatisticky triezvych – cca 20 rokov)...
                 

                     V reálnom živote sa však toto očakávanie často nesplní. Štatistiky rozvodovosti uvádzajú na piedestáli niektoré štáty s 50%nou rozvodovosťou, 30 až 60% ľudí má mimomanželský pomer, 21% dospelých žien je týraných avšak máme aj 5% týraných mužov, u susedov v Česku je to dokonca 7% (ako to krucinál tie češky robia?) Aby bola štatistika nešťastných záverov, spočiatku celkom šťastného vzťahu dokonalá, tak ešte uvediem, čo som našiel vo Wikipédii kedysi dávno pod výrazom „adopcia“:       Adoptovať – osvojiť je možné len maloleté dieťa, ak je osvojenie v jeho záujme. Ak je maloleté dieťa schopné posúdiť dosah osvojenia, je potrebný aj jeho súhlas. O adopcii rozhoduje súd na základe návrhu žiadateľov o adopciu - osvojenie. Na osvojenie je potrebný súhlas rodičov osvojovaného dieťaťa...       ...a sme doma. A bez dieťaťa. Sami dvaja, s kopou papierov, vyhliadnutým dieťatkom (v šťastnejšom prípade árijského typu) kdesi v domove v drevenej postieľke a erárnom pyžamku a s odhodlaním, že sa nevzdám, kým to mimino nebude zaspávať u nás doma. Ale vzdáš, keď biologická matka pošle pohľadnicu, či z ničoho nič v nej dozrejú materinské pudy. Raz darmo, aj naša moderná medicína pri všetkých svojich zázrakoch ešte stále nedokáže to, čo Boh, alebo ak chcete – príroda. Z matky, ktorá nemá byť matkou jednoducho matku neurobíte. A platí to aj duševne. Našťastie omnoho častejšie existujú v praxi páry, ktoré dospejú do rozhodnutia byť rodičmi a milovať potomstvo bez výhrad. Cieľom má byť „byť rodičom“ nie silou mocou „porodiť“. Ak sa to druhé nedarí, tí dvaja sa raz dostanú do štádia, kedy zatúžia ísť na exkurziu do detského domova a zahorieť nefalšovanou rodičovskou láskou ku niektorým z tých obrovských preľaknutých očí v postieľkach. V dôsledku negatívnych skúseností s detskými domovmi socialistického typu sa v súčasnosti podporuje adopcia detí alebo ich výchova v malých domovoch rodinného typu. Dôvodom toho je, že zdravý vývoj dieťaťa a jeho socializácia predpokladá osobný vzťah a intenzívnu komunikáciu s náhradnou mamou či otcom. Tak prečo existujú tie ustavične sa opakujúce scenáre súdnych prieťahov, zisťovačiek, lehôt počas ktorých dieťa pomaly dospeje kým dostane náruč náhradných rodičov? A že sa to dá aj jednoduchšie a bez výhovoriek o tom nesvedčí iba Angelina a Brad, ktorí si doma robia celkom slušnú zbierku malých ľudí zo všetkých kontinentov.   Pri všetkej úcte, opäť po štyroch rokoch s tým môžeme niečo urobiť aj u nás, v mene pomoci a duševnej podpory tým, ktorí si prešli adopčným kolotočom a pre pomoc tým, ktorí ho budú prechádzať v budúcnosti. U Edity Pfundtner som našiel jasné riešenia aj na tento problém a to som už skutočne čítal kadečo. Jeden kvalitne premakaný program na riešenie ošemetných problémov rodín. Reálnych, liberálnych, ľudských, bez hrajkania sa na Billingsa či Boha. Proste keď to raz nejde v súlade s prírodou, tak nech to ide ľahšie aspoň po tej legislatívnej stránke. Tu totiž už dávno nejde iba o rodičov...tu ide najmä o deti. Pretože legislatívna ochrana je ako vzduch....dôležitá až vtedy, keď nie je.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Keď nás tu bude 100 000 Jano sa oholí!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Manažérov denník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Najlepší kúzelnícky trik na svete!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Funkčná rodina II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Konkurz do zamestnania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Dunaj
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Dunaj
            
         
        dunaj.blog.sme.sk (rss)
         
                                     
     
        Žijeme tak krátky život až mi je to smiešne ako sa staviame do roly toho, ktorý má vo svojom eLVéčku všetky planéty. Niekto je hore a niekto je dolu. Ten dolu je na ceste a potkýna sa o ďalších
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    694
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




