
 Budujem nový vzťah. Skôr staro-nový. Snažím sa zo všetkých síl. Hľadám odpovede, na mnohé otázky. Otázky bez akejkoľvek spätnej väzby, ktoré sa mi hromadia vo veľkej krabici. Už nemám miesto, neviem kam ich uskladniť. Chcem sa z toho obrovského zmätku a začarovaného kruhu konečne vyslobodiť. 

Možno, chvíľu áno a nakoniec nie. Nalinkovaný kolobeh citov, udalostí. Keby tak záležalo iba na mne... Nezáleží!

Rozhodla som sa ísť novým smerom. Už ani neviem koľkýkrát. Dostať sa bližšie k zdroju. Pochopiť, prečo sa to takto všetko deje. Aké máš so mnou zámery. Čo bude ďalej. Prečo ma na jednej strane posielaš do boja ale nedovolíš mi zvíťaziť. Budem sa snažiť zo všetkých síl. Sľubujem, že prijmem akúkoľvek odpoveď...

Jedno však viem. Nechcem prežiť to hlúpe leto opäť ako nejaká bábika s vyčesanými vlasmi, namaľovanými perami v krásnych šatách avšak bez duše a s ľadovým srdcom... 
