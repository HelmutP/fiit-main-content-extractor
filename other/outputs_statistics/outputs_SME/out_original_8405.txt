
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Giač
                                        &gt;
                Nezaradené
                     
                 Politický barometer? 

        
            
                                    9.6.2010
            o
            9:50
                        |
            Karma článku:
                2.84
            |
            Prečítané 
            605-krát
                    
         
     
         
             

                 
                    Kto sa v utorok v neurčitých  nočných hodinách  napriek  nedodržanému   vysielaciemu času rozhodol  vyčkať predvolebnú  kampaň, možno sa tak rozhodol aj kvôli  Kapurkovej veselej . Strana  - na prvý pohľad  nezvyčajná a drahá recesia. Keď si však  spojíme  ich vyhlásenia  „nás voľte len vtedy, ak by ste inak nešli  voliť" a či výzvu „zamyslite sa najprv nad ostatnými stranami"     s názormi niektorých blogerov, ktorí volajú po tom, aby sa mohli vhadzovať  aj prázdne volebné lístky  (s významom som proti),  už to nemusí byť  o recesii ale prinajmenšom o nekalibrovanom politickom  barometri.  ...
                 

                  ... Kto sú možní voliči Kapurkovej veselej?  Recesisti ale ako som naznačil, aj nespokojenci s doterajší m bludným   kruhom  z ktorého sa spoločnosť nevie vymotať.  Čo môže priniesť Kapurková veselá.  Ak ju bude voliť mizivé percento, tak  najskôr nič. Ak by sa však dostala do parlamentu tesne na hranici zvoliteľnosti, už by to nebola  len recesia , ale aj vyjadrenie protestu,  prejav, že  aj „nevoliči"  chcú voliť. A ak nevolia, tak preto, lebo sú za prejavenie svojej vôle iným spôsobom.   Akým  spôsobom, to  by sa pravdaže vyčítať nedalo.   Vo volení  Kapurkovej   by som videl  dokonca  veľa   podobného  s našimi  revolúciami a či inými spoločenskými zmenami.  Uskutočnili  sa predovšetkým  preto, lebo masy boli proti niečomu.  A čo sa má stať po víťazstve revolúcie, to niekedy nevedeli nielen masy, ale niekedy ani  vodcovia.  Tí, čo vybojovali na námestiach  rok 1989, vedeli,  že  sú proti vláde jednej strany a nedemokratickej spoločnosti. Nemálo z nich bolo prekvapených, keď  prezident Havel  v novoročnom  prejave vyhlásil za jednu zo svojich  priorít  návrat kapitalizmu.  Ak by  sa Kapurková dostala do parlamentu, aké by mohli nastať varianty?   1.) Bola by málopočetná.  Možno by to vyprovokovalo  rozmýšľanie  nad  uplatňovaním demokracie v štáte, možno na zmenu volebného systému. 2.) Bola by natoľko početná, že bez nej by sa nemohla vytvoriť vládna väčšina. Mali by sme menšinovú vládu. Kapurková by bola jazyčkom na váhe.  3.) Kapurková  by bola natoľko  početná, že sama by mohla vytvoriť vládnu väčšinu.  Pravdepodobne by nemala ambície na zostavení vlády a tak by sme mali znova menšinovú vládu.  4.) Kapurková by získala takmer 100 %.  Nezostávalo by nič iné, len vymenovať  úradnícku vládu.  Takéto varianty by boli možné len za predpokladu, že Kapurková , by ani po úspešných voľbách neašpirovala na obsadenie vládnych pozícii.   Varianty  3.  a 4. sú predpokladom silnej kontroly vlády parlamentom.  Ministrom by nebolo ľahostajné, ak by sa ich odvolávanie dostalo pred parlament.  Teraz už dopredu vedia, že ak odvolanie navrhne opozícia, pokojne môžu spávať.  V prípadoch 2. a 3. by to bola pre odvolávaných ministrov neistota.    Tak ako nevieme, čo prinesie úspešná revolúcia, tak isto nevieme, čo by prinieslo víťazstvo Kapurkovej.   V tom, že sa Kapurková podobá  na revolúciu sú jej šance ale aj slabina.   Ja by som aj uvítal parlament, ktorý by zodpovedal  3. variantu. Veľa nezávislých, ale zároveň taký počet  poslancov z politických strán, ktorý by bol zárukou, že parlament by sa nezmenil na nefunkčný  zbor.      Stane sa  už v týchto voľbách Kapurková politickým barometrom alebo zostane len klubom recesistov?  Ten najhorší  variant by bol, ak by sa z nich vykľul  klub politických dobrodruhov. Politickým realistom je odpoveď jasná už teraz. Ale voľby, sú voľby a až tie dajú správnu odpoveď. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Komunálne voľby prerušené na 38 minút.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Posledný a či ostatný?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Štyri metre štvorcové.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Starý Martin a Božie napomenutie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Paraglajdisti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Giač
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Giač
            
         
        giac.blog.sme.sk (rss)
         
                                     
     
        Vekovú hranicu, keď som mohol hovoriť, že už mám po mladosti a do dôchodku ďaleko, som už prekročil.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    70
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1079
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            O slovenčine
                        
                     
                                     
                        
                            Počítače
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




