

 
       Nápadité obálky Výberu, neprehľadnutelne ležali na komode. Keďže mám po skúške a o čosi viac času zaoberať sa týmto, vzala som pri raňajkách jednu z nich. Štedro nabalená, plná pestrofarebných reklamných ponúk, rôzných výherných listín, ktoré víťazne uvádzali moje meno, ako meno raz výhercu, raz finalistu, raz oficiálneho finalistu, raz zvýhodneného zákazníka....tých titulov sa mi dostalo dosť. 
 
 
     Celkom ma to pobavilo. Toľko úcty naraz a za nič. Podobné tučné obálky s obvyklými poctami a vyznamenaniami sa nám už dostalo viackrát. Pravidelne postupujeme do vyšších súťažných kôl, pravidelne sme najváženejší a najlojalnejší zákazníci Výberu. Spôsob, akým táto veselá s.r.o. získava zákazníkov sa mi zdá minimálne neseriozny, ak nie teda nelegálny. Vo verejne sprístupnenej elekronickej forme obchodného registra má táto s.r.o. ako predmet podnikania uvedené nasledovné: 
 
 
- kúpa tovaru na účely jeho predaja konečnému spotrebiteľovi (maloobchod) v rozsahu voľnej živnosti a iné. 
 
 
    Spôsob, akým realizuje tento predmet svojho podnikania je určite nápaditý, to im v plnej miere uznávam, je originálny, ale navýsosť tomuto všetkému i pochybný. Určite podporujem originalitu a vynaliezavosť konkurencie v presadzovaní sa na trhu. Nikdy by to však nemalo žiadnym spôsobom obťažovať zákazníka, spotrebiteľa  a nekalosúťazne poškodzovať konkurenciu. Súťaž na trhu je už raz taká, že konkurencií škodí. Je to jej imanentnou vlastnosťou. Malo by sa tak diať avšak spoločensky únosným spôsobom. 
 
 
   V čom vidím nekalosúťažnosť, či porušenie dobrých mravov súťaže? 
 
 
  Najskôr právne. V zmysle ust. §  44 ods. 1 Obchodného zákonníka, nekalou súťažou je jednanie v hospodárskej súťaži, ktoré je v rozpore s dobrými mravmi súťaže a je spôsobilé privodiť ujmu iným súťažiteľom alebo spotrebiteľom. Nekalá súťaž sa zakazuje. 
 
 
    Spôsob, akým sa prezentujú naplňa podľa môjho názoru všetky tri znaky nekalej súťaže. I keď je to vec správnej interpretacie, predsa len 
 
 
1. je to jednanie v hospodarskej súťaži , 
 
 
2. je spôsobilé privodiť ujmu súťažiteľom alebo spotrebiteľom ( toto je podľa trošku sporné, či naozaj) a napokon 
 
 
3. je to proti dobrým mravom súťaže ( čím som si istá, že je). 
 
 
    Tieto tri znaky musí naplňať každé nekalosúťažné jednanie. Ich naplnenie potom tvorí základ rôznych skutkových podstát. Napr: lavína, klamlivá reklama, podstrkávanie tovaru, bojkot, diskriminacia. Ja to vidím na pekné, ukážkové prémiovanie. Jeho podstata spočíva vtom, že súťažiteľ svojich zákazníkov neodôvodnene zvýhodňuje a získava ich tým na úkor iných súťažiteľov. Je to jedna z foriem preťahovania zákazníkov. Za nekalé je považované poskytovanie takých výhod, ktoré sa vymykajú rámca obvyklosti pomerov a dobrých mravov súťaže. V mojom konkrétnom prípade vidím nekalosúťažnosť vtom, že zákazník je vidinou výhry, vidinou toho,že je zaradený medzi finalistov, zlatých finalistov a neviem čoho všetkého, zvádzaný k nákupe tovarov väčšej hodnoty. Je vecou interpretacie, čo je nákup väčšej hodnoty. Podľa mňa nákup serie CD, serie kníh v hodnote 2000 Sk je nákup väčšej hodnoty. Ďalej nekalosúťažnosť praktík usudzujem aj zo spôsobu, akým vyberajú finalistov, výhercov. Mám za to, že každý, kto si doteraz niečo kedy objednal je zlatým finalistov. my sme ním pravidelne, bez ohľadu na to, či sme pasívny alebo nie. Aké sú vlastne kritéria toho, aby sme sa finalistom stali? Tým, že som zaradená bez všetkého na zoznam finalistov, získavam neodôvodnenú výhodu. Súťažiteľ ťaží z pocitu vďačnosti, prameniacej z práve poskytnutej výhody. Je otázka akú ujmu môže vyvolať tento postup. Termín ujma by som striktne verbálne nevykladala len ako ujma materiálna, ale aj ako subtilnejšia forma ujmy, napr: stráta času.. 
 
 
    Postup, spôsob, akým sa spoločnosť Readers Digest Výber presadzuje je vzhľadom na vyššie uvedené podľa môjho názoru ak nie nekalosúťažný, minimálne pohybujúci sa na hrane zákona. Zákazníka obťažuje nominaciami na finalistu, poskytnutí prednosti v súťaži a prísľubom  následnej výhry podmienenej objednávkovým formulárom. 
 
 
   Tento názor bol vyslovený v súlade s  čl. 26 ods. 2 Ústavy Slovenskej republiky, garantujúceho politické právo, právo vyjadriť svoj názor,slovom, písmom, tlačou, obrazom alebo iným spôsobom.  
 
 
   
 

