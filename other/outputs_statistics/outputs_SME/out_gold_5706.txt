

 Lucia Janovič je sympatická tridsiatnička s jasnými, nekompromisnými názormi na súčasnú vládu. Rovnako aj jej manžel Adam. Obaja stáli trochu bezradne na Námestí SNP a sledovali hŕstku ľudí, ktorí sa zhromaždili pred malým pódiom. 
 „No, ešte chvíľku počkáme", povedala Lucia a ospravedlňujúco sa usmiala. 
 „V pohode", odpovedal som a „nenápadko" som sledoval policajné autá, ktorých bolo zrazu na námestí akosi priveľa. Aká dojemná starostlivosť. Vo vrecku som mal zubnú kefku a čepieľku na holenie (samozrejme z recesie). Tak sa chodilo na demonštrácie „za komančov"... 
 Demonštrácia mala na môj vkus až príliš pokojný priebeh. Keď ma vyzvali, išiel som na pódium a stručne som porozprával o aktivitách občianskej iniciatívy ĽUDIA PRE LESY. Krátko na to, ako som zišiel z pódia, pristúpil ku mne mladý muž a podal mi ruku: 
 „Ste super. Držím vám palce. Ja som ten hnojár. Ten, čo vysypal ten hnoj pred úrad vlády", povedal a usmial sa. 
 „Ste odvážny chlap.", konštatoval som. 
 „Však aj vy.", zakontroval. 
 No, neviem.  Iste má význam prejaviť odvahu. Hlavne, ak ide o obhajobu a presadzovanie zásad slušnosti . Ale Slovač akoby nestála o to, aby v tejto spoločnosti platili nejaké zásady. Preto sa ten lakmusový papierik farbí na červeno... 
 P.S. „Poďme sa niekam najesť", vyzvala ma manželka Katka, keď sme odchádzali z Námestia SNP. „Fajn, pozývam ťa na švédsku kuchyňu", odvetil som a o pár minút sme zaparkovali pred známym obchodným centrom „na štyri". Vošli sme do reštaurácie na prvom poschodí. Postavili sme sa do rady, čakajúc s táckami v rukách.  Katka sa otočila a smutne konštatovala: „Za nami stojí viac ľudí, ako bolo na tej demonštrácii na námestí. Divné mesto...divný národ". 
   

