
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lisinovič
                                        &gt;
                Z mojej papierovej dielne
                     
                 Môj modelársky rok 2009 

        
            
                                    31.12.2009
            o
            11:11
                        (upravené
                11.3.2010
                o
                16:05)
                        |
            Karma článku:
                11.05
            |
            Prečítané 
            4022-krát
                    
         
     
         
             

                 
                    Na záver starého roka sa pozrieme na nové prírastky do mojej zbierky papierových modelov. Okrem návštevy 65 Kalvárií a zverejnenia 129 článkov na mojom blogu mi totiž pribudlo aj 9 dokončených papierákov...
                 

                 Modely Vám predstavím v poradí, akom boli dokončené. Najrozsiahlejším modelom bola 7-vozňová súprava Pendolina (1:120).      Elektrický rušeň 363.084 ČD v reklamnom nátere zbierky Pomozte dětem (1:120)      Pohľad do búdky rušňovej čaty parného rušňa radu 475.1 ("Šľachtičná")      Lokomotíva Tomáš bol darček pre moju Jedinú:)      Piramídy a sfinga. Týmto modelom som si skompletizoval zbierku siedmich divov sveta.      Motorový vozeň M 120.417 ČSD (1. model zo série Železnice H0)      Kostol v Spišskej Novej Vsi      Motorový vozeň M 262.045 (2. model zo série Železnice H0)      Elektrický rušeň E 499.004 "Bobina" (3. model zo série Železnice H0)      Toľko moje prírastky v uplynulom roku. Okrem vlastnej tvorby som navštívil výstavy papierových modelov v Brne, Prešove, Banskej Bystrici, Nitre, Bratislave a Prahe. Na výstavu modelovej železnice som zašiel do Prešova, Přerova a Košíc. Do Nového roka prajem všetko dobré! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Skutočnosť a model: Grassalkovichov palác v Bratislave
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Putovanie po slovenských Kalváriách (194) - Klokočov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lisinovič
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lisinovič
            
         
        lisinovic.blog.sme.sk (rss)
         
                        VIP
                             
     
         Študent študujúci v škole života. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    519
                
                
                    Celková karma
                    
                                                6.37
                    
                
                
                    Priemerná čítanosť
                    2265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šaštín
                        
                     
                                     
                        
                            Slovenské Kalvárie
                        
                     
                                     
                        
                            Z mojej papierovej dielne
                        
                     
                                     
                        
                            reportáže z modelárskych akcií
                        
                     
                                     
                        
                            v prírode
                        
                     
                                     
                        
                            ulicami miest
                        
                     
                                     
                        
                            železnice a MHD
                        
                     
                                     
                        
                            motorizmus
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            extra
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj brat
                                     
                                                                             
                                            Moja manželka:)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            railtrains.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




