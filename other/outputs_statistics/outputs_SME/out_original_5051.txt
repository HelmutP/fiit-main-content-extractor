
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Múdry
                                        &gt;
                Receptárium
                     
                 Slané nelinecké pečivo 

        
            
                                    22.4.2010
            o
            18:00
                        |
            Karma článku:
                7.67
            |
            Prečítané 
            2584-krát
                    
         
     
         
             

                 
                    Tak som sa nostalgicky zahľadel cez okennú tabuľu na okolitý sveta. Dnes máme pekný, zubato slnečný, ale trošku veterný deň. Nevstávajte od výpočtovej techniky, môžete mi veriť. Práve som bol preveriť aktuálny poveternostný stav na balkóne. Je presne taký istý aj na druhej strane činžiaku. Mám rád, keď aspoň niektoré sociálne istoty fungujú. Borovicové šišky na strome pri dome si veselo praskajú a nám sa snaží udomácniť sršeň na balkóne. Musím urýchlene podniknúť niečo zásadné proti prírode a jarnej únave. Idem niečo upiecť.
                 

                 Pôvodne bolo na pláne syrové pečivo. Nuž, ale v poslednom období, rovnako ako v tom volebnom, sa akosi nechce a nechce dariť. Vraj je vinovatý pokles kvality taveného syra, ale ja si myslím že je to hlavne preto, lebo sa málo modlíme.  Ba priam, že až vôbec nie. Ani linecká cestovina to nebude. Dnes vystúpi na piedestál a užije si svojich päť minút slávy lístkové cesto.   Začneme pekne od podlahy, lebo už to malo byť dávno hotové, kým sa tu ja vykecávam. A pre istotu v opačnom garde, lebo keď prídete na koniec, tak si nepamätáte čo bolo na začiatku. Nech je to raz hore nohami.   Slané lístkové tyčinky (ďalej v texte: to)      Ako vidíme, dielo sa nám vydarilo. Takto pekne naaranžované to - vlastne voľne položené na hocičom, majú veľmi krátku životnosť. Najkrajšia vďaka pre pekára sú zlostné pohľady a zúrivé výčitky stolovníkov, že toho bolo tak málo. A pritom toho bolo naozaj dosť. Lenže to je taká istá pliaga ako zemiakové lupienky. Začnete s prvým a skončíte s posledným.      Už je to hotové, tak to treba vybrať z útrob sálača tepla. Fakt už nemá žiaden zmysel, aby sme to dlhšie naťahovali a čakali na niečo, alebo na niekoho.      Názorná ukážka správania sa objektu približne v polovičke časového intervalu, zvoleného nami na tepelnú úpravu potraviny.      Po ruke máme rôzne druhy zdrojov tepla. Napríklad hrejivé partnerské slovo, lúče zubatého jarného slniečka, spaľujúce pohľady susedské podporené žmurkaním a iné. My zvolíme z kategórie iné - mikrovlnnú rúru. Samozrejme bez mikrovĺn, iba teplovzdušný ohrev. To veľké sú minúty a malé teplota.      Po povrchu jemne s citom rozhodíme rascu a akurátne posolíme. Aby dobre bolo.      Teraz nastáva vhodný okamih na vrstvičku syra. Syrujeme stredne. Ani veľa, ani málo. Jedzte syry, budete mať sily!      Cestové slíže potrieme rozšľahaným vajíčkom. Nech sa to potom ligoce.      Predprípravná fáza delenia je ukončená. Prúžky sú široké 20 milimetrov a hrubé od výroby. Šírku nemusíte vymeriavať, stačí aby boli približne rovnaké. Inak by to širšie ešte nebolo a to užšie by už nebolo vhodné na konzumáciu. Z titulu pripekania produktu k plechu, použijeme papier na pečenie. Plech najprv pofŕkame vodou, lebo inak je to hrôza preveliká. Všelijako by sa nám vykrúcal, roloval späť, neplnil svoju funkciu a celý projekt by takto stál na hlinených nohách. Germa moja ľúbezná, vďaka Ti za túto myšlienku!      Cesto je vybalené, rolka odvinutá a tým to zhaslo. Nič viac s ním nerobíme. Najprv ho načneme pozdĺžne.      Vajíčka sme rozbili do vhodnej nádoby a vidličkou (tak to robím ja) vyšľahli do takejto homogénnej podoby.      Postrúhali sme syr. Nič viac k tomu nemám, tu je škoda slov.      Pracovný sersám II: miska na syr, strúhadlo na syr.      Pracovný sersám I: naberač hotového produktu, lineár školodobrovoľnej ratolesti, vyrezávač cesta vrúbkovaný (nie je podmienka) a husie brko na potretie zo silikónu.      Čo sme potrebovali: lístkové cesto z Lídla. Je naozaj najvhodnejšie. Hovorte si čo chcete, ale je to tak. Komu by sa chcelo, tak nech si urobí vlastné, domáce. Mne sa nechcelo, také aké som kúpil by som aj tak nedokázal. Absentovať by nemal tvrdý syr na strúhanie, slaná soľ, rasca celá, vajíčka podľa objemu výroby.   A teraz postupujeme pekne od tohto bodu až na začiatok.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (50)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Bobuľka a gaštanko
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Uhrovec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Jankov vŕšok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Kolo, kolo mlynské
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Vodná šlapačka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Múdry
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Múdry
            
         
        igormudry.blog.sme.sk (rss)
         
                        VIP
                             
     
          

 Ešte stále strojár. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    176
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1888
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko moje rodné
                        
                     
                                     
                        
                            Nemecko moje prechodné
                        
                     
                                     
                        
                            Pokusy o veselosti
                        
                     
                                     
                        
                            Domáce úlohy
                        
                     
                                     
                        
                            Receptárium
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rolling Stones
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Je z čoho vyberať.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotky
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




