

  Nechce sa mi byť ako všetci, vstávať o pol ôsmej, vypiť svoju kávu a uháňať do nemrazivého rána. Budem radšej snívať o ňom, ale nemôžem sa prinútiť byť viac zasnená. Do oldies zvukov hrám svoje predstavy, tak skutočné, až je nezvyčajné ich vymýšľať v takých variáciách. Nahnevať niekoho, potom rozmýšľať, prečo nútim niekoho do niečoho môjho. Tešiť sa z užívania si, z plánovania hriechu, ktorý ma možno prekvapí a nestane sa. Má prísť už vo februári, rozvíriť ma zo skutočnosti a bláznivo ma rozosmiať. Už ma aj rozplakal, ale nebol to ten skutočný hriech. Mal svoje meno a netýkal sa mňa, ale nikoho tak nezasiahol. Zmenil mi život, vyburcoval ďalších, zostarla som o roky mladšie ako ja. Smejem sa mu, cítim zadosťučinenie, pochybujem o jeho úprimnosti. Sladko ma rozplače, spievam mu, neviem to, chcem ho odniesť, ale nezničiť. 
 
 
Keby som sa so všetkými takto cítila, nemusela hovoriť, čo mu chýba. Prečo sa v každej kuchyni najviac cítim ako doma? Neviem sa schúliť na deke, zaspať na vankúšoch, ale dokážem sa ľahko zabývať, tam kde sa môžem realizovať. Postarať sa o iných, nechať prebudiť materinský cit, potrebovať pomoc muža v chlapských veciach. Detičky v Jeho prítomnosti sú rajom na zemi. Objímam sa, túlim, fyzický kontakt a idem...Chytíme sa za ruky, niečo mi vyčíta, nepočúvam, mám svoju lásku, živšiu ako on. Búšenie srdca mi pripomína Dirty dancing, tancujem, učí ma to, nemá to rád, no ako povieš drahá (miláčik). 
 

