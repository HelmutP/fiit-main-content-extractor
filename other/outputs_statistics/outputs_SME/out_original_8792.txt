
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Telefón, no predstavte si... 

        
            
                                    14.6.2010
            o
            11:21
                        (upravené
                14.6.2010
                o
                11:31)
                        |
            Karma článku:
                4.90
            |
            Prečítané 
            1103-krát
                    
         
     
         
             

                 
                      Ba môžem začať aj celkom inak, pragmatickejšie, otázkou či rovno konštatovaním – no kto by to povedal? Lebo už je to naozaj tak, dnes sa stal telefón niečím celkom iným, ako to bolo voľakedy a som si istý, že to má na svedomí používanie jeho moderných verzií, teda mobilov. Jasné, to je veľmi plytké rozdelenie núkajúcich sa možností a variácií, no v každom prípade sa aj takzvaná pevná linka už dávno a spoľahlivo odstrihla od v pohybe brániacej šnúre, ktorá nás naozaj obmedzovala. Rozšírim toto tvrdenie, ona nás brzdila v rozlete a keď napríklad doma telefonujem, súčasne vykonávam toľko sprievodných činností, koľko potrebujem. A verte mi, je to počuť, dá sa to nielen vycítiť, či veci na seba prezradiť, lebo presne takýto objav som urobil vari pred mesiacom. Pekná udalosť, poučná a doteraz aj s pokračovaním, ba ešte aj ináč, stála za to!
                 

                     Pre vlastné pohodlie a šetrenie som sa už dávno rozhodol nosiť bradu a fúzy. Solídna ozdoba staršieho pána, ak sa nenecháte spustnúť ako dažďový prales a tak si raz za mesiac nechám hlavu obísť strojčekom s trojmilimetrovým nástavcom a dva razy za rovnaký čas sa venujem fúzom, na tie potrebujem nožničky a svoje staré holiace potreby. Vtom najzložitejšom okamihu, keď som si strihal končeky fúzov nad hornou perou ma vyrušilo zvonenie telefónu. Dopoludnia, pred desiatou, no uznajte, to nevyzeralo na nič osobné, tak som si svoj prístroj do kúpeľne pred zrkadlo priniesol, prepnul som si hlasný režim prevádzky a ohlásil som sa svojim spôsobom – počúvam.   Verte mi, hlások to bol naozaj príjemný, taký žensky zrelý, no zamatový, vnucujúci sa, odmietajúci už vopred prerušenie hovoru z mojej strany a vraj prieskum, mohla by som, dovolíte... To viete chlapská zvedavosť zvíťazila, tak neprerušiac činnosť s nožničkami som na ponuku pristal – nech sa páči!   Jój, vy sa holíte? To iba teraz vstávate, vy sa teda máte, vymeňme sa – zašvitoril hlások a naozaj ma prekvapil jasnovideckými schopnosťami. Ako tá žena môže vedieť, čím sa zaoberám a tak som sa spýtal, či má telefón aj s televíznou kamerou, lebo ja fakt nevysielam. Zachichotala sa, už koketne, oznámila mi, že je kreatívna a má fantáziu, že by som sa aj čudoval a podľa reči vycíti mimiku. na tvári toho, s kým telefonuje. A ja vraj akurát teraz pracujem so strojčekom niekde pod nosom, lebo to bolo v hlase cítiť veľmi preukazne. No, mala som pravdu, s troškou vyzývavosti v hlase položila otázku a tak som jej vysvetlil, že iba trošku. Strihám si končeky fúzov nad hornou perou, aby sa netlačili do úst s každým sústom, ale peru veru špúlim, takže uhádla.   Uspokojujúco sa zasmiala a potom už odborne a plynule začala svoju prácu. Spovedala ma o produktoch mojej banky, čo využívam, čo nie, čo potrebujem a čo si ani nevšímam, ako ma oslovuje možný nákup cenných papierov či akcií a tento pracovný rozhovor bol celkom príjemný, lebo v určitej chvíli ma to rozveselilo. To presne vtedy, priznávam, keď začala o tých cenných papieroch a ponuke akcií, čo sa rozbehla už aj na bratislavskej burze a je možné byť na jej terminály on line a tak som svoj účet špecifikoval upresnením – vážená, ja mám účet senior konto.   Predstavte si, vôbec ju to nezaskočilo a dovetkom, že k tomu sa ešte vrátime svoje poslanie zavŕšila poslednou sériou otázok. Dozvedela sa teda nielen názov toho konta, ale aj dokončené vzdelanie, počet ľudí v domácnosti a v mojej starostlivosti a veru aj vekovú kategóriu. Bola ohľaduplná, celkom jej stačili iba rámcové hranice a po chvíli neformálneho ukončenia priebehu ankety som dostal osobnú otázku.   Chodievate von, do mesta, na prechádzky? A na kladnú odpoveď prišla ponuka - príďte dnes o sedemnástej na roh k Bosákovej banke, čo poviete, dá sa to?   Nuž neviem, či aj teraz vycítila mimiku na mojej tvári, lebo celkom iste mi „spadla sánka“, ako sa hovorí a keď som po maličkej chvíli – jasné, musel som zavrieť ústa – prisvedčil, hneď som sa spýtal. A ako vás spoznám?   Zasmiala sa, už ako herečka, flirtuje, uhádol som, no dozvedel som sa, že bude v bielo – čiernom kostýme, taký Eckhardt, ak si pod tým niečo viem predstaviť, je „žiarivá blondínka“, to zdôraznila a určite sa za ňu nebudem hanbiť! Už sa teším, dovidenia o sedemnástej!   No, milí moji a potom že telefón nie je čertov vynález? Nechcem nič preháňať, ale zatiaľ je to všetko tak, ako to má byť, v románoch aj v živote. Všetko sa vyvíja a stupňuje, je tu gradácia a ešte stále sa z toho iba tešíme a prekvapuje nás to. A to som chcel pred niekoľkými rokmi pevnú linku zrušiť, veď mi stačí mobil, no keď som akurát ten venoval svojmu vtedy ešte len prváčikovi, lebo otravoval v tých najneuveriteľnejších chvíľkach, pevná linka mi zostala. Obyčajný telefón, ale predstavte si...             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




