
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Mikloško
                                        &gt;
                Nezaradené
                     
                 Čože je to 50-ka (najmä s krúžkom)? 

        
            
                                    15.3.2010
            o
            21:42
                        |
            Karma článku:
                13.39
            |
            Prečítané 
            3919-krát
                    
         
     
         
             

                 
                    Dnes sa zverejnili kandidátky politických strán k voľbám. Valné zhromaždenie Združenia kresťanských seniorov Slovenska (ZKS) navrhlo a Rada KDH odsúhlasila na č. 50 kandidátky KDH predsedu ZKS, t.j. mňa. Miesto za pecou, by som mal sedieť v parlamente. Stačí k tomu 6000 krúžkov okolo č. 50, čo nie je veľa. Sľúbil som, že ak sa to podarí, budem hájiť záujmy všetkých seniorov a každý mesiac dám z poslaneckého platu 20% na činnosť ZKS. Prečo som na túto výzvu povedal áno?
                 

                 
  
  -  Dres, ktorý nosíte 20 rokov, sa nemení ako ponožky. KDH je stranou slušných ľudí, nemá za sebou privatizačné, korupčné a klientelistické aféry. -  Seniori sú ohrození demografickou a finančnou krízou, ktoré vznikli z deficitu morálky. Navrhnúť riešenia pre seniorov môžu najmä oni, v parlamente ich je málo. -  Zlú situáciu seniorov ovplyvňujú aj trendy v pôrodnosti, rozvodovosti a  chamtivosti, ktoré prispievajú k vylúčeniu seniorov zo spoločnosti. -  Aj seniori sa musia pliesť do politiky, kto je nepolitický, je nekresťanský. Aj Mojžiš, Abraham, Adenauer a Ratzinger úspešne prijali svoje úlohy až vo vysokom veku. Európa starne, klesá pôrodnosť, iba kvôli prisťahovalcom nevymierame. 78% rastu populácie EÚ, ktorá presiahla 500 mil., sú cudzinci. Koncom 21. stor. bude v Európe na 1 Európana 14 Neeurópanov. Priemerný ľudský život sa ročne predlžuje o 3 mesiace, dôchodcovia sú aktívni aj 20-30 rokov. Tretina obyvateľov EÚ sú seniori starší ako 65 (údaje sú zo správy Vývoj rodiny v Európe, Ústav pre rodinnú politiku, Ženeva, 2009.) Populácia EÚ viac ako 65 ročných už prevýšila počet menej ako 14 ročných. Za 15 rokov sa stratilo 10,5 mil. mladých a pribudlo 16,5 mil. starších ako 65. Ročne sa v porovnaní s r. 1980 narodí v EÚ o 774 tis. detí menej. Slovensko je na poslednom mieste v EÚ v počte deti na rodinu - 1,25 (priemer je 1,38, najviac má Írsko - 2,1). Každých 25 sek. sa v EÚ vykoná potrat, od r. 1999 sa nenarodilo 28 mil. detí, akoby 10 štátov EÚ (včítane Slovenska) zmizlo z mapy. Blíži sa čas, keď každý penzista bude mať človeka, platiaceho na jeho penziu. Počet manželstiev v EÚ sa od r.1980 znížil o 725 tis. (na Slovensku o -34%). Rozvodov je o 358 tis. viac, každé druhé manželstvo sa rozpadá, rozvod je každých 30 sek. Za 10 rokov sa v Európe rozviedlo 10,3 mil. manželstiev, na čo doplatilo 17 mil. detí. 36,5% detí, 2 mil. ročne, sa rodí mimo manželstva. Zo 4 domácností EÚ v jednej žije samotár, v dvoch z 3 nemajú deti, 33% má jedno dieťa. Z nákladov 13 Euro na sociálne veci v EÚ iba 1 Euro príde do rodiny. Šesť štátov EÚ má náklady na rodinu väčšie aké 3% HDP, priemer je 2,1%, Slovensko dáva 1,2%.  Táto situácia, ktorú by si mali všimnúť aj cirkvi, prináša nové výzvy, hľadanie novej, generácie presahujúcej rodinnej, sociálnej a vzdelávacej politiky. Starí majú skúsenosti, vzdelanie a historickú pamäť, bez nich budeme opakovať chyby minulosti. Mladí majú odvahu, vedia reči, ľahko si osvojujú počítače a nové technológie, sú však neskúsení. Iba medzigeneračná spolupráca a solidarita  prinesie zlepšenie. Dôsledky tejto tichej „kontrarevolúcie“ treba riešiť aj na Slovensku. Problémy seniorov sú najmä tieto: - nízka zamestnanosť seniorov, po 50-ke sa práca hľadá ťažko, - princípy slobodnej voľby pri poskytovaní sociálnych služieb sú v rozpore so zákonom, - rast cien v zdravotníctve, najmä v zubárstve, vylučuje seniorov z tejto starostlivosti, - slabé poskytovanie domáceho ošetrenia a zariadení pre dlhodobo chorých, - chýbajú programy pre seniorov vo verejnoprávnych médiách, - krivda v penziách do 1.1.2004 pretrváva, penzie sú socialistické a ceny svetové, - priemerná výška dôchodkov k priemernej mzde má klesajúcu tendenciu, - penzia zohľadňuje iba odvody, nie prácu v domácnosti a počet vychovaných detí platiacich odvody, ktorými by mali priamo prispievať na penziu rodičov. Celoslovenská organizácia Združenia kresťanských seniorov Slovenska sa snaží o zmysluplné prežitie života seniorov a ich dôstojné začlenenie do spoločnosti. Chceme, aby seniori, napriek problémom, žili tvorivo a radostne, aby sa rozvíjala mnohogeneračná spolupráca a život viacgeneračných rodín. Združujeme asi 4600 seniorov v 62 kluboch, ich náplňou sú prednášky, školenia, kultúra, púte, šport, zájazdy. Máme dobré medzinárodné styky, sme aktívni členovia Európskej únie seniorov. Máme svojho zástupcu v Rade vlády pre seniorov a v jej komisiách. Pripomínam ešte číslo 50 a heslo: Seniori všetkých regiónov Slovenska, spojte sa!

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (110)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Mladí, príďte do Prahy, na stretnutie Taizé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Ďalší škandál vo verejnom obstarávaní
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Spomienka na tých, čo odišli do večných lovísk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Interpelácia na premiéra Fica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            11. Kalvárske olympijské hry - fotoreportáž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Mikloško
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Mikloško
            
         
        jozefmiklosko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Mám dve na tretiu krát tri na druhú plus dva rokov (bŕŕŕ), 49-ty rok ženatý, štyri deti, jedenásť vnukov. Do r.1989 som bol vedec - matematik, potom politik, poslanec, prorektor, publicista, veľvyslanec v Taliansku (2000-2005), spisovateľ. Od r.2005 som dôchodca, vydavateľ najmä vlastných kníh (7), hudobno-dramatického CD-čka Ona, Ono a On, predseda Združenia kresťanských seniorov Slovenska. Pravidelne športujem, počúvam klasiku a rád sa smejem. Zabudol som, že od 11.3.2012 som sa stal poslancom NR SR, takže som zasa politik. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    216
                
                
                    Celková karma
                    
                                                7.77
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ďalší škandál vo verejnom obstarávaní
                     
                                                         
                       Tento november si už Paška zapamätá
                     
                                                         
                       Spomienka na tých, čo odišli do večných lovísk
                     
                                                         
                       Interpelácia na premiéra Fica
                     
                                                         
                       Obchod s ľudskými orgánmi - zbraň najťažšieho kalibru v informačnej vojne o Ukrajinu
                     
                                                         
                       Písať o tom, čo je tabu
                     
                                                         
                       Rozvod a cirkev
                     
                                                         
                       11. Kalvárske olympijské hry - fotoreportáž
                     
                                                         
                       Holokaust kresťanov v Iraku a Sýrii sa nás netýka?
                     
                                                         
                       Som katolíčka. Nejdem príkladom.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




