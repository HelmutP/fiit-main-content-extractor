

 
V sobotu večer sme sa s priateľkou trochen nudili, tak sme sa rozhodli, že by sme si po dlhom čase mohli pozrieť telku. Keďže UPC – základný súbor veľké možnosti neponúka, rozhodol som sa pre Markízu v naivnom domnení, že by tam o tomto čase (t.j. asi 20,15 hod. SELČ) mohol byť snáááď nejaký film, alebo čo. Ale prd, ja naiva. 
 
 
 
 
 

 
 
Dávali Smotánku. Erika Judínyová robila reportáž z Cat Girl. Cat Girl je show program, tzv. obchod s bielym mäsom, niečo ako Miss. V podstate sa to aj mohlo volať Miss, ale mať tri missky namiesto dvoch by bolo čo? No predsa zbytočné. A ja som sa znovu dostal do toho podivného stavu, že som čumel s otvorenou hubou neschopný pohybu. 
 
 
 
 
Na tejto šou bola vraj najväčším hosťom Pámela Andersón. Ty koki, to táto polovyfučaná nafukovacia anča s dvoma riečnymi balvanmi namiesto poprsia ešte niekoho zaujíma? Sledujúc Smotanu som zistil, že asi hej, lebo všetci z nej boli napichaní, ako keď sme si ešte ako malí gaštančekovia v poslednom ročníku na základnej škole leštili bambusy pri tretej sérii Baywatchu. Ale to už bolo kua dávno! 
 
 
 

 
Najprv moderátorka Erika spovedala tri bloncky sediace pri stole. Bola tam nejaká Dúbravská, potom najznámejší milionársky matrac Mary-Ann Ďurianová a nejaká tretia, ktorá nič nehovorila, len sa rehlila. Dúbravská sa rozplývala, aká je Pámela fasa a že tam bola aj Gábi Drobová, ktorá vysvetľovala Pámele, že čo tie ženské robia a ták. Tá Drobová musí mať niečo do seba, lebo ja by som nevedel, čo taká Ďurianová robí ani nakresliť na papier, nieto ešte vysvetľovať to po anglicky a navyše Pámele Andersónovej. Ale vysvetlenie mojej najobľúbenejšej slovenskej transky Drobovej, rodenej Ďurovkovej bolo asi tiež dosť naprd, lebo Matrac hneď nato spomenula, že Pámela im za to aj zatlieskala. 
 
 
 
 

 
Potom sa Erika presunula k trochu zaujímavejším ľuďom ako Kramár, Dangl a ešte... hm... no ešte... henten... a tak. Inak neviem, či ste si to všimli, ale Erika zhruba každých tridsať sekúnd používa také divné vsuvky, ako že sa s tým človekom, s ktorým sa bavila pred chvíľou ešte rozpráva, aj keď ten je už dávno v pč. A robí to takým špecifickým hlasovým a verbálnym prejavom, asi niečo medzi nevtieravou lascívnosťou skúsenej pracovníčky erotickej linky a matky komunikujúcej so svojím ťažko mentálne postihnutým dieťaťom. Vyzerá to takto: Erika sa rozpráva s Kramárom, potom je záber ako Kramár ide preč a Erika hovorí: „Ááále Maroško, kde si nechal svoju manželku, hmm?“ Je to fascinujúce. Človek má chuť si ľahnúť, upadnúť do katatonického stavu a nechať sa tým hlasom odnášať až na planétu Imbecílius. Ach, rozprávaj Erika, ešte... ááách..... chŕŕŕŕ..... 
 
 
 
 
„Ááále, Ľuboško, nemal by si dopísať ten článoček, hmm?“  

 
„Ježiši, Erika, máš pravdu, už pokračujem. Ty tu zatiaľ seď, nič nehovor a klipkaj očami. Alebo vieš čo? Tu máš vatové tampóny, papaj.“ 
 
 
 
 

 
Takže pokračujeme. Po skončení cat-whore sa všetci celebriti pobrali na afta-party (afta = drobná bolestivá erózia alebo vriedok na ústnej sliznici Zdroj: http://www.cudzieslova.sk) do najsamluxusnejšieho bratislavského podniku kaviareň Bystrica, ehm, teda UFO. To je podnik, kde sa sedí na takých mini-taburetkách a pije z takých mini stolov, asi ako v Čajovni u zhuleného hrocha, akurát je tam drahšie. A tam to žilo, lebo prišla aj Pámela. Všetci slopali len to najdrahšie šampanské Chateau de Snob a vodku značky Stokurjev. Erika samozrejme ďalej vykonávala svoju „prácu“. Klofla aj najznámejšieho česko-slovensko-nemeckého pseudomilionára Hansiho Novotného, ktorého som najprv ani nespoznal, jednak preto, lebo rozprával akoby po slovensky a dvojak nemal na sebe svoj obľúbený sveter so symbolmi vagíny. Toho sa pýtala, ako ide biznis, on odpovedal, že dobre (asi chcel povedať „Tie tři mega, čo sem dostal za Nevěstu pro Milionára som ještě úplne nerozbil.“). Potom sa Erika spýtala Martikána, že ako sa zabáva, on že „dobre“ a išiel preč. Hehe, Martikán je cool, pomyslel som si. Ale Erika to zabila, keď na margo odchádzajúceho Martikána povedala, že „Keby skromnosť bla bla bla, tak by niečo bla bla neviemčo.“ Veru, sám by som to lepšie nepovedal. 
 
 
 
 

 
Potom sa konečne presúvame k zlatému klincu večera – Pámele, ktorej sa všetci pchajú do riti, až majú hnedé hlavy. Erika hovorí, aká je Pámela super a fasa a tak. Ale že ani megahviezde ako Pámele sa nevyhne sem-tam nejaké fó-pá. Erika upozorňuje, že Pámela má pod pazuchou niečo zvláštne – kamera sa približuje k inkriminovanému miestu – divák tŕpne a pýta sa: „Čo tam bude? Obrovská potná škvrna? Semeno? Krv? Zvratky? Hnis?“ Už to napätie nevydržím, chcem to vedieť! Panebože, veď to je... dierka na jej vlnenom oranžovom overale!!! Dierka veľkosti jedného pixela na 17-palcovom LCD monitore!!! Haha, ešteže nie som na jej mieste, uľavene som si vydýchol. Ale Erika našťastie hneď zachraňuje situáciu a upozorňuje, že napriek svojmu pokročilému veku, Pámela vôbec nemá celulitídu. A kamera zároveň eroticky švenkuje po Pámelinom odhalenom stehne. Fakt, žiadna celulitída. Lenže tentoraz vyhral pozorný divák a vie, že Pámela má celulitídu, akurát sa Erika zamerala na zlú časť tela. Mala sa Pámele pozrieť radšej do ksichtu. 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 

 
Potom už nasledujú len lyrické zábery na škeriacich sa celebritov, ako sa držia fšecja okolo pliec a fotia sa spolu pred oknom s výhľadom na nočnú Bratislavu. A ja tak rozmýšľam – keby to okno tak prasklo a vietor by ich vysal von, drbol s nimi na Nový most, kde by ich  pre istotu prešiel nočák, idúci do Vozovne Petržalka a smetiarske auto - stúplo by celoslovenské IQ o 20 percent, alebo o viac? 
 
 
 
 
„Ááále Ľubko, takto predsa nemôžeš rozmýšľať. Chceš, aby ti zase povedali, že si zlý človek? Hmm?.“ 
„Kua, Erika, drž už hubu!“ 
 
 
 

 
 
 


