

 
    K Všeobecnej deklarácii ľudských práv môžeme mať rôzny
postoj a ja sám mám výhrady k niektorým článkom, ktoré podľa môjho
názoru nedefinujú „práva“, ale sociálne požiadavky. Napriek tomu je tento
dokument mimoriadne významný, pretože to bolo prvýkrát v dejinách, keď sa
viacero krajín dohodlo na tom, že existujú univerzálne práva, ktoré platia
naprieč hraníc medzi štátmi a pre všetkých ľudí „bez ohľadu na odlišnosti
akéhokoľvek druhu, ako sú rasa, farba pleti, pohlavie, jazyk, náboženstvo,
politické a iné presvedčenie, národný alebo sociálny pôvod, majetok, rodové
alebo iné postavenie.“ (Článok 2)
 
 
    Napĺňanie Všeobecnej deklarácie ľudských práv v praxi
nebolo jednoduché ani v krajinách s dlhoročnou tradíciou demokracie.
V USA sa niektoré ustanovenia, týkajúce sa rovnosti bez ohľadu na rasu,
uviedli do praxe až v polovici 60-tych rokov a napríklad
v Švajčiarsku získali ženy právo voliť až v roku 1971 (na federálnej
úrovni), v regionálnych voľbách v kantone Appenzell Innerrhoden toto právo
ženy získali dokonca až v roku 1990.
 
 
    Napriek tomu však môžeme bez prehnanej skromnosti tvrdiť, že
v dodržiavaní ľudských práv je „západná civilizácia“ o pár krokov pred
ostatným svetom. Ak však veríme v univerzálnosť ľudských práv, nemali by
sme sa uspokojiť tým, že v našich krajinách s ich dodržiavaním nie sú
zásadné problémy (pretože tvrdiť, že
nie sú žiadne problémy, žiaľ nie je
na mieste).
 
 
    Vo svete sú desiatky krajín v ktorých sú ľudské práva
a občianske slobody hrubo porušované. A kto iný ako demokratické
krajiny, rešpektujúce ľudské práva a občianske slobody ako aj princípy
právneho štátu, by sa mali zaujímať o situáciu v iných krajinách.
Napríklad v Číne, v Severnej Kórei, v Barme, na Kube,
v Bielorusku, v Turkménsku, v Uzbekistane, v Kazachstane, v Iráne,
v Líbyi,
v Sudáne (Darfur) alebo v Zimbabwe (to nie je samozrejme vyčerpávajúci
zoznam krajín, ktorých vlády a vládcovia porušujú ľudské práva).
 
 
    Lenže čoraz častejšie sa stáva, že demokraticky zvolení
politici robia ústupky z týchto princípov kvôli ekonomickým záujmom.
Naposledy sa takto prezentovala Európska únia. V snahe zlepšiť svoje
ekonomické postavenie na africkom kontinente, ktoré sa dostáva pod tvrdý konkurenčný
tlak zo strany Číny, pozvala na Summit EÚ-Afrika v portugalskom Lisabone všetkých
najvyšších predstaviteľov afrických krajín. Vrátane prezidenta, teda skôr
diktátora, Zimbabwe Roberta Mugabeho. Napriek tomu, že v roku 2002 na neho
EÚ uvalila zákaz vstupu na územie únie, kvôli zmanipulovaniu prezidentských
volieb. Lenže predstavitelia ostatných afrických krajín sa za Mugabeho, ktorého
vnímajú ako symbol boja proti kolonializmu, postavili a pohrozili, že na
Summit neprídu, ak nebude môcť prísť aj prezident Zimbabwe.
 
 
    Áno, je ťažké konkurovať komunistickej Číne, ktorá bez
problémov obchoduje a investuje v ktorejkoľvek africkej krajine,
pričom vládcom danej krajiny nevyčíta porušovanie ľudských práv, ktorých sa
sami čínski komunisti dopúšťajú vo svojej vlastnej krajine. Ale klesnúť na
úroveň Číny v snahe vyrovnať „konkurenčnú výhodu“ čínskych komunistov je
nedôstojné pre demokraticky zvolených politikov. A nemorálne. Česť EÚ
zachraňovala Veľká Británia, ktorej predseda vlády Gordon Brown ako aj minister
zahraničných vecí David Miliband sa odmietli na Summite zúčastniť. Miliband
toto rozhodnutie odôvodnil slovami: „Bolo by absurdné pre premiéra alebo pre
mňa, sedieť vedľa Roberta Mugabeho počas diskusie o dobrom vládnutí
a ľudských právach a tváriť sa, že v Zimbabwe neprebieha absolútny
rozpad.“ Mimochodom, tento postoj labouristických politikov ocenili aj opoziční
konzervatívci.
 
 
    Zo zvyšných dvadsiatichšiestich krajín EÚ, ktoré boli na
Summite zastúpené premiérmi alebo ministrami zahraničných vecí, sa väčšina
vyhla výraznejšej kritike Roberta Mugabeho a jeho režimu. Z toho, čo
diplomati „vyniesli“ spoza dverí pre verejnosť (aj médiá) uzavretých rokovaní,
sa zdá, že „ústa
si otvorila“ iba nemecká kancelárka Angela Merkelová a predstavitelia Dánska,
Švédska a Holandska. Aspoň podľa toho, že Robert Mugabe vo svojom prejave
na Summite označil
uvedené krajiny ako arogantný „štvorspolok“. Osobne mi je ľúto, že sa
k týmto krajinám nepridalo aj Slovensko, ale po nedávnom postoji
najvyšších slovenských predstaviteľov ku kandidatúre Kazachstanu na post
predsedajúcej krajiny OBSE som to ani nečakal.
 
 
    No a nakoniec. Čo Európska únia zo svojho servilného
postoja k africkým predstaviteľom vyťažila? Nič.
Lídri Európskej únie a Afriky síce podpísali na záver dvojdňového Summitu všeobecnú,
formálnu a nič neriešiacu dohodu o strategickom partnerstve dvoch
kontinentov, ale ten najdôležitejší dokument, kvôli ktorému sa celý tento
cirkus zorganizoval, teda Európske partnerské dohody o voľnom obchode medzi
EÚ a jednotlivými africkými krajinami ostali nepodpísané.
 
 
    Jediný víťaz je teda Robert Mugabe, ktorý sa napriek zákazu
cestovania do EÚ na územie únie nakoniec predsa len dostal a dokonca si
posedel za okrúhlym stolom s predstaviteľmi európskych demokratických
krajín. Čo nepochybne prostredníctvom štátom (teda ním) ovládaných médií
použije na posilnenie svojej legitimity („však sa pozrite, že ma demokraticky
zvolení európski politici berú ako svojho partnera a legitímneho
predstaviteľa Zimbabwe“) ako aj na svoju oslavu ako „neohrozeného ochrancu záujmov
ľudu Zimbabwe a všetkých Afričanov“.
 
 
    Náš Robert sa svojmu menovcovi našťastie zatiaľ nevyrovná,
aj keď marxistický základ majú spoločný. Ale aj nášmu Robertovi vadia niektoré
Ústavou zaručené práva. Naposledy právo na majetok, ktoré Robertovi prekáža pri
splnení nepremysleného sľubu o dostavaní diaľnice z Bratislavy do
Košíc do konca volebného obdobia. A tak jeho minister pripravil návrh
zákona o vyvlastňovaní súkromného majetku, ktorý porušuje Ústavu Slovenskej republiky:
 
 
„Každý má právo vlastniť majetok.“ - čl. 20 ods. 1 Ústavy
Slovenskej republiky
 
 
„Vyvlastnenie alebo nútené obmedzenie vlastníckeho práva je
možné iba v nevyhnutnej miere a vo verejnom záujme, a to na základe zákona a za
primeranú náhradu.“ - čl. 20 ods. 4 Ústavy SR
 
 
„Ľudia sú slobodní a rovní v dôstojnosti i v právach.“ - čl.
12 ods. 1 Ústavy Slovenskej republiky
 
 
„Pri obmedzovaní základných ľudských práv a slobôd sa musí
dbať na ich podstatu a zmysel.“ - z čl. 13 ods. 4 Ústavy Slovenskej republiky
 
 
    Mimochodom vo Všeobecnej deklarácii ľudských práv sú aj
takéto ustanovenia:
 
 
„Všetci sú si pred zákonom rovní a majú právo na rovnakú
zákonnú ochranu bez akejkoľvek diskriminácie.“ (Článok 7)
 
 
„Každý má právo vlastniť majetok tak sám, ako aj spolu s
inými. Nikoho neslobodno svojvoľne zbaviť jeho majetku.“ (Článok 17)
 
 
„Každý má právo na účinnú ochranu príslušnými vnútroštátnymi
súdmi proti činom porušujúcim základné práva, ktoré mu priznáva ústava alebo
zákon.“ (Článok 8)
 
 
    Salámová taktika je najnebezpečnejšia. Nenechajme si
postupne „okrájať“ svoje občianske práva!
 

