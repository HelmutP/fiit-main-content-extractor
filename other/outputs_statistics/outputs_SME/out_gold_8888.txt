

 Bébé: 
 Haló, Marek? Tu Bébé, čau! 
 Marek: 
 No nazdar, Bébé, pozeral si zápas? 
 Bébé: 
 Noó... to čo bolo? Prečo ste nebojovali až do konca. 
 Marek: 
 No jednoducho smola... 
 Bébé: 
 Ja som si myslel, že ste väčší bojovníci. Vy ste to nejako flákali celý zápas. 
 Marek: 
 Veď vieš, sme na majstrovstvách prvý krát. 
 Bébé: 
 Mali ste plné gate? Však ste boli o triedu lepší. 
 Marek: 
 Nie nemali, len sme hrali takticky. 
 Bébé: 
 A to čo je za taktika? My tu v Afrike na lúke hráme vždy o víťazstvo. 
 Marek: 
 Však aj my, ale ten tlak nám nedal podať plný výkon. 
 Bébé: 
 Aký tlak? To tie trúbky, vuvuzely? 
 Marek: 
 Ale nie... 
 Bébé: 
 Fúkalo ti príliš do vlasov? Mne sa zdalo, že sa šetríte na Talianov. 
 Marek: 
 Hrali sme naplno. 
 Bébé: 
 Ale viac na vlastnej polovici. Aj si pekne strieľal, ale vždy ďaleko vedľa. Nevidel si dobre? 
 Marek: 
 No to si nejaký expert. Nedalo sa, oni sa bránili. 
 Bébé: 
 Až tak sa bránili, že nakoniec vyrovnali. Čo na takýto výkon povedia u vás na Slovensku? 
 Marek: 
 Asi nás nebudú priveľmi chváliť. No sme radi, že sme tu na majstrovstvách. 
 Bébé: 
 Chlapci tu u nás na lúke vravia, že vy Slováci už nevyhráte nič, že ste slabosi. 
 Marek: 
 Tak im povedz, že sa mýlia. 
 Bébé: 
 Asi im nič nepoviem, budú si robiť so mňa srandu. Ale ten mladý Weis sa im páčil, aj ten gól, hoci trošku z ofsajdu. Potom išli hrať futbal na dvor. Škoda, že ste takí pokakaní. 
 Marek: 
 Ale Bébé! 
 Bébé: 
 Idem aj ja hrať na dvor futbal, tak mi nevolaj. Možno sa ešte niekedy ozvem. Mama mi ale hovorí, že sa mám ísť dať holičovi ostrihať, tak neviem Marek. Tak sa zatiaľ maj! Si tam ešte, dúfam, že si neodpadol... 

