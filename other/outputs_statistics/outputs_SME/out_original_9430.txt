
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Kultúrna obec, na slovíčko! 

        
            
                                    23.6.2010
            o
            10:00
                        (upravené
                23.6.2010
                o
                8:13)
                        |
            Karma článku:
                3.16
            |
            Prečítané 
            749-krát
                    
         
     
         
             

                 
                      Moja radosť, keď som v pondelňajšom SME objavil článok Dariny Károvej, riaditeľky Medzinárodného festivalu Divadelná Nitra v rubrike Publicistika, bola naozaj úprimná. Dôvod je celkom jasný, sféra kultúry je v živote spoločnosti už dlho iba okrajová, aspoň čo sa štátnej moci týka a lepšie to veru nie je ani v tej odbornej, či členskej základni, ak mám použiť terminológiu, v ktorej od seba oddeľujem jednotlivé sféry, ktoré kultúra vo svojej mnohorakosti obsahuje. Pani Kárová je „dáma zo sféry profesionálneho divadla“, čo je v amatérskom rozmere samozrejme aj moja láska, preto sa s jej konaním aspoň jeden raz v roku dostávam do anonymného sporu – to je vždy vtedy, keď v Nitre odovzdávajú Dosky a na moje lásky v DJZ nedovidia – no nepochybujem, že celý ten neuveriteľne bohatý register kultúrnosti Slovenska ovláda a má ho pevne v srdci ako životnú lásku. Ináč by ju totižto ani nenapadlo otvárať takúto čarovnú skrinku, ktorá je od nepamäti viac skromná než plná prekvapení či aktivít a nedivte sa, celkom nemajetná, ba vlastne chudobná či celkom  prázdna.
                 

                     To už je pesimistické konštatovanie, ale odkedy si pamätám, tak tento rezort bol vždy poddimenzovaný, nedocenený a obchádzaný, hoci je dobre viditeľný, všetkým na očiach a ešte aj tá budova ministerstva má takú exkluzívnu polohu, že o nej vie jednoducho každý na celom Slovensku. Treba si uvedomiť, že kultúra, to nie je iba profesionálne divadlo, kdeže, kultúra, to nie je len výtvarný kumšt v svojej mnohorakosti a sieť galérií, čo už prenikli do širokej občianskej obce,  to je architektúra miest a sídlisk, prírody, to je kultúra všetkého, čo robí človeka človekom, čo je humanizmus v tom najširšom slova zmysle, jazykový prejav , knihy, elektronické médiá, editorstvo v tej najširšej možnej diferenciácii, hudobná a spevácka scéna, súťaže, festivaly rôzneho druhu, odievanie, móda, priemysel s tým súvisiaci, dizajnový kumšt, životné prostredie, tanec  a to som ešte nevymenoval ani len zlomok možností, čo nám predstavivosť pri vyslovení slova kultúra ponúka.   Darina Kárová je prísna, hodnotí to čo bolo, čo osobne pozná, lebo sama na MK určitý čas pracovala a spokojná veru nebola! Hodnotí možnosti budúcej vlády a podľa programových téz, s akými vykročili strany do volieb už vie, čo sa dá od nich čakať. Poviem to v predstihu – nič moc, je sklamaná! Darine Károvej sa zdá, že rezort kultúry si vlastne nezaslúži nikto z tých relevantných uchádzačov a  vie, že tak ako doteraz sa tam neurobí vôbec nič dôležité, naozaj nič, čo by znamenalo progres a prinieslo by nám úžitok. Chápem ju aj v tom, ako za to viní kultúrnu verejnosť, teda nás všetkých, že sme apatická masa nesnažiaca sa o žiadnu aktivitu a trápi sa aj nad nedôstojnou výkonnosťou profesionálov v jednotlivých kultúrnych oboroch, lebo sa neusilujú preniknúť presne tam, kde sa o kultúre rozhoduje – teda prinajmenej do legislatívnej sféry činnosti byrokracie štátu! U nás sa vždy reaguje iba vtedy, ak sa niečo udeje na očiach verejnosti .- napríklad zemetrasenie na našej prvej divadelnej scéne – a nesnažíme sa o nič tam, kde začínajú aktivity, kde sa láme chlieb, kde sa rozdeľujú granty a hotové financie, čo je náš najväčší nedostatok.   Mám teda jeden nápad a som si istý, že akurát tam by sa malo začať so zmenami. Bolo by treba „rozvášniť“ všetkých v profesionálnych združeniach, všetkých umeleckých či iných zväzov a odborov kultúrnej obce k aktivitám v tom najširšom možnom zábere a zasypať zložky tvoriace riadenie rezortu návrhmi, nápadmi, žiadosťami, i rozhodnutiami tak, aby nebolo cesty späť! Iniciatíva zhora je skôr befel ako aktivita a ak sa všetko zdola povedané aj poriadne spropaguje, ak to dostane na stôl kultúrna verejnosť národa, bude to presne ten rozmer, z ktorého sa nikto naozaj zodpovedný len tak ľahko nevyhovorí. A my sa nebudeme vyhovárať, budeme iba chcieť, požadovať, iniciovať, poukazovať a kritizovať – chceli sme, navrhli sme, bolo by to skvelé a záslužné, no len sa pozrite, kto to nepovolil!?   Jasné, nie je to nič jednoduché, ale umelecká obec je vynaliezavá a kreatívna. S prehľadom a skúsenosťami z prežitého sa presne vie, čo nám chýba, presne aj to, kde nám to chýba a buďte si istí, že sa to pri troške iniciatívy podarí nielen pomenovať, ale aj rozbehnúť. A potom tu ani nemusí byť niekým riadené ministerstvo so špecializáciou, potom bude stačiť miesto s povinnosťou koordinovať a rozdeľovať potrebný interes v tej najrozmanitejšej podobe akurát tam, kde je to treba a odkiaľ prišiel voľajaký múdry nápad, plán, či aktivita. Teda nesťažujeme sa, kultúrna obec si na Slovensku zaslúži viac, všetko odštartujeme od seba, zdola! Kumšt v každej podobe je ten najkrajší dôkaz ľudskej schopnosti byť prvým a nech je to teda akurát takto! Veď Darina Kárová je toho najlepším dôkazom. Ak tá Divadelná Nitra bol jej nápad, čo považujem za takmer isté, lebo je s touto skvelou aktivitou spojená od čias jej vzniku, potom je akurát to príklad v tom najlepšom možnom „prevedení a balení“ – stačí ho obkresliť a prispôsobiť. Nech je tu z ničoho nič plno festivalov, súťaží, tvorivých dielní, kurzov, vzdelávania, škôl na tisíc spôsobov a vlastne fantázii sa medze nekladú. Umenie, medzi nami kumšt, má naozaj neprebernú škálu možností. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




