
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Frnčo
                                        &gt;
                Právo a politika
                     
                 Dvojité štátne občianstvo vo svetle právnej teórie 

        
            
                                    27.5.2010
            o
            15:17
                        (upravené
                27.5.2010
                o
                21:11)
                        |
            Karma článku:
                3.93
            |
            Prečítané 
            1613-krát
                    
         
     
         
             

                 
                    V minulom článku som písal, prečo považujem zákon Maďarskej republiky za bezvýznamný pre slovenských Maďarov. Dnes sa chcem pozrieť na náš “odvetný“ zákon a jeho prípadnú protiústavnosť a návrh SDKÚ-DS z pohľadu teórie práva.
                 

                 
  
       Znenie ústavy ohľadom občianstva nájdeme v čl. 5 a to v dvoch odsekoch:   (1) Nadobúdanie a stratu štátneho občianstva Slovenskej republiky ustanoví zákon.   (2) Nikomu nemožno odňať štátne občianstvo Slovenskej republiky proti jeho vôli.           Musím poznamenať, že právo sa dá vykladať viacerými spôsobmi, a preto sa názory odborníkov ohľadom protiústavnosti straty občianstva nadobudnutím iného občianstva rozchádzajú.           Poznáme viaceré metódy výkladu práva a to napr. gramatický(jazykový) – vychádza predovšetkým z analýzy slov. Slovo vôľa sa dá tiež vykladať viacerými spôsobmi. Môže sa chápať v tom zmysle, že je jeho vôľa resp. aj voľba určiť si, ktoré štátne občianstvo daný občan chce mať, a to je v súlade s novelou, ktorá prešla v parlamente, ale tak isto môže byť vôľa chápaná ako istá požiadavka, tz. že občan ktorý nadobudol iné štátne občianstvo si chce ponechať aj slovenské občianstvo.       Ďalej právo sa dá vykladať napr. aj teleologicky, tz. s prihliadnutím na účel konkrétneho ustanovenia – účelom ustanovenia, že nikomu nemožno odňať občianstvo proti jeho vôli môže byť, že nikto nemá byť svojvoľne zbavený svojho občianstva ako je to vyjadrené aj v medzinárodných dohovoroch.       Na danú problematiku sa môžeme pozrieť aj z pohľadu historického výkladu. Za predchádzajúceho režimu nepohodlní občania boli zbavovaní občianstva a preto sa tomu chcelo v demokratickom režime zabrániť.       Samozrejme právo pozná aj mnohé ďalšie metódy výkladu, ale mne išlo v prvom rade poukázať nato, že otázka protiústavnosti daného zákona nie je úplne jednoznačná a nakoniec bude iba na posúdení sudcov ústavného súdu ku ktorému výkladu sa priklonia.       V otázke významu nášho zákona sa prikláňam k myšlienke, že tento zákon nemá zmysel, pretože medzi SR a MR je uzatvorená medzinárodná zmluva z roku 1961, ktorá vylučuje dvojité občianstvo slovenské a maďarské.       Návrh SDKÚ-DS bol, aby Slovensko prijalo zákon, na základe ktorého by účinky maďarskej novely neuznávalo. Tento zákon v NR SR neprešiel. Ako som už konštatoval v minulom článku, je výlučnou vnútornou záležitosťou štátu, komu udelí občianstvo a komu nie. Preto takéto ustanovenie sa mi zdá bezvýznamné z pohľadu práva, ale teoreticky môže slúžiť ako politická odpoveď na zasahovanie do suverenity Slovenskej republiky vo vzťahu k vlastným občanom na vlastnom území. Tento zákon by určite nezabránil tomu, aby slovenským Maďarom bolo udelené občianstvo, a to z toho dôvodu, že každý štát vykonáva suverenitu iba na svojom území a jeho predpisy nemajú žiaden právny význam pre cudzie subjekty, ktoré sa nenachádzajú v sfére jeho pôsobnosti. Napriek tomu sa mi zdá toto riešenie lepšie, pretože považujem za nešťastné na provokáciu odpovedať ďalšou provokáciou.       Svoj názor ohľadom významu nášho zákona(predposledný odsek) som poopravil vzhľadom na správne upozornenie v diskusii o existencii bilaterálnej zmluvy upravujúcej dvojité občianstvo medzi SR a MR, ale otázku jeho bezvýznamnosti som nezmenil, len som ju prehodnotil z iného hľadiska. Vychádzal som zo zlého predpokladu, že takáto zmluva nebola uzavretá, keďže v médiach som ju nevidel nikde prezentovanú, čomu sa tiež veľmi čudujem a tento fakt som si neoveril v Zbierke zákonov.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Frnčo 
                                        
                                            Ako zvládať stres? Nielen pre právnikov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Frnčo 
                                        
                                            Čo robiť, aby Vaše konanie netrvalo dlho a čo robiť, ak už trvá dlho?
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Frnčo 
                                        
                                            Roman Frnčo - Bežím za krajšie Jazero
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Frnčo 
                                        
                                            Chcelo sa mi bežať, tak som bežal
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Frnčo 
                                        
                                            Do práce na dvoch kolesách - pre a proti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Frnčo
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Frnčo
            
         
        frnco.blog.sme.sk (rss)
         
                                     
     
         Najbližší mi hovoria, že som protivný, inteligentný a vtipný:) V civilnom živote právnik, v súkromnom maratónec a človek zaujímajúci sa o politiku. Ak mi chcete napísať, využite prosím správu na facebook alebo formulár nižšie. Roman Frnčo 
 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    82
                
                
                    Celková karma
                    
                                                2.88
                    
                
                
                    Priemerná čítanosť
                    1754
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Právo a politika
                        
                     
                                     
                        
                            Viera
                        
                     
                                     
                        
                            Rôzne
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ústredná plánovacia komisia pre kapitalistické ekonomiky
                                     
                                                                             
                                            Zmenšovanie úlohy štátu: lekcie z Nového Zélandu
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Stvorení pre beh (Christopher McDougall)
                                     
                                                                             
                                            Svätá Biblia
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




