

 Je to vlastne celkom jednoduché. Terajšia vláda kradne, lebo videla ako kradnú  Dzurindovci. Tak si povedali, že keď mohli oni, môžeme aj my. 
   
 Dzurindovci zase kradli preto, že kradli Mečiarovci. Keď mohli oni, môžeme aj my. 
   
  Mečiar kradol preto, že po páde „komunistov" malo ísť (skoro) všetko zo štátnych do súkromných rúk. Ale Mečiar sa nechcel len tak vzdať majetku. „Komunisti" ho predsa všetok ovládali (a rozkrádali). Navyše vládli tvrdou totalitnou rukou... Mečiar si teda povedal: „Keď mohli oni, môžeme aj my." 
 „Komunisti" po svojom nástupe k moci samozrejme vedeli, ako tu vládol Tiso a spol. Kradli si pre seba (napr. arizáciou), a vládli ako sa im zapáčilo. No, keď mohli Tisovci, môžeme aj my („komunisti")... 
 Tiso sa zariadil tak ako sa zariadil, lebo videl, ako národ za prvej republiky ožobračujú podnikatelia a vláda. Tak si jednoducho povedal, že za nimi predsa nebude zaostávať. Že keď mohli oni, tak môže aj on. 
 Predstavitelia prvej ČSR vedeli, že za Uhorska bolo horšie, teda môžu spokojne okrádať a robiť, čo sa im zachce. Stačí, ak to budú robiť „trošku menej"(aby oproti Uhorsku vyzerali ako menšie zlo). 
 Keď k nám prišli maďarské kmene, videli, ako to tu funguje - elita vládne, zvyšok na nich robí. Tak to prevzali a niečo na dôvažok prihodili (nejaké tie dane, úrady a šľachtu). Aby si nikto nemyslel, že sú mäkkí. 
 Svätopluk (a ďalší vládcovia Veľkej Moravy) okrádali ľudí a bezmedzne im vládli preto, že to tak odkukali od Sama (franského kupca). Ten si to vedel zariadiť - bol kráľ a mohol si robiť, čo chcel. A keď mohol Samo, môžu aj Veľkomoravania... 
 Samo zase len videl, ako fungujú keltské kmene. Vylepšil ale svoj titul - z náčelníka na kráľa. 
 ... 
 No a takto, drahí priatelia, môžete ísť ešte viac dozadu po priamke dejín... 
 Tak teda vidíte, že naši vládni zlodeji nerobia nič zlé - iba pokračujú v dlhokánskej tradícii...   Tak sa na nich nehnevajte a voľte ich aj tohto roku. Veď oni len dodržiavajú tradície. 
   
 Poznámka autora: Tento text nie je založený na overených historických skutočnostiach, ani na rozhovoroch s čelnými predstaviteľmi spomínaných režimov. Je mierne zľahčujúcim  pohľadom autora na storočia okrádania a nespravodlivosti. 
  Ďakujem za pochopenie. 
   

