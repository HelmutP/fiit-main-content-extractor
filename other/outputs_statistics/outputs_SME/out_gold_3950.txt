

 "Máš už snežnice?", dodal trocha provokačne s predpokladom, že určite ešte nemám. Nemal som, ale s rozhodovaním som veľmi neotáľal. Letmo som ešte skontroloval názor súdruhov v SHMÚ na počasie, poslal SMS-kové avízo Jarovi, ďalšiemu fatranskému stíhačovi, a akcia bola na svete. 
    
 Druhý deň ráno sme už s Jarom sedeli v miestnom buse zo Žiliny do Fačkovského sedla. Lukáš nastúpil po ceste. Snežnicový predvoj v podobe štyroch "prešlapávačov" z tímu Hikers.sk mal asi hodinový náskok, takže po úvodnej aklimatizácii z autobusovej +22 na vonkajšiu -8°C teplotu sme začali rezko vykračovať smer Revánske lúky a Kľak. 
  
 
  Po týždni sneženia tie malé biele veci síce už nepadali, ale fúkal čerstvý vietor, ktorý sa snažil rozohnať rannú hmlu. Cestou na Kľak snežnice ešte neboli nevyhnutne nutné. Dúfajúc, že aj ďalej na hrebeni bude snehová vrstva dostatočne zamrznutá a nosná, som pridal do kroku. 
  
 
 Na Kľaku (1352 m) sme inak výrazný vrcholový kríž takmer nerozpoznali. Zabezpečovala to hustá hmla a viditeľnosť približne 20 m. Keďže pocitová teplota zostúpila o ďalších 5 mínusových stupňov a pre nijaký výhľad sa nekonali ani fotografické orgie, po niekoľkých minútach sme pokračovali smer Vríčanské sedlo. Pod Kľakom sme tiež dobehli Hikers tím v zložení dvaja snežničiari a dvaja makači. Po úvodnom podaní rúk, pozdravení sa a loku domácej slivovice sme už spoločne fujazdili do sedla. Hikers.sk mali dosť ťažké batohy, čo sa prejavovalo dvojako: dvaja "light" bezsnežnicoví borci padali v kyprom snehu pri zostupe do kolien, a boli tiež o niečo pomalší. Rozdelili sme sa teda do dvoch skupín: "prešlapávači" a "tí ostatní chudáci bez snežníc". 
 Prešlapávači razili cestu snežnicami, my chudáci makači už len "kráčali" v hlbokých a širokých stopách, modliac sa, aby sme nezapadli ešte hlbšie pod prerazenú stopu. Mimo hrebeň na trase do sedla bol sneh totiž o niečo kyprejší, mäkší a boril sa v niektorých úsekoch aj vyše metra. 
  
 Cesta do Vríčanského sedla sa nám zdala v týchto podmienkach nekonečnou. Podľa značky z Kľaku by sme mali do sedla doraziť za cca 1 hodinu, snežnicový pochod sa však vyšplhal až na približne 2,5 hodiny. 
  
 Uff, konečne v sedle. Zhodili sme batohy, vytiahli termosky a odfukovali. Niektorí z nás začali uvažovať, že na tie Hole to nebude až také jednoduché. Úsek hrebeňa do Sedla pod Úplazom sa pohyboval väčšinou lesmi a lúkami v nadmorskej výške pod 1000 m, kde sme predpokladali hlboký sneh, najmä v lesných porastoch. Hodinky ukazovali 11:30, čas do ďalšieho sedla niečo cez dve hodiny. V zime je treba tento čas korigovať "snehovým koeficientom" x1,5 (závisí od podmienok), takže do Sedla pod Hnilickou Kýčerou sme mali doraziť o nejakej tretej, kde bol plán rozložiť bivak. Do úplnej tmy by potom zostávala približne hodina, dostatok času rozbaliť stany a sladko zaspať. Posilnení touto nádejnou ideou, tatrankami, slivovicou a čajom sme nasadili batohy na plecia a pokračovali ďalej. Next stop: Sedlo pod Úplazom (teda dúfam, že ma niekto nebude žalovať za porušenie jazykového zákona, ako chudáka Hríba)... 
  
 Cestou do Sedla pod Úplazom sme čiastočne opustili lesnú časť hrebeňa a prechádzali úsekom kosodreviny. Snehová vrstva miestami úplne zakrývala najvyššie konce tohto treťohorného reliktu. 
  
 Keď sme sa už blížili k časovej hranici štyroch hodín od opustenia Vríčanského sedla a cieľ bol "tam za tým kopcom...", rozložili sme sa do hruhu a začali bojovú poradu. Kolegovia z Hikers tímu už boli značne vyčerpaní, azda viac od svojich metrákových batohov než od snehu, takže navrhli pokračovať ešte asi pol hodinu, a potom rozložiť bivak. Na nás ostatných troch vychádzal jeden stan, avšak rozmerové parametre Lukáša sme ani obrazne nevedeli vtesnať spolu s ďalšími dvoma osobami do Jarovho stanu. Esom v Lukášovom rukáve bol posed na Jankovej, asi 300 metrov poniže hlavného hrebeňa. "OK, my dôjdeme až tam, prespíme v posede, ráno vyrazíme na Hole a nejako sa na ceste už počkáme..." Keďže sme v tom mraze už nič sofistikovanejšie nevymysleli, rozlúčili sme sa s Hikers a dúfajúc, že posed nie je ďaleko, sme opäť pridali do kroku. 
  
 Bingo! Nádej zomiera posledná, a tak sme k posedu pri Jankovej naozaj v ten deň prišli. Vyzeral úžasne. Lepší horský hotel sme si želať nemohli. Zateplený, zasklený, dokonca aj dverami vybavený. Po ďalšej polhodine sme už spokojne ležali zabalení v spacákoch a pravidelným dýchaním sme ohrievali vnútornú "posedovú" teplotu... Čas: 18:35. 
  
 Do Sedla pod Úplazom sme nakoniec dorazili až druhý deň ráno okolo 10:30 hod. Po Hikers tíme ani vidu, ani slychu. Pred nami určite nešli, videli by sme ich stopy. Najpravdepodobnejšie zbehli niekde dolu do civilizácie a v tomto momente už možno sedeli v najbližšej krčme:) Tu sme začali prvý raz seriózne uvažovať, že na Martinské Hole dôjsť ešte v tento deň je dosť nereálne. Hrebeň nebol vôbec prešľapaný a hĺbka snehu miestami dosahovala úctyhodných 1,5 metra. Vylepšovalo sa však počasie. Polojasno, mierny vietor a teplota niečo pod nulou nám spríjemňovala strmák na Hnilickú Kýčeru. 
  
 Tieto nevinne vyzerajúce lúky znamenali borenie sa pre snežnicových 0,3 m; pre mňa 0,3 m - a viac, t.j. po zadnicu:) 
  
 Sneh opäť zabezpečil predĺženie cesty z pôvodných 35 minút na viac ako hodinu. 
  
 Sedlo pod Hnilickou Kýčerou. Je 11:45; skladáme batohy a obedujeme. Na Veľkú Lúku sympatických 5,25 hod. V lete by to bola taká príjemná prechádzka, no dnes sme chceli dôjsť maximálne do sedla Maríková (na novej značke aj v najnovšej mape od VKÚ nesprávne označeného ako Majbíková, nevedno prečo...) a odtiaľ zísť z hrebeňa do Valče. Časový deficit pol dňa, tu sme pôvodne chceli prvý deň rozložiť naše bivaky, no nakoniec sme boli radi aj za ten luxusný posed. 
  
 Pri stúpaní na Kýčeru sa mi naskytol pri spätnom pohľade do sedla tento pekný výjav... 
  
 Strmák na Kýčeru dal dosť zabrať. Tu som stratil spolupútnikov Jara a Lukáša, títo snežnicoví stíhači nechávali za sebou len charakteristickú stopu (neznalí návštevníci z Prahy a blízkeho okolia by mohli polemizovať o podobnosti so stopou Yettiho:). 
  
 Snehové záveje na ceste z Kýčery pripomínali gigantické ľadovce v miniatúrnom vydaní. Počasie sa definitívne stabilizovalo a umožnilo odložiť aj vrchnú vrstvu oblečenia. 
  
 Ďalšie a posledné sedlo predo mnou bolo Sedlo Maríková. Lúčanský hrebeň tu už len zriedka vykukol spoza lesného porastu, a tak sa mi pri "prvom rovnaní krížov" naskytol tento pekný pohľad. 
  
 Prejdená časť hrebeňa Lúčanskej Fatry. 
  
 Úplne vpravo už vidno začínajúcu bočnú rázsochu najvyššej časti lúčanského hrebeňa, kulminujúceho Veľkou Lúkou, tentoraz takmer dosiahnutým cieľom. 
  
 Pokus o "umelecké" stvárnenie prejdeného hrebeňa. Vľavo vyčnieva Hnilická Kýčera, a vpravo takmer nebadateľne spoza kríka vytŕča "zub" Kľaku. 
  
 Umelecký pokus č. 2; o približne dvesto metrov ďalej... 
  
 Tesne pred Sedlom Maríková, najvyššiu časť lúčanského hrebeňa už vidno zreteľnejšie. 
  
 Sedlo Maríková a koniec putovania. Na Martinské Hole ešte 3 hodiny, avšak na hodinkách čas 15:43, zajtrajšie skoré ranné vstávanie do práce a najmä snehové podmienky, ... to všetko ma odradilo od dokončenia hrebeňovky. 
  
 Valčianskou dolinou do Valče. Repete určite niekedy v lete:) Ak sa chcete pridať na podobné akcie, naše fórum je k dispozícii tu. 

