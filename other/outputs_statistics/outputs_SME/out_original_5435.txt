
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Janka Špirková
                                        &gt;
                Dejatelia
                     
                 Banskoštiavnická stálica hudobného neba 

        
            
                                    28.4.2010
            o
            16:43
                        (upravené
                25.5.2010
                o
                15:04)
                        |
            Karma článku:
                3.49
            |
            Prečítané 
            633-krát
                    
         
     
         
             

                 
                    Žije na tejto planéte už celú polovicu storočia, je stále aktívny po ako fyzickej, tak aj  psychickej stránke, i keď on sám si nahovára, že je „starý unavený muž", za čo bol už neraz aj mnou pokarhaný.
                 

                 
  
       Reč je o jednom veľaváženom pánovi, ktorého pozná snáď každý, kto býva v mestečku Banská Štiavnica a tým nie je nikto iný ako Herr Ján Sedílek s pseudonymom Hymy. Ak sa mám priznať, tak v okruhu mojich známych sa nenachádza človek s väčším zmyslom pre poriadok v slovensko-nemeckej gramatike, pretože je natoľko šikovný a všímavo-vnímavý, že by našiel mýlku hádam aj Ľudovítovi Štúrovi v epitafe, čo ja nemôžem samozrejme hodnotiť ako závažnosť so záporným pólom, práve naopak, pre mňa ako nezávislú zástankyňu spisovného slova je to naozaj veľké plus, no musím sa priznať, dosť chýb našiel v textoch aj mne a koľko ich už ponachádzal v mojej nemeckej tvorbe, toľko ani litrov vody vo vodnej nádrži Turček nemáme.   Spomínaný pán je pre tých, ktorí ho poznajú, jasné, že aj pre mňa muž s veľkým "M", ktorý by zaslúžene mohol nosiť tričká s motívom lietajúceho hrdinu z Metropolisu. Však kto by sa v dnešnej dobe na vlastnú päsť venoval svojmu pedagogickému poslaniu ako Herr Deutschlehrer a vládal uniesť ťarchu rozmanitosti ľudských zmýšľaní a pováh vo vzdelávacej inštitúcii?! Je to učiteľ na prvej priečke učiteľského rebríčka, so spôsobom výučby som spoločne s minimálnym percentuálnym počtom 101,9% študentov zo 100% spokojná a nemám najmenší dôvod na sťažovanie sa, pretože hodiny s ním sú plné humoru, kombinované so štipkou zvedavosti  s esenciou podpichovania, čo si nedovolím odsudzovať, pretože všetko je v rámci srandy.   Ku všetkým je z môjho pohľadu objektívny a na nikom pán Sedílek neSedí(lek) A aby toho náhodou nebolo málo, v piatky sa presúva zo vzdelávacej inštitúcie do „zábavnej štvrte" ako to on sám nazýva, kde rozbieha svoje známe Oldiesky, kde vek návštevníkov nehrá žiadnu rolu. Keďže som to ešte nezočila, nemôžem k tomu napísať nič viac ako len toľko, že som namôjveru ešte od nikoho nepočula negatívne hodnotenia, čo zrejme značí o všestranných kvalitách našej malej veľkej celebrity, pána Deutschlehrera. Nadišiel čas, kedy by ste sa mali presvedčiť sami o kom toto celé je, preto ste všetci pozvaní 4.júna (Pražovňa, Banská Štiavnica).       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Špirková 
                                        
                                            Prosím, prepáč, ďakujem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Špirková 
                                        
                                            Nuda?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Špirková 
                                        
                                            Kliešť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Špirková 
                                        
                                            Slovenské Benátky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Špirková 
                                        
                                            Mala som ja takú triednu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Janka Špirková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Janka Špirková
            
         
        spirkova.blog.sme.sk (rss)
         
                                     
     
        Ak niekto vôbec "zakopne" o slová mojimi dvoma ukazovákmi písané, budem mu veľmi vďačná, ak sa na všetko pozrie s určitou štipkou nadhľadu, pretože ani ja som nespadla z neba ako vševed,...:D
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    674
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Fotoblog
                        
                     
                                     
                        
                            Hausdarling
                        
                     
                                     
                        
                            Dejatelia
                        
                     
                                     
                        
                            Moja problematika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




