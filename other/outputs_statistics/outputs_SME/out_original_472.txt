
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Gríger
                                        &gt;
                Fotografie
                     
                 Paríž 

        
            
                                    23.2.2008
            o
            0:31
                        |
            Karma článku:
                13.72
            |
            Prečítané 
            14224-krát
                    
         
     
         
             

                 
                    Keď som zvykol počas svojich vysokoškolských rokov vystupovať po útrapne precestovanej nedeli z "rýchliku" Bojnice, na Bratislavsky náležite odpornej stanici Predmestie, často som si predstavoval, že to tak nie je. Že v skutočnosti som práve vystúpil z nejakého super rýchlovlaku, kráčam krásnou historickou halou Gare du Nord a vítajú ma slová: "Bienvenue à Paris". Nuž... sny sa majú plniť. Teraz trvá vlakom cesta Londýn-Paríž len niečo cez dve hodiny, a tak si tam rád odskočím vždy keď objavím lacnejší lístok. Dnes tu mám kúsok jeho atmosféry aj pre vás. "Bienvenue à Paris."
                 

                 Výhľad zo strechy "Montparnasse" vám ukáže aké je to lietať... 
  
 ...nad strechami domov... 
   
 ...nad širokými bulvármi... 
  
 ...ale aj na zemi je pekne... 
  
 ...chladné zimné ulice zohrieva všadeprítomná hudba... 
  
  
  
 ...keď k vám zavanú vône od umelcov s vareškou...  
  
  ...a zapadajúce slnko vám nasvieti ulice do oranžova... 
  
 ...mesto predvedie všetok svoj romantický šarm... 
  
  ...a keď sa zabalí do tajomnej noci... 
  
  ...a zažiari miliónmi svetiel... 
  
  ...tak viac jeho kráse nevládzete odolať... 
  
 ...ponoríte sa do jeho farieb a zistíte... 
  
  ...že milujete Paríž... 
   
 ...jeho zdobené priečelia budov... 
  
 ...jeho čisté farby...  
   
  ...ale pozor... 
  
 ...aby vám tie farby neukradla striga... 
  
  ...zachráni vás Émilie na brehu Seiny...  
  
 ...tá z knižiek od ktorých sa nedá odtrhnúť... 
  
 ...keď zavládne nedeľný pokoj... 
  
 ...mesto sa utíši... 
  
 ...pomaly si vás uspí...  
  
 ...a samé seba zabalí do ďalšej Parížskej noci... 
  
 ...bonne nuit, Paris. 
  Všetko nafotené cez víkend 16.-17. Februára 2008.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (61)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Tento blog pokračuje na www.viamalina.com
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (3) - Lake District
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (2) - Yorkshirskými údoliami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Malinová Cesta (1) - Z mesta do sveta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Gríger 
                                        
                                            Dorset
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Gríger
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Gríger
            
         
        griger.blog.sme.sk (rss)
         
                        VIP
                             
     
        Autor foto-cestopisov na: "www.viamalina.com"
   
  
    
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3237
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Malinovou cestou
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Chill on the sun
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Severné Arménsko. Tajuplnými kláštormi
                     
                                                         
                       Papierové modely na Tété model 2013
                     
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  




     






            
    
        
    
  
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 


 


