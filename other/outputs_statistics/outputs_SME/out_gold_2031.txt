

 Čo budeme potrebovať 
 Morčacie prsia, vykostené a zbavené kože. Kyslú kapustu, v tomto recepte jej nikdy nie je dosť, takže ja používam najmenej jeden a pol kilogramu. Celú hlavičku cesnaku, červenú papriku a soľ. To je všetko. 
  
 A čo s tým urobíme 
 Kus mäsa poriadne prešpikujeme cesnakom. Cesnak ošúpeme a strúčiky nakrájame na menšie hranolčeky. Do mäsa zapichneme nôž a popri čepeli vsunieme kúsok cesnaku. 
  
 Cesnakom nešetríme dáme ho poriadne, na husto, pokojne aj viac ako je na obrázku. Potom mäso nasolíme a necháme ho chvíľku odpočívať. 
  
 Kyslú kapustu dáme do pekáča, podlejeme ju dvoma decilitrami vody a posypeme dvoma kopcovitými lyžicami červenej papriky. Na vrch uložíme mäso... 
  
 ... a celý pekáč zabalíme do alobalu. Pečieme v rúre vyhriatej na 170 stupňov trištvrte hodinu. Potom alobal odstránime a pečieme ešte asi pol hodinu. 
  
 Kyslá kapusta má úžasnú vlastnosť. Zabezpečí, že mäso zostane šťavnaté a krehké, hoci sme do jedla nepridali vôbec žiaden tuk. 
  
 Výbornou prílohou sú domáce zemiakové krokety, ale tá naša racionálna strava, veď viete... Takže si pripravíme zapekané zemiaky. 
 Zemiaky očistíme, nakrájame a v slanej vode uvaríme. Potom ich roztlačíme, ale nemusíme byť taký dôsledný ako pri príprave kaše a malé kúsky zemiakov nie sú žiadnym problémom. 
 Do kaše pridáme vajíčko a dve kopcovité lyžice hladkej múky a poriadne premiešame. 
 Zmes preložíme do tortovej formy, vyhladíme, vidličkou nakreslíme krúžky a vložíme do rúry vyhriatej na 170 stupňov. 
  
 Pečieme tridsať minút a príloha je hotová. 
  
 Cesnakom prevoňané morčacie prsia nakrájame na tenké plátky pridáme kapustu, ktorej nikdy nie je dosť a kúsok zapekanej kaše. Kapusta sa vždy minie ako prvá. Mäso je výborné aj studené, nakrájané na tenké plátky namiesto šunky alebo salámy na raňajky. 
  
 Dobrú chuť. 
  
   
   

