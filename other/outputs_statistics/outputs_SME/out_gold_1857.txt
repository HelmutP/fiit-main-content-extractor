

 Bolestne. A s potokom sĺz.  Odchádza s ním všetko to, čo do neho vložili. Všetky prebdené noci, všetky úsmevy, prvé slová, prvé kroky, všetky prechádzky aj boliestky...všetky zázraky, ktoré sa diali denne a boli samozrejmé.  No kamkoľvek sa spolu pohli, bol s nimi otáznik. Kedy? Kam? Komu? Ako? 
 Profesionálny rodič pritúli dieťa preto, aby sa raz vedelo túliť k iným. 
 Dieťa z detského domova nevie smútiť. Nemá za kým. Nemá k nikomu hlboký láskyplný vzťah. Sú tu tety, každú chvíľu iná a majú čo robiť, aby nakŕmili a prebalili izbičku plnú bábätiek. Nezostáva čas na to, aby sa pestovali, aby ich hladili, aby na sa s nimi zhovárali.  Každú chvíľu iná tvár a zmätok v tom najzákladnejšom. V istotách.  Neistota vedie k rezignácii. Sú sácané okolnosťami a zvyknú si na to prirýchlo. Nič nechcú, neplačú, nehnevajú sa, nedožadujú sa pozornosti. Aj tak je harmonogram dňa pevný a svojimi slzami ho neovplyvnia. 
 Deti odchádzajúce z profesionálnych rodín smútia. Bolí ich odchod od milovaných bytostí. Plačú a nechcú.  Ale ich smútok je tým najväčším prejavom toho, že ich vnútro je v poriadku. Že obsahuje dimenziu smútku. To znamená, že ako opozit obsahuje i radosť, lásku, naviazanosť. 
 Ten smútok, ktorý prežívajú je darom. Darom ich novým rodičom, ktorí ho budú mať navždy. 
 To, že smúti, znamená jedno - že vie milovať. 

