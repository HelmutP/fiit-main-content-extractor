

   
 V Chile existuju jedny "popularne noviny" (su velmi oblubene chilskym ludom). Volaju sa "La cuarta" ( link: http://www.lacuarta.cl ). Ak by som mala jednym slovom zadefinovat o aky format zunalistiky ide, asi by som pouzila slovo "pikantne". Hlavne co sa tyka ich slovnika. 
 Vtipne na nich je to, ze ak si cudzinec, tak sa z nich len tak lahko nedovtipis o com sa pise, treba mat po ruke najlepsie nejakeho chilana na rozlustenie toho o com je rec. Nemalo prezyvok politickych akterov sa prave zrodilo v tychto novinach. (Tak napriklad chilsky Prezident Sebastian Piñera ma prezyvku "Don Tatan"). Raz vam rozpoviem ako k nej dosiel :-) 
 Ak sa teda v novinach "La Cuarta" docitate, ze niekto je "amigo de lo ajeno", tym autor chce povedat velmi z lahka a vtipne, ze ide o zlodeja. 
 A teraz preklad slovo po slove: 
 Ser (bez smiechu prosim) [ser] - Byt 
 amigo [amigo] - priatel, kamarat 
 de [de] - z 
 lo [lo] - co (je to nieco neurcite) 
 [ak dame do kopy "de lo" tak to bude znamenat niecoho] 
 ajeno [acheno] - cudzieho 
   
 Dufam ze sa vam nikdy nestane to, ze budete musiet tuto vetu pouzit v suvislosti s vasimi osobnymi vecami. 
 Ty, ktory studujete spanielcinu a mate chut vidiet tento vyraz "v akcii", nech sa paci, tu je ukazka jedneho clanku z novin 20minutos: http://www.20minutos.es/carta/374199/0/CARTAS/MALAGA/ROBO/
 
   
 Zdravim vsetkych :-) 
   
 hasta la victoria amigos! 
   

