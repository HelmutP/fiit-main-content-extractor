
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            terézia podolanová
                                        &gt;
                Naozaj...
                     
                 Služba "pondelkovému ľudstvu" 

        
            
                                    1.4.2008
            o
            23:02
                        |
            Karma článku:
                4.92
            |
            Prečítané 
            3801-krát
                    
         
     
         
             

                 
                    Pondelok ráno. Slnko ledva vyšlo. Autobus z čias dávno minulých. "Milión" ľudí na 30-tich metroch štvorcových.  Hrozba nového týždňa pred sebou. Výraz "dajte mi všetci pokoj, pre istotu pri mne ani nedýchajte, nemám náladu".
                 

                 
  
    A šofér. Človek, ktorý vstával možno skôr ako ostatní pondelkoví cestujúci. Človek, ktorý často už spomínaný výraz nahodí dvojnásobne. Čím prispieva k všeobecne "dobrej" nálade.                 Ale...existujú výnimky. Šofér pozrie do zrkadla nad hlavou a povzbudený množstvom "šťastných" tvárí sa rozhodne konať. Veď úsmev je vysoko toxický a nákazlivý aj pre takých imúnnych ľudí ako sú pondelkoví cestujúci. Ako bonus pridá niekoľko príjemných a veselých poznámok na adresu okolonatlačených.                           Vzduch okolo neho je hneď dýchateľnejší. V priebehu niekoľkých minút sa mu podarí "nakaziť" polovicu autobusu. A nákazlivá radosť sa šíri ďalej.       Treba len dúfať, že sa táto šťastná choroba neudrží len medzi plechmi autobusu a dostane sa aj von. Treba veriť, že aspoň niektorí z nakazených nezabudnú, že ju majú prenášať ďalej. Treba sa modliť, aby bolo takých šoférov, ľudí, viac.       Treba sa pridať...hneď!                                                         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        terézia podolanová 
                                        
                                            Čo rozprával žltý dom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        terézia podolanová 
                                        
                                            V ten deň sa spriatelili
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        terézia podolanová 
                                        
                                            O človeku...ako bolo všetko inak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        terézia podolanová 
                                        
                                            O človeku, ktorý strácal čas...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        terézia podolanová 
                                        
                                            Nádherný deň. Čo povieš?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: terézia podolanová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                terézia podolanová
            
         
        podolanova.blog.sme.sk (rss)
         
                                     
     
        "Počasie je také, aké si ho urobíš." (D. Ondrušek)
A práve preto môže byť aj ten najhorší deň krásny alebo aj najkrajší deň úplne nanič. Treba si len vybrať.


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    806
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Naozaj...
                        
                     
                                     
                        
                            Hory, lesy (foto)
                        
                     
                                     
                        
                            Hmmm...
                        
                     
                                     
                        
                            O človeku...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            kde to žijem...
                                     
                                                                             
                                            Hrdina...
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ctibor Határ: Edukácia seniorov
                                     
                                                                             
                                            Digitálna fotoškola
                                     
                                                                             
                                            John Powell: Prečo mám strach povedať ti, kto som?
                                     
                                                                             
                                            Jan Karonová: Z Mitfordu do Meadowgate
                                     
                                                                             
                                            Luigi Giussani: Nekonečno v nás
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Lenka
                                     
                                                                             
                                            ifka :)
                                     
                                                                             
                                            Zuzu
                                     
                                                                             
                                            jtm
                                     
                                                                             
                                            Dávid Králik
                                     
                                                                             
                                            Róbert Flamík
                                     
                                                                             
                                            Veron sneHuljaková ;-)
                                     
                                                                             
                                            Mária Kohutiarová
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            úplne úžasná stránka
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




