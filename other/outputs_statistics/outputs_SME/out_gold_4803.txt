

 Na prvý pohľad je to jasné, nenažraný klub, chudáci faúšikovia. Situácia má ale oveľa viac aspektov ako sa môže zdať. 
 Na úvod vyberám percento, ktoré v zahraničných (rozumej vyspelých) kluboch predstavuje podiel príjmov z predaja vstupeniek na celkových príjmoch klubu - 30. 
 Na Slovensku futbal nie je biznis a prijmy prakticky neexistujú, vstupné je nastavené aby krylo možno variabilné náklady spojené s organizáciou zápasu. Percento z príjmov by som odhadol menej ako 5. 
 Ďalším problémom resp. odlišnosťou je percento vstupeniek predaných dopredu - permanentiek na celú sezónu. V zahraničí sa ich na niektoré kluby bežne predá aj 75 percent kapacity štadióna, niekde sa dokonca dedia (Newcastle). U nás ide o mizivé percento. 
 Finančné dopady prvých dvoch bodov: 
 Nízky podiel príjmov z predaja vstupeniek je jeden z faktorov prečo musia slovenské kluby dotovať majitelia a je nutný predaj hráčov. 
 Nízke percento predaja celosezónnych vstupeniek má za následok nekalkulovateľné príjmy. 
 Dopady na realitu: 
 Dotácie majiteľa a predaj hráčov sa dajú hodnotiť vyslovene ako nutné zlo, klub (firma) by si mal na seba zarobiť v ideálnom prípade sám a nebyť závislý od neustáleho predaja aktív a vkladov akcionára. Nepredvídateľné príjmy majú veľmi zlý efekt, na druhej strane k nim má totiž klub až príliš predvídateľné náklady. Platy hráčov, zamestnancov, energie toto všetko je splatné v očakávanej výške a čase. Disproporcia je veľmi zlý jav a musí byť opäť vykrytá majiteľom. Bankové financovanie prevádzkových potrieb klubov je z rovnakého dôvodu komplikované, nie je možné preukázať budúce príjmy. 
 Dopady na prípadné financovanie výstavby štadióna bankovými zdrojmi: 
 Štadión je špecifické aktívum, jednoúčelová stavba, využívaná v prípade ligy raz za dva týždne. Jeho kofinancovanie je možné len na základe generovania pravidelného predvídateľného príjmu v prijateľnej výške. Keďže ten neexistuje, táto cesta je zarúbaná. 
 Naspäť k Žiline. 
 Klub stanovil ceny pre fanúšikov, ktorí vlastnia permanentky a klubové karty na 50 EUR príslušné tribúny. Ostatní si zaplatia, záujem je. Čo chce Žilina dosiahnuť? Vyšší podiel predikovateľných príjmov z permanentiek alebo z členského. Chcú pravidelného diváka, nie diváka oportunistu. Tých 300 EUR by som nazval výchovnou lekciou žilinskému faúšikovi, aby si na budúcu sezónu kúpil permanentku. Popri tom trh za 300 EUR aj tak vypredá štadión a Žilina bude mať príjem, ktorý jej nahradí absenciu dopredu predaných celosezónnych vstupeniek. 
 Trošku konkrétnych čísiel: 
 5000 ľudí vlastní permanentku alebo klubovú kartu 
 1000 lístkov pre UEFA 
 1000 lístkov pre hostí 
 1000 lístkov pre potreby hráčov a klubu 
 okolo 2500 lístkov pôjde do predaja v režime "300 EUR+" 
 Podľa klubu na 2500 lístkov existuje 20 násobný dopyt, ich predaj za nižšie sumy by evidentne spustil čierny trh, ktorému chceli predísť. 
 Z uvedeného evidentne narážame na problém - kapacita štadióna. Tu sa kruh uzatvára, na financovanie zvýšenia jeho kapacity sú potrebné bankové zdroje a tie chcú predikovateľný príjmem. 
 Záver: Žilina urobila najlepšie ako mohla a potvrdila prvé miesto v koncepčnosti medzi klubmi na Slovensku. V krátkodobom horizonte maximalizujú zisk z predaja vstupeniek segmentu "300 EUR+", ktorý aj tak pravidelne na zápasy Žiliny chodiť nebude. Zdroje môžu byť ideálne použité ako vlastný kapitál na rozšírenie štadióna. V dlhodobom horizonte vykonali lekciu na kúpu celosezónnych vstupeniek, prípadne režim člena klubu a tým posilnil svoje šance na získanie čiastočného externého financovania rozšírenia štadióna bankami. 
 Nedá mi nespomenúť úryvok z doslova rodinnej tragédie, ktorá sa odohrala po zverejnení cenovej politiky na spomínaný zápas. 
 Jedna dievčina chcela dopriať svojmu otcovi zážitok kúpou lístka na tento zápas. Po zistení ceny sa takmer rozplakala v diskusii. Rada: radšej mu kúp permanentku na budúci rok 
 Objavilo sa množstvo kalkulácií cien zahraničnými klubmi na ligu majstrov, žilinské občianske zruženie spísalo list tu. Odporúčam im zaslať ho ešte do Štrasburgu, organizácii human rights watch, organizácii za oslobodenie Tibetu a moskovskej pobočke organizácie Greenpeace. 
 Nezabudnite "blue is the colour chelsea is the name" a aj keď to nikto nechce priznať, takmer všetci sa idú pozrieť na nich. 
 Ad médiá: prestaňte podporovať prostú kritiku a viac ten problém vstupeniek analyzujte, všetko čo som čítal bolo naozal len trápne prisluhovačstvo väčšinovému názoru. 
   
   
   

