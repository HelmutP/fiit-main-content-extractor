

   
 Výsledkom je konkrétnych 120 bodov, ktoré by podľa nášho názoru mohli zlepšiť život na Slovensku. 
 Niektoré si vyžadujú zásadné zmeny ústavy, na iné stačia jednoduché úpravy zákonov, ale ako celok tvoria systém opatrení, ktoré z hľadiska liberálnej politiky môžu prispieť opäť k rozvoju ekonomiky, spoločnosti a Slovenka ako takého. 
 Nikto z autorov nemá patent na rozum a ja netvrdím, že moje návrhy sú tým jediným riešením a nikto nemôže prísť s ničím lepším. 
 Vecne diskutujme, argumentujme a zvažujme plusy a mínusy tej ktorej úpravy. Osobne si tiež nemyslím, že dekriminalizácia marihuany je problém Slovenska číslo jedna, ale s jeho presadením nemám problém a určite to nebude znamenať nárast počtu užívateľov mäkkých drog o 250% ako som čítal v niektorej diskusii pod článkami. 
 A teraz pár viet k človeku, ktorý programu dával jednotnú štruktúru a ktorý sa stal vďaka niektorým médiám súčasťou ezoterickej skupinky na našej politickej scéne. 
 Martina Poliačika poznám len niečo vyše 7 mesiacov z našich sedení nad programom a nestretávame sa v súkromí, takže z týchto stretnutí vychádza aj môj názor. Sedenia trvali v priemere 3 hodiny a strávili sme spolu nejeden nedeľný večer a nemal to s nami určite ľahké. Udržať jednotnú formu pri 20 kapitolách pri rôznych typoch osobností si vyžaduje veľkú schopnosť argumentácie, rétoriky a aj zrelosti človeka. 
 Za túto prácu, ktorú Martin počas toho polroka odviedol, má môj obdiv a úctu. 
 Čo robí Martin v súkromí mňa nezaujíma a ani nevidím dôvod to riešiť, každý máme nejaké záľuby, jeden chodí na ryby, druhý hrá golf, tretí hrá amatérsky v jazzovej kapele. Osočovať niekoho zato, že sa zaujíma aj o duševné veci, s ktorými sa bežný človek nestretáva a tvrdiť o ňom, že berie drogy a vyzýva k tomu aj iných je primitivizmus. 
 Keď máte výhrady k programu, tak to akceptujem, ale osočovať niekoho zato, že nemá „štandardné" záľuby je smiešne. 
 Držím tomuto programu SaS palce. 
   

