

    
    
   
   
 Naučila som sa znovu milovať a naučila som to aj zopár ľudí okolo seba. Myslím, že toho už viac robiť nemusím. Nech to už vyznieva akokoľvek pateticky a gýčovo, verím, že nad nami bdie a že sa o nás postará. Nám toho zase až tak veľa netreba. Nemáme veľa, skutočne nie, ale koľko toho už jeden človek potrebuje? Problém je v tom, že tu sú „oni". Budú nám tvrdiť, že máme málo. Že predsa musíme chcieť viac. 
 Ale keď sa obzrieme za seba, zistíme, že je tretí mesiac v roku, na ktorý si spomíname iba ťažko. Nič nového sa nestalo. Tí, čo sa naháňali, sú stále v behu. Tí, ktorých presvedčili o vlastnej nedokonalosti, sa stále snažia zmeniť. Tí, ktorí túžili milovať, stále píšu básne namiesto zamilovaných listov. Tak koľkí z nich to skutočne chceli? A koľkých urobilo toto hľadanie šťastnými? 
 Zistila som, že všetko, čo potrebujem si ma nakoniec nájde samo. O mesiac budem sedieť za sviatočne vyzdobeným stolom a ďakovať za to, že som tam. V kruhu ľudí, ktorí ma milujú a z nejakého zvláštneho dôvodu sú presvedčení, že snáď už ani lepšia nemôžem byť. Mali by ste tam vtedy byť. Pozerať sa na tých dvoch, čo budú piť z rovnakých pohárov a večerať z jedného taniera. Ona sa to učila v našej kuchyni, keď videla ako sa pripravujú raňajky v sobotu a ako vonia v nedeľu jablkový koláč. On sa na ňu bude dívať očami, ktoré hovoria, konečne si iba moja. 
 V tú noc budú pri stole sedieť tí najbohatší z bohatých. A ak si prídeš prisadnúť, pochopíš, prečo je tomu tak. 
   
   
   

