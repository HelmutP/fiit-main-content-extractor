
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Šikulová
                                        &gt;
                Doma
                     
                 Sofia je ešte stále prekvapením 

        
            
                                    27.6.2010
            o
            9:00
                        (upravené
                27.6.2010
                o
                1:00)
                        |
            Karma článku:
                8.13
            |
            Prečítané 
            1770-krát
                    
         
     
         
             

                 
                    2 milióny ľudí a dopravné zápchy denne. Najrozheganejšia a najšpinavšia mestská doprava je v Sofii. Staré autobusy z Nemecka a Francúzska. Ktovie z akej doby. Aspoň tie električky sú tak trochu romantické, len sa na ne ťažko vylieza. Potom sa v nich dôchodcovia prevážajú sem a tam.  Novšie autobusy majú nápisy v turečtine a sú urobené pre miniatúrnych ľudí. Nedajú sa tam poukladať nohy.
                 

                 
  
   Donedávna tu veľké nákupné centrá neexistovali. Potom sa postavili naraz tri a odvtedy sa polka mesta prechádza tam. Minimálna mzda v Bulharsku je 240 leva (120 Euro)... a aj tak sa zdá, že všetci nakupujú. A najlepšie handry aj tak nájdete na otvorenom trhu, kde sa hovorí prevažne turecko - cigánsky. Ale tam tí Sofijčania nakupovať nechodia.       My sa chodíme prechádzať na Vitošku. Sofia leží na úpätí pohoria a nám sa to zakaždým zdá neuveriteľné. Tridsať minút a akoby sme boli v Tatrách. Rieky s obrovskými kameňmi, vodopády a kláštory... a ticho a niekedy aj bezľudie. Okolo Sofie sú dediny s jazerami, minerálnymi vodami a veľmi veľa stánkami s pečeným mäsom. Tie sú vlastne aj na Vitoši - kvôli tomu, že na niektoré miesta sa dá dostať aj autom. A tak na Vitoške niekedy stretnete aj dievčatá v opätkoch... prišli na kafe a fašírky s ľutenicou. Po týchto skalách ale nelozia:            Aj my sme boli na Vitoši, keď sa slávilo narodenie Jána Krstiteľa. Po bulharsky Eňovden - 24 júna - najdlhší deň roka, sviatok rastlín, mágie a liečenia. V tento deň si Ján na seba dáva kožuch, aby odišiel za snehom - a sila slnka sa začne zmenšovať.  Stretli sme tety, ktoré prišli zbierať rastliny na 77 chorôb a robili jánske vence. Hovorili o bulharskom Kristovi. A ani nevadilo, že vôbec ešte nebolo 24. júna, ale bola sobota a oni si nazbierali jánske rastliny a odložili na 77 a pol choroby.            Bankya je hádam najobľúbenejšia bulharská minerálna voda a dedinka pri Sofii. Krásne je tam cez týždeň... cez víkend je plná fašírok, že sa cez dym ani nedá vidieť a áut so sofijskými poznávacími značkami. Na vodu si vystojíme rad, lebo Sofijčania si priniesli najmenej päť desať litrových bandasiek každý jeden.         Všetko je tu také bulharské. Samé kaviarničky, obchodíky z papiera, ošarpaná omietka, dva svadobné sprievody a rozbité okná. Proste bulharská pohoda.                                A papierový obchod, akých je v Bulharsku neúrekom. Tento je v Bankyi, ale jeden taký je aj v úplnom centre Sofie. Máte chuť na rakiju či mastiku?    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Kam Sofijčania nechodia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            153 dní protestov a študentská okupácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Konya
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Ospravedlnenie na pamätníku sovietskej armády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Rodopy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Šikulová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Šikulová
            
         
        sikulova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Hlavne o Bulharsku, o živote v Sofii a cestovaní po bulharských mestách a ošarpaných dedinkách. Aj o Turecku, Istanbule a Malatyi. Najradšej však o najkrajšej horehronskej dedine, jej kroji, pesničkách a makovníkoch - jednoducho o Šumiaci.

Janka Šikulová  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2411
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje Bulharsko
                        
                     
                                     
                        
                            Doma
                        
                     
                                     
                        
                            Niekde inde na Balkáne
                        
                     
                                     
                        
                            Moje Turecko
                        
                     
                                     
                        
                            Každodennosť bulharská
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čokoľvek na tomto blogu o cestovaní Bulharskom
                                     
                                                                             
                                            Úžasné foto z dejín Bulharska
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Georgi Gospodinov
                                     
                                                                             
                                            Elif Shafak
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Eliška
                                     
                                                                             
                                            Vita
                                     
                                                                             
                                            Janette Maziniová
                                     
                                                                             
                                            Blanka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dnevnik
                                     
                                                                             
                                            Guardian
                                     
                                                                             
                                            Kommersant
                                     
                                                                             
                                            Lost Bulgaria foto
                                     
                                                                             
                                            Šumiac ešte lepšia stránka
                                     
                                                                             
                                            capital.bg
                                     
                                                                             
                                            &amp;#304;nspiracie
                                     
                                                                             
                                            Le Monde
                                     
                                                                             
                                            Bacchus.bg
                                     
                                                                             
                                            Sofia ulica
                                     
                                                                             
                                            BBC
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nekúpim už topánky u ***** **********
                     
                                                         
                       Okolo Nízkych Tatier: 336 kilometrová cykloporcia
                     
                                                         
                       Prečo nerobia revízori poriadok aj s otravnými cestujúcimi?
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Sarajevo - rozdelené mesto
                     
                                                         
                       Pán Hríb, mám 33 a ja to vidím takto
                     
                                                         
                       Podstránka sme.sk ospevovala T. E. Rostasa
                     
                                                         
                       Chráňme život! Ale nie takto
                     
                                                         
                       Pochod za život na život zabudol
                     
                                                         
                       Podporujem pride pochod, ale podporujem aj rodinu.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




