
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alena Škutchanová
                                        &gt;
                Medické
                     
                 Kurz komunikácie pre medikov: Anamnéza a základné vyšetrovanie 

        
            
                                    27.1.2010
            o
            16:34
                        (upravené
                9.9.2010
                o
                17:11)
                        |
            Karma článku:
                9.40
            |
            Prečítané 
            2851-krát
                    
         
     
         
             

                 
                    Ocitli ste sa vo fakultnej nemocnici. Asi ste aj počuli o spolupráci s medikmi. A tak sa raz objavia medzi dverami izby a spýtajú sa vás, či si vás môžu vyšetriť a čo-to sa popýtať.
                 

                 Tretí ročník je prelomový, po dvoch rokoch teoretických predmetov sú študenti medicíny vypustení medzi ozajstných pacientov.     Všetci sme si rozmnožili kartičky - "ťaháčiky" s otázkami a vyzbrojení novučičkými stetoskopmi a pár základnými radami, čo určite nemáme robiť, sme sa vrhli medzi pacientov. Kurz komunikácie na vlastnú päsť sa mohol začať.   Na koho to slovo padne...  V lete po treťom ročníku som brigádovala ako sanitárka (nižší zdravotný personál). Na stáž prišli zahraniční študenti. Nemala so pve robotu a tak sa ma mladý lekár, čo ich mal na starosti, spýtal, čo by som im nevedela ukázať základné vyšetrovanie. Od samej pýchy, že ma oslovil, som asi aj kúsok podrástla. On sa zbavil roboty a...  Tak sme ostali stáť uprostred chirurgickej JIS. Čo pacient, to ťažký stav. Ľudia ubolení, zoslabnutí, čerstvo po operácii, často nekomunikujúci, poniektorí kvapku mešuge...  Nakoniec som vybrala starého pána duševne pri sebe, čo na tom ani telesne nebol tak zle, ako spolupacienti. Nejako to ale našťastie s nami a slovensko-anglicko-latinskou zlátaninou, nie sice s nadšením, ale aspoň aj bez ujmy prežil a kolegovia mali odcvičené.  Zvyčajne nemáme možnosť vybrať si pacienta. Niekoho nám lekár - vyučujúci pridelí. Niekedy jednoducho vidíme, že z toho nič nebude, že je ten človek celý nanicovatý... To utekáme za doktorom, aby, kým začne sedenie, zohnal náhradu. Ale keď nás odmietne už druhý - tretí, zjavne dobre vyzerajúci človek vo fakultnej nemocnici, už je to nepríjemné aj nám. Prinajmenšom už nestihneme dostať koho iného...   Dedúšik  Bol to taký nepríjemne tmavý, sychravý novembrový deň, znásobený prvým poschodím starej budovy chirurgie a stromami za oknami. Štvrtáci, cítili sme sa zbehlí, ale ťaháčiky ešte slúžili. Aby sme na nič nezabudli.    Starý pán bol na nás zlatý, všetko nám porozprával aj vyšetriť sa nechal.    Jedna kapitolka ťaháčika pojednáva o sociálno-pracovne anamnéze: v akých podmienkach človek býva, kde pracuje... Starý pán nám začal rozprávať to tom, ako žil na dedine, ale že deti dom predali a kúpili mu byt na sídlisku, že mu žena umrela a on je tam sám... Nakoniec sa rozplakal. Ako sme tam boli tri medičky, tak sme ostali stáť ako obarené. Takto sa to nemalo skončiť. Čo sme mu mali v tú chvíľu narozprávať, aby bol v pohode? Aspoň takej, v akej bol, keď sme prišli?!   Sclérosis multiplex, takí zlatí pacienti  Okrem toho, že roztrúsená skleróza je strašná choroba, ktorá človeka postupne oberá o schopnosť pohybu a veľa iných vecí, si v učebnici neurológie si môžete prečítať, že pre ľudí trpiacich touto chorobou je typická eufória, suché konštatovanie av reáli, čo ich poznám, ľudia naozaj zlatí a plní optimizmu.   Zápočtový chorobopis musí vyzerať tip-top a my medici mu venujeme zvláštnu pozornosť. A že to onehdá bol zápočet z neurológie, tak okrem fonendoskopu nás navyše vyzbrojili kladivkami. Stali sa z nás medici ozbrojení a nebezpeční.   Náš pacient (boli sme na neho dve medičky) so sklerózou ležal na posteli tak, ako tam bol naposledy ponechaný. Milý, zlatý, usmievavý, porozprával, aj nám poukazoval, čo všetko dokáže. Napríklad hýbať špičkami na inak nehybných nohách.   Aj kladivká sme vytiahli. Keby to bol býval pacient po zaliečenej mozgovej príhode, mali by sme to rýchlo. Ale toto prebiehalo asi takto: Rozhovor s kolegyňou  pri pokuse o vybavenie reflexu: „Máš ten reflex na druhej ruke? Ja neviem, či je to ono... Poď to oskúšať sem, ty si ho minule vybavila... Má tam vôbec byť?! Myslím že hej, ale nie som si istá... neviem, teda, či to tam podľa knihy má byť, alebo nie. Hej, hej, učila som sa, ale nespomínam si..." A tak sme nášho pacienta hodnú dobu vyklepávali, dopytovali sa. Aby sme neprehliadli niečo, na čom by nás vyučujúci utopil, ak by sme si to vycucli z prsta... A on sa usmieval a bol na nás milý tak veľmi, že sme už len dúfali, že ho zas až tak netrápime a neobťažujeme...   Netýka sa to len pacientov s roztrúsenou sklerózou (aj keď u neho mi to prišlo ešte trochu iné v tom, že je nepohyblivý a nemôže sa pred nami ani schovať...) Jednoducho toto bol jeden z tých pacientov: "Kľudne si vyskúšajte všetko, čo potrebujete, veď aj tí primári a starí doktori sa to kedysi museli na niekom naučiť..." A s tými sa nám spolupracuje nejlepšie.   Je nás veľa.   Niekde sa to naučiť musíme.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Cisársky rez
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Anestézia na poslednej ceste
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Dotykoví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Neprebudení
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Vymysli si niečo pekné na snívanie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alena Škutchanová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alena Škutchanová
            
         
        skutchanova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Lekárka - anestéziologička skautkapoviedkárkaa ešte všeličo možné
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2294
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Anestéziologické
                        
                     
                                     
                        
                            Intenzívne
                        
                     
                                     
                        
                            Medické
                        
                     
                                     
                        
                            Skautské
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Bydžovský - Akutní stavy v kontextu
                                     
                                                                             
                                            Forrest Carter - Škola Malého stromu
                                     
                                                                             
                                            Reif Larsen - Mapa mojich snov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Miro Tropko
                                     
                                                                             
                                            Lenka Rubensteinová
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Ľubica Kočanová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje obrázky
                                     
                                                                             
                                            Moje osobné stránky
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ľudia, ktorí chodia po štyroch
                     
                                                         
                       Pochod za právo na rozhodnutie
                     
                                                         
                       Úsvit a kultúrne zákulisia vedy o sexe. Prípad masturbácie.
                     
                                                         
                       Polovačka na fotky
                     
                                                         
                       Dobrý lekár vždy zachraňuje...
                     
                                                         
                       Čo radí autobusár, kým radí
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       V takýchto chvíľkach, má človek chuť, stále začínať odznova
                     
                                                         
                       Uväznená mitochondria a zrod komplexného života
                     
                                                         
                       Vrkoč
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




