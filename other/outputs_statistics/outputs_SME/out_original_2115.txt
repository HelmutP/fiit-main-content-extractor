
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Kvasnička
                                        &gt;
                Nezaradené
                     
                 s poctivosťou najďalej dôjdeš ! 

        
            
                                    24.1.2010
            o
            20:25
                        (upravené
                1.9.2010
                o
                22:49)
                        |
            Karma článku:
                4.78
            |
            Prečítané 
            595-krát
                    
         
     
         
             

                 
                    Hovorí sa, že lož má krátke nohy, kto klame, ten aj kradne, a že žiadny strom nerastie do neba, a na každého raz dôjde ... Podľa tejto filozofie sa poslednou dobou v úmysle nad tým všetkým zamýšľam, či poctivosť sa v našej krajine pomaly ale iste nestáva skôr sprostým slovom.
                 

                 
  
   V posledných rokoch som sa opakovane stretol s tým, že označenie človeka poctivého bolo v kontexte myslené ako zároveň človeka, viac menej hlúpeho. A treba dodať ,že časť spoločnosti to tak aj akceptuje, čo je , zdá sa mi naozaj smutné,. Stratou obsahu sa trochu začína vytrácať aj pôvodný význam slova. Inými slovami poctivosť už často nie je poctivosť, ale rovná sa : označenie pre hlúposť, zriedka kedy predstavuje človeka poctivého, v zmysle rovnosti k sebe i druhým.      Ja toto všetko vnímam ako dôležitý obraz nášho konania a smerovanie, zrkadlo našej morálky a etiky, našej spoločenskej normy. Súčasný stav mi príde ako za päť minút dvanásť tesne pred tým, ako normálne začne byť pre človeka pokiaľ možno nepoctivý, arogantný, bezohľadný, jedným slovom : človek bez zábran. Všetci ostatní budú hlúpi a na okraji spoločnosti, ktorá je "in".   Od poctivosti dostaneme sa ľahko na vedomie a svedomie. A ja sa pýtam, mali naši predkovia a dedovia vyššiu úroveň vedomia a svedomia, ako máme my? Boli lepšími ľuďmi, alebo sme my lepšími? Určite sa dá odpovedať: bola iná doba. A áno, iste, bola iná doba, s tým nedá, než súhlasiť. Ale čo to vlastne je ten "čas" ...  Iná doba nie je to len všeobecné alibi pre všetko možné?     Bol som pred časom v Rakúsku a tam je rovnaká doba ako tu u nás, ale zjavne tam sa tá poctivosť, teda ten obsah ešte nevytratil. Byť poctivým človekom je tam normálne. Keď urobí niekto niečo na úkor druhých, človek je zbavený možnosti podnikať, nie zo zákona, ale z všeobecnej reakcie ľudí okolo. Nastane situácia, že s vami prestanú obchodovať všetci vaši obchodní partneri, okamžite vám vypovedia zmluvy, lebo je to spoločensky neprijateľné, prestanú sa s vami stýkať susedia, stratíte priateľov, budete sa musieť odsťahovať aspoň do iného mesta, pretože vašim deťom budú nadávať spolužiaci vo škole.     Iba my sme proti tomu všetkému zrejme nejakým záhadným spôsobom imúnny, ľahostajní. Keď napríklad tu u nás niekto niečo nepoctivo získa, veľa ľudí si povie: ten je ale šikovný -ten s tým vypiekol, takto sa to musí robiť. A kde sa to teda vzalo tu u nás. Dostali sme nejakú neznámu infekciu, alebo čo to je? Kedy to priletelo? kedy sa tak stalo? Kde nastal ten zlom ?    Dlhšiu dobu nad tým rozmýšľam a naozaj si nie som istý, či to začalo prvými politickými aférami, pokračovalo s naším tichým ovčím prizeraním všemožným neprávostiam, a teda akceptovaniu, alebo je to len nejaké prechodné obdobie ... A ako z toho von? Naozaj neviem. Možno mi poradíte, asi to nejako nechápem. Myslím, že človek môže byť všemožne nadaný, obdarený intelektom, ale keď nie je poctivý, je mu to všetko k ničomu a vlastne aj nám všetkým.     Ale aj tak verím heslu - s poctivosťou najďalej dôjdeš ! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Koniec ekonomickej slobody Európskych národov !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            7 základných znakov komunizmu !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Rómsky problém ? - riešenie Sociálne Odkázané Spoločenstvá !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Problém dekriminalizácia marihuany ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Slovenský šport je čas na zmenu.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Kvasnička
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Kvasnička
            
         
        jankvasnicka.blog.sme.sk (rss)
         
                                     
     
        Ján Kvasnička
zakladajúci člen strany Sloboda a Solidarita
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    579
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            slobodná európa -nikdy to tak nebude
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            palmeiro.wbl.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




