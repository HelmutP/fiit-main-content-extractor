
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Lisánsky
                                        &gt;
                Komentáre a úvahy
                     
                 Šťastie ? Smola ? 

        
            
                                    13.6.2010
            o
            11:42
                        (upravené
                13.6.2010
                o
                12:24)
                        |
            Karma článku:
                7.81
            |
            Prečítané 
            723-krát
                    
         
     
         
             

                 
                    Jedny z najdôležitejších volieb od Novembra (aj keď ich veľa nebolo) teda skončili tesným, ale veľmi potešujúcim víťazstvom strán stredopravého spektra. Vyplnila sa teda predpoveď komentátorov, ktorí český príklad spred dvoch týždňov označili za možný i na slovenskej politickej scéne (tento názor som zdieľal i ja).  Dobre, to by bolo klišé, ktorého sa nasledujúce dni možno aj presýtime. Teraz tie menej príjemné veci.
                 

                 Robert Fico má dnes mrazivú pravdu v jednom : pri pohľade na červené more volebných okrskov Slovenskej republiky je zreteľné, že pravica je mimo Bratislavy a južného Slovenska (ktoré je beztak viac-menej ľavicové, len trpí ,,nevýhodou" vyberať si hlavne strany presadzujúce záujmy maďarskej menšiny) prakticky vygumovaná. Bolo by mimoriadne cynické, ak by sme si predstavili britský volebný systém - v tom prípade by Smer získal pohodlnú ústavnú majoritu...no,skrátka scenár hodný Mlčania jahniat. Vcelku nepríjemným je tiež sklamanie z výsledku KDH, ktoré neatakovalo dvojciferné hodnoty z prieskumov ani náhodou. Rovnako bude dôležité nasledujúce počínanie Ivety Radičovej, samozrejme za predpokladu, že vládu bude viesť ona. Jej jemný, nekonfrontačný štýl nebude možno pre krajinu zápasiacu s takými výzvami ako v momentálnej situácii stačiť. Myslím, že viac ako Margaret Thatcherovú pripomína Václava Havla, čo je nie vždy šťastným riešením.       Druhá vec, aj keď menej pálčivejšia, je postreh Rudolfa Pučíka z SNS. Jeho tvrdenie o koalícii stredopravých strán, ktoré budú zápasiť o presadenie svojich nosných hodnôt je z istej časti tiež možno znepokojujúcim. Riešení sa ponúka niekoľko, ale za najpoužiteľnejšie budem osobne považovať neformálne ustanovené bloky v koalícii - liberálny, pozostávajúci z SDKÚ a SaS a potom konzervatívny, pozostávajúci z Mostu a KDH. Aj napriek miernemu nepomeru síl sa takýmto postupom dá zaručiť liberálno-konzervatívna vláda s takým okrídleným prívlastkom, ako je vláda rozpočtovej zodpovednosti. Táto rovnováha tomu len napomôže, nehovoriac o skúsenostiach a nespornej inteligencii a citu budúcich koaličných lídrov. A samozrejme, troškou trpezlivosti a šťastia.   Dal by sa ešte spomenúť fakt možného maďarského uvoľnenia. Tento scenár určite nehrá do karát Viktorovi Orbánovi, ktorému jeho najvychytenejší cieľ zahraničnej (a už i domácej) politiky pravdepodobne odišiel do opozície spolu s SNS. SMK ako jeho predĺžená prisluhovačská ruka na domácej politickej scéne sa do parlamentu dokonca ani nedostala - to Orbánovi vnucuje možnosť otvoriť s Mostom rokovania o spolupráci a rešpektovaní sa. Je však možné aj to, že Orbán začne s Bugárom o našich Maďarov bojovať - a z tohoto súboja, ak sa podcení, môže odísť s porážkou práve Béla Bugár. Aby sa to nestalo, potrebuje k tomu podporu koalície. Napriek tomu sú tieto umiestnenia strán v podstate ideálnym stavom.   Potešujúcich vecí je ale stále oveľa, oveľa viac ako tých negatívnych. Do parlamentu sa nedostala HZDS - to netreba popisovať - pre každého demokraticky zmýšľajúceho občana je tento fakt asi poslednou ranou mečiarovskej úbohosti a nastupujúcej senility jeho politiky. Neúspech SNS je mierne zapríčinený prevzatím nacionálnej agendy Robertom Ficom, ale napriek tomu si myslím, že je pomerne reálne predpokladať veľkú krízu vnútri SNS. A nakoniec tá najsladšia odmena - porážka krátkozrakého nacionalizmu, primitívnej vulgarizácie politiky a drzého smiatia sa do ksichtu bežným občanom. V STV sa uvoľní pár miest, zahraničná politika získa viacej obratnosti otočením sa od východu a pričlenením sa k tým alianciám, ktoré nám ponúkajú možnosti vyjednávať. Pravicová vláda v Česku a liberálna v Poľsku tej našej, tiež pravdepodobne pravicovej, teraz ponúkajú možnosť vyvážiť maďarské snahy o odvrátenie vlastných problémov orientáciou na veľkomaďarské nezmysly. To sú skutočné víťazstvá týchto volieb. Pravica ale musí zabojovať. Čakajú ju neuveriteľne zložité 4 roky, ktoré sú zatiaľ naklonené v jej neprospech a pasovať sa bude so Slovenskom, ktoré je silne nakazené červeným morom socializmu. Je ale stopercentne pravda, že nie vždy je percentuálne vyjadrenie vôle ľudu (v tomto prípade podpora Smeru) zárukou jeho múdrosti a neomylnosti ohľadom efektivity politiky. V skratke - masy sa zvyknú riadne mýliť.   Kto ešte teda tvrdí, že Robert Fico voľby vyhral ?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Monopol na diplomaciu končí. Začína sa bezpečnejší svet ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Siedma nemoc : čo napíšeme, to platí !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Byť konzervatívcom je úžasné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Od Atén k Bruselu, Slováci spievajú !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Ľudia včerajška verzus tí ostatní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Lisánsky
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Lisánsky
            
         
        lisansky.blog.sme.sk (rss)
         
                                     
     
        Študent , ktorý píše o tom , čo ho baví a zaujíma ( a verí , že to baví a zaujíma aj iných ) .
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    820
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Komentáre a úvahy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




