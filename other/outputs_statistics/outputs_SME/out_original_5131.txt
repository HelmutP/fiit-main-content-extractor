
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Šipikal
                                        &gt;
                Nezaradené
                     
                 Tajnostkári starí egypťania. 

        
            
                                    23.4.2010
            o
            20:01
                        (upravené
                23.4.2010
                o
                22:01)
                        |
            Karma článku:
                6.10
            |
            Prečítané 
            1446-krát
                    
         
     
         
             

                 
                    Opisovali podrobne takmer všetko zo života. Mali precíznych pisárov, rôzne formy písma. Ale ako postavili pyramídy, o tom nenapísali nič.
                 

                      Starovekí egypťania postavili stavby, ktoré ľudí udivujú aj dnes. Azda najznámejšia Cheopsova pyramída pozostáva, pri výške 146 metrov, z približne 2,3 milióna veľkých kamenných kvádrov. Len jeden taký kváder váži 2 tony. Otázka: ako to dokázali, zamestnáva mnoho ľudí už celé stáročia. Pyramídy boli v čase svojej najväčšej slávy úchvatné. Budili v pozorovateľovi obdiv, ale aj rešpekt. Skúsme sa na chvíľu mysľou preniesť o tisícročia späť v čase a zamierme si to na gízsku plošinu. Keď sa budeme približovať k Cheopsovej pyramíde, uvidíme ešte dve ďalšie a veľkú sfingu. Už z diaľky je vidieť žltý záblesk. Jeden za druhým. Raz je ho viac, o chvíľu menej. Je to pozlátený špic pyramidy, ktorý pri dopade slnečných lúčov rozohráva žiariace trblietanie. Steny pyramíd žiaria belobou. Sme už blízko a už môžme rozoznať v spodných častiach stien, pri základni, pestrofarebné nápisy. Taký bol výsledný efekt. Steny pyramídy sa v závere prác obkladali lešteným vápencom. Preto boli pyramídy tak nádherne biele. To, v akom stave sú dnes, nemá na svedomí ani tak zub času, ako ľudia, ktorí v neskorších časoch vápenec postupne poodlamovali a používali ako materiál na iné stavby. Nakoniec stavebné dielo korunovali osadením pozláteného špicu. Sfinga blízko pyramíd nijako nezaostávala za nimi v dokonalom vypracovaní, tvarom aj farbami. Aj keď ju o stáročia neskôr Plínius nazval "obludou s červenou tvárou". Egypťania vôbec radi používali farby. Paláce, chrámy, vstupné brány, to všetko malo svoj štýl a pestrosť. Ale ako dokázali postaviť pyramídy?   Otec dejepisu Herodotos bol prvý, kto zanechal správu ako postupovali. O sfinge sa nezmieňuje, možno už bola v tom čase po uši v piesku. V diele "Dejiny" popisuje počet robotníkov a čas, ktorý bol potrebný na stavbu. O technike, spôsobe stavania sa zmieňuje dosť nejasne, keď píše o nešpecifikovaných drevených strojoch, ktorými  mohli egypťania  posúvať  kvádre postupne z poschodia na poschodie. Dôveryhodnejšie vyznieva správa Diodorosa, ďalšieho starovekého európana. Píše, že stavba pyramídy sa uskutočnila pomocou rámp, alebo valov po ktorých sa kvádre dopravovali do výšky. Ďalší starovekí spisovatelia a historici už nepriniesli v opisoch nič nové. Skôr už len nadväzovali na práce Herodota, Diodorosa a Strabona, posledného návštevníka pyramíd zo starého sveta, ktorého správa sa nám zachovala.   Dnes sú v tomto smere rôzne diskusie. Je veľa teórií, špekuľuje sa, experimentuje. Niektoré názory sú pritiahnuté za vlasy, iné vyznievajú realistickejšie. Napríklad Dick Parry uvádza, že kamenné kvádre robotníci obložili zo štyroch strán drevenými kolískami, čím vytvorili valec a mohli takto kvádre prekotúľať aj do výšky. Parry sa to snažil dokázať experimentom v praxi, ale myslím, že takýto spôsob prác by bol veľmi riskantný. Zástanci inej teórie popisujú, že kvádre sa posúvali pomocou drevenej guľatiny, ktorú stále postupne namáčali. Najviac je prijímané vysvetlenie o stavbe pomocou rámp. Približuje sa to tomu, čo môžme čítať v spise Diodorosa, ale v súčastnosti sa rozoberá už viac možností ako mohli byť rampy smerované. Zaujala ma teória Josepha Davidovitsa. Rozvinul jú v knihách "Stavitelia pyramíd" a "Nové dejiny pyramíd". Stručne povedané : pyramídy boli postavené pomocou betónu. Presnejšie, z re-aglomerovaných kameňov.  Podľa Davidovitsa bol postup taký, že sa prírodný vápenec rozdrvil, spracoval na betón a potom odlieval do pripravených "šalungov" priamo na stavbe. V ďalšej svojej knihe "Egypt" uzatvára : "Vychádzam z vedeckých analýz, archeologických dôkazov, hieroglyfických, náboženských  a historických aspektov. Na rozdiel od ostatných teórií, ktoré pátrajú iba po technickom vysvetlení stavby pyramíd Gízskej plošiny, či dokonca iba Cheopsovej pyramídy, je moja teória globálnou prezentáciou všetkých egyptských pyramíd behom 250 rokov ..."  V podstate táto teória odstránila problém, z ktorým viac, či menej zápasia iné teórie a to pokiaľ ide o počet robotníkov, dopravu a ukladanie kvádrov. Veľmi by sa tak zjednodušil celý pracovný proces.  Ale ani táto teória si nemôže nárokovať na jednoznačné prvenstvo. Až kým ďalšie výzkumy neodhalia viac, zostane na každom, aby sa priklonil k tomu, v čom vidí pravdu.   Je pozoruhodné, že pri všetkej pisárskej precíznosti, počas behu stáročí, nenašiel sa v Egypte nikto významný, kto by proces stavby pyramídy zaznamenal, alebo záznam inicioval. V každom prípade to nemení nič na skutočnosti, že pyramídy v Gíze sú pretrvávajúcim dôkazom výnimočných matematických, technických, ale aj estetických schopností starovekých egypťanov. A azda im aj prepáčime, že nám síce nezanechali správu ako stavali, ale že postavili. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (54)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Carnarvon: "Vidíte niečo?". Carter : "Ano. Nádherné veci!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Lišiacky človek, skvelý dejepisec. Josephus Flávius.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Šipikal 
                                        
                                            Možno vieme, prečo papyrus, ani kameň, nič nehovoria o Mojžišovi.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Šipikal
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Šipikal
            
         
        sipikal.blog.sme.sk (rss)
         
                                     
     
        Niečo o mne? Rád si vypočujem Shirley Bassey, Sarah Brightman, Robbieho Williamsa, či Barbru Streisand. Mám rád fitness v teórii i praxi, zaujímam sa o všetko, čo súvisí so starovekým Egyptom a biblickou archeológiou. Milujem atmosféru 20.-30. rokov 20.storočia so všetkým tým rozbiehajúcim sa pokrokom a nefalšovanou zdvorilosťou. Relaxom je pre mňa príroda, rodina, šport a spoločnosť priateľov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1321
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




