
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Lazárová
                                        &gt;
                tak mi napadlo
                     
                 naozaj nikdy? 

        
            
                                    1.12.2009
            o
            14:30
                        (upravené
                2.12.2009
                o
                0:45)
                        |
            Karma článku:
                10.69
            |
            Prečítané 
            2508-krát
                    
         
     
         
             

                 
                    Iste ste sa s tým stretli už aj vy - niečo spravíte a v tom okamihu sa nájde vo vašom okolí niekto, kto začne váš čin hodnotiť. No a na Slovensku sa hodnotí najmä negatívne, pozitívna kritika sa stretáva s nálepkami - pcháš sa do zadku, podliezaš, lichotíš, je to nevkusné. Tak teda rozmýšľam, koľko vkusu je v zahanbovaní, výsmechu, či výčitkách...
                 

                 Chodím po svete a všímam si hodnotiacich, odsudzujúcich a kritizujúcich ľudí. Niektorí kritizujú z titulu svojho veku, iní z titulu svojej funkcie, poniektorí z titulu svojej skúsenosti, ďalší z titulu svojho vkusu, niektorí z titulu svojho vierovyznania a poniektorí dokonca z titulu svojej väčšinovej sexuálnej orientácie. Alebo len nevyliečiteľnej netolerancie. Títo ľudia si osvojujú právo udeľovať pochvaly, či rozhrešenia a pritom sa od iných nelíšia ničím iným len obsedantnou túžbou posudzovať, hodnotiť, známkovať a porovnávať. Najmä so sebou samým!   Pretože oni by sa predsa nikdy nemohli dopustiť hlúposti, omylu, nikdy by nemohli zlyhať!   A tak sa vyjadrujú k spôsobu života iných, k výberu partnerov, k rodinnému stavu, k počtu detí, k ich výchove, k zotrvaniu vo vzťahu alebo k rozchodu, vyjadrujú sa k spôsobu obliekania, stravovania, myslenia, prežívania, milovania, alebo nemilovania, občas mám pocit, že majú hodnotiace položky aj na smiech a plač. Ich život je morálnejší, ich sex je prirodzenejší, záujmy hodnotnejšie, trávenie voľného času zmysluplnejšie, vedomosti rozsiahlejšie, vkus vytríbenejší, predvídavosť ostrejšia, zásady neotrasiteľnejšie, repliky inteligentnejšie, vtip údernejší, práca ušľachtilejšia, no najmä súdy spravodlivejšie.   Voľakedy som súdila rovnako plamenne a vypúšťala z úst slová typu: "Toto by sa MNE nemohlo NIKDY stať." A potom ma život preplieskal a ja som zistila, že som schopná všetkých možných slabostí a zlyhaní, že život so mnou dokázal tak zamávať, že som stratila nielen súdnosť ale aj zdravý rozum. Už neplatilo, že by som nikdy nedokázala klamať, nikdy sa nedokázala ponížiť, nikdy nikoho podviesť, nikdy nikomu ublížiť, nikdy sa zosmiešniť, alebo znenávidieť niekoho tak, že som bola len krôčik od toho, aby som uchopila nôž a vrazila ho tomu nenávidenému človeku do chrbta.   Pozerajúc sa na neomylnosť niektorých ľudí z môjho okolia, nechápavo krútim hlavou...   Pretože ja dnes už viem, aká krehká môže byť vo vypätej situácii ta hranica NIKDY...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (113)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Lazárová 
                                        
                                            A bolo za kým ísť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Lazárová 
                                        
                                            Bol som tu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Lazárová 
                                        
                                            Nikto z nás nepozná deň ani hodinu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Lazárová 
                                        
                                            Čaro spoločného čítania
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Lazárová 
                                        
                                            A správna odpoveď znie?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Lazárová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Lazárová
            
         
        lazarova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Nemám rada surovcov a hlupákov... 
 



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    300
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2567
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ostrovné správy
                        
                     
                                     
                        
                            ako sa rodia rozprávky
                        
                     
                                     
                        
                            lebo medveď :)
                        
                     
                                     
                        
                            správy vo fľaši
                        
                     
                                     
                        
                            dotkla som sa lásky
                        
                     
                                     
                        
                            nauč ma povedať zbohom
                        
                     
                                     
                        
                            deti a ich svet
                        
                     
                                     
                        
                            dobrú chuť :)
                        
                     
                                     
                        
                            ľudia okolo mňa
                        
                     
                                     
                        
                            tak mi napadlo
                        
                     
                                     
                        
                            o čom sa (ne)hovorí
                        
                     
                                     
                        
                            smiech je liek
                        
                     
                                     
                        
                            zápisky spoza oceánu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            O mojej knihe na Sieťovke
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            hudbu - vždy a všade
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            * blogy mojich priateľov
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Ema a ružová veľryba
                                     
                                                                             
                                            Medvedík bez mena na FB
                                     
                                                                             
                                            Sieťovka
                                     
                                                                             
                                            Moje knihy na Martinuse
                                     
                                                                             
                                            Krásna stránka o cestovaní
                                     
                                                                             
                                            Galéria slovenských autorov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bosoráctvo sa u nás naozaj dedí. Po praslici!
                     
                                                         
                       Skúste o tom pouvažovať
                     
                                                         
                       Ak by ste nevedeli, ako vyzerá šakal, tak ako orol
                     
                                                         
                       O jednom obchodnom stretnutí a láske k veľrybám
                     
                                                         
                       Slovenský hokejový fanúšik
                     
                                                         
                       Je o päť minút dvanásť...
                     
                                                         
                       Bábätká sa nepýtajú na deň v kalendári
                     
                                                         
                       Na zdravotných sestrách stoja a padajú nemocnice,
                     
                                                         
                       A čo tak lotéria z daňových priznaní?
                     
                                                         
                       Cesta
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




