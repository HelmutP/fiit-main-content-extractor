
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nora-Soul Slížová
                                        &gt;
                o živote
                     
                 Matky, ktoré ohrozujú svoje deti. 

        
            
                                    18.4.2010
            o
            16:59
                        (upravené
                18.4.2010
                o
                17:06)
                        |
            Karma článku:
                7.75
            |
            Prečítané 
            1809-krát
                    
         
     
         
             

                 
                    Nie vždy sa dá jednoznačne povedať, kde leží hranica medzi našou slobodou a trestným činom.  Kde je tá hranica, keď si len vychutnávame svoju slobodu a kedy už ubližujeme?
                 

                 Často keď kráčam po ulici sa snažím vnímať to čo sa deje vôkol mňa. Obrazy, zvuky  vône, atmosféru miesta, kde sa práve nachádzam. Stáva sa, že zazriem veci, ktoré mi pohladia dušu a vyvolajú úsmev na perách aj po týždňoch. Ako napríklad...    Kráčam okolo jazera, pár krokov predo mnou stojí krásne, asi 10 ročné dievčatko s dlhými blond vlasmi. Na nohách ma korčule a rukami objíma otca vôkol pása, hlavu má pritisnutú na jeho brucho, on jej hladka chrbátik, rozprávajú sa.    Ale nie vždy vidím len veci, ktoré tešia dušu. Často zazriem veci, ktoré vo mne vyvolávajú hnev. Ako napríklad...    Stojím na zastávke, pár krokov odo mňa stojí žena v pokročilom štádiu tehotenstva, v ruke drží cigaretu a spokojne vyfukuje dym. Alebo.. kráčam po ulici, prechádzam popri mladej tehotnej ženy, ktorá drží cigaretu a fajčí. Tento pohľad sa mi stáva žiaľ často.    A tak rozmýšľam nad človekom, ktorý ohrozuje život svojho dieťaťa. Ohrozuje jeho zdravie, lebo má chuť na nikotín. Ohrozuje zdravie toho, kto sa ešte nevie brániť, lebo sa jej nechce bojovať so svojou závislosťou. Ohrozuje zdravie nevinného, lebo predsa je to jej dieťa.   Včera som na internete našla takéto príspevky, je to niečo, čo sa len ťažko chápe. http://www.omimine.cz/home/detail?id=2790    Hovorí sa nesúďte, aby ste neboli súdení. Ale v tomto prípade súdim, súdim každú jednu matku, ktorá vystavuje svoje dieťa ohrozeniu kvôli svojej závislosti, kvôli cigarete, kvôli vlastnej hlúposti a egoizmu. A raz, keď sa koleso otočí a ja budem tá, ktorá bude ohrozovať svoje dieťa, chcem aby ma súdili, aby spoločnosť stala na strane  dieťaťa, aby ho ochránili, ak treba aj predo mnou. Lebo fajčenie počas tehotenstva nie je prejavom slobody, ale ohrozovaním zdravia a života dieťaťa.   Shakespeare by povedal, že je tu niečo zhnité v tomto štáte. Ja si myslím, že ak toto mlčky sledujeme, niečo je zhnité v celej spoločnosti. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (121)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Keď stojíme nad hrobom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Milujem oboch - a to rovnako
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Transformácia pesimistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Dining Guide - Kde sa najesť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Byť lepším človekom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nora-Soul Slížová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nora-Soul Slížová
            
         
        slizova.blog.sme.sk (rss)
         
                                     
     
         Človek, ktorý si chce nájsť čas pre uvedomenie si vlastných myšlienok, postojov, názorov. Pochopiť určitú vec, teóriu, či událosť, mi prináša osobný rast a vytúžený pocit štastia.   
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    409
                
                
                    Celková karma
                    
                                                4.81
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja Rodina
                        
                     
                                     
                        
                            Dining Guide
                        
                     
                                     
                        
                            Prúd myšlienok
                        
                     
                                     
                        
                            To čo ma potešilo
                        
                     
                                     
                        
                            To čo ma zarmútilo
                        
                     
                                     
                        
                            Vedeli ste, že...?
                        
                     
                                     
                        
                            o živote
                        
                     
                                     
                        
                            z vlaku
                        
                     
                                     
                        
                            Zvieracie
                        
                     
                                     
                        
                            readers diary
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Smutné ale pravdivé
                                     
                                                                             
                                            Suicide Read This First
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            C.G.Jung
                                     
                                                                             
                                            Fyzika I
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jesus Adrian Romero
                                     
                                                                             
                                            Fly To The Sky
                                     
                                                                             
                                            Rady svojich priatelov
                                     
                                                                             
                                            Hlas svojho srdca
                                     
                                                                             
                                            Svoje myšlienky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj prvý...(blog ktorý som čítala)
                                     
                                                                             
                                            Michal Patarák-Filozof v bielom plašti
                                     
                                                                             
                                            Kamilka- Úprimná duša
                                     
                                                                             
                                            Hirax
                                     
                                                                             
                                            DiDi- proste skvela
                                     
                                                                             
                                            Veronika Bahnová - Favorite Teacher
                                     
                                                                             
                                            Rolo Cagáň
                                     
                                                                             
                                            Jozef Klucho- Doktor bacsi
                                     
                                                                             
                                            Juraj Drobny-ODF- Velky drobec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Forum - psychológia
                                     
                                                                             
                                            I Psychologia
                                     
                                                                             
                                            Macher z KE :)
                                     
                                                                             
                                            Bennett Pologe, Ph.D.
                                     
                                                                             
                                            Kokopelli forum
                                     
                                                                             
                                            Umenie na iný spôsob- Akupunktura
                                     
                                                                             
                                            Ambulancia klinického psychológa
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Blog na SME bol výnimočný, už nie je
                     
                                                         
                       Nenapísaná poviedka
                     
                                                         
                       Čestný občan Róbert Bezák
                     
                                                         
                       Ako som Jožka odviezla na psychiatriu (smutný príbeh s úsmevom )
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Obyčajná láska
                     
                                                         
                       Rande na slepo
                     
                                                         
                       Kvapka krvi
                     
                                                         
                       Bolesť.
                     
                                                         
                       Taká obyčajná autonehoda
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




