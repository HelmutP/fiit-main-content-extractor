
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Sýkora
                                        &gt;
                Festival Jeden svet
                     
                 Filmový festival Jeden svet v Prievidzi v číslach 

        
            
                                    31.3.2010
            o
            11:25
                        |
            Karma článku:
                4.59
            |
            Prečítané 
            470-krát
                    
         
     
         
             

                 
                    Predminulý týždeň v sobotu bol v Dome kultúry v Prievidzi slovenským dokumentom Ako sa varia dejiny ukončený tretí ročník filmového festivalu Jeden svet. „Za organizátorov môžem vyjadriť spokojnosť – počet návštevníkov vzrástol, ale predovšetkým nás tešia ich pozitívne reakcie.“, konštatuje Ivan Sýkora.
                 

                   o festivale | filmy |  hostia |  sprievodné podujatia |  program |  ročník 2008   Súčasťou festivalu bolo 36 projekcií 31 rôznych dokumentov, vrátane doobedných školských predstavení. Prievidza privítala deviatich hostí vrátane Evelyn a Abdiho z Kene či Fedora Gála a Petra Zajaca, tváre Novembra ‘89.    Diskusia s Fedorom Gálom a Petrom Zajacom    Peter Zajac v detaile    Fedor Gál dáva rozhovor   Návštevníkov zaujala aj prezentácia Andreja Bána zo zemetrasením zničeného Haiti. Vďaka podpore sponzorov bolo možné celú cenu vstupného venovať na verejnú zbierku občianskeho združenia Človek v ohrození (ČVO) práve na obnovu Haiti. Spolu so zbierkou počas festivalu, to bolo tento rok 600 €.    Napätí (alebo len zaujatí?) návštevníci vyššie spomenutej diskusie   Poobedných premietaní sa zúčastnilo približne 1060 divákov, ktorý v ankete zvolili najlepším filmom festivalu snímku Príbehy z Jodoku o brutalite režimu v Severnej Koréi. Dokument prezentuje muzikál verne zachycujúci otrasné praktiky v severokórejskom koncentračnom tábore – z ktorého režisér muzikálu pred rokmi utiekol.             Veľmi vydarenými boli projekcie pre školy, ktoré sa okrem Prievidze uskutočnili aj v Novákoch a Bojniciach a navštívilo ich 1 982 žiakov a študentov. Práve mladí ľudia sú často hladní po nie úplne bežnom pohľade na veci okolo nás.    Premietanie pre školy v Novákoch   Ďalším rozmerom festivalu je aj „obyčajná ľudská“ pomoc v rámci možností. Vstupné počas premietaní pre školy, zbierka počas festivalu a individuálni darcovia umožnia 13 adoptovaným deťom z Afganistanu, ktoré by inak museli pracovať na ulici, navštevovať školu po dobu jedného roku v rámci dlhodobého projektu ČVO.    Fotenie s Evelyn a Abdim z Kene   „Oceňujeme výraznú podporu primátora mesta a Kultúrneho a spoločenského strediska v Prievidzi, vďaka ktorým je možné festival uskutočniť na úrovni a napríklad umiestniť vo foyeri Domu kultúry výstavy fotografií a tak dotvoriť atmosféru festivalu.“, hovorí Ivan Sýkora.    Zľava Abdi, Evelyn a Majka (projekt eRka Dary zo slumov Nairobi)   Pokiaľ ste festival Jeden svet tento rok premeškali, nemusíte zúfať. Organizačný tým už začal pripravovať Dozvuky Jedného sveta - to znamená pravidelné premietanie dokumentov, ktoré boli súčasťou festivalu v predchádzajúcich ročníkoch alebo sa jednoducho zatiaľ do výberu nezmestili.    A Abdi do tretice... so školákmi na doobednom premietaní   „Radi by sme Vás pozvali v piatok 16. apríla na projekciu dokumentu Vojnový fotograf, ktorý je silnou výpoveďou reportéra Jamesa Nachtweya, roky pôsobiaceho v krízových oblastiach sveta. Tento dokument bol nominovaný aj na Oscara a určite zanechá silný dojem!             DOZVUKY JEDNÉHO SVETA:  Vojnový fotograf - War Photographer  » Dátum: 16. apríla 2010  » Miesto: Dom kultúry, Prievidza  Viac info na FACEBOOKu         o festivale | filmy |  hostia |  sprievodné podujatia |  program |  ročník 2008   JEDEN SVET V PRIEVIDZI V ČÍSLACH &gt; 3 ročník festivalu v Prievidzi   &gt; 10 rokov Človeka v ohrození  &gt; 36 projekcií   &gt; 31 filmov   &gt; 9 filmových kategórií, vrátane školských predstavení  &gt; 1 060 divákov na poobedných premietaniach  &gt; 1 982 študentov na doobedných premietaniach  &gt; 313 € vstupné určené na adresnú pomoc prostredníctvom zbierky SOS Haiti  &gt; 63 € výťažok (= celá cena) z predaja festivalových tričiek, takisto určený na zbierku SOS Haiti  &gt; 307,09 € hodnota zbierky Tri oriešky pre..., z toho  &gt; 121,59 € Škola namiesto ulice  &gt; 159,20 € SOS Haiti  &gt; 26,30 € Aktivity ČVO  &gt; 2 600 € zložených zo vstupného počas premietaní pre školy, zbierky počas festivalu, individuálnych darcov a podpory partnerov umožní &gt; 13 adoptovaným deťom ulice po dobu &gt; 1 roka navštevovať školu  výťažok &gt; 600 € na zbierku SOS Haiti, zložený zo vstupného, zbierky počas festivalu a podpory partnerov  &gt; 5 diskusií a prezentácii počas poobedného programu  &gt; 9 hostí vrátane Evelyn a Abdiho z Kene a &gt; 3 filmárov  &gt; 2 výstavy fotografií s viac ako &gt; 30 vystavenými fotkami  viac ako &gt; 160 hlasovacích lístkov v ankete o najlepší film festivalu  &gt; 12 hlasov pre víťazný film Príbehy z Jodoku s hodnotením &gt; 1,08  &gt; 20 filmov s hodnotením &gt; 2,00 a lepším 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Nosme si do supermarketov vlastné „sáčky“
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Daruj darček
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Neľudské zlo v koncentračnom tábore Sachsenhausen
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Malé múzeum Stasi, tajnej služby z čias NDR
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Odtíňať ruky fajčiarskym prasatám
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Sýkora
            
         
        matejsykora.blog.sme.sk (rss)
         
                                     
     
        Človek. Aspoň sa snažím.
 Občas trochu stručný a chladný,
 inokedy zas obšírnejší a citlivý.

 Facebook 
 LinkedIn 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    151
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1173
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Občianske združenie BERKAT
                        
                     
                                     
                        
                            Potrebujeme (r)evolúciu?
                        
                     
                                     
                        
                            Život (je krásny)
                        
                     
                                     
                        
                            Svet ľudí sa nás týka
                        
                     
                                     
                        
                            Na Slovensku je to tak...
                        
                     
                                     
                        
                            Adjumani, Uganda
                        
                     
                                     
                        
                            Írsko, Dublin
                        
                     
                                     
                        
                            Londýn, Veľká Británia
                        
                     
                                     
                        
                            Randers, Dánsko
                        
                     
                                     
                        
                            New York, USA
                        
                     
                                     
                        
                            Triedenie odpadu
                        
                     
                                     
                        
                            Business
                        
                     
                                     
                        
                            Užitočné
                        
                     
                                     
                        
                            Festival Jeden svet
                        
                     
                                     
                        
                            Festival Pohoda
                        
                     
                                     
                        
                            SOS Povodne 2010
                        
                     
                                     
                        
                            STOP ZLU
                        
                     
                                     
                        
                            Energia naša každodenná
                        
                     
                                     
                        
                            Každý iný - všetci rovní
                        
                     
                                     
                        
                            Aktualizované z archívu
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo by som nešiel do povstania ?
                     
                                                         
                       TOP 10 výrokov politikov v roku 2013
                     
                                                         
                       Dobrý projekt jedna krádež nezastaví
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




