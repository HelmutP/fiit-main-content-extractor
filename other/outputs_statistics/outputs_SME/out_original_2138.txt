
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Sama Mareca príhody a skúsenos
                     
                 Ako sa Samo z Facebooku odhlásil 

        
            
                                    27.1.2010
            o
            11:32
                        (upravené
                1.6.2010
                o
                13:38)
                        |
            Karma článku:
                18.28
            |
            Prečítané 
            7415-krát
                    
         
     
         
             

                 
                    Som sa naštval, bo za dva dni zas dvaja ľudia na blogu napísali o Facebooku. Že hento-tamto, ako by povedal môj spolužiak Melo, plytvanie času, sociálne inžinierstvo a tak ďalej, veď vieme. Inak je Facebook fasa téma, asi ako Fico, interrupcie a viera, lebo o ňom môžete napísať hocičo a nenapíšete nič nové. Také témy ja úplne milujem.
                 

                 Ale som sa teda naštval. Neviem, čo to do mňa vošlo. Prišlo to náhle, náhlejšie ako konflikt Hvoreckého s Hlinom, v ktorom využívam právo nemať žiaden názor. Aj sa to oveľa náhlejšie vyriešilo.   Stál som vám asi pred hodinou na balkóne v user friendly teplote mínus pätnástich a štandardnej krakovskej zimnej hmle, keď som si znenazdajky povedal: a veruže to zruším! Veď už dáko bude. I som zrušil, trvalo to asi pätnásť minút. Pätnásť preto, lebo štrnásť z nich si všetci moji virtuálni priatelia mohli prečítať odkaz - odpíšte si môj mail, lebo ja stadeto odchádzam.   Deaktivovanie zabralo minútu, ale bola to minúta emočne nesmierne náročná. Facebook to na mňa skúsil ešte pred prvým krokom, keď mi oznámil, že budem chýbať nielen jej, čo sa jej teraz narodilo dieťa, ale aj jej, ktorá je dnes ráno po opici v robote a nebaví ju to. A nielen im dvom, ale aj jemu. On je v Írsku a je nezamestnaný. Citovo vydieračsky pripojil aj fotky, aby som si uvedomil, že za tými menami na obrazovke sa skrývajú reálni ľudia. Čo si niekedy nie je škoda pripomenúť, všakáno.   Skúsil to na mňa aj pri druhom kroku, lebo sa ma spýtal, prečo to vlastne odchádzam. I don´t find Facebook useful, zaškrtol som. Hádam neodhalí, že som klamal. Neni problém, zareagoval promptne, stačí si pozrieť Help a určite sa mi Facebook zapáči viac. Lenže ja som bol odhodlaný a s nemilosrdnosťou Huna s krivou šabľou a koňom s krivými nohami som to odklikol.   To ako fakt? spýtal sa ma v treťom kroku a ja som potvrdil, že to ako fakt. Tak mi aspoň pripomenul, že kedykoľvek sa môžem vrátiť, čo aj pravdepodobne spravím hneď potom, ako dokončím diplomovku. Život je ale niekedy aj o prioritách.   Ta dobre, rezignoval onen Facebook, ktorý inak rovnako ako svoju diplomovku, považujem za živý organizmus s vlastným vedomím. Blbosť, ja viem. Už som len potvrdil heslo, prepísal kód proti spamu a ocitol som sa odhlásený von z toho sveta, v ktorom ma napríklad mohlo upokojiť vedomie, že Juro ešte tú diplomovku ani písať nezačal. Fuj, to som hnusne bonzol. Ale veď sa aj tak Juro nevolá.   Pozrite, ono ten Facebook má svoje výhody, okrem iného aj profesionálne, pretože váš kolega vám to jedno slovo, na ktoré si pri preklade neviete spomenúť, poradí rýchlejšie ako ho nájdete v slovníku. Má svoje výhody, pretože som tam mohol vždy nalinkovať svoj nový článok a potom sa kochať tým, ako sa ľuďom páči. Mohol som sa podeliť so svojim vycibreným hudobným vkusom, byť prvým, kto upozorní na najnovší vládny škandál, mohol som vedieť, že v Bystrici je dnes mínus jedenásť a v Tirane teplejšie. Teraz už nebudem vedieť, že Tina, ktorá je niekde v Amerike, sa rozišla so svojim frajerom a nebude ma to ani zaujímať, čo je len návrat k stavu pred Facebookom. Hej, tak to je, úplne cynicky. Je mi jedno, že sa s ním rozíde. Úplne. Navyše mi to aj vždy jedno bolo. Je to, koniec-koncov, jej život, nie môj.   Že bude môj svet bez týchto informácií presne rovnaký ako predtým, je jasné. Mojich 610 priateľov, z ktorých, a to musím neskromne podotknúť, niekoľkí boli fanúšikmi môjho blogu, to prežije. Tak ako to prežijem ja a radšej sa vyberiem do skutočného sveta plného skutočných ľudí, kde nie všetci sú mojimi priateľmi, ale aspoň sa ich môžem dotknúť. Je to svet reálny, pretože iba v nereálnom svete sú všetci ľudia priateľmi.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (61)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




