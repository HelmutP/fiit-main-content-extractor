
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Flamík
                                        &gt;
                Súkromné
                     
                 Premieňajúca sila dobra 

        
            
                                    24.3.2010
            o
            9:38
                        |
            Karma článku:
                6.86
            |
            Prečítané 
            768-krát
                    
         
     
         
             

                 
                    Tri dni... päť predstavení... 40 členný kolektív zložený výhradne z mladých ľudí. Vekový priemer 15... viac ako 1000 účastníkov... to bolo naše marcové turné po našom okolí... a hlavne sila dobra, ktorá je v mladých...
                 

                 Volá sa to celé „Stačí otvoriť dlaň“. Myslím, že nás to hodne vystihuje. Príbehy mladých. Zrady a klamstvá. Svet ilúzií, ktorý ich zahaľuje do rúška nevedomosti... Žijú a predsa často krát len prežívajú... Aj o tom je toto divadlo, ale nielen. Je hlavne o mladých, ktorí dokázali venovať svoj čas nácvikom textu, tancov, spevu... a predovšetkým vzájomnej súhre pre niečo veľké. Niečo o čo sa oplatí snívať. Pre čo sa oplatí aj žiť...   Pre nás to bola radosť, láska, dávanie sa... nájdenie a dávanie krásy a plnosti života aspoň v čriepkoch, ale predsa tým, ktorí prišli a aj tým, ktorí nám fandia.   A preto vďaka. Im, ale aj každému jednému z vás, kto neláme palicu a nezhasína knôt...   Pekné dni. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Ak môžeš, konaj dobro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Čo iné povedať, ak nie slová nádeje?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Si človekom nádeje?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Všetko najlepšie, tati
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Život a nádej sú silnejšie ako smrť a zúfalstvo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Flamík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Flamík
            
         
        flamik.blog.sme.sk (rss)
         
                                     
     
        Salezián. Od 18. júna 2006 kňaz. Nachádzaný Niekym, kto aj k nemu si stále a verne hľadá cestu... S úctou.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    467
                
                
                    Celková karma
                    
                                                7.38
                    
                
                
                    Priemerná čítanosť
                    950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z môjho vnútra
                        
                     
                                     
                        
                            rozličné hlasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Beato Claudio Granzotto
                                     
                                                                             
                                            O Don Boskovi viac...
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            S. Weil
                                     
                                                                             
                                            F.X.Durwell
                                     
                                                                             
                                            C.S.Lewis
                                     
                                                                             
                                            T. Halik
                                     
                                                                             
                                            Biblia
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Into the wild - soundtrack
                                     
                                                                             
                                            Sakumprásk
                                     
                                                                             
                                            Delirious?
                                     
                                                                             
                                            U2 | ...čokoľvek a kedykoľvek
                                     
                                                                             
                                            Dream theater | ...hudobní blázni
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janka Balážková
                                     
                                                                             
                                            Marek Sefcik
                                     
                                                                             
                                            fajn rehoľná sestra
                                     
                                                                             
                                             
                                     
                                                                             
                                            Miro Lisinovic
                                     
                                                                             
                                            Ivan Gábor
                                     
                                                                             
                                            Matúš Repka
                                     
                                                                             
                                            Jakub Betinský
                                     
                                                                             
                                            Ivo Hampel
                                     
                                                                             
                                            Jozef Bednár
                                     
                                                                             
                                            Juro Begany
                                     
                                                                             
                                            Mirka Zilinska - cit a pravda
                                     
                                                                             
                                            Juraj Šeliga - hlbka
                                     
                                                                             
                                            Robo Kekiš - otvorene oci
                                     
                                                                             
                                            Ondrej Močkor - misionár
                                     
                                                                             
                                            Maroš Karásek - umelec
                                     
                                                                             
                                            Marcel Tomaškovič
                                     
                                                                             
                                            Roman Effenberger
                                     
                                                                             
                                            Peter Grobarčík
                                     
                                                                             
                                            Marek Piváček
                                     
                                                                             
                                            Andrej Skovajsa
                                     
                                                                             
                                            Petra Bošanská
                                     
                                                                             
                                            Mato Habcak
                                     
                                                                             
                                            Karol Trejbal
                                     
                                                                             
                                            Bianka
                                     
                                                                             
                                            Jolana Čuláková
                                     
                                                                             
                                            Martin Liziciar - student s odvahou sa pytat
                                     
                                                                             
                                            pani Kohutiarová -mama!
                                     
                                                                             
                                            Michal Hudec - pravdivosť v hľadaní a objavovaní
                                     
                                                                             
                                            Angelo Burdej - hlboký a jednoduchý
                                     
                                                                             
                                            Karol, človek s rozhľadom a rešpektom
                                     
                                                                             
                                            MartinDinuš a Motorkári
                                     
                                                                             
                                            Jožo Červeň - kňaz na svojom mieste, odvážny
                                     
                                                                             
                                            Rudo Kopinec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Božie Slovo na každý deň
                                     
                                                                             
                                            DELIRIOUS?
                                     
                                                                             
                                            informačný portál
                                     
                                                                             
                                            welcome in dbland
                                     
                                                                             
                                            mnoho o svatych v taliančine
                                     
                                                                             
                                            kvalitne zdroje
                                     
                                                                             
                                            môj  novy domov - Partizánske - Šípok
                                     
                                                                             
                                            pápež a info o ňom
                                     
                                                                             
                                            Prameň života
                                     
                                                                             
                                            sastinska bazilika
                                     
                                                                             
                                            exallievi
                                     
                                                                             
                                            O NÁDEJI...
                                     
                                                                             
                                            o Lumene...
                                     
                                                                             
                                            o Hudbe...
                                     
                                                                             
                                            zdroj pre modlitbu
                                     
                                                                             
                                            Knazske inspiracie
                                     
                                                                             
                                            Spolocenstvo mladych
                                     
                                                                             
                                            telvízna alternatíva LUX
                                     
                                                                             
                                            dobre knihy
                                     
                                                                             
                                            duch. cvicenia
                                     
                                                                             
                                            TO JE ZIVOT
                                     
                                                                             
                                            PUT ZALUBENYCH 2009!!!
                                     
                                                                             
                                            Bazilika Sastin - Straze
                                     
                                                                             
                                            GJB Sastin -Stráže
                                     
                                                                             
                                            kvalita o modlitbe
                                     
                                                                             
                                            salezianska kapela
                                     
                                                                             
                                            saleziáni na Slovensku
                                     
                                                                             
                                            Holic
                                     
                                                                             
                                            recenzie na filmy v pohodke
                                     
                                                                             
                                            salici v Petržke
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kedy sa stávaš človekom?
                     
                                                         
                       Muž milión
                     
                                                         
                       O dvoch dobách
                     
                                                         
                       Cesta za šťastím
                     
                                                         
                       Kňazi - tak trochu inak
                     
                                                         
                       Prvé Dušičky bez Teba
                     
                                                         
                       Som za život – prosím, nazvite ma bigotným demagógom
                     
                                                         
                       Prečo som ešte stále verný svojej žene?
                     
                                                         
                       Stopárske čriepky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




