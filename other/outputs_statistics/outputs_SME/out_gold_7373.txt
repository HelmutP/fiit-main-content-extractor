

 Z mailu čitateľa: 
Na križovatke, ktorej fotku vám posielam v prílohe, som dostal pokutu za 
nerešpektovanie značky STOP. Moje auto bolo v ľavom pruhu (ako je teraz zelený 
Passat). 
Bola pokuta opodstatnená, keď značka STOP sa nachádza IBA ...? 
 
 
 
Obr.1. Reálna situácia. Semafory blikajú na oranžovo. 
 
 
Najprv čo hovoria príslušné predpisy: 
8/2009 Z.z. Dopravné značky a dopravné zariadenia  
§ 60  
 
(1) V cestnej premávke sa používajú  
a) zvislé dopravné značky,  
b) vodorovné dopravné značky,  
c) dopravné zariadenia.  
 
(2) Úprava cestnej premávky vykonaná dopravnými značkami a dopravnými 
zariadeniami je nadradená všeobecnej úprave cestnej premávky.  
 
(3) Ak je nesúlad medzi zvislými dopravnými značkami a vodorovnými dopravnými 
značkami, zvislé dopravné značky sú nadradené vodorovným dopravným značkám.  
 
(4) Pri riadení cestnej premávky dopravným zariadením sú svetelné signály 
nadradené dopravným značkám a ostatným dopravným zariadeniam.  
 
(5) Pokyny policajta a pokyny inej oprávnenej osoby sú nadradené pokynom 
vyplývajúcim z dopravných značiek a dopravných zariadení.  
 
(6) Vlastník nehnuteľnosti je povinný za primeranú náhradu strpieť umiestnenie 
dopravnej značky alebo dopravného zariadenia a ich nosnej konštrukcie na svojej 
nehnuteľnosti.  
 
(7) Na dopravných značkách alebo na dopravných zariadeniach a ich nosnej 
konštrukcii je zakázané umiestňovať čokoľvek, čo nesúvisí s dopravnou značkou 
alebo s dopravným zariadením. Zvislé dopravné 
značky sa umiestňujú pri pravom okraji cesty v smere jazdy vozidiel.
Ak si to vyžaduje bezpečnosť a plynulosť cestnej 
premávky alebo nevyhnutnosť zvýrazniť dopravnú situáciu, je možné umiestniť 
zvislé dopravné značky i na ľavom okraji jazdného pruhu alebo cesty približne na 
rovnakej úrovni oproti sebe.  
 
(8) Zákazové a príkazové dopravné značky, ktoré sa vzťahujú na premávku v 
príslušnom jazdnom pruhu, umiestňujú sa nad týmto jazdným pruhom. Dopravné 
značky upravujúce zastavenie alebo státie sa umiestňujú na tej strane cesty, na 
ktorú sa vzťahujú; to neplatí, ak ide o jednosmernú cestu, kde sa takéto 
dopravné značky môžu umiestniť po oboch stranách cesty.  
 
(9) Na ceste a na mieste pri ceste sa nesmú umiestňovať veci, ktoré by mohli 
viesť k zámene s dopravnou značkou alebo s dopravným zariadením alebo by ich 
zakrývali, alebo ktoré by rozptyľovali a upútavali pozornosť účastníka cestnej 
premávky, alebo ho oslňovali.  
 
(10) Miestom pri ceste sa rozumie priestor, v ktorom je umiestnená dopravná 
značka alebo dopravné zariadenie; takýmto miestom je aj priestor, v ktorom sa 
dopravné značky alebo dopravné zariadenia spravidla umiestňujú.  
 
(11) Dopravné značky a dopravné zariadenia sa umiestňujú tak, aby sa vzájomne 
neprekrývali a účastník cestnej premávky ich mohol včas spozorovať.  
 
(12) Svetelné signalizačné zariadenia udržiava správca cesty. V blízkosti 
svetelných signalizačných zariadení sa nesmú umiestňovať svetelné zdroje, 
ktorých svetlá by mohli viesť k zámene so svetlami svetelných signalizačných 
zariadení.  
 
(13) Podrobnosti o dopravných značkách a dopravných zariadeniach, ich 
vyobrazenie, význam a umiestňovanie na ceste a na mieste pri ceste ustanoví 
všeobecne záväzný právny predpis, ktorý vydá ministerstvo vnútra. 
 
 
Čiže keď semafory neriadia križovatku (ani policajt ap.) platia dopravne značky. 
V križovatke je v prichádzajúcom smere použitá dopravná značka STOP, ktorá 
určuje, že vozidlo musí zastaviť a dať prednosť v jazde. 
 
Čo nie je v križovatke dobre spravené je to, že semafory sú umiestnené po oboch 
stranách cesty, ale značka STOP je umiestnená len vpravo, čo môže uvádzať vodičov do omylu, najmä pokiaľ výhľad vpravo majú 
zakrytý vozidlami v súbežnom pravom jazdnom pruhu. 
 
 
Záver: 
Pokuta bola síce oprávnená, lebo je potrebné sa riadiť zvislým dopravným 
značením umiestením pri pravom okraji vozovky, ale policajti namiesto udeľovania 
pokút, by mali podať podnet správcovi komunikácie a správnemu orgánu aby dali 
dopravné značenie po oboch stranách cesty do vzájomného súladu. Ak je semafor i 
na ľavej strane cesty, tak to značí, že je tak spravené lebo to vyžaduje 
bezpečnosť a plynulosť cestnej premávky alebo nevyhnutnosť zvýrazniť dopravnú 
situáciu. Keď je to tak, tak je potrebné aby aj dopravné značenie platné pri 
vypnutých semaforov bolo rovnaké po oboch stranách cesty. 

