
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Epigramiáda
                     
                 Kto je slovenský mesiáš...? 

        
            
                                    13.4.2009
            o
            7:30
                        |
            Karma článku:
                6.55
            |
            Prečítané 
            2272-krát
                    
         
     
         
             

                 
                    Veľkonočné príbehy staré dvetisíc rokov sú spojené s menom Ježiša Krista. Toto meno vníma dnešný človek ako analógiu mena a priezviska. Nuž nie je to celkom tak. Kristus sa slovakizoval z gréckeho pomenovania christos, ktoré pochádza ako preklad zasa z hebrejského mašiah.
                 

                 
V Emmauzoch...archiv
    Bolo to označenie toho, kto bol pomazaný na znak svojej kráľovskej hodnosti a v biblickej kultúre je spojené aj s osobitným tzv. božím vyvolením. V slovenskej a inej frazeológii sa toto slovo udomácnilo aj ako slovo z úplne inej sémantickej skupiny ako mesiáš. Je tomu tak preto, lebo prororocké spisy hebrejského Tanakhu či kresťanskej Biblie definovali Pánovho pomazaného ako Mesiáša, záchrancu, vysloboditeľa.   Veľkonočný Jeruzalem sa stáva dejiskom mnohých zaujímavých scén. Okrem katolíckej krížovej cesty, pravoslávneho uctievania Božieho hrobu či evanjelizačných happeningov protestantských zoskupení pri tzv. Garden Tomb je to aj zaujímavý fenomén tzv. Jeruzalemského Mesiášskeho syndrómu. Jeruzalemskí psychiatri sa jeho diagnostikovaniu začali venovať od polovice 60. rokov minulého storočia.   Neskoršie zvýšené nároky na kapacity spojené s hospitalizáciou pacientov s týmto syndrómom viedli k vytvoreniu kliniky, kde sa nikto iný okrem „mesiášov" nedostane. Ide vraj o úkaz pri ktorom sa psychicky zdraví jedinci - pútnici a inak nábožensky motivovaní návštevníci Svätej Zeme v emocionálnom vytržení z ničoho nič cítia ako mesiáši. Druhý príchod Ježiša Krista či príchod židovského mesiáša, ktoré majú tieto dva monoteizmy ešte stále na programe, tomuto fenoménu výrazne nahráva. Postihnutí sa dostávajú do stavu, v ktorom prorokujú, identifikujú sa s biblickými postavami a neraz sami seba označujú za očakávaného mesiáša - záchrancu a vykupiteľa.   Okrem oddychu, zlepšeného a kontrolovaného pitného režimu je spoľahlivou súčasťou terapie vycestovanie mimo Svätej Zeme. Tam sa stratia príznaky a „postihnutí" sa vracajú zvyčajne do normálneho života. Možno je to úsmevný príbeh, no prečo o tom neuvažovať vo veľkonočný pondelok aj týmto spôsobom.   Tento svet bol definitívne vykúpený pred dvetisíc rokmi. I keď priatelia židia majú na to iný názor , tomuto veria kresťania. Iný mesiáš už nepríde.  Nič k tomu už dnes nikto nemôže dodať. Navyše nie všetci považujú biblické rozprávanie o jeruzalemskom veľkonočnom ráne za udalosť, ktorá by zmenila či ovplyvnila nejakým zásadným spôsobom ich osobný život. Kto nerešpektuje rok 0, nemusí si robiť ani zvláštne záznamy pri rokoch 1969, 1989 či 2009. Doteraz slovenskí pohlavári deportovali len svojich vlastných obyvateľov. Nastal čas, kedy by si zaslúžili deportáciu do minulosti alebo aspoň do normálneho sveta tí, ktorí si robia z ľudí od Tatier k Dunajskej Strede bláznov. Ako to povedal pán prezident: ... dali novinárom boha..., ale my dobre vieme, že on to tak nemyslel. Ach tá interpunkcia.   Zaoberám sa však jednou otázkou. Kam by asi mali vycestovať najväčší vodcovia slovenskí aby sa zbavili príznakov mesiášskeho syndrómu. Jeho pôvod je u nakazených rôzny, príznaky sú podobné. Slovenský skanzen s obchodmi na korze je znovu obývaný tými pravými a podtatranský hospodársky tiger sa v ich rukách mení na plaché mačiatko. Na poľnej ceste pristáva chorvátsky špeciál. V pilotnej kabíne to vonia ríbezľovým vínom. Na poľsko-maďarskej hranici nič nového. Čo takto jeruzalemská klinika...? Aj drahé hodinky len ukazujú dávno odmeraný čas. Cestou do Emmauz sa všetko ukázalo v dosť inom svetle. Rembrandt a Caravaggio o tom vedia svoje. A emauzskí učeníci tiež. Ak ste o tom počuli viete o čo ide. Aj naši mocní majú poradcov. Ak im toto nebudú vedieť vysvetliť, tak ich platia zbytočne. Môžu "vycestovať" spolu s nimi.   Aj mesiáš kresťanov so slovenským pasom je totiž etnický žid , Ježiš z Nazaretu. "Horšie" by to mohlo byť už len keby bol maďarského pôvodu. O tomto bode sa v civilizovanom svete ani v odborných kruhoch zatiaľ nevedú diskusie. Ale prečo by sme si nezaspievali. Pekné veľkonočné rána, Vám, nám aj tým druhým...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




