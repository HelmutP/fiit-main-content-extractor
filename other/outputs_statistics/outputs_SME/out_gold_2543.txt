

 Nápady svaté Kláry je satirický román, o ktorom som doteraz nikdy nepočul. Bol napísaný v postnormalizačnom období človekom, ktorý mal za sebou už desiatky vydaných diel. A je to nielen satira, ale vtipná satira. Tne na dve strany – robí si srandu i z prívržencov socialistického režimu (stranícki šéfovia, riaditeľ školy, ...), ale i z prístupu cirkvi k štátu a zázraku. 
 
Zázraku? 
 
Áno. Zázrakom a zároveň problémom je malá Klára, žiačka základnej školy a jej nápady. Každého občas niečo napadne. Klárin problém spočíva v tom, že ju napadá, čo by sa mohlo stať. A nielen že ju to napadá. Ono sa to potom aj stáva. 
 
Také voľačo! Samozrejme je to neprípustné. Musí sa to riadne vyšetriť, a to z hľadiska dialektického i religiózneho. Prijať príslušné opatrenia, pravda, berúc do úvahy obsah nápadov, lebo však len blbec by ignoroval také indície. A už to ide... 
 
 
 
 
Na tému vyšetrovanie vinných i nevinných: 
 

Jsou země, jejichž vlády tvrdí, že každý slušný občan musí být alespoň jednou za život v kriminále. Toto tvrzení je oprávněné, jestliže počet občanů, kteří v kriminále nebyli, nestačí ani k vytvoření vlády, v níž proto musí zasedat i kriminálníci.  
 
Rozhodně však platí téměř ve všech zemích, že každý slušný občan je nejméně jednou za život vyšetřován, i na to připravuje budoucí občany škola. Lze říci, že praktickým cvičením v tomto oboru věnuje mnohem vice času než třeba poučení o prevenci početí.  
 
V hodinách vyšetřování, ať už k nim dává podnět ztráta třídní knihy nebo kouř na chlapeckém záchodku, se vyučuje i zkouší vše, s čím se žák setká a co se od něho bude vyžadovat i v praktickém životě: obratné vytáčení i urputné zapírání, otcovská domluva, ústící v existenční nátlak, udávání jako polehčující okolnost i přiznání jako jediný důkaz viny.  

 
Vskutku vynikajúca pasáž, ako hovorí moja milá, taka nadčasová. Vznikla v sedemdesiatych rokoch 20. storočia a dnes platí úplne rovnako. Veď si len skúsme vybaviť nejakú vládu, v ktorej sedia kriminálnici. Že takú nepoznáte? To kde žijete? Aj s prevenciou počatia na tom nie sme úplne ideálne. Strata triednej knihy mi evokuje Cimrmana, dym na chlapčenskom záchode posledný liter vína a kvalita zapieračov v porovnaní s úspešnosťou vyšetrovateľov je v niektorých oblastiach až legendárna. Odložíme, uvidíme. 
 
 
 
Na tému honoru učiteľov (Plavec je riaditeľ školy, ktorý pozýva do školy Kláriných rodičov): 
 

Vůbec se nezeptala, oč běží. Plavec si teď uvědomil, že ani ji, ani jejího manžela za celá léta neviděl ve škole. Pravda, děvče mělo dobré známky a do dnešního dne i dobré mravy, ale stejně: rodiče se mají chodit ptát na děti už z úcty k pedagogům. 

 
Ja Raťafák Plachta smejem sa hlasno. Táto pasáž je naopak archaická až strach. Spýtajte sa svojich známych učiteľov, akú úctu k nim okolie chová. Je až s podivom ako hlboko sme klesli, ako málo si spoločnosť váži prácu učiteľov. Pokiaľ ma moje historické vedomosti neklamú, učitelia nikdy nepatrili k najbohatšej vrstve, ale … škoda reči. 
 
 
 
Na tému odovzdávania citlivých vedomostí ďalším generáciám (pred Tikalom st. si žiadna sukňa v nemocnici a okolí nemôže byť istá, v čase udalostí knihy istá Dušková): 
 

… přisvědčil doktor Tikal; dostal zlost na sebe, že má syna, a na syna, že ho neinformuje o událostech, které zajímají sestru Duškovou. Sebere mu Dokonalé manželství, rozhodl se: podle vzoru svého otce ukrýval do uzamčeného oddělení knihovny pečlivě vybranou literaturu, z níž se měl jeho syn tajně poučovat. 

 
Zaujímalo by ma, koľkí rodičia či iní "nadriadení" v skutočnosti používajú podobnú fintu. Aby si zaistili, že deti či iní "podriadení" sa dozvedia, čo majú, informáciu mierne zneprístupnia a hrajú na prirodzenú zvedavosť a chuť zakázaného ovocia. (Problém nastane vo chvíli, keď dieťa náhodou zákaz rešpektuje alebo nie je zvedavé.) 
 
 
 
Na tému ťažkostí pri nadväzovaní milostných kontaktov: 
 

Avšak dívka vstrčila osvobozené ruce do kapes džínové sukně  a zkusila skákat po jedné noze. 
 
O čem hovořit s dívkou, kterou tajně milujeme, skáče-li po jedné noze? I vědecká díla jako Dokonalé manželství, uvádějící tucty jakýchsi nepohodlných poloh, o tom mlčí, a nablízku není nikdo, s kým se lze poradit. Klára skákala a skákala. Napadlo ho, že není ozvěny bez zvuku. Po mučivé rozvaze začal tedy na časové téma. 

 
Fakt tam nie je žiadna poloha na jednej nohe?  
 
 
 
Na tému výkladu jedného porekadla: 
 

Druhou událostí, která vstoupila do obecní kroniky a městského folklore, byl krátký pobyt jízdního pluku generála Pappenheima, spěchajícího posílit sbory generalissima Valdštejna před bitvou u Lützenu. Pluk tu nocoval 14. listopadu 1632, ale vzpomínka se hlavně váže k 12. srpnu příštího roku, kdy se v S., kde žilo tou dobou 163 dívek a žen ve věku od čtrnácti do čtyřiapadesáti let, narodilo od půlnoci do půlnoci celkem 177 dětí: každé ženě a dívce zmíněných ročníků nejméně jedno. I sám jezuita páter Hlad byl nucen prohlásit, že se S. dostalo obzvláštní milosti Boží , a uspořádal společný křest, při němž všechny děti ženského pohlaví v počtu 109 obdržely jméno Klára: bylo to podruhé, co patronka obce zařídila, aby se místo krveprolití konala veselice, za což jí byl zastaralý gotický kostelík přestavěn v tehdy módní barokní chrám. Významný obolus zaslal v odpověď na petici pátera Hlada sám Valdštejn: upozorněn císařským fiskusem, že páterova stížnost líčí úkaz málo věrohodný, odvětil větou, jež se donesla až k nám: Ich kenne meine Pappenheimer! 

 
Tak konečne viem, ako to bolo. Toten Valdštejn! 
 
 
 
Odporúčam, odporúčam silno! 


