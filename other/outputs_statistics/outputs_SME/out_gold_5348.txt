

 Prejde zopár rôčkov a my sa v živote stretneme so všeličím. Prejdeme rozličnými cestami, musíme čeliť všakovakým nástrahám, prekážkam, zvodom.. Nie vždy a zo všetkého vyjdeme so cťou a tak, že sme na seba hrdí. Musíme robiť kompromisy, občas zvoliť cestu menšieho zla. Takáto skúsenosť zväčša obrúsi aj naše súdy a posunie latku tolerancie voči svojmu okoliu. S pribúdajúcimi rokmi sa akosi z nášho slovníka vytráca to slovné spojenie: “Ja by som nikdy…”  
 Áno, určité hranice tolerancie by mali byť vždy. Sú veci, ktoré nemôžu byť akceptovateľné za žiadnych okolností. Ale pravdupovediac, je ich stále menej. Schopnosť kritického pohľadu na samých seba posúva našu latku tolerancie stále vyššie a vyššie. Až sa popod ňu prepchá už strašne veľa vecí. To neznamená, že nám naše okolie nedokáže ublížiť, nie všetko dokážeme odpustiť, ale pochopiť sa dá už skoro všetko. Prinajmenšom ak sa netýka bezprostredne nás, alebo s určitým časovým odstupom. 

