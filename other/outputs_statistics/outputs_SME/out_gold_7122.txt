

 Semienko odstraňuje zápchu a zastavuje hnačku. Upokojuje sliznicu žalúdka, lieči žaludočné vredy, zvonku priložené zastavuje bolesť. 
 V ponuke lekární nájdeme ľanový olej, ktorý sa môže používať  vnútorne aj na vonkajšie účely. Vnútorne znižuje cholesterol, tlak krvi, lieči reumatické zápaly, spevňuje nechty a vlasy. Zvonku lieči popáleniny a má regeneračné účinky na pokožku, je súčasťou mnohých kozmetických prípravkov. Odporúčam siahnuť po oleji, ktorý je lisovaný za studena, zväčša je označený ako "bio". 
 Čomu vďačí ľanové semienko za svoje liečivé účinky? Ide predovšetkým o vysoký obsah vzácnych omega-3 mastných kyselín, lignanov, fytoestrogénu a vlákniny. 
 Poradca pre naturálnu medicínu Ján Dedík získal z nemeckého zdroja recept na  protirakovinovú kúru, ktorej súčasťou je práve ľanové semienko.  Nakoľko ani farmaceutický priemysel nepodceňuje protirakovinový účinok, myslím, že túto kúru netreba podceňovať. 
 Trojtýždňová protirakovinová kúra: 
 necelú lyžičku ľanového semienka pomelieme (pomôže aj kávomlynček) a posypepeme ním kocku tvarohu, nie nízkotučného, je potrebné, aby mal minimálne 10% tuku. Toto množstvo predstavuje dennú dávku, ktorú si treba rozdeliť do niekoľkých menších porcií. Upozornenie: nemelieme do zásoby, semienko má byť vždy čerstvo zomleté! Zmes môžeme konzumovať prípadne aj s banánom, s ryžou natural, zjemníme jogurtom... 
 Aby sa uvoľnené škodliviny dostávali z tela von je potrebné piť dostatočné množstvo urologických čajov, cviklovú šťavu. Vhodným doplnením je strava v zmysle protiplesňovej diety. 
   
 Semienka zbavujú bolestí vnútorných orgánov. Obklad si pripravíme jednoducho: 
 ľanové semienka, pomleté či celé vložíme do pláteného vrecúška, ktoré na desať minút ponoríme do horúcej (ale nie vriacej) vody. Necháme odkvapkať a čo najteplejšie prikladáme na bolestivé miesto. 
 Výskumy potvrdzujú, že nedostatok omega-3 mastných kyselín sa spája aj s Alzheimerovou chorobou a s problémami pamäti. Semienko je ich bohatým zdrojom. 
 Semienka celé, alebo mleté môžeme používať aj v našej kuchyni. Môžeme ich pridať (trebárs jemne opečené) do šalátu, do vločiek, do cesta na slané pečivo, prípadne ho ním posypeme. Aj na pultoch predajní nájdeme chlieb,  pečivo, či tyčinky ktoré ho obsahujú. 
 Myslím, že toto malinké semienko dá ešte o sebe počuť. Určite ho budeme čoraz častejšie tasiť ako zázračnú zbraň  na mnohé neduhy. Verím, že jeho popularita bude stúpať a jeho účinky budú naďalej prekvapovať. Som presvedčená, že je a bude účinnou zbraňou v prevencii, ale aj pomocníkom v chorobe. 
 Kúpiť si vrecúško so semiačkom už dnes nie je problém. Tak prečo tak neurobiť? 
 **** 
 Dovolím si pripojiť aj vlastnú skúsenosť so semienkom, čo sa týka liečby kašľa: 
 4 polievkové lyžice semienka zalejeme cca 2 deci horúcej vody. Necháme postáť 2-3 hodiny. Precedíme a osladíme medom. Dávka pre deti je jedna čajová lyžička a pre dospelých 1 polievková lyžica 4-5 krát denne. Môžeme pridať aj mlieko v pomere 1:1. 
 Na podporu hnisania: uvaríme ho vo vode na hustú kašu, necháme trochu vychladnúť a zabalíme do bavlnenej látky, prikladáme na boľavé miesto. 
   
   
 Foto aj zdroj informácií: internet 

