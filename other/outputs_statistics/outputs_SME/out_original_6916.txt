
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Klinka
                                        &gt;
                Nezaradené
                     
                 Ako sme robili vlastnú predvolebnú "kampaň" na Facebooku 

        
            
                                    1.6.2010
            o
            22:53
                        (upravené
                4.6.2010
                o
                12:01)
                        |
            Karma článku:
                14.11
            |
            Prečítané 
            2873-krát
                    
         
     
         
             

                 
                    Dá sa naozaj niečo zmeniť cez internet? S kamarátom sme len tak zo srandy založili udalosť na Facebooku. Pozvánky sme rozposlali našim kamarátom a čakali, čo sa stane. A zrazu sa nám to začalo vymykať spod kontroly. O čo vlastne išlo?
                 

                 
 Facebook
         Tak ako určite vám, tak aj nám už lezu krkom všeliaké volebné analýzy, blogy, články. Prakticky nič nové sa v nich nedá dozvedieť. A tak sme sa rozhodli konať. Založili sme udalosť, v ktorej jej členov, užívateľov Facebooku vyzývame, aby si od zajtra až do dňa volieb, čo je vlastne 10 dní, zmenili profilovú fotku na obrázok ktorý je proti súčastnej vládnej koalícii. Čakali sme, že pár našich priateľov sa pridá a aspoň symbolicky si zmení fotku na znak toho, že sa im nepáči, čo sa tu deje.   Po niekoľkých hodinách celá vec ale nabrala rýchly spád. Ľudia sa pridávali šialeným tempom, ich počet rástol každou minútou, postupne pozývali ďalších ľudí čím sa táto komunita ešte viac rozrástla. Začali písať do diskusie, pridávať nové fotky, prezentovať svoje názory.      Po chvíli sa ale aj objavili komentáre ktore znehodnocovali význam skupiny. Preto sme museli vysvetliť, čo bolo vlastne našou motiváciou a aký bol náš plán. Keďže v poslednej dobe, stále viac mladých ľudí na Facebooku vyzyva svojich priateľov aby sa zúčastnili volieb, prípadne sa snažia vysvetliť ktorú stranu by voliť nemali, my sme chceli spravit niečo, čo pokryje obe tieto témy. Prvotný nápad bol zjednotenie ľudí, čo nesúhlasia so súčasnou vládnou koalíciou.       Treba podotknúť, že sme nechceli zlučovat LEN voličov SDKÚ, alebo SaS, alebo SMK, alebo inej strany. Išlo o pokrytie širšej masy, ktorá mala spoločnú len jednu vlastnosť - nesúhlasí s krokmi terajšej koalície. A to kto s akými krokmi nesúhlasi, sme neriešili.       Rýchlo sme pochopili, že musíme ozrejmiť, že našim cieľom nie je anti kampaň štýlom vyťahovania kauz a iných vecí na súčasnú koalíciu. Nejde nám o špinenie ich už aj tak dosť špinavých mien. Taktiež nám nejde o podporu konkrétnej strany. Toto bol jeden z dvoch hlavných oporných bodov nášho snaženia sa.       Druhým bodom bolo, aby sme ukázali, že chceme niečo zmeniť! Že sa nechceme len pozerať! 12.6.2010 prídeme s hrdým pocitom k urnám a pokúsime sa zmeniť budúcnosť tohto národa! Záleži nám na tom, kto riadi krajinu a nie, nie je nám to jedno! Z našej strany je aj snaha týmto apelovať na mladých ľudí, ktorí si myslia, že ich hlas aj tak nič nezmení a že je to zbytočné. Chceme im ukázať, aspoň zmenením profilového obrázku, ktorý uvidia nasledujúcich desať dní, že nie sme pesimisti, čo v deň volieb radšej ostanú doma. Chceme aby sa zamysleli, čo nás vedie k tomu, že si masovo meníme profilové fotky a že nás zaujíma, čo sa okolo nás deje. Aby kamaráti videli, že asi naozaj nie je niečo v poriadku.       Objavili sa pesimistické odkazy, že aj tak nič nezmeníme. A my sa pýtame: kde beriete tú istotu? Odkiaľ to viete? Už to niekto z vás skúšal? Nikto z nás netvrdí, že určite niečo zmeníme, ale opierame sa o slovíčko MOŽNO. O slovo, ktoré dáva nádej. Netrúfame si povedať: presvedčíme toľko a toľko nevoličov. Nie, ale trúfame si povedať, že ak presvedčíme čo len jedného z nich, už to z nás robí víťazov. A to, či naše snaženie bolo, alebo nebolo zbytočné, nech láskavo nechajú zhodnotiť nás.               Momentálne prešlo niečo okolo 23 hodín od založenia udalosti. Za tú dobu sa pridalo vyše 1000 ľudí s tým, že sa účastnia. Ďalších okolo 350 je v stave, že sa zúčastnia možno, vyše 1200 dalo - nezúčastním sa. Na potvrdenie odpovede sa ešte čaká od neuveritelného čísla takmer 8000 užívatelov.       Je nám jasné, že nie každý, kto je zúčastnený si fotku aj naozaj zmení, ale je veľa užívatelov, čo si ju už zmenilo. Počet nezúčastnených zase neznamená, že budú všetci voliť súčasnú koalíciu. Len proste nie každý si je ochotný na desať dní zmeniť profilový obrázok. A z toho obrovského čísla nepotvrdených čakáme, kto sa kam priradí.               A čo sa z toho zakladatelia naučili? V prvom rade sa cítili ako by boli naozaj v centre politického diania. Zatiaľ co Ondrej venoval všetok svoj čas do diskusie a obzrejmovania cieľov, Ivan mal za úlohu čo najviac projekt rozšíriť. Či nám darí dostatočne, uvidíme za pár dní, každopádne je to úplne iné, ako sme čakali. Človek sa už nestáva len hovorcom za vlastný názor, ale snaží sa zrhnúť názor celej skupiny.           Samozrejme, zmenením profilovej fotky na Facebooku sa nič nekončí. Hlavné je 12.6.2010 odovzdať svoj hlas. Inak prestáva mať zmysel to, za čo spoločne bojujeme.             Atualizácia 4.6.2010, 12:00   Vyše 4100 ľudí už potvrdilo svoju účasť, z toho si fotku reálne podľa našich odhadov zmenila asi 1/5 ľudí. Takmer 15000 ľudí sa ešte nevyjadrilo a skoro 7400 ľudí nevie alebo sa nezučastní.           Atualizácia 2.6.2010, 18:00   Vyše 2200 ľudí už potvrdilo svoju účasť, z toho si fotku reálne podľa našich odhadov zmenilo 500-600 ľudí. Takmer 12000 ľudí sa ešte nevyjadrilo a skoro 4000 ľudí nevie alebo sa nezučastní.           Na článku sa podieľali obaja autori   Ivan Klinka &amp; Ondrej Juran           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Jeseň v modranskom chotári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Mali to premyslené!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Sme síce v EÚ, ale do Ruska to tiež nemáme ďaleko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Rožok v Bille stojí 1,1€
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Zavolal som "fízlov"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Klinka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Klinka
            
         
        klinka.blog.sme.sk (rss)
         
                                     
     
        Nemám rád kritiku. Ani pozitívnu. Budem to tu mať asi tažké.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2929
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




