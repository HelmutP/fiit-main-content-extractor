

 Práca bola pre mňa všetkým. Bola na prvom mieste. Vždy som bola názoru - mám prácu, teda mám stály príjem, až potom ide všetko to ostatné. To som si myslela až do momentu, než prišiel deň, keď zrazu práca nebola, nebola výplata. To čo mi ostalo po tejto skúsenosti - to je to skutočne dôležité. Svoje rebríčky som dávno prehodnotila, škoda, že až potom, ako som "padla na hubu". 
 Bolo treba znížiť stavy zamestnancov. To, že si nadriadený vybral mňa bol šok. Bola som služobne najstaršia, mala som prehľad, čo na tom, že len pred 0,5 rokom sa prijímali noví zamestnanci. Spôsob akým mi výpoveď bola oznámená mi najviac zobral dych. A ja krava som 5 rokov v tej firme drela a ťahala nadčasy. Za polhodinku som už stála vonku na ulici s výpoveďou s odstupným v jednej ruke a s osobnými vecami v ruke druhej. 
 Až neskôr som pochopila, akú službu mi vlastne vtedy výpoveďou môj bývalý zamestnávateľ preukázal. To zlé, sa na dobré obrátilo. 
 Pochopila som, že som ťahala nadčasy, no nikto to nikdy neocenil. Kým som ja pracovala, kolegovia fajčili veselo vonku na dvore. Že môj plat bol podstatne nižší, než ten kolegov, hoci sme boli na rovnakej pozícii. Že to s tým dobrým kolektívom a priateľstvom nie je tak, ako sa zdalo. Všetci to iba hrali. Práve to, akí sú priatelia sa prejavilo - v núdzi poznáš priateľa. Keď som ich najviac potrebovala, zrazu som neexistovala, veď už som nebola vo firme zamestnaná. V meste sa mi vyhýbali, na Vianoce ani jeden neposlal SMS. Moje číslo totiž dávno zo svojho mobilu vymazali. 
 Božie mlyny pomaly melú, ale isto melú. 
 Dnes mám skvelú prácu. A bývalá firma? Práve v tomto čase pobočka zatvára v našom meste svoje brány. Tí, čo mi pred 1,5 rokom nevedeli prísť na meno, hoci sa predtým 5 rokov hrdili, akí sú skvelí kamaráti majú teraz výpovede v ruke. Odovzdali firemný mobil, aj firemné auto. Doslova do polhodiny ostali stáť na ceste "s holou riťou". 
 Napriek tomu, ako sa predtým zachovali ku mne, im nič zlé neprajem. Som totiž iný človek. Zmenila som sa, prehodnotila som priority. Konečne som pochopila, konečne som začala žiť. 
 Je to smutné, no je to tak. Niekedy si človek niektoré veci uvedomí, až keď zhora padne na samé dno. 

