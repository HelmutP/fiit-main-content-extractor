

 Posledné slová tohto dňa 
 posielam tebe, milovaná. 
   
 Posledná myšlienka tohto večera, 
 bude na teba, ľúbená. 
   
 Môj posledný výdych 
 bude patriť Bohu, 
 ale ja dúfam, že ním 
 vyslovím tvoje meno. 
   
   
   
 Buďte šťastní, Hirax 
   
 Aktivity Hiraxa: 
 Streda, 16. 6. 2010, Bratislava,  Kníhkupectvo MODUL- Svet  knihy (v centre mesta, Obchodná), Od  16:04 Hiraxova čítačka k  vydanému panamsko-kostarického cestopisu Úcta k  prírode a úsmev ako  zmysel života a básniam Nech je nebo všade, od  17:06  krst spomínaných dvoch kníh + románu od slovenskej autorky Olivie   Olivieri a jej debutu Cicuškine zápisky. Víno, chlebíčky a  pozitívna  nálada zabezpečené. Vstup jeden úsmev :-). 
 Utorok, 1. 6. 2010, Čechy, Oficiálne vydanie českej   verzie  románu  Vteřina před zbláznením (Všechno je, jak je).  Utorok,  22. 6. 2010, Zlaté  Moravce, súkromná čítačka v  reedukačnom zariadení pre  mladistvých.  Utorok, 22. 6. 2010 o  16:02, Piešťany, Kníhkupectvo MODUL (Vinterova),  Hiraxova čítačka, vstup jeden úsmev.  Pondelok,  28. 6. 2010, Topoľčany, 17.00 hod Galéria (program pre  mladých), 18.00 reštaurácia  Kaiserhof Nám.M. R. Štefánika (čítačka  Hirax). 
 Máj-jún  2010: Výstava Hiraxových fotografií z Thajska v  martinskej  kaviarničke  Kamala. Pozor, nejedná sa o žiadnu "galériu".  Bude sa  jednať o 12-14  záberov, ktorými som sa snažil vystihnúť túto  krajinu.  Vstup jeden  úsmev. 
   

