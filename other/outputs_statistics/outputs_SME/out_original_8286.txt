
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Stankovič
                                        &gt;
                Silné zážitky
                     
                 Povodeň v podzemí 

        
            
                                    7.6.2010
            o
            18:29
                        (upravené
                7.6.2010
                o
                19:20)
                        |
            Karma článku:
                11.95
            |
            Prečítané 
            2681-krát
                    
         
     
         
             

                 
                    Povodne na území Slovenského krasu sa neprejavili v takej tragickej forme ako inde. Zaujímavé však je, že čo chýbalo na intenzite povodní na povrchu, akoby im pribudlo v podzemí. Podzemné rieky sa rozvodnili na 100 ročné maximá a súčasní aj starší jaskyniari takéto povodne v jaskyniach nikdy nezažili. Porovnateľná, alebo väčšia povodeň mohla byť v roku 1972. O tejto však nemáme priame pozorovania z jaskýň a historických záznamov je málo. Vieme však (videli sme na vlastné oči), že vody z niektorých vyvieračiek vyvierali aj vo svahu Silickej planiny 50 m nad riekou Slanou. Pamätníci z Plešivca, kde povodeň udrela najsilnejšie, však hovoria, že tohoročná bola horšia.
                 

                 Májová povodeň v Krásnohorskej jaskyni spôsobila vznik menšieho krátera pred vchodom a maximálnym prietokom asi 2 m3 prekonala všetky doterajšie pozorovania. Vidieť rozvodnenú rieku priamo v podzemí je obrovským zážitkom. Bohužiaľ, v Krásnohorskej jaskyni sa už pri prietoku asi 600 - 800 l/s (normálny stav je 20 l/s) vytvorí v nízkych vstupných častiach jaskyne sifón. Netrpezlivo sme čakali, kým hladina poklesne aspoň 5 cm pod jeho strop, aby sme mohli do jaskyne vplávať. 19. 5. sa to konečne podarilo a polosifón bolo možné na nádych preplávať. Vítala nás rozdivočená podzemná rieka v Perejovom dóme. Ďalší priebeh podzemnej rieky už taký dramatický nebol. Aj tak je to silný zážitok. Ako asi mohla jaskyňa vyzerať pri prietoku 2000 l/s si môžeme len domýšľať. S Vladom prejdeme celý hlavný ťah jaskyne až k Marikinmu jazierku. Radosti z plávania v jeho zelenej vode si však musím odrieknuť. Nemám na sebe plávaciu vestu a hlavu sa mi v 9 stupňovej vode zbytočne máčať nechce. Pod neoprén mi natieklo veľa vody, preto rýchlo pádlujem k brehu. Možno nabudúce.      Toto dobrodružstvo ma celkom neuspokojilo. Špekulujem, ako zdolať sifón pri vyššom prietoku. Najjednoduchšie by bolo natiahnuť cez sifón vodiace lano. Možno by sme dokázali sifón preplávať „na hubu" pri hladineo 50 cm vyššej . Sifón by bol dlhý iba 5 m. Neveril som, že v krátkej dobe sa takáto povodeň zopakuje (veď storočná voda býva iba raz za sto rokov), a tak som sa s inštaláciou lana veľmi neponáhľal.                    V pondelok 31. 5. som sa večer stretol s vedcami z UK Bratislavy, s ktorými robíme hydrologický a mikrobiologické výskumy v Krásnohorskej jaskyni a na štvrtok sme dohodli odber vzoriek. Ako sme tak sedeli na terase cukrárne Piroška, spustil sa lejak. Predchádzajúca povodeň znemožnila odber vzoriek z jaskyne a dúfal som, že teraz sa to nestane. Lialo celú noc a pokračovalo aj ráno. Dámy hydrologičky majú asi nejakých vodníckych predkov. Voda chodí všade s nimi. Asi by som mal ísť to vodiace lano nainštalovať. Keď som prišiel k jaskyni, bolo zjavné, že voda rýchlo stúpa. Ak nemá celá akcia skončiť znova neúspechom, musíme okamžite konať. Alarmujem vedcov. Ak chcú odobrať vzorky, musia okamžite prísť k jaskyni. Zoliho dovezú so sebou, aby sme odbery v jaskyni urýchlili. Treba vyliezť aj na Kvapeľ rožňavských jaskyniarov a odobrať vzorky aj tu. O pol hodiny sme už všetci pri jaskyni. Je zjavné, že voda rýchlo stúpa. Veľa času nemáme. Zoli pôjde na kvapeľ, ja s Dr. Semanom ovzorkujeme hlavný ťah a dámy hydrologičky ovzorkujú povrchové vývery.   V čižmách prebrodíme vstupné časti ešte bez namočenia. Odoberieme prvú vzorku a upaľujeme ďalej. V Aboniho dóme už je viditeľný vzostup hladiny. Tvárim sa pokojne, aby sa pán Seman nevystrašil, ale dynamika stúpania hladiny mi naháňa strach. Nie z toho, že by nás v jaskyni zatopilo (hoci aj to sa už jaskyniarom na Slovensku stalo), ale ďalšiu povodeň už nepotrebujeme. Je to len chvíľkový pocit, prevláda jednoznačne eufória, ktorá ma vždy opantá, keď stojím nad rozvodnenou podzemnou riekou. Podobnú rieku cítim priamo v sebe. O rezonancii som však písal už minule, takže sa opakovať nebudem. Kdesi hlboko v hlave mi dokola beží refrén: „Nenainštaloval si vodiacu šnúru, ktovie, či ešte bude príležitosť na plávanie sifónom."   Prekonať Chodbu perál bez namočenia sa už nedá. Voda očividne stúpa. Odoberieme vzorky vo Veľkej sieni zo Sifónu potápačov a mali by sme mazať naspäť. Nejako sa nám nechce. Atmosféra jaskyne je úžasná a navyše ešte máme čas. Zoli musí vyliezť na Kvapeľ a kým mu tam nakvapkajú 2 litre vody, my si môžeme pozrieť Zrkadlovú sieň. Tu akurát zaregistrujem, že sponad hladiny zmizla čiara kryštálov, ktorá sa tu vytvorila počas povodne v roku 2002. Teda predchádzajúca povodeň bola väčšia ako tá spred 8 rokov. V Abonyiho dóme čakáme Zoliho. Vylievame z čižiem vodu a žmýkame ponožky. To je dobrá taktika, ako sa uchrániť pred podchladením nôh. Zoli za chvíľu prichádza a konečne upaľujeme z jaskyne von. V Perejovom dóme, ktorý sme prešli smerom dnu po členky vo vode, naberieme riadne do čižiem. Už je zrejmé, že za nami sa ženie príval, ktorý jaskyňu za niekoľko hodín zaplaví. Vzorky máme. Aspoň čiastočne sa tak naplnil program tejto vedeckej exkurzie. Cesta z Bratislavy nebola zbytočná. Ďalší program musíme zrušiť.   Na druhý deň to vyzeralo vo vchode do jaskyne takto. Prietok 5.000 až 6.000 l/s nikto nikdy ešte z jaskyne vytekať nevidel.      To, že z jaskyne tečie mútna voda, bol pre mňa šok. Ešte nikdy sa to nestalo. Zakalenie pochádza pravdepodobne zo záverečného sifónu, v ktorom sa nachádza veľké množstvo ílovitých sedimentov.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            50. výročie objavu Krásnohorskej jaskyne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Jeden pohľad 2 koruny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Najkrajšia hrebeňovka Volovských vrchov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Peter
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Okolo Rákoša
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Stankovič
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Stankovič
            
         
        jaroslavstankovic.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som starý blázon, ktorý si zo záľuby urobil profesiu a z jaskyniara sa stal správcom Krásnohorskej jaskyne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2042
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Silné zážitky
                        
                     
                                     
                        
                            Jaskyne
                        
                     
                                     
                        
                            Návštevníci
                        
                     
                                     
                        
                            O dobrých knihách
                        
                     
                                     
                        
                            Radzim
                        
                     
                                     
                        
                            Venezuela
                        
                     
                                     
                        
                            Rožňavské cyklotrasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jan Piasecki, Rukopis nalezený v Zaragoze
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




