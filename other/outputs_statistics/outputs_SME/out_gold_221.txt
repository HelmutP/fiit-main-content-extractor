

 

 
 
Banderovci chtěli samostatnou evropskou Ukrajinu. Možná jako autoritářský stát, ale to chtěl tehdy leckdo. Slováky počínaje a Poláky nekonče. Ukrajina byla mezi dvěma mlýnskými kameny a tak chvíli bojovali s Němci, chvíli se Sověty, většinou proti všem. Kdo zná příběh polské samostatnosti a maršálka Pilsudského, nemůže se divit. 
 
 
Byla to válka zub za zub, dítě za dítě, uřezaná prsa za uřezaná prsa, upálení za upálení. Šílené, strašné, děsivé. 
 
 
Kdo jsi bez viny, hoď kamenem. 
 
 
Z hlediska ukrajinského národa to byli hrdinové. Stejní jako vojáci wehrmachtu, Svobodovy armády, slovenské armády, co dotáhla až pod Kavkaz a zpátky, nebo jako partyzáni ze SNP. 
 
 
Stejní hrdinové, jako tisíce a tisíce těch, kteří padli v první světové válce za císaře pána. 
 
 
Banderovci jsou historie. Děsivá i úžasná. Tak jako každá válka. Na tom, že Ukrajinci o víkendu banderovce poprvé oslavovali, nevidím nic zlého. My ostatní se je snažme pochopit. 
 
 
Tak jak se třeba Maďaři snaží pochopit, proč má čestná stráž před sídlem slovenského prezidenta uniformy slovenských vojáků, kteří spolu s císařskými potlačovali maďarskou demokratickou revoluci 1848. 
 

