

 Sereď 4. februára (TASR) - Policajti počas rozsiahlej akcie zameranej na verejný poriadok zadržali vo štvrtok večer v Seredi deväť páchateľov trestných činov. 

 Zároveň objasnili trestné činy krádeží vlámaním, marenia výkonu úradného rozhodnutia a dokonca zadržali aj vojenského zbeha. Policajná akcia bola podľa trnavského policajného hovorcu Martina Korcha reakciou na bezpečnostnú situáciu v meste Sereď. Tú označil za neúnosnú aj krajský policajný riaditeľ Dušan Síleš, ktorému prekáža hlavne verejná prezentácia arogancie zo strany páchateľov trestnej činnosti a bezdôvodné obťažovanie občanov. Za vrchol označil nedávne rozstrieľanie kamier mestskej polície. 

 Podľa Korcha bude polícia podobné akcie organizovať častejšie nielen v Seredi, ale všade tam, kde si to bezpečnostná situácia vyžiada. 


