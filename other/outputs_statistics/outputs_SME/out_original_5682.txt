
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mirka Vávrová
                                        &gt;
                Alica v krajine zazrakov
                     
                 Fejsbúk náš každodenný, daj nám dnes 

        
            
                                    2.5.2010
            o
            19:33
                        (upravené
                2.5.2010
                o
                20:26)
                        |
            Karma článku:
                6.87
            |
            Prečítané 
            2115-krát
                    
         
     
         
             

                 
                    Som PN. Zožierajú ma mrle a príšerne sa nudím. Na stole, na stoličke, na koberci a vôbec všade, kde to zemská tiaž umožňuje, sa rozprestiera kráľovstvo papierových vreckoviek. Bahním si v posteli a na vlastné počudovanie je mi celkom dobre, hoci som si už niekoľkokrát takmer vykašľala pľúca. Poslušne hltám lieky a pijem sirupčeky, no popri tom si dávkujem i vlastnú medecínku - fejsbúk.
                 

                 Dávkovanie? Ideálne nepretržitá konzumácia. Ale berúc v úvahu kontraindikácie, tak deti do 7 rokov, tehotné a dojčiace ženy maximálne 12 hodín denne. Ostatní, a tam spadám aj ja, nemajú limity. A mnohokrát, po istom čase, už ani žiadne zábrany. Takmer vždy, keď sa pripájam do svojej sociálnej siete, ktorá mi pestuje a čičíka "priateľov", dovolím si na sekundu hlboké zamyslenie - prečo to vlastne robím? Presvedčivo odpovedať neviem. V správnosti môjho rozhodnutia pripojiť sa, ma nepodporí ani následný obsah, čiže výplody fantázie, túžob, nenávistí, nudy, veselosti a obmedzenosti mojich priateľov. Nechcem vytŕčať z davu, pridávam sa do diskusie tiež. Šup smajlíka sem, šup ho tam, ch nahradí x, ešte pár slovíčok v angličtine a bude to dokonalé. Čo na tom, že zoči voči by som takúto zlátaninu nepovedala nahlas ani v prázdnej miestnosti? Opil si sa? Papáš banán? Nechce sa ti učiť? Farbíš si vlasy? Napíš nám to, my to bez námietok strávime. Som ironická, viem. No som taká istá a nebudem dnes za sudcu. Vlastne ani zajtra či napozajtra.   Viete, a teraz sa obraciam na vás, zarytí odporcovia vedy a techniky. Tiež mi je to ľúto. Kam sme sa to len dostali? - položím si filozofickú otázku niekoľkokrát denne. Možno by sme sa mohli stretávať, trebárs každý deň o šiestej, po pracovnej dobe, lebo dovtedy všetci tvrdo pracujeme aby sme sa uživili. Veru, doba je ťažká a už to nie je čo to bývavalo. Na týchto stretnutiach by sme mohli spoločne klásť verejnosti  otázky čo nás trápia. Napríklad "kam sme sa to dostali?" alebo "čo len z tých mladých bude". Môžeme pridať aj suché konštatovania, lebo i tie majú svoju váhu. Trebárs "za mojich čias by sa toto nemohlo stať" alebo "keď som ja bol mladý..." Možno, ak by sa sila našich hlasov doniesla až ku kompetentným, dostali by sme aj odpoveď... Ale kto sú kompetentní? Obávam sa, že my sami. Vždy sa nám to vráti ako bumerang. My sme si urobili tento svet takým, akým je. Je plný emailov - nie listov, mobilov - nie pevných liniek, skenerov, tlačiarní, videokamier, foťákov a mnohých ďalších technických vychytávok, ktoré bohužiaľ neviem, na čo slúžia (ale zistím :). Tak prečo sa stále sťažujeme?   Vrátim sa na začiatok. Som PN. Sama doma a so svojou fóbiou z telefonovania prakticky bez kontaktu s okolitým svetom (ak nerátam televíznych moderátorov). Príšerne sa nudím a priam fyzicky cítim, koľko vecí mi uniká, koľko informácií preteká pomedzi prsty. Urobím klik, obrazovka zažmurká a som tam. V tom pestrofarebnom digi-svete. Chýbajú mu síce vône, chute či dotyky, no spojím sa s kolegyňou, rodinou i školou a všetkým dám vedieť, že stále som. Len trochu chorá. No a fejsbúk. Krásny vynález. To množstvo ľudí, ktorých pre mňa objavil, vytiahol zo starých spomienok a oprášil... To množstvo drobných každodenných informácií, ktoré o nich viem. A keď sa stretneme, tak nebudeme tápať v hlbinách mysle, kedyže to bolo naposledy, čo sme sa videli?   Viete čo si rada predstavujem? Seba ako starenku, ktorá je šťastná. Nečíha na mladých susedov spoza záclonky, ani nešpehuje, či tá mladá z vedľajšieho vchodu prišla domov načas. Nešteká na nikoho v električke, nepoužíva slovná spojenia "tá dnešná mládež" a "nemáte úctu k starým ľuďom vy banda jedna" (doplnené o pohyb bakuľky vo vzduchu), nefrfle na dôchodok. Žije tak ako ostatní. Je súčasťou spoločnosti. Nerobí si vrásky, neberie sa príliš vážne. Posiela vnúčatám emaily a prezerá si ich fotky. Trebárs aj na fejsbúku. Hoci v časoch, keď ja budem babičkou, bude už chudáčik úplne, ale úplne out. A ja, spolu s pamätníkmi, sa v kaviarni na rohu budeme sprisahanecky smiať na tom, ako sme sa kedysi pridali do skupiny "Nevolím Fica". Ľudia, neberme sa/to tak vážne.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Letné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pani učiteľke s láskou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pohľad späť (cez spätné zrkadlo)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Malé špongie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Návraty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mirka Vávrová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mirka Vávrová
            
         
        mirkavavrova.blog.sme.sk (rss)
         
                                     
     
        Milujem a neúnavne študujem život.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1418
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Alica v krajine zazrakov
                        
                     
                                     
                        
                            la vita é bella..niekedy
                        
                     
                                     
                        
                            Svet detí (a ja v ňom)
                        
                     
                                     
                        
                            Lásky jednej rusovlásky
                        
                     
                                     
                        
                            Išla Mirka na vandrovku
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dobrá škola (časopis)
                                     
                                                                             
                                            English is easy, Csaba is dead
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                             
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            30 seconds to Mars
                                     
                                                                             
                                            Passenger
                                     
                                                                             
                                            Emeli Sande
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mary - lebo píše ľahučko a krásne
                                     
                                                                             
                                            Sjuzi - lebo :)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Letné prázdniny
                     
                                                         
                       Pani učiteľke s láskou
                     
                                                         
                       Pohľad späť (cez spätné zrkadlo)
                     
                                                         
                       Malé špongie
                     
                                                         
                       Návraty
                     
                                                         
                       Klub anonymných strachoprdov
                     
                                                         
                       Jedna hodina, tri predsudky
                     
                                                         
                       50 centov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




