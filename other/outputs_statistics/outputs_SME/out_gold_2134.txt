

 Voľaktorí ľudia majú všetko, sú „vystatusovaní". Majú radosť, majú stresy, majú víno, majú ženy, majú zmiešané pocity a miešané drinky. Majú obľúbené videá, majú dizlajky lajky komenty, nové fotky na nich staré spomienky, nových friendov a starých nepriateľov. Vykradnuté názory a vlastné citáty od Shaw-a alebo Wilde-a. Nejaké íventy, stratené mačky, nové vzťahy, po 15 otázkach úspešné tehotenské testy. Majú skupiny a sú skupinami, odlišujú sa aby boli rovnakí. Aplikujú nové aplikácie o tom ako im aplikovaná chémia komplikuje ich dekamerón lásky. Jasne a správy na stene o tom jak sme sa včera rozšaluvali na minďár a ešte rýchlo kto ma dnes B-day (fuck me reminder!). Sú slovom „vystatusovaní" až mi z uší vyteká žlč. 
   
 Tak napríklad Milada: Milada je šťastná, Milada má áčka v indexe. Milada má frajera a ďalších dvadsať pičeňov, čo na ňu čakajú v rade. Milada nemá komplexy, vie cudzie jazyky a má obľúbenú farbu a troch gayov kamarátov. Príšerne ju vytáča zima. Milada je moderné dievča, hotová reklama na jogurt, po ktorom vás prejde nafúknuté brucho. 
 Johann: je krutý phán, pravicový liberál celý v čiernom a v saku. Za ním panel a na ňom logo jeho strany, ktorú už vymenil zo štvrtýkrát. Vo voľnom čase nosí rejbeny a v očiach má strach, že ho zežere krokodíl, ktorého má na hrudi z posilky. Johann is in open relationship s celým baletným súborom. Johann je status sám o sebe. 
 Na koniec Ewerlin na duši nosí splín. Jebú ju susedia, babky na pošte, program v telke, hudba v rádiu, kamarát zo škôlky. Občas však zažiari, musíte ju milovať, je originál vypustený ako gin z fľašky od sprite-u. Víno si neriedi a má rada chránený sex. Vždy sa pred ním uchránila. Nikoho nevolí, každý volí ju. Volím te Ewerlin. 
   
 No ja mám svoju slobodu, nasadám na motorku, zapínam palubnú jednotku a utekám zo Spojených štátov stredoeurópskych, opúšťam krajinu vý - tĺkov. Dávam veľký log out na twitter a fejszbúk. Mierim si to na Cádiz, kde nastupujem na Bounty smer La Habana, Fidelovi sčadím cigaru, ošúpem pomaranč a beriem odkaz do Venezuely, medzitým v Mexiku osedlám chupacabru a prejdem s ňou až do Argentíny, kde navštívim rodinu Guevarových dám si mate a kilového stejka, zažijem chute, zrátam všetky farby sveta, opijem domorodkyne rožkom a miestnu bohému fľaškami vínovice. 
 Všetci sme stratení pózeri, ja to viem. Požehnaný buď fuckbúkový panic lebo tvoj je skutočný život, hľa požehnaný i neaktívny registrovaný člen fejszbúku, lebo ty vidíš vychádzať slnko na púti krčmami, dočítaš rozčítanú knihu, stretneš človeka. 
   
   
   
   
   

