
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Ščepko
                                        &gt;
                Trenčín
                     
                 Ako v Trenčíne mýto platiť prestali (Historické fotografie) 

        
            
                                    6.10.2009
            o
            21:21
                        |
            Karma článku:
                12.55
            |
            Prečítané 
            7101-krát
                    
         
     
         
             

                 
                    Priznám sa, že inšpirácia k tomuto článku vznikla pri mojich víkendových rozhovoroch s prof. Jánom Komorovským. Jeho otec, Štefan Komorovský, po návrate z ruských légií, pracoval od 1922 na trenčianskej mýtnici. Ako legionárovi - invalidovi mu bola mýtnica a vyberanie mýta zverené až do začiatkov prvej Slovenskej republiky (Slovenský štát). Aj keď šlo o malý domček, bol na frekventovanej ceste pri cestnom moste. Preto sa pokúsim napísať niečo nie len o mýtnici, ale aj o 1. oceľovom moste. Pán Hanušin to vo svojej knihe vystihol, keď napísal, že „aj napriek doloženej existencie mosta už z roku 1271 bol Trenčín mestom „pri Váhu“ a nie „na Váhu“.“ Drevené mosty však nie len pre povodne, ale aj pre ľadochody či požiare nemali dlhú životnosť. Aj toto bol jeden z faktorov, prečo sa mesto nerozširovalo na pravej strane rieky. Problémy s drevenými mostami sa odstránili až vybudovaním nového oceľového mosta. Ten sa nachádzal v miestach, kde leží súčastný cestný most.
                 

                 Cestný most a doprava pred 1. svetovou vojnou     Oceľový most bol postavený v roku 1897. Postavil ho štát, a to nielen preto, že mesto nemalo dosť peňazí, ale aj preto, že ležal na strategickej a obchodne dôležitej štátnej ceste. Most stavala dánska firma Greesersen a synovia z Budapešti od 27. novembra 1896 a zaťažkávacia skúška bola 10. novembra 1897. Piliere zakladali kesónmi, oceľovú konštrukciu hmotnosti 6875 metrických centov dodali železiarne štátnych železníc v Diosgyöri. Most bol dlhý 285 m, široký 6 m, oblúky v strede mali výšku 8 metrov. Stavba stála štátnu pokladnicu 386 644 korún. [1]   Osud mosta napokon v roku 1945 spečatila ustupujúca nemecká armáda. Nahradili ho dva dočasné drevené a nakoniec dnešný, betónový most. Pozostatky dočasného dreveného mosta sú pri nízkej hladine Váhu vidieť dodnes.      Takýto pohľad na Trenčín sa naskytol pred približne 120-timi rokmi. Teda v čase, kedy cez rieku Váh viedol drevený cestný most. Zaujímavý je aj pohľad na vežu Farského kostola, ktorá v tom čase dosahovala polovičnú výšku. (Zdroj: Archív Trenčianskeho múzea)      Pohľad z opačnej strany. Piaristické gymnázium, Palackého ulica, Zámostie. Zmenilo sa veľa. Rozsiahlejšiu výstavbu na Palackého ulici umožnila až výstavba "bánoveckej" trate, keďze násyp železnice poskytoval určitú ochranu pred rozvodneným Váhom. To, ako vyzeral Váh na konci 19-teho storočia, vidieť aj na fotografii. V pozadí je vidieť drevený cestný most. (Zdroj: Archív Trenčianskeho múzea)       O 30 - 40 rokov neskôr. Posun v rozvoji mesta je evidentný. Mesto v čase, keď muži nosili čierny oblek s cylindrom a dámy dôstojne „korzovali“ vo svojich dlhých róbach po námestí. Bolo to tiež obdobie, kedy obchody na námestí hrdo niesli mená svojich majiteľov. Vtedy možno aj predstaviteľom mesta viac záležalo na tom, aby sa ich mená nespájali s „nemorálnymi praktikami.“ Táto fotografia patrí k mojim najobľúbenejším. Na námestí sa ešte nachádzajú budovy, ktoré neskôr nahradila nová pošta a obytný dom s (bývalou) lekárňou. V pozadí je oceľový cestný most. Pohľad na pravú stranu Váhu dokumentuje rodiacu sa novú mestskú štvrť. (Vydavateľ: Tatra)      Cestný most v roku 1910. Ozdobná tabuľa na hornej konštrukcii mosta niesla názov mosta. Názov nie je celkom jasný, keďže jeden z prameňov uvádza názov Most arcikniežaťa Fridricha, no ďalší písal o Erzsébet híd (Alžbetin most). Pod spomínanou tabuľou visí ešte jedna, ktorá prikazovala pohoničom, aby "kone hnali krokom". (Vydavateľ H. Szold)       Pohľad z opačnej strany.              Pohľad od mesta na pravú stranu Váhu. Na pravej strane je vidieť domček mýtnice.       To, či stožiare na hornej konštrukcii boli osadené za účelom dodávky elektrickej energie pre časť Zámostie, sa mi zistiť nepodarilo. No je to pravdepodobné. (Vydavateľ L. Gansel)      (Vydavateľ neznámy)        Nedá mi nespomenúť niečo o doprave v meste na prelome storočí. Najmä počas trhov a jarmokov mestom prechádzalo značné množstvo vozov. Vzhľadom k ich počtu sa v tomto období objavili vyhlášky, ktoré predpisovali spôsob predchádzania, vyhýbania sa, a taktiež vyhlášky zabraňujúce ničeniu ciest (dláždených) a na ochranu chodcov. Za porušenie dopravných predpisov sa udeľovali pokuty od 2 do 40 korún (pre porovnanie, rohlík či pletenec/caletka stál 20 halierov).  Je známe, že v roku 1913 boli v Trenčíne už štyri automobily. Automobil pred prvou svetovou vojnou nebol mimoriadnym zjavom, keďže výletníci smerujúci do obľúbených Trenčianskych Teplíc prechádzali práve cez toto župné mesto. Tí boli nie len z Moravy, ale aj z Viedne. No už v tom čase sa objavovali sťažnosti na zlý stav trenčianskych ciest.      Fotografia, ktorá mala skončiť v rodinnom archíve. Je nalepená na tvrdom papiery s vytlačeným obvodovým ornamentom. Pod ňou je ceruzkou podpísaný fotograf M. Stern a syn. Záber zachýtávajúci robotníkov pravdepodobne pri oprave/výmene nitov mosta. Zvečnenie na fotografii si nenechali ujsť ani náhodní zvedaví okoloidúci. Podľa spomienok pána Komorovského mal most svojho "správcu", ktorý most nielen zametal, no staral sa aj o jeho bežnú údržbu. Záber je zhotovený z pravej strany Váhu.       Detailnejší pohľad na pracovníkov. Nespozná niekto svojho deda, či pradeda?   Mýtnica   Vrátim sa späť k samotnej mýtnici. Stála pri moste v podobe domčeka. V samotnom domčeku bola podľa spomienok prof. Jána Komorovského kuchyňa, komora, obytná miestnosť, kde bol stôl s truhlicou. Do pivnice a na toaletu sa schádzalo po schodíkoch, ktoré boli umiestnené vedľa mýtnice (z mesta k mýtnici). Pivnica aj toalety boli teda pod úrovňou mosta. Na domčeku medzi mostom a mýtnicou bola závora. V tesnej blízkosti mýtnice sa po vybudovaní tzv. bánoveckej trate v roku 1901 osadili ďalšie závory.   Mýto platili aj početní židovskí podnikatelia, ktorí mali svoje obchody v strede mesta. Podľa pána prof. Komorovského za prechod cez most odovzdávali povozníci jednotlivých podnikateľov lístok s pečiatkou firmy, ktorý sa v mýtnici napichol na hák. Z neho sa na konci mesiaca lístky vybrali, roztriedili  a mohlo sa  ísť inkasovať. Aj z toho vyplýva, že mýtnik mal určité postavenie a bol medzi podnikateľmi v meste  známy. Pri mýtnici sa vyberali aj miestne poplatky od žien z okolitých dedín, ktoré šli do mesta na trh. Tieto poplatky už ale neboli v réžii pána Š. Komorovského.  Pokúšal som sa získať fotografiu alebo pohľadnicu, kde by bolo možné vidieť celú mýtnicu. Nebyť pána J. Hanušina, asi by som ešte dlho hľadal. V tridsiatych rokoch bola už totiž pri pohľade z hradu zakrytá stromami.      Na obrázku je šípkou označený mýtny domček. Vidieť aj bánoveckú trať č. 143, ktorá bola dostavaná v roku 1901. Trať smeruje od Baarovho domu vpravo, pred mýtnicu, popri "wochterni" (domček naproti mýtnice). Wochterňou sa ľudovo nazývala železničná strážnica, kde bol na prelome 30tych rokov správcom pán Janík. Železničná trať bola neskôr znížená do dnešnej podoby. (Vydavateľ N. Weisz)      Pre ľahšiu orientáciu je v spodnej časti vidieť dnešné Štúrovo námestie a v pravom hornom rohu je mýtnica s oceľovým mostom. Fotografia je asi z roku 1904. (Vydavateľ N. Weisz)      Takto vyzeral pohľad z hradu po približne 30tich rokoch od predchádzajúceho záberu.       Keďže pán Komorovský je možno posledný, kto si interiér domčeka pamätá, dovolil som si pre zaujímavosť nakresliť aspoň približný pôdorys mýtnice.      Toto je (mne známa) jediná dochovaná fotografia aspoň časti z interíeru obývacej izby. Pod knihou je spomínaná truhlica. Na fotografii je prof. J. Komorovský ako mladý. K mýtnici má predsa len osobný vzťah, keďže bol to domček, kde sa v roku 1924 narodil.    Zrušenie mýtnice   Pomery v novovzniknutej 1. Slovenskej republike naznačovali, že platenie mýtneho poplatku sa pomaly blíži k svojmu zrušeniu. Je mi známy skutočný príbeh, ktorý rozhodol o urýchlení  jej definitívneho konca. Keďže sa rátalo so zrušením mýta v dohľadnej dobe, mýtnik Š. Komorovský hľadal vedľajšie zamestnanie. Mýtny poplatok v jeho neprítomnosti vyberal jeho švagor Jozef Ondrášek. Tak tomu bolo aj v deň, kedy do Trenčína smerovalo vládne auto s ministrom Šaňom Machom. To spomínaný J. Ondrášek nevedomky zastavil. Zastaviť vládne auto bola v tom čase nie len značná trúfalosť, ale aj bezočivosť. Od vodiča žiadal mýtny poplatok za prejazd mosta. Vodič ho upozornil, že ide o vládne vozidlo a žiadne mýto platiť nemieni. Trval však na svojom s tým, že „vládne – nevládne, platiť sa musí“. To strašne rozčúlilo Šaňa Macha. Pýtal sa, ako je možné, že sa v Trenčíne ešte mýto vyberá, a že sa postará o jeho zrušenie. Tak sa aj stalo. Svoj sľub dodržal. Krátko na to sa už mýto v Trenčíne vyberať nesmelo. K presnému dátumu zrušenia mýtnice sa mi však dopátrať nepodarilo.   Ešte pridávam zábery z okolia mýtnice, ktoré zostali už len na starých fotografiách.      Z tejto cesty sa pravdepodobne zachovala len okrajová časť z ľavej strany. Malo by ju byť vidieť pod dnešným mostom od žel. trate k bývalemu Kinu Hviezda (Katolíckemu domu).      Fotografia zachytená o niekoľko metrov bližšie k mostu a pootočená doprava. Bánovecká trať vtedy ešte pretínala cestu na cestný most, ktorý je za chrbátom fotografa.  Zmenilo sa veľa.       Moje poďakovanie patrí  RNDr. Jánovi Hanušinovi, CSc. za poskytnutie pohľadnice zo súkromnej zbierky. Taktiež aj pánovi prof. PhDr. Jánovi Komorovskému, CSc. za jeho čas a poskytnuté materiály.               Použitá literatúra:  Ján Hanušin, Trenčín na starých pohľadniciach, Dajama, 2. vydanie, 2005   Milan Šišmiš, Trenčín – Vlastivedná monografia 2, Alfa-press, 1996   [1] odsek prebraný z knihy Trenčín – Vlastivedná monografia 2     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Ulica 1. mája - 1. časť (Historické fotografie)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Pozvánka na výstavu Mestských zásahov Trenčín
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Pozvánka na verejnú diskusiu o otvorenej samospráve v Trenčíne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Ďakujem! Naozaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Trenčianskeho primátora prezývajú "Braňo Obálka". Prečo?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Ščepko
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Ščepko
            
         
        scepko.blog.sme.sk (rss)
         
                                     
     
        V roku 2010 ako spoluorganizátor zrealizoval 1. verejnú diskusiu na tému trenčianske nábrežie. Súčasťou ktorej bol bývalý primátor, architekti, ekológ s moderátorom Š. Hríbom. Cieľom diskusie bolo dostať tému nábrežia pred verejnosť a poukázať na pochybné zámery vtedajšieho vedenia mesta. Je organizátorom projektu Mestských zásahov Trenčín 2013.
 
http://www.mestskezasahy-tn.sk



     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    40
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Trenčín
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




