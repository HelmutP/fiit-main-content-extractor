

 Je potrebné poznamenať, že celú anketu zjavne vyhrala sága Twilight, ktorá sa objavila v piatich zo šiestich rebríčkov v celkovom počte jedenásťkrát! Ako táto kniha zanechala stopu v čítateľoch, už vedia tí, ktorí za ňu zahlasovali. Avšak mňa obvzlášť potešilo jedno meno, ktoré sa objavilo na piatich priečkach. Skrýva sa za ním mladá žena, ktorej tituly pribúdajú ako huby po daždi. Do ich deja sa ľahko vžijeme, pretože často pripomínajú náš aktuály život. Spomínanou spisovateľkou je Evita Urbániková, ktorej aktuálne vychádza ďalšia kniha s názvom: Svet mi je dlžný. 
 To, či si aj váš najobľúbenejší spisovateľ objavil vo vyšie uvedených priečkach sa môžete dozvedieť v priamom prenose, ktorý odvysiela STV2 v nedeľu, 23. 5. 2010 o 20 hod. Pre tých, ktorí sa už nevedia dočkať, tu je malý preddavok: 
   
 Dvadsať najobľúbenejších titulov slovenských čitateľov roka 2009 
 
 
Úsvit –  Stephenie      Meyerová, Tatran 
 
Chatrč  –      William Paul Young, Tatran 
 
Mucha –  Dominik      Dán, Slovart 
 
Zatmenie  –      Stephenie Meyerová, Tatran 
 
Mafia  na      Slovensku – Gustáv Murín, Albert Marenčin – Vydavateľstvo PT 
 
Mal to  byť      pekný život – Andrea Coddington, Evitapress 
 
Klamstvá  – Táňa      Keleová-Vasilková, Ikar 
 
Noc  temných      klamstiev – Dominik Dán, Slovart 
 
Hostiteľ  –      Stephenie Meyerová, Tatran 
 
Tisíc  žiarivých      sĺnk – Khaled Hosseini, Ikar 
 
Zdochnuté  srdce      – Eli Elias, Ikar 
 
Všetko  alebo      nič: Príbeh pokračuje – Eva Urbaníková, Evitapress 
 
Nová       miniencyklopédia prírodnej liečby – Igor Bukovský, AKV –  Ambulancia      klinickej výživy 
 
Brida –  Paulo      Coelho, Ikar 
 
Minulosť  ťa      dostane (Slovensko píše román), Evitapress + Martinus.sk 
 
Tajomstvo       príťažlivosti sŕdc – Ruediger Schache, Ikar 
 
Víťaz  je sám –      Paulo Coelho, Ikar 
 
Kľúč –  Joe      Vitale, Ottovo nakladatelství 
 
Srdce v  tme –      Táňa Keleová-Vasilková, Ikar 
 
Bodka 2  – Milan      Lasica, Forza Music 
 
   
 Desať najobľúbenejších prekladových titulov 
 
 
Úsvit –       Stephenie Meyerová, Tatran 
 
Chatrč  –      William Paul Young, Tatran 
 
Zatmenie  –      Stephenie Meyerová, Tatran 
 
Hostiteľ  –      Stephenie Meyerová, Tatran 
 
Tisíc  žiarivých      sĺnk – Khaled Hosseini, Ikar 
 
Brida –  Paulo      Coelho, Ikar 
 
Tajomstvo       príťažlivosti sŕdc – Ruediger Schache, Ikar 
 
Víťaz  je sám –      Paulo Coelho, Ikar 
 
Kľúč –  Joe      Vitale, Ottovo nakladatelství 
 
Dievča,  ktoré      sa hralo s ohňom – Stieg Larsson, Ikar 
 
   
   
   

