
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kobelák
                                        &gt;
                Nezaradené
                     
                 Bežecké lyžovanie vo Vysokých Tatrách: totálny úpadok a nezáujem 

        
            
                                    17.3.2010
            o
            13:20
                        (upravené
                17.3.2010
                o
                14:03)
                        |
            Karma článku:
                21.25
            |
            Prečítané 
            9532-krát
                    
         
     
         
             

                 
                    "Táto trať vedie cez môj pozemok", prihovára sa mi postarší tatranec, kým si uťahujem šnúrky na lyžiarke. "Mali by ste sa lepšie starať o svoj majetok, pozrite sa, ako to tu vyzerá", odpovedám, bežecká trať nie je bezpečná, sú na nej primrznuté zľadovatelé hrudy snehu. "Veď sa starám, chodím po úradoch aj za kdekým, ale nikto mi nechce platiť". Nepochopil, pomáham mu: "ja by som aj zaplatil, keby som mal istotu, že trať bude upravená". Už tu raz vyberali poplatok, bolo to v poriadku. Nechytal sa, zmenil tému: "Nocľah už máte?" "Som tu len na jeden deň". "Škoda, mám voľné izby, lacné" nevzdáva sa. Keď poďakujem, posťažuje sa, že nikto nechodí. Hovorí, že ešte nedávno, sa u neho takmer celú sezónu striedali bežkári z Moravy, niektorí sa vracali aj v lete na huby. Tatrancov uplakaný monológ mi zdvihol tlak. Je smutné, že takto si musí zháňať klientelu, ale netreba ho ľutovať, môže si za to sám. Pred časom urbárnici, medzi ktorých patril, naschvál zarúbali bežecké trate, chceli mať z nich príjmy. Chápem, že chceli zarobiť, ale chceli lóve bez toho, žeby niečo investovali. Teraz sa im to vracia. Nielen Moravákov, ale aj bežkárov odinakiaľ, takto vyhnali z Tatier a teraz plačú.
                 

                 Urbárnikom zobrala víchrica les. Okrem jeho obnovy, mohli by investovať aj do rekonštrukcie bežeckých tratí, najmä ak podnikajú aj v cestovnom ruchu. Návratnosť tejto ivestície by bola oveľa rýchlejšia, než čakať, kým les vyrastie. Stačilo by obnoviť a vyčistiť trate, označiť ich, postaviť zopár drevených mostíkov cez jarky, prípadne aj nejaké odpočívarne pre turistov. Kúpiť snežný skúter a frézu na bežeckú stopu, prípadne ťažšiu techniku si požičať. Mali by to vo vlastnej réžii, vyberali by poplatky od turistov, so športovými zväzmi či nájomcami by uzavreli dohody. Vrátili by sa im aj nocľažníci. Ale to by nesmeli byť zadubenci, ako väčšina "majiteľov" Tatier.   Štyridsať rokov chodím lyžovať na bežecké trate na Štrbskom Plese, za štyridsať rokov bežecký areál schátral, zostalo z neho torzo. Len jeden 5 kilometrový okruh, ktorý sporadicky, z dobrej vôle, udržiava prevádzkovateľ zjazdoviek.   Areál snov, kedysi pýcha Tatier a MS v severskom lyžovani v r. 1970. Desiatky km profilovo krásnych, náročných pretekárskych tratí, o nič menej zaujímavé turistické trate. Skokanské mostíky. Pravidelné preteky Svetových pohárov v bežeckých disciplínach, v skokoch a  v pretekoch kombinátorov. Všetko je preč. Nemáme areál, nie sú podujatia, nie sú športovci, ani výsledky. Až teraz je to naozaj Areál snov, môžme o ňom už len snívať.    Čosi však na Štrbskom Plese pribudlo - konský trus (pólo), nová predražená zjazdovka (za cenu výrubu kosodreviny), päťhviezdičkový hotel a apartmánové resorty pre pár zbohatlíkov. Nič pre obyčajného návštevníka Tatier, turistu, kondičného bežkára.   Voľakedy som sem pravidelne chodieval takmer každé voľno, a tiež na jarné, týždňové pobyty. Ubytoval som sa v hoteli FIS, priamo pri štarte bežeckých tratí. Cez deň som dvojfázovo absolvoval kondičný beh, večer som relaxoval v hotelovom bazéne a saune. Dnes kvôli obmedzeným možnostiam lyžovania a cenám ako pre ruských oligarchov, sem prichádzam  iba na jednodňové výlety. Urobím niekoľko okruhov na nudnej (ak ju prejdete x-krát) trati, peniaze miniem len za obed, okúpem sa a zrelaxulem v parnej saune v Aqua city v Poprade. To všetko len vtedy, ak v Košiciach nie je sneh, na začiatku a konci zimy. Inokedy sa sem prísť neoplatí.    Tejto zimy Mestské lesy urobili v okolí Košíc, na Prednej holici, Jahodnej, Bankove, Alpínke a Furči množstvo bežeckých tratí pre klasické lyžovanie. Okrem toho je v Kavečanoch, nad zjazdovkou, bežecký areál, vhodný aj na korčuliarsku techniku. Bežkárov sem vytiahnu vleky zdarma. Štrbské Pleso sa môže schovať. Čo robia Tatry pre svojich obyvateľov a návštevníkov?   V Tatrách mi chýba aj prepojenie jednotlivých osád bežeckými (v lete cyklistickými) trasami. Nie sú tu jednoduché turistické ubytovne (v drahom Švajčiarsku a ešte drahšom Nórsku, som si mohol v podobnom prostredí, za primeranú cenu, takýto druh ubytovania vyberať). Mohli by sa na ne prestavať nefunkčné liečebne (Solisko) a bývalé odborárske zotavovne.   Tatry majú mnoho nevyriešených boľačiek, isto aj dôležitejších, ako je bežecké lyžovanie, ale aj tento čriepok zapadá do mozaiky absentujúceho, komplexného riešenia tatranskej problematiky.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (76)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Nad Tatrou sa blýska, nie je pôvodná slovenská hymna!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Bezzubý sex :)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            Ak máte deti, tak poznáte odpoveď
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            A čo vy na to, pán Márai: o vlasti a štáte
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kobelák 
                                        
                                            A čo vy na to, pán Márai: o  nás a našich ženách v ich sviatok
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kobelák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kobelák
            
         
        kobelak.blog.sme.sk (rss)
         
                        VIP
                             
     
        Väčšinou som v menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5149
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            NEGEV
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nad Tatrou sa blýska, nie je pôvodná slovenská hymna!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




