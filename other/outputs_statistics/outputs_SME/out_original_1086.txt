
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Paulička
                                        &gt;
                Spoločnosť
                     
                 Materinský jazyk, národnosť a občianstvo 

        
            
                                    20.12.2008
            o
            15:05
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            4311-krát
                    
         
     
         
             

                 
                    Určite viete akej ste národnosti, ale zamysleli ste sa niekedy nad tým, aký je Váš materinský jazyk? Asi to nebolo potrebné, veď vo väčšine prípadov je rovnaký ako Vaša národnosť. Prečo sa potom pri sčítavaní obyvateľstva na to pýtajú? Akú to má vypovedaciu hodnotu? Minulé dni v diskusii k môjmu predošlému blogu jeden z diskutujúcich sa odvolával na to, že materinský jazyk menšiny má viac slovenských občanov ako je príslušníkov menšiny a z toho odvodzuje záver, že menšina je na Slovensku utláčaná.
                 

                  Priznám sa, že sa nepamätám, aký materinský jazyk som uviedol pri poslednom sčítaní obyvateľstva pred ôsmimi rokmi. Mohol som uviesť tri rôzne: slovenčinu, nemčinu a maďarčinu. Ktorý z nich by bol ten správny a pravdivý? Ak beriem do úvahy kultúru, svoje pocity a rodinné prostredie, v ktorom som vyrastal, potom moja materinčina je reč slovenská. Ak beriem za základ pôvodnú národnosť a pôvod mojej mamky, tak by to bola nemčina, aj keď tou sa u nás hovorilo najmenej. No a ak beriem za základ svoju prvú ucelenú vetu, ktorú som podľa rodinného ústneho podania povedal, tak je to jazyk maďarský. Mal by právo z toho niekto vyvodzovať, že som pôvodom Maďar a len v dôsledku zámerného poslovenčovania som sa stal Slovákom? Nech by som však pri sčítaní ktorýkoľvek jazyk napísal, cítim, konám a žijem slovenčinou.     Môj prípad určite nie je ani na Slovensku a ani vo svete ojedinelý. Stačí sa trošku pozrieť do histórie. Akú národnosť má A. S. Puškin, to vieme hádam všetci. Aký by si však uviedol materinský jazyk, nad tým by asi zaváhali viacerí. Myslím si, že to bola francúzština. Veď aj jeho prvé básničky boli písané po francúzsky. Bývalý majster sveta v šachu Botvinnik mal aký materinský jazyk, ruský alebo jidiš? Neviem. Asi sa ho na to nikto neopýtal. On sám však na otázku o národnosti odpovedal: „Pôvodom som Žid, kultúrne som Rus, ako občan som Soviet.“ Tak čo by asi napísal do dotazníka?     Aby sme nešli až tak ďaleko. Aký materinský jazyk by si uviedol velikán maďarskej poézie, maďarský básnik Alexander Petrovič, viacej známy ako Šándor Petőfi. O jeho vzťahu k národu svojho pôvodu a k svojmu  materinskému jazyku neviem nič. Ak niekto vie, nech napíše v diskusii. To, že jeho materinským jazykom určite nebola maďarčina, neznamená, že by sa nemohol cítiť byť Maďarom. Bol, respektíve stal sa Maďarom a ako takého ho musíme uznávať. V priebehu života človeka teda môže dôjsť k zmene jeho národnosti.     Z uvedeného mi plynie, že uvedenie iného materinského jazyka ako je jazyk deklarovanej národnosti nemá takú vypovedaciu hodnotu, aby sa tým dalo operovať v diskusii o stave práv menšín v danej spoločnosti. Z materinského jazyka nie je možné ani odvodzovať národnosť danej osoby. Nanajvýš môžeme usudzovať, že k danej jazykovej komunite má nejaký bližší vzťah.     Problémy vznikajú vtedy, ak niekto nikde neuvádza svoju národnosť, ale uvedie svoj materinský jazyk. V tomto prípade sa často stáva, že sa z materinského jazyka odvodzuje jeho národnosť. V mnohých prípadoch je to jeden z dôležitých dôvodov pre udelenie štátneho občianstva v štáte s týmto jazykom. Ako príklad môžem uviesť nositeľa Nobelovej ceny za fyziku, bratislavského rodáka Filipa Lenárda (1862 – 1947), po ktorom je pomenovaná aj jedna z bratislavských ulíc. Jeho materinský jazyk mu umožnil stať sa občanom Nemeckej ríše. Najzaujímavejšie na materinskom jazyku je, že je veľmi ťažké niekomu dokázať, že jeho materinský jazyk je v skutočnosti iný, ako ním uvádzaný.      Akú národnosť niekomu uviesť je niekedy naozaj problém. Najmä vtedy, ak dotyčná osoba už nemôže vypovedať (nie je medzi živými). Na ten problém som narazil, keď som ako vedúci autorského kolektívu zostavoval „Všeobecný encyklopedický slovník“. Najúspešnejší slovenský (alebo nie?)  olympionik všetkých čias bol Zoltán Halmaj (plávanie – 2 zlaté, 4 strieborné, 1 bronzová medaila). Bol občanom Rakúsko-Uhorska a rodom Záhorák. Samozrejme, že si ho prisvojujú Slováci aj Maďari. Naše dôvody: narodil sa na Záhorí, teda bol Záhorák. Záhoráci sú Slováci, tak bol Slovák. Táto logika je jasná, ale je aj iná: Slovensko patrilo do Uhorska, tak bol Uhor a Uhri, to je pre mnohých synonymum Maďari, teda bol Maďar. Tento problém v „Kronike olympijských hier“ autori vyriešili tak, že pretekárov do roku 1914 označovali ako Uhrov. So šéfredaktorkou Koškovou sme sa rozhodli vo Všeobecnom encyklopedickom slovníku v takýchto prípadoch uvádzať len miesto narodenia.      O koľko jednoduchšie to majú Francúzi. Pre nich je rozhodujúce len miesto narodenia. Kto sa narodil vo Francúzsku, je Francúz a pri tom sa ho ani na materinský jazyk, ani na národnosť nepýtajú. V USA sú zvedaví len na to, či ste, alebo nie ich občanom a ktoré jazyky ovládate. Je neprípustné v životopisoch uvádzať svoj pôvod, pohlavie, vek, materinský jazyk a dokonca k žiadosti o zamestnanie priložiť svoju fotografiu. Máme my zbytočné problémy!  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (45)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Paulička 
                                        
                                            Ale,ale pán Morvay...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Paulička 
                                        
                                            Cyprus a jeho problémy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Paulička 
                                        
                                            Nový článok14.marec1939
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Paulička 
                                        
                                            Ej, ale ten Svätopluk nám narobil!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Paulička 
                                        
                                            Referendum zo žabej perspektívy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Paulička
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Paulička
            
         
        paulicka.blog.sme.sk (rss)
         
                                     
     
        Zvedavý človek, ktorý má rád život v jeho mnohotvárnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    59
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2256
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            technika
                        
                     
                                     
                        
                            kultúra
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O mystickom tele, hornej cirkvi a troch bodkách za Bezákom
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




