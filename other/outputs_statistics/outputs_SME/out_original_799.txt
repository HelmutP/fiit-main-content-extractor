
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Anna Moravčíková
                                        &gt;
                Príroda chrámom je
                     
                 Spln Mesiaca 

        
            
                                    16.8.2008
            o
            9:19
                        (upravené
                23.10.2010
                o
                9:08)
                        |
            Karma článku:
                10.81
            |
            Prečítané 
            7832-krát
                    
         
     
         
             

                 
                    Dnes 16. augusta o 23.16 letného času je prvý z dvoch najsilnejších splnov roku. A zároveň v tento deň je možné pozorovať aj  čiastočné zatmenie Mesiaca viditeľné od 21.hod. 36 min. do 0. hod 44.min.
                 

                   
FÁZY A POHYB MESIACA.  
    Pohyb Mesiaca má výrazný vplyv  na všetky žijúce organizmy, teda aj na človeka. Ovplyvňuje náš každodenný život rovnako, ako každodenne vyvoláva príliv aj odliv.    Pokiaľ žili ľudia vo väčšom súlade s prírodou, vnímali  viac aj silu tohoto vesmírneho kolobehu a dokázali vypozorovať, čo im v určitom postavení a  v určitej fáze Mesiaca prospieva, čo je dobré jesť alebo kedy je dobré nejesť vôbec. Kedy siať, kedy vetrať periny, kedy rúbať drevo na zimné vykurovanie, alebo vyrúbať strom na výrobu nábytku, kedy uskutočniť ozdravujúci zákrok. Vnímali  reakcie  a signály svojho tela, vedeli vycítiť, kedy môžu urobiť niečo dobré pre svoje zdravie a duchovnú očistu.    Na základe pozorovania Mesiaca a jeho fáz sa vyvinul systém pravidiel a praktických rád na každý deň. Mesačné pravidlá sú jednoduché a stále rovnaké.       
SPLN   
  Zem stojí medzi Slnkom a Mesiacom približne v jednej línii, je vystavená protichodnému pôsobeniu ich príťažlivostí.   Mesiac vychádza pri západe Slnka a zapadá pri jeho východe. Celú noc vidíme na oblohe jeden zaguľatený svietiaci kotúč. Je to najvýraznejšia fáza. Organizmus je pri nej vystavený pôsobeniu protichodnej energie a záleží na našej psychike a kondícii, ako sa s ňou vyrovnáme. Niekto zle spí, iného bolí hlava a nepokoj pozorujeme aj u zvierat. Na svet prichádza viac detí, podľa štatistických údajov sa vykazuje vyššia zločinnosť a nehodovosť. Telo zadržiava vo väčšej miere vodu, hromadí ju v tkanivách, väzivo mäkne.     Počas splnu (vrátane trojdňového obdobia pred a po splne) sa snažme vyhnúť:  •	príjmu energeticky bohatej stravy – s veľkým obsahom tukov a cukrov   •	príjmu akýchkoľvek škodlivín do organizmu – t.j. návykové látky (alkohol, cigarety), konzervované,   chemicky upravované  (prifarbované)  potraviny, atď.   •	náročným operáciám, trhaniu zubov   •	očkovaniu    Pôst alebo hladovanie pri splne sa začína hodinou jeho vrcholu vo chvíli, keď začne cúvať.   (To znamená, že ak by ste chceli vyskúšať silu tohoto splnu, začnite sa postiť od 23.hod.16.min. a potom nasledujúcich 24 hodín.)      
NOV   
  Mesiac stojí pred Slnkom, obidve planéty sú v jednom znamení zverokruhu a spoločne pôsobia na Zem. Mesiac aj Slnko vychádzajú aj zapadajú spoločne, preto Mesiac na oblohe nevidíme. Vplyv  príťažlivej sily Slnka aj Mesiaca  počas novu na ľudí, ale aj zvieratá a rastliny má mimoriadny účinok. Choré stromy sa môžu opäť ozdraviť, ak ich tento deň zostriháme. Zem začína vdychovať.    Pôst alebo hladovku najlepšie začíname 12 hodín pred novom a končíme 12 hodín po vrchole novu.  Tento pôst zaručuje predchádzanie mnohým chorobám, pretože detoxikačná schopnosť tela je vtomto čase na vrchole.        
DORASTAJÚCI MESIAC   
  Je nádych.  Naše telo i rastliny všetko, čo príjmu, viac zadržiavajú. Miazga a šťavy rastlín stúpajú hore. U rastlín je posilnená tvorba listov, kvetov a plodov. Je to vhodné obdobie na doplňovanie  látok, ktoré  nášmu telu chýbajú (vitamíny, minerály).       CÚVAJÚCI MESIAC      Je výdych. U rastlín šťavy  klesajú dolu ku koreňom a preto sa posilňuje tvorba koreňov.   Je to čas, kedy sa naše telo dobre čistí a zbaví sa všetkého škodlivého. Čas vhodný na návštevu zubára či kožného lekára.       
ROZDIEL MEDZI PôSTOM A HLADOVANÍM 
    Pri pôste jeme podľa možnosti jeden druh potravy – taký, ktorý neostáva dlho   v žalúdku ani v črevách. Čím menej jednotlivých potravín jeme, tým je pôst účinnejší. Môžeme jesť čerstvú zeleninu (okrem strukovín, kapusty a inej zeleniny, ktorá nadúva), alebo ovocie (nie hrozno a hrušky). Zvýšime príjem tekutín   na 3 litre. Pijeme bylinkové čaje, zeleninové alebo ovocné šťavy (najlepšie čerstvé), čistú  alebo slabo mineralizovanú vodu. Tráviace orgány majú menej práce a  energia  sa využije na regeneráciu a zbavovanie škodlivých látok. Čistenie môže byť sprevádzané častou a riedkou stolicou, čo potvrdzuje, že sa naše telo zbavuje toxínov. Touto očistou  prirodzene schudneme a to primerane k nášmu veku, výške i zdravotnému stavu. Očista je ideálna pri problémoch s pečeňou, obličkami, pri všetkých druhoch alergií vrátane astmy, ale aj ako prevencia pred  týmito ochoreniami.    Pri hladovaní 24 hodín nič nejeme, ani jedno sústo. Pijeme  iba vodu. Tráviace orgány odpočívajú a telo si začne pre svoj chod brať energiu zo svojich zásob. Hladovanie bez jediného sústa je ťažšie, ale  má výraznejšie výsledky.      
Vyberme si podľa svojej kondície a prispôsobme sa požiadavkám organizmu. 
    Na záver želám sebe i Vám, ktorí chcete využiť postavenie Mesiaca a skúsiť očistný pôst, aby sme mali z neho radosť a príjemný pocit, že sme pre naše telesné schránky urobili niečo dobré.   Pekný deň :-)     
     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Moravčíková 
                                        
                                            Tak asi - asi tak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Moravčíková 
                                        
                                            MUDr. Evička Štefáneková - list do neba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Moravčíková 
                                        
                                            Constantin Brancusi: Ponúkam vám čistú radosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Moravčíková 
                                        
                                            Padáš
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Moravčíková 
                                        
                                            Nudíte sa? Potom dovoľte mačke, aby si vás skrotila.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Anna Moravčíková
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Anna Moravčíková
            
         
        annamoravcikova.blog.sme.sk (rss)
         
                                     
     
        Vždy viem nájsť priestor, kde môžem slobodne lietať a byť šťastná.




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1157
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Dnes je opäť krásny deň
                        
                     
                                     
                        
                            Príroda chrámom je
                        
                     
                                     
                        
                            Kvety jabloní
                        
                     
                                     
                        
                            Poslovia radosti a svetla
                        
                     
                                     
                        
                            Facilis descensus averni
                        
                     
                                     
                        
                            Neberme to až tak vážne!
                        
                     
                                     
                        
                            Mýtické bytosti a tvory
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Opýtaj sa detí
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




