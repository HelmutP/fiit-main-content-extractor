
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Baránik
                                        &gt;
                Nezaradené
                     
                 Prečo nie je pravda, že hlavným problémom súdov je Harabin 

        
            
                                    7.6.2010
            o
            12:44
                        (upravené
                10.6.2010
                o
                18:10)
                        |
            Karma článku:
                4.84
            |
            Prečítané 
            1948-krát
                    
         
     
         
             

                 
                    Ako vieme, Štefan Harabin bol, a v predvolebnom období aj stále je, cieľom útokov v súvislosti s rozvratom nášho súdnictva. Debaty sú akosi zo všetkých strán založené na pohodlnej premise, že problémom je Harabin, pričom jeho odstránením, ak by sa to podarilo, bude vec viac-menej vyriešená. Urobia sa nejaké zmeny na súdoch a všetko bude fajn.
                 

                 Nevyzerá to, že by sa Štefan Harabin označeniu za hlavného vinníka nejako vehementne bránil. Pritom na takú obranu má dôkladné možnosti -disponuje podporou na ňom závislých sudcov a členov súdnej rady, a teda by mohol tieto útoky riešiť súdnou cestou ešte oveľa razantnejšie ako doteraz a úspech by mal takmer zaručený. Navyše má tiež vcelku voľný prístup k médiám, ktoré sú vždy pripravené s veľkou zhovievavosťou až bázňou publikovať jeho trápne výlevy a protiútoky na opovážlivcov z radov jeho odporcov bez toho, že by mu, napríklad v rozhovore, kládli otázky, ktoré by na takú povahu jeho vystúpení poukazovali (viď napr. rozhovor s ním na TA3). V podobnom duchu sú prejavy opozičných politikov a médií, vrátane tých necenzurovaných. Dobrým príkladom je rozhovor v SME s poslancom za SMER-DS Mamojkom - opozícia redaktora veľmi mierna a výsledný dojem pre čitateľa, ak nie je odborníkom, je ten, že drobné chybičky systému a osobnosti JUDr. Harabina síce existujú, ale tieto odstránime čo najskôr, čím problém vyriešime.   Ako to vlastne je.   Nemá význam sa donekonečna zaoberať Harabinom, ale tými, ktorí ho tam, kde je, nainštalovali a udržujú. Harabin je figúrka, ktorá si dobre plní svoju úlohu. Súčasťou takej úlohy je rola obetného baránka. Odmenou za to mu je okrem iného to, že si môže (okrem prípadov, na ktorých majú jeho šéfovia záujem) robiť so súdmi a sudcami čo chce, samozrejme za predpokladu, že   (1)     viac-menej spoľahlivo a trvale zabezpečuje, že súdy sú jeho nadriadeným vždy k dispozícii v rozsahu potrebnom na ich ciele (peniaze, moc) a   (2)     horeuvedené robí tak, aby verejnosť, európske inštitúcie a dokonca i jednotlivci z radov nezávislých odborníkov z medzinárodných organizácií mohli byť uchlácholení, že všetko je ako-tak v poriadku, problémy sú na strane jednotlivcov, čo je v zásade riešiteľné.   Poďme si povedať aspoň niečo o tom, ako také ovládnutie súdnictva funguje. Úlohu podmanenia si súdov je možné splniť len ak je prístup komplexný a tento sa prostredníctvom nekonečného prúdu úprav zákonov prispôsobuje novým potrebám, ktoré prípadne vznikajú v dôsledku občas vynaliezavých ochrancov ústavných práv. V rámci plnenia takej úlohy sa používajú nasledujúce postupy:    Falzifikácia      výberových konaní na justičných čakateľov v záujme zabezpečenia trvalosti      súčasného stavu. Harabin upravil zákon tak, že      každý, kto má magisterský titul z právnickej fakulty (napríklad Harabinov syn      to dokázal dokonca aj pred promóciou) sa môže zúčastniť výberového konania,      na základe ktorého, ak je úspešný, sa stane tzv. justičným čakateľom. Ten      si potom (ako to označenie funkcie naznačuje) len nejaký čas „počká" na uvoľnenie      miesta sudcu  (potrebu sudcovskej      skúšky ignorujem - to je formalita). Takže v situácii, kedy súdne orgány      nevedia nič o kandidátovi na funkciu sudcu (okrem toho ako sa volá, čo ale      je rozhodujúca informácia), je zloženie takej čakateľskej skúšky v zásade      jediným kritériom na dosiahnutie toho, aby normálny absolvent právnickej      fakulty stal sudcom. Doživotne. Samozrejme, také výberové konanie na čakateľa      je podľa toho aj veľmi náročné. Extrémne. Až tak, že ho úspešne absolvujú      buďto čistí géniovia (tí sa väčšinou za justičných čakateľov nehlásia)      alebo tí, ktorí poznajú otázky a odpovede vopred. Ako? Testy pripravujú      krajské súdy, ktoré ich texty (ako inak) pred použitím zasielajú na      „kontrolu" ministerstvu. Takže výsledok je potom taký, že nejaký 22 alebo      23 ročný „génius" nielenže zodpovie na všetky otázky úplne a správne ale      ešte aj anglický preklad zo seba sype na papier bez toho, že by sa pritom      pozeral na zadanie. Veď iste, mama sudkyňa alebo ministerská úradníčka      materiály dodala s dostatočným predstihom, takže budúci sudca mal dosť      času na to, aby sa to aj on mohol naučiť naspamäť.       Takže kontinuita súčasného stavu je zabezpečená. Takto ustanovení sudcovia budú doživotne slúžiť svojim chlebodarcom. Nielenže osvedčili svoju bezcharakternosť, ale sú si vedomí, že ich šéfovia vedia ako sa sudcovské posty dostali, a teda sú im nadosmrti zaviazaní.   Ovládnutie      súdov. Súdy sú ovládané       (a)     systémovo, napr. vo veciach verejného obstarávania alebo na ochranu osobnosti pre potreby zastrašovania nepohodlných médií, či praktickou elimináciou dvojinštančnosti súdneho konania, a    (b)    prípad od prípadu.   (Ústavný súd radšej vynechám, ale úplne bude stačiť ak si prečítate reakcie jeho predsedníčky na komentáre novinárov o činnosti/nečinnosti Ústavného súdu - je to obraz morálnej úbohosti a intelektuálnej malosti na poste, ktorý vyžaduje veľkého človeka.)   K systémovému ovládaniu súdov:   -              Verejné obstarávanie sa rieši tak, že sa táto agenda vyčlení do novovytvoreného súdu (Malacky), kde, samozrejme, sú menovaní noví sudcovia. Vieme akí.   -              Veci o ochranu osobnosti sa riešia tak, že sa zmení zákon tak, že je príslušný súd žalobcu a zároveň sa zabezpečí roztrieštenosť súdov tak, aby tých zopár sudcov, ktorí prichádzajú do úvahy, boli vždy tí správni. Nič sa nezverejňuje. Poprípade, ak je to vhodné, u vystrašenej sudkyne sa zabezpečí, že pojednávanie je neverejné.   -              Problém toho, že vec „úspešne" rozhodnutú na prvom stupni by mohlo byť ťažké opakovane takto rozhodnúť na krajskom súde, bol vyriešený tiež novelou občianskeho súdneho poriadku, podľa ktorej krajské súdy nemusia ani verejne pojednávať, ani zdôvodniť svoje rozhodnutia potvrdzujúce rozsudky nižšieho stupňa. Prakticky to potom vyzerá tak, že krajskí sudcovia sa veľmi radi dajú ovplyvniť na to, aby potvrdili rozsudok nižšieho stupňa - nie je to pre ne žiadna práca a pritom sa zavďačia tým, ktorí si to „zaslúžia".       K ovládnutiu súdu v jednotlivom prípade dochádza hlavne obchádzaním pravidla zákonného sudcu tiež niektorým z nasledujúcich spôsobov:   -              Zákonnému sudcovi sa odoberie vec tak, že sa naň pod vhodnou zámienkou privodí disciplinárne konanie (po dobu doriešenia konania je sudca suspendovaný a jeho veci sú mu odobraté) a príslušnú vec predseda súdu (takmer vždy poslušný, pretože bol menovaný Harabinom) pridelí sudcovi, ktorý rozhodne ako je treba.   -              Ďalšou metódou je tzv. stáž. Stáž je prax na inom súde, napr. vyššieho stupňa, alebo na ministerstve. Môže byť stanovená kedykoľvek ktorémukoľvek sudcovi, i keď formálne s jeho súhlasom (skúsil by nesúhlasiť!). Takže, ak napríklad do určitého senátu krajského súdu smeruje vec, ktorá bola „úspešne" rozhodnutá na prvom stupni, a v tomto senáte je nejaká sudkyňa, ktorá sa nedá podplatiť ani zastrašiť, jeden zo spôsobov ako zabezpečiť, že ona nebude rozhodovať danú vec ako sudca spravodajca je poslať ju v potrebnom čase na stáž. Keď bude daná vec rozhodnutá, môže sa vrátiť.   -              Na zvláštnu objednávku pre významných oligarchov sa môže postupovať tiež tak, že sa zaplatí vhodná osoba, napríklad advokát, ktorá sa menuje za nového sudcu. Takýto novomenovaný sudca má predsedom súdu pridelenú skupinu vecí, ktorá (samozrejme) zahŕňa aj predmetnú vec. Po rozhodnutí predmetnej veci daná osoba odchádza zo sudcovského stavu.   Spôsobov je viacej a tieto je možné rôzne kombinovať. Ak sa konkrétny prípad nedá vyriešiť už preskúšanými spôsobmi (zanovitý neúplatný advokát protistrany alebo iná komplikácia), dá sa tiež zariadiť prijatie zmeny zákona tak, aby sa problém odstránil. Jednoducho, spôsob sa nájde, a ak nie, tak sa jednoducho poruší zákon, veď o tom, či bol zákon porušený alebo nie rozhodne Najvyšší súd a o tom či bola porušená Ústava alebo nie rozhodne (prípadne) Ústavný súd. Všetci vieme ako.   Na rozdiel od súdov za komunistov, kedy sa podobné metódy používali tiež, ale bez potreby ich nejako kamuflovať, súčasný stav má veľmi závažný vedľajší účinok, ktorým je zamorenie súdov korupciou a svojvôľou sudcov. Toto, verím, nie je Ficovi príjemné. Bol by radšej, keby sa to dalo bez toho. Ale, jednoducho, ak má súdnictvo ovládať, tak potom toto je cena, ktorú musí zaplatiť.   Tú cenu platíme my všetci a len my všetci môžeme tento stav zmeniť. Je totiž falošnou predstavou, že sa dá vytvoriť nejaké detailné politické usporiadanie, ktoré by pevne zabezpečilo to, že súdy, teda súdna moc ako jeden z troch pilierov delenia štátnej moci, nebude pod vplyvom moci výkonnej, teda vlády. Taký politický systém neexistuje, pretože každý, aj ten najdokonalejší systém je možné znečistiť a skorumpovať, ak si politici zaumienia, že to urobia. O tom, či súdnictvo bude nezávislé nerozhoduje dokonalosť jeho systému a prepracovanosť záruk jeho nezávislosti od vlády. Aj hrubo nedokonalý politický systém by fungoval celkom slušne v krajinách, kde silná politická kultúra jednoducho nedovolí, aby sa také nedokonalosti zneužili na potláčanie základných ľudských práv a slobôd. Aj ten najdokonalejší systém by vcelku ľahko mohol byť zneužitý niekým ako je Róbert Fico, či už prostredníctvom Harabina alebo niekoho iného. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Baránik 
                                        
                                            Ľudia, voľte Národný front!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Baránik 
                                        
                                            Súd nerozhoduje váš spor? Tu je jedno z možných vysvetlení!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Baránik 
                                        
                                            Čo nás učí rozhodnutie ESĽP vo veci obvinených v kauze Cervanová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Baránik 
                                        
                                            O referende inak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Baránik 
                                        
                                            Strach z naozajstnej zmeny alebo prečo zástanci politiky po starom nemajú pravdu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Baránik
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Baránik
            
         
        alojzbaranik.blog.sme.sk (rss)
         
                                     
     
        

Advokát, zástanca spravodlivosti aj tam, kde to nie je ľahké.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    34
                
                
                    Celková karma
                    
                                                6.54
                    
                
                
                    Priemerná čítanosť
                    2384
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




