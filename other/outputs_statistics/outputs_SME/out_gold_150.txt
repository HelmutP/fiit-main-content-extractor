

 
Dr. Fedor bol pôrodník a zároveň psychiater. Rozpracoval zaujímavý projekt. Keď ho u nás predniesol vedeckej obci, bol vysmiaty.  
Švédsko sa stalo jeho druhou domovinou. Zmenil si meno na Freyberg a nebolo to jediné, čo zmenil. 
 
 
Základná myšlienka jeho projektu bola v tom, zistiť, ako intrauterinný vývoj človeka vplýva na jeho ďalší život. A prišiel k nečakaným záverom. 
 
 
Jedným so zdrojov jeho informácií boli deti, ktoré sa narodili z nechcených tehotenstiev. Ako sa k nim dostal? Veľmi ľahko. Zo zoznamov. Boli to tie deti, ktorých matky v tej dobe predstúpili pred interupčnú komisiu, kde vysvetľovali zúčastneným cudzím ľuďom, prečo dieťa nemôžu a nechcú mať. A nepresvedčili ich. Tie nechcené deti sa narodili. 
 
 
Psychiater sledoval ich rast a vývoj celých 25 rokov. Zistil, že ich životy sú si v mnohom podobné. Seba samých takmer všetci popisovali ako nešťastných ľudí, štruktúra ich osobnosti bola depresívna, rozvádzali sa, strácali záujem o vlastné deti a ich osudy boli neraz tragické. 
 
 
Ako psychiater sa často potýkal s prípadmi , keď príčiny fóbií, narcistických psychóz a rôznych hraničných stavov u pacientov nedokázal zanalyzovať. Strávil  s pacientom dlhé hodiny pátraním po príčinách jeho stavu, venovali sa problému zo všetkých strán, ale jeho základy nikde nenašieli a katarzia neprichádzala. Keď nevedel, kedy a kde vznikol v jeho mysli daný problém, s ochorením sa bojovalo veľmi ťažko. 
 
 
Na pomoc prišla retrográdna hypnóza. Pomocou nej vracal svojich pacientov do minulosti. Zistil, že sa dá vrátiť veľmi veľmi ďaleko v čase. Do života pred životom. 
 
 
Dospelí ľudia mu rozprávali ako sa cítili a čo počuli, keď boli ešte súčasťou matky. Konečne našiel odpovede na nezodpovedané otázky, konečne sa nejeden bludný kruh, ktorý spôsoboval pacientovi utrpenie, uzavrel.  
 
 
V hypnóze boli schopní získať vzácne informácie, ucelené pocitové aj faktické celky. Priemerný človek sa za pomoci hypnózy dostal približne do svojho 8 mesiaca od splodenia. Rozprával o tom, čo o ňom matka hovorila, aké zážitky prežívala, aké stresové situácie ju postretli, kto sa v jej okolí vyskytoval a aké slová a pocity si z neho pamätal. 
 
 
V rámci štúdia týchto faktov prišiel k nečakanému záveru. Čím bo človek na vyššej inteligenčnej úrovni, tým sa dostal ďalej v čase. Princíp tejto priamej úmery bol úplne spoľahlivý. Zistil, že tí najinteligentnejší z jeho skupiny boli schopní sa rozpamätať na 4. až 5. mesiac svojho vnútromaternicového života. 
 
 
Rozpracoval metodiku, aby mohol tvrdenia svojich pacientov a klientov overovať. A boli to tisíce prípadov. Desiatky študentov a spolupracovníkov sa vydali do terénu a hľadali dôkazy pre tvrdenia, ktoré hypnotizovaní podávali. Pátrali po kalendároch, denníkoch, dokumentačných materiáloch, korešpondencii. Pýtali sa na detaily príbuzných, starých rodičov, súrodencov. Po zhromaždení tohto množstva údajov, boli všetky výpovede pacientov verifikované. 
 
 
Zistili, že ak matka zažila hrôzy, bola týraná, žila v strachu a úzkosti, dieťa trpelo po narodení úzkostnými atakmi.  
Zistili, že manické stavy, ktorých nemohli nájsť príčinu, mali tiež korene v prenatálnom vývine.  
Najnižšie a najstaršie štruktúry mozgu takto postihnutých skúmali aj pomocou elektromagnetickej rezonancie a zistili, že sú užšie a atrofované. 
 
 
Istý klient, ktorý mal veľký problém s nenávisťou voči starším tlstejším ženám, v hypnóze rozprával o svojom živote v 5. mesiacoch vnútromaternicového vývoja. Rozprával súvisle a myšlienky a pocity formuloval jasne.  
Jeho otec bol na vojne a prichádzal občas na víkend za matkou, s ktorou nebol zosobášený. Často hovoril o potrate a rozchode, čím spôsoboval jeho matke veľkú bolesť.  
Oveľa horšie na tom však bol jej vzťah k svojej matke, u ktorej vtedy bývala. Často sa spolu hádali, panovala tam zlosť a neznášanlivosť. Matka sa na svoju dcéru hnevala, že sa takto spustila, denne jej to vytýkala a nazývala nenarodené dieťa iba pankhartom. Matka po rokoch priznala, že jej syn si po celý život nevedel nájsť cestu k svojej starej mame, napriek tomu, že situácia sa po jeho narodení výrazne zmenila. Stará mama sa s tým vyrovnala, svojho vnuka mala veľmi rada a vždy sa snažila byť ústretová. 
V jeho mysli však stále predstavovala hrozbu pre jeho matku a pre seba. Preto typ týchto žien, pripomínajúcich starú mamu v ňom vzbudzoval nenávisť. 
 
 
Keďže bola pacientova trauma pomenovaná a jej základy sa našli, našli sa aj prostriedky, ako proti nej bojovať a zmierovať sa a jeho liečba úspešne napredovala. 
 
 
 
 
 
Spomienky ukryté v najhlbších častiach nášho mozgu sú do nás vryté nezmazateľne ako do kameňa. Vynárajú sa nečakane a ovplyvňujú náš život viac, ako by sme tušili a chceli priznať.   
 
 
Zdroj: MuDr. Darina Štúrová, psychiatrička a psychoanalytička, seminár pre pracovníkov a spolupracovníkov OZ Návrat  
 
 
 A ešte malý dovetok. Často si na prednáškach a seminároch píšem na okraje osobné poznámky k téme, aby som si neskôr spomenula, čo mi z nich vyplýva pre prax alebo niekedy iba toľko, kde sa moja myseľ pri týchktorých slovách zatúlala. Pri tejto časti som si našla poznámku napísanú veľkými písmenami: ŽIVOT JE DôLEŽITEJŠÍ, AKO SI MYSLÍM! 
 
 

 
 

 

