
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dominika Kuchárová
                                        &gt;
                Nezaradené
                     
                 „Boh zatrať tvoju čiernu dušu..“ 

        
            
                                    30.6.2010
            o
            22:11
                        |
            Karma článku:
                3.07
            |
            Prečítané 
            460-krát
                    
         
     
         
             

                 
                    „.Utrela si slzy a pristúpila k sekretáru, kde bola v ozdobnom ráme fotografia. -„Boh zatrať tvoju čiernu dušu“, vykĺzlo jej z pier pri pohľade na prísnu tvár. Na druhý deň dala do rámiku fotku dvojičiek...“
                 

                   
 Podľa psychologickej definície existuje medzi dvoma základnými ľudskými emóciami – láskou a nenávisťou – mnoho odtienkov. Ja však nedokážem milovať a nenávidieť inak než veľmi.                   Veľmi milujem tú osobu.   Za to, že ma brala takú aká som, bez prikrášlení. Za to, že mi vravievala, že sa nemám nad vecami toľko trápiť, minulosť už ajtak nezmením. Za jej úžasnú vôňu. Nie parfum, vôňu človeka, ktorá ešte dlho visí v povetrí a láka nadýchnuť sa jej. Za to, ako ma vedela chváliť a haniť len vtedy, keď bolo ozaj prečo. Za úsmev, ktorý mi vyčarí vždy, keď si na ňu spomeniem, pretože mi pripomína pekné momenty a ľudia sa vyjavene pozerajú, prečo sa z ničoho nič usmievam. Za momenty, keď bolo ticho a bolo to nádherne napäté. Za úsmevy a dotyky vyvolávajúce príjemné zimomriavky. Za debaty o všetkom aj o ničom... aj za slová, ktoré boli povedané. Aj za tie, ktoré boli povedané neskoro aj za tie, ktoré nikdy vypovedané neboli, lebo sa nenašlo dosť odvahy. Za noci, ktoré som pre túto osobu dusivo preplakala do vankúša, pýtajúc sa Toho hore: „Prečo?“. Za šťastie, že som ju mala česť spoznať. Za fakt, že ani nevie, ako razantne mi vstúpila do života a koľko pre mňa znamená.                   Veľmi nenávidím tú osobu.   Za to, že ma brala takú aká som len vtedy, keď sa jej chcelo. Za to, že mi dookola vyčítala chyby, ktoré patria dávno do minulosti. Za to, ako sa mi pekného slova dostalo ako šafranu. Za úsmev, ktorý v mysli vyčaria pekné momenty, no vzápätí na mňa ako príšera vyskočí z pamäte niečo zlé. Za chvíle, keď bolo ticho a dodnes mám vredy, keď si na to spomeniem. Za úsmevy a dotyky, ktoré tak ako vedeli pohladiť, vedeli aj bolieť. Za debaty o všetkom aj o ničom, ktoré sa neraz končili hádkou – o ničom. Za slová, ktoré boleli, rezali, pálili, až vypálili jazvu. A jazvy sa ťažko hoja. Za noci, ktoré som pre túto osobu dusivo preplakala d vankúša, pýtajúc sa Toho hore: „Prečo?“. Za to, že sme sa možno kedysi nemali stretnúť. A možno aj áno. Práve preto, ako ma to posunulo a ovplyvnilo.                   Zámerne som nepoužila konkrétne údaje, čas ani rod. Je jedno, či je to v mojom prípade muž či žena, hoc aj trojhlavý drak. Možno to totiž rovnako prežívate i vy. Alebo ste si už svoje prežili, prípadne vás to ešte len čaká. Jedno je isté: raz to postretne každého. Aj silná láska, aj silná nenávisť.   Milovať treba večne a podobné city si treba vedieť v sebe pestovať, nenávidieť nie. Nenávisť zotročuje a začierňuje dušu.                   Preto odkazujem všetkým milovaným:   Ďakujem, že ste.               Preto odkazujem všetkým nenávideným:   Snáď vám raz Pánboh odpustí, za všetkú horkú žlč, ktorú si na ľuďoch zbytočne vylievate. A ak vám ani ten nebude vedieť pomôcť a odpustiť, nech radšej naveky zatratí vaše čierne duše. Aby neubližovali viac ostatným.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kuchárová 
                                        
                                            Od Senzi do Senzi alebo ako Borovský Jakubca do kozmu posielal
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kuchárová 
                                        
                                            Nový (ostrý?) mediálny súboj: Šláger TV vs. Senzi (2. časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kuchárová 
                                        
                                            Nový mediálny súboj: Šláger TV vs. Senzi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kuchárová 
                                        
                                            Bubliny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kuchárová 
                                        
                                            Víťazný Telegram
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dominika Kuchárová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dominika Kuchárová
            
         
        dominikakucharova.blog.sme.sk (rss)
         
                                     
     
        ...v zásade robím všetko,čo sa prieči zdravému rozumu,a a niekedy aj na to doplácam..V podstate som ale hrdá na to,že som tým kým som a INÁ UŽ NEBUDEM!Na to som príliš stará..:).Moje motto je prosté a miliónkrát overené:NIKDY NEHOVOR NIKDY.)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    767
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nový (ostrý?) mediálny súboj: Šláger TV vs. Senzi (2. časť)
                     
                                                         
                       Modré z neba alebo „splnený“ sen na televízny spôsob
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




