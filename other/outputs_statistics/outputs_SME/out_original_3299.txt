
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondro Urban
                                        &gt;
                Sociálne médiá
                     
                 Moja Facebook Page – Post Quality 

        
            
                                    25.3.2010
            o
            14:13
                        (upravené
                25.3.2010
                o
                22:33)
                        |
            Karma článku:
                4.24
            |
            Prečítané 
            1226-krát
                    
         
     
         
             

                 
                    Pred týždňom som sa zúčastnil jedného workshopu, kde sme s kolegom prezentovali Sociálne médiá ako (viacmenej) nový spôsob komunikácie firiem. Mimo iného sme robili aj case study na túto tému. Jeden z účastníkov počas „cvičenia" mi povedal, že vlastne post na stránke sa zobrazí iba malému počtu ľudí, kvôli číslu uvedenom ako Post Quality. Nie je to však tak, v tomto príspevku priblížim ako sa čítajú štatistiky na Facebooku, konkrétne teda Post Quality.
                 

                 
Post Quality na FacebookuScreenshot z facebook.com
   Post Quality vo všeobecnosti   Post Quality hodnotí vašu prácu s fanúšikmi vašej stránky. Je to súhrn štatistík alebo ináč povedané interakcia fanúšikov s postami na stránke.   Interakcia fanúšikov   Interakciou môžme považovať všetky druhy odpovede na post. Konkrétne: komentovanie, označenie príspevku „páči sa mi to" (liking) alebo post na Nástenku (Wall) vašej stránky. Je vo vašom záujme, aby takýchto ľudí bolo čo najviac, keďže títo ľudia sú tí, ktorým sa vaše príspevky častejšie zobrazujú a títo ľudia dokážu šíriť ďalej vaše informácie.   Výpočet Post Quality   Prvý údaj, číselný, znamená percento ľudí, ktorých ste dokázali zaujať, k celkovému počtu fanúšikov. Zaujatím opäť myslím interakciu. Druhý údaj, v hviezdičkach, už je menej „racionálny".   Keďže Facebook má k dispozícii detailné údaje o stránkach, tak dokáže vypočítať, ako stránky s podobným počtom fanúšikov dokážu zaujať svojich fanúšikov. Potom jednoducho rozloží ohodnotenie hviezdičkami jednotlivé stránky.   Facebook je známy svojimi algoritmami, keďže má obrovské množstvo údajov o jednotlivých ľuďoch. Skúste porozmýšlať o tom čo ste vy Facebooku prezradili. Nielen priamo, ale aj to s kým chatujete, komu píšete na Nástenku, komentujete, likeujete a pod.   Suma sumarum   V prípade, že si zakladáte na svojom Facebook účte a máte počet hviezdičiek menší ako 3, tak určite vyhľadajte odbornú pomoc  :) V každom prípade mať špecialistu, ktorý sleduje vaše posty a radí vám ako na to, je veľmi dôležité. V opačnom prípade nemusíte postovať vôbec, lebo i tak sa to k vašej cieľovej skupine nedostane. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Problém taxíkov v zlom počasí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Výtlk, výtlčisko a také obyčajné výtlčíky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Onedlho tu už Fico nebude!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Hľadanie konečného riešenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Moja starostka - Táňa Rosová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondro Urban
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondro Urban
            
         
        ourban.blog.sme.sk (rss)
         
                                     
     
        http://twitter.com/ourban
http://facebook.com/ourban
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2255
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Web
                        
                     
                                     
                        
                            Sociálne médiá
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Donovaly: slovenský Aspen, kde Vás smetiari vrátia späť do reality
                     
                                                         
                       Fico vyrába ďalších 65 tisíc ľudí bez práce
                     
                                                         
                       Výpalníci z Úradu vlády sú horší ako smrť.
                     
                                                         
                       Na rómske komunity štátny rozpočet opäť raz zabudol
                     
                                                         
                       Prečo Slovensko nepatrí medzi šťastné krajiny?
                     
                                                         
                       Nie som ako ostatní učitelia...
                     
                                                         
                       Slovensko má depresiu. Fico ju chce liečiť marihuanou.
                     
                                                         
                       Voda bude drahšia a to nie je všetko...(časť prvá)
                     
                                                         
                       V akej krajine to žijeme?!
                     
                                                         
                       Minister šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




