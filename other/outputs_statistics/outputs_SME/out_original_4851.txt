
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Krajmerová
                                        &gt;
                Súkromné
                     
                 Nedosiahnuteľná a vzdialená Čína 

        
            
                                    19.4.2010
            o
            13:14
                        (upravené
                19.4.2010
                o
                13:24)
                        |
            Karma článku:
                2.65
            |
            Prečítané 
            573-krát
                    
         
     
         
             

                 
                    Dnes ešte ozaj neviem, či sa  mi podarí zoznámiť sa s tebou  osobne, lebo práve teraz, v čase môjho odletu do tretej najväčšej krajiny na svete - sa islandské sopky  po 200 rokoch rozhodli spraviť neuveriteľný zmätok na celej našej planéte...  
                 

                 
  
   Dokonca aj najväčšie letecké spoločnosti majú pred nimi taký zákonitý rešpekt, že radšej skrachujú, než aby sa rozhodli so svojimi boeningami vzlietnuť. Je to až neuveriteľné, ale za niekoľko dní na celom svete kvôli chŕleniu obrovských mrakov-rumburakov plných popolčeka, piesku a všelijakého svinstva, ktorý vzlietava vďaka vetrisku až do výšky desaťtisíc metrov (práve tam, kde je v súčasnosti najväčšia letecká diaľnica na našej zemeguli) zrušili desaťtisky letov. O takejto možnosti by boli pred týždňom prehrali stávky aj filištínsky a špekulatívne hraví Číňania, o ktorých sa povráva, že hazardérska vášeň ich raz môže vyniesť do neba - alebo poslať do pekla...   Z toho všetkého vyplýva, že tešiť sa do Číny môžem iba  potajomky, aj to iba ak vo svojich predstavách. V tvojom prípade, moja nedosiahnuteľná Sinaj -  celkom určite platí, že si ťa musím najprv dokonale zaslúžiť. Týka sa to aj čínskej novinárskej akreditácie, ktorej vybavovanie trvá pomaly štvrť roka - a to všetko v dobe vysokých technológií. Myslela som si, že sa Číňania administratívne a organizačne vycvičili na pekingskej olympiáde pred dvoma rokmi, ale kdeže, to bola iba moja naivita.    Na čo sa môžem tešiť?    Nedávno mi pani Slávka, ktorá už nespočetnekrát kráčala po čínskych cestičkách-necestičkách odporučila mnoho zaujímavostí. Vraj sa nemám zabudnúť dať rozmaznávať v niektorom zo šanghajských kaderníctiev, kde sa všetko začína nečakanou masážou šije a hlavy, až potom nabehne dokonale vycvičený čínsky kaderník a za našich jeden a niečo euráča mi spraví ten najúžasnejší účes na svete. To by ma teda nebolo nikdy napadlo, že budem v Číne behať po kaderníctvach... A mám sa potom staviť v niektorej čínskej lekárni, kde objavím najlepšiu jing-jang kozmetiku na svete s maličkými perličkami. A nemám odmietnuť výlety do fabrík na spracovanie čínskeho čaju alebo najúžasnejšieho hodvábu na svete. Od Slávky som dostala dokonca do daru aj čínsko-anglický ťahák pre taxikárov, lebo tí vraj nevedia inak, ako čínsky. A ešte mi poradila vychýrené čínske reštaurácie, o chutiach ktorých sa mi v našich končinách môže iba ak snívať.         Mladá architektka Barborka mi zasa odporúčala nočný život s originálnou ochutnávkou čínskych špecialít na „bicyklových" griloch - najlepšie po 22. hodine, keď už čínski policajti majú po večierke a chystajú sa do ríše snov - lebo takéto podnikanie je tu pravdaže zakázané. Vraj si tam pochutnala na rozlične nastajlovaných špajlách so zeleninkou, mäskom, ovocím a dokonca všelijako ugrilovanými chrobákmi a hadmi. Bŕŕŕ! Ale  aj úžasnou ananásovou ryžou - najlepším to zákuskom na svete.   A ešte ju uchvátila šanghajská Oriental Pearl TV Tower so sklenou podlahou. No, výšky príliš nezbožňujem, ale za dobré fotečky a vyhliadkové spomienky vydržím aj nepríjemné závrate. A potešila by som sa vraj určite pohľadu na Old City and Yuyuan Garden,  kde si udržiavajú zdravie s čínskymi bojovými umeniami á la taichi tunajší Číňania až do neskorého veku života. Barborku zaujali aj smiešne obrátené domy - podľa povery väčšinou na juh alebo mega moderné fasády ulíc, za ktorými sa nachádza ozajstný čínsky život s domčúrikmi a čínskymi trhmi.    Iný spôsob dopravy    Toto všetko by som uvidela - keby už konečne začali lietať lietadlá. Kedy to bude? NEVIEM... Ale 7-dňová únavná cesta vlakom do Číny ma ozaj neláka - hoci sa ide cez Rusko a Mongolsko. Priateľka Vierka mi spomínala, ako sa takto za bývalého režimu vracala z trojročného diplomatického pôsobenia v Pekingu, pretože sa znenazdajky komunisti rozhodli  ušetriť na letenkách a nižšie postavený personál posielali do vlasti po železnici. Ale výhodou by zasa bolo, že vláčikom islandský popolček nevadí. Keby som mala viac času... tak azda by som sa s dobrou kartárskou partiou vybrala nakoniec do Šanghaja i vlakom - ale niekedy pred tridsiatimi rokmi. Dnes mi je už čas vzácny a zdravie o to viac. Nekonečné vysedávanie vo vlakoch - to nie je moja vysnívaná adrenalínová disciplína.    Predsa si len počkám na lietadielko. Však EXPO v Šanghaji trvá až do 1. novembra - čiže celého pol roka. Medzitým možno stihnem aj svadbu našej Evičky, cestu do Londýna či Izraela a plno iných činností. Zatiaľ iba pokorne čakám so zbalenou batožinou, smutno-nádejne sa teším  na priateľku Ivanku a zaháňam boľavé myšlienky na odložený let do Číny rozmanitými činnosťami - z rešpektu k nevyspytateľnej, ale obrovskej sile našej matičky prírody...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Voniaš ako...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Týrané Slovenky, nedajte sa !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Spod plesovej krinolíny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Prírodný detox ľudského organizmu zvládne každý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Rok čínskeho draka má optimistický charakter
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Krajmerová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Krajmerová
            
         
        krajmerova.blog.sme.sk (rss)
         
                                     
     
        Autorka v júli 2012 zomrela. Blog nechávam na jej pamiatku. Dcéra Katka
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    235
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2101
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voniaš ako...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




