

  
 Vianočný čas, čas bieleho snehu a teplých bylín pre kone. Presne o tomto vianočnom čase je aj kniha „Modrý pták", ktorú napísal Maurice Maeterlinck v roku 1908. Ja som ju čítal vo vlaku a rád by som sa s vami podelil o svoje pocity, myšlienky a chvíle strávené s knihou. Divadelné hry sa čítajú zväčša krátko a táto nie je výnimkou. 
   
 Keby sa ma niekto spýtal, o čom je tá kniha, povedal by som, že o Vianociach, o snehu, o šťastí, o starých rodičoch, o láske. Vidíte, to je vlastne jedna otázka, prenasledujúc ma už nejaký ten piatok. (A ako sa poznám, isto som sa na ňu už neraz pýtal.) Ktoré dielo v literatúre by sme mohli označiť, že v ňom cítiť lásku? Áno, kdekto začne rozprávať o Romeovi a Júlii, Grófovi Monte Cristovi, o Troch mušketieroch a čo ja viem o čom. Hoci, ja osobne by som prvé dielo, ktoré mi príde o láske, povedal, že to je vianočná poviedka Začarovaný od starého pána Dickensa. A k tým všetkým, o ktorých by sme sa mohli porozprávať, by som pridal Maurica Maeterlincka. (Ono, tu sa vlastne aj samá otázka pýta, že čo je láska a ako ju človek spozná.) 
   
 Dielo belgického spisovateľa „Modrý pták" začína ako rozprávka. Príde mi ako Nekonečný príbeh starého lesa. Nie, nie je tam slávny Falco, odvážny Átrej, krásna Detská cisárovná, či kameňožrúti (hoci treba uznať, že ich bicykel stál za to), ani Ygramul, či tajomný Auryn. Je to príbeh detí, žijúc kdesi v strome. Ich domov bol symbolom ochrany, autor si možno preto schválne vybral strom. Strom prežije búrku, chujavicu, letné slnce, ktoré praží ako pražiar svoju kávu. Stromy prežijú záplavy, poskytujú nám ochranu a myslím, že popri svojich rodičoch sú to prvé bytosti, ku ktorým ucítime lásku. S nimi nie sme nikdy sami. 
   
 Ako som už spomínal, dielo začína ako rozprávkový Nekonečný príbeh. Dvaja súrodenci, Tyltyl a Mytyla, deti nežného srdca. Dostanú sa do krajiny, aby vyhľadali Modrého vtáka, je to ich úloha. Nejako takto aj začínajú rozprávky nášho detstva, dospievania. (Mal by som pokračovať v tom, že aj našej dospelosti, nuž ešte som  nedospel.) Keď si trochu prelistujem v hlave, nepríde vám, že každý človek na zemi má nejakú úlohu? 
   
 Dve deti sa dostanú do sveta, kde je všetko fantastické. Dostanú sa na kľukatú cestu, ktorú im neraz ktosi skríži. Skríži im ju niekto, kto im je blízky. A ono to aj v živote podľa mňa tak býva. Najčastejšie kráčame po ceste a palicu, ktorá nás podopiera, tú nám dávajú najbližší ľudia. Podobne je to aj s deťmi, stretnú svojich milovaných, čo už sú na inom svete. 
   
 „Babička Tylová: Mám pocit, že nás dnes přijdou navštívit naše vnoučata, která ještě žijí. 
 Dědeček Tyl: Určitě... Myslí na nás, protože já se necítím ve své kůži a v nohách mám mravenčení." (Maeterlinck, 2010, s. 30) 
   
 Stretnutie starých rodičov a vnúčat nám odhaľuje ďalšiu symboliku, symboliku mŕtvych, symboliku hrobov. Čím sú pre nás cintoríny? Bolestivé miesta, pri ktorých máme pocit, že tu nie sme sami? Keď niekto zomrie, ostane v nás ako súčasť niečoho, s čím sa nikdy nezmierime. 
   
 „Babička Tylová: Pořád tady čekáme, až nás ti, co žijí, přijdou navštívit... Pŕicházejí tak zřídkakdy! Kdy to jen bylo naposledy...? Na Všechny svaté, když vyzváněl kostelní zvon." (Maeterlinck, 2010, s. 31) 
   
 Autor píše o svete mŕtvych. Píše o nich, že sú to ľudia, ktorí sa nezmenili a rovnako sa nezmenilo ani ich správanie voči nám. Nezmenilo sa ani prostredie, ktoré sme si pamätali s nimi. Napríklad dom starých rodičov a detí. Tyltyl a Mytyla sa dostali k svojim mŕtvym starým rodičom a opäť zacítili vôňu medu. Ja si myslím, že dom starých rodičov nám poskytuje vôňu, ktorú si nesieme z detstva a je to „lístok do detstva", keď ju opäť zacítime. 
   
 Dielo sa mi stalo blízkym aj preto, lebo autor doň vkladá určitý druh nežnosti. Objímajú sa, dostávajú pusu od mamky, od starých rodičov. 
   
 „TYLTYL (okouzleně): A tyhle krásne šaty, z čeho jsou? Snad ze stříbra, z hedvábí nebo z perel? 
 Mateřská láska: Ne, jsou z polibků, pohlazení. Každé políbení jim přidá jeden měsíční nebo sluneční paprsek! 
 TYLTYL: To je zvláštní! Nikdy bych nevěřil, že jsi tak bohatá! 
 Mateřská láska: Všechny matky jsou bohaté, jestliže milují své děti. Žádna není chudá, žádna není ošklivá. Láska je vždycky nejkrásnejší z Radostí. A když vypadají smutně, stačí jediný polibeh, který dostanou nebo dají, aby se všechny jejich slzy proměnily v hvězdy až na dně jejich očí." (Maeterlinck, 2010, s. 69-70) 
   
 Autor dáva svojmu príbehu ešte mnoho z detského sveta, ich myšlienky. Napríklad v treťom dejstve, keď sa deti dostanú na cintorín. 
   
 „MYTYLA: A kde jsou ti mrtví? 
 TYLTYL: Tady pod tím trávníkem nebo pod těmi velkými kameny. (Ukazuje na náhrobní desky). 
 MYTYLA: To jsou dveře do jejich domovů? 
 TYLTYL: Ano. (Maeterlinck, 2010, s. 75) 
   
 Nebudem vám prezrádzať dej, ani ako to skončí. Rozlúčim sa s tým, čo ma zasiahlo najviac. Otázka skutočného nešťastie. Čo je vlastne najväčšie nešťastie? 
   
 „TYLTYL: A co ti dva, co se drží za ruce? To jsou bratr a sestra?  
 Dítě: Ale kdepak! Jsou směšní! To jsou zamilovaní! 
 TYLTYL: Co to je? 
 Dítě: Ja nevím. To jim tak říka Čas, z legrace! Pořád se dívají jeden druhému do očí. Líbají se a dávají si sbohem. 
 TYLTYL: A proč? 
 Dítě: Zřejmě nebudou moci odejít společne." (Maeterlinck, 2010, s. 90) 
   
 Myslím si, že to je to najväčšie nešťastie. Človek si na svojej kľukatej ceste nájde niekoho, kto ho na nej chytí za ruku. A potom, potom z tej cesty nezídu spolu. To je to najväčšie nešťastie, že neodídeme spoločne s ľuďmi, ktorých milujeme. 
   
 Bibliografický odkaz: 
   
 MAETERLINCK, M. 2010. Modrý pták. Praha : Triton, 2010. 122 s. ISBN 978-80-7387-3855. 
   
 Zoznam obrázkov: 
   
 &lt;http://www.tridistri.cz/webshop/data/books/modry_ptak_web-1267018853.jpg&gt; [cit. 2010-05-15] 
 &lt;http://www.tridistri.cz/webshop/data/books/modry_ptak_web-1267018853.jpg [cit. 2010-05-16] 
   
 PS: Knihu zoženiete prakticky hocikde. 
   

