
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Flamík
                                        &gt;
                Súkromné
                     
                 O dobrote srdca a slzách vnútri... 

        
            
                                    3.6.2010
            o
            13:21
                        |
            Karma článku:
                5.38
            |
            Prečítané 
            630-krát
                    
         
     
         
             

                 
                    Tento článok píšem ako poďakovanie Vám pani... za Vašu dobrotu, obetavosť srdca i čas darovaný pre iných. Nie je to strata. Nikdy. Vďaka Vám... a nielen Vám, ale aj toľkým iným, pre ktorých dar neznamená stratu...
                 

                 Pamätám si Vaše slová zastreté oblakom smútku, únavy, a predsa nie sklamania alebo znechutenia, ale práve naopak odvahy a túžby ísť a dávať svoje srdce pre iných...   Pamätám na Vaše pocity z víťazstiev iných, kde niekedy v nás môžu znieť tóny melanchólie, predsa však vo Vás nebolo sklamanie, závisť, hnev, ale radosť z ich radosti...   A nakoniec pamätám si Váš záujem o tých, ktorí majú rodičov, ktorí trpia vnútri a ich bolesť tak páli ich deti... slová znejúce záujmom a túžbou pomôcť...   Som rád, že Vás poznám a som rád, že môžem byť jedným z tých, ktorí stoja na Vašej strane. Strane lásky, dobra, viery v dobrotu srdca i napriek jeho krehkosti...   Želám pekné a pokojné dni.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Ak môžeš, konaj dobro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Čo iné povedať, ak nie slová nádeje?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Si človekom nádeje?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Všetko najlepšie, tati
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Flamík 
                                        
                                            Život a nádej sú silnejšie ako smrť a zúfalstvo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Flamík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Flamík
            
         
        flamik.blog.sme.sk (rss)
         
                                     
     
        Salezián. Od 18. júna 2006 kňaz. Nachádzaný Niekym, kto aj k nemu si stále a verne hľadá cestu... S úctou.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    467
                
                
                    Celková karma
                    
                                                7.38
                    
                
                
                    Priemerná čítanosť
                    950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z môjho vnútra
                        
                     
                                     
                        
                            rozličné hlasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Beato Claudio Granzotto
                                     
                                                                             
                                            O Don Boskovi viac...
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            S. Weil
                                     
                                                                             
                                            F.X.Durwell
                                     
                                                                             
                                            C.S.Lewis
                                     
                                                                             
                                            T. Halik
                                     
                                                                             
                                            Biblia
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Into the wild - soundtrack
                                     
                                                                             
                                            Sakumprásk
                                     
                                                                             
                                            Delirious?
                                     
                                                                             
                                            U2 | ...čokoľvek a kedykoľvek
                                     
                                                                             
                                            Dream theater | ...hudobní blázni
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janka Balážková
                                     
                                                                             
                                            Marek Sefcik
                                     
                                                                             
                                            fajn rehoľná sestra
                                     
                                                                             
                                             
                                     
                                                                             
                                            Miro Lisinovic
                                     
                                                                             
                                            Ivan Gábor
                                     
                                                                             
                                            Matúš Repka
                                     
                                                                             
                                            Jakub Betinský
                                     
                                                                             
                                            Ivo Hampel
                                     
                                                                             
                                            Jozef Bednár
                                     
                                                                             
                                            Juro Begany
                                     
                                                                             
                                            Mirka Zilinska - cit a pravda
                                     
                                                                             
                                            Juraj Šeliga - hlbka
                                     
                                                                             
                                            Robo Kekiš - otvorene oci
                                     
                                                                             
                                            Ondrej Močkor - misionár
                                     
                                                                             
                                            Maroš Karásek - umelec
                                     
                                                                             
                                            Marcel Tomaškovič
                                     
                                                                             
                                            Roman Effenberger
                                     
                                                                             
                                            Peter Grobarčík
                                     
                                                                             
                                            Marek Piváček
                                     
                                                                             
                                            Andrej Skovajsa
                                     
                                                                             
                                            Petra Bošanská
                                     
                                                                             
                                            Mato Habcak
                                     
                                                                             
                                            Karol Trejbal
                                     
                                                                             
                                            Bianka
                                     
                                                                             
                                            Jolana Čuláková
                                     
                                                                             
                                            Martin Liziciar - student s odvahou sa pytat
                                     
                                                                             
                                            pani Kohutiarová -mama!
                                     
                                                                             
                                            Michal Hudec - pravdivosť v hľadaní a objavovaní
                                     
                                                                             
                                            Angelo Burdej - hlboký a jednoduchý
                                     
                                                                             
                                            Karol, človek s rozhľadom a rešpektom
                                     
                                                                             
                                            MartinDinuš a Motorkári
                                     
                                                                             
                                            Jožo Červeň - kňaz na svojom mieste, odvážny
                                     
                                                                             
                                            Rudo Kopinec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Božie Slovo na každý deň
                                     
                                                                             
                                            DELIRIOUS?
                                     
                                                                             
                                            informačný portál
                                     
                                                                             
                                            welcome in dbland
                                     
                                                                             
                                            mnoho o svatych v taliančine
                                     
                                                                             
                                            kvalitne zdroje
                                     
                                                                             
                                            môj  novy domov - Partizánske - Šípok
                                     
                                                                             
                                            pápež a info o ňom
                                     
                                                                             
                                            Prameň života
                                     
                                                                             
                                            sastinska bazilika
                                     
                                                                             
                                            exallievi
                                     
                                                                             
                                            O NÁDEJI...
                                     
                                                                             
                                            o Lumene...
                                     
                                                                             
                                            o Hudbe...
                                     
                                                                             
                                            zdroj pre modlitbu
                                     
                                                                             
                                            Knazske inspiracie
                                     
                                                                             
                                            Spolocenstvo mladych
                                     
                                                                             
                                            telvízna alternatíva LUX
                                     
                                                                             
                                            dobre knihy
                                     
                                                                             
                                            duch. cvicenia
                                     
                                                                             
                                            TO JE ZIVOT
                                     
                                                                             
                                            PUT ZALUBENYCH 2009!!!
                                     
                                                                             
                                            Bazilika Sastin - Straze
                                     
                                                                             
                                            GJB Sastin -Stráže
                                     
                                                                             
                                            kvalita o modlitbe
                                     
                                                                             
                                            salezianska kapela
                                     
                                                                             
                                            saleziáni na Slovensku
                                     
                                                                             
                                            Holic
                                     
                                                                             
                                            recenzie na filmy v pohodke
                                     
                                                                             
                                            salici v Petržke
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kedy sa stávaš človekom?
                     
                                                         
                       Muž milión
                     
                                                         
                       O dvoch dobách
                     
                                                         
                       Cesta za šťastím
                     
                                                         
                       Kňazi - tak trochu inak
                     
                                                         
                       Prvé Dušičky bez Teba
                     
                                                         
                       Som za život – prosím, nazvite ma bigotným demagógom
                     
                                                         
                       Prečo som ešte stále verný svojej žene?
                     
                                                         
                       Stopárske čriepky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




