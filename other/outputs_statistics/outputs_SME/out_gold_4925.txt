

 Jacques Dutronc - Paris s'eveille 
 Ked pocujem "Paris s'eveille", mam naozaj pred ocami hodiny ukazujuce pat hodin rano a Pariz, ktory sa prave prebudza. Temu  ranneho Pariza Jacquesovi Dutroncovi navrhol jeden z jeho hudobnickych priatelov a veru, dvakrat mu nebolo treba hovorit. Skvely text plny slovnych hraciek a metafor bol inspirovany piesnou "Tableau de Paris (a cinque heures du matin)" z roku 1802. Pokial ide o hudbu, s tou sa autori trapili dlhsie, zdala sa im malo napadita, az kym sa do tvorby nezamontoval flautista z nahravacieho studia a nenahral improvizovane solo, ktore do skladby paradne zapadlo. Ved posudte sami. 
 






 
 Jacques Dutronc patri medzi stalice francuzskej hudby uz niekolko desatroci. Okrem spevu aj komponuje a obcas hra vo filmoch. Je manzelom Françoise Hardy, ich syn Thomas je tiez uspesnym hudobnikom. 
 Renaud - Mistral gagnant 
 Piesen "Mistral gagnant" nahral Renaud v roku 1985 a venoval ju svojej dcere. Je spomienkou na jeho vlastne detstvo a historka z nahravania hovori, ze Renaud vahal, ci piesen vobec zaradit na pripravovany album, kedze sa mu zdala prilis osobna. Zavolal teda svojej vtedajsej manzelke a zahral jej ju do telefonu. Ta mu odpovedala: "Ak ju na album nedas, opustim ta!" Renaud odvtedy zije s uplne inou zenou, zato piesen sa vo Francuzsku stala kultovou. 
 






 
 Renaud, vlastnym menom Renaud Sechan, patri medzi najoblubenejsich francuzskych spevakov. Je znamy nielen svojimi piesnami, ale aj vyhranenymi nazormi a kritikou spolocnosti. Ako herec sa objavil napr. vo filme Germinal. 
 Veronique Sanson - Ma reverence 
 Specificky hlas Veronique Sanson, ktory sa neda pomylit a jej osobne vyznanie. Pred par rokmi nadherne interpretovane jednou zo sutaziacich v show "Nouvelle Star". 
 






 
 Veronique Sanson rozdelila svoj zivot a karieru medzi Francuzsko a americky kontinent. Spieva od konca 60tych rokov vlastne az do teraz. Okrem hudobnych pocinov je znama aj vystrelkami v osobnom zivote, najznamejsi je snad jej kuriozny odchod od vtedajsieho manzela Michela Bergerea zaciatkom 70tych rokov. Odisla z domu s tym, ze si ide kupit cigarety a uz sa k nemu nevratila, odcestovala do USA, kde sa vydala za Stephena Stillsa z kapely Crosby, Stills &amp; Nash. 
 Michel Polnareff - La poupee qui fait non 
 Piesen, ktora preslavila spevaka menom Michel Polnareff, pochadza z roku 1966. Hudbu pozostavjucu len z troch zakladnych akordov zlozil sam interpret, slova narazaju na "uvolnenie mravov" vo Francuzsku v 60tych rokoch. Hovoria o dievcati, ktore by uz mohlo povedat "ano" namiesto stale opakovaneho "nie".  Na nahravani gitar povodnej verzie sa zucastnil Jimy Page. Piesen neskor prebrali viaceri spevaci, sam Polnareff ju nahral aj po nemecky, taliansky a spanielsky. 
 






 
 Michel Polnareff je dalsou zo stalic francuzskej hudby. Pochadza z hudobnickej rodiny, na klaviri zacal hrat uz ako stvorrocny, jeho otec pisal piesne pre Edith Piaf. Jeho poznavacim znakom su tmave okuliare s bielym ramom a obdlznikovymi sklami. 

