
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Banáš
                                        &gt;
                Úvaha
                     
                 Rozchod alebo dve strany rovnakej mince. 

        
            
                                    24.4.2010
            o
            21:45
                        (upravené
                24.4.2010
                o
                21:50)
                        |
            Karma článku:
                8.74
            |
            Prečítané 
            3455-krát
                    
         
     
         
             

                 
                    Zmierenie s rozchodom je veľmi bolestivá skúsenosť. Každý jeden z nás si tým v živote prešiel alebo ešte len prejde, záleží však na tom, ako sme to schopní zvládať. No a čo môže viesť k tomu, aby sa jedno krásne obdobie života dokázalo razom zmeniť na splnenú nočnú moru?
                 

                     Ten kto raz miloval a sklamal sa,  veľmi dobre pozná, aké je to mať zlomené srdce a všetko s tým spojené. Raz milujeme tajne, potom naša láska nie je naplnená alebo sa nechávame klamať samými sebou. Také lásky a sklamania patria odjakživa k mladosti.       Časom prídu prvé vzťahy a všetko je vážnejšie. Každý rozchod bolí, aj ten čo sa zmelie rovnako rýchlo ako novopečený vzťah.    Oveľa horšie je to ale pri dlhšom vzťahu, kde sa vytvorí medzi partnermi akýsi silný návyk. Ani takéto vzťahy však nie sú skalopevné – časom sa môže jeden partner druhému zunovať, môžu si prestať rozumieť alebo v horšom prípade – jeden z nich zaklame, podvedie.                 Najväčšie komplikácie samozrejme sprevádza krach manželstva – rozvod. Nepríjemnosti s tým spojené, je snáď zbytočné podrobnejšie rozoberať.   Niektoré manželstvá, však stroskotajú až príliš predčasne. Možno je to spôsobené aj dobou, v ktorej žijeme. Ľudia sa berú v čoraz pokročilejšom veku.   Niekedy však ani rozvod nemusí byť natoľko nepríjemný, keďže čoraz častejšie býva zvykom, že manželia spolu už dávno nežijú, keď príde k rozvodu ich zväzku.       Čo je však na rozchodoch najhoršie, je následná bolesť.   Niektorí ľudia však bolesť z rozchodu znášajú veľmi ťažko. Najčastejšie ide o mladých ľudí, predčasne sklamaných láskou, ale aj o tých starších, ktorí po dlhoročnom manželstve začnú s trpkosťou spomínať na krásne prežité spoločné roky, plné lásky a spoločnej dôvery.        Ľudia sa rozchádzajú z tých najrozličnejších dôvodov. Na niektoré musia prísť spoločne, niektoré sú však jednostranné a tie bolia najviac – postupná ignorácia, keď vás partner zo dňa na deň len tak odhodí, keď sa vás preje. Alebo klamstvá zo strachu či sklon k podvádzaniu, k prílišnému užívaniu si života a zabúdaniu na zaužívané zvyky ale hlavne na city.        Dôvod rozchodu hrá určite najdôležitejšiu rolu v tom, ako zvládať bolesť, na ktorú žiaľ neexistuje žiadny účinný liek. Sú takí, ktorým trvá návrat do bežného života skutočne krátke obdobie, no existujú ľudia, ktorí dokázali milovať tak veľmi, že sa z rozchodu či rozvodu nikdy nedokážu celkom spamätať.        Jedno je však isté. Bolesť zo straty milovaného človeka patrí k tým najväčším, ktoré môžu človeka v jeho krátkej púti životom postretnúť. Neubližuje len psychike, často má nepriaznivé účinky aj na fyzickú kondíciu človeka.        A ako ste vy znášali rozchod? Čo by ste poradili ostatným, či mladým alebo tým starším?          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Ako za starých časov 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Dva póly filmového roka 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 2
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Banáš
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Banáš
            
         
        milanbanas.blog.sme.sk (rss)
         
                                     
     
        Ak chceš vedieť aký som - spoznaj ma. Ak chceš vedieť ako píšem - čítaj ma :) 
Čo viac dodať? Som začínajúci autor z Liptovského Mikuláša.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    129
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1458
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Liptovský Mikuláš
                        
                     
                                     
                        
                            Úvaha
                        
                     
                                     
                        
                            The Game
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Vyjednávač
                        
                     
                                     
                        
                            Záhady
                        
                     
                                     
                        
                            Legendy
                        
                     
                                     
                        
                            Poviedka
                        
                     
                                     
                        
                            Spektrum
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Luceno J. - Háv klamu
                                     
                                                                             
                                            Barry D. - Veľké trampoty
                                     
                                                                             
                                            Keel J. - Mothman Prophecies
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lady GaGa
                                     
                                                                             
                                            Depeche Mode
                                     
                                                                             
                                            Hudba z prelomu 80 a 90 rokov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Sčobák
                                     
                                                                             
                                            Peter Bombara
                                     
                                                                             
                                            Pavol Baláž
                                     
                                                                             
                                            Juraj Lisický
                                     
                                                                             
                                            Dominika Handzušová
                                     
                                                                             
                                            Roman Slušný
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Supermusic
                                     
                                                                             
                                            Gorilla.cz
                                     
                                                                             
                                            Hokejportál
                                     
                                                                             
                                            Facebook
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




