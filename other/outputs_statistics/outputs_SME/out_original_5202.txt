
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Bogric
                                        &gt;
                História
                     
                 14. rok vojny- Výprava Lacedemončanov proti Argu. 

        
            
                                    25.4.2010
            o
            6:45
                        |
            Karma článku:
                3.77
            |
            Prečítané 
            674-krát
                    
         
     
         
             

                 
                    V predchádzajúcej časti som v skratke opísal aj trinásty rok vojny, takže pokračovanie tohto opisu, oproti Tukydidovmu Historiai z ktorého čerpám tieto poznatky, bolo v mojej verzii trochu skrátené. Už som skôr   poukázal na to, akú politiku robil Argos a ako zaútočil proti Epidauru, ktorý bol spojencom Lacedemončanov. Lacedemončania prišli Epidauru na pomoc, ale urobili to aj z iných dôvodov, ktoré tu boli spomenuté v skorších článkoch. Veliteľom lacedemonského vojska bol Agis, Archidamov syn, ktorý asi veľa pochytil od svojho otca, hlavne pomalosť, ale asi aj nechuť bojovať, hoci preukázal nádherné taktické myslenie a dokázal protivníka preľstiť. V tejto časti a v niekoľkých ďalších častiach tohto roku vojny opíšem aj podmienky vzniku konfliktu a boj, ktorý bude mať pre ďalší priebeh peloponézskej vojny podstatný význam. Týmto rokom ešte nevznikne priama vojna medzi Aténčanmi a Lacedemončanmi ( prímerie porušia Aténčania až v 18. roku vojny), ale tie akcie veľmi podstatne ovplyvnia ďalšie dianie. V tomto období mali Lacedemončania iba niečo málo nad 5000 hoplitov.
                 

                     " Uprostred nasledujúceho leta sa Lacedemončania vybrali na výpravu s celým vojskom aj s heilótmi proti Argu, keďže ich spojenci, Epidaurčania, potrebovali pomoc a niektoré peloponézske mestá už od nich odstúpili a ani s ostatnými to nebolo dobré. Nazdávali sa, že ak patrične nezakročia, tento stav sa môže ešte zhoršiť. Veliteľom bol Agis, Archidamov syn, lacedemonský kráľ. Preto uprostred leta sami Lacedemončania podnikli výpravu proti Argu s celým svojím vojskom a heilótmi. Na výprave sa zúčastnili aj  Tégejčania a ostatné spojenecké mestá Arkádie. Iní spojenci z Peloponézu a od Korintu sa zhromaždili vo Fliunte. Z Boiótie prišlo 5000 hoplitov a rovnaký počet ľahkej pechoty, potom 500 jazdcov a za každým jazdcom sedel jeden pešiak. Z Korintu prišlo 2000 hoplitov a z ostatných miest toľko, koľko mohli vystrojiť. Iba z Fliunta prišlo všetko vojsko, lebo sa zoraďovali na vlastnom území.   Argejčania sa dozvedeli o vojnových prípravách Lacedemončanov hneď od začiatku a keď Lacedemončania pochodovali k Fliuntu, aby sa pripojili k ostatným, aj Argejčania sa dali na pochod. K nim sa pridali Mantinejčania so svojimi spojencami a 3000 elejskými hoplitmi. Na pochode sa stretli s Lacedemončanmi pri Metydriu v Arkádii, kde obidvaja protivníci zaujali postavenie oproti sebe na pahorkoch. Argejčania sa pripravili do boja, lebo Lacedemončania boli sami, ale Agis v noci s vojskom nepozorovane odtiahol do Fliunta, aby sa tam spojil s ostatnými spojencami. Keď to Argejčania zistili, aj oni sa dali na svitaní na pochod, najprv do Argu a potom do Nemeie, kde podľa ich mienky, mohli zastihnúť Lacedemončanov s ich spojencami. Ale Agis, namiesto toho, aby s vojskom postupoval týmto smerom, išiel so svojimi Lacedemončanmi, Arkaďanmi a Epidaurčanmi ťažko schodnou cestou a zostúpil do roviny Argu. Súčasne Korinťania, Pellenčania a Fliunťania tiahli inou strmou cestou. Boióťania, Megarčania a Sikyončania na jeho rozkaz postupovali po ceste, kde zaujali postavenie Argejčania. Agis sa domnieval, že ak Argejčania začnú proti ním útočiť a budú brániť rovinu, Boióťania so svojou jazdou ich zaženú, obmedzia ich činnosť, alebo ich aspoň zastavia. Podľa tejto úvahy zoradil vojsko, vpadol na rovinu a vyplienil Samintos a iné mestá v okolí Argu.   Keď to Argejčania zistili, bolo už svetlo, odpútali sa od Boióťanov a ich spojencov a vyrazili na pomoc prepadnutým mestám z Nemeie. Narazili na tábor fliuntského a korintského vojska. Niekoľkých Fliunťanov a Korinťanov pobili, ale na oplátku tiež mali niekoľko mŕtvych. Medzi tým Boióťania, Megarčania a Sikyončania postupovali cestou do Nemeie, ale Argejčanov tam už nezastihli, lebo tí zbadali, že ich územie je vyplienené a tak sa spustili na rovinu a zoradili sa do boja. Lenže zrazu zistili, že oproti ním sú Lacedemončania a  ostatní spojenci, ktorí ich obkľúčili. Zo strany od roviny im bránili prístupu do mesta Lacedemončania s Arkáďanmi a Tégejčanmi, zhora Korinťania a Fliunťania a od strany z Nemeie Boióťania, Sikyončania a Megarčania. Okrem toho nemali jazdu, lebo Aténčania, ako jediní zo spojencov ešte nepriši. Väčšina Argejčanov a ich spojencov nepokladali situáciu za takú kritickú, lebo majú predsa Lacedemončanov obkľúčených na svojom území, v blízkosti svojich miest. Ale dvaja Argejčania, Trasylos, ktorý bol jeden z piatich stratégov a Alkifron, lacedemonský proxenos, prišli k Agidovi v čase, keď sa vojska už mali každú chvíľu stretnúť v boji a povedali mu, aby sa nepúšťal do boja, lebo Argejčania sú ochotní prijať rozhodnutie rozhodcovského súdu, ak ich Lacedemončania z niečoho obvinia a ak s nimi uzavrú dohodu, vraj s nimi budú v budúcnosti nažívať v mieri.   Obaja Argejčania konali v tejto záležitosti z vlastnej vôle, bez príkazu argejského ľudu. Agis prijal ich návrhy, ani sa neporadil s viacerými ľuďmi, lebo sa zhováral iba s jedným veliteľom tejto výpravy a uzavrel prímerie na 4 mesiace, počas ktorých sa mali splniť sľuby tých dvoch; vzápätí odišiel  vojskom, pričom nepovedal nič ani svojim spojencom.   Lacedemončania a ich spojenci sa podriadili rozkazu v zmysle vojenskej disciplíny, ale medzi sebou obviňovali kráľa. Skutočne, podľa ich mienky, tu premárnili príležitosť zaútočiť na nepriateľa, ktorý bo zo všetkých strán obkľúčený pechotou a jazdou, no predsa museli odísť, neuskutočniac nič, čo mohlo vykonať také veľké vojsko. A naozaj, ešte nikdy sa v Grécku nezhromaždilo také obrovské vojsko. A k tomu sami vyberaní vojaci, schopní sa stretnúť v boji nielen s Argom a celým jeho spojenectvom, ale aj s ktoroukoľvek inou mocou, ktorá by sa  k Argu pridala. Takto obviňovali vojaci Agida, keď ustupovali a vracali sa do svojich miest.   Ale aj Argejčania obviňovali tých, ktorí uzavreli prímerie bez rozkazu ľudu, lebo aj oni boli presvedčení, že im Lacedemončania utiekli vo chvíli, ktorá sa im už málokedy naskytne. Boli by totiž bojovali blízko svojho mesta s pomocou mnohých statočných spojencov. Po návrate sa zastavili v Charadre, kde po vojenskej výprave zasadal vojenský súd a dokonca začali kameňovať Trasyla. On sa však zachránil útekom k oltáru, no jeho majetok bol skonfiškovaný."   Niekedy sebavedomie niektorých ľudí je priveľké a až tak, že si neuvedomujú určitú skutočnosť. Môj názor je tiež taký, že Argos mohol utrpieť takú porážku z ktorej by sa spamätať už vôbec nemusel, lebo ako spomenul Tukydides, stálo proti nemu veľké vojsko, jeho vojaci boli obkľúčení a k tomu im chýbala jazda. Hoci by boli bojovali pri svojom meste, ale nie je záruka, že by väčšina ich vojska bola vyviazla z obkľúčenia bez väčších strát a že by im obránci mesta mohli poskytnúť lepšiu ochranu. Prečo Agid ustúpil, to sa asi nedozvieme (je možné, že bol podplatený), ale následky boli neblahé. Alkibiades sa zase ukáže, ako dokonalý politik, ktorý pre svoju a slávu a svoj  prospech dokáže urobiť čokoľvek a ani sa neštíti presvedčiť iných k porušeniu. O Alkibiadovi a jeho krokoch v politike budem písať v nasledujúcich článkoch o tejto vojne a bude toho popísané dosť.   "Onedlho prišlo na pomoc Argu 1000 aténskych hoplitov a 300 jazdcov pod vedením Lacheta a Nikostrata, no Argejčania sa i napriek tomu neodvážili porušiť prímerie. Prosili Aténčanov, aby opustili Argos a spočiatku im ani nedovolili zúčastniť sa na ľudovom zhromaždení s ktorým sa Aténčania chceli dohodnúť, kým ich k tomu naliehavými prosbami neprinútili Mantinejčania a Elejčania. Aténski vyslanci, medzi ktorými bol aj Alkibiades, oznámili v ľudovom zhromaždení Argejčanov a ich spojencov, že Argejčania nemali právo uzavrieť mier bez súhlasu ostatných spojencov a teraz (keď sa dostavili Aténčania na miesto) treba opäť začať bojovať.   Takto prehovorili spojencov a hneď sa vydali na pochod proti arkádskemu Orchomenu všetci okrem Argejčanov, ktorí síce súhlasili, no spočiatku nešli do vojny, ale nakoniec sa predsa pridali. Všetci sa utáborili pred Orchomenom, začali ho obliehať a podnikať proti nemu výpady nielen preto, lebo ho chceli dobyť, ale aj preto, lebo tam boli zhromaždení zajatci, ktorých tam zhromaždili Lacedemončania.   Vtedy obyvatelia Orchomenu začali mať obavy- ich opevnenie nebolo silné a nepriateľského vojska bolo veľa a navyše nik im neprichádzal na pomoc- že zahynú skôr, ako im prídu na pomoc a tak uzavreli s Mantinejčanmi dohodu o spojenectve a sľúbili, že vydajú rukojemníkov a tých, ktorých tam zanechali Lacedemončania.   Keď sa spojenci zmocnili Orchomenu, začali sa radiť proti ktorému mestu majú podniknúť najprv výpravu. Elejčania chceli tiahnuť proti Lepreiu, Mantinejčania proti Tegei. Argejčania a Aténčania sa pridali na stranu Mantinejčanov, ale tým urazili Elejčanov, ktorí sa urazili a vrátili sa domov. Ostatní sa pripravovali na ťaženie proti Tegei, o to väčšmi že medzi Tegejčanmi sa našli občania, čo chceli vydať mesto zradou."   Z taktického hľadiska sa Argejčania a Aténčania nerozhodli zlé, lebo Tégea poskytovala Lacedemončanom, ako ich najspoľahlivejší spojenec, najviac vojakov, ktorí navyše boli veľmi spoľahliví. Spoľahlivejší, ako lacedemonskí hopliti, peltasti a ľahkoodenci z radov heilótov. Ako sa vyvŕbi to ťaženie opíšem v nasledujúcej časti, v ktorej sa jedno mesto zbaví obvinenia zo zbabelosti.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            23. rok vojny- 1. časť- Alkibiadove vojenské výpravy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Xenofón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            22. rok vojny- Úspechy a porážky aténskeho vojska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            21. rok vojny- kapitola č. 2- Politika Farnabaza, osud Syrakúzanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Kapitola č. 1 – Pokračovanie Tukydida
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Bogric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Bogric
            
         
        bogric.blog.sme.sk (rss)
         
                                     
     
        Kto nepozná minulosť, nepochopí súčastnosť a nebude vedieť, čo ho môže čakať v budúcnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    138
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    722
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vyhoďme do vzduchu parlament!
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Starosta je vinný a čo tí druhí
                     
                                                         
                       Nehoráznosť a zvrátenosť
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Naši zákonodarcovia
                     
                                                         
                       Karel Kryl – nekompromisný demokrat aj po revolúcii
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Závisť aj po dvoch rokoch
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




