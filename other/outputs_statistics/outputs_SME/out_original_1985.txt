
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Zákutný
                                        &gt;
                Moja MYŠ(a)LIENKA
                     
                 Diagnóza: Svadba 

        
            
                                    23.12.2009
            o
            8:02
                        (upravené
                23.12.2009
                o
                0:13)
                        |
            Karma článku:
                9.99
            |
            Prečítané 
            3556-krát
                    
         
     
         
             

                 
                    Sú naozaj za vodou tí ľudia, ktorí sú po svadbe? Zaujali ma príbehy mojich kamarátok. Veď posúďte sami...
                 

                 
  
   Problém dediny spočíva v tom, že žena, ktorá zostane starou dievkou, je v kuloároch starších báb veľmi často pretrásaná. A toho sa mnohé obávajú. Podľa socialistickej módy sa ženy vydávali ako 18 ročné. Povedzte mi, ktorá žena v 18-tke dokáže určiť, aký partner v živote bude pre ňu ten pravý. Možno neprežila ani sklamanie, ani tu pravú lásku a už sa chce zaviazať na najbližších 60 rokov jednému človeku. Taká žena teda ani nevie posúdiť, či je ten jej partner dobrý alebo zlý...aspoň sa domnievam.   Začnem u mojej bývalej známej. Tá bola tiež presvedčená, že svadba má prísť na druhý deň po maturite, aby ju ľudia nepovažovali sa vyvrheľ spoločnosti. Dnes ma táto dáma 25 rokov, na krku neschopný alkoholik, ktorý jej nerozumie, tri deti a hypotéka na ďalších 30 rokov. Rozvod naprichádza v úvahu, pretože mienka ľudí je dôležitejšia ako jej život. Tak sa teda tvári, že je veselá a takto jej to vyhovuje... Pýtam sa „dokedy"?   Ďalšou do zbierky je slečna, ktorá, keď si uvedomila, že už má 23, je príliš stará a súca na vydaj. Preto rozhodila siete u svojich známych, aby jej našli životného partnera. A hľa, našiel sa slobodný starec v kristových rokoch. Túžba jeho mamy, aby sa konečne oženil bola abnormálne vysoká. Či sa mu chcelo či nie, či si rozumeli či nie, museli sa vziať. On ju predstavil rodine, ona jeho a už by bolo príliš nezdravé rozchádzať sa. Rodina ju brala za nevestu. Tak po polročnej známosti hor sa do svadobnej siene. Myslíte si, že stačí poznať človeka tak krátko? Bodaj by im to vyšlo...   Kamarátka vždy snívala o pompéznej svadbe. Ešte počas školy študovala svadobné časopisy. Našla prvého človeka, ktorý by si ju vzal a o chvíľočku bola svadba. Nehovorím, že im to neklape, ale ani klape...načo im však bol ten uponáhľaný postup?   Najviac ma však mrzí najnovší poznatok. Je to baba, ktorú mám veľmi rád. Bude sa vydávať. Nie z lásky, ani pre peniaze. Nemiluje ho, ale on je súci na ženenie. Tlačí ju do svadby, ktorá už teraz nemá budúcnosť.  Chce mať konečne ženu, ktorá mu bude prať, variť a on sa bude nerušene venovať svojej milovanej práci. Dievča, ktoré je sklamané láskou svojho bývalého. Je krásna, múdra a šikovná. Mohla by mať na každý prst jedného, ale tento si ju uchmatol ako prvý a začal prípravy na svadbu. Má dosť času na vydaj, ale bojí sa, aby nezostala sama. Berie jeho ponuky, aj keď sníva o inom živote. Čo myslíte ako asi tento príbeh skončí?...   Keď sledujem svojho suseda, viem, že jediné čo si v svojej 50tke praje, je niekto, kto by s ním za celý deň prehodil len zopár slov. Celý život je sám. Chápem ho. Chýba mu manželka alebo družka. Nikto v živote nechce byť sám, ani ja nie. Prečo sa však zaviazať hneď pri prvej príležitosti a bez istoty, že človek, ktorého si chcem vziať je ten, ktorého milujem. Žena je vraj v 35-ke najkrajšia, tak kde sa ponáhľať. Možno som mladý a neviem to adekvátne posúdiť, ale každý si zaslúži prežiť skutočnú lásku a nežiť doma s tyranom, alkoholikom alebo človekom, ktorý ani nevie kedy ste sa narodili. Veď život prináša mnohé situácie a v 25-ke ani v 30-tke ani v 40-tke nekončí...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Zákutný 
                                        
                                            "Tá sláva zabrať dáva, našťastie pomáha tráva."
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Zákutný 
                                        
                                            Aj v Orange a T-mobile chcú mať Vianoce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Zákutný 
                                        
                                            Moja spomienka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Zákutný 
                                        
                                            Vzťah na diaľku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Zákutný 
                                        
                                            Boli sme na exkurzii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Zákutný
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Zákutný
            
         
        zakutny.blog.sme.sk (rss)
         
                                     
     
        Vyhodťe ma dverami, ja sa vrátim oknom podám Vám ruku a poviem: "Ďakujem, aj tak som potreboval vyvetrať..."
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5736
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ne(politické)
                        
                     
                                     
                        
                            Ne(technické)
                        
                     
                                     
                        
                            Čo život dal...
                        
                     
                                     
                        
                            Moja MYŠ(a)LIENKA
                        
                     
                                     
                        
                            Cítim sociálne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Mafia v Bratislave
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Natália Blahová
                                     
                                                                             
                                            Mária Dudová Bašistová
                                     
                                                                             
                                            Maroš Černý
                                     
                                                                             
                                            Daniela Nemčoková
                                     
                                                                             
                                            Anca Jamerson
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Tá sláva zabrať dáva, našťastie pomáha tráva."
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




