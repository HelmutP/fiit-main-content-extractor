

 
pretože tu keď sa niekto vykloní z okna a pozrie na vás, dokáže sa ešte usmiať alebo vám len tak zamáva, 
 
 
 
 
 
 
 
 
pretože po námestí sa preháňajú cyklisti a inlinisti a nikto ich v tom neobmedzuje, ako v Bratislave, 
 
 
 
 
 
  
 
 
Pretože Košice majú veľké, zachovalé historické jadro so zeleným námestím, s palácmi i mešťanskými domami, ktoré som kedysi prelozila od pivníc po povaly, ale ešte stále ich nepoznám tak dobre, aby som v bočných uliciach nemala čo objavovať - napríklad tajomné dvory s malými hrobčekmi neznámych škrečkov, 
 
 
 
 
 
 
 
 
 
 
 
  
 
 
  
 
pretože milujem krásne toskánske stĺpiky s jemne vydúvajúcimi sa bruškami, 
 
  
 
 
pretože si mesto chráni svoj industriál - nájdete tu zrekonštruovanú požiarnu zbrojnicu i starý pivovar, 
 
 
 
 
 
  
 
 
pretože tu v lete na námestí býva toľko kvetov, ako kedysi v Banskej Bystrici a kto má unavené nohy, môže si ich ochladiť vo fontáne alebo si zahrať šach, 
 
 
 
 
 
  
 
 
pretože tu žijú básnici, ktorí píšu svoje reflexie na múry domov, 
 
 
  
 
 
pretože tu nájdete začarované kľučky a tajomné okná a na jednom z domov dojemne insitnú Sixtínsku madonu od Rafaela, 
 
 
 
 
 
 
 
 
  
 
 
pretože tu zazriete smutných ľudí, kráčajúceho majstra Jakobyho, babky v tuhých taftových sukniach či ponáhľajúcu sa vílu, 
 
 
  
 
 
 
 
 
  
 
 
 
 
 
pretože na sídlisku Lunik 9 žije a pracuje Jožko Červeň, 
 
 
 
pretože aj tu sa občas v meste robia blbosti (napríklad stavba Auparku na Námestí osloboditeľov alebo boje o postavenie Útvaru hlavného architekta, takže Košiciam nemusím až tak veľmi závidieť, ale napriek všetkému mám pocit, že stred mesta je pre ľudí - 
 
 
- a pretože je očividné, že sa tu ľudia ľúbia... 
 
 
 
 
 
 
 
 
Tak aj preto mám Košice rada :-) 
 

