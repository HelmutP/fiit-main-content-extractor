

 Človek potrebuje pre svoje prežitie okrem tých základných vecí ako je vzduch, voda, potrava... aj spoločnosť ostatných ľudí. Dobre, priznávam, zčasti je to pravda ale myslím si, že nie celkom 100%-ná. Každý z nás si hľadá niekoho na koho sa môže spoľahnúť, komu môže venovať lásku a od koho ju aj dostane naspäť. Počul som veľa príbehov, keď si ľudia mysleli, že takého človeka nepoznajú a že sú na celom svete sami... Ako prepáčte ale to považujem za blbosť a mierne sebecké. 
 Zoberme to takto: idem po ulici, oproti mne masa ľudí... opýtam sa vás, koľko z nich sa podľa vás pozeralo pred alebo aspoň okolo seba? Možno 2 - 3. Ostatní si vôbec nevšímali okolie, nevšímali si veci, do ktorých narážali a čo je ešte horšie, nevšmali si ,,tie bytosti" okolo seba. Všetci pochodovali vo svojom svete, dvere a okná pozatvárané, MP3 alebo handsfree v ušiach... Tak teraz neviem, chce človek spoločnosť alebo nie? 
 
 Ja osobne patrím k tej menšine čo ideme s hlavou vzpriamenou, nasávam všetko okolo seba. Musím povedať, že zatiaľ sa mi darí obklopovať sa tými správnymi ľuďmi...áno občas sa nezhodneme alebo pohádame ale práve to krásne tajomstvo je, že si vždy nájdeme cestu späť nech je akokoľvek ťažká alebo dlhá. 
 Nechcem sa povyšovať ale som hrdý na to čim som, a o to viac, pretože viem, že celú moju osobnosť formujú práve týto ľudia ze čo im neuveriteľne ďakujem! 
 
 Preto ak sa cítite akokoľvek osamelí alebo opustení, vedzte, že aj vy nájdete toho pravého človeka / ľudí! Stačí si len otvoriť oči a uvidíte to čo ste doteraz nevideli alebo nechceli vidieť. Celý svet vás miluje svojím osobným spôsobom, veď čo by bol svet alebo spoločnosť bez hociktorého z nás...všetci sme v tom spolu záleží len na nás ako sa budeme jedn k druhému správať!!! 
   
 PEACE &amp; LOVE :):):) 
   
   

