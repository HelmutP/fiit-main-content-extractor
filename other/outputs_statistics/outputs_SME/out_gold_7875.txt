

   
 Vstúpim tam už len sporadicky, niekoľko kamarátok zásadovo odmieta Facebook, tak aspoň cez "rýchlu poštu" udržiavame aký taký kontakt, aby sme sa jedna druhej úplne nestratili. Aj vďaka ich skúsenostiam som dospela k istej typológii osôb mužského pohlavia pohybujúcich sa v týchto vodách. Diagnózy, ktoré sa sporadicky opakujú  a trpia nimi hlavne muži...Najčastejšie ma oslovovali takíto: 
 Don Juani bez fantázie 
 Ide o zvláštne zoskupenie chlapcov, či chlapov, ktorí oslavujú veľké množstvo žien. Stačí, keď je slečna alebo žena on-line a už ju skúšajú chytiť do pasce. Ani si nedajú tú námahu byť originálny. So spolubývajúcimi sme si niekedy čítali správy, ktoré nám nejaký „pokecman" poslal a často sme zisťovali, že nás tri oslovil ten istý človek. Skopírovanou vetou bez štipky fantázie, ktorú poslali v krátkom čase snáď všetkým prihláseným dievčatám relatívne vyhovujúcim vekom.  Postupne sme si mohli začať robiť zoznam nickov, ktorých sa vyvarovať, lebo sa nám to nestalo len raz. Väčšina bohužiaľ neoplýva zdatnou pamäťou a po pár týždňoch skúša opäť. Na tej istej adrese, ktorá je nesprávna. 
 Notorickí "pozývači" 
 Neviem koľkýkrát sme už odmietli ich pozvanie na kávu alebo do kina. Nevzdávajú sa a skúšajú to znovu a znovu. Nevedia pochopiť a možno ani nechcú, že naozaj nemáme záujem. Slová odpovede „možno raz" nikdy neznamenajú kladnú šancu. Ak by niekto odmietol moje pozvanie kamkoľvek, asi by som sa druhýkrát neobťažovala pozývať ho opäť. 
 Skutoční Don Juani 
 Zopár druhov, ktorí však predstavujú mizivé množstvo, vedia ako na to. Vždy majú po ruke správne slová a bez problémov zaujmú. Snažia sa urobiť si čo najväčšiu zbierku dievčín a virtuálnych kamarátok. Z veľkého množstva sa predsa vždy nejaká chytí. Áno, ovládajú umenie ovládania J No tie múdrejšie dievčatá ich po čase odhalia, tie hlúpejšie sadnú na lep. Jedna moja kamarátka to vystihla dokonale: „Nikdy by som sa s nikým nechcela zoznamovať na chate, lebo by som nemohla tomu človeku veriť...to čo píše mne by mohol písať ďalším piatim..." A presne toto je typické práve tomu skutočnému Don Juanovi. Jemu to prejde. Pozor!!! Niekedy sa však za príťažlivou fotkou muža môže skrývať obyčajná žena, ktorá si rada robí žarty. Kamarátka si takto vytvorila nick s mužským meno a profilom. 
 „Baby ma zbožňujú, lebo presne viem, čo im napísať a čo chcú počuť," povedala mi. 
 Úchylní Don Juani/Exhibicionisti 
 Posielajú vám e-mailom svoje fotky. Nie tváre ani celého tela. Je na nich len tá časť tela, ktorou sa akože asi chcú chváliť. Ani nestihneme reagovať, že nemám záujem nič také vidieť. Potom sa nás niekto z času na čas spýta, akú máme veľkosť topánky, prípadne, či nemáme doma nejaké staré topánky, alebo či nemáme záujem vidieť dotyčného v nejakej chúlostivej situácii. Iní chcú byť iba otrokmi alebo túžia vymlátiť dušu z nás. Alebo: Potrebujeme niekoho, kto by nás pri tom natáčal, ty nemusíš robiť nič, iba sa na nás dívať...A samozrejme dostaneš za to zaplatené... 
 Koľko ľudí, toľko chutí. Dostala som zopár správ s nechutným obsahom, ktoré ani nemám silu zverejniť, mám pri písaní v tomto smere isté zábrany. 
 Zadaný Don Juan hľadá lepšiu 
 „Mám frajerku, ale už ma nepriťahuje. Chcel by som nejakú druhú, ale neviem ako sa rozísť s tou terajšou. Prosto to nie je také jednoduché." Nejak takto sa prihovárajú nekoneční hľadači. Absolútne nefér chovanie voči aktuálnym partnerkám. Radšej volia cestu klamať a podvádzať pred rozchodom. Jasné, že napokon im v konečnom dôsledku ide aj tak o to isté a všetko ospravedlňujú slovami, že musia mať sex...lebo ho potrebujú. Kamarátka jednému odporučila kúpu vákuovej pumpy alebo gumenej Anče, s ktorými môžu spokojne trénovať a dokonca im do toho ani kecať nebude. 
 Sexuchtiví Don Juani 
 Keď mi prvýkrát prišla od niekoho ponuka na sex, strašne ma nahnevala. Určite som vtedy autora mala chuť rozniesť v zuboch, veď v profile mám jasne napísané, aký je môj postoj k príležitostným radovánkam. Po čase som pochopila, že tento typ profily nečíta. Pohľadnejšia žena na fotke úplne stačí k tomu, aby ju oslovil a jednoducho skúša rad za radom. Neraz, tváriac sa seriózne, mi napísal nejaký muž, ktorý s manželkou hľadajú do trojky mladú ženu, no i dievčina, ktorá chcela zistiť, aké by to bolo práve so ženou. 
 Niekedy mi to nedalo a prihovorila som sa takému „chtivcovi" otázkou, či mu skutočne stačí so ženou iba sex. Vraj áno, ak to obom stranám vyhovuje a dokonca to môže fungovať veľmi dlho...a vraj sa skôr či neskôr vždy nejaká nájde, najvďačnejšie sú študentky, čo si chcú privyrobiť... 
 Ženatý Don Juan hľadá milenku 
 Takéhoto blbečka som poslala kade ľahšie naposledy. To už bolo teda fakt dávno:-) Ešte som si skopírovala aj ten náš „rozhovor," lebo som už vtedy vedela, že raz o týchto somároch napíšem článok. 
 on: ahoj, ako sa máš? 
 ja: čo hľadá ženatý muž na pokeci? (po prečítaní základných informácií v profile) 
 on: možno priateľku... 
 ja: a manželka nestačí? 
 on: nie :) 
 ja: ďalší produkt tejto doby... 
 on: tak veru 
 ja: smutné a zarážajúce zároveň... ale je ti jasné, že si na zlej adrese? 
 on: jasné 
 ja:  tak? ale dík za inšpiráciu na blog...rozčúliť sa niekom - to je najlepšia inšpirácia... 
 on: tak ahoj 
 Nemý Don Juan 
 „Pokecáš?"takto bežne vyšle rýchlu správu potencionálny záujemca o nadviazanie známosti, aj keď len virtuálnej. 
 „Jasné, prečo nie," zvykla som obvykle zareagovať a čakala som na spôsob, akým sa dotyčný začne debatu. Často zabil práve odpoveďou: „A o čom?" A bolo po diskusii plnej súvetí. 
 Nechápem. Niekto ma osloví a predpokladám, že sa chce o mne niečo dozvedieť. Vzápätí však zabudne, čo to vlastne bolo...či? J Občas som zvykla byť trpezlivejšia a posielať rozvitejšie vety, dať mu šancu a skúsiť niečo z diskutujúceho dostať, no márne. Buď vie komunikovať a písomne sa zdatne vyjadruje alebo má smolu. Jednoslovnými vetami ešte nikto nikoho neočaril. 
 Opačným prípadom by mohol byť Don Juan oplývajúci košatým vyjadrovaním a bohatou slovnou zásobou, trpiaci len malým nedostatkom - neovláda pravopis. Nemám na mysli drobné prehrešky typu - množné číslo prídavných mien v nominatíve, či predložky s/z. Kaskáda hrubíc a preklepov odradí aj tú najvytrvalejšiu „pokecuchtivú." Teda v prípade, že ona záhadu písania i/y ovláda :-) 
 Zopár svetlých výnimiek :-)  
 Nechcem byť zase tak krutá. Vždy sa nájdu aj výnimoční ľudia. Zopár takých, som mala tú česť spoznať aj ja v džungli zvanej „Pokec." Občas to boli práve tí, ktorí si príliš neveria a neoplývajú veľkým sebavedomím. Zväčša to boli práve takí, ktorí ani nehľadali a našli. Aj ja osobne poznám zopár šťastných párov, ktorých životné cesty splietol práve opovrhovaný chat. Možno aj takých, ktorí trpia zbytočnými komplexami a v skutočnosti nemajú prečo. Tiež som celý život patrila medzi nich. Niekoľko hodnotných ľudí, s ktorými nás spájali spoločné hodnoty, postoje a názory, či vášeň pre istý druh hudby. S podaktorými sa skutočne dalo „pokecať." 
 Nikdy mi o nič iné ani nešlo, len chlapi zväčša nemali záujem len o písanie. Aj kamarátke sa stalo, že si s niekým dlhšie písala a keď odmietla pozvanie na kávu, okamžite letela zo zoznamu priateľov...Nuž neviem, čo si predstavujú ľudia pod slovom „pokec," ale ja predovšetkým "rozprávanie" a v tomto prípade virtuálne. Istým spôsobom nesie v sebe zvláštne čaro, no pozor, vie aj prerásť cez hlavu, ak narazíte na ten správny proťajšok...:-) 

