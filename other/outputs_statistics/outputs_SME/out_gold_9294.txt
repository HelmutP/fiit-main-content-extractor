
 Píšem o thajskej masáži v kaviarni v Pressburg Cofee, čarovnom ostrove Slovákov uprostred thajskej divočiny zvanej Koh Samui.

Thajská masáž bolí. Vlastne nie ona BOLÍ!!!. Keď ti jemné thajské masérky vykrútia rameno za hlavu, skočia pätou na chrbát alebo ti vytiahnu nohu z kĺbu, vtedy kričíš: „ARROY MAK MAK!“  Masérka sa vtedy na teba súcitne pozrie a opýta sa: „Ty si z Európy však?“

Počas nášho pobytu sme si pravidelne dopriali thajskú masáž, tradičnú, olejovú, aloe vera, foot masáž a podobne. Zo začiatku sme si mysleli ako silno nám dávajú plážové masérky zabrať, no až neskôr sme zistili, že to bolo iba „hladkanie turistov“. Skutočná masáž začala až neskôr. V kaviarni, v našom „domácom“ masážnom salóne sme sa od masérok dozvedeli, že existujú tri druhy tradičnej masáže. Rovnako ako existujú tri druhy štipľavej tradičnej polievky Tom Yam – pre turistov, pre odvážnych turistov a  pre Thajcov.

Kým ti nepraští v kostiach nie je to masáž, kým ti z očí nestriekajú slzy nie je to polievka.

Dali sme si teda thajskú masáž, tú tradičnú. Vypýtali sme si médium – pre odvážnych turistov. ... Keď hostia cez sklenené okno videli čo sa s nami robí s údivom v očiach na nás civeli. Raz nám dala ruku za hlavu, inokedy kľačala na slabinách, alebo si z úsmevom na perách užívala našu bolesť sprevádzanú známymi výkrikmi keď tlačila lakťom pod lopatkou.

Nech už to vyzeralo akokoľvek dnes viem otočiť hlavou o 180 stupňov, ruky si spojím z chrbtom a dokonca si zaviažem šnúrku na topánke bez toho aby som si kľakol. 

Úplnou lahôdkou však je keď ti objaví miesto na nohe, kde ťa zabolí... Takú radosť v očiach má azda len thajský boxer, ktorý práve vyhral domáce majstrovstvá pred tisícovým publikom. Neprejde ten bod alibistickým konštatovaním typu: „Niečo máte so žalúdkom.“  Práve naopak! Zakúsne sa do boľavého miesta a masíruje pokiaľ bolesť úplne neodíde – ARROY MAK MAK!!!!  Skučíš od bolesti, cítiš, že sa ti v žalúdku krúti, v duchu si pripomenieš tie najhoršie nadávky ale vydržíš to....Cítiš, že ti to pomáha... Po hodine sa už len blažene usmievaš, zatváraš oči a zaspávaš. 

Takže vedz, že keď nezažiješ Arroy MAK MAK, nebol si na masáži. A nezabudni, tie na pláži to dokážu tiež, len si to musíš vypýtať. 
 
