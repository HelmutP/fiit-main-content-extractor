
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Kubuš
                                        &gt;
                Irak
                     
                 Výlet do severného Iraku 

        
            
                                    26.3.2010
            o
            9:40
                        (upravené
                26.3.2010
                o
                9:57)
                        |
            Karma článku:
                13.35
            |
            Prečítané 
            3659-krát
                    
         
     
         
             

                 
                    Túžba navštíviť Irak, aspoň ten severný vo mne driemala už vyše roka. Až začiatkom marca som si však povedal "idem", zbalil batoh a rozhodol som sa splniť si ďalší zo širokej plejády snov. Autobus Istanbul, lietadlo Diyarbakir, pár prestupov a zrazu stojíme na irackej hranici pod obrovskou červeno-zeleno-bielou vlajkou s veľkým zlatým slnkom v strede. Vitajte v irackom Kurdistane!
                 

                    Nádych, výdych, turecké výstupne pečiatky, prvý iracký čaj v čakárni, niekoľko otázok, kam, prečo, na ako dlho, veľká modrá pečiatka v pase a odrazu sme za hranicami. Krásny pocit. Krásny a zároveň zvláštny, pretože človek má očakávania aké to všetko bude, čo všetko zažije. Prvú noc ostávame spať pár kilometrov za hraničným prechodom v prvom meste Zakho. Krajinu pokryla noc a nemá zmysel sa ponáhľať. Našli sme lacné ubytovanie v centre mesta a ideme sa prvý krát poprechádzať večerným mestom. Ľudia sa už vytratili z ulíc a prvého koho stretávame je chlapík vystupujúci z auta s preveseným kalašnikovom na pleci. To bol len prvý z tých mnohých, ktorých sme napokon po ceste stretli. Večer zažívame náš prvý výpadok elektriny a celé mesto sa na pár minút ponorilo do tmy. Ráno nám na recepcii nechcú nechať batohy. Ktovie čo im preblesklo hlavou, keď sme si ich tam chceli odložiť a tak v plnej poľnej vybiehame do ulíc. Jedinou pamiatkou Zakha je staroveký most Delal, ktorý pretína rieku Chabúr. Chvíľku sme si po ňom polozili, prešli na druhú stranu, pozdravili sa s miestnymi a prvý krát sme si všimli, že majú končeky prstov zafarbené atramentom. Včera sa konali v Iraku voľby.         Vlajka Kurdistanu, ktorá veje v každom meste na každom kroku   Staroveký kamenný Delal Bridge ponad rieku Chabúr  Dohuk nás privítal svojim chaosom. Tým krásnym orientálnym chaosom, ktorý sa rozlieva všetkými uličkami. Starí Kurdi v typickom oblečení, v dlhých širokých nohaviciach, so šatkou uviazanou okolo pása a sem tam aj okolo hlavy si nás premeriavajú a padajú prvé pozdravy. Dievčatá nie sú všetky zahalené, ale naopak zdá sa mi, že väčšia väčšina môže cítiť vietor vo vlasoch. Celým centrom sa rozlieva pravý bazár. Ako to už býva zvykom, majú tu určite všetko na čo si človek spomenie. Túlame sa medzi ľudí, obchodíky a užívame si pohľady a pozdravy miestnych. Turistov sem veľa nezavíta, tak nás nikto nikam neťahá ako v Egypte, ale všetko je akési bezprostrednejšie. Malé čajovne sú zaplnené ľuďmi, tak sa pridávame aj my. Tu v Iraku sa pije tak sladký čaj, že musí byť na spodku pohárika aspoň 3 centimetrová vrstva cukru. Na prvý krát nám prišiel neskutočné sladký, na druhý krát sa naše pocity nezmenili, no pri ďalšom poháriku sme si zvykli a vychutnávali jeho chuť priamo na ulici. Piť čaj na ulici má svoje čaro, pretože človek môže pozorovať čo všetko sa okolo neho deje. V Dohuku sme dokonca zažili najnebezpečnejší zážitok našej cesty. V Dream City, mieste plnom kolotočov sme si boli zajazdiť na motokárach a v plnej rýchlosti sme do seba s Mišom vrazili. Skúsili sme si voľný pád a v parku plnom ľudí a zábavy človeku ani len na um nezíde, kde sa to vlastne nachádza. Aj keď Dohuk nemá žiadne očarujúce pamiatky, má v sebe niečo, prečo si ho návštevník musí zamilovať. Aj my sme sa do neho ešte vrátili na jednu noc pri spiatočnej ceste.    Odvolené   Naša "káblová" ulica v Dohuku   Starý muž na prechádzke bazárom   Káble, káble, všade káble   Pestré farby v bazáre   Usmievavý kamarát z bazáru   Neodmysliteľné kurdské vlajky na ulici   Tu si prišli na svoje všetky miestny ženy a dievčatá   Túlame sa bazárom   Momentka z bazáru   Barzání pri povolebných oslavách   Večer sa ulica zaplnila oslavujúcimi ľuďmi   Jeden z mnohých vypitých čajov     Neďaleko od Dohuku leží posvätné mestečko Lalish. Pre Jazídov je najposvätnejším miestom na celom svete. Už pár kilometrov pred Lalishom vidím v krajine prvé pyramídové svätyne, tak tuším, že budeme blízko. Vstup do dediny stráži ozbrojený vojak a miestni tu žijú v údolí medzi horami. Lalish je pre nich tak svätý, že väčšina domácich Jazídov kráča po celom mestečku bosí. Milí ľudia za nami hneď prichádzajú a sprevádzajú nás svojimi uličkami. Najprv si pozrieme nádherný výhľad na celý Lalish a potom zídeme až do svätyne, kde je pochovaný Šejk Adi ibn Musafir, stelesnenie Anjela Páva, ktorý zohráva v ich náboženstve vedúcu úlohu. Pri vstupovaní nesmieme stúpiť na prach dverí. Ten Jazídi vždy pobozkajú a prekročia. Vo vnútri je niekoľko sarkofágov a pestré látky. Na nich sa viažu a rozväzujú uzle pre šťastie.   Svätyne Jazídov v Lalishi majú pyramídový tvar   Jeden z miestnych obyvateľov posedáva na 1000-ročnom strome   Posvätné miesto. Hrobka Šejka Adi ibn Musafira   Vo vnútri posvätnej hrobky  Najväčším mestom irackého Kurdistanu je Arbil. Nad mestom tróni staroveké Citadela a pod ním sa rozlieva mikrokozmos s názvom bazár. Tisíce tvári, obchodíkov a nespočetné množstvo tovaru. Kam sa pohneme sme stredobodom pozornosti a ľudia chcú vedieť odkiaľ sme, odfotiť sa, alebo sa aspoň na chvíľku pozdraviť s cudzincom. Citadela pôsobí z diaľky majestátne, no akonáhle naše kroky prekročia vstup nadšenie opadne. Za jej múrmi je len niekoľko polorozpadnutých domov a celkom pekne zdobený minaret. Zato výhľad na celé mesto, ktoré máme stojí za to, aby sa sem človek pozrel. Parkom sa túlame popod zničený minaret Sheikh Choli a odbočky ukazujú smer na Mosul, Bagdhad či Kirkuk. Ako rád by som sa poprechádzal Bagdadom, keby to bolo len trochu možné. Pri vyslovení Mosulu majú strach aj samotní Kurdi a slovo „Mosul“ najčastejšie ukončia gestom kedy si prstom prejdú cez krk. Ani oni tam nechodia. Veď ani nieje dôvod. Parky v Arbile sú neskutočné. Samé jazierka, športoviská, lavičky, zelená tráva ako stvorená na piknik, desiatky ľudí, ktorí si sem prišli oddýchnuť. Prečo také niečo majú v Iraku a u nás nie? V kresťanskej štvrti Ain Kawa sme navštívili niekoľko kostolov. Každý z nich má ochranku ozdobenú samopalmi a pri vstupe na nádvorie kostola dostávame otázku, ktorej sa tu nikto nezačuduje „Máte u seba nejaké zbrane?“ Najzaujímavejším je nepochybne kostol sv.Jozefa. Patrí chaldejskej cirkvi a s troškou fantázie pripomína staré babylonské stavby.  Stali sme sa aspoň na chvíľku súčasťou mesta, poznáme niektoré jeho ulice, bazár, sem tam stretávame ľudí, ktorých sme už stretli a našli sme si čajovňu kam chodíme ako domáci tráviť večery. Kecáme pri pohárikoch sladkého čaju a hráme karty, ktoré sa podobajú na domino. Večer vybehnú Kurdi do ulíc a oslavujú volebné výsledky (myslel som, že už sú oficiálne, keď takto oslavujú). Stojíme na ulici a všetko pekne sledujeme. Davy ľudí mávajú kurdskými vlajkami, žlto-červenými vlajkami Barzániho strany PDK, kričia oslavné heslá, v diaľke niekto vypálil salvu zo samopalu a všetci spoločne sa tešia, že zvíťazil Barzání a život v Kurdistane bude zase o čosi lepší.   Vstup do starovekej Citadely   Tradičné kurdské uviazanie šatky okolo pása   Domčeky obkolesujúce Citadelu   Staroveká Citadela nad Arbilom   Baghdad či Kirkuk? Najistejšie to bude ale v Minaret Parku :)   Minaret Sheikh Chooli   Obrovský park okolo minaretu   Farby Arbilského bazára   Kto by si tu vedel vybrať?    Citadela ponorená do slabého osvetlenia   Krása nočného Arbilu   Aj v Arbile sa v noci oslavuje   A oslavuje a oslavuje   Jalil Khayat, najväčšia mešita Arbilu   V obľúbenej čajovni   Ozbrojená ochranka počas piatkovej omše   Chaldejský kostol sv.Jozefa   Výlet ku kurdskej pevnosti Khanzad asi 15km od Arbilu  Do Suleymanieyh som sa vybral sám. Z Arbilu to je 180km, takže cesta trvá okolo troch hodín. Cesta vedie cez mestečko Koya, kde treba prestúpiť a pokračovať priamo do Suleymaniyeh. Je tu aj priama cesta, tá však vedie cez Kirkúk a tam to môže aj nemusí byť drsné. Ak ide človek cez Koyu užije si nádhernú kurdskú krajinu. Hory stúpajú, klesajú, niektoré sú pokryté snehom, iné zase vyzerajú ako tufové vežičky Kapadokie. Prechádzame okolo Dukanu, kde je obrovská priehrada, najväčšia svojho druhu v celom Iraku. Nechám sa vyhodiť pri známej Červenej pevnosti, kde Saddám zatváral Kurdov a postupne likvidoval. Prechádzam sa sálou plnou maličkých zrkadielok. Každé jedno symbolizuje jednu obeť pod jeho režimom. Keď si myslím, že už som na konci, zase ma čaká ďalšia zákruta plná malých zrkadiel. Je ich tu akosi priveľa. Vonku stoja odstavené tanky, odpaľovacie zariadenia a iné zbrane. Chvíľku si posedím na tráve s kurdskými pašmergami a idem sa túlať porozbíjaným domom. Aj dnes tu človek nájde veľké množstvo guliek v stenách. V suteréne je miestnosť zaplnená červeným svetlom. Po stenách visia fotky z mestečka Halabja, ktoré vstúpilo do dejín po tom, čo počas operácie Anfal použil Saddámov režim chemické zbrane. Vyše 5000 ľudí prišlo o život. Samotné fotky sú brutálne, ale ešte depresívnejšie je prostredie. Rozbitý dom, holé, chladné steny, ostré červené svetlo, ktoré miestami oslepuje až kráčam s prižmúrenými očami a zimomriavkami po celom tele. Vonku sa túlam uličkami až som sa dostal k bazáru. Suleymaniyeh je najmodernejším irackým mestom a v jeho centre to aj cítiť. Vysoké budovy, moderná zástavba, parky, široké cesty. Vyzerá inak ako zvyšok Iraku. Bazár je zase miestom, kde sa zastavil čas. Je tu najkrajší iracký bazár aký som videl a dokonca aj ľudia mi prídu ešte príjemnejší ako v neďalekom Arbile. Kupujem si čaj, ale predavač moje peniaze odmieta a podáva mi veľké vrecko plné čaju so slovami „Ja som Kurd a ty si môj hosť“. Kde všade na svete sa to ešte deje? Je radosť sa tu prechádzať, fotiť, pozerať sa ako ich život obteká okolo mňa kým stojím na mieste. Miestami mi to pripomína baghdadské trhovisko, ktoré poznám z obrázkov. Zjem lacný faláfel a ešte raz sa prejdem tým všetkým aby som to prežil znovu, aj keď viem, že už to rovnaké nebude.    Zrkadlová miestnosť v Suleymaniyeh. Jedno zrkadlo. Jeden ľudský život.   Zoči voči   Zjazvené steny Červenej pevnosti   Na nádvorí je vystavený ťažká technika   Typický ruch v centre starého mesta   Kurdi v typickom oblečení   Predáva sa všetko a všade   Mladé dievčatá zo Suleymaniyeh   Atmosféra starého bazáru   V srdci starého bazáru   Chaos, neporiadok, krása, to je bazár   Kto si dá hlavu?   Veľká mešita v Suleymaniyeh  foto: Tomáš Kubuš, Zakho, Dohuk, Lalish, Arbil, Khanzad, Suleymaniyeh, 8.-12.3.2010  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Libanon 2014 (foto)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Buzkaši na brehu kirgizského Issyk Kul
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Sýrska Resafa. Prekvapenie v púšti
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Tatev. Najkrajší kláštor Arménska
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Severný Cyprus 2013 (foto)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Kubuš
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Kubuš
            
         
        kubus.blog.sme.sk (rss)
         
                        VIP
                             
     
         Milujem cestovanie, cudzie krajiny, jedlá, čaj, Turecko, Blízky či Stredný východ, Indiu, fotografovanie, písanie..no jednoducho CESTOVANIE!!! mám sen precestovať celý svet, zdvihnúť sa a ísť, zastaviť sa na miestach po ktorých túžim a pokračovať až na jeho koniec, potom sa vrátiť a ísť opäť, ale inou cestou...   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    292
                
                
                    Celková karma
                    
                                                8.29
                    
                
                
                    Priemerná čítanosť
                    3047
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Grécko s batohom 2007
                        
                     
                                     
                        
                            Káhira - Istanbul 2008
                        
                     
                                     
                        
                            Čarovná Perzia 2008
                        
                     
                                     
                        
                            Central Asia, Iran 2009
                        
                     
                                     
                        
                            Južný Kaukaz 2010
                        
                     
                                     
                        
                            Turecko, Irak 2010
                        
                     
                                     
                        
                            Turecko
                        
                     
                                     
                        
                            Severná Európa
                        
                     
                                     
                        
                            Maroko
                        
                     
                                     
                        
                            Monako
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Blízky Východ - Stredný Východ
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Francúzsko
                        
                     
                                     
                        
                            Anglicko
                        
                     
                                     
                        
                            Benelux
                        
                     
                                     
                        
                            Blízkovýchodné dobrodružstvo
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Grécko
                        
                     
                                     
                        
                            Ukrajina
                        
                     
                                     
                        
                            Rusko
                        
                     
                                     
                        
                            Irak
                        
                     
                                     
                        
                            Nemecko - Rakúsko
                        
                     
                                     
                        
                            Malta
                        
                     
                                     
                        
                            Tunisko
                        
                     
                                     
                        
                            Čechy a Morava
                        
                     
                                     
                        
                            Cyprus
                        
                     
                                     
                        
                            India, Nepál 2013
                        
                     
                                     
                        
                            Juhovýchodná Ázia
                        
                     
                                     
                        
                            Arábia
                        
                     
                                     
                        
                            Balkán
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Za zvoncami karaván
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Vilček
                                     
                                                                             
                                            Renáta Kuljovská
                                     
                                                                             
                                            Braňo Skokan
                                     
                                                                             
                                            Frenky Hříbal
                                     
                                                                             
                                            Zdenko Somorovský
                                     
                                                                             
                                            Peter Dzurinda
                                     
                                                                             
                                            Marek Pšenák
                                     
                                                                             
                                            Peter Gregor
                                     
                                                                             
                                            Andrej Hajdušek
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dobrodruh
                                     
                                                                             
                                            CestovanieSvetom
                                     
                                                                             
                                            Blízky Východ
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo navštíviť Thajsko?
                     
                                                         
                       Beslan desať rokov po tragédii
                     
                                                         
                       Skopje - mesto, kde sa snúbi nové so starým
                     
                                                         
                       Krížom čarovnou, ale chudobnou Indiou
                     
                                                         
                       Tartu a Čudské jazero
                     
                                                         
                       Škandinávsky road trip: Bergen
                     
                                                         
                       Cestoval som v Afganistane. Blázon alebo fajnšmeker?
                     
                                                         
                       Lahemaa - v krajine zátok, lesov a močiarov
                     
                                                         
                       V horskom pohraničí Čečenska a Dagestanu
                     
                                                         
                       Cez Turkmenistan a Uzbekistan do centra Hodvábnej cesty
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




