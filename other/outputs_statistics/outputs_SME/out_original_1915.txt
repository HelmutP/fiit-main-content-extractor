
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                              Drogy (Vlado Schwandtner)
                                        &gt;
                Články našich detí
                     
                 Smutný raj - nový začiatok 

        
            
                                    1.12.2009
            o
            14:07
                        |
            Karma článku:
                14.63
            |
            Prečítané 
            5191-krát
                    
         
     
         
             

                 
                    Melisa dopísala svoju knihu z ktorej ste si mohli prečítať ukážky v niekoľkých článkoch na tomto blogu pod názvom „Smutný raj“. Prišlo mi veľa e-mailov so žiadosťou o pokračovanie. Zatiaľ sa čitatelia nedočkajú ďalších častí, no až upraceme surové texty, ktoré boli písané jedným dychom bez odstavcov a s chybami, tak dúfam, že v krátkom čase sa tú knihu podarí vydať. Navrhol som Melise, nech začne písať na vlastnom blogu. Veľmi som ju zaskočil, lebo ona svoje písanie považovala zatiaľ iba za očistu vlastnej duše. Momentálne si užíva stav bez drog,  v ktorý už nedúfala. Po dvanástich rokoch tvrdého drogovania, prostitúcie a asociálneho života sa cíti možno ako v raji, aj keď si uvedomuje, že nič nemá vyhraté.
                 

                 Melisa:  Hmm, ako to napísať. Začnem asi tým, že s Vladom sme vo vojnovom stave. Je to dosť prehnaný výraz, ale pravdou ostáva, že už skoro týždeň tu s ním diskutujem o mojej budúcnosti. Hľadáme niečo zmysluplné, čomu by som sa mohla venovať, čo by ma napĺňalo a robilo šťastnou. Pre mňa zatiaľ veľká neznáma. Za posledné obdobie, čo som tu, ma robilo šťastnou hlavne to, že som išla na záchod vykonať veľkú potrebu aspoň dva krát do dňa. Je to obrovský úspech, keď to porovnám s obdobím fetovania. Keďže heroín spôsobuje zápchu, tak som sa dostala na záchod asi raz za dva, alebo tri týždne, aj to som si musela pomôcť nejakými preháňadlami. Tak, nejako samo od seba to nešlo. Podrobnosti tu opisovať nebudem, myslím že sa to nehodí.  Vlado mi dnes ráno povedal, aby som napísala článok a možno mi to pomôže rozhodnúť sa. Navrhol mi pred pár dňami, aby som začala písať na blog. Akosi sa neviem nadchnúť pre túto vec. Ale nepripisovala by som tomu nejakú veľkú vážnosť. Ja do všetkého vstupujem akosi bez emócií. Tie ma chytia až po čase. Nemôžem za to, mám to pokazené. Spôsobilo to možno moje dvanásť ročné fetovanie a možno moja povaha. V našej rodine sa emócie veľmi nenosia.  Keď som sa Vlada opýtala, načo všetko by to bolo dobré, jeho odpovede nemali konca kraja. V živote som nepočula toľko argumentov. A ja stále nemôžem pochopiť k čomu by to bolo, o čom by som mala písať, prečo, načo, začo,... Kto už len nato bude zvedavý?! Vlado mi oponuje tým, že som vlastne jeden z najväčších odborníkov, čo sa týka narkománie, prostitúcie a ešte všeličoho iného, práve preto, lebo som to sama prežila. Tieto témy sú, najmä u nás veľmi tabuizované a možno práve ja by som ich mohla ozrejmiť a dať odpovede na doposiaľ nezodpovedané otázky. A keď mi to nevyjde, tak si stále môžem spraviť výučný list na svadobné poschodové torty, podľa slov Vlada. Takže sa vlastne nemám čoho báť.   Priznajme si, že Vlado tiež nie je celkom normálny. Ale faktom ostáva, že chce len to najlepšie pre mňa a ja sa toho budem držať. Možno to vyznie tak, že nemám vlastný názor, ale opak je pravdou. Práveže mám toho názoru akosi priveľa a to mi spôsobuje ťažkosti. Aby som sa do toho pustila, musím sa presvedčiť, že to bude na dobrú vec, bude to prospešné mne, ako aj ostatným, bude ma to tešiť a v neposlednom rade ťahať dopredu. Tak presne si predstavujem zmysluplnú vec. Len si to neviem zatiaľ logicky zosumarizovať. A to mi hovorila aj mama, že nemám logické myslenie. Cha!  Pred dvoma týždňami som dopísala knihu, ktorá sa týkala práve týchto tém, teda môjho života. Myslela som si, že tým sa to pre mňa všetko skončilo. Vlado ma vyviedol z omylu. On si stále myslí, že mám veľa čo povedať, lebo do knihy sa ani zďaleka všetko nezmestilo. A ešte dodal, že by to bolo veľmi užitočné. Spomínal, že ľudia vedia tak strašne málo a ich názory sú také scestné a takisto aj ich riešenia. Teda prd platné. Stále si neviem zvyknúť na myšlienku, že práve ja by som mohla usmerňovať ľudí tým správnym smerom. Zas na druhej strane, prečo „nezachraňovať“ ľudské životy?!  Veď aj mňa zachránil niekto úplne cudzí. Teda vtedy mi bol cudzí, dnes tak už Vlada neberiem.  Tak teraz naozaj neviem, čo mám robiť?! Čo mi naozaj dopomôže k tomu, aby som bola šťastná? Lebo to je najzásadnejšia vec, ako som sa dozvedela, aby mi to v živote fungovalo a neťahalo ma to naspäť k drogám. Tak poradí mi prosím niekto?   Aj taká obyčajné vec, že sa človek dokáže bez problémov „vykakať“, môže za určitých okolností vyvolať eufóriu. Melisa sa učí znovu žiť, zvládať obyčajné jednoduché veci a darí sa jej to dobre. Aj keď sama je vlastne ešte na kolenách, myslím si, že písanie jej veľmi pomôže. A nielen jej. Svoje príbehy, postoje a zážitky dokáže sprostredkovať úžasným spôsobom, o čom sa môže každý presvedčiť prečítaním jej článkov „Smutný raj“. A práve tieto jej výtvory zaiste dokážu pomáhať aj iným ľuďom, aby sa do podobných situácií ako ona nedostali. Tiež aj tým, ktorí sa tam už dostali a potrebujú zaradiť spiatočku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          Drogy (Vlado Schwandtner) 
                                        
                                            Len tak, o svitaní a slnečniciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          Drogy (Vlado Schwandtner) 
                                        
                                            Pani Adamková
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          Drogy (Vlado Schwandtner) 
                                        
                                            Géniusko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          Drogy (Vlado Schwandtner) 
                                        
                                            Boh má iba čisté deti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                          Drogy (Vlado Schwandtner) 
                                        
                                            Tak a je po tom...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera:   Drogy (Vlado Schwandtner)
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                  Drogy (Vlado Schwandtner)
            
         
        drogy.blog.sme.sk (rss)
         
                                     
     
        O problémoch mladých, ich rodín a hlavne o nebezpečenstve závislostí. Tiež o tom, ako predchádzať životným katastrofám, prípadne ako ich úspešne riešiť.
Prvý kolektívny blog na SME.Tento blog budú svojimi článkami zaplňovať rôzni autori, ktorí sa dostali do problémov s drogami, alebo majú k tomuto problému čo povedať.
  
 
ZÁZEMIE - úžasné združenie, kde som podpredseda
   



  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    64
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3887
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Články našich detí
                        
                     
                                     
                        
                            Články rodičov našich detí
                        
                     
                                     
                        
                            Články príbuzných našich detí
                        
                     
                                     
                        
                            Denníky
                        
                     
                                     
                        
                            Všeobecné
                        
                     
                                     
                        
                            Raperus anonymus narcoticus
                        
                     
                                     
                        
                            Dobrá víla Zuzka
                        
                     
                                     
                        
                            JÚLIA FUČÍKOVÁ
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       SMER buď nepozná realitu alebo vedome zavádza
                     
                                                         
                       Len tak, o svitaní a slnečniciach
                     
                                                         
                       Pani Adamková
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




