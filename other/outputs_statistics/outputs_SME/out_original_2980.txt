
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Gogola
                                        &gt;
                Bubny a bubeníci
                     
                 Bonzo večne živý 

        
            
                                    20.3.2010
            o
            16:28
                        (upravené
                26.5.2010
                o
                18:26)
                        |
            Karma článku:
                4.88
            |
            Prečítané 
            1182-krát
                    
         
     
         
             

                 
                    Šesťdesiatku má dnes už hociktorý sopliak, poznamenal svojho času môj vtedy tuším osemdesiatročný, dnes už nebohý dedko. Bohužiaľ, nie každý sa tohto veku dožije. Osud to nedoprial jednému z najvýznamnejších rockových bubeníkov, Johnovi Bonhamovi. Zomrel v septembri 1980 ako tridsaťdvaročný. Doplatil na svoj úprimný, nefalšovane priateľský vzťah k alkoholu. Mňa osobne irituje, že na nedožitú Bonzovu šesťdesiatku si koncom mája 2008 nespomenul na Slovensku asi nikto. Tak som sa rozhodol napísať pár riadkov ja.
                 

                 JUBILANT   John Henry Bonham, prezývaný Bonzo, sa narodil 31. mája 1948 v anglickom meste Redditch, grófstvo Worcestershire. Hre na bicie nástroje sa začal venovať už v detstve, ako päťročný si postavil „sadu“ z plechoviek od kávy a napodobňoval svoje vtedajšie idoly – Geneho Krupu a Buddyho Ritcha. Keď mal desať rokov, jeho matka (Boh jej žehnaj za to rozhodnutie) mu kúpila jeho prvý malý bubon (alebo, ak chcete „rytmičák“,prípadne „šroťák“,eventuálne „virbel“). V pätnástich už mlátil do svojej prvej ozajstnej sady značky Premier. Mimochodom, solídne hudobné vzdelanie sa mu vyhlo, ale pri jeho talente to nemožno považovať za handycap. V detstve a mladosti neprejavoval veľký záujem o štúdium. Na jednom z jeho prvých školských vysvedčení jeho pedagóg formuloval hodnotenie: „Skončí buď ako zametač, alebo ako milionár“. Veľmi trefné a jasnozrivé. Našťastie pre Bonhama, správne bolo „B“. Po ukončení štúdia na Wiltan House public school pracoval v otcovej stavebnej firme ako robotník, popri tom hrával s rôznymi miestnymi kapelami (Terry Webb and the Spiders, The Nicky James Movement, The Blue Star Trio, The Senators, A Way of Life, Crawling King Snakes so spevákom Robertom Plantom, The Nicky James Movement, Steve Brett and The Mavericks, Band of Joy - opäť s Plantom). Od roku 1968 sa živil hrou na bicie ako profík, ale spočiatku to nebol ktovieako výnosný biznis. Hral v doprovodnej skupine amerického speváka menom Tim Rose, ale jeho kredit stúpal a „poškuľovali“ po ňom aj Joe Cocker a Chris Farlowe. Keď Robert Plant a Jimmy Page hľadali bubeníka do novovznikajúcej kapely, postavenej na základoch rozpadnutých Yardbirds, Plant si spomenul na excelentného hromotĺka, s ktorým sa v minulosti stretol v kapele Band of Joy. Bol to John Bonham.   KAPELA   Bonham nápadom vôbec nebol nadšený. Honorár 40 libier týždenne za účinkovanie v kapele Tima Rosea mu pripadal v porovnaní s ponukou od Pagea a Planta ako onen známy „vrabec v hrsti“. Úlohy „uloviť“ Bonhama sa ujal nový manažér kapely Peter Grant, bezmála dvojmetrový valibuk, ktorý nerozumel slovnému spojeniu „nedá sa“. Page a Grant nakoniec Bonhama dostrkali k rozhodnutiu prijať ponuku na spoluprácu a vznik kapely bol na dosah ruky. Post basgitaristu obsadil výnimočne talentovaný muzikant John Paul Jones. Hneď prvá skúška v malom londýnskom štúdiu na Gerrard Street naznačila obrovský potenciál zoskupenia. Repertoár vznikol krátkom čase a kapela vyrazila na prvé krátke turné po Škandinávii pod názvom New Yardbirds. Krátko nato sa rozhodli zmeniť si názov na Led Zeppelin. Rozpisovať podrobnú históriu Led Zeppelin by bolo asi zbytočné. Bola to kráľovská jazda štyroch rockerov, ktorí sa stali ikonami hardrocku a hviezdami prvej svetovej veľkosti. Záujemcom odporúčam publikáciu A celebration z pera Dave Lewisa (Champagne avantgarde, Bratislava 1994) alebo Schody do neba: Led Zeppelin bez cenzury autorov Richarda Colea a Richarda Trubo (Volvox globator, Praha 1998). Príbeh skupiny Led Zeppelin je príbehom ako vystrihnutým podľa šablóny „sex, drogy a rock &amp; roll“, pričom v prípade Johna Bonhama zohral svoju rolu okrem drog hlavne alkohol. V lekárskej správe o jeho smrti bolo uvedené, že zomrel na otravu alkoholom po požití štyridsiatich štandardných odmeriek vodky (0,8 litra) v priebehu dvanástich hodín a následným zadusením vlastnými zvratkami v spánku. Stalo sa to 25. septembra 1980. Mal tridsaťdva rokov.   Smrť Johna Bonhama znamenala zánik skupiny Led Zeppelin. Napriek fámam, že na miesto bubeníka v kapele kandidujú takí borci ako Cozy Powell, Carl Palmer, Aynsley Dunbar, Carmine Appice, Barriemore Barlow, Simon Kirke, či Bev Bevan, pokračovanie činnosti bolo pre zvyšok kapely nepredstaviteľné. Bonzova smrť bola nenahraditeľnou stratou. 4. decembra 1980 Led Zeppelin uverejnili tlačové vyhlásenie: „Radi by sme dali na vedomie, že strata nášho vzácneho priateľa a hlboká úcta k jeho rodine spoločne s vedomím nerozlučnej jednoty, ktorú zdieľame s našim manažérom, nás viedli k rozhodnutiu, že nemôžeme ďalej pokračovať“.   DISKOGRAFIA   Led Zeppelin (marec 1969, 6. v britskej hitparáde, v rebríčku 79 týždňov)   Led Zeppelin II (október 1969, 1. v britskej hitparáde, v rebríčku 138 týždňov)   Led Zeppelin III (október 1970, 1. v britskej hitparáde, v rebríčku 40 týždňov)   Four Symbols (november 1971, 1. v britskej hitparáde, v rebríčku 62 týždňov)   Houses of the Holy (marec 1973, 1. v britskej hitparáde, v rebríčku 13 týždňov)   Physical Graffiti (február 1975, 1. v britskej hitparáde, v rebríčku 27 týždňov)   Presence (apríl 1976, 1. v britskej hitparáde, v rebríčku 14 týždňov)   Sondtrack Songs Remains the Same (október 1976, 1. v britskej hitparáde, v rebríčku 15 týždňov)   In Through the Out Door (august 1979, 1. v britskej hitparáde, v rebríčku 16 týždňov)   Coda (november 1982, 4. v britskej hitparáde, v rebríčku 7 týždňov)   albumy, ktoré Bonzo nahral mimo Led Zeppelin:   Lord Sutch and Heavy Friends (1970)   Roy Wood: Keep your Hands on the Wheel (z albumu On the Road Again – 1978)   Paul McCartney a skupina Wings: Back to the Egg       BICIE   Bonzo počas svojej kariéry hral na rôzne sady bicích. Preferoval značku LUDWIG, v rôznych obdobiach experimentoval z rôznymi materiálmi. Začal pri javorovom dreve (s povrchovou úpravou green sparkle, silver sparkle), od roku 1973 skúšal plexi (model Amber Vistalite) a od roku 1977 až do konca svojej kariéry oceľ (model Stainless steel). Na všetky mal „obuté“ blany REMO Black dot a Emperor. Bonzo používal masívny veľký bubon s priemerom 26” , 14 alebo 15” tom, 16” a 18”, resp. 16” a 20” kotle a nenapodobniteľný 6-1/2” hlboký, 14” malý bubon LUDWIG Supra-Phonic. Od roku 1970 používal sadu doplnenú o 36-palcový gong PAISTE, od roku 1972 pridal k sade dva (24“ a 26“) tympany. Na veľký bubon hral pomocou bubnovej šľapky Speed King. Čo sa týka činel, Bonzo používal sadu PAISTE 2002: 15“ Hi-hat Sound edge, 18 a 20“ Medium Crash a 22 alebo 24“ Medium Ride. Nad horným tanierom hajtky zvykol mať vodorovne uchytený „ching ring“, malú tamburínku, ktorá svojim zvukom zdôrazňovala metrum skladby pri zošľapávaní pedálu hajtky. Mimochodom, jeden činel značky PAISTE má Bonzo aj na svojom hrobe, opretý o náhrobný kameň. Zrejme dar anonymného obdivovateľa. Vo viacerých kluboch sú vystavené údajne autentické Bonzove súpravy. Napríklad v Rock Café v Barcellone opatrujú sadu LUDWIG, avšak chaotické rozostavenie bubnov a činel budí dojem, že ich občas pri vysávaní popresúva pani upratovačka, zrejme bez bubeníckej kvalifikácie. Americká firma LUDWIG, kedysi svetový líder vo svojom segmente, oživuje záujem bubeníkov, hrajúcich zeppelínovský revival svojou sadou Vistalite – korpusy bubnov vyrobené z plexi v oranžovej farbe pripomínajú Bonza v jeho najslávnejšom období.   SÓLA   Jedinečnou prezentáciu originálnej a virtuóznej hry boli Bonzove sóla na bicích, pravidelná súčasť koncertných show skupiny Led Zeppelin. Bonzove sólo na bicích bolo spravidla súčasťou skladby Moby Dick z druhého albumu Zepelínov. Táto skladba patrila do koncertného repertoáru od novembra 1969 až do roku 1977. Jej dĺžka kolísala podľa Bonzovej chuti a invencie, spravidla trvala približne dvadsať minút. Krátke inštrumentálne intro skladby prechádzalo do improvizácie na tomoch a kotloch, kedy Bonzo varioval tempo a rytmus skladby a zabával sa vyludzovaním zvukov a rytmických motívov pomocou paličiek i pästí, prípadne dlaní. Jedinečná lahôdka pre odborníkov i laikov. Počas Bonzovho sóla si zvyšok kapely dal „cikpauzu“. V rok 1975 Bonzo doplnil do sóla motív zo skladby Whole Lotta Love, ktorý hral na elektronicky snímaných tympanoch.   ZVUK   Spomínať na Bonza a nepripomenúť jeho priekopnícky sound bicích by bolo trestuhodné. Na druhej strane – spievať ódy na jeho brutálne hromobitie by bolo nosením dreva do lesa. Bonzo prenášal obrovskú silu svojich paží a nôh do svojich bicích a vôbec ich pritom nešetril. Jeho technik, Jeff Ochletree, ktorý absolvoval s Led Zeppelin v sedemdesiatych rokoch dve turné, konštatoval: „Bonzove bicie zneli skvelo na pódiu i v štúdiu. Dôvodov bolo niekoľko: skvelý výkon hráča, správny spôsob umiestnenia mikrofónov a nahrávanie v „živých“ priestoroch – nahrával v štúdiách s vysokým stropom, drevenou podlahou a množstvom akustických odrazových plôch.“   Zvukový majster Eddie Kramer, ktorý často nahrával s Bonzom, mu nevnucoval svoj koncept, ale prijal a rozvinul ten Bonzov. V snahe autenticky zachytiť jeho divoký, razantný štýl, volil optimálny pomer medzi kontaktným a priestorovým snímaním zvuku, pričom umiestnil mikrofóny relatívne ďaleko od bicích, aby dosiahol priestorový, dunivý zvuk. Bonzo používal rezonančnú blanu veľkého bubna bez otvoru, pričom zvuk bol snímaný dvoma mikrofónmi: jedným v strede blany a druhým bližšie k okraju bubna. Osobitou kapitolou je tlmenie (muffling) zvuku bicích. Bonzo takmer úplne vylúčil používanie „dusítok“, tlmiacich rezonanciu blán. Blany na veľkom bubne netlmil vôbec, výnimočne na to používal nastrihaný novinový papier. Jeho technik Jeff Ochletree raz menil blany a zabudol vložiť novinový papier do „kopáka“. Bonzovi sa zvuk bubna páčil a konštatoval, že znie dobre. Jeff poznamenal, že zabudol dať do bubna noviny, načo Bonzo zareagoval tak, že zazrel naň ako diabol a povedal: „Začínaš určovať pravidlá, čo?“   Bez ohľadu na značku a typ bicích, blán, činel, spôsob ladenia a snímania zvuku bicích, rozhodujúcim faktorom bol Bonzo sám. Bol exaktným dôkazom, že skutočný muzikant tvorí svoj, autentický a nezameniteľný sound. Bonzo si ho vzal so sebou do hrobu a všetci jeho napodobovatelia boli a sú málo úspešní. Ale Bonzo napriek tomu, že už takmer tridsať rokov nie je medzi živými, ostal medzi nami vo svojich nahrávkach. Dovolím si parafrázovať komunistických ideológov a pozmeniť ich slogan o Leninovej nesmrteľnosti: vďaka svojmu bubeníckemu kumštu je Bonzo večne živý. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Referendum–dum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Bubeníkove osudy III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Stopárov sprievodca Banskou Bystricou (1. časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Včera som stretol dôchodcu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Otvorený list budúcemu ministrovi pôdohospodárstva
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Gogola
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Gogola
            
         
        petergogola.blog.sme.sk (rss)
         
                                     
     
        Komárňan, domestifikovaný v Banskej Bystrici. Milovník prírody a dobrého vína. Fanúšik rockovej hudby, bubnov, bežiek a Monthyho Pythona.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Banská Bystrica
                        
                     
                                     
                        
                            Ľudia a lesy
                        
                     
                                     
                        
                            Zbrane
                        
                     
                                     
                        
                            Bubny a bubeníci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




