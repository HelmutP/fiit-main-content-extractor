
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alexander JÁRAY
                                        &gt;
                Kde neplatia zákony fyziky
                     
                 ***** Latentná hmotnosť telies. 

        
            
                                    4.3.2010
            o
            18:31
                        (upravené
                2.2.2014
                o
                17:48)
                        |
            Karma článku:
                4.77
            |
            Prečítané 
            1878-krát
                    
         
     
         
             

                 
                    Tu sa musím vyznať pánu Bohu a vám bratia a sestry v Kristu i v atikristu, že prvotným impulzom k mojej opozičnej činnosti (môjho rebelantstva) voči štátom plateným šíriteľom, iba dojmových Newtonových zákonov pohybu hmoty v stave chemických prvkov, (voči fyzikálnym a matematickým grázlom ako aj smradom zo SAV ale aj zo škôl a univerzít SR) bol môj šiesty zmysel, čiže mne vnuknutá metafyzická intuícia, ktorá vo mne deň čo deň vyvolávala  nepríjemný podmienený reflex, vo forme  nutnosti spochybniť obsah tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie.
                 

                           Latentná hmotnosť telies.   Tu sa musím vyznať pánu Bohu a vám bratia a sestry v Kristu i v atikristu, že prvotným impulzom k mojej opozičnej činnosti (môjho rebelantstva) voči štátom plateným šíriteľom, iba dojmových Newtonových zákonov pohybu hmoty v stave chemických prvkov, (voči fyzikálnym a matematickým grázlom ako aj smradom zo SAV, ale aj zo škôl a univerzít SR) bol môj šiesty zmysel, čiže mne vnuknutá metafyzická intuícia, ktorá vo mne deň čo deň, vyvolávala nepríjemný podmienený reflex, vo forme potreby za každú cenu spochybniť obsah tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie.   Pritom mojím prvotným impulzom spochybnenia Zákona akcie a reakcie bolo jeho tvrdenie, že počas dynamickej silovej interakcie, sila akcie je rovnako veľká sile reakcie a to hlavne preto, lebo v tom prípade nepoznal som reálnu odpoveď na otázku, ako a podľa čoho sa dá exaktne zistiť, že ktorá z uvedených dvoch rovnako veľkých síl je iná, ktorá je silou akcie a ktorá sila reakcie.   Moja mentálna neschopnosť odpovedať na túto otázku, vyvolávala vo mne nepríjemný, ponižujúci, ba až chorobný komplex menejcennosti.   Druhým mojím impulzom spochybnenia Zákona akcie a reakcie bolo jeho tvrdenie, že i keď sila akcie je rovnako veľká opačne orientovanej sile reakcie, tie dve sily sa navzájom nezrušia.   Moja mentálna neschopnosť odpovedať aj na túto otázku, vo mne už vyvolávala násobne nepríjemný, ba až neurologický, čiže chronický komplex menejcennosti.   Trápila ma tá skutočnosť, že prečo iba ja sám na tomto svete neviem pochopiť výrok fyzikov, že pri silovej interakcii medzi dvomi telesami sa účinky dvoch rovnako veľkých a opačne orientovaných síl akcie a reakcie navzájom nerušia a to vraj preto, že každá z týchto dvoch síl pôsobí na iné teleso.   Trápila ma tá skutočnosť, že prečo iba ja sám na tomto svete neviem pochopiť výrok fyzikov o tom, že prípade silovej interakcie medzi dvomi telesami nejde o jedinú silovú interakciu, ale ide o dve navzájom izolované, preto úplne rôzne silové interakcie, pričom vraj jedna sila +F12 pôsobí iba na jedno teleso a druhá sila - F21 pôsobí iba na druhé teleso.   No mojím rozhodujúcim impulzom na pokračovanie kontinuálneho procesu spochybňovania zdravého rozumu, autora i šíriteľov tretieho Newtonovho zákona, Zákona akcie a reakcie, bola absolútne primitívna, temer nulová reakcia štátom vydržiavanej vedecko-akademickej obce SR, na moje exaktné argumenty, dokazujúce bludný obsah Zákona akcie a reakcie.   Oslovení relativistickí, štátom vydržiavaní fyzici, na obhajobu tretieho Newtonovho zákona, Zákona akcie a reakcie, vždy uvádzali iba učebnicové texty, ktoré práve preto museli byť v súlade s Newtonovmi závermi, ale na druhej strane arogantne odmietli akceptovať, či aspoň reagovať na moje exaktné i filozofické protiargumenty, ktoré antagonisticky protirečili obsahu tretieho Newtonovho zákona, Zákona akcie a reakcie.   Ja, ako predstaviteľ reálnych, (teda v žiadnom prípade nie relatívnych, nie Newtonových a nie Einsteinových) vied, ale pritom aj veľký ľudomil a verný kresťan, vedený kresťanskou láskou k vedcom nízkym v duchu (ktorým patrí vedecké kráľovstvo, čiže SAV, ako aj školy a univerzity SR) začal som intenzívne hľadať príčinu toho, prečo relativistickí fyzici (niektorí vysoko odborne vyspelí, čiže úplne nabifľovaní učebnými látkami) pudovo ignorujú moje jednoznačne exaktné argumenty, prečo oni sa k ním zásadne nevyjadrujú a prečo sa pridržiavajú výlučne Newtonových osobných dojmov, ktoré on pred viac ako tristo rokmi vyslovil o silových interakciách.   A v zmysle biblického hesla, že ten kto hľadá aj niečo nájde, po viacročnom usilovnom, systematickom a vytrvalom hľadaní, našiel som konečne odpoveď na otázku, prečo sa fyzici celého sveta i SR, pridržiavajú Newtonových dojmov o silových interakciách.   Odpoveď na uvedenú otázku  je opísaná v mojom nasledovnom analytickom rozbore tretieho Newtonovho zákona pohybu,   v analytickom rozbore Zákona akcie a reakcie.      Úvodom predmetného analytického rozboru, uvádzam oficiálne znenie Zákona akcie a reakcie, tak ako ho učitelia fyziky, bezducho nanucujú (tlačia do hláv) žiakom a študentom Slovenskej republiky a ako si ho následne žiaci i študenti SR, musia bezducho nabifľovať.   -   A.) Dva hmotné body na seba pôsobia rovnako dvomi veľkými silami opačného smeru.   F12 = - F21   kde F12 je sila, ktorou pôsobí teleso 1 na teleso 2, a - F21 je zodpovedajúca sila, ktorou pôsobí teleso 2 na teleso 1.   B.) Jedna zo síl sa nazýva sila akcie a druhá sa nazýva sila reakcie.(Aby bolo jasné že sú dve.)   C.) Tento zákon hovorí, že pôsobenie telies je vždy vzájomné, a toto pôsobenie súčasne vzniká a súčasne zaniká.   D.) Pritom účinky síl akcie a reakcie sa navzájom nerušia, pretože každá z týchto síl pôsobí na iné teleso (nejde o rovnováhu síl, kedy sa rušia dve rovnako veľké sily opačného smeru).   E.) Ak pôsobí 1. teleso na druhé teleso, pôsobí aj 2. teleso na prvé teleso.      Každá z uvedených vied, pomocou ktorých sa na školách a univerzitách SR definuje tretí Newtonov zákon, je totálny nezmysel. Následkom čoho, pre odhalenie blunosti predmetného zákona pohybu, stačí podrobiť analýze hociktorú vetu tejto definície.   Ja som sa rozhodol podrobiť analýze iba vetu A. v znení:      „Dva hmotné body na seba pôsobia rovnako veľkými silami opačného smeru.   F12 = - F21   kde F12 je sila, ktorou pôsobí teleso 1 na teleso 2, a - F21 je zodpovedajúca sila, ktorou pôsobí teleso 2 na teleso 1.“      Obsah vety A, je obrazne vyjadrený obrazom (č.1).             Z uvedeného obrazu na prvý pohľad vyplýva, že relativistickí fyzici môžu mať eventuálne aj pravdu a že ja eventuálne môžem hlásať bludy.   Tu mnou použitý výraz: „môžu mať eventuálne aj pravdu“, namiesto výrazu: „majú pravdu“, vyplýva z môjho exaktného argumentu, ktorý je znázornený na priloženom obraze (č.2).            Na obraze (č.2) je znázornená silová interakcia medzi dvomi telesami s rôzne veľkou hybnosťou, pri ktorej teleso s väčšou hybnosťou, (ktoré práve preto je akčné teleso) odovzdáva svoju hybnosť telesu s menšou hybnosťou, (ktoré práve preto je reakčné teleso) počas ich vzájomnej, kontaktnej a jedinej silovej interakcii.   Pritom reakčné teleso na impulz sily akcie nezrýchľuje svoj pohyb v opačnom smere spomaľujúceho sa pohybu akčného telesa, ako sa domnieval I. Newton, Einstein ...., ale že smer spomalenia i zrýchlenia obidvoch telies, počas ich vzájomnej a jedinej kontaktnej silovej interakcie, prebieha v jednom spoločnom (nikdy nie opačnom) smere, presne tak, ako to tvrdím ja.   Práve tento môj exaktný argument, pomocou ktorého ja spochybňujem osobné dojmy Newtona na silové interakcie medzi dvomi telesami, je aj jedinou príčinou toho, že prečo každý člen armády štátom vydržiavanej, relativistickej vedecko akademickej obce SR, musí byť ticho, (musí držať hubu) pričom ja môžem ich nazývať popravde grázlami a vedeckými smradmi, darmožráčmi, alebo akokoľvek ináč.   A to všetko beztrestne, ba dokonca aj pod ochrannou rukou článku 32, Ústavy SR.      V čom konkrétnom sa teda Newton mýlil vo svojom Zákone akcie a reakcie, čo ušlo jeho pozornosti, ako aj pozornosti jeho nasledovníkov?   Odpoveď na danú otázku dáva nasledovný obraz, na ktorom je znázornená silová interakcie, ktorá bola Newtonom i Einsteinom a ktorá je aj súčasnými fyzikmi, katastrofálne mylne interpretovaná.   Obraz (č.3)         Súčasná rozumovo degenerovaná, preto iba relativistická Slovenská i svetová obec fyzikov, túto silovú interakciu opisuje nasledovným, katastrofálne mylným spôsobom:   „Celú zostavu umiestnime na vodorovnú podložku (laboratórny stôl). Pružinu ohneme do tvaru U a zafixujeme slučkou z ražnej nite. Pred voľný koniec napnuté pružiny položíme do žliabku guľu. Prepálením ražnej nite dôjde k uvoľneniu pružiny a následne k odleteniu gule jedným smerom (pozor na zranenia a poškodenia vybavenia učebne) a pohybu vozíka druhým smerom. Podstata javu: Reakčná sila sa prejavuje pohybovými účinkami na vozíček..“   Tento citát som opísal z jednej úspešnej diplomovej práce, nachádzajúcej sa na webe.   Tak isto by uvedenú silovú interakciu opisoval aj I. Newton i Einstein a tak isto ju opisujú aj žijúci učitelia fyziky SR.   Všetci menovaní, takto zobrazenú silovú interakciu považovali a považujú za silovú interakciu medzi dvomi telesami, medzi vozíkom a guľou, ktorá prebieha podľa nasledovnej matematickej rovnice:   +F12 = -F21   Za predpokladu, že hmotnosť vozíka a hmotnosť gule je rovnaká, tak zrýchlenie gule a vozíka v predmetnej silovej interakcii má rovnako veľkú hodnotu a prebieha opačným smerom a to presne tak, ako to vyplýva z tretieho Newtonovho Zákona akcie a reakcie.   Tak potom prečo sú podľa Jeho vedeckej svätosti a Jeho neskonalej skromnosti, ako aj Jeho geniálnej geniality,   GRSc. Alexandera Jozefa JÁRAYa,   žijúci fyzici SR, grázli, a prečo nie užitočný a naslovovzatí odborníci?   Alebo, prečo užitočný a naslovovzatí fyzikálni odborníci SR , podľa ich osobných dojmov s duševne chorým Alexandrom JÁRAYom (ktorý neustále útočí na ich zjavnú debilitu) nezatočia v zmysle zákona a Ústavy SR, raz a navždy???   Odpoveď na tieto dve otázky nachádza sa latentne skrytá v nasledovnej vete, ktorou fyzici opisujú priebeh silovej interakcie zobrazenej na obraze č.3:   „Pružinu ohneme do tvaru U a zafixujeme slučkou z ražnej nite. Pred voľný koniec napnuté pružiny položíme do žliabku guľu. Prepálením ražnej nite dôjde k uvoľneniu pružiny, ako aj k odleteniu gule jedným smerom a pohybu vozíka druhým smerom.“   Z uvedeného textu plynie tá objektívna skutočnosť, že po prepálení ražnej nite dôjde k rozpínaniu pružiny a následkom rozpínajúcej sa pružiny, k odleteniu gule jedným smerom a k pohybu vozíka druhým smerom.   Táto realita je dobre viditeľná aj zrakom každého človeka na zemi.   No to ale v žiadnom prípade ešte neznamená, že v tejto silovej interakcii, medzi sebou interagujú iba dve telesá, čiže iba vozík a guľa, ako si to myslel Newton, Einstein a ako si to myslia všetci štátom vydržiavaní zamestnanci SAV a ako to v rozpore s objektívnou realitou i zdravým rozumom, bezducho učia učitelia fyziky na školách a univerzitách SR. (č.4.)      Fyzikmi celého sveta i fyzikmi SAV nepoznaná objektívna realita je ale taká, že v tejto silovej interakcii, neinteragujú medzi sebou iba dve telesá, vozík a guľa, ale že v tejto silovej interakcii, medzi sebou interagujú tri hmotné telesá a to:   1. vozík s nulovou hybnosťou,   2. guľa s nulovou hybnosťou,   3. a pružina ohnutá do tvaru U   a zafixovaná slučkou z ražnej nite, ktorá má v sebe naakumulovanú potenciálnu energiu.   Potenciálna energia ohnutej pružiny, zafixovaná slučkou z ražnej nite, nemôže sa uvoľniť len preto, že sila jednej vetvy ohnutej pružiny je rovnako veľká ako sila druhej vetvy ohnutej pružiny, pričom obidva (statické) sily ohnutej pružiny pôsobia v opačnom smere a preto navzájom eliminujú svoje dynamické silové účinky, podľa rovnice   +F = I-FI.   Až po prepálení ražnej nite ohňom sviečky, dôjde k uvoľneniu potenciálnej energie pružiny a tým aj k pretransformovaniu pôvodne jedinej statickej rovnice   1.(+F = I-FI), (ktorá fixovala pružinu v nehybnom - statickom stave) na dve dynamické nerovnosti.   2.(+F &gt;I-FI);   1(+Fpružiny &gt; I-FIvozíka) + 1(+Fpružiny &gt; I-FIgule)   a tým pádom potenciálna energia pružiny už môže začať zrýchľovať ako vozík, tak aj guľu v opačných smeroch, v zmysle tých dvoch dynamických rovníc.   V tomto prípade skutočne ide o dve rôzne silové interakcie a to:   1.) o silovou interakciou medzi pravou vetvou pružiny a vozíkom a   1(+Fp. pružiny &gt; I-FI vozíka   2.) o silovou interakciou medzi ľavou vetvou pružiny a guľou.   1(+Fp. pružiny &gt; I-FI gule   Pri tejto, podvojnej silovej interakcii, obidve uvedené telesá sa zrýchľujú, na úkor úbytku potenciálnej energie tretieho telesa, na úkor úbytku potenciálnej energie pružiny, na úkor jej dynamického rozpínania sa.   V prípade, že ohnutá pružina by nebola pevne pripevnená k vozíku, ako je to v zobrazené na obraze č.3, výsledkom uvedenej silovej interakcie by bol zrýchlený pohyb gule a vozíka v opačnom smere, pričom pružina by ostala ležať nehybne v priestore medzi vzďaľujúcim sa vozíkom a vzďaľujúcou sa guľou.č.5.            Z uvedeného prípadu jednoznačne vyplýva, že na obraze č.3, nie je znázornená silová interakcia medzi dvomi telesami, prebiehajúca podľa jedinej mylnej (presnejšie debilnéj) Newtonovej rovnici:   1.(+F = -F),   ale že v tomto prípade prebieha silová interakcia medzi tromi telesami a podľa dvoch reálnych JÁRAYových (dynamických, presnejšie geniálnych) nerovností.   2.(+F &gt;I-FI)   Ani túto objektívne reálnu, (ktorá je aj očami viditeľná) skutočnosť materiálnej prírody v stave chemických prvkov, nie je schopný svojimi očami vidieť a svojim rozumom pochopiť, žiadny štátom vydržiavaný relativistickí fyzik SR ani zo SAV.   Túto objektívne reálnu skutočnosť materiálnej prírody v stave chemických prvkov, je schopný svojimi očami vidieť a svojim rozumom pochopiť, zatiaľ iba jeden človek na zemi a to občan SR,   GRSc. Alexander Jozef JÁRAY.   Pre prípad ktorý je opísaný na obraze č.1, je situácia zdanlivo zložitejšia, lebo tam na prvý a povrchný pohľad, prebieha silová interakcia skutočne iba medzi dvomi osobami (dvomi telesami) pričom sila ktorá priťahuje osoby k sebe (alebo odtlačuje od seba) nie je viditeľne viazaná na separátne tretie materiálne teleso (na žiadnu ohnutú pružinu s naakumulovanou potenciálnou energiou) ale je latentnou (neviditelnou) súčasťou svalov dvoch, vzájomne silovo interagujúcich osôb.   A teraz sme sa dostali k meritu veci, z ktorého pramení zásadný, ba až katastrofálny omyl ľudstva, vo forme bezduchého akceptovania nezmyselného obsahu tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie, ako pravdivého zákona prírody.   Tak ako Newton za svojho života nedokázal identifikovať tretiu silu, tretie separátne pružné teleso pri silovej interakcii medzi vozíkom a guľou, sprostredkovanú rozpínajúcou sa hmotnou pružinou, ako je to znázornené na obraze č.3, tak ju nedokázal identifikovať ani pri vzájomnom priťahovaní a odtláčaní sa dvoch osôb znázornených na obraze č.1.             Dve osoby v predmetnej silovej interakcii na obraze č.1 majú počiatočnú nulovú hybnosť a tak oni nemôžu svojou nulovou hybnosťou, nijako na seba navzájom pôsobiť kontaktnou silovou interakciou.   K silovej interakcii medzi uvedenými osobami s pôvodne nulovou hybnosťou môže dôjsť výlučne iba pomocou tretej sily a to, latentnej, neviditelnéj sily, ktorá je ukrytá vo svaloch rúk jedného, či obidvoch osôb s nulovou hybnosťou.   A až tá tretia neviditeľná, latentná sila, sila ukrytá vo svaloch jedného či obidvoch osôb, môže zmeniť nulovú hybnosť obidvoch osôb znázornených na obraze č. 1.   Latentná potenciálna sila svalov rúk jedného či obidvoch osôb s nulovou hybnosťou, substituuje, nahrádza tú viditeľnú a rozpínajúcu sa (U) pružinu, ktorá je viditeľná pri silovej interakcii, znázornenej na obraze, č.3.   Takže ani silová interakcia znázornená na obraze č.1, nie je silovou interakciou medzi dvomi telesami, ale silovou interakciou medzi tromi telesami.   No a túto reálnu skutočnosť materiálnej prírody, nie je schopný svojimi očami vidieť a svojím rozumom pochopiť ani jeden fyzik na svete, ale ani žiadaný fyzik v SAV.   To preto, že oni nerozmýšľajú, ale iba bezducho opakujú v školách nabifľované Newtonové omyly, skôr keci, ktoré sú iba tradicionalisticky, teda zo zotrvačnosti, pritom neprávom povýšené na pravdy prírody.   Vo veci pravdivosti obsahu tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie, konštruktívne kriticky rozmýšľa iba jeden človek na svete a ten človek sa volá,   GRSc. Alexander Jozef JÁRAY.   Najzávažnejší prírodný objav občana SR, GRSc. Alexandra Jozefa JÁRAYa spočíva v nasledujúcej vete:   „V prírode neexistuje silová interakcia medzi dvomi telesami ale iba medzi tromi telesami.“   Úlohu tretieho telesa (čiže substitúciu rozpínajúcej sa, alebo stáčajúcej sa pružiny) pri silových interakciách medzi dvomi telesami, prezentuje latentná, očami neviditeľná pružnosť elektrónových obalov atómov chemických prvkov, ktoré pri kontaktnej silovej interakcii medzi dvomi (atómovými) telesami s nenulovou hybnosťou, sú schopné absorbovať do seba kinetickú, nárazovú energiu telies, vo forme ich spomalenia sa a následne ju vrátiť telesám späť vo forme zmeny ich rýchlosti, ako aj vo forme zmeny smeru ich pohybu.            Bez tretej, latentnej hmotnosti dvoch telies, (zobrazená oranžovo žltou farbou) vzájomná silová interakcia medzi nimi by nemohla prebehnúť. Momentálnu stratu kinetickej energie akčného telesa, dočasne nakumuluje do seba tretia latentná hmotnosť obidvoch telies. Keďže pružnosť telies je funkciou času, debilné tvrdenie Newtona a relativistických fyzikov, o tom, že sila akcie vzniká naraz so silou akcie odporuje aj Einsteinovej teórii relativity, lebo aj keby pružina sa stáčala rýchlosťou svetla, aj tak by to nemohlo byť naraz, ale na viac raz!!   Preto: „Každé teleso vytvorené z atómov chemických prvkov, nosí v sebe latentne ukryté aj druhé bezhmotné teleso, (latentnú bezhmotnú pružinu) pomocou ktorého dokáže interagovať s iným telesom, tiež vytvoreným z atómov chemických prvkov, pri ich vzájomnej, kontaktnej silovej interakcii.   Výnimočnosť, Jeho vedeckej skromnosti a Jeho vedeckej geniality,   GRSc. Alexandra Jozefa JÁRAYa   spočíva v tom, že on dokázal objaviť túto dosial neznámu, ale objektívne existujúcu, druhú, bezhmotnú, preto iba latentnú (očami neviditeľnú) hmotnosť telies, ktorá svoju materiálnu existenciu prejavuje iba počas silových interakcií medzi telesami (iba počas kumulovania, či odovzdávanie kinetickej energie telesám podliehajúcim silovým interakciám) a ktorá po skončení silových interakcii zaniká.   Druhá, latentná hmotnosť telies, v zotrvačných sústavách je neidentifikovateľná.   Vo svetle JÁRAYovho objavu druhej, latentnej hmotnosti telies, rozbor ostaných viet definujúcich obsah tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie, je už irelevantný, už je úplne zbytočný.   Vo svetle JÁRAYovho objavu druhej, latentnej hmotnosti telies, obsah definície tretieho Newtonovho zákona pohybu, Zákona akcie a reakcie, ako celku, je od začiatku do konca iba totálny blud.   A to by bolo všetko.                       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Overte si, či ste múdrym človekom s IQ 201 bodov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Ženský genetický kód!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Experimentálny dôkaz neplatnosti „Všeobecnej teórie relativity“ pohybu matérie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Veritas vincit (Pravda víťazí.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Čas sem, čas tam, nám je to všetko jedno.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alexander JÁRAY
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alexander JÁRAY
            
         
        jaray.blog.sme.sk (rss)
         
                                     
     
        Pravde žijem, krivdu bijem, verne národ svoj milujem. To jediná moja vina a okrem tej žiadna iná.
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    255
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1326
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kvantová matematika
                        
                     
                                     
                        
                            O zločinoch vedcov
                        
                     
                                     
                        
                            Kde neplatia zákony fyziky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Overte si, či ste múdrym človekom s IQ 201 bodov.
                     
                                                         
                       Ženský genetický kód!
                     
                                                         
                       Veritas vincit (Pravda víťazí.)
                     
                                                         
                       Čas sem, čas tam, nám je to všetko jedno.
                     
                                                         
                       Kvíkanie divých svíň.
                     
                                                         
                       Ako nemohol vzniknúť priestor a hmota.
                     
                                                         
                       Odpoveď na otázku z Facebooku.
                     
                                                         
                       Moja dnešná elktornická pošta (26. 2. 2014).
                     
                                                         
                       Kde bolo tam bolo, bola raz jedna „Nežná revolúcia“.
                     
                                                         
                       Zákon zachovania hmoty.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




