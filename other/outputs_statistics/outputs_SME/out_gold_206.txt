

     Horúce leto 1969. Pete s manželkou sa zabávali na terase svojho domu s talianskou novinárkou Orianou Fallaci. Oriana tvrdila zarputilo, že slová, ktoré povedal Neil Armstrong pri vstupe na mesačnú pôdu, mu boli nariadené z centrály NASA. Bola taká, neznášala byrokratov. 
 
 
   Pete sa snažil ju presvedčiť. Aj keď nemohol na 100% tvrdiť, že tá veta skrsla v Neilovej hlave, bol presvedčený o tom, že to bol Neil, kto sa rozhodol, či ju povie, alebo nie. 
 
 
    Fallaci neverila a tak vymyslel spôsob, ako jej to dokázať. Povedal jej dopredu, čo povie pri vstupe na mesačný povrch. Neverila, že mu to dovolia. A tak sa s ňou stavil o 500 dolárov. 
 
 
 
Pete Conrad zostupuje na povrch 
 
 
    Keď Pete dosiahol posledný stupienok na rebríku, pripevnenom na nohe LM Intrepid, odrazil sa a s jemným dopadom pristál na povrchu Mesiaca: „Whoopie! Man, that may have been a small one for Neil, but it's a long one for me!" (Možno to bol malý krok pre Neila, ale je to dlhý skok pre mňa!). Pete vyhral stávku. Pre spravodlivosť treba ešte povedať, že výhru nikdy nedostal. 
 
 
    Vyjadrenie, snímané po prvý krát aj farebnou televíznou kamerou si vypočulo oveľa menej ľudí, ako to Neilove, predsa len, bol to už tretí muž na povrchu Mesiaca a záujem sa nedal porovnať s prvým pristátím. 
 
 
    Pete najprv robil presne to isté, čo Neil a Buzz, testoval terén, jeho nosnosť a svoju schopnosť pohybu. Nasledoval odber núdzových vzoriek a fotografovanie. Pete sa tešil, že sa neobjavili žiadne ťažkosti s pohybom po povrchu, naopak, práca mu išla výborne od ruky. O chvíľu počuli všetci, ktorí sledovali vysielanie z Mesiaca zvláštny zvuk - tlmené pohmkávanie, občas prerušené chichotom. Mohlo by sa zdať, že Pete mal nejaký záchvat eufórie, alebo mu, jemne povedané preskočilo. Ale tí, čo ho poznali vedeli, že je to len prejav absolútnej pohody - Pete bol v dokonalej pohode a užíval si pobyt na povrchu plnými dúškami. A pritom pracoval ako drak, neznášal, keď sa práca zdržovala, mal v pláne držať sa presne kontrolného zoznamu (checklistu), pripevneného na svojom ľavom rukáve tak, aby sa v ňom dalo listovať. 
 
 
    Tieto checklisty boli pre úspech EVA nepostrádateľné, bol to jeden z prvkov misie, na ktorom strávil najviac času práve člen posádky s najväčším zmyslom pre detail - Al Bean. 
 
 
    Pri EVA na povrchu, ktorá mala trvať 3,5 - 4 hodiny nebolo možné, aby astronauti ovládali všetky postupy naspamäť. Navyše mali byť tieto prechádzky dve. Množstvo úloh muselo byť zoradené tak, aby bol drahocenný čas na mesačnom povrchu využitý čo najúčelnejšie. Veď hrubé odhady hovorili o tom, že minúta na povrchu Mesiaca stála amerických daňových poplatníkov približne milión dolárov. Preto sa checklistom venovala veľká pozornosť a obsahovali okrem plánu činnosti aj havarijné postupy pre prípad zlyhania niektorých častí skafandra, či ostatnej výstroje na mesačnom povrchu.  
Ešte sa k nim, z celkom príjemného dôvodu, vrátime. 
 
 
    Po 20 minútach sa k Petovi pripojil aj Al Bean. 
 
 
  
Al Bean sa pridáva k Pete Conradovi na povrchu Mesiaca 
 
 
    Po pár minútach zoznamovania sa s pohybom na povrchu mal Al prvú úlohu - premiestniť televíznu kameru z miesta, z ktorého snímala Conradov výstup, na statív a ďalej od LM tak, aby diváci mohli sledovať prácu astronautov na povrchu. A tu sa Beanovi stala nehoda, ktorá ho ešte dlho po návrate z Mesiaca mrzela. Napriek tomu, že sa nič dramatického nestalo. 
 
 
    Ako už bolo povedané na začiatku, Al nemal príležitosť zoznámiť sa farebnou televíznou kamerou, ktorá bola inštalovaná na LM Intrepid. Vlastne ju mal v rukách po prvý krát, doteraz cvičil len s kusom dreva na trojnožke. A to bolo zrejme príčinou, že si vôbec neuvedomil, že pri premiestňovaní kamery obrátil jej objektív priamo do ničím nefiltrovaného slnečného kotúča. Snímacia Vidiconová elektrónka sa doslova upražila a okrem niekoľkých horných riadkov, ktoré slnečné svetlo nezasiahlo, prestala snímať obraz. Automatika expozície dokončila dielo skazy, keďže vyše 4/5 obrazu bolo čiernych, automaticky zosvetlila aj ten malý kúsok obrazu, ktorý Vidicon ešte snímal, na žiarivý biely pruh. 
 
 
    Al samozrejme netušil, že závada je definitívna a na Mesiaci neopraviteľná. Kontroloval káble, točil s nastavením objektívu, dokonca po kamere klepal aj svojim geologickým kladivkom. Nemohlo to pomôcť a ani nepomohlo. Televízny prenos z druhého pristátia ľudí na Mesiaci sa práve definitívne skončil. 
 
 
    Prvou vážnejšou úlohou prvej EVA bolo rozloženie a spustenie prvej skutočnej vedeckej stanice na povrchu Mesiaca - ALSEP (Advanced Lunar Scientific Experiment Package - zostava pokročilých lunárnych vedeckých experimentov). Táto zostava bola pôvodne plánovaná už pre misiu Apolla 11, po rozhodnutí o zrušení druhej EVA na Apolle 11 by na jej rozloženie a spustenie nezostalo dosť času. Preto sa prvá zostava ALSEP dostala na program až počas prvej EVA Apolla 12. 
 
 
    ALSEP sa skladá z viacerých experimentov a základnej stanice, ktorá poskytuje všetkým experimentom energiu a vysiela výsledky meraní späť na Zem. Energia sa získava z malého plutóniového generátora, ktorý sa plní palivom až na povrchu a ktorý celú zostavu robí nezávislou od slnečného svetla a súčasne poskytuje dostatok energie po dostatočne dlhú dobu (1 rok). Samovoľný rozpad Plutónia238 vytvára teplo, ktoré premieňajú termoelektrické články na elektrickú energiu s výkonom cca. 63 Wattov. Toto množstvo stačí pre všetky experimenty, ako aj pre vysielanie výsledkov meraní späť na Zem. 
 
 
 
Zostava ALSEP na povrchu Mesiaca. Úplne vľavo je LID, napravo od neho LSM,  
nasleduje centrálna stanica, spektrometer slnečného vetra a úplne vpravo je PSE 
 
 
    Lunárny povrchový magnetometer (Lunar surface magnetometer - LSM), zložený z troch nezávislých senzorov magnetického poľa slúži na zisťovanie magnetického poľa Mesiaca, vyvolávaného slnečným vetrom. Na základe informácií o jeho veľkosti a smerovaní sa bude dať ľahšie určiť zloženie Mesiaca. 
 
 
    Lunárny detektor ionosféry (Lunar Ionosphere Detector - LID) slúži na štúdium nabitých častíc v lunárnej atmosfére. V spojení s Lunárnym detektorom atmosféry (Lunar Atmosphere Detector - LAD) sa tak sledujú nabité, aj nenabité častice mesačnej, neuveriteľne riedkej atmosféry. 
 
 
    Pasívny seizmický experiment (Passive Seismic Experiment - PSE) má, rovnako ako LSM slúžiť pre zisťovanie vnútornej štruktúry Mesiaca, tento krát na základe merania seizmických vĺn. Hlavným zdrojom týchto vĺn majú byť jednak vzostupové stupne LM po ich odhodení na obežnej dráhe a od misie Apolla 13 aj tretie stupne Saturnu V, ktoré budú po odpojení a vytiahnutí LM z adaptéra riadeným vypúšťaním paliva navedené na takú dráhu, aby dopadli na povrch Mesiaca. 
 
 
    Posledným prístrojom v sade ALSEP je detektor prachu (Dust detector - DD), ktorý má merať množstvo prachu, usádzajúceho sa aj na ostatných experimentoch, aby bolo možné odhadnúť, nakoľko sú ich dáta presné a neovplyvnené prachovu pokrývkou. 
 
 
    Do zostavy ALSEP nepatril výskum slnečného vetra, ktorý sa skladal z dvoch častí. Jedna časť - vak pre zachytávanie častíc slnečného vetra sa inštaloval v blízkosti LM, pretože ho astronauti brali so sebou späť na Zem, druhá, spektrometer slnečného vetra zostáva na Mesiaci a bola tiež napájaná z centrálnej stanice ALSEP. 
 
 
    Rozloženie celej zostavy ALSEP zaberie astronautom takmer hodinu času. Nie je to namáhavá práca, má ale aj málo spoločné s nejakou tvorivou a výskumnou činnosťou. Všetko, čo musia astronauti urobiť, je rozložiť prístroje, dať ich do prevádzkyschopného stavu, zrovnať ich orientáciu s ohľadom na Slnko (slúžia na to matice a šablóny, ktoré sa musia kryť s tieňom, vrhaným prístrojom), prepojiť s centrálnou stanicou a túto zapnúť. Všetko samozrejme podľa cheklistu. 
 
 
    O finálnu verziu checklistov sa starala záložná posádka - Dave Scott a Jim Irwin, ktorá sa rozhodla spestriť ich kresbičkami Snoopy-astronautov (podľa postavičky populárneho comic-stripu Charlesa Schulza - Peanuts). Ilustrácie si môžete pozrieť na nižšie uvedených obrázkoch. Niekedy spôsobovali značný chichot, rušiaci komunikáciu medzi Hustonom a mesačným povrchom. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  
Narážka na rivalitu medzi príslušníkmi US Navy a US Airforce medzi astronautmi.  
Hlavná posádka Apolla 12 bola celá z US Navy, záložná zas kompletne z US Airforce. 
 
 
Najväčší smiech ale vyvolali špeciálne prílohy checklistov, doplnené Scottom a Irwinom na poslednú chvíľu tak, že o nich Conrad a Bean nevedeli. Veď posúďte sami: 
 
 
 
"Videl si nejaké zujímavé kopčeky a údolia?" 
 
 
 
"Nezabudni, popíš nerovnosti" 
 
 
 
"Sleduj - jej aktivity" 
 
 
    Samozrejme sa o tom nedalo hovoriť priamo na Mesiaci, kedy počúvalo komunikáciu mnoho tisícov ľudí po celej Zemeguli. Trochu nevysvetliteľného smiechu síce ľudí zarazilo, ale skutočná pravda o jeho príčine bola zverejnená až po návrate z Mesiaca. 
 
 
    Prvá EVA skončila zberom geologických vzoriek z okolia LM, na jej konci urobil Al Bean ešte jednu symbolickú vec - z vrecka na skafandri vybral svoj strieborný odznak astronauta - nováčika, odznak, ktorý nosil dlhých šesť rokov. Keď sa vráti z Mesiaca, dostane rovnaký odznak, ale zo zlata - odznak vesmírneho veterána. A tak sa napriahol a hodil svoj strieborný odznak čo najďalej do Oceánu búrok. 
 
 
    Al Bean nespal na Mesiaci dobre. Na vine bola jeho povaha, odlišná od chladnokrvných skúšobných pilotov. Nebál sa, to určite nie, len jeho pozornosť a opatrnosť boli v stave pohotovosti. Hneď na začiatku doby spánku ich oboch prebral hukot čerpadla chladiaceho systému, ktoré sa náhle zaplo. Kabína LM vôbec nebola pre spánok ktovieako pohodlná. Cez clony na oknách presvitalo ostré slnečné svetlo, klimatizačný systém hučal tiež celkom obstojne a spať v skafandri, aj keď bez prilby a rukavíc tiež nebol práve najlepší nápad. Ale Al aj Pete na tom trvali, mali obavu, aby im mimoriadne abrazívny mesačný prach nepoškodil niektorý zo spojov na skafandroch, čo by im znemožnilo hermeticky sa v nich uzavrieť. Prišli by tak o druhú, z ich pohľadu oveľa dôležitejšu a zaujímavejšiu EVA. 
 
 
    Pete sa tiež veľmi nevyspal. Príčinou neboli obavy zo zlyhania niektorého zo systémov LM, ako u Ala. Príčina bola veľmi prozaická - kruh, do ktorého sa pripájala prilba sa mu zarezával do pleca, pretože na jeho skafandri bola zle nastavená dĺžka pravej nohavice. Dĺžka sa nastavovala sadou laniek, ktoré boli zašnurované okolo lýtka ako obväz. Pete musel zobudiť Ala a požiadať ho, aby všetky lanká uvoľnil, mierne povolil a znovu poriadne zašnuroval, veď nikto nechcel, aby sa povolili počas práce na povrchu. Celá táto operácia trvala asi hodinu a keď skončili, začali sa pripravovať na svoju druhú a súčasne poslednú prechádzku. Prechádzku, ktorá viedla k sonde, 31 mesiacov čakajúcej na túto návštevu - Surveyor 3. 
 
 
    Surveyor 3 bol druhou sondou série Surveyor, ktorá dosiahla svoj cieľ - mäkké pristátie na mesačnom povrchu. Z štartovacieho komplexu 36B na Kennedyho myse odštartoval v špici rakety Atlas-Centaur 17. apríla 1967. 20. apríla 1967, po dvoch poskokoch, kedy sa nevypol pristávací motor, na tretí pokus Surveyor definitívne zakotvil v Oceáne búrok. Základnou misiou Surveyora bolo testovanie vlastnstí povrchu Mesiaca, jeho únosnosti, aby sa zistilo, či sa na Mesiaci vôbec dá pristávať. Tomu slúžilo špeciálne rameno s násadou, podobnou malému bagru, ktoré vyrylo do povrchu niekoľko rýh, všetko pod kontrolou televíznej kamery, ktorej signál sa prenášal cez vysokorýchlostnú anténu na Zem. 
 
 
 
Pohľad na Surveyor 3 zo smeru od LM Intrepid 
 
 
    A práve táto televízna kamera, spolu s časťami kabeláže a časťami konštrukcie sondy mali byť hlavným úlovkom Conrada a Beana počas ich druhej EVA. 
 
 
    Ešte včera vyzeral kráter Surveyor veľmi neprístupne. Nízko visiace Slnko vrhalo ostré tiene a stena krátera sa zdala byť na pochod príliš strmá. O 13 hodín neskôr sa Slnko na obzore posunulo len o pár stupňov, ale výzor krajiny, v ktorej Al a Pete pristáli, sa jemne zmenil. Najdôležitejšou zmenou bol práve výzor krátera Surveyor. Jeho včera ešte príkre svahy sa za tých pár hodín zmenili na jemný zostup a výstup, pohodlne zvládnuteľný v tuhom skafandri. 
 
 
    Než sa ale astronauti dostali k Surveyoru, čakala ich presne naplánovaná trasa. Tých trás bolo niekoľko a líšili sa najmä tým, kde sa začínali. Intrepid sa nachádzal takmer presne na trase číslo 4, tomu bol potom podriadený program EVA. Pete si pred letom pýtal od geológov mapy a tak dostal fotografie z Lunar Orbitera, s vyznačenými trasami. Teraz sa ukázalo, že to bol výborný nápad. Pete a Al sa pohybovali klasickým mesačným skokom, ktorý má jednu obrovskú výhodu - môžete si počas neho oddýchnuť, keďže letíte podstatne ďalej ako na Zemi. Jediným problémom je, že už pri odraze musíte presne vedieť, kam dopadnete, aby ste nohou neskončili v nejakom malom kráteri, alebo ryhe, či na nejakej nečakanej vyvýšenine. Takýto spôsob pohyu bol zábavný a náročný na pozornosť zároveň, ptreto bol Al Bean fascinovaný tým, ako sa Pete dokázal pri tempe ich skokov nielen orientovať v mape, ale ešte si aj strážiť miesta dopadu a odrazu. 
 
 
    Cieľom EVA bol v prvom rade zber vzoriek, ale nie hocijakých. Krátery na Mesiaci sú ako vrty - každý zásah meteoritu vyvrhne na povrch materiál z hĺbok, čím väčší kráter (a teda väčší meteorit, ktorý ho vytvoril), tým hlbšie vrstvy sa dostali na povrch. Z dynamiky dopadu meteoritov vyplýva, že najhlbšie vyvrhnuté vrstvy by sa mali nachádzať na hranách kráterov, čo najbližšie k samotnému stredu. 
 
 
    Hneď pri prvej zastávke - na severnej hrane 120 metrového krátera, nazvaného Hlava, dostali Pete a Al geológov do vytrženia. Objavili totiž svetlejší prach - prach, ktorý sa sem zrejme dostal pri vzniku krátera Copernicus, vzdialeného 370 kilometrov severne. Tento svetlý prach umožní vedcom po dopravení na Zem určiť dobu, v ktorej vznikol Copernicus, ktorého vznik je dôležitou udalosťou v geologických dejinách Mesiaca. 
 
 
 
Apollo 12 výrazne rozšírilo akčný rádius astronautov. Takto ďaleko od LM sa Neil a Buzz na Apolle 11 nikdy nedostali. Je to presne v zmysle techniky postupných krokov, ktorú NASA v programe Apollo uplatňovala. 
 
 
    Počas ďalšieho presunu zrazu Beanovi zaľahlo v ušiach - jeho skafander je zrejme netesný! Keď preskúmal všetky ukazovatele, zistil, že je všetko v poriadku, ale bral to ako upozornenie, že vesmírne vákuum, ktoré ho na povrchu Mesiaca obklopuje, neodpúšťa žiadne chyby. Až po skončení letu inžinieri usúdili, že zrejme telom niekoľko krát zakryl výpustný kyslíkový ventil skafandra a preto mu tlak v skafandri v skutočnosti o niečo málo stúpol a nie klesol. 
 
 
 
Surveyor 3. Hore sú solárne panely, doprava trčí rameno s "bagrom" na test nosnosti povrchu. 
 
 
    Pred letom sa objavili určité obavy, aby sa Surveyor pri otrasoch, spôsobených návštevou, nezačal súnúť dolu po svahu, na ktorom stál. Preto sa k nemu Conrad s Beanom približovali veľmi opatrne a zboku, po vrstevnici. Prvé čo si všimli bola farba. Surveyor už zďaleka nebol biely, jeho jasná biela farba sa zmenila na svetlohnedú. Po analýze sa zistilo, že je to najmä prachom, ktorý sa na sonde usadil pri jej vlastnom pristátí, ako aj počas pristátia Apolla 12. 
 
 
 
TV kamera Surveyora. Všimnite si pásik zotretého prachu na zrkadle (smeruje od stredu nahor) ktorý utrel Pete prstom. Obraz sa do kamery odrážal práve týmto zrkadlom, ktoré sa za 31 mesiacov pobytu sondy na povrchu pokrylo vrstvou mesačného prachu. 
 
 
    Ako sa ukázalo, obavy zo zosuvu pôdy, či Surveyora boli neopodstatnené a tak si Pete a Al pomocou veľkých nožníc na kov odniesli bohatú korisť: časť kabeláže, časť konštrukcie a najmä to, čo Pete považoval za hlavný úlovok - televíznu kameru Surveyora. Jej jemná elektronika bola niekoľko mesiacov skúmaná na zmeny, vyvolané prostredím, v ktorom sa nachádzala a prekvapila všetkých svojim vynikajúcim stavom. Ostatne ako všetky časti Surveyora 3, ktoré Apollo 12 prinieslo späť na Zem. 
 
 
 
Pete Conrad pri Surveyori. V pozadí Intrepid. 
 
 
    Počas návštevy Surveyora 3 mali Pete a Al v pláne ešte jeden malý žart. Podarilo sa im prepašovať do skafandra malú samospúšť k 70 mm fotoaparátu Hasselblad, ktorý sa používal počas EVA, bez toho, že by o tom okrem jedného technika v KSC niekto vedel. Plánom bolo umiestniť fotoaparát na statív a odfotiť sa spoločne pred Surveyorom. Pete tipoval, že fotka skončí na titulke magazínu Life a určite sa nájde spústa ľudí, ktorá sa začne pýtať, kto to vlastne ten záber urobil? Pete na začiatku EVA vybral samospúšť z vrecka skafandra a vhodil ju do sáčku na vzorky. Keď ale prišlo na lámanie chleba, nedokázal Al v množstve teflonových vreciek so vzorkami samospúšť nájsť. Hovoriť sa nedalo, ich plán by sa prezradil, a mrhať drahocenný čas vyložením všetkých vzoriek a hľadaním samospúšte považoval za trestuhodné. Takže z plánu nakoniec nič nebolo. 
 
 
 
"Turistická" fotografia Ala Beana pri Surveyori 3 aj s LM Intrepid v pozadí. 
 
 
    Keď sa Pete a Al vrátili k LM, uctili si aj pôvodného pilota LM v posádke Apolla 12. Odznak pilota C.C. Williamsa leží na malom kameni neďaleko LM Intrepid pri kráteri Surveyor v Oceáne búrok na Mesiaci. Krátko pred odletom ho tam položil jeho nasledovník v posádke Apolla 12, Alan Bean. C.C. Williams je tiež dôvodom, že v znaku Apolla 12 sú štyri a nie tri hviezdičky. Jedna z tých štyroch je jeho. 
 
 
    Odlet z Mesiaca bol bezproblémový, rovnako, ako spojenie s Yankee Clipper, kde Conrada a Beana čakal tretí kamarát, Dick Gordon. Mal za sebou vyčerpávajúci program fotografického prieskumu a veľmi sa tešil, že má kamarátov späť na palube. Po odpojení vzostupovej časti LM poslúžil Intrepid mesačnej vede ešte posledný krát - riadene bol "zhodený" na povrch Mesiaca, aby pasívny seizmometer mohol zaznamenať šírenie vĺn z jeho dopadu. 
 
 
    Apollo 12 strávilo na obežnej dráhe Mesiaca ešte celý jeden deň, ktorý bol vyplnený najmä fotografovaním potenciálnych miest pristátia ďalších misií - Fra Mauro pre Apollo 13 a krátera Descartes pre Apollo 14. 
 
 
    Traja kamaráti sa po bezproblémovom zážihu SPS vydali na dvojdňovú cestu domov a nastal čas, kedy mali konečne priestor rozmýšľať nad tým, čo zažili, čo urobili a čo dokázali. Pete Conrad si pred letom na Mesiac sľuboval, že ho tento let nesmie zmeniť. Počas návratu si spokojne uvedomil, že sa to naozaj nestalo. Napriek úspechu celej misie, ktorá okrem blesku počas štartu a drobnosti s TV kamerou prebehla bez najmenších problémov, napriek tomu, že dokázal pristáť presne na určené miesto, Pete vedel, že všetko, čo počas letu dosiahol nijako nezmenilo jeho povahu a zmýšľanie. Nekonečný tréning, roky príprav a náročného testovania ho pripravili tak, že samotná cesta na Mesiac nebola oveľa viac, ako len ďalšia vydarená simulácia. Jediné, čo bolo iné, bol samozrejme výhľad a beztiažový stav. Nehovoril o tom s nikým a preto ho prekvapilo, keď mu jeho kamarát Al Bean počas letu späť k Zemi povedal: "Je to ako v tej piesni: To je všetko, čo tam je?" Ukázalo sa, že nebol sám, kto mal podobné myšlienky. 
 
 
    Apollo 12 pristálo 24. novembra 1969 v južnom Pacifiku a po skončení karantény bolo jasné, že táto posádka tiež už spolu nepoletí. Pete síce žartoval, že by NASA vlastne nemusela na Mesiac posielať nikoho iného, že oni traja by na to stačili, postupne si striedajúc pozície v posádke, dobre ale vedel, že sa chce pohnúť ďalej. Otvorene povedal svojim kamarátom, že odchádza tam, odkiaľ vytiahol Beana do svojej posádky, do programu Apollo Applications, pripravujúceho Skylab. Vyzval aj kamarátov, aby sa k nemu pridali. Al Bean nasledoval svojho veliteľa. Pete velil posádke Skylab 2, ktorá túto poškodenú stanicu vlastne zachránila pred úplným znefunkčnením (sám považoval tento let za oveľa väčší úspech, ako let Apolla 12), Al Bean potom velil posádke Skylab 3, ktorá stanovila doteraz nikým nedosiahnutý rekord v počte splnených úloh počas jedinej misie. 
 
 
    Dick Gordon sa ale rozhodol inak. Ako jediný z trojice, ktorý si nesplnil pôvodné prianie prechádzať sa po mesačnom povrchu, rozhodol sa bojovať o post veliteľa misie. Stal sa veliteľom záložnej posádky Apolla 15 a bol nominovaný za veliteľa hlavnej posádky Apolla 18. Apollo 18 ale nikdy nevzlietlo a tak sa Dickov sen nikdy nesplnil. Keď Apollo 18 zrušili, odišiel od NASA a do vesmíru už nikdy neletel. 
 
 
    Cesty astronautov Apolla 12 sa rozišli. To hlavné, čo ich spájalo im ale zostalo. Zostali doživotnými kamarátmi. Stalo sa to, čo hovoril Al Bean: "z Mesiaca som si priniesol to, čo som mal, už keď som tam letel - mojich najlepších kamošov, Pete Conrada a Dicka Gordona." 
 

