
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef April
                                        &gt;
                Politika
                     
                 Hra s voličom 

        
            
                                    22.3.2010
            o
            9:07
                        (upravené
                7.4.2010
                o
                19:33)
                        |
            Karma článku:
                8.48
            |
            Prečítané 
            360-krát
                    
         
     
         
             

                 
                    Po posledných turbulenciách a zemetraseniach v najsilnejšej opozičnej strane, došlo ku ráznym krokom, ktoré by mali očistiť jej meno a pozíciu v Slovenskej politike.Naozaj???
                 

                     Po posledných turbulenciách a zemetraseniach v najsilnejšej opozičnej strane, došlo ku ráznym krokom, ktoré by mali očistiť jej meno a pozíciu v Slovenskej politike.Naozaj???   Zdá sa mi, že si ešte stále pán predseda SDKú myslí o svojich voličoch, že sú „hlúpučký“ a nedokážu čítať „jeho hru“. Zaujal ma výrok pána Dzurindu, keď sa vyjadril, že pokiaľ nová líderka Radičová vo voľbách nezíska minimálne 20 percent voličov ( pričom za jeho vedenia kampane dosahoval strana maximálne hranicu 18 percent) s jej budúcou spoluprácou už nebudú počítať. Byť na jej mieste tak hneď požiadam o verejné ospravedlnenie za takéto vydieračské útoky, no z tohto postoja je zrejmé jej postavenie v celej tejto maškaráde. Je predsa prinajmenšom smiešne dosadiť na čelo strany osobu, ktorá má byť len akýmsi marketingovým ťahom a akousi náplasťou na oči voličov. Podotýkam, že primárky v SDKú vyhrala pani Žitňanská, nakoľko však SDKÚ po nevyriešených a nevysvetlených kauzách horí pod zadkom zvolili si do čela  mediáne známu tvár ktorá je navonok prezentovaná, akoby so starými maniermi strany nemala nič spoločné. Snaha o očistenie strany a o opätovné získanie voličov, ktorí odišli do konkurenčnej SAS, može však výjsť navnivoč, keďže v pozadí stále striehne bývali predseda strany, ktorého sa pred voľbami snažia čé najlepšie zatajiť. Stále však platí, že povolebné rokovania sa konajú na úrovni predsedov strán nie marketingových lídrov. No na unfair praktiky zo strany SDKú sme  si hádam už  zvykli.    Po prečítaní  si rozhovoru ktorý bol uverejnený v jednom nemenovanom pravicovom týždenníku, kde novonastolená vodkyňa pravice prezentovala svoje názory, mi miestami nebolo jasné, či pani Radičová háji záujmy pravice alebo  ľavice. Stále omieľanie frázy sociálny štát, mi skôr evokovalo ľavicovú líderku ešte z čias komunizmu. V jej víziách o budúcom smerovaní politiky sa ukazovali len sociálne programy pre ľudí, prerozdeľovanie verejných financií a zvyšovanie financovania vysokého školstva. No kde ostali stále predhadzované hodnoty pravice, ktoré su nam prezentované ako jedine správne??? Je úbohe siahať po programe ľavice, len z dôvodu vysokých volebných preferencií a dúfať, že im to opäť voliči uveria.  Toto má byť tou údajne  jedinou správnou alternatívou voči súčasnej vláde?   Ale veď Slovensko potrebuje nové riešenia na prekonanie hospodárskej krízy. Najviac  zarážajúcou bola jej odpoveď na otázku redaktora, o prerozdeľovaní verejných financií , kde sa ukázala jej vzdelanostná úroveň. Jediné, čo ju pri tejto téme napadlo bolo školstvo a oblasť sociálnych vecí... t: minor-bidi; mso-ansi-language: SK; mso-fareast-language: EN-US; mso-bidi-language: AR-SA;"&gt;Takéto vyjadrearnie , by za žiadných okolnosti nemala verejne povedať osoba, ktorá by mohla byť  nebodaj našou budúcou premiérkou. ( ale hádam nie je až natoľko naivná, že si myslí že pán Dzurinda bude na dovolenke večne J )   Možno si pán Dzurinda myslel, že dosadením mediálne známej tváre, ktorú verejne podporuje  „ slovenská smotánka“ sa zabudne na jeho kauzy o financovaní jeho strany, či strate 19 miliónov korún z kasy SDKú, no je na omyle a dúfam, že nie som jediný ,ktorý mu neskočí na jeho hru  ...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef April 
                                        
                                            Topiaci sa aj slamky chytá..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef April 
                                        
                                            Teplý vzduch do plachiet opozície
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef April 
                                        
                                            Pravicová ľavica?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef April
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef April
            
         
        jozefapril.blog.sme.sk (rss)
         
                                     
     
        človek ktorý sa snaží vnímať veci aj z inej strany ako je to prezentované mienkotvornými médiami
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1815
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Fandím Čechom
                     
                                                         
                       Niekomu hrozí basa
                     
                                                         
                       Fatálny omyl vedcov o príčine klimatickej zmeny
                     
                                                         
                       Flašíková Beňová: "čo by sa tak mohlo páčiť bulváru, poraďte :-D"
                     
                                                         
                       Polícia sponzoruje súkromné PR aktivity
                     
                                                         
                       Regulačné „výpaľníctvo“ Roberta Fica
                     
                                                         
                       Premiér R.Fico išiel ponúknuť Američanom peniaze z našej  peňaženky
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




