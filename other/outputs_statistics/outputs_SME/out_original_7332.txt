
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Anton Šmotlák
                                        &gt;
                Nezaradené
                     
                 Fico sa bojí! 

        
            
                                    26.5.2010
            o
            13:19
                        (upravené
                26.5.2010
                o
                13:33)
                        |
            Karma článku:
                18.47
            |
            Prečítané 
            3173-krát
                    
         
     
         
             

                 
                    Nachádzame sa na konci volebného obdobia a náš premiér ho končí symbolicky. Tak ako nám vládol celé štyri roky, tak sa s nami mieni aj rozlúčiť (snáď definitívne). 
                 

                 
  
   Jeho vládu sprevádzala triedna nenávisť, miliardové kradnutia v priamom prenose, vytváranie vnútorných a vonkajších nepriateľov, pravidelné zlyhávanie polície,  bezprecedentný rozklad súdnictva a morálny úpadok v spoločnosti. A ako Fico končí? Novými bilbordmi zo strachu z SMK –  „DALI MOC SMK, UROBIA TO ZNOVA! Zastavte koalíciu SDKÚ-KDH-SMK!“   Róbert Fico iste cíti, že sa v spoločnosti niečo deje. Podobne ako v roku 1998, keď sa občianska spoločnosť samovoľne začala politicky angažovať a stále viac a viac prejavovať. Posledné prieskumy ukazujú, že pravica bude mať reálnu možnosť vytvoriť vládu. Do toho prišiel Martin Shooty Šutovec, ktorý za necelý týždeň spontánne od ľudí vyzbieral vyše 1,5 milióna korún. Facebook ovládla opozícia, otvorené štrajky a prejavy občianskej nespokojnosti celú predvolebnú atmosféru len dokresľujú.   Nemám žiadne ilúzie o Csákyho SMK a ich postoj jedinej opozičnej strany ochotnej rokovať so Smerom už len vyvoláva úsmev na tvári terajšej SMK. No držme sa faktov. SMK bola vo vláde 8 rokov a za ten čas sa Slovensko dostalo z čiernej diery Európy (v ktorej bolo zásluhou HZDS, SNS a zakladateľov a privatizérov SMER-u) medzi elitné štáty EÚ a malo povesť stredoeurópskeho tigra. Preto si ani nekladiem otázku, či je väčší problém SMK vo vláde alebo SMER, HZDS či SNS. Odpoveď je jasná.    Verím, že po voľbách bude mať Slovensko opäť pravicovú koalíciu aj s našimi Maďarmi.   Možno Ficovi len krivdím. Chudák, možno musí robiť negatívnu kampaň, lebo po 4 rokoch na čele vlády nemá čo pozitívne po sebe odovzdať a ani povedať.    Good bye, Robo! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anton Šmotlák 
                                        
                                            Poctivý odpočet
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anton Šmotlák 
                                        
                                            Detailisti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anton Šmotlák 
                                        
                                            Lenivci, kšeftári a my ostatní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anton Šmotlák 
                                        
                                            Sex, drogy a Petržalka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Anton Šmotlák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Anton Šmotlák
            
         
        antonsmotlak.blog.sme.sk (rss)
         
                                     
     
        "Nepripodobňujte sa tomuto svetu, ale premeňte sa obnovou zmýšľania, aby ste vedeli rozoznať, čo je Božia vôľa, čo je dobré, milé a dokonalé“
(Rim 12, 2).
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1715
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Anton Šmotlák - moja osobna stránka
                                     
                                                                             
                                            OZ ZICHERKA
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




