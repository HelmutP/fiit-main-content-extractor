
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mikuláš Čikovský
                                        &gt;
                Nezaradené
                     
                 Čo odkryli francúzske elektrošoky? 

        
            
                                    21.3.2010
            o
            14:01
                        (upravené
                21.3.2010
                o
                12:24)
                        |
            Karma článku:
                6.45
            |
            Prečítané 
            1699-krát
                    
         
     
         
             

                 
                    Francúzi vo svojom programe chceli ukázať silu televízie. To, ako sme pod vplyvom nejakej inej vonkajšej autority ochotní nechať sa zmanipulovať (článok v sme.sk). Nezávisle od výsledku pokusu, už samotný pokus televízie, potvrdil to, o čo chceli tvorcovia zabojovať. Televízia má silu.
                 

                 Podobné psychologické pokusy sa už roky nerobia. Rešpektuje sa, že vedecký výskum na katedrách psychológie nemá prednosť pred problematickým priebehom týchto pokusov.   To, čo nie je možné na katedre, je možné v televízii. A naopak: v televízii je to možné len preto, že pokus prebieha pod dohľadom iných vonkajších autorít psychológov.   Pokusné osoby dávali elektrické šoky, lebo sa spoliehali na silu (autoritu) televízie a tá spätne odvoláva na autoritu psychológov, ktorí však už podobné pokusy aj tak nerobia kvôli problematickým priebehom týchto pokusov a výsledok už aj tak dopredu všetci vedia.   Kruh, ktorý vzniká pri ospravedlnení tohto pokusu, odkazuje na to, na čo chceli tvorcovia poukázať - ospravedlnenie prostredníctvom inej vonkajšej autority. Kým televízia obhajuje pokus prostredníctvom vonkajších autorít z vedy, tak vedci zase televíziu využívajú na to, čo si oni sami nemôžu dovoliť: púšťať sa do podobných pokusov na školách. Televízia však áno, taká je jej sila. Sila nie toho, že dokáže zmanipulovať ľudí, to je známe, ale sila, že sa môže do niečoho takého pustiť. Je tu to, na čo chceli tvorcovia poukázať.   V psychológii morálky sa učí teória mravného vývinu Lawrenca Kohlberga, ktorý náš vývin vidí v šiestich štádiách. Od detských štádií, kde dieťa koná podľa toho, za čo dostane odmenu, ktorá je preňho vtedy cieľom, cez štádium, kde sa jedinec podriaďuje zákonu. Správne konanie je také, ktoré je v súlade so zákonom. Zaujímavá je piata a šiesta rovina morálneho vývinu, kde človek robí to, čo sám považuje za správne. Už ani zákon, ani iná vonkajšia autorita neriadi jeho konanie. Naopak vie, že nie každý zákonný predpis je to, za čo sa oplatí padnúť v boji. Niekedy práve boj proti zlému právnemu predpisu stojí za to a je zdôvodniteľný „len" morálne (čo mimochodom neznamená „len", ale „práve preto").   Problém je však v tom, že kým to, čo vnímame pod vágnym pojmom „morálna zdatnosť", sa skrýva skôr v posledných dvoch štádiách, tak väčšina dospelých ľudí sa k nim len hlási, ale ich podľa Kohlbergových výskumov nedosahuje. Ešte raz a s príkladom! Väčšina ľudí nie je schopná konať morálne v situáciách, pri ktorých nemajú vopred naučené jasné pravidlo (napr. Desatoro), podľa ktorého by mali konať.   Ak však inštitúcie ako autority na niečom dlho trvajú, tak aj to najzákladnejšie pravidlo sa dá porušiť. Veď bolo tu obdobie, keď nezabiješ neplatilo na Židov. Dnes nepredstaviteľné, ale v našej blízkej minulosti uskutočnené. Ľudia sa dokážu prispôsobiť tomu, čo je všade okolo nich. Len to treba vedieť podať. Začať tým, že Židia škodia ľuďom v tejto krajine a hoci je ich len niekoľko percent, koľko majetku vlastnia (ako keby ten majetok tým ostatným kradli). komentár L. Krivošíka   To, že ako biologické bytosti máme psychologické schopnosti adaptovať sa, teda zvyknúť si na skoro všetko okolo nás, neznamená, že to máme pripustiť v normatívnej rovine morálky. Naopak. Na zlé veci si nemáme zvykať. To sa dá len tak, že si budeme stále opakovať, že aj keď to a to konanie je všade okolo nás, nie je správne.   Ak sa vrátime ku Kohlbergovmu výskumu, tak počet ľudí, ktorí dosahujú piate a šieste štádium, je približný počtu ľudí, ktorí sa pri tom televíznom pokuse odmietli podriadiť „vyššej" autorite televízie.   Čo ale robiť s tým, že žijeme v spoločnosti, kde kedykoľvek v budúcnosti je ďalšie zabíjanie nejakého národa latentne prístupné?   Filozofka Hannah Arendtová v knihe Původ totalitarismu píše: Až moderná doba so svojou technikou a byrokraciou, kde každý vykonáva len svoju časť úlohy a nevidí dôsledky celého systému, umožnila také rozsiahle ohlúpenie všetkých, za ktorým naopak nikto nevidí svoju vinu. „Veď ja som len konal to, čo by v mojej situácii urobil každý." Tak sa obhajoval Adolf Eichmann pred súdom v Izraeli. Veď on len organizoval ľudí a zháňal vagóny na transporty, čo preňho počas vojny nebola vôbec jednoduchá úloha. Tu je pekne vidieť, ako dôsledky našich konaní už nie sú problémom pre naše (s)vedomie. Holokaust tak podľa Arendtovej nie je škvrnou na našej modernej dobe, ale naopak je v jeho základoch.   Pokus Francúzov ukázal ľuďom to, akí sme, či niektorí ešte budú chcieť počuť to, že ukázal, čo je v našej prirodzenosti. Teraz treba hovoriť o tom, čo robiť, aby sme takí neboli. To je úlohou politiky; nie politikov, ale politiky, ktorá je vo svojej definícii vecou nás všetkých.   Politika nemá byť len bojom ekonómov s odborármi o rozpočtoch štátu a rodín pracujúcich, ale má byť o tom, čo je nám všetkým spoločné. Preto mal J. P. Sartre blízko k socialistom, lebo vedel, že to, o čo ide, nie sú rozpočty rodín či štátu, ale ide o to, čo je „social" - spoločné. A tým spoločným nie sú len peniaze, ale naša morálka. Len marxista môže veriť, že naša morálka ako nadstavba bude závisieť len od peňazí ako materiálnej základne.   Východiskom z problému našej identity tak má byť politika chápaná ako rozprávanie o veciach, ktoré sú nám spoločné. Ak sa o tom budeme rozprávať - bude to v našom vedomí - a časom sa to stane súčasťou nás vo svedomí. Naše názory sú odrazom toho, o čom sa rozprávame, lebo o tom aj väčšinou premýšľame. Takže politika, ale chápaná nielen ako ekonomika.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mikuláš Čikovský 
                                        
                                            Chcú feministky pre mužov rovnaké poistky?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mikuláš Čikovský 
                                        
                                            Signál Brazílčanov s postavením hráčov v ofsajde
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mikuláš Čikovský 
                                        
                                            Priznal sa rozhodcovi, schytal krik a nadávky od svojich
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mikuláš Čikovský 
                                        
                                            Vlastencom z lásky a nikdy inak až na večné časy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mikuláš Čikovský 
                                        
                                            S tankom po Tatrách
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mikuláš Čikovský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mikuláš Čikovský
            
         
        cikovsky.blog.sme.sk (rss)
         
                                     
     
        Takmer vôbec nemyslíme na prítomnosť. Iba budúcnosť je naším cieľom. A teda nežijeme vlastne nikdy, len dúfame, že raz žiť budeme.  (Pascal, Myšlienky)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2053
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Francúzka mala zaplatiť za telefón 12 biliárd eur
                                     
                                                                             
                                            Banka morálne upadla, ide jej len o zisk  Čítajte viac: http://e
                                     
                                                                             
                                            Za šoty 99% dostala Markíza pokutu 100-tisíc
                                     
                                                                             
                                            Druholigový futbalista odmietol úplatok a dostal sa do reprezent
                                     
                                                                             
                                            Spoločnosť ovládajú pijavice
                                     
                                                                             
                                            Gréci šetria silno, Slováci by neuverili
                                     
                                                                             
                                            Rusi hovoria o motore a bordeli
                                     
                                                                             
                                            Uhliarikov kritik skončil aj u Radičovej
                                     
                                                                             
                                            Žena s odvahou
                                     
                                                                             
                                            Lipšicovi ľudia tajili Mokys
                                     
                                                                             
                                            Expolicajt predával falošné vysvedčenia prominentom. Dostal podm
                                     
                                                                             
                                            Odchádza primátorka, ktorá vyvolávala priveľké vášne
                                     
                                                                             
                                            Islanďania volili bežných občanov
                                     
                                                                             
                                            Šéf ochranárov Jasík končí po štyroch dňoch
                                     
                                                                             
                                            Orbán potrestal Ústavný súd, zobral mu časť právomocí
                                     
                                                                             
                                            Ani 99 rán im nestačilo
                                     
                                                                             
                                            Parlament v Kazachstane dal prezidentovi doživotnú imunitu
                                     
                                                                             
                                            Prezidenta Kalmykie navštívili mimozemšťania, teraz má problém
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




