

 Po prvý raz som si naplno uvedomila, čo je to bariéra, keď sa mi narodil syn. Zrazu som sa nemohla dostať na rôzne miesta, ktoré mi boli pred jeho narodením dostupné. Teda – mohla. Ak som chcela vynášať sedemkilový kočík a štvorkilové dieťa cez rôzne schody a iné prekážky. Vtedy som sa začala pozerať na svet očami vozíčkarov. 
   
 Po pár rokoch má moja kamarátka priateľa. Chalan je po havárii na invalidnom vozíku. Chcela som ich pozvať na Vianoce na návštevu. Náš byt je veľkorysý. Zmerala som všetky vchodové dvere, paráda, pohoda. Ako posledné som odmerala dvere na toalete a kúpeľni. Smola. Tam sa Majo nedostane. 
 . 
 Normálne som sa hanbila ... Začala som premýšľať, kam všade sa nedostane ... čo všetko mu je odopreté ... Sisa mi neskôr potvrdila, že veľmi nerád chodí von, pretože všade natrafí na problém. Koho by to bavilo, stresovať sa neustále, či prejde tam, onam, či bude môcť zotrvať na návšteve ... Majo pred nehodou strašne rád cestoval. Snívala som spolu s ním, ako by sa to znova dalo.  Snívali sme nejaký čas, a potom povedal: „Aj keby som sa tam dostal, čo by som tam robil? Všade samé obmedzenie ...Nechajme to tak ...“ 
   
 A ona? Ona je kaderníčka. Marketing jej treba zlepšiť. Strihá aj baby, hoci nápis „Pánske a detské kaderníctvo“ trošku pletie. Vpálila som k nej dnes, už som sa nemohla vidiet v zrkadle. Nenápadné „podloubí“ ústiace k opravovni obuvi na jednej z dvoch hlavných ulíc. Dve kaderníctva. V jednom mali dnešné termíny obsadené. Vedľa stála v útulnom malom priestore krehká blondínka s nádhernými hustými vlnitými vlasmi. Spýtala som sa na termín. Prvé, čo mi povedala, bolo, že je nepočujúca. Ja že „OK a máte voľný termín?“ 
 . 
 Dohodli sme sa. Prišla som načas. Bola milá. Chvíľu som sa potrebovala ladiť na jej reč, ale naozaj iba chvíľu. Vyjasnili sme si predstavu strihu a išlo sa na vec. Po chvíli sa ma spýtala, prečo som prišla práve k nej. 
 . 
 -         „A prečo nie? Mali ste voľný termín, ešte som u Vás nebola, tak prečo nie?“ 
 -         „Pretože nepočujem. Ľudia často prídu a keď zistia, že nepočujem, otočia sa  a odídu.“ 
   
 V tej chvíli som sa začala hanbiť. Mladá žena, ktorá so svojim handicapom dokáže podnikať, pracuje veľa, aby uživila seba a dieťa, pracuje dobre, je milá, má výbornú atmosféru na pracovnom mieste ... Prečo ju tak urážate ?!?! 
 . 
 Hanbila som sa za všetkých, ktorí jej toto urobili. Ktorí nezvládli svoj predsudok, strach z neznámeho, bariéru, za ktorú nemajú odvahu nazrieť. Z čoho máte strach? Nie je nakazená! Je to mladá žena, ktorá sa dokázala postaviť na vlastné nohy napriek ťažším podmienkam! Koľkí z vás to nedokázali! Cítím riadny hnev ... 
 . 
 Je 21.storočie. Ľudia so zdravotným problémom už dávno nesedia schovaní v tmavej komore. To iba my ich do tej komory stále strkáme. Odstraňovať fyzické bariéry je úctyhodné ...ale  bariéry voči ľudskosti ... koľko ich je ... 
 . 
 Prečo jej to robíte? Neviete, ako bolí odmietnutie? Odmietate to, že nepočuje ... a ona vám hovorí, že je kaderníčka a poskytuje dobrú službu ...  
 . 
 DAJTE JEJ ŠANCU. Prosím. 
   
 Venované Janke, ku ktorej budem odteraz chodiť, užívať jej služby a odporúčať ju. 

