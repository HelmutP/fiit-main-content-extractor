
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Kudryová
                                        &gt;
                Súkromné
                     
                 druhý list 

        
            
                                    13.4.2010
            o
            15:18
                        (upravené
                13.4.2010
                o
                15:31)
                        |
            Karma článku:
                4.33
            |
            Prečítané 
            697-krát
                    
         
     
         
             

                 
                    Už sa vytvárajú mračná.. slabe je to slniečko ešte veru. Práve sme prekopali jahody a popretŕhali, aby mohli zdravšie rásť. Teraz sedím na terase a myslím na mamu. Rada tu sedávam, nik ma tu nevidí, príjemne ma hrejú slnečné lúče keď je pekne. Som trochu smutná. Kto vie, ako sa má mama. Tu na slovensku je vždy tak šťastná a plná energie, no tam v čechách je veľmi sama.
                 

                 
východ slnkaevekudry
  Tak veľmi by som jej chcela pomôcť, mať ju pri sebe. Chúďa mama, čaká na otca až jej tu nájde nejaký bytík. Celý život len na niečo čaká ,,čo s ňou bude?,, Prečo ju otec robí tak veľmi nešťastnou? no prečo? Pritom jej stačí naozaj tak málo, po celý život stojí po jeho boku, aj v dobrom aj v zlom, aj v chudobe či bohatstve. Prišla úplne o všetko, aj o jednu z jej dvoch dcér. Prečo nepodporí otec aj ju? veď ona to robí pre neho ustavične. Ah.. pomohla by som jej úplne so všetkým, lenže takto sa musím dívať nato, ako sa má nie veľmi dobre a som úplne bezbranná, pretože jej nemôžem pomôcť. Peniaze, peniaze.. všade sú len peniaze, tie hýbu dnešným svetom. Nenávidím peniaze, pretože tie robia ľudí nešťastnými a naše medziľudské vzťahy zlými. Mi ľudia strácame postupne všetko len aby sme mali peniaze. Je to tak smutné, že sa nepozeráme na svet aj inými očami, že toľkí trpia pre peniaze. Pritom moja mama nepotrebuje veľa ku šťastiu, ju robia aj tie najmenšie veci šťastnou, ako je posedieť si pri rannej kávičke s cigaretou na záhrade. Miluje kvety, hlinu, prírodu. Čo by dala zato, keby mala svoju vlastnú aspoň ako takú záhradku. Myslím si, že to je jeden z momentov, kedy vidím mamu šťastnou.. keď sa kutre v záhrade. Je tu také ticho, dokonca hrdličky hrkútakú a to mi ťa mama pripomína. Pamatám si, ako sme raz sedeli na záhrade u babky (no už je to roky) a ty si len tak zavrela oči a počúvala hrkútanie hrdličiek a povedala si ,,mmm toto ma tak ukludňuje je to krásny zvuk,,  áno je ... a tu na slovensku tu v týchto končinách je ich počuť ustavične, zato tam v čechách nie. ,, Ah mama, tak moc mi tu chýbaš, prosim ťa prídi,, ... ja viem... nemáš kde byť. Veď v dnešnej dobe, aj prácu upratovačky je ťažké nájsť. Je úplne nepochopiteľné koľko hlúpostí a zbytočností ešte aj pre takúto prácu vyžadujú.. žiadosť, živopis, alebo dokoca živnosť? pre Boha ale načo? Načo je toto všetko potrebné? Veď moja mama je už staršia žena, a nič iné nechce ako pracovať bez akých koľvek papierovačiek a zbytočností. Bože ako jej mám pomôct? povedz ako? Veď sama som na tom nie veľmi dobre. Prosim ťa Bože podaj jej pomocnú ruku, je to dobrá žena isto to vieš sám najlepšie.. nenechaj ju takto trpieť, dovol aby sa mala aspoň raz dobre, veď už nieje najmladšia. Daj jej šancu na lepší život tu na zemi ešte po kým žije. Každý deň na ňu myslím.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kudryová 
                                        
                                            Úskostné stavy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kudryová 
                                        
                                            ,,povedz mi, čo počúvaš a ja ti poviem kto si,,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kudryová 
                                        
                                            Naše Nitrianske cesty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kudryová 
                                        
                                            prvý list
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Kudryová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Kudryová
            
         
        kudryova.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    666
                
            
        
     

    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




