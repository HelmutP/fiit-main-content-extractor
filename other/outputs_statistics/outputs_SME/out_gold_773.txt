

 Stránky rôznych časopisov sa hemžia mnohými „zaručenými" receptami na zdravé stravovanie a diétami na chudnutie. Iba máloktoré spôsoby stravovania sú však „evidence-based", čiže založené na experimentálnych dôkazoch o ich prospešnosti pre zdravie a štíhlu líniu. K vedecky podloženým stravovacím metódam však patrí tzv. stredomorská strava. Niekoľko štúdií v minulosti ukázalo jej veľmi pozitívne účinky pre zdravie. Nová štúdia v júlovom vydaní popredného vedeckého časopisu New England Journal of Medicine ukázuje, že stredomorská diéta je pre chudnutie rovnako účinná  ako nízkocukrová diéta a účinnejšia ako nízkotuková diéta. 
 
 
Čo je stredomorská strava? 
 
 
Pred venovaním pozornosti štúdiám si najskôr stručne povedzme, z čoho pozostáva stredomorské stravovanie. 
 
 
  
 
 
Schematicky stredomorskú diétu vyjadruje pyramída, ktorej základňu tvoria potraviny, ktoré sa prijímajú denne: veľa čerstvého ovocia a zeleniny, olivový olej, celozrnné pečivo, ryža, cestoviny, strukoviny, syry, jogurty, denne malé množstvo vína. Na užších vyšších poschodiach môžete v pyramíde vidieť potraviny, ktoré sa jedia párkrát za týždeň: hydina, ryby, vajíčka, sladkosti. Červené mäso sa jedáva zriedka. Doplnkom zdravého stravovania je samozrejme častá fyzická aktivita (napríklad vo forme intervalového tréningu). Slovenská strava teda za stredomorskou zaostáva najmä nedostatkom čerstvého (nie zavareného) ovocia a zeleniny, olivového oleja (ktorý obsahuje tzv. nenasýtené mastné kyseliny) a rýb. Naopak obsahuje prebytok nehydinového mäsa, sladkostí a tukov obsahujúcich tzv. nasýtené mastné kyseliny. Detailný opis stredomorskej diéty nájdete v knihe „Eat, Drink, and Be Healthy" od Waltera Willetta a Patricka Skerretta z Harvardskej univerzity. 
 
 
Ochrana srdca, ciev a mnohé ďalšie blahodarné účinky 
 
 
Je už dlhšie známe, že stredomorská strava pomáha chrániť pred najväčším zabijakom vo vyspelých krajinách, ktorým sú ochorenia srdca a ciev. Má však ochranný účinok aj na rozvoj cukrovky, rakovinových ochorení, Alzheimerovej choroby, chronickej obštrukčnej choroby pľúc alebo na vznik astmy u detí (ak sa pred pôrodom zdravo stravuje ich matka). Nedávne štúdie ukázali, že ľudia, ktorí sa stravujú vo forme stredomorskej diéty žijú dlhšie ako iní Európania. Nadšeným propagátorom stredomorskej diéty je u nás napríklad internista profesor Viliam Bada. 
 
 
Účinná diéta na chudnutie 
 
 
Ak by ste sa radi zbavili nadbytočných kilogramov, iste vás budú zaujímať účinky stredomorskej diéty na hmotnosť. Spomenutá nová štúdia s názvom DIRECT (Dietary Intervention Randomized Controlled Trial, prevedená v Izraeli) zistila, že nízkocukrová a stredomorská diéta (so zníženým príjmom kalórií) sú efektívnymi alternatívami ku klasickej nízkotukovej diéte. Zaujímavé je, že obe mali pozitívnejšie účinky na metabolizmus tukov (zníženie LDL cholesterolu a zvýšenie HDL) a na hladinu cukru v krvi ako nízkotuková strava (pre detaily pozri link pod článkom). 
 
 
 
 
 
Na grafe možno vidieť, že k zníženiu váhy došlo vo všetkých troch skupinách, ale u stredomorskej a nízkocukrovej bolo zníženie väčšie. Nízkocukrová diéta môže byť zo začiatku motivujúcejšia kvôli rýchlejšiemu poklesu váhy, ale z dlhodobého hľadiska vedie k porovnateľnému poklesu hmotnosti aj stredomorská diéta. 
 
Mnohí ľudia si teraz užívajú stredomorské dobroty počas letných dovoleniek. Z medicínskeho hľadiska im možno len vrelo odporúčať, aby v ich konzumácii pokračovali aj po návrate domov. 

