

 
 Trojsyrové cestoviny
 
 
 Čo budeme potrebovať.
 
 
Pol kilogramu očistených fazuľových luskov, 20 dekagramov syru Niva (odporcovia plesnivých syrov môžu nahradiť fetou), 20 dekagramov neúdeného tvrdého syra, jeden syr v črievku Bambino, jednu papriku, cesnak, olivový olej a samozrejme cestoviny.  V tomto recepte použijeme soľ len do vody, v ktorej uvaríme cestoviny. 
 
 

 
 
A čo s tým urobíme  
 
 
Na panvicu dáme trochu olivového oleja a rozohrejeme. Pridáme na tenké prúžky nakrájanú papriku a pražíme, kým sa paprika nerozvonia. Nato nasypeme očistené fazuľové lusky, premiešame, pridáme roztlačený cesnak, podlejeme vodou a dusíme do mäkka. 
 
 
Keď sú lusky mäkké, vydusíme z nich všetku tekutinu a do horúcich nasypeme nastrúhaný syr Niva (Feta) a jeden črievkový syr. Okamžite vypneme pod panvicou plyn a všetko poriadne premiešame. Keďže syry sú dostatočne slané, syrovo luskový maglajz sa nemusí vôbec posoliť. 
 
 

 
 
Uvaríme cestovinu v osolenej vode. Do zapekacej misy potretej olejom dáme polovicu cestovín, zasypeme ich syrovo luskovým maglajzom a na ten príde druhá polovica cestovín. Vrch bohato posypeme tretím syrom nastrúhaným na jemnom strúhadle.  
 
 

 
 
Pečieme v rozohriatej rúre na 200 stupňov, až kým syr na povrchu nevytvorí peknú chrumkavú kôrku.  
 
 

 
 
 
 
 
Cestoviny zapečené s párkom
 
 
Čo budeme potrebovať
 
 
Paradajky, cibuľu, cesnak, paradajkovú šťavu, tvrdý syr, črievkový syr, petržlenovú vňať, olivový olej a samozrejme cestoviny. 
 
 
Dva kusy obyčajných párkov na fotke budete hľadať márne, ako obvykle niečo som musel zabudnúť. Tak si ich tam pekne prikreslite. 
 
 

 
 
Do panvice dáme olej a prisypeme na drobno nakrájanú cibuľku. Orestujeme a pridáme na kocky nakrájanú paradajku a na polkrúžky nakrájaný obyčajný párok. Jemne posolíme, podlejeme paradajkovou šťavou, pridáme cesnak a dusíme kým sa nevydusí prebytočná šťava. Na koniec primiešame nadrobno nakrájanú petržlenovú vňať a odstavíme z plameňa. 
 
 

 
 
V osolenej vode uvaríme cestoviny. Pekáč vymažeme olivovým olejom. Rozprestrieme polovicu cestovín. Rovnomerne zasypeme paradajkovo párkovým maglajzom. Na neho vytlačíme syr z črievka.   
 
 

 
 
Pridáme druhú polovicu cestovín a vrch posypeme jemne nastrúhaným tvrdým syrom.
 
 

 
 
Zapečieme rovnako ako predchádzajúce cestoviny. 
 
 

 
 
Tak dobrú chuť. 
 
 

 
 
 
 

