
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Schultz
                                        &gt;
                basne
                     
                 Keď to nejde, tak to nejde, alebo, "keď sa čerti pobavia" 

        
            
                                    25.2.2010
            o
            9:55
                        (upravené
                25.2.2010
                o
                10:09)
                        |
            Karma článku:
                5.71
            |
            Prečítané 
            1775-krát
                    
         
     
         
             

                 
                    Pracujem v oblasti techniky, v ktorej sa vyskytne veľa "náhod"...
                 

                 Pracujem v oblasti techniky, v ktorej sa vyskytne veľa „náhod“, „naschválov“, a „záhad“. A to aj napriek  tomu, že pristupujeme k práci zodpovedne a cieľavedome. Ľudove povedané, náhoda je blbec, alebo jednoducho sedí tam „žaba na prameni“. Tých prirovnaní by sa našlo veľa a bol by koniec stránky. Sme doslova posledná „inštitúcia“, kde sa  snažíme rozlúsknuť aj najtvrdší oriešok, prečo motor nepracuje.   Jedného dňa sme začali hľadať príčinu, prečo sa zastavil motor vozidla Ford Windstar, 3,0. Schematicky a premeraním bolo preverených cca 90 spojení, všetky čidlá, snímača, napájania, hmotové spojenia, riadiaca jednotka atď. Vystriedali sme niekoľko diagnostických zariadení a neustále  sporadicky chýbal signál otáčok motora. Osciloskop nezachytil markantné chyby. Signál bol , o chvíľu nebol a tak dookola. Bolo to nepochopiteľné. Kontrolovalo sa znova   Vozidlo sa krátko štartovalo snáď aj päťdesiat -krát  a nič. Až „keď sa čerti pohrali“, zostalo ozubené koleso snímača otáčok stáť práve tak, v takej polohe, že sme uvideli miesto, kde bol „vylomený „ zub. Koleso má svoje miesto, kde je medzi zubami medzera, ale toto bolo naviac.   Podobne sme sa „vytešili“ s motorom Ford Focus ,taxislužby, kde motor vypol pri prejazde pravotočivou zátačkou a nebolo to výpadkom nedostatku paliva. Keďže  záležalo od  odstredivej sily v zatáčke, ako rýchlo motor vypol, hľadali sme všetko čo sa môže vôbec touto silou posunúť a motor zastaviť. Ak bola zatáčka mierna, alebo malá rýchlosť, vozidlo fungovalo. Keďže to bolo prevážne mestské  taxi, hľadali sme súvislosti, pretože iné vozidlá s podobným počtom ubehnutých kilometrov tento problém nemali. Urobili sme pokus  pre overenie  hypotézy a ostrú zatáčku sme prebehli s vypnutou spojkou. Motor sa nezastavil.   Sú chvíle v živote, kedy človek akoby mal niekedy s rozvahou ustúpiť pre okolnosťami, alebo náhodami. Akoby akceptovať momenty, ktoré nám chcú niečo naznačiť. Ako naposledy, keď nás zavolali k vozidlu s pohonom na LPG a ktoré na ceste už pár minút „rozťahovali“ a plyn bolo cítiť aj kilometer za nimi. Doslova sme ich vyhnali od  vlastného vozidla. Ale vždy lepšie, jako s nimi o chvíľu „lietať.   Opísal som svoju skromnú skúsenosť a pripájam sa k Zdenke Prednej a súhlasím, že keď to nejde, tak to nejde. Nechajme čertov kým sa pohrajú. Aj ich to predsa časom prejde.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Schultz 
                                        
                                            Človek a pes
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Schultz 
                                        
                                            Pokušenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Schultz 
                                        
                                            Majáčik
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Schultz 
                                        
                                            Biblia Lásky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Schultz 
                                        
                                            Tajchy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Schultz
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Schultz
            
         
        schultz.blog.sme.sk (rss)
         
                                     
     
        Keď žijem, tak milujem. Keď milujem, viem že žijem. -  O LÁSKE PRE LÁSKU S LÁSKOU  /2008/,  O LÁSKE AJ TROCHU INAK /2008/, OZVENY LÁSKY 2009/
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    218
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    486
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            basne
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Svet je veľký ako objatie
                     
                                                         
                       Egolapka (Pasca slabých)
                     
                                                         
                       Odpusť mi
                     
                                                         
                       Pigmentové škvrny
                     
                                                         
                       Človek a pes
                     
                                                         
                       Uväznená jeseň
                     
                                                         
                       Pokušenie
                     
                                                         
                       Majáčik
                     
                                                         
                       Jednosmerná pošta
                     
                                                         
                       Skupenstvá noci
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




