
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kolísek
                                        &gt;
                Súkromné
                     
                 Podprahové signály. 

        
            
                                    1.4.2010
            o
            18:39
                        |
            Karma článku:
                3.79
            |
            Prečítané 
            455-krát
                    
         
     
         
             

                 
                    Už pred viac ako 100 rokmi vyslovil Sigmund Freud názor a ten platí prakticky dodnes, že naša myseľ je rozdelená na časť vedomou, ktorú môžeme ovplyvniť a na časť nevedomú, ktorá je mimo náš vplyv.Činnosť srdca a mnohých ďalších telesných orgánov riadi mozog bez nášho pričinenia a riadia aj naše emócie, stavy, pocity a nálady.
                 

                     Mozog je veľmi zložitý organizmus az prevažnej časti jeho činnosť nedokážeme zatiaľ vysledovať. Údajne využívame len 10% výkonu mozgu a tento názor je možné dokladovať schopnosťami niektorých ľudí, ktorí si dokážu zapamätať treba telefónny zoznam alebo prečítajú knihu o 200 stranách za pol hodiny a potom zopakujú text z pamäte ako by knihu znova čítali. Napriek tomu, že disponujú fenomenálne pamätí, niekedy sa stáva, že nevie ako sa zaviažu topánky. V týchto prípadoch je určitá časť mozgu fenomenálne na úkor inej časti, nie je to však pravidlo. Pri skúmaní mozgov géniov a obyčajných ľudí, nenašli žiadne zásadné fyzikálne rozdiely a napriek tomu výsledok činnosti je diametrálne rozdielny. Sigmund Freud, ktorý pomenoval svetovo preslávenou psychoanalýzu, ktorej základnou myšlienkou je nájdenie nevedomých pudových príčin psychických stavov, ktoré môžu mať vplyv aj na fyzický stav osoby. Svojimi objavmi o vlastnostiach vtedy skôr neznámeho a záhadného nevedomia úplne zmenil takmer všetky oblasti ľudského života.  Podprahové signály, ako už z názvu vyplýva, pôsobí pod prahom nášho vedomia, teda vedomá myseľ nie je uspôsobená s takýmito signály pracovať a sú určené pre naše podvedomie, podľa Freuda, nevedomia.Podprahové vnímanie vychádzajú zo známych porekadiel: "Stokrát opakovaná lož sa stáva pravdou" alebo "Opakovanie je matka múdrosti." Ak sú podprahové vnímanie upravená tak, aby je vedomá myseľ nezaregistrovali, ale na podvedomé úrovní bola akceptovateľná, nastane situácia, kedy tieto oznámenia pôsobí aj na naše emócie, stavy, pocity a nálady, ktoré nie je možné signály v oblasti počuteľnosti alebo viditeľnosti, ovplyvniť. Väčšina mozgových frekvencií je mimo oblasť počuteľnosti teda medzi 0.1-16 Hz a naše uši nenarazia takto nízke frekvencie aj keby v prostredí vznikli. Aký je stav osoby pri určitej frekvencii je merateľný encefalografom EEG, ktorý s pomocou elektród umiestnených na povrchu hlavy sníma tieto frekvencie. Frekvenčné krivky EEG majú charakteristický vzhľad a krivka je pochopiteľne iná pri aktívnej činnosti, ako pri spánku. Ak poznáme aké stavy vyvoláva určité frekvencie a vieme o metóde ako túto nízku frekvenciu mozgu vnútiť, môžeme navodiť situáciu, kedy bude mozog pracovať podľa našich pokynov. Súčasťou ucelenej podprahové audio nahrávky sú teda nielen frekvencie pôsobiace na činnosť mozgu, ale aj podprahové sugestívne oznámenie, zameraná na splnenie konkrétneho cieľa. Podprahové sugestívne oznámení sú principiálne zhodná s hypnotickými oznámeniami, nie je však potrebné žiadne kvalifikovaných osôb v oblasti hypnózy, toto riešenie má rýdzo technické parametre. Závažná je tá skutočnosť, že na rozdiel od hypnózy, kde fungujú vedomej kontrolné mechanizmy, chrániaci pred realizáciou nebezpečných a kriminálnych skutkov, u podprahové sugescie sú všetky kontrolné mechanizmy vyradené z činnosti a naše postupy sa budú riadiť sugestívnymi oznámeniami bez možnosti tieto postupy ovplyvniť. . Meniť nedobrovoľne stavy mozgu je zakázané vo väčšine štátov sveta, bohužiaľ je potrebné povedať, že rovnako sa technológia pre zmenu správania ľudí využívajú a kvôli ich jednoduchosť odhalenie nie je ďaleko tvrdenie, že aj bežná reklama nejaké nevedomé informácie obsahuje.  Ale vedomé ovplyvnenie mozgu jednej konkrétnej osoby zakázané nie je, je teda možné uvažovať o situácii ako mozog stimulovať a zmeniť nielen svoje správanie a postupy, ale aj fyzický stav.       Kolísek /senior/.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Neobvyklá příležitost.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Povinné svícení automobilů a ekologie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Na kolik si ceníte svoje zdraví ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Vytvoření vlastní podprahové audio nahrávky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Zázračné byliny – VII.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kolísek
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kolísek
            
         
        kolisek.blog.sme.sk (rss)
         
                                     
     
        Můj zájem je především zdraví a to jak fyzické tak i psychické. Sestavil jsem unikátní regenerační zařízení a i z vlastní zkušenosti tvrdím, že prokazatelně uzdravuje lidi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    451
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




