

 Pomohol mu v tom vietor, obal bol veľký, z hrubšieho igelitu, skoro ma zhodil. Už sa vznášal nad rozďavenou papuľou kontajnera, no od istého prepadnutia do jeho pažeráka ho zachránila myšlienka. 
 Zľahka mi napadlo, z čoho asi tak ten obal môže pochádzať. Ľahučká otázka, ku ktorej som nedokázal spárovať odpoveď. Nech som obal prevracal, okukoval z každej strany, dokonca som sa ho pokúšal vyformovať do pôvodného stavu, nič nepomáhalo. Neprezradil. 
 Viem, na svete sú dôležitejšie a vážnejšie veci ako nejaký obal. Je tam toho, obalov máme okolo seba ako maku. Obaľujú všetko, na čo si len spomeniete. A práve to ma znepokojuje. Obaly sa stali súčasťou nášho života a my ich ani poriadne neregistrujeme. Možno akurát vtedy, keď sa do nich zadrapneme a zúrivo ich strhávame. 
 Neustále do mňa dobiedzala otázka: Čo je to obal? Vtedy som si uvedomil, že obal je znak vyspelej spoločnosti, odvážne by som ho nazval až synonymom hygieny. 
 Už som nejaký rok na tomto svete a tak si pamätám aj doby, keď sa nákupy balili do novinového papiera, pol kila tresky sme si odnášali v mastnom papieri. Veľmi dobre si pamätám na staré vlhké a tmavé domy na námestí, z ktorých sa šíril odporný smrad. 
 Našťastie sme sa chopili rozumu a vieme, odkiaľ nám hrozí nebezpečenstvo. Rozhodli sme sa dať hygienu na prvé miesto. Takto sme si vytvorili sterilné prostredie, všetko je pekne zabalené: predmety dennej spotreby, predmety dlhodobej spotreby, predmety vášne, záujmu, myšlienok. Viem, že existujú aj hygienické medzery. Tak napríklad deti z pôrodníc sa prenášajú do domácnosti nehygienicky. No to chce iba čas a vhodný obal. 
 Veru, aby sme dlho a zdravo žili, svetu vládne hygiena a čoraz viac obalov. Obaly sa množia a množia. Priznávam, že mi napadla kacírska myšlienka. Čo keď ich jedného dňa bude toľko, až nás zadusia? 
 Zobali vrabce, zobali, igelitové obaly... 
   

