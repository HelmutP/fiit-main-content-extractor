

   
 Avšak ďalšie poradie ľudských potrieb a hodnôt sa v poslednom storočí začalo natoľko miešať a prehadzovať, že ich nemôžeme nazývať stálymi prioritami, no len nestabilnými hobby. 
 Potreba sa stala voľnočasovou aktivitou. Potreba priamej komunikácie s ľuďmi sa postupne vytrácala, až kým nebola nahradená e-mailom, počítačom, mobilom. Teraz častejšie zavoláme alebo zamailujeme priateľom než by sme mali kvôli nim vyjsť von z domu. Každý má totiž viac povinností a nemá toľko času, aby sa mohol „prechádzať", preto sa i vzťahy a priateľstvá už riešia touto formou. 
 Potreba manuálne pracovať (nielen kvôli uživeniu rodiny) bola nahradená činnosťou technických výdobytkov ako práčka, sušička prádla, umývačka riadu a rovnako zmrazené a následne mikrovlnkou zohriate jedlo nám „zamedzilo" možnosť čokoľvek vykonať. 
 Ale je lenivosť číslo dva v rebríčku našich hodnôt? Je tým správnym riešením nechať na techniku všetko to, čo má byť pôvodne úlohou človeka? 
 Ľudia sa už navzájom nepočúvajú ako kedysi, ale dnes i malé dieťa počúva nahlas mp3 prehrávač pri ceste zo školy. Komunikácia medzi ľuďmi už v tejto dobe naozaj nemá miesto? 
 Našťastie, technika je stále na druhom mieste v živote väčšiny a myslím si, že tam ešte veľmi dlho zostane, hlavne vďaka tomu, že v čase krízy materiálna újma vždy prevýši nad interaktívnou. 
   

