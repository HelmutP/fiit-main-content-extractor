
 Nahodím na seba rifle, mikinu, obujem na nohy tenisky a kráčam. Len tak ulicou, s kapsou plnou mojich snov a plánov, nakreslené farbičkami na biely papier, ktorý s očakávaniami pevne držím v rukách. 

Hmm zvláštne. Ako ľudia veria, dúfajú, očakávajú.Vravia si, že ak dosiahnu svoj sen..vtedy budú konečne štastní. 
Vierohodne tvrdia, že ak naplnia soje túžby, už nič nebudú chcieť...v nič nebudú dúfať, veriť. Budú nahromadený spokojnosťou.

Keď to však dosiahnú, spokojosť ich obchádza na míle ďaleko. 
V hlave si tvoria dokonalý plán, ako dosiahnuť niečo lepšie, niečo čo ich " určite " spraví viac šťastným..

Túžime po tom, čo nemáme. Hľadáme, dúfame, veríme v lepšie dni, v lepšie časy,v osoby , ktoré sa nám pripletú do života..
A prestávame si všímať nepatrné maličkosti pozostávajúce z mozaiky momentov..
Z úsmevu, ktorý nám niekto podaruje...
Z pomocnej ruky, ktorú nám niekto podá..
A vieme sa tešiť " len "  z nového auta, či tenisiek čo si kúpime...

Sme presýtení materializmom..a duchovné hodnoty nám utekajú pomedzi prsty..

Čo ak sa raz zobudíme s pocitom, že sa nemáme na koho tešiť ? 
Lebo, šťastie už dávno nebude o pocitoch.. 
