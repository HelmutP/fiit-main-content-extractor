
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magda Kotulová
                                        &gt;
                Nezaradené
                     
                 Vychovávateľ 

        
            
                                    10.3.2010
            o
            15:47
                        |
            Karma článku:
                9.15
            |
            Prečítané 
            1848-krát
                    
         
     
         
             

                 
                    Zmrákalo sa. Večer redol. Nastupovala noc. Spúšťala sa okúňavo do ulíc. Neóny sa najprv rozbreskli, roztancovali, a potom osvetľovali ostro všetko: spiace autá po krajoch ciest, konáre a kmene stromov, dráhu áut, na ktorej sa črtali mátožné biele čiary a hrubé biele šípky.
                 

                 Na rohu ulice vo svite neónu stál aj on. Rukou sa opieral o okrasný stromček na pokraji chodníka. Chvíľočku. Potom sa pokúsil urobiť zopár krokov. Nešlo mu to. Utekal k zemi. Ťahala ho. Priťahovala. Vrátil sa neisto k mladému stromčeku a znova ho objal. Tentoraz však nestál: klátil sa, krútil.   Podišiel k nemu akýsi chlapec: „Neponáhľate sa domov, všakver?“ doberal ho.   „Niééé... ja ne-ne-máám domov...“ koktal cez zuby.   „Nejaký musíte mať...“   „Nemm-ááám...“ pretiahol.   „A kde spávate?“   „Na... nooo... ako príííde...  v po-podnááájme...“ preťahoval.   „Na môj dušu, vy máte poriadne v hlave, čo strýčko?!“ chlapcov hlas zaznel výsmešne.   „Ba veruuu... Akuráát takýýých fafrrnkov učíím, ako si ty... Praaavda mi neverrííš... hik..hik...“ zaštikútal.   „To je sila!... Neverím... Chalani,“ obrátil sa smerom niekde do mĺkvej brány, „poďte sem!... Umriete od smiechu!... Máme tu učiteľa...ha-ha-hi-hi...“   K postave, čo sa knísala ako pri morskej chorobe, podišlo viac holobriadkov.   „Chlaaapci, dooveďte ma na Lippptovskúúú... túú vedľa... zaveeeďte maa...“ prosíkal.   Chlapčiská ho obstúpili.   „Vy ste fakt učiteľ?“ vyzvedal sa jeden. Najvyšší.   „Nieeéé  učiteľ... ani riaditeľľ... ale vycho-vychovávateľ... hik... To je rozdiel môj milýýý, len vycho-chovávateľ... vychoo... hik...“ zapotácal sa a len-len, že nespadol.   Najvyšší ho zachytil: „A prečo potom slopete, nehanbíte sa?“ nepriateľsky zavrčal.   „Nemôôžem za tooo, mileeenký môôj, neemôžem...“ chlácholivo bľabotal ochmelenec.   Chlapec sa odporom zatriasol a pomykal hlavou: „Chalani, lezte domov!“ skríkol na rozrehotaných kamarátov. „Zavediem ho domov, lebo sa ešte niekde zabije!...“   „Ty si faaajn, káámoško... si faajn!“ kolembavý podgurážený mužíček vystrel obe ruky.   Chlapčiská sa chichúňali. Nechápavo sa dívali na priateľa. Dlháň položil ruku opilca      okolo svojho tenkého krku a začal sa s ním terigať ulicou...       V autobuse   Autobus sa hrnul dopredu. Dusno v ňom zasahovalo každého. Pot sa z čiel rinul a zakrádal sa poza uši, do vlasov, na hrdlo, krk. Šofér naraz skrútil prudko volant. Autobus sa zakyvotal. Cestujúci v ňom div nepopadali.   Chlapček s taškou, s veľkým uteráčiskom na vrchu sa tiež zaholengal. Vychýlil sa zo svojho sedadla, zasiahol nohou vedľa stojacu pani.   Pani hnevlivo zagánila.   Chlapča nadvihlo tvár: „Prepáčte, teta, nechcel som!“   Žena ani nemukla.   Vtom chlapča vyskočilo zo sedadla ani podstrelené: „Teta, nech sa páči, sadnite si!...“   Ženou trhlo, červeň jej obliala líca. „O chvíľu vystupujem!“ vychrstla.   Chlapček sa zakabonil. Opäť vhupol na sedadlo. Preplnenú aktovku s uterákom si pritiahol tesnejšie k sebe. Tvárička sa mu zhrčila: prebleskol ňou lúč nepochopenia.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            U kaderníčky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Oprstienkovaná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Daniel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Keď zakape mobil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Registrácia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magda Kotulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magda Kotulová
            
         
        magdakotulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som mama štyroch detí, stará mama pätnástich vnúčat a dvoch pravnúčat. Príležitostná publicistka. Občas sa "niečo" pokúsim napísať, keď ma čosi nahnevá alebo urobí spokojnou. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    318
                
                
                    Celková karma
                    
                                                10.84
                    
                
                
                    Priemerná čítanosť
                    1470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kódex blogera na blog.sme.sk (aktualizovaný 10. 7. 2012)
                     
                                                         
                       Ocko náš
                     
                                                         
                       Príjemné výročia si treba  pripomínať
                     
                                                         
                       Lezúň
                     
                                                         
                       Garancia vrátenia peňazí. To určite!
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Vôňa ženy
                     
                                                         
                       Vodopády Slunj
                     
                                                         
                       Mladý, nervózny vodič a starká
                     
                                                         
                       Nikdy, nikdy, nikdy sa nevzdávaj
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




