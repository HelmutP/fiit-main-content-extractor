




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 Vojna v Afganistane 
 
 Vydan? 15. 5. 2003 o 0:00 Autor: PETRA PROCH?ZKOV? agent?ra Epicentrum pre SME
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (3)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 FOTO ? ARCH?V 
 

 Sovietsky zv?z bojoval v Afganistane takmer 10 rokov. Neuspel a s hanbou musel od?s?. Svoje vojsk? za?al s?ahova? pred 15 rokmi - 15. m?ja 1988. Za?iatok odsunu sovietskych vojsk z Afganistanu nebol poslednou oper?ciou rusko-afganskej vojny. Afganistan je dodnes pre Rusov ?iernou morou. 
 Nejde ani tak o drogy, ktor? cez Rusko pr?dia do Eur?py, ?i o talibov, ktor? sa v poslednom ?ase zase aktivizuj?. Najv??ie starosti robia Moskve vlastn? vojaci, ktor? pre?li afganskou vojnou a vr?tili sa nakoniec domov. 
 Tu sa op? dali do boja. Tentoraz nie proti mud?ahed?nom, ale proti vlastn?mu, nen?viden?mu ?t?tu. Vojnov? veter?ni z Afganistanu s? jednou z najob?vanej??ch skup?n rusk?ho podsvetia. 
 Pre Rusko sa preto afgansk? vojna neskon?ila v momente, ke? vojaci 40. arm?dy za?ali bali? svoje torby a oto?ili p?sy tankov smerom na severoz?pad. Zabudnut? bude a? vtedy, ke? zomrie posledn? ??astn?k bojov. 
 Afganistan zost?va pre Rusov traumou, ?iernou ?kvrnou na vojenskej hist?rii krajiny. ?iernej?ou, ne? bola okup?cia ?eskoslovenska ?i inv?zia do Ma?arska. Afganistan toti? trval pr?li? dlho a k ?plnej okup?cii nikdy nedo?lo. 
 
 
 

 Desa? rokov tu z?rili boje, v ktor?ch zahynulo pod?a r?znych ?dajov od 800 000 do 1 500 000 Afgancov a pod?a ofici?lnych odhadov 15-tis?c rusk?ch vojakov a d?stojn?kov. Vojna st?la Moskvu 6 mili?rd rub?ov a dodnes sa h?adaj? stovky nezvestn?ch. 
 Rusi sa nikdy nezmierili s por?kou, ktor? v Afganistane utrpeli. Na rozdiel od Ameri?anov nedok?zali svoju prehru reflektova? ani v umeleckej forme. Nenakr?tili jedin? celove?ern? film na t?mu afgansk? vojna, len n?znakom podobn? americk?m filmom o traumatickej sk?senosti z Vietnamu. Ani jeden film, z ktor?ho by bolo patrn? z?falstvo nad t?m, ?o to t? na?i vodcovia predviedli. V?etky naopak opisuj? rusk?ch vojakov ako hrdinsk?ch z?chrancov a afgansk?ch partiz?nov, ako bytosti podobn? divej zveri. 
 Dodnes sa rusk? historici sna?ia ospravedlni? vojnu najr?znej??mi v?myslami, dodnes je v Rusku mnoho vojensk?ch expertov i politikov, ktor? by najrad?ej vr?tili ?as nasp? a ujali sa velenia celej oper?cie s v??ou rozhodnos?ou. I ke? z arch?vov je zrejm?, ?e u? nieko?ko mesiacov po vstupe sovietskych vojsk do Afganistanu v Moskve pochopili, ?e urobili zl? rozhodnutie. 
 Od prv?ho ?toku a? po podp?sanie ?enevsk?ch doh?d v apr?li 1988, pod?a ktor?ch sa mali sovietski vojaci 15. m?ja 1988 da? na pochod domov, uplynulo prive?a ?asu. A tak sa v?ro?ie za?atia odsunu nijako neoslavuje. Rad?ej. 
 Cel? akcia trvala 9 mesiacov do 15. febru?ra 1989. St?le ?ij?ci o?it? svedkovia na ?u spom?naj? sk?r s hr?zou v o?iach, ne? ako na koniec vojnov?ho ?a?enia. 
 Velite? oper?cie gener?l Boris Gromov na v??inu ot?zok o udalostiach 15 rokov star?ch odpoved?: ?V?chod, to je tenk? ?ad.? Ako jeden z m?la je schopn? prizna?, ?e na z?falej situ?cii, v ktorej sa dnes Afganistan nach?dza, m? Moskva lev? podiel. 
 Ner?d hovor? o podrobnostiach, napr?klad t?ch, ktor?ch n?sledky je dodnes mo?n? v Afganistane vidie?. V snahe ochr?ni? vracaj?cich sa sovietskych vojakov pred jasaj?cimi mud?ahed?nmi dal Gromov vyp?li? a vybombardova? p?s ?stupovej cesty smerom k hranici Afganistanu s Uzbekistanom. 
 Odchod sovietskej arm?dy a americk? vojensk? i finan?n? podpora susedn?ho Pakistanu, kde sa ako nov? sila proti Rusom zrodil Taliban, za?al ?al?iu trag?diu n?stup extr?mneho islamu. 
 Rusi s? dnes radi, ?e sa pod vplyvom nov?ch vojen a nov?ch dramatick?ch udalost? na tr?pne v?ro?ie, ak?m ur?ite vstup i odchod sovietskych vojsk z Afganistanu v o?iach rusk?ch ob?anov je, zabudlo. 
 Zo spomienok na K?bul dnes v Rusku ?ij? e?te vyvezen? vz?cne koberce v bytoch gener?lov, historick? expon?ty ako telef?nny pr?stroj Rusmi zavra?den?ho afgansk?ho vodcu Chafizulla Amina, tajn? dokumenty ukradnut? zo ?t?tnych afgansk?ch arch?vov a veter?ni. T?, ktor? bud? svojimi ?inmi do konca svojich ?ivotov pripom?na?, ko?ko stoj? jedno chybn? rozhodnutie.	 
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (3)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 21:30  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v utorok st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
KOMENT?R PETRA SCHUTZA  
Len korup?n? ?kand?ly preferencie Smeru nepotopia
 
 Ak chce opoz?cia vo vo?b?ch zvrhn?? Smer, bez roz??renia reperto?ru si neporad?. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 23:50  
Taliban zabil v ?kole v Pakistane 132 det?
 
 Pakistan a? teraz poriadne zatla?il na islamistov. Odplatou je ?tok na deti vojakov. 
 
 
 
 
 




 
 

 
ZAHRANI?IE  
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi
 
 Na demon?tr?cii proti Orb?novej vl?de bolo v utorok u? len nieko?ko tis?c ?ud?. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






