

 Dáždik prší, dáva melanchóliu duši. 
 Všade voda frčí, ľudia sú mokrí ako vodníci. 
 Mnohí sú smutní a nevrlí, ťahajú so sebou aj tých veselých. 
 Zrazu, hurá! Nebo sa rozjasnilo a v očiach sa rozsvietilo. 
 Slniečko prinieslo jas a na duši už nesedí ďas. 
 Všetci sa von vyrojili, úsmevmi svet obohatili. 
 Svieža zeleň a kvietky tiež ožili, aby nám spolu s dobrým vzduchom a slnkom, život spríjemnili. 
 Apríl mesiac šialený, preň sú príznačné takéto premeny. 

