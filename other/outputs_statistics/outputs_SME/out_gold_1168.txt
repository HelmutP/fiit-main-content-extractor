

 
Hoci plynová vojna je každoročným rituálom ukrajinsko - ruských vzťahov už niekoľko sezón, až toht о ročná sa ukázala ako najvážnejšia. Z môjho pohľadu, z dvoch príčin: ekonomickej a politickej. 
 
 
Ekonomická je spojená s cenou na strategicky dôležitú surovinu. Ak si zoberieme, že nedávno bola Ukrajina prinútená požiadať o pomoc MVF, je zjavné, že na Ukrajine  nie je s peniazmi všetko v poriadku. Najskôr ich vôbec niet,  alebo niet ich v tom objeme ako je potrebné pre štát. Tu vzniká tlak na najnižšiu možnú cenu. Rusi sú zas pod iným tlakom, rezký pád cien na trhoch v komoditách ktoré sú najdôležitejšie pre ruský export. Plus rekordný odtok kapitálu . Preto aj pre Rusov je ekonomická otázka veľmi dôležitá a cena dodávok za plyn pre Ukrajinu sa stala kritická. 
 
 
Druhá príčina - politická.  Ukrajina je už dlhodobo v stave politickej nestability a v tejto situácii ekonomickej krízy sa všetko ešte znásobilo.  Pričom aj táto nestabilita má svoje politické pozadie. Kým „vlasťi" (politická moc) krajiny bojujú o túto samotnú „vlasť", ekonomikou sa nemá kto zaoberať.    Bolo prekvapením, že ekonomika Ukrajiny, napriek veľkým problémom a vzhľadom na nejasnú politiku rástla. Pri zhoršení svetovej ekonomiky, hlavne v pre Ukrajinu dôležitej metalurgii, výsledok nedal na seba dlho čakať.   
 
 
  Na Ukrajine sa zastavila veľká časť priemyslu a začala sa ekonomická kríza, ktorá zasiahla všetky oblasti. Toto už aj tak zlú politickú situáciu neuľahčilo, naopak, teraz bolo potrebné rýchlo nájsť vinníka . Hľadanie kompromisov sa stalo nereálne. Politici nie sú dnes schopní nájsť níč zjednocujúce ako v zahraničnej tak i vnútornej politike. A ak ste sledovali jednania okolo plynu už dávnejšie, vždy sa niekto chcel dohodnúť, pričom druhá strana vždy prerušiť. Jedna strana dotiahla zmluvy k podpisu, druhá to stihla prerušiť v poslednej chvíli... 
 
 
Dokonca aj na Slovensku je známa situácia, keď prezident, zaujatý všemožnou pomocou Gruzínsku, v poslednej chvíli odobral premiérke lietadlo na cestu do Moskvy a tá odletela obyčajným spojom.   
 
 
Čiže problém s plynom, o čom sa na Slovensku tak málo hovorí (Ukrajinu niekto tlačí do EU a NATO na zlo Rusku), je aj výsledkom, nestability na Ukrajine, kde neexistuje možnosť dôjsť ku konsensu ani v pre krajinu principiálne dôležitej otázke. Ale netreba zabúdať ani na tvrdú reakciu Ruska, ktorú je čiastočne možné vysvetliť aj ekonomickou situáciou v samotnom Rusku a snahe raz a navždy vyriešiť každoročne opakujúci sa folklór s plynom. 
 
 
 Nesmieme však zabúdať na vplyv emócii . Za tri pokazených „Novych Goda" sa dá jeden druhého naozaj znenávidieť. Je možné, že tohtoročná oslava „Novogo Goda" bola taká ostrá, pretože už vyjednávači boli unavení aj z pohľadu na seba, nie ešte aj schopní čokoľvek vyjednať. Veď za dobu týchto "povinných tančekov" si verejne jeden na druhého toľko povedali... 
 
 
Ale poďme k GAZPROMu. Urobil všetko tak ako mal? 
 
 
Ak Gazprom začal skracovať dodávky plynu, majúc stopercentné potvrdenie, že je Ukrajina zlodej, v tomto zmysle mal na to právo. Každoročne Gazprom vyjednáva aj z Bieloruskom a ostatnými exCCCP krajinami. Ak by dopustil, žeby aj tu, (podľa príkladu Ukrajiny) beztrestne kradli plyn, znova by sa zopakovali 90 roky, kedy všetci plyn dostávali, stál „kopejky" a existovali kolosálne nedoplatky a dlhy. O tejto dobe sa na Slovensku vie málo, alebo zámerne mlčí, tak ako sa zamlčuje pred čitateľmi plno iných reálií spoza východných hraníc. . 
 
 
Dnešní mienkotvorní borci za pravdu bojovali na iných frontoch a tieto krajiny bola iná galaxia, alebo v tej dobe umývali riad nikde v Londýne, alebo maturovali. Ak by Rusko pokračovalo v dodávke plynu, bez toho aby si všímalo „vorovstvo" (krádež), mohlo by to slúžiť ako negatívny príklad pre ostatných „tranzitérov".  
 
 
 Ktorí sme to zažili na vlastnej koži vieme, že každý takýto ústupok má dlhodobé následky.  Aj Rusi si veľmi dobre pamätajú ako sa plyn rozdával, Jelcinská doba sa končila s kolosálnymi dlhmi za plyn, ktorý pre tieto krajiny Rusi dodávali za smiešne ceny. A to bol dosť nepríjemný moment, teraz zosilený tým, že Rusom peniaze treba až príliš. 
 
 
 Dostával som otázky, prečo Rusi najprv nezapojili EU a až potom by zatočili kohútikmi. Veď je pravda, že Ukrajinci teraz ako nikdy sú závislí, od našej mienky (my - ako EU - lebo môj názor na Ukrajinu je príliš radikálny - storočia do jej priatia hoci aj do Európskeho spolku verejných záchodov ....) a EU, keďže u nás začalo byť zima, asi by šla skôr na pomoc GAZPROMu ako Ukrajine. 
 
 
No tak, ako aj v situácii z augusta 2008, kedy sa Rusko obrátilo pred tým, ako vstúpilo do Južného Osetska a  Abcházka na OSN, kde predniesli návrh, aby agresor prestal vraždiť civilné obyvateľstvo, OSN to odmietlo, postavilo sa na stranu agresora (už vidím reakcie ľudí, ktorí sledovali vojnu na Kaukaze cez oči našich mienkotvorcov, bojovníkov za jednostrannú pravdu...), tak aj teraz sa GAZPROM obracal na EU, ešte do momentu vypnutia plynu. No tak isto ako v Gruzínsku, Európa nepochopila čo jej hrozí a urobila to najhoršie - zaujala neutrálnu pozíciu - že je to vnútorná vec URAJINA - RUSKO.   
 
 
Podľa mňa si EU zvykla, že už niekoľko Nových rokov prichádza Východ Európy s tým istým scenárom. Ak sa to stalo prvý krát - nezvyčajné, druhý krát - je to zábavné, tretí krát je to už v ako vo vtipe „tendencia adnaka".  Všetci si mysleli, že všetko je ako zvyčajne. A je možné, že sa zástupcom EU z GAZPROMU nedonieslo, že tentoraz to nie je zvyčajná situácia.   
 
 
No a teraz, téma málo známa na Slovensku aj pre "mienkotvorných" poloprofesionálov (opäť čakám búrlivé reakcie....): Prvým kto vypol plyn bola Ukrajina (tak ako sa zamlčuje, kto začal prvý vraždiť toto leto na Kaukaze).  
 
 
Plyn z Ruska šiel, no za hranice „Nezaležnoi" sa nedostal. Hádam len Slovensko v ten deň dostalo o tretinu menej. 
 
 
Osobne pre mňa je situácia dosť nepochopiteľná, hoci reč ide o Ukrajincoch a tu sa s mojimi skúsenosťami dosť ťažko hľadá vždy hocaký zmysel.   
 
 
Nechápem prečo zavreli Ukrajinci plyn celkom.  Veď Ukrajina nekradla všetko. Aký zmysel bolo nedodávať plyn do Európy. Aký zmysel malo zavrieť rúru ? 
 
 
Veď keď zavreli plynovod smerom von, uzavreli pre seba cestu viesť ďalšie jednania. Kým sa EU zakopávala, ignorovala problém, kým sa do sporu nezapájala, nemalo to zmysel, kľudne by ešte dva - tri týždne potiahli - minimálne. Veď podľa informácie aj z Ruska, kradli nie až také veľké množstvá plynu. Veď si zásoby bez platenia mohli navýšiť potichúčky na viac ako dnešných 12 mesiacov. 
 
 
No nemôžem vynechať aj Rusko: zjavne má v tomto konflikte aj skryté záujmy. 
 
 
Veď kým je aktuálny hocaký konflikt okolo energií, vždy to má vplyv na cenu ropy. Hoci, ako sa ukázalo za tie tri týždne , až tak veľmi jej cena nestúpla. Dnes je však dôležitejšie aby ešte viac nespadla. Veď na konci mesiaca  Urals stál 35 $, a dnes okolo 40 $.   
 
 
+ 5 dolárov  - to zasa nie je tak málo. 
 
 
Ďalší skrytý dôvod je výstavba nových plynovodov okolo Ukrajiny. 
 
 
............................................................................................................................................................  
 
 
Čerpal som výhradne v zdrojoch na Internete s koncovkou .ru (rambler.ru, komersant.ru, utro.ru) napriek tvrdeniu naších mienkotvorcov, že sú prenášači výhradne Putinových myšlienok. 
 
 
A čo si myslia Rusi o budúcnosti Ukrajincov? (Aspoň do podpísania ďaľšieho "Novogodnogo soglašenia"): 
 
 

 
 
 
 
 
Chachol, pamätaj si! 
 
 
Toto je Tvoja budujúcnosť. 
 
 
(budujúcnosť - buduJUŠČee - od priezviska prezidenta - inak budúcnosť -  budučše). 
 
 
Chachol - urážlivý ruský výraz pre Ukrajincov.   
 
 
Darina 14008 - 02 A - 2009 
 
 
Výrobca: Naftogaz Ukrajiny  
 

