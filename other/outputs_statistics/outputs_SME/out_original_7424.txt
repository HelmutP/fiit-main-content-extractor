
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marta Novotná
                                        &gt;
                Poézia
                     
                 Peripetie jednej lásky... 

        
            
                                    28.5.2010
            o
            8:00
                        (upravené
                27.5.2010
                o
                20:44)
                        |
            Karma článku:
                6.54
            |
            Prečítané 
            638-krát
                    
         
     
         
             

                 
                    Smutne sedí, do tmy hľadí. všade ticho, nikde, nikto.
                 

                 
  
   Nesvieti nič. Nevidí nič. Jej oči sú smutné a v nich sú slzy lesklé.   Smutná je, lebo jeho lásku nemá. Smutná je, lebo iná ju dostáva.   Srditý je a veľmi drzí je. Kritizuje a tvrdohlavý hrdina je.   Odišla niekam jeho dobrota. Odišiel jeho krásny, nežný cit. Odišla jeho milota, vyhľadáva samý nepokoj a hic.   Nič nie je dobré. Všetko zle robí. On sám všetko vie a ona ničomu nerozumie.   V spoločnosti ju kritizuje. Ona neverí, že to on, jej milý je. Pri iných vytrvalo laškuje, hrdinom každej udalosti nasilu je.   Háji sa a tvrdí, že iná nie je. Svietiaci a bzučiaci mobil často skrýva a svoje e-maily pod heslom zakrýva. Tá iná sa jej nakoniec, sama ozýva.   Ťažko uveriť. Toľké šťastné roky, tak ľahko zahodiť a krásnu, vernú lásku vymeniť. Ozaj, je to ťažko uveriť.   Háji sa, že to len zábava bola. Ona sa presvedčila, že mal pre inú plno láskavého slova. Vyčerpal nie jej, ale inej, milé, ľúbiace slová.   Pre ňu už pekné slová nezostali. Lebo vraj on také nevie. Len tak hlesne z povzdiali. Lebo on je chlap nesmelý.   Často sedí sama, bez láskavého slova. Pozerá do tmy v ich izbe, do očú sa jej tisnú horké slzy, teskné.   Jej myseľ zostala chorá, potrebuje láskavého slova. Srdce neposlúcha, milotu a lásku nedostáva.   A on, nechce uveriť, že takú chorobu možno len jeho vernou láskou   a láskavým slovom vyliečiť. Bez zbytočných klamstiev, verným byť a sám sebe život nezničiť.   Život je už taký. Niekedy skúša dvojicu ľudí. Jeden deň ti prišlo šťastie, druhý deň si okúsila dusiace sklamanie.   Hovoria jej: “Nezúfaj! Možno príde iný.” Ona však nechce vymeniť. Dúfa, že snáď, sa ten neverec, stále jej milý, navráti.   Bude u nich znova krásny život? On ju presviedča, že bude u nich kvitnúť lásky kvet? Jediná jej podmienka je, že bez tej inej. Presviedča ju, že samozrejme, vraj, nie je tupec.   Nakoniec tomu uverí a zdá sa, skončia šťastne. Možno jej veľkému záletníkovi napadne, že je pre neho na tejto Zemi, to najdrahšie a najlepšie.   Ale ... ktovie? Pochybnosti zostali. Ale ... ktovie? Pocity nedôvery ešte nezmizli. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Spoločná jeseň života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Požiar v susedovej chate
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Deň blbec, alebo ponáhľajúci sa predavač
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Kniha života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marta Novotná 
                                        
                                            Mladý, nervózny vodič a starká
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marta Novotná
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Do mňa sa omáčaj
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Nežná
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marta Novotná
            
         
        martanovotna.blog.sme.sk (rss)
         
                                     
     
         Som optimista, ktorý niekedy odoláva tvrdej realite, niekedy sa vracia do minulosti, detstva a mladosti, ale snažím sa veselo a i vážne, žiť v tomto reálnom svete. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    200
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1438
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vtipné príbehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Automobil
                        
                     
                                     
                        
                            Doprava a dopr. predpisy
                        
                     
                                     
                        
                            Služby
                        
                     
                                     
                        
                            Foto - články
                        
                     
                                     
                        
                            Sviatky, kultúra a tradície
                        
                     
                                     
                        
                            Škola a spomienky
                        
                     
                                     
                        
                            Staroba
                        
                     
                                     
                        
                            Počítač a rodina
                        
                     
                                     
                        
                            Príroda a jedlo
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Žena, rodina, deti, spomienky
                        
                     
                                     
                        
                            Príroda a zvieratká
                        
                     
                                     
                        
                            Vzťahy
                        
                     
                                     
                        
                            Práca, dôchodok, spomienky
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Drogy
                        
                     
                                     
                        
                            Zdravie
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            články Viawebtour
                                     
                                                                             
                                            články Natankuj
                                     
                                                                             
                                            články Sme-Žena,
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vitamínová abeceda od Evy Krajmerovej (čítam so svojím vnukom)
                                     
                                                                             
                                            Varovanie z lásky od Venuši Indrákovej
                                     
                                                                             
                                            Slovensko v Šanghaji,Čína okolo nás od E.Krajmerovej,I.Magátovej
                                     
                                                                             
                                            Odsúdená na lásku, Tvoje oči sú cesta od Danky Janebovej
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Každú dobrú hudbu, podľa nálady.
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mnohé, práve v tom okamžiku pre mňa zaujímavé.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Žena-Sme
                                     
                                                                             
                                            www.viawebtour.sk
                                     
                                                                             
                                            www.natankuj.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Noc patrí básnikom
                     
                                                         
                       Ráno so slzou v oku
                     
                                                         
                       Ponuka
                     
                                                         
                       100 dní - veľa, alebo málo v živote človeka ?
                     
                                                         
                       Koncom týždňa
                     
                                                         
                       Píšem list v machu a papradí
                     
                                                         
                       Spoločná jeseň života
                     
                                                         
                       Požiar v susedovej chate
                     
                                                         
                       Náš krásny a predsa prázdny svet
                     
                                                         
                       Dotkni sa ma
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




