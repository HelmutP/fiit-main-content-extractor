
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Fotoreportáže
                     
                 Púť do Levoče cez Slovenský raj 

        
            
                                    6.7.2010
            o
            4:27
                        (upravené
                6.7.2010
                o
                9:26)
                        |
            Karma článku:
                12.05
            |
            Prečítané 
            3699-krát
                    
         
     
         
             

                 
                    Z článkov Jana Urdu som sa dozvedel o sieťovom mesačnom lístku ŽSR pre dôchodcov. V piatok som si teda kúpil lístok za 18 euro, ktorý ma oprávňuje cestovať 31 dní po slovenských železniciach včítane tatranských električiek a zubačky. Prvé 2 dni platnosti líska som využil naplno, použil som 14 vlakových spojov. Popritom som ako starý turista stihol prešľapať vyše 40 km. V sobotu som sa zúčastnil levočskej púte so zachádzkou do Slovenského raja a v nedeľu som bol vo Vysokých Tatrách.
                 

                 Keďže žijem v okrese Sobrance, ktorý nemá ani meter železnice, v piatok večer som sa presunul na bicykli do 20 km vzdialených Michaloviec, kde som si v synovom byte zriadil predsunutú základňu pre svoje vlakovýlety. Vyrazil som prvým spojom o 3:57 hod., v Košiciach som prestúpil na IC rýchlik do Popradu a v zápätí som sa vrátil osobákom do Spišských Tomášoviec. Išiel som si urobiť reparát za nevydarený pokus spred 10 rokov, keď som cestou do Svitu na pomaturitné stretnutie po 35 rokoch plánoval prejsť Prielomom Hornádu. Nestihol som vtedy vystúpiť v Spišských Tomášovciach, tak som vystúpil v nasledujúcich Letanovciach. Čakal ma obávaný prechod cez povestný Letanovský mlyn, ktorému som sa chcel pôvodne vyhnúť. Našťastie vtedy v Letanovciach okrem mňa a húfu Cigánov vystúpila aj 5-členná skupina skúsených turistov. Cestou k Letanovskému mlynu som dostal inštrukcie, ako uchrániť svoj movitý majetok pred dobiedzajucími Cigánikmi, čo sa nám aj úspešne podarilo.   Aj keď som v dávnejšej minulosti prešiel väčšiu časť Slovenského raja, na Tomášovskom výhľade a v Čingove som nikdy nebol. Keďže veľmi rád kombinujem viacero akcií dokopy, čo moja drahá polovička na mne neznáša, aj teraz sa mi púť na levočskú Mariánsku horu málila, tak som si ju predĺžil o prechod Slovenským rajom cez Smižany a Spišskú Novú Ves, ktorú som si tiež ešte nikdy poriadne neobzrel.   Pred pol ôsmou ráno som už pochodoval s plnou poľnou cez Spišské Tomášovce k Slovenskému raju. Južnú polovičku dediny tvorí tzv. "kolónia", obývaná cigánskym obyvateľstvom, žijúcim na rozdiel od neďalekého Letanovského mlyna v solídnych pestrofarebných domoch. O bezpečnosť prechodu cez kolóniu som nemal obavy, aj keď som išiel sám.         Keďže pomaly chodiť neviem, zachvíľu som bol pri Tomášovskom výhľade, kde som stretol prvých turistov. Skalná stena Tomášovského výhľadu je 200 m dlhá a 25-32 m vysoká. Stena z vápencových hornín je takmer kolmá a v hornej časti prechádza až do previsov. Je jedinou skalnou stenou, vyhradenou pre skalolezcov v Slovenskom raji.                           Po  polhodine rýchlej chôdze som bol pri prvých chatách rozsiahlej chatovej osady Čingov. Prechod cez Čingov po žltej značke do Smižian je zle značený, preto som sa musel párkrát spýtať chatárov na cestu, kým som sa nedostal cez spleť cestičiek na výpadovku.            Po 4 kilometroch som došiel do najväčšej slovenskej obce Smižany, ktorá je nekonečne dlhá a prakticky je spojená s mestom Spišská Nová Ves. Na konci dlhočiznej rovnej ulice sa mi črtal pohľad na najvyššiu kostolnú vežu na Slovensku (80 m). Obzrel som si centrum mesta a presunul som sa na železničnú stanicu.                     Oddychol som si vo vláčiku, ktorý ma previezol na juhozápadný okraj Levoče. Po vstupnú bránu do historického centra sú to od vlaku 2 km, druhý kilometer sa ide pozdlž mestských hradieb. Prešiel som sa po historickej Levoči, nazrel do kostolov a po množstve klasických odpustových stánkov som konečne našiel jeden s občerstvením. Dal som si cesnakový lángoš, ktorý mi veľmi chutil, tak som to zdupľoval a zapil vodou. Ešte som sa v predajni potravín predzásobil skromnou večerou a vydal som sa na 3 km vzdialenú Mariánsku horu. Tempo som musel zvoľniť, pretože už v Spišskej Novej Vsi ma začal pobolievať ľavý bederný kĺb, čo ma dosť vyľakalo. Bola ešte len sobota tesne popoludní, no po asfaltovej ceste na Mariánsku horu sa štveralo množstvo pútnikov od 3 do 90 rokov.                        Pol kilometra pod vrcholom je niečo, ako občerstvovacia stanica, s vodovodnými kohútikmi, WC a lavičkami. Po príchode ku kostolu som si išiel poplniť kresťanské povinnosti, potom som obišiel celé okolie. Prekvapilo ma množstvo stanov v lese v okruhu 200 m okolo kostola, ktoré v televízii ani na fotografiách z púte nevidieť. Druhým veľkým prekvapením pre mňa bolo zastúpenie cigánskeho etnika medzi pútnikmi, ktoré odhadujem na 20%. Zodpovedá to síce zloženiu obyvateľstva na Spiši, ale z množstva Cigánov, ktorých poznám, chodí do kostola málokto, preto ma to prekvapilo. Kĺb ma už poriadne bolel, tak som sa zložil na lúke pred kostolom. Vláčil som so sebou aj spacák pre prípad, že by som sa rozhodol zostať cez noc medzi pútnikmi. Moje staré kosti a boľavý klb ma po hodine polihovania natoľko rozladili, že som sa rozhodol vrátiť na noc do Michaloviec. Cestou dole do Levoče som pokrivkával vedľa asfaltky po tráve, pretože hore kopcom vystupoval neuveriteľný dav pútnikov, celé procesie s krížmi, zástavami a spevom.                                                    Po dokrivkaní do mesta moja púť pokračovala ešte nekonečné 2 km k vláčiku, no mal som šťastie na dobré spoje a šťastlivo som docestoval. Moje cestovateľské plány za nepoznanými krásami Slovenska na celý mesiac boli pre problémy s klbom ohrozené, pretože hneď prvý deň som prepálil tempo. Musím poslúchnuť svoju polovičku a uvedomiť si, že už nie som mladík. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




