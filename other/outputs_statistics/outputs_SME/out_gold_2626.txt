

 Športový redaktor a moderátor Markízy Matúš Krutý v stredajšom rannom spravodajskom bloku takto uviedol video z tvrdého nešťastného stretu dvoch hráčov  pozemného hokeja (reportáž č.8-spravodajský blok, 6:31): 
 No a ešte pred výsledkami zámorskej NHL si vychutnajte jeden súboj z pozemného hokeja. Pozemný hokej ani zďaleka nie je taký tvrdý šport ako hokej, ale niekedy padne kosa na kameň. Respektíve koleno na hlavu? 
 A na záver pridal ešte s úsmevom: 
 Tak si to vychutnajte, prajem príjemné teleráno. 
 Nasledovalo video s dvoma opakovačkami, s pesničkou "Dávej, ber" od Sagvana Tofiho.  Ha ha. Veľká sranda. 
    Vychutnávate si? 
   
 No. Nemali by ľudia s takýmto vnímaním cudzieho nešťastia byť náhodou automaticky diskvalifikovaní pre prácu novinára? 
   
 Aká lacná je naša nafta: „Máme druhú najlacnejšiu naftu v Európe,“ oznámil hlásateľ správ Jaroslav Zápala v Markíza minulý piatok.  Reportér Tomáš Velecký to zopakoval a ukázal aj tabuľku: 
 Po znížení spotrebnej dane totiž cena nafty spadla a dodnes sa drží okolo 1 eura. Po Cypre sme v Európe pre kamionistov druhou najlacnejšou krajinou. 
  
 Zdrojom má byť portál natankuj.sk. Ako ma upozornil Martin Májek, uvedené ceny v krajinách síce sedia, ale okrem Cypru poradie neplatí, tak to uvádza aj článok natankuj.sk, ktorý vyšiel v rovnaký deň ako reportáž Markízy.  Slovensko nebolo minulý týždeň druhé, ale siedme. Poľsko bolo 10. a Rakúsko 11. 
    
   
 PR pre novostavbu: Čitateľ Martin Andor ma upozornil, že nedávna videoreportáž na sme.sk robí novej Eurvee v podstate len skrytú reklamu. Vo videu vystupuje iba hovorkyňa projektu, padá kopu reklamných slov. Nemalo to video byť v inzertnej sekcii?  Nezastaviteľná STV: Ťažko nájsť týždeň, kde by sa dalo obísť v tomto blogu spravodajstvo STV. Určite ste si všimli, že jedinou televíziou a veľkým národným médiom, ktoré obišlo Slotove vulgárnosti o Mikloškovi, bola verejnoprávna televízia. Veď diváci verejnoprávnej televízie nepotrebujú  poznať rozdiel medzi slušným a primitívnym poslancom.   V pondelok v STV reportáži o chudobe zase Rita Bočáková povedala: 
 Ministerstvo práce preto prednostne riešilo situáciu sociálne slabých rodín s deťmi, zvýšilo im dávky v hmotnej núdzi. Prejavilo sa to aj v európskych porovnaniach chudoby.   Viera TOMANOVÁ, ministerka práce, soc.vecí a rodiny SR , /Smer-SD/ -------------------- Že sa nám podarilo z 13 % dostať sa na 10, 9 %. 
 Keby si novinárka robila svoju robotu dobre, nikdy by tieto čísla a udalosti nemohla takto pospájať. Uvedené čísla, tzv. riziko miery chudoby od Eurostatu, sú pre roky 2005 a 2008. Už pre rok 2007 však číslo spadlo na 10,7% a o rok neskôr už v podstate ostalo na totožnej úrovni (10,9%). Dávky v hmotnej núdzi, ktorými sa chváli Tomanová, boli nadpriemerne (medziročne plus 13-17%) zvyšované pre rodiny s deťmi až v septembri 2008, čiže tieto dávky nijako nemôžu vysvetľovať ten pokles od roku 2005, ktorý navyše skončil už v roku 2007. Pokles rizika chudoby je skôr výsledkom rapídneho pádu miery nezamestnanosti, a ten zase vďaka vysokému ekonomickému rastu v tých rokoch. A ani s tým samozrejme nemá ministerka práce nič spoločné.   Ibaže by niekto robil príspevok pre STV. 

