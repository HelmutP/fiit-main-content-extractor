

 Autobusová zastávka MHD: 
 Dvaja postarší občania sa rozprávajú o voľbách. 
 "Počúvaj, Fero, koho budeš voliť?" 
 "Asi SMER. A ty?" 
 "Si sa zbláznil? Voľ radšej SNS. Iba táto jedna strana dokáže obhajovať záujmy Slovákov. Tí špiny maďarské sa až príliš rozťahujú. Iba Janko ich dokáže zlikvidovať. A konečne bude pokoj. Janko je najlepší človek na Slovensku. V živote som nepoznal charakternejšieho politika ako je on." 
 "Je to dobrý človek. Ale asi predsa budem voliť Fica." 
 "Rob, ako myslíš. Ale pamätaj, už aj tí teplí nám tu idú pochodovať. Aby si sa jedného rána neprebudil a nezistil, že musíš hovoriť maďarčinou a okolo teba budú pobehovať samí buzeranti." 
 Poznámka: Veru, Janko je charakter. Uráža každého, kto mu vadí. Je symbolom alkoholizmu, močí z balkónu, jeho stranu sprevádzajú samé kauzy, ale aj keby vyvraždil  polovicu Európy, tak dostane percentá od svojich skalných voličov. Lebo prosto on je ten najlepší človek na Slovensku... 
 I keď zdá sa, že by v týchto voľbách mohol mnoho stratiť. Netreba však veriť prieskumom, nie sú dôveryhodné. Ale možno sa prebudíme 13. júna tohto roku a zistíme, že Slováci konečne dostali rozum. Možnože nie... 
   
   

