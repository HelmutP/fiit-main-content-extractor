
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Rusina
                                        &gt;
                Camino Santiago de Compostela
                     
                 8. etapa do Compostely alebo pôstne putovanie 

        
            
                                    24.4.2010
            o
            17:06
                        (upravené
                24.4.2010
                o
                21:08)
                        |
            Karma článku:
                8.17
            |
            Prečítané 
            1283-krát
                    
         
     
         
             

                 
                    Ešte je nočná tma, keď skladám stan. Bludičky prešli po ceste nad mojou čistinkou. Ranný pútnik ďalej doskáče.
                 

                 Dobieham ich pri brode potoka. Opatrne preskakujeme z kameňa na kameň. Nikto nechce mať vodnú nádrž v botách hneď takto z rána. Mĺkvo vchádzame do dediny. Oni sa odpojujú zlákaný neónmi snack-baru s prívlastkom non-stop. Zrejme v ňom poraňajkujú. Neraňajkoval som. Jednak nemám čo a jednak je Veľký pôst. Pribúda svetla, ale slnka sa možno nedočkám. Obloha je zavalená ťažkými oblakmi. Netrvá dlho a začne sa z nich valiť voda. Úsek je pekný, les, len chodník sa zvažuje nadol. Šľachy sa pripomínajú a ďalší pútnici ma začínajú predbiehať. Peší a aj jazdci. "Bom Camino"     Za lesným úsekom ma v prístrešku čaká civilná ochrana v reflexných oranžových kombinézach. Zapisujú si moju národnosť a pýtajú sa, kde som spal. "Tenda," ukazujem na batoh. "Čistinka pri potoku" neviem po španielsky. Za spoluprácu mi dávajú do zápisníka pečiatku. Pokračujem ďalej. Dážď pokračuje tiež. Vlastne mi až tak neprekáža. Bolo už aj horšie. Predvčerom napríklad, alebo úsek v kopcoch za Ponte de Lima. Jednoducho som si zvykol.   Moju myseľ začína zamestnávať hlad. V poslednej dobe som mal vysokú spotrebu. 400 gramovú nutelu za deň, lekvár na posedenie... Zákon zachovanie energie. Teraz sa snažím kráčať proti nemu. Uvidím, ako dlho vydržím. Predsavzal som si, že najskôr vyhľadám nejaké jedlo v meste Padrón, podľa brožúrky najbližšie väčšie mesto. Okolo obchodíkov prechádzam zrýchleným krokom bez pozretia na ne. Našťastie ich len zriedka stretám. Väčšinou sú zavreté.  Do Padrónu vstupujem cez kolotoče. Žiarivé farby v kombinácii so šedivým dažďom. Pusté, ani noha. Už sú tu pripravený na koniec pôstu. Chvíľu mi trvá, kým objavím otvorenú padariu. Kupujem si klasicky pečivo s niečím vo vnútri. Tentokrát mi je úplne jedno s čím, hlavne že s niečím.   Stretám sv. Jakuba...      Ďalšie kroky vedú do kostola. Zisťujem, že obrady sa začínajú o 17:30. Tak to mám niekoľko hodín času. Čo budem robiť tak dlho? Idem teda na nákup jedla na Veľkonočnú nedeľu. Objavujem stánok v ktorom zrejme predávajú miestne tradičné jedlá. Klobásy rôznych veľkostí a farieb, veľké bloky syra, a mega super pão rôznych typov. Úžasné je, že stánkár z času na čas odkrojí z produktov na ochutnávku prístojacim pri stánku. Kým si rozmyslím, čo si vlastne kúpim, mám už vyskúšané viaceré syry a super pão. Obed mám teda za sebou. Pýtam si pol kila hnedého super pão s hrozienkami (je tak dobrý, že sa dá jesť bez ničoho ako koláč) a dve nožičky klobásy. Ten pol kila super pão odkrajuje zo snáď desaťkilového bochníku. Klobásu nemám vyskúšanú, ale vyzerá dobre. Platím 5 eur. Ešte mi odkrajuje ďalší kúsok toho super pão grátis.  S nákupom idem na prechádzku po meste. Albergue je od kostola cez most. Dostávam pečiatku. Odchytáva ma anketárka. Spočiatku som neochotný pre ťažký batoh na pleciach. Dôvod návštevy Galície - Santiago, národnosť - slovenská, zamestnanie - študent, odkiaľ som prišiel - Porto, koľko miniem denne - cca 6 eur, aký typ ubytovania využívam - väčšinou stan, chcem sa vrátiť do Galície - áno, odporučím Galíciu kamarátom - áno...  Za mostom je premeň. Príležitosť doplniť zásoby vody. Súsošie v nike zobrazuje sv. Jakuba apoštola pri krste kráľovnej Lupa ako pripomienka evanjelizačnej činnosti apoštola v Galícii (infobrožúrka).     Nižšie je reliéf s výjavom prevozu tela svätca späť do Galície po smrti v Ríme.      Zaparkovaná kópia starovekého Galského vozíka. Samozrejme je urobená z kameňa. I štruktúra dreva je na ňom vysekaná.      Na jednom námestí objavujem kamenný kríž s typickou štylizáciou.       Mimoriadne ma na ňom zaujal jeho podstavec s výjavmi. Koľká jednoduchosť a krása...                Infobrožúrka píše o ďalších zaujímavostí v meste, ale medzičasom sa po doobedných dažďoch cez oblaky predralo slnko. Dávam prednosť vyhrievaniu sa na lavičke pred kostolom. Mám trocha divný pocit s toľkej nepohyblivosti po týždni putovania, súčasne je to veľmi príjemné. Aspoň po oblohe putujú oblaky. Sú neškodné, neprší z nich len tienia. Počuť hudbu a ruch od kolotočov. Tuším s tým ani tak nečakali na koniec pôstu ako na pekné počasie.  Na obradoch sa opäť okrem miestnych ukazujú aj pár pútnikov. Tentokrát to nemali ďaleko z albergue cez most. Niektoré tváre poznám ešte od minula. Počas obradov sa kladie mimoriadny dôraz na liturgiu. Špeciálni asistenti v špeciálnych háboch prinášajú veľký kríž na poklonu. Oltárny kríž odhalujú efektne z rebríka.        Po obradoch nasleduje procesia. Toto nepoznám a čakám spolu s ďalšími prizerajúcimi čo sa bude diať. Aj kňaz pred kostolom čaká (neustále pozerá na hodinky).   Náhle sa sprievodu kapely pripochoduje nejaká delegácia, pravdepodobne zástupcovia mesta, a napochodujú do kostola.     V kostole sa zrejme niečo deje, vonku sa zase len čaká. Som zvedavý ako to bude pokračovať a akú úlohu budú mať špicaté masky. Trvá to však dlho, vyše pol hodiny. Batoh ťaží a dnes by som sa chcel k Santiagu natoľko priblížiť, aby som nasledujúci deň na obed bol pri katedrále. Odchádzam bez pointy.        Až keď som na vidieckých cestách tesne za mestom, ozve sa od kostola muzika. Trvalo to skrátka dlho.        Do západu slnka sa mi ešte podarilo prejisť 7 kilometrov. Po poobednom šlofíku sa k večeru pomaly prebúdza dážď. Dlho si nevyberám miesto, nechcem rozkladať stan v daždi. Končím v lesíku neďaleko cesty. Večeriam kúsok super pão s hrozienkami. Zajtra budem v Compostele, aj keby som to mal doplaziť.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            10. etapa do Compostely alebo kto nemá v hlave, ten má v nohách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            9. etapa do Compostely alebo ako som sa neskoro v noci doplazil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            7. etapa do Compostely alebo posledná večera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            6. etapa do Compostely alebo osud premočených smradlavých ponožiek
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            5. etapa do Compostely alebo dom s modrou bránkou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Rusina
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Rusina
            
         
        martinrusina.blog.sme.sk (rss)
         
                                     
     
        Momentálne erazmus študent v Porte (Portugalsko)na fakulte Belas Artes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2020
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Camino Santiago de Compostela
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




