
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Phillipa Marco
                                        &gt;
                Iné
                     
                 Do neba volajúca dehonestácia! Prečo Slováci-hlupáci?! 

        
            
                                    13.5.2010
            o
            18:39
                        (upravené
                15.5.2010
                o
                18:59)
                        |
            Karma článku:
                7.73
            |
            Prečítané 
            987-krát
                    
         
     
         
             

                 
                    Pamätám si, keď éterom zaznela...
                 

                      Pamätám si, keď éterom zaznela len okrajovo skoro nenápadná správa: „Situácia po rozdelení spoločného štátu Slovákov a Čechov sa vyvíja tak, že Slováci idú z biedy do blahobytu a Česi z blahobytu do biedy.“ Konštatovanie priam až zarážajúce po rokoch proklamovania toho, že sme v rámci jednej republiky existovali ako dva rovnoprávne štáty. Určite si väčšina Slovákov všimla politicky či ľudsky vysoko neetické a nekultúrne vyhlásenie madam Olbraitovej,  kedy sa vyjadrila, že na mape Európy je čierna diera a myslela tým Slovensko.  Táto pani okrem iného určite zabudla, že tu na tomto kuse zeme priamo v srdci Európy žijú a dýchajú konkrétni ľudia, teda my.   Prečo taký úvod? Odhliadnuc od nespočetných politických káuz a od nečestnosti mnohých indivíduí vo vládnych kruhoch, sloboda daná Slovákom po revolúcii a po osamostatnení, dovolila, aby sa prejavili a ukázali svetu, že gro Slovákov sú múdri, pracovití a húževnatí ľudia. A to aj napriek pomerne častým zmenám v politickej garnitúre, napriek tomu, že táto, či tá predtým neboli a nie sú ideálne, a teda tzv. slovenskosťou doslova protislovenské a tzv. ústretovosťou doslova  hádžuce polená pod nohy. Možno sa pani Olbraitová nestíhala čudovať, možno je jej bolo úplne jedno, ale jedna vec je istá.  Slovensko prijalo euro v predstihu pred mnohými postkomunistickými krajinami. A to hovorí naozaj veľa. Neboli to ani zďaleka páni vo vláde, čo splnili podmienky, boli to ľudia, tí, čo zabojovali a ukázali svetu, že sú niekto. Hodnota eura stúpa, či klesá. Ľudia sú spokojní, alebo naopak nie. Bolo to dobré, nebolo to dobré? Polemizujú, no  generálne si uvedomujú, že to bolo nezvratné a s menšími či väčšími problémami v konečnom dôsledku prínosom. Čo by ale málo ostať nemenné - level devízy človeka. Platidlo, ktoré má vyššiu hodnotu ako euro, či hocijaká iná mena - menej pokory a menej kverulantstva. My, Slováci, národ holubičí - je to akoby naša pečať -všade sa proklamuje. Hlava dole a ponosovanie. Tak pomaly ale isto prichádzame o devízy, ktoré nám boli prirodzene dané. To treba zastaviť.  Preštudovať si skutočné dejiny Slovenska a nie tie, čo nám roky predostierali učebnice dejepisu. Vyhľadať tisícky osobností, ktoré v toku dejín reprezentovali svojim umom a zručnosťou slovenský národ na celom svete. Nie je ich možné menovať, pretože sú ich naozaj tisíce a veľké množstvo z nich vykonalo niečo naj...vynašlo niečo, čo nikto predtým. Mali by sme poopraviť samotný pohľad na seba a prestať sa neustále sťažovať. Na tomto mieste  by sme sa mali poučiť od Čechov. Oni to jednoducho vedia. Všetko české je ohromné a výnimočné. A takýto obraz o sebe vysielajú do sveta. Potom sa tak aj svet na nich pozerá. Aký je typický obraz Čecha s priemernou životnou úrovňou? Usmiaty, bezstarostný s ruksakom na chrbte, žijúci celý život v dvojizbovom byte, ktorý sa má vynikajúco a všade to rozhlasuje. Oproti nemu ustarostený, uponáhľaný Slovák, ktorý síce má vlastný dom, chatu, auto, ktorý zabezpečil aj svoje deti, ale pritom sa má zle. Časté proklamovanie toho, že Slováci sú hlúpy národ zo všetkých strán a aj zo strán Slovákov samotných je do neba volajúca skrivodlivosť, ktorú my, sami páchame na sebe. Opýtajte sa seba osobne, či ste hlúpy. Opýtajte sa, či sú hlúpe vaše deti, vaši rodičia a vaši priatelia alebo vaši známi. Pozerám navôkol a som obklopená nespočetným množstvom múdrych, pracovitých, tvorivých a investigatívnych ľudí, ktorých stretávam v práci, na ulici, v kaviarni, v divadlách...všade. Jedna vec je istá: na celom svete sú ľudia hlúpi a múdri, dobrí a zlí, vzdelanci a nevzdelanci, všade sú zlodeji, klamári a  vrahovia, ľahké ženy a pasáci. Typická vlastnosť menom hlúposť pre národ jednoducho nejestvuje. Nehovorme to o sebe. A slovami básnika:  „Nehaňte ľud môj, že...“ Táto báseň hovorí za všetko. Možno by si ju bolo potrebné oprášiť a prečítať aj srdcom. Nádherné odkazujúce a plnovýznamové slová. Zvukomaľba, krasoreč, posolstvo, transformácia idey do reality, do uvedomenia sa.      Nehaňte ľud môj, že ľud je mladý,  klebetárski posmievači!  V mladom sa veku ide do vlády,  starému sila nestačí;  keď predkov nemal – a či ich nemal,  ktorí prežili čas zlatý? –  čo bys’ sa mu ty zato posmieval?  Na potomkov on bohatý.    Nehaňte ľud môj! že nemá dejov  slávy svojej minulej sklad;  on ešte peknou kvitne nádejou,  budúcnosť má jeho poklad:  Počuješ bájne hlasy povestí?  Hovoria tie, keď čas čuší,  zázračný z nich on svet má vyviesti,  budúci svet v nich on tuší.    Nehaňte ľud môj, slepí sudcovia!  že ľud môj je len ľud sprostý;  často sú múdri hlúpi ľudkovia  dľa súdu svetskej múdrosti:  Počuješ? Spieva slovenské pole,  spievanky zrodia Homérov;  len ľúbosť útlu spievajú hole,  no budú aj bohatierov. –    Nehaňte ľud môj, ústa nečisté!  že odhodok on je hlúpy;  múdrosťou jeho vykúpení ste  a ešte vás raz vykúpi!  Či nepočuješ slová rozumné,  čo deň po dni opakuje?  Večné v nich pravdy, hlboké,  ten ľud vedu si hotuje.    Nehaňte ľud môj! že je len malý,  že nevládne celým svetom.  Kde telo ducha k zemi nevalí,  tam duch lieta ľahším letom.  Na helénsky že ľud si spomnite –  duch si svet vlastný utvorí;  či v žaludi hôr zárod vidíte?  Nie! – no viďte dubísk hory!    Nehaňte ľud môj! že je chudobný,  že kraj biedny, smetisko má;  to je blud! má svet sebe podobný,  má hory, rieky, zlato má! –  A keď by nemal? – ale má hlavy,  má obchodu silný zárod. –  Alebo či sú čriedy a bravy  a koniarne komus’ národ?    Nehaňte ľud môj! že je ľud tichý,  že rád trpí, že je slabý; –  pravda, surovej on nemá pýchy,  nie je v zápasoch pochabý.  Vy to neviete, že duch národov  práve takú povahu má:  keď má vystúpiť s činnou slobodou,  najprv ticho myslí, dumá. –    Nehaňte ľud môj! že úcty nemá,  že ľud môj je potupený. –  Slávnejšie ešte svet tupí plemä,  nuž kto je tam zhanobený? –  Ach, zlé sú časy! na všetky strany  svet žertvuje bohom zlatým. –  Ale nechaj mi národ bez hany;  národ patrí k veciam svätým!       Menej pokory, menej kverulantstva a viac sebaúcty by bolo aj cestou k naprávaniu neblahého status quo, do akého sa Slovensko dnes dostáva vďaka nečestnosti tých, ktorí nám v rámci zákonodarstva určujú, čo máme, nemáme, môžeme a nemôžeme robiť a pritom oni sami nemajú o tom poňatie a ak náhodou majú, tak sa ich to dotýka čisto z pozície, čo je dovolené Bohovi, nie je dovolené volovi.  Existuje však zákon, ktorý je všeobecne platný a nepotrebuje byť zakotvený v žiadnej ústave. Diktuje hosama matka príroda. Do päťlitrového džbánu vojde len 5 litrov vody. Dostávame sa na hranu medzi právo a bezprávie a otázka znie; Dokedy ešte? Dokedy bude možné stavať na práci a húževnatosti tzv. obyčajných ľudí. Aká demagógia! Zrazu chcú všetci stáť na strane „obyčajných“ ľudí. Ale nie je to náhodou tak, že skvelá existencia práve tých zrejme „neobyčajných“ ako pán x, pán y, či pani yx, je postavená  na tých „obyčajných“.  No vážne! Chcelo by to identifikáciu! Kto tu je obyčajný a kto neobyčajný!?!   Prešli sme vývojom a bolo by načase dozrieť. Sládkovič píše: „Nehaňte ľud môj, že je ľud mladý, klebetárski posmievači.“ Áno, náš národ sa znova a znova rodil a znova sa stával mladým. Byť mladým je úžasné, ale treba aj dozrieť, pretože zrelé ovocie je zdrojom semien a semeno je zdrojom nového života.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Phillipa Marco 
                                        
                                            Blahoslavení chudobní duchom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Phillipa Marco 
                                        
                                            Ja tie gorily veľmi ľutujem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Phillipa Marco 
                                        
                                            Necrotainment verzus úcta k mŕtvym..."rozlúčka" s Muamarom Kaddáfím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Phillipa Marco 
                                        
                                            Vivat azet, vivat pokec. Rozdajme si to slovom. 20.kapitola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Phillipa Marco 
                                        
                                            Richard Müller a jeho curriculum vitae na koncerte Potichu tour
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Phillipa Marco
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Phillipa Marco
            
         
        marco.blog.sme.sk (rss)
         
                                     
     
        ja som ja
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    60
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    995
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Iné
                        
                     
                                     
                        
                            Pomýlený svet ľudí
                        
                     
                                     
                        
                            Rozdajme si to... (úryvky)
                        
                     
                                     
                        
                            Dejiny písané rýmom
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radka Cepláková
                                     
                                                                             
                                            Palko Vrabec
                                     
                                                                             
                                            Marcel Páleš
                                     
                                                                             
                                            Jan Marton
                                     
                                                                             
                                            Dana Janebová
                                     
                                                                             
                                            Janka Bernáthová
                                     
                                                                             
                                            Vladimír Schultz
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            martinus
                                     
                                                                             
                                            "povedzme, že áno"
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zo Slovenska treba odísť ...
                     
                                                         
                       TA3 vystrihla nepríjemné otázky novinárov na Fica
                     
                                                         
                       100 dní pre Mariána Kotlebu
                     
                                                         
                       Slzy bez mena
                     
                                                         
                       Sľúb mi
                     
                                                         
                       Aj bez mäsa sa dá dobre navariť
                     
                                                         
                       Cukor – zabijak ľudstva číslo jeden (11 krokov ku zdraviu, 9.krok)
                     
                                                         
                       Hurá, vyhrali sme!
                     
                                                         
                       Neposlušná
                     
                                                         
                       17. November 1989
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




