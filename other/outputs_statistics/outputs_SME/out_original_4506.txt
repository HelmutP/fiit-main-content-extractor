
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samuel Masarik
                                        &gt;
                Nezaradené
                     
                 Nové baroko 

        
            
                                    13.4.2010
            o
            22:20
                        |
            Karma článku:
                3.23
            |
            Prečítané 
            1378-krát
                    
         
     
         
             

                 
                    Richard Müller: Baroko. Autorom textu je Michal Horáček, spev: R. Müller, Anna K., Ivan Tásler a Marián Čekovský. Čo mi napadá pri počúvaní tejto piesne...
                 

                      Jej refrén hovorí  o krajine bez vlastného neba. Na videoklipe sa zároveň objavuje pražský Žižkov.           „Dvě nahé holky“,  schúlené pri hrobe o ktorých hovorí text mi pripomínajú renesanciu.  V hrobe leží Tycho Brahe („Tycho de Brahe“), dánsky astronóm z obdobia renesancie, ktorý celý život upieral svoj zrak do neba. Umrel v Prahe. Okolo jeho smrti existuje viacero dohadov, podľa jedného mýtu mu zlyhali obličky  kvôli otrave ortuťou počas alchymistického pokusu. Možno sa pokúšal uvariť  elixír života, dosiahnuť večný život. Dve nahé dievčatá na jeho hrobe mi  symbolizujú apatiu z konca obdobia renesancie, po ktorej prichádza baroko. Návrat  k Bohu, obživa kresťanstva bez trpkej príchute  minulosti v Európe.          „Andělé, odhalte tváře, pro slepé harmonikáře...“ biedu modernej doby symbolizuje nezáujem o tých, ktorí potrebujú našu pomoc. Sú pritom „důverně známý“ tí, ktorí potrebujú pomoc. „Jste-li tak důverný známý, pojďte a zpívejte s námi“.. o tom, jak mocné je baroko... pokračuje text piesne.       Čo je tou „denní můrou“ ktorá nám „sežrala století“? Dvadsiate storočie a postmoderna v ktorej centre bol človek a jeho záujmy sa neosvedčili. Výsledkom bolo zabité storočie, stámilióny mŕtvych a vyhodený potenciál rozvíjajúcej sa  Európy. Zdá sa mi, že Európa v čase obnovy mrhá svojou šancou na novú nádej,  nové baroko.   Žeby  si autor textu myslel opak? Možno to vyjadruje „noc lámání kostí“ pražskej smrtky, tej „bestii“ ktorá nás straší smrťou. Kto  iný ju zrazí k zemi „tak jak to umí jen baroko“?.        Nikomu neodopieram inú interpretáciu textu, ale vo mne pieseň evokuje optimizmus a novú Nádej v smrti a zmŕtvychvstaní Ježiša Krista. V tom, že naše hriechy si odtrpel a kriesi nás svojím duchom do nového života.      Dáva nám príklad a hlavne dôvod nemstiť sa, nekradnúť, hľadať pravdu... iracionálne niekoho milovať.         O novom Kráľovi, ktorý porazil smrť spieva aj pieseň Viva la Vida („Smrť a jej priatelia“) od skupiny Coldplay.       "Taková zář je jen jedna..."  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samuel Masarik 
                                        
                                            Breivik, Hitler, kresťanstvo a ufóni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samuel Masarik 
                                        
                                            Veríte evolúcii?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samuel Masarik 
                                        
                                            Homosexualita v psychológii a spoločnosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samuel Masarik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samuel Masarik
            
         
        samuelmasarik.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2178
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




