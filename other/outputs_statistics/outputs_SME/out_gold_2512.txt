

 Jeho korene siahajú do 17. storočia, keď bola škola súčasťou jezuitskej univerzity. V 19.storočí odovzdal cisár František I správu gymnázia premonštrátom. 
 Až do r. 1918 si škola pod vedením premonštrátov zachovala katolícky charakter. 
 V roku 1919 prešla škola do správy štátu a vyučovacím jazykom sa stala slovenčina. Vyučovali tu skvelí profesori (Karol Murgaš, významný kultúrny dejateľ, bol prvým riaditeľom československého štátneho reálneho gymnázia), škola mala úspešných absolventov. Toto Gymnázium prežilo aj ťažké vojnové roky, keď boli Košice okupované horthyovským Maďarskom. Neprežilo však obdobie privatizácií a reštitúcií v deväťdesiatych rokoch 20.storočia. 
   
 Po nežnej revolúcii v roku 1989 sa budova Gymnázia dostala v rámci reštitúcií späť do rúk cirkvi. Tá spočiatku ponechala Gymnázium v budove, no v roku 1997 sa po majetkových ťahaniciach muselo toto najstaršie Gymnázium v Košiciach zo svojej historickej budovy v centre mesta vysťahovať do budovy Základnej cirkevnej školy na Ulici Laca Novomeského na Kuzmányho sídlisku. Základná cirkevná škola sa mala presťahovať do historickej budovy v centre mesta. To sa však nestalo. 
   
 Keď sa dnes prejdem po Kováčskej ulici, pozriem sa na starobylú budovu môjho bývalého Gymnázia, srdce ma zabolí. Na miestach, kde v minulosti študovali celé generácie študentov (mnohí sa neskôr vypracovali na významné osobnosti našej spoločnosti), sa dnes nachádzajú obchody, butiky, reštaurácie alebo jednoducho priestory na prenájom. 
   
 Chápem, že z takého druhu prenájmu má cirkev väčší zisk, no stále som veril, že     pre cirkev sú dôležitejšie iné hodnoty ako peniaze. Stále som myslel, že cirkev patrí k tým inštitúciam, ktoré podporujú vzdelanie. Žiaľ, aj táto skúsenosť ma presvedčila o opaku. 
   
 Money talks. Reč peňazí je silnejšia. 
   
 Vzdelávanie v našej spoločnosti opäť raz prehralo. Po koľkýkrát už? 
 My, absolventi  Gymnázia v Košiciach na Kováčskej ulici č.28 však nezabudneme... 

