
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Iveta for president 

        
            
                                    1.3.2009
            o
            17:31
                        |
            Karma článku:
                17.30
            |
            Prečítané 
            25117-krát
                    
         
     
         
             

                 
                    Poslední traja slovenskí prezidenti majú tri veci spoločné: sú to komunisti, dôchodcovia a muži.
                 

                    Komunisti tu páchali zlo 40 rokov. Brali ľuďom slobodu a kradli im ich životy. Zato dali masám najesť, ale to bolo všetko. Nesmelo sa cestovať a nesmelo sa podnikať. Smelo sa mať rovnako na hovno a smelo sa držať hubu. Je hanbou pre Slovensko, že od roku 1993 boli všetci traja prezidenti komunisti a je ešte väčšou hanbou, že 20 rokov po revolúcii tu straší jeden z nich. Ešte väčší výsmech je Milan Čič, vedúci kancelárie prezidenta a benchmark v prevracaní kabátov. Pred revolúciou bol námestníkom ministra, ministrom a písal knihy ako "Ochrana socialistickej ekonomiky" a po revolúcii bol predsedom vlády, predsedom Ústavného súdu a písal knihy ako "Cesta k demokracii". Zlé jazyky vravia, že nebyť Čiča, Gašparovič by nikdy nedoštudoval a preto tam teraz jeden páprdo drží druhého.   Dôchodcovia patria na dôchodok a nie do úradu, ktorý je síce mocensky bezvýznamný, ale reprezentačne nie. Michal Kováč mal v deň svojej inaugurácie 62 rokov a 7 mesiacov, Rudolf Schuster mal 65 rokov a 5 mesiacov a Ivan Gašparovič 63 rokov a 3 mesiace.Ak bude Ivan Gašparovič opäť zvolený, bude mať 68 rokov a úradovať až do veku 73 rokov. Potom môže kandidovať ešte na člena politbyra Sovietskeho zväzu. Chápem, že treba mať dostatok životných skúseností, preto aj Ústava stanovuje minimálny vek kandidátov na 40 rokov, no určite nie je dobré to hnať do opačného extrému.   Na základe vyššie uvedeného by pre našu krajinu bolo skutočným posunom vpred, ak by najbližší prezident nebol ani komunista, ani dôchodca. Navyše, v prípade Ivana Gašparoviča existujú ďalšie dôvody, prečo ho nevoliť a to najmä aktívna účasť na noci dlhých nožov, kedy naše štátne zriadenie dostalo vážne zabrať. Za aktívnej účasti Ivana Gašparoviča, ktorý bol vtedy predsedom parlamentu! Oproti tomu sú Nafta Gbely, BMG, či "ingliš is ízi" drobnosťami.   Preto by bolo načase mať konečne hlavu štátu, za ktorú sa nebudeme musieť hanbiť. A práve preto podporujeme Ivetu Radičovú. Je v najlepšom veku (52 rokov a 4 mesiace), má vzdelanie, dobrú povesť, ovláda jazyky, určite by nám nerobila hanbu a ako žena by zaručene viac zviditeľnila Slovensko vo svete. Navyše má (okrem samotného prezidenta) ako jediná reálnu šancu tieto voľby vyhrať.   Pre stranu Sloboda a Solidarita je preto kandidátom na prezidenta Iveta Radičová.       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (282)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




