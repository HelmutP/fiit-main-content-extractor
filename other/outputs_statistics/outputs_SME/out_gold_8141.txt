

 Môlča 24. júna (TASR) - Spolu 67 jazdcov z 15 krajín súťažilo cez víkend v Môlči pri Banskej Bystrici o prvenstvo v 8. pretekoch seriálu 12-dielneho seriálu majstrovstiev Európy v autokrose. Medzi európskou špičkou, v ktorej dominujú najmä ruskí jazdci, obsadili Slováci miesta do prvej dvadsiatky. Najúspešnejší boli Igor Vlasatý a Roman Vlčko, ktorí zhodne skončili na 8. mieste. V divízii junior buggy obsadil David Gabriel 6. a Michal Dibala 7. miesto. 
 Výsledky: 
 divízia D3A (do 1600 cm3): 1. Petr Turek (ČR/Škoda) 5:13,66, 2. Sebastian Korn (Nem./Suzuki) 5:14,71, 3. Christian Freischlad (Nem./Büchl EB) 5:20,82, ...8. Igor Vlasatý (SR/Seat Buggy) 5:33,99, 
 divízia D1 (do 3500 cm3): 1. Rustam Minihanov (Rus./Ford Puma) 5:29,28, 2. Airat Šaimiev (Rus./Toyota Corolla) 5:32,28, 3. Helmut Wild (VW Polo Turbo) 5:35,18,... 8. Roman Vlčko (SR/Nissan Sunny) 5:59,88, 10. Jozef Zimerman (Toyota Corolla) 1:00,03, 
 divízia 3 (do 4000 cm3): 1. Fritz Duisendstra (Hol./VW) 5:07,63, 2. Mathias Behringer (Nem./Audi) 5:09,12, 3. Jaroslav Hošek (ČR/Audi 8) 5:19,68,... 15. Miloš Marton (SR/Ford Cosworth) 5:51,29,  
 junior buggy: 1. Jakub Knoll (Kawasaki) 4:02,47, 2. Libor Kříž (Kawasaki) 4:03,18, 3. Vladislav Štětina (všetci ČR/Yamaha) 4:08,72,... 6. David Gabriel (SR/Pro Pulsion) 4:18,01, 7. Michal Dibala (Yamaha Lassak) 4:27,17. 

