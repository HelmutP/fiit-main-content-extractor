

 „Fíha, ten pumpár mi vydal o 150 korún viac. Musíme sa vrátiť." 
 „Oci, veď sa ponáhľame a myslíš že ten pumpár si to vôbec všimne ? Vieš aké robia pumpári kšefty každý deň?" 
 „Musíme sa vrátiť". 
 „No čo Ti povedal ?" pýtam sa otca , keď sa vrátil z pumpy. 
 „Pozrel sa na mňa podozrievavo a ani poriadne nepoďakoval." 
 „A už si spokojný ?" 
 „Ano, som." 
   
 Po roku 2000 mi otec poskytol ešte krajší príklad Kantovho kategorického morálneho imperatívu. Vykradli mu chatu na vinici. Išiel na poisťovňu. Spísal zoznam asi 20 vecí čo zmizli, poctivo doložil odkladané nákupné bloky, prípad zaevidovaný, uzavretý. Po niekoľkých mesiacoch  robil otec poriadok v garáži a .......... našiel vysávač , o ktorom si myslel že bol ukradnutý a ktorý bol na zozname poistného plnenia. Zavolal do poisťovne, že chce urobiť zmenu v zozname ukradnutých vecí. Referent mu odkázal, že takéto veci jedine osobne. Sadol v Nitre do auta a išiel do Levíc do poisťovne: 
 „Dobrý deň, som tu kvôli poistnej udalosti č. ....., chcel by som urobiť zmenu v zozname ukradnutých vecí." 
 „Hmm...prípad je už uzavretý. Asi to už nepôjde a čo tam chcete pridať ?". 
 „Nič nechcem pridať, ale odobrať .... vysávač, ktorý som dnes našiel v garáži". 
 „Prosím??" referent myslel, že zle počuje. 
 Zrejme sa tento prípad rýchlo rozniesol po celej poisťovni, pretože pred otcovým odchodom prišiel sám riaditeľ, usmieval sa a povedal, že s takýmto niečím sa ešte nestretol počas celej kariéry. 
   
 Immanuel Kant (1724-1804) , tento geniálny nemecký mysliteľ nám dal doposiaľ neprekonaný koncept morálky. Povedal, že morálka činu spočíva v jeho motíve ( robiť správne veci zo správnych dôvodov, z dobrej vôle ): „Aj keby výsledok nášho úsilia konaného na základe dobrej vôle bol nulový, stále bude žiariť ako diamant oproti krásnemu výsledku vykonanému zo zištného dôvodu." 
 Kant uvádza príklad majiteľa obchodu, ku ktorému príde neskúsený zákazník. Majiteľ vidí, že by mu mohol poľahky predať aj horšiu kvalitu za vyššiu cenu, ale neurobí to, kvôli reputácii, aby sa náhodou o ňom nešírilo zlé meno. Podľa Kanta takéto rozhodnutie nemá žiadnu morálnu hodnotu, pretože je urobené z nesprávneho dôvodu, zo zištného motívu. 
 Jedine vtedy, ak človek odolá osobnému záujmu, túžbe, inklinácii, dokonca aj motívu altruizmu, iba vtedy je jeho čin morálne cenný. 
 Čiže v prvom rade sa musí človek oprostiť od emócií, preferencií a túžob a konať na základe čistého rozumu. Avšak aj potom môžu dobré skutky človeka viesť dva imperatívy. Prvým je hypotetický imperatív ( ak chceš X, rob Y... ak chceš mať dobrú reputáciu, neklam zákazníka.... a pod.). Druhým je kategorický imperatív ( skutok je dobrý sám o sebe, konaný z „dobrého dôvodu", z tzv. morálnej povinnosti). 
 Samozrejme, že Vám ako zákazníkovi, alebo človeku môže byť jedno na základe ktorého imperatívu Vám niekto poskytne skvelé služby, alebo Vám pomôže. Takéto konštatovanie však nijako neznižuje nadčasový Kantov odkaz. 
   

