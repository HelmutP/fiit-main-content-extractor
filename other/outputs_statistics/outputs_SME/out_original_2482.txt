
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Súkromné
                     
                 Na chvíľočku, priatelia... (pred odchodom) 

        
            
                                    8.3.2010
            o
            7:11
                        (upravené
                8.3.2010
                o
                8:42)
                        |
            Karma článku:
                7.23
            |
            Prečítané 
            1347-krát
                    
         
     
         
             

                 
                    V utorok večer balím... Už dlho som nepotrebovala baliť kufor "na prežitie" mimo domu  a tak cítim v sebe akýsi, slovami ťažko opísateľný,  zmätok. Ako pomôcku využívam odporúčania z "pozvánky".
                 

                 
  
   Hm, ono to nie je pozvánka v pravom zmysle slova. Mne sa však  také pomenovanie viac páči , lebo  trochu brzdí rýchlosť môjho strachu. Vám však prezradím, že v skutočnosti  ide o objednávací lístok na nástup hospitalizácie na neurochirurgické oddelenie nitrianskej nemocnice.       Po dlhom  trápení, premýšľaní, zvažovaní, útrapách mysle i tela, som sa rozhodla, že ponúknutý termín opravy mojej zdevastovanej a vysunutej platničky prijmem. Je to správne rozhodnutie? Neviem. Nahlodáva mi ho chvíľami  pochybovačný škriatok  Myslerozvracač. Dnes  sa ho chystám zniesť zo sveta.  Vykrútim mu krk, vystrojím mu pohreb a navždy sa s ním rozlúčim. K tomu činu som sa rozhodla práve teraz.   Trápim sa už veľmi dlho, striedajú sa obdobia hrozné, zlé, lepšie a tak dookola. Práve tie "lepšie" podnecujú Myslerozvracača k scestným nápadom typu - ono sa to spraví, nikdy nebolo tak zle , aby nemohlo byť horšie, už si na bolesť zvyknutá, veď nejako to zvládaš...   Dosť! Vyhlasujem svoje rozhodnutie za nezvratné. Idem do toho! Pán doktor, prosím, zreparujte ma!   *******   Oddýchnete si odo mňa, cha,  nebudem vás istý čas zaťažovať svojimi výlevmi, názormi, témami., diskusnými príspevkami. Ale netešte sa dlho! Ak sa mi podarí prebrať z narkózy, tak sa iste vrátim! Verte - neverte, už teraz sa na vás teším. Ja vás mám totiž veľmi rada. Vás, majiteľov vlastných blogov, vás, stálych čitateľov a prispievateľov do diskusií, vás, náhodných návštevníkov blogu a čuduj sa svete, aj vás, čo vyjadrujete nesúhlas, protirečíte a odsudzujete obsah môjho blogu. Bez vás (všetkých) by totiž môj blog nemal význam. Bez vás by som sa mohla vrátiť k písaniu do šuflíka ...    Články mojich obľúbencov mi budú  tiež chýbať, keď sa vrátim všetko doženiem.    Milí priatelia, (nemám problém vás takto osloviť, lebo mnohí z vás, ste sa mojimi priateľmi ozaj stali), mala som v pláne vymenovať niekoľko mien blogerov, za ktorými sa skrývajú  moje spriaznené duše, mala som v úmysle vymenovať aliasy, za ktorými stoja zlaté duše diskutérov, prípadne aj tie menej spriaznené. Chcela som vás menovite pozdraviť. Ale upustila som zo zámeru. Bol by to dlhý zoznam.    Verím, že tí, ktorých mám na mysli, vytušia... vlastne vedia, že som myslela práve na nich. Prosím, držte mi palec! Keď sa vrátim domov, bohatšia o dve titánové skrutky, ozvem sa.   Už teraz viem, že mi budete chýbať. Iné mi však neostáva, len si vás všetkých vziať v pomyslenom šuflíčku mysle so sebou. Môžem?   Prajem vám všetkým krásne dni, prajem veľa dôvodov na úsmev, pozitívnu myseľ a prúd každodenných radostí.   Do čítania, priatelia!       PS: foto - odborník, ktorému sa zverím do rúk   PS2: Dámy, všetko najlepšie k dnešnému MDŽ!   PS3: Džentlmeni, už ste obdarili božtekom (prípadne kvietkom....) a zablahoželali dáme svojho srdca?        foto zdroj : internet     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (87)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




