
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Rusina
                                        &gt;
                Camino Santiago de Compostela
                     
                 6. etapa do Compostely alebo osud premočených smradlavých ponožiek 

        
            
                                    22.4.2010
            o
            21:16
                        (upravené
                22.4.2010
                o
                22:44)
                        |
            Karma článku:
                9.42
            |
            Prečítané 
            1755-krát
                    
         
     
         
             

                 
                    Zobúdzam sa skoro, ešte je vonku šero. Využívam to a rýchlo sa balím.  Ako tieň opúšťam svoj dom s modrou bránkou cez jeho záhradu. Narážam v  nej na drobnú architektúru - obdĺžnikový visutý domček, ktorú som po  ceste videl snáď v každej záhrade. Jej funkcia mi je neznáma, ale  napadlo mi, že by sa v nej tiež celkom fajn spalo...
                 

                     Masť zabrala. Nie stopercentne, ale jednoznačne ranné rozchádzanie  nie je už také útrapné. Za chodu raňajkujem čokoládu, čo som si kúpil  ešte v Tui. Čoskoro stretám míľnik s označením 106,7 kilometra. Obľúbil  som si tieto míľniky. Vždy sú pozitívne, totiž zakaždým je na nich  menšia vzdialenosť do cieľa. Teraz ma vedú cez les. Samozrejme, po  nočnom odpočinku s len občasným prebúdzaním, začína svoj deň aj dážď.  Snažím byť rovnako vytrvalí ako on.   Ešte je ráno, keď obchádzam čosi ako odpočívadlo pre pútnikov. Pitná  voda, lavičky, prístrešky, záchod, dokonca bar. Dostávam v ňom ďalšiu  pečiatku ako dôkaz o svojej púti. Odtiaľ kráčam dolu kopcom a dlhou  rovnou cestou cez priemyselnú zónu. Niekde na jej konci ma predbieha  pútnik. Tiež sólo. Tiež sa snaží vytrvalo šliapať podobne ako vytrvalo  leje. Nedávame sme sa do reči. Len pozdrav: "Bom camino".   Cesta vedie znova lesom, dalo by sa mu pokojne pridať prívlastok  "daždový". Spolupútnik sa brodí blatom prvý. Na jednom úseku predbieham a  som to pre zmenu ja, ktorý testuje brodivosť blata. Nakoniec mi uniká.  Šľachy mi nedovoľujú kráčať jeho tempom.     Ešte pred obedom vstupujem do urbárnej zóny mesta Porriño. Niekde  tam míňam stý kilometer. Už len dve cifry. Popri ceste sa ma snažia  pritiahnuť najrôznejšie snack-bary a reštaurácie svojmi Pilgrim menu.  Ranná čokoláda ma síce energeticky zásobila, ale dieru v bruchu  nezaplnila. Ešte mám primálo kilometrov nohách aby som si robil pauzu,  ešte nie je čas obeda, a nakoniec, Pilgrim menu sú nad limit môjho  pilgrim-budget. Chce to moju overenú padariu so super pão. Hovorím si,  že to vydržím buď do centra mesta, alebo po prvú padariu.     Vstupujem do centra (mysliac tým historické budovy) a súčasne  objavujem padariu. Niet o čom. Super pão nemajú, tak kupujem obyčajný.  Ešte si kupujem pečivo s niečim vo vnútri. Málokedy viem, čo vlastne  kupujem. Rozhodujem sa podľa vzhľadu. Predavač sa pýta: "El Camino?"  "Si." Potom povie nejakú číslovku a za ňou: "kilometras Redondela" teda ďalšie mesto.  Portugalské číslovky do sto viem. Toto znelo ako päť. Overujem si  informáciu pomocou portugalčiny a piatich prstov na pravej ruke: "Cinco  kilometras?". Trikrát tesne za sebou zopakuje mojich päť prstov, čiže  pätnásť. Míľniky nedávajú predčasne nádej. Moje prebudenie do reality  muselo byť zrejme vidieť, lebo mi na to pridáva muffin grátis. Muffiny  ja rád. "Mucho gracia."   Pečivo konzumujem za chôdze. Hmmmm, bol to šťastný žreb. Pekne veľa  chutného mäsa vo vnútri. Dôležité bielkoviny. Zahnem ešte do  supermercado a kupujem španielsku verziu nutely. Samozrejme dvojfarebnú,  nech nejem stále to isté.   V centre sa pýtam na albergue pre ďalšiu dôkazovú pečiatku. Ľudia ma  navigujú k mostu cez rieku. Za ňou je novostavba albergue. Zvonka  vyzerá solídne, no viac sa nedozvedám. Je zavreté. Predpokladám, že ho  otvoria až v poobedných hodinách, keď sa dovalí ďalšia vlna pútnikov. Ja  zrejme plávam niekde medzi vlnami, keďže som nocoval na polceste medzi  jednotlivými albergue.   Plávanie za chvíľku nebude už len metafora. Silno leje. Skrytý pod  dáždnikom pokračujem ďalej a z Porriña prechádzam na vidiek.  Niekedy  poobede sa mračná na okamih roztrhajú a objavujem ďalšie pútnické  odpočívadlo niekde za obcou Mos. Ohradený zatrávnený pozemok, pitná  voda, lavičky, prístrešok a v strede toho novopostavená kaplnka  miestnymi "amigos de camino". Obedujem. Ako dvíham batoh, znova začína  liať. Aspoň mi nemokol chleba.     Dobieham skupinu mladých pútnikov (predbehli ma, kým som obedoval).  Majú celkom pomalé tempo, tak sa k ním pridávam. Kráčajú mĺkvo. Vlastne  to nie je až také nezvyčajné. Aj osamelý pútnik z rána bol mĺkvy a títo  tu aj napriek tomu, že sú skupina, kráčajú mĺkvo. Je to ten stav, keď sa  všetko sústredí na jedinú vec: el Camino. Nohy sa rytmicky posúvajú  vpred, oči ich strážia a len na križovatkách akoby len periférnym  videním vyhľadávajú žlté šípky ukazujúce smer, myseľ sa medzi  jednotlivými míľnikmi snaží vypočítať, koľko kilometrov ešte zostáva.  Šum dažďa ako zvuková kulisa. Treba sa doslova nútiť pozorovať krajinu,  alebo nad niečim premýšľať. Zapamätal som si Slovo, prirovnanie Božieho  kráľovstva k horčičnému zrnku (Lk 13, 18-19), ktoré som si prečítal  včera pred spaním. Bolo krátke. Občas som si ho len v duchu povedal a  tak som ho niesol zo sebou. Každý pútnik niečo nesie. Tak miestny ľudia  rozpoznávajú pútnikov a pomáhajú im na ceste.   V mĺkvej procesii dôjdeme až do mesta Redondela. V jej centre v  renesančnej budove sa nachádza albergue. Skupina tam končí, platia 5 eur  za postel a dostávajú pečiatku. Ja si pýtam len pečiatku. Chcem ešte  pokračovať ďalej.      Zastavenie využívam na ošetrenie šliach mastičkou a preväz sediac v  malej knižnici albergue. Niektoré brožúrky je si možno vziať zo sebou.  Samozrejme sú o Camine. Berem o portugalskej ceste, ktorú akurát  šliapem, ale aj ďalšie cesty. Francúzska cesta, severná cesta. Je ich  niekoľko. Už teraz som rozhodnutý, že ešte niekedy budem putovať do  Santiaga. Ach, prečo to len tlačili na ťažký kriedový papier.  Starostlivo si vyberám len tie najužitočnejšie. A potom nachádzam  ilustrovanú knihu s čínskym textom. Niektoré strany odfotím, ale chcelo  by to prefotiť takto celé a cez noc si nabiť baterky. Pokušenie. Je však  ešte skoro skončiť, ešte zostáva pár hodín svetla ktoré sa dajú  zameniť za kilometre. A tie kilometre treba násobiť dvoma, lebo ráno to  bude môj náskok. Dlhá, vytrvalá chôdza s čo najmenej prestávkami sa  stala mojou stratégiou, čo som pre zapálené šľachy prišiel o moju rýchlu  chôdzu. Teda aj portugalskú cestu si ešte zopakujem, s tým, že si  naplánujem nocľah v Redondele.    Opúšťam Redondelu prechodom cez kopec. Dobré je, že dážď asi  tiež odpočíva v albergue ako ostatní pútnici, a už mi neprší. Prechádzam  okolo pútnického prameňa s pitnou vodou.         Za kopcom je mestečko Arcade v zátoke. V jeho uličkách nachádzam ďalší  prejav sympatie miestnych obyvateľov k pútnikom         Cez Ponte Sampaio a ďalší kopec.       Tu dostávam odmenu za dnešný upršaný deň. Dúhu som síce videl veľakrát v  uplynulé dni (lebo mi aj veľa pršalo), ale táto bola akosi mimoriadna.  Vidieť strechu domu, z ktorej vychádza. Najprv len civím, potom  odhadzujem dáždnik ohnúc na ňom ďalší lúč a fotím.       Nejdem na návštevu do toho domu. Je to na druhej strane údolia, odkiaľ  som práve prišiel. Pokračujem do malej osady nad mestom. Začínam si  uvedomovať, že deň sa končí a ja som si nedoplnil zásoby vody. Mám len  na pár glgov, ktoré by som bol schopný hneď vypiť, ale teraz si ich  radšej šetrím. V osade nikoho. Slnko začína zapadať. Mením hlavnú  prioritu zo zohnanie vody na nájdenie miesta na nocľah. Tuším, že to  nebude ľahké, lebo terén je dosť zvlnený. Som rozhodnutý šliapať, kým  nenájdem vhodné  miesto. Dostávam sa až na vrchol kopca. Tvoria ho  balvany s kamenným krížom. Niet počuť zvuky civilizácie, som teda pekne hlboko v lese. Miesto sa mi páči. Je tu aj míľnik s presne  72 kilometrom, teda  som od rána prešiel 34 kilometrov. Rozkladám si  stan hneď pod balvanmi míľniku. Ošetrím si šľachy, povečeriam chleba s  nutelou (biela tejto nutely mi fakt chutí), symbolicky zapijem, aby mi  viac zostalo na ráno, kto ako ďaleko je to najbližší dom.  Zuby si v rámci šetrenia neumývam. Premočené super smradlavé ponožky vyhadzujem von zo stanu. Pravdupovediac, nosil som ich niekoko dní za sebou. Tuším celé tri dni. Aj keď boli ráno vlhké (suché boli asi len raz čo boli na radiatore v Rubiães ), som si ich naťahoval. Boli to totiž jediné hrubé ponožky, okrem tých, ktoré som mal do spacáku, ktoré mi neškrtili opuchnuté šľachy. Už je to však neznesiteľné, zajtra ich hodím do kontajnera. Ešte raz si spomeniem na Slovo o Božom kráľovstve a rýchlo zaspávam.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            10. etapa do Compostely alebo kto nemá v hlave, ten má v nohách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            9. etapa do Compostely alebo ako som sa neskoro v noci doplazil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            8. etapa do Compostely alebo pôstne putovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            7. etapa do Compostely alebo posledná večera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            5. etapa do Compostely alebo dom s modrou bránkou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Rusina
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Rusina
            
         
        martinrusina.blog.sme.sk (rss)
         
                                     
     
        Momentálne erazmus študent v Porte (Portugalsko)na fakulte Belas Artes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2020
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Camino Santiago de Compostela
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




