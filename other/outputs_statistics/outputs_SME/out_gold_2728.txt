

 V zákone o turizme sa navrhuje nová organizačná štruktúra v cestovnom ruchu. Citácia zo zákona: 
 Plnenie úloh v cestovnom ruchu zabezpečujú : 
 a)      Ministerstvo kultúry a cestovného ruchu  Slovenskej republiky (ďalej len „ministerstvo"), 
 b)      vyššie územné celky, 
 c)      obce, 
 d)      príspevková organizácia zriadená ministerstvom (ďalej len „agentúra"), 
 e)      krajské organizácie cestovného ruchu (ďalej len „krajská organizácia"), 
 f)       oblastné organizácie cestovného ruchu (ďalej len „oblastná organizácia"). 
   
   
 Oblastná organizácia je partnerstvo súkromného a verejného sektora, ktoré pôsobia v rovnakom regióne a "podnikajú" v rovnakom biznise - cestovnom ruchu. V praxi vznikali tieto oblastné organizácie už pred vznikom tohoto zákona - Klaster Liptov, Klaster Turiec, Klaster Orava. Stačí si na miesto slova klaster (z anglického cluster - zlúčenina, zhluk - poznáme hlavne z automobilového priemyslu) dosadiť oblastná organizácia. Klaster, alebo oblastná organizácia ako hovorí zákon, je združenie právnických ôsob - podnikateľov v CR, škôl, samospráv - ktorí vložia do organizácie svoj peńažný vklad (v prípade skôl know-how, pracovnú silu atď). Vznikne určitá suma peňazí, ktorá sa použije na rozvoj daného regiónu - spoločná propagácia regiónu, rezervačný systém, spoločné produkty, značenie v regióne atď. Zákon je prelomový v tom, že štát finančne podporí tieto klastre a to rovnakou sumou, akou podporili klastre samosprávy. Zjednodušene v Klastri Liptov na rok 2010 dalo mesto Ružomberok 1000€ a mesto Liptovský Mikuláš ďalších 1000€. Štát teda prispeje rovnakou sumou ako samosprávy - 2000€. 
 Treba povedať, že návrh zákona bol, aby štát podporil klaster (oblastnú organizáciu) rovnakou sumou, akú vyzbierajú všetci členovia, nielen samosprávy. Keď netečie, aspoň kvapká. 
 Ďalšou novinkou bude krajská organizácia, v ktorej sa budú združovať všetky oblastné organizácie v danom kraji a príslušný VÚC. V žilinskom VÚC by to boli oblastné organizácie Turiec, Liptov, Orava, Kysuce. Zo zákona o turizme: 
 Členský príspevok oblastnej organizácie krajskej organizácii je  najmenej 15% z členských príspevkov získaných oblastnou organizáciou od obcí a podnikateľských subjektov v kalendárnom roku. 
 Štát naoplátku podporí krajskú organizáciu rovnakou sumou, akou do nej prispel VÚC a jednotlivé oblastné organizácie. A tu padá kosa na kameň. Krajská organizácia je totiž umelou organizáciou, ktorej úlohy doteraz plnil VÚC. Je to zbytočný medzičlánok, ktorý je na malom slovensku absurdný. Výsledkom bude, že krajské organizácie budu ťažkopádne a neefektívne, mnohé oblastné organizácie do krajských ani nevstúpia(nakoľko im to zákon nedáva za povinnosť). Podnikatelia v cestovnom ruchu majú dosť starostí z prevázdkou, nemajú čas behať na zasadnutie oblastnej organizácie, potom na zasadnutie krajskej organizácie, potom samostatne rokovať so samosprávami aby dali väčší príspevok, nakoľko štát dá potom rovnako veľký príspevok, hneď odtial lobovať na VÚC aby dali krajskej organizácií väčší príspevok, lebo štát to opäť bude navyšovať o rovnakú sumu. 
 V skratke, je dobré, že sa takýto zákon prijal, ani náhodou však nieje dokonalý. Moje pripomienky: 
 1.) Zrušiť krajské organizácie - ich koordinačné, školovacie a iné úlohy bude plniť tak ako predtým VÚC. 
 2.) Presunúť peniaze z emisií, mýt, ochranárskeho benzínu a iných nezmyslov do zvýšenia podpory štátu klastrom. Štát by mal prispieť rovnakou sumou, akú vyzbierajú členovia klastru. 
    
   
 Záverom. 
 Cestovny ruch je také klišé. Každý vám povie, že ho treba podporovať, ale nikto nevie ako. Zákon o turizme je dobrý v tom, že sa pokúša zavádzať systém, ktorý tu tak dlho chýbal. Je treba tento zákon doladiť a konečne začať v cestovnom ruchu koncepčne pracovať. Verím, že sa klastre po slovensku rozšíria a že rozvoj regiónov je správnou cestou. 

