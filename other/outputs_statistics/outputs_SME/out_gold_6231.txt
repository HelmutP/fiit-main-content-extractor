

 Podľa článku v SME (http://ekonomika.sme.sk/c/5367435/unia-zachrani-euro-za-kazdu-cenu-vytvori-fond-so-750-miliardami-eur.html) bude celková výška fondu dosahovať 750 miliárd eur, z čoho 60 miliárd poskytne Európska komisia, 440 miliárd sprostredkujú štáty eurozóny a 250 miliárd pridá Medzinárodný menový fond. 
 Dá sa predpokladať, že podiel jednotlivých členov eurozóny sa vypočíta podľa ich kapitálového podielu v Európskej centrálnej banke. Týmto spôsobom sa totiž vypočítavala účasť krajín na pomoci Grécku. V prípade Slovenska tento podiel pri vstupe do spoločnej meny činil 0.6934% (http://www.ecb.int/ecb/orga/capital/html/index.en.html), ale kedže naša pomoc Grécku dosiahne 816 miliónov eur z celkovej sumy 80 miliárd eu poskytnutej členskými štátmi, jednoduchý výpočet ukáže, že naša kapitálová účasť v ECB sa vyšplhala na 1.02%. Pri sume 440 miliárd a zachovaní rovnakých pomerov, čo sa dá považovať za pravdepodobné, teda suma za ktorú bude Slovensko zodpovedať na novovznikajúcom fonde bude predstavovať 4 miliardy 488 miliónov eur. Na podporu predstavivosti by sa dalo podotknúť, že toto číslo: 
 
 presahuje čistý tok fondov z Európskej unie na Slovensko za celú dobu nášho členstva viac ako štvornásobne  
 je porovnatelné s projektovanými výdavkami Slovenska na starobné dôchodky na rok 2010  
 presahuje projektovaný výber daní z príjmu fyzických aj právnických osôb na rok 2010 skoro a jednu miliardu eur  
 
 Okrem toho samozrejme netreba zabudnúť, že Slovensko má aj 0.16% kapitálovú účasť v MMF (tam sme už tie zdroje ale dávnejšie poskytli, dalšie nebude pravdepodobne treba) a pravdaže platí aj do rozpočtu Európskej komisie, takže sa bude nepriamo podielať aj na tých dalších 250tich respektíve 60tich miliardách, ktoré poskytnú tieto inštitúcie. 
 Je dosť možné, že tieto zdroje nebudú musieť byť poskytnuté priamo fondu - môže to byť skôr záväzok ich požičať v prípade, že to fond uzná za vhodné. To sa ešte uvidí, keď budú zverejnené dalšie podrobnosti. 
 Či táto suma bude stačiť na zvrátenie súčastnej dlhovej krízy a či je táto cesta to správne riešenie sú už otázky pre iný článok, ale dúfam, že sa mi aspoň podarilo priblížiť ako sa na tomto projekte bude Slovensko finančne podielať. 

