
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Iveta Rajtáková
                                        &gt;
                Boli sme v...
                     
                 Bilboard pri Košiciach alebo: Tu sa už asi nezastavíme 

        
            
                                    17.3.2010
            o
            7:16
                        (upravené
                17.3.2010
                o
                7:22)
                        |
            Karma článku:
                10.86
            |
            Prečítané 
            3202-krát
                    
         
     
         
             

                 
                                                                      O tom, kedy sa pije Earl Grey, kde o tretej popoludní nevedia, či vám urobia dezert a o tom, či je „dark" biela čokoláda. Čiže, povedané slovami Pavla Satka, ďalšie malomeštiacke magoriny o štyroch hviezdičkách.                              
                 

                                                     Už keď sme sa vydali na cestu do Trnavy, na posledné Roxankine krasokorčuliarske preteky tejto sezóny, a tesne za značkou Prečiarknuté Košice, nám do očí udrel bilboard propagujúci WELLNESS Hotel Chopok v Demänovskej doline, štyrikrát ohviezdičkovaný, bolo rozhodnuté, že na spiatočnej ceste budeme znovu pokúšať osud.                                       Vraciame sa z Trnavy. Je skoré popoludnie, sobota, nikam  sa neponáhľame. Na hotel Chopok sme samozrejme nezabudli, no napriek tomu sme sa my, ktorých nepoučiteľnosť sa stáva legendárnou, rozhodli, že obedovať budeme predsa len niekde inde.                                        Na Moteli Gombáš pri dedine Hubová nesvietili žiadne hviezdičky, len elektrické OPEN na dverách. Tu nám určite neprinesú namiesto Parmigiano Reggiana eidam.                                     Naše trúfalé očakávania sa naplnili. Moje halušky aj Roxankine pirohy boli presne také, ako sme si ich vypýtali, teda bez stopy po masti a slanine, čo nie je, napriek tomu, že to pri objednávaní jedla stále zdôrazníme, až taká samozrejmosť, ako by sa mohlo zdať.                                     V tom najhoršom prípade, tak, ako sa nám to stalo naposledy na Spišskom salaši na Sivej brade prinesú na prvý pokus jedlo aj s týmito dvoma, výslovne neželanými ingredienciami, na druhý pokus, o dve minúty, to isté jedlo, z ktorého slaninu pozbierali.                                     Táto malá odbočka je v skutočnosti nekrológom za mojou citovou náklonnosťou k tejto krčmičke, ktorá sa zrodila v ešte v detstve, keď sme sa v nej z času načas zastavili na výlete s rodičmi....Vtedy to bola jedna miestnosť, v nej možno osem stolov a neďaleko salaša bola obora s jeleňmi. Jelene tam už dávno nie sú, salaš zniekoľkonásobil svoju kapacitu, ale moja láska k tomuto miestu húževnato odolávala týmto zmenám. Nič však nevydrží večne, a po niekoľkých ťažkých skúškach korunovaných opísanou skúsenosťou, sa vo mne niečo zlomilo a túto kapitolu svojho života považujem za uzatvorenú. Uznávam, táto krčma na to neskrachuje.                                              V každom prípade táto skúsenosť svedčí o tom, že o zábavu sa hosťom dokážu postarať nielen v krčme, ktorá má okolo mena viac hviezdičiek viac ako je vo Veľkom voze, ale aj v takej, ktorá hrá na ľudovú alebo pseudoľudovú (alebo ako nazvať tie odrhovačky, ktoré tam púšťajú) nôtu.                                        Takže ďakujeme Motelu Gombáš a mierime do Demänovskej doliny, kde sa usadíme do príjemných kresiel, budú tam hrať Vivaldiho alebo aspoň Sade a hýčkať nás tými najrafinovanejšími vydumaninami.                                        Začiatok vyzerá sľubne. Usadíme sa v Lobby bar s kozubom na jednej strane a akváriom na druhej. Áno,  je tu príjemene. Aj čašník prichádza pomerne skoro po tom, čo zaujmeme miesto vedľa kozuba. Podá nás síce iba dva ponukové lístky, ale ja sa s Roxankou podelím. Nie prvý raz.                                         Dezerty, dezerty, kde sú tu dezerty ? Nie sú. Tak asi majú aj iný lístok. A čašník je opäť pri našom stole.                                         Dezerty ? Áno, majú, ale.......nevie či nám ich kuchár urobí, lebo už pripravuje večeru. Ale, opýta sa. Sú tri hodiny popoludní.                                         Ani to tak dlho netrvalo, čašník znovu prichádza. Áno, urobia nám dezerty. A priniesol aj lístky. Dva, samozrejme.                                        Takže -  tri druhy sorbetu, palacinky, horúce maliny.                                           Aha. Tak ja si z týchto dezertov vyberiem čaj, Earl Grey, Juraj pivo a Roxanka ešte nevie.                                         Roxanka rozmýšľa a ja pozorujem dohasínajúci oheň v kozube. Earl Grey, Earl Grey  - niekoľkokrát nahlas zopakované - ma vytrhne zo zadumania.                                         Skupina štyroch čašníkov  na konci barového pultu je zjavne uprostred vážnej, zaujatej a predovšetkým hlasnej debaty, na tému - čo to vlastne chcem. Á, už nato prišli !                                          Earl Grey, Earl Grey ! Čo vymýšľa, to je čaj,  ktorý sa pije VČASNE ráno. Ráno ! Ten sa popoludní nepije. Určite, pije sa skoro ráno.                                          Medzi čašníkmi je zjavne znalec. Len neviem, či jeho kategorický názor vychádza z jeho prebohatých znalostí čajovej etikety, alebo z chabých znalosti angličtiny, ktoré mu pomohli zameniť slovo earl so slovom early, a na tomto fatálnom omyle založil celú svoju teóriu pitia Earl Greya.                                         Debata pokračuje. Assam..........toto majú. Nech mi ho prinesie. Áno, Assam majú. Porada na tému, aký čaj priniesť hosťovi  ( či hosťke ?) ktorá vymýšľa blbosti, a ktorej obsah mohli zaznamenať všetci, ktorých od diskutujúcej skupiny nedelila protihluková bariéra, skončila. Náš čašník sa vracia.                                        Ospravedlňuje sa, ale Earl Grey nemajú. Veď to už viem. Len by ma zaujímalo, odkiaľ čerpá jeho kolega tie prebohaté vedomosti o správnom čase pitia podávania tohto čaju.                                        Náš čašník nevie odkiaľ. Tak ma radšej poinformuje, že čaj majú cejlónsky.                                       Cejlónsky ? Nebola reč o Assame ?  Náš čašník nevie či majú Assam, trvá na tom, že majú cejlónsky. A ako sa volá ? Náš čašník si myslí, že cejlónsky čaj.                                       Vzdávam sa. Nech teda prinesie čaj cejlónsky.                                           Nášmu čašníkovi viditeľne odľahlo, chystá sa rýchlo využiť okamih, keď od neho nikto z nás nič nechce a ujsť. Neskoro.                                       Roxanka chce horúcu čokoládu. Počkať, počkať, hneď to nájde v lístku.  Tak, tu to majú. Creamy white a creamy dark. Takže horúcu čokoládu krími dárk .                                       Náš čašník pokračuje vo svojej one man show.                                       „ Bielu alebo tmavú ? "                                        Dieťa na okamih zaváha, či sa nepoplietlo, potom sa spamätá a odpovie:                                        „ Tmavú. "                                        To už je skutočne všetko, čo od neho dnes budeme chcieť. Už naozaj môže odísť.                                       O chvíľu je pri nás. Prinesie pivo, tmavú dárk krími čokoládu a cejlónsky čaj Assam.                                       Na druhý deň na internetovej stránke hotela zistím, že Lobby bar okrem iného poskytne hosťom chutné a lahodné čaje v širokej ponuke......                                       Už chýba len to, aby sa niekomu podarilo vyškoliť personál, že Assam nie je cejlónsky čaj, earl v názve čaju nie je to isté čo early, a dark, nie je biela čokoláda.                                        Dezert nakoniec predsa len bol. Ale nie v hoteli Chopok.               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (195)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ženy nevedia čo chcú
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            V Slovenskej sporiteľni alebo v Kocúrkove?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ad: Rodič a rodič alebo ukážka hrubozrnnej demagógie par excellence
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Súmrak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Buďte ako deti. Apdejt
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Iveta Rajtáková
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Iveta Rajtáková
            
         
        rajtakova.blog.sme.sk (rss)
         
                                     
     
        Som Iveta Rajtáková.Asi to podstatné, čo sa dá o mne povedať.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    132
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2336
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Služby
                        
                     
                                     
                        
                            Poézia alebo pocta Sufenovi
                        
                     
                                     
                        
                            Svet nie je children friendly
                        
                     
                                     
                        
                            ja tomu nerozumiem
                        
                     
                                     
                        
                            Videli sme
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                                     
                        
                            Boli sme v...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Z iného sveta
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog Iveta Rajtáková
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




