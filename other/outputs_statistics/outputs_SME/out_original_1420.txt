




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 Kol?n nad R?nom 
 
 Vydan? 25. 2. 2005 o 0:00 Autor: MIRIAM ZSILLEOV?
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 Bola to osada, ktor? na mesto pov??ila cti?iadostivos? jednej cis?rovnej. A cti?iadostivos? pom?hala mestu prekon?va? jedno storo?ie za druh?m. Jeho obyvatelia si zvykli na blahobyt a nikdy sa ho nevzdali, ?i u? to bol stredovek alebo priemyseln? revol?cia. Pre?ili Franc?zov, Prusov, Nemeck? r?u i bombardovanie spojencov po?as II. svetovej vojny. A od r?mskych ?ias si vraj zachovali v?rok: Kto nevidel Kol?n, nevie ?o je Nemecko. Kol?n nad R?nom zalo?il cis?r Claudius presne pred 1955 rokmi. 
 Na za?iatku nebol nikto bezv?znamnej?? ne? r?msky cis?r Gaius Julius Ceasar. Ten sa v roku 58 pred na??m letopo?tom rozhodol doby? cel? Gal?ciu. Vojensk? v?pravu viedol cez Por?nie, kde vtedy ?il keltsko-germ?nsky kme? Ebur?nov. T? sa najprv podvolili, ale nesk?r proti Ceasarovi povstali a zabili asi dev?tis?c jeho mu?ov. Ceasar bol z?riv? a vr?til sa s desiatimi l?giami. Ebur?nov vymazal z hist?rie. Por?nie ostalo po tejto trestnej v?prave takmer vy?udnen?. 
 Zmeni? sa to rozhodol v roku 38 pred na??m letopo?tom Marcus Vipsanius Agrippa, miestodr?ite? celej Gal?cie. Za?al s v?stavbou toho, ?omu sa dnes hovor? infra?trukt?ra: vykl?ovali lesy, budovali cesty a pevnosti. Na r?mske vojsko to ale bolo pr?li?. Potrebovali ?ud?, ktor? by miesto ?alej zve?a?ovali a br?nili. Na?li ich na druhej strane rieky R?n. Boli to R?mu naklonen? Ubieri. Osada pomenovan? ako Oppidum Ubiorum sa r?chlo rozrastala. Tu sa v roku 15 n?ho letopo?tu narodila vnu?ka Marcusa Vipsaniusa Agrippu a neskor?ia cis?rovn? Julia Agrippina mlad?ia, na ktor? dodnes Kol?n?ania s v?akou spom?naj?. 
 
 
 

 Agrippinu vydali do R?ma u? ako 13-ro?n?. Pre?ila svojich dvoch z?mo?n?ch man?elov i vyhnanstvo, aby sa z nej v 24 rokoch stala bohat?, atrakt?vna a cti?iadostiv? vdova, ktor? sa vydala za svojho str?ka cis?ra Claudia. Pr?ve pre t?to svadbu museli dokonca zmeni? r?mske man?elsk? z?kony. 
 Ako cis?rovn? po?adovala Julia pre seba adekv?tne mocensk? postavenie. Aby demon?trovala svoju moc, ?iadala od man?ela, aby jej rodn? osadu pov??il na r?mske mesto. Stalo sa a mesto dostalo n?zov Colonia Claudia Ara Agrippinensium. E?te dlho po z?niku R?mskej r?e sa mu hovorilo Agrippinenser. Meno Colonia, dne?n? Kol?n, sa presadilo a? v 9. storo??. 
 Nov? mesto pre??valo hospod?rsky rozmach. Okrem miestnych sa tu za?ali us?d?ova? aj Rimania z najrozli?nej??ch vrstiev: penzionovan? vojaci, obchodn?ci, ?radn?ci, d?stojn?ci. Okolo mesta postavili osem metrov vysok? a takmer tri metre hrub? m?r, ktor? mal odradi? nepriate?sk?ch Germ?nov od ?toku a z?rove? demon?trova? moc r?mskeho imp?ria. Vydr?al a? do stredoveku. Za jeho m?rmi sa budovali k?pele, divadl?, chr?my, ale aj kanaliz?cia, ?i vodovod. Miestni si r?chlo zvykli na prepych, ktor? Kol?nu nebol nikdy cudz?. 
 Od 12. do 15. storo?ia to bolo naj?udnatej?ie a najmajetnej?ie mesto v nemecky hovoriacom priestore. Pln? obchodn?kov, p?tnikov i zvedavcov. Nesk?r pribudli u?enci prvej mestskej univerzity, ktor? obyvatelia mesta zalo?ili v roku 1388. 
 Rozmach ale vystriedal ?padok a s hospod?rskym leskom str?calo mesto postupne aj svoju moc. V roku 1794 obsadili mesto franc?zske jednotky a Kol?n pri?iel o titul sv?t?, ktor? popri Jeruzaleme, Byzancii a R?me mohol nosi? od 12. storo?ia. Znovuzrodenie pri?lo v ?ase priemyselnej revol?cie. V?aka premyslenej komun?lnej politike odolalo Prusom a nesk?r Nemeckej r?i a za??valo rozmach, ktor? trv? dodnes. 
 Kol?n sa dok?zal pozviecha? aj po druhej svetovej vojne. Bombard?ry spojencov vtedy zni?ili 90 percent mesta a po?et obyvate?ov klesol z 800-tis?c na 40-tis?c. 
 Dnes Kol?n uspokoj? v?etk?ch. Milovn?kom umenia pon?ka desiatky m?ze? a stovku gal?ri?, turistom Gotick? katedr?lu - D?m, ktor? stavali 632 rokov, obchodn?kom svetozn?me ve?trhy a mlad?m "ka?d? de? jednu party". Aspo? to tvrd? report? v ekonomickom t??denn?ku, ktor? Kol?n nad R?nom korunoval na mesto Singles. ?ij? tu ?udia v?etk?ch farieb a presved?en?. ?ij? a nechaj? ?i?. Cis?rovn? Julia Agrippina by bola so svoj?m mestom spokojn?.  
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 20:00  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v?era st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 18:45  
Taliban za?to?il na citliv? miesto Pakistanu. Na deti
 
 Taliban zabil v ?kole v Pakistane najmenej 141 ?ud?, v??inou deti. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  Cynick? obluda: Medzi?asom na opustenom ostrove 
 Stroskotanci maj? ve?k? ??astie, ?e s? dvaja. Lacn? vtipy na ?rovni ich autora. 
 
 
 
 
 




 
 

 
SPORT.SME.SK  ?i ostanem v Slovane? Sp?tajte sa Maro?a, vrav? Vondrka 
 ?pekuluje sa o jeho odchode zo Slovana, hoci tr?ner ho v t?me chce. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






