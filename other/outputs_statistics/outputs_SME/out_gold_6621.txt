

 Sledujem občas pri dlhotrvajúcich dažďoch výšku vodných stavov na adrese: http://shmu.sk Okrem predpovede počasia je tam aj každú hodinu aktualizovaná výška hladín väčších potokov a všetkých riek na Slovensku. 
  
  
  
 Cestou naspäť som sa zastavil pri "našom" bociaňom hniezde. Od poslednej návštevy 
  
 pribudli na ňom navláčené konáre. 
  
 Chvíľu sa zdalo, že je opustené. No netrvalo dlho a priletel jeho skutočný majiteľ. Vtedy som medzi raždím uvidel aj hlavu jeho družky, ktorá tam nepozorovane sedí na vajciach. Dážď, nedážď, o budúce deti sa treba starať. 
  
 Posledné obrázky su z Váhu. Taký vysoký som ho už dlho nevidel a podľa predpovedí sa ešte zvýši. 
  
  

