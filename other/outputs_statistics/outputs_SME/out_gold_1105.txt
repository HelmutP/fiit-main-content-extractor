

Mesto Košice na súde prišlo o sedemsto hektárov svojho územia, s ktorým zrejme stratí aj 50 miliónov korún (1,66 milióna Eur) na majetkových daniach. Vyše dvanásťročný spor o to, či pozemky pod hutníckou spoločnosťou U.S. Steel patria do katastra obce Sokoľany alebo Košíc, právoplatne skončil. 
 
Koncom októbra Najvyšší súd zamietol odvolanie mesta proti rozsudku Krajského súdu v Košiciach, ktorý v novembri 2007 potvrdil rozhodnutie katastrálneho úradu o vrátení pozemkov do katastra Sokolian. Urobil tak bez toho, aby sa obsahom odvolania zaoberal s tým, že bolo podané neskoro. 
 
 
Sporné lehoty 
 
 
Právnik Košíc Daniel Adamčík pochybenie odmieta. Odvolanie podal desať dní po tom, ako si rozsudok krajského súdu prevzal na pošte, teda v lehote, ktorá je pätnásť dní. Najvyšší súd však uplatnil domnienku, podľa ktorej, ak si adresát neprevezme zásielku uloženú na pošte do troch dní, považuje sa za doručenú. "Mám listinné dôkazy, ktoré ju vyvracajú," tvrdí Adamčík. 
 
 
viac v Košickom korzári zo dňa20.12.2008 
 
 
Kto je Daniel Adamčík? 
 
 
Klikol som na internet meno a čo nevidím: KANDIDÁTNA LISTINA pre voľby do národnej rady Slovenskej republiky 17. júna 2006 za KDH. pč.31: Daniel Adamčík, JUDr., rok narodenia:1955, povolanie: Advokát, trvalý pobyt: Veľké Kapušany 
 
 
Žeby on? 
 
 
Takže čo sa stalo a takto to celé asi funguje v Košiciach a inde? 
 
 
Kolega Adamčík mi iste odpustí ale asi všetci uznáme, že v metropole východu je minimálne 100 advokátskych kancelárií, ktoré sídlia v Košiciach a nemajú problém každý deň mať v práci niekoho, kto preberá poštu. Keďže Najvyšší súd sa obsahom nezaoberal lebo odvolanie bolo podané neskoro vyvstáva otázka čo sa stalo. 
 
 
Netušíme, ostáva nám len úfať, že JUDr. Daniel Adamčík uháji svoju pozíciu. Bolo by totiž smutné, ak by sa súd kauzou ani nezaoberal, vzhľadom na premeškanie lehoty na odvolanie. 
 
 
„Smutné" tiež je, prečo si mesto Košice vybralo na obhajobu svojich práv advokáta z Veľkých Kapušian. Nič proti Veľkým Kapušanom, veď odtiaľ pochádza aj „boss Adamčo". Avšak aj za predpokladu, že kolega Adamčík by bol špecialista na danú problematiku, tak jeho job pre mesto Košice vzhľadom na aktuálny výstup kauzy je nateraz "hm,hm,hm" a vyvoláva pochybnosti ako vo funkčnej advokátskej kancelárii môže nastať stav problematičnosti pri doručovaní súdnych obsielok a nerieši sa merito veci - majetková ujma pre mesto Košice vo výške 50 miliónov ročne ale vec, kedy začala plynúť lehota na odvolanie.  
 
 
KDH v Košiciach a inde 
 
 
Faktom je, že po voľbách v júni 2006, keď boli KDHáci odídení z flekov v štátnej správe a primátorom Košíc sa stal v decembri 2006 člen KDH, zachránila KDHákov samospráva mesta Košíc. Tá pomaly ale isto vytvára nové a nové miesta pre väčšinou svojich, konkrétnejšie pre KDHákov a im blízkych. 
 
 
(Ne)štastie, že v komunálnych voľbách v Košiciach KDHáci uspeli, lebo to začína vyzerať akoby mali problém sa niekde zamestnať a chcú ich iba na magistráte. Aj z VUC museli odísť, tam je Zdenko a on si pre zmenu stiahol z mesta zas svojich. 
 
 
Nič proti KDHákom. V princípe sú to v mnohých prípadoch čestní a charakterní ľudia. Ak by pridali ešte schopnosti, vedomosti a ťah na bránku, tak na politickej scéne niet lepšej voľby. 
 
 
Bohužiaľ KDH je strana, ktorá sa uzavrela do seba a jej členovia si zo strany urobili živnosť na prežitie. Česť výnimkám. Prežívajú so smiešnou podporou obyvateľov a to napriek katolicizmu drvivej väčšiny občanov SR, čo im však stačí aby mali dobrý plat, napr. na košickom magistráte. Takže páni z KDH, kľudne si dávajte fleky svojím, lebo tak to proste chodí, ale TRVÁM na tom ako aj jeden múdry človek povedal: „Ak budem mať dvoch rovnako schopných a jeden je náš, tak job dostane náš." Ale ak náš bude slabší, nevidím dôvod, aby job dostal slabší. 
  
 
 
Všeobecnou otázkou teda zostáva, či je vhodné riešenie zamestnávať fajn človeka, ktorý je síce NESCHOPÁK a MOTÁK ale NÁŠ. 
 
 
Páni na košickom magistráte, domnievam sa bez ohľadu na čokoľvek, že nemáte právo moje DANE daromne premrhať či stroviť a aj týmto Vás verejne vyzývam: 
 
 
Žiadam Vás, aby pri používaní zdrojov mesta Košíc ste prihliadali len na schopnosti a znalosti subjektov, ktoré participujú na/pri obhajovaní práv mesta Košice.  
  
 
 
Táto výzva, je učinená v rámci občiansko právnej prevenčnej povinnosti predchádzania vzniku škodám s tým, že spoliehanie sa na nízku účasť voličov v roku 2010 a opätovné zachovanie si pozícii v Košiciach je možné riešenie, avšak nevylučuje to, že v prípade neschopnosti pri obhajovaní záujmov Košíc (napr. kauza Strelingstav - ohrozených 500 miliónov mesta Košíc a iných káuz) sa môžete ocitnúť v pozícii obdržania bezprecedentnej žaloby voči Vám ako konkrétnym osobám pre neschopnosť. 
 
 
So želaním úspešného roka 2009 zostávam s pozdravom a mám za to, že vyhováranie sa na finančnú krízu a pod. nebude považované za dostatočný dôvod na obhajovanie svojej neschopnosti. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
 
 
 
 
 
 
 
 
 
 
 
 
 


