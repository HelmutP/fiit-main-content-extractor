

  
  
  
 Firma s temer 130 ročnou tradíciou založená vizionárom Georgom Eastmanom sa zmieta v kŕčoch. Firma, ktorá dala svetu kinematografický film, kompaktný fotoaparát aj prvú digitálnu zrkadlovku stratila budúcnosť. Keď oslávila pred rokmi sté vyročie svojej existencie začala zrazu vrstviť jednu strategickú chybu za druhou. Odsunula digitalizáciu na druhú koľaj, tvrdošíjne verila v klasické filmy a spoliehala sa na „evanjelizáciu“ nových masových trhov v Číne a Indii. Verila príliš sama v seba, stále sa vracala akoby nostalgicky ku svojim koreňom a prestala aktívne formovať zákazníka. Chcela iba poctivo slúžiť. „You push the button, we do the rest...“ 
  
  
  
  
  
  
 (Nasnímané na film KODAK Gold 400 zrkadlovkou Olympus OM4Ti+Zuiko 24/1:2) 
  
  
  
  
   
 „There is a lot of more sexy things...“, varovali tí dvaja mladí lektori na školení v Londýne marketingových manažérov z celej Európy. Vysvetľovali, že zákazník sa slobodne rozhoduje o tom čo si kúpi za svoje voľné prostriedky. Že sa rozhoduje trebárs medzi dovolenkou pri mori a novým televízorom. A rozhoduje sa z pohľadu zavedeného výrobcu ako je Kodak, niekedy úplne iracionálne. Mnohé nové produkty z úplne iných odvetví totiž pomaly ale iste začali byť pre spotrebiteľa „more sexy“. Tí dvaja mladí chalani už vtedy niekedy v deväťdesiatom šiestom varovali, že napríklad mobilné telefóny odčerpajú práve tie voľné peniaze, ktoré predtým poctiví američania venovali na zvečnenie svojich detí, dovoleniek a zábavy. Firma, ktorá ovplyvnila svet, stratila víziu a z entuziazmu zostala mašinéria, ktorá zotrvačnosťou beží dodnes. Žije z hodnoty svojej značky a iba niekoľko málo profesionálov dokáže oceniť skutočnú kvalitu jej filmov, ktorú digitál dlho nedosiahne.  
  
 
 
   

 
 
 

 
  foto (C) Tibor Javor 
 

