
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Diana Ďurinová
                                        &gt;
                Súkromné
                     
                 listy 

        
            
                                    29.3.2010
            o
            8:22
                        (upravené
                29.3.2010
                o
                8:32)
                        |
            Karma článku:
                2.91
            |
            Prečítané 
            611-krát
                    
         
     
         
             

                 
                    Kedy ste naposledy dostali list? Nemyslím tým pohľadnice od rodiny na rôzne sviatky, ani zamietnutia žiadostí o zamestnanie, výmery za komunálny odpad a podobné úradné záležitosti. Ale obyčajný, rukou písaný list, poskladaný a vložený do obálky s vašim menom? Od kamaráta, priateľa, milenca...
                 

                 Ja si nepamätám. Neviem, ktorý môžem označiť za posledný. Keby som bola vedela, že na dlhé roky je to posledná pripomienka pomaly dožívajúceho spôsobu písomnej komunikácie medzi ľuďmi, bola by som si ho odložila a opatrovala ako najväčší poklad. Mala by som ho založený v obale, alebo zatavený do plastu, aby sa mi nezničil.    Prepásla som ten moment.   Kedysi som písala veľa listov a veľa ich aj dostávala naspäť. Stále som mala niekoľko stálych korešpondenčných kamarátov z rôznych oblastí svojich záujmov. Moje listy bývali dlhé, často som ich písala na etapy. Bavilo ma to.   (Občas aj nejaký ten zaľúbený, ale len pár sa dostalo k adresátovi, zväčša som ich zabila roztrhaním, alebo upálením.)        O pár dní som očakávala odpovede. Chodila som do schránky, nazerala či už niečo neprišlo. Tešila som sa na každý priateľský riadoček, na listy, na ktoré by som mohla promptne odpovedať a tak dať priechod svojej posadnutosti.    Medzi časom sa aj do môjho života votrel internet a aj ja, ako mnoho iných, fungujem na niekoľkých úrovniach virtuálnych realít. Všetko riešim mailovaním, skypovaním, cez „efbéčko“...    Slastný pocit otáčania kľúčikom a škrípavého otvárania schránky som nahradila klikaním hesla a rozčuľovaním sa nad pomalým naťahovaním sa údajov (rozumej dlhšie ako sekundu). Namiesto toho, aby som odpovede očakávala za pár dní, najradšej by som ich mala hneď, alebo aspoň do pár hodín. Pokiaľ neprídu do niekoľkých dní, už mám obavy, či sa adresátovi nestalo niečo zlé, alebo si namýšľam kadejaké hlúposti (v závislosti od toho, od koho očakávam odpoveď).       Premýšľam, či vydajú niekedy zbierku ľúbostnej alebo inej zaujímavej korešpondencie, ktorú budú tvoriť súčasní autori, umelci, alebo iné zaujímavé osobnosti. Priznajme si, že elektronické listy končia zväčša v elektronickom koši, keď sa naše skutočné pohnútky snažíme natlačiť do virtuálneho sveta.    Alebo si niekto bude všetko zálohovať a potom to vydajú pod názvom "Vybrané maily svetu", alebo "Výber z ľúbostného skypovania"... Príspevky často budú musieť prejsť korektúrou, lebo by to mohlo vyzerať asi takto...“Nedokazem prestat mysliet na tvoje usta. To ako si sa ma usmievala, ako si na mna pozerala vlozilo do mna nadej, ze sa raz mozno budem moct vpit do teba. Ze raz okusim vsetky vabive zakutia tvojho tela.“ Bez úpravy by to asi stratilo iskru. Ale to už by nebol originál. Ktohovie.        A z mojej schránky na chodbe vyťahujem šeky, faktúry, výmery za komunálny odpad...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Diana Ďurinová 
                                        
                                            tepečela bujakči, alebo každá rodina má to svoje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Diana Ďurinová 
                                        
                                            trapasník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Diana Ďurinová 
                                        
                                            deň po...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Diana Ďurinová 
                                        
                                            keď sa nosí drevo do lesa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Diana Ďurinová 
                                        
                                            beh, zniev a korytnačky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Diana Ďurinová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Diana Ďurinová
            
         
        durinova.blog.sme.sk (rss)
         
                                     
     
        Člen literárneho klubu Poet v Prievidzi
práce zvyknem podpisovať dievčenským menom Laslopová

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            literárne akcie
                        
                     
                                     
                        
                            výlety, relax, akcie- tipy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




