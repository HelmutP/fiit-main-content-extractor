
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Mihalik
                                        &gt;
                Nezaradené
                     
                 Eros Ramazzotti v Košiciach 

        
            
                                    3.3.2010
            o
            0:51
                        (upravené
                3.3.2010
                o
                1:14)
                        |
            Karma článku:
                10.51
            |
            Prečítané 
            3330-krát
                    
         
     
         
             

                 
                    Talianska hudobná legenda na svojom fantastickom koncerte 27. februára 2010 v Košiciach...
                 

                 
Vzácny autogram...Copyright (C) 2010 Tomáš Mihalik
   Eros Ramazzotti v Košicach       Je to už veľmi dávno, čo som Erosa počul po prvý raz, bolo to ešte okolo roku 1986, keď som ako 10-ročný počul v rádiu Adesso Tu... pieseň bola skvelá, pamätal som si aj meno speváka, ale potom dlho nič, až po revolúcii v roku 1990 prišla Se Bastasse Una Canzone... a odvtedy sa ho neviem zbavit. Ani nechcem :)   Že príde raz do Košíc, môjho rodného aj súčasného mesta, to som iba dúfal a sníval... Keď som sa to minulý rok dozvedel z internetu, skoro som odpadol a čakal, kedy to bude na ticketportali (kde asi inde,že?)... denne som chodil na stránku a čakal... na jeho stránke som videl, že bude aj v Prahe a bolo mi jasné,  že uvidím dva koncerty...       Tak som sa v predvečer 16. Novembra vybral do stovežatej, aby som si na 20-te výročie nežnej revolúcie pozrel koncert speváka, ktorý má sprevádzal od detstva. O2 Aréna bola plná na prasknutie, nakoniec aj Steel Aréna doma v Košiciach... Pražský koncert mal skladbu piesní z novších, ale pravdaže hlavne z osvedčených hitov... Eros patrí medzi to málo spevákov, ktorý bez problémov spieva každú pieseň vo svojej tónine, nepotrebuje pomoc halfplabacku... bez váhania spieva aj bez hudby – jeho špecialita je Fuoco nel fuoco – skrátka pán spevák...       Košické vystúpenie bolo pre mňa naozaj zážitkom, ktorý začal 3 hodiny pred jeho začiatkom. Šiel som autom na koncert, cesta ma zaviedla popri hoteli Hilton, kde som videl niekoľko fotografov (medzi nimi aj bývalých kolegov z novín) a striebornú dodávku. Povedal som si, že niet čo stratiť, tak som vystúpil a chcel som sa opýtať, kedy má asi vyjsť Eros von a ísť do Steel Areny. Lenže, keď som bol pri dodávku, oproti mi šiel on sám. Povedal som mu, „Eros, please, autogram“. Keďže som v ruke zvieral taliansku vlajku, neodolal... Opýtal sa pre koho to je a podpísal ju s venovaním... Medzitým som mu ešte povedal (anglicky), že som bol na jeho koncerte v Prahe, ale že toto je môj domov, preto tu nemôžem chýbať... na to mi povedal „I'll be back“, podal mi ruku a už musel ísť...   Košický koncert bol iný... samého ma to prekvapilo, ale zloženie skladieb bolo iné ako v Prahe. Úvodná pieseň bola samozrejme rovnaká – Appunti E Note, vyplývala z dramaturgie, keď pristál v kontajneri na javisku :) Zvyšok bola zväčša skvelá zmes najväčších hitov s novinkami z už nie tak nového albumu Ali E Radici... Osobne si myslím, že tento koncert bol oveľa uvoľnenejší, v Prahe síce samotné státie fungovalo perfektne, tancovalo a spievalo sa ako o život, ale v Košiciach dokázal roztancovať veľkú časť „sediaceho! hľadiska, ktorá si až do konca nesadlo...    Tu však sa však dokázal Eros natoľko odviazať, že 3 piesne si strihol bez kapely (v Prahe jednu), hral sa s hlasom, tónmi, tempom, zmenil formát na jazz, skrátka, zabával sa tak, ako celá hala. A keď zbadal asi meter odo mňa v úplne prvom rade asi 10 ročné dieťa s privretými očami, ľahol si na pódium a imitoval spánok, ale stále spieval :) ... nakoniec mu venoval svoju šiltovku...   Necelé dve hodiny koncertu bolo bezpochyby fantastickým zážitkom pre každého, kto tam v ten večer bol... svetové popová legenda, ktorá je verná svojmu štýlu, človek, ktorý si nikdy nepotrpel na maniere celebrít, ktorý keď mu odchádzal hlas, tak jednoducho ľudsky dal najavo, že nevládze (na konci už naozaj nevládal s hlasom, ale na výkone to nebolo ani trochu poznať), keď odišiel trubkárovi mikrofón, tak mu pohotovo „požičal“ svoj... tak tento spevák dokázal svojmu medzinárodnému publiku v Košiciach, že jeho 25-ročná úspešná kariéra nie je náhoda...    Mne ostáva len dúfať, že svoj sľub spred Hiltonu, že sa vráti, naplní čo najskôr...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Až mi smutno prišlo...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Vietor fúka na Montmartri
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Všetko najlepšie Majstre :-)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Nie domov...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Mihalik 
                                        
                                            Nesmeli ani plakať...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Mihalik
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Mihalik
            
         
        mihalik.blog.sme.sk (rss)
         
                                     
     
        Bývalý novinár so záujmom o všetko užitočné.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1434
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




