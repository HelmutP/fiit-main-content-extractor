

   
   
   
   
   
   
 Láska nerieši pravidlá 
 žije si svoj vlastný svet 
 neraz pripomína 
 rozmarného vtáka let  
    
  Je ako kruh 
 nepozná hranice... 
    
  Pádov sa nebojí 
  zahanbí  každého 
  kto proti nej zbrojí 
   
  Úsmevy rozdáva 
  zaseje túžby kvet 
 smútok odháňa 
  lepí ako med 
   
   
   
 **** 
   
 B.Yongsová (úryvok z knihy Človek ako dar): 
 Túžba po spoločnom vzťahu votkaná azda do každej duše robí radosti života sladšími a zjemňuje trpkosť strát... 
 Ešte aj západ slnka má nádhernejšie farby, ak máme niekoho, s kým ho môžeme pozorovať. 
   
   
 foto: zdroj internet 
   
   
   
   
   
   
   
   
   
   
   

