
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniela Nemčoková
                                        &gt;
                postrehy
                     
                 Lyžička medu pre teba 

        
            
                                    11.5.2010
            o
            10:51
                        (upravené
                11.5.2010
                o
                11:04)
                        |
            Karma článku:
                10.79
            |
            Prečítané 
            1633-krát
                    
         
     
         
             

                 
                    Keď som navštívila jednu príbuznú, stretla som sa s jej staručkou mamou, ktorá strácala zrak, chorľavela a slabla... A hoci celý život bola naučená pracovať, teraz bola odkázaná na pomoc svojej dcéry. Zostal mi v pamäti tento obraz: Vyše 80-ročná starenka sa pomaly prechádzala po dvore a nariekala: „Načo je na svete taký neužitočný človek ako ja? Nemôžem nič robiť! Prečo ma Pán Boh nevezme k sebe?" Cítila sa nepotrebná  a to ju pomaly zabíjalo. Lebo život človeka má zmysel, keď sa cíti dôležitý.
                 

                     O tejto dôležitosti píše Dale Carnegie (Ako získať priateľov a pôsobiť na ľudí). Odráža sa od tvrdenia veľkého amerického filozofa Johna Deweya, ktorý túžbu po dôležitosti pokladá za najsilnejší pud. Keby vraj naši predkovia nepociťovali takú pálčivú túžbu po dôležitosti, civilizácia by zanikla. Bez nej by sme žili ako v praveku.   Vynára sa mi ešte jeden obraz. Je z filmu o sv. Františkovi Asiskom, ktorý sa stal dobrovoľne chudobný, aby pomáhal ešte biednejším. S ich pomocou staval kostol, kameň po kameni. Všetkým rozdelil prácu, ktorú robili so zanietením. Dojalo ma, keď ukázali jednu ženu, ktorá nemala ruky ani nohy. Sedela v bezpečnom kútiku, z kterého mala výhľad na stavbu. Sv. František sa pozrel na ňu spýtavým pohľadom, či kamene ukladá správne. Keď prikývla, pokračoval ďalej. Takto sa stala naoko nepotrebná bytost dôležitá. Znamenalo to pre ňu zrejme veľa, lebo sa usmievala.   A Dale Carnegie hovorí: „Dosiaľ som nepoznal človeka, ktorý by nepracoval lepšie a usilovnejšie, keby sa mu namiesto kritiky dostávalo uznania."    Ja by som to nazvala pozitívnou motiváciou. Veď o tom hovorí aj známe príslovie: Na lyžičku medu nalákaš viac včiel, než na sud octu.   A nemožno si to zamieňať s pochlebovaním, lebo medzitým je rozdiel. Chválený by čoskoro zistil, že to nie je úprimné, a teda ani účinné...   Myslím, že toto pozná každý rodič, ktorý sa snaží dieťa k niečomu motivovať. Pozitívna motivácia urobí viac ako negatívna.   Keď som sa naposledy zhovárala s triednou učitelkou o problémoch so synom, bola som milo prekvapená jej prístupom. Bol totiž prítomný aj on. Učiteľka ho hladila po vlasoch a hovorila mu, čo treba urobiť lepšie.... Aj jeho to zaskočilo, lebo za prehrešky čakal sprchu výčitek... Ona ho však posadila do prvej lavice vedľa svojho najlepšieho kamaráta a povedala, že pokial bude dodržiavať dohodnuté pravidlá, môžu byť spolu. Takmer som nechápala, aký to malo účinok. On sa naozaj snažil! Zlepšenie nastalo okamžite...   A ešte jedna otázka? Prečo je v človeku tak hlboko zakorenená túžba milovať a byť milovaný? Nie je to vlastne túžba byť pre niekoho dôležitý?   Na záver už len jeden výrok: „Každý, koho stretnem, je v niečom lepší než ja. Preto sa mám od každého čo učiť." (Emerson)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Oplatí sa bojovať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Prípad rozviazaných šnúrok a poľského kamionistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Kráska z internetu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Bolesť - nepríjemná spoločníčka, dobrá učiteľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            "Dnes slnko vyšlo aj pre mňa"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniela Nemčoková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniela Nemčoková
            
         
        nemcokova.blog.sme.sk (rss)
         
                                     
     
        Som obyčajná žena-matka veľkej rodiny.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    117
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            deti
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            osobné príbehy
                        
                     
                                     
                        
                            príbehy iných
                        
                     
                                     
                        
                            socializmus
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Huljak
                                     
                                                                             
                                            Adriana Bagová
                                     
                                                                             
                                            Pavol Biely
                                     
                                                                             
                                            Ivana O Deen
                                     
                                                                             
                                            Marcela Bagínová
                                     
                                                                             
                                            Marek Ševčík
                                     
                                                                             
                                            Mária Kohutiarová
                                     
                                                                             
                                            Emília Katriňáková
                                     
                                                                             
                                            Peter Zákutný
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Oplatí sa bojovať
                     
                                                         
                       Babka, neseď stále za tým Facebookom
                     
                                                         
                       Prípad rozviazaných šnúrok a poľského kamionistu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




