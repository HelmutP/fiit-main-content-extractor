
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Giač
                                        &gt;
                O slovenčine
                     
                 O mäkkom „ľ". 

        
            
                                    20.6.2010
            o
            19:50
                        (upravené
                20.6.2010
                o
                19:55)
                        |
            Karma článku:
                4.61
            |
            Prečítané 
            1177-krát
                    
         
     
         
             

                 
                                    V svojom minulom článku som sa zveril so svojim „objavom"  v Slovenských  národných novinách.   Konkrétne s  článkom „Ypsilon naďalej zostáva s postrachom" od profesora Jána Findru. V tomto článku to zas bude niečo o mäkkom ľ.
                 

                                    Ján Findra,  v článku o ypsilone,  okrem spomienky na odbornú diskusiu  v šesťdesiatych rokoch minulého storočia,  zároveň obhajoval  jazykovedcov pred tou časťou verejnosti, ktorá tvrdí, že jazykovedci sú zodpovední  za to, že ypsilon prežil.       Diskusia, ktorá sa rozvinula k môjmu článku,   akoby  chcela názor pána profesora potvrdiť.  Len málo reagujúcich  si myslí, že nie je nutnou súčasťou slovenčiny.  A len jeden hlas sa dal pochopiť  tak, že by neboli potrebné žiadne vybrané slová. Okrem reagovania  na ypsilon sa objavili aj komentáre k mäkkému   „ľ „ a „ ä".  Z čisto logického hľadiska sú pojmy ako mäkké „i",  mäkké  „ľ" a či tvrdé „y" nezmyselné.  V slovenčine sa niekedy odpútavame od prísneho logického kritéria.  Napriek tomu,  alebo práve preto,  dochádza k lepšej zrozumiteľnosti.                   Ja pochádzam z nárečového prostredia,  ktoré nemá problémy  s používaním mäkkého ľ, alebo ä. Skôr naopak.  Je v ňom tendencia zmäkčovať aj tam, kde je to v rozpore so spisovnou slovenčinou (napr. mäd teda med).  Je kuriózne,  že ani počas vysokoškolských  čias v Bratislave som nebol  v kontakte s nikým, čo by mal problémy s „ľ". A to som sa stýkal  aj s trnavčanmi .  Až keď som prišiel  do  praxe.   Jedna  kolegyňa.  Hovorila spisovne,  ale Paľo bol pre ňu  Palo.     V roku 1989 ma preto šokoval  revolučný spevák   s výslovnosťou „slúbili".  Nebol  som  sám.  Niekto si ho podal s uverejnením správičky, že  údajne dostal ponuku naspievať pesničku: „Ľaľa Paľo, teľa ti je v ďateline".   Zdá sa, že hanba na celom Slovensku, teda aspoň v kruhoch uznávajúcich potrebu kultivovaného prejavu podľa kodifikovanej podoby.   Ale ako sa ukazuje, nie je to až taká hanba, kvôli ktorej by sa bolo treba plaziť popod kobercom.  V Slovenských národných novinách  (21/2010) sa Ján Findra zmieňuje:   V systéme slovenských hlások sa v Nauke  reči slovenskej neuvádza  mäkké ä ani mäkké ľ. Svoje hláskoslovné riešenie odôvodňuje  Štúr takto: „Mekuo  „ľ"  len u daktorých Slovákov je v običaji a už aj tam pomali zakapáva, odkjal ako aj preto že je to zvuk pre svoju rozťeklosť ňepríjemní a detinskí v čistej slovenčiňe lepšje keď sa viňechá". Podobné dôvody uvádza aj pri mäkkom ä.   Ján Findra  ide so svojimi poznatkami  ďalej. Tvrdí, že výslovnosť mäkkého ä je ojedinelá a skôr sa vyslovuje široké „e".  S ľútosťou konštatuje, že ani s výslovnosťou mäkkého „ľ",  to nie je všetko v poriadku.  Dokonca ani v prostredí, kde sa predpokladá kultúrny a kultivovaný jazykový prejav.  A tak  Ján Findra  polovážne nadhadzuje kacírsku otázku, či  Štúr nevystihol vývojové tendencie.    Ľudovít Štúr s citovaným názorom nepochodil  ani v tábore  svojich spolupracovníkov (M.M. Hodža a  J.M. Hurban) . A preto o šesť rokov neskoršie uzrela svetlo sveta kodifikácia,  pre ktorú bola štúrovská kodifikácia východiskom.  Ako všetci vieme alebo nevieme, norma bola publikovaná Martinom Hattalom v diele „Krátka mluvnica slovenská".   Niekde sa hovorí o hodžovsko-hattalovskej reforme, inde zas o opravenej slovenčine.  Napriek tomu, že skomplikovala  štúrovskú slovenčinu  sa v literárnych kruhoch rýchlo ujala. (Dôkaz toho, že niekedy je lepšie, keď je niečo komplikované. :)  Ak by  v budúcnosti nosili stúpenci   Štúra odznak s jeho vyobrazením,  na znak toho, že treba odstrániť  „ä", „ľ" , ja by som sa rozhodol pre odznak Hodžu a Hattalu. A Vy?   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (42)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Komunálne voľby prerušené na 38 minút.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Posledný a či ostatný?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Štyri metre štvorcové.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Starý Martin a Božie napomenutie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Paraglajdisti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Giač
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Giač
            
         
        giac.blog.sme.sk (rss)
         
                                     
     
        Vekovú hranicu, keď som mohol hovoriť, že už mám po mladosti a do dôchodku ďaleko, som už prekročil.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    70
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1079
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            O slovenčine
                        
                     
                                     
                        
                            Počítače
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




