

 
Maľovanie na sklo mi pripadalo zložité, kým som si to neskúsila. Premiéra celkom ostrá - darčeky pre svadobných hostí (nechcelo sa mi kupovať nejaké hotové neosobné veci). Z množstva druhov farieb na sklo som si vybrala riediteľné vodou, ktoré sa nemusia vypaľovať. Nie sú to tie farby, ktoré sa po zaschnutí dajú odlepovať napríklad z okna na zrkadlo. Farbičky som kúpila v obchode DaVinci, ale nemal by byť problém zohnať ich v obchode s papierom a kancelárskymi potrebami alebo v hračkárstve. K tomu tenký štetec, ďalší na lak a ten lak, samozrejme. A kontúrovacie pasty v tube - ja som si vybrala zlatú a striebornú, pretože som celkom presne plánovala dva motívy, jeden orámovaný zlatou a druhý striebornou.  
Prvý pokus bol na umytom zaváracom pohári, podľa knižky pre deti. Kreslila som priamo na pohár, bez predlohy alebo šablóny. V knižke bolo, že kontúrka sa dáva najprv, ale sa mi to nepozdávalo, lebo farba, hoci priehľadná, niekde kontúrovaciu farbu zamazala, inde nie a vyzeralo to čudne. Druhý pokus už bol ostrý, nakreslila som si lístky a hviezdice na papier, povystrihovala a nalepila som šablóny do umytých svietnikov (IKEA, 4 ks v krabičke, asi 40 korún). Svietnik som si nastokla na ruku a začala som - podľa plánu, striedala som svetlejšie a tmavšie zelené časti so žltými. Farbičky schli takmer hneď.   
 
 
 
 
prvý krok, zelená 
 
 
 
 
lístkový svietnik je vymaľovaný a okontúrovaný (farba sa vytláča priamo z tuby, nič zložité), hviezdicový schne 
 
 
 
 
hotové darčeky pre svadobných hostí, kontúrka je prelakovaná bezfarebným lakom - neskôr som prišla na to, že je najlepšie prelakovať celý obrázok, pretože vodou riediteľné farby sa môžu (nemusia) pri umývaní predmetu odlúpiť (našťastie to bol jednoduchý obrázok, ktorý som si namaľovala znova) 
 
 
 
 
Špeciálne svadobné poháre sme skoro nemali, Miloš ležal v špitáli, na objednanie nejakých gravírovaných a extra tvarovaných bolo neskoro a všetky na internete zhliadnuté sa mi videli príliš prezdobené. Asi tri dni pred svadbou sme kúpili baňaté poháre a pomaľovala som ich sama, aspoň sú osobité... 
 
 
 
 
svietnik na druhý pokus, prvý nevydržal umývanie (kontúrka vydržala), tak som ho prelakovala celý a spätne aj štvorlístky na svadobných pohároch 
 
 
 
Po vyše roku som sa vrátila k maľovaniu na sklo - vypálila som horko-ťažko sviečku Brise (príliš veľa vosku a krátky knôt, oheň sa topil v roztopenom vosku, ktorý som vyrezávala a vylievala), plechový držiak na knôt som odlepila od dna a mordovala som sa s vyškrabávaním lepidla. Odtrhla som potlačenú fóliu, umyla som svietnik a išlo sa na to. Plánovala som mozaikový vzor, takže nebolo treba ani šablónu, maľovala som priamo na sklo (svietnik zasa nastoknutý na ruke). Hneď ako som začala, som si zmyslela, že využijem všetky farby, aj bielu, ktorou som vytvorila dve hviezdy a tie som postupne obmaľovala obdĺžnikmi, štvorcami a tie som potom dopĺňala rôznymi -uholníkmi: 
 
 
 
 
 
 
Každá hviezda je orámovaná inou kontúrou, okolo striebornou farbou orámovanej hviezdy je zlatá kontúrka a naopak. To preto, že som sa nevedela rozhodnúť, ktorú kontúrovaciu pastu použijem - tak dostali príležitosť obe. Mohlo to zostať aj bez kontúrovania, ale takto to vyzerá trochu ako vitráž. Hala-bala vzhľad je zámerný, takže sa mi to robilo celkom rýchlo. Lak už bol tuhý, tak som ho riedila riedidlom a teda dlho schol - asi štyri dni.  
 
 
 
 
 
 
 
 
 
 
mozaikový svietnik pri okne 
 
 
lístkový svietnik s mozaikovým  
 
 
 
Zatiaľ posledný výtvor - potrebovala som plexichránič na masku, tak som si ho prispôsobila (iní si maľujú masky, ja mám čiernu, tak si pomaľujem aspoň chránič): 
 
 
 
 
 
 
 
nakreslené a nalepené šablóny, už aj vymaľované - bočné sú maľované zvnútra, predná je namaľovaná zvonku 
 
 
 
 
 
 
dvakrát namaľované, okontúrované a prestriekané niekoľkokrát bezfarebným autolakom - ten však kontúrku celkom nechránil, tak som musela použiť svoj starý tuhý lak so štetcom 
 
 
 
 
 
 
hotovo - plexichránič na maske; nalakované obrázky som ešte prestriekala tým autolakom a trochu zmramorovateli - nevadí, aj tak už je štvorlístok zamazaný šmuhou od puku 
 

