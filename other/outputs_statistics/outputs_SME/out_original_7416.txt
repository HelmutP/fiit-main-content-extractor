
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Helena Smihulova Laucikova
                                        &gt;
                Súkromné
                     
                 Matematika v poézii 

        
            
                                    27.5.2010
            o
            18:29
                        |
            Karma článku:
                5.18
            |
            Prečítané 
            1045-krát
                    
         
     
         
             

                 
                    To nejde dohromady, to je zlý nadpis. Akože nejde dohromady. Ide. Ty sa v tom  vyznáš ako hus v pive, Hela. Tak priznaj , čo si mala na maturitnom vysvedčení z matiky?!Dobre, dobre, ale som neprepadla!  Jedno majú matematické vzorce  a poézia spoločné, nemusíme im rozumieť, ale aj tak na nás pôsobia.
                 

                 Často rozmýšľam o veciach, ktoré neviem ani len definovať, nieto, aby som im rozumela. Taký  vesmír, poézia, matematika, tak tam môžem rozmýšľať do zbláznenia a výsledok?! Žiaden, ale aspoň si cibrím rozum, aj keď mi je jasné, že túto krížovku nikdy nevylúštim.   Nám laikom a diletantom je hej, veď to nemáme v popise práce, aby sme dosahovali výsledky.  Niekedy si poviem, funguje to aj bez vysvetlenia, tak čo. Na niektoré otázky aj tak neexistujú odpovede a možno je to tak lepšie.  Einstein,  veď hej , relativitu času prezradil, ale vraj prišiel ešte na niečo, ale to  „to" pre istotu nikomu nepovedal, lebo by sme  vraj mohli z toho zošalieť:)   Jedna vec ma, skutočne, veľmi zaujíma v  poézii. Ja sama som raz napísala jednu báseň, ale je taká originálna, že je nezaraditeľná a aj  nezverejniteľná.   Samozrejme je nezrozumiteľná aj pre mňa, ale ten pocit, keď si ju s hrdosťou a dojatím  čítam, mám presne taký istý ako keď som ju písala, pri plnom vedomí a triezvosti. Takže možno to aj je ozajstná báseň a som prvá, ktorá vymyslela tento spôsob písania.   A tu je súvislosť  matematiky a poézie, čo  ma zaujíma.  Či najprv niekto pekne poukladal  myšlienky s určitým počtom slov, spôsobom a príjemne zneli, preto  to niekto po ňom  zopakoval, a ostatní len doplňujú  slová podľa daného vzorca.  Čo iné je jamb, alebo haiku ,alebo čojaviemčo , aha, zase matematika s myšlienkou.   Možno by  najlepšiu báseň napísal počítač. Zadali by sme mu formu  aj obsah a vybavené. Ani by som sa nečudovala, keby to niekto skúšal , ale to by bol určite matematik s dušou poeta. Lebo základom každej básne  od jednoduchej detskej riekanky,  po zložité a umelé  tvary ,  je práve duša básnika ukrytá v básni.   Dobre je, keď čitatelia nájdu svoju spriaznenú básnickú dušu. Básne sú ako liek, nie sú na recept, ale keď vás bude bolieť duša vyskúšajte, nemusíte trikrát denne, ale aspoň raz začas, podľa potreby. Upozorňujem, že môžu byť aj silne návykové a niekto nemôže bez nich žiť.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Omrvinky z panského stola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Straka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Konzum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Výstava jesene
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Helena Smihulova Laucikova
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Helena Smihulova Laucikova
            
         
        smihulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som bojazlivá smelá žena 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    349
                
                
                    Celková karma
                    
                                                10.59
                    
                
                
                    Priemerná čítanosť
                    1267
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Dvere do minulosti
                        
                     
                                     
                        
                            psie rozprávky
                        
                     
                                     
                        
                            politika z kuchyne
                        
                     
                                     
                        
                            Magdine puzzle
                        
                     
                                     
                        
                            čítala som
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prvé snehové vločky
                     
                                                         
                       Králická tiesňava a vodopád
                     
                                                         
                       Jesenné Dudince
                     
                                                         
                       Keď zakape mobil
                     
                                                         
                       Straka
                     
                                                         
                       Zviazané v sieti - fotografie
                     
                                                         
                       Zdravá žena sa podobá na vlka
                     
                                                         
                       Chvíľa v záhrade - fotografie
                     
                                                         
                       Keď si vinár utiera slzu
                     
                                                         
                       Ebola v Španielsku
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




