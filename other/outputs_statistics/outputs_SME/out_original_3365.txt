
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Hirman
                                        &gt;
                Nezaradené
                     
                 Ad: Blokovacie zariadenia („papuče“) – ako to teda je (?) 

        
            
                                    29.3.2010
            o
            8:30
                        (upravené
                29.3.2010
                o
                8:16)
                        |
            Karma článku:
                7.36
            |
            Prečítané 
            2782-krát
                    
         
     
         
             

                 
                    Dňa 22.3.2010 som uverejnil blog http://hirman.blog.sme.sk/c/223505/Blokovacie-zariadenia-papuce-obmedzenie-vlastnickeho-prava.html. V ňom som rozvinul úvahu o použití tzv. „papúč" v dvoch rovinách pohľadu na túto problematiku. Právnu  rovinu "papuča" versus ústava a nie menej podstatnejšiu rovinu,  skôr filozofickú. Jednu z nich som ukončil s tým, že podľa mojej mienky je použitie "papúč" v rozpore s ústavou. Nie som jediný, kto takto uvažoval alebo uvažuje. A keď sa povie/napíše „A" a človek sa dozvie aj „B", patrí sa o to „B" podeliť aj s ostatnými.  Teraz tak činím.
                 

                 Ako som uviedol, sú dve roviny pohľadu.  Prvou rovinou je spomenutá rovina právna. Ozval sa mi pán pod pseudonymom  „hank", ktorý ma nasmeroval  na rozsudok Ústavného súdu Slovenskej republiky (ÚS SR) z roku 1997 (II. ÚS 8/97). Vtedajší Generálny prokurátor podal podnet na ÚS SR, v ktorom namietal vtedajšie ustanovenie o použití „papúč"  pre obecnú políciu.  Táto právomoc totiž bola daná obecnej polícii prostredníctvom všeobecne záväzného nariadenia., a nie zákonom ako je tomu dnes. Okrem iného v tejto súvislosti namietal aj možný rozpor nasadzovania „papúč" s ústavou, presnejšie tiež s čl. 20 ods. 4 (takže v „duchu" príspevkov niektorých „tieždiskutérov" rovnako uvažoval "nezmyselne," prípadne ako „vychcálek",ako „primitívny sebec", ako ****** a iné..v tom prípade ma teší, že úroveň môjho uvažovania je v ich očiach na úrovni uvažovania generálneho prokurátora). Ústavný súd, okrem iného, v tejto veci uviedol, že:   "Použitím technického prostriedku na zabránenie odjazdu dopravného prostriedku sa vlastníkovi proti jeho vôli znemožňuje užívanie jeho majetku." Avšak, ako ďalej uviedol: „Povinnosť, ktorá sa ukladá ako dôsledok protiprávneho správania vlastníka a vlastníkovi sa ňou ukladá nútené obmedzenie jeho práv vyplývajúcich z práva vlastniť majetok, nemá povahu obmedzenia vlastníckeho práva podľa čl. 20 ods. 4 ústavy.", a ďalej „Obmedzenie vlastníctva, ktoré namietol navrhovateľ, nepredstavuje zásah do ochrany vlastníckeho práva podľa čl. 20 ods. 4 ústavy. Má povahu obmedzenia práva vlastniť majetok podľa čl. 20 ods. 1 ústavy." Po slovensky, že použitie "papúč" nie je v rozpore s ústavou, teda nie je skutočne obmedzením vlastníckeho práva.   Taktiež ma ten istý prispievateľ upozornil  na fakt, že dané vozidlo stojí na parkovacom mieste protiprávne (je to priestupok), a teda stojí na zákaze zastavenia/státia. Vychádzať možno z úvahy, že §25 ods. 1 píše: Vodič nesmie zastaviť a stáť ...písm. o) na platenom parkovisku, ak vozidlo nemá zaplatený poplatok za parkovanie. Teda ide o zákaz zastavenia a státia, a obecná polícia má právo podľa §16a v takom prípade použiť "papuču".   Nielen v tejto diskusii zarezonovala pochybnosť, či môže obec/mesto ako vlastník komunikácie zveriť  právomoc správy tohto vlastníctva formou prenájmu na iný subjekt bez zákonného splnomocnenia, a preto je parkovné, a teda aj "papuča", v rozpore so zákonom, prípadne s ústavou.  V kontexte ďalších nálezov, napr.  PL. ÚS 6/01, vzniká iná úvaha, či vyššie obmedzenie vlastníckeho práva vlastníka dopravného prostriedku použitím "papúč" platí aj v prípade postihu nezaplateného parkovného, ktorého výber je zverený správcovi komunikácie, ktorým sa stal iný (súkromný) subjekt, ako vlastník komunikácie. Teda iný ako obec/mesto/štát. Teda obmedzenie vlastníctva v zmysle čl. 20 ods. 1  nemožno uplatniť v prípade vlastník versus správca. V prípade Košíc však k tomu došlo navyše bez  verejnej súťaže, a teda pravdepodobne protizákonne. A keďže je výber  parkovného realizovaný na protizákonnom základe, môže sa niekomu javiť  protizákonným aj ukladanie sankcií.  No existuje prezumpcia správnosti, že rozhodnutie orgánu je právoplatné kým sa nepreukáže opak. A zákonnosť výberu správcu alebo vôbec zákonnosť zverenia správy obcou/mestom opäť nemá nič spoločné s faktom zákazu zastavenia a státia na platenom parkovisku bez úhrady.   Rovina  filozofická, ostáva otvorená, preto ju pripomeniem. "Papuča" je   teda v súlade s ústavou, no je aj naozaj vhodným zabezpečovacím   prostriedkom? Páchateľom  priestupku je  vodič a nie majiteľ auta, pričom  nemusí ísť vždy o jednu  a tú istú  osobu. Vlastník je však zodpovedný za  svoje vlastníctvo.  Zmysel/význam   "papuče" ako prostriedku zabezpečenia vyššej  pravdepodobnosti identifikovania priestupcu a  následného výberu pokuty  je jedna vec. Správna. Aj keď napríklad správca  výberu parkovného za  svoju činnosť v tomto prípade aj tak príjem nemá,  pretože iné auto tak  či tak aj po nasadení "papuče" na danom mieste  nezaparkuje a parkovné  nezaplatí.  Pokuta je príjmom obecného rozpočtu.  Naďalej však ostáva  riziko nebezpečenstva vzniku možnej škody na  majetku  (samotného  zablokovaného auta alebo na inom majetku) alebo na  živote zablokovaním  auta, resp. neodstránením tejto prekážky, ktorú auto  tvorí zastavením a  státím na zakázanom mieste. Naďalej ostáva otázka  subjektívnej  zodpovednosti za vznik tejto škody a určenia podielu  zodpovednosti,  prípadne náhrady škody. Vodič? Majiteľ auta?  Obecní/mestskí policajti?  Nikto? Pravdepodobnosť, že dotyčný priestupca,  vodič, už nezaparkuje na  zákaze je veľká a výchovne prínosná. No v  prípade škody, trvalého  poškodenia zdravia alebo nebodaj smrti kvôli  včasnému neodstráneniu  prekážky, keďže pri záchrane, hlavne života, ide  zväčša o každú  minútu/sekundu, je to malý prínos. Príchod polície k  "papuči" vždy trvá  nejaký čas a jeho trvanie je vždy relatívne krátke  alebo relatívne  dlhé. Následné hľadanie a určenie vinníka neodbijeme len  suchým  konštatovaním, že **** zaparkoval tam, kde nemá. Tomu ****,  resp.  majiteľovi auta, ak by nezablokovali auto "papučou", vodič alebo   majiteľ auta by včas odišiel. Život by bol zachránený, pokuta zaplatená a   výchova k parkovaniu zabezpečená. Ak by včas vodič neodišiel aj bez   "papuče" plne by zodpovedal sám a nerozdielne za následky. "Papuča" je   teda v súlade s ústavou, no je aj naozaj vhodným zabezpečovacím   prostriedkom?   Teší ma, že som v prípade ústavnosti "papuče" neuvažoval zle, i keď v tomto kontexte som 13 rokov meškal, ale ako sa vraví, nikdy nie je neskoro. Na druhej strane niektoré témy sa cyklicky opakujú a vzbudzujú myšlienkové vášne, ako napr. táto, ako som sa presvedčil. A vyvolala inú úvahu v kontexte danej problematiky. A keďže som sa  stretol  s náznakmi na možný nesúlad s ústavou, ktorý naznačovala aj odborná verejnosť, to ma podnietilo a podnecuje k týmto úvahám.   Ďakujem, pán prispievateľ  „hank", za nasmerovanie k možnej odpovedi :-)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (40)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Hirman 
                                        
                                            Dúhová absurdnosť rovnosti (Ad: Rovné právo na manželstvo)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Hirman 
                                        
                                            Poslanci nie sú tajní agenti. Či?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Hirman 
                                        
                                            Hrabko Leško Procházka a ich slovný mariáš.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Hirman 
                                        
                                            Blokovacie zariadenia („papuče“) – obmedzenie vlastníckeho práva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Hirman 
                                        
                                            Katka odpusť našim sudcom, odpočívaj v pokoji
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Hirman
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Hirman
            
         
        hirman.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolské vzdelanie získané na Fakulte Verejnej správy UPJŠ KE. "Malý" doktorát som získal na tej istej fakulte z verejnej politiky a ústavného práva. Momentálne aj poslanec miestneho zastupiteľstva.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2335
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




