

 Príbeh Rapa Nui sa odohrával ďaleko a dávno. Duch Rapa Nui ale žije okolo nás i v nás. 
 V roku 2004 potvrdil slovenský tím projektu CORINE Land Cover 2000 po analýze satelitných snímkov, že od roku 1990 do roku 2000  reálne zmizlo na Slovensku 50,6 km2 lesa. 
 Začalo to misiou Svetovej Banky, ktorá v roku 1993 so slovenskou vládou bez akejkoľvek účasti verejnosti rokovala o projekte „Ekologické obhospodarovanie lesov“. V ňom odporúčala ako hlavný liek na dlhodobo stratové slovenské lesníctvo, zvýšiť ťažbu dreva zo 4,4 mil. m3 v roku 1991 na 11 mil. m3 v roku 2030 s následným zvyšovaním v ďalších rokoch. Novely lesného zákona a zákona o ochrane prírody, ktoré sa od skončenia misie pripravili a schválili, toto zvýšenie umožnili predovšetkým zvýšením ťažby v našich chránených územiach.  
 V Národnom parku Poloniny zväčšili ťažbu takmer trojnásobne, zo 75 000 m3 ročne na 220 000 m3 ročne. V Národnom parku Nízke Tatry zväčšili ťažbu viac ako dvojnásobne, napríklad v piatich lesných hospodárskych celkoch z 553 099 m3 na 1 184 276 m3 počas najbližších desiatich rokov! V pohorí Čergov, v území európskeho významu, zväčšili ťažbu trojnásobne, napríklad v lesnom hospodárskom celku Sabinov zo 187 920 m3 dreva za posledných desať rokov, na 522 037 m3 dreva v najbližšej desaťročnici. 
 „Pokoj občania, pokoj“, vravia lesníci.  
 „Všetko je úplne v poriadku, dokonca rozloha našich lesov sa neustále zväčšuje.“ 
 Je to skutočne tak? Je skutočne všetko v poriadku? 
 Známym dôsledkom zlého lesného hospodárenia sú lokálne povodne. Ich vznik závisí nielen od dostatočných zrážok ale aj horninového podložia a geomorfológie povodia. V pohoriach, ktoré sú flyšové, zložené z pieskovcov a bridlíc, majú zásadný význam pre zadržiavanie zrážkových vôd lesy. Pieskovce s bridlicovými platňami totiž sami o sebe veľmi zle prevádzajú povrchový odtok, ktorý vzniká pri veľkých zrážkach, do podzemných vôd. Na takejto hornine sú predovšetkým staré lesy s bohatou živou lesnou pôdou darom z nebies. 
 Flyšovými pohoriami sú u nás predovšetkým pohoria severovýchodného Slovenska, pre mňa osobne má zvláštny význam Čergov. 
  
  Jeho hlboké jedľovobukové lesy, v ktorých som vyrastal, ma naučili podstatne viac ako ktorýkoľvek učiteľ v mojom, živote. Moji rodičia, starí rodičia i prastarí rodičia žili v kotli prastarých lesov, uprostred ktorého sa zlievali stovky horských potokov do horskej riečky Ľutinka. Dedinka sa volá Olejníkov a v posledných rokoch sa stala nechceným pozorovateľom dôsledkov lesnej ťažby na kolísanie prietokov Ľutinky. 
 Zrážky na území Slovenska pomerne precízne zaznamenáva Slovenský hydrometeorologický ústav v Bratislave a tak nie je problém pozrieť sa do dažďovej minulosti Čergova. Zaujímavé sú hlavne dažde trvajúce aspoň päť dní, na väčšom území, žiadne nepredvídateľné lokálne prietrže, kde množstvo spadnutej vody sa dá len veľmi hrubo odhadovať.  
 16. až 20 júla 1949 spadlo do povodia Ľutinky, veľkého 50 km2, 119 milimetrov zrážok, 
  
  25. až 29. júna 1958 108 milimetrov, 
  
  27. júna až 1. júla 1973 99 milimetrov. 
  
 Ale 8. až 12. júna 2005 97 milimetrov, najmenej zo sledovaného obdobia. 
  
 Zrážku od 30. mája do 4. júna 2006 ešte nemáme spracovanú, ale situácia vyzerala veľmi podobne ako v roku 2005. V roku 2007 sa takáto meteorologická situácia nevyskytla. 
 Dôsledok týchto „monzúnových“ dažďov bol na Čergove v roku 2005 podstatne horší ako napríklad v roku 1973. Pri menších zrážkach ako v minulosti padla cesta vedúca Olejníkovom. 
  
 V roku 2006 sa to zopakovalo. Cesta opäť zmizla. V suchom roku 2007 pršalo poriadne len jeden deň, a starosta Ľutiny, dediny pod Olejníkovom, musel začiatkom septembra vyhlásiť povodňové ohrozenie oblasti.  
 
 
  
 Čím to je, keď množstvo vody spadnutej z oblohy sa oproti minulosti nemení, ba klesá? Čo sa v krajine zmenilo? Geomorfológia? Nie. Hornina z ktorej je vystavaný Čergov? Nie. Zrážky? Nie. 
 Tak čo? 
 Pozrime sa na letecké snímky Čergova od roku 1949 a zamerajme sa na náš „kotol“, kde sa horské potôčiky zlievajú a vytvárajú horskú riečku Ľutinka. 
  
  
 Už pri letmom pohľade je jasné, že to čo sa za tie roky zmenilo, je LES.  To všetko čo je na obrázkoch je podľa lesníckych štatistík les. Les veľmi rozdielnej kvality, les, ktorý v konečnej fáze hospodárskeho spracovania si už názov les ani nezaslúži. Družicové a letecké zábery túto premenu jasne podchycujú. 
 Od roku 2003 sa začal prudko uplatňovať nový lesný hospodársky plán (zvýšenie ťažby z 187 920 m3 dreva na 522 037 m3 dreva za desať rokov) a výsledky je hneď vidno. 
 Rapa Nui je veľmi, veľmi ďaleko a udalosti na ňom sú už dávno zabudnuté. Čo si myslel polynézsky drevorubač, keď rúbal posledný strom?  
 „Aspoň mám nejakú prácu?“ 
 Čergovské jedľobučiny sú ešte tu a teraz.  
 Čo si pomyslí čergovský drevorubač až mu na prelome mája a júna zaleje Ľutinka dom? 
 „Aspoň mám nejakú prácu?“  
 
 
 
  
 
 
 
 
 
 

