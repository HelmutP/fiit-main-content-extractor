

 Neviem ako vy, ale ja si v poslednom čase púšťam hudbu prevažne z počítača, z čoho musia mať veľkú radosť (a majú) najmä moji kolegovia, ktorí ju tým pádom musia počúvať spolu so mnou. Ak nejakú pieseň vo svojej fonotéke nemám, tak si ju vyhľadám na Youtube. Nedávno mi napadlo, že by nebolo zlé zostaviť rebríček najpopulárnejších slovenských skladieb na internete a tak som sa do toho pustil. Najskôr som začal vyhľadávať tých najznámejších interpretov a následne ich najpopulárnejšie hity. Mohlo sa teda stať, že nejaký spevák alebo skupina by mali mať vo výslednom poradí aj viac skladieb, ktoré mali väčší počet klikov, než ostatní umelci, ktorí sa do rebríčka dostali, ale na druhej strane len takto mohol vzniknúť objektívny obraz, akú majú najpopulárnejší slovenskí speváci, speváčky a skupiny obľúbenosť u poslucháčov. 
 Totálnym prekvapením pre mňa však bolo, keď som pri tomto systematickom hľadaní narazil na pesničku Mandarinka Darinka v podaní Michaely Paštékovej, ku ktorej nejaký fanatik urobil aj veľmi nápaditý videoklip, ktorý by som si dovolil odporúčať do vašej pozornosti. Dovolím si tvrdiť, že tejto slovenskej pesničke by vďaka spomínanému klipu rozumeli aj deti z Grécka, Grónska, Gruzínska, Ghany či Guatemaly. A ako sú na tom interpreti, ktorí v posledných rokoch stabilne obsadzujú popredné umiestnenia v anketách ako sú Slávik či OTO? Legendou medzi kapelami je Elán, ktorá je veľmi populárna je aj na internete. Elán má až tri pesničky s klikanosťou nad 1 milión - Jedenáste prikázanie, Láska moja a svoj starý hit Neviem byť sám. Do rebríčka som zaradil všetky tri skladby a rovnako som postupoval pri tvorbe skupiny Desmod, ktorá však prekvapujúco má len jednu pesničku, ktorú si ľudia pustili viac než milión krát a to skladbu Vyrobená pre mňa. Ich ďalší hit Zhorí všetko čo mám si pustili návštevníci stránky 816-tisíc krát a duet Pár dní v podaní Kulyho a Zuzany Smatanovej mal "len" 606-tisíc kliknutí. Aj tak je to však najúspešnejšia pesnička v podaní niekoľkonásobnej zlatej Slávice, ktorá inak so svojími vlastnými pesničkami na internete príliš neboduje a tak je možné, že jej víťazstvá v rôznych anketách sú naozaj skôr výsledkom maximálného úsilia jej funklubu, než nejakej všeobecnej obľúbenosti u ľudí. Pomerne sklamaný som bol aj z hitov košickej formácie No Name, ktorej najvychytenejšou pesničkou je Ty a tvoja sestra, ale pozretá bola "len" 293 673 krát. A len o čosi lepšie je na tom spoločný song s kapelou Chinaski Na na na, ktorý si pustilo o 30-tisíc ľudí viac. Podobne na tom je aj prešovská skupiny IMT Smile, ktorá najviac zabodovala s piesňou Mám krásny sen. Pomerne málo populárny na internete je aj Meky Žbirka, ale do rebríčka sa napokon dostal so skladbou 22 dní. Speváčka Misha mala smolu, pretože jej sólo skladbu Čiarky, ktorú videlo približne pol milióna ľudí, nechalo vydavateľstvo EMI z youtubu stiahnuť, takže som ju do rebríčka nemohol zaradiť. Najväčším ale negatívnym prekvapením bol pre mňa úradujúci zlatý Slávik Peter Cmorik, ktorý skončil v mojom rebríčku na poslednom mieste. Jeho najsledovanejšie video pochádza ešte z čias, kedy bol v SuperStar a ide o jeho interpretáciu skladby Bad day od Daniela Powtera, ktorú si pozrelo 189 231 ľudí, ale bohužiaľ jeho vlastné skladby sa veľkej obľube u poslucháčov netešia. Ujala sa jedine pieseň Dážď, ale oficiálny klip, ktorý bol prehratý viac než 200-tisíc, dalo vydavateľstvo Sony Music Entertainment z youtubu stiahnuť. Rovnako dopadla aj Tina s piesňou Viem, že povieš áno, ktorá by mala šancu dostať sa vysoko nakoľko si ju stihlo pozrieť viac než 600-tisíc fanúšikov jej hudby. Jednoznačnými kráľmi webu sú páni zo skupiny Horkýže slíže, ktorých videoklipy patria k najvyhľadávanejším na internete a pravdpodobne by zvíťazili s pesničkou RNB Soul aj v mojej súťaži, keby aj oni nedoplatili na to, že oficiálny klip k tomuto songu nechalo ich vydavateľstvo z youtubu stiahnuť.  Pomerne vysoké prehrávanie mala aj väčšina piesní zo súťaže SuperStar, ale do rebríčka som zaradil len hymny finalistov, pretože pri nich išlo o pôvodnú domácu tvorbu. 
 Rebríček najpopulárnejších skladieb na stránke www.youtube.com k 12.3.2010: 
 1. Michaela Paštéková - Mandarinka Darinka  2 224 490 
 2. Tina a Rytmus - Náš príbeh                         2 187 421 
 3. Jana Kirschnerová - Pokoj v duši                1 982 701 4. Desmod - Vyrobená pre mňa                      1  738 475 5. MC Erik a Barbara - Keď príde láska k tebe 1 546 165 6. Kristina -  Vráť mi tie hviezdy                        1 501 550 7. Horkýže slíže - Silný refrén                          1 484 100 
 
 8. Kontrafakt - Moji ľudia                                 1 421 179 
 9. Lobo - Suzanna                                           1 398 162 10. Elán - Láska moja                                      1 222 439 
 11. Rytmus - Cigánsky sen                              1 153 270 
 12. Elán - Jedenáste prikázanie                      1 125 669 
 13. Dara Rolins a Rytmus - Party DJ                 1 139 414 
 14. Kristina - Ešte váham                                 1 089 308 
 
 15. Pavol Habera - Boli sme raz milovaní          1 070 876 
 16. Elán - Neviem byť dám                                1 033 467 
 17. Team - Reklama na ticho                                985 853 
 18. AMO a Misha - Čo s tým urobíš                       979 779 
 19. H16 - Zarábaj keš                                           903 706 
 20. Rytmus a DJ Witch - Nikdy sa nezavďačíš       875 434 
 21. Peter Nagy - Kristínka                                     857 122 
 22. Desmod - Zhorí všetko, čo mám                      819 705 
 23. Katarína Knechtová - Muoj bože                      774 079 
 24. Jožo Ráž - Voda, čo ma drží nad vodou            734 613 
 25. Iné kafe - Ráno                                                729 558 
 26. Desmod - Zober ma domov                              712 588 27a.Marika Gombitová - Vyznanie                           674  948 
 27b.Twiins - I don´t know                                     660 349 
 
 28. Dara Rolins - Túžim                                          655 602 29. Maduar - Hafanana                                         642 495 
 30. Tina a Čistychov - To, čo ma láka                    621 908 
 31. Zuzana Smatanová a Kuly - Pár dní                605 936 
 32. Michaela Paštéková - Sprayer Frayer             598 296 33. Kristina - Horehronie                                      596  720 
 34. Rytmus - Zlatokopky                                      571 486 
 35. Peter Kotuľa - Tak ako                                   567 393 36. Lobo - Ty si moja láska                                   567  074 37. Tublatanka - Pravda víťazí                             565 940 
 38. Darina Rolincová - Čo o mne vieš?                   562 011 39. Vec a Pio Squad - Turista                                554  434 
 
 40. Polemic - Komplikovaná                                   554 225 
 41. Vec ft. Tina-bez teba                                       534 778 
 
 42. Metalinda - Slnko nevychádzaj                       511 110 43. Richard Muller - Srdce jako kníže  Rohan         488 420 
 44. Banket - Po schodoch                                     482 224 
 45. Maduar - Do It!                                               470 927 
 46. Finalisti čs. SuperStar - Príbeh nekončí           465 221 
 47. Finalisti SuperStar3 - Dotkni sa hviezd           455 057 
 48. Peter Nagy - Poďme sa zachrániť                   426 235 
 49. Silvia Slivová - Nemožná                                 425 926 
 50. Miki Mora - Čo?                                               411 119 
 51. Robo Grigorov - Modlitba lásky                       408 210 
 52. Lobo a Peter Kotuľa - Len s tebou sa dá        399 141 53. Vašo Patejdl - Ak nie  si moja                           391 011 
 54. Taktici a Lucie Vondráčková - Keď ma podvedieš 383 066 
 55. Para  - Keď ťa stretnem                                     383 062 
 56. Martin Madej - Skús môj svet                            382 000 
 57. Zuzana Smatanová - Tam, kde sa neumiera        370 930 
 58a. AMO - Bratislava                                                 325 224 
 58b. Robo Grigorov - Ona je Madonna                       283 981 
 
 59. Barbara Haščáková - Fontána lásky                    280 790 
 60. Zona A - Cigánsky problém                                  273 413 
 61. Papajam - Keď ťa nechá dievča                           263 240 
 62. Miroslav Žbirka - 22 dní                                       260 332 
 63. G8 - Vráť trochu lásky medzi nás                        259 121 64. Tina - Závislý                                                       256 175 
 65. Peha - Slnečná balada                                        245 027 66. Vec a Zverina - Gde ste                                       233 932 67. P.S. -  Ak mám o teba prísť                                   232 523 68. Kamil Mikulčík, Nela Pocisková - Leť tmou             229  212 
 69. Zdenka Predná a Tina - Vietor                             227 854 
 70. Finalisti SuperStar1 - Kým vieš snívať                   224 823 
 71. Metropolis a Desmod - Mlyny                               218 755 72. Gladiator - Keď sa láska podarí                           210  320 
 73a. Emily - Dam du bi dam                                         207 347 
 73b. Tweens - Ranná sms                                          199 163 74. Hex - V piatok podvečer                                       197 630 
 75. Zenit - Milujem                                                     197 094 
 76a. Martin Kelecsényi - Musím už ísť                           184 581 
 76b. Exil - Kamarát                                                     182 998 
 
 77. Zdenka Predná - Kam má ísť                                155  379 78. Ingola - Dievča neplač                                          145 154 
 79. Finalisti SuperStar2 - So mnou vždy môžeš rátať 139 757 
 80. Komajota - Anjel                                                   129 402 81. Kuly a Lucia Nováková - Ak sa bez teba zbláznim  126 518 82. Mária Čírová - Búrka                                               126 263 83. Martina Schindlerová - Sám boh vie                        103 727 
 84. Misha - Náladu mi dvíhaš                                        102 900 85. Peter Cmorik - Mám pocit                                         81 649 
 
   

