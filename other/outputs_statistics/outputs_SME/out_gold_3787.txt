

  
 Thea  ma prišla vyzdvihnúť na letisko, trochu som u nich v Cavite city posurfovala net, uploadla fotky a zaplávala si v bazéne ich community centra, ktoré majú minútu od domu. 
  
 Keby som ja mala takýto bazén minútu od domu, máčala by som sa tam každý deň. 
   
 Večer som opäť vyrazila na cestu na ryžové polia na sever krajiny.  Bohužiaľ, nič tam nelieta, takže jediná možnosť bola autobus. Keďže  odchádzal o 11 pm, od nich sme kvôli zápcham museli vyraziť o 9.30 pm.  Sadla som na bus a šup ho do Baguia, letného hlavného mesta Filipín. 
   
 Cesta tam trvala asi 7 hodín, na stanici som si zobrala taxík a presunula sa na ďalšiu, odkiaľ išli autobusy na ryžové polia, teda do Sagady. To bolo okolo 6 am ešte tma, lenže pomaličky sa rozvidnievalo a to čo som videla, bolo dosť naháňajúce hrôzu. Prvú hodinu cesty som sa modlila, aby sme nespadli do priepasti pod nami a aby ma tam nezoškrabovali špachtľou. Išli sme po Hanselmovej horskej ceste a keby sa niečo stalo, neviem, ako by ma identifikovali. Cítila som sa ako Indiana Jones, úzka kľukatá cesta, z jednej strany skaly, z druhej priepasti, samé zosuvy pôdy, takže zrazu nikde žiadna cesta a vodič si predbiehal, akoby mu to robilo najväčšiu radosť na svete. Po hodine som si povedala, že sú tu aj iní ľudia, hoci domáci, ale tvária sa, že je to v pohode, tak som sa prestala strachovať a verila som, ze po ďalších 4 hodinách v poriadku dorazíme do cieľa. Je to jedna z najscenérickejších ciest sveta a výhľad je naozaj úžasný, aj keď možno nie pre každého žalúdok. 
   Do Sagady sme dorazili okolo obeda, zohnala som si ubytko a poď ho skúmať okolie. Checkla som mapu, mali tam byť nejaké ryžové polia, jaskyne a na skalách zavesené truhly. Fotila som ostošest, veď som na Filipínach urobila viac než 1200 fotiek, až som popri ryžových políčkach, kde stále niekto zapaľoval pôdu, narazila na dvoch Čechov, ktorí práve vychádzali z jaskyne. Hodili sme reč a oznámili mi, že do jaskyne sa dostanem len s guidom (sprievodcom). 
   
  
 Deti, ktorým skončila škola, čakajú na jeepney. 
   
  
 Lemon pie house. 
   
  
 Kvety na mojom balkóne. 
   
  
 Vedeli ste, že banány majú takéto kvety? 
   
  
 Ryžové polia v Sagade. 
   
  
 Pôdu zúrodňujú vypalovaním, preto ten dym v pozadí. 
   
 Trošku som sa pri nej ponevierala až jeden vyliezol von.  Prehovorila som ho, aby išiel ešte so mnou naspäť. Tak ako všetci, s  ktorými som sa počas celej cesty bavila, sa veľmi čudoval, že som sama.  Asi po stýkrát som mu vysvetlila, že som vlastne prišla navštívit  kamošku, ktorá má dve malé deti a tak si výlety podnikám sama. 
  
 Môj sprievodca po Sagade, Filip. 
   
 Vbehli sme do jaskyne a milý guide mi hneď povedal, že sa zamokríme, zašpiníme, budeme vo vode po kolená a budeme liezť po skalách. To by nám veru doma nikto nedovolil. Mal so sebou petrolejku a tou nám svietil, dnu bol veľký rámus spôsobený netopiermi. Batohy a šľapky sme nechali na jednom mieste a šup ho loziť, ako bol sľúbil. Poukazoval mi rôzne vzniknuté tvary - slony, korytnačky, kráľovský trón; prebrodili sme sa jazierkami, polozili skaly a previsy a asi po hodine a pol sme vyliezli von. 
  
 Voda, ktorou sme sa brodili, bola poriadne studená. 
   
  
 Čokoládovo-vanilková torta. 
   
  
 Jediné osvetlenie jaskyne - petrolejová lampa. 
   
 Keď som sa sťažovala, že som videla len 3 zavesené truhly, povedal, že keď sa mi tak páči chodenie, ukáže mi ďalšie. Nasledujúce 3 hodiny sme ani nič iné nerobili, len v každom skalnom útvare Sagady hľadali truhly. Ľudia tu veria v animizmus, a tak sa dajú pochovať do truhly, ktorú zavesia na skalu, aby ich duša ľahšie vzlietla hore. Sem-tam sa laná, na ktorých tie truhly visia, pretrhnú a truhly spadnú, pričom sa kosti vysypú na zem. 
   
 Truhly zavesené na skalách. 
  
 Pobehovali sme až do zotmenia a dohodli sme sa, ze ráno spolu o 6.30 vyrazíme na slávne ryžové polia do Banaue, dokonca nechcel, ani aby som mu zaplatila, vraj sa vyrovnáme na druhý den. Spýtala som sa na odporúčanie dobrej reštaurácie na večeru a rozišli sme. 
   

