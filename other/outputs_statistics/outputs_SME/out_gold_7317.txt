

   
 Kolotoč povinností, radostí a sklamaní, východniarskych telefonátov a dvojzmyslených sms ma pohltil, pokrčil do klbka a hodil do zátvorky. Zavesila som si na krk nápis: momentálne som nedostupná, zavolajte neskôr prosím. 
 Búrka vytopila pol dediny, za pár minút, potichu a nečakane. Na okno som nakreslila imaginárne zátvorky pre mračná, aby aj slnko malo priestor. Prosím, nezmažte ich, dáždnik chcem vytiahnuť až na jesenné prechádzky parkom. 
 Opäť sa mi jazyk rozplietol a hovorí všetko, čo mu moja myseľ káže. Na pol oka vidím vysmiate tváre poväčšine spolužiačok, ktoré so mnou trávia viac času ako moja mama. Počujem ich smiech, ktorý sa mieša s nabiflenými odpoveďami na nezmyselné otázky vyučujúceho. Aj myšlienky vkladám do hranatej zátvorky, aby niekomu náhodou neublížili. Bezpečné miesto v denníku, ktorý znesie naozaj všetko. 
 Pred letom odložím celý svoj život do jednej zátvorky, nech mi ho postráži, pokiaľ ja sadnem do lietadla a obletím zemeguľu, aby som prežila najlepšie leto v živote, fotila každý môj krok, písala vám najdlhšie maily a posielala zátvorkových smajlíkov. 
   

