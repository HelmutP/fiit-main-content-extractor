
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomíra Romanová
                                        &gt;
                Intenzívna medicína
                     
                 Márna liečba 

        
            
                                    19.4.2010
            o
            17:17
                        |
            Karma článku:
                16.87
            |
            Prečítané 
            4695-krát
                    
         
     
         
             

                 
                    Objem prostriedkov vložených do zdravotníctva je konečný.
                 

                 Vrtuľník vzlietol. Po parabole plával a pristál na helioporte nemocnice. Rýchla ujúkala, fičala k vrtuľníku. Telo vyložili a naložili, viezli ho na oddelenie. Bolo to naozaj iba telo. Udržiavané v živom stave dýchacím prístrojom, litrami tekutín a gramami liekov, ktoré sa za normálnych klinických okolností podávajú v miligramoch.   Pred hodinou otvorili v inej nemocnici prastarej babičke brucho a zistili, že je to záležitosť u nich neriešiteľná. Volali. Volali o pomoc do väčšej nemocnice, že chcú, veľmi chcú, zachrániť ju. Z volania vyplynulo, že babička je viac na onom ako našom svete, má prasknutú aneuryzmu (výdutinu) na brušnej aorte (hlavnej tepne, ktorá prechádza bruchom), krváca, má nemerateľný tlak a srdce jej bije rýchlejšie a rýchlejšie. Lejú do nej všetko možné, lieky, transfúzie, roztoky... Doktorka, čo prebrala hovor súhlasila. Nech privezú, priletia, ak  myslia, že treba.    Babku zašili a naložili a neskôr nalodili, odleteli a prileteli. Modrá, bez náznaku, že vníma, s nemerateľným tlakom krvi a rýchlou krivkou EKG, rozvratom vnútorného prostredia spôsobeným nedokrvenosťou tkanív, zlyhaním obličiek, v šoku. Prognóza nepriaznivá aj pre mladších pacientov. Okamžite sa dostala na operačný stôl. Počas operácie zmizla krivka EKG, srdce zastalo. Podali jej ďalšie lieky, stláčali hrudník, resuscitovali obeh. Srdce sa pohlo a váhavo pokračovalo počas výmeny cievy ( bypass aorty) až do konca operácie. Z operačnej sály prišla modrá ako slivka. Tlaková krivka sa slabo chvela na monitore, veľmi nízke hodnoty. Pulz bolo možné nahmatať len na krčnici. Ani kvapku moču sme z nej nevycedili. Srdce nevládalo. Dýchal prístroj. Krv sa nezrážala. Vnútorný rozvrat bol nezlúčiteľný so životom. Riešili sme neriešiteľné. Trápenie od prvého rezu v menšej nemocnici po posledný úder srdca vo väčšej trvalo niekoľko hodín. Spotrebovali sa lieky, zásoby krvi, infúzne roztoky, obväzy, hadičky, katétre, operačné sety a pomôcky, emócie a energia.     Všetko bolo nanič.   V prípade, že sa zdravotný stav (psychický aj fyzický) pacienta zhoršuje napriek všestrannej liečbe a podpore zdravia, nie je schopný získať akýkoľvek prínos od medicínskych aktivít, tak sa to volá márna liečba. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (44)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomíra Romanová 
                                        
                                            Gasping
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomíra Romanová 
                                        
                                            Vážené zdravotné poisťovne, vďaka. Začnem predčítať rozprávky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomíra Romanová 
                                        
                                            Vlasy dupkom sú na svete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomíra Romanová 
                                        
                                            Sérioví pacienti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomíra Romanová 
                                        
                                            Márna liečba 3. Fajka v plienkach.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomíra Romanová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomíra Romanová
            
         
        romanova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Lekárka, ktorá pracuje na ARO. Má sestru - fyzik, otca - literárny vedec a anglického špringeršpaniela - filozof/hedonista.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    128
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6227
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Koncert
                        
                     
                                     
                        
                            Anestéziológia
                        
                     
                                     
                        
                            Môj priateľ Timi
                        
                     
                                     
                        
                            Intenzívna medicína
                        
                     
                                     
                        
                            Povzdychy
                        
                     
                                     
                        
                            A vôbec
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Garth  Stein
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jaromír Nohavica
                                     
                                                                             
                                            KK: Uspávanky
                                     
                                                                             
                                            Hapka- Horáček
                                     
                                                                             
                                            Raduza a som z nej paf
                                     
                                                                             
                                            Katka Koščová
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            bojím sa, že niekoho vynechám
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.zachranazivota.sk
                                     
                                                                             
                                            Pubmed
                                     
                                                                             
                                            www.katkakosc.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




