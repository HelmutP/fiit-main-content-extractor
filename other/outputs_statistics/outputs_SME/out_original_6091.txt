
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Poloha
                                        &gt;
                Kresťanstvo s ručením obmedzen
                     
                 Podobenstvo o Samaritánovi - kto bol hostinský a čo bol hostinec? 

        
            
                                    7.5.2010
            o
            22:02
                        (upravené
                7.5.2010
                o
                22:17)
                        |
            Karma článku:
                5.49
            |
            Prečítané 
            1560-krát
                    
         
     
         
             

                 
                    Podobenstvo o milosrdnom Samaritánovi sa nachádza v Biblii v 10-tej kapitole Lukášovho evanjelia vo veršoch 25-37. Odkaz je tu: http://www.biblia.sk/sk/1/biblia/sev/luk/010.html Zaujímavou postavou v tomto príbehu je hostinský. Ten si žije vo svojom svete, stará sa o svoj hostinec a hostí. Zabezpečuje im potravu, nocľah, dočasnú strechu nad hlavou... Určite sa snaží, aby sa jeho hostia cítili príjemne a mali všetkého dostatok. Nechcem si pustiť fantáziu na  prechádzku a predstavovať si plagáty hovoriace o živej hudbe a zľavách pri viacdennom pobyte atď. To nechám na iných. Rád by som sa zameral hlavne na hostinského, pretože práve on sa stal zodpovedným za starostlivosť o pacienta. Zrejme sa dostatočne dobre staral o svoj podnik, keď získal dôveru Samaritána.
                 

                Hostinský, ktorý mal určite mnoho iných povinností - musel sa predsa starať o svoj hostinec a to predstavuje vyťaženie na 100 percent - dostal k tomu ďalšiu, nemenej dôležitú úlohu. Ale ak je niekto dobrým hotelierom, to ešte neznamená, že môže byť aj dobrým lekárom. Nech sa na to pozerám akokoľvek, vychádza mi z toho, že na to nemohol byť sám. Musel mať na chod svojho hostinca dobre organizovanú skupinu ľudí, kde každý má svoje miesto, prácu a poctivo vykonáva svoje povinnosti. Inak by to Samaritán rýchlo zabalil a hľadal bezpečnejšie miesto, kde sa o toho nešťastníka postarajú. Našťastie sa nič také nestalo a preto sa javí zrejmé, že tento hostinec musel mať kvalitné služby, s tým sa určite spájala jeho popularita, vďaka nej mal viac zákazníkov, ktorí mu robili dobré meno a tak ďalej... Ak Samaritán predstavuje Pána Ježiša, hostinec je určite Cirkev a hostinský bude jej správca - v menšom by šlo o farnosť a jej predstaveného(farár, kňaz...) Znamená to, že ak hostinec(farnosť) rastie, určite je Samaritánovi(Pánovi Ježišovi = Bohu) dobre známa a pribúdanie chorých znamená v prvom rade to, že ju Samaritán(Boh) často a s dôverou navštevuje. To tiež znamená, že tam pribúda práce s chorými, ale Samaritán predsa s každým nemocným necháva aj svoj dar, ktorý by mal postačovať na zabezpečenie starostlivosti o jeho/jej uzdravenie. Zdá sa akoby v prípade správne fungujúcej cirkvi nešlo ani tak o hostinec, ale skôr o nejaký lazaret. Tento dojem je však mylný, pretože neberie do úvahy vyliečených pacientov, ktorí sa starajú o nových chorých. Pre takúto cirkev by sa dal použiť skôr pojem z Králickej češtiny: "léčebnice" - to znamená, že sa tu chorí liečia. Často krát to ale vyzerá tak, že cirkev je nemocnica(miesto, kde sa zhromažďujú nemocní) a len dúfam, že nie z tohto dôvodu niektoré evanjelikálne cirkvi nazývajú svoje bohoslužby "zhromaždenia." V dnešnej dobe sa dostávajú na povrch informácie, že v niektorých farnostiach sa nielenže nelieči, ale dokonca ubližuje. A ako to už býva, neutrpí tým na povesti len konkrétny "hostinec," ale celá "sieť navzájom prepojených hostincov." Čiže nielen farnosť, ale aj celá cirkev, do ktorej konkrétna farnosť patrí. K tomu by som povedal len toľko, že tak ako hostinec spravuje hostinský, tak aj farnosť spravuje farár a on je za svojich pacientov pred Bohom zodpovedný. A tak ako hostinskému povedal Samaritán, keď odchádzal, že mu doplatí všetky výdavky, ktoré by mal navyše pri liečbe chorého, rovnako budú znieť aj slová pre každého správcu farnosti: "Tak, koľko som ti ešte dlžný za tvoju starostlivosť?"

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Poloha 
                                        
                                            Myšlienky z púte: Sme pútnici na Zemi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Poloha 
                                        
                                            Kam sa podel môj syr?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Poloha 
                                        
                                            Peniaze a viera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Poloha 
                                        
                                            O čiernych ovciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Poloha 
                                        
                                            Myšlienky z púte: Kedy sa začína cesta pútnika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Poloha
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Poloha
            
         
        poloha.blog.sme.sk (rss)
         
                                     
     
        Som človek bežného výskytu, ale nie hromadného typu. Kresťan, technik, so záujmom o históriu, ale vždy hľadajúci iný pohľad na veci ako je ten "bežný".
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    28
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    815
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kresťanstvo s ručením obmedzen
                        
                     
                                     
                        
                            Paradoxy
                        
                     
                                     
                        
                            Camino de Santiago
                        
                     
                                     
                        
                            Ekonomické rozprávky
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




