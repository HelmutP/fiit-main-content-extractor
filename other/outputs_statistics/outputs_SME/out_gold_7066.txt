

 Nie všetci však majú šancu na takéto právo, znie to možno neuveriteľne, ale nemajú. Keď sa ja s mojím frajerom vezmeme, tak predsa to pôjde automaticky... Ale nie všetci sa môžu zobrať, aj keď sú slobodní a žijú v partnerskom vzťahu a chcú spolu zostarnúť. 
 Dnes som na Dúhovom pride videla veľa: príliš málo odhaleného tela na pohoršovanie sa, veľa rešpektujúcich a slušných ľudí, a príliš veľa nenávisti a slzného plynu na demokratickú krajinu v 21. storočí. 
 Nechcem obhajovať právo na registrované partnerstvo alebo na manželstvo platením daní. Napísala som to, lebo okrem mnohých iných vecí je to ďalšia, ktorá nás spája s lesbami, gejmi a neheterosexuálne orientovaných ľudí. Sme na jednej lodi, s rovnakou vládou, rovnakým daňovým zaťažením, s rovnakým počasím a virózami. Tak prečo nie s právom na oficiálny spoločný život? 
 Verím, že na Dúhový pride 2011 príde viac ľudí so všetkými farbami v duši, a menej tých odetých do nenávisti. 

