
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Červeň
                                        &gt;
                Súkromné
                     
                 Noc "padajúcich hviezd" 

        
            
                                    11.8.2008
            o
            11:00
                        |
            Karma článku:
                6.89
            |
            Prečítané 
            2163-krát
                    
         
     
         
             

                 
                    Takejto noci sa podľa mňa nevyrovná žiaden ohňostroj, pretože ohňostroje nech sú akokoľvek pekné a pestré, sú očakávateľné. Jednu takúto noc som zažil niekedy ešte v roku 1991. Mali sme vtedy duchovné cvičenie v dedinke Kvačany pri Kvačianskej doline, a keďže bola horúca noc, rozhodli sme sa niektorí prenocovať "pod širákom" na dvore. Bola to noc niekedy pred 15. augustom. Bol som veľmi prekvapený z toho, čo všetko sa dalo uvidieť, bolo to také množstvo meteoritov, aké som odvtedy ešte nikdy nevidel. V podstate až dnes som sa dozvedel, že práve v tom roku bol roj perzeidov v maxime. Na nové tohtoročné maximum ma upozornili mladí z Kostolian nad Hornádom, kde momentálne pôsobím. Ak nám do toho nič nevôjde, sadneme na autá a vyrazíme niekde do tmy, ďalej od Košíc. Vyrazíme pravdepodobne už túto noc. Nie som si však istý, na ktorý svah by sme mali vyliezť. Neviem, či predpokladám správne, ale ak tento roj nie je vidieť z južnej pologule, tak bude pravdepodobne najviditeľnejší asi na severnej strane oblohy, takže si asi nájdeme svah niekde uprostred hôr obrátený smerom na sever. Určite sa tu na blogu nájde niekto, kto sa vyzná lepšie do astronómie a z uvedených údajov, ktoré som našiel na stránke International Meteor Organization, nám poradí, kde by sme sa mali "vytrepať" a či by to nebolo lepšie zajtrajšiu noc... :-)
                 

                 
  
  Keďže niektorí mladí, ktorí veľmi radi poslúžia ako animátori vo farnosti, by si veľmi radi pozreli tento úkaz, rád si ho pozriem s nimi. Nie som nejaký vášnivý astronóm, ale v takýchto situáciách si veľmi dobre uvedomujem veľkosť a rozľahlosť vesmíru, a tiež skutočnú veľkosť človeka v porovnaí nielen s obrovskými vesmírnymi priestormi, ale aj v porovnaní s tým, čo všetko je o človeku napísané v Biblii. Pre  viac znalých veci ako ja, umiestňujem tu časť zo stránky, kde sa o tom, čo sa udeje počas týchto nocí píše odbornejším jazykom: "Perzeidy boli jedným z najvzrušujúcejších rojov v devädesiatych rokoch minulého storočia, keď dosahovali v novom maxime EZHR 400+ v rokoch 1991 a 1992. Frekvencie tohto maxima pomaly klesali ku 100–120 koncom storočia až po rok 2000, keď sa vôbec neobjavilo. Tento vývoj nebol neočakávaný, keďže zvýšenie aktivity a toto maximum (prvý krát pozorované v roku 1988) boli spojené s prechodom materskej kométy roja 109P/Swift-Tuttle perihéliom v roku 1992. Keďže obežná doba kométy je 130 rokov, v súčastnosti sa vzdiaľuje do okrajových častí Slnečnej sústavy a tak sa podľa teórie aj znižuje pravdepodobnosť podobných maxím a ich intenzita. Podľa niektorých predpovedí sa malo toto skoré maximum znovu zjaviť v období 2004–2006. A skutočne, v roku 2004 bolo blízko jeho časovej polohy zaznamenané krátke výrazné maximum. Rok 2005 ale nepriniesol žiadne zvýšenie frekvencií a návrat v roku 2006 bol iba málo pozorovný pre nepriaznivú fázu Mesiaca. Na základe údajov z obdobia 1991–1999 bol vyvodený priemerný ročný posun „starého“ maxima s hodnotou +0°05 v dĺžke Slnka, čo v roku 2008 zodpovedá 12. augustu, 16.40 UT (lambda sol = 140°21), čo je niekoľko hodín po „tradičnom“ maxime (uvedené v tabuľke), ktoré bolo zatiaľ zaznamenané pri každom návrate. Tretie maximum, ktoré od roku 1999 nebolo pozorované, nastane (ak) 12. augusta, 21.30 UT (lambda sol = 140°4). Podľa posledných pozorovaní je očakávateľné iba „tradičné“ maximum, ale pozorovatelia by si mali byť vedomí aj týchto ďalších možných časov, aby podľa nich mohli naplánovať pozorovanie. Dorastajúci Mesiac zapadá medzi miestnou polnocou a 01.30 v noci 12.–13. augusta pre stredné severné zemepisné šírky, ktoré sú najlepšie umiestnené na sledovanie roja (z miest ďalej na sever zapadá Mesiac ešte skôr). Ostáva tak čas na pozorovanie do svitania. Z týchto miest je radiant spoľahlivo nad obzorom od 22–23 hodín miestneho času a stúpa po celú noc. „Tradičné“ maximum má najlepšie pozorovacie podmienky v severnom tichomorí, zahŕňajúc najzápadnejšie časti severnej Ameriky, rovnako ako najvýchodnejšie časti Japonska a Číny. Na pozorovanie sú vhodné všetky metódy. Napríklad video pozorovania z poslednej doby pomohli spresniť polohu radiantu a potvrdili, že domnelý zložitý tvar radiantu je takmer isto iba domnelý. Jedinou nevýhodou tohto roja je nemožnosť ho pozorovať z južnej pologule." Zdroj informácií: http://www.imo.net/calendar/slovak/2008#per Všetkým prajem príjemný deň a niektorým, ktorí chcú aj počas prázdnin a dovoleniek prežiť noc ináč ako pred televízorom, či pod nejakým iným druhom umelého osvetlenia, prajem aj príjemnú dnešnú, prípadne zajtrajšiu noc. :-)

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (54)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Fico vymetá všetky kúty aj v osadách...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Budú Fica oblizovať aj zozadu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Napíš europoslancovi, nech neblbne...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Ukrajina potrebuje EÚ ale aj EÚ potrebuje Ukrajinu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Skončí postávanie pred kostolom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Červeň
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Červeň
            
         
        cerven.blog.sme.sk (rss)
         
                        VIP
                             
     
        Mám rád život s Bohom a 7777 ľudí. S obľubou lietam v dobrej letke, no nerád lietam v kŕdli. Som katolícky kňaz, pochádzam z dediny Klin na Orave. Farár vo farnosti Rakúsy. (pri Kežmarku)
...........................................


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1475
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3095
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            BIRMOVANCI
                        
                     
                                     
                        
                            ŠTIPENDISTI
                        
                     
                                     
                        
                            7777 MYŠLIENOK PRE NÁROČNÝCH
                        
                     
                                     
                        
                            Aké mám povolanie?
                        
                     
                                     
                        
                            Akú mám povahu?
                        
                     
                                     
                        
                            Cesta viery
                        
                     
                                     
                        
                            Duchovné témy
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Fotografie prírody
                        
                     
                                     
                        
                            Fotografie Rómov
                        
                     
                                     
                        
                            História mojej rodnej obce
                        
                     
                                     
                        
                            Kniha o Rómoch
                        
                     
                                     
                        
                            Lunikovo
                        
                     
                                     
                        
                            Mladí vo firme
                        
                     
                                     
                        
                            Náčrt histórie slov. Saleziáno
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Pastorácia Rómov
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Pokec s Bohom
                        
                     
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Sociálna práca
                        
                     
                                     
                        
                            Sociálna náuka Katolíckej cirk
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Vedeli sa obetovať
                        
                     
                                     
                        
                            Vzťah je cesta cez púšť
                        
                     
                                     
                        
                            Zábava
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štipendium pre chudobné zodpovedné dieťa
                                     
                                                                             
                                            Prečo si firmy neudržia mladých ľudí?
                                     
                                                                             
                                            Dejiny mojej dediny
                                     
                                                                             
                                            Vzťah je cesta cez púšť
                                     
                                                                             
                                            Akú mám povahu?
                                     
                                                                             
                                            7777 MYŠLIENOK PRE NÁROČNÝCH
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sociálny kódex Cirkvi - Raimondo Spiazzi
                                     
                                                                             
                                            Sociálna práca - Anna Tokárová a kolektív
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Free Download MP3 ruzenec
                                     
                                                                             
                                            MP3 ruženec
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Krištofóry - katolícky liberál
                                     
                                                                             
                                            Peter Herman - realistický idealista
                                     
                                                                             
                                            Michal Hudec sa nebojí písať otvorene
                                     
                                                                             
                                            Mária Kohutiarová - žena, matka, človek
                                     
                                                                             
                                            Branislav Sepeši - lekár o etike
                                     
                                                                             
                                            Katrína Janíková - neobyčajne obyčajná baba
                                     
                                                                             
                                            Martin Šabo - šikovný redemptorista
                                     
                                                                             
                                            Juraj Drobny - veci medzi nebom a zemou
                                     
                                                                             
                                            Peter Pecuš a jeho postrehy zo života
                                     
                                                                             
                                            Miroslav Lettrich - svet, spoločnosť, hodnoty
                                     
                                                                             
                                            Karol Hradský - témy dnešných dní
                                     
                                                                             
                                            Salezián Robo Flamík hľadá viac
                                     
                                                                             
                                            Salezián Maroš Peciar - fajn chlap
                                     
                                                                             
                                            Motorka a cesty: Martin Dinuš
                                     
                                                                             
                                            Ďuro Čižmárik a jeho mozaika maličkostí
                                     
                                                                             
                                            Paľo Medár a scientológovia
                                     
                                                                             
                                            Pavel Škoda chodí s otvorenými očami
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Modlitba breviára
                                     
                                                                             
                                            Nezabíjajte deti!
                                     
                                                                             
                                            Občiansky denník
                                     
                                                                             
                                            Dunčo hľadaj
                                     
                                                                             
                                            Správy z Cirkvi
                                     
                                                                             
                                            Prvý kresťanský portál
                                     
                                                                             
                                            Lentus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voľby nemožno vyhrať kúpenými hlasmi najbiednejších Rómov!
                     
                                                         
                       Jednoduchý rozdiel medzi kandidátmi na prezidenta.
                     
                                                         
                       Doprajme Ficovi pokoru!
                     
                                                         
                       Fico vymetá všetky kúty aj v osadách...
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                                                         
                       Vďakyvzdanie
                     
                                                         
                       Treba homofóbiu liečiť?
                     
                                                         
                       Procházka, Kňažko, Kiska... a jediný hlas
                     
                                                         
                       Napíš europoslancovi, nech neblbne...
                     
                                                         
                       Prezidentské voľby: Prečo budem voliť Procházku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




