

   
 Reagujúc na exaktné argumenty jeho vedeckej svätosti, najväčšieho prírodovedného génia všetkých dôb, HCs. GRSc. Alexandra JÁRAYa, ktorými on vedecky dokazuje dôvodné podozrenie z dementnosti učiteľov matematiky SR, minister školstva nahodilo vybral jedného učiteľa matematiky strednej školy, ako aj riaditeľa Matematického Ústavu AV a požiadal ich, aby sa oni dobrovoľne podrobili psychiatrickému vyšetreniu, ktoré by malo to exaktné JÁRAYovo dôvodné podozrenie ich demncie vyvrátiť.  
 Oslovení s psychiatrickým vyšetrením ochotne súhlasili a aj sa na ňom dobrovoľne zúčastnili. Tu je zápis z toho psychiatrického vyšetrenia. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Účastníci predmetného psychiatrického vyšetrenia: 
 MUDr. Prof. Svätopluk Tobák: DrSc. Primár psychiatrickej kliniky. 
 Učiteľka gymnázia sv. Berousa (žena) 
 Riaditeľ MÚ AV (muž) 
 Predmetní matematici, v tejto kauze nechceli vystúpiť z anonymity.  
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Srdečne vás vítam. Viete o čo ide, nebudem to vyšetrovanie preťahovať, aj ja som osobne presvedčený o tom, že argumenty pána Járaya nie sú opodstatnené, že matematici SR netrpia demenciou, ale formality vyšetrenia musím dodržať, lebo ja za toto vyšetrenie budem odmenený príslušným horárom a na viac aj vzatý na zodpovednosť. 
 Takže ja sa vás oboch najprv pýtam, vy na toto psychiatrické vyšetrenie pristupujete dobrovoľne? 
 Budú vaše odpovede pravdivé? 
 Žena: Áno. 
 Muž: Áno. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Súhlasíte s tým aby vaše odpovede mohli byť aj verejne publikované? 
 Žena: Áno. Samozrejme, preto som tu!  
 Muž: Áno. Veď kvôli tomu som sem prišiel! 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Prosil by som toto vaše prehlásenie potvrdiť aj podpisom. 
 Žena: Podpisuje s pravou rukou, graciózne a s cynickým úsmevom. 
 Muž: Podpisuje ľavou rukou, vážne, s vedomím zodpovednosti za vlasť i za národ. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Takže všetko je v najlepšom poriadku, môžeme pristúpiť k veci.  
 Odpovedzte mi obaja na nasledovnú jednoduchú otázku:  
 Ak sa stretnú dvaja kamaráti tak sa pozdravia napríklad pozdravom servus, alebo ahoj. 
 Ak sa stretnú dvaja komunisti tak sa pozdravia pozdravom napríklad česť práci. 
 Ale ak sa stretnú dvaja pápeži, akým pozdravom sa oni podľa vás navzájom pozdravia? 
 Žena: Pochválen buď Ježiš Kristus! 
 Muž: Aj ja by som povedal: Pochválen buď Ježiš Kristus! 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Odpovede nie sú správne, lebo dvaja pápeži sa stretnúť nemôžu. Kým nezomrie jeden, nezvolia druhého. Ale to len tak na zohriatie, lebo to s matematikou nemá nič spoločného, takže nasleduje moja druhá otázka.  
 Pán JÁRAY vo svojich exaktných argumentoch uvádza, že každý matematik je preto duševne chorý a pre spoločnosť nebezpečný človek, (dokonca až grázel) lebo on sa za štátne peniaze zaoberá výlučne ničím, čiže iba nepolapiteľnými preludmi, aj to iba primitívnymi preludmi vo forme matematických čísiel, ktorými sa ale nedá opísať pravda materiálnej prírody. Teda, že každý matematik sa zaoberá s takými číslami, ktoré s pravdou prírody nemajú nič spoločného. Ktoré, ako on tvrdí, sa analyticky nedajú aplikovať v životnej praxi človeka, ktoré iba mätú zdravý rozum žiakom a študentom a kvôli ktorým veľa žiakov a študentov úplne zbytočne prepadá z matematiky.  
 Ale čo je podľa mňa najzaujímavejšie, že každý matematik  sa zaoberá iba takými číslami, ktorých hodnotu, podľa pána JÁRAYa, tvoría iba násobky ničoho. 
 Takže ako viete vy, každý jeden osobitne, na tieto argumenty pána JÁRAYa reagovať.  
 Je pravdou, že matematika sa zaoberá iba násobkami ničoho?  
 Žena: Viete vy o tom, že ten jaray je duševne chorí blázon? Mám reagovať na názory toho blázna? 
 Muž: Ani ja si nemyslím, žeby som mal odpovedať na dojmy  človeka, ktorý päťkrát za sebou bol uznaný psychiatrami za blázna, lebo on aj  podľa nich hlása matematické a fyzikálne bludy.  
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Vážení, uvedomte si, že mojou úlohou nie je vyšetrovať psychický stav pána Járaya, ktorého zatiaľ žiadaný súd neuznal za „blázna“ na základe odborných posudkov jeho matematických a fyzikálnych výrokov, ktoré na škodu veci posudzovali zatiaľ  len a len, čiže výlučne iba psychiatri. 
 Mojou úlohou je vyšetriť váš psychický stav a to na základe odpovedí na tie otázky, ktoré ja vám kladiem a nie na tie, ktoré mi vy kladiete. 
 Takže odpovedajte na otázku:  
 Je pravdou, že matematika sa zaoberá iba ničím a násobkami ničoho?  
 Žena: Ja vyučujem iba to, čo mi nariaďujú učebné osnovy. Iné vyučovať nesmiem. Nie som platená za rozmýšľanie na tým, že to čo ja učím je aj pravda, alebo nie. No a učebné osnovy mi nariaďujú učiť matematiku takých čísel, ktoré sú násobkami matematických bodov, ak chcete tak aj násobkami ničoho. Ja som iba propagátorka, učiteľka štátom predpísanej bodovej matematiky a nie teoretička matematiky! 
 Muž: Tu by sa žiadala konkrétnejšia otázka, ktorá by indukovala aj tú skutočnosť, že matematika, povedzme takých čísel, ktorých hodnotu predstavujú násobky ničoho, čiže násobky matematických bodov, škodí, alebo je nápomocná robotníckej triede, sedliakom a inteligencii, či ona pomáha občanom SR. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Takže v konečnom dôsledku priznávate, že matematika sa zaoberá výlučne číslami ktorých hodnota predstavuje násobok ničoho? Povedzte áno, alebo nie! 
 Žena: Áno.  
 Muž: Áno. Ale to je v prospech občanov SR.  
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Vážení, zbytočne sa nestresujte, výsledok tohto posudku nebude mať žiadaný vplyv na vaše ďalšie pôsobenie v školstve a vo vede. To čo učíte teraz, to budete učiť až do smrti. 
 Moja druhá otázka znie: Ak všetky matematické čísla sú násobkami ničoho, ako si vy matematici viete predstaviť jedno žiadne nič, čiže jednu nulu, jeden matematický bod a ako dve a viac ničôt, viac núl, čiže množinu matematických bodov?  
 Žena: Ja si to neviem predstaviť, ja vyučujem čísla v podobe orechov alebo gaštanov. Niekedy používam namiesto čísel zvieratá, ako sliepky, husi, kačky, či kozy, ale aj vajcia. Nulami, čí ich ekvivalentmi, teda diferenciálmi, zaoberá sa až vyššia matematiky, tú zatiaľ chvála bohu na našom gymnáziu ešte nemáme. Takže tak. 
 Muž: Matematika už dospela tak ďaleko, že používa takzvané binárne čísla, čiže  limitné čísla, ktoré môžu mať raz hodnotu matematického bodu, teda hodnotu bezrozmerného matematického objektu o nulovej dĺžke, čiže výlučne iba hodnotu veľmi tenkej myšlienky, inokedy zase reálnu materiálnu nenulovú, jednorozmernú hodnotu, i keď iba nekonečne malú jednorozmernú dĺžkovú hodnotu zvanú dx, teda hodnotu nekonečne tenkéj myšlienky. A to sa pri troške dobrej vôle dá predstaviť. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Chcete mi povedať, že v matematike násobkov ničoho platia také pravdy ktorých aj opak je pravdou? 
 Žena: Odmietam sa vyjadrovať. Žijem v usporiadanom manželstve a ešte máme nesplatenú hypotéku. 
 Muž: Samozrejme. Matematika je o pravde a jej opaku, ktorá je tiež pravdou. V tom tkvie čaro i vznešenosť, ale aj zložitosť matematiky. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: O tej dvojitej matematickej pravde vedia aj študenti?  
 Žena: Odmietam sa vyjadrovať, opýtajte sa študentov. Ja nie som ich hovorcom. 
 Muž: Ktorí to vedia, vedia a ktorí to nevedia, prepadnú z matematiky.  
 aaaaaaaaaaaaaaaa 
  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
  
 Svätopluk Tobák: Ako matematika násobkov ničoho, matematika prázdnych čísel rieši rovnicu:  
 2 + 2m = ? 
 Žena: Neviem, takú rovnicu v učebných osnovách nemám, ale ona pokiaľ sa dobre pamätám, nenachádza sa ani v žiadnej inéj učebnici matematiky. Mám dojem, že ide o Einsteinovu rovnicu, opisujúcu pohyb predmetu (*****) v zákrute.  
 Muž: V prvom rade je potrebné hodnotu každého materiálneho, jednorozmerného metra, nahradiť číslom bezrozmerným, teda takým číslom, ktoré je násobkom nuly, alebo násobkom matematického bodu, poprípade ktoré je číslom limitným, ktoré je násobkom hodnoty dx.  
 V prvom rade je potrebné meter odmaterializovať, čiže diferencovať, čiže zamatematizovať.  
 To metrové, jednorozmerné číslo je potrebné premeniť na násobok diferenciálov a potom by sa našlo aspoň partikulárne riešenie už v predmetnej diferenciálnej rovnici.  
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák:  Ako matematika násobkov ničoho, matematika prázdnych čísel rieši rovnicu: 
  2 . 2m = ? 
 Žena: Na ale to je snáď každému dieťaťu jasné: 2 . 2m = 4m. 
 Muž: Súhlasím z paňou kolegyňou, 2 . 2m = 4m.  
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Aj ja by som s tým súhlasil, keby tu nebola  pripomienka pána JÁRAYa v nasledovnom znení:  
 Ak máme dva kovové metre  a chceme ich vynásobiť číslom dva, ako tento súčin matematika dokáže konkrétne predviesť v každodennej praxi občanov SR?  
 "Skade matematika vezme tie druhé dva kovové metre, aby z tých prvých dvoch kovových metrov, po ich vynásobení  bezrozmerným číslom dva, vznikli štyri kovové metre? Há? Há? Há?" 
 Čo sa stane z dvoma kovovými metrami, keď na ne niekto zakričí, „hej dva kovové metre, ja vás násobím číslovkou dva“? 
 Takže opíšte mi láskavo, krok po kroku, ako v matematike násobkov ničoho, prebieha násobenie medzi jednorozmernou materiálnou veličinou 2m s bezrozmerným myšlienkovým číslom 2.  
 Ako sa po uvedenom súčine zrodia tie druhé dva kovové metre? 
 Žena: Som z toho jeleň, prepáčte, jelenica. Vzdávam sa odpovedi. (Utiera si spotené čelo a niečo si neustále mrmle pod nosom o tom jarayovi.) 
 Muž: To sú veľmi hlúpe otázky, s takými som sa nestretol na žiadnych medzinárodných konferenciách. A verte mi, že ja mesačne som na dvoch takýchto vedeckých konferenciách.Tam sme už riešili aj otázku, koľko anjelov sa zmestí na hrot ihly. Ale toto ešte nie! 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Takže ani jeden z vás nevie povedať, skade sa po vynásobení dvoch kovových metrov číslom dva objavia štyri kovové metre? 
 Žena: Skade by som to mohla vedieť ja, keď ani pán akademik to nevie? 
 Muž: Podľa učebných osnov MŠ, je to tak. Učiteľ matematiky povie nahlas pred triedou: „dva kovové metre ja vás násobím číslom dva“ a službu konajúci žiak, po tejto vete učiteľa matematiky, k tým dvom kovovým metrom priloží ďalšie dva kovové metre. 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Aké jednoduché a  ja by som na to nikdy neprišiel. No a dopracovali sme sa už k poslednej otázke? 
 Ako matematika násobkov ničoho, matematika prázdnych čísel rieši rovnicu: 
  2 : 2m = ? 
 Žena: Čert mi bol dlžný toto vyšetrenie, kvôli tomu korunovanému bláznovi z Košíc sa tu zosmiešňujem, pričom v škole užívam vysokú autoritu múdrej a skúsenej matematičky. Zato ma rodičia mojích žiakov obdarúvavajú, sliepkami, kačkami, vajciami, ale aj neživými predmetmi.  
 Preto moja odpoveď znie, nemám ani šajnu aký je výsledok toho podielu. Zavrite toho blázna do blázinca, nech nás matematikov už konečne prestane urážať otázkami na ktoré nevieme odpovedať. Nech nám dáva iba také otázky, na ktoré vieme odpovedať!  
 Veď žijeme v Slovenskej republike, čiže tu sme doma. A doma si budeme klásť iba také otázky na ktoré poznáme odpovede, teda výlučne Slovenské otázky a nie kadejaké paneurópské, či svetácké otázky !!! 
  
  Muž: Tento podiel je jarayom umelo vyfabrikovaný chyták, nastražený na nás, rodu verných, Slovenských, národne orientovaných matematikov, ktorí si vysoko vážime samostatnosť a zvrchovanosť Slovenskej republiky, v spoločenstve s ostanými štátmi Európy v Európskej únii.  
 Touto umelo vytvorenou a preto ani počítačmi neriešiteľnou rovnicou podielu 2:2m =?, jaray cieľavedome dehonestuje autoritu pronárodnej matematickej obce Slovenskej republiky, a tiež útočí na základy demokratického poriadku na Slovensku, ako aj proti jej vláde a parlamentu, ale aj na stabilitu budovy parlementu a Bratislavského hradu.  
 Nie v poslednom rade, on útočí touto rovnicou aj na umelcov národného divadla v Bratislave, ako aj na novostavbu ND.  
 Ak jaray bude môcť aj naďalej na slobode konštruovať svoje matematické aparáty, jeho takzvanej kvantovej matematiky, tak on nakoniec pomocou nich zapríčiní zánik Slovenskej republiky ako aj  pád budovi ND, Bratislavského hradu i budovy NR SR. (Možná i pád budovy Ústavného súdu v Košiciach.) 
 Takže s tým niečo treba už konečne robiť. To je moja odpoveď na výslednú hodnotu predmetného podielu 2:2m =? 
   
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 Svätopluk Tobák: Ďakujem vám za vaše odpovede na moje otázky.  
 Tým sa vyšetrovanie vašej domnelej matematickej demencie, exaktne vysvetlenej pánom JÁRAYom skončilo, môžete ísť domov.  
 Výsledky tohto vyšetrenia budú doručené ministrovi školstva do 15. dní. 
 No vopred musím vás oboznámiť s tým, že po tomto vyšetrovaní, ja už pána JÁRAYa  nepovažujem za neprekonateľného blázna. 
 aaaaaaaaaaaaaaaaaaaaaa 
  
 -------Galileo-------Newton-------Lorentz-------Einstein-------Járay------- 
 
  Diskusia je možná na nasledovnej adrese:  
 http://jaray.blog.sme.sk/clanok.asp?cl=212953&amp;bk=72279  
 
 
  Doslov. (23. 10. 2009) Autor tohto článku, najväčší prírodovedecký génius všetkých dôb, Jeho vedecká svätosť, HCs. GRSc. Alexander JÁRAY, po jeho opätovnom prečítaní, je sám prekvapený nad genialitou ducha ktorý ho k napísaniu tohto článku inšpiroval.   Autor tohto článku, najväčší prírodovedecký génius všetkých dôb, Jeho vedecká svätosť, HCs. GRSc. Alexander JÁRAY, si vysoko  cení reálny obsah tohto článku, ako aj bezradnosť dotknutých persón brániť sa proti tu opísanej realite, vecnými argumentmi. Autor tohto článku, najväčší prírodovedecký génius všetkých dôb, Jeho vedecká svätosť, HCs. GRSc. Alexander JÁRAY, aj naďalej bude sa riadiť úvahou Jána Wericha: „Nad hlupákmi sa nedá zvíťaziť, ale proti hlupákom je potrebné viesť boj každý deň, aby oni vedeli, že nad múdrosťou moc nemajú.“ Vážený čitateľ, ak máš nejakého kamaráta, vzdelaného, či nevzdelaného  hlupáka, odporuč mu prečítať tento článok. S úprimným pozdravom:  Najväčší prírodovedecký génius všetkých dôb, Jeho vedecká svätosť,  
 HCs. GRSc. Alexander JÁRAY. 
  
 

