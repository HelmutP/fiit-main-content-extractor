
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Turanský
                                        &gt;
                Brazília
                     
                 Rio de Janeiro - mesto pod ochranou neba 

        
            
                                    23.3.2010
            o
            18:48
                        (upravené
                23.3.2010
                o
                20:10)
                        |
            Karma článku:
                7.97
            |
            Prečítané 
            1304-krát
                    
         
     
         
             

                 
                    Starší miestni obyvatelia tvrdia, že práve toto je posledná zástavka na ceste do neba. Už len samotný názov je legendou. Mesto farieb, pláží, karnevalu, futbalu ale aj centrum obchodu, ekonomiky a vedy. Mesto pod rukami Krista. Vitajte v Riu.
                 

                 Copacabana...stačí sa nadýchnuť teplého morského vzduchu, vône kokosu, ktorá sa plaví vánkom a ucítite tú neuveriteľnú pulzujúcu energiu tohto miesta. Započúvajte sa do zvukov samby, veselého smiechu a šumu mora. A nakoniec otvorte oči, kochajte sa slnečnými lúčmi, ktoré sa odrážajú od modrastej hladiny mora. Dotknite sa zlatistého zohriateho piesku a ponorte sa do hmatateľnej pohodovej atmosféry, ktorá tu panuje.         Cristo Redentor (Kristus Spasiteľ)...keď ho uvidíte prvýkrát, pocítite rešpekt, úžas, silu, ktorú dodáva Riu táto obrovská socha. Je majestátny. Týči sa nad mestom a akoby ho objímal svoji rukami. Rio de Janeiro máte ako na dlani.         Pão De Açúcar...opačný vrch k vrchu Kristovmu. Pri sviežom čaji Matté si vychutnáte výhľad na vody Altantického oceánu. Veriaci tvrdia, že čistá duša môže na horizonte uzrieť obrysy Afriky (z hľadiska zakrivenia zemegule je to nemožné).      Rua de Carnival...každý rok tu dva dni bez prestávky hrajú bubny sambu, caipirinha tečie prúdom, fotoaparáty turistov pretínajú tmu, pri fakľovom osvetlení sa mihajú postavy černošiek, belošiek aj aziatok v kostýmoch najrozličnejších farieb, tvarov a strihov. Veľkolepá dúha. Mesto farieb. Doslova.      Maracanã...staňte sa na pár okamihov slávnym futbalistom, vstúpte na rovnaký trávnik, kde kedysi vybehli aj hviezdy ako Pelé, Beckenbauer, Puskás, Zidane a ďalší...Túžbou každého hráča je hrať aj na Maracane. Oprávnene.            Rio de Janeiro...je to ako sen. Sen, ktorý sa vám sníva pred očami. Sen, ktorého sa môžete dotknúť, cítiť ho a on nezmizne. Pokojne snívajte ďalej...        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Nie všetko "značkové" je naozaj "značkové" alebo o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Posledný večer
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Amazónia - Tajomná a očarujúca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Komunisti ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Turanský 
                                        
                                            Z blata do kaluže
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Turanský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Turanský
            
         
        tomasturansky.blog.sme.sk (rss)
         
                                     
     
        Som študent, bývalý výmenný, ktorý si absolvoval rok v Brazílii a teraz si zvyká znovu na realitu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1528
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Brazília
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Andrzej Sapkowski - Boží Bojovníci
                                     
                                                                             
                                            Roald Dahl - Neuveriteľné Príbehy
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Led Zeppelin
                                     
                                                                             
                                            Sex Pistols
                                     
                                                                             
                                            Ramones
                                     
                                                                             
                                            Rádio Expres
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




