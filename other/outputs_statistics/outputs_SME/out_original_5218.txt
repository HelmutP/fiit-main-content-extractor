
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Dudáš
                                        &gt;
                Absurdistan
                     
                 Ako sa maľovala garáž alebo Nie je všetko tak ako vyzerá 

        
            
                                    25.4.2010
            o
            12:48
                        |
            Karma článku:
                3.17
            |
            Prečítané 
            1136-krát
                    
         
     
         
             

                 
                    Bol krásny nedeľný deň, na vidieku oslavovaný, ako inak - prácou. Pani domu so susedami si sedeli na lavičke opreté o bránku pred domom a popíjali kávu po tom, ako úspešne dobojovali s obedom. Deťúrence sa im škobŕtali popred nohy a niečo po sebe vykrikovali. Raz za čas pribehli k mame a poťahali ju za nohavice. Tá ich vždy mávnutím ruky vykázala na trávou polstrovanú hraciu plochu. Pohľadom prebehli všetky prítomné, neúspešne a predsa vytrvalo hľadali najslabší článok, ktorý by bol ochotný ísť im robiť kolotoč.
                 

                 
  
       Domáci pán mal medzitým inú zodpovednú náplň práce - natrieť garáž. Rozkazy boli jasné: vnútro, nabielo. Keďže sa garáž nachádzala hneď naproti cez cestu, stačilo gazdinám prejsť pár krokov od miesta kde sedeli, aby videli ako sa práca vyvíja.   Najstaršie chlapča znova pribehlo ku skupinke dospelých a poťahalo mamu za nohavice. Tá sa naň zhovievavo usmiala: "Choď pomáhať tatovi, dobre?"   Dieťa si niečo zamrmlalo, ale dalo si vysvetliť, že rušiť dospelých pri kávičke sa nevypláca a odišlo.   Chvíľu na to sa pani domáca uprene pozerajúc niekam do diaľky nahlas zamyslela: "Tomu môjmu už asi načisto šibe..."   Niektorému z okolostojacich spolukávipijúcich samotné konštatovanie nestačilo a jednoduchým prečo sa opýtal na dôvod.   "Pozri sa hen - natiera aj okno!" krútila hlavou.   "Čo robím?" opýtal sa muž s novinovou maliarskou čiapkou na hlave. "A kde vlastne máme synátora?"       Poučenie: Nechcite aby vám deti pomáhali s domácimi prácami.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dudáš 
                                        
                                            Jadrová energia? Áno, ďakujem!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dudáš 
                                        
                                            Milá bigotná Slovenka, milý netolerantný Slovák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dudáš 
                                        
                                            Neísť na pohreb vlastnej sestre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dudáš 
                                        
                                            One night in Bangkok – ladyboy, drogy a ananás
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dudáš 
                                        
                                            Majú Slováci šancu stať sa bohatým národom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Dudáš
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Dudáš
            
         
        branislavdudas.blog.sme.sk (rss)
         
                                     
     
        Na mlčanie bude dosť času v hrobe.
 Píšem aj na www.brainspark.sk 
 Pozrite si aj moje fotky 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2626
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Absurdistan
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dan Roam - Nápady na ubrousku
                                     
                                                                             
                                            Dan Ariely - Predictably Irrational
                                     
                                                                             
                                            Richard Phillips Feynman - To snad nemyslíte vážně
                                     
                                                                             
                                            Nassim Nicholas Taleb - The Black Swann
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Florence And The Machine - Between Two Lungs
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mišo Duchoň
                                     
                                                                             
                                            Peťa Dzurovčinová
                                     
                                                                             
                                            Peťo Gergely
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




