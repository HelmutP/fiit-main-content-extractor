

 Možno si po niekoľkých záberoch bobra, ktoré len nedávno odvysielali naše médiá, poviete, veď na tom nič nie je. Mýlili by ste sa. Bobry v poľských parkoch nie sú navyknuté na prítomnosť ľudí, ale skôr na prítomnosť rôznych predátorov. Zahliadnuť ich a vyfotografovať nie je vôbec jednoduché. Pri svojej prvej niekoľkodňovej návšteve sa mi to nepodarilo, tak som bol zvedavý, ako to dopadne na druhý pokus. 
 Do cieľa našej cesty sme dorazili až po zotmení. Narýchlo sme si našli lúku neďaleko príjazdovej cesty a rozložili si spacáky. Aké bolo naše prekvapenie, keď sme ráno asi štyri metre od našich spacákov našli pravdepodobne zubrí trus. 
 Zbaliť veci a vyraziť. Približne po hodine a pol nachádzame prvé bobrie obydlie. Čerstvo obhlodané kmene stromov a kríkov naznačujú, že obydlie je obývané.  
 
 






Bobry si hravo poradia aj s hrubými kmeňmi stromov.

Zväčšiť 




 Prítomnosť bobrov potvrdzujú i vyšmýkané cestičky smerujúce k bobriemu hradu. My sa však rozhodujeme ísť ďalej. Nevyhovuje nám prostredie. Tentoraz si bobry vybrali za svoj domov človekom vytvorený umelý rybník. Nás však zaujímajú pravé bobrie priehrady a nádrže. Berieme do ruky mapu a vytipujeme si ďalšiu lokalitu. 
 Približne o ďalšiu trištvrte hodinku nachádzame bobriu hrádzu ukrytú v lese. 
 
 






Zobraziť panorámu




 I tu sú zjavné známky osídlenia. Miesto sa nám páči, ale ešte nie je ani poludnie a do večera, kedy sa bobry prebúdzajú, je ešte ďaleko. Rozhodujeme sa preskúmať ďalšie územie s tým, že sa sem večer vrátime. 
 Nevrátili sme sa. Našli sme ešte mnoho ďalších bobrích obydlí, z ktorých časť bola obývaná a z časti sa už zrejme bobry odsťahovali. 
 
 






Zobraziť panorámu




 
 






Zobraziť panorámu




 Nakoniec sme sa usídlili vedľa veľkej bobrej priehrady. Podľa množstva čerstvo obhryzených stromov a vychodených chodníčkov sme usúdili, že by tu mohla byť najväčšia pravdepodobnosť uvidieť a vyfotografovať bobra. 
 
 






Bobry si dokážu poradiť aj s hrubým bukom.
Zväčšiť




 
 






Bobroviská priťahujú množstvo rôznych druhov vtákov.  (Pinka lesná na bobrom ohlodanom strome.)
Zväčšiť




 Hneď popoludní sme začali s pravidelným monitorovaním hladiny najväčšej bobrej priehrady, no nič sa nedialo. Slnko sa už dotýkalo horizontu, keď sa z nádrže ozvalo hlasné "prásk". Spočiatku sme mu nevenovali pozornosť. Po niekoľkých minútach sa ozvalo ďalšie prásk. To sme už spozorneli. Podišiel som k nádrži a pozoroval hladinu. Nič podozrivé som nevidel, čo by nasvedčovalo aktivite bobrov. O chvíľku sa priamo predo mnou ozvalo ďalšie hlasné prásk. To už privolávam ostatných. Niečo sa tu deje. A opäť prásk. Ešte nestihli ani dôjsť ku nádrži a opäť prásk, prásk, prásk, ... A to sme už všetci videli ako sa priamo pred nami do vody zrútil strom. Prekvapene sme pozreli na seba a čakali, čo sa bude diať. Hladina sa postupne utíšila a nikde nič. Moji spoločníci sa už vrátili k načatej večeri. Mne to ale nedalo a zostal som na brehu jazierka. Po chvíľke som pod stromami spadnutými do vody spozoroval pohyb. Myslel som si, že je to jedna zo žiab, ktorých bolo v jazierku neúrekom a celý deň nám nehanebne predvádzali svoje orgie. 
 
 






V okolí bobrích hrádzí sa darí aj plazom a obojživelníkom.
Zväčšiť




 
 






Žaby nehanebne predvádzali svoje orgie.
Zväčšiť




 Tentokrát to však žaba nebola. Jasne rozoznávam bobriu hlavu, ktorá obozretne kontroluje okolie nádrže z prítmia popadaných stromov. Rýchlo privolávam ostatných členov "expedície". Cvak, cvak cvak, ukrytí na brehu pozorujeme správanie bobra. Niekoľkokrát sa vynoril a keď nás spozoroval, s hlasným plesknutím chvosta o hladinu zmizol v kalnej vode. 
 
 



((boborvideo))


 
Autor videa: Emil Grobauer




 
 



((boborvideo2))


Žaby s bobrami zjavne nažívajú v priateľskom spolužití. Občas bobry používajú ako dopravné prostriedky. 
Autor videa: Emil Grobauer




 
 








Bobrov sme sa dočkali až za súmraku.
Zväčšiť




 Hoci sme bobry zazreli ešte niekoľkokrát, svetelné podmienky mi už nedovolili fotografovať. 
 Misia splnená, aj keď stále je čo vylepšovať. Cestou domov preberáme možnosti návratu. Kto raz uvidí prírodu Bieszczad, má nutkanie sa do nej vrátiť. Máme čo závidieť. Teším sa na čas, kedy aj u nás budú také krásne bobrie priehrady a chránené územia sa stanú skutočne chránenými. Tak, aby si v nich bobry dokázali nájsť svoje miesto pre hrádzu bez obáv, že im ju "prevalcuje" lesný traktor. 
 Na záver dobrá správa, hlavne pre rodiny s deťmi: "bieszczadska kolejka" už premáva. 

