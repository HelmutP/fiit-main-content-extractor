

 Keď sa mi malí tmolia pod nohami, vyťahujú mi z cukru lyžičku na oblízanie a varešku z misy na ochutnanie, nemám to srdce ich odháňať. Čo rozsypú poutieram a vysvetľujem, na čo  potrebujem to aj ono. Veď som bývala rovnaká. Najradšej v kuchyni s babičkami. 
 Zatvorím oči a počujem prababičku, ako mi vysvetľuje, že štrudľové cesto musí byť také tenké, aby sa cez neho dali noviny čítať. Vytieram pekáče tak, ako to robila babička, a počujem zasa ju - „krupica je na vysypanie lepšia a nezabudni si dobre rozrátať, ktorú časť koláča treba robiť ako prvú". Mám špeciálne hrnce na roztápanie tuku, na sladké a slané, ako mala ona. Využívam rozpálenú rúru, aby zbytočne nechladla, ako ma to obidve  naučili. Miešam ich vareškami a mám aj ich pomôcky, podedené. A najmä knihu receptov. 
 Myslím na nich pri roztĺkaní orechov, pri roztápaní polevy aj odkladaní sladkých zvyškov na inokedy. V duchu s nimi hovorím, keď sa mi niečo nedarí a pýtam si radu. A usmievam sa, keď si predstavím, ako by sa tešili, že koláč zmizol pol hodiny po vytiahnutí z rúry. 
 V  kuchyni mojich babičiek bolo vždy krásne a sladko. Špajza bola vždy plná dobrôt aj do zásoby. Vždy mali poruke maškrty a nápady na nové dobrôtky. V pohotovosti zásterky, a najmä vždy čas byť v tej kuchyni spolu. Aj spolu s nami. 
 Milujem svoju kuchyňu prehriatu a voňavú. Plnú ľudí, čakajúcich aj sýtych, alebo len zvedavo nakúkajúcich do rúry a hrncov. 
 Zajtra budem piecť koláčiky pre priateľov. So svojimi deťmi, pár nechám aj im. Teším sa na koláčiky. Ale neviem, či nie viac na to pečenie. :) 

