
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Dunaj
                                        &gt;
                Nezaradené
                     
                 Konkurz do zamestnania 

        
            
                                    11.6.2010
            o
            14:36
                        (upravené
                12.6.2010
                o
                8:01)
                        |
            Karma článku:
                3.34
            |
            Prečítané 
            1143-krát
                    
         
     
         
             

                 
                    Máme deň pred voľbami. Deň kedy nehodno ani do blogov nazrieť, pretože budú plné predvolebných agitácií. Posledné záchvevy a výkriky typu „Voľte toho! Voľte hentoho!, či priamo bez hanby „Voľte nás!“
                 

                 Bez hanby píšem schválne, nakoľko keď si predvolebnú situáciu premietnem do pracovného výberového konania, kde príde skupinka sebavedomých nezamestnaných , oblečených do rovnakých tričiek a za sprievodu dychovky začne zamestnávateľa presviedčať „Zamestnajte nás!“, alebo kde prichádzajú jednotlivci ohovárajúci tých ďalších za dverami, v sakách, či v tričkách, s transparentmi a make-upom, tak mi to príde celé bizarné.   Lenže presne na toto ten zamestnávateľ zaberá. Pretože tým zamestnávateľom sme my, každý, ja i ty. Sedím na porotcovskej stoličke a na malé pódium prichádza prvý adept – má červené tričko, tvrdé vlasy a neľútostivý pohľad. Premeria si ma a spustí. Spustí o tom, aký má Slovensko najnižší deficit v rámci Európy, ako pravica kradne, etc., koľko reforiem urobil, koľkým zabránil, koľko dobrých rozhodnutí mal...pozerám medzitým jeho cévéčko. Áno, chlapík sa dostal už pomerne vysoko, dostávam do rúk referencie v podobe bilboardov. Hmm...niečo tu nesedí...resp. nevonia...nik ho nemá rád. „Prosím Vás“...idem sa ho opýtať....zahriakne ma. Opýta sa sám seba, aj si odpovie. A odpovie aj za mňa...presne tak, ako som to videl v telke predtým, než som ho pozval na tento konkurz. Píšem si do poznámok „príliš arogantný, príliš autoritatívny, málo obľúbený“... „Ďakujem, ozveme sa Vám“, odprevádzam ho frázou, ktorá z úst personalistov neveští nič dobré. Ešte vo dverách sa vzprieča a rozpráva mi v kuse o Slovensku, medzitým však prichádza ďalší, tentokrát modrý. Povie pár všeobecných fráz a potom spustí masívne ohováranie toho čo bol vnútri pred ním. Otváram oči...“Ďakujem, ozveme sa Vám, ďalší!“ Vchádza postarší, neurčitý, s pohľadom kázateľa a krížom na krku. Hovorí mi o novej ceste a rozohňuje sa o hriechoch toho, ktorého som zamestnával doposiaľ. Na chvíľu zauvažujem nad jeho vierovyznaním a s už známou frázou ho odprevádzam k dverám.    Takto to pokračuje až do neskorých popoludňajších hodín, striedajú sa tváre, tričká, starší i mladší, každý má niečo zaujímavé alebo excentrické vo svojom programe. Na chvíľu sa zamyslím nad tým, ako by prosperovala moja firma, keby som vyskladal jej portfólio práve z tých najpovážlivejších bodov jednotlivých kandidátov, vtom ma vyruší vchádzajúci adept. Má sivé vlasy, prísny pohľad a úžasný make-up. Sadne si a začne mi bez vyzvania rozprávať, ako sa tí cigáni čo mi zametajú chodník pred firmou ulievajú, že on by to neživil. Potom zmení taktiku a začne sa rozohňovať nad mojím obchodným partnerom zo susedného južného štátu. „Ale veď...viete, my máme korektné vzťahy, berieme od nich tovar“, slušne mu oponujem, aby som ho presvedčil, že corporate identity nehodno narúšať preklínaním dobrých partnerských vzťahov. Je radikálny a razantný...“Ďakujem, ozveme sa Vám“. Ešte vo dverách si čosi nepekné zamrmle na dámu s oranžovou gerberou, ktorá vchádza dovnútra. Predstavuje sa mi ako JUDr., a začne úplne mimo ekonomickosocialistickonáboženskonárodovecké témy. „Mám syna, raz bude ako Vy. Vychovávam ho sama a chápem všetky ženy, ktoré žijú samé, alebo s deťmi, prípadne s partnerom, ktorý im nevyhovuje. No chápem aj mužov, ktorým nevyhovujú partnerky, či hospodárska situácia. Ja by som vo Vašej firme rada lobovala za tento sektor a pripravovala podmienky pre Vašich budúcich zamestnancov, ktorí v tomto období ešte len nosia plienky.“ Usmeje sa a čaká na moje otázky. Pýtam sa jej pozerajúc si jej kvalitné cévéčko: „A nebudete často na óčeérke?“ „Som tu práve preto, aby ani ostatné Vaše zamestnankyne nemuseli riešiť tento problém“, usmeje sa a ja sa usmievam tiež. Potenciál mojej firmy totiž dosť spočíva v spokojnosti jej zamestnancov, pretože viem, že nie firmy, ale ľudia robia obchod.   „Dobre pani Pfundtnerová, od pondelka môžete nastúpiť.“ 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Keď nás tu bude 100 000 Jano sa oholí!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Manažérov denník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Najlepší kúzelnícky trik na svete!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Funkčná rodina II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Funkčná rodina
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Dunaj
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Dunaj
            
         
        dunaj.blog.sme.sk (rss)
         
                                     
     
        Žijeme tak krátky život až mi je to smiešne ako sa staviame do roly toho, ktorý má vo svojom eLVéčku všetky planéty. Niekto je hore a niekto je dolu. Ten dolu je na ceste a potkýna sa o ďalších
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    694
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




