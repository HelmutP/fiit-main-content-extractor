

 Len včera ráno sa dozvedela, že v pondelok jej zomrel dobrý kamarát. Bol len o pár rokov starší ako ona a už dlhší čas mal problémy s drogami. Bol však na liečení, no zjavne to psychicky nezvládal. . . preto svoje problémy ukončil po svojom. . . skokom z okna! O necelých 24 hodín od prijatia tejto správy sa scenár zopakoval - mladá dievčina, ktorá ešte navštevovala len základnú školu, si vzala dobrovoľne život takým istým spôsobom! 
   
 Vôbec som týchto mladých ľudí nepoznala, hoc bývali neďaleko, no pri čítaní riadkov plných veselých spomienok a úprimných sústrastí od priateľov či úplne neznámych ľudí sa aj mne tisli slzy do očí. 
 Veď prečo rieši mladý človek takto svoje problémy? Dennodenne zomierajú mladí preto, lebo si myslia, že ich problém sa nedá vyriešiť. . . A pritom stačí tak málo. 
 Vôbec si neviem vysvetliť ich počínanie, aký musí nastať skrat v ich mysli, keď za svoje jediné riešenie pokladajú smrť. Avšak asi nie som jediná. . . 
 Len dúfam, že tam hore je vám lepšie. . . 

