

 Bola som tam len ja a pani magistra. Po mojej kultivovanej žiadosti sa ona zasekla ako robot - v polohe - pravá ruka zdvihnutá a ústa otvorené. Zo zadu prišiel pán, myslím si, že jej manžel, tiež od fachu. Vyzeral dosť zdesene. Koľko? A načo vám toho toľko je. Spýtal sa ma. Ja som sa začudovane pozerala. No, oni sa mi zvyknú zlomiť, tak mi ich dajte radšej viac. A poprosím aj pečiatku, aby mi to preplatili. 
 Magistra siahla do zásuvky a začala rátať. Nie veľmi ochotne. On si ma zatiaľ úplne bez hanby obzeral. 
 Kým mi to doplo, čumela som zasa ja. A potom som začala vysvetlovať. 
 Injekčné striekačky potrebujem na dávkovanie kvapaliny do prístroja. Ihly sa mi občas zvyknú zlomiť,alebo upchať, preto ich chcem raz toľko. A najhrubšie mi vyhovujú najviac. 
 A môžem vám ukázať ruky, že si naozaj nič nepichám. 
 Potom už nasledoval len účet s pečiatkou a náš dlhý smiech. 

