
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kluvánek
                                        &gt;
                Nezaradené
                     
                 Kopernik a začiatky heliocentrizmu 

        
            
                                    15.3.2010
            o
            20:21
                        (upravené
                16.3.2010
                o
                12:54)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            4085-krát
                    
         
     
         
             

                 
                    Kopernikov heliocentrický model je považovaný za rozbušku, ktorou začala vedecká revolúcia. Zároveň je to aj začiatok najdlhšie trvajúceho (priam archetypálneho) konfliktu medzi vedou a cirkvou. Kopernik, spolu s Galileim, je oslavovaný nielen ako geniálny vedec ale aj ako neohrozený bojovník za pravdu. Ako to v takýchto prípadoch býva, priebeh skutočných udalostí bol postupne nahradený všeobecne prijímanou „históriou". Tá sa ale časom stala viac legendou než objektívnym popisom. Radi by sme preto pripomenuli pár historických faktov o Kopernikovi a o prvopočiatkoch heliocentrizmu.
                 

                 
  
   Mikuláš Kopernik (obr. 1) sa narodil v Toruni 19. 2. 1473. Ako desaťročný stratil otca, neskôr aj matku. O jeho výchovu a vzdelanie sa staral strýc, neskorší Warmijský (oblasť na severe Poľska) biskup. Kopernik študoval na univerzite v Krakove, kde sa prvý krát stretol s astronómiou. Štúdium v Krakove neukončil (čo bolo v tej dobe pomerne časté) a po krátkej pauze pokračoval v Boloni a Padove. Tam študoval medicínu a právo. Intenzívne tiež spolupracoval so známym astronómom Máriom de Novarom. Už tam ho vraj začali prvý krát nahlodávať myšlienky heliocentrizmu. Medicínu doštudoval v Boloni, doktorát z kanonického práva nakoniec získal vo Ferrare. Bol všestranná osobnosť. Ovládal niekoľko jazykov, bol cirkevný právnik, lekár, prekladateľ, umelec a zastával mnohé významné funkcie. Okrem iného bol diplomat a generálny administrátor warmijskej oblasti. Riadil obranu hradu Olštýna pri nájazdoch nemeckých rytierov. Publikoval štúdiu Traktát o minci, v ktorom riešil problémy inflácie a zhodnotenia meny. Matematike a astronómii sa venoval len „popri zamestnaní", no aj to stačilo, aby sa stal sa jedým z najvýznamnejších vedcov histórie. Svojim heliocentrickým modelom postavil astronómiu z hlavy na nohy a odštartoval vedeckú revolúciu. Z jej výsledkov ťažíme až do súčasnosti. Pripomenutím jeho vedeckých zásluh je aj výrazný kráter na Mesiaci, nesúci jeho meno a pomenovanie prvku 112 Copernicium, Cp.      Obr. 1 Portrét Kopernika zo 16. storočia a rekonštrukcia jeho tváre podľa lebky nájdenej v hrobe pri oltári vo Fromborskej katedrále. DNA lebky súhlasí s DNA vlasov, nájdených v jednej z kníh z Kopernikovej knižnice, ktorá sa, ako vojnová korisť, nachádza na univerzite v Uppsale, Švédsko.   Detronizácia ľudstva a heliocentrizmus   Za najdôležitejší a najodvážnejší Kopernikov čin je všeobecne považované zosadenie Zeme z privilegovanej pozície v centre vesmíru a jej odsun medzi „bežné" planéty. Kopernik tým zasadil prvú ranu ľudskej predstave o vlastnej výnimočnosti (za ďalšie rany sú považované objavy, že Slnko nie je v strede našej Galaxie, že naša Galaxia nie je jediná a rozhodne nie je v centre Vesmíru, že planéty existujú aj pri iných hviezdach a pod.) Oslavovali ho za to umelci, filozofi, vedci (od astronómov a fyzikov až po biológov) aj psychológovia (Freud hovoril o Kopernikovej urážke „naivnej sebelásky" ľudstva). Podľa súčasných predstáv boli filozofi a teológovia v tej dobe presvedčení o jedinečnom postavení ľudskej rasy, čomu zodpovedalo postavenie Zeme v centre vesmíru. Táto rozšírená predstava má však len málo spoločné so skutočnými názormi ľudí v časoch Kopernika a Galileiho. Grécki filozofi, najmä v stredoveku uznávaný Aristoteles, spoločne s arabskými, židovskými a kresťanskými učencami totiž vôbec nepovažovali stred Vesmíru za  privilegované a najlepšie miesto. Podľa nich to bola skôr zatuchnutá pivnica, kde bolo  ľudstvo odsúdené zotrvávať na nehybnej Zemi. Pozícia v strede Vesmíru bola dôkazom hrubosti a skazenosti hmoty a ľudí. Nebo bolo „hore" nad Zemou. Čím vyššie ste sa dostávali do nebeských sfér, tým tam vládli dokonalejšie zákony a žili dokonalejšie bytosti. Peklo, naopak, bolo umiestnené do samotného centra Zeme, ako najskazenejšie miesto. Ešte v roku 1615 kardinál Bellarmine, známy žalobca Galileiho, povedal, že „Zem je veľmi ďaleko od neba a sedí nehybne v strede sveta". Koperníkovo učenie preto neurazilo pýchu ľudí 16. storočia, skôr ich pozdvihlo bližšie k Bohu.   Ďalším „faktom", ktorému sa dnes všeobecne verí, je, že Kopernikov heliocentrický model bol od začiatku podstatne jednoduchší a presnejší než starý Ptolemaiov geocentrizmus. No cirkev nútila vedcov používať geocentrizmus, čím brzdila vývoj vedy. História je v tomto prípade opäť trochu iná. V Koperníkovej dobe bol všeobecne akceptovaný Ptolemaiov geocentrický systém a Aristotelova prírodná filozofia. Tie hovorili, že Zem stojí nehybne v strede Vesmíru, nie že je stredom Vesmíru (to je dosť podstatný rozdiel). Do stredu sa dostala preto, že bola ťažká (nie že by bola privilegovaná). Podľa Aristotela totiž ťažké veci padali do stredu Vesmíru (napr. kamene, jablká, ľudia) a ľahké veci stúpali do neba (napr. plamene). Zem sa nachádzala k stredu najbližšie ako to len bolo možné a bola preto nehybná (nebolo už kam ďalej padať). Na naše pomery to nebola silná vedecká teória, ale vysvetľovala aspoň každodennú skúsenosť. Slnko a ostatné planéty sa pohybovali podľa Ptolemaia po dokonalých kružniciach okolo Zeme vo vyšších (nebeských) sférach. Aby sa vysvetlil komplikovaný zdanlivý pohyb planét po oblohe (opisujú slučky na hviezdnom pozadí, pohybujú sa voči hviezdam raz jedným, potom druhým smerom, dokonca sa zastavujú) ku kruhovej dráhe sa pridávali dodatočné kruhové epicykly (Obr. 2). Vznikol tak zložitý Ptolemaiov systém viac ako 40 kružníc, vysvetľujúci kvalitatívne aj kvantitatívne pohyby planét. Rovnako aj Kopernik použil dokonalé kruhové dráhy planét. Ani on sa nedokázal odpútať od Aristotelovej myšlienky, že pohyby nebeských telies sú dokonalé = kruhové. Musel preto zaviesť epicykly. Oba systémy tak nielen že dávali prakticky rovnaké výsledky, ale boli takmer rovnako komplikované (kým Kepler neprišiel s elipsami). Prijatie Koperníkovho heliocentrizmu teda nebolo len výmenou dvoch, viac menej podobných astronomických systémov, no znamenalo súčasne odmietnutie celej dovtedajšej fyziky a astronómie. Navyše prinieslo vlastné problémy: ak sa Zem pohybuje, ako to, že to necítime?, čo ju vlastne dalo do pohybu?, prečo hmotné veci padajú na Zem, keď už nie je v strede Vesmíru?, prečo nie je stred Vesmíru v strede Zeme, toho najhmotnejšieho objektu?,... No a v tej dobe neexistovali nijaké presvedčivé vedecké argumenty, prečo by mal byť Koperníkov heliocentrizmus správny - neboli jednoznačné dôkazy o pohybe Zeme.      Obr. 2 Ptolemaiov geocentrický model s epicyklami (červené). V skutočnosti mal ešte epicykly na epicykloch.   Kopernikov systém tak narazil na odpor. V prvom momente však nie zo strany katolíckych teológov, ale zo strany vedcov. Kopernikova práca, 6 stranový Malý komentár, obsahujúca heliocentrický systém, kolovala medzi astronómami už od roku 1514. Jeho prelomové dielo De Revolutionibus Orbium Coelestium (O pohyboch nebeských sfér, Obr. 3) vyšlo až v roku 1543, tesne pred Kopernikovou smrťou, celých 28 rokov po zverejnení heliocentrického systému. Kopernikova idea preto v tej dobe už nebola nijakou novinkou. S určitým ohlasom sa stretla u astronómov, ktorí ju brali ako jeden z príspevkov k vysvetleniu pohybu planét. Samotný predpoklad o pohybe Zeme bol považovaný skôr za matematický trik, umožňujúci spresniť výpočty. Podľa niektorých historikov aj samotný Kopernik zastával tento názor a v pohyb samotnej Zeme nikdy neveril. Svoje priklonenie sa k myšlienke heliocentrizmu vysvetľoval potrebou vytvorenia astronomického modelu vhodnejšieho na tvorbu kalendárov a predpovedanie polôh hviezd než ten Ptolemaiov.   Vedci a filozofi Kopernikovej dobe tak väčšinou zostávali aj naďalej pri Prolemaiovom geocentrizme. Až spresnené merania Tycha de Brahe (1546 - 1601, mimochodom odporcu heliocentrizmu) ukázali lepšiu presnosť Kopernikovho modelu a rozhodujúci krok smerom k akceptovaniu heliocentrizmu spravil Kepler (1571 - 1630). Ten nahradil kruhové dráhy eliptickými. Zjednodušil tak heliocentrický systém a spresnil výpočty. Zároveň Galilei (1564 - 1642) priniesol nielen dôkazy v prospech Kopernika, ale ukázal aj potrebu vypracovania novej fyziky. Všetko nakoniec zavŕšil Newton (1642 - 1727) v svojom kolosálnom diele. Heliocentrizmus bol teda definitívne prijatý až viac ako storočie po Kopernikovi.   Thomas Kuhn, známy filozof vedy, vidí v Kopernikovom prípade typickú ukážku priebehu vedeckej revolúcie. Kopernik, podľa Kuhna, v podstate zlyhal vo vysvetľovaní pohybu planét. Jeho revolučná myšlienka pohybujúcej sa Zeme však našla odozvu u niekoľkých jeho nasledovníkov. Tí jej nakoniec zabezpečili prijatie medzi konzervatívnou väčšinou vedeckej komunity.      Kopernik a cirkev   Reakcia katolíckej cirkvi na zverejnenie Kopernikovho heliocentrického modelu mala byť prudká a rýchla, pretože toto učenie odporovalo Biblii. Reálne to však začalo medzi prívržencami heliocentrizmu a cirkvou „škrípať" pól storočia po Kopernikovej smrti.  Kopernik, sám vážený člen katolíckeho kléru, nemal nijaké problémy pre svoje vedecké názory. Jeho heliocentrická hypotéza bola známa od roku 1514. V roku 1533 prebehli v Ríme prednášky o Kopernikovom modeli, na ktorých sa so záujmom zúčastnili niekoľkí kardináli, dokonca sám pápež Klement VII. V roku 1536 arcibiskup Schönberg listom nabádal Kopernika k urýchlenému publikovaniu jeho myšlienok. De Revolutionibus... bol nakoniec publikovaný až v roku 1543, prakticky v čase Kopernikovej smrti, s venovaním pápežovi Pavlovi III, ktorému sa listom ospravedlnil za podivný návrh o pohybe Zeme. Na vydaní knihy dohliadal Koperníkov spolupracovník matematik Rheticus. V záverečnej fáze sa však musel vrátiť na univerzitu a požiadal o pomoc protestantského teológa Andreasa Osiandera. Ten pridal ku knihe neautorizovaný úvod, obraňujúci knihu pred prípadnými útokmi zo strany teológov. Okrem iného zdôraznil, že v prípade heliocentrizmu sa jedná len o nedokázanú hypotézu. Zdá sa teda, že Kopernik s vydaním svojej knihy váhal. Pravdou je, že heliocentrizmus si mnoho obdivovateľov medzi vedcami nezískal ani 28 rokov od svojho publikovania a myšlienka pohybujúcej sa Zeme sa zdala mnohým vtedajším vzdelancom vyslovene smiešna. Kopernik si zároveň určite uvedomoval, že jeho teória sa dostáva do sporu s Bibliou, hoci sa neobjavili proti nej výhrady zo strany katolíckej cirkvi. Navyše nesmieme zabúdať na všetky povinnosti uberajúce Kopernikovi z času, ktorý mohol venovať písaniu svojho diela. Otázka, či sa bál viac reakcie vedcov alebo teológov, či jednoducho nemal dostatok času na písanie, teda zostáva otvorená.    Na rozpory Kopernikovej teórie a Biblie ako jeden z prvých verejne poukázal už v roku 1539 Luther. Známy je jeho výrok že Jozue nezastavil Zem ale Slnko. Reformácia, zdôrazňujúca Bibliu ako fundamentálny základ kresťanského učenia, bola na tieto otázky zvlášť citlivá. Melanchthon, Kalvín a ďalší potom našli množstvo iných protirečení a aktívne vystupovali proti heliocentrizmu. Novo založené protestantské cirkvi však nemali prostriedky na tak silný tlak ako katolíci. Na druhej strane dokázali pružnejšie zareagovať a rýchlejšie akceptovali heliocentrizmus po jeho vedeckom dokázaní než väčšia a staršia katolícka cirkev s prepracovanou hierarchickou štruktúrou.   Prvá negatívna reakcia na Kopernikovo učenie zo strany katolíckeho kléru prišla v roku 1546. Argumenty dominikána Tolosaniho a cenzora Spinu však rýchlo upadli do zabudnutia. Heliocentrizmus bol, ako hypotéza, prednášaný dokonca aj na univerzitách a zaoberali sa ním aj jezuiti. Dlhé roky prakticky bez výraznejšieho ohlasu na revolučné názory viedli až k teórii, že prvé vydania Kopernikovej knihy sa prakticky nečítali. To vyvrátil v roku 2004 Gingerich podrobnou prehliadkou všetkých existujúcich zväzkov z prvých dvoch vydaní De revolutionibus... (našiel množstvo poznámok od pôvodných majiteľov). Kopernikova práca tak nezostala nepovšimnutá, skôr môžeme povedať, že jeho myšlienky zreli.   Giordano Bruno, upálený v roku 1600, je predstavovaný ako prvá obeť boja cirkvi proti heliocentrizmu. Pravdou ale je, že Bruno bol upálený najmä ako čarodejník a heretik. Podľa Cavendishových Dějin magie Bruno „dúfal v zrušenie kresťanstva a obnovenie egyptského náboženstva", „otvorene priznával, že nadprirodzené sily, ktoré svojou mágiou ovláda, sú diablami" a okrem iného bol tento skrachovaný dominikán exkomunikovaný ako luteránskou tak aj kalvínskou cirkvou. Brunovo vedecké presvedčenie pri odsúdení nezohrávalo výraznejšiu úlohu, hoci jeho široké rozvinutie heliocentrizmu, z ktorého vyvodzoval závažné teologické dôsledky (spochybnenie božskej podstaty Ježiša Krista, panteizmus,...), mu pred tribunálom určite nepomohlo. Jeho proces začal byť interpretovaný ako protivedecký až v 19. storočí.   Skutočný odpor cirkvi proti heliocentrizmu sa objavil až s príchodom ďalších argumentov v jeho prospech (najmä v spojitosti s Galileom, čo je už ale iný príbeh) a s reakciou na reformačné hnutie. Našlo sa veľké množstvo protirečení medzi cirkevným učením a heliocentrizmom. Kopernikov De Revolutionibus... sa dostal v roku 1616 na Index a vyčiarknutý z neho bol až v roku 1835 (ostatné diela, obhajujúce heliocentrizmus, boli z Indexu vynechané „už" v roku 1758). V rámci propagandy použili katolíci aj samotný nepravdivý argument o Kopernikovej degradácii ľudstva a odsune Zeme z význačného postavenia vo Vesmíre. Prvý krát sa objavil po roku 1650, v čase akceptovania heliocentrizmu vedcami, zrejme ako pokus získať na svoju stranu ľudové masy. Edikt inkvizície voči Kopernikovi bol zrušený v roku 1822, no formálnu bodku za stáročným nešťastným sporom dal až Ján Pavol II v roku 1992 zrušením ediktu proti Galileimu.       Peter Kluvánek.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Zákernosti novej kategorizácie liekov, alebo ako ušetriť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Paradox dvojčiat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Všeobecná teória relativity – deväťdesiatnička s tuhým koreňom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Zaostrené na atómy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Bezpečnosť cestnej premávky z hľadiska fyziky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kluvánek
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kluvánek
            
         
        kluvanek.blog.sme.sk (rss)
         
                                     
     
        Hlava rodiny, občas sa venujúca aj iným radostiam (práca, šport, fyzika, knihy,...), výsledky ktorých niekdy naberajú elektronickú formu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5238
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




