
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriela Kisová
                                        &gt;
                Vizuálne umenie
                     
                 Súčasná kresba v Košiciach 

        
            
                                    19.4.2010
            o
            16:28
                        (upravené
                20.4.2010
                o
                12:34)
                        |
            Karma článku:
                2.90
            |
            Prečítané 
            961-krát
                    
         
     
         
             

                 
                    Keď si do Wikipédie zadáte pojem kresba, prečítate si toto: „Kresba je vizuálne umenie, ktoré je vytvárané množstvom kresebných pomôcok na dvojrozmerné médium. (...) Umelec, ktorý kreslí môže byť označený ako kreslič (návrhár)." Táto suchopárna definícia vám ale ani zďaleka nenaznačí, aký bohatý je register súčasnej kresby a ako v tomto médiu pracujú súčasní „kresliči" a „kresliarky". Aktuálna výstava vo Východoslovenskej galérii v Košiciach s názvom Nenápadné  médium je práve projektom, ktorý mapuje rozličné polohy kresby  u vybraných autorov a autoriek viacerých generácií. Jej kurátorka Mira  Sikorová-Putišová zostavila výber, po zhliadnutí ktorého začnete  pochybovať o prívlastku v názve výstavy.
                 

                 
Hartworkers, záber z inštalácie, VSG 2010Gabriela Kisová
   Už úvod výstavy avizuje, že nejde o tradičný prehľad kresieb na papieri. Práca dvojice Hartworkers (Lucia Dovičáková a Jozef Tušan) je veľkorozmernou kresbou na stenu. Autori v nej zobrazili seba v rôznych konfliktných situáciách. Sčasti autobiografické dielo má byť posledným výstupom dvojice, ktorá pri práci očividne zažíva viac starostí ako radostí. Reflexia spoločnej umeleckej tvorby prebehla našťastie s nadsázkou a humorom a výsledok spoločných bojov stojí za to.   Autobiografický základ majú aj ďalšie práce na výstave. Vizuálny denník Svätopluka Mikytu z rokov 1999-2009 je priestorovou inštaláciou, do ktorej môže divák priamo vstúpiť a takpovediac prehrabávať sa v jeho autoportrétoch. Kresba je integrálnou súčasťou Mikytovej tvorby a táto práca ju ukazuje v procese ako malý súkromný archív. Virtuálny skicár (2010) Veroniky Rónaiovej je tiež inštaláciou, v ktorej autorka vo fragmentárnej podobe „preniesla" tvorivý chaos svojho ateliéru na galerijnú stenu. Jej príspevok obsahuje v koncentrovanej forme mnohé z kľúčových tém, okolo ktorých sa točí celá výstava: procesuálnosť, fragmentárnosť, koncepcia, priestorovosť, znakovosť, intermediálne presahy, pohyb a čas v súčasnej kresbe.   Práve posledné dva aspekty sú príznačné aj pre tvorbu nemeckej umelkyne Anny Tretter, ktorá je na výstave zastúpená konceptom s názvom From Košice to Žilina (2005-2007). Sediac vo vlaku s papierom na nohách sa autorka odovzdala do role pasívneho seizmografu, zaznamenávajúceho pohyby a rytmus ubiehajúcej cesty. Výstupom sú datované kresby a video. Hegel považoval kresbu za vrcholové umenie, pretože odhaľuje rukopis a genialitu autora. Postmoderný koncept Anny Tretter popierajúci vlastný rukopis je zasadený do opačného diskurzu, ktorý pochoval predstavu geniálneho tvorcu a nahradil ho autorom, strácajúcim kontrolu nad vlastnou tvorbou.   Okrem prác na papieri, plátne a textile (P. Kalmus, B. Sirka, V. Frešo, M. Blažo, J. Bartusz, V. Žáková), ďalších multimediálnych inštalácií (R. Čerevka, M. Murín), záznamov intervencií a konceptov (M. Lányi, M. Kvetan) sú obohatením výstavy animované videá Daniely Krajčovej a Mikuláša Podprockého. Na záver mi nedá nespomenúť náčrty Rudolfa Sikoru, takzvané „mindmaps", ktoré sú verejnosti prezentované po prvý raz. Pôsobivé skice pre inštalácie Sikorových retrospektívnych výstav v Prahe a Bratislave sú na jednej strane pracovnými verziami, ktoré vznikli rýchlym ťahom, no v ich dôslednosti dokumentujú autorovo konceptuálne myslenie v „kozmických" súvislostiach. Výstava ponúka zaujímavý prehľad aktuálnych podôb kresby a koketuje s posúvaním hraníc média, ktoré patrí do základnej výbavy každého umelca. Možno nenápadné, ale nenahraditeľné médium.   Výstava vo VSG v Košiciach potrvá do 3.5.2010 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Kisová 
                                        
                                            Košický public art na pol ceste
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Kisová 
                                        
                                            Aké umenie majú v nemeckom parlamente
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Kisová 
                                        
                                            Fotografia, ktorá mi nedá pokoj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Kisová 
                                        
                                            V letnej čitárni so slovenskými kurátormi a kurátorkami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Kisová 
                                        
                                            S Jarom Kyšom o bozkávaní ruskej vlajky a o kŕmení holubov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriela Kisová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriela Kisová
            
         
        kisova.blog.sme.sk (rss)
         
                                     
     
        Vizuálne umenie ma zaujíma. Venujem sa mu pracovne a aj vo voľnom čase. Kvalitné výtvarné umenie by malo byť v našej spoločnosti viac zakotvené a preto o ňom píšem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2147
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vizuálne umenie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            DESIGNBOOM
                                     
                                                                             
                                            HORE BEZ - Tv.SME o umení
                                     
                                                                             
                                            Flash Art
                                     
                                                                             
                                            VERNISSAGE.Tv
                                     
                                                                             
                                            ARTYCOK.Tv
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




