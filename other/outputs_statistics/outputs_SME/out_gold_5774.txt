

   
 Turnaj organizuje Sekcia nevidiacich a slabozrakých športovcov Slovenska (SNSŠS). Organizačne turnaj zabezpečuje klub Štart Levoča a koná sa v priestoroch Slovenskej knižnice pre nevidiacich Mateja Hrebendu v Levoči. 
   
 Listina účastníkov:                     Elorating 
 1. Orvisky Vladimír                      1880 
 2. Murín Miroslav                         1852 
 3. Škultéty Stanislav                    1844 
 4. Karnek Ján                               1751 
 5. Gajdoš Ján                               1731 
 6. Kováč Imrich                             1713 
 7. Dendis Emil                               1697 
 8. Felong Peter                             1681 
 9. Hovančík Marcel                        1676 
 10.Novosad Dušan                       1570 
 11.Horbaľ Jozef                             1526 
 12.Belík Štefan                              1000 
 13.Holík Ján                                   1000 
 14.Horbaľová Ivana                       1000 
 15.Minarčák Pavol                          1000 
 16.Papp Ladislav                            1000 
   
   
   
   
   
 Výsledky dnešného 3.kola: 
 číslo stola Biely Čierny 
 1. Orviský Vladimír                - Karnek Ján                      0:1 
 2. Škultéty Stanislav               - Murín Miroslav              0:1 
 3. Gajdoš Ján                          - Kováč Imrich             0,5:0,5 
 4. Dendis Emil                          - Holík Ján                  0,5:0,5 
 5. Horbaľová Ivana                 - Felong Peter                0:1 
 6. Minarčák Pavol                  -  Horbaľ Jozef               0,5:0,5 
 7. Papp Ladislav                     - Hovančík Marcel           1:0 
 8. Novosad Dušan                  - Belik Štefan                 1:0 
   
   
   
 Turnaj sa bude hrať systémom: jedno kolo každý deň do piatku 7. mája. 
 Hrací systém turnaja: Švajčiarsky, 7 kôl., počet hráčov: 16 
 Rozhodcami turnaja sú: Bartolomej Marci, Benjamin Beluš 
 Riaditeľ turnaja: Imrich Kováč 
 Tempo partie: 2 hod na 40 ťahov plus 1 hodina dohrávka 
   
 Predchádzajúce dni: 
   
 Výsledky 1. kola: 
 číslo stola Biely Čierny 
 1. Orvisky Vladimír      - Hovančík Marcel      1:0 
 2. Novosad Dušan        -         Murín Miroslav         0:1 
 3. Škultéty Stanislav     -         Horbaľ Jozef           1:0 
 4. Belík Štefan               -         Karnek Ján             0:1 
 5. Gajdoš Ján                -         Holík Ján                 1:0 
 6. Horbaľová Ivana       -         Kováč Imrich            0:1 
 7. Dendis Emil               -         Minarčák Pavol         0:1 
 8. Papp Ladislav           -         Felong Peter            0:1 
   
   
   
 Výsledky 2.kola: 
 číslo stola Biely Čierny 
 1. Kováč Imrich           - Orviský Vladimír            0:1 
 2. Murín Miroslav       - Gajdoš Ján                  0,5:0,5 
 3. Felong Peter           - Škultéty Stanislav          0:1 
 4. Karnek Ján              - Minarčák Pavol             1:0 
 5. Belik Štefan            - Dendis Emil                   0:1 
 6. Hovančík Marcel    - Horbaľová Ivana            0:1 
 7. Holík Ján                 - Novosad Dušan              1:0 
 8. Horbaľ Jozef            - Papp Ladislav             0,5:0,5 
   
   
   
   
   
   
 Tabuľka po 3. Kole: 
 1.      Karnek Ján                  3 body 
 2.      Murín Miroslav          2,5 boda 
 3.      Škultéty Stanislav       2 body 
 4.      Orviský Vladimír        2 body 
 5.      Gajdoš Ján                  2 body 
 6.      Felong Peter               2 body 
 7.      Minarčák Pavol          1,5 boda 
 8.      Kováč Imrich              1,5 boda 
 9.      Holík Ján                     1,5 boda 
 10.  Dendis Emil                1,5 boda 
 11.  Papp Ladislav             1,5 boda 
 12.  Horbaľová Ivana         1 bod 
 13.  Horbaľ Jozef               1 bod 
 14.  Novosad Dušan          1 bod 
 15.  Belik Štefan                0 bodov 
 16.  Hovančík Marcel        0 bodov 
   
 Turnaj pokračuje zajtra 4. Kolom. 
   
 Autor: Mgr. Pavol Minarčák 
   
   

