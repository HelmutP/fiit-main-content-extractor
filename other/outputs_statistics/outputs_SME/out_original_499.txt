
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alexander JÁRAY
                                        &gt;
                Súkromné
                     
                 ****Nebojte sa zubára. 

        
            
                                    7.3.2008
            o
            14:42
                        (upravené
                24.1.2012
                o
                17:49)
                        |
            Karma článku:
                6.37
            |
            Prečítané 
            2441-krát
                    
         
     
         
             

                 
                       Na Popolcovú stredu som konečne sa odvážil odísť k zubej lekárke, ktorú my doporučila moja žena, lebo chcem mať chrup v poriadku na promócii mojej dcéry. Ja sa priznám, že od zubára sa bojím oveľa viacej ako od smťky, lebo som presvedčený, že smťka nemučí čolveka tak bolestne, ako zubný lekár. Posledé ťahanie zubu prežil som ako vojak v Prešove roku 1963. Vtedy prvú a nie malú bolesť som pocítil po tom, čo mi vojenský lekár vpichol do ďasna hrubú injekciu. Ale pri vyťahovaní zalomeného koreňa, som videl všetky hviezdy na nebi v sprievode s bolesťou, ktorú cíti maliar, keď si rebríkom pricvikne svoje varlatá (gule).
                 

                 
Žiadný strach pred zubármi !!! 
          Na Popolcovú stredu konečne som sa odvážil odísť k súkromnej zubej lekárke, ktorú mi odporučila moja žena, lebo chcem mať chrup v poriadku na promócii mojej dcéry.     
 Ja sa priznám, že od zubára sa bojím oveľa viacej ako od smrtky, lebo som presvedčený, že smrtka nemučí človeka tak bolestne, ako zubný lekár.   Posledné ťahanie zubu prežil som ako vojak v Prešove roku 1963. Vtedy prvú a nie malú bolesť som pocítil po tom, čo mi vojenský lekár vpichol do ďasna hrubú injekciu.    No potom, pri vyťahovaní zalomeného koreňa, už som videl všetky hviezdy na nebi (aj tie ešte neobjavené) v sprievode s bolesťou, ktorú cíti bytový maliar, keď si rebríkom pricvikne svoje varlatá (gule).           
 Vojenský lekár vidiac, že ja sa smrteľne trápim, odhodlal sa mi zmraziť okolie zubu nejakým prípravkom a na viac, postriekal rozjatrenú ranu obsahom injekčnej striekačky.    Po polhodine vtláčania dláta medzi ďasno a koreň zuba, koreň zalomeného zuba bol konečne po kusoch vytiahnutý, ďasno dolámané a ja spotený a zronený.    Ešte tri dni som musel brať prášky, aby som necítil tú neprekonateľnú bolesť aj v noci, pričom  celý nasledovný týždeň som sa nemohol normálne najesť.    Táto neblahá skúsenosť s vojenským  zubárom ma viedla k tomu, že ja som sa od zubárov chránil ako čert od (95%) svätenej  vody.     
 Po mojom tohoročnom osudovom rozhodnutí, vstúpil som do ordinácie súkromnej zubárky na Mäsiarskej ulici v Košiciach. (Bývalá budova starej polikliniky.)         
 Ona mi kázala sadnúť si a následne urobila optickú - vizuálnu prehliadku môjho chrupu. Za tých päť minút prehliadky som zaplatil 570 Sk a dostal som odporučenie k vytrhnutiu môjho zuba na dentálnu chirurgiu do starej nemocnici na Rastislavovej ulici, samozrejme v Košiciach.    Druhý deň som sa tam so strachom dostavil a bol som pozvaný jedným mladým lekárom do ordinácie s príkazom, aby som si sadol do zubárovej stoličky. Aby som situáciu opísal plastickejšie, tak išlo o miestnosť so šiestimi zbárskými stoličkami, šiestimi zubármi a šiestimi sestričkami.   Urobil som čo som musel a povedal som si: „Poručené pánu Bohu“, otvoril som ústa a zavrel oči.         
 Pán doktor mi povedal, nech sa dívam na neho a tak som sa díval na neho. V tom on vzal do ruky injekciu na konci ktorej bola tenká ihla a bezbolestne mi ju vpichol do ďasna, najprv z vonkajšej strany, potom chvíľu počkal a vpichol ju do vnútornej časti ďasna, už bezbolestne. Ja som čakal, že mi povie, choďte na chodbu a buďte tam potiaľ, kým nezaberie injekcia.          
 Nestalo sa. Po malej chvíli pristúpil ku mne lekár a kázal mi otvoriť ústa. Do ruky vzal akýsi nástroj a ja som automaticky zavrel oči a chlapsky čakal čo bude nasledovať. Aká neznesiteľná bolesť bude nasledovať.          
 No behom 5. sekúnd už bolo po všetkom a ja som pritom necítil žiadnu bolesť. Pán doktor mi povedal, vypláchnite si ústa a dajte si tu gázu do úst, držte ju tam 20 minút. Tým bola operácia vytiahnutia môjho zuba ukončená.   Po skončení účinku injekcie, neobjavila sa žiadna bolesť.   Ja z radosti nad bezbolestným vytiahnutím zuba,  gratuloval som lekárovi za jeho perfektný výkon, pričom on sa na mňa nechápavo díval, či mi náhodou ničo v hlave nepreskočilo. A čo je pre mňa nepochopiteľné, on nepýtal žiadne peniaze za ten zákrok.    Pravdepodobne budem musieť platiť obvodnej zubnej lekárke, ktorá ma tu poslala. Pán lekár sa volá Dr. Peter. Pojdiš.   Takže odo dneška, nielenže nebojím sa od zubárov, ale teším sa na stretnutie s nimi v ich ordinácii, tak ako sa tešia aj tí mladí ľudia po tom, čo im bezbolestne vytiahli lekári zub         
 Tu musím konštatovať, že zubárska veda za posledných 40 rokov urobila míľový krok v pred. Preto každému občanovi Slovenskej republiky vrelo  odporúčam:   Nebojte sa (košických) zubných lekárov, sú už bezbolestní.   Malá poznámka.   Pred 40 rokmi injekcia bola domácej výroby a bola zadarmo, dnes je injekcia švajčiarskej výroby a je potrebné za ňu zaplatiť.    Pred 40 rokmi injekcia umŕtvila polovicu tváre, no neumŕtvovala bolesť okolo zuba. Dnes umŕtvuje bolesť okolo zuba dokonale a behom troch minút.    Pred 40 rokmi vyťahovanie zubných koreňov sprevádzala vždy veľká bolesť, dnes už je to bezbolestné. Dnes sa musíte opýtať lekára, pán doktor už je zub vonku?   To píšem len pre tých, ktorí by si želali návrat do doby spred 40. rokov a pritom nemajú zuby v poriadku .   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Overte si, či ste múdrym človekom s IQ 201 bodov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Ženský genetický kód!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Experimentálny dôkaz neplatnosti „Všeobecnej teórie relativity“ pohybu matérie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Veritas vincit (Pravda víťazí.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alexander JÁRAY 
                                        
                                            Čas sem, čas tam, nám je to všetko jedno.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alexander JÁRAY
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alexander JÁRAY
            
         
        jaray.blog.sme.sk (rss)
         
                                     
     
        Pravde žijem, krivdu bijem, verne národ svoj milujem. To jediná moja vina a okrem tej žiadna iná.
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    255
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1326
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kvantová matematika
                        
                     
                                     
                        
                            O zločinoch vedcov
                        
                     
                                     
                        
                            Kde neplatia zákony fyziky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Overte si, či ste múdrym človekom s IQ 201 bodov.
                     
                                                         
                       Ženský genetický kód!
                     
                                                         
                       Veritas vincit (Pravda víťazí.)
                     
                                                         
                       Čas sem, čas tam, nám je to všetko jedno.
                     
                                                         
                       Kvíkanie divých svíň.
                     
                                                         
                       Ako nemohol vzniknúť priestor a hmota.
                     
                                                         
                       Odpoveď na otázku z Facebooku.
                     
                                                         
                       Moja dnešná elktornická pošta (26. 2. 2014).
                     
                                                         
                       Kde bolo tam bolo, bola raz jedna „Nežná revolúcia“.
                     
                                                         
                       Zákon zachovania hmoty.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




