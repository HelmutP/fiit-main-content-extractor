
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Adamovičová
                                        &gt;
                Nezaradené
                     
                 Vedieť počúvať a vnímať. 

        
            
                                    6.6.2010
            o
            22:12
                        |
            Karma článku:
                3.15
            |
            Prečítané 
            453-krát
                    
         
     
         
             

                 
                    Je zaujímavé,  že ľudia, ktorých v živote stretávame, majú pre nás posolstvo, informáciu a každý človek, nejakým spôsobom obohatí náš život ak sme vnímavejší.
                 

                       Stačí si viac všímať a hlavne druhým načúvať.       Pravú tvár človeka zistíme vtedy, ak sa musí obetovať pre niečo, niekoho, alebo ak sa jedna zo strán  nachádza v nepríjemnej situácii.           Dobre nám je ak sme zdraví, máme financie a po ruke spriaznené duše blízkych a priateliek / priateľov.       No tých pravých spoznáme v čase, keď nie sme na tom najlepšie.       Mám zopár priateliek, pri ktorých viem, že stačí zdvihnúť telefón a sú k dispozícii alebo ja im. Hm, akurát keď pozerám na slovo priateliek – napadlo ma slovné spojenie priateľ = liek.            Tiež rada pomôžem, ak vidím, že človek potrebuje šancu a podstrčím ho mierne dopredu.           Rada stretávam ľudí známych aj neznámych a nestačím sa diviť, ako rôznorodo pristupujú ku svojím starostiam a pohromám.        Moja kamarátka je veľmi veselá žienka.        Toľko smiechu vie rozdať, ale bojovne mi povie, že musí isť na nepríjemnú operáciu, lebo má zlé výsledky.        Je mladá, chodí pravidelne na kontroly k lekárovi a tak má šancu, že sa z toho úspešne dostane a vie, že v tom nezostane sama.       Ľudia v mojom okolí majú radosti aj starosti, v práci majú zodpovednosť, cítia tlak a mňa už neprekvapuje, keď sa len tak zastavia, posťažujú si a odchádzajú s úsmevom.       ,,Pri tebe sa človek nabije energiou“ vravia mi a mnohí sa pýtajú, kde ju čerpám.            Nerozmýšľala som nad tým, ale asi sa na vec pozerám z viacero uhľov pohľadov...            Som rada, ak vidím v mojom okolí človeka, ktorý má možnosť niečo dosiahnuť a tu šancu využije.        Je mnoho ľudí, ktorých život položil na kolená, nevládali vstať boli na niečom, niekom závislí....        Dostali sa z toho len preto, že im zo začiatku niekto, hlavne odborne pomohol a oni vtedy zacítili to svetielko, ktoré naštartovalo zasa ten ich pud sebazáchovy.       Patrí im môj obdiv, ak dokážu odolávať všetkým pokušeniam, ktoré by ich  vtiahli späť, na dno ...   Je to pre nich tá najťažšia,  zároveň najdôležitejšia vec na svete...       Niet nad zlého závistlivého človeka. Taký človek sa určite necíti dobre je nešťastný a toto je jeho prejav.       Je veľmi ťažké uvedomiť si také správanie a zmeniť sa...            Ak taká osoba bude mať šťastie, natrafí na dobrého človeka, otvorí svoje srdce a  zamyslí sa nad sebou, ma šancu obrátiť svoj život k lepšiemu...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Adamovičová 
                                        
                                            Diaľnica I 60
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Adamovičová 
                                        
                                            Masaker na pohotovosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Adamovičová 
                                        
                                            Konkurencia v obchodoch.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Adamovičová 
                                        
                                            Povodňová hrôza a jej dielo skazy.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Adamovičová 
                                        
                                            Ako to je v bežných obchodoch.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Adamovičová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Adamovičová
            
         
        adamovicova.blog.sme.sk (rss)
         
                                     
     
         Mám rada život a radosti z neho vyplývajúce.... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1021
                
            
        
     

    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




