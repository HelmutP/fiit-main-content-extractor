

 V období totálne neschopnej trojkoalície počas 4 rokov nevyšiel čas na nič rozumné. Tlačovky súdruha priemera, na ktorých by mal bilancovať a ponúkať čosi konkrétne do ďalšej "štvorročnice", ukazujú v plnej nahote to, čím oslovuje svojho voliča - neschopnosť. To obdobie jeho vládnutia sa dá rozdeliť na dvoje a charakterizovať veľmi stručne a výstižne takto: 
 Prvých 3,5 roka - Za všetko môže predošlá vláda. A ostatný polrok - Sú to ale bezcharakterní ľudia v tej opozícii. 
 Ešte snáď zvýšil čas na odvolávanie ministrov, ktorých bolo požehnane. To, aby sa obohatilo, platmi, odstupným i rabovaním, čo najviac "slušných" straníkov. Každý odstupujúci minister vlastne vykonal dobrý skutok, keď v duchu sociálneho cítenia, doprial aj inému... A potom ešte čo sa udialo? Aha! Kauza, kauza, kauza,... Táto vláda bola jedna veľká neuveriteľná kauza a ja by som si prial, aby bola len akýmsi kresleným vtipom zverejneným na nejakej nepodstatnej nástenke. Žiaľ, je to krutokrutá realita, ktorú žijeme. Niet čo k tomu dodať. Alebo predsa - ten komunista bohorovne sadne pred novinárov a nerieši ich otázky, iba ich ponižuje a hlavný bod tlačovky, ktorému sa venuje, je slogan opozičnej strany! Je toto normálne? Toto je tá vec, ktorá dnes trápi túto krajinu? Čudujem sa novinárom, že na tie jeho choré predstavenia ešte chodia... 
 Popustil som uzdu svojmu (hrubému) črevu (snažil som sa vymyslieť trefný slogan pre súdruhovu stranu, aby tromfol tie opozičné) a, s prepáčením, vyšlo zo mňa toto: 
 TÍ, ČO NA VÁS NES...RÚ, SÚ KOMUNISTI ZO SMERU! 
 Môžte to pokojne použiť. Veď slogan nemusí byť pravdivý, stačí, keď zaujme. Čo vy na to, súdruh priemer? Tromfnite opozíciu opäť! 
   

