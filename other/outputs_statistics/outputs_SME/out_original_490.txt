
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s medveďom - Kysnuté knedle 

        
            
                                    9.3.2008
            o
            12:00
                        |
            Karma článku:
                10.38
            |
            Prečítané 
            59919-krát
                    
         
     
         
             

                 
                    Milí moji.  Týmto  našim nedeľným príspevkom plníme sľub daný návštevníkovi našich stránok a kolegovi blogerovi Igorovi. Zároveň budeme radi, keď potešíme aj ostatných priaznivcov nášho kuchárskeho blogu, ktorí žijú dlhodobo v zahraničí a so zháňaním knedle majú veľké problémy a samozrejme aj vás ostatných, ktorí si chcete overiť svoj kuchársky talent či prekvapiť rodinu.
                 

                V dnešnej dobe je situácia v obchodoch s dostupnosťou kvalitnej kysnutej knedle úplne odlišná od dôb minulých. Preto drvivá väčšina kuchárov knedle z kysnutého cesta kupuje. Je to samozrejme o uľahčení práce a ušetrení času, ale čo takto vrátiť sa do doby našich materí a babičiek a vyskúšať si urobiť klasickú parenú knedľu.   Potrebujeme:     0,5 kg polohrubej (hrubej) múky, cca 3dcl mlieka (nie nízkotučné!), 1 vajíčko, 1 polievkovú lyžicu kryštálového cukru, 0,5 kocky droždia (25g), soľ a hladkú múku na opracovávanie cesta.          Tak a hurá na vec:     Najprv si pripravíme kvások. Mlieko zohrejeme do tepla (nie horúca) a rozmiešame v ňom polievkovú lyžicu kryštálového cukru, kopcovitú kávovú lyžičku hladkej múky a rozdrobené kvasnice. Počkáme, kým nám kvasnice nezačnú pracovať a kvások nenaskočí.     Ja neodporúčam vmiešať kvasnice či už klasické, alebo sušené priamo do cesta. Nech už je ich kvalita akákoľvek vyhlásená. Je to z jednoduchého dôvodu. Na kvásku môžeme zistiť, či sú kvasnice v poriadku, zatiaľ čo priamym vložením do cesta sa o túto možnosť pripravíme. Ak nebude niečo v poriadku, budeme nútení vyhodiť celé cesto a nie iba zlý kvások.     Kvások, ktorý do 10 minút nezačne peniť a jemne šumieť vyhodíme a ideme si kúpiť nové droždie. Kvások, ktorý síce peniť začne, ale popri bežnej aróme droždia bude mať aj kyslastú arómu BEZ MILOSTI VYHODÍME a ideme kúpiť nové droždie. Kyslastý zápach starého droždia by nám prenikol do celého cesta a už by sme sa ho nezbavili.          Do polohrubej múky vlejeme kvások, pridáme jedno vajíčko, kopcovitú kávovú lyžičku soli a cesto dobre vymiesime.           Vymiesené cesto musí byť kompaktné a dosť tuhé, aby sa nám v pare nerozdrobilo. Preto k 3 dcl mlieka podľa možnosti nepridáme žiadnu ďalšiu tekutinu. Samozrejme praxou zistíme, či si môžeme dovoliť experimenty, aby bola knedľa ešte nadýchanejšia ako páper. Tá moja bola nadýchaná, ale taká hutnejšia, zároveň s domácim 'šmakom'.     Vymiesené cesto posypeme zo všetkých strán hladkou múkou, (aby sa pri kysnutí nelepilo na steny nádoby), prikryjeme a necháme na teplom mieste aspoň 3/4 az hodinu kysnúť. Počas kysnutia môžeme občas cesto premiesiť.            No a kým cesto vykysne, pripravíme si pravý domáci nefalšovaný parák na knedle. Nie že by sa paráky nedali kúpiť v obchode, ale načo, prosím vás... Rovnako dobre poslúži veľký hrniec s plienkou.     Vezmeme veľký hrniec, nalejeme do neho tak do polovice vodu, ďalej vezmeme gumu z trenírok (ja som vzal zo starých slipov.:)), plienku či iný kúsok handričky (závesy, plesové šaty či obrusy necháme na mieste, dobre...:)) a prekryjeme ňou hrniec a dobre napevno gumou priviažeme. Už len stačí prekryť druhým trochu menším hrncom a parák je na svete.            Cesto nám krásne vykyslo.           Rozdelíme ho na minimálne dve časti a vyformujeme z nich požadovaný tvar knedlí. Ak sa chceme vyhnúť futuristickému tvaru hotových knedlí (všimnite si moje hotové knedle, najmä tú vľavo:)) každý kús cesta ešte pred vyformovaním dobre a trpezlivo vymiesime na pomúčenej doske, aby sa nám pekne rovnomerne spojilo. Ale fakt je, že kvalita cesta je rozhodujúca a aj krivá knedľa sa dá krásne nakrájať, takže nikto nič nezbadá. Aspoň vidíte, že aj Medvede majú svoje dni.:)          Keď nám v našom paráku zovrie voda, položíme do stredu plienky jednu z knedieľ,          prikryjeme parák druhým hrncom a necháme na miernom plameni knedľu pariť cca 20 minúť. Potom to isté zopakujeme pri druhej knedli. Počas parenia každá knedľa vyplní praktický celý otvor hrnca, takže ak by ste chceli robiť naraz obe knedle, zrejme by ste stvorili knedľové monštrum. Škoda, že som zabudol odfotiť hotovú knedľu na paráku.     Tak a tu je výsledok. Parenú knedľu pred podávaním nakrájame na nie príliš hrubé plátky a zohrejeme nad parou. V čase mikrovlniek je dobré nakrájané knedle naložiť na jeden tanier postriekať vodou a minútku zohrievať v mikrovlnke. Nie je to ako nad parou, ale dá sa aj tak.          Tak milí moji, dnes ešte nekončíme. Tak ako sa robia parené kysnuté knedle, robia sa aj kysnuté knedle varené vo vode. Toto je jedna z nich. (Vladimír Tomčík: Knedle na tisíc spôsobov. Vydavateľstvo Perfekt.)     Potrebujeme:     0,5 kg hrubej múky, 2,5 dcl mlieka, 2 včerajšie pečivá, 100 g masla, 2/3 kocky droždia (30g) 1 lyžicu kryštálového cukru, soľ a hladkú múku na opracovanie cesta.          Z teplého mlieka, kryštálového cukru, lyžičky hladkej múky a kvasníc so pripravíme kvások, ktorý necháme 10 minút postáť.     Vlejeme ho do múky, pridáme 50 g masla, pol kávovej lyžičky soli.          Dobre vymiesime hutné cesto, posypeme ho z každej strany hladkou múkou a necháme 1 hodinu kysnúť.            V tom čase si nakrájame pečivá na kocky a na druhej časti masla ich usmažíme do zlatova.          Cesto nám vykyslo. Vzhľadom k hustote nečakajme veľký nárast objemu, ale malo by byť vláčne a skutočne prekysnuté.          Do neho následne vmiesime          usmažené kocky pečiva.            Cesto môžeme opäť rozdeliť na dve časti a vytvarovať podlhovasté knedle.          Každú knedľu položíme na čistú utierku. Posypeme hladkou múkou a obe utierky zavinieme. Následne ich z každej strany podviažeme tak, aby ostal priestor na nárast knedle varom. (2-3 cm od kraja knedle z každej strany)     Potom obe zabalené knedle vložíme do posolenej vriacej vody, necháme zovrieť, stiahneme plameň na úplné minimum tak, aby voda len pomaly prebublávala, zakryjeme pokrievkou a veľmi pomaly varíme 1 a štvrť hodiny. V polovici varenia môžeme knedle obrátiť.     Po uvarení necháme zabalené knedle ešte aspoň 15 minút chladnúť a potom ich rozbalíme.          Nižšie vidíme výsledok. Vzniknú tuhšie, ale veľmi chutné knedle, ktoré budú mať na priereze efektnú štruktúru (prstenec vyplnený trochu sypkejším cestom) s univerzálnym využitím. Odporúčam najmä k pečeným mäsám, ale aj k niektorým druhom omáčok (kôprová, chrenová či sviečková...)          Tak a nižšie vidíme obe knedle nakrájané.            Parená knedľa našla využitie hneď. Veď tak dávno sme nemali taký dobrý echt domáci Segedín.           No a druhá knedľa si zabalená v mrazničke na svoje použitie ešte počká. Ostatne podobným spôsobom je možné skladovať aj knedľu parenú a potom ju zmrazenú vybrať, rozmraziť v mikrovlnke a opäť nakrájanú napariť či zohriať. Bude ako čerstvá.     Dobrú chuť.           tenjeho        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (88)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




