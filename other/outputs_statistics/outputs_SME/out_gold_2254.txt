

  
 Nedávno otriasla nielen hokejovou verejnosťou správa, že útočník Liptovského Mikuláša v tomto klube sezónu nedohrá. Stiahol si ho k sebe jeden z najťažších súperov, ktorí môžu športovca vyzvať na súboj - choroba.  
 Už počas sezóny si nejeden divák mohol povšimnúť, že niečo nie je v poriadku. Bodovo to tak hrozne nevyzeralo - v 31 zápasoch k 9 gólom pridal 10 asistencií - fanúšikovia však vedia, že Haluška vie "drviť" oveľa lepším tempom, a najmä obraz na ľade býval úplne odlišný od toho, akým sa prezentoval v závere minulého roka. Vyhovárať sa na vek? V jeho prípade akoby nemožné, Juraj Halaj už dobrých desať rokov "nestarne" - ba naopak, čím ďalej, tým je pre mužstvo platnejším. Až teraz je všetkým jasné, kde bol pes zakopaný... Hokejistovi diagnostikovali leukémiu.  
   
  
 V Poprade sme ho mali štyri roky, a nech bol počas nich pohľad fanúšikov na centra akýkoľvek, keď odišiel späť do Liptovského Mikuláša, nebolo kamzíka, ktorému by "v stáde" nechýbal.  
 V jeho poslednej sezóne u nás bol najstarším hráčom, no stále patril v produktivite do top desiatky (vo svojej prvej sezóne s kamzíkom na hrudi tabuľku popradskej produktivity s prehľadom vyhral, v tej poslednej bol spolu s Jarolínom na druhom mieste s dvojbodovou stratou na kapitána Krotáka, pričom však odohral menej zápasov). 
 Pán hráč. Známy skvelými prechodmi do pásma, mal výborný ťah na bránu, famózne presné prihrávky. Jeden z najlepších krokov u nás bolo hodiť k nemu mladých hráčov, spomeniem napríklad Jasovského, ktorého talent vynikol práve v spolupráci s Halajom. Jeho prehľad v hre mu môžu mnohí kolegovia závidieť. Na ľade vie nechať srdce, je to obrovský bojovník, svoje skúsenosti vie nielen pretaviť do hry, ale ich aj posunúť ďalej. Jeho najväčším mínusom je, že nemôže hrať večne... 
   
 "Ďuriii, poprav šebe prilbuuuu!!!" Hláška z tribún, ktorá sa preň na isté obdobie stala typickou. "Devätnástka" vždy hrávala so vztýčenou hlavou. Verme, že tak odohrá aj tento boj! 

