
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Kučerová
                                        &gt;
                Nezaradené
                     
                 Životný brainstorming 4 - Prečo "Životný brainstorming"? 

        
            
                                    14.6.2010
            o
            12:23
                        (upravené
                27.6.2010
                o
                11:04)
                        |
            Karma článku:
                2.63
            |
            Prečítané 
            1079-krát
                    
         
     
         
             

                 
                    Pristavím sa pri tom, lebo už je to celkom ďaleko od začiatku môjho písania, prečo sú niektoré z mojich príspevkov pod spoločným názvom Životný brainstorming. Aj keď som si to pri prvom až tak neuvedomovala, teraz vidím vďaka diskusiám k nim celkom jasne, prečo som mala vnútornú potrebu začať práve tak.
                 

                 Brainstorming je kľuč k otvoreniu brány k (nie len) mojim postrehom. Ak ho zahodíte - ste k ním kritickí príliš skoro - neprešli ste jeho prvou fázou, a preto osamotená druhá zostáva len štandardnou krokovou metódou. Pri všetkej úcte k nej, bránu neotvorí.   Preto „Brainstorming".      Nie je to tak, i keď som sa tak možno niekedy vyjadrila, že by som si myslela, že píšem len pre nejakú tajomnú „elitu". Nie. Píšem pre kohokoľvek, kto má chuť, a vrelo vás nabádam (aj za cenu toho, že idem s „kožou na trh") k tej chuti chytiť ten kľúč do ruky - zabudnúť v úvahách na všeobecne zaužívané riešenie a hľadať iné, netradičné, odvážnejšie. Len to skúsiť, ako živú hru, nechať tú búrku prejsť i svojou vlastnou hlavou. Alebo inak: obuť si tie sedemmíľové čižmy na svoje vlastné nohy a byť trpezlivý v ich nosení, kým sa im neprispôsobia. Keď priveľmi tlačia, proste ich vyzuť a neskôr skúsiť znova. Raz sa určite poddajú, veď sú rozprávkové:-). I moje ma riadne potrápili a opätovne trápia, keď niečomu nerozumiem, keď mi niečo nezapadá do mozaiky života, a ešte vždy ich vyzúvam, keď práve neviem, ktorým smerom sa v nich pustiť. No keď naberiem silu, znova ich obúvam, lebo v tom vidím zmysel, už ich poznám, už im verím.   Hovorí sa: „Lepšie raz vidieť, ako tisíckrát počuť.", ja hovorím: „Lepšie raz zažiť, skúsiť na vlastnej koži, vlastným telom, vlastnou hlavou, vlastnou úvahou, vlastnou emóciou, ako tisíckrát čítať."   A (aj) preto „Životný"...      Brainstorming generuje riešenia, ktoré môžu i nemusia byť správne, ak ich však zamietnete príliš skoro, nielenže sa tak možno pripravíte o podstatne lepšie riešenie, ale ste aj v tom smere zablokovali tvorivosť v sebe. Dôvod, prečo mi je metóda brainstormingu taká sympatická, je práve ten - tvorivosť podporuje.   Ak veríte, že spôsob zrodu detí nemá významný vplyv na spoločnosť, že nesúvisí s tým, aká je, môže to byť práve tak naivná viera, ako sa vám môže zdať tá moja - že ho má, že súvisí. Nechcem vám siahať na vašu vieru pre život (i keď sa môj brainstorming bezprostredne dotýka života každého z nás, a to je druhý dôvod, prečo „životný"). Tú si zmeníte sami, keď/ak to tak budete cítiť. Ja vás len pozývam zahrať sa, ako sme sa hrávali, keď sme boli deti, hoc s inou „hračkou", a inou technikou, ako to robia vedci s problémom, ktorý nevedia, ako sa rieši, s rovnakou zvedavosťou, nepopísanou nezaujatosťou, otvorenosťou a úprimnosťou: Čo by nastalo, keby sme všetci uverili opaku?  Keby sme zrod života a všetko s tým súvisiace brali aspoň tak vážne ako politiku (všimli ste si niekedy napr., aký podiel má na blogoch a ich čítanosti?)? Keby sme zrod života a všetko s tým súvisiace brali vážne ako niečo od čoho významne závisí kvalita nášho života a podľa toho sa k nemu začali správať - so záujmom, s plnou pozornosťou a sústredením, s pocitom práva a schopnosti tvoriť si vlastnú predstavu, vlastný názor (podobne ako pri politike), s ambíciou pochopiť, ako by to bolo najlepšie? Čo by nastalo? Skutočne veríte tomu, že nič? Veríte?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Kučerová 
                                        
                                            Aj smrť je súčasťou života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Kučerová 
                                        
                                            Životný brainstroming 7 - Bravčová masť namiesto skalpela
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Kučerová 
                                        
                                            Životný brainstorming 6 - Gravitácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Kučerová 
                                        
                                            Kde sme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Kučerová 
                                        
                                            Veteráni
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Kučerová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Kučerová
            
         
        kucerova.blog.sme.sk (rss)
         
                                     
     
        Žena, matka, manželka, hĺbavý človek zaujímajúci sa o skryté súvislosti života, sveta.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1222
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




