
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zora Pauliniová
                                        &gt;
                Pamiatky a interpretácia
                     
                 Búrajte pamiatky. Nič sa vám nestane. 

        
            
                                    12.12.2007
            o
            9:51
                        |
            Karma článku:
                10.02
            |
            Prečítané 
            6813-krát
                    
         
     
         
             

                 
                    Ešte nikdy sa mi nepísal článok tak ťažko. Tí, čo mali konať, nekonali, alebo konali neskoro. Tí, čo nemali konať, konali protiprávne.
                 

                   Práve búrajú Kablovku.     Konečne, povedia si zástancovia Skyscraper City, veľkej ilúzie o tom, že ak Bratislavu zastaviame výškovými budovami, narastie aj jej význam.     Bože, povedia si tí, ktorí rozumejú hodnotám tohto územia  na viacerých úrovniach - historickej, spoločenskej, urbanistickej i technickej.       Kablovka nie je  komplexom náhodne postavených budov. Je príkladom urbanistickej štruktúry, kde sa postupne vrstvili výrobné, riadiace  i dopravné funkcie, je odkazom na kus histórie, spojený s priemyslom, inováciami i životom robotníkov nášho mesta. Feiglerova kotolňa, jedna z troch budov, navrhnutých na zápis do zoznamu pamiatok je príkladom tradičnej industriálnej architektúry s vyváženou kompozíciou, remeselne zvládnutým detailom a zaujímavým tehlovým tvaroslovím - lizénami (náznakmi plochých stĺpov), rímsami či vežičkami. Už na prvý pohľad ponúka niečo klasické, hodnotné a pritom zanedbané...           Je, vlastne bola. Začiatkom júna 2007 získala firma HB Reavis búracie povolenie na 14 objektov (nie na všetky). V súčasnosti je areál Kablovky veľkým búraniskom okrem pár zostávajúcich objektov...            Vďaka iniciatíve  Fakulty architektúry spracoval v lete Pamiatkový úrad návrh na zápis do Ústredného zoznamu pamiatok  a prvýkrát ho poslal na Ministerstvo kultúry 12. 9. 2007; kvôli administratívnej chybe sa musel návrh opraviť a MK ho opäť prijalo 3. 10. 2007. Návrh  sa nesnažil zachrániť všetko - z  viacerých objektov boli navrhnuté na pamiatkovú ochranu už len tie tri, na ktoré ešte nemal investor búracie povolenie. Jednalo sa o objekty kotolne s komínom a lanovne.     Čo urobil investor     Investor porušil zákon - okrem stavieb, na ktoré mal asanačné rozhodnutie, bez povolenia zbúral stavbu - komín, ktorý bol navrhnutý na zápis do zoznamu kultúrnych pamiatok. Napriek tomu, že vedel, že patrí medzi objekty navrhnuté na zápis. Napriek tomu – čo je ešte dôležitejšie – že vedel, že platí stavebný zákon, ktorý hovorí, že na odstránenie stavby treba povolenie. Pokuta za porušenie stavebného zákona v tejto časti je zrejme dostatočne nízka (maximálne 2  milióny, dolná hranica 0), aby stálo za to riskovať protiprávne konanie. A nakoniec, kto vie či a kedy ju dostane...     Vyzerá to, že investor sa zakrýva. Strážna služba, ktorú kontaktovala privolaná hliadka mestskej polície tvrdila, že nemajú kontakt na vedúceho stavby – čo znie neuveriteľne v situácii, keď prebiehajú búracie práce. Aj statický posudok, ktorým investor zdôvodňoval zbúranie pamiatky, predložil podľa poslanca Ivana Bútoru až post festum – po zbúraní.     Investor ohrozuje aj ďalšiu pamiatku, pretože búranie stavby s pomocou mechanizmu s hydraulickými kliešťami vyvoláva také dynamické rázy, ktoré môžu poškodiť navrhované stavby, ktoré sú v tesnej blízkosti . Príliš to pripomína prípad historických Nitrianskych mlynov, z ktorých vďaka iniciatívnemu investorovi nezostalo nič.                 Čo urobilo ministerstvo     Ministerstvo malo po podaní návrhu začať správne konanie.  Keby ministerstvo správne konanie začalo, pamiatky by boli predbežne chránené. Zatiaľ neurobilo nič na reálnu záchranu navrhovaných pamiatok a vyzerá to, že viac ako v súlade so svojou misiou  konalo v súlade so záujmami investora.     Podľa informácií z viacerých strán zvolalo ministerstvo predbežné rozhovory, ktorých sa zúčastnili predstavitelia Starého Mesta a firmy HB Reavis. Po týchto rozhovoroch svoju aktivitu prerušilo a správne konanie do dnešného dňa nezačalo. „Žiadosti o zápis pamiatok nám chodia na poslednú chvíľu,“ povedal mi Pavol Ižvolt zo Sekcie kultúrneho dedičstva, ktorý návrh vybavuje: “Investor v prvom kole kontaktuje Krajský pamiatkový úrad, aby zistil, či stavby, o ktoré sa zaujíma, nie sú pamiatkovo chránené. Medzitým sa nám ozve niekto, kto chce pamiatku zachrániť, no my sme už viazaní krokmi, ktoré boli podniknuté (Pozn. autorky: Koho krokmi je MK viazané - investora?) . Návrhov je veľmi veľa a z našej strany je už neskoro.“      Nie je neskoro, kým pamiatka stojí; nie je neskoro ani teraz.     Staromestskí poslanci riešili na zasadnutí územnoplánovacej komisie otázku záchrany Kablovky i otázku protiprávnej asanácie. Poslankyňa Soňa Párnická privolala na miesto búrania hliadku mestskej polície, poslanec Ivan Bútora začal zisťovať situáciu na stavebnom úrade a na ďalších inštitúciách - až vtedy sa vlastne ukázalo, že ministerstvo nekonalo.        Čo treba spraviť                                                                                                                                                                                                                                                                                                                                                                                                    Ministerstvo by malo bezodkladne začať správne konanie vo veci vyhlásenia navrhnutých pamiatok. Aj keď nie sú ministerskí úradníci viazaní žiadnym termínom, od 3. októbra , kedy bol návrh podaný, uplynulo dosť času, aby sa tak stalo. Ministerstvo kultúry by malo predovšetkým napĺňať svoju misiu a nechať investorov, nech svoje záujmy obhajujú sami.     Mestské časti s mestom  Bratislava  a  v spolupráci s pamiatkarmi, Fakultou architektúry STU a občianskymi združeniami (ochranármi, umelcami...) by mali pripraviť spoločnú stratégiu záchrany industriálneho dedičstva Bratislavy. Tí, čo sa trochu orientujú v svete architektúry, vedia, že imidž mesta, jeho veľkosť a autenticita či genius loci nevznikne prebudovaním územia stavbami bez identity . Existujú stovky miest, ktoré ukazujú, že industriálne stavby sa dokážu stať plnohodnotnou a atraktívnou súčasťou  živého mesta s novými funkciami.      Mnohé industriálne stavby v Bratislave ešte stoja. Nie sú zatiaľ chránené, ale zároveň nie je na všetky z nich vydané búracie povolenie. Ešte stále nie je neskoro...     V Amerike sa niektoré mestá stávajú celé "industriálnymi rezerváciami".          V americkom meste Lowell bola jedna časť textilnej továrne prerobená na lofty - luxusné byty, druhá sa stala atraktívnym múzeom, kde sa deti z celej krajiny chodia učiť  o priemyselnej revolúcii, emigrácii a ľudských právach...          Aj textilná továreň v Lipsku sa stala miestom, kde si možno kúpiť krásny byt.          Foto: Zora Pauliniová, Marián Paulini       Posledná snímka je zo stránky Výzkumného centra průmyslového dědictví.     Klub ochrancov technických pamiatok - zaujímavá stránka, prinášajúca informácie o ochrane pamiatok.     Kontakt na Skyscraper City
            Doplnené 14. decembra: Odpoveď od ministerstva kultúry, v akom štádiu je správne konanie o vyhlásení Kablovky za pamiatku.               

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (84)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zora Pauliniová 
                                        
                                            Môj Milý Sajfa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zora Pauliniová 
                                        
                                            Ľudia 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zora Pauliniová 
                                        
                                            Vyžeňme ich z ulíc
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zora Pauliniová 
                                        
                                            Keď odchádza Ďurkovský
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zora Pauliniová 
                                        
                                            Holandsko - moje zázraky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zora Pauliniová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zora Pauliniová
            
         
        pauliniova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Pozerám sa na svet a nechávam sa inšpirovať. 



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    119
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5749
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Samospráva
                        
                     
                                     
                        
                            Verejné priestory
                        
                     
                                     
                        
                            Trendy v riadení a rozvoji
                        
                     
                                     
                        
                            Pamiatky a interpretácia
                        
                     
                                     
                        
                            Občianska participácia
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Reflexie
                        
                     
                                     
                        
                            Pekné chvíle
                        
                     
                                     
                        
                            Mobilita
                        
                     
                                     
                        
                            Architektúra
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tony
                                     
                                                                             
                                            Veronika Bahnová
                                     
                                                                             
                                            Peter Weisenbacher
                                     
                                                                             
                                            Pavol Pálfy
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




