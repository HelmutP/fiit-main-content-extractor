
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondro Urban
                                        &gt;
                Sociálne médiá
                     
                 Dve videá, dva rôzne prístupy 

        
            
                                    27.3.2010
            o
            18:28
                        (upravené
                11.4.2010
                o
                14:21)
                        |
            Karma článku:
                3.22
            |
            Prečítané 
            1134-krát
                    
         
     
         
             

                 
                    Jedna z veľmi dôležitých súčastí prezentácie prostredníctvom sociálnych médií je video. Akonáhle máte video zavesené na youtube.com (v prípade špecifického targetovania vimeo.com), tak ste v kolotoči a v hre. Riešenie videí prostredníctvom vlastných "custom-made" prehrávačov síce nie je úplne zlé, ale ste de facto mimo dosahu sociálnych sietí. V tomto blogposte sa povenujem dvom rôznym hudobným videám, ktoré majú úplne iný prístup k svetu sociálnych médií, ale oba sú správne.
                 

                 Prístup číslo 1   Geniálny amaterizmus. Prvé video je z produkcie víťazky nemeckej úrovne súťaže Eurovision Song Contest, čo je jeden z najtradičnejších formátov klasických médií. No napriek tomu v posledných dňoch si získava obľúbenosť medzi divákmi netradičných médií, teda youtube.com. Toto video je dnes 7. v poradí najsledovanejších hudobných videí (s celkovým počtom videní okolo 3,5 mil.).           Toto video je zlé, pretože:     otrasná kamera, pripadá mi to celé akoby niekomu dali do rúk profesionálnu kameru a jimmy jib, pričom ten človek nevedel čo s tým   otrasná choreografia, baba z ulice postavená pred kameru, tá baba sa dá zhrnúť ako "zkaderuka zkadenoha"   neostré zábery   aranžmá piesne nič moc   hrozná angličtina   otrasné osvetlenie, v niektorých záberoch sa pomaly speváčka nedá identifikovať   nalíčenie akoby vyšla z domu, totálny amaterizmus   ten náhrdelník na krku, bože môj   nezmyselné tetovanie na vnútornej strane ramena ruky     Toto video je úžasne úspešné a dobré, pretože:     otrasná kamera, pripadá mi to celé akoby niekomu dali do rúk profesionálnu kameru a jimmy jib, pričom ten človek nevedel čo s tým   otrasná choreografia, baba z ulice postavená pred kameru, tá baba sa dá zhrnúť ako "zkaderuka zkadenoha"   neostré zábery   aranžmá piesne nič moc   hrozná angličtina   otrasné osvetlenie, v niektorých záberoch sa pomaly speváčka nedá identifikovať   nalíčenie akoby vyšla z domu, totálny amaterizmus   ten náhrdelník na krku, bože môj   nezmyselné tetovanie na vnútornej strane ramena ruky     Prečo som si odporoval? Jednoducho tieto všetky faktory pomáhajú k tomu, že tej babe sa dá uveriť, že len pred minútou ste sa s ňou stretli v meste, kde ona bola s partiou (svojich adolescentných kamarátov). A teraz si jednoducho odbehla a zaspievala pesničku. A bolo jej úplne jedno kto ste, čo robíte, ona sa jednoducho zabavila so svojím špecifickým šarmom. To robí túto pesničku atraktívnou.   Prístup číslo 2   Inovatívny profesionalizmus. Takto sa dá zhrnúť najnovšie video Lady Gaga. Bez ohľadu na nejaké moje sympatie alebo antipatie voči hudbe tohto štýlu, jedno sa musí uznať: Lady Gaga je neuveriteľný marketingový nástroj. Jej je úplne jedno čo je na titulke bulváru, hlavne že tam je ona. Tento prístup už viacero ľudí skúšalo, ale nikto to nedotiahol do takéhoto extrému.           Toto video sa určite nedá nazvať nudou. Pesnička len dopĺňa jednu kontroverznú show, plnú odhalovania sa, nemravných gest. Nie je to klasický hudobný klip, rozumne zvolila prístup použitý už dávnejšie Michaelom Jacksonom pri klipe Thriller. Toto je krátky film.   Ako spomína medialne.sk, jej nové video je plné product placementu. Áno, logicky. Doba sa mení a namiesto boja s pirátstvom sa treba zamerať na rozumný predaj reklamného priestoru. Predáva sa pieseň a spolu s ňou aj "nealkoholický nápoj Diet Coke, chlieb Wonder Bread, šalátový dressing Miracle Whip, telekomunikačný operátor Virgin Mobile, fotoaparát Polaroid, on-line zoznamka PlentyofFish.com či limitovaná edícia laptopu HP Envy." (citované medialne.sk) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Problém taxíkov v zlom počasí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Výtlk, výtlčisko a také obyčajné výtlčíky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Onedlho tu už Fico nebude!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Hľadanie konečného riešenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondro Urban 
                                        
                                            Moja starostka - Táňa Rosová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondro Urban
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondro Urban
            
         
        ourban.blog.sme.sk (rss)
         
                                     
     
        http://twitter.com/ourban
http://facebook.com/ourban
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2255
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Web
                        
                     
                                     
                        
                            Sociálne médiá
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Donovaly: slovenský Aspen, kde Vás smetiari vrátia späť do reality
                     
                                                         
                       Fico vyrába ďalších 65 tisíc ľudí bez práce
                     
                                                         
                       Výpalníci z Úradu vlády sú horší ako smrť.
                     
                                                         
                       Na rómske komunity štátny rozpočet opäť raz zabudol
                     
                                                         
                       Prečo Slovensko nepatrí medzi šťastné krajiny?
                     
                                                         
                       Nie som ako ostatní učitelia...
                     
                                                         
                       Slovensko má depresiu. Fico ju chce liečiť marihuanou.
                     
                                                         
                       Voda bude drahšia a to nie je všetko...(časť prvá)
                     
                                                         
                       V akej krajine to žijeme?!
                     
                                                         
                       Minister šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




