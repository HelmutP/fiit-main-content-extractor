
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Kaščák
                                        &gt;
                Nezaradené
                     
                 Sanktuárium v Lagiewnikách - miesto premeny? 

        
            
                                    21.5.2010
            o
            20:20
                        (upravené
                21.5.2010
                o
                21:23)
                        |
            Karma článku:
                2.36
            |
            Prečítané 
            170-krát
                    
         
     
         
             

                 
                    Čo to má znamenať? - pýtam sa sám seba keď nastupujem do autobusu na spiatočnú cestu dlhú vyše 200 kilometrov, a vidím tých vyše 40 000 nadšených ľudí a pozorujem ich reakcie. ,,Neuveriteľné! Som taký šťastný, som taká šťastná, som napĺnená milosťami, radosťou, je to ťažko uveriteľný zážitok na ktorý budem do konca života pamätať, dotkol sa ma sám Boh.....
                 

                 
  
   Nech je to už čokoľvek, je to nevysvetliteľné. Je to niečo čoho je príčinou Božie milosrdestvo. To krásno ktoré tam ľudia denno - denne zažívajú možno vidieť, počuť, zažiť naozaj len tam. Len tam pri hlavnom oltári Baziliky Božieho milosrdenstva. Je fascinujúce aspoň pomyslieť čo to!, ako to!, prečo!... Fascinujúca mystika!   Sedím som v druhej lavici od Božieho oltára. A počúvam. Ticho. Zrazu začínam mať pocit akokeby mi už naozaj nič nechýbalo. Akokeby som bol najbohatší človek na svete, akokeby som mal ženu, 4 malé deti,akokeby som mal luxusný bavorák..., v tej chvíli som bol najšťastnejší človek na svete. Ten pocit... Ten pocit, priznávam, dnes už nieje taký aký som mal vtedy, no čo stojí za zmienku a čo je naozaj nevysvetliteľné, ako som videl, počul, nielen mne sa to stalo a nie len raz. Ale vždy keď to miesto navštívim. Čo to je? Sú to premenené modlitby na nekonečné milosti ktoré sa modlia v Sanktuáriu dennodenne možno tisíc, nespomínajúc prvé soboty v mesiaci, každoročné púte na sviatky a iné ďakovné púte? Sú to milostí prameniace z Božieho milosrdenstva? Sú to milostí od orodovnice, blahoslavenej sestry Faustíny od ktorej kanonizácie prebehlo 10 rokov? Sú to...? Sú to. Nevysvetliteľné krásne, požehnané skutočnosti ktoré dodávajú životu človeka nádej a vieru v Boha, svojho stvoriteľa, že tak ako nás On poslal na tento svet z lona matky, tak má pre nás pripravené miesto vo večnom kráľovstve bez konca. To miesto je asi miesto premeny: premeny problémov, bariér, ťažkostí, hriechov, starostí.... na šťastie, radosť, nádej, silu, odpustenie. Tí, ktorý ste tam už boli, istotne mi dáte za pravdu. A oplatí sa tam vracať rok čo rok, mesiac čo mesiac, a prosiť, predniesť, vykričať sa a takto premeniť na šťastie, radosť, pokoj. Božie milosrdenstvo, dôverujeme Ti! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Kaščák 
                                        
                                            Človeče, nezlob se! – Tatry – človek
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Kaščák 
                                        
                                            Život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Kaščák 
                                        
                                            Sestra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Kaščák 
                                        
                                            Rodičia, spamätajte sa!!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Kaščák
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Kaščák
            
         
        marekkascak.blog.sme.sk (rss)
         
                                     
     
        Som človek ktorý je pre svet len niekto, no pre niekoho je celý svet. Nie som žiaden anjelik, nič výnimočné, robím chyby, nie som perfektný, niekedy som blázon, ale som sám sebou. Naučil som sa zniesť bolesť, skryť smútok a smiať sa so slzami v očiach, len preto, aby som ukázal, že sa mám dobre..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    158
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Človeče, nezlob se! – Tatry – človek
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




