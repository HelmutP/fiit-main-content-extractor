
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Dano už vládu vymyslel. Je dobrá! 

        
            
                                    15.6.2010
            o
            7:07
                        |
            Karma článku:
                8.61
            |
            Prečítané 
            1906-krát
                    
         
     
         
             

                 
                    Mohlo by to byť takto, čo povieš, oslovil ma dnes ráno sused pri novinovom stánku, kde postával už najmenej desať minút s úmyslom celkom iste sa so mnou neminúť. Je to jeho nemenný rituál už naozaj niekoľko dlhých mesiacov, presne od chvíľky, keď sme sa dohodli na tom, ako rovnako sme prežívali svoje mládenecké roky. Je starší, ešte stihol vojnu odslúžiť u „pétepákov“ voľakde na Ostravsku, priamo v bani a smolu v živote mal akurát preto, že jeho otec sa nevedel zmieriť so stratou poľnosti, ktorú si mohol kúpiť po návrate z Ameriky voľakedy počas prvej republiky. To bol vlastne aj môj dedičný hriech, mne to isté „zariadil“ dedo a tých päť rokov vekového rozdielu ma predsa len v mnohom ušetrilo. Môj sused fajčí, ako Turek, žiadna osveta v jeho prípade neplatí, iba mávne rukou a dozviem sa, že ma má rád, ale s fajčením už neprestane! Aby si vedel, na to mám ten príplatok za tú „panskú vojnu“ a celý dôchodok dám babe ako vždy. Ešteže mi pridá drobné na pivo! A ja jej za to kupujem Nový čas, má tam krížovku, ale nenadávam, ja zasa holú babu!
                 

                     Ale aby som nezahováral, cigarety nechajme cigaretami, ja som to včera už všetko podelil a bolo by dobre, keby sa to voľajako rozkríklo! Rozdelil som to dobre, spravodlivo, ako ty hovoríš aj podľa odbornosti a danosti a hoci som mal s niektorými nováčikmi starosti a Lipšic sa mi vôbec nepáči, malo by to silu! Syn mi to schválil a teraz je na tebe, aby si to napísal do toho blogu! Čím skôr, aby sa to nezahovorilo, lebo ten Gašparovič ma strašne s...e! Veď uznaj musí to byť ešte väčší fajčiar ako ja, veď ledva chytá dych a fučí mu to kade tade ako v zlom komíne, no machruje! Čo to s tou vládou odkladá? Veď s tým pandrlákom hranatým sa nikto nechce baviť ani o futbale, tak čo zdržuje?   Predseda vlády je jasný, Radičová to zvládne a v tom balíku novín, čo mi od teba priniesol náš Petrík som našiel krásne článočky akurát o nej. Vraj čistá, vraj škriepna a vraj strašne tvrdohlavá – no povedz, nie je to pre peknú babu čo má všetko čo treba dobrá kvalifikácia? Ako tá moja, tá to diriguje presne takto a vidíš, fajčím a stále som tu, pijem pivo a ešte pôjdeme aj na dovolenku k Bete do Čiech, takže nemá chyby!   No a za šéfa parlamentu ponúkam až dvoch a ešte neviem, ako by to bolo lepšie. A ty by si koho vybral, Figeľa či Dzurindu?   Moja replika ho sklamala, Dzurindu nie je poslancom, ten sa do parlamentného kresla posadiť nemôže, ale starého Davidíka to nepomýlilo. No vidíš, bude to ľahšie, tak sa do tej vysokej stolice posadí Figeľ, je to fešák, bude tam pasovať ako, no onô a Dzurinda bude minister zahraničných vecí, alebo dopravy! Kvalifikáciu má, povedal si mi, že bol výpravca či čo a naviac je tu voľakde od nás, takže mašinky istotne ľúbi. No, keď už som spomenul to zahraničie, ešte je tu ten čo je v Bruseli, ten čo už tam ministroval a bolo by možno lepšie, keby tam bola taká ťažšia váha, Mikulášovi lepšie pristane tá doprava. A móóóóc  som rozmýšľal aj o tom ako to urobíme na financiách. Pasoval by tam Mikloš, ten tomu rozumie a je aj východniar, takže bomba, ale potom som si povedal, že ten už toho má celkom iste po krk. S tým Počiatkom sa iba hádal, furt mu kadečo vyhadzovali na oči, tak tam posadíme Sulíka! Pozdáva sa mi, je taký obyčajný, aj si vraj vypije aj fajčí a peniažkom rozumie, to povedal aj Mikloš a bude to tak určite dobré.   A teraz ešte tá spravodlivosť, mne by tam pasoval Lipšic, je to taký bitkár, vraj má ranu, lebo hovorili že má skoro 190  centimetrov a ani v parlamente sa nikoho nebál, no potom ma napadla tá Lucia Žitňanská. Kočka, elegantná a už to robila, takže už vie, ako s tým lazarom mafiánskym, čo sa ani o svoje deti postarať nedokáže, zatočí! Vraj na to bude treba špeciálny zákon, lebo sa na tom „najvyššom“ zakopal do zákopov ako za vojny, ale verím jej! A Lipšica  posadíme na vnútro, nech s policajtmi urobí čo treba, tí iba zobali tomu krásavcovi ako kury rovno z ruky, bude to pre nich dobrá zmena!   Simon je jasný, Maďari sú dobrí gazdovia, ten nech sa stará o role a popri ňom nech sa ako starý minister presadí aj Mikloš. Dal dokopy financie a teraz nech sa pousiluje na školstve, namiesto tých hejslovákov! Ty vždy hovoríš, že to boli aj za teba najhorší ministri, tak ich treba zahnať od válova. A zdravotníctvo sa mi obsadzovalo ťažko, nepoznám kádre a ani osobnosti, ako si ma naučil, takže hádať sa vie akurát tak Novotný, ale to už je tých modrých v tej vláde akosi veľa, nie? Nerátal som, ale tých poznám najlepšie!   Sociálne služby a rodina sú jasné, hovoril si mi, že ten Jožo Mihál je ti známy z tých blogov, tak nech sa predvedie. Tú Tomanovú, čo tam šafárila ako z plnej špajzy nech radšej vyhodia čo najskôr, lebo tá by ešte stíhala porozdávať korunky svojim kamarátkam a nech je rada, ak pôjde iba do penzie! Veď si istotne započul, ako zasa skončil ten jej sociálny podnik?!   Jasné, ešte je tu ten výrobný sektor, obrana , ale tam to už nebude národ tlačiť. Ba obranu by som celkom zrušil, nech je to špecializovaná zložka na vnútre a basta! Však vojaci už sú profíci, tí si vystačia, no nie? O tom sa vie, každý deň, to sa nás dotýka všetkých, veď to volajú silový odbor a boli by strašne nezodpovední, keby si vybrali zle! Nech sa snažia, čo povieš?   Ale toto tam napíš tak akosi vcelku, zajtra si to prečítam a kľudne tam cituj, že to povedal starý Dano Davidík, ten s kvalifikáciou pétepáka, baníka a nešťastného chlapa, čo nemohol študovať, ale našťastie sa dobre oženil!   A nezabudol som ani na Bélu, ten nech zastupuje Figeľa, veď to už robil a aj vtedy som mu odpustil to „vijéme“, akože vieme, lebo je to vraj echt Maďar. A z Hrušovského by bol dobrý podpredseda vlády pre tie menšiny, je právnik a určite by to robil lepšie ako ten dejepisár zo SMER-u, ktorý bol iba smiešny pre všetkých, čo o tom voľačo vedia. A bolo by fajn niečo vymyslieť aj pre toho zo SaS-u, toho moderátora Krajcera, ten má dobre prerezanú hubu a celkom rýchlo mu to myslí! No bacha dal by som pozor na tých štyroch obyčajných u Sulíka, to je aj na mňa silná káva a to nebol dobrý ťah. Radikálov táto vláda nepotrebuje, to naozaj nie. A neľúbia sa mi ani tí Slováci u Bugára, furt mám v oku ten nepodarený poslanecký sľub, čo predviedol voľakedy ten Zajac! Vraj sú to múdri ľudia, ale nič som o nich dvadsať rokov nepočul, tak načo majú byť v prvom rade! Nech si poslancujú, nech nás prezentujú voľajakými múdrosťami a to bude stačiť, no nie?   No, ako som to vydumal, dobré nie!? A Dano Davidík si zapálil novú cigaretku a vybrali sme sa po čerstvé pečivo. On s paličkou, s Novým časom, lebo SME mu popoludní pošlem aj s voľajakým časopisom a s dobrým pocitom, že svoj deň včera nepremrhal, naopak! Tie jeho rady verejne ponúkam a vôbec ma to netrápi, má to logiku, vidieť v tom zdravý fištrón a veľa praxe z horšieho i lepšieho žitia i bytia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




