
 1. Dav je nestály. Ak kríčí raz „hosana“, je možné, že o niekoľko málo dní bude hučať „ukrižuj“
2. Nikto nie je doma prorokom
3. Aj jeden z našich najbližších priateľov nás môže zradiť, za pár drobných
4. Aj jeden z najbližších priateľov nás môže zaprieť
5. V ťažších životných situáciách zostávame často opustení
6. Mama je osoba, ktorá nás nikdy nezradí, ktorá aj v tých najťažších chvíľach zostáva pri nás
7. Sfanatizovaný dav ľudí vydal Krista na kríž. Verejná mienka nie je synonymom pre mienku múdru a spravodlivú
8. Ľudia napriek tomu dávajú na názor verejnej mienky a rozhodujú sa podľa neho. Pričom následne si alibisticky umyjú ruky
9. Napriek tomu, že sú situácie, keď nás druhí urážajú, keď sa nám posmievajú, vydržme
10. Nesúďme iných. „Otče, odpusť im, pretože nevedia, čo činia.“
11. Kto je bez viny, nech hodí kameňom
12. Každý z nás si v živote nesie svoj kríž
13. V našom živote sme každú chvíľku bičovaní osudom
14. Napriek ťažobe kríža či bičovaniu osudom, musíme vždy vstať pod krížom a niesť ho ďalej
15. Pomôže, ak máme pri sebe niekoho, kto nám s krížom aspoň na časti našej Krížovej cesty môže a chce pomôcť
16. Každý z nás si však až na Golgotu svoj kríž musí vyniesť sám, inej cesty niet
17. Aj tá najťažšia chvíľa v našom živote však nikdy nie je beznádejná. Pretože nádej existuje. Po každom „ukrižovaní“ prichádza „zmŕtvyvchstanie“
18. Ak nezabudneme žiť lásku, bude sa nám na tomto svete žiť lepšie

Som veriaci človek. A práve viera mi každoročne pomáha intenzívnejšie prežívať tajomstvá Veľkej noci. Pomáha mi aj v tých ťažších časoch hľadať sily niesť svoj kríž, aj vtedy, keď sa mi zdá, že tých síl už veľa nezostáva. Vtedy si uvedomím jediné – že musím vydržať. Že v minulosti tu bol niekto, kto vydržal oveľa viac. A preto nemám prečo to vzdať práve ja. Veď náš život je práve o tej ceste. Len na nej spoznávame samých seba. Len na tej svojej ceste spoznávame zmysel a pravé hodnoty života...

 
