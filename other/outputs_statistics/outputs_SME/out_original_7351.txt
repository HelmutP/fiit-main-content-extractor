
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Bibko
                                        &gt;
                Ľudia a spoločnosť
                     
                 Jeho Svätosť  XIV. Dalajlama - Tändzin Gjamccho 

        
            
                                    26.5.2010
            o
            18:16
                        (upravené
                24.3.2013
                o
                16:41)
                        |
            Karma článku:
                5.22
            |
            Prečítané 
            1598-krát
                    
         
     
         
             

                 
                    Keď začne odriekať modlitby, ľudia zatajujú dych. Jeho prítomnosť je pre väčšinu neopakovateľnou udalosťou v každej krajine ktorú navštívil. Dalajlama je človekom osvieteným, stelesňujúcim múdrosť a húževnatým bojovníkom za mier na celom svete. Dovolím si tvrdiť, že Jeho Svätosť je jedným z najtolerantnejších ľudí, ktorí kedy ľudská civilizácia mala. Tento duchovný a donedávna aj politický vodca Tibetu disponuje obrovskou charizmou ktorá sa ešte väčšmi znásobuje faktom, že prakticky nemá žiadnu politickú moc. Utečenec a mních, ktorý svoju krajinu už asi nikdy neuvidí...
                 

                 Tändzin Gjamccho je v poradí štrnástym dalajlámom („oceán múdrosti"), duchovný a svetský vodca Tibeťanov. Osamotený človek, a predsa spojený vo svojej viere so všetkými bytosťami. Jedným zo základných bodov tibetského budhizmu je práve to, že nič a nikto neexistuje sám pre seba, všetko so všetkým súvisí ako v jednom obrovskom ekologickom systéme. Do malej kancelárie v Dharamsale (India) prichádzajú odvšadiaľ pozvania a prosby. Prosia dalajlamu, aby celebroval rituály, aby sa zúčastnil osláv, konferencií, aby prednášal a rečnil, aby prišiel prijať ocenenia a čestné doktoráty. Mnohým prosbám vyhovie, súhlasí s väčším počtom podujatí, ako môže vlastne absolvovať. Jeho cieľom je pomáhať.   Vlastne preto je tu. Preto sa už  po štrnásty krát narodil, je to hlavná úloha jeho ľudského bytia. Podľa budhistickej viery musia ľudia neustále prichádzať na svet, v nekonečnom kolobehu znovuzrodení. Opäť žiť znamená opäť trpieť. Samé ilúzie a veľké sklamania. Zúfalstvo nad pominuteľnosťou všetkého. Márne pokusy o udržanie ľudí, veci a stavov. Takto to bude navždy. Iba ak by človek pracoval na sebe. Dobrými skutkami, dobrým zmýšľaním a jasnou mysľou. Potom, po nekonečne mnohých ďalších životoch, možno predsa príde vykúpenie, koniec všetkým ilúziám, dosiahnutie úplného vedomia mysle - osvietenie, ktorým sa človek stáva Budhom. Potom nasleduje nirvána - ukončenie kolobehu reinkarnácií, ukončenie všetkého utrpenia, úplné vyhasnutie.   Dalajlama má tento zdĺhavý proces, prinajmenšom podľa tradovaných náboženských predstáv, už dávno za sebou. Tak, ako všetci lámovia, ako nazývajú Tibeťania svojich duchovných vodcov, je už dávno Budhom. Čiže je už natoľko vyvinutý, že by mohol vstúpiť do nirvány. Ale zo súcitu k ostatným stvoreniam to neurobil. Osvietený, ktorý znova prijal ľudskú podobu, aby mohol pomáhať ostatným bytostiam, aby im bol príkladom a aby im ukázal, ako sa aj oni môžu duchovne obohatiť.   Medzičasom už celý svet pozná jeho úsmev, jeho zdraviacu a žehnajúcu pravicu pred hruďou, jeho štyri jazvy po injekciách na holom pravom ramene, stredovekého vládcu, ktorý sa stal postmoderným anjelom, klenot, ktorý spĺňa želania a ktorý sa môže zjaviť všade tam, kde ho na čokoľvek potrebujú. Od roku 1989, keď dostal Nobelovu cenu za mier ako výraz uznania nenásilného boja za sebaurčenie Tibeťanov, patrí dalajlama k posledným veľkým morálnym autoritám na svete, ktoré sú kompetentné vyjadrovať sa k veľkým otázkam. Menej dogmatický ako terajší pápež Benedikt XVI., duchovnejší ako Nelson Mandela, spoločensky uhladenejší ako Matka Tereza a pod menším diplomatickým tlakom ako Pan Ki-mun (generálny tajomník OSN).   Podobne ako tieto autority je aj dalajlama uctievaný, lebo ľudia, prinajmenšom tí zo Západu, veria, že v ňom nachádzajú dobrosrdečnosť, veľkorysosť a nezištnosť. Hodnoty, ktoré im svet politiky, hospodárstva a priemyslu, svet s pochybnými morálnymi hodnotami dať nemôžu. Zatiaľ čo jeho myšlienky sformulované v hádankách dokážu sledovať určite len učenci, vysoký lámovia a mnísi najvyššieho zasvätenia, obracia sa dalajlama stále aj na zbožných ľudí, ktorí svojmu náboženstvu veľmi nerozumejú, ale zato veria s veľkým zápalom. Obracia sa na tibetských mužov, ktorí neúnavne odriekajú mantry. Na ženy s hrubými okuliarmi, ktoré nevedia ani čítať ani písať. Viac ako čokoľvek iné si želajú požehnanie jeho bezprostrednou prítomnosťou. Niektorí absolvovali dokonca dlhu cestu z Tibetu, len aby boli niekoľko dní v jeho blízkosti. Šli peši. Potajomky. V ľade a snehu cez najvyššie hory sveta. S rizikom, že ich chytia čínske vojenské hliadky. Pre nich je dalajlama ako otec a to nie len duchovný.       Ako dlho toto obdobie už trvá! Cez 50 rokov čakania. Čakanie na to, že sa niečo zmení, že bude lepšie. Že sa už skončí život v exile. Pre dalajlamu i pre všetkých ostatných Tibeťanov. Zatiaľ čo niektorí ešte bojujú s potláčaným hnevom a beznádejou, väčšina sa naozaj snaží o vnútornú vyrovnanosť. Lebo ako hovorí Jeho Svätosť: „žiadna neprávosť nemôže trvať donekonečna". No aj napriek tomu sú aj rozvážni Tibeťania čoraz nervóznejší. Dalajlama 6. júla 2010 oslávi 75 rokov svojho života. Ešte je vo forme. Ale čo bude po ňom?   Podľa tradície sa narodí v novom tele, aj keď Tändzin Gjamccho už uvažoval o možnosti, že reťaz reinkarnácií sa skončí a inštitút dalajlamu jedného dňa prestane existovať. Ale keď aj niekoľko rokov po smrti XIV. dalajlamu nájdu niekde malého chlapca, ktorého príslušní lámovia uznajú ako 15. reinkarnáciu, bude trvať ešte veľa rokov kým vyrastie. Kto má tak dlho udržať rozbitý svet Tibeťanov?   Dalajlama dávno vie, že zápas o nezávislý tibetský štát je prehratý. Preto sa vyše 20 rokov usiluje len o autonómiu Tibetu v rámci Číny. To sa nepáči mnohým jeho krajanom. No čínske vedenie aj naďalej nazýva tohto prostorekého mnícha nebezpečným separatistom. Keď dalajlama zomrie nebudú mať partnera na rokovania. A nebude nikoho, kto by výsledky rokovaní vedel aj presadiť. Pokiaľ sa dá, bude sa 14. dalajlama (možno aj posledný dalajlama) snažiť pomáhať svojmu národu. Bude dbať o svoje zdravie a každé ráno bude robiť duchovné cvičenia pre dlhý život.   Tibeťania po celom svete zatiaľ nespočetnekrát opakujú na stránkach exilovej vlády: „Nech Tändzin Gjamccho, ochranca krajiny snehu, žije naveky. Nech je mu požehnané, aby sa jeho snahám bez zábran darilo."   „Osamotená ľudská bytosť bez ľudskej spoločnosti nemôže prežiť."   „Prirodzenosťou všetkých javov je meniť sa v každom okamihu, naznačuje nám to, že všetkým javom chýba schopnosť trvať, chýba schopnosť byť stále rovnakými."   „Ak problém má riešenie, tak prečo sa strachovať? Ak problém nemá riešenie, tak prečo sa strachovať?"   „Zlosť a nenávisť sú ako rybársky háčik. Je veľmi dôležité sa presvedčiť, že sme sa na ňom nechytili."   „Keď si uvedomíte, že ste urobili chybu, urobte ihneď kroky k náprave."   „Čím viac sa snažíme držať minulosti, tím smiešnejší a znivočenejší sa náš život stáva."       Jeho Svätosť XIV. DALAJLAMA - Tändzin Gjamccho     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Bibko 
                                        
                                            Sedem vecí ktoré na Alberte milujem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Bibko 
                                        
                                            Prince’s Island Park a Peace Bridge
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Bibko 
                                        
                                            Annona muricata
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Bibko 
                                        
                                            Mladý jačmeň, unikátny výživový doplnok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Bibko 
                                        
                                            Kanadskí Slováci
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Bibko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Bibko
            
         
        bibko.blog.sme.sk (rss)
         
                                     
     
        Kdekoľvek ste a kamkoľvek kráčate, kráčajte celým srdcom. Dávajte viac, očakávajte menej a žite naplno!

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    962
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ľudia a spoločnosť
                        
                     
                                     
                        
                            Aj psi sa majú lepšie
                        
                     
                                     
                        
                            Všetko alebo nič
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Dôležité
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Som zdravotná sestra. Personál v slovenských nemocniciach? Otrasný
                     
                                                         
                       Mám panický strach z našich nemocníc. Zažila som tam horor
                     
                                                         
                       Online „šmejdi“ môžu zavolať aj vám
                     
                                                         
                       Aj zvieratá zomierajú lepšie ako pacienti v našich nemocniciach
                     
                                                         
                       Modré z neba alebo „splnený“ sen na televízny spôsob
                     
                                                         
                       Pod Kriváňom k obedu
                     
                                                         
                       Spencer a Hill môžu dať Bondovi lekciu
                     
                                                         
                       Kŕmenie motýľov v Niagara Falls
                     
                                                         
                       Prince’s Island Park a Peace Bridge
                     
                                                         
                       Annona muricata
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




