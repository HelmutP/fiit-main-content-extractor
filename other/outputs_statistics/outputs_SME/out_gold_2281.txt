

 Bol raz jeden človek... 
 Putoval zo zeme do zeme navštívil mnoho krajín a v každej získal nejaké učenie a vedomosti. Jeho názory sa postupne menili a širší obzor vedomostí mu umožnili spoznať svet lepšie. Znalosti, ktoré mal, mu však pomohli len dočasne a jeho srdce vždy túžilo po niečom dalšiom. A tak sa vždy vydával ďalej a ďalej Chcel nájsť niečo, čo by obsiahlo všetku múdrosť a základné stavebné kamene sveta. Prameň v ktorom by jeho duša okúsila večnosť poznania a pravdy. Veril, že jeho poznanie mu odkryje pravdu. Jeho vlasy zošediveli a ruky už nemali tu silu ako kedysi. No v ňom úž driemalo mnoho skúsenosti a zážitkov. Jeho vedomie bolo vždy bystré, ale nohy by nevydržali ďalšiu púť. A tak sa rozhodol ukončiť svoju púť. Uchýlil sa do starej chatrče a rozmýšľal o svojom živote. Jeho hodnoty boli zmätené, vedel že ak je niekto presvedčený o vlastnej pravde, tak niečo prehliadol. Uvedomil si, ako si postupom života vytváral rôzne pravdy v ktorých sa odzrkadľoval jeho vnútorný svet a podľa toho aj súdil. No vždy sa našlo niečo nové a jeho vnútorný svet prijal iné morálne hodnoty. To sa dialo celý jeho život a teraz na sklonku svojho života videl, ako sa tieto morálne hodnoty navzájom bijú. Takmer každá vylučovala nejakú inú. Trhali sa a nenávideli. Niektoré boli silné, iné slabšie. Každá obhajovala niečo iné a stavala na iných základoch. Tieto učenia hlásali rôzne pravdy, no ich pohľad bol vždy jednosmerný a tvrdý. Cítil z nich silu, ale zároveň aj zraniteľnosť a hrozbu. Aj tie čo hlásali spolupatričnosť a mier sa trhali s tými, ktoré boli nevšedné a kruté. Hocijaká myšlienka, ktorú by vypustil do toho búrlivého mora učení, by sa roztrhala na kusy medzi tým všetkým. Nevedel prijať nič o čom by nemal predsudky a zložité koncepty myšlienok. Jeho filozofia a vedomosti siahali ďaleko, no jeho duša bola ranená. Celý svoj život vláčil toto ťažké bremeno, ktoré sa len obaľovalo a rástlo. 
 Keď ho jeho rodina doviezla z chatrče domov, rozhodol sa navštíviť záhrady a miesta v ktorých  vyrastal. Bolo chladne ráno a vedel že nadišla jeho chvíľa. V jeho očiach sa blýskali spomienky na detstvo, jednoduchosť a bezstarostnot, ktorú tu zažíval ako dieťa. Jeho dni vtedy boli dlhé a krásne. Život mal jednoduchý a záhadný charakter. Oči svietili neskúsenosťou, naivitou a snahou po poznaní. Videl, čo ho dohnalo až sem. 
 Sklopil zrak.. nevedel čo ma robiť. Videl nesúvislosti v svojej hlave, cítil vônu záhrady a dymu, počul hukot aut z nedalekej cesty a predovšetkym si uvedomoval svoje reakcie na okolie. Videl to jasne. Vtedy si spomenul na jedno učenie, ktoré dostal od chudobného človeka ktorého stretol po ceste. 
  Je tu, čosi nezrodeného, v život nepovstalého, nezformovaného. Keby toho nebolo, nebolo by cesty von z toho, čo je zrodené, v život povstalé, stvorené, sformované. 
   
  Tento mier je to najvyššie - je to ukončenie, oslobodenie sa od všetkých motívov niečím sa stávať, koniec chytania sa opory, sloboda, vyhasnutie lipnutia. 
   
  Posadnutý chamtivosťou, zmietaný nenávisťou a zaslepený nevedomosťou, s bludivou mysľou - tak človek koná ku svojej vlastnej škode aj ku škode druhých, tak zažíva zármutok a strasti. 
 Kto však odloží chtivosť, nenávisť a nevedomosť, kto potom nejde na úkor svojej ani ostatných, ten už nebude vystavený ďalšiemu utrpeniu, zármutku a strasti.  
   
  Toto je nirvána tu a teraz viditelná, nadčasová, vlastnou skusenosťou prítomná, jasný cíel představujúca, mudrými osobne prežívaná. 
   
 Bol raz jeden človek... 
 Bol tam a videl všetko a nič naraz. 
 Uvidel hranicu aj nekonečnosť. 
 Tmu aj svetlo. 
 Začiatok aj koniec. 
 Usmial sa a v tichosti zomrel. 
  
   
   
 






 

