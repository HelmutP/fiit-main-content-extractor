

 Ako už zrejme samotný názov napovedá, jedná sa o akýsi druh športu malých detí - plávanie. 
 Naše plávanie sa odohrávalo v známom vynovenom wellnes centre Patince, v sedacom bazéne s teplou vodou cca. 32 stupňovou. Na ich stránke je možné vyplniť prihlášku, a ony Vás zaradia do nasledujúceho kurzu, či už pre začiatočníkov, alebo pokročilé bábätká. 
 Pred začiatkom kurzu je potrebné doniesť potvrdenie od lekára, že vaše bábätko je spôsobilé na účasť na tomto kurze. Je to dôležitá podmienka. 
 Plávanie a uskutočnuje každý týždeň 2 dni, v našom prípade po sebe idúce a to po hodine. Z toho trávite 45minút vo vode s bábätkom a cc. 15 minút v saune, prispôsobenej teplote malých človiečikov, aby sa zahriali a osušili. 
 Plávanie z počiatku neberte doslovne:-) 
 Naša lektorka postupovala, ako sa nám občas zdalo pomaly, ale neskôr sme zistili, že cielene doviedla bábätka k plávaniu, čiže zanáraniu na povel. Veď  skoro každého z nás hneď napadol obal známej skupiny, kde pod vodou pláva bábätko s úsmevom a každý rodič by si prial, aby to jeho dieťatko plávalo presne tak po prvej hodine.  Nie to však také jednoduché. Malé deti sa nevedia dlho sústrediť na jednu vec, či činnosť a práve preto je signál sprevádzaný riekankami a pesničkami. Spievajúce mamičky, ponaťahujú svaly detičiek, prepláchnu ich, zahrajú sa s nimi a budujú im budúci vzťah s vodou. A práve toto je dosť dôležité tiež, aby Vaše dieťatko vedelo, že voda je kamarát a nebálo sa jej. 
 Cieľom hodiny bolo naučiť chlapcov aj dievčatká na slovo - tzv. signál naučiť zadržať dych. Začínali sme opatrne - prázdnou dlaňou. Potom sa pridala do ruky voda. Na rad prišiel aj pohárik s vodičkou, nakoniec sa pridalo zanáranie. Keď boli detičky zbehlé aj v tom, začali sme ich ponárať aj bez signálu. A to bolo naším cieľom. Bábatká ku koncu vedeli zadržať dych a pri styku s vodou sa usmievali. 
   
 Podľa ohlasov mamičiek sa detičky absolvujúce tento kurz stali ohybnejšie -  napr. rýchlejšie lozili, začali sa pretáčať z bruška na chrbátik a opačne. Boli otužilejšie, a mali kladný vzťah s vodou. Výborne to využili na letných dovolenkách a pravidelnom kúpaní doma - pri oplachovaní hlavičky sprchou stačilo povedať signál a diaťatko vedelo, čoho čaká. Zavrelo očká, zadržalo dych. a v neposlednom rade tu spoznalo  veľa kamarátov podobného veku, či aspoň s láskou k vode. 
 Ako sme zistili pri kontakte so skupinou pokročilých detí, neskôr sa kurz uberá smerom- upevňovania lásky k vode a využívania ponárania. Nasleduje plávanie pod vodou medzi 2 dospelými na asi mertovú vzdialenosť, podplávania predmetov, točenie sa vo vode ako vývrtka, ale aj preplávania prekážky ako je kruh. 
 Cena kurzu je 115eur. Ak si spočítate, koľko času strávite príjemne so svojou ratolesťou, spoznáte iné mamičky a detičky a váš potomok si osvojí vzťah k vode je táto cena primeraná, aj sa vám bude zdať primalá. 
   

