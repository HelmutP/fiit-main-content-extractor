
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Belko
                                        &gt;
                Tipy a triky
                     
                 Prepojené vlastnosti dokumentu vo Worde 2007 

        
            
                                    10.6.2010
            o
            7:37
                        (upravené
                10.6.2010
                o
                0:56)
                        |
            Karma článku:
                6.69
            |
            Prečítané 
            2620-krát
                    
         
     
         
             

                 
                    Word má k dispozícií veľmi zaujímavú funkcionalitu, ktorá umožňuje vlastnosti dokumentu vkladať do textu ale aj využívať pri vyhľadávaní a zoskupovaní vo Windows.
                 

                 Základné vlastnosti môžete pridať k dokumentu kliknutím na tlačidlo Office &gt; Pripraviť &gt; Vlastnosti. Toto zobrazí v hornej časti aplikácie panel vlastností, ktorý môžete vypísať podľa potreby. Rozšírené vlastnosti môžete pridať kliknutím vľavo na Vlastnosti dokumentu &gt; Rozšírené vlastnosti . Zobrazí sa okno, kde na poslednej záložke Vlastné pridávate ďalšie vlastnosti. Pozor nie všetky sa zobrazujú v okne prieskumníka Windows.    Tie, ktoré sú zobrazené v hornej časti môžete pridať aj priamo do textu dokumentu. Urobíte to na záložke Vložiť &gt; Rýchle časti &gt; Vlastnosť dokumentu . Toto vloží na miesto kurzoru ovládací prvok, ktorý bude zobrazovať zadanú vlastnosť. Tento text vlastnosti môžete prepísať a zmení sa aj položka vo vlastnostiach dokumentu zobrazená v hornej časti. Tiež to funguje aj opačne, keď prepíšete v hornej časti vlastnosť, tak sa zmení aj v texte v ovládacom prvku.   Platí to aj pre zobrazené vlastnosti v prieskumníkovi Windows v dolnej časti okna. Môžete ich meniť aj v tomto okne a zmena sa zobrazí aj v texte dokumentu. Po zmene je potrebné vpravo kliknúť na tlačidlo Uložiť . Ďalšou výhodou je možnosť vyhľadávania a zoskupovania vo Viste, alebo Sedmičke pri zobrazených podrobnostiach. Môžete si pridať stĺpec, ktorý bude tieto vlastnosti zobrazovať.   Táto funkcionalita má opodstatnenie pri tímovej spolupráci, kedy sú uložené v priečinku veľké množstvá dokumentov od rôznych autorov, s rôznymi vlastnosťami a potrebujete sa v tom rýchlo orientovať a dokumenty používať. Funguje to ale len pri Office Open XML formátoch (docx). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Máte radšej bežné školenie alebo webinár?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Analyzoval som podporovateľov R. Fica v Exceli 2013
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Microsoft vrátil do Outlooku 2013 funkciu, ktorú pôvodne odobral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Hromadne si zmeňte utajenie udalostí v Outlooku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Pripojenie pomocníka Office 2013 na internet (+video)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Belko
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Belko
            
         
        belko.blog.sme.sk (rss)
         
                        VIP
                             
     
        Môžete ho stretnúť ako lektora na počítačových školeniach, pri IT konzultáciách vo firmách, na letných terasách a v kaviarňach ako pozoruje dianie okolo seba, ale aj na potulkách po gréckych ostrovoch, pretože počítače nie sú jediné čo ho zaujíma.Ostrovné správy popisuje na osobnej stránke www.dovolenkar.sk Aktívne prispieva na svoj portál Tipy a triky v MS Office.. 
  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    344
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3490
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Tipy a triky MS Office 2013/20
                        
                     
                                     
                        
                            Externé tipy a triky MS Office
                        
                     
                                     
                        
                            SharePoint, spolupráca,Office3
                        
                     
                                     
                        
                            Tipy a triky
                        
                     
                                     
                        
                            Návody
                        
                     
                                     
                        
                            Stalo sa ...
                        
                     
                                     
                        
                            Office 2010/2013 Beta
                        
                     
                                     
                        
                            Microsoft KB články
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Portál Tipy-triky.sk v MS Office
                                     
                                                                             
                                            .
                                     
                                                                             
                                            Správa a optimalizácia siete a IT
                                     
                                                                             
                                            Dovolenkar.sk– dovolenkové informácie
                                     
                                                                             
                                            Windows User Group
                                     
                                                                             
                                            Google+ profil
                                     
                                                                             
                                            Jednoduché video návody
                                     
                                                                             
                                            letenky.dovolenkar.sk - LETENKY do celého sveta
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




