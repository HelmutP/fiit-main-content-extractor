

 
 
  
Výborné pôdne a poveternostné podmienky na juhozápade Nemecka, na svahoch Čierneho lesa - Schwarzwaldu, umožňujú pestovať nielen kvalitné hrozno, ale aj iné druhy ovocia: hrušky, slivky, marhule, maliny, černice a nie na poslednom mieste aj čerešne a višne. A z nich tam dokážu vypáliť kvalitnú čerešňovicu, čiže - ako hovoria Nemci - Kirschwasser. Napriek tomu, že v tomto nemeckom slove je slovo wasser - voda, ide - podobne ako pri iných druhoch ovocia - o aromatickú pálenku.
   
 Pravá schwarzwaldská čerešňovica s fajnovou arómou býva súčasťou mixovaných nápojov, ale i ovocných šalátov, zmrzlín a uplatnila sa dokonca aj pri pečení koláčov. Schwarzwaldská višňová torta sa však vôbec nezrodila v tomto pohorí, ako by sa na prvé počutie jej názvu zdalo. Jej rodiskom bola kaviareň Agner (podľa bedekra od konca 60. rokov minulého storočia už neexistuje) v Bad Godesbergu. (Sú to pôvodne kúpele a neskôr aj diplomatická štvrť v bývalom hlavnom meste Spolkovej republiky Nemecko Bonn). Otcom chýrnej torty je Josef Keller, ktorý zomrel ako 97-ročný v roku 1981 v Radolfzelle pri Bodamskom jazere. Tajomstvo sa údajne dostalo na svetlo sveta až rok po Kellerovej smrti. Josef Keller sa v roku 1915 pripravoval v spomínanej kaviarni na majstrovské skúšky. V rámci nich mal servírovať smotanovú tortu s čerešňami. Napadlo mu pritom ochutiť smotanu čerešňovicou. Touto novou pochúťkou ulahodil najmä bonnským študentom, pre ktorých sa kaviareň Agner stala významným miestom zábavy. V roku 1919 sa Keller presťahoval k Bodamskému jazeru a svoj recept zverejnil prvýkrát až o osem rokov neskôr. Pomenovanie Schwarzwaldská sa však do názvu nedostalo náhodou, keďže višne i pálenka, ktorú pri príprave použil, pochádzali z tohto pohoria. 
   
 Dnes môžete Schwarzwaldskú višňovú tortu kúpiť bežne v obchode. 750-gramová torta vyjde asi na 3 - 3,5 €. 
   
 Samozrejme, že šikovná kuchárka si túto lahôdku môže pripraviť aj doma. V bedekri som sa dočítal, čo na to potrebujeme:100 g masla, 100 g cukru, 1 balíček vanilkového cukru, 4 vajcia, 75 g mandlí, 100 g čokolády, 50 g múky, 50 g pšeničnej múčky mondamin, dve lyžičky prášku do pečiva a do plnky 6 - 7 lyžíc čerešňovice, 500 g čerešní (najlepšie višní), po 50 g ríbezlí a malín a pol litra šľahačkovej smotany. Samozrejme, že fantázii sa ani v tomto prípade medze nekladú, no v každom prípade vyrobíme nielen fantastický zákusok, ktorý sa priam rozplýva na jazyku, ale aj poriadnu kalorickú bombu. 
   
 Či Schwarzwaldskú višňovú tortu ochutnáte doma alebo niekde počas dovolenkového pobytu, prajem vám dobrú chuť. 
  
  
  
  
   
 Na internete som cez google našiel nasledujúci recept na „čierno-čerešňovú tortu“, ktorý si po opravení gramatických a štylistických chýb dovolím priložiť, keďže pochádza zrejme z dielne lepšieho kuchára, ako som ja, a chýba v ňom vari len jedno: čerešňovica (nemusí byť práve schwarzwaldská). 
   
 Cesto: 4 vajíčka, 100g cukru, 150g tmavej čokolády, 100g masla, 5 polievkových lyžíc čerešňového sirupu (nálevu z čerešní), 50g múky, 1 čajová lyžička prášku do pečiva, štipka soli, 50g pšeničnej múčky (mondamin), 150g pomletých lieskových orechov, maslo na vymastenie okrúhleho plechu s priemerom 24 cm. 
   
 Plnka: 500g sladkej smotany, 1 balík vanilkového cukru, 1/8 l čerešňového sirupu (nálevu), 750g višní (z konzervy), 50-80g postrúhanej čokolády. 
   
 Postup pri ceste: 
 1. Rúru vyhriať na 190° C. Vajíčka a cukor vymiešať do penista.  
 2.Čokoládu polámať na menšie kusy a spolu s dvoma polievkovými lyžicami vody roztopiť vo vodnom kúpeli. 
 3. Medzitým maslo roztopiť a nechať vychladiť. 
 4. Maslo, roztopenú čokoládu a čerešňový nálev pridať k vajíčku a cukru.  
 5. Múku, prášok do pečiva, soľ a mondamin zmiešať. Orechy v suchej panvici popiecť do svetlohneda. 
 6. Múku, prášok do pečiva, soľ a mondamin zmiešať s orechmi a pridať k cestu z vajíčka. 
 7. Pekáč vyložiť papierom na pečenie, pridať cesto a okolo ¾ hod. piecť. 
 8. Tortové cesto nechať vychladnúť, potom dvakrát vodorovne prerezať. 
   
 Postup pri plnke a plnení: 
 1.Smotanu s vanilkovým cukrom tuho vymiešať. Čerešne nechať odkvapkať. 
 2. Tortové cesto pokvapkať rovnomerne čerešňovým sirupom a na spodné dva pláty naniesť asi 1 cm šľahačky zo smotany. 
 3. Čerešne rovnomerne poukladať na šľahačku a mierne vtlačiť. Potom tie tortové pláty poukladať na seba. 
 4. Vrch torty a boky potrieť zvyškom šľahačky a posypať postrúhanou čokoládou. 
   
 Do smotany môžeme pridať spevňovač šľahačky alebo studenú roztopenú želatínu, aby bola šľahačka pevnejšia. 
   
   
   

