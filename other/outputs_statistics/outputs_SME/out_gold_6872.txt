

 1) Blížite sa k Bratislave a radi by ste do odbočili doprava na Hydinársku ulicu. Ešte pred 2 týždňami to bolo možné. Dnes už nie. Je jednosmerná. Na hlavnej ceste chýba dopravné značenie zákaz odbočenia vpravo. 
 2) Vyčkáte si v rade a tešíte sa, že odbočíte na Šamorínsku a prejdete okolo nemocnice.  Omyl. Neviem či od pondelku, alebo utorku 18.5.2010 je do tejto a priľahlej Devätinovej zákaz vjazdu motocyklom a automobilom. A svete čuduj sa, nikde nevidíte oznámenie o zmene dopravného značenia a o uzatvorení cesty. 
 Tieto novinky majú za následok, že sa ranná cesta detí do školy a rodičov do zamestnania predĺžila minimálne o 20 minút. 
 Pre šoférov, ktorý preferovali pri ceste z Dunajskej Lužnej, Miloslavova a okolia tzv. Opileckú cestu  (Vinohradnická ul.) majú stále nové prekvapenia. 
 1) Staromlynská jednosmerná. Fajn, na križovatke Staromlynská, Vinohradnická, Devätinova a Pri trati sme prestali chodiť rovno, ale odbočili doľava na Devätinovu, alebo doprava Pri trati. 
 2) Ale zo smeru Vinohradnická je prikázaný smer vpravo. Tak vačšina šoférov sa otočí na priľahlom parkovisku a ide aj tak doľava. 
 3) Tí, ktorí išli po ulici Pri trati a zatočili na ul.8.Maja a Janka Krála majú tiež smolu. Ul.8.mája bola opatrená značkou Zákaz prejazdu. 
 Záver: 
 Všetky príjazdové cesty nás nútia ostať na ulici Svornosti (hlavné cesta od Šamorína). 
 Podunajské Biskupice sa zabarikádovali a pečlivo sa strážia mestskými policajtmi. 
 Milá paní starostka, dobre sa vám spáva? Nám nie, lebo už od rána musíme zažívať stres čo nové ste pre nás pripravila. Hľadal som a hľadal informácie o uzávierkach ale márne. 
 autor: Dan Bel 

