
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Rôzne
                     
                 Švédsky návrh o zákaze hotovosti je len lobizmus 

        
            
                                    7.6.2010
            o
            0:03
                        (upravené
                29.1.2012
                o
                22:55)
                        |
            Karma článku:
                12.85
            |
            Prečítané 
            2686-krát
                    
         
     
         
             

                 
                    Švédi diskutujú o úplnom zákaze hotovosti. V skutočnosti je to len lobizmus kartových spoločností, bánk a nenažraných štátnych úradníkov. Vysvetlím prečo.
                 

                 
  
   Kartové spoločnosti:  Kto si vybavoval posterminál, aby u neho mohli zákazníci platiť kartou, bol prekvapený, že kartové spoločnosti strhávajú z obratu okolo 5 percent ako poplatok za službu.    Skúsme počítať. Podnikateľ nakúpi tovar s režijnými nákladmi za 90 €, predá ho za 100 €. Ak zákazník platí hotovosť tak podnikateľov zostane 8,10 € a štátu odvedie 1,90 €. Ale ak zákazník platí kartou tak podnikateľovi zostane 4,05 €, štátu odvedie 0,95 € a víťazom je  kartová spoločnosť, ktorá dostane 5 €.   Veľa malých podnikateľov, ktorí majú nízku maržu, si nemohlo dovoliť platiť 5 percent kartovej spoločnosti a posterminál zrušili. Kartové spoločnosti sa zbadali až keď niektoré veľké nákupné reťazce odmietli používanie platieb kartou. Až potom začali poskytovať rôzne zľavy za množstvá až tak, že poplatok klesol na 2 percentá v niektorých prípadoch ešte menej ale pridali fixnú zložku poplatku.   Kartovým spoločnostiam ide o tom aby mali svoje percentá zo všetkých platieb aj tých čo sa v súčasnosti vykonávajú v hotovosti. Preto sú za zrušenie hotovosti   Banky:  V súčasnosti banky musia mať rezervu v hotovosti, či u seba v trezore alebo v na účte v emisnej banke vo výške aspoň 1% vkladov.  Čiže keď vložíte do banky 1 €, tak banka môže na svojich účtoch pre požičiavanie peňazí prihodiť sumu 99 €. Potom pri splatení úveru dostane naspať oných 99 € plus úroky a takto sa vyrábajú peniaze (ak by ste to náhodou nevedeli).   Ak by hotovosť padla, tak takáto brzda výroby peňazí by prestala existovať. To nehovorím o príjmoch bánk z poplatkov za oveľa väčší počet operácií.   Existuje ešte možnosť aj virtuálnych peňazí, ale tejto možnosti sa banky boja ako čert kríža. Virtuálne peniaze nie sú účty na ktorých je suma, ale je to iných spôsob fungovania elektronických peňazí, ktorý by znamenal, že emisná banka vytvorí register všetkých emitovaných elektronických peňazí (ako má teraz zoznam bankoviek) a v ňom bude zapísané kto danú elektronickú bankovku práve vlastní.   Týmto spôsobom by banky mohli požičiavať len reálne elektronické peniaze a nemohli by vytvárať nekryté peniaze.   Štátni úradníci:  O štátnych úradníkoch  je známe, že dokážu minúť akúkoľvek sumu čo ekonomika vyprodukuje. Ak ekonomika nestíha tak štátni úradníci dokážu krajinu zruinovať požičiavaním si peňazí od iných štátov.   V prípade, že by nebola hotovosť ale každá transakcia išla cez banky a kartové spoločnosti, nič nebráni štátnym úradníkom okrem terajších daní a poplatkov zaviesť daň aj z akéhokoľvek transferu peňazí. Stačí napríklad ak by rodičia prispeli svojim deťom finančne a hneď by štát chcel z toho odvody do poisťovní a daň z príjmu fyzických osôb. Navyše takýto evidovaný príjem by bol dôvod ako je dôvodom aj teraz na zrušenie materskej alebo sociálnej podpory, rodinných prídavkov ap.   Samozrejme štátni úradníci chcú vycicať z ekonomiky maximum aby mohli  prerozdeliť (sebe) čo najviac. Keby to nebola pravda tak by už dávno zaviedli strop pre výdavky štátu.   Ďalšie dopady:  Strata možnosti ochrany pred krachom banky. Ľudia, ktorí majú niečo našetrené, tak si na účtoch nechávajú len sumu ktorú v prípade krachu banky istí štát. Ostatné peniaze ukladajú v sejfoch banky alebo transformujú na drahé kovy. Ak by bola zrušená hotovosť, tak sa bude používať zahraničná hotovosť. Ak by sa hotovosť zrušil všade, alebo zakázalo vlastniť zahraničnú hotovosť ako to platilo počas socializmu, tak by zostali len drahé kovy, ktorých cena by eskalovala.   Sto percentná kontrola občanov, ktorí by bez použitia karty a zaevidovaní v elektronických systémoch nemohli ísť ani na wc.   Ak si myslite, že poplatky za operácie by sa znížili tak vás vyvediem z omylu. Vo viacerých štátoch vznikli spoločnosti, ktoré umožňovali s nástupom internetu vykonávanie mikroplatieb. Platil sa len paušál za účet a nie za transakciu. Lenže tieto spoločnosti skončili po tom čo ostatné banky zacítili konkurenciu. Klepli im po prstoch cez emisnú banku, ktorá ako že poskytovanie takýchto finančných transakcii nepovolila.   Výplata mi chodí na účet, používam karty rôznych kartových spoločností a hotovosť mám pri sebe len minimálnu a tak to môže riešiť každý čo má rešpekt pred okradnutím. Ale ani karty nie sú chránené pred zneužitím. Stačí kartu ukradnúť a spraviť transakcie na takých miestach kde sa nevyžaduje overenie pinom, alebo priložiť obeti k  hlave pištoľ a zastreliť ju keď pin neprezradí. Čiže celé odôvodnenie, že sa to robí s cieľom potlačenia kriminality je bluf!   Záver: Osobne považujem tlak na zrušenie hotovosti len ako lobizmus v snahe prerozdelenia zisku v ekonomike toho ktorého štátu a zvýšenie kontrolovateľnosti každého občana štátnou mocou. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




