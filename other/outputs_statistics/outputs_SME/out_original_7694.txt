
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Lipták
                                        &gt;
                Nezaradené
                     
                 Väčšinový volebný systém 

        
            
                                    31.5.2010
            o
            18:10
                        (upravené
                31.5.2010
                o
                18:18)
                        |
            Karma článku:
                5.79
            |
            Prečítané 
            1187-krát
                    
         
     
         
             

                 
                    V týchto dňoch určite veľa ľudí stojí pred dilemou koho voliť. Zahodiť svoje volebné právo nechcú, no na druhej strane kandidujúce strany a zoskupenia v nich nevzbudzujú takú dôveru, aby im dali svoj hlas. Za posledných dvadsať rokov tu snáď nezostala jediná politická strana, ktorá by nebola skorumpovaná, nebola spájaná so žiadnymi škandálmi, ... . Ako teda ďalej? Založiť novú politickú stranu? Tých tu už bolo.... Buď zostali bezvýznamnými straničkami , ktoré postupne zanikli, alebo sa dostali na výslnie, do parlamentu, no tam sa rýchlo skompromitovali, a teda ich životnosť bola maximálne štvorročná. Ja vidím riešenie v zmene volebného systému.
                 

                 V našej krajine je historicky zaužívaný pomerný volebný systém. Volič volí politickú stranu, ktorá ho zaujme a ďalej sa nestará. Kandidátku na poslancov zostavila samotná strana a volič môže ovplyvniť poradie kandidátov krúžkovaním. Zmena poradia sa však často nepodarí. Keď je odvolené, kandidáti na najvyšších miestach sa na štyri roky stanú poslancami parlamentu. Voličova úloha sa skončila. Volič má minimálnu šancu kontrolovať svojho zástupcu v parlamente, lebo ani sám nevie, kto konkrétne je ten jeho poslanec, zástupca v parlamente. Čo nezávislí kandidáti? Kandidáti, ktorí nechcú byť spájaní so žiadnou politickou stranou? Tí majú smolu.   V tomto systéme veľa šancí na úspech nemajú. O všetkom totiž rozhodujú strannícke centrály.       Väčšinový volebný systém podľa mňa oveľa viac plní zastupiteľskú funkciu parlamentu v spoločnosti. Vo väčšinovom volebnom systéme by bola krajina rozdelená na 150 volebných obvodov (150 kresiel v parlamente). V každom volebnom obvode (menší okres, menšie mestečko, mestská časť..) by kandidáti súperili o poslanecký mandát. V dvojkolových voľbách by sa víťaz stal poslancom za daný volebný obvod.       V čom je hlavný rozdiel medzi oboma systémami? Vo väčšinovom systéme kandidáti poznajú daný volebný obvod, lebo v danom obvode zväčša žijú. A takisto voliči poznajú kandidátov, vypočujú si ich sľuby a riešenia a rozhodnú sa, koho podporia. Po voľbách majú svojho konkrétneho poslanca, na ktorého sa môžu obrátiť, ktorého prácu môžu sledovať a nakoniec ju môžu zhodnotiť v ďalších voľbách. Poslanci sú teda viac závislí od svojich voličov ako od stranníckych centrál. Ak s nimi budú voliči spokojní, môžu sa dostať do parlamentu aj ako nezávislí, teda bez podpory politickej strany. Voliči sa nemusia rozhodovať len podľa politickej príslušnosti kandidáta. Ak napríklad poznám kandidáta osobne ako slušného a šikovného človeka, podporím ho, aj keď nie som priaznivcom jeho politickej strany. Naopak, ak kandidáta mojej obľúbenej politickej strany poznám ako nedôveryhodného človeka, moju podporu nedostane.       Ja si myslím, že zmenou volebného systému  na väčšinový by sa politika stala opäť raz vecou verejnou. Nie len raz za štyri roky, v deň volieb. Tento systém funguje napríklad vo Veľkej Británii a v mnohých krajinách funguje v kombinácii s pomerným systémom. Čo myslíte? Bol by prínosom aj pre Slovensko? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (46)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Lipták 
                                        
                                            Spomienka na Ľudovíta Kukorelliho (1914-2014)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Lipták 
                                        
                                            Nakupovanie v nedeľu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Lipták 
                                        
                                            Rozhovor s Tomášom Jurčom, hokejistom Detroitu Red Wings
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Lipták 
                                        
                                            Rozhovor s Tomášom Marcinkom, hokejovým reprezentantom SR
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Lipták 
                                        
                                            Tatrabanka- najlepší idú za nami...II
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Lipták
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Lipták
            
         
        jaroslavliptak.blog.sme.sk (rss)
         
                                     
     
        Minds are like parachutes. They only function when they are open.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    74
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1682
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Spomienka na Ľudovíta Kukorelliho (1914-2014)
                     
                                                         
                       Prečo sú slovenské hotely prázdne?
                     
                                                         
                       Aj takým primátorom bol Rudolf Schuster
                     
                                                         
                       Rozhovor s Tomášom Jurčom, hokejistom Detroitu Red Wings
                     
                                                         
                       Rozhovor s Tomášom Marcinkom, hokejovým reprezentantom SR
                     
                                                         
                       V Slovenskej sporiteľni alebo v Kocúrkove?
                     
                                                         
                       Bolí vás niečo? Nie, sestrička, iba duša.
                     
                                                         
                       Prepáč nám to, Nasťa!
                     
                                                         
                       Morvayova krymská olympiáda
                     
                                                         
                       Tatrabanka- najlepší idú za nami...II
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




