
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Lorenzová
                                        &gt;
                Nezaradené
                     
                 Slovenský triler 

        
            
                                    23.3.2010
            o
            19:28
                        |
            Karma článku:
                18.06
            |
            Prečítané 
            4896-krát
                    
         
     
         
             

                 
                    Keď som pred jedenástimi rokmi absolvovala psychosociálny výcvik v rámci týždennej prípravy náhradných rodičov, riešili sme teoretickú otázku: Čo by sa muselo stať, aby naše deti skončili v detskom domove? Väčšinou sme povedali, že taká situácia nemôže nastať, lebo keby sme aj obaja rodičia boli po smrti, máme takú dobrú rodinu a priateľov, ktorí by sa o naše deti úplne samozrejme postarali, tak ako by sme sa aj my postarali o ich deti, keby bolo treba. Ó, akí sme boli naivní!!! Veď o deťoch rozhodujú úradníci, ktorým nič (ne)ľudské nie je cudzie!
                 

                 
  
   Na tomto výcviku som spoznala Danku, Jarka a ich deti. Vtedy mali 4 deti, 3 blonďavé podobné na rodičov a jedno dievčatko výrazne iné, ázijské korene nemohla zatajiť. Dozvedela som sa na moje prekvapenie, že malá Evička, zväčša omotaná otcovi okolo krku, ktorá by mu akoby z oka vypadla, nie je ich, teda už je, ale majú ju z detského domova rovnako ako exotickú Nikolku.   Za týždeň sme sa spoznali lepšie než sa bežne spoznajú ľudia za celý život. Keďže sme „rovnaká krvná skupina“ a mamka Danka je vo veku mojej najstaršej dcéry, stala som sa „jej adoptívnou mamkou“.   Ich túžbou bola veľká rodina. Postupne prijali ďalšie deti. Deti – to je ich život. Ich dom, jeho dispozičné riešenie i zariadenie, ich veľké auto, denný rozvrh i víkendový program..., všetko je prispôsobené deťom, všetko sa točí okolo detí!   Až sa im to stalo...   Americké trilery sú často založené na tom, že osoba reprezentujúca štátnu moc, je tyran, sadistický despota a manipulátor, zneužívajúci svoju právomoc. Obeť je zúfalo bezmocná. Komu úrady uveria? Nemá úniku. Trpíte s ňou – ale veď je to len film!   Ak sa to stane Vašim blízkym, už to nie je adrenalínový relax na jeden večer.   Práve to sa im stalo. Celý príbeh je dlhý a ako film plný zlovestných detailov. Čoho sú schopní nekompetentní ľudia!? Majú kompetencie v zmysle právomocí daných im úradom, avšak majú osobnostné predpoklady a profesijné zručnosti??? Sociálna pracovníčka a psychologička, neváhajúc niekoľkokrát porušiť zákon a mnohokrát dobré mravy sa rozhodli rodinu zničiť. Nebrali do úvahy skutočnosť, pozitívne posudky od obce, zamestnávateľa, školy, lekára, diagnostického ústavu,... Nakoniec dala kurátorka, teda UPSVaR dal návrh na zrušenie pestúnstva nad všetkými 6 deťmi, ktoré žili od útleho detstva v tejto rodine. Súd zamietol návrh na zrušenie pestúnstva nad 3 súrodencami, mladšími deťmi a aby sa vyhol možnému odvolávaniu zo strany UPSVaR-u rozhodol, že na 3 staršie deti ako aj na rodičov Danku a Jarka má vypracovať posudok súdny znalec. Než k tomu došlo, UPSVaR vyžiadal od krajského súdu predbežné opatrenie na odobratie Nikoly z rodiny a jej umiestnenie do náhradnej osobnej starostlivosti istej prapodivnej konštelácie (ťažko nazvať rodinou) http://evalorenzova.blog.sme.sk/c/203348/SOS-Vypocuje-niekto-Nikoline-volanie-o-pomoc.html . Nikola ma vtedy telefonicky kontaktovala a s jej súhlasom som Vás požiadala o pomoc uverejnením jej listu. Niekoľkokrát utiekla, ale nakoniec sa nechala kúpiť, zastrašiť, ukecať. Nad rámec svojich kompetencií jej sociálka zakázala kontakt s pestúnmi, preložili ju na inú školu a pod. A to stále za ňu boli teoreticky zodpovední pestúni. Hoci je to veľmi smutné, že sa po takmer 13 rokoch strávených v rodine, z vlastnej nezrelosti, hedonistického chápania života a pubertálnej hlúposti a nezodpovednosti sa dostala do situácie, kedy už jej asi niet pomoci.    Súdny znalec vyhodnotil rodičov – pestúnov za osoby, ktoré spôsobom života, osobnostnými a charakterovými vlastnosťami ako aj fyzickou a psychickou spôsobilosťou poskytujú vhodné vzory správania pre maloleté deti. Tiež uviedol, že je naliehavo potrebné ukončenie konfliktného vzťahu UPSVaR-u s pestúnmi a umožniť im zabezpečiť si potrebné odborné konzultácie a poradenstvo podľa ich vlastného výberu, pretože na to majú dostatočnú zodpovednosť.   S ťažkým srdcom, ale na základe vlastného rozhodnutia, ku ktorému dospeli, keďže nemohli mať reálny vplyv na Nikolu a jej ďalšie správanie, požiadali Danka a Jarko o zrušenie pestúnstva nad Nikolou. Tým súd celú záležitosť uzavrel a všetky ostatné deti rozhodol v rodine ponechať. Víťazstvo? Áno, keby... UPSVaR sa chopil najťažšieho kalibru: vymyslel si sexuálne zneužívanie!!! V piatok pred týždňom sa dostavili za sprievodu polície a všetky deti odviezli od pestúnov do detského domova!   Každému psychológovi, ktorý diplom nedostal za modré oči, musí byť jasné, že takto sa sexuálne zneužívané deti a pedofili nesprávajú.    Za Jarka dám ruku do ohňa! Kľudne by som mu zverila svoje deti či vnúčatá!       Obeť v amerických trileroch nakoniec po napínavých peripetiách nájde cestu k heppyendu. Nadriadení či spolupracovníci zlosyna mu sprvu dôverujú, najprv s ním spolupracujú, ale neskôr začínajú vnímať podozrivé detaily a zvláštne súvislosti. Začína im svitať.    Aj prievidzské (a)sociálne pracovníčky dohonia ich zlomyselné konštrukcie a očividné klamstvá. Ich nadriadení by mali tiež spozornieť! Prečo asi pred týždňom mladík, ktorému poskytovali „sociálno-právnu ochranu“ sa s baseballovou pálkou „vyšantil“ na úrade a hlavne v kancelárii psychologičky? Ako citlivo a odborne ho priviedla k tomuto amoku?    A hlavne by mali zaregistrovať skutočnosť, že všetci stoja za Jarkom, Danielou a ich deťmi! Lebo oni si zaslúžia úctu a nie šikan!       A deti, ktoré Danka s Jarkom už raz zachránili pred životom v detskom domove majú byť doma! Preto ak viete, pomôžte radou či skutkom, aby to bolo čím skôr.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (81)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Barborka vôbec nemusela byť v detskom domove.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            "Lucinka bude dobrá mama!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Mohol tu byť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Prečo je u nás lepšie byť opusteným psom,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Lorenzová 
                                        
                                            Aj toto je v najlepšom záujme dieťaťa?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Lorenzová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Lorenzová
            
         
        evalorenzova.blog.sme.sk (rss)
         
                                     
     
        Som mamou biologických aj prijatých detí, pracujem v OZ DOMOV V RODINE, ktoré združuje profesionálne rodiny a pomáha a podporuje všetky formy náhradného rodičovstva.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2580
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pozrimeže, kto sa to objavil v pozadí „veľkej daňovej lúpeže“
                     
                                                         
                       Som plačúci Bratislavčan
                     
                                                         
                       Myslí to bratislavský Magistrát s čiernymi skládkami vážne? časť 4.
                     
                                                         
                       Pre tehotné deti nezdvihli úradníčky zadok ani telefón
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




