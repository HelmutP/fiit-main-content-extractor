

 NEW YORK/LOS ANGELES - Necelé štyri roky po páde "dvojičiek" Svetového obchodného centra (WTC) nakrúti režisér Oliver Stone film založený na príbehu dvoch policajtov, ktorí 11. septembra 2001 uviazli v  troskách. 
 Jedného z policajtov, seržanta Johna McLoughlina stvárni herec Nicolas Cage, oznámilo filmové štúdio Paramount Pictures s očakávaním, že film bude mať premiéru v budúcom roku. 
 "Je to práca kolektívneho zápalu, vážne premýšľanie o tom, čo sa stalo, a nesie v sebe súcit, ktorý lieči," uviedol Stone v piatkovom  vyhlásení. "Je to skúmanie hrdinstva v našej krajine, je to však súčasne medzinárodné vo svojej ľudskosti." 
 Hoci hviezdny tím Stone-Cage filmu pravdepodobne zabezpečí vysokú pozornosť, nejde o prvú snímku venujúcu sa útokom. Dosiaľ išlo skôr o nezávislé filmy a televízne dokumenty. V roku 2002 hral Anthony LaPaglia vo filme The Guys  hasičského kapitána, ktorý pri zrútení WTC prišiel o ôsmich mužov. Celovečernú snímku o útokoch v súčasnosti vyvíja aj konkurenčné štúdio Columbia Pictures, ktoré si zaistilo filmové práva na knihu 102 Minutes od dvoch reportérov New York Times, opisujúcu interval medzi nárazom prvého uneseného lietadla do WTC do pádu prvej budovy.  	 (tasr) 

