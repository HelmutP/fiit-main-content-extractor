
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Jurkovič
                                        &gt;
                Digitalizácia múzejných zbiero
                     
                 Evidencia alebo automatická identifikácia? 

        
            
                                    1.6.2010
            o
            13:44
                        (upravené
                21.6.2010
                o
                8:07)
                        |
            Karma článku:
                3.45
            |
            Prečítané 
            960-krát
                    
         
     
         
             

                 
                    Väčšina z nás, ešte nebola na svete, keď v roku 1949 dvaja mladíci Silver a Woodland prihlásili na patentový úrad čiarový kód. Určite sa pri jeho vymýšľaní zapotili, lebo sa im to podarilo kdesi na pláži vo Floride. Úradníkom trvalo tri roky, kým sa rozhodli vydať im na takú hlúposť patent. Po dvadsiatich dvoch rokoch od patentovania po prvý krát zoskenovali Američania so svojho charakteristického produktu - žuvačky také tie čiarky, aké sú dnes nalepené na KAŽDOM tovare. To bol začiatok revolúcie v skladovom hospodárstve, počas ktorej sa zmenila manuálna evidencia tovaru na automatickú identifikáciu a ktorá povýšila logistiku na vednú disciplínu.
                 

                 Dnes, tuším že aj babky na trhovisku používajú registračné pokladnice so   skenermi čiarového kódu, a tak sa vlastne ani nie je čo diviť, že   automatická identifikácia sa pomaly začína uplatňovať aj v pamäťových   a fondových inštitúciách. V tých trochu modernejších, ako sú napríklad   knižnice, si už roky nevedia predstaviť prácu bez tejto technológie. My   múzejníci, si doteraz nevieme predstaviť prácu s ňou, lebo naša úcta   k tradíciám nás núti pridržiavať sa overených a stáročia zaužívaných   metód. Napriek tomu sme sa v rámci CEMUZ - v jednom z pilotných podprojektov s príznačným názvom „Múzeum 21. storočia" - pokúsili sformulovať víziu automatizácie depozitárneho systému.  Vlastne sme si aj vyskúšali, ako funguje automatická identifikácia múzejných zbierok, keď použijeme technológie vyspelého skladového hospodárstva.  Výsledkom  skúšania je celý rad poznatkov, medzi nimi aj toto: ak to ide v jednom múzeu, mohlo by to fungovať aj  v iných (viď http://portal.cemuz.sk/portal/portal.aspx?bo=b6portal.wo&amp;fn=top&amp;c=1000326&amp;t=1000176 ).   Okrem poznatkov sme dosiahli aj lapidárnejšie výsledky: komplexná automatizácia správy zbierok SNM-Múzea židovskej kultúry v Bratislave, komplexná digitalizácia zbierok a tiež celkom komplexná metodika uplatnenia technológie RFID v oblasti správy zbierok (viď http://old.snm.sk/docs/M-3-RFID.00.pdf). Takže nie čiarový kód, ale RFID - rádiofrekvenčný identifikátor.  Nebudem podceňovať všeobecnú rozhľadenosť čitateľov a vysvetľovať, čo to je za technológiu ( http://sk.wikipedia.org/wiki/Vysokofrekven%C4%8Dn%C3%A1_identifik%C3%A1cia ), len podotknem, že jej využitie v múzeu je neporovnateľne širšie oproti čiarovému kódu v knižniciach. Jedným z profitov spomínaného pilotného projektu je, že vieme napríklad aj to, ako túto technológiu nasadiť v múzeu, čo to stojí peňazí, koľko práce je potrebné vynaložiť a ako má byť organizovaná.   Ďalším ziskom projektu je rozšírenie katalogizačného modulu ESEZ 4G o rozhranie RFID a nástroje na sledovanie a vyhodnocovanie pohybu zbierok a osôb v priestoroch múzea (depozitároch, expozíciách). To znamená, že všetky pracoviská, využívajúce nástroje vedomostného systému múzeí SR, môžu okamžite začať prechod na automatickú identifikáciu zbierok.   Ak by sme sa naozaj chceli pustiť aj v múzeách do modernizácie vnútorných odborných činností - evidencie nadobúdania a správy zbierok, tak na začiatok potrebujeme okrem softwarových nástrojov ESEZ 4G aj základné RFID zariadenia, ktoré sprostredkovávajú komunikáciu medzi vedomostným systémom a RFID tagmi. Nie je to nič moc. Úplne minimalistické počiatočné náklady na HW nepresiahnu 500 EUR. K tomu však treba ešte pripočítať štítky s tagmi pre každý predmet. Ich cena sa pohybuje od 0,5 - 3 EUR (v závislosti od typu a množstva odberu). Okrem peňazí však treba vložiť do modernizácie aj značnú sumu práce. Múzeum, ktoré sa odhodlá prejsť na automatickú identifikáciu, musí počítať s tým, že každý jeden predmet prechádza cez ruky kustóda a každý jeden predmet musí mať rozšírený záznam v katalogizačnom module o RFID tag.   Ak by sme chceli túto manipuláciu zefektívniť a popri RFID „tagovaní" si zabezpečiť aj plnenie istých povinností, ktoré vyplývajú múzeám z vyhlášky, napríklad: urobiť revíziu zbierok a zároveň aj obrazovú dokumentáciu každého predmetu, potrebovali by sme ešte nejaké zariadenia navyše. No a ak by sme chceli tieto procesy vysoko zefektívniť , tak môžeme použiť komplex zariadení, ktoré sme nazvali „Dokumentačný modul" (popis projektu je v prezentácii, ktorá sa dá stiahnuť z adresy http://portal.cemuz.sk/portal/portal.aspx?bo=b6portal.wo&amp;fn=ShowOne&amp;open=0&amp;c=1000326&amp;t=1000296 ). Ten by už bol drahší -niečo cez 4 000 EUR. No oproti komponentom súčasných informačných megasystémov, fungujúcich v modernom skladovom hospodárstve, je to takmer zadarmo. A dodajme, že je to komplexné a vysoko účinné riešenie automatizácie a štandardizácie vstupu kultúrneho objektu do digitálneho systému - ako vedomostného, tak aj depozitárneho.   Pre užívateľov ESEZ 4G je teda všetko pripravené na poloautomatickú digitálnu obrazovú dokumentáciu a automatizáciu identifikácie zbierkových predmetov i osôb, ktoré prichádzajú do styku so zbierkami. Jasné, že to má háčik - vlastne dva:   1.      Múzea by to museli chcieť.   2.      Múzeá by na to museli mať.   Prvým háčikom je náš tradicionalizmus (alebo konzervativizmus?), druhým je to, na čo sa dnes vyhovára už skoro každý -  ekonomická kríza. Takže by sme to vlastne mali celé „zabaliť", lebo tradicionalisti sa asi nebudú chcieť učiť novým metódam a tam, kde nie sú centy na chleba, ťažko vydolujeme „euráče" na techniku. Je to dosť nešťastná situácia, ktorá by mohla viesť k retardácii, a nielen múzejníctva. Píšem to v podmieňovacom spôsobe, hoci mi nenapadá nič, čomu by som naozaj veril, že môže túto situáciu v dohľadnej dobe zvrátiť. Ale pred štvrťstoročím by som tiež neveril, že sa rozpadne svetová socialistická sústava.   Vynález Silvera  a Woodlanda čakal dva roky na patentovanie, dvadsať rokov na širšie uplatnenie a päťdesiat na revolúciu v skladovom hospodárstve.  Silver (+1963) sa nedočkal toho najväčšieho „boom" spôsobeného ich vynálezom no Woodland (*1921) ešte žije. Jemu je to zrejme jedno, ale napríklad ja by som bol rád, keby sa  ešte za jeho života rozšírila automatická identifikácia aj do našich múzejných depozitárov. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Jurkovič 
                                        
                                            K digitalizácii múzejných zbierok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Jurkovič 
                                        
                                            Digitalizácia kultúrneho dedičstva a múzeá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Jurkovič 
                                        
                                            Mlčanie múzejníkov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Jurkovič 
                                        
                                            Komercionalizácia alebo marketing
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Jurkovič 
                                        
                                            OPIS nepomôže múzeám ani centom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Jurkovič
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Jurkovič
            
         
        janjurkovic.blog.sme.sk (rss)
         
                                     
     
        Neviem o sebe nič, čo by stálo za zverejňovanie.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    820
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Digitalizácia múzejných zbiero
                        
                     
                                     
                        
                            Komentáre
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




