
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabo Németh
                                        &gt;
                Súkromné
                     
                 Odhalené jadro 

        
            
                                    19.2.2010
            o
            18:30
                        (upravené
                19.2.2010
                o
                18:39)
                        |
            Karma článku:
                3.77
            |
            Prečítané 
            396-krát
                    
         
     
         
             

                 
                    Diametrálne odlišní
                 

                 a predsa zahľadení do seba,   meníme často kurz.   Riešime obyčajné veci,   držiac sa nad vodou.       Čas nám nevráti  krídla,   keď delíme svet   s jeho bolesťami.   Jediný menovateľ láska,   je konečný kompas,       ktorý určí smer,   nechá brehom istotu,   že každá loď   s plachtami proti vetru   rozbije vlny na atóm   a potvrdí.       Ísť proti vetru sa oplatí,   keď nájdeš dávno stratené.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky a čas
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Plač kvetov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Nočná návšteva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Dušičková
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabo Németh
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Milujú sa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabo Németh
            
         
        gabonemeth.blog.sme.sk (rss)
         
                                     
     
         profesionálny knihovník, básnik, spisovateľ, od roku 2006 člen Spolku slovenských spisovateľov. Vydal básnické zbierky: Za súmraku, Slnko nad básňou, Rodinný album. Venuje sa duchovnej poézii. Chce byť pokračovateľom katolíckej moderny. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    917
                
                
                    Celková karma
                    
                                                2.76
                    
                
                
                    Priemerná čítanosť
                    335
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            denná čítannosť
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            A300
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            len a len dobré knihy
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                             
                                     
                                                                             
                                            denník Sme všetky články
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Skôr než zaspím od S.J. Watson
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Queen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            blog spisovateľky Miroslavy Varáčkovej
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetky dobré a kvalitné blogy na Sme.sk
                                     
                                                                             
                                            facebook
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Muž z kríža
                     
                                                         
                       Spomienky a čas
                     
                                                         
                       Jesenné dotyky
                     
                                                         
                       Plač kvetov
                     
                                                         
                       Nočná návšteva
                     
                                                         
                       Spomienky
                     
                                                         
                       Na prechádzke
                     
                                                         
                       Dušičková
                     
                                                         
                       Ranný obraz
                     
                                                         
                       Po stopách vlastného príbehu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




