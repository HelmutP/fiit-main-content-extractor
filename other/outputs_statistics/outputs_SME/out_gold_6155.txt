

   
 Túžba tlejúca v tvojej duši 
 predstavami vo mne búši 
 spaľuje ma vôňou kvetov 
 v dialógu dvoch tiel bez slov 
   
 Zhoreli sme vášňou do tla 
 láska sa nám tvárí dotkla 
 popol zmätie do pálivej noci 
 bytostí vo Fénixovej moci 
   
 Po zrnkách nás opäť skladá 
 DNA lásky do citov vkladá 
 sme tajnou Pandorinou skrinkou 
 bez ťarchy bremena vinníkov 
   
 Očistení putom blízkosti 
 vpíšeme do knihy večnosti 
 príbeh o bludisku a hľadaní 
 o tajomstve a naplnení 
   

