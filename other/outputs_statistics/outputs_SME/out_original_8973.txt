
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marie Stracenská
                                        &gt;
                o kadečom
                     
                 Slovensko – maďarská etuda 

        
            
                                    16.6.2010
            o
            14:42
                        (upravené
                16.6.2010
                o
                17:06)
                        |
            Karma článku:
                10.99
            |
            Prečítané 
            1379-krát
                    
         
     
         
             

                 
                    „Čosi čosi pošta čosi čosi." Vo vchode pri schránkach stretávam tetu zo štvrtého. „No, asi ešte pošta neprišla", odpovedám podľa najlepšieho uváženia, vychádzajúc zo situácie a jej  nesúhlasného krútenia hlavou.
                 

                 „Čosi čosi egeď eček čosi čosi čosi." No, tak to už ale teda fakt neviem, čo odpovedať. Nastupujeme do výťahu.   „Ja neviem po maďarsky", hovorím tete už aspoň po desiaty raz, odkedy tu bývame.   „Ima kiči tudom slovákul." Hej, teta, viem, že vy tiež toho veľa po slovensky nezvládnete.   Tak sa na seba aspoň usmievame. Štyri poschodia spolu mlčíme.   Teta má asi osemdesiat, býva so sestrou.  Obe sú milé a príjemné, ibaže okrem dobrý deň - jónapot sme si nikdy nič viac neboli schopné povedať. Nie, že by nebola snaha. Ale je to teta zo starej školy a ja zas prisťahovalec . Takže naša konverzácia nad kočíkom sa zvykla končiť mojím porozumením „kiči bobo" - čo som sa naučila, že je lichôtka bábätku. Ona zvykla pokračovať, ja som zas vysvetľovala, že nerozumiem, tak sme si potom len zamávali. Teraz sa snaží hovoriť občas s mojimi deťmi. Im nevadí, že nerozumejú a odpovedajú po slovensky - hocičo, čo si myslia, že teta myslí. Ono to tu tak je - že staré panie toho veľa po slovensky nenahovoria. V obchodoch a na úradoch zvyčajne také staré tety nepracujú, takže nemám žiaden problém. Mladší po slovensky vedia. A koniec koncov, od tých starých tiet je milé, že sa chcú prihovoriť.   Bola to naša slobodná vôľa ísť bývať do skoromaďarského mestečka. Je to trochu exotika, občas smiešne a zaujímavé, niekedy príjemne nevedomé. No a čo. Aspoň viem naozaj z vlastnej skúsenosti posúdiť, čo je pravdy na slovensko - maďarskom „napätí".   „Dovidenia", zdraví ma teta vystupujúc na svojom poschodí zašmodrchane, zatiaľ čo ja snažím presne v tej istej chvíli aspoň trochu trafiť maďarské „vislát". Zakývame si a obe sa zasmejeme.   To je aj v slovenčine aj v maďarčine rovnaké. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Vianoce vo štvrtok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Nepoznám žiadne dieťa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Ranná káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Cez hrádzu zrýchlene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Poschodová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marie Stracenská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marie Stracenská
            
         
        stracenska.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som žena. Novinárka, lektorka a konzultantka. Mama nádherných a šikovných dvojčiat. Manželka, dcéra, sestra, priateľka. Neľahostajná. Mám rada svet. Rovnako rýchlo sa viem nadchnúť, potešiť a rozosmiať ako pobúriť a nahlas rozhnevať. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    701
                
                
                    Celková karma
                    
                                                12.16
                    
                
                
                    Priemerná čítanosť
                    1779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o deťoch
                        
                     
                                     
                        
                            o vzťahoch
                        
                     
                                     
                        
                            o kadečom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Moja rozprávka
                     
                                                         
                       Zaťovia
                     
                                                         
                       Nápadník
                     
                                                         
                       Univerzitná nemocnica vyriešila problém Richterových prezervatívov
                     
                                                         
                       Obrovská stará dáma
                     
                                                         
                       Tutoľa máme kopčok
                     
                                                         
                       Bojovníčka
                     
                                                         
                       Vifon
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




