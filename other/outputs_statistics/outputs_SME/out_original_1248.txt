
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Minárik ml.
                                        &gt;
                Iné
                     
                 Biskup neprirovnal Radičovú k Hitlerovi! 

        
            
                                    25.2.2009
            o
            16:46
                        (upravené
                14.3.2010
                o
                14:03)
                        |
            Karma článku:
                14.37
            |
            Prečítané 
            4377-krát
                    
         
     
         
             

                 
                    Nie biskup Baláž ostro zaútočil na Radičovú, ako tvrdí SME, ale SME ostro a lživo zaútočilo na biskupa Baláža. Čím viac predstavitelia Katolíckej cirkvi vystupujú proti názorom, ktoré Iveta Radičová reprezentuje, tým viac sú vystavení kampani s cieľom diskreditovať ich hlas vo verejnej diskusii. Biskup Baláž veľmi jasne pomenoval morálne princípy, na ktorých musí stáť dobrá spoločnosť a jej predstavitelia. Morálka skutočne nie je vecou spoločenskej dohody. A hitlerovské Nemecko je naozaj dobrým príkladom toho, kam vedie nerešpektovanie tejto pravdy.
                 

                 Meno Radičová v kázni vôbec nezaznelo. Biskup pripomenul svojím poslucháčom, že nie je ľahostajné, koho volíme. Pripomenul to, čo má byť známe každému katolíkovi, totiž že politická moc je podriadená Božiemu zákonu. Biskup povedal, že "žiaden politik nesmie povedať, že morálka je dohodou spoločnosti." Toto nemôže prekvapiť nikoho, kto pozná učenie Biblie a Katolíckej cirkvi.   Biskup tiež povedal, že " táto veta je hrozná a má strašné dôsledky." Ukázal, aké tieto dôsledky v histórii boli. Hitlerov a Stalinov režim, ktoré povýšili iné princípy nad prirodzený zákon, považujeme dnes za ukážku toho najhoršieho, čoho je ľudská spoločnosť schopná. Ale ak je morálka vecou dohody, podľa čoho súdime nacistov a komunistov? Ak je toto pravdou, norimberské procesy nie sú spravodlivosťou, ale justičnými vraždami.   Je pochopiteľné, že liberálov, ktorí Ivetu Radičovú podporujú, takéto prejavy iritujú. Radi by ju prezentovali ako kandidátku zastupujúcu kresťanských voličov. Dokonca s asistenciou niektorých kresťanov, napríklad z radov zmäteného KDH. Biskupi a kňazi, ktorí pripomínajú, ako má vyzerať kresťanský postoj v politike, rozbíjajú túto pekne namaľovanú fasádu.   Nemožno sa teda čudovať, že sa predstavitelia cirkvi stávajú terčom nevyberaných útokov. Je smutné, že sa k takýmto bulvárnym hlúpostiam pripojilo aj SME. Biskup Baláž sa zachoval ako pastier, v duchu povzbudenia svätého Pavla: "Hlásaj slovo, naliehaj vhod i nevhod, usvedčuj, karhaj a povzbudzuj... Lebo príde čas, keď neznesú zdravé učenie, ale nazháňajú si učiteľov podľa svojich chúťok, aby im šteklili uši." (2 Tim 4, 2-3)   Otec biskup, vďaka!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (305)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Ako veľmi Fico skrivil rovnú daň?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Od Husáka do Fica: Koniec revolúcií na Slovensku?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Prečo nemôžem podporiť požiadavky slovenských učiteľov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Zaostávajúca Cirkev ako nádej pre dnešnú dobu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Pussy Riot a hranice slobody slova
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Minárik ml.
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Minárik ml.
            
         
        pavolminarik.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ v Prahe.  

  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    168
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2657
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Ekonomické
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            O Amerike
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            G. Guareschi: Don Camillo
                                     
                                                                             
                                            G. Weigel: The Final Revolution
                                     
                                                                             
                                            P. Seewald: Světlo světa. Papež, církev a znamení doby
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vladimír Palko
                                     
                                                                             
                                            Zuzka Baranová
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Impulz
                                     
                                                                             
                                            Konzervatívny inštitút
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




