
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s Medveďom - Kuracia čorba a kavarma 

        
            
                                    28.2.2010
            o
            12:00
                        (upravené
                25.2.2010
                o
                11:56)
                        |
            Karma článku:
                11.77
            |
            Prečítané 
            7127-krát
                    
         
     
         
             

                 
                    Tak deti, zase kura. Dávno tu už nebolo, takže je na čase, vrátiť sa k tradícii uverejniť aspoň jeden kurací recept mesačne.   Nie, nebojte sa. To vám neurobíme. Tento jednoduchý, nádherne farebný bulharský obed sa mi však natoľko podaril a Medveď pel takú chválu, že mi to dáva smelosť dúfať, že rovnako poteším aj vás, aj keď len virtuálne. Navyše kavarmu, ako jedno z najtypickejších bulharských jedál (popri džuvečoch a musakách) som sľúbil uverejniť už pred dobrými dvomi rokmi a čorba je zas najznámejší typ bulharskej polievky. A samozrejme celého Balkánu...
                 

                 Kuracia čorba:   Keďže kura budeme potrebovať na obe jedlá, je ideálne na oba chody vybrať jedno poriadne veľké kura, z ktorého po uvarení malú časť dáme do čorby a zvyšok použijeme do kavarmy.   Na čorbu ďalej potrebujeme:   1-2 veľké hrubostenné farebné papriky, 2 rajčiny, 1 cibuľu, 1 mrkvu, 2 vajíčka, zväzok petržlenovej vňate, 1 malú smotanu, poriadnu hrsť pevnej cestoviny (mrvenica, široké rezance, slovenská ryža, vrtuľky a pod...), olej, celé aj mleté čierne korenie a soľ.      Kura dobre umyjeme a vložíme do väčšieho hrnca s posolenou vodou, niekoľkými guľôčkami čierneho korenia a očistenou veľkou cibuľou. Uvaríme ho do mäkka. Môžeme použiť tlakový hrniec. (45 - 50 min.)      Vývar použijeme na polievku. Kura po vychladnutí oberieme od kostí a cca 15 - 20 dkg z mäsa nakrájame na malé kúsky do polievky. Zvyšok odložíme do kavarmy. Cibuľu zjeme.   Papriky a rajčiny očistíme a nakrájame na malé kocôčky, mrkvu očistíme a nastrúhame nahrubo, vajíčka dobre vyšľaháme a vňať nasekáme najemno...      Na troche oleja dobre urestujeme nastrúhanú mrkvu. Bulhari by sa s tým nepárali, polievka však bude mať nádhernú farbu. Pridáme vývar, nakrájané papriky s rajčinami a po zovretí chvíľu povaríme.      Ďalej vsypeme vňať, čierne korenie, cestoviny a 5 minút varíme.      Kyslú smotanu dobre vymiešame s jednou naberačkou polievky, čo nám zabezpečí, aby sa nám smotana po naliatí do vriacej polievky nezrazila. Po opätovnom zovretí polievky so smotanou, vlejeme do nej miešajúc vyšľahané vajíčka a opäť necháme zovrieť. Tí z nás, ktorí sú frustrovaní z detských dôb a vajce im v polievke prekáža, použijú len vaječné žľtky, ktoré zmiešajú s kyslou smotanou pred naliatím do polievky.      Na záver do čorby nasypeme nakrájané kuracie mäso a premiešame, prípadne dosolíme.      Skvelá hustá kuracia čorba je na svete.      Podávame ju pokropenú niekoľkými kvapkami čerstvej citrónovej šťavy a posypanú čerstvou nasekanou petržlenovou vňaťou.      Kuracia kavarma:   Potrebujeme:   Na 4-6 porcií 2-3 veľké farebné hrubostenné papriky, varené kuracie mäso (cca 70 dkg - 1kg), 2 konzervy talianskych rajčín, alebo 3/4 kg čerstvých rajčín nakrájaných na kúsky, 1,5 kg póru, toľko vajec, koľko bude porcií kavarmy (dietári vynechajú), petržlenovú, alebo zelerovú (ešte lepšie) vňať, mleté čierne korenie, olej, saturejku (čubricu) a soľ.      Varené kuracie mäso nakrájame na väčšie kúsky, papriky podobne, pór nakrájame na kolieska a nebojíme sa pokrájať aj zelenšiu časť stonky. (listy nie...:)) Petržlenovú vňať nasekáme.      Pór dobre urestujeme na oleji, pridáme papriku, rajčiny z konzerv, ktoré podľa potreby poroztláčame, soľ, čierne korenie a saturejku (zarovnanú polievkovú lyžicu), prikryjeme a cca 10 minút podusíme. Následne vsypeme vňať, kuracie mäso a premiešame.      Ochutnáme a ak je potrebné dosolíme.      Deti, my sme si z Bulharska doviezli tzv. 'džuvetčetá'. Sú to krásne malé hlinené nádoby s pokrievkami a otvormi na odvod pary, v ktorých sa kavarmy a džuveče zapekajú po jednotlivých porciách. Chuť a vôňa  pokrmu z nich je neopísateľná. Ak také nemáme, môžeme použiť iné typy zapekacích nádob, či štandardne pekáč.      Nižšie vidíte, že sme použili ako džuvetčetá, aj väčšiu keramickú zapekaciu nádobu.      Zeleninovú zmes s kuracím mäsom naberieme do olejom vymastených zapekacích nádob a navrch každej porcie vklepneme po jednom surovom vajíčku. Pečieme v rúre zohriatej na 200 stupňov okolo 20 minút.      Ak máme džuvetčetá, na posledných 5 minút zapekania ich môžeme odokryť, aby nám porcie kavarmy zozlátli.      Deti čo vám budem...:) Podávame s čerstvým chlebom, alebo pečivom. Pokojne celozrnným.      Dobrú chuť.       tenjeho         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (45)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




