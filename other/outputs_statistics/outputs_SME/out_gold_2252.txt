

 "Aj so slaninkou?" spýta sa ma kamarátka. "Samozrejme, že so slaninou, bez nej sa to neráta. Musí jej byť veľa a halušky musia byť poliate aj masťou. Keď zhrešiť, tak poriadne." 
 Pri spomienke na tú lahôdku sa mi vybaví tvár mojej starej mamy, jej kolísavý krok, povinná zástera prehodená na šatách, hnedá stužka vo vlasoch, ktorá jej pridržiavala jemné zvlnené vlasy, úsmev ukrytý uprostred vrások a jej nekonečné rozprávanie. Bývala na Liptove, kde bola bryndza povestná svojou lahodnosťou. Halušky nám robievala na dvojako - v piatok (v pôstny deň) poliate len maslom, inokedy s vytopenou slaninkou. Občas nám ich posypala nadrobno nasekanou pažítkou a ak sa zastavil aj ujo a doniesol za plnú konvičku žinčice, gurmánsky zážitok bol dokonalý. 
 Neviem, prečo moje deti nemajú radi toto jedlo a na kopec neforemnej masy štedro posypanej horúcimi škvarkami (ako hovorievala moja starká) sa dívajú s dešpektom. Možno preto, že som im v detstve neporozprávala povesť o tom, ako si kráľa Mateja voľakedy haluškami uctili. Možno preto, že som nenosila pri ich varení zásteru, nevoňala pomarančovými šupkami a nemala záhradu s čerstvou pažítkou a pokrivenými jabloňami, ktoré rodili sypké a kyslé jablká. Možno sa im nikdy nespojili s čarovnými spomienkami na prázdninový pobyt u starých rodičov. 
 A možno by sa nič nezmenilo. Veď ktoré dieťa už dnes dá na nejaké rozprávky a povesti? To len ja som taký blázon. 
  A kto vie? Možno sú aj iní takí snílkovia ako ja... 
   
   
   

