
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Rôzne
                     
                 Vo volebnej miestnosti ma čakalo prekvapenie 

        
            
                                    12.6.2010
            o
            13:38
                        (upravené
                29.1.2012
                o
                22:54)
                        |
            Karma článku:
                12.45
            |
            Prečítané 
            3006-krát
                    
         
     
         
             

                 
                    Od rána som zaregistroval množstvo ľudí, ktorí išli okolo domu smerom ku škole a naspäť. Pripadalo mi to ako deň školského vyučovania, akurát, že žiaci nemajú aktovky a sú už dospelejší.
                 

                 Chvíli predpoludním som išiel odvoliť aj ja. Pri všetkých doterajších voľbách, volebná miestnosť zívala prázdnotou, okrem volebnej komisie sem tam nejaký volič, ale teraz som zostal riadne prekvapený.    Množstvo ľudí čakajúcich v troch radoch. Jeden na vydávanie voličských obálok a dva rady k stolíkom na úpravu hlasovacích lístkov. Vekové zloženie bolo od staručičkého deduška o paličke, ktorý s veľkou námahou a trasúcou rukou hodil obálku do urny cez o niečo mladších dôchodcov, ľudí strednom veku až po mladých ľudí vrátane prvovoličov. Oblečenie mali od decentného obleku, cez bežné oblečenie na tento horúci deň, až po oblečie na relax a šport. Nechýbali ani ľudia v extravagantnom oblečení s pirsingami všade kde sa dalo.    Takto rôznorodých ľudí vidieť spolu čakať v rade je niečo výnimočné. Je vidieť, že terajšie voľby zaujali všetky vekové skupiny a voliť prišlo nečakané veľa ľudí. Ak to bude takto pokračovať aj poobede, tak voličská účasť bude voči predpovediam nečakane vysoká.   K účastí na voľbách vyzývajú nielen média ako rozhlas, TV ale aj sociálne siete na internete.         Obr. 1. Informácia z FaceBooku - Hlavný baner držiaci sa na vrchu informuje o voľbách a počíta koľko ľudí z facebooku už odvolilo.     Obr. 2.  Informácia z Pokecu, ktorá sa objaví pri prihlásení užívateľa s výzvou a linkou na volebné spravodajstvo.        Záver:   Netrúfam si odhadnúť ako dopadnú voľby, ale jedna vec je úž istá. Tieto voľby vzbudili u voličov mimoriadne veľký záujem. Nakoniec niet sa čo čudovať. Na jednej strane je ponúkaný (dočasný) blahobyt na cenu zadlžovania sa a na druhej strane sú návrhy ako riešiť terajšiu situáciu bez zvyšovania štátneho dlhu. Uvidíme čo si voliči na Slovensku vyberú. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




