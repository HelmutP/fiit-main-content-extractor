
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            MO Plusko
                                        &gt;
                Akcie
                     
                 Cirkusanti na snehu 

        
            
                                    30.3.2010
            o
            16:38
                        (upravené
                6.10.2011
                o
                19:09)
                        |
            Karma článku:
                4.11
            |
            Prečítané 
            1019-krát
                    
         
     
         
             

                 
                    A ešte přišél aj Cirkus Plusko........    ....rútilo sa na nádejných cirkusantov z mailu už 2 týždne dopredu. Najprv Chvojnycký kurýr, potom aj rozhlas ... v Chvojnici zjavne všetci vedia, že cirkus tam už je. To iba my sme vyrazili až 11.3. A veru nám dve chvojnické obyvateľky, spolucestujúce, povedali, že cirkus u nich bol v piatok, v kulturáku. Zmeškali sme?
                 

                 
  
  Cez stanicu v Senici sa prehnala cirkusová úderka, rozdávajúca pozvánky a z Vrboviec s nami dokonca cestovala pôvabná pokladníčka, ktorá nám predala vstupenky ... nezmeškali!   Vítal nás  sneh, pľuvač ohňa, kúzelník, klaunka, ... a Alegria (v rodinných cirkusových kruhoch zvaná Alergia), ktorá sa stala našou hymno - znelkou počas najbližších troch a ešte mnohých ďalších dní.   Mysleli ste si, že cirkusanti majú cez zimu prázdniny? Maringotky sú niekde zazimované a všetci sú v civilizácii, so svojimi rodinami? Tak to ešte nepoznáte Cirkus Plusko! Cirkusanti telom i dušou, cez zimu splavujú, rybárčia magnetické písmenká, aby poskladali čo najlepší slogan pre svoj cirkus, skúšajú vlastné atrakcie, aby mohli opraviť kolotoče, autodróm, preveriť strelnicu, či dom hrôzy. Popri tom všetkom stíhajú aj športovať, cestovať okolo sveta a voliť nového principála. Kto by veril, že sme to všetko stihli vlastne za 2 dni???   A môj najväčší zážitok? Jedna veľká cirkusová rodina ... Človeče, 6-členné družstvá, za každý hod kockou treba obehnúť kolečko a splniť úlohu, aby ste mohli posunúť svojho panáčika. Všetci neustále behajú po vyšmýkanom okruhu, sneží. Nášmu družstvu sa podarilo hodiť 6-tku iba asi dvakrát, obiehame stále dookola za 2, 3, či 4 políčka. Ďaleko zaostávame za ostatnými. Prví sú už v cieli. Ale čo sa nestalo? Fanto sa rozhodol, že nám pomôže a obiehal naďalej s naším družstvom. A potom sa pridali ďalší a ďalší a ďalší. Dodnes verím, že by sme pôvodne do cieľa nedošli, nevládali sme a náskok bol veľký. Ale davová psychóza ti nedá a keď toľkí bežali za nás, bežali sme tiež. Až kým sme posledného panáčika nedostali domov. Úžasné!   Potom by ma už nemalo prekvapovať ako rýchlo sme na záver cirkusového sústredenia pochopili, že šifru vylúštime, iba keď sa všetci spojíme. Záverečná figúra, prekrásne video a potlesk. Všetci sme uspeli, všetci sme sa stali cirkusantmi. Niektorí si nacvičili visenie na lanách, iní klaunskú zostavu, trénovali sme rôzne žonglérske odrody a odvážlivci si hádzali pravú horiacu guľu. Skupina tanečníkov sa učila aj synchronizovaný thriller dance .....:)    Čo dodať? Určite si nenechajte ujsť nadchádzajúcu sezónu Cirkusu Plusko!    Gabča   P.S.: Ešte jedna súkromná špecialitka na záver. Cirkusanti sa v nedeľu vrátili do svojich bežných životov, prudko poznačení Alegriou, spoločným videom, fotkami a spomienkami. No ja som aj v bežnom živote klaunkou. Naozaj. S červeným nosom navštevujem deti v nemocnici, tak ako doteraz. Ale teraz až viem, že patrím do Cirkusu! ĎAKUJEM   Pozn.: Gabča píše o zážitkovej akcii Cirkus Plusko. Viac fotiek nájdeš TU alebo TU. Je libo video?:)

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        MO Plusko 
                                        
                                            Banda ukričaných detí na lúke! Paráda.
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        MO Plusko 
                                        
                                            Čokoľvek, čo spravíš, môže niekoho inšpirovať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        MO Plusko 
                                        
                                            Vyjadriť názor nahlas?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        MO Plusko 
                                        
                                            Kto dokáže otočiť kľúčom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        MO Plusko 
                                        
                                            O praskaní bublín
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: MO Plusko
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                MO Plusko
            
         
        plusko.blog.sme.sk (rss)
         
                                     
     
         Sme aktívni mladí ľudia a radi robíme akcie pre mladých. Našou víziou sú aktívni mladí ľudia otvorení novým podnetom s túžbou objavovať.  

 V priateľskej atmosfére preto rozvíjame potenciál mladých ľudí prostredníctvom metód zážitkovej pedagogiky. 

 Outdoor, zážitok, byť samým sebou - to sú slová, ktoré sme my. My, plusáci. 

   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1133
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Akcie
                        
                     
                                     
                        
                            Ľudia
                        
                     
                                     
                        
                            o Zážitku
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Gymnasion - časopis o zážitkovej pedagogike
                                     
                                                                             
                                            Fond her: 52 nejlepších her z akcií a kurzú - Instruktoři Brno
                                     
                                                                             
                                            Učení zážitkem a hrou - Franc, Zounková, Martin
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Right Said Fred - Stand Up (for the champions)
                                     
                                                                             
                                            Mike and the Mechanics - From the West Side to the East Side
                                     
                                                                             
                                            Israel Kamakawiwo&amp;#699;ole - Somewhere Over the Rainbow
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Hogno
                                     
                                                                             
                                            Pupek
                                     
                                                                             
                                            Rolo
                                     
                                                                             
                                            Ďuri
                                     
                                                                             
                                            Pospo
                                     
                                                                             
                                            Mon'
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ...o zážitok viac.
                                     
                                                                             
                                            Leto s Pluskom
                                     
                                                                             
                                            Video - emotívna ochutnávka zážitkovej akcie
                                     
                                                                             
                                            Adaptačné kurzy Pluska
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




