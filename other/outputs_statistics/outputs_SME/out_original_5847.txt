
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Simona Gulisová
                                        &gt;
                Úvahy
                     
                 Peniaze sú všetko a na vzhľade záleží 

        
            
                                    4.5.2010
            o
            17:35
                        |
            Karma článku:
                8.14
            |
            Prečítané 
            1886-krát
                    
         
     
         
             

                 
                    Keď na prelome tisícročia vybuchovali ohňostroje a na mostoch svietili gigantické číslice 2000, mali ich vymeniť práve za tento „slogan“. Možno aj vtedy by si sa ľudia smiali ako aj teraz , ale všetci v kútiku duše vieme, že je to trpká pravda
                 

                     Keď na prelome tisícročia vybuchovali ohňostroje a na mostoch svietili gigantické číslice 2000, mali ich vymeniť práve za tento „slogan". Možno aj vtedy by si sa ľudia smiali ako aj teraz , ale všetci v kútiku duše vieme, že je to trpká pravda. Reči ako „Zdravie si za peniaze nekúpiš" sú už v dnešnom svete pasé. Nie len na Slovensku, ale aj všade na svete platia zákony ulice- kto si viac priplatí, dostane kvalitnejšie. Kto si nepriplatí, zomrie na chrípku. O takýchto veciach sa stále hovorí so servítkou pred ústami, ale kto aspoň raz nezažil, že sa musel pánovi doktorovi líškať,aj keď len za zubný strojček pre syna. Mohli by sme takto pokračovať o nespočetných veciach okolo zdravia, zdravotníctva, skorumpovaných lekárov, ktorým treba šupnúť do vrecka zelenú stoeurovú bankovku, aby si poriadne vykonali svoju prácu. (Boh/Alah/hocikto žehnaj vínimkám). Keby sa pozrieme na fenomén „štastie" tiež zistíme, že aj šťastia sa dá kúpiť na kilá. Denno denne stretávam stovky ľudí, ktorí sa neustále sťažujú na prácu, výdavky, pracovnú dobu od nevidím do nevidím a v neposlednom rade mizerný plat. Kto viac zarába, zažije viac, je viac šťastný. Ak máš peniaze, nebudeš na smrteľnej posteli spomínať na hodiny odsedené za pásom, ale na all inclusive resort niekde na Hawai. Apropo, keď už sme pri tej láske, isto by mnohí oponovali, že toto je asi jediná nespoplatiteľná vec. Vytrénované zlatokopky by však nesúhlasili. Oni milujú peniaze a ich zas milujú ich plešatí, pupkatí manželia. Všetci sú spokojní.   S láskou je spätá veta: „Na vzhľade nezáleží". No prd makový. Nech sa prihlási jeden, kto sa bezhlavo zamiluje do niekoho duše. Takéto prípady sú už len z odelenia ženských románov a ,,úspešných" filmov Rosamunde Pilcherovej pre kategóriu 60+. Ak sa žena v dnešnej dobe nedokáže predať svojím vzhľadom, nikdy nedostane vyššiu pracovnú pozíciu, nikdy si ju nevšimne muž a nikdy s ňou nebude nikto viac, ako len dobrý kamarát. Verím, že každý jeden človek má v sebe niečo krásne, ale keď nezaujme na prvý krát, nikto to krásne nenájde.   A o tom to celé je, ak nemáš peniaze a nedokážeš vizuálne zaujať si v tomto svete stratený.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (205)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simona Gulisová 
                                        
                                            O tom, v čom je náš národ dobrý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simona Gulisová 
                                        
                                            Keď tancujú jedni, tancujú aj druhí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simona Gulisová 
                                        
                                            Alchymista
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simona Gulisová 
                                        
                                            Plyš
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Simona Gulisová 
                                        
                                            O nebi, o pekle alebo o niečom medzi tým
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Simona Gulisová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Simona Gulisová
            
         
        gulisova.blog.sme.sk (rss)
         
                                     
     
        "Som hlboko povrchná osoba." (Andy Warhol)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    912
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




