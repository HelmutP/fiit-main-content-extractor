
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Bohovicová
                                        &gt;
                Nezaradené
                     
                 Zodpovednosť za budúcnosť 

        
            
                                    6.5.2010
            o
            20:54
                        (upravené
                8.5.2010
                o
                4:50)
                        |
            Karma článku:
                6.95
            |
            Prečítané 
            460-krát
                    
         
     
         
             

                 
                    Zodpovednosť je slovo, ktoré sa vytratilo z povedomia a z myslí občanov Slovenska. Je však najmä v predvolebnom období potrebné uvedomiť si jednoduchú skutočnsť, a síce, že o tom, ako naše milované Slovensko vyzerá a vyzerať ešte dlho, dlho bude, rozhodujeme my! Len a len my!
                 

                 Zodpovednosť je slovo, ktoré sa vytratilo z povedomia a z myslí občanov Slovenska. Je však najmä v predvolebnom období potrebné uvedomiť si jednoduchú skutočnosť, a síce, že o tom, ako naše milované Slovensko vyzerá a vyzerať ešte dlho, dlho bude, ROZHODUJEME MY ! LEN A LEN MY!   V tom spočíva zodpovednasť, ktorá je možno väčšia ako si mnohí pripúšťajú a možno vôbec uvedomujú. Stačí sa pozrieť do ktorejkoľvek krčmy alebo na sobotňajší dedinský futbalový zápas. Desiatky a stovky našich spoluobčanov preberajú politiku, svorne "frflú" ako je tu zle a aký je ten či onen a aký malý dôchodok má ten-ktorý práve sa na danom mieste vyskytujúci dôchodca.   Pravda je taká, že ZA VŠETKO SI MOŽEME SAMI !   Prvoradou otázkou v tejto súvislosti je tá najhlavnejšia a najzávažnejšia: BOL/A SI VOLIŤ?   Odpoveď A: NIE.  - tak čo sa sťažuješ? Tým, že si nevhodil/a pred tri a pol rokom svoj volebný lístok, si umožnil/a vyhrať voľby a vládnuť koalícii na čele so stranou SMER - a kde sme teraz? Štátny dlh sa neustále zvyšuje, čo znamená že my, podotýkam PRACUJÚCI OBČANIA ho budeme musieť zaplatiť. V televízii počúvame samé nepravdy, polopravdy alebo nepočujeme, čo by sme mali počuť, no populistické a voličovnadháňajúce tipy a triky - tých je každodenne až až...počnúc zahmlievaním finančnej krízy (všade naokolo zúrila a besnila, len u nás zázračne dlho dlho nebola...), rozhadzovaním našich ťažko zarobených a poctivo daňovému úradu odvedených peňazí až po... (doplň). Štátny dlh totiž znamená, že naše milé Slovensko si niekde peniaze požičalo a my všetci ich budeme musieť vrátiť. Len pre Tvoju informáciu, milý nevolič, vývoj štátneho dlhu za posledné roky (prosím pozor, nemýliť si milióny a miliardy, pozorne čítať):   Dzurinda  Rast dlhov v Rokoch 2002 – 2006 2002 - 15,979 mld. € 2003 - 17,205 mld. € 2004 - 18,726 mld. € 2005 - 16,846 mld. € 2006 - 16,769 mld. € Nárast dlhu o 790 miliónov eur   Fico  Rast dlhov v Rokoch 2006 – 2010 2006 - 16,769 mld. € 2007 - 18,053 mld. € 2008 - 18,613 mld. € 2009 - 22,585 mld. € 2010 - 26,519* mld. €  (* Prognóza Štatistického úradu SR)   

   Nárast dlhu o 9,75 miliardy eur   zdroj:http://www.cas.sk/clanok/161605/vlada-dzurindu-a-fica-takto-za-4-roky-zadlzili-kazdeho-z-nas.html      Odpoveď B: ÁNO.  - tak čo sa sťažuješ. TY SI ROZHODOL. Zamyslel si sa nad tým, koho volíš a kam nasmeruješ svoju budúcnsť, budúcnosť svojich rodičov, prípadne aj starých rodičov, svojich detí, vnúčat, susedov, kamarátov,.....doplň koho chceš, lebo ZODPOVEDNOSŤ ZA KOHOKOĽVEK Z NÁS LEŽÍ AJ NA TVOJICH PLECIACH!   Chcem len dodať, že týmto článkom nikoho nenabádam k ničomu inému ako k tomu, aby sa zastavil, zamyslel a rozmyslel si, čo s nami bude ďalej. Záver si urobte, prosím, sami...najmä zo vzhliadnutých čísel, ktoré sú faktami, a budeme ich musieť raz vrátiť...      ...každý a všetko...   VOĽBY SÚ AJ TVOJOU MOŽNOSŤOU, AKO NIEČO ZMENIŤ!   VOĽBY SÚ AJ TVOJOU MOŽNOSŤOU, AKO POVEDAŤ SVOJ NáZOR!   VOĽBY SÚ AJ TVOJOU MOŽNOSŤOU, AKO ŽIŤ A NIE LEN PREŽIŤ!   NENECHAJ O SEBE ROZHODOVAŤ INÝCH!   ROZHODNI TY! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Bohovicová 
                                        
                                            Štatistiky rómskej kriminality
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Bohovicová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Bohovicová
            
         
        bohovicova.blog.sme.sk (rss)
         
                                     
     
        Som kto som a som to ja.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    355
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




