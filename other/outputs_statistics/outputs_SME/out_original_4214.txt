
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            štefan michalík
                                        &gt;
                Nezaradené
                     
                 Normálne zlato 

        
            
                                    9.4.2010
            o
            11:37
                        (upravené
                9.4.2010
                o
                11:44)
                        |
            Karma článku:
                2.00
            |
            Prečítané 
            243-krát
                    
         
     
         
             

                 
                    "Šesťdesiate roky boli lepšie ako päťdesiate a lepšie ako sedemdesiate - to je moja charakteristika 60. rokov,“ týmto výrokom Milana Lasicu  by sme najlepšie uviedli knihu Juraja Šeba, Zlaté 60. roky. Pri literárnom návrate o vyše 40 rokov späť, nás knižka doslova vtiahne svojim obsahom do starej, v mnohých oblastiach, v porovnaní s dneškom, originálnejšej  Bratislavy.
                 

                 
Titulná strana  jednej z kníh J.Šebazdroj: Internet
   Obsah knihy nám umožní vychutnať si minulosť, popretkávanú spomienkami autora na krčmičky a iné podniky Bratislavy, z ktorých mnohé už neexistujú, ale vďaka faktografickej presnosti rozprávania autora, si aj mladší čitateľ utvorí presnú predstavu, ako asi vypadali. Podobne aj život v tých časoch, ovplyvnený socializmom, ktorý pred vyše dvadsiatimi rokmi   odvial čas do nenávratna, sa vyznačoval jednotnosťou a krvopotným zháňaním vecí každodennej spotreby. Nám mladším pripadajú autorove spomienky o dlhých radoch na banány, či mäso, ako zlý sen… 60. roky  minulého storočia mali v sebe podľa autora neopakovateľný závan nádeje a  slobody, ktorý bol prerušený  brutalitou sovietskych tankov na vyše 20 rokov. Životu po okupácii v roku 1968 sa autor venuje vo voľnom pokračovaní knihy s názvom Normálne 70. roky. Či tieto boli rokmi s prívlastkom normálne, ťažko povedať. Faktom ostáva, že aj napriek nepriazni, ktorú  v spoločnosti narobila tzv. normalizácia, ľudia dokázali žiť svojim vlastným životom. Mali svoje radosti, starosti, uzatvárali sa do svojho sveta, ktorým sa stali buď ich panelákové byty, chalupy či záhradky. V knihe nájdeme tiež postavy a postavičky, ktoré akosi patria ku koloritu každého mesta, na ktoré ľudia s viac či menším úsmevom, či povzdychom spomínajú. Dá sa povedať, že ako „bígbíťák“ sa Šebo aj vo svojej druhej knihe venuje modlám  vtedajšieho hudobného světa, medzi ktoré patrili v tých rokoch : Doors, Sex pistols, ABBA, Black Sabath,  u nás Marika Gombitová, Varga, Ursiny, Elán, Karol Duchoň,  Modus a ďalší. Vo Vatikáne zvolili nového pápeža, Rusi s Američanmi podpísali dohody o odzbrojení v Helsinkách, o Charte 77 sa dozvedáme, až keď Gott a Pilarová podpísali Antichartu a väčšina obavateľstva počúva západné rozhlasové stanice, dnes to vyznieva dosť zvláštne, kvôli hudbe či správam.. Skutočne sa pracovalo iba na fuškách a kradlo sa či podplácalo. (nepripomína vám to niečo?) Vtedajšie deti vyrastali v panelákoch s vedomím, že v škole nemajú hovoriť to, čo počuli doma. Prioritou pre nich boli: nahrávky z burzy, plagát z Brava či  rifle z Tuzexu za bony od veksláka, počúvali Rádio Ö3 a na koncerty chodili do Budapešti. 60 km vzdialená Viedeň bola ďalej ako Ulanbátar. Namiesto socializmu s ľudskou tvárou tu bol "socializmus plného taniera". Začalo fungovať RVHP "(Radujme sa, Veseľme sa...H...o máme, Podeľme sa!). Humorný, ale, žiaľ, až príliš pravdivý pohľad na jedno desaťročie.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        štefan michalík 
                                        
                                            Diera v múre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        štefan michalík 
                                        
                                            Žltý diabol, žltý Boh…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        štefan michalík 
                                        
                                            Orwel ako vyšitý? Alebo: to sme chceli?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        štefan michalík 
                                        
                                            Kde sa podel zdravý rozum?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        štefan michalík 
                                        
                                            Nájdi svoj pokoj...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: štefan michalík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                štefan michalík
            
         
        stefanmichalik.blog.sme.sk (rss)
         
                                     
     
        človek, ktorý si dokáže priznať svoj omyl.
občas znechutený, ale inak vcelku spokojný
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    74
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    534
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            malostranské povídky
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ach, tá slovenčina...
                     
                                                         
                       Guido Deutsch, nepriateľ Slovenského štátu
                     
                                                         
                       Dvakrát do tej istej rieky...nevstúpim
                     
                                                         
                       Žltý diabol, žltý Boh…
                     
                                                         
                       Diera v múre
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




