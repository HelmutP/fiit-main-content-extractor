
 Teoreticky to nemalo byť zložité – veď podľa zákona o ochrane osobných údajov prevádzkovateľ IS s osobnými údajmi vedie tzv. evidenciu IS, ktorá obsahuje základné informácie o systéme a údajoch v ňom spracúvaných, a túto evidenciu je povinný sprístupniť každému, kto o to požiada. Od oslovených organizácií som žiadal poskytnutie tejto evidencie plus uvedenie počtu osôb (subjektov) ktorým sú údaje IS sprístupňované, takže teoreticky sa dala moja žiadosť vybaviť veľmi rýchlo – kópiou existujúcej evidencie a spočítaním používateľov IS. Pre presnosť je ešte potrebné spomenúť, že kvôli tomu počtu používateľov som žiadosť formuloval ako žiadosť podľa „infozákona“ (evidencia, vedená podľa zákona o ochrane osobných údajov, síce má položku „okruh subjektov, ktorým môžu byť údaje sprístupnené“, ale nevyžaduje sa uvedenie ich počtu).

  

Ako to už chodí, prax sa od teórie značne odlišovala. Hoci teoreticky by oslovené organizácie mali dávno viesť požadovanú evidenciu,  nebola výnimkou reakcia cca po týždni, ktorá spočívala v tom, že lehotu na poskytnutie požadovaných informácií si organizácia predĺžila kvôli „spracúvaniu  a odsúhlasovaniu požadovaných  materiálov“ či kvôli „preukázateľným technickým problémom spojeným s vyhľadávaním a sprístupňovaním požadovaných informácií“. Ďalej, poskytnutá odpoveď spravidla obsahovala iba niektoré položky evidencie a ani následné upozornenie na znenie zákona, ktoré predpisuje rozsah evidencie ako aj povinnosť jej sprístupnenia, nemuselo stačiť („ukážkový“ príklad spomínam v predchádzajúcom článku). Bežné bolo aj ignorovanie tej časti žiadosti, ktorá požadovala uvedenie počtu osôb, ktorým sú údaje sprístupňované.

  

Hoci sa mi (zatiaľ) nepodarilo dosiahnuť pôvodný cieľ, prieskum predsa len priniesol zaujímavé poznatky. Okrem viditeľného ignorovania ustanovení zákona o ochrane osobných údajov „národným operátorom pre eHealth“ (pozri predchádzajúci článok) je to napríklad aj rozpor medzi poskytnutými informáciami. Konkrétne – dve rezortné organizácie uvádzajú, že údaje zo svojho IS sprístupňujú aj Ministerstvu zdravotníctva SR, zatiaľ čo Ministerstvo v odpovedi na moju žiadosť tvrdí, že spracúva len osobné údaje vlastných zamestnancov na účely personálnej a mzdovej agendy. Takýto rozpor sa dá interpretovať aj tak, že na Ministerstve zdravotníctva sa pracuje s osobnými údajmi občanov, ktoré nie sú v evidencii IS vôbec podchytené. Na takú možnosť poukazuje aj skutočnosť, že z poskytnutých informácií nie je jasné, kde sa vlastne vyskytuje tzv. osobitná zdravotná dokumentácia (§19 ods. 4 zákona č. 576/2004 Z.z. o zdravotnej starostlivosti a službách súvisiacich s poskytovaním zdravotnej starostlivosti), ktorú má Ministerstvo viesť a uchovávať (§45 písm. p) zákona č. 576/2004 Z.z.). 

  

Za povšimnutie stojí aj celkový prístup organizácií rezortu zdravotníctva k poskytovaniu informácií o IS s osobnými údajmi, ktoré prevádzkujú. Nie som nejaký zberateľ takých informácií, ale môžem len nostalgicky spomínať na žiadosť o poskytnutie evidencie IS, adresovanú nemenovanej (ale inak dobre známej) banke - odpoveď za niekoľko dní a bez vytáčok poskytnuté kópie evidenčných listov IS, ktoré banka prevádzkovala. Keď to ide tam, prečo nie aj v rezorte zdravotníctva?

  

Vyzerá to tak, že osobné údaje súvisiace so zdravotnou starostlivosťou sa vyskytujú nielen v systémoch poskytovateľov zdravotnej starostlivosti a v zdravotných poisťovniach, ale aj v iných organizáciach rezortu zdravotníctva, pričom sú dôvody pochybovať o náležitej úrovni plnenia ustanovení zákona o ochrane osobných údajov zo strany týchto organizácií. Slovenský eHealth sa ešte len rozbieha a počet miest (systémov), kde sa budú vyskytovať osobné údaje občanov súvisiace so zdravotnou starostlivosťou zrejme len porastie ... a zdá sa, že prinajmenšom pre oblasť praktického uplatňovania zákona o ochrane osobných údajov bude eHealth postavený na základoch, ktoré nevzbudzujú veľkú dôveru (a ani nebadať snahu o získanie si dôvery v tomto smere). To nie je práve potešiteľný záver z prieskumu, ktorého pôvodný cieľ bol o niečom inom.

  

 
