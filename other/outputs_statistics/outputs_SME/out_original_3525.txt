
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Belko
                                        &gt;
                Tipy a triky
                     
                 Počet na základe podmienky v Excel (Kontingenčná tab. vs. funkcia) 

        
            
                                    29.3.2010
            o
            8:02
                        (upravené
                28.3.2010
                o
                22:59)
                        |
            Karma článku:
                10.58
            |
            Prečítané 
            5878-krát
                    
         
     
         
             

                 
                    Nedávno som dostal otázku ako v MS Excel spočítať položky na základe inej hodnoty. V MS Excel 2007/2010 je na to priamo vstavaná funkcia, ale kontingenčná tabuľka je lepšia.
                 

                 Otázka znela nasledovne: „Pán Belko, chcem Vás poprosiť o radu, ako postupovať, keď potrebujem z Excel. tabuľky, kde sú údaje o mužoch, ženách, ich príjmoch,... vytiahnuť počet napr. mužov, ktorí majú prijem nad určitú sumu, napr. 1.000,- aby ich nebolo potrebne fyzicky spočítavať? Ďakujem. Jarka"   Urobiť to môžete dvomi spôsobmi funkciou COUNTIFS alebo pomocou kontingenčnej tabuľky. Funkcia vyžaduje viac argumentov, aby ste dosiahli požadovaný výsledok. Kontingenčná tabuľka je omnoho pohodlnejšia a zobrazuje výrazne viac možností na analýzu. Funkcia ktorá by pre podobnú otázku ako položila čitateľka vyzerala takto =COUNTIFS(A:A;"&gt;25"; A:A;"&lt;35";B:B;"muž") a spočítavala by počet mužov vo veku 26 až 34 rokov, pretože nie je použité väčší/menší a rovný.   Vytvorenie funkcie a kontingenčnej tabuľky si môžete pozrieť v krátkom videu (ospravedlňte zhoršenú kvalitu zvuku).  
 
            
            
            
            
            
	   Ak by ste nepotrebovali členiť muži/ženy, tak by sa dalo uvažovať o použití funkcie COUNTIF v tomto tvare =COUNTIF(A:A;"&gt;25") - COUNTIF(A:A;"&lt;35") ale pozor, výsledok správny nebude. Tieto dve funkcie spočítajú všetky hodnoty väčšie ako 25 a nie len po hodnotu 35. Tiež spočítajú všetky menšie ako 35 až po najmenšiu hodnotu. Preto výsledok nebude rovnaký ako pri funkcii COUNTIFS. Detailnejšie sa budem vysvetleniu rozdielov venovať v inom videu na www.mstv.cz o niekoľko dní. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Máte radšej bežné školenie alebo webinár?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Analyzoval som podporovateľov R. Fica v Exceli 2013
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Microsoft vrátil do Outlooku 2013 funkciu, ktorú pôvodne odobral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Hromadne si zmeňte utajenie udalostí v Outlooku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Pripojenie pomocníka Office 2013 na internet (+video)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Belko
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Belko
            
         
        belko.blog.sme.sk (rss)
         
                        VIP
                             
     
        Môžete ho stretnúť ako lektora na počítačových školeniach, pri IT konzultáciách vo firmách, na letných terasách a v kaviarňach ako pozoruje dianie okolo seba, ale aj na potulkách po gréckych ostrovoch, pretože počítače nie sú jediné čo ho zaujíma.Ostrovné správy popisuje na osobnej stránke www.dovolenkar.sk Aktívne prispieva na svoj portál Tipy a triky v MS Office.. 
  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    344
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3490
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Tipy a triky MS Office 2013/20
                        
                     
                                     
                        
                            Externé tipy a triky MS Office
                        
                     
                                     
                        
                            SharePoint, spolupráca,Office3
                        
                     
                                     
                        
                            Tipy a triky
                        
                     
                                     
                        
                            Návody
                        
                     
                                     
                        
                            Stalo sa ...
                        
                     
                                     
                        
                            Office 2010/2013 Beta
                        
                     
                                     
                        
                            Microsoft KB články
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Portál Tipy-triky.sk v MS Office
                                     
                                                                             
                                            .
                                     
                                                                             
                                            Správa a optimalizácia siete a IT
                                     
                                                                             
                                            Dovolenkar.sk– dovolenkové informácie
                                     
                                                                             
                                            Windows User Group
                                     
                                                                             
                                            Google+ profil
                                     
                                                                             
                                            Jednoduché video návody
                                     
                                                                             
                                            letenky.dovolenkar.sk - LETENKY do celého sveta
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




