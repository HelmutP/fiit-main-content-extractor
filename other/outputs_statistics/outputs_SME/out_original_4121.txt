
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Bardiovská
                                        &gt;
                Nezaradené
                     
                 Slovensko na ceste za Gréckom 

        
            
                                    7.4.2010
            o
            20:16
                        (upravené
                7.4.2010
                o
                20:28)
                        |
            Karma článku:
                10.82
            |
            Prečítané 
            1150-krát
                    
         
     
         
             

                 
                    Slovensko a Grécko, dve rozdielne krajiny, ktoré majú veľa spoločného v oblasti politiky. V oblasti politiky preto, že obidve vlády vedú nezodpovednú politiku voči občanom. Ešte donedávna sme Grécko považovali za jednu z najrozvinutejších štátov v Európe a Európskej únii. Nuž dnes je vidno, že sme sa mýlili.
                 

                 Grécky dlh sa dnes pohybuje okolo 120 % HDP a tento dlh stále rastie. Pomaly, ale isto vedie tento štát k bankrotu. Tento stav bol spôsobený mrhaním verejných financií, sľubovaním nesplniteľných sľubov, zavádzanie nielen občanov, ale aj ostatných štátov  EÚ o stave hospodárenia s financiami. Dnes sa už nik nečuduje, že Grécko stratilo dôveru občanov EÚ, a pomaly stráca aj dôveru štátov EÚ. Občania Grécka nebrali do úvahy výstražné signály, ktoré bolo možné badať z politiky vlády. Rovnaký nezáujem badať aj v našom štáte... Občania prehliadajú výstražné signály, ktoré sa začali objavovať už pred rokom 2008, kedy štátny dlh Slovenska narástol z doterajšieho dlhu 30 % HDP na 60 % HDP. A tento dlh neustále rastie a bude rásť, každú sekundu narastá dlh Slovenska o 157,87 Eura. To znamená, že zatiaľ čo my tvrdo pracujeme, tak nás vláda stihne za tento čas zadĺžiť o 721,40 Eur (zdroj: www.zadlzili.sk), pre niektorých táto suma predstavuje jeden mesačný príjem a pre niektorých predstavuje sumu, ktorú ich mesačný príjem nedosahuje. Tento dlh sa teda začal zvyšovať už pred krízou. Príčina rastu dlhu je pritom rovnaká ako pri Grécku...mrkanie verejnými financiami, sľubovanie nesplniteľných sľubov, zavádzanie občanov a štátov EÚ dnešnou vládou.   Ak sa dnešnej vláde podarilo zvýšiť za necelé dva roky dlh z 30 % HDP na 60 % HDP, tak za ďalšie dva roky sa tento dlh  môže navýšiť na 90 - 120 % HDP. Je to alarmujúce, že Slováci rovnako ako Gréci nevidia, že ich vláda okato okráda, ešte jej aj budú tlieskať ako dobre to robí. Fakt, že Slovensko sa začalo enormne zadlžovať svedčí aj kritika Európskej komisie v Správe o verejných financiách na Slovensku zo 7. októbra 2009: „V niektorých členských štátoch eurozóny rovnako priaznivé hospodárske prostredie takisto umožnilo financovanie rýchleho rastu na účet hromadenia rozsiahlych deficitov bežného účtu (hlavne EL, ES, PT a CY, ale aj IE, MT, SI, SK), zatiaľ čo v iných členských štátoch sa hromadili stále vyššie prebytky na bežnom účte (DE, LU, AT, NL, FI)." Problém, ktorý nastáva po tomto zadlžovaní je, že dlh nezaplatí naša vláda, ale poskladáme sa naň my všetci, z našich výplat, resp. z odvodov, z daní, z dôchodkov. A čo je horšie na tento dlh sa budú musieť skladať aj budúce generácie, naše deti, ktorým tak pripravíme ešte mizernejší život, ako bol ten náš...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Bardiovská 
                                        
                                            Šetriť či nešetriť? A ak, tak na kom šetriť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Bardiovská 
                                        
                                            Klamstvo, klamstvo a zase klamstvo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Bardiovská 
                                        
                                            Fukuyama - Konec dějin a poslední člověk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Bardiovská 
                                        
                                            Bilancia dvoch vlád
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Bardiovská 
                                        
                                            Nad Tatrou sa blýska, hromy divo bijú....
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Bardiovská
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Bardiovská
            
         
        janabardiovska.blog.sme.sk (rss)
         
                                     
     
        Človek má tri cesty ako múdro konať. Najskôr premýšľaním: To je tá najušľachtilejšia. Druhá napodobovaním: To je tá najľahšia. Tretia skúsenosťou: To je tá najtvrdšia.(Konfucius)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    912
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




