
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivo Dudáš
                                        &gt;
                Nezaradené
                     
                 Reakcia na stanovisko ministerstva školstva k petícii učiteľov 

        
            
                                    17.5.2010
            o
            19:29
                        (upravené
                17.5.2010
                o
                23:16)
                        |
            Karma článku:
                11.91
            |
            Prečítané 
            4456-krát
                    
         
     
         
             

                 
                    4. mája 2010 zverejnilo Ministerstvo školstva Slovenskej republiky svoje stanovisko na Petíciu učiteľov a ich sympatizantov za prepracovanie systému kariérneho rastu pedagogických zamestnancov. Stanovisko nájdete na http://www.minedu.sk/index.php?lang=sk&amp;rootId=5999. Som jedným z autorov tejto petície, preto by som chcel na vyjadrenie MŠ SR zareagovať.  Ak mám stručne zhodnotiť stanovisko MŠ SR, tak ho v mnohých prípadoch považujem za zavádzajúce a scestné, pretože často nereaguje na jednotlivé body petície. Ak by mal niekto problém pochopiť, čo som tým myslel, tak som tie časti, ktoré podľa mňa nereagujú na jednotlivé body petície, aj podčiarkol. Moje reakcie sa teda týkajú iba tých častí stanoviska, ktoré sa skutočne vyjadrujú k jednotlivým bodom petície.
                 

                    1. bod petície: Nesúhlasím s tým, aby bolo vykonanie atestačných skúšok pedagogických zamestnancov podmienené získavaním kreditov a žiadam o zrušenie tejto podmienky.   Stanovisko MŠ SR k 1. bodu našej petície: Podľa doterajších predpisov predchádzalo aj vykonaniu I. alebo II. kvalifikačnej skúšky vzdelávanie, dokonca v rozsahu 100 hodín. Zámer zákona získavať kredity nadväzuje na predchádzajúci systém a jeho cieľom je motivovať pedagogických zamestnancov ku kontinuálnemu vzdelávaniu prostredníctvom kreditového príplatku. Umožňuje tzv. horizontálny rast získavaním a udržiavaním profesijných kompetencií pre výkon špecializovaných činností alebo činností vedúcich zamestnancov. Zároveň nahrádza povinnosť 5 alebo 10-ročnej praxe pre vykonanie I. alebo II. atestácie, čím umožňuje  rýchlejší postup do vyššej platovej triedy. Je reálne, že za cca 5 - 6 rokov môže pedagogický zamestnanec dosiahnuť II. atestáciu, kým doteraz to bolo najmenej 10 rokov.   Moja reakcia: Osobne som sa spýtal 5 učiteľov, ktorí absolvovali 1. kvalifikačnú skúšku pred prijatím zákona 317/2009, na ministerstvom spomínaných 100 hodín vzdelávania, ktoré vraj predchádzali 1. alebo 2. kvalifikačnej skúške. Ani jeden z týchto učiteľov nemusel absolvovať žiadne vzdelávanie. Spôsob získania 1. kvalifikačnej skúšky mi popísali všetci rovnako: metodické centrum vypísalo témy prác, ktoré museli uchádzači vypracovať a obhájiť. Po obhajobe získali 1. kvalifikačnú skúšku. Nemuseli teda absolvovať žiadne vzdelávanie. Rovnakú odpoveď som dostal aj na Metodickom centre v Košiciach, teda to môžem zovšeobecniť na všetkých učiteľov.   Taktiež je tu množstvo učiteľov, ktorí si mohli podľa starých pravidiel urobiť 1. alebo 2. kvalifikačnú skúšku už tento alebo budúci rok. Keďže však musia naháňať kredity, získanie 1. alebo 2. atestačnej skúšky sa im oddiali. Argumentovať pred takýmito ľuďmi tým, že nový zákon umožňuje rýchlejší postup do vyššej platovej triedy, je dosť smiešne, keďže im sa postup do vyššej platovej triedy práve vďaka novému zákonu odkladá.       2. bod petície: Žiadam, aby bol tzv. malý doktorát uznaný ako náhrada 1. atestačnej skúšky pedagogických zamestnancov.   Stanovisko MŠ SR k 2. bodu našej petície: Ustanovenie § 9 vyhlášky MŠ SR č. 41/1996 Z. z. o kvalifikačných predpokladoch pedagogických pracovníkov uznávalo niekoľko náhrad I. alebo II. kvalifikačnej skúšky, medzi ktorými bolo aj získanie akademického titulu „doktor". Získanie tohto titulu však nebolo viazané len na odbor výchova a vzdelávanie, takže učiteľom s I. kvalifikačnou skúškou sa mohol stať aj pedagogický zamestnanec, ktorý tento titul získal mimo svojho odboru. Zároveň treba povedať, že ako náhrada I. kvalifikačnej skúšky sa uznávalo doplnenie pedagogickej spôsobilosti alebo špeciálnopedagogickej spôsobilosti, čo bolo a je v skutočnosti získanie kvalifikačného predpokladu, aby činnosť pedagogického zamestnanca mohol vôbec vykonávať.   Zákonodarca sa z uvedeného dôvodu rozhodol brať do úvahy len také vzdelávanie, ktorým sa zvyšuje stupeň vzdelania. Takým vzdelávaním je doktorandské štúdium a získanie III. stupňa vysokoškolského vzdelania (PhD.), ale musí byť získané v príslušnom odbore a zamestnanec musí mať najmenej 6 rokov praxe v odbore. Takýto zamestnanec sa zaradí do kariérového stupňa zamestnanec s II. atestáciou.   Moja reakcia: Nikde v petícii sa nespomína, že by mali byť uznané tzv. „malé doktoráty" tým ľuďom, ktorí ich získali mimo svojho odboru. Tento bod je preto postavený trochu všeobecnejšie, aby sa o ňom mohlo ešte diskutovať. Podľa mňa je jednoducho nespravodlivé, ak sa zo dňa na deň zmenili podmienky pre mnohých učiteľov, ktorí si urobili „malý doktorát" v nádeji, že sa vyhnú 1. kvalifikačnej skúške a táto možnosť im bola zrazu odopretá. Tiež si myslím, že ak obháji učiteľ „malý doktorát" vo svojom odbore, prinesie mu to viac úžitku, ako absolvovanie často zbytočných a nekvalitných kurzov.       3. bod petície: Nesúhlasím s tým, aby bol kreditový príplatok vyplácaný iba počas doby platnosti kreditov, ale aby sa stal trvalou súčasťou tarifného platu pedagogických zamestnancov.   Stanovisko MŠ SR k 3. bodu našej petície: Kreditový príplatok predchádzajúci systém odmeňovania nepoznal. To znamená, ak by nebol prijatý zákon č. 317/2009 Z. z., nebol by ani kreditový príplatok. Pedagogickí zamestnanci by sa museli vzdelávať tak ako doteraz, ale bez príplatku.   Ak by rezort školstva pristúpil na požiadavku petície, aby sa stal trvalou súčasťou platu, kreditový príplatok by stratil svoj význam a opodstatnenie. Zamestnancovi by stačilo, povedzme, v priebehu 3 rokov absolvovať vzdelávanie, písanie odborných článkov alebo učebnice, získať 60 kreditov a do dôchodku by nemusel už na sebe pracovať, nakoľko príplatok by mu bol aj tak vyplácaný. Kreditový príplatok má predovšetkým motivačnú úlohu k celoživotnému vzdelávaniu pedagogických a odborných zamestnancov, čo je aj cieľom tohto zákona.   Moja reakcia: V prvom rade by si mali páni na ministerstve uvedomiť, že nebojujeme proti zákonu ako celku, ale proti jednotlivým negatívnym dopadom na zamestnancov školstva. Preto považujem úvodný odsek tohto stanoviska za úplne scestný. Nekritizujeme vznik kreditového príplatku! Kritizujeme jeho dĺžku platnosti!   Moja predstava o motivácii k celoživotnému vzdelávaniu má však trochu inú logiku, ako to prezentuje ministerstvo školstva. MŠ SR chce pedagogických a odborných zamestnancov motivovať tým, že im vezme už získaný príplatok, pokiaľ sa nebudú ďalej vzdelávať. Ja by som navrhoval, aby sme im dali napr. po siedmich rokoch možnosť ďalšieho vzdelávania a ďalšieho príplatku. Myslím, že ich to tiež bude motivovať k celoživotnému vzdelávaniu.       4. bod petície: Žiadam, aby bola výška kreditového príplatku pedagogických zamestnancov zvýšená nasledovným spôsobom: zo súčasných 6% na 10% za 30 kreditov a zo súčasných 12% na 20% za 60 kreditov.   Stanovisko MŠ SR k 4. bodu našej petície: Zákon č. 317/2009 Z. z. vychádza z ekonomických možností štátu. MŠ SR navrhlo výšku kreditového príplatku tak, aby príplatok nepresahoval rozdiel v platových triedach.   Moja reakcia: Vychádzam zo stanoviska mnohých politikov, že platy učiteľov sú poddimenzované, a zo sľubov, že s tým niečo urobia. Nech to dokážu skutkami. Ak by som ja bol na mieste ministra školstva a neodklepli by mi mnou požadovaný balík peňazí pre rezort školstva, podal by som demisiu a povedal by som ostatným členom vlády, nech si hľadajú iného blázna, ktorý bude len sľubovať, ale nič nesplní. Vyzývam všetkých úradníkov na MŠ SR! Otvorte už konečne oči! Hlavný problém školstva je nedostatok peňazí a zlé prerozdeľovanie tých, ktoré sú k dispozícií! Myslím tým hlavne normatívy, ktoré sa odvíjajú od počtu žiakov. Ospravedlňovať sa ekonomickými možnosťami štátu je smiešne, ak si vlády dávajú za jednu z priorít práve vzdelanie.   Ak je problém v tom, že MŠ SR nechce, aby výška kreditového príplatku presahovala rozdiel v platových triedach, navrhujem nasledovné riešenie, nad ktorým som ani nemusel dlho rozmýšľať: zvýšenie tarifných platov v jednotlivých triedach.       5. bod petície: Nesúhlasím s tým, aby boli kvalifikovaní začínajúci učitelia zaradení do 9. platovej triedy, a žiadam, aby sa dostali opäť do 10. platovej triedy.   Stanovisko MŠ SR k 5. bodu našej petície: Doterajší systém odmeňovania podľa zákona č. 553/2003 Z. z. bol nastavený tak, že sa odvíjal od najnáročnejších činností, ktoré zamestnanec vykonával. Na základe tohto systému sa do 12. platovej triedy mohol dostať len zamestnanec, ktorý vykonával činnosti v špeciálnej škole alebo v špeciálnom školskom zariadení. Taktiež bolo ťažko obhájiteľné, že pedagogický zamestnanec, ktorý získal I. kvalifikačnú skúšku, je zaradený do vyššej platovej triedy, lebo vykonáva náročnejšie činnosti, ako keď skúšku nemal, pričom bol učiteľom tej istej triedy a tých istých žiakov.   Nový systém odmeňovania zaviedol odmeňovanie podľa získaného stupňa vzdelania a dosiahnutého kariérového stupňa. Týmto systémom sa otvorili dvere k I. atestácii aj zamestnancom, ktorí získali nižší stupeň vzdelania ako vysokoškolské vzdelanie II. stupňa (učitelia MŠ, vychovávatelia, majstri odbornej výchovy, učitelia ZUŠ). Zároveň systém vytvoril zamestnancom motivačné prostredie pre vertikálny rast -  začínajúci - samostatný - prvoatestovaný - druhoatestovaný, nakoľko každý postup je spojený s rastom tarifného platu a následne rastom pohyblivých zložiek platu, pretože sa vypočítavajú z tarifného platu.   Moja reakcia: Nie náhodou je celé stanovisko MŠ SR podčiarknuté. Považujeme ho totiž za úplne scestné a vôbec nereaguje na 5. bod petície. Zrejme niekto nepochopil, prečo žiadame preradenie začínajúcich kvalifikovaných učiteľov z 9. do 10. platovej triedy. Skúsim to teda vysvetliť a pánom na MŠ SR odporúčam, aby čítali pomaly a pozorne. Dôvodom je to, že pred prijatím zákona bol hrubý plat kvalifikovaného učiteľa, ktorý začal pracovať v školstve, o 45 EUR vyšší, ako je to po prijatí zákona. Keby niekto nepochopil, tak to skúsim vysvetliť názornejšie. Kvalifikovaný začínajúci učiteľ si mohol každý mesiac pred prijatím zákona kúpiť cca o 38 bochníkov chleba viac, ako si bude môcť kúpiť po prijatí tohto zákona. Ak niekto nájde v stanovisku MŠ SR reakciu na tento problém, potrasiem mu pravicou.       6. bod petície: Nesúhlasím s tým, aby začínajúci učitelia museli ukončiť adaptačné vzdelávanie záverečným pohovorom a otvorenou hodinou pred skúšobnou komisiou.   Stanovisko MŠ SR k 6. bodu našej petície: Systém adaptačného vzdelávania nie je novinkou. V rámci predchádzajúceho systému sme ho v zmysle vyhlášky MŠ SR č. 42/1996 Z. z. nazývali uvádzanie do praxe, ktoré sa v zmysle ustanovenia § 4 ods. 4 ukončoval záverečným hodnotením pred trojčlennou komisiou. Adaptačné vzdelávanie nie je povinné len pre pedagogických zamestnancov, musia ho absolvovať aj štátni zamestnanci, zdravotníci, súdni úradníci či čakatelia prokuratúry.   Moja reakcia: Nikde v petícii netvrdíme, že systém adaptačného vzdelávania je novinkou. Naším cieľom nie je zrušiť systém adaptačného vzdelávania. Iba považujeme za zbytočné a dehonestujúce, aby začínajúci učiteľ musel ukončiť toto vzdelávanie záverečným pohovorom a otvorenou hodinou pred skúšobnou komisiou.   Je naozaj pravda, že aj pred prijatím nového zákona fungovalo ustanovenie, ktoré uvádza vo svojom stanovisku MŠ SR. Na základe tohto ustanovenia mala celý proces uvádzania do praxe odobriť trojčlenná komisia. Realita však bola taká, že takáto komisia na väčšine škôl nikdy nezasadala. Ale to samozrejme nemôžem dokázať. Osobne nepoznám jediného učiteľa, ktorý by musel pred prijatím nového zákona absolvovať na konci uvádzania do praxe záverečné hodnotenie pred trojčlennou komisiou. A to ich teda poznám dosť. Dodržiavanie tohto nariadenia podľa mojich informácií nekontrolovali ani školské inšpekcie. Tie si totiž pýtali iba potvrdenie o uvedení učiteľa do praxe, ktoré bolo podpísané uvádzajúcim učiteľom a riaditeľom školy. Takže ani školská inšpekcia nevyžadovala potvrdenie o rozhodnutí trojčlennej komisie, preto sa toto nariadenie v praxi nedodržiavalo.   V súčasnej dobe sú riaditelia povinní vytvoriť stanovisko uvádzacej komisie, ktoré bude obsahovať jej hodnotenie daného učiteľa. Keďže pôjde o stanovisko celej komisie, bude musieť byť podpísané všetkými jej členmi, a teda sa už v praxi nebude obchádzať.  Žiaľbohu sa budem opakovať, ale očividne je to potrebné. Záverečný pohovor a jedna otvorená hodina neprinesú objektívny pohľad na schopnosti daného učiteľa. Každý riaditeľ si počas prvého roku môže urobiť dokonalý obraz o kvalitách začínajúceho učiteľa bez toho, aby učiteľ absolvoval záverečný pohovor a otvorenú hodinu pred skúšobnou komisiou. Riaditeľ totiž u takéhoto učiteľa pravidelne hospituje a rovnako uňho hospituje uvádzajúci učiteľ. Zároveň má riaditeľ k dispozícii reakcie zo strany rodičov, žiakov a aj ostatných členov pedagogického zboru. Ak učiteľovi záverečný pohovor a otvorená hodina nedopadnú najlepšie a riaditeľ bude na základe celoročnej práce presvedčený o jeho kvalitách, tak dotyčný učiteľ neukončí úspešne adaptačné vzdelávanie???       Za najzavádzajúcejšiu časť celého stanoviska MŠ SR považujem jednoznačne záverečný odsek, ktorý znie nasledovne: Ak by ministerstvo školstva pristúpilo k uskutočneniu jednotlivých požiadaviek petície, premietlo by sa to konkrétnych krokov, akými sú:   - zrušenie kreditového príplatku,   - I. a II. kvalifikačnú skúšku by mohli vykonať len pedagogickí zamestnanci s vysokoškolským vzdelaním druhého stupňa, kariérový rast by mali zastavený všetci pedagogickí zamestnanci s nižším ako vysokoškolským vzdelaním II. stupňa a všetci odborní zamestnanci,   - neexistoval by príplatok uvádzajúcemu zamestnancovi,   - znovu by boli zamestnanci zaraďovaní do platových tried podľa najnáročnejšej činnosti,  - zamestnanci s I. a II. kvalifikačnou skúškou by boli zaradení najviac do 11. platovej triedy a do 12. platovej triedy by sa dostali len špeciálni pedagógovia a odborní zamestnanci.   Moja reakcia: Ani jeden z týchto bodov sa v petícii nespomína. Dôležité je, aby si ľudia na MŠ SR uvedomili fakt, že nebojujeme proti zákonu ako celku, ale proti tým bodom, ktoré priniesli zhoršenie. Sme za zachovanie dobrých myšlienok nového zákona, ktoré prinášajú pozitíva. Ak napr. vďaka novému zákonu získal uvádzajúci učiteľ príplatok, považujeme to za správny krok a nikde v petícii proti tomu nebojujeme. Ak ministerstvo tvrdí, že výsledkom prijatia našej petície do praxe bude napr. zrušenie príplatku uvádzajúcemu učiteľovi, tak ide buď o nepochopenie petície, alebo o zavádzanie verejnosti, alebo o úmyselné klamstvo. A rovnaký názor mám aj na všetky ostatné body záverečného zhodnotenia našej petície zo strany MŠ SR.   Na stanovisku MŠ SR ma teší jedna vec. Fakt, že sa k našej petícii oficiálne vyjadrili, znamená, že chápu jej váhu. A to je naozaj výborné znamenie. Je to podľa mňa dobrý začiatok na to, aby sa v školstve pohli veci k lepšiemu.   Podrobnejšie informácie o petícii sa dozviete na http://peticiaucitelov.szm.com/, prípadne si prečítajte naše blogy http://ivodudas.blog.sme.sk/c/225514/Peticia-ucitelov-za-prepracovanie-systemu-karierneho-rastu.html alebo http://banas.blog.sme.sk/c/225527/Ucitelia-dajte-najavo-svoj-nazor-podporte-peticiu-za.html. Ak by ste nás chceli priamo kontaktovať, môžete tak urobiť prostredníctvom mailovej adresy peticiaucitelov@gmail.com.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Dudáš 
                                        
                                            Ministerstvo školstva sa rozhodlo šetriť na platoch učiteľov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Dudáš 
                                        
                                            Ako vláda peniaze pre učiteľov hľadala!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Dudáš 
                                        
                                            Prečo by učitelia mali štrajkovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Dudáš 
                                        
                                            Výzva na podporu štrajku učiteľov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivo Dudáš 
                                        
                                            Petícia učiteľov za prepracovanie systému kariérneho rastu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivo Dudáš
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivo Dudáš
            
         
        ivodudas.blog.sme.sk (rss)
         
                                     
     
        Som mladý učiteľ, ktorý sa snaží žiť podľa hesla: Slová potešia, ale skutky presvedčia.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2866
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




