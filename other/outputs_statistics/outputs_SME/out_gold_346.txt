

 Najprv len tak pre ukážku. Nákup na nepečené Vianoce vyzeral takto.
 
 

 
 
Stále platí, že čím kvalitnejšia čokoláda tým lepšie. Vždy používame horkú čokoládu. Na čokoládu na varenie treba pri týchto receptoch úplne zabudnúť.
 
 
Čokoláda sa rozpúšťa vo vodnom kúpeli. Do väčšieho hrnca dáme vodu. Vložíme menší hrniec, do ktorého nalámeme čokoládu. Keď voda vo väčšom hrnci začne vrieť, mali by sme vodný kúpeľ odstaviť z ohňa. Rozpúšťajúcu sa čokoládu je dobré precízne vymiešať.
 
 

 
 
V mnohých receptoch je čokoláda pomiešaná s čokoládovo orieškovým krémom na spôsob Nutely. Vtedy postupujeme tak, že krém pridáme  až do roztopenej čokolády a poriadne vymiešame.
 
 

 
 
A poďme už na jednotlivé recepty
 
 
Domáce Ferrero Rocher
 
 
Potrebujeme dve čoko Tatranky , dve Liny, 100 g horkej čokolády, tri lyžice čokoládového krému a  arašidy.
 
 
 
 
 
Arašidy nalúpeme a pomelieme na stredne hrubom mlynčeku.
 
 

 
 
Do misy vložíme Tatranky a Liny a tĺčikom na mäso ich poriadne rozmlátime. Prisypeme za hrsť pomletých arašidov.
 
 
Vo vodnom kúpeli rozpustíme čokoládu a čokoládový krém.  Vylejeme to oblátkovo-orechovej hmoty, poriadne premiešame a necháme vychladnúť. 
 
 
 
 
 
Mokrými rukami vyformujeme guličky, ktoré obalíme v pomletých arašidoch. Necháme na chladnom mieste poriadne stuhnúť
 
 
Po stuhnutí naukladáme do papierových košíčkov a v krabici uložíme do chladu.
 
 
  
 
 
Čokoládové košíčky
 
 
Budeme potrebovať horkú čokoládu, dve tyčinky Ladové kaštany, dve lyžice čokoládového krému a mandle. Budeme sa trochu hrať na umelcov a k tomu potrebujeme ploché štetce a malé papierové košíčky. 
 
 
 
 
 
Vo vodnom kúpeli roztopíme horkú čokoládu. Papierové košíčky uložíme po dvoch do seba a štetcom ich zvnútra celé natrieme čokoládou.
 
 

 
 
Potrebujeme dosiahnuť, aby čokoládová hmota v košíčku bola dostatočnej hrúbky a bola pevná. Postupov som vyskúšal mnoho a najviac sa osvedčil tento. 
 
 
Košíčky natrieme čokoládou vždy tak päť až šesť kusov. Odložíme do mrazničky. Po stuhnutí natrieme druhú vrstvu a opäť odložíme do mrazničky. Opäť necháme stuhnúť a natrieme tretiu vrstvu. Po tom už uložíme košíčky do chladničky a necháme tuhnúť najmenej hodinu.
 
 
Potom veľmi opatrne odstránime z košíčkov papier a získame čokoládové košíčky na plnenie.
 
 
Plnku vyrobíme tak, že vo vodnom kúpeli rozpustíme Ladové Kaštany a čokoládový krém a necháme stuhnúť do konzistencie, ktorá sa dá ľahko tvarovať.
 
 
 
 
 
Hmotu preložíme do ozdobovacieho vrecúška s hviezdicovým zdobítkom a do každého košíčka vtlačíme ozdobne plnku. Do stredu zapichneme mandľu hrotom dole a necháme poriadne stuhnúť.
 
 

 
 
Po stuhnutí naukladáme do papierových košíčkov a v krabici uložíme do chladu.
 
 
  
 
 
Cukríky z gaštanového pyré
 
 
Ak si dokážete gaštanové pyré vyrobiť, spravte si ho sami. Ak nie kúpte si hotové mrazené. Na tieto cukríky budeme potrebovať horkú čokoládu, 200 gramov gaštanového pyré, marcipán a čokoládovú ryžu.
 
 

 
 
V mise zmiešame gaštanové pyré a rozpustenú horkú čokoládu, hmotu necháme vychladnúť, aby sa z nej dobre tvarovalo.
 
 

 
 
Vytvarujeme valčeky, ktoré z bokov obalíme v čokoládovej ryži. Z marcipánu vytvarujeme malé peniažteky, ktoré prilepíme na vrch valčekov. Necháme v chlade poriadne stuhnúť. 
 
 

 
 
Po stuhnutí naukladáme do papierových košíčkov a v krabici uložíme do chladu.
 
 

 
 
 
Domáce Mozartove gule
 
 
Budeme potrebovať jeden dezert Zlatý nugát, marcipán, horkú a mliečnu čokoládu.
 
 
 
 
Z cukríkov zlatý nugát v rukách vytvarujeme guličky.  Z marcipánu vytvarujeme placku a nugátovú guličku v nej obalíme.  
 
 

 
 
Vo vodnom kúpeli spolu rozpustíme jednu horkú a jednu mliečnu čokoládu. Každú guličku napichneme na špajľu a namočíme v rozpustenej čokoláde. Ak máme kuchynskú mriežku necháme na nej odkvapkať a stuhnúť. Ak nie musíme si pomôcť studeným tanierom. Po stuhnutí guličky zrežeme ostrým nožom a prebytočnú čokoládu okrájame. 
 
 
 
 
Uložíme do papierových košíčkov.
 
 
  
 
 
Niečo dobré zo zvyškov a karamelu
 
 
Potrebujeme jednu konzervu sladeného kondenzovaného mlieka, ktorú jeden a pol hodiny varíme v hrnci plnom vody a necháme ju vychladnúť. Všetky zvyšky čokolád, krémov a mletých orechov, ktoré nám zostali. Na ozdobu mandle. 
 
 

 
 
Všetky zbytky čokolád rostopíme vo vondnom kúpeli. Piškóty pomelieme na jemnom mlynčeku. Kondenzované mlieko vyklopíme do misy. Prisypeme orechy a piškóty a prilejeme rozpustenú čokoládu. 
 
 

 
 
Všetko spracujeme na hmotu a necháme vychladnúť aby z nej dobre tvarovalo. 
 
 

 
 
Vytvarujeme guličky, ktoré zvrchu trocha sploštíme a vtlačíme do nich mandľu. Necháme poriadne vychladnúť. 
 
 

 
 
Po stuhnutí naukladáme do papierových košíčkov a v krabici uložíme do chladu.  
 
 

 
 
Tak a je to hotové. Celé naše dvojdňové snaženie vyzerá takto.
 
 
 
 
 
V chladničke zabalené v potravinárskej fólii vydržia cukríky dva týždňe, alebo do prvého nájazdu barbarov.
 
 
Želáme vám všetkým krásne Vianoce. Nech sú plné splnených túžob a snov.
 
 
Danko a Roman. 
 
 
 
 

