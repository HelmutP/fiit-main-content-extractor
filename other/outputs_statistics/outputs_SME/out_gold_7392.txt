

  
 V roku 1997 sa vydal na sólovú drahú, ktorú odštartoval albumom „Petr Muk".   Gitarový zvuk definoval nové smerovanie. Nasledovali úspešné albumy „ Jizvy lásky" s hitmi Neusínej, Stoupám ti do hlavy, Tančíš sama, Zrcadlo. S dvojročným odstupom na trh prichádza album „Dotyky snu" /2002/. V roku 2004 pripravil album cover verzií „Oh L´Amour" skupiny Erasure. O rok neskôr v roku 2005 vydáva tretí radový album „Osud ve dlaních". Pre svojich verných fanúšikov pripravil výberovku Slunce / To nejlepší v roku 2007. Po pri vlastnej tvorbe bol veľmi dobrým muzikálovým spevákom. Účinkoval v niekoľkých veľmi úspešných projektoch Golem, Rusalka, Johanka z Arku, Galileo, Mona Lisa. 
 Tri pred tragickou smrťou prichádza na hudobný trh album „V bludišti dnu". Album je plný gitarovo rytmických skladieb /Nebe nehledej/, ale aj jemných tónov /Spolu/. Na svoje si prídu milovníci elektronických zvukov, ktoré dotvárajú atmosféru všetkých skladieb. V jedenástich skladbách, textoch sa striedajú nálady radosti, bolesti, ktoré spevák v posledných rokoch svojho života prežíval. Album vznikol v spolupráci s Ľudkom Fialom, Honzom Dvořákom s kapely Jerusalem. 
  
 Po 20 rokoch sa pre mňa skončil jeden veľký hudobný príbeh, ale odkaz zostáva naďalej „V bludišti dnu" 
 Jsem rád, že tu mužem spolu být 
 tu slanou kapku roku v dějinách 
 ta poušť ten mráz s tebou dá se snést 
   

