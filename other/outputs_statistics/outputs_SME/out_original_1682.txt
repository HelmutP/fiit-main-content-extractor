
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lýdia Koňárová
                                        &gt;
                Capri
                     
                 Šaman Axel Munthe z vily cisára Tibéria na Capri 

        
            
                                    26.4.2010
            o
            8:26
                        (upravené
                19.2.2011
                o
                13:45)
                        |
            Karma článku:
                9.50
            |
            Prečítané 
            1852-krát
                    
         
     
         
             

                 
                    Ak sa spýtate, kto bol Axel Martin Fredrik Munthe, každý si skôr spomenie na jeho sugestívnu Knihu o San Michele, príbeh o živote a smrti, ako na jeho filantropizmus, nekompromisný a priekopnícky boj proti týraniu zvierat a dlhoročnú prax lekára a psychiatra. Svoje jasnovidectvo, umenie čítať  myšlienky, krotiť divé šelmy, upokojovať agresívnych, psychicky  narušených ľudí, vyliečiť beznádejné prípady a počuť strieborné zvončeky škriatkov v brezovom lese  pripísal laponskej  pestúnke Lene.
                 

                        Talentovaný, citlivý a extravagantný Axel predbehol myšlienky zjednotenej Európy o celé storočie. Plynulo rozprával švédsky, anglicky, francúzsky a taliansky.               Narodil sa vo švédskom Oskarshamne posledný októbrový deň roku 1857 do rodiny so štyristoročnými flámskymi koreňmi.                Svoje jasnovidectvo, umenie čítať myšlienky, krotiť divé šelmy, upokojovať agresívnych, psychicky narušených ľudí, vyliečiť beznádejné prípady a počuť strieborné zvončeky škriatkov v brezovom lese pripísal laponskej pestúnke Lene.              Kojí ho dva roky a tajne mu šamansky kvapká do mlieka krv z havrana. Maličký Axel miluje vtáčiky a nosí si do postele vajíčka v nádeji, že v teple ich vysedí. Keď zaspí a všetky porozbíja, otec ho nemilosrdne zmláti. Podobne, ako pri úteku zbierky myší a hadov do kuchyne, kde na smrť vystrašili malú sestričku.               Ako desaťročný vylúpi hrob a prinesie si pod posteľ lebku ešte s chumáčom červených vlasov. Okrem bitky vyfasuje aj dvojdňovú tmavú samotku.                Zúfalý utečie v mraze z domu. Nikto ho nechápe. Nájdu ho spať v hlbokom lesnom snehu, opečiatkovanom stopami vlčej svorky. Neroztrhala ho.                Gynekológiu a pôrodníctvo študuje v Uppsale a Montpellier. Počas bezstarostných prázdnin s makarónmi a skvelým červeným vínom na Capri sa mu sníva sen, že si raz postaví vysoko na útese, ku ktorému vedie sedemstosedemdesiatsedem fenických schodov, dom so sfingou.               Nedočkavo túži po jeho splnení čo najskôr, vyčerpávajúco neoddychuje a štúdium na parížskej Sorbonne ukončí ako najmladší, dvadsaťtriročný absolvent.                Koncom toho istého roka 1880 sa žení s Ultimou Hornbergovou. Zoznámili sa počas štúdií. Bezdetné manželstvo sa do desiatich rokov rozpadne.                Je otvoreným zástancom eutanázie a v prípade bolestivých prípadov neváha často použiť uspávajúcu morfiovú injekciu.               Mimoriadne na neho zapôsobia pokusy slávneho profesora Jean-Martin Charcota s hypnózou v nemocnici Salpêtrière.               S veľkým úspechom ju zaradí spolu so sugesciou a telepatiou do svojej praxe. No odborné kruhy ho považujú za šarlatána.               Keď zistí, že profesorove pokusy v mene osobnej slávy menia bezbranne dôverčivých, chudobných ľudí na pokusné králiky a psychické trosky, postaví sa proti nemu.               Tvrdo za to zaplatí. Stratí bohatú klientelu. Tá sa desí hnevu pomstychtivého, samoľúbeho a intrigánsky diktátorského lekára.               Nevie zbohatnúť. Neposiela klientom účty. A peniaze tých, čo disciplinovane zaplatia sami, porozdáva chudobným sirotám. Bezplatne ošetruje prisťahovaleckú kolóniu švédskych umelcov a robotníkov.               Pacientky ho zbožňujú a poniektoré aj fanaticky špehujú. Žiarlivý bratanec jednej z nich ho dokonca doženie do súboja. Neznáša násilie, no keď sa vyzývateľ samoľúbo chváli, že denne cvične zostrelí zo desať lastovičiek, výzvu príjme a postrelí ho do pleca.                Nevie povedať nie. Svojej domácej, mŕtvolne bledej mamzel Agáty s dlhými čiernymi pazúrmi, sťaby egyptskej múmii z balzamovaného oka vypadla a ktorej úsmev mŕtveho Lazara, zo desať dní ležiaceho v hrobe, spoľahlivo vrhne do mdlôb každého hosťa, sa zbaví, až keď privedie domov malého chlapčeka.    Škandál ! To je určite jeho nemanželský syn ! Tiež má plavé vlasy a modré oči. Ona si svoje dobré meno nepokazí ! Spolu s ňou odišli aj puritánski klienti. No Axel plesá.               Chlapcovej mame zachránil pri pôrode život. Slobodná mladá anglická aristokratka zverí dieťa pestúnom. Nenapadne jej, že žena obuvníckeho alkoholika ho takmer utýra na smrť. Axel ho po troch rokoch vypátra v Bretónsku a podarí sa mu nájsť mamu. Dieťa však o krátky čas zomrie na tuberkulózu.               V romantických ruinách starovekej vily rímskeho cisára Tiberia a stredovekej kaplnky svätého Michala, s výhľadom na čarovný záliv a sopku Vezuv, si začne stavať v roku 1887 dom podľa vlastného nákresu. Je perfekcionista, aj trikrát múry zbúra a nanovo postaví, kým je spokojný.              Peniaze sa minú a on sa usadí v roku 1880 v Ríme v dome na Španielskom námestí, kde zomrel  Keats.                V roku 1892 sa stáva obľúbeným lekárom švédskej kráľovskej rodiny. Bronchitíde princeznej Viktórie z Bádenu sa venuje takmer štyridsať rokov, až do jej smrti v roku 1930.               Každý rok ju pozýva na pár mesiacov  na Capri. V kaplnke organizujú koncerty, ona hrá na klavíri. Chodia na spoločné prechádzky okolo ostrova a pomáha mu zozbierať peniaze na Monte Barbarossu. Domáci si šepkajú, že sú milenci.               V roku 1907 sa ožení s mladou anglickou aristokratkou Hildou Pennington - Mellor. Jej rodine patria dve sídla. Hellens v Herefosdshire, jedno z najstarších v Anglicku a Southside House zo sedemnásteho storočia v londýnskom Wimbledon Common.                V záhrade, v ktorej lord Byron odovzdal svoje dielo vydavateľovi Johnovi Murrayovi, odovzdá Axel rukopis Príbehu o San Michele jeho následníkovi a menovcovi.               Splodí dvoch synov, Petra a Malcolma. S nekonečnou láskou píše o chudobných deťoch v Paríži, Bretónsku, Ríme, Neapole, Messine a Anacapri, no svojich dvoch synov v knihe vôbec nespomenie. Vŕta mi v hlave, prečo ?                Manželke sa zdal príliš ľudový ? Bezprecedentne nedisciplinovaný ? Nechcela, aby jej kazil výchovu ? Alebo, aj keď nechcel, sa čoraz viac podobal na otca ? Zamĺknutého čudáka, ktorý s vlastnými synmi nevedel komunikovať ?                Tri roky po svadbe Axel venuje Hilde ako svadobný dar štrnásťizbové letné sídlo Stengården pri jazere Siljan v Dalarne, obľúbenom mieste svojej mladosti.        Hilda ho zariadi talianskym, anglickým a francúzskym nábytkom zo sedemnásteho, osemnásteho a devätnásteho storočia. Prejemnelú anglickú záhradu zasadí do dramaticko drsnej švédskej prírody. Spolu s chlapcami tu trávi leto. Po jej smrti v roku 1967 ho premenujú na Hildasholm.              Axel sa tam vracia až v neskorú jeseň. Evidentne ho teší, že sa stal na Anacapri malým zbožňovaným bohom. Na sviatosť Sant´Antonia hostí všetkých vo svojom dome.               Neznáša fotografovanie.  Zo steny kuchyne sa sedemnásťročný usmieva priamo do objektívu, srší šarmom, kypí energiou a bezstarostnosťou, je plný života.                O päťdesiat rokov neskôr sa odvracia bokom. Má studené a nešťastné oči. Akoby mu bol jedinou oporou len pes.               Sebazničujúco, riskantne, obetavo a bezplatne pomáha chorým v čase cholery v Neapole (1884) a raneným po zemetrasení a tsunami v sicílskej Messine (1908, šesťdesiattisíc obetí). Pri jeho hrôzostrašných opisoch ma bolí srdce spolu s ním.               Z kláštora "dei sepolti Vivi", ktorého mníšky radšej všetky zomreli na choleru, než aby porušili príkaz zakladateľky a opustili jeho múry, si odnáša obrubu studne, kedysi pohanský obetný oltár bohovi Mitrovi.              Sicíliu mu pripomína kovová posteľ zo šestnásteho storočia.                Vášnivo bojuje proti týraniu psov, sov, opíc a  vtáčikov. Trpí pri šibnutom, sadistickom zvyku, objavenom starými Grékmi a masovo rozšírenom Rimanmi, vypichovať rozžeravenou ihlou škovránkom, slávikom, žlnám, drozdom, peniciam, stehlíkom, červienkam, trasochvostom, sýkorkám, prepeliciam, hrdličkám a lastovičkám oči, aby svojim spevom prilákali do sietí ďalšie obete, predávané bezcitným žrútom vtáčích paštét alebo do klietok na rozptýlenie unudených  detí a žien.                Po dlhoročnom snažení odkúpi horu Monte Barbarossa, ich oddychovú zastávku pri severno – južnom sťahovaní a zmení ju na prírodnú rezerváciu.               Počas prvej svetovej vojny príjme britské občianstvo a ošetruje vojakov na bojisku. Píše knihu Červený kríž, Železný kríž.        Očná choroba mu ohrozí zrak. S krvácajúcim srdcom musí opustiť žiarivo slnečné svetlo Capri a utiahnuť sa do Švédska.               Celé noci nemôže spávať. Priateľ mu poradí liečiť sa písaním. V roku 1929 vydáva Príbeh o San Michele.                Kritici ju odsúdia, no má obrovský čitateľský úspech. Preložia ju do 45 jazykov (v Čechách zmenia jej názov na Knihu o živote a smrti, na Slovensku na Knihu o San Michele) a boduje medzi najlepšie predávanými knihami dvadsiateho storočia. V dejinách švédskej literatúry však o Munthem nenájdete ani mäkké f.                Operácia mu zachráni zrak a môže sa vrátiť do tieňa San Michele. Posledné roky života je hosťom švédskeho kráľa. Zomiera 11. februára 1949 ako 92-ročný. San Michele daruje švédskej vláde.                Syn Malcolm Pennington Mellor Munthe zdedí jeho altruizmus a odvahu. Počas druhej svetovej vojny sa ozve jeho škótsky pôvod a dá sa naverbovať k Highlanderom klanu Gordonovcov.               Potrebuje viac adrenalínu, prestúpi k špeciálnym výkonným jednotkám ako špión a maskovaný za starú dámu sabotážne vyhadzuje do luftu mosty na švédskom a nórskom území, okupovanom nacistami.                Organizuje škandinávsku odbojovú sieť pod názvom Red Horse, zachráni rodinu filozofa Benedetta Croce zo Sorrenta v otcovej vile na Capri a zúčastní sa vylodenia spojencov v Anziu. Je vyznamenaný Vojnovým krížom za odvahu.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (28)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Tajná láska poetky Wallady, poslednej omejadskej princeznej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Zo Smrtnej hory fúka na Trygve Gulbranssena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Pizza Clandestina s Vitom Corleone
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Ako Pavol Jantausch vzdelaním národné sebavedomie podporoval
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            De Syv Sostre, Sedem sestier
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lýdia Koňárová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lýdia Koňárová
            
         
        konarova.blog.sme.sk (rss)
         
                                     
     
        ochranca prírody a pamiatok
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    54
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3598
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Chansons
                        
                     
                                     
                        
                            Bretónsko
                        
                     
                                     
                        
                            Capri
                        
                     
                                     
                        
                            Korzika
                        
                     
                                     
                        
                            Normandia
                        
                     
                                     
                        
                            Nórsko
                        
                     
                                     
                        
                            Paris
                        
                     
                                     
                        
                            Prado
                        
                     
                                     
                        
                            Pro veritate cum caritate
                        
                     
                                     
                        
                            Sardínia
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Škótsko
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Thassos
                        
                     
                                     
                        
                            Wien
                        
                     
                                     
                        
                            Zámky na Loire
                        
                     
                                     
                        
                            P&amp;#234;le-m&amp;#234;le
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Granada, Boabdil a kráľovná Aisha
                                     
                                                                             
                                            Meč z Toleda, tajný sen každého kráľa
                                     
                                                                             
                                            Diego Velázquez - Los Borrachos
                                     
                                                                             
                                            Vodný výťah pre lode vo Falkirku
                                     
                                                                             
                                            Siracusa, jedno z najslávnejších miest starovekého Stredomoria
                                     
                                                                             
                                            Córdobsky emirát a kalifova mešita
                                     
                                                                             
                                            Zámok Cheverny a Mona Lisa v oranžérii
                                     
                                                                             
                                            Zámok Chambord a dvojité schodisko Leonarda da Vinciho
                                     
                                                                             
                                            Zámok Ussé a Kráska spiaca v lese
                                     
                                                                             
                                            Zámok Blois a prekliaty básnik François Villon I.
                                     
                                                                             
                                            Zámok Blois, kabinet s jedmi a kredenc II.
                                     
                                                                             
                                            Aliki a pristávacia dráha pre mimozemšťanov
                                     
                                                                             
                                            Florencia, Catherine de Medicis a zámok Louvre
                                     
                                                                             
                                            Bayeux, Odon a tapiséria kráľovnej Matildy
                                     
                                                                             
                                            Perníkové chalúpky v Kerascoete
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




