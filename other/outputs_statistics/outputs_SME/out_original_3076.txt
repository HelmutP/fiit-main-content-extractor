
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Sama Mareca príhody a skúsenos
                     
                 Ad: Samo a slepá starenka 

        
            
                                    22.3.2010
            o
            10:13
                        (upravené
                1.6.2010
                o
                13:40)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            3445-krát
                    
         
     
         
             

                 
                    Link na svoj posledný článok spolu s krátkym sprievodným textom som preposlal vedúcemu oddelenia sociálnej a krízovej intervencie mesta Banská Bystrica. Dnes som dostal odpoveď, ktorú si skrátenú môžete prečítať. Nechávam ju bez ďalšieho komentára, azda okrem toho, že pánovi Langsteinovi za ňu ďakujem. Oceňujem jej rýchlosť - mail som písal v piatok večer a dnes ráno som dostal reakciu. Rovnako oceňujem, že sa v prípade angažuje aj nad rámec svojich povinností.
                 

                 Prajem pekný deň pán Marec,   Podľa popisu a štýlu reči som identifikoval dotyčnú osobu ako pani Etelu Záslavovú, ktorá v minulosti bývala v Skubíne. Podľa jej vyjadrenia odkúpila dom od rodinného príslušníka Štefana Záslava za 30 tis. Sk, o čom nemá doklad a dotyčný už nežije. Predmetný dom je v dezolátnom stave rozpadnutý a nie je evidovaný na Katastrálnom úrade, čiže je nelegálny.   S pani Záslavovou som v kontakte spôsobom, že raz za týždeň príde za mnou na Oddelenie sociálnej a krízovej intervencie MsÚ a rozprávam jej o krokoch, ktoré pre ňu zabezpečujeme a čo má ona robiť. Dlhú dobu počas liečenia nohy sa jej poskytlo ubatovanie v nocľahárni "Večierka" Pod Urpínom 6, Banská Bystrica, ktorej zriaďovateľom je ÚzS SČK Banská Bystrica. Stadeto chodila za mnou, že tam nechce viac byť a z toho dôvodu sme jej vybavili adekvátne zariadenie s Diecéznou charitou - vedúcou zariadenia Domu pre núdznych, Tajovského 1, Banská Bystrica - Ľubicou Michalíkovou.   Toto zariadenie sme pri jeho budovaní obsahovo zamerali pre osoby bez prístrešia, ktoré sú v postproduktívnom veku a nemá im kto pomôcť pri zabezpečení ich práv a právom chránených záujmov (vybavenie dôchodku, umiestnenie v zariadení adekvátnom zdravotnému stravu a veku a pod.). V tomto zariadení sa aj menovaná zdržiavala aj keď mala konflikty s ostatnými klientmi hlavne z dôvodu nedodržiavania hygieny. Prostredníctvom Domu pre núdznych sa jej vybavuje posúdenie odkázanosti na sociálnu službu - na Banskobystrickom samosprávnom kraji, ktorý môže posúdiť klienta, či má nárok na umiestnenie v Domove sociálnych služieb alebo v inom type zariadenia. Momentálne podľa vyjadrenia charity sa tak deje.   Minulý týždeň som pani Záslavovej vybavil, aby sa na čas kým BBSK posúdi jej žiadosť, zdržiavala v zariadení Domu pre núdznych - hlavne preto, aby sme boli s ňou v kontakte. Toto nevyužila a podľa toho, čo píšete tam ani nebola. Dnes som si to overil v Dome pre núdznych a potvrdili mi, že sa tam nezdržiava. Takže sa rozhodla byť v rozpadnutom dome v ktorom nič nie je i napriek tomu, že sme jej vysvetľovali, že nemá právny nárok sa tam zdržiavať a môžu ju hocikedy z tade vyviesť. Ponuka Domu pre núdznych trvá i dnes. Zariadenia sociálnych služieb nemôžu nasilu niekoho držať v zariadení ak sám nechce. Pani Záslavová by sa mala v prvom rade riadiť odporúčaniami ľudí, ktorí sa jej snažia pomôcť a spolupracovať s nimi v záujme veci inak je sťažená adekvátna pomoc jej osobe.   Pani Záslavová mala štyri deti z ktorých Elena zomrela a Ľubica žije v okr. Poltár, Valéria vo Veľkom Krtíši a Ján v Modrom Kameni. Je na nej, či chce s nimi nadviazať kontakt a či jej niečo bráni v tom isť za deťmi. Paradoxom je, že my sa dlhodobo snažíme jej pomôcť a vybavujeme za ňu veci - od čoho sme aj tu - a ona túto pomoc odmieta prijať.  To, čo v jej živote je negatívne je to, že sa okolo nej "motajú" bezdomovci príživníci, ktorí vedia, že poberá dôchodok a oberú ju oň. Keby bola v zariadení, tak by sa táto hrozba minimalizovala. Toľko zatiaľ v danej veci.   S úctou Karol Langstein 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10790
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




