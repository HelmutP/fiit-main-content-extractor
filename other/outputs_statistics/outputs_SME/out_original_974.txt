
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Sýkora
                                        &gt;
                výplody strapatej hlavy
                     
                 Volal som Robovi Ficoóvi! 

        
            
                                    30.10.2008
            o
            6:44
                        |
            Karma článku:
                9.23
            |
            Prečítané 
            2896-krát
                    
         
     
         
             

                 
                    Som vcelku plachý človek, nesmelý pri nadväzovaní nových kontaktov. Ale neodolal som! Keď už mi Ficoó volá a hrali sme spolu futbal, tak snáď mu môžem zavolať aj ja. A najlepšie v nedeľu skoro ráno ako má vo zvyku aj on. Ale bola to chyba. Zdvihla manželka! Jej rozospatý hlas ma rozľútostil. Môj hnev sa miernil. Chcel som začať ostro: „Roboó, nesér ma!“, ale keď zdvihla ona, len som zahabkal: „Éhm, dobré ránko, prepáčte, že ruším. Je pán manžel doma?“
                 

                 
Shooty je skvelý...©SME, 27.10.2008, 20ta strana, inak www.shooty.skscanner v práci
      „Spí", odpovedala prekvapujúco pokojne. „Vrátil sa z ďalekého Vietnamu. Bol tam chudáčik takmer týždeň, musel tam jesť len ryžu a pomaly vlastnoručne budovať už zrušenú (rozumej zbytočnú, pozn. ivans) ambasádu. A navyše, vieš si, Ivan predstaviť", prešla do osobného tónu, čo ma prekvapilo. Že by si Robó uložil moje meno do mobilu? Fíha, veď ja sa pomaly stávam rodinným priateľom Ficoóvcov. „Tak vieš si predstaviť", pokračovala, „že on tam letel pravidelnou linkou, ani si nemohol na tej dlhej ceste topánky vyzuť a nôžky povystierať, aby náhodou niekomu nezasmrdelo, ó!"     Chcel som oponovať, že celé jeho komančovanie už smrdí veľmi. A nechutne. A že jeho „Ficoó Reisen" ma už nebaví, nech si cestuje po Vietname, Bielorusku, Kube, Líbyi a Venezuele za svoje, že ja to zo svojich daní odmietam platiť a že....Ale pani F. prerušila moje myšlienky náhlou zmenou tónu hlasu, kedy dosť rozhorčene zvolala: „A to som musela ešte so Silviou (rozumej Glendová, hovorkyňa, pozn. ivans) napísať text platenej správy, aby sa národ dozvedel, čo si môj Robkoó vytrpel na tej ceste komerčnou linkou. Div, že som tú správu nemusela platiť zo svojho. Ale Silvi mi povedala: „nechaj to tak, ja to celé zacvakám z rezervy šéfa, tá je bohatá a ľahučko sa prekračuje a míňa a míňa...To si národ nevšimne!" „Chichi, nie je tá Silvinka rozkošná?", položila rečnícku otázku pani Ficoóva a nečakala ani na odpoveď a mlela, mlela a mlela ďalej. Ach to bude ale účet, blyslo mi hlavou o 10 minút, kedy som si stihol s prepáčením aj odskočiť. Mal by som ju nejako prerušiť, asi zabudla, že volám ja a tiež asi aj s kým volá, nakoľko som v slúchadle začul otázku: „A čo myslíš, mám si k tomu novému červenému kostýmu kúpiť klobúčik, ako mala tá anglická kráľovná na bankete? Veď vieš ktorý, videla som jej ho v televízii." Skoro zo mňa vyhŕklo: Ty ťapa, veď ste tam mali ísť obaja osobne! Čo ste robili, keď ste tam nešli?!, ale len som niečo zašomral. Ale pani Ficoóva si odpovedala sama: „Vieš, mali sme tam ísť aj my, ale nemohla som sa odtrhnúť od upratovania a prala som. Aj žehlenia bolo veľa. Ani Róbertoóvi sa nechcelo, bolela ho hlava z II. piliera. A začala ho aj z prvého. Stále premýšľa akoby to celé vyriešil?! Hlavu mu ide chudáčikovi rozdrapiť, aj som mu musela dať Ibalgin 400ku. A aj ten Shooty, Schutz so Štulajterom  (kde je Sýkora, pozn. ivans) stále do neho rýpu! Že je majster demagógie, že chce znárodňovať, no uznaj Gitka(?!), je toto možné?! Veď on chce ľuďom len dobre!!"     Gitka?! Nechápal som! Veď ona asi zabudla s kým volá a myslí si, že trkoce so svojou priateľkou. Juj, zľakol som sa. Čo keď mi začne rozprávať nejaké rodinné pikošky?! Asi by som mal položiť. Ale keď som začul v diaľke Roboóv rozospatý hlas, ako sa pýta: „Kto volá? Luky? (rozumej Lukašenko, prezident Bieloruska,, pozn. ivans). S kým voláš, drahá?", tak som zbystril. Konečne je hore. Konečne mu to vytmavím. Poviem mu rovno do očí, že ako môže takto verejne klamať a zavádzať občanov. Že už sa príliš nafukuje bublina jeho preferencií a keď bude priveľká, môže ľahko prasknúť. Nech si uvedomí, že je predsedom vlády SR v roku 2008, že sme v NATO a EÚ, že nie je generálnym tajomníkom ÚV KSS a nie sú 80te roky. A že som ho nevolil a nikdy nebudem! A spolu so mnou ho nevolilo ďalších 74% oprávnených voličov! A že nesie zodpovednosť za celú republiku a že.....           Ale prúd mojich nevyslovených výčitiek prerušila pani Ficoóva, keď zašvitorila: „Drahá, už musím končiť. Róbertó sa už vyspinkal. Ideme leštiť valašku s hviezdou na čepeli. Veď vieš ktorú, už som ti minule u kaderníčky o nej hovorila. Daroval mu ju osobne Luky so slovami: „MOLODEC, ty charašo zbijaješ!" A dal mu BOZK na čelo. Fúj, to bolo nechutné. Už som sa zľakla, že mu dá „brežnevovku" na ústa?! No uznaj, nebolo by to hrozné?! Ale už musím naozaj končiť, zavolaj aj na budúce! Papá!" A kým stihla položiť, započul som ju ako volá do spálne: „Ty môj zbojníček, ty môj hrdina, čo bohatým berie a chudobným(rozumej svojím, pozn. ivans) dáva, už som hneď pri Tebe aj s tvojou obľúbenou valaškou....."     KLAP. Položila. Konečne ticho. Úff, to bolo! Vystrel som zmeravenú ruku a pošúchal stŕpnuté ucho. Hotovo! Nuž takto som ja v jednu nedeľu volal Robovi Ficoóvi!           Možno ste še pobavili! To by bolo fajn. Ale až taká sranda to nie je. Stačí si ozaj prečítať 20tu stranu SME z pondelka 27.10.2008 a ono nás ten humor prejde. V takom Bielorusku by som za takýto srandablog išiel do väzenia. V takom Rusku by som si ho ani nedovolil napísať. Ak sa milý bloger (rozumej, volič, občan) nespamätáme, tak v našom širokorozchodnom  Slovenrusku v roku 2011 nám dá FICO K.O. Neveríš?! Minsk aj Moskva sú „zatracene" blízko! Jedinou nádejou je, že snáď sa mlčiaca väčšina spoluobčanov preberie a knokaut mu zasadíme my! Je to v našich rukách a najmä hlavách! Ako hovorí na citovanej strane novín Lajos Grendel: 4444 0568789 26483 42457 122 61 8965  431 4           p.s. a keď už som z tej strany SME využil všetky podnety, tak aj článok V tieni finančnej krízy sa nás môže dotknúť, keď by došlo k najhoršiemu a vládna trojka by nás, svojich odporcov, nedajbože, pozatvárala  :-). Stratili by sme volebné právo do europarlamentu. Neviete ako dlho chodí pošta z Ilavy do Štrasburgu?           p.s.2  dúfam, že Petit Press a Sme nebudú robiť problémy s autorskými právami, cé v krúžku patrí Sme, ja som len jednu noc nemohol poriadne spať a tak som čítal a potom písal....       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Tri rohy jedenástka aneb Jarek a Róbert zažiarili v Prievidzi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Prečo by som nešiel do povstania ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Dobrovoľná brigáda v dnešnej dobe? Áno, podarila sa!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Nech sa dielo podarí.....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            42 do šťastia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Sýkora
            
         
        ivansykora.blog.sme.sk (rss)
         
                                     
     
        mierne unavený, ale vcelku optimistický štyridsiatnik, čo má rád prírodu, šport, čokoládu, modrú farbu a číslo 4 a riadi sa životným krédom "Rozhodujúci je človek"

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    128
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1573
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o behu a inom športovaní
                        
                     
                                     
                        
                            Berkat - pomoc
                        
                     
                                     
                        
                            fotoreportáž
                        
                     
                                     
                        
                            cesty/necesty
                        
                     
                                     
                        
                            výplody strapatej hlavy
                        
                     
                                     
                        
                            rozhovory
                        
                     
                                     
                        
                            Pozorovania a minipríbehy
                        
                     
                                     
                        
                            starinky
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            nohavica/plíhal to je klasika
                                     
                                                                             
                                            Pink Floyd kapela mladosti je stále skvelá
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nielen o behaní
                                     
                                                                             
                                            nielen o maratóne
                                     
                                                                             
                                            dobrá inšpirácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       7. ročník filmového festivalu Jeden svet v Prievidzi sa blíži
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




