
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marianna Slobodová
                                        &gt;
                Súkromné
                     
                 Chýba mi ten pocit nenávisti 

        
            
                                    2.4.2010
            o
            13:29
                        (upravené
                5.4.2010
                o
                9:44)
                        |
            Karma článku:
                3.04
            |
            Prečítané 
            712-krát
                    
         
     
         
             

                 
                    Dnes tak krásne svieti mesiac až sa mi chce zavýjať. Som nepoučiteľná. Aspoň ja určite. 
                 

                 Lámanie kostí, bolesť, ktorá vytláča z mysle všetko podstatné. Rozlieva sa po tele mysliac na to, že človek vie. Cíti. Razí si cestu až do hlavy a tu postojí. Nemá sa kam ponáhľať. Kruto sa obzerajúc zo strany na stanu. Nechce odísť. Prečo by aj. Tu je prosto...fajn.       Už asi zošaliem.       Mierim do ticha. Opatrne našľapujúc na tú správnu nohu idem k jedinej záchrane dnešnej noci. Sypký prášok, biely ako sneh vyšumí v kvapke vody a ja netrpezlivo čakám. Zatvorím oči a vychutnávam si tú bolesť. Lebo už o chvíľu odíde. Načas sa s ňou rozlúčim. Ešteže človek vie, na koho sa v dnešnej dobe môže spoľahnúť. On ma nikdy nesklamal. Môžete to povedať o niekom?   Nesmelý, tichý spoločník, ktorý sa nikdy nepýta. Len ti podá mokrú ruku, studenú ako ľad. Sadnem si s ním na kraj postele a rozumieme si bez slov.       Pomoc prišla v pravú chvíľu.       Zajtra začnem odznova. Dúfajúc, že šialenosť dnešku odíde a ja budem ako vymenená. Prosto iný človek. Kráčajúci bez toho, aby som krívala. Smejúci sa bez toho, aby som trpela. S hlavou čistou.   Jasne si spomínam na ten pocit, keď som si plnými dúškami vychutnávala čistý vzduch. V svojej podstate. Nespletený s čiastočkami smogu, ktorý je jeho verný spoločník. Vtedy môj mozog plne fungoval. Vzruchy cez jednotlivé synapsie plne fungujúc predávali  informáciu, ktorá sa dostala do cieľa. Bez toho, aby jej niekto hatil cestu. Chýba mi to. Ten zvláštny pocit bez bolesti. Bez obmedzení. Stratila som a až teraz viem čo.   Nie je to ani zdravie, ani pocit bez bolesti.       Slobodu!       Ísť kde chcem. Kedy mi to moje nohy dovolia a mozog uvíta. Rozhodovať sa s čistou mysľou. Nebrať ohľady. Môcť robiť veci všedné i nevšedné. Obuť si tenisky, ktoré si pamätajú aj taký deň, v ktorý som ich zaťažovala rovnomerne.   Bože, ako som len nenávidela behať. A teraz mi ešte aj ten pocit nenávisti chýba. Derie sa mi na jazyk tak často opakovaná veta, že snáď ani nie je možné ešte raz s ňou popísať papier. A predsa.       Každý chce to, čo nemá.       Niečo som stratila? Niečo mi chýba. Tvoja CHYBA. Mala si viac pamätať. Robiť to toľko krát, až by sa ti z toho zakrúti hlava. Vrylo by sa mi to do pamäti presnejšie. Mohla by som si spomenúť. Teraz len lovím v mysli spomienky a je mi ľúto, že sú také nejasné. Príliš bledé, aby boli čitateľné, príliš chabé, aby boli mojím potešením.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Slobodová 
                                        
                                            Fatamorgána lepšieho zajtrajšku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Slobodová 
                                        
                                            Bdejte, lebo nepoznáte dňa ani hodiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Slobodová 
                                        
                                            Vrany mám radšej tie "strakaté"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Slobodová 
                                        
                                            Dr. Jekyll Mr. Hyde v oblasti kuchárskeho umenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marianna Slobodová 
                                        
                                            Brat na ceste lodi poznania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marianna Slobodová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marianna Slobodová
            
         
        mariannaslobodova.blog.sme.sk (rss)
         
                                     
     
        Mením názor tak často, že pomaly predčím, čo do počtu, i ľudí, ktorí si menia ponožky raz za deň.
A milujem. Svoj nový foťák, svoju dcérku a svojho muža. Tí poslední dvaja nie sú noví. No je úžasné, že ich mám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1053
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Paul Hoffman-Ľavá ruka Boha
                                     
                                                                             
                                            Charlotte Bronteová-Jana Eyrová
                                     
                                                                             
                                            J.R.R.Tolkien-Silmarillion
                                     
                                                                             
                                            Stephenie Meyerová - Twilight
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Mama a ja
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




