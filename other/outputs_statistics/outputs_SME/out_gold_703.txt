


	
	
	
	
	

(Ladytron - Black Cat ) 
 
 
História britskej štvorice Ladytron  môže byť rozpísaná na niekoľko strán, no nie je ani problém ju zminimalizovať na niekoľko faktov, ktoré sa odohrali po stretnutí sa všetkých štyroch a následnej myšlienke stvoriť kapelu. Začínali elektropopovým albumom „604,“ ktorý bol síce hudobnými kritikmi pochválený a dostal pomerne dobré známky, no komerčne sa veľmi neuchytil. Ladytron boli našťastie trpezliví a pracovití a hneď po roku im vyšiel jeho nástupca „Light &amp; Magic.“ Na sile mu pridala skladba „Seventeen “ so známym klipom, kde je miestnosť rozdelená jednostranne priehľadným zrkadlom. Na jednej strane stojí skupina veľmi chladne a bezcitne spievajúca skladbu a pozerajúca sa na tancujúce sedemnástky, z ktorých postupným vylučovaním ostane tá vyvolená. Podarený a výstižný refrén „They only want you when you're seventeen /When you're twenty-one / You're no fun” (opakovaný 7x!) sa vryl do hláv tancachtivých poslucháčov a meno tejto štvorice sa stávalo čoraz známejším. Vytúžený prelom bol zhmotnený v tretej štúdiovke “Witching Hour,” ktorá vyčnievala spomedzi svojho žánra hlavne vďaka svojej originalite a množstvu koncertov, ktorým ju podporili. Po problémoch s nahrávaciou spoločnosťou, ktorá ohlásila krach, koncertovali približne 18 mesiacov, rozširovali základňu svojich fanúšikov, stávali sa známejšími a populárnejšími. 
 
Teraz sa píše rok 2008 a oni sa hlásia opäť k slovu. Stali sa súčasťou dnes veľmi populárnej vlny skladieb zdarma a do virtuálneho priestoru uvoľnili otvarák albumu “Velocifero,” po bulharsky naspievanú “Black Cat .” Po vypočutí sa hneď hudobným svetom rozšírila správa, že Ladytron sa vydali aj do vôd žánru gothic. Vizuálna prezentácia v tme pri sviečkach a tajomná atmosféra skladby tieto dohady silno potvrdzovali a po nahliadnutí k menám spolupracovníkov je hneď čosi jasnejšie. Okrem samotného kvarteta, ktoré sa výrazne podieľa aj na samotnej produkcii svojej hudby, sa v menách producenta zjavil aj Alessandro Cortini, známy hlavne vďaka spoluprácii s priekopníkom industrialu, Nine Inch Nails. Nič nie náhodou; Ladytron boli Trentom Reznorom vybraní ako predkapela k jeho európskemu turné. Určite aj Alessandro ovplyvnil trochu drsnejší a rockovejší zvuk nahrávky. Druhým spolupracovníkom je Vicarious Bliss, ktorého meno sa spája s duom Justice a DJ-om Mehdim. 
 
Samotný image akýchsi humanoidných androidov zahalených do čierneho sa neveľmi zmenil. Štvorica pôsobí naďalej veľmi temným a bezcitným dojmom, no tento pocit ostáva len pri sledovaní ich vizuálnej stránky. V textoch písaných ženskou dvojicou, z ktorých obe sú dokonalé femme fatale, sa nachádza okrem priam vecných konštatovaní niektorých javov, s ktorými sa stretávajú (uctievanie mladosti v staršej “Seventeen,” či závislosť od kokaínu v “Sugar”) vnášajú do skladieb dávku irónie (refrén “Ghosts,” ku ktorému sa ešte vrátim), či dokonca aj zážitky ovplyvnené citmi (hlavne druhá polovica “Black Cat”). Skupina sa tak síce stavia do role akýchsi spievajúcich strojov, ale po bližšom preniknutí cez závoj chladu, sú veci akési jasnejšie. 
 

	
	
	
	
	

(Ladytron - Deep Blue) 
 
Pri názve novinky “Velocifero,” na ktorý sa novinári často pýtajú, vždy vravia, že im išlo hlavne o zvuk tohto slova. Vo voľnom preklade to znamená dodávateľ, či nositeľ rýchlosti (bringer of speed). Už začiatok naznačuje, ktorým smerom sa bude všetko uberať. Tvrdší a energický beat dáva tušiť, že Ladytron sa v žiadnom prípade nemienia s poslucháčom len tak hrajkať, všetko naňho hodia rovno, bez toho, žeby zmäkkčovali, čo dosladzovali. Klincom celej skladby je nastupujúci vokál Miry Aroyo, ktorá upustila od spevu a rozhodla sa použiť oveľa sugestívnejší rečnícky prejav vo svojej rodnej bulharčine. Spojením skôr nekompromisného hudobného podkladu, textu v jazyku väčšine nezrozumiteľnom a v prípade Miry priam uhrančivom a spôsobu reči, pôsobí skladba priam nepreniknuteným a záhadným dojmom. Jedná sa o päť minút tápania v syntetickej elektronike a magickom hlase, ktorý môže pokojne vyslovovať zaklínadlo na poslucháča, bez toho, aby tušil, čo sa deje. Slová sú však skôr o spomienke na spoločný deň na útese, kde by sa adresát skladby premenil z mačky na orla a odletel niekam do neznáma. Ladytron sa posunuli a popri banalitách sa venujú aj pocitom. 
 
Na albume je tajomná “Black Cat” nasledovaná ľahšie stráviteľnou “Ghosts,” pre zmenu spievanou Helen Marnie. Na ktoromkoľvek portáli som sa dostal k recenzii na tento album, všade sa spomínal refrén “There's a ghost in me/ Who wants to say I'm sorry/ Doesn't mean I'm sorry” a skutočne niet divu. Do hudby akurátne pasujúce slová nazýva Pitchfork dokonca “enigmatic hook ,” teda akýmsi záhadným háčikom, či tajomnou pascou. Vokál, v ktorom dokáže spojiť opojnú sladkosť behom sekundy premeniteľnú na ostrý nôž, dopĺňajúci pozornosť si vynucujúce slová a ľahko a pútavo napísaná hudba zaujmú hneď na prvý krát. Ladytron urobili veľmi dobrý krok, keď sa rozhodli vypustiť “Ghosts” ako pilotný singel spolu so zaujímavým a troška nudným videom. Niektorí ho dokonca prirovnávajú k prácam Davida Lyncha, no v porovnaniach netreba zachádzať až takto priďaleko, keďže na ploche takmer piatich minúť sa toho nedá povedať až toľko, ako vo filme. 
 

	
	
	
	
	

 
 
Po týchto dvoch skladbách však našťastie neprichádza útlm a každá jedna by sa dala dlhšie analyzovať. V energickej nálade so štipkou irónie pokračuje aj “I'm Not Scared,” ku ktorej bola vo viacerých recenziách  pridaná aj nálepka takmer post-punkovej pesničky, no toto hranie sa so žánrovými škatuľkami je pomerne diskutabilné. Jedná sa o silnú synth-popovú vypaľovačku s ostrým rytmom a vďaka melodickosti aj dobrým hitovým potenciálom. Trend prepracovaného electro-popu zachováva aj “Runaway” pracujúca s náročnejšou témou úteku z domova s ľahkosťou. Slová o beznádeji a strachu podáva priam bezstarostne a otázky “Where you waking up today / My little runaway,” resp “Where you gonna sleep today / My little runaway” kladie akoby len mimochodom. Touto nevtieravosťou a uvoľnenosťou, akoby navracala nádej vo vyriešenie problem a v bezpečnosť, ktorá akoby čakala už za rohom. Tak tomu síce byť nemusí, Ladytron však tvoria hudbu, ktorá kladie otázky a jednoduché odpovede; nepasujú sa do role záchrancov sveta. Textové klišé celej skladby dodáva akúsi magickosť, ktorá nezachádza k pachuti nechutného gýča, na ktorom občas balansujú, no darí sa im nepadnúť doň. 
 
Aj nasledujúce skladby sa držia tempa a nálady, občas vtipné, potom mrazivé, vždy však energické a vyzývajúce od nečinnosti k pohybu. Z radu vytŕča “Kletva ” a to nielen kvôli druhýkrát a naposledy použitej bulharčine. Jedná sa o starý song  nasiaknutý folkom, ktorý sa objavil v jednom bulharskom detskom filme v sedemdesiatych rokoch. Skladba a hlavne prirodzenosť a veselosť jej prevedenia vytŕčajú z rady čisto elektronickej masy. Pozornosť si zaslúži aj silná a tanečne drsná “Deep Blue,” ktorá by sa mohla dočkať vynikajúcich remixov a zaradení do setov mnohých DJ-ov. Pre alternatívnejšie parkety je ako stvorená. Príjemnou bodkou za vyše piatimi desiatkami minút je “Versus” so slovnými hračkami vrcholiacimi opakovaním titulného slova do skomoleniny “versus, versus, versus, me versus me.” V pozadí sa objavuje popri širokých elektronických plochách aj gitara prinášajúca celkom osviežujúce upokojenie. 
 

	
	
	
	
	

 
 
Každému je isto jasné, že Ladytron nemienia svojou hudbou riešiť nejaké problémy, či načierať do hlbín spletitej ľudskej duše. Ostávajú skôr na jej povrchu a vyzývajú ju k zabudnutiu na trápenia a prechod k veselosti, tancu, zábave, odstráneniu nepríjemných pocitov a venovaniu sa príjemnejším veciam. Od chladnej bezcitnosti minulých albumov veľmi neodišli, no primiešali k nej aj drsnosť a chuť gotickej tajomnosti. S gýčmi striehnucími vrámci žánra za každým rohom sa vysporiadavajú s humorom sebe vlastným a ostávajú v hraniciach vtipu a dobrého vkusu. Často používanú repetitívnosť obracajú na účinnú zbraň pôsobiacu jednak satiricky, no na druhej strane nútiacu sa na ten ďalší a ďalší raz zamyslieť sa nadvýznamom a hľadať nové kombinácie zmyslov jednotlivých slov. Robiť dobrý synth-pop, bez toho, že by mal poslucháč pocit nechutnej ružovej cukrovej vaty, nie je až také ľahké. Kvarteto z Veľkej Británie sa však zaraďuje k tým, čo premieňajú mínusy na plusy a tvoria balík energie, tajomstva a (seba)irónie. Či bol electroclash mŕtvy, alebo nie, je veľmi diskutabilné, no tak, či tak, Ladytron zasahujúce aj dimenziu tohto žánru zvládajú na výbornú. Vhodné na letné noci na parkete.
 
 
 
 

