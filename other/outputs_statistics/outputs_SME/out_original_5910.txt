
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Minárik ml.
                                        &gt;
                Iné
                     
                 Ad Ján K. Balázs: Kríza cirkvi sa prehlbuje 

        
            
                                    5.5.2010
            o
            15:39
                        (upravené
                30.11.2010
                o
                9:05)
                        |
            Karma článku:
                10.18
            |
            Prečítané 
            3033-krát
                    
         
     
         
             

                 
                    Dnešný komentár Jána Krstiteľa Balázsa je v mnohom trefný. Autor si však neodpustil určité osobné poznámky, ktoré si zaslúžia reakciu.
                 

                 Ján K. Balázs veľmi správne pripomína pokusy o diskreditáciu banskobystrického biskupa Rudolfa Baláža za Mečiarovej vlády. Nepochybne vtedy biskup urobil chyby a vtedajšia moc sa snažila využiť jeho neobratnosť vo verejných veciach. Škoda, že Ján K. Balázs nepripomenie, ako túto biskupovu slabú stránku zneužila Iveta Radičová. Jej pokus prinútiť biskupa Rudolfa Baláža relativizovať vlastné vyjadrenia je podľa mňa rovnako pokusom o jeho diskreditáciu (ibaže by niekto považoval za hodnotné, ak si biskup nestojí za svojimi slovami). Selektívne videnie? Alebo je jemná a sofistikovaná diskreditácia menej zlá ako hrubá a násilná?   Pokiaľ ide o zmluvu o výhrade vo svedomí, nemyslím si, že "cirkev naplno dala priechod svojmu hlasu", ako tvrdí Ján K. Balázs. Naopak, cirkev zohrala veľmi nešťastnú rolu a svojím zapojením pomohla odporcom zmluvy (aj tu sa prejavila neskúsenosť a neobratnosť vo verejných veciach). Pripomeňme si, kto túto zmluvu potopil, a to aj za cenu klamstiev a porušovania dohôd. Bola to politická strana, ktorej Ján K. Balázs dáva svoju verejnú podporu, keď otvorene podporoval jej prezidentskú kandidátku a dnes podporuje jej volebnú líderku.   A obľúbená lož na záver - podľa Jána K. Balázsa si niektorí "nemorálnym spôsobom kopli do prezidentskej kandidátky, čím poslúžili ďalšiemu legitimizovaniu obrazu mečiarizmu. Ale keď na opačnej strane stojí vulgárny politik alebo populista so sociálnou demagógiou..." Pripomeňme si, že predstavitelia cirkvi, ktorí kritizovali Ivetu Radičovú, spravidla zároveň podporovali Františka Mikloška. Neviem o nikom v cirkvi, kto by kritizoval Ivetu Radičovú a zároveň otvorene podporil Gašparoviča. Je podľa Jána K. Balázsa František Mikloško vulgárny politik, atď.? Pripomeňme si, že vo voľbách boli viacerí kandidáti, aj keď sa nás ľudia z okolia Ivety Radičovej snažili od začiatku presvedčiť, že sú len dvaja. A Ján K. Balázs sa nás snaží presvedčiť, že upozorniť na problematické názory či životné postoje prezidentskej kandidátky je kopnutím a slúžením mečiarizmu. (Pri všetkej úcte, Mikloško vybojoval proti mečiarizmu určite viac súbojov ako Radičová, a to aj v prezidentskej kampani.)   Ján Krstiteľ Balázs otvoril dnešným komentárom dôležitú tému. Škoda, že mi dobrý dojem pokazil svojou potrebou presadzovať jeho osobnú agendu.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (157)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Ako veľmi Fico skrivil rovnú daň?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Od Husáka do Fica: Koniec revolúcií na Slovensku?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Prečo nemôžem podporiť požiadavky slovenských učiteľov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Zaostávajúca Cirkev ako nádej pre dnešnú dobu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Pussy Riot a hranice slobody slova
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Minárik ml.
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Minárik ml.
            
         
        pavolminarik.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ v Prahe.  

  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    168
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2657
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Ekonomické
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            O Amerike
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            G. Guareschi: Don Camillo
                                     
                                                                             
                                            G. Weigel: The Final Revolution
                                     
                                                                             
                                            P. Seewald: Světlo světa. Papež, církev a znamení doby
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vladimír Palko
                                     
                                                                             
                                            Zuzka Baranová
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Impulz
                                     
                                                                             
                                            Konzervatívny inštitút
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




