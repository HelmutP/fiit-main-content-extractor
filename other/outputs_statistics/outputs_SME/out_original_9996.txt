
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Iliaš
                                        &gt;
                Nezaradené
                     
                 Kto za to môže? 

        
            
                                    6.7.2010
            o
            14:33
                        |
            Karma článku:
                7.49
            |
            Prečítané 
            1489-krát
                    
         
     
         
             

                 
                    My Slováci sme takí – za všetko môže niekto iný. Najprv to boli Maďari, potom Česi, Rusi, Rómovia, inokedy zemepáni, imperialisti, banky či monopoly. Vždy nás niekto utláča alebo nám krivdí, ale my – biedny ľud sme vraj nevinný holubičí národ.
                 

                 Najnovší trend je obviňovanie Najvyššieho. Typický scenár z večerných televíznych správ: stane sa nešťastie (pohroma, autonehoda, alebo vražda). Po detailných záberoch nešťastných udalostí sa jeden z účastníkov zdôverí redaktorovi s ťažkým srdcom: ako to mohol Všemohúci dopustiť? Ako sa to mohlo stať tomu a tomu, kto nikdy nikomu neublížil?       To, že nešťastné udalosti väčšinou predchádzala chamtivosť, nevera, smilstvo, opilstvo, nezodpovednosť, arogancia atď, sa už v reportážach nezvýrazňuje. A diabla z nešťastia neobviňuje nikto. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (40)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Prečo sa cirkev pletie do politiky, alebo o dvoch ríšach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Cirkev a peniaze
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Zachránia nás elektromobily ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Indiáni, EÚ, inžinier a kedy sa minie ropa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Iliaš 
                                        
                                            Nie je to až také zlé
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Iliaš
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Iliaš
            
         
        igorilias.blog.sme.sk (rss)
         
                                     
     
        Kresťan, inžinier, otec... pracujem v oblasti tepelnej energetiky.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4822
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Energia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Obamov omyl s elektromobilmi
                                     
                                                                             
                                            CO2 a teploty: Popletená příčina a důsledek
                                     
                                                                             
                                            Sloboda prejavu - Je každý antiputinovec hrdina ?
                                     
                                                                             
                                            Stredovek - doba temna?
                                     
                                                                             
                                            Nie Euroválovu
                                     
                                                                             
                                            Národná tragédia
                                     
                                                                             
                                            Pán prezident, nezavadzajte!
                                     
                                                                             
                                            Rakúsko je dosť blízko
                                     
                                                                             
                                            Aby ste v Londýne niekoho neurazili!
                                     
                                                                             
                                            Svätí arizátori, orodujte za nás
                                     
                                                                             
                                            Konečne diskusia o odluke cirkví
                                     
                                                                             
                                            Ale čo ak sa naozaj neotepľuje?
                                     
                                                                             
                                            Podľa prezidenta som hrozba pre mier
                                     
                                                                             
                                            Pozeral som Modré z neba. Dvadsať sekúnd
                                     
                                                                             
                                            Keď pomoc nepomáha ale škodí
                                     
                                                                             
                                            Modly, ktorým sa klaniame
                                     
                                                                             
                                            Na Figeľa tlačia fanatickí kresťania
                                     
                                                                             
                                            Robotný Cigáň
                                     
                                                                             
                                            Čo mi vadí na súčasnej homo eufórii?
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio 7
                                     
                                                                             
                                            Experimental_FM
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pavel Kábrt
                                     
                                                                             
                                            Iný môj blog na Evanjelik.sk
                                     
                                                                             
                                            Miroslav Lettrich
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Evanjelik
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




