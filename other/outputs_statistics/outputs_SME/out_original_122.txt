
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Júlia Hubeňáková - Bianka
                                        &gt;
                Rodinné striebro
                     
                 Dráma pri grilovaní 

        
            
                                    26.8.2007
            o
            19:05
                        |
            Karma článku:
                9.82
            |
            Prečítané 
            2153-krát
                    
         
     
         
             

                 
                    Aj vy radi v sobotu grilujete? Keď už je všetko vypratané, vyvoňané, obriadené a treba už len tú povestnú vodku, ehm bodku za skvelým dňom? Králik (nemyslím Dávida) lepšie bude Zajko (Mirko, prepáč) alebo Zajaček (Branko, fakt ma to mrzí). Skúsim inak: Zajac (čo už tam po jednom exministrovi) a nejaké dobré kúsky bravčového mäska pokojne odpočívali v tajomstve kuchára už dva dni.
                 

                Gril aj pivo naberali správnu teplotu. Manžel v objatí svojej milovanej zástery začal veliť: „Natrhejte rajčata, papriku  a ňákej okurek, ty přines chleba a ty zas tohle a všichni dohromady...“ Kým deti (ne)poslušne plnili príkazy, ja som čušala ako voš pod chrastou a tvárila sa, že som kachlička, ups, to patrí inde. Sedela som na svoje plastovej stoličke a dočítavala detektívku od Agathy Christie. Už som takmer na 100 % vedela, kto bude vrahom, keď v tom mi -nič netušiacej- syn strčil do ruky kuchynský nôž a so slúchadlami na ušiach na mňa zreval: „Rež!“ Takmer som zaťala, ale pod nos mi podložil lopár a misku rajčín. Rezala som ako divá. Z mojich dlhých nechtov ostalo torzo a otvorená kniha od Agáthy bola postriekná červenou paradajkovou šťavou. O nejakú chvíľu, sa našou ulicou začala šíriť omamná vôňa grilovaného mäska. Toto je inak najhoršia časť celého procesu. Darmo by dakto vravel, že postoj chvíľa, si krásna. Slinné žľazy produkujú ostošesť, a tak raz má aj Bundáš z nás srandu, že sme zaslintaní. Bzučíme ako muchy okolo grilu, ale mohutné telo nášho ocka nám veľké nádeje neskýta. Občas trochu dymu do tváre, občas povel na otvorenie piva, viac nám strach nedovolí. Mňa sa to však netýka. Dnes vydržím. Detektívka vrcholí a aj keď mi dcéra tvrdila, že vraha neodhalím, som si takmer istá, že je to doktor Quimper, lebo je taký milý, bezúhonný, starostlivý. Tí sú najhorší. Agatha, nedobehneš ma. Mám to však ťažké, najmladší syn priamo predo mňa položil predné stehienko. Prepáč, Agatha, o chvíľu som späť. Postoj chvíľa, si krásna, teraz je ten moment. Všetci prežúvajú malé kúsky mäska, ktoré nám náš chlebodarca skromne vyčlenil z grilu. Zadné stehná, to najlepšie z králika, budú až potom, keď sa vraj trošku zasýtime. Pripomína mi to svadbu v Káne galilejskej. Aj tam najprv pili kyslé víno, až potom to dobré. Nuž čo, proti větru se chcát nedá, aj toto nás naučil ocko, poslušne chrúmkame malé kúsky, dostatočne dlho prežúvame, nie však kvôli racionálnemu stravovaniu, ale aby nám to dlhšie vydržalo. Bundáš s kŕčami v bruchu sedí pri nás a od radosti, že mu sem tam hodíme kostičku, žerie aj malé rajčiny. V tom zakvílila záchranka a za ňou hasiči. V meste bežné u nás čudo. Ako sme boli, tak sme vybehli pred bránku. Ja aj s kosťou medzi chýbajúcimi zubami. Zaujímavé, čo asi nikdy nepochopím, ale naša susedka zakaždým vie, čo sa stalo. Síce sa to nikdy nepotvrdí, ale obdivujem jej kreativitu. Agatha je pri  nej len také šumítko. Poochkali sme, pozalamovali rukami a pomaly sa vraciame: ta het ku grilu. Byť v tom momente kachličkou, bolo by mi lepšie. Bundáš ležal vyvalený v tráve ako paša a medzi svojimi prednými labami mal zadné stehno. Z králika, samozrejme. Keď sa manžel k nemu priblížil, iba zavrčal. Vtedy sme pochopili, ako sa asi, chudák Bundáš, cíti, keď grilujeme a on len čaká, kedy mu čo kto od stola hodí. Zahanbení sadáme k stolu a ja vyťahujem z chladničky železnú rezervu: špekáčky. Mimochodom, bol to doktor Quimper.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            O čistote srdca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Pán Matovič, hlbšie sa už klesnúť nedalo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Pán predseda, blahoželám
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Letný čas:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Júlia Hubeňáková - Bianka 
                                        
                                            Garancia vrátenia peňazí. To určite!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Júlia Hubeňáková - Bianka
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Júlia Hubeňáková - Bianka
            
         
        hubenakova.blog.sme.sk (rss)
         
                                     
     
        Radkova manželka a mama Miša, Veroniky a Raďa.










        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    352
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2465
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodinné striebro
                        
                     
                                     
                        
                            Bakalári
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Pokrútené paragrafy
                        
                     
                                     
                        
                            Moje bá-sničky
                        
                     
                                     
                        
                            Politika a ja
                        
                     
                                     
                        
                            Report
                        
                     
                                     
                        
                            Moje povedačky
                        
                     
                                     
                        
                            Evanjelium podľa...
                        
                     
                                     
                        
                            Fotofejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Rozhovor v časopise Zrno
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Predstavujem vám moje knihy
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Viem, viete, vedia :)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Verbum Domini
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




