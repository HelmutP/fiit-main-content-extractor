

 V histórii týchto programov ma najviac ohúrila osôbka, ktorá so Slovenskom nič spoločné nemá, ale určite našincom nie je neznáma. Volá sa Susan Boyle a hoci sa okrem iných aj Sharon Osbourne o nej vyjadrila veľmi nechutne a netaktne, množstvo ľudí na svete ju obdivuje. 
 Je možné, že práve Susan bude inšpiráciou pre mnohé naše talenty. Veď ukázala ľuďom, že ak má človek  dar od otca Nebeského, nikdy nemusí byť neskoro prezentovať sa  na pódiu. Len treba pozbierať odvahu a ničoho sa nebáť. Poznám veľa ľudí s výnimočnými schopnosťami, ktorí sa nikde nechcú pohnúť, lebo sa boja sklamania , neúspechu a  posmechu v médiách. Myslia si, že keď už nevyzerajú ako hviezdy zo Smotánky, nemôžu sa nikde ukázať. Pokladám to za obrovskú škodu. Keby sa nebáli vyliezť na svetlo,  potom by chabý priemer predvádzajúci sa často s veľkou  dávkou drzosti,musel zalieť do úzadia. 
 Držím palce všetkým, čo naozaj v sebe talent majú, aby sa im podarilo dosiahnuť úspech a hlavne, aby si z podobných rečí, aké sa nafúkaná Sharon nehanbí trepať do éteru, nič nerobili. 

