

 Vzhľadom na závažné nežiadúce účinky a zvýžený výskyt cievnej mozgovej príhody a infarktov EMEA uzavrela, že „prínosy sibutraminu už neprevyšujú možné riziká, a preto registrácia v celej EÚ má byť pozastavená ”.  
  
 Liek je u nás viazaný na lekársky predpis pod ochodnými názvami Reductil /Abbott/ a Lindaxa /Zentiva/. Patrí do skupiny látok, ktoré zvyšujú pocit  sýtosti a energetický výdaj organizmu. Tým pomáhal spolu s diétou a telesnou aktivitou znížiť telesnú hmotnosť pacienta.  
  
 Komisia pre humánne lieky (CHMP), ktorá pracuje pri EMEA rozhodla na základe dlho očakávaných záverov štúdie SCOUT /Sibutramin Cardiovascular Outcome Trial/ , v ktorej sledovali účinky lieku u starších pacientov, diabetikov so srdcovocievnymi komplikáciami. Aj na Slovensku bolo v štúdii zaradených  775 z celkového počtu 9000 pacientov.  
     
  
 Náhle stiahnutie sibutramínu z trhu krajín Európskej únie sa považuje všeobecne za prekvapivé. Podobne ako v októbri 2008 zastavenie predaja nádejného lieku na chudnutie ribonamant /ACOMPLIA/.  Na našom trhu tak zostáva jediné antiobezitikum orlistat /Alli/, ktorý zasahuje v organizme do činnosti enzýmov na štiepenie tukov. Nepôsobí na centrálnu nervovú sústavu, ale lokálne v tráviacom trakte, kde zabraňuje vstrebávaniu tukov z potravy. Liek je voľne predajný v lekárni aj bez lekárskeho predpisu.  
  
   

