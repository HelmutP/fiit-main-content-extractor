
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Múdry
                                        &gt;
                Slovensko moje rodné
                     
                 Draškovičovský kaštieľ 

        
            
                                    29.3.2010
            o
            8:00
                        (upravené
                28.3.2010
                o
                23:54)
                        |
            Karma článku:
                6.48
            |
            Prečítané 
            1741-krát
                    
         
     
         
             

                 
                    Diaľnica, ako veľmi dobre vieme,  je veľmi dobrá a ľudstvu prospešná vec. Zrýchľuje presun, čím šetrí náš drahocenný čas a ešte nám aj zarába. Na mýtnom. Aj minule. S akou radosťou a vnútorným uspokojením som zvieral volant. Kamión oproti, ja predbieham tri za sebou, ej bisťu, budú štedré vianočné darčeky. Sedím si v aute na zadku a popritom zarábam. Určite sa to odzrkadlí aj na mojom dôchodku. Ale nie o tom som chcel. Rýchlostná cesta má však aj jeden taký drobný nedostatok. Prekonáme pohodlne množstvo kilometrov a krajina si veselo beží oproti. A to je kameň úrazu. Na diaľnici sa totiž nedá vonkoncom kochať. Tá krajina je taká rýchla, že z okolitého územia vnímame iba biedny zlomok. Takže kto chce niečo nové objaviť, tak nech sníme nohu z plynu, vyhodí blinker a šupito presto fičí dolu z diaľnice.
                 

                 Tak aj robíme. Schádzame zo Šiferkinej cesty (strihala ona stuhu strihala s vtedajším...). Vidíte, aj ona videla figu borovú, lebo tvrdla iba na diaľnici. Prekľučkujeme sa Novým Mesto nad Váhom a komótne sa presunieme do osady Čachtice. Tak a sme v cieli. Viac informácií nájdete tu: Obec Čachtice         K Čachticiam prináleží aj hrad. Ale o tejto známej zrúcanine si porozprávame niekedy na budúce. Názorná ukážka:      Dnes sa budeme venovať kaštieľu.      Ako som sa dočítal, ide tu o budovu neskororenesančnú. Stráži ju jeden čierny a mierumilovný havo.      Betuš bola údajne fúria na druhú. Možno ste videli o nej film. Tak to potom viete viacej ako ja, lebo ja som ho ešte nezhliadol.      S rôznymi elementmi pochybnej povesti mali určite nedobré skúsenosti aj v minulosti. Nechce sa mi veriť, žeby takéto mreže na okná vyrobili kvôli záletníkom, čo si robili zálusk na miestne paničky.      Ja keď vidím exponát užitého umenia - karafu s vínom, tak som neudržateľný.      Budova je jednoposchodová. Čo je na prízemí, tak to je tajné. Na poschodí je muzeálna výstava. Ani ste sa nenazdali a už som Vás tam vtiahol.         O neslávne známej hradnej pani (A.B) sa toho popísalo kadečo. Údajne bola orientovaná na nepoškvrnené mladé dievky. Ale aj manžela mala. Aha akého fešáka.      Asi slobodná mamička aj so svojou ratolesťou.      Už sme skoro v polovičke, tak nasleduje prestávkový pohľad rovno z okna.      Súčasťou expozície je aj dobový nábytok.         Kamarát od vedľa - Stibor zo Stiboríc      A ešte raz „spanilá" deva Alžbeta.      Zbierku dopĺňajú rôzne sakrálne predmety,            ale aj veci svetské, dennodennej potreby:         Keď rinčia zbrane, múzy mlčia.      Hlavné je, aby sa každá poriadna dievka dostala pod čepiec.      Kedysi každodenná práca, dnes ukážka fortieľu našich predkov.      Prvopočiatky priemyselnej výroby v Uhorsku. Kolovrátok do každej rodiny.      Keramika nás sprevádza od nepamäti. Každý región, a to nielen u nás, má svoje typické krčahy, taniere.      No ale to je parádne fešákovské zrkadlisko. „Zrkadielko, zrkadielko, povedz že mi, kto..."      Slovenskí blogeri, združení v literárnom spolku Tatrín, sa raz dohodli, že takto to už ďalej bez spisovného jazyka nepôjde a preto sa tu v roku 1847 stretli.      Prihlásili sa k novému spisovnému jazyku. Tu sú hlavní vinníci.      Tak to bola malá exkurzia po priestoroch múzea v Čachticiach. Na samostatnú návštevu tejto obce je to trochu málo, preto prípadným záujemcom odporúčam skombinovať to s prehliadkou  Čachtického hradu. Ale o ňom bude samostatný článok, lebo si to zaslúži. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Bobuľka a gaštanko
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Uhrovec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Jankov vŕšok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Kolo, kolo mlynské
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Vodná šlapačka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Múdry
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Múdry
            
         
        igormudry.blog.sme.sk (rss)
         
                        VIP
                             
     
          

 Ešte stále strojár. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    176
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1888
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko moje rodné
                        
                     
                                     
                        
                            Nemecko moje prechodné
                        
                     
                                     
                        
                            Pokusy o veselosti
                        
                     
                                     
                        
                            Domáce úlohy
                        
                     
                                     
                        
                            Receptárium
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rolling Stones
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Je z čoho vyberať.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotky
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




