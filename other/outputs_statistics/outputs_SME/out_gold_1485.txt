

 Prešiel som všetky (aspoň si myslím, že všetky) zákony a vyhlášky zverejnené v zbierka zákonov, a nenašil som povinnosť, že keď svietia 
predne svetlá pre denné svietenie, tak nesmú svietiť zadné červené svetla. Ba naopak našiel som povinnosť, že pri zapnutých predných stretávacích svetlách musia svietiť zadné červené svetlá, s tým, že za nezníženej viditeľnosti sa namiesto predných stretávacích svetiel môžu použiť denné prevádzkové svietidlá. 

 Preto som sa obrátil na autora predmetného materiálu čo je spoločnosť TESTEK, s.r.o., ktorá na základe poverenia udeleného Ministerstvom dopravy, pôšt a telekomunikácií SR (MDPT SR) zabezpečuje vykonávanie činnostítechnickej služby technickej kontroly vozidiel podľa § 25 ods. 1 písm. b) zákona č. 725/2004 Z. z. o podmienkach prevádzky vozidiel v premávke na pozemných komunikáciách a o zmene a doplnení niektorých zákonov. 

 Z rozsiahlej a podrobnej odpovede vyberám: 

 V Európe existujú dva systémy schvaľovacích predpisov pre vozidlá. Prvým, starším, je práve systém EHK OSN. Okrem SR a ostatných členských štátov Európskej únie sú tieto predpisy aplikované aj vo zvyšku Európy a tiež v mnohých mimoeurópskych krajinách. Druhým je systém, ktorý o mnoho rokov neskôr začalo budovať Európske spoločenstvo (ES) pre členské štáty únie, na báze smerníc Európskeho parlamentu, Rady alebo Komisie. 
  

 Ak bolo vozidlo schválené podľa predpisu EHK č. 48, série zmien 04, a to mohlo byť najskôr od 7.8.2008, prosto muselo plniť podmienky v tomto predpise uvedené, okrem iného aj zákaz svietenia obrysovými svietidlami spolu s dennými prevádzkovými. 
  

 V oblasti svetiel platí dnes v systéme Európskej únie smernica 76/756/ES naposledy zmenená smernicou 2007/35/ES (transponovaná do nášho práva vyššie zmieneným nariadením vlády SR č. 144/2006 Z. z. v znení neskorších predpisov), ale tá zatiaľ prebrala len predpis EHK č. 48 vo vtedy platnej sérii zmien 03. To znamená, že vozidlo, ak bolo schvaľované v niektorej členskej krajine únie, nemuselo po 7.8.2008 nevyhnutne byť schválené v stave, ako vyžaduje EHK č. 48 série zmien 04, ale mohlo byť schválené tak, ako pripúšťa smernica 2007/35/ES, čiže podľa EHK č. 48 série zmien 03. 

Novšia pozmeňujúca smernica 
2008/89/ES (zatiaľ ešte nebola do nášho práva transponovaná), ktorá preberá 
predpis EHK č. 48 v sérii zmien 04, zamedzí členským štátom schvaľovať vozidlá kategórie M1 (osobné automobily) podľa starších predpisov až od 7.2.2011. 


 Práve preto je v informačnom dokumente, ktorý máme na našom webe, v zátvorke uvedený odkaz na schválenie podľa EHK č. 48 série zmien 04 od 7.8.2008, a nie na schválenie vo všeobecnom slova zmysle. 

 Marián Rybianský  
riaditeľ a konateľ TESTEK, s.r.o.   



 Záver: 
 Z uvedeného je zrejme, že na našich cestách počas 
nezníženej viditeľnosti môžu jazdiť vozidla, ktoré svietia len dennými svetlami a rovnako môžu jazdiť aj vozidla, ktoré svietia dennými a súčasne aj zadnými obrysovými (červenými) svetlami. Záleží od toho podľa akého predpisu bolo vozidlo homologizované. Do 7.2.2011 možno nadaľej homologizovať aj podľa starého predpisu. Zrejme táto právna variabilita je vysvetlením aj pomerne agresívnej ale slušnej diskusie v predchádzajúcom článku. 

 Ak sa nič nezmení, tak trend v legislatíve je taký, že u nových áut cez deň za nezníženej viditeľnosti sa bude svietiť len prednými svetlami pre denné svietenie alebo stretávacími svetlami spolu so zadnými červenými svetlami a osvetlením tabuľky s evidenčným číslom. 

