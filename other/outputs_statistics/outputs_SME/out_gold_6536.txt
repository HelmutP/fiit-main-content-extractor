

 Dítěti v člověku 
 V každém člověku je skryto dítě, to znamená výtvarný pud, a jeho nejmilejší hračkou není miniatura lodi, vypracovaná do nejmenších podrobností, ale ořechová skořápka s ptačím pírkem jako stěžněm a s křemínkem jako kapitánem. Toto dítě chce, aby mu bylo dovoleno spoluhrát, spolutvořit také v umění, a nechce být pouhým obdivujícím divákem. Neboť toto "dítě v člověku" je nesmrtelným tvúrcem v lidské duši... CHRISTIAN MORGENSTERN 
 Niektoré veci človek pochopí až na svojich deťoch. Prvýkrát som začal vnímať veci o skutočnej tvorivosti ktorá v nás drieme, keď náš Miško poukladal svoje obľúbené figúrky na zem a prehlásil, že figúrky "pozerajú Blka". Kto je to Blk sme sa dodnes nedozvedeli. 
  
 Potom prišla skladačka. Dobre si ju pozrite. Je to loď, ktorá má prednú časť, kabínky, okná, lodnú skrutku. A potom je náklaďák. Kolesá, kabína, vlečka. Ani fľaška s čajom nie je náhodný element. Všetko je umiestnené na svojom mieste. Aspoň podľa predstavivosti dvojročného chlapca. Po úmyselnom rozobraní sa kúsky dostali späť na svoje miesto. Poriadok musí byť. 
   
  
  Kde mizne úžasná predstavivosť a tvorivosť, ktorou v detstve každý oplýva? Každé dieťa absolútne verí, že je tvorivé a má nespútanú fantáziu, s ktorou lieta na mesiac v rakete vyrobenej z niekoľkých kociek a kúskov dreva. Spýtajte sa dospelých čo si myslia o svojej tvorivosti. Väčšina je presvedčená, že žiadnu nemá. Kde zmizla? Kedy zmizla? 
 Firmy zúfalo nastavujú latku pri prijímaní zamestnancov. Len si prezrite inzeráty. Dôležitá podmienka pre úspech - tvorivosť. Paradoxne sú firmy preplnené ľuďmi s prehnanými ambíciami, ale tvorivosť naďalej postrádajú. 
 Neostáva nám iné než pripustiť, že nesmierne tvorivý potenciál sa z človeka postupne vytratí počas školských rokov. Školské systémy nielen u nás sú nastavené a perfektne fungujú tak, že z človeka vyženú všetok tvorivý potenciál a natlačia doňho vedomosti. Najväčšie talenty minulého storočia rozvinuli svoj talent mimo školy - tam o nich nikto neuvažoval ako o talentoch. Nie je to divné? 
 Dnes vychovávajú učitelia deti pre svet a zamestnania, ktoré tu budú o dvadsať rokov. Kto z nás vie aký to bude svet? Rozvíja sa neuveriteľne rýchlo, za 10 posledných sa udialo viac ako v celom živote našich rodičov. Aké to budú profesie? Nikto nemá potuchy. 
 Školský systém, ktorý pripravuje deti na budúci svet, vychovával nás a našich rodičov. Dnešné deti ale nie sú ako my a naši rodičia. Sú iné, vnímavejšie, učia sa rýchlejšie a chápu abstraktné veci ktoré nám často dochádzajú až keď dospievame. Dnešné deti súčasný školský systém dávno prerástli. Nezajíma ich. Sú oveľa ďalej než učitelia, ktorí tiež nedržia krok. Kto pre tie decká tvorí školský systém, ktorý ich má pripraviť na ten svet, o ktorom nevieme aký bude? V akých rukách sú naše deti? 
 Je odpoveďou na ich potreby zavedenie vlasteneckých opatrení? Hymna, alkohol, nacionalizmus, rasizmus? Spolky politikov plniacich si svoju agendu? 

