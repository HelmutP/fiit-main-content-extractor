
 tzv. „(distributed) denial of service“ útoku – ak ho aj spomenuli, tak len tak mimochodom, bez hlbšej analýzy dôsledkov. Myslím, že je to chyba. Ono, voliť cez Internet sa od využívania obvyklých služieb poskytovaných prostredníctvom Internetu (e-shopy, stávkovanie a pod.) líši v dosť podstatnej charakteristike – príležitosť odovzdať svoj hlas je obmedzená len na istý časový interval, pričom zároveň nikomu nesmie byť odopreté právo voliť v ľubovoľnom okamihu daného časového úseku. Na zmarenie volieb v podstate postačí, ak je voličom zabránené uplatniť si svoje právo voliť vtedy, kedy sa rozhodnú (v rámci času, stanoveného na voľby, samozrejme). A teraz si predstavme voľby cez Internet, kedy trebárs 5 hodín pred riadnym ukončením niekto spustí (DDoS) útok, ktorý voličom znemožní odovzdať svoj hlas... Dobre pripravený DDoS útok nie je jednoduché „odstaviť“, v každom prípade to môže trvať dosť dlho. Evidentne časti voličov bude znemožnené odovzdať svoj hlas, čiže voľby by nemali byť uznané za platné...  Posunúť v takom prípade čas dokedy je možné hlasovať? Ak by sme aj akceptovali že je možné „meniť pravidlá“ počas volieb, nedá sa vylúčiť, že sa útok opäť zopakuje, prípadne aj viackrát...

  

Zdá sa, že jednoduché prenesenie zaužívaného spôsobu volieb – teda stanovenie časového úseku, počas ktorého má každý oprávnený volič možnosť podľa vlastného uváženia využiť svoje hlasovacie právo – do prostredia Internetu prináša problémy, ktoré súvisia s menšou náročnosťou marenia volieb, ako to je v „papierovom“ prípade.
Domnievam sa, že voľby cez Internet by mali byť aktuálne až vtedy, keď sa (právnikom, lebo ide o korektnú interpretáciu toho, ako vlastne chápať naplnenie práva voliť) podarí rozlúsknuť tento oriešok. Inak by bolo škoda venovať úsilie a prostriedky na realizáciu volieb, ktoré by bolo až príliš ľahké zmariť.

  

P.S. Pre navrhované riešenie typu "voľby cez Internet budú len ako doplnková možnosť, vždy bude možné ísť voliť do volebnej miestnosti klasickým spôsobom" by som rád videl rozumné argumenty pre zavedenie takého doplnku - úspora celkových nákladov na voľby medzi nimi asi nebude...

  
 
