

 1.    IRÓNIA: Z politika, ktorému je z nás do plaču, je nám už roky do smiechu. 
 2.    STAVITEĽSTVO: Za Fica sa toho síce postavilo veľa, ale taký dobrý stavbár, aby dokázal vybudovať vládu, nie je. 
 3.    MÉDIÁ: Za neúspech politiky nikdy nemôže politika samotná, ale média. 
 4.    ÚKRYT: Skrytý volič Vladimíra Mečiara je ukrytý za epitafom. 
 5.    TELEVÍZIA: Voľby zrejme vyhráva televízia. V prípade Csákyho ich prehrala. 
 6.    METAMORFÓZA: Virtuálni voliči sa zhmotnili a volili offline. 
 7.    DÚHA: Konzervativizmus, liberalizmus, maďarsko - slovenská strana, premiérka. Nasledujúce (4) roky budú dúhové. 
 8.    MATKA: Ženy majú prirodzene vrodenú materinskú starostlivosť. Slovensko si zaslúži materinskú lásku. 
 9.    AUTORITA: Aj napriek tomu majú ľudia radšej tvrdú ruku otca. 
 10.  MOSTY: Most ovládol juh, neovládol však most Márie Valérie. 
   

