
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Patrik Dvořák
                                        &gt;
                Nezaradené
                     
                 Veselo je na Slovensku - Úplatok za pád vlády a iné príhody 

        
            
                                    28.6.2010
            o
            10:11
                        (upravené
                28.6.2010
                o
                10:59)
                        |
            Karma článku:
                5.34
            |
            Prečítané 
            1257-krát
                    
         
     
         
             

                 
                    Na Slovensku je nám zase veselo. Miluje nás hlavne Taliansko, ktoré sme zázrakom, ktorý nebýva tak často, by som povedal, že tak raz za 100 rokov a tým zázrakom myslím naše víťazstvo nad Talianskom na MS vo futbale.
                 

                 Nebyť tejto správy, ktorá si myslím, že potešila každého jedného Slováka, boli by sme odkázaní na klasickú politickú zábavu, o ktorú sa nám momentálne postaral „obyčajný" človek, pán Matovič, ktorý dostal „obyčajnú" ponuku od svojho kamaráta, aby nechal padnúť vládu za 20 mil. Eur.   Ale je našim obyčajným ľuďom dobre. Naši obyčajní ľudia, myslím tým teda nás všetkých zrejme, pretože všetci sme obyčajní ľudia, všetci si môžeme dovoliť dať 20 mil Eur za pád vlády, platiť si miliónové kampane... V podstate pán Fico mal vlastne pravdu. Naša krajina je na tak obrovskej úrovni, že naši obyčajní ľudia majú milióny a milióny eur.   Nová vláda sa nám buduje. V rámci partnerských vzťahov, ako hovorí pani Radičová, to eroticky iskrí. Hlavne medzi liberálmi SaS a starodôstojnou dámou KDH. Vymieňajú sa a dohadujú sa medzi sebou rezorty ako keby sme boli na centrálnom trhovisku. Už nám tu len chýba pustiť si pieseň od skupiny Lojzo - Na centrálnom trhovisku. Mnohí z nás si ju ešte pamätajú a možno že pri takýchto piesňach by výmeny a boje o rezorty išli oveľa veselšie.   Čo sa týka generálneho prokurátora pána Trnku, tak kým je „zatiaľ" vo funkcii, povedal by som, že prežíva najzaujímavejšie pracovné obdobie za svoje posledné roky pôsobenia. Pretože toľko politických káuz, z ktorých každá je na hrane zákonnosti, tak  toľko, si myslím, že už veľmi dlho nemal. Pravdepodobne neurobí nič, ako neurobil nič za celé roky, veď  čo sa vlastne aj dá čakať od generálneho prokurátora, ktorý musí byť politicky poslušný. A teraz musí riešiť také nepríjemnosti ako je 20 mil. za pád vlády, nahrávky pána bývalého premiéra. ...a kopu iných a iných nepríjemných vecí, ktoré kazia miesto na krásnom vyleštenom pracovnom stole. A možno práve teraz je tá vhodná situácia ako ich hodiť do koša. Tak ako to už bežne na našom Slovensku býva.   Takže záverom len dodať: Aspoň že naši futbalisti nám s Talianskom urobili trocha radosti a na niekoľko  okamihov sme zabudli na problémy, ktoré nás trápia. Potešili sme sa všetci. Z otvorených okien domov sa ozývali zvuky víťazstva a to nás na chvíľu všetkých zblížilo a stali sme sa jedným národom športových fanúšikov. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Dvořák 
                                        
                                            Ďakovný list Európskej komisii za múku!!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Dvořák 
                                        
                                            Podaj prst, oserie ti ruku...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Dvořák 
                                        
                                            Otvorený list Európskej komisii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Dvořák 
                                        
                                            Milovaní cestári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Dvořák 
                                        
                                            Ani na sex ťa už nepoužijem...!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Patrik Dvořák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Patrik Dvořák
            
         
        patrikdvorak.blog.sme.sk (rss)
         
                                     
     
        Vždy treba mať dobrú náladu a vďaka nej vie byť život vždy veselý.

Nepodliehajme panike.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5009
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




