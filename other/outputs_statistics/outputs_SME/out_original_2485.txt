
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            zuzana minarovičová
                                        &gt;
                Nezaradené
                     
                 Nepál 2009 - návraty - krajina 

        
            
                                    10.3.2010
            o
            6:44
                        |
            Karma článku:
                11.44
            |
            Prečítané 
            1885-krát
                    
         
     
         
             

                 
                    Krajina a hory boli presne to, čo ma do Nepálu ťahalo. Nesklamali - boli krásne, rozprávkové, so všetkými náladami a rozmarmi počasia. Chvíľku mi aj slzy vyhŕkli, keď sme prechádzali popod Dhaulaghiri a Annapurnu za úplnej hmly a dažďa. Ale vzápätí mi to vynahradilo jedno skvelé ráno, východ slnka a skvelá partia rovnakých fotomaniakov ako ja. Hor sa do Himalájí.
                 

                    Pohľad z lietadla - počas hmly sa na mňa ako predzvesť skvelého treku usmiali tieto annapurnské himalájčatá.      Letisko v Jomsone pôsobilo milo, teplo a priateľsky.      Hneď po východe z neho sme zdvihli hlavy na okolité kopce a hľa ...      Zasnežené zrázy Nilgiri North (7.061 m n. m.).      Smerom k hraniciam Horného Mustangu - koryto rieky Kali Ghandaki.      Pohľad späť.      Cestou stretávame karavánu, prach máme všade.      Chvíľami je slnko, chvíľami sa zmráka.      Dolný Mustang - vysokohorská púšť.      Hraničné mestečko Kagbeni.      Púštna búrka.      Pohľad za hranice, kde už nepálske víza neplatia.      Rieka a jej krása.      Ranný Nilgiri North (7.061 m n. m.).      Výhľad z okna nášho hotela menom Majesty na Nilgiri North (7.061 m n. m.) a Tilicho Peak (7.134 m n.m.).      Budhistický kláštor v Muktinathe (3.760 m n.m.).      Krajina Mustangského kráľstva - pohľad z Muktinathu.      Všadeprítomné chorteny.      Kláštor v Jharkote.      Farebné mantry oživujú túto krásnu, ale pustú krajinu.      Na streche kláštora v Jharkote.      Aj v Himalájach je už jeseň.      Pohánkové pole čaká na zber.      Farby púšte.      Cesta z Jharkotu do Ekhlobatti.      Mustangské chrbáty.      Svetelná hra.      Ako figúrky.      Ešte niekoľko kilometrov a sme "doma".      Kráľovstvo Horný Mustang.      Do Muktinathu sa dá prísť aj na jeepoch.      Tyrkysová voda Kali Ghandaki.      Prší, rieka zmenila svoju farbu. V mrakoch sa skrývajú sedemtisícovky.      Úpätie Dhaulagiri - v sanskrite Biela hora (8.167 m n.m.).      Hmla pod Annapurnou - v sanskrite Bohyňa úrody (8.091 m n. m.)      Vodná riava v Annapurnskom údolí.      Úpätie Annapurny (8.091 m n. m.).      Mraky sa prevaľujú zo strany na stranu.      Konečne vychádza slnko.      Dedinka v údolí.      Masív Annapurny (8.091 m n. m.)      Bohyňa úrody sa konečne prestáva hanbiť.      Annapurna South (7.219 m n.m.).      Svitanie nad Sikhou.      Zlatý kúpeľ Bielej hory.      Tukuche Peak (6.920 m n.m.).      Kráľ Zlaté slnko zohrieva svoje princezné.      Meniace sa farby počas polhodinového časového rozpätia od prvých slnečných paprskov.      Svetlo nad Tukuche Peakom (6.920 m n.m.).      Tapa Peak (6.000 m n. m.).      Svitanie nad jednou z Annapurnien.      Svetlo na vrchole.      Každou minútou je teplejšie.      Krivky v rannom šere.      Machhapuchhre (6.993 m n.m.) smerom od južnej Annapurny (6.993 m n.m.).      Machhapuchhre (6.993 m n.m.) - ranný pohľad z Pokhary.      Gýčovka zo strechy hotela v Pokhare.      Machhapuchhre (6.993 m n.m.) býva svojou krásou a tvarom prirovnávaná k európskemu Matterhornu - z dôvodu jej zasvätenia bohovi Shivovi je zakázané na ňu liezť.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        zuzana minarovičová 
                                        
                                            4 na figu dni, alebo ako som si od pľúc pofrflala .-)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        zuzana minarovičová 
                                        
                                            Whisky &amp; Risky Expedition-Scotland 2012- Edinbra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        zuzana minarovičová 
                                        
                                            Whisky &amp; Risky Expedition-Scotland 2012 - Falkirk Wheel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        zuzana minarovičová 
                                        
                                            Whisky &amp; Risky Expedition-Scotland 2012- stretnutie s Wallaceom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        zuzana minarovičová 
                                        
                                            Whisky &amp; Risky Expedition-Scotland 2012- Glasgow, stanovačka a hrad
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: zuzana minarovičová
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                zuzana minarovičová
            
         
        minarovicova.blog.sme.sk (rss)
         
                        VIP
                             
     
        "Jediný spôsob ako obstáť v skúške, je skúšku podstúpiť. Inak to nejde."

Náčelník Majestátna Čierna Labuť (z knihy Marlo Morgan Posolstvo od protinožcov)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    164
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1825
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zápisky z kliniky
                     
                                                         
                       Tri príbehy
                     
                                                         
                       Kultúrny šok
                     
                                                         
                       Za čínštinou do Dánska
                     
                                                         
                       marec v raji ... a ešte o kúsok ďalej
                     
                                                         
                       Varíme s medveďom - Medvedí cesnak
                     
                                                         
                       300 dní na ceste okolo sveta
                     
                                                         
                       Obyčajná láska
                     
                                                         
                       Varíme s medveďom - Ribollita
                     
                                                         
                       Prepáč nám to, Nasťa!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




