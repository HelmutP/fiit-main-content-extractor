
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Hlina
                                        &gt;
                Nezaradené
                     
                 Hotovosť bude kráľom. 

        
            
                                    20.3.2010
            o
            10:21
                        (upravené
                20.3.2010
                o
                10:51)
                        |
            Karma článku:
                7.33
            |
            Prečítané 
            1559-krát
                    
         
     
         
             

                 
                    Pri všetkej tej záplave rôznych ekonomických teórií, ktoré sa budú na nás valiť pred voľbami, a ktoré ani nedočítame, resp. ak dočítame, skonštatujeme, že sme tomu až tak nerozumeli, a druhý raz sa nám to už nebude chcieť čítať. Skúsim dať aj ja jednu, ktorá bude, ako sa hovorí, povedaná po lopate.
                 

                 Vyššou formou prognostika je prorok. Údajne skúšali výkonný počítač nakŕmiť údajmi, a ten prognózoval, a je to možno aj čiastočne budúcnosť, že počítače namodelujú, aké budú vývojové tendencie keď ... alebo keď .... .   Určite však viem povedať už teraz, že počítače sa nestanú prorokmi. Prorokov bolo a bude málo, musia mať dušu. Pre potreby ateistov treba modifikovať, že musia mať telesnú teplotu. Na to, čo bude v najbližšom období netreba prognostikov, a už tobôž nie prorokov, mne stačilo sa pohybovať v podnikateľskom svete v Ázii. Napríklad v Pekingu nesmú jazdiť nákladné auta cez deň. Kvôli časovému posunu som často krát nevedel zaspať v noci a pozeral som, aká je premávka nákladných aut v noci. Dovolím si tvrdiť, že keby to niekto analyzoval, tak bije na poplach v Európe, resp. u nás doma už dávnejšie. Keby niekto videl tú obrovskú premávku tak obrovského množstva tovaru, ktoré sa presúva hore - dole, ale najmä do prístavov na lode do Ameriky a Európy, tak by musel spozornieť. Ja som to videl, a preto som pozorný už dávno. Každý, kto niečo kúpil od druhej polovice roku 2008 až doteraz, nie je až taký dobrý investičák. Samozrejme, opačne to sedí tiež: ten, kto v tomto období predal, dobre urobil. Prežili sme šialenstvo, kde byt stál večer viac, ako ráno. A nahovárali sme si, že to tak bude navždy. Mne stačil pohľad z okna hotela v Pekingu v noci, a vedel som, že to tak nebude. Sila ekonomiky sa presunula do Ázie, a nám je potrebné trochu vytriezvieť z toho ošiaľu.  CASH bude kráľom. Dokonca cisárom. Chráňte si hotovosť. Disponujte hotovosťou.   Nevzdávajte sa hotovosti do rôznych super nákupov. Super nákupy ešte len prídu. Nikoho nebude zaujímať, že máte pozemok tam, alebo hentam, alebo byt v novostavbe a dom na okraji mesta .   Dôležité bude to, či máte hotovosť. Hotovosť na zaplatenie tovaru, na zaplatenie dodávky služieb.  Držte si svoje aktíva v najlikvidnejšej forme, a to v hotovosti /samozrejme v banke/. Nič nekupujte v hotovosti, ak už, kupujte na splátky/bez úroku/. Držte si hotovosť, lebo tá bude mať najvyššiu cenu.  Samozrejme, toto nie je teória, ako riadiť ekonomiku štátu, ale odporúčanie, ako prežiť čo najmenej bolestivo obdobie, ktoré príde. A ono príde bez ohľadu, kto bude po voľbách vládnuť. To, ako dlho bude trvať, však už výraznou mierou ovplyvnia výsledky volieb.  . 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Čo sa Penta v Číne nenaučila
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Zrušiť zdravotné odvody je nebotyčná hlúposť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Nočné rokovanie bude aj pred Paškovou vilou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Smer zlegalizoval nezákonné billboardy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            O slovenskej žurnalistike rozhodne Shooty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Hlina
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Hlina
            
         
        alojzhlina.blog.sme.sk (rss)
         
                                     
     
        Alojz Hlina - nezávislý poslanec NR SR, občan Slovenska, občan mesta Bratislava, oravský bača. www.alojzhlina.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    518
                
                
                    Celková karma
                    
                                                8.94
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak kvôli tomu vyhodili Čaploviča?
                     
                                                         
                       Politický nekorektné úvahy k Dňu Európy a Dňu víťazstva
                     
                                                         
                       Hanebná nominácia na ústavného sudcu
                     
                                                         
                       Stane sa  SMER chrapúňom  roka?
                     
                                                         
                       Košičania, budete svietiť ako baterky?
                     
                                                         
                       Simpsonovci a Hlinovo hrdinstvo
                     
                                                         
                       Ako udržiavajú P.Paška a R.Kaliňák krehkú rovnováhu?
                     
                                                         
                       Môžete ísť do kostola, ale omša nebude
                     
                                                         
                       Keď SMER zabezpečuje koledy
                     
                                                         
                       Študenti, ako chutila držková u premiéra?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




