
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Gažík
                                        &gt;
                Nezaradené
                     
                 Grécko sme my 

        
            
                                    6.5.2010
            o
            18:55
                        (upravené
                6.5.2010
                o
                19:27)
                        |
            Karma článku:
                4.80
            |
            Prečítané 
            901-krát
                    
         
     
         
             

                 
                    Súčasné problémy Grécka nastavujú zrkadlo viacerým zúčastneným stranám. A všetkým politickým stranám, vrátane tých slovenských.  Skôr či neskôr sa tiež budú musieť vysporiadať s tým, ako sa postavia k jednej z kľúčových otázok výkonu politiky: uskutočňovať zodpovednú rozpočtovú politiku z dlhodobého hľadiska, a zároveň vyhrávať voľby z krátkodobého hľadiska.
                 

                 Občania nechcú počúvať o rozpočtových škrtoch, úsporných opatreniach, reformách. Nezaujíma nás, aké dôsledky pre budúcnosť verejných financií môže mať súčasný život na dlh. Všetko je len v rovine „štát daj, Európska únia zaplať"... Aj po 20tich rokoch demokracie máme stále obrovský problém prevziať na seba aspoň časť zodpovednosti za to, čo sa deje okolo nás a najmä čo sa bude diať po nás. Oveľa pohodlnejšie je postúpiť túto zodpovednosť ďalej. Občania na štát, členské štáty na Európsku úniu, Európska únia na USA, USA na globalizovaný svet... Grécka je preto rovnako aj našim problémom. Politika na „výstupe"  totiž zodpovedá očakávaniam občanov na „vstupe" v demokratickom mechanizme. Ten síce umožňuje kontrolovať a vyvodzovať politickú zodpovednosť vo vzťahu k „výstupom" v politike, je však pomerne  bezbranný vo vzťahu ku korekciám na „vstupe". V tomto sme ako Grécko. Chceme sa mať lepšie a očakávame, že nám to niekto zariadi. Nezaujíma nás, koľko to bude stáť naše deti, lebo abstraktný moloch štátu depersonifikuje účasť jednotlivca na jeho financovaní. Naháňame sa za smiešnymi zľavami po hypermarketoch, ale vôbec nerozmýšľame nad tým, koľko raz bude musieť každý z nás zaplatiť zo svojich daní za 13te dôchodky či predražené diaľnice. S našim vzťahom k Európskej únii je to ešte horšie. Tú vyslovene vnímame ako dojnú kravu. To, že môžeme vďaka Európskej únii slobodne cestovať, uchádzať sa o prácu v iných členských štátoch, žiť tam a požívať rovnaké práva, ako aj čerpať obrovskú finančnú pomoc berieme úplne automaticky. Ako by malo byť samozrejmé, že občania Veľkej Británie sa s nami radi podelia o svoju prácu, že nemeckí občania nám radi prispejú na našu dopravnú infraštruktúru zo svojich daní... Vôbec sa nezamýšľame nad tým, vďaka čomu sme si to zaslúžili. A už ani vo sne nás nenapadne rozmýšľať, čím by sme ako občania Slovenska mali a/alebo chceli prispieť do Európskej únie. K Európskej únii sme si nevybudovali žiadny vzťah. Veľa sme od nej dostali, ešte viac od nej očakávame, ale prakticky ničím sme zatiaľ do nej neprispeli.  A nemyslím to len vo finančnom vyjadrení. Preto som rád, že na pôžičku Grécku sa poskladajú aj občania Slovenska. Ponechávam úplne stranou ekonomické argumenty týkajúce sa opodstatnenosti a zmysluplnosti tejto pôžičky. Teším sa z nej, lebo vďaka tomu si možno začneme pomaly uvedomovať, že Slovensko je Európska únia a že Grécko sme aj my. Európska únia sa doposiaľ snažila budovať európsku identitu najmä prostredníctvom hmatateľných výhod, ktoré môžu občania získať. Plnohodnotný vzťah však vyžaduje, aby sme si uvedomili, že do Európskej únie musíme niečím aj prispieť. Je dobré, že každý občan Slovenska už vie, koľko ho bude Grécko stáť. Je zlé, že žijeme stále v ilúzii, že náš vlastný život nad pomery na Slovensku nestojí nič. Grécko sme my. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gažík 
                                        
                                            Inteligentné eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Gažík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Gažík
            
         
        gazik.blog.sme.sk (rss)
         
                                     
     
        človek hľadajúci... a pracujúci. Aktuálne v spoločnosti Telefónica Slovakia.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    596
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




