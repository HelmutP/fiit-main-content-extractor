
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Škamlová
                                        &gt;
                Slová a príbehy
                     
                 Ako stratiť kamarátov a zachrániť si zdravie 

        
            
                                    2.4.2010
            o
            15:39
                        (upravené
                2.4.2010
                o
                15:53)
                        |
            Karma článku:
                7.88
            |
            Prečítané 
            2079-krát
                    
         
     
         
             

                 
                    Alebo ako nedostať žlčové kamene...
                 

                 Na ilustráciu si môžeme uviesť dve situácie zo života.  Scénka č. 1: Chlapík sedí v poloprázdnej reštaurácii a čaká na jedlo, ktoré si objednal už pred pol hodinou. Po ďalších pätnástich minútach, keď už majú všetci pred sebou svoj obed okrem neho, mu pretečie trpezlivosť a osopí sa poriadne z ostra na čašníčku. Scénka č. 2: Dievčina s horúčkou sedí v čakárni. Je jej zle a ešte ju predbiehajú mamičky s deťmi, školopovinné ratolesti a rôzni iní ľudia. Nemá chuť sa s nimi dohadovať a tak nepovie nič.  Moja otázka znie: Kto z týchto dvoch prípadov dostane skôr žlčové kamene? Muž v reštaurácii alebo dievča čakajúce na doktora?   Správnu odpoveď sa dozviete na konci článku :-)  Z iného súdku: JA: „Úplne sa teším z tohto jarného počasia.“ ONA: „Ráno teda vôbec nie je pekne. To tak milujem... ráno mrzne a poobede je horko.“ JA: „Hej? Ja keď som dnes išla ráno na stanicu už bolo teplučko.“ ONA: „Tak si to ešte zajtra uži, lebo celý víkend bude pršať.“ JA: „Hádam nebude.“ ONA: „Bude, pozri si predpoveď počasia.“ (mimochodom minulý týždeň naozaj cez víkend nepršalo)  Jedna vec je vyventilovať sa – ponadávať, pokritizovať, pofrflať v rámci relaxu po ťažkom dni/týždni/mesiaci, druhá byť v tom živote zameraný len na to zlé. Sami viete s kým sa vám lepšie rozpráva – či s usmiatym dobre naladeným slniečkom alebo rosničkou predpovedajúcou katastrofy. Možno to trošku môžeme zvaliť na našu mentalitu – stále čakáme na to zlé, aby sme mali na čo frflať.  Z vlastných skúseností viem, že ľudí skôr zaujmete (a podľa mňa istým spôsobom i potešíte), ak im poviete, že ste sa pohádali s frajerom/upadli do bezvedomia/okradli vás. Prípadne porozprávate o nejakom rodinnom škandále, faux pas, či o prekvapivo zlom lekárskom náleze. Aby som svoju úvahu rozviedla, všimla som si, že v takej situácii ľudia vedia lepšie reagovať. Keď im poviete dobrú správu, či sa pochválite s úspechom, nevedia čo robiť. Môžu prejaviť záujem či uznanie, no veľakrát to nie je tak celkom úprimné, lebo by sa tým úspechom radšej chválili oni sami. Ak však oznámite niečo smutné, je to pre nich situácia známa (zrejme preto, že negatívne naladených ľudí stretávame častejšie). Vtedy je konečne ich čas utešovať, pýtať sa na podrobnosti a poskytnúť pomoc. Na jednej strane pekné od nich, ale keď na to hľadíme týmto spôsobom, je to trošku smutné. Štatisticky vzaté, častejšie ako pomoc potrebujeme rešpekt. Rešpektovanie našich názorov, činov, hodnôt... Lebo ak nás niekto spochybňuje či zosmiešňuje, darmo má nálepku kamarát.   Najťažšie to majú melancholici, ktorí svoje pocity prežívajú vo svojom vnútri ako dievča v čakárni. Žlčové kamene totiž vznikajú z nahromadenej zlosti a uduseného hnevu a ak sa im chceme vyhnúť, je užitočné si osvojiť pár vecí. Proces ich osvojovania však nie je taký jednoduchý, preto môžete začať pre začiatok s celkom príjemnou aktivitou. Potrebujete na to len hocijaký malý zošit, notes alebo slovníček a pero. Každý večer si zapíšte, čo vás v ten deň potešilo a nabilo energiou a tiež, čo vás zarmútilo a o energiu pripravilo. Vďaka tejto činnosti, ktorú sme dostali na domácu úlohu v škole, som sa utvrdila v tom, že niektoré osoby nie sú tak celkom pre moje zdravie prospešné. A tu sú tie tipy a triky čo s tým: -	dookola si opakovať, že sú to tiež len ľudia, ktorí sa snažia ako najlepšie vedia -	radšej sa s nimi pohádať ako mlčať (ak máte pocit, že je váš hnev oprávnený) -	ich hlúpe poznámky s mega pokojom ignorovať (chce to však cvik, pre začiatočníkov odporúčam medovku a jogu) -	pýtať sa, pýtať sa, pýtať sa (prečo si myslíš toto, prečo robíš tamto, prečo si mi povedal/a ono) -	občas prehodnotiť, či im vlastne chceme novinky o našom živote naozaj zdeliť -	proste sa im vyhýbať   Držím vám i sebe palce a mám taký pocit, že tento víkend bude zase krásne slnečný, tak hor sa načerpať energiu do lesa:-)   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Škamlová 
                                        
                                            Čo zostane...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Mária Škamlová 
                                        
                                            Milujem jeseň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Škamlová 
                                        
                                            Cesta na psychiatriu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Škamlová 
                                        
                                            A raz možno budem dospelou...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Škamlová 
                                        
                                            Nová jeseň, nový začiatok
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Škamlová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Škamlová
            
         
        skamlova.blog.sme.sk (rss)
         
                                     
     
        Zbieram príbehy a náušnice, úsmevy a pohľadnice, okamihy a vône, slová i bôle...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    66
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1213
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Okamihy
                        
                     
                                     
                        
                            Pohľadnice a vône
                        
                     
                                     
                        
                            Slová a príbehy
                        
                     
                                     
                        
                            Úsmevy a náušnice
                        
                     
                                     
                        
                            Bôle
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            O rodičoch, deti–robičoch a nepodarených mečoch a prasliciach  Č
                                     
                                                                             
                                            Bez domova, bez energie
                                     
                                                                             
                                            Rozprávka o najlepšom jabĺčku
                                     
                                                                             
                                            Ušijem ti lásku
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Banana Yoshimoto - Kitchen
                                     
                                                                             
                                            Hrvoje Hitrec - Krátki ľudia
                                     
                                                                             
                                            Milan Kundera - Nesmrtelnost
                                     
                                                                             
                                            Vojtech Mihálik - Appassionata
                                     
                                                                             
                                            Alessandro Baricco - Tento príbeh
                                     
                                                                             
                                            Fiona Agombargová - Nekonečná energia
                                     
                                                                             
                                            Rüdiger Dahlke - Nemoc jako řeč duše
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Matej Kubriczky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Clovek s velkym srdcom
                                     
                                                                             
                                            Život plný života
                                     
                                                                             
                                            Poetický Martin
                                     
                                                                             
                                            Pokreslená (ne)známa
                                     
                                                                             
                                            Farebná Katka
                                     
                                                                             
                                            Jednoducho Saša
                                     
                                                                             
                                            O drogách...
                                     
                                                                             
                                            Hĺbavý Matej
                                     
                                                                             
                                            Na zahriatie
                                     
                                                                             
                                            Inžinier Vladimír
                                     
                                                                             
                                            Svetobežnícka Sima
                                     
                                                                             
                                            Chutná Zemlovka
                                     
                                                                             
                                            Všestranná Aďuš
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Doba Magazin
                                     
                                                                             
                                            SAShE
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




