

 Zastavím sa a zrazu sú všetky moje problémy preč. Vnímam ako šumí tráva, ako v diaľke spieva slávik, len nedávno prebudené motýle šantia v povetrí a aj ryby vyskakujú z vody, aby aspoň na krátky moment ucítili pohladenie jemného vánka. A keď ma na mojej bosej nohe pošteklí malý vytrvalý mravec, precitnem otvorím oči a pocítim nekonečný pokoj. 
 Žijeme v každodennom zhone, túžime po chvíľke pokoja. A pritom je to také jednoduché, ľahnúť si do trávy, zatvoriť oči a nechať sa hladkať teplým vánkom po tvári. V prírode dokážeme nájsť odpovede na všetky naše otázky, trápenia. 
 Keby sa len, tá sladká opojná vôňa jari dala konzervovať, ukladala by som ju do skiel pre všetkých tých, ktorý nemajú to potešenie plnými dúškami vychutnať si jej krásu. 
 A keby sa nadýchli, uvideli by spletité chodníky hustými voňavo rozkvitnutými tŕnkami...lilavé fialky krčiace sa medzi steblami prvej jarnej trávy...malé klíčiace semienka dubov raziace si cestu za slnkom...v pozadí sa mihli krídla škovránka...a na okraji lúky počuť žblnkot tečúceho prameňa. 
 Na mokrej lúke si vyzúvam topánky, kráčam bosá. Nohy sa mi zabárajú no trávy, cítim jej sviežosť každým krokom. Slnko mi zohrieva chrbát, mám chuť spievať a tancovať ako lesná víla po lúke. Cítim sa ako jediný človek na zemi, ako keby medzi mnou a prírodou vládla nekončiaca harmónia, ktorá napĺňa moju dušu spokojnosťou a láskou. 
 Tu môžem byť sama sebou, tu ma nik neodsúdi za moje myšlienky, nik nebude chcieť vedeť prečo som aká som. Pochopí to. Nebude sa pýtať na dôvod. Vypočuje a nebude ma súdiť. Prijme ma takú aká som. 
   
   
 Nechajme sa inšpirovať prírodou. 
   

