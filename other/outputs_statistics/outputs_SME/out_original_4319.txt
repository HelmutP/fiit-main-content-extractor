
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Šikulová
                                        &gt;
                Moje Bulharsko
                     
                 Jar BG - Koprivštica 

        
            
                                    10.4.2010
            o
            23:05
                        (upravené
                11.4.2010
                o
                17:59)
                        |
            Karma článku:
                7.48
            |
            Prečítané 
            1110-krát
                    
         
     
         
             

                 
                    Povedala by som ti, že ťa vezmem do iného sveta. Keby sa ten svet mal pomenovať jedným slovom, tak by to bolo slovo: pestrý... a tak trochu ošarpaný, s ošúchanou modrou farbou. Kde zima trvá deväť mesiacov a na jar padajú krúpy. Na holi sneh a v záhrade vykvitnutá čerešňa. Svet sám pre seba... Koprivštica.
                 

                     
 
       Opustíš mesto panelákov, nefunkčných tovární s rozbitými sklami, mesto opustených železničných staníc a psov naháňajúcich rýchlosť vlaku. Cez okno sleduješ sneh, pasúce sa ovce, spiace kone. Vedľa teba sedí pán čo každej žene povie, že je krásna. A snáď sa to v tom Bulharsku zlepší, povzdychne si.              Na stanici ťa žiadný autobus do mesta nečaká a tak čakáš ty, dlho, dlhšie na tej prázdnej stanici. Pred tebou je len hora z ktorej vychádza po daždi biela para. A na stanici jedna mačka a výpravca čo sa snaží dovolať šoférovi autobusu. Nakoniec príde... Keď sa ho potom pýtaš, kedy pôjde na stanicu znova, tak len prehodí, že to až tak presne nevie.       
 
       
 
              Mesto jednej rieky a nie jednej studne, šikmých kamenných mostov, predavačiek vyšívaných obrusov a tkaných kobercov, drevených veránd, farebných domov, tajomných zanedbaných kostolov a veľkých reštaurácií, do ktorých nikto nechodí. Počuť odtiaľ pieseň "Dievča macedónske".       
 
       
 
       
 
       Vo farebných domoch sa usadíš do nízkych sedačiek s červenými vankúšmi a neznámy hlas ti recituje bulharskú poéziu. Na strope a za sklenenými dvierkami skríň nachádzaš obrázky z Egypta a Istanbulu. Domy s kobercami z ovčej vlny, plné maličkých postelí pre maličkých ľudí.       
 
       
 
              Kúpiš si sladký lokum s orechom uprostred... nie jeden, ale päť. Ucítiš voňu šarenej soli, malinového lekváru a orechov nakladaných v mede pri každom jednom stole pristavenom pri ceste. Občas sa tam predávajú aj narcisy.       
 
       
 
       
 
       Narcisy rastú aj v záhrade pri chráme.                     Studené chrámy ti trochu pripomínajú trh prebytočných harabúrd. Ikonostas je okrášlený umelými ružovými kvetmi, čipkovanými obrúskami, patričkami.  Naokolo spráchnivené knihy  a huňaté koberce.  V tichu sa ozýva len praskot z piecky, pri ktorej sa zohrieva predavačka sviečok. Ikony sú pokvapkané voskom zo žltých medových sviečok, skláňajú sa nad nimi tváre žien v kvetovaných šatkách.       
 
       
 
       
 
       
 
       Autobus na stanicu nejde, veď ani šofér to nevedel presne. Ale odvezie ťa auto niekoho z miestnych. Vo vlaku pri tebe sedí pani s igelitkou plnou tulipánov. Nesie si kus jari do Sofie.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Kam Sofijčania nechodia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            153 dní protestov a študentská okupácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Konya
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Ospravedlnenie na pamätníku sovietskej armády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šikulová 
                                        
                                            Rodopy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Šikulová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Šikulová
            
         
        sikulova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Hlavne o Bulharsku, o živote v Sofii a cestovaní po bulharských mestách a ošarpaných dedinkách. Aj o Turecku, Istanbule a Malatyi. Najradšej však o najkrajšej horehronskej dedine, jej kroji, pesničkách a makovníkoch - jednoducho o Šumiaci.

Janka Šikulová  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2411
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje Bulharsko
                        
                     
                                     
                        
                            Doma
                        
                     
                                     
                        
                            Niekde inde na Balkáne
                        
                     
                                     
                        
                            Moje Turecko
                        
                     
                                     
                        
                            Každodennosť bulharská
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čokoľvek na tomto blogu o cestovaní Bulharskom
                                     
                                                                             
                                            Úžasné foto z dejín Bulharska
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Georgi Gospodinov
                                     
                                                                             
                                            Elif Shafak
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Eliška
                                     
                                                                             
                                            Vita
                                     
                                                                             
                                            Janette Maziniová
                                     
                                                                             
                                            Blanka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dnevnik
                                     
                                                                             
                                            Guardian
                                     
                                                                             
                                            Kommersant
                                     
                                                                             
                                            Lost Bulgaria foto
                                     
                                                                             
                                            Šumiac ešte lepšia stránka
                                     
                                                                             
                                            capital.bg
                                     
                                                                             
                                            &amp;#304;nspiracie
                                     
                                                                             
                                            Le Monde
                                     
                                                                             
                                            Bacchus.bg
                                     
                                                                             
                                            Sofia ulica
                                     
                                                                             
                                            BBC
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nekúpim už topánky u ***** **********
                     
                                                         
                       Okolo Nízkych Tatier: 336 kilometrová cykloporcia
                     
                                                         
                       Prečo nerobia revízori poriadok aj s otravnými cestujúcimi?
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Sarajevo - rozdelené mesto
                     
                                                         
                       Pán Hríb, mám 33 a ja to vidím takto
                     
                                                         
                       Podstránka sme.sk ospevovala T. E. Rostasa
                     
                                                         
                       Chráňme život! Ale nie takto
                     
                                                         
                       Pochod za život na život zabudol
                     
                                                         
                       Podporujem pride pochod, ale podporujem aj rodinu.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




