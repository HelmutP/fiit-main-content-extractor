

    
 Pred dvomi týždňami John Pleasants, CEO spoločnosti Playdom, zaoberajúcej sa tvorbou hier určených pre sociálne siete, odhalil niečo z ich ekonomiky. Pri aplikácii týchto princípov na Farmville to vypadá v skratke takto: 
  
 Farmville má v súčasnosti viac ako 82 390 000 registrovaných používateľov. Z nich je približne 30 000 000 užívateľov, ktorý používajú túto aplikáciu každý deň. Približne 2% z nich pravidelne nakupujú virtuálny tovar (virtuálne peniaze, za ktoré si v tejto hre vylepšíte svoju farmu). 600 000 ľudí , každý z nich minie na tento virtuálny tovar priemerne 20 dolárov mesačne. 
   
 600 000(používateľov) x $20 = $12 000 000 mesačne 
 $12 000 000 x 12 mesiacov = $144 000 000 ročne 
   
 Treba si uvedomiť že Zinga (tvorca Farmvill, Mafia Wars, Cafe World,...) je v prvom rade marketingová firma a nie firma tvoriaca hry.  
   
 Zaujímavé čísla vzhľadom na to, že v tejto hre vlastne vykonávate manuálnu prácu, zbierate vajíčka a hnojíte svoje políčka. Keď sú ľudia ochotní minúť na toto približne 144 miliónov dolárov ročne, asi na tom niečo bude. Idem do záhrady a skúsim si v nej spraviť svoj vlastný Farmville, ale tento bude reálny. 
   
  PS: ak vás zaujímajú social games, odporúčam vypočuť si skvelú prednášku na tému „The social Game Revenue Machines“ (tu) 
   

