
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alena Oravcová
                                        &gt;
                Nezaradené
                     
                 Malý a zúrivý a Robinson -  Jozef Urban 

        
            
                                    26.2.2009
            o
            19:30
                        |
            Karma článku:
                10.93
            |
            Prečítané 
            6188-krát
                    
         
     
         
             

                 
                    Začiatok osemdesiatych rokov minulého storočia. Pri okne postavená obyčajná školská lavica jedného košického gymnázia. A kontrastne k tomu za oknom nedočkavo čakajúci usmievavý svet. Zradný úsmev však, s poriadne špicatými zubami. Toto bol svet, do ktorého Jozef Urban skočil rovnými nohami a ktorý ho prinútil vyvrátiť ideu, že všetko v poézii už bolo povedané.
                 

                 
  
       Nechcem písať biografiu, oprašovať kult tejto osobnosti či filozofovať nad jeho životom. Chcem len upozorniť na to, že existuje aj iný Urban než Milo a že 28. apríla 2009 uplynie už okrúhle desaťročie od smrti tohto človeka. Na stránkach vydavateľstiev a internetových kníhkupectiev nájdete iba suché "tragicky zahynul pri dopravnej nehode v roku 1999". Smrť sa ako vždy priplazila nečakane, uprostred jeho tvorivej práce, a prešla mu svojou ľadovou rukou po tvári. Nasledujúci deň v novinách slovenskému textárovi a básnikovi venovali sotva desať riadkov. Smrť to bola dosť nepríjemná, jeho auto sa zrazilo s v protismere idúcim kamiónom. Seat bol doslova vtlačený pod nápravu nákladiaku, ktorý ho ešte 90 metrov tlačil pred sebou. Bilancia : Jozef Urban bol namieste mŕtvy.                   Veríme menej svojim novým láskam    a púpavám    a snom    a sebe samým   Človek má iba málo krajín   S kvietkovanými lietadlami       Inzerátový ideál    Čo nefajčí a nepije   A pretože je život krutý   Vo filmoch treba    happy end        Kolumbom do rúk vajcia    namäkko   No predsa človek nerád nechá lístok   Odpusťte že som    utiekol            ( Malý zúrivý Robinson, báseň Malý zúrivý Robinson, 1985 )           Odpustime mu to. Veď uznajme, ako sa darí dnešným Robinsonom? Chcel by Jozef Urban žiť vo svete dvadsiateho prvého storočia? Keď jeho rebelantská generácia rockových básnikov vymiera? Veď kto už dnes vie, že báseň Schody do neba ( zo zbierky Dnes nie je Mikuláša ) je rovnomenná s tou od skupiny Led Zeppelin? A že Led Zeppelin nie je len do slovenčiny preložené vybájnená olovená vzducholoď ? A kto aj vie, je to vôbec dnes ešte aktuálne?               Ty máš čas   Je po koncerte   Vtedy sa chodí pešo   Kašle sa na hromadnú dopravu   Nasledujte A odpusťte A verte   V kalužiach mesiac tragicky postavený na hlavu       Hudobníci si zbalia aparáty   a rozmýšľajú   do ktorej krčmy ktorý pôjde       Je po Sydovi Barretovi po Bonhamovi po Pink Floyde   Umelci umierajú blúznivo a krátko           ( Dnes nie je Mikuláša, báseň Schody do neba, 1983 )           Takže nech jedinou zásluhou, ktorú tomuto človeku pripíšeme, nie je iba text šlágru od skupiny Elán. Mal predovšetkým talent, už počas gymnaziálnych čias vyhrával literárne súťaže. Aj keď porotca jednej z nich, Vojtech Mihálik, mu už ako pätnásťročnému zasranovi vyčítal skepsu a prehnaný cynizmus prýštiaci z jeho básní. V neskoršej tvorbe pribudol ešte pesimizmus a pochmúrnosť. To sú ale hlavné charakteristické črty jeho zbierok. Bez cynizmu by Urban nebol Urbanom.       Žijeme o ničom. Žijeme tvárou k zemi.  Psi z podzemia sú preč. A my sme ako oni.  Ak toto má byť svet, preboha, načo je mi?  Snežienka vyrastie. A vopred sa už skloní.    Bolo to zúfalé. Zúfalo nevyhnutné.  Dobro je iba v nás. Aj tak sa končí hrozne.  Nádej je améba. Vždy si z nej niekto utne.  Láska je ako jazz - umiera skôr než doznie.    Ak niečo chápeme, nevieme, či to sedí.  Je málo otázok a veľa odpovedí.  Musím si v ústraní vyliečiť naivitu.       ( Snežienky a Biblie, 1996 )       Ja ho neberiem ako svoju nedosiahnuteľnú modlu. Ale ako človeka na plný úväzok, ktorý mal chuť do sveta vykričať mnoho vecí. Sice malý, ale zúrivý svojimi dynamickými veršami je dodnes veľkou inšpiráciou pre literárne činných ľudí. Kebyže niet jeho náhlej smrti, nepochybujem, že ho je počuť dodnes. Ale život je zrejme zúrivejší...           Mŕtvi   sú živí bez práva mať hlas   Sarkasticky si klopú na čelo   Tisíckrát sme sa snažili    byť pánmi   a miliónkrát nás to    bolelo   Blues pre môj ostrov kope do verklíkov   kde stáda slepcov melú    naše hlasy   Má čakať na svoj vyrátaný osud   Má počkať na smrť    ale nepočká si...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Oravcová 
                                        
                                            Prečo práve Matkin?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Oravcová 
                                        
                                            Kurt Donald Cobain – Príď taký, aký si
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alena Oravcová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Tatiana Lajšová 
                                        
                                            Recenzia: Fantázia 2014
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alena Oravcová
            
         
        alenaoravcova.blog.sme.sk (rss)
         
                                     
     
        Bežný výnimočný človek...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3301
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




