

 Celá tvoja rodina je umelecky orientovaná. Otec scenárista, mama textilná výtvarníčka, dedo bol sochár. Aké bolo vyrastať v takejto rodine? 
 Super, mal som najdokonalejšie detstvo. Jednak preto, lebo dedo vytvoril dosť veľkú rodinu a to má hneď niekoľko výhod. Keď je v rodine veľa detí, nemôžete mať všetko, takže vás to núti čím skôr sa postaviť na vlastné nohy. Všade však máte svojich ľudí, na ktorých sa môžete spoľahnúť. A ďalšia skvelá vec je, že som mal možnosť stretnúť neskutočné množstvo rôznych ľudí, vidieť rôzne pohľady na svet a spoznať rôzne jazyky, kultúry, naboženstvá a všetko si to reálne vyskúšať, čo ja osobne to vnímam ako dar. 
 O umelcoch sa hovorí, že žijú často mimo reality, akoby vo vlastnom svete. Bolo to tak aj u vás? 
 Nie, moja mama musela čeliť normálnej realite. Musela sama vychovať štyri deti, nikdy som nemal pocit, že by žila mimo reality, ani nikto iný v našej rodine. Myslím, že je to trošku fáma. Umelec by mal prinášať iný pohľad, možno nový, ale to neznamená, že žije mimo reality. 
 Ako si na tom s reálnym životom ty? Živí ťa fotografovanie alebo musíš pri tom robiť aj niečo iné? 
 Jasné, fotografovanie ma živí, ale popri tom sa musím ešte strašne veľa hrať. Robiť veci pre radosť, aby to fotenie nebola len technická práca pre klienta, čo je zaujímavé do tej miery, kým je čo sa učiť alebo skúšať. No potom je to rutina a to človeka vyčerpáva, a tak musí vymýšlať všetko preto, aby si nezhnusil vlastnú prácu. Preto fotím vlastné projekty, robím workshopy v škôlke, niekedy predavám na trhu, fotím CD albumy kamošom, zakladám spoločenstvo fotografov, take samé normálne veci. 
 Venuješ sa módnej fotke, čo ťa na nej najviac baví? 
 Najprv som si myslel, že je to práca zo ženami a ženským telom, že to bude super! Ale ono je to vždy všetko trošku inak. Väčšinou to ešte ženy nie sú a to telo je hlavne taký vešiak. Málo ľudí si uvedomuje, že kvalitná módna fotka musí ukazať šaty. Ale inak je to živá fotka, stále sa dá niečo vymýšľať, robiť veci, čo sa mi páčia. 
  
   
  
 Tvoje meno sa začalo skloňovať hlavne v súvislosti so súborom fotografií Cigáni na pekno. 
 Je to môj posledný súbor, s ktorým som získal aj zopár cien. Podarilo sa mi vytvoriť fotograficky úplne nový prístup k rómskej tematike, keďže  Rómovia sa všade zobrazujú ako opice, čo žijú na smetisku. 
 Tvoje „cigánske" fotky  ukazujú Rómov v inom svetle ako fotky, na ktoré je verejnosť zvyknutá. Nie je to istý spôsob štylizácie? Že ich štylizuješ a robíš z nich niečo, čo ich nevystihuje, nie je im vlastné... 
 Áno, je to štylizácia, úplne rovnaká, ako keď fotím portrét, titulku pre časopis. Príde stylista, makeup artista a naštylizujú nám krásnu herečku alebo moderátorku. To isté som urobil aj v osadách, poprosil som Rómov, aby si obliekli najkrajšie šaty a snažil som sa ich odfotiť pekných. Neštylizujem ich do niečoho iného. Pretože si nemyslím, že sú všetci rovnakí, že nerobia a iba využívajú systém. Je tam veľa Rómov, ktorí sa snažia niečo zmeniť. Napríklad Gusto Bača je rómsky starosta v obci Stráne pod Tatrami. Naučil sa písať granty, postavil 60 sociálnych bytov, stavia hygienicke centrum, aby sa aj tí najchudobnejší mohli umyť a vyprať si a aby doktor mal kde prezrieť deti. A takýchto je omnoho viac. 
  
 Ako dlho si  pracoval na tomto projekte? Ako k tomu pristupovali samotní Rómovia? 
 Pracoval som na tom asi rok a pol. Rómovia boli z projektu úplne nadšení. Rómovia sa radi fotia a samozrejme sa tešia, keď ich niekto chce ukázať pekných. Všetci moji známi ma upozorňovali, že ma okradnú a aj ja som mal strach, lebo som tam potreboval priniesť techniku za veľa peňazí.  Ale môžem pokojne povedať, že mi doteraz nezmizlo vôbec nič. Reakcie okolia sú tiež pozitívne, je to niečo úplne nové, ľuďom sa to páči. 
 Projekt Cigáni však nie je ešte dokončený, keďže chceš fotiť len tých, ktorí pracujú. Je ťažké nájsť pracujúcich Rómov? V ktorých oblastiach si fotil? 
 Prvé kontakty boli náročné, ale teraz to už ide samo a všade stretávam Rómov, ktorí sa snažia niečo zmeniť. Fotím zatiaľ v okolí Kežmarku. 
 Na Slovensku majú ľudia v sebe dosť silno zakorenený rasizmus. Stretol si aj s nejakými vyslovene negatívnymi reakciami? 
 Áno, na prezentácii Pecha Kucha Night sa ľudia v hľadisku smiali a jeden Talian vyletel, že som rasista a že z nich robím opice na posmech. Potom som za ním šiel a dlho som sa s ním o tom rozprával. Nechápal som to, pretože tí ľudia ani fotky nie sú smiešne. Paradoxne to bol Talian a nie Slovák. 
  
   
  
 Plánuješ projekt so stvárnením slovenských povestí a rozprávok. O čo konkrétne ide? 
 So scénografom Jurajom Kuchárkom máme rozpracovaný projekt slovenských rozprávok, ktoré zozbieral Samuel Czambel. Sú to hlavne rozprávky z východu. Czambel posobil súbežne s Dobšinským, ale nie je vobec taký známy. To ma na tom baví, hľadať tu na tom našom Slovensku témy, čo sú už zabudnuté a znova ich oprášiť a prerozprávať nanovo. Slovensko skrýva nespočetné množstvo nádherných tém. 
 Aké je fotografovanie na Slovensku? Sú tu dobré podmienky alebo to záleží od toho, aké podmienky si vytvorí samotný fotograf? 
 Myslím si, že v každom odbore si človek musí spraviť podmienky, nielen vo fotografovaní. Slovensko je veľmi malý trh, z čoho vychádzaju aj naše podmienky-  sú malé, ale myslím, že je to stále lepšie. Teraz som bol na dvoch fotografických festivaloch v Poľsku a všade boli ľudia veľmi prekvapení, že taký malinký štát má veľmi veľa dobrých fotografov. Vo finále Ceny Grand Prix v Lodzi boli zo 600 fotografov z celého sveta dvaja Slováci. Myslím, že na taký malý národ je veľký úspech. 
 Vnímaš na Slovensku konkurenciu? 
 Vôbec nie. Každý človek je iný a nikto nemôže urobiť tu istú fotku. Nič také nepociťujem. 
 Čo je podľa teba pri fotografovaní najťažšie? Dá sa fotke priučiť alebo musíš mať talent? 
 Najťažšie je fotiť, ak vás to nebaví. Je ťažké urobiť dobrú fotku, ja som zatiaľ urobil jednu. Ale to závisí aj od kritérií aké človek má. Fotka je predovšetkým jazyk a  remeslo, ktorému sa dá priučiť. A zvládne to každý. 
 Koho práca zo slovenských alebo svetových fotografov sa ti páči? 
 Myslím, že na Slovensku sa formuje nová vlna fotografov.  Andrej Balco, Martin Kolár, z mladých Radana Valuchová, Ján Kekeli, Boris Nehmeth, Dalibor Krupka. To sú ľudia, v ktorých verím a dúfam, že o nich ešte budeme počuť. 
  
  
   
   

