

   
 A tak sa mi vyžalovala, akú škodu jej spôsobili susedia nad nimi, ktorí prerábajú byt... 
 Najprv som na ňu hľadela zúčastnene a potom hovorím: 
 „Čo sa trápiš, veď sú to len veci! Človeka to mrzí, to hej. Ale to predsa nie je najdôležitejšie..." 
 Ťažko sa hľadajú slová, ktoré utešia, aby nevyzneli ako fráza a boli aj pravdivé. Naše priateľstvo je založené na úprimnosti... Ale naučili sme sa navzájom povzniesť nad problémy a robiť si žarty aj zo starostí, aby nás nezlomili... A tak sa náš rozhovor končí väčšinou výbuchmi smiechu... Tentokrát mi postačil aj jej úsmev cez slzy... Na druhý deň sa mi poďakovala, že som ju zbavila depky a že sa to už rieši... 
 Ako to bolo predtým? Od prvej chvíle, keď mi ju vedúci predstavil, sme si nepadli do oka. Sme síce rovnako staré, ale dosť odlišné. Nerozumela som jej spôsobu reči ani jej humoru. Robila sa „múdra", hoci ja som služobne staršia, mala hlúpe pripomienky, ktoré vedela šplechnúť do tváre. A ešte si myslela, aké je to úžasne vtipné... Išlo ma z nej poraziť! 
 A tak som sa radšej uzavrela do seba, nerozprávala sa s ňou, ak to nebolo nutné. Chodili sme okolo seba a zazerali jedna na druhú. Keď som prišla domov, bola som z toho otrávená a kládla som si otázku, že dokedy budem musieť toto znášať. Strašne mi liezla na nervy. Neznášala som ju! 
 Napokon som si povedala: „Danka, máš na výber (keďže o zamestnanie som dobrovoľne prísť nechcela) - buď sa z toho zblázniš, alebo si na ňu zvykneš!" Rozhodla som sa pre tú druhú možnosť a začala som ju počúvať a snažila som sa ju pochopiť. Sem-tam sa mi podarilo povedať na to trefnú poznámku, ktorá ju rozosmiala... Postupne sa z toho stal akýsi rituál. Ona niečo povedala, ja som hodila poznámku a nasledoval výbuch smiechu. Tak si už na to zvykla, že ani nezačala skôr robiť, kým nedostala svoju dávku humoru. 
 Sama som tomu nerozumela. Veď o mne sa hovorí, že nemám príliš zmysel pre humor. Alebo to bolo tým, že som ju počúvala a chcela jej porozumieť? Neviem. Ale čím ďalej, tým sme sa smiali viac a viac a hovorili si dôvernejšie veci a našli si cestu k sebe. 
 Teraz už jedna na druhu nedáme dopustiť a v práci si pomáhame. Stali sa z nás dobré priateľky. Chodenie do práce nie je záťaž, ale relax. Našla som niekoho, s kým sa môžem smiať a to je veľká vec! 
 Prečo si často tak komplikujeme život zlými vzťahmi? Niekedy stačí málo... Nemusí sa to vždy podariť, ale oplatí sa aspoň to vyskúšať! 
   

