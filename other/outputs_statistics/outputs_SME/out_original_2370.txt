
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Natália Blahová
                                        &gt;
                úvahy
                     
                 Rozhrešenie pre Markízu 

        
            
                                    25.2.2010
            o
            9:15
                        (upravené
                25.2.2010
                o
                9:37)
                        |
            Karma článku:
                13.93
            |
            Prečítané 
            6756-krát
                    
         
     
         
             

                 
                    Nie je to o dobrom konci, ale možno o lepšom začiatku.
                 

                 V septembri a októbri minulého roku odvysielala Markíza dva diely Modrého z neba, v ktorých hlavnými hrdinami boli malí chlapci. Obe relácie boli hrubým zásahom do ich súkromia a do ich práv.       Občas sa zamýšľame nad tým, či je skutočne nutné, aby na televíznych obrazovkách bežala v nekonečnej slučke čierna kronika. V rôznych vyhotoveniach, v nápaditých prevlekoch, vždy s dávkou násilia, s príchuťou tragédie a puncom bolesti.  Možno si sebakriticky povieme - veď si môžem vybrať, môžem to vypnúť...nech si vysielajú, čo chcú!  Keď je niekto pre pobavenie davu ukameňovaný, očakávame, že nastúpia tí, čo strážia hranice etiky a zákona, že to vyriešia a uvediú na pravú mieru.  Je to však vo veľkej miere na nás. Môžeme sa rozhodnúť, či sa ozveme alebo budeme ignorovať.   No ignorancia sa stáva veľkým problémom vtedy, keď sú na tieto účely používané deti. Deti, ktoré vedia o svojich právach málo, tieto práva nevedia komunikovať, nemajú silu si ich obhájiť a sú vystavované stresovým situáciám, ktorých následky nevedia vopred zhodnotiť.   Tak ako dvaja chlapci z Modrého z neba - Tomáško a Dárius.   Vždy sa dá urobiť aspoň niečo, bohužiaľ väčšinou neskoro, no dáva nám to malú nádej, že nielen médiá môžu ovplyvňovať nás, ale i my môžeme ovplyvňovať médiá.   Podnet riešila Rada pre vysielanie a retransmisiu a nadelila  spoločnosti Markíza pokutu vo výške 20.000 eur za porušenie ustanovenia zákona o zobrazovaní fyzického alebo psychického utrpenia maloletých v súvislosti s odvysielaním programu Modré z neba zo dňa 7. októbra 2009.   Nie je to šťastný koniec. Šťastným by bol vtedy, keby sa tento príbeh nikdy neodohral. Verím však, že to bude aspoň nachvíľu mementom pre tvorcov programov, ktorí sa zahrávajú s krehkými osudmi ľudí.    Citové vydieranie je najjednoduchším nástrojom zvyšovania sledovanosti , preto mnohé záleží na našej vnímavosti k právam iných a hlavne tých najbezbrannejších.  Takže buďme ostražití, ak si budeme nabudúce utierať slzy do vreckovky a zvážme, či nebolo pre pár zbytočne vyronených sĺz niekomu ublížené.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (53)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Alipaškovia a ich majetky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Čo bude po SME
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Závažné podozrenie zo sexuálneho zneužívania dieťaťa odfláknuté
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Žasnem a neprestávam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Trnava – mesto za euro alebo za zlámaný groš
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Natália Blahová
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Natália Blahová
            
         
        nataliablahova.blog.sme.sk (rss)
         
                        VIP
                             
     
          
 
 
Srdečne ďakujem za Vašu priazeň. Nájdete ma aj tu: nataliablahova.sk
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    363
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5102
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            úvahy
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            pocitovky
                        
                     
                                     
                        
                            knižnica
                        
                     
                                     
                        
                            Komentáre
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            u nás
                        
                     
                                     
                        
                            foto
                        
                     
                                     
                        
                            nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vymierame a vlastná hlúposť nás dorazí
                                     
                                                                             
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                                                                             
                                            301
                                     
                                                                             
                                            Pre mamičky, detičky a iné -ičky
                                     
                                                                             
                                            SaS plní sľuby a drží slovo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




