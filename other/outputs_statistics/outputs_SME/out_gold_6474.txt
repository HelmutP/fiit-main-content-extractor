

 Ak som napísal, že je to mesto sympatické, vychádzal som z mojej najnovšej skúsenosti, z nedávnej dovolenkovej návštevy tohto mesta a jeho Vartovky. 
  
 Na Vartovku sa dá ísť turisticky, ale aj autom... Skoro 6 hodín chôdze je to na Čabraď. To je mimo rámec našich turistických pochodov, tak sme sa po dopoludňajšej túre na Čabraď vybrali do Krupiny autom, že sa tu naobedujeme. 
  
 Verní Google mapám sme si mysleli, že autom prídeme až pod Vartovku, ale nepresné zadanie pre nášho GPS sprievodcu nás priviedlo na protiľahlý kopec, tak sme záverečnú časť výstupu absolvovali pešo. 
 Vartovka nesklamala, je na kopčeku nad mestom a keby tam historicky nestála, museli by ju Krupinčania postaviť v súčasnosti, hoc aj s podporou EÚ. 
  
 Niekedy sa mi zdá, že na rozhľadne akosi zabúdame. Mohli by sme sa poučiť v Čechách, kde ich je veľa (159) historicky zachovaných, aj nových a dotvárajú tak atmosféru kultúrnej krajiny. 
 Pod Vartovkou bol nachystaný táborák, informácie o jeho poslaní sa však rozchádzajú. Jedni nám hovorili, že to je nachystaná vatra zvrchovanosti a druhí, že vraj odložená vatra na počesť oslobodenia, odložená kvôli počasiu. Tak či onak, pri jej zapálení v sobotu už nebudeme... 
  
 Príjazdová cesta k Vartovke. 
 Podľa wikipedie je na Slovensku 34 rozhľadní. Len na šiestich z nich som bol... Resty, resty, budú cesty... 
  
 Časť mesta Krupina z veže vyhliadky. Celé okolie je odtiaľto ako na dlani. 
  
 Odchádzame z vyhliadkovej veže na prehliadku mesta, aj s predstavou dobrého neskorého obeda... 
  
 Cestou obchádzame kozu, ktorej sa páči ležať práve na našom chodníku. Boli sme pre ňu príjemným spestrením nudného, všedného kozieho dňa. 
  
 Krave s okuliarmi budeme tiež chýbať. Smutne zabučala na pozdrav. 
  
 Sme v meste, na námestí Sv. Trojice. Oproti je najkrajší dom - Obecný úrad v Krupine. Milá pani, ktorá stojí medzi stĺpmi vchodu, nám ochotne poradila, kadiaľ je najlepšie začať prehliadku mesta. 
 Hneď vedľa úradu je turistické informačné centrum, kde nám dali niekoľko informačných brožúr a kúpili sme si aj mapu mesta. Pani informátorka nám prezradila, že propagované podzemné chodby ešte nie sú sprístupnené verejnosti a o Tureckých studniach síce počula, ale nevie kde sú, lebo ich ešte nevidela... 
  
 Umenie na námestí, vo fontáne (ak to tak možno povedať...). Vari to nemá byť Sládkovičova Marína? Predstavoval som si ju trochu inak... 
  
 Rodný dom Andreja Sládkoviča (Braxatorisa) je v uličke za Obecným úradom. 
 Krupina je mestečko, kde sa veľmi nenachodíte, ak vám stačia historické zaujímavosti v centre mesta. Všetko je tu na dosah ruky. 
  
 Detail hlavy trojičného stĺpa na námestí. 
  
 Rímskokatolícky kostol s opevnením. 
  
 Kostolné okolie. 
  
 Veža. 
  
 Kríž pred kostolom. 
  
 Opevnenie kostola. 
  
 Hneď vedľa kostola a fary je táto moderna. Za ňou pokračujú staré hradby, mestský park a nemocnica. 
  
 Niet čo dodať - Mestská nemocnica. 
  
 Pekný a čistý, aj keď malý mestský park. 
  
 Z jednej strany história... 
  
 Príroda priamo v centre mesta. 
  
 Mestské hradby budované v rokoch 1551 - 1564. 
  
 Minulosť je súčasťou budúcnosti... Kto to povedal? 
  
 Aj taká býva realita - Pozor, zo strechy padá škridla... 
  
 V Krupine existuje aj Kultúra... Fakticky. 
  
 Evanjelický kostol nás vracia na námestie, do neďalekej malej reštaurácie so znamením zverokruhu "škorpion", s príjemnou obsluhou a výborným jedlom. Úloha dňa je splnená, ostanú spomienky... 
   
   
   

