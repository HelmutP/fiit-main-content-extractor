

 Dosť intenzívne som premýšľal, či má vôbec  zmysel začínať niečo s vysvetľovaním konkrétnych kvantitatívnych metód na tomto blogu, alebo sa im máme vyhnúť  kilometrovou obkľukou. No, ak vám však nedáme možnosť nakuknúť do kuchyne analytických a exploratívnych metód, asi ťažko si budete môcť predstaviť ako sa tvoria také tie pekné farebné jednoducho vyzerajúce grafíky. Ale ako vám to ilustrovať a vysvetliť, aby ste po prečítaní článku cez noc nekričali zo sna a od toľkého šoku si necrvkli? No je to výzva. Keďže ja výzvy lúbim, skúsim to podať čo najnázornejšie. 
 Čo je mi známe, tak minimálne nižšie spomínaná faktorová analýza nevznikala v gebulách matematikov,či štatistikov ale vznikla  v hlave psychológa, pričom ju zdokonalili a vypilovali vedci zo spoločensko-humanitných vedných oborov. Ááách, to boli  časy. Predstavte si, že by niečo také malo vzniknúť na našich omáčkovo zameraných odboroch. Hehe, ta šme še pobavili. Toľko srandy na začiatok hadám nie je ani zdravé. 
 Jej použitie je tiež dosť široké, od už spomínanej experimentálnej psychológie, cez marketing až po geo-, hydrochémiu, ekológiu a my vám budeme ukazovať jej použitie na našich zväčša takých skôr ekonomických príkladoch. Inak podobný princíp zníženia dimenzií používa jej slabovyvinutá sestra "Analýza hlavných komponentov" (Pre anglofilných uchylákov to je "Principal component analysis"). Využíva sa pri zostrojení dobre známeho indexu Dow Jones. 
 Odpoveď na vyššie spomínanú otázku o hodnotení missiek znie: „Podľa svojich preferencií predsa!" Tej a tej fyzickej črte priradí váš mozog  nejakú tú váhu a následne vy tú črtu podvedome  obodujete, pričom sa to zohľadní pri vytváraní rebríčka. Napr. Máte slabosť na dlhonohé? bum, hneď sú dlhonohé medzi prvými. Alebo máte slabosť na pekné papulky? bum,  hneď je čislo 9, 12 a 1 mimo hry. Nakoľko je táto črta pre vás veľmi dôležitá a všetky v tomto ohľade dosiahli mizerné hodnotenie. A tak ďalej. Váš mozog podvedome oskóruje jednotlivé pipenky a dokonca chápe aj rozdiely v tomto hodnotení. Niečo na spôsob, postaviť proti sebe Sklenaříkovú a Kelišovú a tváriť sa , že veď Kelišová je strieborná. 
 Ono nie je problém zoradiť cicušky vedľa seba, keď meráte len jednu črtu, teda aké má ktorá dlhé nohy alebo potom ich zoradiť podľa kolkátiek dudov, či podľa najkošatejše  vyjadrenej formulky o svetovom miery . 
 No ale my chceme niečo objektívnejšie, integrálnejšie, systémovejšie, fungujúce ako mozog, čo pohltí všetky tieto veličiny. My nechceme tri oddelené rebríčky missiek merajúce dudy, nohy a svetový mier, my chceme ten rebríček len jeden a aj aby premietal estetické rozdiely medzi nimi! A keďže ja ako analytik mám byť za chudáka asexuálneho hermafrodita, ja si tie váhy, ktoré obyvateľstvo k týmto oddeleným aspektom priradí, nemôžem len tak vycucnúť z prsta. Mne nezostáva nič iné, ako nato prísť inak. BTW. To scucnutie 3 kategórií do jednej sa volá zníženie dimenzie. Ono existujú aj veľmi jednoduché metódy, ako také rebríčky spraviť,ale nakoľko my sme velkí chlapci, my si vyberáme tie najvycibrenejšie. 
 Ak vám teda ide čisto o spravenie z 10 rebríčkov hodnotení len jeden integrálny,ktorý ukážete mame, že jaký/á ste king/queen, môžete použiť spomínanú „slabovyvinutú" Analýzu hlavných komponentov. 
   
 Tým, ktorí majú kuráž a veľkú predstavivosť patrí text č.1 a 2, slušňáci a matematickí asexuáli preskočte len k bodu 2: 
 1. Káždá z pôvodných premenných má v súbore objektov nejakú variabilitu, ktorú meriame rozptylom. Rozptyl je vlastne nositeľ informácie. PCA je ordinálna metóda (ordinála- priamka spájajúca dva združené body priemetu toho istého bodu), ktorá umožňuje redukovať počet dimenzií v euklidovskom znakovom priestore (definovanom vzájomne korelovanými kvantitatívnymi znakmi, resp. premennými), tak aby došlo k minimálnej strate informácie. Metóda nahrádza pôvodný súbor pozorovaných premenných súborom nových, vzájomne nekorelovaných, orthogonálnych „syntetických" premenných tak, že prvá nová súradnicová os (prvý hlavný komponent) je vedená v smere najväčšej variability medzi objektmi (štatistickými jednotkami). Druhá os je kolmá na tú prvú a je vedená v smere druhej najväčšej variability atď. Teda pôvodný súradnicový systém sa natáča do smeru max. variability, pričom euklidovské vzdialenosti zostávajú nezmenené. [1] 
 2.   Pekný koniec 
  Ľudská kreativita nemá hraníc  
 Fajn, takže, keď sme si každý podľa svojho vysvetlili tento matematický základ môžeme postúpiť od slabovyvinutej k obdarenejšej príbuznej. Faktorová analýza. PCA vysvetľuje len rozptyl, zatiaľ čo stará Faktorka ide ďalej. 
   
 Faktorová analýza sa snaží odhaliť skrytý faktor, ktorý determinuje dajme tomu vaše preferencie a správanie. Keď je niekto športový fanda, tak skôr bude jeho rebriček pozeraných kanálov  a programov inklinovať k pozeraniu eurosportu, či galaxysportu a zápasov ako TV paprike a Pošte pre teba. Touto metódou je možné odhaliť, či takéto skryté faktory existujú a predurčujú vaše správanie, takže v určitých smeroch budú výsledky viac skorelovaná s určitou črtou. 
 Na zvýraznenie tohto podvedome fungujúceho faktora sa používa v tejto metóde tzv. Rotácia. Rotácia krajšie znie, ale inak sa jedná o geometrickú transformáciu faktorových váh. 
 Jej rotačný princíp spočíva vo zvýraznení dominancie určitých faktorov. Predstavte si fotku "José na pláži". Západ slnka čarokrásne fantastický, ale vám na tej fotke furt niečo vadí. Ešte aj to malé nepodarené decko Vám do záberu vbehlo. Nechcete fotku "José vo výrazných  zelených trenkách s pohodenou otvorenou konzervou pašteky plus hlava susedovho malého Jana." Vy chcete len siluetu Josého a slnko! No čo urobíte? Ak vám to funkcie umožňujú a trochu sa pohráte, vytiahnete tie farby, o ktoré vam ide. Červenú oranžovú, ružovú  a neviem čo a zmeníte osvetlenie, aby to malé usmrkané dieťa bolo čo najmenej vidieť. Jednoducho chcete aby tá fotka bola hlavne o západe slnka. Tie zvyšné veci tam sice sú, čo už s nimi narobíme, ale sú nepodstatné a otom je rotácia. 
 Matematický backround tejto metódy si nechám pre seba, keďže myslím ,že oboznamovať vás s názvami pripomínajúce ten ostrý vysokotónový zvuk vrtačky u zubárky, či kvetnaté tituly rímskych cisárov veľa prospechu nenarobí. 
   
 Naučiť sa obe metódy aplikovať na softvéri nedá viac práce ako naučiť sa odrecitovať typológiu stratégií podľa Kotlera. Len v tomto prípade vás nikto nenúti nič bifliť, či nebodaj poznať do bodky matematický algoritmus, stačí mať "šajnu" a ostatné sa nalepí na vás pri vašom záujme tak rýchlo ako Misska na hokejistu.  A narozdiel od teoretickej znalosti 4 krokov ECR koncepcie sa niečo aplikačné naučíte a reálne to k niečomu je. 
   
 Lukáš Pastorek 
 Klub Dispersus 
 Step Out of Range 
   
 Pre tých, ktorí sa neboja, že si cvrknú: 
   
 http://support.sas.com/publishing/pubcat/chaps/55129.pdf 
 http://en.wikipedia.org/wiki/Factor_analysis 
 http://www.psych.cornell.edu/Darlington/factor.htm 
 [1] Stankovičová, I., Vojtková, M.: Viacrozmerné štatistické metódy s aplikáciami. Bratislava : Iura Edition, 2007. ISBN 978-80-8078-152-1. 
   
   

