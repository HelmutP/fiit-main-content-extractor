

   
 
 
  
 
 Tento týden v TV uveřejnili velmi smutný příběh pěkného pudlíka barvy aprikot starého asi 4 roky. Jeho labilní a psychicky naprosto narušený páníček jeho tělíčko omlátil o strom a bezvládný uzlík bolesti a bezvědomí pohodil do trávy. Pak vítězoslavně odešel z bojiště jako vítěz. On, pán Všemocný si poradil s malým pudlem, dal mu co proto a ukázal mu, kdo je tady pánem. Proč? Proč se rozhodl takto psíka jednou pro vždy zbavit? A proč si ho pořídil? 
   
 Proč si v moderní době lidé pořizují do moderního bytu domácí mazlíčky? Pes na zahradu k domku, prosím. Bude hlídat domek, plašit potenciální zloděje a slepice, nebo bude plašit kuny, aby neplašily slepice, bude rozhrabávat záhonky, nebo bude koukat z boudy, jestli se za oknem nemihne hlava páníčka. Prostě čtyři nohy a jedna hladová tlamka navíc, do koloritu zapadne a sežere, co zbude od oběda nebo co kdo hodí přes plot. Ale pejsek do bytu? Bude se tam umět chovat? 
   
 Tady je, zdá se, zřejmě zakopaný pes. Naivní potenciální páníček totiž netuší, že pes v rozrušení z osamocení může štěkat a kazit sousedské vztahy, že může demolovat nábytek a kazit partnerské vztahy, či dokonce že mu může být tak špatně, že v bytě pozvrací a pokálí koberec třeba i v každé místnosti, protože nechápe, kam může a kam ne, a hlavně je mi zle. A to ještě ke všemu i pes má své dny, takže když někde v okruhu kilometr hárá fena, dvě, tři i více, tak pes tyto své dny může trávit vytím, běháním a skákáním na okno, ignorováním plné misky, ale zato trpícím průjmem z ničeho, nespavostí a otravováním každou hodinu, že by rád šel ven. A to, prosím pěkně, každý pes, na velikosti nezáleží, takže i malý pes je pak velký problém. Problémy se skutečně nemocným psem, srdcařem nebo trpícím třeba artrózou takticky pominu. 
   
 I když potenciální páníček bude umět psovi shánět potravu, vyměňovat čerstvou vodu v misce, ošetřit zraněnou tlapku, skočit s ním jednou ročně na povinné očkování a promine mu znečištěný koberec, případně rozcupované hračky, stále je to málo. Páníček musí svého psa ovládat. Opačná kombinace, že by pes ovládal páníčka, by mohla být u větších plemen vražedná, to se nesmí stát. Nicméně i malý pes, není-li ovládán, dokáže způsobit velkou kalamitu. Jak? Třeba hupsne do jízdní dráhy cyklistovi nebo rovnou motoristovi, který ze soucitu se psem neuváženě dupne na brzdu a … podle situace leccos. 
   
 Vypadá to, že pes na svého páníčka klade mnoho požadavků. Jednak schopnost pečovat a starat se, jednak ekonomickou způsobilost platit veterinární péči a potravu, jednak toleranci a ke všemu ještě dovednost ovládat. Pořád to ale není vše. Páníček musí umět mít svého mazlíčka rád. Pes totiž nemá vypínač on/off, nebo play/off, jak by někdo v dnešní přetechnizované době rád očekával. Je to živý tvor s vlastní povahou a duší, částečně nevypočitatelnou. Psa v bytě nelze šoupnout na zimu do komory nebo za skříň, ani uschovat na dvě tři hodinky do ledničky. Některý pes dokáže sice dvě tři hodinky spát na svém pelechu, ale někteří psi se dokáží motat pod nohy v nejnevhodnější dobu vždy a pravidelně. Zatím nebylo zjištěno, že by snad nějaký pes trpěl ponorkovou nemocí na ostatní členy domácnosti. Co bylo v tomto směru zjištěno o člověku, je všeobecně známo. Proto páníček psa v bytě musí oplývat láskou ke svému čtyřnohému parťákovi, jinak vztah nebude fungovat. 
   
 Jaké vlastnosti musí mít člověk, který pětikilového pudlíka meruňkové barvy dokáže otřískat o strom do bezvědomí zvířete? Musí to být psychická troska, egoistický, labilní a citově plytký tvor, který jménu člověk dělá ostudu.  

