
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Holko
                                        &gt;
                Nezaradené
                     
                 Nepotrestané zločiny, alebo hrubá čiara za minulosťou? 

        
            
                                    15.4.2010
            o
            13:24
                        (upravené
                15.4.2010
                o
                13:41)
                        |
            Karma článku:
                10.65
            |
            Prečítané 
            1674-krát
                    
         
     
         
             

                 
                    Je pre mňa skľučujúce, keď si spomeniem na spoločenské predsavzatia novembra 1989 a konfrontujem ich so súčasnou mravnou biedou, ktorej sme svedkami deň čo deň. A je to ešte horšie keď porovnávam situáciu u nás a našich susedov. Napríklad v Čechách, kde paradoxne napriek  relatívnej životaschopnosti komunistov, sa spoločnosť dokáže vysporiadať s minulosťou. Aspoň čiastočne, no rozhodne lepšie než my.
                 

                 V 89-om som mal iba 17 a našťastie som priamo nepocítil najtvrdšie zločiny komunizmu, no dokážem si predstaviť, že nedávne odsúdenie prokurátorky Poledňákovej, žalobkyne v justičnej vražde  Milady Horákovej, musí pôsobiť ako balzam na dušu všetkých politicky stíhaných. Proces bol v ČR veľmi sledovaný a jeho výsledok sa rodil dlho a ťažko, no predsa. http://www.novinky.cz/domaci/165324-prokuratorka-milady-horakove-nastoupila-do-vezeni.html  Principiálny význam tohto rozsudku výrazne prevyšuje nad efektom trestu ako takého, no práve o to ide. Koľko zločincov, aktérov politických justičných vrážd a prenasledovaní bolo odsúdených u nás? Hrozí vôbec niečo také?   Ako ďalšiu lastovičku, ktorá, dúfam, prinesie leto, som postrehol nedávne rozhodnutie Olomouckého súdu vo veci násilnej kolektivizácie, za ktorú taktiež po prvýkrát v českej histórii padol trest. http://www.lidovky.cz/soud-poprve-potrestal-komunistu-za-kolektivizaci-fww-/ln_domov.asp?c=A100407_223041_ln_domov_tai   A opäť paralela z našich končín. Nielenže nikto nebol a zrejme ani nebude odsúdený za násilné združstevňovanie, ale súdruhovia dokázali okradnúť ľudí hneď 2-krát. Najprv v 50-tych a potom v 90-tych rokoch za privatizácie. Veď, kto si myslíte, že sú tí, čo dnes predstavujú veľkopodnikateľov vysávajúcich všetky dotácie a kričiacich po zachovaní intenzívneho poľnohospodárstva ala socializmus. Maloroľníkov a farmárov by najradšej udupali pod zem, vraj škodlivý environmentálny prístup tlmiaci výrobu. Majú v tom jasno ako vždy, ibaže za tie roky vládnutia a striedania jedného schopnejšieho ministra za druhého, nenašli ani minimum riešení pre poľnohospodárstvo a okrem tupých hesiel a fráz si taktiež nezabúdajú šiť zákony a hlavne dotácie na mieru.    Keď si dobre spomínam, celé to začalo  niekedy za Havla frázou „nebudeme jako oni" alebo „uděláme hrubou čáru za minulostí". Len koho sa opýtal, či chce takúto čiaru urobiť. Pýtali sa napríklad politických väzňov? Pýtali sa okradnutých roľníkov?   A čo sa stalo ako jedna z prvých vecí po rozdelení Československa? Na Slovensku sa zrušil najpodstatnejší pilier vysporiadania s minulosťou - lustračný zákon. A tak dnes, na hambu celého sveta, ale hlavne na našu škodu, je verejný život opäť v rukách komunistických karieristov, ktorí sú ochotní pre svoj prospech zapredať úplne všetko. Naša mládež je vychovávaná a vzdelávaná v školách, kde sú ich magnificencie a spectability evidentnými kolaborantmi zločineckej ŠtB. A tak ďalej, a tak ďalej...   Čiara za minulosťou? Áno, bohužiaľ červená. Pritom u našich susedov je nemysliteľné, aby sa niekto bez čistého lustračného osvedčenia stal rektorom alebo dekanom. Pritom komunisti tam tradične majú celkom slušné zastúpenie v parlamente. Asi to bude tým, že u nás z pôvodnej KSS zostalo iba torzo a všetci „šikovní" aparátnici a karieristi brilantne prezliekli kabáty a dnes vysedávajú v kostoloch v prvých laviciach a sú z nich sociálni demokrati, národní socialisti a bohvie kto všetko.    Už sme si pomaly zvykli. Aspoň tí starší. Mladí si nepamätajú a tak si nemuseli zvykať. A to je veľké riziko pre spoločnosť, ktorej najvyšší činitelia bagatelizujú zločiny komunizmu a význam slobody. Nebezpečné pre spoločnosť, kde majú opäť zelenú súdruhovia, ktorým sa prepečie podvod, krádež, ... ublíženie na zdraví...všetko.   Ak nechceme dopustiť periodicitu dejín a vrátiť „staré časy", musíme sa zobudiť. A predložiť účet tým, ktorí naďalej prospievajú na náš úkor a profitujú z našej nedôslednosti. Boľševik je totiž škodná, ktorú nestačí zahnať do kúta. Nikto nie je lepšie trénovaný vo vyčkávaní v kúte a čakaní na svoju príležitosť ako títo vetrom ošľahaní normalizátori. Nejde len o hrabanie sa v minulosti, čo mimochodom opovrhujúco tvrdia práve oni, ide o budúcnosť.  A očistu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Probiotické potraviny – inovácia aj pridaná hodnota.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Drahé potraviny - výsledok spoločnej poľnohospodárskej politiky EÚ.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Spoločná poľnohospodárska politika EÚ – fair play a la Brusel.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Sme krajinou psieho utrpenia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Mlieko z automatu - prevárať či neprevárať?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Holko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Holko
            
         
        holko.blog.sme.sk (rss)
         
                                     
     
        Veterinár, vedec, odborný poradca v poľnohospodárstve. 
                                                      

Feedjit Live Blog Stats

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    26
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2997
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Moja spoločenská aktivita
                                     
                                                                             
                                            Moja práca
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




