
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Orient  
                                        &gt;
                Juhovýchodná Ázia
                     
                 Z čoho sa varí v Thajsku 

        
            
                                    27.5.2010
            o
            18:23
                        (upravené
                27.5.2010
                o
                16:45)
                        |
            Karma článku:
                3.95
            |
            Prečítané 
            970-krát
                    
         
     
         
             

                 
                    Keď sa mi včera večer znova nepodarilo vtesnať thajskú politiku do predpísaného počtu riadkov, už som chcel tému vzdať. Ráno som však cestou do práce prešiel popri páchnucom vyhorenom nákupnom centre, ktoré tu zostalo od posledných nepokojov, a tak som sa nakoniec rozhodol nechať iné témy na neskôr. Tu je teda niekoľko neučesaných poznámok ku krátkym TV šotom thajských vojakov krčiacich sa za obrnenými transportérmi a s lietajúcimi molotovovými kokteilmi nad hlavami.
                 

                 Všeobecný vzorec thajského politického diania od r. 1932, keď sa krajina stala konštitučnou monarchiou, politológovia popisujú takto: (obvinenie vlády z korupcie a neefektívnosti) uzurpovanie moci, nová ústava, parlamentné voľby, kríza, (obvinenie) uzurpovanie moci, .... Toto viedlo k osemnástim ústavám, osemnástim vojenským prevratom a vyústilo až do udalostí dnešného dňa. Z čoho sa to všetko varí?    Dôležitou zložkou je pretlak centier politickej moci.    Thajsko, ako jediná krajina Juhovýchodnej Ázie, nebolo nikdy formálne kolonizované a tento prvok národnej hrdosti má za následok aj to, že tradičné centrá reálnej politickej moci ani ich vnímanie neboli narušené. Na rozdiel od okolitých krajín koloniálna vláda nikdy nespochybnila zvrchovanosť monarchu. Kráľ sa teší bezprecedentnej popularite a náboženskej úcte (čo neplatí v rovnakej miere o ostatných členoch kráľovskej rodiny) a bezpochyby, všetky kontroverzie bokom, je symbolom, kde sa koncentruje povedomie národnej identity a občianskej lojality Thajčanov.    Armáda, ako ochranca monarchie, náboženstva a štátu, je tradične prestížnou inštitúciou. Do r. 1973 mala armádna elita takmer suverénne postavenie v politickej štruktúre krajiny. Monarchia bola síce armádou vnímaná ako politický rival, ale keďže sa jej nikdy nepodarilo anulovať vnímanie kráľa ako prirodzenej politickej autority, využívala charizmu monarchie na sledovanie svojich vlastných záujmov a legitimizovanie vojenského režimu. Karty sa obrátili počas študentskej revolúcie v októbri 1973, keď kráľ otvorene vystúpil z tieňa vojenskej elity, priklonil sa na stranu prodemokratických síl a začal budovať svoju prestíž nezávisle na armáde. Štátne inštitúcie pomáhali utvárať formu „demokracie s kráľom ako hlavou štátu“ a povedomie monarchie ako kvázi-politickej inštitúcie potrebnej na ochranu demokracie.    Popularita armády mala svoje vrcholy aj pády, dnes je ale jednoznačne napoly nezávislým, na frakcie rozdeleným hráčom v politickom boji o moc, aj keď jej lojalita monarchii a národu je permanentne deklarovaná.    Politické strany sú ideologicky nevyhranené (monarchia a demokracia sú univerzálnou rétorickou témou) a parlament funguje skôr na dohodách medzi mnohými fluktuujúcimi frakciami naprieč straníckym spektrom. Kupovanie hlasov je tradičnou časťou parlamentných volieb, rovnako ako kupovanie poslancov a celých strán (ako to bolo napríklad v prípade Thaksinovej strany Thai rak Thai) v rámci parlamentu. Toto je dôsledok živého vplyvu tradičného systému patrón – klient na thajskú psyché. Patrón (zamestnávateľ, nadriadený, spoločensky vyššie postavený, politický reprezentant ...) udeľuje klientom benefity podľa pravidiel, ktoré sú len vágne definované. Imperatív lojality klienta je rovnako neostrý. Polícia – podľa výskumu Čulalongkorn University najskorumpovanejšia a najmenej dôveryhodná inštitúcia v krajine – sa správa podľa stredovekého systému sakdina, kde príjem toho, kto v mene panovníka vyberal dane, vykonával kontrolu a udeľoval povolenia a privilégiá, pochádzal od klientov, nie od štátu. Slovo „demokracia“ sa v stretoch politických síl využíva ako univerzálny štít na obhajobu vlastnej politiky. Vojenský režim pod vedením poľného maršála Sarita Thanarata zaviedol v 50tych rokoch termín „demokracia thajského štýlu“. Pre tento typ demokracie, ktorá nemá byť len implantátom zvonka, ale má vyhovovať thajským podmienkam, platí: 1. ústava nesmie byť len duplikátom západných ústav a voľby nie sú nevyhnutné, 2. demokracia musí prispievať k politickej stabilite, 3. musí byť sprevádzaná liberálnym ekonomickým systémom, minimálne riadeným štátom, 4. musí podporovať monarchiu, 5. musí podporovať národný rozvoj. Protithaksinovská Ľudová aliancia za demokraciu (PAD, tzv. Žlté košele, ktoré v nov. 2008 obsadili a na týždeň uzavreli dve bangkokské letiská ) navrhovala zredukovanie volenej časti dolnej komory parlamentu na 30%, pričom zvyšok mal byť doplnený „penzionovanými úradníkmi a dôležitými ľuďmi“, lebo nevzdelané, ľahko korumpovateľné masy najmä vidieckeho obyvateľstva nedokážu zodpovedne rozhodovať o chode štátu. Okrem toho armáda má neustále dohliadať nad politickým dianím a Ministerstvo obrany má byť vyňaté spod politickej kontroly.   Expremiér Thaksin Čhinavat (podporovaný „Červenými tričkami“) zas kládol mandát más ako protiváhu svojmu flagrantnému zneužívaniu demokratických inštitúcií a budovaniu vlastnej kleptokratickej administratívy s klasickou štruktúrou patrón – klient.    Toto všetko, spolu s ekonomickým boomom a pádom, spoločensky aktívnymi a krízami postihovanými náboženskými inštitúciami, strednou a vyššou strednou vrstvou nespokojnou so svojim politickým postavením, problémom chudoby najmä v severovýchodnej časti Thajska a troch južných „moslimských“ provinciách, kvasí pod pokrievkou thajskej kultúry uhladeného spoločenského kontaktu vyhýbajúcemu sa priamej konfrontácii až do chvíle, keď hrniec prekypí.       Miloš Hubina, Mahidol University, Bangkok         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Suši sa môže jesť rukou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Civilizácie – zániky a vzniky...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Mačča – kráľ čajov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Tea tree nie je čajovník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Orient   
                                        
                                            Mozaikové mesto
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Orient  
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Orient  
            
         
        orient.blog.sme.sk (rss)
         
                                     
     
        Spája nás FascinÁzia; kultúry a krajiny Orientu sa stali súčasťou našich životov. Spolupracujeme v rámci OZ Pro Oriente (www.prooriente.sk)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1282
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Blízky východ
                        
                     
                                     
                        
                            Ďaleký východ
                        
                     
                                     
                        
                            India
                        
                     
                                     
                        
                            Juhovýchodná Ázia
                        
                     
                                     
                        
                            Všeobecné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




