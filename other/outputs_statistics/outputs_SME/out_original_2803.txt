
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lenka Haluzová
                                        &gt;
                Zrkadlo mojej mysle
                     
                 Čaro našej knižnice 

        
            
                                    17.3.2010
            o
            18:25
                        (upravené
                17.3.2010
                o
                18:57)
                        |
            Karma článku:
                6.75
            |
            Prečítané 
            619-krát
                    
         
     
         
             

                 
                    Naposledy som ju navštívila pred dvomi rokmi.A zrazu som tam stála,ani neviem ako.Skrátka som cítila neuveriteľnú potrebu navštíviť ju,našu malú dedinskú knižnicu.
                 

                 Niečo ma tam opäť pritiahlo.Knihy som si tam požičiavala už od útleho detstva a naspamäť som poznala všetky police.S tetou knihovníčkou som dokázala presedieť a klebetiť takmer celé popoludnie.   Časom sa to však zmenilo a ja som na knižnicu zanevrela.No dnes nechápem prečo.Tá zvláštna vôňa starých kníh,zaprášené police a vždy usmiata knihovníčka sú krásna spomienka na moje detstvo.Dnes som si prezerala rôzne knihy a našla som aj také,ktoré by mali obrovskú historickú hodnotu.V knihách je skrytý kus histórie.,,Knihám sa internet nikdy nevyrovná!" uvedomím si.Vzápätí sa pristihnem pri tom,ako prehrňam strany na svojich obľúbených detských knížkach.Všetko je tam tak,ako kedysi.Nič sa nezmenilo.Dokonca aj Anna zo zeleného domu stále leží na tom istom mieste.Pousmejem sa,predsa len niečo sa zmenilo.Police,ktoré sa mi kedysi zdali obrovské a mohutné,dnes vyzerajú akoby sa rokmi zcvrkli.Och,ja zbožňujem knihy.Dávajú mi nekonečný priestor na sebarealizáciu.Siaham po nich,či ráno alebo večer,v každej časti dňa.   Ako veľmi si ich vážim.,,Raz napíšem svoju!" vykríknem a knihovníčka odpovie:,,Na to sa už veľmi teším."Usmejem sa a predstavím si krásny farebný obal s mojim menom vpísaným zlatým písmom na mieste autora.Potom už len siaham po prvej knihe,ktorú vidím a čítam si stručný obsah:*V podstate na ničom nezáleží.Keď si nešťastná,spýtaj sa sama seba:,,Skutočne na tom záleží?"(Helen Fieldingová)*   A preto tak milujem knihy.Ako keby tušili,v akej sa nachádzame situácii.Ako keby vedeli,akú vetu zvoliť,aby nás upokojili.Táto veta mi totižto otvorila oči a neskutočne ma potešila.A ja viem,že to,čo ma dnes prilákalo do knižnice,nebola len čistá náhoda.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Haluzová 
                                        
                                            Symfónia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Haluzová 
                                        
                                            A že my sme nevychovaná mládež!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Haluzová 
                                        
                                            Ako vlny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Haluzová 
                                        
                                            Ach,ten výber alebo múza so supermarketu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Haluzová 
                                        
                                            Typický večer
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lenka Haluzová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lenka Haluzová
            
         
        haluzova.blog.sme.sk (rss)
         
                                     
     
        Svojej fantázii dávam krídla a srdcu dvere dokorán :o)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    551
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zrkadlo mojej mysle
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Umberto Eco,Boris Filan-Wewerka,Jozef Banáš-Kód 9
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Ludovico Einaudi
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




