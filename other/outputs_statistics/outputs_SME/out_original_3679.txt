
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Kamil Krnáč
                                        &gt;
                Spoločnosť
                     
                 Ficov meč solidarity 

        
            
                                    1.4.2010
            o
            17:21
                        |
            Karma článku:
                22.14
            |
            Prečítané 
            3356-krát
                    
         
     
         
             

                 
                    Ako je všeobecne známe, náš pán premiér bojuje za práva chudobných tým, že trochu menej chudobných ozbíja cez vyššie dane. Skutočne bohatým totiž jeho zbrane neublížia, chudobným nijako nepomôžu, no jeho spasiteľský imidž vodcu sa tým zas a znova utešene vylepší.
                 

                 Robert Fico, ktorý bol v roku 2002 ešte stúpencom rovnej dane a v roku 2006 vyhlásil, že tí, čo majú miliardové zisky musia platiť viac, v súčasnosti hlása:   "Slovenský daňový systém je pre firmy príťažlivý, nechceme ho meniť....My zásadne nechceme meniť daňový systém z hľadiska právnických osôb, kde si myslíme, že je treba zachovať určitú atraktivitu slovenského prostredia pre zahraničných investorov....Chceme prehlbovať prvky solidarity a progresivity, pokiaľ ide o daň z príjmu fyzických osôb."   Na Slovensku môže podnikateľ pôsobiť buď v pozícii právnickej osoby (akciovky, eseročky, a pod.) alebo fyzickej osoby (živnostníci). Z jeho slov mi teda vyplýva, že ak podnikáte ako právnická osoba - ideálne zahraničná, potom ani pri vašich miliardových ziskoch nebudete terčom spravodlivého sociálneho hnevu pána Fica.   Ak ste ale na Slovensku podnikateľom-živnostníkom, budete musieť zabudnúť na akékoľvek príjemné podnikateľské prostredie. Nikoho trápiť nebude, že napríklad zamestnávate ľudí a ani to, že vo vašom prípade hrozí oveľa menšie riziko, že sa jedného krásneho dňa odsťahujete za lacnejšou pracovnou silou niekam na východ. Ak náhodou zarobíte 4000 eur, ktoré ste plánovali investovať do rozvoja svojej firmy, stávate sa podľa rétoriky Roberta Fica skvelým adeptom, aby sa o toto vaše donebavolajúce bohatstvo sociálny štát náležite postaral.   Už zjavne dávno neplatí volebný program strany SMER z roku 2006, ktorý popri 19%-nej dani pre právnické osoby navrhoval "zaviesť samostatnú vyššiu sadzbu dane z príjmu vo výške približne 25% na prirodzené monopoly, dominantné podniky, finančné a bankové inštitúcie". Skutočným miliardárom sa asi takéto niečo nepozdávalo, a tak si Robert Fico vymyslel pre voličov divadelnú hru novú. V hlavnej úlohe ako udatný rytier v nej nemilosrdne skolí hnusnú beštiu tak, aby si nikto nevšimol, že beštia bola papierová a meč drevený. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zákonom chránené gorily
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zmeňte požiadavky novembrového protestu, prosím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Najvyšší kontrolný úrad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Mám pokračovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Vráťme moc ľuďom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Kamil Krnáč
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Kamil Krnáč
            
         
        kamilkrnac.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




