
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Kmecová
                                        &gt;
                Nezaradené
                     
                 Prvý pozdrav zo Štokholmu 

        
            
                                    15.4.2010
            o
            21:30
                        |
            Karma článku:
                5.79
            |
            Prečítané 
            568-krát
                    
         
     
         
             

                 
                      Slnko vykuklo spoza mrakov, dni sú dlhšie, sneh sa roztopil. Toto sú potrebné atribúty na to, aby majitelia veľkých i malých kaviarní a reštaurácií vyložili na chodníky stoličky prútené, drevené, alebo iba také obyčajné, priložili k nim stolíky a stoly, ktoré pasujú k stoličkám. A možno niekedy i nie. Plus kvety, ktoré  nesmú chýbať a samozrejme teplá, mäkučká deka  ladiaca k vytvorenému exteriéru. To, že vonkajšia teplota nedosahuje ešte ani 10°C, nevadí. Potrebné slnko je tu a deka pred chladom ochráni. To je Štokholm. Hlavné mesto Švédska. Hovorí sa o ňom, že je Benátkami severu. Niečo na tom je. Štokholm je očarujúce mesto. Je to kráľovské mesto a Štokholmčania sú naň patrične hrdý. Tento rok je pre Štokholm ešte výnimočnejší. Kráľovská rodina totiž vydáva korunnú princeznú Viktóriu.
                 

                 
  
   K životu v Štokholme, ale aj v celom Švédsku patrí káva a kaviarne. Okrem pozdravu HEJ HEJ (v preklade znamená ahoj) , ktorý som sa naučila ešte pred príchodom do Švédska, to  bolo slovo FIKA, ktoré ma zaujalo. Fika  vo veľmi voľnom preklade znamená oddych, stretnutie s priateľkou alebo priateľmi  pri káve s  koláčikom (môže byť i čaj, v krajnom prípade). Malé pracovné stretnutie - FIKA. Ale hlavnú úlohu tu zohráva kaviareň, káva a niečo sladké.  Rozobrať pojem FIKA je celá veda. Na to nestačí ani jedna káva. Ako mi povedala moja švédska  kamarátka, to má oveľa, oveľa hlbší význam.   Priznávam sa, mám rada kaviarne a mám rada FIKA. Rada v nich vysedávam, pozorujem ľudí alebo len tak nasávam atmosféru mesta. Zvlášť si vyberám také kaviarne a reštaurácie, ktoré vedia spojiť príjemnú atmosféru, dobrú kávu alebo nejakú chutnú pochúťku so zaujímavým interiérom. Vtedy nepozerám na cenník. Keďže nejaké dobré a zaujímavé kaviarne už mám odskúšané, rada by som Vám ich odporučila.   Ako som už spomenula, je tu jar, prichádza leto a poniektorí z Vás zavítajú ako turisti do rôznych miest Švédska, a predovšetkým do Štokholmu. A práve pre Vás by bolo možno zaujímavé vedieť, kde sa dá oddýchnuť pri káve, zahasiť smäd alebo niečo malé i veľké si zajesť. Možno nielen  dať si potrebnú dávku kofeínu do tela a niečo pod zub, ale i pohladiť dušu a oči. Budem sa snažiť priblížiť Vám prednostne sieť štokholmských kaviarní a reštaurácií, ktoré sú z môjho pohľadu zaujímavé a kvalite, a zároveň v blízkosti najčastejších turistických zastavení.   Takže HEJ DO (dovidenia) a prvú FIKA si dáme v sieti kaviarní Coffee House by George.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Berns Salonger – Stockholm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Espresso House - www.espressohouse.se
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Cafe Foam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Coffee House by George
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Kmecová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Kmecová
            
         
        kmecova.blog.sme.sk (rss)
         
                                     
     
        Slovenka momentálne žijúca v Škandinávií. Zaujímam sa o interiérový dizajn, bývanie, životný štýl v tejto časti Európy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    720
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




