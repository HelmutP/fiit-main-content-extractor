
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Steven Nagy
                                        &gt;
                Vtipné
                     
                 Odporná fajta! 

        
            
                                    24.10.2007
            o
            6:03
                        |
            Karma článku:
                23.13
            |
            Prečítané 
            11097-krát
                    
         
     
         
             

                 
                    Letisková hala Heathrow. Stovky cestujúcich čaká v dlhej fronte na poslednú kontrolu dokumentov pred nástupom do Jumba British Airways. Väčšinou Európania, menej Indov a sem-tam čierni Afričania. Mrs Boer, päťdesiatnička letí späť do Johannesburgu v Južnej Afrike po návšteve v Londýne. Ovešaná zlatom a bižutériou, s perfektným účesom, manikúrou, módne oblečená tiež tam stojí. Nazlostená, že musí tak dlho čakať. Asi tretí pred ňou vo fronte stojí  šedivý  černoch! Skoro žiadna fronta pre  biznis a prvú triedu, iba hŕstka bielých. A sú uprednostnení! Mala som letieť v biznis triede, si myslí, lebo takto ktovie, koho budem mať vedľa seba? Po kontrole pasu a miestenky prešla úzkou chodbou a rampou k dverám lietadla. Kabína už bola  skoro plná, niektorí cestujúci nakladali príručné batožiny do skriniek nad sedadlami, ale väčšina už sedela a sledovala prichádzajúcich,  pripravovala si pohodlie na dlhý let. Mrs Boer skúmajúc svoju miestenku sa ťahala k svojmu miestu.
                 

                 
  
                    Ale...čože?!  To  nie je možné!!  To akoby na zlosť!!! Miestenka ju určovala  sedieť   presne vedľa toho  starého čierneho kafíra, ktorý stál pred ňou vo fronte! Sedieť a  trpieť  skoro jedenásť hodín  letu! Vedľa odporného kafíra!              Kýva na  letušku, ktorá  sa prebrodí k nej  v tej mase ľudí  medzi sedadlami. Mrs Boer  zrúkne na ňu:  “Nevidíte, čo sa tu robí? Dali ste ma sedieť vedľa černocha!”  zasyčala.  “Nebudem predsa sedieť vedľa niekoho z takej odpornej fajty. Nájdite mi iné sedadlo!”              Letuška ju utíšila: “Idem sa podívať...... možno vám nájdeme niečo iné.”              Starý Afričan si iba vzdychol, skromne sklopil oči a  s obavami čakal  čo sa stane. Celý život bol zvyknutý na podobné osočovanie.                 Mrs Boer si sadla iba na kraj sedadla, však to len na krátko. Kradmo vrhla   pohľad na ujka a čakala s príručnou batožinou v rukách na návrat letušky.              Tá sa za nejakú chvíľu  vrátila a hovorí:              “Bola som za kapitánom a ten mi povedal, že niet ani jediného voľného sedadla v turistickej, ba ani biznis triede, ale ešte stále máme jedno voľné miesto v prvej triede.”              Mrs Boer sa natešene zahniezdila  ale než  stačila reagovať, letuška pokračovala:              “To je celkom neprípustné  aby naša spoločnosť dovolila hocikomu  prejsť z turistickej triedy  do prvej.  Ale v tomto prípade kapitán sa rozhodol, že urobí  výnimku.   Bolo by predsa len  škandalózne,  sedieť vedľa takej  odpornej fajty.”  Potom sa obrátila na skrúšeného  Afričana:                 "Preto pane, bolo by naším potešením, keby ste si vzal  svoju príručnu batožinu a  šiel so mnou. Máme pre vás miesto v prvej triede.”                  Ostatní pasažieri, ktorí boli v blízkosti a boli svedkami celého nechutného incidentu, sa v tom momente postavili  na nohy a hlasne aplaudovali krásnemu gestu kapitána lietadla.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Tajomný Milionár
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Slovenské predavačky - úprimný nezáujem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Skutočná Austrálska "Love Story"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Hanbite sa pán M.!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Lietajúce sosáky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Steven Nagy
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Steven Nagy
            
         
        stevennagy.blog.sme.sk (rss)
         
                        VIP
                             
     
        Don't cry for me Argentina.. I'm on my way to see you soon.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    115
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poznávacie
                        
                     
                                     
                        
                            Vtipné
                        
                     
                                     
                        
                            Smutno-vážne
                        
                     
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Informačné
                        
                     
                                     
                        
                            Existencionalistické
                        
                     
                                     
                        
                            Osobné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Fastball
                                     
                                                                             
                                            James Blunt
                                     
                                                                             
                                            Jason Mraz
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivka Vrablanska
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľudmila Schwandtnerová,
                                     
                                                                             
                                            Martin Marušic
                                     
                                                                             
                                            Jozef Klucho
                                     
                                                                             
                                            Jozef Javurek
                                     
                                                                             
                                            Boris Burger
                                     
                                                                             
                                            Ján Babarík
                                     
                                                                             
                                            Natália Bláhova
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Počítače
                                     
                                                                             
                                            Zo zahraničia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Deň na ceste okolo sveta na Novom Zélande
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Vodičov pokutujeme aj za drobnosti, ale ostatných nie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




