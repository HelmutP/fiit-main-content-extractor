

   
  Nuž čo, nechala som ho odísť a čakala som ďalej, medzi tým som si urobila pár fotiek a dala sa do reči s jedným Kanaďanom. Tiež sa chystal na ryžové polia, staré 2000 rokov a vysvitlo, že bol aj v Bratislave a už 16 rokov si len tak cestuje po svete. Guide sa neobjavil ani do 7.00, tak som sa rozhodla, že ďalší jeepney si už odísť nenechám. Vedno s Kanaďanom a ďalšími cestujúcimi sme teda vyrazili. 
  
 Pre Filipíny typický dopravný prostriedok - jeepney. 
    Po hodine a pol sme prestúpili v Bontocku na autobus do Banaue. Ten išiel podobnou horskou cestou ako ja predchadzájúci deň do Sagady, len nie takou strmou. Po ceste som sa zoznámila s Bárou a Jirkom od susedov a nebola som už jediný nedomorodý cestujúci v autobuse ako po dva predchádzajúce dni.     
  
 Takýto pohľad nám umožnil autobusár ešte pred vystúpením. 
  
 Dorazili sme do dediny  Banaue a zisťovali, ako je to s ryžovými poliami, ak by sme sa chceli  posunúť ďalej ešte v deň. Tie pravé ryžové polia by sme uz nestihli, tak sme sa  rozhodli zostať na noc. Ja som sa síce plánovala vrátiť do Baguia a  českí spolubratia mali namierené do Viganu, ale také niečo, zapísané v  UNESCO, si predsa nemôžeme nechať ujsť. My traja sme si prenajali jeepney a  vybrali sa do sedla, odkiaľ sa na polia ide. Cestou sme prechádzali  okolo vanu zapadnutého do blata. Ďalšia cesta ala Indiana Jones. Ale už  som si zvykla. V sedle nás vyhodili, miestni sa hneď ponúkali za guidov a  jeden nám sľúbil, že ak sa do 5 pm vrátime, zoberie nás späť do dediny. 
   
  
 Opäť pár krokov bližšie k ryžovým poliam. 
  
 Keď sa prechádzate cez polia, neveríte vlastným očiam, že tam naozaj  ste. 
  
 Rastlinka, ktorá je symbolom šťastia pre kmeň Ifugao. 
 Čakala nás džungľa, neopísateľné ryžové polia a na konci cesty dokonca vodopád. Išli sme nakoniec bez guida. Domorodci vraveli, ze to trvá tak hodinu k poliam a my sme to zvládli aj so zastávkami na fotenie do pol hodiny. Zistili sme si, koľko to trvá k vodopádu a rozhodli sme sa, že do tej 5 pm by sme to mali otočit. Polia boli fantastické, ťažko sa rozhodnúť, či ma viac uchvátili ako ostrovy v okolí El Nida, ale aj keby som už nič iné nevidela, táto cesta stála za to. Ja som nemala so sebou plavky, ale kamoši sa okúpali a švihali sme spät, lebo sme sa chceli dostat spať. Vždy som bola vpredu, tak sme sa dohodli, že ja pôjdem prvá a zastavím vodiča, aby počkal aj Báru a Jirka. Tak sa aj stalo, do stanoveného času som bola naspäť v sedle a na spolucestujúcich sme ešte čakali asi hodinu. Vodič si vypýtal príplatok, lebo sa stmievalo a cesta bola nebezpečná. Čo sme mali robiť, nemali sme na výber. Aspoň nás odviezol do hostela Las Vegas. Ja som ešte naháňala internet a potom som bez večere zalomila, lebo som bola uťahaná. 
   
 Takto vyzerajú malé ryžové rastlinky. 
   
  
 A takto ľudia, ktorí sa o ne starajú. 
   
  
 Bonus k ryžovým poliam, vodopád. 
 V nasledujúce ráno sme nasadli na autobus smerujúci do Baguia, kamoši vystúpili asi o 2 a pol hodiny skôr a menili smer na Vigan. Do Baguia som dorazila asi o 5 pm, ďalší deň zabitý cestovaním, keďže ešte troška svietilo slnko, zamierila som do botanickej zahrady, keď som už bola v meste kvetov a festival PANAGBENGA trval ešte 2 dni. Pred vchodom som sa cvakla s tetuškami a dvoma ujkami z kmeňa Ifugao  žijúcimi na mieste, odkiaľ som práve prišla v tradičnom oblečení a do tmy som sa ešte prechádzala mestom, až som narazila na SM obchodné centrum, kde bolo napísane free wifi. Viac mi nebolo treba. O 11 pm som mala ísť na autobus späť do Manily, takže som mala nejaké 2-3 hodinky na surfovanie. Iphone bol takmer vybitý, tak som si najskôr musela kúpiť nabíjačku, lebo zástrčky neboli európske ako u Thei. Zistila som čo nové doma, uploadla fotky a išla hľadať stanicu, odkiaľ odchádzal môj autobus. 
  
 Baguio počas festivalu Panagbenga. 
  
   
  
 Pôvodní obyvatelia kmeňa Ifugao pred botanickou záhradou. 
  
 Pohľad na botanickú záhradu tesne pred zatvorením. 
  
   
 
     

