

 Podľa Roberta Fica predstavitelia novej stredopravej koalície na neformálnom rokovaní v parlamente arogantne oznámili, že preberajú moc v štáte a jeho strane dávajú k dispozícii nejaké pozície v NR SR. "Prekvapuje nás, že odmietajú najelementárnejšie požiadavky opozície," povedal. Pozrime sa teda na to, aké sú tie "najelementárnejšie požiadavky" opozície: 
 1. Požiadavka na miesto predsedu parlamentu odôvodnená tým, že Smer získal najviac hlasov vo voľbách. 
 Robert Fico zabúda na to, že funkcia predsedu parlamentu nie je zlatá medaila, ktorá sa udeľuje prvému bežcovi. Je to jeden z troch najdôležitejších ústavných postov spolu s funkciou prezidenta a predsedu vlády. Aké sú právomoci predsedu národnej rady? 
 
 zvoláva a riadi schôdze Národnej rady Slovenskej republiky,  
 podpisuje ústavu, ústavné zákony a zákony,  
 prijíma sľub poslancov Národnej rady Slovenskej republiky,  
 vyhlasuje voľby do Národnej rady Slovenskej republiky, voľbu prezidenta Slovenskej republiky a voľby do orgánov územnej samosprávy,  
 vyhlasuje ľudové hlasovanie o odvolaní prezidenta Slovenskej republiky,  
 vykonáva ďalšie úlohy, ak tak ustanoví zákon.  
 
 Predsedu národnej rady volí parlament nadpolovičnou väčšinou hlasov všetkých poslancov. Koalícia by musela byť na hlavu padnutá, keby takýto dôležitý post zverila opozícii iba preto, že si to jej vodca želá. 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
 
  2. Alternatívna požiadavka na dve miesta podpredsedov parlamentu pre opozíciu s tým istým odôvodnením, že Smer vyhral suverénne voľby. „Pomer 3:2, aby bol predseda a dvaja podpredsedovia NR SR za koalíciu a dvaja podpredsedovia za opozíciu," žiada Fico. 
 
 Niekto mal asi v škole problémy s matematikou, keď mohol vypotiť takúto hlúposť. Rátajme spolu. V parlamente máme 6 politických strán. Z toho štyri koaličné a dve opozičné. Podľa návrhu Roberta Fica by mali zastúpenie vo vedení parlamentu zo štyroch iba tri koaličné strany ale zároveň obe opozičné. Nuž aj v tomto prípade by musela mať koalícia problémy s duševným zdravím, keby takýto návrh prijala. Ale pozrime sa pre istotu, ako to Robert Fico riešil doteraz. V predchádzajúcom parlamente bolo tiež šesť politických strán, z toho tri koaličné a tri opozičné. Ako boli rozdelené funkcie vo vedení parlamentu? Predseda: Pavol Paška (Smer). Podpredsedovia: Miroslav Číž (zase Smer), Anna Belousovová (SNS), Tibor Cabaj (HZDS), Milan Hort (SDKÚ). Kde je zástupca KDH? Nikde, nezostalo pre neho miesto. Kde je zástupca SMK? Nikde, nezostalo pre neho miesto. Zato pre Smer sa našli miesta až dve. Takže poďme opäť počítať. Tri koaličné strany = štyri miesta vo vedení parlamentu. Tri opozičné strany = jedno miesto vo vedení parlamentu. Kto to tu blábolí o arogancii moci? 
 3. Požiadavka na miesto predsedu výboru pre zdravotníctvo s odôvodením: „Počas nášho vládnutia ho mala opozícia. Ak si zoberieme, čo všetko chcú v zdravotníctve urobiť, znovu vrátiť zisky zdravotným poisťovniam, chcú z nemocníc urobiť akciové spoločnosti a pripraviť ich na privatizáciu, tak sme sa domnievali, že by bolo správne, keby niekto sedel za opozíciu v tomto výbore a kontroloval by správanie sa ministra zdravotníctva."   Z 19 výborov národnej rady ponúka koalícia predsednícky post opozícii v deviatich z nich. Tesnejší pomer ako 10:9 v tomto prípade neexistuje. Takže je mi záhadou o akej arogancii novej koalície Robert Fico hovorí. Výbor pre zdravotníctvo je jeden z tých desiatich, kde chce mať predsedu koalícia. Fico a spol sa bude môcť realizovať v ďalších deviatich. A zastúpenie v zdravotníckom výbore predsa opozícia bude mať, preto správanie sa ministra zdravotníctva bude môcť kontrolovať, takže čím nás to Robert Fico zase ohlupuje?  4. Požiadavka na miesto predsedu výboru pre obranu a bezpečnosť s odôvodnením: „Nie je žiadne tajomstvo, že nová koalícia nás chce kriminalizovať. Dôkazom je rozhovor predsedu SaS Richarda Sulíka pre český denník, kde povedal, že Fico patrí do kriminálu. Za týchto okolností chceme, aby predseda výboru pre obranu a bezpečnosť bol z opozície." 
 Názor Richarda Sulíka na to, že Robert Fico patrí do kriminálu vôbec nie je relevantný k tomu, či sa ten pán do kriminálu aj dostane. Tak isto s tým nemá absolútne nič spoločné ani predseda výboru pre obranu a bezpečnosť. Do kriminálu môže Roberta Fica dostať iba sudca a nik iný. Aj keď to bude ešte závisieť aj od toho ako Robert Fico a jeho družina zahlasujú pri návrhu na obmedzenie poslaneckej imunity. 
 P.S. 
 Práve som bol v písaní tohto blogu v najlepšom, keď Jozef Kostelanský zavesil na web blog s podobným názvom a podobnou témou. Vidno, že nielen ja mám ten názor, že arogancia toho odchádzajúceho pána nemá hraníc. 
   
  

