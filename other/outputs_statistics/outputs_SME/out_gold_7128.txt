

 Na ústa ti jemne motýľ sadol. 
 Ukazujúc krídla ohromil ťa svojou vôňou 
 a jemným dotykom neuveriteľna 
 rozbúril ti spiace vnútro.... 
   
 Na ústa ti jemne motýľ sadol. 
 Pošteklil ťa zatváriac sa sťa motýlí zázrak. 
 Náhlil si sa uveriť mu prirýchlo, 
 načahujúc za ním prihorúce dlane. 
   
 Na ústa ti jemne motýľ sadol. 
 Bez námahy presvedčiac ťa, 
 že priletel k tebe rovno z raja. 
 Vedel dobre, že klamaný budeš rád... 
   
 Na ústa ti jemne motýľ sadol. 
 Ľahulinký, krídlami ševeliac a sľubujúc peľ. 
 Tak priľahký a  prelietavý,   
 ako vie byť iba neúprimný bozk. 
   
   
   

