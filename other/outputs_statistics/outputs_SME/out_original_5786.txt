
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniel Hudec
                                        &gt;
                Nezaradené
                     
                 V STV nemôže byť tribúna voľná. 

        
            
                                    4.5.2010
            o
            0:34
                        (upravené
                4.5.2010
                o
                0:52)
                        |
            Karma článku:
                11.07
            |
            Prečítané 
            1344-krát
                    
         
     
         
             

                 
                    Je až do neba volajúce, kam je schopná tzv. verejno-práva televízia zájsť. Nestačí, že viackrát porušila základnú povinnosť snažiť sa pri vedení diskusie zachovať nestrannosť a objektivitu, čo sa prejavilo v jednostranne obsadených politických diskusiách napr.v relácií O 5 minút 12. Ona dokázala vopchať politikum a jeden preferovaný svetonázor už aj do takej relácie, ako je "Voľná tribúna 2010" s podtitulkom diskusia o stratégií rozvoja slovenskej spoločnosti.
                 

                Človek by predpokladal, že v takomto formáte televíznej relácie sa dozvie rôzne názory a uhly pohľadu, ako by sa mala spoločnosť v budúcnosti rozvíjať, čo sú priority a ako ich dosiahnuť. Samozrejme s predpokladom, že o problematike budú hovoriť výsostní odborníci, alebo osobnosti s rôznorodými (nevadí, keď aj protikladnými ) názormi starostlivo vybrané z celého spoločenského spektra. Samozrejme kvôli divákovi, pre možnosť jeho lepšieho výberu a konfrontovania si rôznych názorov. A tu zrazu príde STV 2 s nasledovnou zostavou diskutujúcich. Viera Tomanová - socialistická ministerka sociálnych vecí,ktorá je momentálne stále vo funkcií a z dôvodu blízkych volieb je politicky hyperaktívna. Jej názory sú všeobecne známe a pokiaľ sa neprihovára "ľuďom práce" (pojem je z vlastnej autorskej dielne pani ministerky - TM), nemá už aj vzhľadom na pokročilý predvolebný čas s jej odbornými názormi zmysel polemizovať. Sú proste politické, ideologické a preto pri takejto vážnej diskusnej téme absolútne neadekvátne a rušivé. Peter Magvaši ako bývalý socialisticko-ľavicový sdl-kársky exminister, inak vzdelaný a múdry človek, bol predsa len príliš kompatibilný s názormi pani ministerky. V STV museli poriadne podumať, aby si spomenuli na ešte nejakého ľavicového ministra v novodobej histórií SR. Okrem pani Tomanovej bol totiž on už len jediný.  Ďalším diskutujúcim bol podivuhodný profesor s neúmerným množstvom titulov v červenej kravate, importovaný z Karlovej univerzity v Praha. Keďže vieme, akého politického zamerania sa javí byť ČR po blízkych voľbách, zrejme nebol problém v záujme zachovania zdania objektivity ohonorovať  "socáka" z Prahy a previesť ho na ukážku k nám. Jeho absolútne odmietnutie slovenskej daňovej a dôchodkovej reformy, ktorú mimochodom svojou nečinnosťou vlastne po 4 rokoch potvrdila aj naša tzv. ľavicová vláda, vyznelo tragikomicky. A to aj preto, lebo to hovorí predstaviteľ krajiny, ktorej plánovaný rozpočtový schodok na rok 2010 5,3% HDP je takmer identický s naším, a to v ČR nastupujú k moci socialisti ešte len teraz. Aj s výhľadom nepriamej podpory komunistov!! v parlamente. Prejem našim bratom s troškou irónie veľa úspechov, trpezlivosti a po štyroch rokoch včasné vytriezvenie. Ďalším akože diskutujúcim bol kolaburujúci viceprezident socialistických odborov, ktorý z dôvodu intelektuálnej prevahy spoludiskutujúcich radšej nepovedal takmer nič. Však ani nemusel, ním bezvýhradne podporovaní ľudia to zvládli aj za neho. Výnimkou bol jediný skutočný ekonomický expert pán Peter Staněk zo SAV , snažiaci sa o ucelený, odborný, nepolitický a vyvážený pohľad na sociálno - ekonomické súvislosti. Vzhľadom na zameranie ostatných diskutujúcich, vyznievali jeho inak zdravé a realistické vstupy  v kontexte vývoja relácie ako hlas volajúceho v púšti. Napriek tomu, že určite nemal za cieľ v tejto relácií byť v názorovej opozícií, ale len v rôznorodosti a autonómnosti vlastného názoru, vývoj relácie ho do tejto pozície automaticky posunul. A k tomu ešte intelektuálne ladený moderátor STV, ktorý vôbec nezakrýval svoje sympatie k prezentovanému zideologizovanému štýlu diskusie. Dokonca ho ani len nenapadlo zasiahnuť, keď sa pani ministerka rozkokošila a zmietajúc pod zem bývalú vládu a konkrétne strany, ktoré sa nemohli z dôvodu neprítomnosti brániť, si pomýlila mediálny priestor s grátis agitkou pre stranu Smer. Doslova prehlásila, že v tejto relácií zastupuje názory predstaviteľov Smeru a potom to príslušne rozviedla. A moderátor-džentlmen sa len s úsmevom prizeral. V každom prípade sa musí uznať, že STV pracuje na volebnej kampani svojho šéfa, ktorý ale nesedí v priestoroch tejto inštitúcie, čoraz sofistikovanejšie. Keďže nedeľné samodiskusné politiské relácie sa najmä po poslednej prezentácií "posilneného" národovca zrejme dostali do trápneho svetla a poodhalili čo-to o motivácií stvoriteľov formátu, bude zrejme potrebné posledné plánované vystúpenie veľkého šéfa na záver programu nejako poistiť. A cesta sa už rysuje, veď vlastne stratégiu vývoja Slovenska od SAV si objednal on, takže je jeho. A len on zrejme bude rozhodovať, ako a najmä kým ju my obyčajní ľudia máme mať odprezentovanú. Aby sme ju "správne" pochopili.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Hudec 
                                        
                                            Občania z Piešťan.
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Daniel Hudec 
                                        
                                            Potemkinovský podchod alebo podzemný priestor, ktorý neexistuje .
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Hudec 
                                        
                                            Cesty božie a cesty slovenské...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Hudec 
                                        
                                            Ako to je s diaľnicami pán premiér ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Hudec 
                                        
                                            Ako som kus zakázaného Slovenska našiel.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniel Hudec
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniel Hudec
            
         
        danielhudec.blog.sme.sk (rss)
         
                                     
     
        Veci a udalosti treba pomenovávať pravým menom, inak uviazneme v osídlach lží a pretvárky...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1290
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




