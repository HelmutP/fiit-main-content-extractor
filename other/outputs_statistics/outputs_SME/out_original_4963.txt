
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Červeň
                                        &gt;
                Spoločnosť
                     
                 Hystéria okolo pedofílie v Katolíckej cirkvi utícha 

        
            
                                    21.4.2010
            o
            11:15
                        (upravené
                21.4.2010
                o
                16:21)
                        |
            Karma článku:
                13.09
            |
            Prečítané 
            12884-krát
                    
         
     
         
             

                 
                    Podľa očakávaní túto hystériu vyvolali nezodpovedné médiá, ktorým stačilo preveriť si niektoré informácie pred ich uverejnením. A to dokonca médiá svetového formátu ako BBC, teda tým našim sa nemôžeme až tak čudovať. Je pravda, že aj v Katolíckej cirkvi sa tieto prípady stali, a už minule som sa jasne vyjadril, že ak by sa stal čo i len jediný prípad, bolo by to príliš veľa. Je pravda aj to, že sa vyskytli ojedinelé prípady, kedy niektorí cirkevní predstavení namiesto postúpenia prípadov civilným súdom tieto dokázané skutky ututlávali napriek jasnému zákonu cirkevného práva, podľa ktorého ich mali vydať civilným súdom. Všetci, ktorí spáchali takýto skutok, ako aj všetci, ktorí ich kryli, si zaslúžia spravodlivý trest a aj biskupi, ktorí takéto prípady kryli by mali byť súdení za spolupáchateľstvo. Pedofilov radi ani ich spoluväzni, a vo väzení im dávajú pocítiť, ako nimi opovrhujú. V minulom článku som omylom uviedol, že v Amerike za 50 rokov bolo odsúdených 50 kňazov, teda jeden za rok, bolo ich viac 54. Akého počtu kňazov sa to týkalo?
                 

                 
  
  Za toto obdobie bolo v Amerike okolo 109.000 kňazov. Je pravda, že obvinených bolo viac, až 4392. Teda na jedného obvineného pripadalo 24 neobvinených, čo je veľmi vysoké číslo. Na jedného odsúdeného však pripadlo 2018 neodsúdených. Teda ak v Amerike poznáte 2018 kňazov, jeden z nich môže byť pedofil. Počet žalôb bol ovplyvnený aj vyplácaním štedrého odškodnenia zo strany Katolíckej cirkvi v Amerike, čo je podľa mňa veľká hlúposť, pretože žiadna firma nevypláca za svojich zamestnancov škodu, ktorú spôsobili z vlastného rozhodnutia v rozpore s vnútornými predpismi firmy. A zvlášť v Amerike, kde sa radi súdia pre čokoľvek. Jedna babička tam vysúdila od prepravnej spoločnosti dosť vysokú sumu za to, že sa pri čakaní na spoj potkla na pobehujúcom dieťati, a nikde nebola umiestnená tabuľka s nápisom: "Pozor na pobehujúce deti!" Najväčším paradoxom je, že tým pobehujúcim dieťaťom bola jej vnučka. V Nemecku bolo 210.000 obvinení z pedofílie, z toho medzi obvinenými boli aj 94 kňazi, odsúdených z nich bolo 16, čo je pol promile z celkového počtu. Médiá ignorujú 209.900 ďalších prípadov a venujú sa pol promile prípadom. Toto sa už nedá nazvať slovom nevyváženosť a nezaujatosť. Za posledných 20 rokov prišlo do Vatikánu 3.000 obvinení, z toho bolo 30 odsúdených. Ak si to prepočítame na počet 400.000 kňazov, tak celosvetovo, ak poznáte 133 kňazov, jeden z nich bol obvinený z pedofílie, no skutočného odsúdeného pedofila by ste museli hľadať medzi 13.333 kňazmi. Oveľa viac ostražití by rodičia mali byť voči rodinným príslušníkom, najmä voči nevlastnému otcovi svojho dieťaťa, voči učiteľom detským lekárom, trénerom. Tým nechcem nič zľahčovať. Každý jeden sviniarsky pedofil, nech je to aj kňaz aj ten čo ho kryje, si zaslúži spravodlivý trest. Reči o tom, že Katolícka cirkev chce tento problém ututlať, sa nezakladajú na pravde. Zabúda sa na to, že sám pápež jasne vyzval obete, ktoré s tou bolestnou skúsenosťou žijú a mlčia, aby "podávali žaloby na kňazov ak im ublížili". Dám tu reakciu z diskusie pod posledným článkom: "Hoci klérus mohol urobiť oveľa viac, obvinenia o ututlávaní sú prisilné. Obnovený Kódex kánonického práva z roku 1983 obsahuje pridanú časť: „Klerik, ktorý sa ináč previnil deliktom proti šiestemu prikázaniu Desatora, ak totiž delikt spáchal s použitím násilia alebo vyhrážok, alebo verejne, alebo s osobou mladšou ako šestnásťročnou, má byť potrestaný spravodlivými trestami nevynímajúc v prípade potreby prepustenie z klerického stavu” (CIC 1395). Hranica veku sa nedávno zvýšila na 18 rokov. Prirodzene, to nie je jediná vec, čo Cirkev urobila. Pápež Pavol VI. roku 1967 verejne varoval pred následkami sexuálnej revolúcie a v encyklike O kňazskom celibáte sa venoval otázke kňazstva čeliaceho prostrediu sexuálnej neviazanosti. Vyzval biskupov prevziať zodpovednosť za kňazov v ťažkostiach, hľadať pre nich vhodnú pomoc, v kritických prípadoch ich vyňať z pastorácie a sprísniť rozhodovanie o budúcich kandidátoch na kňazstvo. Roku 1975 vydala Svätá stolica dokument podpísaný kardinálom Ratzingerom Vyhlásenie k niektorým otázkam týkajúcim sa sexuálnej morálky, ktorý sa zaoberal aj problémom homosexuality, pedofílie a efebofílie medzi kňazmi. Roku 1994 vznikla v USA ad hoc Komisia pre otázky sexuálnych deliktov, ktorá vydala smernice 191 diecézam, ako riešiť spomínaný problém. Diecézy reagovali vypracovaním konkrétnych stratégií. V tom čase sa problém pedofílie v spoločnosti komplikoval najmä rozšírením detskej pornografie. Predtým Cirkev verila, že je to liečiteľná choroba. Kňazi sa posielali do rozličných liečebných centier po celom území. Po skončení liečby biskupi dôverovali odbornému posudku o spôsobilosti kňaza vykonávať službu. Hoci to nemôže ospravedlniť ich prípadnú nedbanlivosť, bližšie to vysvetľuje okolnosti ich konania. Po najnovších škandáloch v Amerike okamžite vznikli špeciálne komisie a advokátske skupiny zaoberajúce sa ihneď každým obvinením." (http://blog.sme.sk/diskusie/1665373/18/Papez-pedofili-a-kydajuce-media.html) Sme vďační všetkým serióznym novinárom, ktorí pomáhajú zverejňovať prípady nevinných obetí, a takto pomáhajú odhaľovať pedofilov. Sme vďační najmä tým, ktorí o tomto probléme informujú seriózne, vyvážene, bez hystérie a bez emotívnych skreslení. Rovnako sme vďační všetkým, ktorým záleží na tom, aby Katolícka cirkev bola aj v tejto oblasti jasným zástancom prezumpcie neviny voči obvineným a zástancom potrestania v prípade dokázania viny. Je dobre, že najmä rodičia detí začali byť voči tomuto problému viac ostražití, pretože nikto si nepraje, aby jeho dieťa malo v živote akúto skúsenosť. Pod článkom ponúkam link na debatu o tejto téme v televízii LUX, kde dňa včera v relácii "Doma je doma" o tejto téme rozpráva hovorca KBS Jozef Kováčik. Ešte pripojím aktuálny status z Facebooku: Štefana ukameňovali plní hnevu, bez toho, aby ho vôbec súdili. Prvýkrát sa ešte hrali na "spravodlivých" a zavolali falošných svedkov. Jeho život bol pre nich výčitkou, "jeho tvár bola ako tvár anjela" a výčitkou bola aj jeho smrť, kedy sa modlil za svojich vrahov. Šavol, ktorý vyrastal pri múdrom Gamalielovi, pochopil... Čísla som neuviedol, aby som kohokoľvek obhájil, každý pedofil patrí do basy. Čísla som uviedol len preto, aby sa nezabudlo aj na ostatných pedofilov mimo Katolíckej cirkvi, aby aj oni boli súdení a odsúdení...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (826)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Fico vymetá všetky kúty aj v osadách...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Budú Fica oblizovať aj zozadu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Napíš europoslancovi, nech neblbne...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Ukrajina potrebuje EÚ ale aj EÚ potrebuje Ukrajinu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Skončí postávanie pred kostolom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Červeň
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Červeň
            
         
        cerven.blog.sme.sk (rss)
         
                        VIP
                             
     
        Mám rád život s Bohom a 7777 ľudí. S obľubou lietam v dobrej letke, no nerád lietam v kŕdli. Som katolícky kňaz, pochádzam z dediny Klin na Orave. Farár vo farnosti Rakúsy. (pri Kežmarku)
...........................................


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1475
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3094
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            BIRMOVANCI
                        
                     
                                     
                        
                            ŠTIPENDISTI
                        
                     
                                     
                        
                            7777 MYŠLIENOK PRE NÁROČNÝCH
                        
                     
                                     
                        
                            Aké mám povolanie?
                        
                     
                                     
                        
                            Akú mám povahu?
                        
                     
                                     
                        
                            Cesta viery
                        
                     
                                     
                        
                            Duchovné témy
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Fotografie prírody
                        
                     
                                     
                        
                            Fotografie Rómov
                        
                     
                                     
                        
                            História mojej rodnej obce
                        
                     
                                     
                        
                            Kniha o Rómoch
                        
                     
                                     
                        
                            Lunikovo
                        
                     
                                     
                        
                            Mladí vo firme
                        
                     
                                     
                        
                            Náčrt histórie slov. Saleziáno
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Pastorácia Rómov
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Pokec s Bohom
                        
                     
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Sociálna práca
                        
                     
                                     
                        
                            Sociálna náuka Katolíckej cirk
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Vedeli sa obetovať
                        
                     
                                     
                        
                            Vzťah je cesta cez púšť
                        
                     
                                     
                        
                            Zábava
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štipendium pre chudobné zodpovedné dieťa
                                     
                                                                             
                                            Prečo si firmy neudržia mladých ľudí?
                                     
                                                                             
                                            Dejiny mojej dediny
                                     
                                                                             
                                            Vzťah je cesta cez púšť
                                     
                                                                             
                                            Akú mám povahu?
                                     
                                                                             
                                            7777 MYŠLIENOK PRE NÁROČNÝCH
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sociálny kódex Cirkvi - Raimondo Spiazzi
                                     
                                                                             
                                            Sociálna práca - Anna Tokárová a kolektív
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Free Download MP3 ruzenec
                                     
                                                                             
                                            MP3 ruženec
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Krištofóry - katolícky liberál
                                     
                                                                             
                                            Peter Herman - realistický idealista
                                     
                                                                             
                                            Michal Hudec sa nebojí písať otvorene
                                     
                                                                             
                                            Mária Kohutiarová - žena, matka, človek
                                     
                                                                             
                                            Branislav Sepeši - lekár o etike
                                     
                                                                             
                                            Katrína Janíková - neobyčajne obyčajná baba
                                     
                                                                             
                                            Martin Šabo - šikovný redemptorista
                                     
                                                                             
                                            Juraj Drobny - veci medzi nebom a zemou
                                     
                                                                             
                                            Peter Pecuš a jeho postrehy zo života
                                     
                                                                             
                                            Miroslav Lettrich - svet, spoločnosť, hodnoty
                                     
                                                                             
                                            Karol Hradský - témy dnešných dní
                                     
                                                                             
                                            Salezián Robo Flamík hľadá viac
                                     
                                                                             
                                            Salezián Maroš Peciar - fajn chlap
                                     
                                                                             
                                            Motorka a cesty: Martin Dinuš
                                     
                                                                             
                                            Pavel Škoda chodí s otvorenými očami
                                     
                                                                             
                                            Ďuro Čižmárik a jeho mozaika maličkostí
                                     
                                                                             
                                            Paľo Medár a scientológovia
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Modlitba breviára
                                     
                                                                             
                                            Nezabíjajte deti!
                                     
                                                                             
                                            Občiansky denník
                                     
                                                                             
                                            Dunčo hľadaj
                                     
                                                                             
                                            Správy z Cirkvi
                                     
                                                                             
                                            Prvý kresťanský portál
                                     
                                                                             
                                            Lentus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voľby nemožno vyhrať kúpenými hlasmi najbiednejších Rómov!
                     
                                                         
                       Jednoduchý rozdiel medzi kandidátmi na prezidenta.
                     
                                                         
                       Doprajme Ficovi pokoru!
                     
                                                         
                       Fico vymetá všetky kúty aj v osadách...
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                                                         
                       Vďakyvzdanie
                     
                                                         
                       Treba homofóbiu liečiť?
                     
                                                         
                       Procházka, Kňažko, Kiska... a jediný hlas
                     
                                                         
                       Napíš europoslancovi, nech neblbne...
                     
                                                         
                       Prezidentské voľby: Prečo budem voliť Procházku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




