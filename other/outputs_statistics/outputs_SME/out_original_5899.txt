
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tibor Javor
                                        &gt;
                Fotografie
                     
                 Zabudnuté bratislavské pohľady 

        
            
                                    5.5.2010
            o
            12:58
                        (upravené
                5.5.2010
                o
                15:35)
                        |
            Karma článku:
                7.27
            |
            Prečítané 
            1444-krát
                    
         
     
         
             

                 
                    Stačí prejsť o jednu či dve uličky ďalej od obvyklých trás turistických skupín a dýchne na vás stáročná atmosféra starej Bratislavy. Akosi čistá, a prostá hluku a vôní reštauračných terás či mondénnych shopingov. Zabudnutá krása oprýskanej omietky a mystických zákutí. Veru, táto atmosféra sa ťažko fotí...
                 

                       Do tých uličiek chodievam s fotoaparátom už pár rokov a vždy dokážem nájsť iný pohľad. Možnože som aj vďačný za to, že mnohé z domov sú temer na spadnutie. Je to veľmi kontrastné a fotograficky zaujímavé. Ťažko povedať, ale zdá sa mi, že na zachytenie tejto nálady sa asi najlepšie hodí klasická manuálna analógová zrkadlovka a dobrý film, ktorý sa dá technicky kvalitne skenovať. Skúšal som aj digitál, ale ten akosi tú náladu odfiltruje... Ponúkam zopár záberov tak, ako som ich zachytil klasicky na „celuloid“...   Bez akýchkoľvek úprav.       Kliknutím sem alebo sem sa priamo presmerujete na moje stránky, kde nájdete ďaľšie obrázky z týchto uličiek a môžete ich aj ohodnotiť...                                                       foto (C) Tibor Javor   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Voľne podľa NY Times: Mýty o ruských oligarchoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Biele kone sú na Slovensku legálne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Autobusovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Polemika s Miroslavom Kocúrom o zdiskreditovanej cirkvi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Javor 
                                        
                                            Ad SME: Značka umiera posledná
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tibor Javor
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tibor Javor
            
         
        tiborjavor.blog.sme.sk (rss)
         
                                     
     
         Elektrotechnický inžinier a amatérsky fotograf. Občas s iným pohľadom na tradičné hodnoty. Spoznáte ma aj pod nickom "commandcom". 

 Nejaké moje pesničky nájdete tu: 

 https://www.youtube.com/channel/UC7GEmXuGedNznBj2SzSeL1g/videos 

 a fotografie tu:  

 http://fotky.sme.sk/fotograf/7406/commandcom 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    310
                
                
                    Celková karma
                    
                                                7.64
                    
                
                
                    Priemerná čítanosť
                    2002
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Všeličo
                        
                     
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Potulky
                        
                     
                                     
                        
                            Sally White
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Uznávať Pentaboys?
                     
                                                         
                       Oranžové nafúknuté mŕtvoly v špinavom Dunaji alebo Trans Danube Swim
                     
                                                         
                       Aj ja som teda homofób?
                     
                                                         
                       Rozhovor kresťana s Bohom
                     
                                                         
                       Aj ja som NASRANI
                     
                                                         
                       Rok s mužom v manželstve
                     
                                                         
                       Sedembolestná a ibalgin. A slovenský národ.
                     
                                                         
                       Odpustenie pre minulosť a dôvera do budúcnosti
                     
                                                         
                       Marián Kuffa o zvieratách
                     
                                                         
                       Ak by som bol gay, robil by som lepšie kampane?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




