

 Strávili sme ju na sedliackom dvore Gladerhof v kraji Korutánsko, vysoko nad dedinou Oberdrauburg. Dedina je označená čiernou šipkou, Gladerhof červenou a biela šipka uprostred ukazuje na hrad Hohenburg. 




 Oberdrauburg. 



 A toto je rieka Drau, po našom Dráva, po ktorej má názov celý kraj - Drautal. 



 Jej umelecké stvárnenie v neďalekom Greifenburgu. 

  

 Gladerhof patrí Eve Marii a Adalbertovi Brandstätterovcom a vyzerá takto. 



 Stráži ho desaťročný vlčiak  Timmy, ktorý dozerá na správny chod hospodárstva. 











 V útulnej jedálni s vypchatou srnkou a ovešanými stenami nás každé ráno čakali chutné raňajky. Všetko okrem pečiva je domáceho pôvodu, med je od susedov. Maslo bolo tuhé, že sa takmer nedalo natierať a mlieko chutilo dokonca aj mne, hoci normálne ho nepijem už dlhé desaťročia. 







 Po raňajkách treba nakuknúť do maštale, čo je nové u susedov. S miernymi výčitkami svedomia spočítam prasiatká - uf, odľahlo mi, sú všetky. Raňajková šunka bola zrejme ešte z minuloročných zásob. 



 Všetko v poriadku, kobylka Nelly, kohút so svojimi konkubínami, aj všetci ostatní sú na svojich miestach. 









 Ešte dohliadneme, aby sa napapali aj najmenší a potom už môžeme ísť po svojom. 









 Krajina je tu nádherná. Ráno sa kopce postupne vynárajú z hmly, ... 

















 ... až nakoniec zaleje slnko svojimi lúčmi všetko navôkol. 
























 Okolie ponúka množstvo výletných tipov. Z bohatej ponuky si vyberáme ZOO v neďalekom Asslingu, v ktorom sú umiestnené zvieratá žijúce v týchto končinách. 







 Vrazili sme do mývala 20 centov, ale potom nás žral. Teda žral nám z ruky zakúpené krmivo. 



 Pozri, aký som fešák! 



 Tsss, nič moc, takých som už videla... 



 V ZOO sa prechádza skupinka vysmiatych dôchodcov. Jedna pani začujúc našu ľúbozvučnú slovenčinu podíde bližšie a pýta sa, či sme Slovinci alebo Chorváti. Učila sa kedysi srbochorvátštinu a v nás vidí príležitosť na oprášenie svojich dávnych vedomostí z dvoch semestrov štúdia. Neviem, či je to mojou skvelou nemčinou alebo jej slabou chorvátštinou, ale rozumiem jej len vtedy, ak mi podá simultánny preklad. Nič ju však nedradí a tak mi pri každej klietke rozpráva, ako kedysi navštívila "Cesky Budvar", že Slováci pracujú v Rakúsku ako ošetrovatelia starých ľudí a že bola kedysi kapitánkou na lodi a šoférkou autobusu. Volá sa Thelma, je zo Štajerska a najbližšiu dovolenku chce stráviť vo Vysokých Tatrách. Ktohovie, či jej to napadlo spontánne alebo ide o dlhodobo plánovaný proces zdokonalenia sa v srbochorvátštine. 
 Sme pred klietkous nápisom "Adler".  
"Kako u vas Adler?" 
"Orol." 
"Aaaaa, orrrrrrrrrro." 
"Nein, oroL." 
"Aaaaaaaaa, orrrrrroLA". 
"OroL!" 
"OrrrrroL", teší sa novonaučenému slovíčku. "Sehr interessant!" 

 Páči sa mi optimizmus staršej generácie. Stredom ZOO prechádza letná sánkárska dráha. Jeden z dôchodcov sa práve púšťa dolu svahom, na hlave šiltovka čelom vzad. 
"Bremse!!!!!!!!!", kričia na neho ostatní zo skupinky a dobre sa bavia. 
"Holariaooooooooooooooooooooooo!", vyspevuje na celé kolo a rúti sa nadol. 

 Dúfam, že aj ja budem vo vyššom veku podobne naladená. Aj keď na sánkársku dráhu ma asi nikto nedostane. 

 Ďalším tipom na výlety po blízkom okolí sú hrady a zámky. Niektoré sme navštívili, napríklad Schloss Bruck a Hohenburg. 




 Alebo sa môžeme povoziť na neďalekom 11 km dlhom jazere Weissensee, kde je možnosť kúpania a rybársky raj. V roku 1974 tu bol ulovený najťažší jazerný pstruh Európy - vážil 22 kg.  
Kapitán zaujal svoje miesto pri kormidle a lodná siréna dala pokyn na odchod. Šťastnú plavbu! 



 Na záver jeden tip voňavý. Susedný Irschen si buduje povesť prírodnej a bylinkovej dediny.  



 Pred deviatimi rokmi dostal niekto originálny nápad a zrejme aj určitú poporu od štátu. Nakúpili sa slnečné kolektory, rôzne sadeničky a semená, dali sa zhotoviť jednotné tabuľky s názvami rastlín, všetko sa popichalo do zeme a teraz tu chodia turisti, vyvaľujú oči a rozťahujú nozdry, aby neprepásli nič z tej krásy.  



 Dedinou vedie značkovaný okruh, pri ktorom sa možno prechádzať v púpavou označených záhradách, posedieť na lavičkách a pri množstve potôčikov a jazierok, vidieť vodný mlyn, navštíviť obchod s voňavými suvenírmi alebo výrobňu prírodnej kozmetiky. 



 Vôňa levandule sa mieša s vôňami šalvie, tymiánu a trezalky a mne sa vybavia spomienky na to, ako som  s babičkou chodievala k Rajčianke a na Hradisko zbierať bylinky. V duchu som na povale a cítim teplo letného podvečera a zvláštnu arómu, ktorá tam ostala ešte dlho po tom, ako sa dosušila posledná várka liečivých čajov, ktoré sa babičke vždy podarilo namiešať tak, že boli príšerne horké. Ale možno sa mi to len zdalo. 

 Odskočíme k miestnemu vodopádu. Svah je obsypaný jahodami, čučoriedkami a malinami. Najeme sa dosýta, oddýchneme si,  a aspoň na diaľku sa pokocháme pohľadom na hrad Stein, ktorý je v súkromnom vlastníctve a teda verejnosti neprístupný. 
 


Cestou sa zdravíme s ujom, ktorý tu hrabličkami upravuje turistický chodník. Prehodíme zopár slov a ako všetci, ktorých tu stretávame, nám na rozlúčku zaželá príjemnú dovolenku.

  

 Ľudia sú tu milí, usmievaví, priateľskí. Prví nás pozdravia, zamávajú, keď ideme okolo nich autom. Naozaj, nepreháňam. 


 Krajina mi pripomína zelený obrus so zvieratkami, ktorý máme doma na kuchynskom stole. Aj tu na zelených lúkach a na dvoroch vidieť ovečky, kravy, kone, mačky, sliepky, psov. 









 Podvečer sa zvieratá vracajú z paše a my z výletov. 



 Toto je najmladší obyvateľ Gladerhofu, Sebastian. S mačiatkami nezaobchádza práve najnežnejšie. Niekedy ich strčí psovi do papule, inokedy mu vypadnú z rúk. 



 Občas mu ich preto  zoberieme a schováme. Beží za nami s plačom,  kričí "meine, meine" a hľadá ich a my sa pritom cítime ako ušľachtilí hrdinovia aj hnusní podliaci súčasne. 



 Tak, pre dnešok sú zachránené a môžu sa s dôverou pozerať do budúcnosti. 



 Alebo spokojne zaspať. 



 A my tiež. Keby sme chceli, za poplatok môžeme spať aj v sene. Snáď niekedy nabudúce, teraz musíme nabrať sily, pretože nás čaká druhý rozmer našej  3 in 1 dovolenky. Ale o tom až nabudúce. 

