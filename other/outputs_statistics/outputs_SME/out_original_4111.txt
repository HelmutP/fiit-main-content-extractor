
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Robert Kiss
                                        &gt;
                Nezaradené
                     
                 Koniec štúrovskej papierne 

        
            
                                    8.4.2010
            o
            22:14
                        |
            Karma článku:
                13.08
            |
            Prečítané 
            2674-krát
                    
         
     
         
             

                 
                    Na pozadí Veľkonočných sviatkov v tichosti dodýchala jedna z firiem na Slovensku. Ďalšia v poradí, poviete si, veď takých za ostatné mesiace bolo. Miestni tvrdia, že táto firma nemusela. Keby ju nepotopili jej vlastní majitelia...
                 

                 
  
       Papiereň Smurfit Kappa Štúrovo, a.s. vyrábala takzvaný fluting, papier na zvlnenú vrstvu vlnitých lepeniek. Keď si domov prinesiete chladničku, televízor, alebo počítač a bližšie sa pozriete na stenu papierovej škatule, v strede uvidíte vlnu. To je ten papier.   Ľudia, čo tam pracovali, boli frajeri, aj robotníci používali pri práci počítače. Podľa odborníkov sú papierenské stroje (môžu byť dlhšie aj viac než sto metrov) konštrukčne rovnako zložité, ako veľké dopravné lietadlá. A títo ľudia im rozumeli. Pred šiestimi rokmi to bola prvá a jediná papiereň v Európe, ktorá zaviedla bezsírnu technológiu varenia polocelulózy. Oproti klasickej sírovej je ekologicky podstatne priaznivejšia a v tom čase ju malo iba asi 15 papierní na svete (v USA a v Kanade). Títo ľudia vedeli viac než koľko je dva plus dva a preto sa nad oficiálnym zdôvodnením konca firmy iba trpko pousmiali.   „Bohužiaľ, kríza...", bolo cítiť za slovami o tom, že „plánované zatvorenie papierne súvisí s veľkým nadbytkom výrobných kapacít na európskom trhu v segmente vlnitej lepenky, čo v kombinácii s globálnym ekonomickým úpadkom viedlo v poslednom období k výraznému zníženiu dopytu."   No tak, Tony, nekašli sa!    Väčšina akcií štúrovskej papierne patrí rodine Smurfitovcov, jednej z najbohatších írskych dynastií, zamestnávajúcich okolo 40 tisíc ľudí v približne 400 firmách. Dermot Smurfit, strýko terajšieho prezidenta koncernu Michaela Smurfita, dlho na dôchodku nevydržal. Pred niekoľkými rokmi si kúpil vo Fínsku papiereň Powerflute. Vyrába veľmi podobný papier ako donedávna štúrovská papiereň. Ako na potvoru, aj trhy (najmä južná a západná Európa) boli rovnaké. Jeho firma však nešla tak, ako si predstavoval.   Dermot bol chlap, čo svoje deti viedol pevnou rukou. Bulvárny denník Sunday Times napísal, že vyrastali v rybárskom mestečku Dalkey (predmestie Dublinu), kde mali svoje letné domy aj speváci Chris de Burgh, Bono, či Enya. Jeho dcéra Victoria spomína - „keď som chcela vreckové, hoci iba pol libry, musela som si to odmakať, napríklad umyť otcov Rolls-Royce." Podľa otca deti potrebujú pracovnú etiku, zmysel pre hodnoty a byť si vedomí toho, že príležitosť treba využiť, lebo sa nemusí opakovať.   Niekedy príležitosť neprichádza, ale dá sa jej „pomôcť". V Štúrove začali niečo šípiť, keď na pokyn ústredia dostala kartonážka Smurfit Kappa Obaly Štúrovo, a.s., tesne susediaca s papierňou, pokyn nakupovať papier z iných papierní. Navyše v čase, keď ceny papierov dosahovali nízke úrovne, predstavenstvo papierne precenilo jej hmotný a nehmotný majetok na nulu a firma vykázala v 1. polroku 2009 obrovskú stratu 36,6 milióna eur. Takže všetko, čo bolo vo firme, malo odrazu nulovú hodnotu. Ľudskou rečou povedané - majitelia nevideli možnosť, že by im firma v horizonte piatich rokov priniesla zisk. Nechceli ju vidieť, pretože hoci o krátky čas na to začali ceny papierov rásť a rástli by aj tržby, majitelia už boli rozhodnutí.   Najmä vďaka tomu, že firma nezaspala na vavrínoch a v dobrých rokoch hľadala rezervy na čo najefektívnejšiu prevádzku, bola pravidelne v zisku. Do ústredia v Dubline odviedla „pôžičku" vo výške približne 1,8 miliardy korún! To snáď stačí k téme o finančnom zdraví firmy a jej možnostiach.   Štúrovská papiereň ako jediná v skupine vyrábala polochemický typ flutingu. Skupine zrejme chýbať nebude, veď Tonyho kartonážkam ho môže dodať papiereň strýka Dermota. Kto by na jeho mieste nevyužil takú príležitosť? A aby nebodaj niekomu v budúcnosti nenapadlo oživiť výrobu v Štúrove, írski majitelia si pri jej predaji kladú podmienku, že fluting sa tam vyrábať nebude. Čoho sa boja, keď to „nemá perspektívu"?   Kde je vôľa...   Táto firma neskončila, ako mnohé iné na Slovensku. Finančnú krízu nenaznačovalo nič, výplaty boli pravidelné. Dlhoročným zamestnancom ešte príde na účty odstupné vo výške minimálne dvanástich platov. Koľkí z iných firiem na Slovensku by s nimi menili? No štúrovčania nechceli odstupné, chceli pracovať ďalej. Verili firme, ktorá výrazne poznačila ich životy a ktorú mali úprimne radi. Na okolí sa hovorilo, že skrachovať môže čokoľvek, ale nie táto papiereň. Takú mala povesť.   Úžasný ľudský potenciál a to, čo sa tí ľudia za štyridsať rokov naučili o výrobe papiera, sa už asi tak ľahko nedá pozliepať. To nie je model - zoberieme sto ľudí z ulice, postavíme ich k montážnej linke a hotovo. Vyrobiť dobrý papier s presne vyšpecifikovanými vlastnosťami je umenie. Zamestnanci papiereň s obrovským nasadením oživili, keď ju v roku 1993 zničil požiar a deväť mesiacov nevyrábala. Ale toto rozhodnutie majiteľov nedokážu „rozchodiť". Azda všetci hostia obdivovali odbornosť, ale aj ľudské kvality osadenstva. Pracovali tu a pomáhali si bez ohľadu na to, či rozprávali po slovensky, alebo po maďarsky. Ich príbeh bol dlhé roky skvelým príkladom hesla - kde je vôľa, tam je cesta   Pracoval som pre túto firmu externe ako konzultant niekoľko rokov. Ďakujem všetkým ľuďom, ktorých som tam stretol. Naučil som sa od nich veľa, nie iba o výrobe papiera... Vo veľkej miere spoluvytvárali tvár Štúrova. Veď napokon aj to chýrne termálne kúpalisko bolo postavené z peňazí podniku a za štyridsať rokov sa z ospalej dediny na periférii poľnohospodárskeho kraja stalo útulné, vyše desaťtisícové mestečko. Len domáci vedia, že život tu už nebude, ako predtým.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Kiss 
                                        
                                            Keď vôľa žiť je silnejšia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Kiss 
                                        
                                            Ani rak to nemá ľahké...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Kiss 
                                        
                                            Spisovatelia, pomôžte ekonomike!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Kiss 
                                        
                                            Spomienky na babku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Kiss 
                                        
                                            Zas tá blbá reklama!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Robert Kiss
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Robert Kiss
            
         
        robertkiss.blog.sme.sk (rss)
         
                                     
     
        Som foter, príslušník generácie X. Fotím, píšem, prekladám, vychovávam. To posledné je najťažšie.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2477
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotookamihy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




