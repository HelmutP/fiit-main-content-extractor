

 Doteraz každý autoservis musel byt "autorizovaný" dva krát. 

 
Prvý krát ako podnikateľ, ktorý poskytuje služby, musel preukázať potrebné personálne a technické vybavenie. Väčšina techniky používaná v autoservisoch musí byť certifikovaná, to isté platí o nakladaní s nebezpečnými látkami, odpadmi atd. 

 
Druhý krát musel byť autoservis autorizovaný na základe "obchodných  kontaktov" s výrobcom vozidla. Následkom tejto druhej autorizácie boli servisu preplácané výkony a náhradné diely pri opravách počas záručnej doby. 

 
Niektorí výrobcova vozidiel išli v monopolizácii tak ďaleko, že servisnú dokumentáciu a náhradné diely dodávali len spriateleným autoservisom, navyše určovali/odporúčali cenu za hodinu práce. Pre ostatných boli náhradné diely nedostupné, alebo len za premrštenú cenu. 

  
Nie je núdza ani o také situácie, že vozidlo dostalo poruchu v odľahlom území a miestny autoservis, pre nedostupnú servisnú dokumentáciu nedokázal odstrániť ani banálnu poruchu, ktorú u iných značiek dokáže podľa návodu na obsluhu vykonať sám vodič. Napríklad výmena žiarovky alebo upchaný palivový filter. 

 
Teraz podľa nových predpisov sú výrobcovia a importéri vozidiel povinný do 3 rokov spraviť také zmeny, aby servisná dokumentácia bola prístupná pre všetky autoservisy a dodávka náhradných dielov tiež.  K tomu si musia spraviť režim preplácania nákladov za záručné opravy. 

 
Odhadujem, že proti tomuto rozhodnutiu EU bude tlak najmä od výrobcov a spriatelených servisov, ktoré si u danej značky vytvorili monopol. Tržby zo servisu vozidiel tvoria v priemere 40% ceny vozidla a práve o túto časť trhu teraz ide. 

 
Je dosť pravdepodobné, že výrobcovia budú na túto zmenu reagovať tak, že na svojich stránkach uvádzať hodnotenie jednotlivých autoservisov, či daný autoservis má od nich certifikát a aká je spokojnosť od zákazníkov. Výsledkom možno bude kategorizácia servisov na tri skupiny. Prvá ktorú výrobca bude odporúčať na základe získaných certifikátov a dobrých ohlasov. Druhá, pred ktorou bude výrobca varovať na základe sťažností zákazníkov a tretia najpočetnejšia, kde sa hodnotenie nebude vymykať z priemeru alebo nebude na základe čoho posúdiť kvalitu servisu pre danú značku. 

 Záver: 
Vítam rozhodnutie, ktoré prezentoval európsky komisár pre hospodársku súťaž Joaquín Almunia, lebo umožňuje súťaž na trhu a ruší monopolné praktiky, čo v konečnom dôsledku sa prejaví vyššou kvalitou a nižšou cenou pre konečného spotrebiteľa.
 


