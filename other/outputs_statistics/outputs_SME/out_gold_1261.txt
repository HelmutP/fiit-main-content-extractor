

 Pritom samotná metodológia rebríčka značne zvýhoďnuje anglosaské, najmä americké univerzity, a tiež univerzity silné v prírodných vedách. Veľkú časť základu pre porovanie výzkumnej výkonnosti, 20 percent, totiž tvoria citácie vyučujúcich v amerických časopisoch Science a Nature. Samozrejme, nepublikujú tu len americkí vedci, ale zvýhodnenie je zrejmé. (Zoberte si problémy s prekladmi alebo vôbec rozdielnymi akademickými kultúrami.)   
     
 Pre tento rok sa zostavovatelia rebríčka chvályhodne rozhodli pre inštitúcie, ktoré nemajú prírodné vedy, tento indikátor rozpustiť. Ale predpojatosť voči humanitným vedám tu ostáva. Ďalších 10 percent váhy totiž tvorí počet Nobelových cien alebo Fieldsových medailí, získaných absolventami tej-ktorej univerzity. Nobelové ceny pritom početne dostáva viac ľudí za prírodné vedy. Za fyziku, chémiu a medicínu sú totiž prakticky vždy spojené pre viacerých ľudí. Podobne síce aj za ekonómiu, ale v menšom počte za literatúru alebo mier. Nehovoriac o tom, že nositelia posledných dvoch sa často ani v akademickom prostredí nepohybujú. No a Fieldsova medaila je, samozrejme, za matematiku.   
 Vôbec, počet Nobelových cien získaných absolventami jednoducho nijako nevykresľuje aktuálnu výkonnosť univerzity.    
     
 Ďalších 20 precent hodnotenia zahŕňa počet vyučujúcich, ktorí získali Nobelovu cenu alebo Fieldsovu medailu. Takže tu možno odpadá námietka neaktuálnosti, ale nie tá, týkajúca sa zvýhodňovania prírodných vied.   
 Takže to už máme 50 percent cekovej váhy rebríčka. Podobne by sa možno dalo diskutovať o použitých citačných databázach (aj tu sa tento rok snažili zostavovatelia urobiť určité zlepšenia), ale to už zrejme nebude také vážne. 

 Pravdaže, podobné skreslenia sa vyskytujú aj v iných, dlhodobejších rebríčkoch. Ako perličku uvediem príklad, pre ktorý nemusím chodiť ďaleko. Moja univerzita nedávno založila, spoločne s Univerzitou v Hull, fakultu medicíny. A, čuduj sa svete, minimálne v jednom uznávanom rebríčku britských denníkov sa táto nová škola dokonca hneď umiestnila na čele tabuľky. Stačí sa však pozrieť na metodológiu zostavovania a vyvstane veľmi jednoduchý dôvod. Veľkú váhu v rebríčku majú položky ako vynaloženie peňažných prostriedkov na študenta v jednom roku. Ale v tomto prípade ide o novú fakultu, ktorá stavia BUDOVY! Samozrejme, že náklady sú astronomické. Škole to krásne na jeden rok nafúkne skóre.   
     
 Tým nechcem povedať, že rebríčky nie sú dobrá vec. Ale často sa stretávam s tým, najmä v britskom a americkom prostredí, ako sú ľudia doslova fixovaní na lokalizáciu univerzít, alebo jednotlivých akademických programov, v takýchto rebríčkoch. Ak sa ich škola dostane o dve priečky vyššie, sú šťastní. Je to nezmyselné, rebríčky sú veľmi povrchným ukazovateľom kvality, ako som sa to tu skratkovito snažil načrtnúť. Je fajn použiť ich na prvotnú orientáciu, napríklad, ak idem na školu v cudzej krajine a potrebujem si rýchlo zistiť, kde môžu mať kvalitné prostredie. Ale netreba sa nimi nechať zaslepiť. A keď sa takýchto ľahko titulkovateľných správ zmocnia média – hotová pohroma! Rebríčky sú jednoducho dobrým služobníkom, ale zlým pánom.    
     
     
 (Aby som bol úplne férový, zostavovatelia vyššie citovaného rebríčka dnes sami na svojej stránke uvádzajú prípadné metodologické problémy. A mimochodom, USA majú určite mimoriadne kvalitné univerzity, takže ich dominancia v akomkoľvek rebríčku nemôže prekvapiť. Skôr ide o to, že pri podrobnom pohľade zrazu zistíte, že aj takmer provinčná americká univerzita si častokrát vedie lepšie ako špičková univerzita v inej krajine a začnete sa škrabať na hlave...)   
  
   

