

 
 
 Náhodou som ho včera stretla. Od začiatku bol akýsi smutný a na otázku, prečo je tomu tak, mi s vlhkosťou v očiach povedal: 
 Dnes sme sa fotili v škole. Teda všetci okrem mňa. Mama mi nechcela dať peniaze. Vraj sú to zbytočnosti. Triedna učiteľka mi povedala, že mi tú fotku zaplatí, ale nemohol som to prijať. Vieš koľko krát som si už požičal peniaze na desiatu ?  
 Keď som sa ho spýtala, aké bude mať vysvedčenie zahanbene povedal, že asi samé trojky a štvorku z matematiky. Priznal sa, že učenie mu až tak nejde, ale že aj keby sa chcel snažiť viac, nemá na to čas, lebo musí stále doma pomáhať, lebo inak ho mama zbije. Hneď na to som si všimla pár dostatočne zreteľných modrín na jeho tele. 
 Zveril sa mi, že neznáša, keď jeho otec pije, že potom sa nevyspí celú noc.  
 Od pani učiteľky sme zistili, že chlapec na tému, aký je jeho sen, napísal sloh na veľkú stranu, že jeho snom je bývať v detskom domove, kde by ho nebili a kde by nebol hladný a mohol by kľudne v noci spať. 
 
 
 Jeho prípad bude asi čoskoro v rukách niekoho zodpovednejšieho ako sú jeho rodičia, ale prečo takí rodičia potom deti majú? Ako môže matka ne-matkou byť? Ako sa môže správať tak barbarsky, bez akéjkoľvek lásky a ešte aj s nenávisťou? Zaslúži si také žena, aby ju jej vlastné dieťa nazývalo mamou?  
 Niektoré ženy by dali všetko pre to, aby raz mohli byť matkami a aby si svoju úlohu vychovávať zo svojho potomka, čo najlepšieho človeka, čo najlepšie plnili.... avšak nie je to pre nich možné. Prečo je to potom možné pre tie, ktoré si to vôbec nevedia ceniť a nezaslúžia?  


