
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Romana Dériková
                                        &gt;
                Isn´t this ironic?
                     
                 Naučte sa byť správnym intelektuálom uznávaným v spoločnosti! 

        
            
                                    25.5.2010
            o
            18:42
                        (upravené
                25.5.2010
                o
                19:18)
                        |
            Karma článku:
                10.21
            |
            Prečítané 
            2021-krát
                    
         
     
         
             

                 
                    Ocitli ste sa v spoločnosti celkom aj vzdelaných (mladých) ľudí. Vzdelaní (mladí) intelektuálni ľudia sa rozprávajú a vy sa cítite strateno.
                 

                 (no dobre, očarenie vám veľmi nevyjde, ono intelektuáli na Slovensku nie sú veľmi cenení. ale proste všetci vám uveria, že ste intelektuál a viac sa faktom "očarenia" zaoberať nejdeme)  Modelová situácia Ocitli ste sa v spoločnosti celkom aj vzdelaných (mladých) ľudí. Vzdelaní (mladí) intelektuálni ľudia sa rozprávajú a vy sa cítite strateno. Mládenec X vysvetľuje potrebu dekriminalizácie a legalizácie marihuany pomocou hľadania odpovedí na základné ontologické otázky. Mládenec F sa zaoberá vplyvom množenia sa prvokov na ekonomickú situáciu v Rwande a všetko to podčiarkuje špeciálnou teóriou relativity. Slečna K sa usmieva a vždy je schopná pohotovo doplniť konverzáciu o trefný bonmot, ktorý krásne zvýrazňuje jej rozhľadenosť. Mládenec Z je nihilista, stále krúti hlavou a hovorí, že všetko je napiču, on už všetko pozná a nič nemá zmysel a sem tam hodí do plenu slová ako "decentralizácia", "hypnóza", "integrácia", "efemérny".  Sem tam na vás hodia uškŕňajúci sa pohľad a vy zúfalo hľadáte tému, ktorou ohúrite, nervózne si odchlipkávate z piva (ostatní pijú víno) a hovoríte si, že ste mali radšej ísť pozerať hokej.  Nezúfajte! Musíte si osvojiť základný poznatok. Každý správny intelektuál je ODBORNÍKOM na niečo, o čom často ani neviete, že existuje. (všetci ostatní, čo sa zaoberajú všeobecne známymi vecami, o ktorých sa dopočuli na strednej a príde im to akurát tak kúl a mega sú pseudointelektuáli - zapíšte si to do poznámkových zošitov, prosím. ak chcete byť intelektuálom, témy ako Sartre, Baudelaire, Socrates či stratená generácia sú TABU! - jedine, že by ste ich použili vo veľmi NEKONVENČNOM KONTEXTE- )  Zamyslite sa a teraz si ukážeme vynovenú verziu modelovej situácie. -No čo, Pištík? A teba čo baví? -No, ja sa dlhoročne zaoberám keňskou národnou literatúrou.  prichádza uznanlivé hmkanie. Hm Hm Hm  (nihilista má problém a odfrkne si)  -Keňská literatúra? Sračky. -Nie, nie, náhodou, je to veľmi zaujímavé čítanie. Pokiaľ by ste mali záujem, tak vám na začiatok odporúčam knihy ich najvýznamnejšieho spisovateľa Uweka Wom Komkatchaa(pripojte nejaký klokotavý zvuk)  (v spoločnosti rastie záujem)  -A čím sa zaoberal? -No, on má veľmi špecifický štýl plný naivity a rozprávkovosti, avšak pri hlbšom štúdiu keňskej kultúry sú tam badateľné postrehy o ich politickej situácii, silná satira či dokonca aj nespokojnosť so sexuálnym životom. Často používa motív osamelej hory, múdreho kmeťa či rozprávajúcej líšky. Niektoré jeho ťahy sa snaží napodobniť ich ďalší spisovateľ Puwendo Kalaláááá (klokotavý zvuk), ale on sa zameriava skôr na bezduchosť spoločnosti.  (znova uznanlivé hmkanie)  Verte mi. Naozaj to nie je ťažké. Robím to už dlhé roky. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (50)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Tekvicové kadečo v období tekvicových orgií.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Raňajky sú to najdôležitejšie!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Zápal močových ciest a štátnice? Och, áno.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            "Mám džuge.": Ako ma deti učia cigánčine.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Romana Dériková 
                                        
                                            Milí rodičia!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Romana Dériková
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Romana Dériková
            
         
        derikova.blog.sme.sk (rss)
         
                        VIP
                             
     
         moja mama hovorí, že sa lesknem ako koňovi gule. 

 sedlák v meste. študent. barista. čašníčka. učiteľka angličtiny v nízkoprahu armády spásy. dobrovoľník v sociálnych službách. nehorázne sarkastický trúd. náhodný cestovateľ, stopovateľ. a sem tam o tom všetkom rada niečo zdelím. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    69
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1291
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            rmn the cestovateľ
                        
                     
                                     
                        
                            Isn´t this ironic?
                        
                     
                                     
                        
                            Romanka kuchtí
                        
                     
                                     
                        
                            Stretla som...
                        
                     
                                     
                        
                            Zápisky z frontov
                        
                     
                                     
                        
                            Cítim
                        
                     
                                     
                        
                            London
                        
                     
                                     
                        
                            This is music
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            F. M. Dostojevskij - Uražení a ponížení
                                     
                                                                             
                                            Ingmar Bergmann - Filmové povídky
                                     
                                                                             
                                            Sjón - Syn stínu
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            last.fm štatistiky, ak by to niekoho zajímalo
                                     
                                                                             
                                            Swans
                                     
                                                                             
                                            Connan Mockasin
                                     
                                                                             
                                            Kid Koala
                                     
                                                                             
                                            Roots Manuva
                                     
                                                                             
                                            Nine Inch Nails
                                     
                                                                             
                                            Chaozz
                                     
                                                                             
                                            The Horrors
                                     
                                                                             
                                            The Good, The Bad &amp; The Queen
                                     
                                                                             
                                            Feist
                                     
                                                                             
                                            Fionna Apple
                                     
                                                                             
                                            Erik Truffaz
                                     
                                                                             
                                            Bonobo
                                     
                                                                             
                                            Alice In Chains
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            žoviálny VKTR
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tekvicové kadečo v období tekvicových orgií.
                     
                                                         
                       Bratislavské momentky - fotografie
                     
                                                         
                       Raňajky sú to najdôležitejšie!
                     
                                                         
                       Zápal močových ciest a štátnice? Och, áno.
                     
                                                         
                       O falošných tónoch na maturitnom koncerte
                     
                                                         
                       Je Čaplovič blbý, alebo nie?
                     
                                                         
                       mala som štyridsať, bolo dobre. teraz mám dvadsať a pubertu
                     
                                                         
                       "Mám džuge.": Ako ma deti učia cigánčine.
                     
                                                         
                       Sarajevo (fotografie)
                     
                                                         
                       Tallinn (fotografie)
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




