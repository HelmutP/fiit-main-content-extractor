

 ... a zrazu všetko stálo. Nešli semafory. Policajti, hoci ich bolo na križovatke neúrekom, dopravu neriadili, ale zastavili. A trvlo to asi dvadsať minút. Boli tri po obede. Ľudia v autobusoch a autách nervózneli a každý sa snažil nakuknúť dopredu. Sanitka blikajúca modrými svetlami iba márne hulákala v zápche vyvolanej pre lízanie. 
 ... o štvrtej sa to zopakovalo. Namiesto dvadsiatich teraz už iba desať minút. Potom opäť spustili semafory a všetko išlo ďalej. 
 .... O piatej chaos na ZOO. Zase vraj pre nejakého medveďa. Z mlynskej doliny autobus meškal dvadsať minút, lebo nik nemohol z križovatky, ani do križovatky. Potom prešli štyri drahé autá a potom sa zasa čakalo, asi aby sa nikto nešiel pozrieť, čo za vola nevie šoférovať a potrebuje prázdnu cestu. 
 A čo? Ohlási sa niekto okrem o chvíľu za blbca vyhláseného blogera? Nie. Národ si už zvykol. Veď napokon, nie je to prvý ruský medveď, ktorému sme museli na Slovensku hlavu lízať. 

