

 Otázka čitateľa: 
Dobrý deň, často čítam Váš blog a zaujímalo by ma, či sa dá niekde zodpovedne 
zistiť, či môže sedieť dieťa do 150 cm resp. 12 rokov v autosedačke na prednom 
sedadle. Jeden známy tvrdí, že je to možné a  nehovorím o škrupinke. Dieťa 
má 5 rokov a kúsoček nad meter. Veľmi by ma zaujímala odpoveď. Ja si myslím, že 
ohrozuje svoju dcéru, má tam aj airbag spolujazdca. 
  
V zmysle platných predpisov  "Nariadenie vlády 554/2006 Z.z." pri preprave dieťaťa do výšky 150cm  
na prednom  (aj inom) sedadle je povinnosťou použiť schválený zadržiavací systém a vyplýva to konkrétne z: 
 § 4 Používanie bezpečnostných systémov vo vozidlách 
kategórie M1, N1, N2 a N3 
(1) Ak sa na sedadle vozidla vybavenom bezpečnostným pásom prepravuje dieťa s 
výškou menšou než 150 cm, musí byť toto sedadlo vybavené detským zadržiavacím 
zariadením podľa § 3 ods. 2 vhodným pre deti podľa hmotnostných skupín uvedených 
v § 3 ods. 1.  
(2) Detské zadržiavacie zariadenie nesmie byť obrátené dozadu na sedadle, ktoré 
je chránené čelným airbagom, ak nebol airbag vyradený z činnosti alebo nebol 
automaticky deaktivovaný. Obrátenie dozadu je otočenie v opačnom smere, ako je 
smer jazdy. 
(3) Na sedadle vozidla, ktoré nie je vybavené bezpečnostným systémom, sa nesmú 
prepravovať deti do veku troch rokov. 
  
 Existuje ešte výnimka pre územie Slovenska podľa ktorej v niektorých 
špecifických prípadoch je dočasne možná preprava aj bez zadržiavacieho systému. 
 § 7 Spoločné a prechodné ustanovenia 
(1) Podmienky prepravy dospelej osoby s telesnou výškou menšou ako 150 cm vo 
vozidlách kategórie M1, M2, M3 a N1, N2, N3 ustanovuje osobitný predpis.9) 
(2) Vo vozidlách kategórie M1 a N1, v ktorých sú umiestnené dve detské 
zadržiavacie zariadenia na zadných sedadlách a v ktorých nedostatok priestoru 
bráni umiestneniu tretieho detského zadržiavacieho zariadenia, možno dieťa vo 
veku tri a viac rokov a s telesnou výškou menšou ako 150 cm pripútať 
bezpečnostným pásom.  
(3) Ak vozidlá kategórie M1 a N1 nie sú vybavené detským zadržiavacím zariadením 
alebo je týchto zariadení vo vozidle nedostatočný počet, možno dieťa vo veku tri 
a viac rokov pri preprave v obci pripútať na zadnom sedadle vozidla 
bezpečnostným pásom, ktorým je vozidlo vybavené. 
(4) Preprava dieťaťa je zakázaná na predných sedadlách za podmienok ustanovených 
v odseku 2. 
 Z uvedeného vyplýva:
 
 
 Na prednom sedadle je zakázaná preprava deti do výšky 150 cm pokiaľ nesedia v schválenom zadržiavacom systéme. 
 Na prednom sedadle je zakázaná preprava deti aj v schválenom zadržiavacom systéme pokiaľ sedia v protismere jazdy a vozidlo ma aktívny airbag pre dané sedadlo. 
 Ak má dieťa výšku nad 150 cm tak je z hľadiska telesnej konštrukcie považované z dospelú osobu a musí byť riadne pripútane bezpečnostným pásom tak ako dospela osoba. 
 Výnimka pre tretie dieťa ak už dve sedia v zadržiavacích systémoch a tretí zadržiavací systém sa nezmesti, platí len na zadne sedadlo a pre dieťa staršie ako tri roky. 
 Výnimka pre prípady, že zadržiavacie systémy nie sú, platí len pre deti staršie ako tri roky a len na zadných sedadlách a len v obci. 
 



 Záver: 
Na prednom sedadle môžete prepravovať dieťa do 150 cm zadržiavacom systéme v smere jazdy. Ak mate pochybnosti ohľadom bezpečnosti, odporúčam sa obrátiť sa na 
výrobcu vozidla, nakoľko výrobca vozidla môže tieto podmienky v technickej 
špecifikácii ešte sprísniť. Existujú aj zadržiavacie systémy viazane na vozidlo, 
ktoré sú často jeho voliteľnou výbavou. Na základe skúsenosti viem, že u bežných 
typoch vozidiel na našom trhu ani jeden výrobca nezakázal prepravu deti vedľa vodiča v priamom smere. Naopak, rada výrobcov tento spôsob prepravy podporuje dodatočnými 
doplnkami, nakoľko vedia, že často bude jazdiť len matka s dieťaťom. Je to aj preto, že ak dieťa sedí na zadnom sedadle a vodič sa otáča 
počas jazdy dozadu, výrazne sa zvyšuje riziko nehody, ďalej preto, že a pasívna a aj aktívna bezpečnosť cestujúcich 
na predných sedadlách sa za posledné desatročie podstatne zvýšila. 

