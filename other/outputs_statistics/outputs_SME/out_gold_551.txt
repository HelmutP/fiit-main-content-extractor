

 Potom zavolal Ľube, ktorá zavolala Katke, a 4 minúty po jej „prorockom" maili nasledoval druhý: „Tak som to privolala, rubu lesik na Krasovskeho!!!"  
 
 
Rýchlo do batohu foťák, mobil, počítač, zoznam redakcií, a občiansky preukaz. Ochranár totiž vopred nikdy nevie, či bude volať políciu, aby niekoho odvliekla, alebo bude polícia odvliekať jeho. Rozdiel väčšinou robí skutočnosť, či policajt uvidí nejaký papier s razítkom, alebo nie.   
Už keď sme sa blížili k Sadu Janka Kráľa, bolo zrejmé, že drevorubačská firma sa činila od skorého víkendového rána. 
 
 

 
 
Nakladanie časti kmeňov. 
 
 

 
 
Eliška, konateľka združenia Nádej pre Sad Janka Kráľa. Všetky stromy boli vopred narezané, aby ich nebolo možné zachrániť ani príchodom ochrancov ÚCHO.  
 
 
 
 
 
Predstavitelia firmy ukazujú polícii, mestskému poslancovi a aktivistom oznámenie o výrube.  
 
 
 
 
Na oznámení je dátum a číslo stavebného povolenia, ktorým bolo podmienené povolenie na výrub. Takže starosta a úradníci združeniu nedali vedieť, hoci sľubovali. Občianske združenie bolo pripravené vstúpiť do stavebného konania, avšak to by sa muselo dozvedieť o jeho začiatku. Výrub by mohlo oddialiť aj podanie odvolania voči stavebnému rozhodnutiu, ale to by sa občania museli dozvedieť o jeho vydaní. Na elektronickej úradnej tabuli Petržalky nebolo ani v sobotu večer. Ochranári si potom medzi štyrmi očami vyčítajú, že uverili slovám politika, že sa spoľahli na mestskú webovú stránku a nechodili osobne každý deň na úrad, že nevyhlásili začiatkom marca občianske hliadky... To však znamená, že dôslední ochrancovia prírody by sa mali správať, ako keby boli s týmto štátom a vlastnou samosprávou vo vojnom stave.  
 

 
 
Odchádza jeden les.
 
 
 
 
 
Pri pohľade na pne, ktorými bola posiata plocha bývalého lesíka, som si uvedomil, aký je človek s tými svojimi papiermi a predpismi tragikomický. Že vraj 31. marca končí mimovegetačné obdobie... Tieto stromy určite nečítali vyhlášku, inak by si nedovolili mať zelené lístky na konároch a kmene plné miazgy dva dni pred stanoveným termínom.  
Jeden z pňov, v ktorom padajúci kmeň vytrhol dieru, vyzeral ako studnička.  
Trápne sú tiež tituly niektorých „odborníkov". Áno, hovorím konkrétne a menovite, o dendrologickom znaleckom posudku č. 1/2006 vypracovanou Doc. Ing. Gabrielou Juhásovou, CSc., znalkyňou v odbore záhradníctvo - okrasné dreviny, z ktorého vyplynulo, že „väčšinu stromov na danom území vzhľadom na ich zlý zdravotný stav a malú pravdepodobnosť prežitia nie je možné zachrániť ani po prípadnej revitalizácii." Po výrube sa zistilo, že zo 127 stromov ich bolo 101 vo výbornom zdravotnom stave . Teraz by sem mala prísť pani docentka, aby si na vlastné oči overila odbornosť svojho posudku. (Mimochodom, overil som si u aktivistov zo združenia Pre Prírodu, že to bola tá istá odborníčka, ktorá dala posudok, že je choré storočné ginko v Trenčíne.)  
 
 

 
 
Novinári prichádzali a odchádzali, pracovníci drevorubačskej firmy už prestali protestovať, že ich niekto „bez povolenia" fotografuje, a pustili sa do najväčšieho topoľa.
 
 
 
 
 
 
 
 

 
 
 
 
 
Velikán klesá k zemi. 
 
 
  
 
 
Takýto pohľad sa otvoril zo Sadu Janka Kráľa po vyrúbaní lesíka Krasovského. Mnohí až teraz pochopia, prečo ochranári tak urputne bojovali za zachovanie lesíka.
 
 
  
 
 
Smútok jednej dryády.
 
 

 
 
Raz som videl v dokumentárnom filme, ako prístavní robotníci pomocou čepelí na dlhých rúčkach rozrezávali obrovské telo ulovenej veľryby. Tiež im to šlo s prekvapivou ľahkosťou.  
 
 

 
 
Človek má rozpätie rúk rovnaké, aká je jeho výška. Mám 180 cm, takže tento topoľ mal priemer kmeňa okolo 160 cm a bol absolútne zdravý. Je to stredne rýchlo rastúca drevina, mohol mať okolo sto rokov. 
 
 

 
 
Katka už má veľa takýchto smutných fotografií. Ročne absolvovala okolo 360 výrubových konaní, a aj keď má zákony v malíčku a obrovskú nezlomnosť, Bratislava je príliš veľká, investori príliš draví a úrady príliš skorumpované.  
 
 

 
 
Katka a Eliška spomínajú, čo všetko urobili pre záchranu lesíka. A čo nestihli urobiť.  
Lenže stačí si pozrieť túto tlačovú správu  alebo ich web , a je vám jasné, že vynaložili možno viac energie, než koľko potreboval investor na prípravu celého toho polyfunkčného mrakodrapu, čo má stáť na mieste lesíka. Pracovali v každej oblasti - odbornej, právnej, mediálnej, lobistickej, ukážkovo komunikovali s verejnosťou. Na fakulte architektúry si dali vypracovať projekt revitalizácie lesíka na park a jeho prepojenia so Sadom Janka Kráľa. Zorganizovali petíciu, ktorú podpísalo 14 000 ľudí. Pripomienkovali územný plán. Odvolávali sa skoro proti každému kroku investora. Písali poslancom. Zapojili vedcov, ktorí v lesíku zdokladovali chránené živočíchy. Zhromaždili podklady na vyhlásenie lesíka za chránený krajinný prvok. Keď jedna úradníčka rozhodla, že lesík štátnym chráneným územím napriek požiadavke vedcov nebude, vyhlásili Územie chránené občanmi - lesík obkolesilo živou reťazou okolo 2000 bratislavčanov.  
 
 

 
 
Keď sa mal pozemok predávať, ochrancovia ÚCHO zaplnili magistrát a primátor Ďurkovský tento bod rokovania radšej stiahol. V tento deň by poslanci určite hlasovali proti predaju pozemku investorovi. 
 
 
  
 
 
Keďže pozemok patril mestu, občania vyhlásili verejnú zbierku a ponúkli zastupiteľstvu vyššiu cenu, ako dával developer.  
Prečo by si mali niečo vyčítať?  
Poznám tieto stavy z nášho považskobystrického diaľničného združenia. Dohnali sme vec až na Najvyšší súd a do Európskej komisie, ale ušlo nám stavebné konanie. Má si občan vyčítať, že nechodil každé ráno kontrolovať na úrad vývesku, pretože sa musí nejako živiť a nie je ochotný obetovať jednej kauze celý svoj osobný život? 
 
 
Koreňom problému je totiž bez najmenších pochybností konkrétne hlasovanie konkrétnych bratislavských poslancov, ktorí predali pozemok developerovi napriek tomu, že pozemok chceli odkúpiť občania a premeniť ho na park.  
Ak chcete vidieť mená bratislavských poslancov, tu je výpis z hlasovacieho zariadenia.  Developeri chceli pozemok odkúpiť za 4000 Sk/m2. Občania však mali pripravenú ponuku za 4001 Sk. Vtedy poslanec Branislav Záhradník (SDKÚ) navrhol zvýšenie ceny na 5000 Sk/m2 a „modrý valec" (poslanci za SDKÚ majú v bratislavskom zastupiteľstve drvivú väčšinu) predaj odsúhlasil. Mimochodom, okolité pozemky sa predávali za 19 000 Sk/m2!  
Približne v tomto období kauza prerástla do celoslovenských rozmerov. Osobne som bol členom skupiny, delegovanej sieťou Ekofórum, aby upozornila na tento problém  vedenie SDKÚ. Po prechode do opozície totiž táto strana začala otvárať nové, zelené témy. Napríklad lesný zákon, ktorý zakazoval cyklistom vstup na lesné chodníky. Organizátori cyklodemonštrácie pred prezidentským palácom, poslanci Dzurinda a Mikloš, nám ponúkli vystúpenie na ich akcii. 16 organizácií Ekofóra s touto spoluprácou súhlasilo, ale dalo veľmi jasne najavo , že kvôli súladu v jednej téme nemienime byť nekritickí voči SDKÚ v tých oblastiach, kde je SDKÚ pri moci a uplatňuje ju proti životnému prostrediu alebo verejnému záujmu. Ako príklad zaznel od zástupcu Ekofóra z cyklistickej tribúny pred prezidentským palácom práve Lesík Krasovského: „Čerstvým príkladom je predaj pozemkov za 5000,-Sk pri Národnej kultúrnej pamiatke Sad Janka Kráľa. Napriek 2 ročnému masovému odporu občanov a odbornej verejnosti bola vašimi poslancami odsúhlasená výstavba výškových hotelov a garážového domu, na mieste súčasného lesíka. Takýchto príkladov, kde poslanci Vašej strany nepripúšťajú participáciu občanov na tvorbe svojho mesta alebo obce Vám členské organizácie siete EKOFÓRUM môžu priniesť viacero."  
 
 
  
 
 
Mikuláš Dzurinda počas prejavu ale aj potom počúval zástupcov zelených mimovládok veľmi pozorne. (V ruke práve drží žlto-čierny informačný materiál o lesíku Krasovského.) 
 
 
Rovnako pozorne nás počúval aj poslanec Frešo, a stretli sme sa kvôli tomu aj s generálnym sekretárom SDKÚ Hudecom. Všetci nám dávali zapravdu, avšak opakovali to isté: vedenie strán - a tento problém sa netýka len SDKÚ - nemá prakticky žiadne páky na svoje regionálne pobočky. „Môžeme ich len upozorňovať a dohovárať im." 
Tento pocit každý aktívny občan dôverne pozná. Tiež mu často nezostáva nič iné, než hodiť odignorované petičné hárky ľahostajným poslancom na hlavu.  
 

	
	
	
	
	

 
 
 
Obávali sme sa, že záujem špičiek SDKÚ o túto tému bola možno len profesionálna póza. Ale v sobotu, počas výrubu lesíka, sme dostali zo spoľahlivého zdroja informáciu, že kauza ignorovanej 14-tisícovej petície bola jedným z bodov, ktorými začal vážny rozpor medzi bratislavskou pobočkou SDKÚ (Mikuš, Branislav Záhradník...) a vedením SDKÚ .  
Opäť sa potvrdilo, aké je to nešťastné, keď ľudia volia poslancov na komunálnej úrovni len podľa značky strany, či podľa odlesku mena celoslovenského lídra. Ignorácia občanov potom prechádza naprieč politických spektrom: v Petržalke občanov valcuje SDKÚ, v Rači SMER (viď škandál starostu Zvonára s Materským centrom Ráčik).
 
 
Zo straníckych súbojov však u nás ešte žiadny ochranca prírody nádej nenačerpal.  
Na fotografii je zachytený moment, kedy Eliška hovorí Katke, že lesík Krasovského po vyrúbaní presadia. Zoberú výhonky starých, okoliu skvele prispôsobených stromov a práve z nich raz vyrastie nový park. 
 
 

 
 
Odrezky treba nabrať čím rýchlejšie, robotníci možu prísť odpratávať drevo aj v nedeľu ráno.  
 
 
 
 
 
Eliška sa rozbehla po rúbanisku a hľadala z každého veľkého stromu pár nepoškodených konárov. Mimovoľne som si spomenul na scénu z filmu Boj o oheň, kedy zúfalí praľudia hľadajú v nepriateľmi rozmetanej pahrebe iskričky, z ktorých by sa dal rozfúkať nový oheň. Podobnú scénu natočil režisér Annaud aj vo filme Meno Ruže: vyhorela obrovská kláštorná knižnica, a mnísi sa brodia zhoreniskom a zbožne zachraňujú obhorené kúsky stránok. Každý les je knižnicou, každý strom je knihou, popísanou neopakovateľným genetickým kódom. 
 
 

 
 
Nechýbalo veľa, a náhodný okoloidúci by na nás zavolal políciu. Čo si už myslieť o podozrivých postavách, ktoré s pílkou v ruke a baterkou na čele snoria po nočnom rúbanisku.  
 
 

 
 
Ráno sme odrezky zaviezli do záhradníctva Plantago vo Veľkom Bieli.   
 
 

 
 
Lesík v aute. 
 
 

 
 
Majitelia záhradníctva Plantago  sú sympatickí nadšenci, pred pár rokmi už takto pomáhali bratislavským ochranárom presadiť koreň obrovského platanu , vyrúbaného kvôli Riverparku na nábreží Dunaja.  
 
 

 
 
Koreň prežil, vyhnal desiatky výmladkov, z ktorých budú nové stromy.  
 
 
Tu je zopár mladých platančekov, čoskoro pripravených na vysadenie: 
 
 

 
 
Z Veľkého Bielu sme sa ešte išli pozrieť na Krasovského, pozrieť sa ako to vyzerá „deň potom", a nabrať zopár mladších výhonkov.
 
 

 
 
(V pozadí siluety Bratislavského hradu a Nového mosta.) 
 
 
Tento istý priestor bol pred niekoľkými mesiacmi obkľúčený živou reťazou:  
 
 

 
 
Aj keď väčšina lesíka padla, títo ľudia nechodili nadarmo. Podarilo sa zachrániť menšiu, cestou oddelenú časť lesíka, kde bol pôvodne plánovaný hotel.  
 
 
 
 
 
Úradníci sa síce pri prekresľovaní územného plánu „pomýlili", pozemok nezaradili medzi ochrannú zeleň, ale bratislavskí ochranári boli bdelí a tieto stromy nepadnú.  
 
 
 
PR agentúra pracujúca pre investora sa to pokúsila vydávať za „ústretovosť firmy voči občanom". Vlastne, mala pravdu. Na dnešnom Slovensku množstvo developerov berie rešpektovanie územného plánu len ako slabosť, alebo ušľachtilý postoj hodný mediálnej pochvaly. 
 
 

 
 
Napriek miliardám sú naši developeri v podstate zaostalí. Ešte sa nenaučili vnímať verejnú zeleň ako vynikajúcu šancu na dotvorenie svoje stavby - zatiaľ ju vnímajú len ako potenciálnu stavebnú parcelu. Najlepším PR by teraz bolo odkúpenie pozemku od firmy IPD a vytvorenie nového parku. To by bolo gesto hodné miliardára.  
 
 
 
 
 

 
 
Pozoroval som čo sa vlastne stane, keď zmizne taký les. Zvieratá chvíľu nechápali.  
 
 
Videli sme kŕdeľ divých kačíc, ktorý párkrát zakrúžil nad lesíkom, hľadajúc obľúbené tiché miesto, a potom odletel preč.  
Najsmutnejší bol pohľad na starých ľudí.  
Dôchodca pomaly kráčal chodníkom, ktorým chodil možno desiatky rokov na prechádzku okolo lesa. Teraz mu do očí bilo to prázdno, ktoré zostáva po zrezaných stromoch. Prišiel aj starý pán Thurzo, ktorý so svojim fotoaparátom už roky dokumentuje miznúci svet pôvodnej ľudovej architektúry.
 
 
 
 
 
Zrazu sa objavila skupinka adrenalínových cyklistov. Netrvalo dlho a už poskakovali po hromadách navŕšených kmeňov.  
 
 

 
 
Pre nich to neboli doráňané telá nádherných bytostí, ale nové telocvičné náradie.
 
 
Spomenuli sme si na deti, hrajúce sa aj na troskách vojnou zničených miest. Žiť a hrať sa treba. 
 
 
 
Nádej, o ktorej hovorím v titulku, nespočíva len v odrezkoch, zakoreňujúcich kdesi vo Veľkom Bieli. 
 
 
Dnes nám totiž už nezostáva nič iné, len ďakovať za také veľké poučenie. Dostala ho každá skupina, ktorá sa podieľala na príbehu lesíka Krasovského.    
 
 
1. Občianski aktivisti. Kurt Vonnegut takýmto ľuďom už dávno odkázal: „Nie je dôvod, prečo by dobro nemohlo zvíťaziť nad zlom, iba sa anjeli musia zorganizovať na spôsob mafie." Opäť sa potvrdilo, že takéto kauzy sa prehrávajú na detailoch. Keby mali včas informáciu o stavebnom povolení... keby podali odvolanie s odkladným účinkom... keby ochrancovia ÚCHO udržali vlastnými telami stromy čo len jediný deň, vypršala by platnosť výrubového konania... Mafiáni určite neskladujú dokumenty v prútených košoch v kuchyni a nehrabú sa po nociach svojpomocne v zákonoch. Pravda a verejná podpora nám dávajú veľkú silu, ale na druhej strane sa točia miliardy. Nemôžeme sa spoliehať na sľuby politikov a úradníkov, úprimné výrazy v tvárach sú súčasťou ich profesie. Teraz musia aktivisti kauzu dotiahnuť, dohnať k zodpovednosti všetkých, ktorí sa dopustili nejakého pochybenia, inak zákonite stratia autoritu.  
2. Úradníci. Zistili, že sa nemôžu spoliehať na to, že ich mená zostanú v utajení. Pavlovičová, Margočová, Mišovichová, Zahradník, Mikuš, Ďurkovský, Ftáčnik... 
 
3. Politici. Musia sa rozhodnúť, či mienia naďalej vyhrávať voľby pomocou masmediálnej manipulácie platenej korupčnými peniazmi a straníckym výpalným zo všetkých tých dopravných či drevárskych podnikov.  Alebo vsadia na podporu organizovaných občanov, u ktorých si získali autoritu riešením reálnych problémov. 
 
4. Investori. Keby teraz za nimi prišla tá pani, čo sľubovala že občania budú ako obvykle ľahostajní a neinformovaní, a ona im všetko vybaví na úradoch a v zastupiteľstve, asi by ju hnali. Keby šli na „čistý" pozemok, hoci aj niekoľkonásobne drahší, ich polyfunkčný mrakodrap už dávno mohol stáť. Sobotňajším výrubom zdanlivo vyhrali, ale bolo to Pyrrhovo víťazstvo. Stratu času, peňazí a škody na dobrom mene ťažko vyčísliť.
 
 
5. Obyčajní ľudia, nahnevaní občania. Uvedomme si konečne, že podpísať nejakú petíciu alebo zamávať transparentom nestačí.  Keď vás nabudúce osloví nejaký aktivista vo veci ktorú považujete za správnu, povedzte hneď po podpísaní petície vetu: „Chcem vám ponúknuť hodinu mojej práce alebo sto korún." Ak sa tí idealisti budú brániť, že netreba, spýtajte sa ich: „Koľko ste pretelefonovali len za tento týždeň kvôli vašej kauze? Dávate právnikovi toľko peňazí, aby vo vašej kauze mohol pracovať naplno a neušiel vám nejaký hlúpy dátum?" A ak aj tak nedokážu prijať vašu zhúžvanú stokorunu, pridajte sa k nim alebo vytvorte s priateľmi vlastnú skupinu. Je nás zatiaľ málo, ale cítiť to vo vzduchu - časy sa menia. Urobme všetko preto, aby bol lesík Krasovského našou poslednou veľkou stratou.   
 
 
Lesík Krasovského padol,  hoci sa čoskoro znovozrodí z konárikov, ktoré čakajú na svoj čas v záhradníctve vo Veľkom Bieli. Ale park na Belopotockého , parčík a ihrisko na Pribinovej ulici v Považskej Bystrici , les v Tichej a Kôprovej doline , desiatky známych i neznámych miest sa ešte držia.  
 
 

Ako dlho sa udržia, záleží aj na vás. 
 

