
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Patrik Nemes
                                        &gt;
                Aussieland
                     
                 Thajsky kulturny festival a volba Miss Thai v Melbourne. 

        
            
                                    26.3.2010
            o
            8:30
                        (upravené
                26.3.2010
                o
                10:15)
                        |
            Karma článku:
                4.46
            |
            Prečítané 
            856-krát
                    
         
     
         
             

                 
                    Pred par dnami som sa vybral s fotoaparatom na Federation Square tu v Melbourne. Bolo nadherne pocasie. Vedel som, ze na namesti bude Thajsky Festival ich kultury a jedla, avsak o programe som nemal vela informacii. To, co som vsak neskor videl na podiu sa mi velmi pacilo. Mix uzasnych kostymov a farieb, tradicnej hudby a tancov sa prelinal s volbou Thajskej kralovnej krasy a sutazou v thajskom kickboxe :) Pozyvam vas cez moje fotky na malu prechadzku rozpravkou :)))  
                 

                    1. Ked som prisiel na namestie, bubenici uz koncili svoje bubnujuce predstavenie a "nezname postavicky" sa len tak ponevierali okolo :)      2. Pri kazdej akcii v meste sa mysli na deti a tak rozni sasovia maju o zabavu postarane :) Balonikove zvieratka a vecicky sa rozdavaju a potesia mnohych. Inak tou pistolkou ma potom odstrelil, ked som ho odfotil :)))      3. Priprava pred sutazou krasy.... vsetko musi byt ok...      4. Poza pred sutazou      5. Netusim, co znazornuje tato postavicka, ale vyraz ma zaujimavy :)      6. Javiskova spirala :)      7. Krasne motyle dotancovali a uklonili sa.... inak uklon so zopatymi rukami musel byt zakazdym :) Zaber je z Blessing Dance.      8. Oficialna cast otvorenia festivalu...      9. Starosta mesta Melbourne sa prihovoril, pochvalil vsetkych a prestrihol oficialne pasku...      10. Hymna sa spievala najskor australska a po nej aj thajska.... kazdy spieval tu co poznal :)      11. Spievat sa nazivo nespievalo ale tancovat sa zato tancovalo :) Tento zaber je z Muay Thai Worship Dance.      12. No a sutaz thajskej krasy sa zacala.... vsetky sutaziace som sem nedal ale mozete si ich prezriet v linku pod clankom.      13. Porotu tvorili styria porotcovia... hodnotila sa nielen krasa sutaziacej...      14. Toto je najaky tradicny odev z niektorej lokality Thajska.....      15. Slnko palilo ale slecnam nevadilo...aspon sa teda tvarili :)      16. Na zaciatku som mal jedenastku kandidatku spolu s inou sutaziacou co sa nedostala do vyberu piatich... no a sutaziaca s cislom 11 sa stala novou "Miss Thai 2010"      17. Chcel som vyskusat co to z thajskej kuchyne ale cakat a pretlacat sa cez hladnych ludi sa mi nechcelo...mozno nabuduce skocim do nejakej thajskej restiky v meste....      18. V maziarikoch pomiesali vselico mozne a papanica hotova :))))      19. Koho nebavila sutaz krasy tak si bol pozriet thai kickbox...      20. Tento zapas trval asi jednu minutu.... hrac z cerveneho rohu odstupil pre vyskoceny klb v ramene ....bad luck mate.      21. Vratme sa vsak k rozpravkovym postavickam na javisku...      22. Pozy vyjadrujuce rozne nalady a charakter ucinkujuceho.      23. Tanecnici medzinarodnej tanecnej skupiny Sbun Nga.      24. "Bird Dance"  Vtaci tanec.....      25. Kostymy hrali roznymi farbami.... a usmevom sa nesetrilo.... :)))      26. Poklona tanecnikov po skvelom tanecnom cisle.      27. A zasa iny styl ....      28. Naspat do rozpravky... :)      28. Skupina bola na turne v Australii...na druhy den vystupovali v Sydney.      29. Usmev, radost, mladost..... svet gombicka :)      30. Tanecnici pridali nieco aj z tanca moderneho a tiez zapojili ludi z obecenstva....      31. Jedna z poslednych poklon po skvelom tanecnom prevedeni... boli odmeneni burlivym potleskom z obecenstva.      32.Na zaver nam vsetkym zakyvali vlajockami, rozpravke bol koniec a festival pokracoval vo svojom vecernom programe hudobnym koncertom.       Ak si chcete prezriet viacero fotiek tak ich viac najdete tu a z volby Miss Thai 2010 tu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Nemes 
                                        
                                            Klobukova Jar (fotografie)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Nemes 
                                        
                                            Taky obycajny den... (fotografie z ulice)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Nemes 
                                        
                                            Autosalon Melbourne 2011
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Nemes 
                                        
                                            Melbourne Moomba 2011
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Patrik Nemes 
                                        
                                            Australian Open 2011 (fotky)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Patrik Nemes
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Patrik Nemes
            
         
        nemes.blog.sme.sk (rss)
         
                                     
     
        vo volnom case rad fotim...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    37
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1516
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Aussieland
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




