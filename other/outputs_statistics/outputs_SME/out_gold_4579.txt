

 Neprešli ani dva dni, keď mi moja najlepšia kamarátka Erika zavolala uprostred dňa a v trochu nesúvislom monólogu sa mi snažila podať bárs akú skvelú správu. Tušila som, že je skvelá, pretože okrem jej speedy reči som v zákulí registrovala jačanie a výskanie. Až po piatich minútach mi  bola schopná vysvetliť, čo sa stalo. "Vydali ťa", kričala. Hej a za koho?, pomyslela som si v tej chvíli. "Publikovali tvoj blog", opakovala v pravidelných intervaloch. Ešte stále som nechápala a začínala som sa cítiť ako náš Ďuri Kemka, ktorý má podobnú prirodzenú neschopnosť dávať si dve a dve dokopy. "Oni ťa publikovali, chápeš?. Budeš sa so mnou ešte kamarátiť? Dáš mi podpis? Prehovoríš so mnou ešte niekedy?", sypala na mňa jednu otázku za druhou. 
 Keď som to všetko predýchala, došlo mi, že vydali môj článok. Ešte v ten deň som si v mobile našla správu: "Máš to v schránke ty spisovateľka." Tak rýchlo ako mi moje krátke nohy dovolili som odšprintovala k schránke a o pár minút som už čítala svoj prvý publikovaný blog s názvom Je ok ak poviete stop ľuďom, ktorým niet ani rady, ani pomoci." Vyšiel v prílohe SME pre ženy a ja som stále nemohla uveriť, že pod ním svieti moje meno. 
 Je to skvelý pocit a  popravde ho neviem ani popísať. Pre mňa je to akési zadusťučinenie a motivácia, že  predsa len existujú ľudia, ktorým sa páči, čo píšem. Viem, že sa mám ešte veľa, čo učiť, ale ako povedal môj otec: "Prvá lastovička je už na svete, dcéra moja." 
 Týmto  kratučkým ďakovným blogom chcem vyjadriť vďaku tým, ktorí sa zaslúžili o moje prvé publikovanie. 
 Vrelá vďaka všetkým. 
 Vaša Radečka :D 

