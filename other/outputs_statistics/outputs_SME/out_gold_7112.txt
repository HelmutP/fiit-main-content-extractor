

  
 Postupne sa Lucerna zapĺňa hosťami. 
  
 Miesta pre hudobníkov síce zatiaľ zývajú prázdnotou ale ako podmaz slúži hudba z reproduktorov a nekonečná ľudská vrava. 
  
 Úvodný tanec zahajuje Ondřej Havelka so svojou tanečničkou. 
  
 A ide im to veľmi dobre. 
  
 Všetko však raz končí a aj táto sympatická tanečná dvojica sa lúči. 
  
 Nastupuje poloprofesionálny tanečný pár, ktorý predvádza svoju charlestonovskú verziu. 
  
 Parket je prázdny a všetci s napätím pozorujú krok za krokom. 
  
 Niektoré tanečné kreácie už v okolostojacich vyvolávajú pocit, že je čas na vlastné prevedenie. 
  
 A veru čas nastal. Ondřej nôti úvodnú melódiu. 
  
 Spokojne sleduje ako sa tanečný parket zapĺňa. 
  
 Tešia ho radostné výrazy na tvárach tanečníkov. 
  
 Rovnako ako nadšené zanietenie pri krútení sa tanečných partneriek. 
  
 Výhľad prenecháva trojici spevavých dám, nech si tiež vychutnajú pohľad na tancachtivé obecenstvo. 
  
 A veru, niektorí hostia tancujú tak rýchlo, až by sa z toho jednému zakrútila hlava. 
  
 Trosičku, ale naozaj len trošičku spomalia. 
  
 Zahľadia sa na svoje nohy. 
  
 Chytia sa za ruky. 
  
 Pozrú si z očí do očí. 
  
 Tí, ktorí sa cítia zdatní, sa prihlasujú na súťažné kolo. 
  
 Kapela s napätím sleduje, koľkým súťažiacim bude hrať do kroku. 
  
 Tí, ktorí sa na súťaž necítia, si v prítmí tancujú svoje súkromé tanečné kolo. 
  
 Súťažiaci sa veru nehanbia a skáču z nohy na nohu. 
  
 Po skončení súťažného kola sa opäť všetci spoločne venujú tónom a krokom charlestonu. Len tak, pre vlastné potešenie. 
  
 Ondřej ohlasuje novinku. 
  
 Pripravujeme sa na prekvapenie. 
  
 Tým prekvapením je profesionálna dvojica majstrov v tanci charlestonu priamo z Londýna, ktorá nás naučí niekoľko tanečných variácií. 
  
 One step left, one step right ... 
  
 Show your emotions and feelings ... 
  
 You are good ... 
  
 Posledná pieseň pre majstrov. 
  
 Ondřeja tiež chytila chuť trochu sa pohúpať z boku na bok. 
  
 Do rytmu mu spievajú krásne devy s ešte krajším pozadím. 
  
 Sú ako zo starodávneho filmu. 
  
 Krásne odeté a jemne sa smejúce. Pravé dámy. 
 Dúfam, že ste si so mnou príjemne zatancovali aj Vy. Zas raz teda niekedy - do tancovania :-). 
   

