
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Dunaj
                                        &gt;
                Nezaradené
                     
                 Aj na domácich teroristov existujú lieky 

        
            
                                    7.6.2010
            o
            21:39
                        (upravené
                7.6.2010
                o
                21:44)
                        |
            Karma článku:
                3.16
            |
            Prečítané 
            583-krát
                    
         
     
         
             

                 
                    Týrané ženy spravidla mlčia. Mnohokrát v záujme zachovania rodiny alebo pociťujú hanbu či dokonca vinu. Ešte častejšie z dôvodu, že o svojom týraní vlastne ani nevedia, „iba cítia diskomfort“.
                 

                   
 Domáce násilie na ženách je ešte aj v súčasnosti tabuizovanou témou. Mnoho týraných žien považuje násilie za dverami svojho bytu za súkromný problém svojej rodiny. Azda každý z nás však zachytil fakt, vyplývajúci z výsledkov kampane mimovládnych organizácií, podľa ktorých má každá 5. žena na Slovensku skúsenosť s domácim násilím. Výsledky spomínaného prieskumu však vyvracajú predstavu, že násilím trpia len ženy zo sociálne slabších rodín, či ženy s nižším stupňom vzdelania. Nezatvárajme preto oči nad týmto problémom. Dimenzia domáceho násilia je alarmujúca a žiadna spoločnosť nemôže tvrdiť, že sa v nej nevyskytuje. Domáce týranie nie je len súkromným problémom týranej ženy, je to celospoločenský problém . V prevažnej väčšine prípadov je obeťou násilia žena - manželka, partnerka či družka. Domáce násilie má množstvo foriem. Fyzické násilie od fackovania, až po pokus o vraždu. Inou, no nemenej závažnou formou je psychické týranie. Psychické násilie zahŕňa správanie, ktorého účelom je zastrašiť alebo prenasledovať. Tento druh násilia sa prejavuje vo forme hrozieb opustenia alebo týrania, domáceho väznenia, neustálym dohľadom, hrozbami odobrať právo opatrovať deti, hrozbami ničenia predmetov, izoláciou, verbálnym útočením a neustálym ponižovaním. Takto týrané ženy spravidla mlčia. Mnohokrát v záujme zachovania rodiny alebo pociťujú hanbu či dokonca vinu. Ešte častejšie z dôvodu, že o svojom týraní vlastne ani nevedia, „iba cítia diskomfort“.    Ešte stále sa pričasto pripisuje vina obetiam za to, že násilie vyprovokovali, alebo že sa nedokážu vymaniť zo vzťahov poznačených násilím. Psychoterorista je totiž prefíkaný a vie svoju obeť manipulovať. Čo je ešte tragikomickejšie, nestáva sa to iba ženám. Istá štúdia nedávno zverejnila štatistiku týrania mužov, ktorá sa vo svete v rôznych krajinách pohybuje od 5 do 20%. Experti v oblasti násilia páchaného na ženách či mužoch ako príčiny uvádzajú nečinnosť v konaní proti násiliu, predsudky, mýty a spoločenské tolerovanie domáceho násilia. Ešte masívnejšie sú nálezy v komunitách.  Ako ďalšie príčiny uvádzajú skutočnosť, že agresori za svoje správanie nie sú sankciovaní, taktiež zneužívanie nerovnomerného rozdelenia moci medzi mužmi a ženami i ekonomická závislosť ženy od muža. Dobre a povedzme si pravdu, koľko bolo prípadov z praxe, že týraná žena prišla na policajnú stanicu a verejná moc sa správala akoby viac ochraňovala samotného ubližovateľa než obeť?   Násilníci často nevnímajú svoje správanie ako výsledok naučeného správania a neveria tomu, že ich násilné správanie môže mať negatívne dôsledky, to je ten horší prípad, ktorý má korene vo výchove alebo prístupe spoločnosti.    Násilie voči ženám a vo vzácnych prípadoch aj voči mužom sa týka nás všetkých. Zo strany štátu je možnosť kontaktovať Úrady práce, sociálnych vecí a rodiny. Ich pracovníci im musia zabezpečiť poradenstvo a v prípade potreby aj psychologickú pomoc. V prípade neplnoletých osôb má právo zisťovať rodinné a sociálne pomery dieťaťa a navrhovať súdu úpravu výkonu rodičovských práv. Na úrovni samospráv je povinná poskytnúť pomoc obetiam domáceho násilia obec. Po novelizácii zákona prešli originálne kompetencie zo samosprávnych krajov práve na obce, pre ktoré je udržanie takýchto zariadení luxusom. Súhlasím s Editou Pfundtnerovou, že domovy, útulky, krízové centrá či azylové domy potrebujú väčšiu všímavosť a legislatívnu úpravu zo strany štátu, taktiež vyriešenie otázky ich financovania. Tak, ako sa dopodrobna rozpiplal a upravil zákon v prípade hniezd záchrany, čo bol vynikajúci krok,  potrebujú občas záchranu aj mamy. Lebo ak bude postarané o nich, tak budú aj spomínané hniezda zívať prázdnotou.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Keď nás tu bude 100 000 Jano sa oholí!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Manažérov denník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Najlepší kúzelnícky trik na svete!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Funkčná rodina II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Konkurz do zamestnania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Dunaj
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Dunaj
            
         
        dunaj.blog.sme.sk (rss)
         
                                     
     
        Žijeme tak krátky život až mi je to smiešne ako sa staviame do roly toho, ktorý má vo svojom eLVéčku všetky planéty. Niekto je hore a niekto je dolu. Ten dolu je na ceste a potkýna sa o ďalších
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    694
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




