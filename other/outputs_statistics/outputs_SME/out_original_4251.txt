
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslava Balážová
                                        &gt;
                Sila spomienok
                     
                 Najlepší priateľ 

        
            
                                    9.4.2010
            o
            20:45
                        (upravené
                9.4.2010
                o
                20:53)
                        |
            Karma článku:
                4.73
            |
            Prečítané 
            647-krát
                    
         
     
         
             

                 
                    Pre milovníkov zvierat, špeciálne mačičiek ......
                 

                      Prichádza jar, pomaly, ale isto .........       No u mňa je stále zima, chlad, srieň mi padá do duše....... nenávratne .......       Stromy začnú pomaly nahadzovať listy, trávička sa zazelenie, už vidieť spod snehu prvé snežienky, ich krehké hlávky sa nakláňajú nad zmrznutou zeminou záhradiek. Milé.       Už ani mrazov nebolo, ale dnes ráno meteostanica zase ukazuje mínus štyri  L       Mínus jeden život.       Mínus jedno priateľstvo.       Mínus jedna oddaná dušička .       Mínus jeden zo zmyslov života.       Prečo strata tak veľmi bolí ?       Prečo sa musí umierať ?       Nemá sa oplakávať, kto ešte žije, hoci má vymeraný čas na tejto Zemi. Ale dá sa to  ???       Teória je pomerne nekomplikovaná, jednoducho sa predkladá, no prax - to je už „iná káva".       Cítim si vlastné slzy -  sú slané a teplé, také mám aj líca, aké iné by mali byť, keď tadiaľ vedú   potôčiky sĺz ?       Život je síce krutý, ale aj krásny . Poďme spomínať na to krásne .........Vstúpte do komnaty spomienok  - nech sa páči .       Tri malinké klbôčka - dve trojfarebné, jedno hrdzavobiele. Mäkké kožúšky. Velikánske najprv modré, neskôr zelené okále. MAČIČKY. Naše tri poklady spomedzi mnohých pred nimi.    Šantenie po veľkej záhrade, pomedzi hriadky zemiakov, prelet nad kvietkami, po mäkkej trávičke.... Nádherný obraz, keď unavené prídu k mame a pijú mliečko. Neskôr všetky tri obstanú misku a počuť už len slastné mľaskanie J Teplé mliečko robí zázraky...... aspoň, kým boli malí, neskôr robilo „iné" zázraky ! Postupne zistili, že ich teritórium siaha aj do domu -  chodba, kuchyňa, pravdaže, aj izby treba preskúmať, a to najmä periny . Šup do nich -  aké mäkké..... Neskôr najobľúbenejšie miesto. V zime zase na koberčeku pod gamatkami.... No lenivé neboli, Božechráň -  každý z nich chytil denne minimálne dve myšky, ktoré doniesol pekne na dvor ukázať / o tých sme teda logicky vedeli / . Úplný a presný počet je nám záhadou. Maznanie, tuľkanie, papanie - ich celodenná činnosť kombinovaná so spinkaním. Pohoda a balzam na dušu -  MOJU, našu. Pradenie, vrnenie, alebo ako to kto nazýva - ozaj príjemnejší zvuk nepoznám. My sme to mali trojnásobne. Ako aj tri misky, tri nádobky na ..... veď viete na čo J Neskôr sa to zdvojnásobilo , odvlani ubudla ďalšia micka ......... nie je už ani ten dvor, ani tie záhony zemiakov, ani kvetinové hriadky.........ostali len tieto spomienky a posledný kocúrik..... ten najkrajší, najmilší, hrdzavobiely okánik. Pozerá na mňa tými svojimi nádhernými očkami a ja mu neviem pomôcť .    Alebo viem ? Je na veterine, v teplúčku, každý deň dobré jedlo....... no koniec sa blíži v nenávratne .        Denne sa chodím tešiť ale aj trápiť .       Láskavá náruč veterinárky Mišky ........ a moja.  Len neviem, či ma to teší, alebo skôr trápi......   Sama v sebe sa nevyznám.......som zmätená .......postupne odchádza 20 rokov môjho života, ktorý bol aj vďaka Ryškovi zmysluplný.       Ostanú spomienky, fotografie, zážitky, pocity -  a to je dosť, dosť na to, aby som si uvedomila, že všetko raz začína a raz končí. Toť neyvrátiteľný kolobeh života .....       O pár dní bude koniec.       Alebo nový začiatok  ? Možno  - nové tri klbká ......... J       Ryšinko, si ešte medzi nami, môj drahý kocúrik, strašne ťa mám rada, a ďakujem , že som s Tebou mohla prežiť naozaj nádherných 20 (!!!!!!!!!) rokov. Už si zaslúžiš pokoj, a aby ťa už nič neboľkalo. Aký bol Tvoj kocúrikovský život, pokojný, nikdy si ani len nezavrčal, ani teraz, keď Ti pani doktorka ošetruje očko a robí všakovaké divy s Tebou, vrátane injekcií, taký dúfam, bude pre mňa aj život bez Teba ...... Ryšičko môj .................kocúriček............. L       Je mi hrozne.       Idem za Ryškom. Pohladkať, pritúliť, popočúvať pradenie ...... poďakovať ......  poplakať si .       apríl 2010       Ryško je stále tu, medzi nami, ešte niet miesta pre slzy. A možno ani nijaké nebudú, veď mal a stále má krásny život, je obklopený ľuďmi,ktorí milujú tieto milé šelmičky.....       20 rokov .................veľa alebo málo ?   Veľa pre krásne chvíle, veľa kvôli nášmu priateľstvu, veľa.......veľa pre všetko.   Ale málo pre to, aby som vyjadrila, ako mne a celej našej rodine naplnil tento milý hrdzánik život. Naučil nás mnohému, daroval nám veľa mačičkovskej lásky, veľa tepla do sŕdc, veľa pritúlení, veľa všetkého, čo len mačička môže človeku dať.   Dáva nám to najcennejšie -  seba.   Ryško, veľmi Ťa ľúbim !!!!!!!!   Nikdy nezabudnem ......             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslava Balážová 
                                        
                                            Prajme si zdravie  !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslava Balážová 
                                        
                                            Výberové konanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslava Balážová 
                                        
                                            Je rodičovská dovolenka naozaj dovolenkou ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslava Balážová 
                                        
                                            Čo dokáže 30 minút naviac ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslava Balážová 
                                        
                                            Čo zraňuje detskú dušičku ?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslava Balážová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslava Balážová
            
         
        jaroslavabalazova.blog.sme.sk (rss)
         
                                     
     
        Som, kto som :o)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    904
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Napísané životom
                        
                     
                                     
                        
                            Ach,tie deti .....
                        
                     
                                     
                        
                            Sila spomienok
                        
                     
                                     
                        
                            Čo ma trápi
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




