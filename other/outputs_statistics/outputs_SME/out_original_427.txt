
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alfonz Halenár
                                        &gt;
                Komunisti okolo nás
                     
                 Komu klame Milan Čič? - II/III 

        
            
                                    4.7.2005
            o
            7:36
                        |
            Karma článku:
                12.07
            |
            Prečítané 
            3852-krát
                    
         
     
         
             

                 
                    Aj keď je zákonom č.125/1996 komunistický režim považovaný za nemorálny a protiprávny, jeho čelní predstavitelia dodnes zastávajú rozhodujúce posty v tomto štáte. Jedným z nich je aj komunista Čič. Zastával vysoké posty za čias KSČ, zastáva ich aj po zrútení režimu v roku 1989. Komu klame Čič? Klamal komunistom alebo klame dnes? Alebo je ten dnešný spoločenský systém natoľko podobný tomu do r. 1989, že nemusí klamať nikomu?!
                 

                 
 oficiálna stránka prezidenta SR
   Na pevný postoj Čiča k socialistickému zriadeniu ČSSR ukazuje jeho rozhovor s istým súdruhom Rusňákom, ktorý mu v r. 1988 ako ministrovi spravodlivosti SSR prišiel povedať o hospodárskej trestnej činnosti na JRD. Hnev súdruha Rusňáka bol taký, že naskutku chcel vystúpiť z KSČ. Nato Čič odvetil, že svojim konaním podkopáva budovanie komunizmu a ak vystúpi z KSČ, nebude mať právo na spravodlivosť...(!)   Podľa príslovia o hadovi na prsiach, v júni 1990 sa Čič stal opäť predsedom vlády. Vláde SSR totiž predsedal už od decembra 1989 spolu s komunistami V. Lexom (otcom komunistu I. Lexu), Mečiarom, Duckým a udavačmi pre ŠtB ako Markuš.  Nemuseli sa veľmi červenať. VPN a OF dali na kadidátky množstvo členov KSČ, takže sa to na vrcholných postoch v ČSSR komunistami len tak hemžilo - predsedom vlády ČR sa stal komunista Pithart a predsedom federálnej vlády komunista Čalfa. Takto prebehla handrová na najdôležitejších postoch.   Keď boli Mečiarove amnestie z roka 1998 na Ústavnom súde, predsedal mu Milan Čič (1993-2000). Môžme ďakovať opäť jeho "citu pre spravodlivosť", že boli v plnom rozsahu uznané za platné? Isté je iba to, že s dôsledkami amnestií zastupujúceho prezidenta Mečiara sa naša spoločnosť bude potýkať ešte dlho.  Po ukončení práce na ÚS sa ihneď stal podpredsedom strany Stred (Mjartan) - čím ukázal, ako vyzerá "nezávislý" a nadstranícky predseda ÚS. Zo strany Stred po doporučení(...!) v r. 2001 vystúpil a vo voľbách do VÚC kandidoval na predsedu VÚC za koalíciu HZDS/SMER/SDĽ/SOP/ZRS.  Dnes sa opäť naplno venuje politike. Podľa TV záberov sa zdá, že je pravou rukou prezidenta Gašparoviča. Gašparovičovi sa niet čo čudovať: sám roky ukazuje, že myslí a cíti komunisticky. Tí, čo volili opäť "menšie zlo", posunuli vlastne do popredia aj Čiča...   Skutočne nás na najdôležitejších postoch majú zastupovať ľudia, ktorí už desaťročia ukazujú, nakoľko dokážu byť rôzni v rôznych podmienkach? V 50. rokoch jestvovali komisie, ktoré vylučovali študentov z vysokých škôl v rámci tzv. triedneho boja. V jednej takej sedel aj Milan Čič. Signatár Charty 77 a odporca prednovembrového režimu p. Vladimír Pavlík mi k tomu poskytol dôkaz - kópiu zápisnice.  
 Ukážka 1. strany úprimného a kultúrneho jednania...  
 Neviete, kto boli - okrem Čiča - tí, čo vyhadzovali študentov pre ich "závadné" kádrové materiály?  Peter Colotka: 1969 - 1988 predseda vlády SSR, podpredseda vlády ČSSR, člen ÚV KSČ. A čo mená Štefan Luby a Karol Plank...?   P. Pavlík mi taktiež poskytol kópie zo Socialistického súdnictva č.4/1970, kde Čič uverejnil článok, v ktorom zákony č. 231/1948 Zb. a č. 232/1948 Zb., podľa ktorých komunistickí zločinci v 50. rokoch obesili mnoho nevinných ľudí - včítane Milady Horákovej, označil za významný nástroj v boji s kontrarevolučnými živlami...! Zoscanované ich nájdete v III/III, spolu s ostatnými stranami zápisnice a Čičovým podpisom.   A čo na to povieme my - trpko sa pousmejeme a pôjdeme si odkrojiť ďalší chlieb s maslom a šunkou a s chuťou sa doň zahryzneme? Alebo radšej na toho, kto takto píše, zaútočime s poznámkou: mám rád takých bojovníkov za spravodlivosť 16 rokov po...!?   Som presvedčený, že niektorí z veľkého množstva statočných, schopných a minulým režimom nezaťažených ľudí, sa po tomto nebudú báť ukázať na zločincov, čo sedia na ústavných postoch alebo v samospráve. Lebo ak tak učiníme viacerí, ľudia spozornejú a začnú si dávať veci do súvisu. Začnú premýšľať nad vlastným zmyslom pre Spravodlivosť. A možno v ďalších voľbách nedajú komunistovi - v akejkoľvek strane dnes je! - svoj hlas.   Ak sa to podarí, slovo Spravodlivosť sa posunie bližšie smerom k tejto zemi.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Hliadka MsP na stanovišti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Mestský zákon o informovaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: S Kositom na večné časy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: možná korupcia na MZ feb23_2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alfonz Halenár 
                                        
                                            Ke: Sneh a psí trus
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alfonz Halenár
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alfonz Halenár
            
         
        halenar.blog.sme.sk (rss)
         
                                     
     
        Pracovník IT, poslanec Košíc a poslanec Košického kraja.
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    114
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2932
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Samospráva
                        
                     
                                     
                        
                            Demokracia
                        
                     
                                     
                        
                            Komunisti okolo nás
                        
                     
                                     
                        
                            Politické strany
                        
                     
                                     
                        
                            Aktuálne reakcie
                        
                     
                                     
                        
                            Poslanci Košíc
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Barbara Masin: Odkaz
                                     
                                                                             
                                            Štuktúry moci na Slovensku 1948-1989
                                     
                                                                             
                                            Josef Frolík: Špion vypovídá
                                     
                                                                             
                                            Margaret Thatcher: Umění vládnout
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Black Eyed Peas: I´ve got a feeling
                                     
                                                                             
                                            Faustas blog (skvelý podcast)
                                     
                                                                             
                                            BBC podcasty
                                     
                                                                             
                                            ČRo: Lidé pera (podcast)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Barbara Masin: Gauntlet (anglicka verzia knihy)
                                     
                                                                             
                                            D-FENS (politika, spoločnosť,...)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




