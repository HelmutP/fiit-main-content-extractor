

 Blížiace sa voľby zo sebou nesú aj nežiaducky plnú schránku. Najde sa v nej mnoho terčov na hranie šípok. Niekedy si zahádžete do Fica, inokedy do Slotu a prejde istý čas a prestane Vás to baviť. 
 Odkedy sa začala predvolebná kampaň, tak  najdeme v schránke nejaký plagátik s tvárou lídra strany. Tak ma takto raz neskoro v noci napadlo ako s tými plagátikmi osožne naložiť. Kúriť sa už s nimi nedá, bo už máme plyn. Tak som vymyslel hru. Hrať môžu maximálne piati, aby sa striedalo čo najrýchlejšie, narysujú sa kruhy po tvárach politikov, určia sa body za určitú časť. Pokiaľ to bude ako si to predstavujem, tak tých plagátikov bude aj málo. Veď do politikov si znovu štyri roky nezaháždeme. Šípky do politikov aj do čaju, pre priateľou je názov mojej novej spoločenskej hry. Príjemné chvíle pri šípkovom čaje  so šípkami v čele s (ne)priateľmi. 
   

