

 V playboyskej samote mi pozerá do tváre 
 a hľadá stopy po minulosti. 
   
 Rozsvietim večer kvapkami, 
 myslím na dážď a tvoje ruky, 
 v ktorých som našiel 
 svet ako zápalková krabička. 
   
 Horím.  Verím listom, 
 ktoré leto posiela strom iba doporučene. 
 V nich stojí tajomstvo mojej diagnózy. 
   
 Dnes som sa zaľúbil. 

