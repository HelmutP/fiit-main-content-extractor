
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Dian
                                        &gt;
                Nezaradené
                     
                 Malta (splnený sen) - za pár euro 

        
            
                                    21.6.2010
            o
            9:23
                        (upravené
                29.7.2010
                o
                21:59)
                        |
            Karma článku:
                3.90
            |
            Prečítané 
            785-krát
                    
         
     
         
             

                 
                    Niekde v tretine cesty loďou zo Sicílie do Tunisu sa nachádza malý štátik európskej únie. Skladá sa z troch ostrovov a hrdo nesie názov Malta. Až donedávna som si myslel, že tento ostrov je osamotený. Lenže k nemu patria ešte ostrovy Commino a Gozo. Dôvod na to, aby som si splnil konečne svoj mladícky sen a išiel sa pozrieť ako to v tomto maličkom štátiku stredozemného mora vypadá. Pretože toto spoznávanie nebolo na mesiace ale len na dva dni, veľa vecí som nepotreboval. Dôležitejšie boli záznamové médiá, aby som sa mohol podeliť o tento zážitok aj s Vami všetkými. V mojom batohu preto nechýbala kamera, mimochodom prvý krát som sa ju odhodlal použiť pod vodou, a samozrejme fotoaparát. Ja som pripravený. A vy?
                 

                 Po prehliadke Bologne a zdokumentovaní krás tejto metropole Talianska, som konečne poobede sedel v lietadle na Maltu. Dopredu som si pripravoval pristávací manéver na malte, pretože som to chcel zdokumentovať. Vidieť ostrovy z vtáčej perspektívy je predsa len o niečom inom ako ich mať po ruke na zemi. Vzlietli sme. Asi po 15 minútach obchádzame ostrov Sardíniu a na celých 45 minút som nevidel z okienka Boeingu 737 nič len oblaky a ich tiene nad morom.   Po chvíľke sa z reproduktorov ozval hlas pilota ktorý hlásil výšku 12 tisíc metrov a to najdôležitejšie - Palermo a o chvíľku aj Trapani. Mimochodom moja ďalšia vysnívaná destinácia. V tom momente som vedel že sedím správne. Z pravej strany som za daľších 15 minút mohol už pozerať na jeden z troch maltských ostrovov Gozo. Vedel som že za chvíľku budem už pevne na nohách stáť na Malte. Celé to pristátie som si natočil na kameru. Let bol hladký a celkom pohodlný.   Už keď som vystúpil z lietadla, dostal som facku v podobe tepla, ktoré tu panovalo. Po tom daždi v Bologni to bola príjemná zmena. Už v letiskovej hale som vedel že tu sa mi bude veľmi páčiť a tiež ma prepadávala myšlienka, prečo iba na dva dni. Prechod cez letiskovú halu je úchvatný. Možno aj na letisku v Bratislave či Košiciach by sme si mohli zobrať príklad z letiska na Malte. Krok za krokom je Vám podávaná do očí história a vznik Malty. Od prehistórie až po súčasnosť.   Tou históriou som vlastne na Malte aj začal.      Na letisku som čakal na bus do Valletty odkiaľ som mal ďalší bus do Qawra strediska. Ten autobus č. 8 bol však niečo na čo som sa veľmi tešil. Keď už dorazil, bol to pre mňa šok. Ten autobus poznal podľa mňa druhú svetovú vojnu a tu jazdil!!! A úplne v pohode! Červeno žlté zafarbenia, skoro lavice na sedenie, zvonček na zastavenie na lanku, skoro bez okien...a to všetko za smiešnych 47 centov!       Dobrodružstvo môže začať.   Cesta do Valletty trvá asi 30 minút a bus prekonáva snáď aj nadzvukovú rýchlosť. Hlavne cestou dolu kopcom. Unesený týmto busom som točil aj von z okna, fotil v buse a kochal sa cestou. Po chvíľke však oproti ide Škoda 120 a za ňou Škoda Favorit a v mojej hlave sa myšlienky menili na skutočnosť. Návrat do minulosti. Vrátil som sa o 30 rokov späť!         Po 30 minútach v buse som konečne dorazil na autobusovú stanicu pred bránou do Valletty. Je to vlastne len jeden veľký kruhový objazd na ktorom stoja ešte staršie ale aj modernejšie autobusy. Cítil som sa ako v sne a fotil som čo sa dalo. Skoro ako japonský turisti.   Je už takmer 19.30 a ja viem že sa ešte potrebujem dostať 17km západne od Valletty do strediska Qawra. Moja zvedavosť však nepoznala hranice a preto som po chvíľke vstúpil cez bránu do hlavného mesta Valletty. Historické budovy, strieľajúca batéria, kostoly a múzea lemovali všetky možné ulice. Na tých hlavných boli reštaurácie a puby. Stanovil som si čas na 60 minút aby som stihol aj nejaký ten bus. Snažil som sa toho vidieť čo najviac, no napokon som videl asi len 3 ulice a pevnosť Fort Saint Angelo.   Autobus č. 159 a ani č. 59 už nepremávali a jediné čo mi ostávalo bol spoj č. 49. ten mal odchod o 21tej ale už o 20:45, keď som k nemu dorazil ja, bol plný. Našťastie som si sadol. Hneď za vodiča. Cena z Valletty do Qawry bola 54 centov!!! Naviac zážitok sa nedá spoplatniť. Sedel som za vodičom kde po ľavej ruke som mal miesto (dvere tam neboli) na výstup a nástup. Pri rýchlejšej jazde (bola skoro stále) sme mali všetci v buse čo robiť aby sme nevypadli cez tento vchod. Autobusár prejav oval však dôveru a chladnú hlavu aj v tých situáciách pri ktorých sa na semaforoch na Slovensku nadáva. Je už tma a ja strácam prehľad kde sú vlastne zastávky. Po chvíľke sa však z ničoho nič ozval zvonec. Prekvapený som pozeral okolo seba že čo to bolo. Jeden Angličan sediaci za mnou sa začal smiať a ukázal mi ako to tam vlastne funguje. Jednoducho keď chcete vystúpiť, zatiahnete raz za lanko pod stropom busu a to zazvoní na zvonček pri vodičovi. Ten Vám potom následne zastaví na zastávke, alebo aj tam kde si poviete. Čím viacej ľudí vystupovalo a bus sa vyprázdňoval, tým viacej ste počas jazdy lietali ako loptičky po autobuse. Prechádzame cez mestečko mosta a po chvíľke sme už na mieste a zároveň na konečnej busu č. 49.      Už je tma a ja som musel požiadať vodiča autobusu o pomoc v hľadaní cesty do hotela Qawra Inn. Nakoniec mi pomohli práve tí Angličania, čo v buse sedeli za mnou. Cestou do hotela ma ešte presviedčali aby som hotel zmenil. Ani nie 10 minút od zastávky je môj hotel. Ak teda môžem toto miesto špiny a pachu nazvať. Hotel Qawra Inn. Na stránke vypadá krásne , izby veľké, postele pohodlné a dve noci s raňajkami za 28Euro... no po tom čo som ani nedostal kľúče od izby, boli vraj jediné, a recepčná má odviedla na moje miesto „pohodlia" som na tento hotel zmenil názor. Pustil som si klímu, ktorá hrozne smrdela, pri sprchovaní mi z teplej vody išla hrdza, a pod radiátorom bol ešte odkaz prasknutia a vytečenia. Jediný svetlý moment v tej tme bolo asi len to, keď som otvoril dvere na balkón a v tme sa pokochal na jemne nasvietený bazén. Nepomohli ani protesty na recepcií a keď som dostal ďalšie 3 kľúče aby som si teda vybral inú, tak to bolo ešte horšie.      Cestou do hotelu som videl veľa kaviarničiek a pubov čo „fičali" na majstrovstvách sveta vo futbale. Ich plazmy a veľa fanúšikov robilo riadnu atmosféru. V prevažnej miere to boli Angličania čo mali nad talianmi prevahu. No medzi nimi boli aj Austrálčania, Brazílčania či Gréci. Moju chuť na pivo to však nezmenilo a tak som si prisadol a dal si jedno orosené. Po chvíľke druhé a aj tretie. Nakoniec som okolo polnoci dostal chuť na niečo mňamy. Až okolo 01:00 ráno som sa vrátil na izbu a ani neviem ako som zaspal.      Po raňajkách, ktoré som si radšej ani nedal (praženica plávala vo vode) som sa rozhodol že tento deň strávim niekde na výletnej lodi. Hneď oproti hotelu, na rohu budovy, je malý obchodík s predmetmi. Tu mi pani vysvetľovala čo by sa dalo vidieť a čo stojí za to. Pristál som teda na výlete loďou na ostrov Commino a kúpanie v Blue Lagoon. Za 15 euro, pre mňa prišli až k hotelu a na ten istý ma aj vrátili. Loď vyplávala o 11tej a ja až na lodi som spoznal mladý pár zo Slovenska. Veľmi veľa sme spolu predebatili, fotili. Vyrušila nás len pani na lodi, ktorá nám ponúkla za 8 euro „flek" na rýchlom člne, ktorý s nami urobí obhliadku zátok a jaskýň. Tak som si priplatil. Po necelej hodinke, keď sme sa mohli kochať jachtami, rýchlymi člnmi, či jaskyňami, sme konečne dorazili do Blue Lagoon.      Nádherné! Krásne! Tyrkysová voda! Jednoducho ráj na zemi!       Len čo sme zakotvili, tak som sa presunul do rýchleho člnu, kde nás bolo asi 15. Brazílčania, Austrálčan, Nemci, Kolumbijčan ja a nejaký Angličania. Sedel som na špici lodi a hneď som aj vytiahol kamerku. Bolo na čo. Takú krásu som už dlho nevidel. Oki-Koki-Banis, tak sa totiž volala tá loď, nás zobral krásnymi jaskyňami, videli sme miesta kde sa kúpu len vyvolený...a ja som stal točil. Po 30 minútach ma čakal zážitok ešte väčší. Konečne som sa ponoril do vody v Blue Lagoon. Bál som sa, aby moje modré plavky príliš nesvietili v tej nádhernej čistej tyrkysovej vode. Dve hodinky kúpania a zasa späť do Qawry a do hotelu rodiny Adamsovcov.      Smäd je prezlečený hlad a tak som toto pravidlo využil. V čínskej reštaurácií som si za 9euro dal výdatné menu a pivo k tomu. Pozrel som si futbal a išiel si lahnúť. Ďalší deň ma čaká presun do Valletty a neskôr aj na letisko.         Je 18.6.2010 a ráno bolo také ako to predošlé. Ľudia sa pomaly začali hrnúť do bazénu a ja som to radšej s raňajkami neriskoval. Stačil mi len melón. Okolo 10tej som bol už späť vo Vallette a tam som si okrem fantastickej kávičky dal aj raňajky za 3,50 Euro - Brushketta. Jemne opečené chlebíky potreté olivovým olejom a na nich čierne a zelené olivy s kúskami paradajok. Super! Premiestnil som sa do miest, odkiaľ je neuveriteľný výhľad na more, na Fort Saint Angelo. Posledné snímky, posledné foto...      Na autobusovej stanici som spoznal ďalších Slovákov. Tí boli však tak rýchly, že na letisko išli o jeden bus skôr. Ja som ešte nakúpil nejaké predmety a išiel som tiež. Za hodinu sa zatvára moja brána a ja ešte vo Vallette. Na viac adrenalín sa zvyšoval aj tým, že bus stál aj tam, kde skoro vôbec nemusel. Nakoniec som dorazil pred svoju odletovú bránu v pohode 30 minút pred zatvorením.   V lietadle som dostal veľkú chuť na whiskey a tak som si objedal. Však bolo „Buy one, get one free". Až keď mi ho doniesli, tak som sa zoznámil s letuškami Katkou a Zuzkou ktoré boli zo Slovenska, jedna z okolia Trnavy a druhá z Bratislavy. Let potom veľmi rýchlo ubehol a ani neviem ako, ale opäť som stál v Bologni na letisku.   Nádherný výlet. Už teraz sa teším na deň, keď sa na Maltu celkom určite vrátim.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dian 
                                        
                                            Cesta na Mallorku - časť II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dian 
                                        
                                            Cesta na Mallorku - časť I.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dian 
                                        
                                            Bologna za pár euro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dian 
                                        
                                            Návšteva Bátoričky na Čachtickom hrade
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Dian 
                                        
                                            Silvester 2009 a Nový rok 2010 v Prahe
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Dian
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Dian
            
         
        branislavdian.blog.sme.sk (rss)
         
                                     
     
        Verím že som
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1613
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




