
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            veronika babičová
                                        &gt;
                Nezaradené
                     
                 Kto som - úvod 

        
            
                                    3.5.2010
            o
            18:21
                        |
            Karma článku:
                1.51
            |
            Prečítané 
            322-krát
                    
         
     
         
             

                 
                      Poznáte všetci internetové zoznamky. Tam sa ma jeden z dotyčných opýtal, či píšem pravdu, alebo si vymýšľam. Moja odpoveď bola: nemám si prečo vymýšľať, môj život je dosť zaujímavý.. Chvíľu som nad tým potom (až potom) rozmýšľala a prišla som na to, že je to pravda..  
                 

                     Vždy, keď niekoho stretnete, teda sa zoznamujete, odpovedáte stále na tie isté otázky. Kto ste, odkiaľ ste, koľko máte rokov (táto otázka padne so zdvorilostným úsmevom) a čo je vašou prácou.. Neskôr sú to otázky o vašom voľnom čase a možno aj o príbuzných. A potom konverzácia samovoľne pokračuje..Ak ste si navzájom aspoň trošku sympatickí..   Takže kto som? Som žena.. Veronika.. Psychologický profil si nechám na neskôr..   Odkiaľ som? Z malého mestečka, v ktorom sa skoro nikdy nič nedeje..   Koľko mám rokov? 25..Čiže pre niekoho som mladé mäsko a pre niekoho stará dievka..   Moje zamestnanie? Bola som už hocičím..To rozoberiem neskôr..   Tak to boli  základné otázky. Prejdeme na ďalšie:   Voľný čas? Najradšej asi spím. A hneď potom čítam. Chodím von, čí už do prírody alebo do baru.   Príbuzní? Mám mamku, otca a dvoch úžasných bratov, o ktorých sa budem ešte veľa rozpisovať.   Základné zoznamovanie máme teda za sebou.. Ak som vám sympatická, aspoň trošku, pustime sa do podrobností.   O čom že to chcem písať? O mne.. Aké sebecké..Mám na to ale dobrý dôvod. Raz som svoje práce poslala do jednej literárnej súťaže. Mala som štrnásť či pätnásť. Nikto by sa nemal čudovať, že moja prozaická práca bola o láske. Ale kedže som sama o láske nič nevedela, netvrdím, že teraz viem, bolo to samé klišé. Ešte teraz sa za tie práce hanbím. Vrátili mi to späť s tým, že by som mala strašne veľa čítať a písať o tom, čo poznám. Držím sa tej rady dodnes. Čítam strašne veľa a všetko. Až mi to neprospieva, pretože potom čakám od života viac ako mi môže dať. Dokažem sa do knihy úplne vcítiť. Niekedy sa mi stane, že potom zabudnem na to, kto som vlastne ja. Musím konečne začať žiť svoj život..A kedže mám písať o tom, čo poznám, snažím sa toho prežiť, čo najviac. Z toho ma málokto z môjho okolia radosť, hlavne moji rodičia. Ale nieže by som to robila schválne. Ja sa vždy nechám uniesť svojimi prvotnými pocitmi a až potom, keď sa zo sna zobudím,zistím, že som mala rozmýšlať hlavou a nie srdcom..Lebo to niekedy klame.. Nešla som  a ešte stále nejdem priamou cestou.. Toto je prvý krát, čo som sa odvážila písať o sebe. Možno mi to pomôže pochopiť samú seba a možno ale len možno si splním môj najväčší sen.. A postavím sa do cesty životu a prestanem byť zbabelá ako všetci, to mi napísal jeden človek, na ktorého nikdy nezabudnem. Ale poďme pekne po poriadku..   Postupne sa spozname:-D     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        veronika babičová 
                                        
                                            naposledy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        veronika babičová 
                                        
                                            priznanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        veronika babičová 
                                        
                                            rozpravka???
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        veronika babičová 
                                        
                                            potopa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        veronika babičová 
                                        
                                            Zase
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: veronika babičová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                veronika babičová
            
         
        veronikababicova.blog.sme.sk (rss)
         
                                     
     
        som obyčajné priemerné dievča:-D
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    9
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    721
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Úplne nepodstatný blog o ničom
                     
                                                         
                       Modré z neba alebo „splnený“ sen na televízny spôsob
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




