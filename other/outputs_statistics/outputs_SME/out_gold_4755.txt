

 Jany prezradí, či, a aké aféry už stihli túto začínajúcu hudobnú skupinu postretnúť, čo si myslí o Superstar, porozpráva o úspechoch a prvých neúspechoch,  a vysloví definitívnu odpoveď na otázku: „Musí byť hudobník optimistom?" 
   
 Začnem názvom, prečo práve Integria? 
 No, pravdupovediac to nebol môj nápad... Chceli sme nejaký názov, ktorý by nás vystihoval, a ktorý by oslovil ľudí. Slovo „integria", pokiaľ mi to bolo dobre vysvetlené, znamená „jednota", a keďže sa snažíme držať pokope, lepšie meno by nás vystihnúť nemohlo. 
 Zaujímavý zmysel... Čiže, mohol by si predstaviť ostatných členov skupiny, vrátane seba? 
 Ako to myslíš? Akože meno a také drísty? 
   
 Hej,  v podstate... 
 Fajn, ako prvého by som predstavil bassgitaristu (vynikajúceho bassgitaristu) Paľa Bačkaya a hlavného speváka skupiny, bez ktorého by sme neboli tam, kde sme. Ďalší je Fero Dadej, spevák, gitarista, vokalista. Tretím je Peter Bačkay, náš bubeník. Oni (bezo mňa) mali ešte pred Integriou  inú skupinu, no a po istej dobe - úplnou náhodou - som sa cez jednu moju kamarátku stal aj ja jej oficiálnym členom. 
 Odkiaľ vlastne pochádzate, respektíve, kde zvyknete skúšať a ako často. 
 Ja bývam v Šarišskej Trstenej, zvyšok v Podhoranoch neďaleko Prešova. V Podhoranoch máme skúšobňu, teda musím dochádzať. Skúšame vždy, keď sa dá. V priemere dva-tri razy do týždňa, niekedy viac. 
   
 Chce sa ti, vlastne, dochádzať? 
 Určite sú i obdobia, kedy sa nedarí, a kedy sa nevieme nikde pohnúť - vtedy sa mi dosť nechce, ale... ono... takéto obdobia len treba prečkať. Predsa nemôže byť vždy všetko ideálne a jednoduché. 
   
 Keďže ste východoslovenská kapela, myslíš, že to s presadzovaním sa nebudete mať také ľahké ako, povedzme, bratislavské kapely? 
 Nezdá sa mi, žeby bol regionálny rozdiel problémom. Ak sa nám podarí osloviť ľudí z východu, nevidím dôvod, prečo by sme neoslovili aj ľudí zo západu. 
   
 Akurát si sa pridal na Facebooku do skupiny Na hlúpu otázku, hlúpa odpoveď... Snáď nie?! 
 To platí len pri hlúpych otázkach. 
   
 (Skúsim dávať lepšie...) Aký žáner hudby preferujete? Spievate autorsky vlastné piesne alebo aj od iných interpretov? 
 Ja osobne preferujem rock. Kapela sa tiež plaví po vodách rocku či poprocku. Asi ako každá začínajúca skupina, i my sme začínali s prevzatými piesňami známych kapiel a postupne sme sa zdokonaľovali. Neskôr - na šťastie - prišli na rad naše texty a naša hudba. 
 To hej, asi všetky kapely začínajú podobne. Povedal si, že teraz sú na rade vaše texty a vaša hudba - kto ich vymýšľa? 
 Väčšinu sme napísali my, no máme aj dosiaľ nezhudobnené texty od iných ľudí - priateľov, známych. Čo sa týka hudby, tú spravidla komponujeme sami. 
 O koľkých vašich piesňach by si zatiaľ mohol povedať, že sú fakt vydarené? Stačí približne. 
 Ťažko povedať, koľko je vydarených. Vždy je na nich veľa práce, ale ako-tak máme hotových cca 10 piesní. 
 Je ťažké zhudobniť nejaký text? Robíte to spolu, alebo si rozdelíte úlohy a jeden spraví, povedzme, jednu slohu, druhý refrén... Alebo vyberiete toho najskúsenejšieho, ktorý „onotuje" celý text? 
 Eh, normálne je ťažké odpovedať, či je to ťažké alebo nie. Niekto si môže povedať: „Čo už len na tom môže byť zložité?!" Nuž, pokiaľ nepríde dajaký nápad alebo nenakopne múza, zostáva to ťažkým. No zvyčajne piesne tvoríme tak, že niekto príde s takým konkrétnejším nápadom na hudbu, a ten už spolu rozvíjame ďalej. Predsa len, štyri hlavy majú viac nápadov, než jedna. 
   
 Plánujete vydať CD alebo ho už nebodaj máte? 
 Zatiaľ (a bohužiaľ) nemáme žiadne cédečko, ale máme niekoľko ponúk na produkciu, len momentálne chýbajú finančné prostriedky. Zháňame... zháňame... zháňame... a veríme, že čoskoro budeme môcť  do toho ísť naplno. 
 Zdá sa mi to alebo je fakt ťažké (po finančnej stránke) viesť kapelu? 
 Muzika ide riadne do peňazí... Aparáty, gitary, bicie, ozvučenie; všetko stojí nemalé peniaze a bez sponzora či príjmu je to skoro nemožné. 
 Takže každý hudobník musí byť optimista! 
 Áno! 
  
   
 Kto je frontmanom skupiny? Ty zastávaš aký post? 
 Frontmanom je Paľo. Ja vybavujem nahrávanie. 
   
 Z toho vyplýva, že ste už kdesi museli nahrávať! 
 Tak, tak. Zatiaľ sme nahrali iba jednu pesničku v štúdiu, konkrétne Skúšaš vrátiť čas. 
 Je to kvôli nedostatku času alebo financií? 
 Čas, ten by sme si určite našli. Hlavným dôvodom sú financie. 
 K peniazom sa už radšej nevracajme... Plány do budúcnosti? Máte nejaký veľký sen? 
 Plány? Teraz je našou prioritou, čo najviac hrať, zháňať fanúšikov, financie, a začiatkom mája by sme mali ísť do štúdia nahrať singel Chcem ti niečo povedať. Veľký sen? Aby nás ľudia počúvali, podporovali a aby sme robili dobrú hudbu. Potom vydať CD a hrávať po celom Slovensku. 
 Uvažovali ste už o nejakom pracovnom názve CD? 
 To je len vecou dohody, no zatiaľ nemáme potvrdené nahrávanie CD, takže na tieto veci máme čas. 
   
 Máte ako kapela hudobné vzory? 
 Myslím, že každý má svoj vzor. Napríklad mojím vzorom je gitarista kapely Simple Plan. 
   
 Chceš byť ako on alebo si držať takú „prirodzenú odlišnosť"? 
 Nechcem sa naňho úplne podobať, preto som skôr za „prirodzenú odlišnosť". 
   
 Existuje niečo konkrétne, čo ťa inšpiruje? 
 Mňa dokážu inšpirovať mnohé dobre veci. 
 Pekne si sa z toho vymotal. Keby si sa mohol spýtať jednu jedinú otázku niekoho zo Simple Plan, ako by znela? 
 Či by som si s nimi nemohol zahrať. 
 A ja som si myslel, že mi zľahčíš výber otázky... Môžem si s vami zahrať? Ale nie. Prejdime na inú tému. Existuje niečo, čo vás, podľa teba, odlišuje od ostatných kapiel? 
 Mne sa to ťažko hodnotí. No od Jožka Harničára, u ktorého sme boli v štúdiu nahrávať, sme sa dopočuli niekoľko dobrých vecí. Spomeniem, napríklad, naše hlasy a spevy; podľa neho, traja z nás vedia spievať, vraj máme vokály, za ktoré by sa nemusela hanbiť žiadna kapela, máme veľmi dobré nápady a... stačí, nemusím to preháňať s chválami. 
   
 Žiadne také, len sa pekne pochváľ, koľko je za vami vystúpení. 
 Presne neviem... Máme za sebou niekoľko dobrých koncertov v Košiciach, ale aj jeden zlý v Prešove. A momentálne myslíme len na koncert ktorý bude 24. apríla o 19.00 v City Clube v Prešove, kde to chceme naplno rozbaliť! 
   
 Čo znamená zlý koncert? Na akých miestach hrávate? 
 Zlý = nevydarený. Jednak sme mali veľké problémy s ozvučením a jednak to bol koncert na jednej škole po programe, po ktorom odišla celá škola preč, takže nás to veľmi mrzelo a trápilo. Hrávame, kde sa dá. 
   
 Prajem vám veľa šťastia, aby už nikto nikdy neodišiel! Aké reakcie sa zatiaľ dostali vašim piesňam? Ktorá je momentálne tá naj? 
 Ďakujeme. Reakcie sú veľmi pozitívne, čo nás teší a núti ísť ďalej. Momentálne tá naj? Ťažko povedať, záleží na fanúšikoch. Uvidíme, keď nahráme Chcem ti niečo povedať, lebo tá je veľmi žiadaná. 
 Vravel si, že hlavne ty vybavuješ nahrávania. Si teda producentom a manažérom Integrie? 
 Manažérom je Fero,  ja vybavujem nahrávanie preto, lebo s Jožkom Harničárom veľmi často komunikujem a poznám ho z nás asi najviac. 
  
 Spravme mu reklamu. O aké nahrávacie štúdio ide? Spolupracuje sa s ním dobre? Aký bol váš prvý deň v nahrávacom štúdiu? Ako dlho ste nahrávali pieseň Skúšaš vrátiť čas? 
 Hárdy studio. Spolupráca s ním je výborná - vie poradiť a takisto pomôcť. Prvý deň v štúdiu? Zo začiatku sme sa možno trocha obávali, že čo z toho bude... možno trošku tréma. Ale neskôr to bolo úplne v pohode, nahrávali sme ju celý deň od rána. Je to len taká demo nahrávka. 
   
 No vôbec neznie ako demo! 
 Nuž, niektorí to, zdá sa, nepočujú, ale určite má pár chýb, ktoré by sa dali odstrániť, čiže keby sa nahrávalo CD, nahrala by sa úplne odznova a poriadne. 
   
 Čo máš rád na hudbe, čo ťa na nej fascinuje? Predpokladám, že hej. 
 Úplne najradšej mám tie emócie a pocity, ktoré dokáže vyjadriť. Vyjadruje smútok, bolesť, ale aj šťastie. Takmer úplne všetko... je to až neuveriteľné. 
 Na koľkých nástrojoch vieš hrať? Teda okrem gitary. 
 Hrať by som možno vedel na viacerých, no otázka znie, ako. Ale nie. Gitara, akordeón, klavír, bassgitara a bicie trocha. 
 Vidím to na zmenu frontmana... 
 Nerob srandy. 
 Stala sa nejaká kuriozita alebo niečo fakt šialané v spojitosti s Integriou? 
 Akurát, že pred dvoma rokmi sme sa rozpadli na pol roka... 
 Existuje podľa teba nejaký dokonalý typ fanúšika hudobnej skupiny? Ako by mal vyzerať takýto dokonalý fanúšik Integrie?  
 Dokonalý fanúšik by mal stáť za kapelou vždy; aj keď sa jej darí, aj keď nie. 
  
 Boli ste na nejakej súťaži? Myslíš, že súťaže ako napríklad Superstar, Eurovízia, Slovensko má talent, sú dobrou cestou ku zviditeľneniu sa alebo by sa mal každý o svoju slávu postarať sám? Pravdupovediac, veľmi tie súťaže nemusím. Superstar som pozeral, ale Eurovíziu a SMT málokedy. I keď niekedy tam nejde o talent, iba o zviditeľnenie. 
 Takže je lepšie budovať si kariéru sám? 
 Ako, súťaž ti môže pomôcť. No nepáči sa mi, že veľa ľudí hlasuje podľa toho, ako sa im dotyčný páči, a nepozerajú na spev či charakter. 
   
 Čo ty, fandil si niekomu? 
 Popravde, ani nie. Páčil sa mi spev niektorých spevákov, ale nebudem ich tu menovať. 
   
 Postretli už Integriu nejaké aféry? 
 Myslím, že zatiaľ nie. Našťastie. 
   
 Okej, chceš mi ešte niečo povedať? Ja si k tomu domyslím otázku.  
 Len chcem pozdraviť všetkých ľudí, držte nám palce! 
 Ktorá vaša pieseň sa ti najviac páči a prečo? 
 Najviac asi Bozk vo dverách, pretože je to pesnička plná energie a ako hudobník sa na nej dokážem poriadne odviazať. 
  
   
 Čo je, podľa teba, podstatnejšie v piesni. Hudba? Text? 
 Ťažká otázka. Podľa môjho názoru je to u každej piesni iné - niekde je podstatnejšia viac hudba, niekde text. A hlavne netreba zabudnúť, že je to u každého človeka iné... 
   
 U teba? 
 Dobrý text spojený s dobrou hudbou, to je ideál! 
   
 Takže, na záver sa ti chcem poďakovať za tento rozhovor, a ak chceš, môžeš odkázať začínajúcim mladým spevákom, hudobníkom a kapelám, čo len chceš. 
 Treba hlavne tvoriť, hrať, prežiť - aj sklamania -, a ono to príde. Samozrejme, neustále sa zlepšovať... 
   
   
   
 Janymu ďakujem za krásne interview a dávam do pozornosti webovú lokalitu, kde je možné stretnúť sa s hudbou Integrie z očí do uší. 
 www.bandzone.cz/integria 
   
 Staňte sa fanúšikmi Integrie na Facebooku. Stačí kliknúť na odkaz a pridať sa k fanúšikom! 
 http://www.facebook.com/profile.php?id=1296243850&amp;ref=ts#!/pages/Integria/354976007897?ref=ts 
  

