

 Prišiel mi e-mail, že sa mám v bližšie určený deň,      
 dostaviť na školenie a nemám si zabudnúť 
 so sebou priniesť dobrú náladu.  
 Tak s tou náladou by to bolo O.K.   
   
 Mám rada tento druh školení. Všeličo sa človek naučí, oboznámi s novinkami, niečo skonzumuje a vypadne na pár dní z denného kolotoča. 
 A tak som sa vybrala. Keďže školenie bolo v neďalekom meste, žiadne hotely a iné ,,hladomorne“ nehrozili, padlo rozhodnutie, že pôjdem vlakom.  
   
 Je to len hodinka cesty a zostane mi aj časová rezerva.  
 Vonku bolo chladno a vo vlaku teplučko. 
 Zavesila som vrchný odev, vyložila z kabelky časopisy, balíček keksov, minerálku, telefón, skontrolovala ,,fasádu“ a slastne zaborila oči do časopisu. 
   
 V tom prichádza predajca cestovných lístkov. 
 ,, Na predmestie poprosím“ a žmurkám jedným okom. Sprievodca na mňa s údivom pozerá a vraví, že vlak nejde až do krajského mesta 
 a o päť minút musím vystupovať – konečná, lebo trasa ďalej je zrušená. 
   
 V rýchlosti som sa zbalila, na najbližšej zastávke vystúpila a s telefónom na uchu letela na autobusovú stanicu. 
   
 Mobilný operátor ma uisťoval, že o päť minút mi odchádza  mnou požadovaným smerom, diaľková linka do daného cieľového mesta. 
   
 Cesta ubiehala rýchlo, v pohodlí a ja som rozmýšľala, ako sa dostanem na predmestie,  zo stanice  je to predsa riadny kus cesty. 
   
 Na konečnej práve odchádzala autobusová linka do mesta. 
   
 Prestúpila som si a s vodičom dojednala miesto vystúpenia.  
 Odtiaľ ma už čakala dlhšia trasa pešo na predmestie. 
   
   
 Moje kroky viedli vedľa frekventovanej cesty. Vodiči, nie vodičky spomaľovali, s náznakom, že by ma aj zviezli. Tak som pozerala radšej rovno pred seba. V diaľke sa črtal podchod. Opustený... Zamrazilo ma a dúfajúc, že vyjdem odtiaľ živá som vyšla po schodoch hore. 
   
 Predo mnou, samozrejme policajti. Až potom mi došlo, že vodičský preukaz pýtať určite nebudú a vydýchla som si. Lenže, čo čert nechcel, moja cesta skončila. Prečo? Lebo predo mnou bola železničná trať a za ňou nič, len breh smerom dolu, žiadny chodník a koniec trasy. 
 Ako sa dostanem na ulicu za koľajami?  Policajti mi veru nevedeli poradiť, nakoľko neboli domáci. Jasné -  tajná akcia, vírilo mi v hlave. 
   
   
 Prešla som cez koľajnice a pozrela dolu. To už na mňa volal jeden ochotný bezdomovec, dolu pod kopcom:,, Poďte sem,  tadiaľto, zíďte dolu kopcom a prejdite cez túto dieru v plote. 
   
 Čas ma tlačil a inú možnosť som nemala. Poslúchla som, poďakovala a opatrne prešla v mojich čižmách s kabelkou, dolu kopcom, pomedzi ploty a garáže. 
 S malou dušičkou som sa rýchlo presunula na druhu stranu ulice a zmizla v školiacom stredisku. 
   
 Všetci čakali už len na mňa. Nasledovalo moje ospravedlnenie 
 a dali mi pokutu – povedať vtip. 
   
   
 Tak som rozpovedala celu moju cestovnú peripetiu. 
 Nastala dobrá atmosféra a v nej sme pokračovali celý deň.   
   
 V duchu som sa smiala, že toto by nevymyslel ani najlepší komik. 
 Veď niet nad také situácie, do ktorých  nás vmanipuluje sám život. 
  
   
 Zamyslenie: 
 Som nežný aj krutý, som život. Plačeš?  
 Aj v slze je sila. Tak choď a ži!                     
   

