
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Kopaničáková
                                        &gt;
                Negatívne
                     
                 Patria študenti do pekla zvaného Átriové domky? 

        
            
                                    29.8.2007
            o
            18:56
                        |
            Karma článku:
                10.79
            |
            Prečítané 
            5513-krát
                    
         
     
         
             

                 
                    Áno, študentské časy. Najkrajšie dni života. Napadne nám masa mladých ľudí, kopa zábavy a predovšetkým veľký, neorganizovaný, častokrát nezvládnuteľný bordel spôsobený čiastočne jednotlivými krízami mnohých osobností žijúcich v ňom, alebo skrátka už len bordel z princípu, pretože veľa mladých znamená veľa rôznych nápadov pokope a zrejme to tak má byť. Flámovanie, bifľovanie, stresujúce dni zaplnené prednáškami či brigádami, ale všetko za to stojí – nie však v Mlynskej doline v Bratislave.
                 

                Upresňujem – na Átriových domkoch v Slávičom údolí.   Už niekoľkokrát „Paľbou spaľovaný“ internát za celé tie roky veľa zmien na sebe neučinil. Či skôr kompetentní neučinili na ňom. Viem, že hovoriť o špine na chodbách, hmyze v izbách, neznesiteľnom ovzduší uprostred leta na nesmierne „dômyselne“ riešených poschodiach spájaných čudnými uličkami a katakombami znamená tisíci krát sa opakovať. Viem, že nie som prvou ani poslednou študentkou, sťažujúcou sa na podmienky života v tomto, mnou nazvanom PEKLE (lebo ak som si dovtedy nevedela predstaviť peklo, teraz sú jeho zhmotnením jednoznačne a jedine iba Átriové domky – všetko v jednom ALL INCLUSIVE – špina, teplo a smrad – a ak to zveličíme, úchyláci a bezdomovci môžu zosobňovať čertov). Viem, že nie som ani prvou a poslednou, ako mi to istý pán na ubytovacom oddelení sucho skonštatoval, ktorá má tú smolu a pri odchode zo slávnych internátov musí platiť za chýbajúci inventár, ktorý milé pani upratovačky akosi zabudli pri odhlasovaní jednotlivých ubytovaných odpísať – moja smola, že cez leto mám iné starosti ako kontrolovať po neznámom dievčati izbu, či náhodou nechýba paplón. Moja smola, že som zabudla porovnať inventárny list starý dva roky so skutočným stavom izby. Moja smola, že cez leto býva na internátoch skoro päťtisíc študentov, ktorí sa preubytovávajú tak rýchlo, že sa izby nestíhajú kontrolovať, vetrať a nedajbože ešte aj čistiť. Moja smola, že osemdesiat percent študentov kašle na hygienu – sú deformovaní pobytom v tomto motivujúcom prostredí plnom ukladajúceho sa prachu. Moja smola, že štát nemá peniaze a pani upratovačky predsa nebudú platiť za škody spôsobené nejakými úbohými švábmi alias študentmi. Nevadí, že ani ja neviem nič o chýbajúcom inventári – veď teta upratovačka nebude mať za to postih, zodpovedná som ja, platím ja, nech sa deje čokoľvek. Mimochodom, ak by sa do môjho úplného odchodu z izby nasťahoval niekto ďalší, logicky nie som poslednou odchádzajúcou a zodpovednosť sa znova prenáša naňho – skúsme sa zamyslieť, ak by takto jedna izba fungovala povedzme rok, kto by už po roku vedel, aký inventár bol na začiatku a aký má byť teraz? Zodpovednosť prenášaná z jednej hlavy na druhú, prehadzovanie kompetencie, no v konečnom dôsledku vždy a všade platí len študent. Nehodný, úbohý šváb. (Formujúca sa inteligencia štátu.)     Logické vyjadrenia tu nefungujú, platia len pravidlá. Sú postavené na hlavu ako viacero zákonov a smerníc v našom malom Slovensku. Nechcem hanobiť všetko kvôli pár mojim problémom a dvom letám odžitým ako externý študent na brigáde v Bratislave, vďaka čomu som takmer prišla o nervy. Áno, buďme optimisti, dejú sa tu aj horšie veci. (Ak toto vôbec možno nazvať optimizmom.) Lenže internáty v Mlynskej doline nemajú absolútne nič spoločné s ubytovaním, nebodaj by sme ešte mali tú odvahu rozoberať v kontexte s EÚ – detto. Ale to by hádam nijaký rozumný človek, najmä z tých, čo tu kedysi prežili čo i len týždeň, alebo aj v tomto momente prežívajú svoje študentské dni, nemohol porovnávať. Je šialené, že budovy ako sú Átriové domky, ešte vôbec stoja. S výnimkou pár zrekonštruovaných blokov sa môžem na pomenovaní Ubytovacie zariadenia iba veľmi ironicky smiať. Architekta, ktorý tento zložitý bludiskový komplex navrhol, by som s radosťou vyplieskala po holom zadku za to, že si z nás študentov svojou skvelou stavbou urobil „prdel“. Ľudia (nechcem konkrétne menovať, aby som sa náhodou nesekla), ktorí tento komplex aj v takom stave, ako je, naďalej prevádzkujú, si v ňom veru zaslúžia bývať sami do konca života. Samozrejme, poviete mi, kde by toľkí študenti bývali, keby to zatvorili. Každý nemá predsa na prenájom bytu. Bratislava, a najmä Mlynská dolina, praská vo švíkoch. Pod vplyvom toho sa študenti, žijúci v extrémnych podmienkach (asi 40 – 50 osôb na dve sprchy plus už predtým spomínané okolnosti života), uskromňujú s vyjadrením: „Ale veď tu je strašná sranda, skvelý život, nemenil/a by som, už som si zvykol/la... A je to strašne LACNÉ.“     ???     Nikomu jeho názor neberiem. Priority sú priority. Pomlčím o bufetoch a potravinách v okolí internátov. Tu sa už totiž o kvalite za rozumnú cenu vonkoncom nedá hovoriť. Jedným slovom – ryža.     Viete, čo ma počas tohto leta na Átriových domkoch pobavilo najviac? Keď som sa jedno ráno zobudila na to, ako mi do izby bez rozpakov vošiel pán v uniforme s rúškom na tvári a vystriekal mi tam pár deci odporne páchnucej dezinsekčnej tekutiny. Aspoň bol milý a pri odchode pekne pozdravil, no ja si dodnes prehýbajúc sa od smiechu pri tej spomienke kladiem otázku, či je hmyz naozaj najvážnejším problémom Átriových domkov.     Á, zabudla som na SBS službu. Už naozaj nechcem kritizovať... ale je normálne, že sa niekto ide s prepáčením... potrhať pri kontrole na vrátnici, aby sa mu náhodou nejaký nehodný nezaregistrovaný švábik neprepašoval do priestorov, kde nemá čo hľadať? Asi áno. Veď... kto by netúžil zažiť ten neuveriteľný komfort Átriových domkov? (Natočím na ne reklamu – budúcnosť mám už teda zabezpečenú, hurá.) Pritom aj napriek tejto neuveriteľnej snahe strážnej služby plniť si svoje povinnosti zo zadnej strany prenikajú nechránenými miestami na internát už spomínaní čerti v podobe stratených existencií spoločnosti...     Preto pozor, ak na takéhoto čerta natrafíte, mali by ste mať pri sebe amulet, aby ste sa pred ním dokázali chrániť. Inak to na Átriových domkoch nemusíte ani prežiť.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kopaničáková 
                                        
                                            Odhaľovanie sochy Svätopluka alias sloboda médií v priamom prenose
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kopaničáková 
                                        
                                            Nie je mi to jedno, ale...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kopaničáková 
                                        
                                            Nie, až taký chorý určite nie si
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kopaničáková 
                                        
                                            Malý príklad na moc médií
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Kopaničáková 
                                        
                                            Ktorou nohou leziem z postele
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Kopaničáková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Kopaničáková
            
         
        kopanicakova.blog.sme.sk (rss)
         
                                     
     
        Expert na znevažovanie kníh jedlom. Nerada sa k tomu priznávam, ale milujem čítať knihu, keď jem. A keďže čítam aj jem veľmi rada, potom tak moje knihy aj vyzerajú...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1728
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            O nesmrteľnosti chrústa
                        
                     
                                     
                        
                            Negatívne
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vlastina Svátková
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




