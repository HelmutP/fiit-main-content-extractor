

 Bol piatok a ja som musela čakať dlhšie ako zvyčajne, no úprimne povedané, bolo mi to lautr fuk. Nemala som náladu komukoľvek čokoľvek vysvetľovať a nad čímkoľvek premýšľať. Sotva otvorila dvere a podala mi ruku na privítanie, bezdôvodne som sa rozplakala. Ako som si vyzliekala kabát, snažila som sa usporiadať si myšlienky, no nešlo to. Nedalo sa. 
 Okrem môjho nekonečného a neopísateľného zúfalstva si na máločo z tej gaučovej session spomínam. Viem, že som rozprávala o svojich nočných morách a vyriešenom probléme "od minula". Pamätám si, že v tom momente ma prekvapila môjmu stavu nezodpovedajúca výrečnosť, niekoľko súvislých viet... Vybavujem si chuť jahodového čaju a to, ako som celý čas hľadela na dno šálky, spracúvajúc otázky, ktoré mi boli kladené. Ani neviem, či som odpovedala. Z neznesiteľnej katatónie ma prebral až pohľad na ňu a hŕbu zapísaných papierov. 
 „Regino, třeba byste mohla zvážit jiný druh pomoci a navštívit lékaře. Psychiatra." 
 Nič. 
 „Bránili byste se takovému druhu pomoci?" 
 Ticho. Potom som prikývla na znak súhlasu, hneď nato razantne pokrútila hlavou a nakoniec som ticho hlesla - Neviem. 
 Nafasovala som telefónne číslo a napriek tomu, že v normálnom stave by som sa urputne bránila predstave, že som „psychiatrický prípad", v tej chvíli mi na ničom nezáležalo. 
 „Víte, na tom není nic špatného. Není třeba se ničeho obávat, třeba se budete cítit líp. Slíbíte mi, že se mu ozvete, pokud se budete cítit hůř?" 
 Nemo som prikývla, smrkajúc do slzami premáčanej vreckovky. V mojom stave je na mňa už aj psychológ krátky, preblesklo mi hlavou, no na sebaľútostné dumky v tej chvíli nebol čas. Hodina vypršala a tak som sa opäť zababušila do kabáta a po tichom pozdrave vyšla do snehovej víchrice. 
 Na nasledujúce štyri dni to pre mňa bol jediný a posledný kontakt s vonkajším svetom. 
   
   

