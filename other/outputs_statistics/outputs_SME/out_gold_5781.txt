

   
 Pomaly sa ztmieva. Doslova. O deviatej nevidno na päť metrov. Žiadna svetlá mesta, žiadny ruch áut. Najbližšie mesto je tri dni plavby. Plavím sa už štvrtý deň. Od začiatku ma trápia žalúdočné problémy, zachraňuje ma iba Reasec. Mám spálený krk a ramená, na tele mám viac od komárov uštipnutých miest ako tých neuštipnutých. Taký repelent, čo ochráni v Amazónii ešte nevymysleli. Napriek tomu sa cítim neuveriteľne. Toto miesto je nabité mystikou. 
   
  
  
   
 Ležím v hojdacej sieti asi meter nad zemou, naokolo je dokonalá tma a ticho. Z lesa sa ozýva spev vtáka, ktorému sa hovorí matka noci. Pripomína plač dieťaťa a opakuje sa presne po polminútovom intervale. Obďaleč zašpliecha voda. Aligátor práve ulovil svoju večeru. 
   
 Bude pršať. Už som tu jednu nočnú búrku zažil. Nikdy v živote som sa viac nebál. Nebesá sa v tej chvíli otvorili, blesky pretínali oblohu sprevádzané hlasitým hromobitím, dážď čeril hladinu rieky, loď narážala do stromu, o ktorý bola priviazaná. Lano sa nebezpečne napínalo, strom v okolí praskali pod prívalmi dažďa. Lano sa utrhlo. Loď ťahá do stredu rieky, nekontrolovateľne mieri k plytčine. Sedem ľudí robí, čo môže aby zabránili nárazu o dno. Nebo pretne posledný blesk a ukáže sa mesiac. Ako keby sa nič nestalo. Noc je zrazu čistá, svoju moc už dokázala. Takéto náhle príchody dažďa sú tu naporiadku aj trikrát za deň. 
   
  
   
  
  
   
 Napriek svojím nástraham ma Amazónia stále viac priťahuje. Úplne ma pohltila. Dokážem stáť na prove lode a pozorovať okolie dve-tri hodiny vkuse. 
 Už druhý deň ma trápi silný úpal. V noci mi horúčka vystúpila na 39 stupňov, ráno sa to opäť upokojili. Ale niesom jediný. Cez noc sa nás cez palubu nakláňalo a zvracalo osem. Je deväť hodín večer, teplota vzduchu tesne pod 30. Všetci ležíme na podlahe lode v dlhých nohaviciach, inak sa komárom ubrániť nedá. Niektorí ľudia čítajú pri chabom svetle petrolejky, iní hrajú domino alebo iba tak ležia úplne vyčerpaní celodennou horúčavou. 
   
  
  
   
 Nastupujem do malého motorového kanoe, ktoré si pomaly kliesni cestu v úzkom toku jednej z mnohých vedľajších riek Amazónie. Okolo je dokonalé ticho noci, len sem-tam zašuchoce v lese. Z vody nás pozorujú lesklé oči aligátora. Nad nami z konára na konár preskočí opica. 
 Predstavujem si, čo by sa stalo keby sa pokazila loď. Nie je tu telefónny signál, najbližšia dediny osem dní pešo cez podmočený terén pralesa. 
   
  
   
 Trojhodinová jazda na kanoe, tažko priechodným pralesom nás privádza do indiánskej osady. Na vlastné oči vidím ľudí nepoznačených technikou a vedou našej doby, ktorí žijú iba z toho čo vypestujú a ulovia. Deti sa boja našich kamier, neskôr ich nesmelo berú do rúk a o pár minút už veselo robia nezmyselné zábery. Z tejto indiánskej osady ide radosť, energia vyplývajúca zo života v nepoškvrenenej prírode. 
 Skáčem do vody k sladkovodným delfínom ružovej farby, ktoré absolútne nie sú plaché a našu pozornosť si doslova vyžadujú. Napríklad kusancom do ruky. 
   
  
  
  
  
  
   
 Pozerám na západ slnka, som tu posledný večer a z toho neuveriteľného ticha, ktoré panuje navôkol mám zimomriavky. Les sa chystá na noc, utícha. To, čo vo mne zanechala Amazónia nikdy neutíchne. Prežil som týždeň v ničím nerušenej prírode, bez internetu, telefónneho signálu, teplej vody, postele, vankúša a zamiloval som si to. Ešte stále som z toho záhadného miesta v šoku a nedokážem rozpovedať a opísať všetko, čo by som chcel. 
  

