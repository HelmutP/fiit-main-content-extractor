
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Koiš
                                        &gt;
                Spoločnosť a politika
                     
                 Verím, že Čechom nemusíme dlho závidieť 

        
            
                                    30.5.2010
            o
            22:00
                        |
            Karma článku:
                12.33
            |
            Prečítané 
            2898-krát
                    
         
     
         
             

                 
                    Ako dopadnú voľby u nás? Ak by to bolo podľa "českého vzoru" (viď graf), mohli by sme oslavovať. Pravica porazí ľavicu, Robert Fico odstúpi, a budeme mať reálnu šancu zostaviť vládu rozpočtovej zodpovednosti a opäť sa staneme reformným ostrovčekom strednej Európy, akým bolo Slovensko (z pohľadu zahraničia) v rokoch 2002-2006. K niečomu podobnému sa po sobotňajších voľbách priblížili práve Česi. Napriek tomu si myslím, že im nemusíme dlho závidieť, skôr si môžeme navzájom blahoželať. Tým všetkým sme si už prešli vo voľbách 2002, a myslím, že aj oni si zaslúžia jednu zodpovednú vládu uprostred viacerých nezodpovedných. Vo vnútri článku nájdete aj graf českých volebných výsledkov zasadený do slovenského kontextu...
                 

                 
Pôvodný graf - Česká televize, upravil autor 
   ČSSD neuspela, napriek ostrej kampani  Čo ma však prekvapilo, bola neuveriteľne konfrontačná kampaň, ktorá českým socialistom nepomohla. Kamkoľvek ste sa pohli, narazili ste na tvár Jiřího Paroubka. Tu sľuboval 13. dôchodky zo zisku energetického gigantu ČEZ-u, tam sľuboval progresívne zdanenie, zrušenie poplatkov v zdravotníctve, viac dní nemocenskej, inde zase vyššie príspevky pre mamičky na materskej, lepší život pre obyčajných ľudí (rozumej tých s najnižšími prijímami) atď. Jednoducho, sľuboval všetkým všetko a vrcholom cynizmu bol predvolebný televízny spot, kde konštatoval, že sociálna demokracia nezadlžuje krajinu, ale naopak vytvára podmienky pre prílev ďalších miliard do štátneho rozpočtu. Nasledovalo konštatovanie manželky Petry, že Jirka Paroubek je jediný politik, ktorý skutočne pracuje pre ľudí.  ČSSD si okrem vlastnej kampane zaplatila stovky billboardov varujúcich pred "modrou chorobou", pred tunelármi, klientelizmom a korupciou zosobnenou v nových stranách Veci verejné a TOP 09, a výrazne investovala aj do očierňovania ODS. Subjektívne sa mi však zdalo, že podstatne viac urážok sa ušlo práve dvom novým stranám, pri ktorých socialisti cítili najväčšie nebezpečenstvo.  Celá táto predvolebná hra trvala viac než rok, a mala jediný cieľ - vyniesť Jiřího Paroubka na vrchol českej povolebnej reality. Dokonca sa začalo nahlas hovoriť o tom, že práve Paroubek by mohol po Václavovi Klausovi ovládnuť o niekoľko rokov aj pražský hrad.  Paroubek pochopil a odišiel  Prešlo približne 30 hodín od skončenia českých volieb, a všetko je inak. Paroubek už nebude ani premiérom, ani prezidentom, a o štyri roky zrejme ani poslancom. Pomerne rýchlo pochopil, že jeho populistický arzenál na väčšinu voličov nezabral.  Priznám sa, že Čechom teraz v tichosti závidím. Ešte pred niekoľkými dňami nič nenasvedčovalo takémuto výsledku. Ale na druhej strane si spomínam, ako veľká časť Čechov pred ôsmimi rokmi závidela nám našu novú pravicovú formáciu, ktorá vzišla z volieb 2002 ako blesk z čistého neba, napriek tomu, že všetci už vtedy očakávali nástup Ficovej "tretej cesty". Aj vtedy sa mnohí pravicovo zmýšľajúci Česi začali pomaly zmierovať s myšlienkou, že podobnú reformnú vládu aká bola na Slovensku v rokoch 2002-2006 nikdy mať nebudú. A teraz sa im to podarilo.  Ako to dopadne 12. júna na Slovensku?  Stále však verím, že Čechom nebudeme musieť tento ich volebný výsledok dlho závidieť. Stále je šanca, že aj u nás môže dôjsť k podobnému prekvapeniu. Stačí si len zameniť značky - do priloženého grafu s výsledkami českých volieb som vsunul značky slovenských strán, ktoré mi ich najviac pripomínajú - namiesto ČSSD dáme SMER, výsledok ODS nahradíme logom SDKÚ, v úlohe slovenskej TOP 09 sa predstaví SAS, a výsledok Vecí verejných okopíruje KDH. A potom len budeme dúfať, že SNS nedosiahne toľko čo českí komunisti, a že HZDS sa bude môcť porovnávať len s výsledkami KDÚ-ČSL alebo Strany zelených.   Maďarské strany som zo zoznamu zámerne vynechal, pretože v českých podmienkach nemajú vhodné protipóly, to ale neznamená, že im neprajem vstup do parlamentu (jednu z tých dvoch maďarských strán možno budem aj voliť).  Počty sú to určite zaujímavé, dokonca aj priložený graf by bol ako z ríše snov. Môže to aj u nás dopadnúť tak ako na obrázku, alebo sa 13. júna ráno zobudíme, a zistíme, že všetko je inak. Naivita je krásna vec, a v duchu českých volieb si môžeme povedať, že síce nevieme, ako to u nás skončí, ale dobre sa to (u nich) začalo... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (82)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Malé desatoro SaS: Koncesionárske poplatky (už) nezrušíme!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Referendum podľa Fica: občianska povinnosť len keď sa mi to hodí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Pán Kmotrík postaví štadión, prispejeme mu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Do tankoch a na živnostníkov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Omeškal som sa so splátkou úveru. O mínus jeden deň (aktualizácia)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Koiš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Koiš
            
         
        kois.blog.sme.sk (rss)
         
                                     
     
        Rád píšem o všetkom, čo ma zaujme. A rád dávam najavo aj pozitívne skúsenosti, ktoré nevnímam ako samozrejmé.


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    91
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Letecká doprava
                        
                     
                                     
                        
                            Cestovanie vlakom
                        
                     
                                     
                        
                            Spoločnosť a politika
                        
                     
                                     
                        
                            Médiá @ komunikácia
                        
                     
                                     
                        
                            Hokej a futbal
                        
                     
                                     
                        
                            Život v Bratislave
                        
                     
                                     
                        
                            Život v Prahe
                        
                     
                                     
                        
                            Ostatné články
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Gabin - La Maison
                                     
                                                                             
                                            Tom Waits - What's he building in there?
                                     
                                                                             
                                            Kings of Convenience - I'd rather dance with you
                                     
                                                                             
                                            One Night Only - Just for tonight
                                     
                                                                             
                                            Counting Crows - A Murder of One
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Ščobák
                                     
                                                                             
                                            Jozef Havrilla
                                     
                                                                             
                                            Jan Potůček
                                     
                                                                             
                                            Sergej Danilov
                                     
                                                                             
                                            Lukáš Polák
                                     
                                                                             
                                            Zdeno Jašek
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Richard Sulík
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Digitálně.tv
                                     
                                                                             
                                            Digizone.cz
                                     
                                                                             
                                            RadioTV.sk
                                     
                                                                             
                                            RadioTV.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chrániť ľudí pred vrahom (a naopak)
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




