

 Počul som, že učiteľka v materskej škole vysvetľovala deťom, že v lete je teplejšie preto, že vtedy je Zem bližšie k Slnku, ako v zime. Zažil som, že učiteľka fyziky presviedčala chudáka žiaka o neplatnosti Archimedovho zákona keď tvrdila, že vztlaková sila, ktorá pôsobí na teleso, celkom ponorené v kvapaline, je závislá od hĺbky, v akej je toto teleso ponorené ... Podobné príklady žiaľ ako rodič zažívam častejšie, ako by si dokázal predstaviť aj človek s veľmi rezervovaným názorom k súčasnému školstvu. 
 
Ale nasledovná pasáž z učebnice Zemepisu pre 1.ročník gymnázia, SPN Bratislava, 1.vydanie, rok 1983 ma dostala do kolien. 
 
 
 
Takže kto sa chce pozrieť do budúcnosti, hor sa na 180. poludník. Pri jeho prekročení bude stále ten istý dátum. Ak sa však vrátite späť, bude o deň viac. A tak zasa rýchlo späť a nazad. A ste dva dni dopredu. Dobre, každé tam-späť môže trvať povedzme niekoľko hodín, ale stále sa budete do budúcnosti približovať rýchlejšie, ako bežný smrteľník. 
 
A knihu "Okolo sveta za 80 dní" neberte vážne. Najmä záver, v ktorom sa cestovatelia (cestovali smerom na východ) vrátili naspäť do Londýna a zistili, že je tam o 1 deň menej, ako si mysleli. To len autor knihy nechodil do pokrokovej slovenskej školy a preto píše také bludy. 
 
Zaujímavosťou citovanej knihy je to, že na obrázku nad textom je to správne. Ale text vylučuje akúkoľvek pochybnosť, že to autor myslel skutočne vážne. Že nejde o nejaký preklep, či nešťastnú formuláciu. Samozrejme najhoršie na tom všetkom je, že sa nájdu "učitelia", ktorí za správny považujú práve ten text ....  
 
 
 
 
 
A na záver mená "pedagógov", ktorí by si zaslúžili buď Nobelovu cenu, alebo 25 na holú ... Dobre si prezrite tie tituly pri ich menách. To som zvedavý, za čo to dávajú. Za odborné znalosti a ich uplatnenie v praxi to isto nebude ... 
 
 
 

