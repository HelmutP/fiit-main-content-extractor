

  
   
   
  
 Pred piatimi rokmi bol rok fyziky, presnejšie rok Einsteina, teda ešte prsnejšie sté výročie zverejnenie Einsteinovej špeciálnej teórie relativity, no a v tomto jubilejnom roku mali vedci za úlohu spopularizovať Einsteinove teórie relativity, do takej miery aby ich pochopil aj nadaný pes. Po piatych rokoch od tejto celosvetovej propagačnej činnosti fyzikov konštatujem, že Einsteinove teórie nielen že nie sú pochopené obzvlášť nadanými psami, ale ani učiteľmi fyziky na vysokých školách a univerzitách. Ak dôkaz môjho tvrdenia zverejňujem na mojom blogu  jednu moju diskusiu o Einsteinových teóriách s profesorom fyziky a známym popularizátorom fyziky zo Západoslovenskej univerzity, k obsahu jeho nasedujúceho článku: 
 http://blog.sme.sk/diskusie/1692980/1/13535397/Vseobecna-teoria-relativity--devatdesiatnicka-s-tuhym-korenom.html#13535397 
 Predmetná diskusia prebieha nasledovne: 
   
 Aký je pohyb relatívny a aký absolútny. 
 Ak niekto píše o Einsteinových teóriách reaktivity, tak vždy, až na 101% cituje iba obsah učebníc fyziky, bez toho aby vedel aký je rozdiel medzi relatívnym a iným druhom pohybu, povedzme napríklad medzi absolútnym pohybom.   To je aj tento prípad. Laikovi sa zdá že autor tohto článku je odborník, fyzik na najvyššej úrovni, pritom ide o človeka archivára, ktorý predkladá čitateľovi archívne materiály spájajúce sa k téme relativity pohybu, no nevedno čoho, či svetla, zvuku, alebo jadrových častíc, či nie náhodou aj pohybu hmoty v stave chemických prvkov.   Z tohto dôvodu aj diskusia k tomuto článku je iba relativistická, teda o všetkom možnom, no o ničom konkrétnom. Toto moje konštatovanie považujte za moju konštruktívnu kritiku a nie za zneváženie vašej publikácie. Takže aby diskusia o relativite pohybu v spojitosti s Einsteinovými teóriami relativity mala hlavu aj pätu, uvezte prosím váš osobný dojem o rozdiely medzi absolútnym a relatívnym pohybom (zatiaľ bez udania čoho konkrétneho).   Teším sa na odpoveď. 
  
  
 Reagovať | genius |  
   
 Takze ste si tu knihu o relativite neprecitali...  Clanok je prehladom experimentalnych dookazov VTR. Kedze ich je tolko a z tolkych roznych oblasti, asi tazko by sa dalo na sme blogu podrobne vsetko opisovat. Je to naozaj vycuc z roznych clankov. Ale myslim, ze ma svoj zmysel, minimalne ako ukazka impozantnej experimentalnej evidencie, na ktorej VTR (aj STR) stoja, napriek castemu vyskytu roznych geniov, ktori v nej hladaju (dokonca nasli) chybu.  Relativny je kazdy druh pohybu. Vzdy treba hovorit, vzhladom k comu, k akej suradnicovej sustave sa pohybujete. Ked ste vo vlaku, relativne voci sedacke ste v pokoji, no relativne voci stromu pri ceste ste v pohybe. To je ta relativita pohybu.  Specialna TR je specialna, pretoze hovori, ze vsetky rovnomerne priamociaro sa pohybujuce sustavy su rovnocenne. Nijakym experimentom v ramci sustavy samotnej (ani mechanickym, ani elektrickym ani jadrovym, ...) nemozno urcit, ci sa rovnomerne priamociaro pohybujete, alebo stojite. Takze neexistuje absolutny pohyb - nie je mozne povedat, ze ten a ten sa naozaj pohybuje rovnomerne priamociaro no ten druhy v skutocnosti stoji. Voci niekomu sa mozete pohybovat, voci inemu stat. Ci ste v takom alebo onakom pohybovom stave (pokoj, rovnomerny pohyb lubovolnou rychlostou) v kazdom pripade pouzivate rovnaku fyziku a rovnako prebiehaju vsetky procesy (fyzikalne, chemicke, biologicke...). Inak princip relativity je Galileiho vynalez, Einstein sa ho len tvrdohlavo pridrzal v case, ked sa zdalo, ze elektromagneticke javy by nam mali umoznit urcit, ci sa naozaj pohybujeme alebo nie (ale experiment to aj tak nepotvrdil, takze Albert mal pravdu). 
 Reagovať | PeKa | 
  
 Veľmi pekne vám ďakujem za vašu odpoveď, i keď priamo ste nezadefinoval rozdiel medzi relatívnym a absolútnym pohybom, dá sa z vašej odpovedi vycítiť, vydedukovať, že za absolútny pohyb by ste považovali taký pohyb hmoty, pri ktorom by ste vedeli určiť nejakým experimentálnym spôsobom aspoň smer pohybu hmoty vesmírom, keď nie už priamo jeho vesmírnu rýchlosť. Nakoniec očakávať že definíciu absolútneho pohybu by nepoznal učiteľ na univerzite je dosť nepravdepodobné.  
 Ale vrátim sa k vášmu citátu: „Inak princip relativity je Galileiho vynalez, Einstein sa ho len tvrdohlavo pridrzal v case, ked sa zdalo, ze elektromagneticke javy by nam mali umoznit urcit, ci sa naozaj pohybujeme alebo nie (ale experiment to aj tak nepotvrdil, takze Albert mal pravdu).“  
 No o tom sa dá veľmi pochybovať, lebo „Galilleov vynález“ je ale o úplne inom, než si to Einstein vysvetľoval.  
 Galieo tvrdil, že zmena rýchlosti pohyb hmoty nevplýva na zmenu žiadnych fyzikálnych parametrov (rovnomerným pohybom)  pohybujúcej sa hmoty, (sústavy) zatiaľ čo Albert Einstein tvrdil, úplný opak a to, že zmena rýchlosti telies vyvoláva v hmote zmenu troch jej základných parametrov a to, zmenu priestorovej geometrie, zmenu rýchlosti plynutia času, a nakoniec zmenu hmotnosti pohybujúceho sa telesa, pričom konkrétna zmena uvedených troch fyzikálnych parametrov je viazaná no konkrétnu, teda na absolútnu rýchlosť pohybu telies, lebo v opačnom prípade celá Einsteinova špeciálna teória nemala zmysel, lebo do podielu v2/c2 by sa za hodnotu v mohla dokladať akúkoľvek relatívna rýchlosť, pričom ten podiel funguje v Lorentzových rovniciach iba vtedy, keď to v je absolútna, čiže jediná rýchlosť pohybu sústavy.  
 Aby som tému nerozťahoval, bol by som vám vďačný, keby ste sa vyjadril k hodnote v2 v Lorentzových rovniciach, presnejšie o akú rýchlosť ide v prípade v2.  
 Pochopenie definície absolútneho pohybu je nevyhnutné  pre zadefinovanie relativity pohybu, lebo potom sa mieša absolútny pohyb z relatívnym, takže ako sa z toho môže vyznať fyzikálny laik, keď ani Einsteinovi nebol absolútny pohyb jasný.  
 Einstein svojou špeciálnou teóriou v skutočnosti zadefinoval absolútny pohyb hmoty, ale keďže vtedy ešte občan A. JÁRAY, nežil nemal mu to kto povedať. 
 Úprimne sa teším na odpoveď. 
  
  
 
 A tu sa (ako vždy) diskusia o Einsteinových teóriách končí, lebo to podstatné, čiže akou konkrétnou rýchlosťou sa teleso v matematických rovniciach Alberta Einsteina pohybuje, nevedel ani sám Einstein. Einstein tvrdil, že ak sa teleso pohybuje rýchlosťou blízkou rýchlosti svetla, teda 0,999 c tak sa spomaľuje starnutie astronautov. Einstein ale nepovedal, že podľa čoho sa dá zistiť kedy sa teleso pohybuje uvedenou rýchlosťou.  
 Pravda je taká, že podľa samotnej teórii relativity sa to nikdy nedá zistiť, ale zato sa dá o tom kecať do aleluja.  
 Skutočnosť, že pri rýchlosti 0,999 hmota sa už nemôže nachádzať v atómovom stave, teda ani v bunečnom stave, teda o spomalení starnutia astronautov  nemôže byť ani reči, Einstein sa takticky vyhol. Ak pripustíme pravdivosť Einsteinových teórii relativity, tak iba na pohyb svetla, či na efekty poľnej formy hmoty. Na hmotu v stave chemických prvkov, Einsteinova teória relativity nemá žiadaný vplyv i keď jej sláva bola odvodená práve kvôli mylne predpovedaným efektom, ktoré by mali vzniknúť v hmote v stave atómov chemických prvkov pri vysokých (relatívnych - neidentifikovateľných) rýchlostiach, približujúcich sa k rýchlosti svetla. 
 Autorovi článku napísal som toto:  
 
------------------------------------------------------------------------
 Vážený Pán peka.  
 
 
 Vážený Pán peka, vyzývam vás do diskusii o obsahu vášho článku a ak máte relevantné argumenty, určite ma roznesiete na "kopytách vedy". Je to seriózna ponuka, ktorou aj vy môžete prispieť k osvete vedeckých poznatkov a nie v poslednom rade aj k obhajobe autority relativistických vedcov, ktorých ja beztrestne nazývam grázlami. Verím že vy v tejto diskusii nemôžete prehrať.  
 Diskusia je začatá na stránke:  
 http://jaray.blog.sme.sk/clano...  
 Teším sa na vaše príspevky.  
 Reagovať |  genius |   
 ------------------------------------------------------------------------- 
 Vazeny pan GPSc Jaray,  
 tazko sa diskutuje s niekym kto urobil tak hlboku brazdu v sucasnej vede a pritom je "veľmi skromný a ešte viacej geniálny jedinec, vysloboditeľ - ľudstva z bludov štátmi vyživovaných, abstraktných vedeckých grázlov." Jednak nie som statom plateny a tym padom na to, aby som vas presvedcil logickymi argumentmy jednoducho nemam dostatok casu (neurazte sa, ale mam lepsiu robotu nez hrach o stenu hadzat), kedze z Vasich prispevkom je jasne, ze potrebny cas by divergoval k nekonecnu. No a nie som tiez ochotny zvysovat vam karmu svojimi kvalitnymi prispevkami, ktore ste obratom zeverejnili na blogu.  
 S pozdravom PeKa.  
 -------------------------------------------------------------------------  
 PS: Navyse som za kazdu recesiu a ta Vy robite tu recesiu tak dobre, ze som Vam na chvilu normalne uveril, ze to nehrate (az kym ste ma neupozornili na to, ze praca v gravitacnom poli nezavisi od drahy telesa, len od rozdielu vysok pociatocneho a koncoveho bodu, ked som si zacal robit recesiu ja).  
 Takze vsetko dobre, pokracujte dalej a nebojte sa, raz to pochopia aj ostatni a potom nalezite ocenia Vas zmysel pre humor (mozno aj ti "grazli" na SAVke:). 
  Reagovať | PeKa |  
 ------------------------------------------------------------------------- 
  
 Pán Peka!  
 Samozrejme, že nemusíte robiť nič čo nechcete. Ale ako autor fyzikálnych článkov na internete a v (povedzme) vedeckých časopisoch, nemali by ste mať problém povedať aká je rýchlosť (v) menovateli, Einsteinom použitej Lorentzovej rovnici, absolútna či relatívna. Ide o dve jasné odpovede a pre vás o určenie jednej z nich. To by vám nemalo robiť problém. Tak by to vyzeralo, že z dôvodu neschopnosti odpovedať na uvedenú otázku z boja zutekávate. To by pre relativistickú vedu rozhodne nešlo ku duhu.  
 Reagovať |  genius |  
 Sa netvarte, ze takuto vec neviete. Ved ked nieco kritizujete, musite sa v danej teme dobre vyznat. v je velkost vzajomnej rychlosti dvoch sustav voci sebe. Jedna z nich sa pohybuje relativne rychlostou v v urcitom smere voci druhej, druha zasa rychlostou velkosti v v opacnom smere voci prvej.  
 Reagovať | PeKa |  
 -------------------------------------------------------------------------  
 Pohyb svetla je relatívny, či absolútny?  
 Aby mohol nastať efekt zvaný paradox dvojčiat, aby jeden starol rýchlejšie ako druhý, nemôžu sa predsa pohybovať rovnakou relatívnou rýchlosťou, to by postavilo Einsteinovu teóriu na hlavu. Ako pomocná otázka k pochopeniu hodnoty (v) v Einsteinových rovniciach je nasledovná otázka. Ak je pohyb svetla absolútny, tak o čom sú teórie relativity. Ak je pohyb svetla relatívny, ale stálej absolútnej hodnoty, tak vzťahom k čomu je pohyb svetla relatívny. Ide o tak naivné otázky, že sú skôr na výsmech ako na odpoveď, ale aj po ukončení smiechu žiadna odpoveď na dané otázky neprichádza, lebo neexistuje. Tak ako pri pojme energia fyzika nevie o čom rozpráva, tak isto aj pri pojme relatívny pohyb, so stálou a konečnou (absolútnou) rýchlosťou fyzika nevie o čom hovorí. A čo je ešte horšie ani to nechce vedieť.  
 Reagovať |  genius |  
 ------------------------------------------------------------------------- 
 Autor článku by dostal za jeho obsah na univerzite jedličku, no v skutočnosti splodil znôšku experimentom vyvrátených nezmyslov, ktorých filozofické zdôvodnenie nie je ani možné. Autor článku si nevšimol, že k tomu by mohli platiť Einsteinove rovnice, musí sa do nich dosadiť absolútna rýchlosť pohybu hmoty, lebo v opačnom prípade celá Einsteinova múdrosť je v kýbli. 

 
  ------------------------------------------------------------------------- 
Až niekedy budete čítať vedecké články o Einsteinových teóriách pohybe astronautov rýchlosťou  0,9 c, tak si dajte otázku, prečo toho kozmonauta premenila NASA na prúd protónov a neutrónov, lebo  pri takejto rýchlosti už atómy ani bunky existovať nemôžu, no môžu existovať sprostosti Einsteinových teórii relativity absolútneho pohybu hmoty v stave chemických prvkov. 
 Autor tohto článku ma mohol zosmiešniť pre moje neznalosti z fyziky, no stal sa opak, on radšej  zdupkal z tejto diskusie aby sám nebol zosmiešnený z neznalosti elementárnych princípov fyziky. 
 A takým sa vláda SR snaží dávať prniaze na základný výskum.  
 Na východe naši Rusínski spoluobčania na to zvyknú hovoriť:  
 Tiperka taky svit: 
  
 
 
   
 
 
   


