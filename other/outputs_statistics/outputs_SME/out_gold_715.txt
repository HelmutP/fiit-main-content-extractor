

 
Presne pred dvoma rokmi (26.júna 2006) som sa rozhodol, že sa nenechám kopať múzou len tak pre nič za nič. Výsledkom bola žiadosť o registráciu na blogoch SME. Po prvých nesmelých článkoch, ma táto aktivita, ako mnohých, úplne pohltila. Prišla aj kríza a ja som chcel veľakrát  s blogovaním praštiť, vždy ma však podržala komunita ľudí, ktorí sú mojou krvnou skupinou. ´ 
 
 
Práve oni sú tým najcennejším, čo mi blogovanie dalo. Tu je zopár mojich lások z blogu. 
 
 
Boris Burger 
 
 
 
Kto by nemal rád Boriska?! Starosvetský gentleman s dobrými spôsobmi, aké sa dnes už nenosia. Má už niečo prežité, preto nikdy nikoho neodsudzuje. Práve pre jeho jemnocit som si ho tak obľúbil. Ako som mu raz napísal do diskusie – nikto nevie tak hmatateľne vystihnúť atmosféru ako on, keď píše svoje zážitky alebo spomienky. 
 
 
Prečítajte si niečo od Borisa 
 
http://burger.blog.sme.sk/c/108448/Nieco-malo-o-detstve-o-auguste-68.html 
 

 
 
 
 
 
 

 
Gabina Weissová 
 
 
Blogerka roka 2006. Žena, mama dvoch krásnych dietok, ktorá svoj život žije naplno a snaží sa popri práci novinárky pomáhať dievčatám, ktoré zažívajú to, čo si ona prežila – anorexiu a bulímiu. Obdivujem jej elán a energiu, s akou sa do všetkého púšťa. 
 
 
 
Prečítajte si niečo od Gabiny 
 
 
http://weissova.blog.sme.sk/c/55601/Do-neba-sa-uz-nedostanem.html 
 

 
 

 
 

 
 
 
 
 
 
 
 
 
Martin Marušic 
 
Starý známy. Keď sme spolu hrávali šach za Dúbravku, nikdy by som nepovedal, čo sa z neho vykľuje – Mr. Bean blogov SME. Neviem, či niekto priniesol do blogosféry take spestrenie ako práve Martin svojimi návodmi ako piť pálenô a neožrať sa pod obraz Boží. O tom, že Martin vie písať aj seriózne články svedčia jeho odborno-náučné tunelárske opisy. Nebyť našich politikov, bol by to najväčší slovenský tunelár. 
 
 
Prečítajte si niečo od Martina 
 
 
http://marusic.blog.sme.sk/c/82570/Ako-som-blicoval-a-dostal-za-to-cestne-uznanie.html 
 

 
 
 
 
 
 
Juraj Drobný 
 
 
Naša “velebnosti”, ako ho nazvala Marína. Je šéfom môjho milovaného festivalu Verím Pane v Námestove, pracuje v Lux Communication, práve rozbieha kresťanskú TV LUX. Okrem toho je kňazom, ktorý si vie získať každého. Aspoň mňa teda hej.  
 
Prečítajte si niečo od Juraja  
 
http://jurajdrobny.blog.sme.sk/c/81641/Dnes-v-noci-zomrel-Bunheng.html 
 
 

 
 
 
 
 
Dávid Králik 
 
 
Pán Učiteľ – s veľkým P aj U. Kto čítal Dávidove články, vie, že tento učiteľ berie školu niečo ako poslanie a studnicu nekonečných nápadov. S deťmi dokáže vtipkovať, často ich nachytá, robí si z nich prvý apríl, v humore mu nesiahajú ani po členky. S Dávidom som mal tú česť spolupracovať na workshope o blogoch pre deti zo slovenských škôl. Bez jeho pomoci by to nebolo ono. 
 
 
Prečítajte si niečo od Dávida  
 
http://davidkralik.blog.sme.sk/c/149965/Ked-pisomka-dopadne-fakt-zle.html 
 

 
 

 
 

 
 

 
 

 
 
 
 
 
Viki Holman 
 
Prvý bloger, s ktorým som sa virtuálne stretol. Moderátor Rádia Frontinus, vášnivý obdivovateľ syrov, kedysi aj motoriek a v neposlednom rade neúnavný organizátor blogerských splavov. Hĺbka Vikiho článkov ma dostala, na druhej strane vie poukázať aj na vtipné “Prdelky” ako to sám nazval. 
 
Prečítajte si niečo od Vikiho - tento článok ma absolútne prenikol  
 
 
http://holman.blog.sme.sk/c/51885/Stretol-som-dnes-dieta.html 
 
 

 
 

 
 

 
 

 
 
 
 
 
Andy Sekanová 
 
 
Bratislavčanka, ktorá vie pútavo písať o svojom meste. Pre mňa je takou naozajstnou kamarátkou, aj keď  sme sa stretli len zopár krát. Veľmi ma povzbudila písať články o Slovensku, za čo som jej vďačný. 
 
 
Prečítajte si niečo od Andy    
 
 
http://sekanova.blog.sme.sk/c/123454/V-uliciach-Bratislavy.html 
 
 

 
 

 
 

 
 

 
 

 
 

 

 
 
 
 
 
 
 
 
 
Róbert Dyda 
 
 
Robovi som dlho nevedel prísť na chuť až raz mi jeden človek povedal, že Robo je naozaj férový chlapík. Som rád, že som sa o tom sám presvedčil. Nedávno som s ním robil rozhovor, pri ktorom sme sa dobre porozprávali. Jeho Literárna Bobopédia (LB) patrí podľa mňa medzi najhodnotnejšie články na blogu. 
 
 
Prečítajte si niečo od Roba 
 
 
http://dyda.blog.sme.sk/c/51865/Skusenost-si-nevygooglia.html 
 
 

 
 
 
 
 
Peťo Mikšík 
 
 
S Peťom som sa spoznal len nedávno. S neuveriteľnou trpezlivosťou mi pomáhal s technickou úpravou audio súborov. Páči sa mi na ňom taká pohodovosť – asi sa mu aj zíde ako administrátorovi blogov SME. Na blog dáva často zaujímavé veci z hudby. 
 
 
 
Prečítajte si niečo od Peťa 
 
 
http://petermiksik.blog.sme.sk/c/137591/Ako-som-za-totaca-emigroval-do-Polska.html 
 
      
 

 
 
 
 
 
Miro Šedivý 
 
 
Kto by nepoznal Mirov túlavý bicykel?! Jeho túry cez Alpy až po malebné francúzske, belgické a nemecké mestečká som zhltol jedným dychom. Svojho času sme po večeroch s Mirom chatovali na ICQ, s málokým by som to tak dlho vydržal, ale s Mirom to bola naozaj zábava. Asi si kvôli nemu zavediem internet a nainštalujem znova ICQ.   
 
 
Prečítajte si niečo od Mira 
 
 
http://sedivy.blog.sme.sk/c/61442/Tulavy-bicykel-v-Provensalsku-Alpach-a-blizkom-okoli.html 
 
  
 
 
 
 
 
Karol Sudor 
 
 
 
Pán novinár a recesista zároveň. Jeho rozhovory a videospovede politikov mu vyniesli ocenenie Ńaj- rozhovory roka 2007. V tomto je mojím vzorom. Páči sa mi jeho bezprostrednosť, s akou berie život. Tak sa aj (ne)pripravuje na rozhovory s respondentami z najvyšších kruhov a predsa jeho dialógy sú plné konfrontácie a zaujímavých spovedí. O jeho humore svedčí aj svadobný dar, ktorý mi dal – tabletky proti depresii.   
 
 
Prečítajte si niečo od Karola 
 
 
http://sudor.blog.sme.sk/c/143869/Peter-Stastny-o-Golonkovi-aj-Sirokom-a-naozaj-bez-obalu.html 
 
 

 
 

 
 
 
 
 
 
Roman Daniel Baranek 
 
 
Vlastne Roman a Danko, dvaja blogeri píšúci pod jedným menom. Ich recepty by mohla závidieť nejedna gazdiná a je krásne vidieť ako sa ženy učia od nich variť. Keď moja manželka stretla Danka, povedala – “to je ale sympaťák” – veru je.   
 
 
Prečítajte si niečo od Danka a Romana 
 

 
http://baranek.blog.sme.sk/c/144958/Varime-s-medvedom-Tvarohove-kolace-ktore-zvladne-kazdy.html 
 
 

 
  
 
Karol Trejbal 
 
 
 
Karolove články z medicínskeho prostredia ma prinútili zamyslieť sa nad životom, pretože od života k večnosti nie je ďaleko. Karola vnímam ako sympatického mladého lekára, ktorý ešte nestratil svoje ideály – to myslím teraz ako pozitívum.   
 
 
Prečítajte si niečo od Karola 
 
http://trejbal.blog.sme.sk/c/64920/Ked-ani-smrt-nie-je-nepriatel.html 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 
 
 
 
 
 
Alenka Technovská 
 
 

 
Krásna a milá fotografka. Keď mi niekto povie, ako som niečo pekne odfotil, vždy ho odkážem na naozajstných fotografov ako je Alenka, nech sa pozrie, čo je to dobrá fotografia. Alenka to všetko stíha popri skúškach na vysokej škole, ktoré s ňou prežívame všetci na interných diskusiách.   
 
 
Prečítajte si niečo od Alenky 
 
 
http://technovska.blog.sme.sk/c/92382/Fotografie-kategoria-ludia.html 
 

 

 
 
 
 
 
 
Jozef Javurek 
 

 
Asi nikto nepíše tak pekne a komplexne o Slovensku ako Jožko. Jeho fotoreportáže o Slovensku sú pre mňa zdrojom inšpirácie. Keby som bol vydavateľ, raz by som mu vydal knihu o Slovensku.    
 

 
Prečítajte si niečo od Jozefa 
 
http://jozefjavurek.blog.sme.sk/c/146281/Trnava-in-vivo.html 
 
 
 
 
 
Natália Blahová 
 
 
Naty je veľká bojovníčka. To sa ukázalo nedávno pri návrhu zákona v NR SR, ktorý by znemožnoval rýchlu adopciu detí z detských domovov do rodín. Má na starosti Asociáciu náhradných rodín, do ktorej dáva hádam ešte viac ako celú seba. Jej odhodlanie bojovať za dobrú vec môže byť velikánskym príkladom.   
 
 
Prečítajte si niečo od Naty 
 
 
http://nataliablahova.blog.sme.sk/c/150711/Vyzva-poslancom-NR-SR.html 
 
 

 
 
 
 
 
 
Zora Paulíniová 
 
 
Moja bývalá profesorka angličtiny na strednej škole. Kto by to povedal, že ju stretnem ako blogerku?! Je milovníkom historických pamiatok, ktoré chráni aj svojimi článkami na blogu. Pre mňa je symbolom angažovanosti, bol by z nej vynikajúci lobista.    
 
 
Prečítajte si niečo od Zory 
 
http://pauliniova.blog.sme.sk/c/128854/Smrt-pamiatky-v-priamom-prenose.html 
 

 
 
 
 
 
 
 
 
 
 
Jozef Klucho 
 
 
Pán gastroenterológ so zmyslom pre humor. Ten mu nechýbal, keď pre mňa napísal rady skúseného harcovníka do manželstva pre mňa nováčika v tomto stave. Okrem toho píše zväčša o svojom odbore, ktorým sú všelijaké gastro problémy – bližšiu definíciu nechám na neho.   
 
 
Prečítajte si niečo od Jožka 
 
 
http://klucho.blog.sme.sk/c/103971/Niekolko-rad-buducim-manzelom-urcene-vylucne-muzom.html 
 
 

 
 
 
 
 
 
Katka Maliková 
 
 
Margarétka romantická. Katku som stretol len raz a bol to pre mňa veľký zážitok, pretože Katarínka je nesmierne príjemným človekom. Známymi sú jej kokteily namiešané z citov ženskej duše.    
 
 
Prečítajte si niečo od Katky 
 
http://katarinamalikova.blog.sme.sk/c/118793/Spomienky-z-detskej-zasuvky.html 
 

 
 
 
 
 
 
Zuzka Šubová 
 
 
Rázna baba, lekárka na onkológii na Kramároch, ktorá vie otvoriť ústa. Neohrozená diskutérka, blogerka a mama troch detí. Za svoj názor vie bojovať, aj keď je v menšine. Často píše krátke postrehy odpočuté zo života. 
 
 
Prečítajte si niečo od Zuzky 
 
 
http://subova.blog.sme.sk/c/151632/Odpocute-na-cintorine.html 
 
 

 
 

 
 

 
 

 
 
 
 
 
 
Tak to ste vy, moji obľúbení blogeri, verím, že som vás potešil a že mi odpustíte "krádež" foto z vášho blogu:-) 
 
 

 
 
Ak ste sa v zozname nenašli, nezúfajte, možno som len trochu zabudol. A vonkoncom to neznamená, že nečítam váš blog:-) 
 

