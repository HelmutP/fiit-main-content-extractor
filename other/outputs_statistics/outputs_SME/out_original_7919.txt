
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alžbeta Ožvaldová
                                        &gt;
                Nezaradené
                     
                 ulica svornosti, podunajske biskupice 

        
            
                                    3.6.2010
            o
            10:41
                        (upravené
                9.6.2010
                o
                15:32)
                        |
            Karma článku:
                4.10
            |
            Prečítané 
            1484-krát
                    
         
     
         
             

                 
                    Nevídaný rozmach automobilizmu, ktorý je príznačný pre našu dobu, ale aj nevídaná individuálna bytová výstavba, ktorá je zase príznačná pre obce ležiace juhovýchodne od Podunajských Biskupíc, neustále zhoršujú dopravnú situáciu na území mestskej časti, negatívne ovplyvňujú životné prostredie a zhoršujú kvalitu života našich obyvateľov.
                 

                 Vo svojich sťažnostiach poukazujú na skutočnosť, že vodiči z okolitých obcí, predpokladajúc, že prejazdom uličkami mestskej časti si skrátia trasu aj čas, vytvárajú každé ráno množstvo nebezpečných dopravných situácií - nerešpektujú obmedzenie rýchlosti jazdy dopravným značením a neumožňujú našim obyvateľom výjazd na komunikáciu zo svojich pozemkov. Táto neúnosná situácia pretrváva už niekoľko rokov a žiadajú prijať opatrenia na obmedzenie prejazdu tranzitujúcich vozidiel obcou.  V neposlednom rade je potrebné brať v úvahu, že množstvo tranzitujúcich vozidiel mestskou časťou porispeieva k opotrebovaniu komunikácií v správe mestskej časti do takej miery, že každoročne  musíme odsúhlasovať zvýšenie rozpočtu na opravu výtlkov a rozpadov v komunikáciach, ktoré plynú z daní občanov našej mestskej časti Tento nepriaznivý stav dopravnej situácie, ktorý je najvypuklejší v období rannej dopravnej špičky nás donútil prijať opatrenia v zmene organizácie dopravy na vstupe do mestskej časti (Hydinárska, Šamorínska, Devätinova, Staromlynská, Vinohradnícka ulica). Je nám ľúto, že tieto obmedzenia sa dotknú aj niektorých obyvateľov našej mestskej časti alebo vodičov s cieľovou dopravou v Podunajských Biskupiciach. Riešením pre nich však je pravé odbočenie na prvej svetelne riadenej križovatke Mramorová, ktorá prechádza Vetvárskou rovno do stredu obce. O zmene v organizácii dopravy sme e-mailovou správou vyrozumeli Stella servis – dopravné informácie, ktorý mal zabezpečiť vysielanie vo všetkých médiách od 12. 5. 2010.   Zákon č. 135/61 Zb. o pozemných komunikáciách (cestný zákon) a jeho vykonávací predpis Vyhláška FMD č. 35/84 Zb., ktorým sa vykonáva zákon o pozemných komunikáciách, rozdeľuje komunikácie podľa ich dopravného významu a určuje ich spôsob používania. Miestne komunikácie slúžia miestnej doprave, teda komunikačným účelom s cieľovou dopravou v obci. Pre medzimestskú dopravu sa využívajú štátne cesty, rýchlostné komunikácie a diaľnice. V konkrétnom prípade je to štátna cesta I./63 (Ul. svornosti), ktorá je navyše aj cestou pre medzinárodnú premávku E-575 zo smeru od obce Rovinka a štátna cesta 572 zo smeru Most pri Bratislave. Mestská časť spravuje miestne komunikácie III. a IV triedy, budovanie komunikácií vyšších tried patrí do pôsobnosti iných orgánov. Mestská časť však iniciovala už trikrát rokovania ohľadom rozšírenia Ul. Svornosti na ktoré sme pozvali KDI, KÚCD, BSK, Magistrát a NDS. Z NDS ani raz neprejavili o to záujem. A popritom to nie je problém len Hlavného mesta Ba, ale celého juhovýchodného regiónu od Bratislavy.   Žiaľ, v živote už je to tak, že realizácia akokoľvek dobre mieneného úmyslu neprinesie len pozitívny výsledok – odbremenenie ulíc v centrálnej časti obce od tranzitujúcej dopravy, ale niekde aj negatívne zasiahne do života obyvateľov. Návrh projektu zmeny organizácie dopravy bol tri razy prepracovaný z dôvodu zásahov a neakceptovania našich výhrad Krajským úradom pre cestnú dopravu a pozemné komunikácie v Bratislave, ktorý je cestným správnym orgánom pre štátnu cestu I./63 a teda priamo zodpovedným za súčasný stav. Bez viny nie sú ani starostovia okolitých obcí, ktorí okrem vydávania stavebných povolení a iniciovaní petície pre najnevýhodnejší variant výstavby rýchlostnej cesty R7 – variant „C“, ktorý bude zaúsťovať do Ul. Svornosti, toho pre pôvodných, ani nových obyvateľov veľa neurobili, aby sa bez problémov a zdržaní dostali za povinnosťami do hlavného mesta. V novembri minulého roka som navštívila ministra  dopravy pôšt a telekomunikácií, kde výsledkom rokovania bolo, že R7 musí byť robený súbežne s D4 (nultým obchvatom), aby sa rýchlostná komunikácia napojila na diaľnicu. Je mylná tá predstava, že R7 sa môže robiť bez nultého obchvatu. Zasa vrátime dopravu na Ul. Svornosti. Čo s tým získame? Pozbierame autá aj z okolia Miloslavova a zacpeme štátnu cestu po Dunajskú Stredu? Nakoniec si myslím, pokiaľ sa urobí R7 a D4 prejde veľa rokov. A preto rýchlym preventívnym riešením je rozšírenie Ul. Svornosti, ktorá sa dá, len treba chcieť.   Obyvatelia Podunajských Biskupíc nie sú druhoradí občania tejto republiky, ktorí musia dlhé roky znášať nečinnosť vyšších orgánov. A obyvatelia okolitých obcí by si mohli uvedomiť, že vlastne bojujeme za nich, za lepšie podmienky pri vstupe do hlavného mesta, čo starostovia okolitých obcí pri nevídanej bytovej výstavbe nejako pozabudli, že infraštruktúra nie je vybudovaná na zabezpečenie plynulého vstupu do Bratislavy za prácou.   Verím, že tento stav nebude mať dlhotrvajúci charakter a veci sa pohnú dopredu.       Alžbeta Ožvaldová, starostka MČ Ba - Podunajské Biskupice         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Ožvaldová 
                                        
                                            na aktuálnu tému
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Ožvaldová 
                                        
                                            list premiérke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Ožvaldová 
                                        
                                            dobrá správa pre všetkých!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Ožvaldová 
                                        
                                            rozšírenie cesty i/63
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alžbeta Ožvaldová 
                                        
                                            na aktuálnu tému
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alžbeta Ožvaldová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alžbeta Ožvaldová
            
         
        ozvaldova.blog.sme.sk (rss)
         
                                     
     
        starostka mestskej časti Bratislava - Podunajské Biskupice
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    886
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




