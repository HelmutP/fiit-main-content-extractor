
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Babič
                                        &gt;
                Nezaradené
                     
                 Slovenské absurditky - krátko zo Športu 

        
            
                                    23.6.2010
            o
            23:30
                        |
            Karma článku:
                13.34
            |
            Prečítané 
            3440-krát
                    
         
     
         
             

                 
                    Nie, nepôjde o všade prepieraný futbalový turnaj v Afrike lež o reštauráciu, ktorej by ste sa mali vyhnúť širokým oblúkom - Hotel Dom Športu. V práci jej hovoríme aj "Stroj času", pretože obsluha, prostredie i jedlá v nej Vás razom prenesú do čias spred nežnej revolúcie. Obedy v tomto zariadení sme brali ako určitú občasnú recesiu, no to, čo sa tam udialo pri nedávnom obede prekročilo všetky medze únosnosti...
                 

                     Boli sme piati - kolegovia, čo chceli spolu poobedovať, pohovoriť a samozrejme najesť sa. Bolo preplnené a tak sme sa usadili k stolu, pri ktorom normálne sedávajú štyria - ten piaty si ku kratšej hrane stola prisunul stoličku a v duchu "dobrých ľudí sa všade veľa zmestí" sme sa usadili.   Veľmi rýchlo k nám prišiel nemý čašník a položil pred nás misu so slepačou polievkou. Na stole nie sú žiadne taniere ani príbor. Časť príborov priniesol po upozornení sám, zvyšok sme zozbierali zo stolov okolo. Pozorovať potulujúcich sa hostí, čo sa v záujme toho, aby nejedli rukami či nechlípali priamo z misy stávajú na pár sekúnd obsluhou tu vídať bežne.   Kolegovi sa po pár lyžiciach polievky zažiada chlieb a ide oň poprosiť. Kombinácia slepačia polievka-chlieb je menej ako bežná no podráždenú otázku "Už ste videli jesť slepačiu polievku s chlebom?" si platiaci zákazník určite nezaslúži.   Nevadí, s polievkou prehĺtame aj údiv, ale bojujeme ďalej. Po pár minútach prichádza mĺkva, do našej sekcie pridelená a pekne zdutá "čašníčka". Postaví sa k vytŕčajúcemu kolegovi a direktívne nariadi "Tú stoličku tam!", ukazujúc pritom niekam preč. Tomu takmer zabehne, najprv skúsi "to ste mysleli vážne?" pohľad, potom vysvetľuje, že takto sedia ľudia aj pri iných stoloch.   "Tam ale nemusím prechádzať, takže ak nechcete mať na hlave taniere..."   "Vy ste čo za obsluha?", pýta sa kolega.   "Výborná", odpovedá pohotovo a lakonicky táto deva v post pubertálnom veku.   (Zatiaľ pri vedľajšom stole): "Vedúca-dôchodkyňa" dobieha pánov v strednom veku ako sa dvíhajú a spucuje ich ako malé deti, že nabudúce si majú niekoho zavolať, nie nechávať peniaze na stole. Tí sa nezmôžu na slovo a odchádzajú.   Nahnevaní sa presúvame k inému, väčšiemu stolu , pričom si veci samozrejme presúvame sami. Vedúca-dôchodkyňa zatiaľ bľačí po svojej chase: "Ako to, že tu ešte nemajú polievku a už majú hlavné jedlá, čo je to za bordel?"   (Zatiaľ pri ešte inom vedľajšom stole): Dievčina, ktorá si objednala špagety s boloňskou omáčkou ešte síce nedostala polievku, no už má na stole hlavné jedlo - motýliky s boloňskou omáčkou. Fotí si ich do mobilu s komentárom, že také špagety ešte nevidela.  Môže byť zrejme rada, že to neboli kolienka.   V našej pätici si dvaja objednávame mäso, kôprovú omáčku a zemiaky a traja špagety. Po 40 minútach, v teraz už poloprázdnej miestnosti už kolegovia vylízali zo svojich tanierov aj posledné molekuly a my traja sme už aj zabudli, že sme jedli nejakú polievku. Voláme "vedúcu". Tá príde a povie, že platiť máme deve, ktorá nás obsluhovala. Dvaja platíme do centu presne, zvyšní traja odchádzame.  Bol to sen?   Ak sa chcete nechať za 3.40 EUR, čo tu stojí obedné menu vytočiť a ponížiť - je to miesto pre Vás. Inak nemá spoločné nič so športom ani s pohostinstvom.   Česť práci!   Odkaz na Hotel Dom Športu 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Hrubá čiara
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Slovenská reštaurácia v kocke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Slovenské absurditky - zákaznícky servis v predajni GATE
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Slovenské absurditky - výmena peňazí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Slovenské absurditky - vybavovanie dokladov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Babič
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Babič
            
         
        babic.blog.sme.sk (rss)
         
                                     
     
        Absolvent Fakulty riadenia a informatiky ŽU (2003). Od marca 2004 do októbra 2008 pracoval a žil v Severnej Karolíne, USA. Už opäť doma.  Ex redaktor a šéfredaktor univerzitného magazínu. Angažuje sa aj na blogu sociálna dynamika. Miroslav Babic's Facebook profile
 

 WeatherReports.com 
 

     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3840
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Život v USA
                        
                     
                                     
                        
                            Inšpirácia
                        
                     
                                     
                        
                            Tipy na dobré filmy
                        
                     
                                     
                        
                            Recepty z USA
                        
                     
                                     
                        
                            Photographie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Blbnutie a z dlhej chvíle
                        
                     
                                     
                        
                            Keď blog musí veľa zniesť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Som rada, že moja mama vie zapletať cibuľu
                                     
                                                                             
                                            Som späť - priateľ, čo sa druhýraz narodil
                                     
                                                                             
                                            Ahoj mami
                                     
                                                                             
                                            Život predtým a život potom
                                     
                                                                             
                                            O depresii
                                     
                                                                             
                                            Kde je domov?
                                     
                                                                             
                                            Zakladám klub tých, čo nikdy nezbohatnú
                                     
                                                                             
                                            Bonmoty Miloša Zemana
                                     
                                                                             
                                            Kým nás smrť nerozdelí
                                     
                                                                             
                                            Čo dokáže slovenčina
                                     
                                                                             
                                            Made in Tajikistan
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            D.Goleman - Social intelligence
                                     
                                                                             
                                            Robert M.Pirsig - Zen and the art of motorcycle maintenance
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Karel Kryl
                                     
                                                                             
                                            Dance, trance, house music
                                     
                                                                             
                                            Zuzana Smatanová
                                     
                                                                             
                                            Alanis Morissette Discography
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marta Vitáriušová
                                     
                                                                             
                                            Soňa Hrúziková
                                     
                                                                             
                                            Peťa Salajková
                                     
                                                                             
                                            Katka Pisutova
                                     
                                                                             
                                            Radovan Valachovic - spojka v NYC
                                     
                                                                             
                                            Gabi Weissowá
                                     
                                                                             
                                            Jozef Zubrický - o ekonómii zaujímavo a netradične
                                     
                                                                             
                                            Silvia Knowles
                                     
                                                                             
                                            Sused odvedľa :-) (Grzegorz Grabowski)
                                     
                                                                             
                                            Lucia Bačíková
                                     
                                                                             
                                            Food Guru
                                     
                                                                             
                                            Až raz budem takto písať...
                                     
                                                                             
                                            Sveťo Styk
                                     
                                                                             
                                            Mária Lazárová
                                     
                                                                             
                                            Mirka Gúčiková
                                     
                                                                             
                                            Maya Semanová
                                     
                                                                             
                                            Peter Bigoš
                                     
                                                                             
                                            Peter Piatka
                                     
                                                                             
                                            Miriam Novanská
                                     
                                                                             
                                            Leonard Muška
                                     
                                                                             
                                            Sandra Dernerová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Wikipedia - SK
                                     
                                                                             
                                            Alma Mater - ŽU
                                     
                                                                             
                                            Fotografie amatérov i profesionálov
                                     
                                                                             
                                            Univerzitník ŽU - ŽUŽO
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hrubá čiara
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




