

 Odmeňovanie zamestnancov štátnej služby 
 Od januára začala platiť aj novela zákona o štátnej službe, ktorá mení odmeňovanie štátnych úradníkov. Doteraz boli jediným kritériom, od ktorého závisela výška mzdy, odpracované roky praxe. To zákon ruší a namiesto toho zavádza bodové hodnotenie podľa kvality práce a výkonu úradníka. Zákon delí štátnych úradníkov do platových tried, podľa pracovného zaradenia, v každej triede je jednotný plat, ku ktorému môže úradník získať príplatky podľa toho, koľko bodov získal. Body sa udeľujú podľa kvality práce. Za kvalitnú prácu bude možné získať príplatky až 100 percent tarify v jeho platovej triede. Zákon ruší príplatok k dôchodku, odchodné bude len vo výške jedného tarifného platu. 
 Počet platových tried sa však zvyšuje o dve z dnešných 9 na 11 - do najvyšších dvoch tried budú patriť zamestnanci vykonávajúci najnáročnejšie činnosti na ústredných orgánoch, a tí, ktorí tvoria zásadné zákony súvisiace s členstvom v Európskej únii. 
 Platové tarify štátnych zamestnancov (v Sk mesačne) 
 
 



Platová trieda 
Platová tarifa 


1. 
7 350 


2. 
7 940 


3. 
8 610 


4. 
9 360 


5. 
11 190 


6. 
11 880 


7. 
13 460 


8. 
14 430 


9. 
16 450 


10. 
18 760 


11. 
21 390




  Čo sa zmení pre zamestnancov v štátnej službe 
 * Popri stálej, dočasnej a prípravnej štátnej službe sa zavedie aj tzv. nominovaná štátna služba. Kto do nej bude chcieť byť zaradený, musí zložiť skúšku, ktorú organizuje Úrad pre štátnu službu. Do tejto služby sa bude môcť prihlásiť každý štátny zamestnanec v stálej štátnej službe a v prechodnom období aj ktorákoľvek fyzická osoba, ktorá bude mať dostatočné vzdelanie, znalosti jazyka a dobré bodové hodnotenie. 
 * Iba pre zamestnancov v nominatívnej službe bude platiť definitíva. Predpokladá sa, že do nominatívnej služby vstúpi asi 10 percent z 30-tisíc štátnych zamestnancov. Nominovaná štátna služba by mala tvoriť jadro štátnej služby, preto sa pre jej členov definitíva zachováva - nebudú môcť byť prepustení, iba zaradení mimo činnej štátnej služby pri znížení počtu štátnozamestnaneckých miest, majú nárok na niektoré ďalšie finančné výhody. 
 * Pre ostatných zamestnancov štátnej služby definitíva končí. Nebudú zaraďovaní mimo činnú štátnu, ale služobný úrad im bude môcť dať výpoveď. 
 Kto sú štátni úradníci 
 * zamestnanci ministerstiev a iných úradov štátnej správy (napríklad novovzniknuté úrady, ktoré nahrádzajú okresné úrady), zamestnanci kancelárie Národnej rady, kancelárie prezidenta, zamestnanci Najvyššieho kontrolného úradu, Ústavného súdu, súdov, prokuratúry, zamestnanci polície a väzenskej stráže a vojska, nie však samotní policajti, vojaci či strážcovia. 
 Povinnosti štátneho zamestnanca podľa zákona o štátnej službe 
 * zachovať mlčanlivosť o veciach, ktoré sa dozvedel pri výkone služby, 
 * vykonávať štátnu službu nestranne a politicky neutrálne, 
 * zdržať sa konania, ktoré by mohlo viesť ku konfliktu záujmov osobných a verejných a na každý takýto konflikt upozorniť, 
 * nezneužívať informácie zo služby v prospech seba a blízkych, 
 * štátny zamestnanec v službe nemôže prijímať dary a iné výhody od právnických a fyzických osôb, 
 * nemôže sprostredkúvať pre seba ani inú fyzickú či právnickú osobu obchodný kontakt so štátom, obcou, VÚC, štátnym podnikom, alebo inou právnickou osobou zriadenou štátom, obcou či VÚC, 
 * nemôže nadobúdať majetok od štátu, obce či VÚC inak, ako vo verejnej súťaži či dražbe, 
 * zneužívať výhody z vykonávania služby aj po skončení štátnozamestnaneckého pomeru, 
 * štátny zamestnanec nesmie podnikať alebo vykonávať inú zárobkovú činnosť, okrem poskytovania zdravotnej starostlivosti v štátnych zariadeniach, okrem prednášania, publikovania a vedeckého bádania, práce v detských táboroch, spravovania majetku, členstva v komisiách. Ako znalec alebo tlmočník môže štátny zamestnanec pôsobiť iba, ak ide o službu pre súd, alebo štátne obecné či samosprávne orgány. Ak jeho príjem z týchto činností presiahne za rok sumu 50-tisíc, musí to oznámiť. 
 * štátny zamestnanec nesmie byť členom riadiacich, kontrolných alebo dozorných orgánov právnických osôb vykonávajúcich podnikateľskú činnosť. To neplatí, ak ho do tohto orgánu vyslal zamestnávateľ, nesmie však za túto prácu dostávať odmenu. 
   

