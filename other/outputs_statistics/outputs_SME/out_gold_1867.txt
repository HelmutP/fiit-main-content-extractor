

 Austrália         1  AUD  22,464  22,690  22,577 
 Belgicko        100  BEF  99,290  100,288 99,789 
 Česká republika	1  CZK	1,099	1,111	1,105 
 Dánsko            1  DKK  5,282   5,336   5,309 
 Fínsko            1  FIM  6,578   6,644   6,611 
 Francúzsko	1  FRF	5,943	6,003	5,973 
 Grécko          100  GRD  12,380  12,504  12,442 
 Holandsko         1  NLG  18,240  18,424  18,332 
 Írsko             1  IEP  47,084  47,558  47,321 
 Japonsko        100  JPY  28,461  28,747  28,604 
 Kanada            1  CAD  21,583  21,799  21,691 
 Luxembursko     100  LUF  99,290  100,288 99,789 
 Nórsko            1  NOK  4,677   4,725   4,701 
 Portugalsko     100  PTE  19,642  19,840  19,741 
 Rakúsko           1  ATS  2,904   2,934   2,919 
 Nemecko           1  DEM  20,417  20,623  20,520 
 Španielsko      100  ESP  24,240  24,484  24,362 
 Švajčiarsko	1  CHF	25,121	25,373	25,247 
 Švédsko           1  SEK  4,411   4,455   4,433 
 Taliansko      1000  ITL  19,033  19,225  19,129 
 USA               1  USD  29,648  29,946  29,797 
 V. Británia	1  GBP	45,675	46,135	45,905 
 EMS-ECU           1  XEU  37,623  38,001  37,812 
 MMF-ZPC-SDR	1  XDR	43,773	44,213	43,993 

