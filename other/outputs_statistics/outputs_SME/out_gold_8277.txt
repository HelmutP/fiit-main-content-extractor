

 Milý admin, vieš, nechcem vyznievať ako mladá puberťáčka, ktorej niečo nedovolili a teraz sa nahlas obraňuje a vykrikuje, že to celé bolo neprávom. V skutočnosti si chcem len tak nahlas vzdychnúť, oddýchnuť si a spraviť si písaciu blogovaciu pauzu v štúdiu. 
 Vieš, nechcem zo seba robiť nejakú dôležitú osobu a budovať si vážnosť, ktorú ani nemám. A naozaj by som s obrovským úsmevom a roztiahnutými rukami prijala, keby sme si tu všetci mohli tykať tak, ako to robia vo Švédsku. Nie, nie, nemyslím taký dehonestujúci súdruhovský spôsob tykania, ale taký milý, omladzujúci, severský. Ale bohužiaľ, slovenčina odlišuje tykanie od vykania a predsa len, som už vo veku, keď v tých formálnych záležitostiach uprednostňujem, keď mi ľudia vykajú, pretože mám inak pocit (vďaka nuansám nášho krásneho jazyka), že ma považujú za menejcennú. A viem i to, že v internetových diskusiách je zvykom si tykať, čo považujem za normálne, pokiaľ je však celá oper stránka môjho blogu či celá navigácia na stránke urobená tak, že mi tu vykajú, myslím, že je normálne, aby si tak spravil i v rôznych upozorneniach, ktoré mi posielaš. (a toť i vysvetlenie môjho tykania) 
 Veru, k upozorneniam. K môjmu včerajšiemu článku si mi napísal: 
 "zdravime, clanok sme Ti presunuli na sukromny blog, pretoze kodex nedovoluje zvyraznovat pismo v perexe ani titulku. preto poprosime opravit perex na normalne pismo a potom si mozes vratit clanok na titulku (ale v povodnom case zverejnenia - 14:22). dakujeme za pochopenie, pekny den admini " 
 Tak som si veru písmo upravila a podľa Tvojich pokynov ho vrátila na titulku. 
 Prišlo mi ešte aj P.S. 
 "PS este poprosime o odstranenie vulgarizmov v clanku, podrobnosti o nevyhnutnom pouzivani vulgarizmov najdes v kodexe (vlavo). dakujeme, admini" 
 Nuž reku, i tie som odstránila (boli len dva, ak sa nemýlim) 
 A zrazu mi prišla ešte jedna správa. 
 "zdravime, prosime, aby si si nikdy nevracala na titulku clanok, o ktorom spravcovia rozhodli, ze tam nepatri. dakujeme za pochopenie, admini " 
 Ktorej som akosi nebola schopná porozumieť, pretože predsa v prvej správe si mi jasne napísal, že si článok môžem na titulku vrátiť. Ja by som ho tam samovoľne nevracala (ako som pripomenula aj v správe, na ktorú si mi neodpovedal), keby to nebolo vyslovene spomenuté v tej správe. 
 Vieš, mne nevadí, že nemám článok na titulke. Teda. Možno i trochu áno, mám rada vysoké číslo pri čítanosti, ale viem to predýchať. Predsa len je blogovanie pre mňa oddychom od všetkých tých odborných textov, ktoré píšem do školy a nebudem sa kvôli oddychovej činnosti vytáčať do nepríčetnosti a trieskať si sklamane hlavu o stenu, keď nie je po mojom. 
 Ja som len človek, ktorého štve nedorozumenie. 
 Ja si ten článok naozaj na titulku vracať nemienim, ale prosím Ťa, už mi nikdy nespomínaj, že si na titulku môžem niečo vrátiť, keď mi potom otrieskaš o hlavu moju bezbrehú samovoľnú činnosť ignorujúcu pravidlá serveru. 
   
 Ďakujem a prajem Ti pekný deň. 

