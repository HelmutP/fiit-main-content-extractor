
 Svieže ráno
tráva v pozore
rosou si pripila 
na dnešný deň

brezové listy 
vytiahli mimikry
a v poznačení slnka
cvengá ich ligot 
ako zlaté dukáty 

Deň si berie do úst
zvony
a na zvonici zvoní
že chce pokoj a mier
žiadne výstrednosti
pod pás

Slnko je zlatý topás
a šteklí moje oči
až ich privieram

Taká bezstarostná nedeľa...

Ticho plávam poludním
aj vánok sa ukľudní

Je čas zapískať na zore
nech sa tlačia hore 
a večeru
dať svoj hlas
aby večernica
zasadla do parkov
a na lavičkách
tajne vypočula
milencov

v podvečerný čas...

*

 
