
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Piršč
                                        &gt;
                Nezaradené
                     
                 Sylvain 

        
            
                                    7.4.2010
            o
            9:54
                        (upravené
                29.2.2012
                o
                19:56)
                        |
            Karma článku:
                5.80
            |
            Prečítané 
            852-krát
                    
         
     
         
             

                 
                    Bol mladý intelektuál z Francúzka. Vek okolo tridsiatky - vysoký, štíhly, svetlovlasý. Jeho angličtina bola európska, spisovná, ľahko zrozumiteľná. V múzeu ľudovej architektúry v Humennom, kde som ho sprevádzal, už je tomu vyše desať rokov, mi bol hneď sympatický. Priamy pohľad, vyrovnaná myseľ.
                 

                     Sadli sme si v areáli múzea pod prístreškom s drevenými stolmi - debata bola plodná. Pochádzal z francúzskeho vidieka, ale už štyri roky býval sám na predmestí Paríža, kde si kúpil jednoizbový byt. Okrem Čiech a  Slovenska - na východné Slovensko pricestoval z Prahy nočným rýchlikom - plánoval pokračovať v poznávacom výlete ďalej do Poľska, Litvy a Lotyšska.   Po týchto úvodných, zoznamovacích informáciách pochválil som sa mu svojou znalosťou francúzskej literatúry: Verne, Hugo, Balzac, Dumas, Rousseau, Voltaire, de Montaigne... Sylvain po každom mene uznanlivo prikývol hlavou s úsmevom vnímavého pedagóga, ktorého prehnane horlivý žiak chce ohúriť svojimi po nociach nadobudnutými vedomosťami. Počúval ma sústredene, pritom neustále akoby o niečom rozmýšľal a nakoniec prerušil môj literárny rozbeh vetou: „A poznáš Louis-Ferdinand Céline?"   Napísal mi to meno na kúsok papiera, keď videl, že mám problémy s pochopením výslovnosti. Moje oči prešli tisíckami titulov rôznych kníh v rôznych kníhkupectvách a antikvariátoch a bol som si istý, že na takéhoto autora môj zrak dovtedy nikdy nenarazil. Také čudné mužsko-ženské meno by sa nedalo prehliadnuť. Sylvain podkopával moje vedomosti o francúzskej literatúre ďalej: „Bol to jeden z najvýznamnejších francúzskych románopiscov 20-tého storočia."   Keď zbadal moje rozpaky, zľahčil moju nevedomosť: „Nemohol si ho poznať, lebo Céline bol antikomunista a jeho knihy boli u vás určite počas komunistického režimu zakázané. Ale ty vieš po anglicky, keď prídem domov, pošlem ti preklad jeho najslávnejšieho románu, v angličtine sa volá: Journey to the end of the night."   Keď sme sa po hodinke rozlúčili, nebral som vážne jeho ochotu poslať mi tú knihu. Nedal som mu žiadne peniaze, poznali sme sa len krátko a bol som zvyknutý zo svojho okolia, že niečo sa náhodou v debate povie, prisľúbi, a život ide ďalej, šou pokračuje.   O pár dni som zabudol na jeho prísľub, ale na moje veľké prekvapenie o mesiac mi došla dobierka z Francúzska, z Paríža - vyše štyristostranový zmienený román s priloženým lístkom, na ktorom bolo rukou anglicky napísané: ´Many pleasant  reading hours! - Sylvain´   Takú veľkodušnosť som nemohol ignorovať. Poslal som mu na oplátku farebnú publikáciu o jednom z našich najoriginálnejších umelcov 20-tého storočia, ktorého Sylvain určite nepoznal, najvýznamnejšom predstaviteľovi slovenskej grafiky Albínovi Brunovskom.   Na internete som si potom vyhľadal, že Louis-Ferdinand Céline nebol len antikomunista, ale aj antisemita. Za svoje protižidovské postoje bol po skončení druhej svetovej vojny v neprítomnosti odsúdený k trestu smrti za kolaboráciu, ale v roku 1951 bol amnestovaný a vrátil sa z emigrácie z Dánska späť do Francúzska.   Keď som sa dozvedel tieto skutočnosti, zvažoval som, či sa mám vôbec púšťať do čítania tohto rozsiahleho románu, či mi to bude stáť za to, pretože sa jednalo o autora, ktorého meno bolo sprofanované vo vlastnej vlasti, a navyše, ja si pri čítaní anglických kníh neznáme slovíčka málokedy domýšľam, prácne si vyhľadávam ich význam v slovníku.   Začal som teda čítať román skepticky, s predsudkom, ale rýchly spád deja ma posúval ďalej a ďalej, a po prečítaní poslednej kapitoly som musel skonštatovať, že v tomto prípade sa oplatilo oddeliť umeleckú úroveň diela od neskorších autorových politických postojov.   Píšem to všetko kvôli tomu, lebo minulého roku vyšlo prvé vydanie tejto knihy aj v slovenčine s názvom: Cesta do hlbín noci. Kúpil som si ju z internetového kníhkupectva, ešte raz som si naplno vychutnal, teraz už v rodnom jazyku, Célineho brilantný štýl, nadnesenú iróniu, očarujúce metafory - aj vďaka vynikajúcemu prekladu Kataríny Bednárovej. Musím dať Sylvainovi za pravdu - Louis-Ferdinand Céline mal talent od Boha - ale nie od židovského.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Šéfova dcéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 2.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 1.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Papierová prilba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Silvester 1988
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Piršč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Piršč
            
         
        pirsc.blog.sme.sk (rss)
         
                                     
     
        Foto: 10.9.2012









 
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bieda a charakter
                     
                                                         
                       Kvetografie
                     
                                                         
                       Led Zeppelin - Celebration Day
                     
                                                         
                       Racheli
                     
                                                         
                       Krása sveta
                     
                                                         
                       O Slovensku, ktoré nik nechcel
                     
                                                         
                       Splynutie ticha
                     
                                                         
                       spomienkovy
                     
                                                         
                       Ďakujem Ti, Vierka
                     
                                                         
                       Emily Dickinsonová: Srdce chce najskôr slasť, potom - zomrieť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




