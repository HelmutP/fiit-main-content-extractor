
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eduard Chmelár
                                        &gt;
                Médiá
                     
                 Hovorme otvorene o korupcii v médiách 

        
            
                                    6.12.2008
            o
            19:57
                        |
            Karma článku:
                13.18
            |
            Prečítané 
            7803-krát
                    
         
     
         
             

                 
                         Keď som sa rozhodol zverejniť poznatky o korupcii v médiách, vedel som, že otváram Pandorinu skrinku a treba byť pripravený na všetko. Nemám na mysli výhražné e-maily, ktoré som dostal v priebehu včerajšieho dňa, lebo podobné psychopatické prejavy sa stali bežnou súčasťou môjho života, odkedy sa občiansky angažujem vo veciach verejných. Mám na mysli oveľa mocnejšie sily, ktoré majú záujem na tom, aby sa korupčnou kultúrou našich médií nikto nezaoberal. Tento jav má totiž oveľa závažnejšie rozmery, než by sa mohlo zdať zo zdanlivého pokoja, ktorý panuje v médiách.
                 

                  Už špecializovaná správa pre Transparency International Slovensko z roku 2001 upozorňuje, že vysporiadanie sa s korupciou v médiách bude patriť spolu so súdnictvom k tým najcitlivejším. Hoci už Národný program boja proti korupcii prijatý v roku 2000 upozorňoval na tento fenomén, odvtedy sme sa nepohli z miesta. Vieme, že v redakciách bežne pracujú novinári, ktorí sú k svojim článkom priamo alebo nepriamo motivovaní záujmovými skupinami. Vieme, že ekonomicky silní klienti si najímajú novinárov na poškodenie konkurencie.  Vieme, že majitelia médií nezriedka žiadajú svojich zamestnancov, aby spracovali materiál na objednávku klienta (inzerenta) spoločnosti. Vieme, že korupcia v médiách má tendenciu samú seba legitimizovať a legalizovať. Vieme o rozsahu tzv. korporatívnej cenzúry v médiách. Napriek tomu sa médiá tvária, akoby tento problém neexistoval a dôkazy bagatelizujú či zametajú pod koberec. Ako je však možné bojovať proti korupcii v iných oblastiach, ak sa novinári tvária, že ich sa to netýka?       Keďže sa s týmto fenoménom nevedia vysporiadať médiá samotné, chytil sa tejto témy celkom prirodzene premiér Fico. A chytil sa jej tým najnešťastnejším, účelovým spôsobom, ktorý otupuje hrot jeho kritiky. Ficov populizmus však nemôže byť výhovorkou – tento problém tu je. A je to čertovsky veľký problém. Registrujú ho všade, ale u nás je obzvlášť vážny. Najznámejším (zdôrazňujem – najznámejším, nie najväčším) prípadom korupcie médií u nás je smutne slávna lyžovačka v Alpách, ktorú zaplatila poisťovňa Allianz 11 slovenským novinárom zo Sme, Hospodárskych novín, Trendu, STV, TV Markízy a TA3, ktorí sa venujú dôchodkovej reforme. Desivé boli reakcie dotknutých, ktorí na akcii nevideli nič zlé. Ak prijmeme ich logiku, potom nie je nič zlé ani na návšteve ministra Počiatka na jachte J&amp;T v Monaku...       Jedným z dokázateľne účinných nástrojov na potláčanie korupcie v médiách je tvorba redakčných štatútov. Naši novinári však tento vlastný záujem pri nedávnom boji o podobu tlačového zákona vôbec nepresadzovali. Namiesto toho kopali za vydavateľov, akoby si neuvedomovali, že ich záujmy nie sú vždy totožné. V roku 2004 vypracovala OBSE tzv. Kódex riadenia, ktorý má zabezpečiť úplnú redakčnú nezávislosť. Hádajte, koľko vydavateľských firiem na Slovensku ho podpísalo. Ani jedna.       Je mi jasné, že boj proti korupčnej kultúre v médiách nebude jednoduchý a že dotknutá strana využije všetok svoj vplyv a moc, aby takúto verejnú diskusiu umlčala, zbagatelizovala, zosmiešnila či škandalizovala. Ale nie je mi jasné, aká je v celej záležitosti úloha mediálneho glosátora Gabriela Šípoša. Hneď na úvod pripomeniem, že sa snažím vyvarovať polemiky s týmto pánom z niekoľkých dôvodov. Po prvé, na základe jeho výstupov ho nepovažujem za celkom kompetentného v tejto oblasti. Pán Šípoš je uznávaný medzi novinármi, nie medzi mediálnymi odborníkmi. Pripúšťam, že občas sa mu podarí trafiť klinec po hlavičke, ale jeho metódy nemôže brať vážne nikto z odbornej branže. Na to nemá nielen potrebný nadhľad, ale zjavne ani vzdelanie. Viem, že toto moje konštatovanie emocionálne rozkýve množstvom jeho anonymných fanúšikov na tejto stránke, ale to je asi tak všetko a je mi to srdečne jedno. Druhou príčinou, prečo mi je táto polemika nepríjemná, je fakt, že kritický čitateľ, ktorý nečíta blogy pána Šípoša ako písmo sväté, si už isto všimol, že nezhody medzi nami dvoma začali nadobúdať osobný charakter. Podotýkam, že nie mojou vinou – tento pán mi venuje viac pozornosti ako aktívnym novinárom, čo hovorí za všetko. Aj mediálny kritik musí cítiť za svoju prácu zodpovednosť – o to väčšiu, ak ju vyžaduje od novinárov. Gabriel Šípoš už napísal množstvo nezmyslov a strápnil sa nejedným konštatovaním, a to nielen na moju adresu, no ani raz som nereagoval na tie bludy samostatným blogom. Ak to dnes robím, nevedie ma k tomu osobná márnomyseľnosť, ale principiálna pozícia v otázke korupcie médií, ktorú pán Šípoš z mne neznámych dôvodov bagatelizuje a demagogicky odvádza pozornosť od podstaty problému. Mám prirodzene na mysli jeho postoj k diskusnej relácii venovanej stavu slovenskej žurnalistiky (STV2, 4. decembra 2008).       Najprv k jeho výhradám voči zloženiu hostí: ani ja som nebol nadšený z takejto zostavy. Ak som do relácie prijal pozvanie, bolo to najmä preto, že Slovenská televízia bolo jediné relevantné médium, ktoré bolo ochotné vytvoriť priestor na diskusiu o korupcii medzi novinármi. Okrem toho som svojich oponentov v rozhlasových či televíznych diskusiách nikdy nekádroval. Nerobil som to ani v čase, keď som sa zúčastňoval na reláciách Pod lampou, kde som bol „ideologicky“ rovnako osamotený, ibaže v opačnom garde. Spomenie si ešte pán Šípoš, ako na mňa zvykli nasadzovať presilovku 3 + 1 (napríklad traja diskutujúci o terorizme – Ivo Samson, Roman Joch, Tomáš Zálešák plus ako štvrtý ešte angažovanejší moderátor)? Prečo sa vtedy neozval? Prečo mu vtedy nevadilo odborníkmi toľko kritizované jednostranné obsadzovanie relácií? Prečo ho nezaujímala objektívnosť STV v čase, keď sa verejnoprávna televízia absolútne nezaoberala inými alternatívami voči Miklošovým reformám, keď nedala nijaký priestor zásadným opozičným návrhom a namiesto toho zaraďovala dojímavé šoty o tom, ako Dzurindových ministrov trápili komáre? STV bola vo vleku vládnej moci tak za predchádzajúcej, ako aj za súčasnej koalície.       Pri ďalšej interpretácii obsahu diskusie však už pán Šípoš otvorene zavádzal, ak nemám povedať, že klamal. Vždy si dám dobrý pozor, čo označím za lož. V našom verejnom priestore totiž význam tohto slova zdevalvoval, pretože mnohí novinári i politici ho používajú veľmi účelovo, držiac sa hesla „spojenec sa mýli, protivník klame“. Ak však nemám označiť výplody pána Šípoša za číre klamstvo, musím začať veriť tomu, že bol pri formulovaní svojich výrokov mimoriadne nesústredený.       V prvom rade nikto tam nepovedal, že „naše médiá sú na tom na rozdiel od minulosti vo veľmi zlom stave, že za to môžu najmä súkromné médiá”. To je naozaj veľmi svojvoľná interpretácia jadra problému. Jednak sa tam nehovorilo o hocijakej minulosti, ale veľmi konkrétnej a špecifickej, a jednak sa kritika nesústreďovala iba na súkromné médiá, ale na mediálny systém ako celok. Rovnako nikto tam nekritizoval „pravicové“ médiá – toto je rýdzi ideologický komplex pána Šípoša a ja mu v tom asistovať nemienim. S tým súvisí aj jeho ďalšie „nedorozumenie“, že nikto z nás tam nekritizoval stav v STV. To nie je pravda. Minimálne pán Budaj mal ostré poznámky na jej adresu. Rovnako tak nie je pravda, že nikto nespomenul „čierne roky“ našej žurnalistiky v deväťdesiatych rokoch. Odhliadnuc od toho, že takéto označenie je odborne neadekvátne, pretože nezohľadňuje skutočnosť, že paradoxne pochmúrny politický vývoj priniesol niektoré novinárske tvorivé vrcholy – napríklad v podobe analýz Mariána Leška – musím pánovi Šípošovi osviežiť pamäť, lebo ak v tej chvíli nespal, musel zaregistrovať, že som venoval kritickú poznámku aj tomuto obdobiu. Napokon, nezaznievala tam nijaká bezkoncepčná kritika, sám som uviedol niekoľko pozitívnych príkladov kvalitnej žurnalistiky v súčasnosti, a to bez ideologického hľadiska, pričom sa pýtam, čím to je, že pán Šípoš takéhoto odborného nadhľadu nie je schopný. Je až úsmevné, ako si na otázku, ktoré médiá sú pre krajinu najužitočnejšie, sám odpovedal „bezkonkurenčne Sme“, najmä ak to dáme do súvislosti s výrokom Tomáša Bellu, že vraj Gabriel Šípoš je voči Sme ešte kritickejší ako k ostatným denníkom...       Postrehy o stave novinárstva v šesťdesiatych rokoch a dnes sú už naozaj iba ventilovaním osobných dojmov pána Šípoša, dokumentujúcim žalostný prehľad tohto glosátora v danej oblasti. Každý priemerný študent novinárstva by ho vedel zahanbiť v sume poznatkov o tomto období, ktorá sa nedá získať tým, že si prelistujete niekoľko čísel Smeny a svoju pozornosť sústreďujete na formálne znaky. Použiť ako argument, že „teraz je kvalitnejší papier, existuje farebná tlač, možnosť používať infografiky či skrátiť čas od uzávierky po tlač na minimum”, ako to sformuloval v inom blogu, je nesmierne hlúpe. Je to ako keby sme chceli poprieť najslávnejšiu éru britských novín The Times v 19. storočí poukázaním na dnešný spôsob ich výroby. Sila publicistiky, pán kolega, nie je v rotačke, ale v ľudskom duchu. Nemáte potuchy o tom, čo v danej dobe zosobňovali mená ako Roman Kaliský, Gavril Gryzlov či Ladislav Bielik, nemáte potuchy o dobovom kontexte vývoja, v ktorom sa naša žurnalistika po prvý raz a naposledy dotkla svetovej špičky. Napísať, že vtedajšia novinárska generácia „nesiaha z pohľadu žurnalistiky súčasným serióznym denníkom ani po členky” je čosi tak tupé, že to môže vysloviť iba hochštapler. Nebrať do úvahy, že sme mali reportérov, ktorých oceňoval Sartre i Solženicyn, že tu pracovali novinári, ktorí pretavili vernosť ideálom žurnalizmu do osobného života či dokonca smrti, že táto doba vyprodukovala jediného Slováka, ktorý získal World Press Photo, a namiesto nich vyzdvihovať Schutza, Baťa a Kordu a tvrdiť, že „novinárčina u nás nikdy nebola lepšie robená ako dnes“ - to chce mimoriadnu dávku kultúrnej ignorancie. A zbaští mu to skutočne len tunajšia anonymná úderka (asi preto som pána Šípoša ešte nikdy nevidel na žiadnej vedeckej konferencii venovanej médiám).       Dalo by sa nad tým mávnuť rukou, keby sa pán Šípoš nepustil do oveľa závažnejšieho zavádzania – do bagatelizovania korupcie médií na Slovensku a zámerného prekrúcania faktov (ak len nemám uveriť tomu, že náš glosátor trpí dyslexiou). To, ako dokázal dezinterpretovať jeden renomovaný výskum, je priam neuveriteľné. Reč je o známom celosvetovom výskume korupcie v médiách, ktorý vykonal tím pod vedením profesora Kruckeberga z Katedry komunikačných štúdií na Univerzite v Severnej Iowe. Práve o ňom som informoval divákov STV, pretože z neho vyplývajú mimoriadne alarmujúce fakty pre našu krajinu a nijaké relevantné médium o ňom neinformovalo (ak nechcete za informovanie považovať márnomyseľnú Šípošovu poznámku, že aj on o tom písal – niekoľko riadkov na blogu – a škandalózne skresľujúcu interpretáciu Národnej obrody). Slovenské médiá patria medzi najskorumpovanejšie v Európe a rovnako pluralita médií je horšia ako v Rusku (pravda, na to vari ani nepotrebujeme výskumy, vidíme to sami). Reakcia Gabriela Šípoša sa pomaly stáva predvídateľnou: obvinil ma z toho, že preháňam a používam klamstvá. Uveďme teda fakty (posúdenie, kto má pravdu, nechávam na čitateľa):       Pán Šípoš tvrdí, že výskum bol postavený na 8 kritériách a nie na viac ako stovke (presnejšie 105), ako som uviedol ja. Nuž, náš mediálny glosátor si evidentne mýli 105 otázok (kritérií) s 8 faktormi, ktoré z týchto kritérií vyplynuli. Mám vôbec podozrenie, že držal v ruke tlačovú správu zverejnenú na internete a nie „štúdiu“, podrobné výsledky výskumu. Ani slovná ekvilibristika mu pri pohľade na tieto výsledky nepomôže zamlčať fakt, že Slovensko patrí skutočne medzi krajiny s najvyšším stupňom korupcie médií v celej Európe. To, že meradlom plurality nebol obsah, ale pomer obyvateľstva a počtu titulov mienkotvornej tlače, si pán Šípoš jednoducho vymyslel – nad toľkou demagógiou zostáva až rozum stáť. Rovnako svojvoľná je jeho interpretácia, že výskum sa týkal iba pravdepodobnosti, nie skutočnej existencie korupcie (sic!). To je lož ako vyšitá a len Gabriel Šípoš vie, prečo to robí, ak profesor Kruckeberg niekoľkokrát vyjadruje znepokojenie nad „mierou takto preukázaného uplatkárstva v médiách“. Nenašiel som nikde upresnenie, čoho sa má týkať chýbajúca protikorupčná legislatíva – ja som to pochopil tak, že priamo médií – ale nevylučujem, že v tomto prípade môže ísť o celú spoločnosť, ako tvrdí pán Šípoš – ale mení to niečo na podstate problému? Nula ostáva nulou. Kto tu teda zavádza?       Som čestný človek a vyprosím si, aby o mne vynášal prvoplánové súdy niekto, kto nezvláda rutinné odborné postupy pri mediálnej analýze, kto zneužíva mediálny priestor na vybavovanie si osobných účto, kto triafa od boka a čaká, čo to urobí. Pán Šípoš sa nespráva ako mediálny watch dog (strážny pes), ale skôr ako guard dog (služobný pes), ktorý je vycvičený, skrotený, takže niečo prehliada a na niečo útočí. Ak subjektívne napomína novinárov za drobné prehrešky a skutočné problémy v médiách zametá pod koberec, pýtam sa, komu slúži takáto kritika odvádzajúca pozornosť od najzávažnejších vecí.       Významný český novinár Karel Hvížďala nazval súčasnú situáciu na mediálnej scéne „zamatovou normalizáciou“. Opäť sa nám totiž vracajú časy, keď má veľká časť spoločnosti úplne iný názor na politiku a súčasný život, než občanom prezentujú médiá. Res publica sa zmenila na public relations. Ak do tohto diskurzu vstúpi niekto, kto tvrdí, že súčasná žurnalistika je tou najlepšou a že kvalita informácií sa zlepšuje, a médiá si ho za túto vľúdnosť s láskou pestujú ako dvorného analytika (pričom nevedia rozlišovať medzi analyzovaním a glosovaním), je to poriadne, ale poriadne trápna fraška. Pán Šípoš je na najlepšej ceste, aby sa jeho blogy stali rovnako patologickou, okrajovou záležitosťou, ako anonymné diskusné príspevky k nim. Nebudem strácať čas s postavičkou, ktorá niekoľkoročný výskum a medzinárodne uznávanú metódu jedného z najrešpektovanejších mediálnych sociológov súčasnosti Deana Kruckeberga odpáli jedinou vetičkou, že by to „bral s rezervou“.       Čomu však rozhodne budem venovať svoju energiu, je očista mediálneho prostredia. Klasický žurnalizmus je poctivé remeslo, ktoré dnes stráca svoju tvár. Stráca ju vďaka tomu, že z novinárov sa stala márnomyseľná sociálna skupina neschopná sebareflexie. Práve preto, že žurnalisti patria medzi ten typ povolaní, ktoré sú hodnotené (povedané slovami Maxa Webera) podľa tých predstaviteľov, ktorí stoja eticky najnižšie. Korupcia médií je už niekoľko rokov jedným z najzávažnejších problémov tejto spoločnosti. Čím viac ju novinári spochybňujú, tým viac ju bude súčasný premiér zdôrazňovať – to je v jeho stratégii logické. Neviem, o čo sa Robert Fico vo svojom dnešnom prejave opieral, ale ak naozaj o moje vystúpenie v STV, ako to tvrdí Sme, potom ho zinterpretoval dosť svojsky. A vôbec ma neteší, že moje výroky účelovo zneužíva človek, ktorý mi donedávna nevedel prísť na meno. Ale to neznamená, že budem korupciu v médiách hanebne spochybňovať, ako to robí Gabriel Šípoš. Verte mi, že to nie je ľahká pozícia, keď proti Vám stoja mediálne reťazce a na druhej strane sa usilujete upozorňovať na tento závažný spoločenský problém bez politického krytia. A nechcem to dramatizovať, ale nie je mi všetko jedno, ak som dostal výhražný mail s otázkou, či to mám so synom všetko pod kontrolou, pričom syna vychovávam sám a v tej chvíli bol sám doma. Ale kto ma pozná, vie, že vyhrážky na mňa nikdy neplatili. A čestných novinárov vyzývam, aby sa nebáli o týchto veciach hovoriť. Tak, ako sa museli nájsť statoční sudcovia, ktorí prehovorili o korupcii v justícii, musia sa nájsť statoční žurnalisti, ktorí očistia tento stav. Falošná stavovská solidarita v konečnom dôsledku nikomu nepomôže. Parafrázujúc Aristotela dodávam na záver, že novinár je priateľ, ale väčším priateľom je pravda.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (261)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eduard Chmelár 
                                        
                                            Prečo to tu nemá zmysel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eduard Chmelár 
                                        
                                            Fakty, pred ktorými neujde ani zaujatý bloger
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eduard Chmelár 
                                        
                                            Majú Slováci monarchistické sklony?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eduard Chmelár 
                                        
                                            Radšej s moslimom ako s ateistom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eduard Chmelár 
                                        
                                            Diskriminácia a predsudky voči ateistom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eduard Chmelár
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eduard Chmelár
            
         
        chmelar.blog.sme.sk (rss)
         
                                     
     
        Jediný režim, voči ktorému nemám výhrady, je pitný.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    78
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5608
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zbližovanie ľudstva
                        
                     
                                     
                        
                            Kultúra mieru a nenásilia
                        
                     
                                     
                        
                            Udržateľná spoločnosť
                        
                     
                                     
                        
                            Kritika politiky
                        
                     
                                     
                        
                            Médiá
                        
                     
                                     
                        
                            Rúcanie mýtov
                        
                     
                                     
                        
                            Pro Slovakia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            USA s ľudskou tvárou
                                     
                                                                             
                                            Diskusia o islame
                                     
                                                                             
                                            Odklínanie krajiny (19)
                                     
                                                                             
                                            Abnormálna normalizácia (18)
                                     
                                                                             
                                            Otcovia česko-slovenskej federácie (17)
                                     
                                                                             
                                            Podoby komunistického teroru na Slovensku (16)
                                     
                                                                             
                                            Prevrat v réžii KSS (15)
                                     
                                                                             
                                            Sloboda vzácnejšia ako samostatnosť (14)
                                     
                                                                             
                                            „...za šťastie naše i šťastie ľudstva“ (13)
                                     
                                                                             
                                            Nie je štát ako štát (12)
                                     
                                                                             
                                            Na ceste k totalite (11)
                                     
                                                                             
                                            Boj za autonómiu (10)
                                     
                                                                             
                                            Budovanie republiky a počiatky populizmu (9)
                                     
                                                                             
                                            Vojna o Slovensko (8)
                                     
                                                                             
                                            Slovenský podiel na vzniku republiky (7)
                                     
                                                                             
                                            Začiatky čechoslovakizmu  (6)
                                     
                                                                             
                                            Rozkol v národnom hnutí (5)
                                     
                                                                             
                                            Na prahu kultúrnej genocídy (4)
                                     
                                                                             
                                            Od slovenského veľkokniežatstva k Okoliu (3)
                                     
                                                                             
                                            Zrodenie národa (2)
                                     
                                                                             
                                            Mýty, legendy a kontinuita (1)
                                     
                                                                             
                                            Otvorený list predstaviteľom Smeru
                                     
                                                                             
                                            Od historického víťazstva k historickej hanbe
                                     
                                                                             
                                            Prečo sa Európa nemôže spoliehať na NATO
                                     
                                                                             
                                            Posledné roky Iraku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sem píšem o médiách
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Changenet
                                     
                                                                             
                                            Britské listy
                                     
                                                                             
                                            Je to tak
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




