

 Čo je permakultúra je ťažká
otázka. Je to podobné, ako keby ste chceli jednoduchú
odpoveď na otázku - čo je život.
 
 
Krátky
pohľad do histórie 
 
 
Svojho času lesnícky inžinier
Bill Molison pracoval pri ťažbe dreva v austrálskych
dažďových pralesoch. Bolo to este v časoch, ked sa
v pralesoch ťažilo pre drevo, nelikvidovali, nepálili
sa pralesy pre získanie poľnohospodárskej pôdy.
Prales vzápätí dorástol a vyzeral rovnako ako pred
ťažbou...
 
 
Molisona zaujala táto obrovská regeneračná
schopnosť a rozbehol výskumnú prácu
ohľadom tejto vitálnej schopnosti pralesa. Poznatkov pribúdalo a všetko nasvedčovalo
tomu, že je v pralese systém, ktorý
umožňuje túto obrovskú životaschopnosť... Pohrával sa
s myšlienkou, či sme schopní zopakovať vzory
z prírodného pralesa a vysadiť nový..
 
 
A práve vtedy Mollisonovi
napadla geniálna myšlienka vysadiť takýto prales, ale
v kombinácii s (pre ľudí) užitočnými
rastlinami..
 
 
Takže asi tušíte, čo
nasledovalo.. Keď sa mu po čase podarilo skombinovať vegetáciu
do jedlého pralesa.. nazval to po anglicky Permanent
agriculture, a ujala sa skratka „permaculture“.
 
 
Dnes je po svete možno niekoľko
miliónov nadšencov, ktorí experimentujú s týmto
princípom divočiny na zjedenie... Neustále
pribúdajú nove poznatky, pretože príroda má
na každom kúsku planéty svoje špecifiká a je
tu tiež nový prvok pre ňu - teda človek s kreatívnosťou
ktorého prales nepočítal. 
 
 
Odkiaľ brať vzory, ak na
Slovensku nemáme dažďový prales? 
 
 
Určite ste počuli, že niekto ma na
záhrade džungľu, ktorá väčšinou vznikla tak,
že majiteľ nechal pozemok napospas prírode...
 
 
 
 
 
Väčšinou už takúto
džungľu ani nepolieva, nehnojí, nepostrekuje... a prírode
sa darí.
 
 
Ak sa nebudeme ohliadať na estetické
kritériá (pretože to je veľmi individuálna - ľudská
miera),  v podstate by sme mohli uvažovať o implantácii
rôznych užitočných rastlín do takejto
džungle... Ideálne je nasadiť, čo najviac trvácnych
rastlín, krov, stromov ... a potom už len zberať úrodu. 
 
 
Áno to je pekná
predstava, záhradkári to už robia.. a predsa
ešte nejaká práca okolo toho navyše je... niekedy až
kríže pobolievajú.. 
 
 
Stáva sa.. To je pravé to
„estetické“ kritérium. Že by napr. záhradkárovi
pasovalo vysadiť šalát na rovnaký kúsoček,
kde sa darí aj slimákom. Môžeme si predstaviť, že
slimáky budú mať iný vzťah ku šalátu ako
záhradkár... Naopak ak záhradkár vysadí
šalát tam, kde kraľuje ježko, ktorý si zgustne na
slimákoch, je šanca, že na šaláte si pochutná aj
majiteľ pozemku. 
 
 
A to sú tie „výskumy“
permakulturistov po celom svete - nájsť tie najlepšie
kombinácie a kompromisy, aby príroda pracovala aj
pre nás a namiesto nás...
 
 
Pokiaľ sa nájdu zákonitosti,
ktoré fungujú všeobecne ..tak sa postupne zaradzujú
do databázy permakultury...  Je to teda otvorená kniha,
kde každý môže prispievať..
 
 
Sú však princípy, ktoré
fungujú nezávislé na klimatických
podmienkach.. 
 
 
Princípy permakultúry 
 
 
Napríklad, že nebojujeme, ale
spolupracujeme s prírodou..
 
 
Že v prírode nie sú
odpady, len prvky jedlého – potravinového reťazca,
kde každý prvok, alebo jeho výlučok slúži ako potrava
iného.
 
 
Že neznásilňujeme prírodu,
aby rešpektovala naše „hriadky“, ale kopírujeme skôr
prírodné vzory..
 
 
Využívame poznatky o symbióze
rastlín a živočíchov, plesní a baktérií,
aby sme posilnili vitálne aktivity v pôde.
 
 
Že rešpektujeme výškové
požiadavky rastlín, zónovanie...
 
 
Napríklad, ak máme vlhkomilné
rastliny, asi by sme sa nanosili krhly vody, aby sme ich počas
suchého leta uspokojili.., preto urobíme na pozemku rybníček,
na brehu ktorého vysadíme takéto rastlinky.
 
 
Môžeme vysadiť možno menej
úrodné divoké rajčiny, ale neriskujeme pohromu
spôsobenú zemiakovou plesňou ...
 
 
Vysadíme ovocné stromy odolné
proti chorobám, možno plody nebudú mať banánovú
príchuť, ale nebudeme musieť striekať chémiou, a čo
sa týka vitamínov, tak  sa určíte neokabátime.
 
 
Odmietanie intenzívnej chémie
je ďalšou zásadou. Príroda nemá skúsenosti
s umelými látkami a veľmi ťažko sa vysporadúva
s veľkými dávkami chémie. Preto radšej
používame výluhy z prírodných
materiálov, ktoré vie spracovať..
 
 
Samozrejme, ak by sme mali pozemok
ohrozený kobylkami, použijeme v krajnom prípade
aj syntetické prostriedky. Dnes už existuje aj rozsiahly
výskum okolo biologickej ochrany rastlín, takže
prírode môžme pomôcť k rýchlejšiemu
nastoleniu rovnováhy.
 
 
Ďalším princípom je, že
nenechávame pôdu obnaženú (opäť kopírujeme
prírodný vzor), ale nastielame priestor medzi jedlými
plodinami mulčom (teda biologickým materiálom slamou,
senom, kompostom, zelenou hmotou, prírodnými látkami),
ktoré nielen udržujú vlhkosť pôdy, ale zároveň
bránia rastu nežiadúcich burín a hlavne sú
priestorom pre život baktérií, ktoré zlepšujú
vitalitu pôdy, zvyšujú množstvo humusu v nej a 
schopnosť zadržiavať viacej vlahy.
 
 
  
 
 
Pochopiť vodný režim
na pozemku patrí k najdôležitejším princípom
permakulturnej „vedy“.
 
 
Ešte k tomu mulču...
 
 
  
 
 
Tým, že v ňom vytvoríme
domčeky pre chrobáčiky, pripravujeme sa na účinnú
pomoc pri eliminovaní vošiek a ďalších škodcov,
ktorí by mohli byť kalamitou pre našu záhradku.
Niektorí permakulturisti si  pestuju lienky a pod., tak
že nechávajú rôzne dnom obrátené
hrnce, kde sa môžu schovať.
 
 
 
 
 
Búdky pre vtákov sú
dnes už bežnou praktikou aj pre ekologických záhradkárov.. 
 
 
Budúcnosť permakultúry na Slovensku
 
 
Aj u nás narastá
počet ľudí, ktorí sa snažia zdravo stravovať a nie
každý je pritom záhradkár.
 
 
Mnohí si kupujú
biopotraviny, takže aj u nás už ekologickí
farmári začínajú profitovať z toho, že
využívajú prvky permakultúry pri pestovaní vo
veľkom. Na západe je to skôr životný štýl,
že sa rodiny vyberú na víkend, či dovolenku stráviť
na takejto permakulturnej farme, záhrade..
 
 
S podobnými
ľuďmi, ktorým nie je ľahostajný osud planéty
a tiež chcú prispieť k trvaloudržateľnosti..
 
 
Je to veľká šanca pre náš
vidiek, kde v spojení s vidieckou turistikou určite
hostia ocenia možnosť natrhať si plody z permakulturnej záhrady 
 
 
Pramene informácií 
 
 
Ak si do vyhľadávača dáte
„permaculture“, zistíte, že informácie do konca života
neprečítate..
 
 
Sú už preložené
niektoré materiály do Česko - slovenčiny..., ale tie
sú skôr pre ľudí, ktorí absolvovali už
nejaký úvodný kurz , alebo strávili
nejaký čas na takomto pozemku.
 
 
Vlani vyšla kniha Heleny Vlašinovej ,
kde sú prístupnou formou zhrnuté jej skúsenosti,
ktoré prednáša študentom na Mendelevovej poľnohospodárskej a lesníckej univerzite v Brne,
resp.  záhradkárom.
 
 
Kniha ma názov „Zdravá záhrada“, čo - myslím si - je aj čiastočná
odpoveď na úvodnú otázku - o čom je
permakultura.
 
 
 
 
 
Nabudúce: Ako svojim egoizmom pomôcť k záchrane sveta.
 
 
 
 
 
Foto: archív Asociácie permakultúry 
 
 
 
 

