

   
 Keďže Slovensko je súčasťou Eurozóny tento problém sa týka aj nás. Kým vo väčšine štátov je tento problém vedený iba v odbornej rovine na Slovensku je pomoc Grécku z politizovaná v dôsledku blížiacich sa parlamentných volieb. 
 Predovšetkým dve opozičné pravicové strany sa snažia získať v predvolebnom boji body pre seba. Nemyslia pri tom na Slovensko, že môžu takýmito názormi relevantných politických strán stratiť na svojej reputácii. Slovensko tvorí necelé 1 % z celkového hospodárstva Eurozóny. V prípade, že sa drvivá väčšina krajín Eurozóny dohodne na spoločnom postupe Slovensko zo svojim 1 % hospodárstva nemôže ísť proti prúdu. 
 Hlavne posledné verejné vystúpenia Mikloša a Sulíka ohľadne Grécka nemožno nazvať inak ako o predvolebnej agitácii. Premiér Róbert Fico  verejne vyhlásil, že o poskytnutí pôžičky musí rozhodnúť parlament a keďže sa Slovensko nachádza mesiac pred parlamentnými voľbami musí toto rozhodnutie prijať nový parlament. Hlavnú podmienku, ktorú premiér stanovil je prísna rozpočtová politika Grécka. 
 Mikloš a Sulík si nevšimli alebo si nechcú všimnúť tieto podstatné fakty. Ide im len o to aby mohli očierňovať premiéra a tým aj Slovensko resp. vydierať ako to bolo pri Lisabonskej zmluve. Ich počínanie nemožno nazvať inak ako farizejstvo alebo čistý populizmus. 
 Ďalším podstatným faktom je, že poskytnutie pôžičky je spojené rozpočtovými škrtmi. Po posledných udalostiach čo sa odohrali v Grécku pochybujem o tom, že budú schopný zoškrtať svoje výdavky. Takže k poskytnutiu pôžičky možno ani nepríde. 
   

