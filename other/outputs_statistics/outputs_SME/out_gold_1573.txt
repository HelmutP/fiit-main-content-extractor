



 

 
 Europoslankyňa Monika Beňová si uctila pamiatku Jána Pavla II. na začiatku aprílového zasadnutia Európskeho parlamentu v tričku s portrétom Marilyn.	FOTO - TASR/AFP 
 



 Minútou ticha, po ktorej nasledoval potlesk poslancov, si v pondelok uctil Európsky parlament v Štrasburgu pamiatku zosnulého Jána Pavla II. Viac ako slová predseda europarlamentu Josepa Borrella o  pápežovi však zahraničných novinárov zaujímalo oblečenie slovenskej europoslankyne Moniky Beňovej. 
 Tá si na pietnu chvíľu obliekla tričko s veľkým portrétom Marilyn Monroe na hrudi. Oblečenie bolo na danú príležitosť také neštandardné, že sa Beňová po prvý raz ocitla ako europoslankyňa na fotografiách v databáze  zahraničných agentúr. 
 Diana Dubovská, slovenská poslankyňa a zároveň podpredsedníčka výboru pre vedu a technológie Parlamentného zhromaždenia NATO, zhodnotila podobný výber oblečenia za nevhodný - a to aj podľa protokolu. 
 "Každý, kto reprezentuje Slovensko v zahraničí, by sa ho mal naučiť. Protokol nepustí. Hoci, mali sme aj  horšie prípady - keď sa jeden kolega vtedajšieho SDK vybral na prijatie v NATO vo svetlých ponožkách a sandáloch," povedala. 
 Módna návrhárka Iveta Ledererová, ktorá radí v oblečení politikom, to zhodnotila: "Monika Beňová je atraktívna žena, ale je tam malý problém - občas sa stane, že si zmýli podujatia, na ktoré sa oblieka. Tento  typ oblečenia rozhodne nie je typom, ktorý sa má nosiť na danú chvíľu, ale ani do zamestnania, čo vykonáva." 
 Oblečenie, ktoré si Beňová vybrala, sa hodí do umelecky ladených a zábavnejších typov zamestnaní, alebo na voľnočasové podujatie, alebo si ho možno obliecť v rámci recesie. 
 Ledererová: "Ak však nešlo o  zábavnú udalosť či akciu Európskeho parlamentu, určite bolo to oblečenie nevhodné. Myslím si, že okolitých ľudí, jej kolegov, to oblečenie mohlo aj trochu urážať." 
 Monika Beňová na margo kritiky však len rozčúlene reagovala: "Ja sa nestarám do toho, kto čo nosí oblečené. Bola by som rada, keby sa konečne prestali  starať do toho, čo nosím oblečené ja." Podľa nej bolo tričko s portrétom Marilyn v súlade s danou situáciou, aj s protokolom. 
 Štandardné sa to zrejme nezdalo zahraničnej agentúre AFP, ktorá zaradila fotografiu europoslankyne v tričku s portrétom Marilyn do databázy. V nej ju navyše označila za slovinskú političku. Môžeme byť tentoraz radi,  že si nás opäť zmýlili so Slovinskom? 
 Dubovská: "Čo sa týka mýlenia si našich dvoch krajín, je mi to veľmi ľúto. V zahraničí za to bojujeme, vždy svojich kolegov z iných krajín opravujeme. Je to veľká hanba a už to trvá dlho. Teraz za taký omyl môžeme byť v princípe radi, ale radšej by som bola, keby sa už nikto v  tejto veci nemýlil." 
 

