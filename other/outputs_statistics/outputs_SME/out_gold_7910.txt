

   
   
 Právo voliť je predovšetkým o  veľkej zodpovednosti a podľa môjho názoru by mali byť voľby povinné pre každého,  aby nerozhodla za všetkých len tá časť obyvateľstva, ktorá sa k voľbám dostaví a  výsledky tak závisia hlavne od množstva a štruktúry voličov. Ak príde voliť málo  ľudí, môže to dopadnúť všelijako a ja nechápem, ako môžu byť ľudia takí apatickí  a ako nám v takýchto ťažkých časoch môže hroziť najnižšia účasť spomedzi  všetkých doterajších parlamentných volieb. Aj dve-tri tisíc hlasov voličov môže  rozhodnúť o tom, aká bude nakoniec vládna koalícia, pretože dosť bude závisieť  aj od toho, koľko strán sa do parlamentu dostane a práve tých pár tisíc hlasov  môže pomôcť nejakej teraz slabšej strane sa do parlamentu  dostať. 
 Iste sú mnohí občania znechutení z  našej politiky a myslia si, že ich hlas nie je dôležitý, že je jedno kto bude  zvolený a aj tak sú "všetci politici rovnakí". Ja som presvedčený, že medzi  kandidujúcimi stranami, ktoré majú reálnu možnosť dostať sa do parlamentu, sú  obrovské rozdiely a štýl aj spôsob vedenia ich politiky sú diametrálne odlišné.  Buďte preto, prosím, zodpovední, zúčastnite sa volieb a využite svoje volebné  právo. A pri Vašej voľbe dobre porozmýšľajte, komu dáte svoj hlas, zamyslite sa  nad jednotlivými sľubmi a programom strán a hlavne si spomeňte, ako sa správala  tá strana v parlamente v minulosti, keď bola pri moci alebo pri nových stranách  pozrite na ich kandidátov a na ich predchádzajúcu  prácu. 
 Budúcu sobotu teda treba ísť  jednoznačne voliť - práve v čase krízy je veľmi dôležité, akým smerom sa bude  krajina uberať a akým spôsobom sa bude "gazdovať" s našimi peniazmi (peniaze  štátu sú peniaze nás všetkých a platíme ich vo forme daní, odvodov a všetkých  možných poplatkov) - či si zvolíme pokračovanie súčasného štýlu vládnutia  budovania sociálneho štátu na úkor ďalšieho zadlžovania alebo sa prikloníme skôr  k stranám s úspornejšími opatreniami a začne sa šetriť. Pre inšpiráciu sa stačí pozrieť na susednú Českú  Republiku, akú cestu si  vybrali. 
 Ľucia, ktorí nepôjdú voliť nemajú  podľa mňa v budúcnosti morálne právo sťažovať sa na zvolených ľudí v parlamente  a nadávať na politikov, "ako všetci kradnú", keďže oni voľby odignorovali a bolo  im jedno, kto sa do parlamentu  dostane. 
 A ešte malá rada na záver - voľte  toho, komu by ste zverili do rúk svoje vlastné peniaze a poverili ho ich  spravovaním (ako efektívne a hospodárne nakladali jednotlivé strany s peniazmi  nech každý zhodnotí  sám). 
 Ja voliť určite pôjdem a dúfam že  aj Vy. 
   
   

