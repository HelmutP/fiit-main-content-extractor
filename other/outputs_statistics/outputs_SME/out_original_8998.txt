
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Kotian
                                        &gt;
                Nezaradené
                     
                 Okom futbalového laika I. 

        
            
                                    16.6.2010
            o
            20:06
                        (upravené
                16.6.2010
                o
                20:17)
                        |
            Karma článku:
                5.29
            |
            Prečítané 
            1105-krát
                    
         
     
         
             

                 
                    Po bitke je každý kaprál generálom - a niet nikoho, kto by robil nástenky na izbe revolučných tradícií. A tak si budem všímať iba pár zaujímavostí, ktoré snáď neunikajú fajnšmekrom.
                 

                     O tom, prečo sme dostali gól v poslednej minúte nadstaveného času, písali už všetci a mne sa to nechce opakovať. Nikto si však doteraz nevšimol kľúčovú rolu rozhodcu pri tomto góle (meno Damon zrejme nebolo celkom náhodné). Keby nadstavil iba dve minúty, boli by sme vyhrali a nebolo by šťastnejšieho národa na zemeguli. Takto túto nám predurčenú rolu prevzali Novozélanďania.   Hoci je dôležité, ako sme onen gól dostali, prinajmenej rovnako dôležité je, ako sme ten premiérový samostatný gól dali (lebo federálnych slovenských bolo viac - ja si pamätám napríklad Petrášove na MS 1970 - lebo nás otec ráno zobudil a my šarvanci sme pozerali, ako sa Laco Petráš po góle Brazílii v rohu šestnástky prežehnal). A tu načim poznamenať, že to bol gól z ofsajdu, čo ukázal aj opakovaný záber s naznačenou linkou súperovho obrancu a našich dvoch útočníkov,ktorí boli k súperovej bránke bližšie. To sa však v slovenských novinách, teda v texte slovenského novinára (čítal som iba Pravdu a Sme, možno to inde bolo) nedočítate. Zato Bildu a Kickeru to neuniklo.   Dôvodov, prečo tak dlho odolávali Severní Kórejčania platonickému útočeniu Brazílčanov, nájdu odborníci na futbal kopec, aj dva. Podľa mňa to bolo najmä preto, lebo severokórejským futbalistom nikto nepovedal, akí sú Brazílčania dobrí, a že by sa ich mali obávať, preto si proti nim toľko trúfali. A v severokórejských novinách - priznávam, nemám v tom prehľad - sa asi dosť ťažko dočítate, že niektorý iný národ môže mať lepších futbalistov ako Kim čong il.   Drsný však bol záver - zvyčajne si po zápase hráči podávajú ruky a vymieňajú dresy - v tomto po zápase ostali v strede ihriska dve nekompatibilné a nekomunikujúce skupinky - akoby bola jedna malomocná. Čo (by) som od brazílskych osobností neočakával.   Opäť, po ikstý raz  prihrávka k autovej čiare, jeden španielsky útočník/záložník/obranca sa pohráva s loptou a súperovými obrancami a po niekoľkých nevyhnutných kľučkách centruje do švajčiarskej šestnástky, kde sú všetci ostatní spoluhráči, pretože ani jednému počas celého zápasu nepríde na um, že si možno loptu aj naraziť a dostať súpera do úzkych - a v šestnástke si s týmito stereotypnými loptami súper na 99,5 percenta poradí. A toto sa opakuje po celý zápas - s výnimkou švajčiarskych protiútokov a excelentnej strely Xabiho Alonsa do brvna, čo bolo jediným prekvapujúcim momentom v španielskych dresoch. No dobre, ešte to, že Vicente del Bosque už po 60 minútach zistil, že by nemal stavať mužstvo iba na hráčoch domácej ligy a predsa len poslal na trávnik Fernanda Torresa (na nominovanie Cesca Fabregasa ešte zrejme nedozrel čas).     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            7 dôvodov, prečo sme skončili druhí, alebo okom hokejového laika 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Aký veľký je prúser Richarda Sulíka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Súd s Tomom Nicholsonom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Kedy ak nie teraz?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Gorily verzus ovce
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Kotian
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Kotian
            
         
        robertkotian.blog.sme.sk (rss)
         
                        VIP
                             
     
        som novinár na voľnej nohe - odkedy som odišiel zo Sme.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2633
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




