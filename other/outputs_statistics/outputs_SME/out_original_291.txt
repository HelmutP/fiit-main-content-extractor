
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Kohutiarová
                                        &gt;
                Súkromné
                     
                 Ako sa volá najhlbšie slovenské jazero? 

        
            
                                    13.11.2007
            o
            21:04
                        |
            Karma článku:
                11.36
            |
            Prečítané 
            5466-krát
                    
         
     
         
             

                 
                    To bola otázka z pokafranej písomky mojej dcéry, štvrtáčky základnej školy. Ale vďaka tomu som sa všeličo dozvedela.
                 

                V prvom rade to, že Maďarsko sa premiestnilo. Tí, ktorí ho prosím budete hľadať na mape smerom dolu od Bratislavy, nečudujte sa, ak tam nebude. Podľa štvrtákov vrátane mojej dcéry šlo na západ.  Možno v tom bude mať prsty špecifický názov nášho sídliska. Ono chúďatko periférne leží síce smerom na západ, ale keďže ho stavali za čias súdruhov, nemohlo sa tak volať. A tak sa volá... Sever. Vyzerá to tak, že táto pomätenosť sa preniesla aj na deti. Alebo žeby to bolo niečím iným?   Trnava sa v písomke nechala oblafnúť Bratislavou, ktorá jej tvrdila, že jedno číslo v jej dobových záznamoch o vzniku je zle čitateľné a je teda zákonite najstarším mestom našej milovanej zeme. To viete, ženské a rafinované záležitosti... Ako sa v tom majú chudáci štvrtáci vyznať?   Najväčšia pikoška to bola s tým jazerom. Viktorkina odpoveď bola zjavne na prvom písmene doškrtaná a poprepisovaná.   "Čo to vlastne tam máš napísané?"   "No Vincovo pleso.", pomrvila sa mi dcéra.   Vyprskla som smiechom.   "Akého Vinca? Vinco si kúpil len les a v ňom kupko, a na viac mu neostali peniaze. Na žiadne pleso už nemá..."   Viki sa začervenala. "No možno toho z Nitry, toho Vinca..."   "Ktorého z Nitry, prosím ťa?"   "No toho, o ktorom bolo to divadelné predstavenie, vieš, čo sme na ňom boli pred mesiacom."   "Moja milá, žiadne Vincovo pleso nie je. Vinco si nič nekúpil. Žiadny. Naozaj. To najhlbšie jazero sa volá Hincovo pleso. V Tatrách, chápeš?"   Už sa smeje aj ona.   "Vieš, mami, len Marek napísal správnu odpoveď. Ostatní sme to napísali takto... Ale neboli sme najhorší, Adam napísal niečo ešte lepšie." Uškŕňa sa "znalkyňa" slovenských pomerov.   "A čo, prosím ťa?   "Napísal: Vincov ples...     
 Tak, aby ste vedeli. Ten Vinco má riadny recht. Aspoň u štvrtákov. Má balík, za ktorý si kúpi a premenuje všetko, a ešte k tomu si urobí aj vlastný ples.   Tomu sa hovorí božský život.   Ale písomku kvôli Vincovi pokafrať štvrtáci nemuseli. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Rada volím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Tato
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Môj chlap sa skloňuje podľa vzoru hrdina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Hmmm, nič nebanujem...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Neberte nám školu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Kohutiarová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Kohutiarová
            
         
        kohutiarova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Divožienka. Manželka svelého muža. Mama 7 krásnych originálov. Človek, ktorý neprestáva snívať, smiať sa, vidieť veci inak, ako sú na povrchu.Občas to bolí iných i mňa - nie je to predsa "normálne".
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    205
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2172
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pastoračný plán Katolíckej cirkvi na Slovensku 2007-2013
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetko o grantoch
                                     
                                                                             
                                            John a Stasi Eldredge: Očarujúca
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            rádio Lumen
                                     
                                                                             
                                            ticho
                                     
                                                                             
                                            Spirituál Kvintet
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            David Kralik
                                     
                                                                             
                                            Ivanka Iviatko
                                     
                                                                             
                                            Juraj Drobný
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            o zahradach a stavbe domu /hadajte, kvoli comu/
                                     
                                                                             
                                            o deťoch a rodine
                                     
                                                                             
                                            MC Bambulkovo
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                                                         
                       Môj chlap sa skloňuje podľa vzoru hrdina
                     
                                                         
                       Čo už, 50tka
                     
                                                         
                       Pochopila som ...
                     
                                                         
                       Učitelia nevydržia štrajkovať dlhšie ako tri dni...
                     
                                                         
                       Moja spoveď alebo cesta hľadania partnerského šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




