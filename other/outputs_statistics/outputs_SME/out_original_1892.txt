
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dušan V. Mikeš
                                        &gt;
                Kultúra
                     
                 Lucia Siposová: Sami seba neskutočne obmedzujeme 

        
            
                                    25.11.2009
            o
            10:00
                        (upravené
                10.3.2010
                o
                17:33)
                        |
            Karma článku:
                11.16
            |
            Prečítané 
            6844-krát
                    
         
     
         
             

                 
                    Rozhovor s herečkou a spisovateľkou Luciou Siposovou o herectve, ale aj o zážitkoch z dlhodobého pobytu v USA a o jej knižnom debute Hello. My name is Anča Pagáčová. 
                 

                 Aký je Váš dnešný deň? Aké obdobie prežívate?    Dnešný deň je fajn, len už od rána nič nestíham (smiech). Mám taký... hovorím tomu hviezdny deň: ráno som fotila, teraz dávam rozhovor a večer mám divadelné predstavenie, takže je to jeden z takých tých hereckých dní.    Tak som vám skočil do hviezdneho dňa.    Lebo som si vás tak naplánovala (smiech). Obdobie prežívam také jesenné, melancholické, aj dobrého veľa, aj horšieho.        Foto: archív L.S.   Vraj každý, kto sa vráti z Ameriky, prevráti všetko hore nohami. Odtiaľ prichádza aj seriálová Zuza, ktorá vtrhla do seriálu Rádio Fresh ako víchrica. Zodpovedá táto postava vašej prirodzenosti? Ste to „celá vy"?     Je to poloha ktorá sa mi hrá úplne dobre, prirodzene, nemusím analyzovať, nemusím sa nejako mimoriadne pripravovať. Spontánne vyšla zo mňa, snažím sa, aby to bola komická postava, takže tam pridávam napríklad chôdzu, pohyby, úškrny... nie je to absolútne celá ja, ale stotožňujem sa s ňou, je mi blízka, je to človek, ktorého by som určite mala rada, keby sa ocitla v mojom okolí.    Pred časom ste sa vyjadrili, že by nebolo dobré sedieť na dvoch stoličkách - mysleli ste tým súčasné účinkovanie v seriáloch Rádio Fresh a Panelák. Medzičasom ste sa však začali v Paneláku, v postave divácky obľúbenej upratovačky a au-pairky Vierky, znovu objavovať.     V Paneláku nastalo nedorozumenie. Keď som získala úlohu v Rádiu, volala som do Paneláka, nech ma už nerozpisujú. Ale tá informácia sa bohužiaľ nedostala k scenáristovi a on ma už napísal do celej aktuálnej série. To posledné, čo som natáčala bolo ako Vierka kradla, oni ju vyhodili, a tým sa pre mňa ukončila jej etapa. Vlastne sa nevedelo, čo bude ďalej. Nezdalo sa, že by sa tá postava mala ešte kam posunúť.   Nakoniec ste ešte museli s tou metlou trošku postáť...   Ja som si hlavne myslela, že tam idem už len niečo dotočiť, nie je problém, máme dobré vzťahy a ja som za to vďačná. V novinách sa zrazu objavilo, že budem ešte v celej sérii. Spôsobila som asi dosť veľa problémov, dúfam, že sa na mňa nehnevajú, bolo to bohužiaľ nedorozumenie.   Premieta sa už Váš krátky film X=X+1? V akom stave je nakrúcanie „veľkého" filmu podľa Vášho scenára?   Ja by som to poopravila, ten krátky film nie je môj, to je film režiséra Juraja Krasnohorského, ktorý to napísal aj zrežíroval. Je aj spoluautor scenára podľa môjho námetu, ktorý má zatiaľ pracovný názov Tigre tu nie sú šťastné. No a ten jeho krátky film sa mi veľmi páči a Juraj mi tým len potvrdil, že chcem, aby režíroval aj náš „veľký" film. Krátky film by sa mal objaviť v kinokluboch v januári ako predfilm k nejakému celovečeráku. Ešte nevieme, ktorý to bude. „Veľký" film budeme točiť v máji - v júni 2010, keďže potrebujeme teplé počasie, teraz nám už nezostáva nič iné, len čakať na ďalšie leto. V podstate sa nič nezastavilo, len sme to nechali na zimu zamrznúť.   Mohli by ste „veľký" film tematicky priblížiť?   Je to kriminálna komédia, dva paralelné príbehy, viacero uhlov pohľadu, ktoré sa niekde stretnú. Určite by sme chceli urobiť film, ktorý je vtipný, zábavný, žiadna ťažká preumelkovaná dráma. Chcem, aby to bol zároveň umelecký film, ale nie umelecký sebauspokojujúci (smiech), ale uspokojujúci veľa ľudí. Ja mám rada dobré veci, z ktorých sa teší veľa ľudí, takže chcem komerčný film.   Komerčný?   Komerčne úspešný, aby ho videlo čo najviac ľudí.   Krátky film sa čím zaoberá?   Krátky? To je na dlhšie... opisuje ľudí ako roboty, mechanické automaty a ich správanie sa v nečakanej životnej situácii.   Psychologický?   Áno, je psychologický, funguje na princípe spoločenskej hry, réžijne dosť náročná štúdia. Juraj Krasnohorsky je absolvent matematiky a fyziky na univerzite v Ženeve. Páli mu to. Je to taká surreálna komédia (nikdy neviem, či sa hovorí surrealistická alebo surreálna). S veľkým nadhľadom, s nadsázkou a štylizáciou, ale v podstate to rieši taký psychologický model Elisabeth Kübler-Ross, švajčiarsko-americkej psychologičky.   Spomeňme aj Vaše divadelné aktivity.   Dnes večer mám predstavenie v Trnave s Katkou Feldekovou Smrť v ružovom, je to životopisná hra o Edit Piaf, ktorú napísal Ľubomír Feldek. Hrám tam takú trojrolu. My nemáme svoj priestor, tak chodievame po Slovensku, vždy niekde zahráme. V Štúdiu S hrám v predstavení Mínus dvaja s Lasicom a s Polívkom, čo je hrozne príjemné, veľká česť! Ďalej Mesiac na dedine v divadle Astorka, takú menšiu postavu, ale dosť výraznú. No a to sú momentálne všetky moje divadelné aktivity, priznám sa, že ani  nebažím po viacerých. Na divadlo potrebujete strašne veľa vášne, energie a času a všetko je to samozrejme za 5 korún. A ja neviem, či mám toľko vášne, energie a času, aby som sa tomu venovala, takže si ten čas rozdeľujem medzi iné veci a už by som asi viac divadla nezvládla.   Splnil sa Vám sen o vlastnom bývaní. Máte už všetko - knihy, poličky, staré rádio?   Mám, už to začína vyzerať veľmi dobre. Zistila som, že všetko, čo som do tých 35 metrov chcela dať, sa ani nepomestí, takže prebehli veľké čistky, čo je dobre, lebo hovorí sa, že keď si človek uprace v skrini, tak si uprace aj v živote. Strašne sa z toho teším, je to v starom pavlačovom dome, čo bol vždy môj sen. Ani som netušila, že nájdem malinký byt v takej starej štvrti za peniaze, ktoré unesiem, aj keď hypotéka nie je nič príjemné, s týmto neistým povolaním.    Tri roky ste strávili v Amerike (2003 - 2007). Robili ste časníčku a chodili ste na herecké kurzy, tancovali v divadelnej show. Čo sa stalo s Vaším americkým snom? Čo s ním urobila Amerika, zažitá na vlastnej koži?   Vlastne ani neviem ktorý to bol ten môj americký sen. Nie som až taká... cieľavedomá. Išla som do tej Ameriky za zážitkami, chcela som dobrodružstvo, mať strašne veľa príbehov, stať sa takým plnším človekom cez to, čo všetko zažijem a myslím si, že to ten účel splnilo. Nepotrebovala som sa stať americkou hviezdou a potom prísť a vidíte, ja som vám hovorila, že som najlepšia alebo neviem čo, vyslovene som chcela dobrodružstvo.   Nemali ste problém s tým, že doma ste herečka, tam čašníčka?   Aj byť čašníčkou bola pre mňa malá rola môjho malého filmu, ktorý napísal život sám. Nebrala som to tak, že teraz to musím robiť, lebo nemám inú možnosť. Bavilo ma to. Úprimne. Až na to, že človek je potom unavený, už tých tri a pol roka stačilo.          Foto: archív L.S.   Aj u nás existujú podobné herecké kurzy?   Pokiaľ viem, tak nie, ale určite môžete prísť za nejakým hercom, o ktorom sa vie, že učí. Ale ja si myslím, že my sa bojíme pýtať a robiť niečo, čo sa „nerobí". Individuálnou formou by sa určite dalo, skupinové kurzy u nás nie sú, ale viem, že v Prahe už niečo také začalo. My sme strašne obmedzovaní, človek na Slovensku má pocit, že musí sa už ako 14-15-ročný, potom ako 18-ročný rozhodnúť, čo chce robiť celý život, to mi príde dosť brutálne, proste už pri tom musí zostať a nemôže to zmeniť. Vo vyspelejších krajinách ľudia neblokujú sami seba, že toto je  jediné, čo teraz musím a iné nemôžem, neboja sa život v nejakých obdobiach meniť. Herec u nás... keď máte viac ako 25 rokov, tak vás už na školu nezoberú. Ale prečo by niekto nemohol začať v 50tke? Možno by bol lepší ako ktokoľvek, kto to reálne vyštudoval. Proste nemá tú školu a nie je braný ako herec, spoločnosť mu nedá šancu.    Toto zďaleka neplatí len v herectve.   Nielen v herectve, presne tak, ale to je podľa mňa hrozné, sami seba neskutočne obmedzujeme.   Ako by bol podľa vás u nás prijatý kontroverzný erotický videoklip, nabadájúci Američanov k bezpečnému sexu?   Myslím si, že v tomto sme už dosť zrelá spoločnosť, možno až prezretá. Mám pocit, že by to bolo úplne v pohode, keby to išlo u nás v televízii. Tá myšlienka a odkaz je v poriadku a hlavne to má humor, to je humor predsa... mne to prišlo vtipné, dosť sme sa nasmiali. (Klip sa dá nájsť na osobnej stránke Lucie Siposovej v sekcii Video).    Vyskytuje sa v USA podobný alebo iný druh závisti ako na Slovensku?   Ťažko hodnotiť, nerada hodnotím, že to tak je alebo to tak nie je, ale podľa toho, čo ja vnímam, tak Slovensko je veľmi špecifické, čo sa závisti týka. Myslím si, že je to naša veľmi výrazná črta, veľmi negatívna črta. Sami si škodíme, lebo závisť nerobí nikomu dobre, nikomu zle (len tým, ktorí ju pociťujú). Na Slovensku, keď má človek niečo lepšie,  tak už príde milión 500 takých tých podozrievavostí, že ako k tomu prišiel. Že ľudia nedoprajú je jedna vec, ale že už apriori myslia zle o tom človeku, že ten, kto má peniaze, nemôže byť poctivý, ani dobrý. Nemusí človek iba tunelovať, aby bol bohatší ako ostatní, naozaj sú rôzne spôsoby ako zarobiť peniaze a sú čestné. V Amerike to ľudí motivuje.Tam som si nevšimla takú strašnú závisť. Aj keď teda mne nebolo čo závidieť (haha). Dokonca si pamätám, keď som sa vracala z Ameriky, keď som niekomu lichotila, pochválila, napríklad „máš pekné vlasy, šaty, blbosti...", tak na mňa pozerali ako na debila, že čo afektujem, čo sa predvádzam. V Amerike, tam sa neboja prejaviť jeden druhému úctu, obdiv, náklonnosť, takisto ako si vedia vynadať, tak si vedia aj povedať to dobré, vedia vyjadriť svoju emóciu bez hanby.   Len či je to úprimné alebo či to tak len vyzerá?   Je to plytké, ale nie je to neúprimné. Sprímnejní to ľuďom deň.   Naša predstava o Američanoch je, že sú povrchní. Nenaplnila sa?   Nie. My hľadáme, čo kde pokritizovať, v čom sme my menej povrchní? Máme 10 ľudí v živote,  ku ktorým sme nepovrchní alebo povedzme 20, ja mám možno o troch viac, ku ktorým som nepovrchná, s ktorými naozaj do hĺbky niečo riešim. Ostatní ľudia... som k nim povrchná, k ostatným ľuďom, veciam atď. Čo je to nepovrchné? Keď je človek milý a pochváli vás, prečo by to bolo neúprimné? Tu len preto sme akože úprimní, že vás nepochválime? Alebo že nepovieme niečo povzbudivé? Jediné, čo mne v Amerike vadilo, bolo to ich tliachanie, také, že sa na niečom dohodnem a to neplatí. Naozaj, oni neraz tliachajú, že uvidíme sa, a niekde sa aj dohodneme, a ja tam prídem a dotyčný tam nie je. V tomto som ja hrozne zodpovedná, toto mňa v Amerike najviac hnevalo.          Foto: archív L.S.   Do akej miery je postava Anče Pagáčovej z Vašej knihy Hello. My Name Is Anča Pagáčová autobiografická?    Ťažko tu hovoriť o percentách, ja som sa vždy chytila nejakého zážitku, ktorý sa v ten deň pritrafil mne alebo niekomu v mojom okolí. Alebo niekto povedal nejakú vetu, ktorú som strašne chcela niekde tam zakomponovať, tak som hneď domyslela príbeh. Je to knižka v prvej osobe, ide to zo mňa a je to úprimné a niekde to ide až za nejakú hranicu. Lebo mňa baví balansovať na tej hrane, ale v percentách sa v tomto prípade vyjadriť neviem. (Tu nájdete ukážky z knihy).   Poznáte aj Vy niekoho, koho máte radšej ako on/ona Vás?   Ale určite, určite áno. Sú takí ľudia, s ktorými mám takýto vzťah. Neviem na koho narážate.   Krásu, atraktívnosť a sexuálnu príťažlivosť - vlastnú, aj iných žien a mužov - spomína Anča Pagáčová nespočetnekrát. Zdajú sa byť v jej hodnotovom rebríčku dosť dôležité.    To je to, čo človek vníma ako prvé. Dôležité je, aká je krása a príťažlivosť hlboká. Ak je príliš plytká, rýchlo sa stáva nezaujímavou.   Hrdinka knihy stretla človeka, presvedčeného, že Dvojičky si Američania zrútili sami. Stretli ste takých aj vy?   Áno.   Pri možnom predpojatom očakávaní konzumistickej „matkinoidnej" literatúry by sme možno neočakávali, že bude spomenutý Boh, hoci v bizarných súvislostiach.    Lepšie neočakávať. A predpojato už vôbec nie.   Milujete ilúzie. (Americkou ilúziou môže byť - aspoň podľa knihy - napríklad na playback spievajúca „profesionálna" speváčka, ktorá v skutočnosti spievať nevie). Sú slovenské ilúzie iné ako americké?   Vďaka našim televíznym štruktúram, ktoré sa nechávajú smelo inšpirovať tými americkými, pomaly aj my vytvárame slušné ilúzie. Len ich ešte nevieme tak dobre predať. Ilúzie sú však dočasné. Len skutočné veci vydržia.   Aký druh jogy je BIKRAM joga? Zvláštne sa mi zdá, že tam tečie pot. Ak je tam „90 horúcich minút bolesti", človek celý červený, je to vôbec ešte joga?   Tak to volajú. Neviem, čo bikram znamená, to som si mohla zistiť... V horúcom prostredí si pekne krásne naťahujete svalstvo. Ohýbate sa ako blázni, tečie z vás pot prúdom... ťažko sa tam dýcha, ale po tej námahe je vám fantasticky. Nie každému by to asi sedelo.   Ste prinajmenšom druhá slovenská spisovateľka, ktorá spomína tému globálneho otepľovania a klimatických zmien. („plytvaná energia, pripečená civilizácia, ktorá kašle na globálne otepľovanie"). Snažíte sa preto aj o nejaké reálne zmeny v životnom štýle?    Troška. Snažím sa neplytvať. Ani svetlom, ani jedlom, ani vodou...recyklujem, delím odpad, keď to ide. Dosť ma hnevá, že na mnohých miestach vôbec nie sú smetné koše na delený odpad.   Vedeli ste už predtým, že „very gay music" znamená „veľmi veselá hudba"?   Áno. Gay je veselý. To som sa dozvedela už dávno pri skúšaní muzikálu Klietka bláznov na Novej scéne.   Američania sú vraj odborníci na tematické večierky. Aký najzaujímavejší, najbizarnejší alebo najnezmyselnejší ste zažili?    Tie najbizarnejšie som spomenula v Anči Pagáčovej. Nechcem sa opakovať.   Ako sa cítite na „smotánkových" akciách?   Musím povedať, že mám vďačnú povahu. Som spoločenská a s ľahkosťou zvládam aj túto súčasť svojej práce. Nevadí mi sa troška popredvádzať. Keď zrovna  na to nemám náladu, nikam nejdem.   Zdala sa Vám Argentína v pozitívnom zmysle odlišná od Brazílie, ktorá je v knihe vykreslená ako „neintelektuálska, divoká, uvoľnená, pudovo založená krajina, kde sú samé rozmaznané hlúpe deti a kam bohatí Američania a Európania chodia na sexturistiku a drogové opojenia?"    Nemôžem hovoriť o Argentíne, lebo zatiaľ poznám len Buenos Aires. Určite postráda istú dávku divokosti, ktorú má Brazília, tiež neoplýva nádhernými plážami. Ale Buenos Aires má v sebe akúsi melanchóliu zvädnutej aristokracie, skrytú vášeň a tango. Zastal tam čas. A to sa mi páči.   Po úspechu Vášho knižného debutu, neuvažujete o ďalšej knižke?   Presne tak, uvažujem. Ale to je asi tak všetko. Podľa môjho námetu sme s Jurajom Krasnohorskym napísali scenár k celovečernému filmu, ktorý budeme točiť na jar.   Ako najradšej relaxujete?   S vínom a s priateľmi.       Lucia Siposová (1980)   Narodila sa v Bratislave. Absolvovala konzervatórium, rok študovala španielčinu a španielsku kultúru na Univerzite Komenského. Bola členkou dievčenskej skupiny Four, v STV moderovala reláciu Sedmička, hrala v TV seriáli Záchrannári. V rokoch 2003 - 2007 žila v New Yorku, kde navštevovala herecké kurzy, hrala v niekoľkých krátkych filmoch a reklamách. Účinkovala vo filmoch  režiséra Filipa Renča Na vlastní nebezpečí (2007) a Hlídač 47 (2008). V súčasnosti hrá v divadle Astorka v predstavení Mesiac na dedine, v Štúdiu L+S v predstavení Mínus dvaja, v hre o Edith Piaf Smrť v ružovom. Účinkovala v televíznych seriáloch Panelák a Rádio Fresh. Jej veľkou záľubou je argentínske tango. Počas pobytu v USA napísala knihu autobiografických fragmentov s názvom Hello. My name is Anča Pagáčová. Podľa životopisu, uvedeného v závere knihy, je okrem iného aj speváčka, hudobníčka, modelka na vlasy, interiérová dizajnérka, fotografka, pozorovateľka, opatrovateľka, plavkyňa motýlika, jogínka, kolotočiarka, iluzionistka atď. (celkove uvádza 37 rôznych aktivít).     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan V. Mikeš 
                                        
                                            Nový album Ultravox: na toto sme čakali 26 rokov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan V. Mikeš 
                                        
                                            Vydávať cudzie zážitky za vlastné je tiež plagiátorstvo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan V. Mikeš 
                                        
                                            Čo asi nebude v programe žiadnej politickej strany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan V. Mikeš 
                                        
                                            Kruhy v obilí (Jaroslav Rumpli - ukážky z románu)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dušan V. Mikeš 
                                        
                                            Ako začínali (o debutoch slávnych kapiel)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dušan V. Mikeš
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Tatiana Lajšová 
                                        
                                            Recenzia: Fantázia 2014
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dušan V. Mikeš
            
         
        mikes.blog.sme.sk (rss)
         
                                     
     
        Má sa výborne (má kde spať a má čo jesť).
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    97
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3226
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            René mládenca ... spomienky
                        
                     
                                     
                        
                            Haluze haluzné
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Viera a filozofia
                        
                     
                                     
                        
                            Zdravie, príroda a životný štý
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Muzička
                        
                     
                                     
                        
                            Vzťahy a vzťahy k vzťahom
                        
                     
                                     
                        
                            Cice
                        
                     
                                     
                        
                            Cestoviny
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Mobily a žiarenie, technológie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Eva Bacigalová: Čistá nuda
                                     
                                                                             
                                            Jaroslav Rumpli: V znamení hovna
                                     
                                                                             
                                            Ivan Kolenič: Mlčať
                                     
                                                                             
                                            Márius Kopcsay: Mystifikátor
                                     
                                                                             
                                            Andrijan Turan: Sťahovanie vtákov
                                     
                                                                             
                                            Pavel Hirax Baričák: Raz aj v pekle vyjde slnko
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Therion: Les Fleurs du mal
                                     
                                                                             
                                            Visage 2013
                                     
                                                                             
                                            Richard Wright: Wet Dream
                                     
                                                                             
                                            Deep Purple 2013
                                     
                                                                             
                                            Ultravox 2012
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Vegetariánska reštaurácia, Obchodná 58, BA
                                     
                                                                             
                                            výstava Dych Zeme, Šamorín 11.10.2008
                                     
                                                                             
                                            skupina Jednofázové kvasenie
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




