
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Žatko
                                        &gt;
                Politika
                     
                 Poďakovanie Františkovi Mikloškovi 

        
            
                                    8.7.2010
            o
            0:09
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1339-krát
                    
         
     
         
             

                 
                    Dnes zasadnú do poslaneckých lavíc v národnej rade ľudia, ktorí budú mať ďalšie štyri roky v rukách osud Slovenska. Po dvadsiatich rokoch bude medzi nimi chýbať muž, ktorého si veľmi vážim. František Mikloško. Je pre mňa symbolom človeka, pre ktorého sú morálne zásady silnejšie ako osobné výhody. Ukázal to už počas komunistického režimu, keď sa venoval práci v tajnej cirkvi. Ukázal to v marci 1988, keď sa odvážil oficiálne oznámiť štátnej moci uskutočnenie "Manifestácie za náboženskú slobodu a dodržiavanie ľudských práv". Napriek tomu, že ho ŠtB zatkla, sviečková manifestácia sa uskutočnila a stala sa prvým masovým verejným prejavom odporu proti komunistickej moci v Česko-Slovensku. Ukázal to počas svojho 20-ročného pôsobenia v laviciach národnej rady. Napriek tomu, že mu mnohí toho veľa vyčítajú, nemôžu mu vyčítať jedno. Nemôžu mu vyčítať kontá vo švajčiarskych bankách, nemôžu mu vyčítať jachty, honosné vily, ba ani rýchle autá, nemôžu mu vyčítať, že do politiky išiel preto, aby sa nabalil. František Mikloško je pre mňa príkladom toho, že nie každý sa musí v politike zašpiniť. Vďaka ti František za to.
                 

                 
 zdroj: sme
 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (45)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Žatko 
                                        
                                            Bugárovci, Sieť a Skok v jednom šíku so Smerom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Žatko 
                                        
                                            Smer - strana papalášov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Žatko 
                                        
                                            Zlý starosta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Žatko 
                                        
                                            Starostovi Devínskej Novej Vsi sa rozpadla jeho jedenástka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Žatko 
                                        
                                            Imre a Gejza v Národnej rade
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Žatko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Žatko
            
         
        janozatko.blog.sme.sk (rss)
         
                                     
     
         www.janzatko.eu 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    161
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2472
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Devínska Nová Ves
                        
                     
                                     
                        
                            Rozhovory
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Kanada
                        
                     
                                     
                        
                            Osobnosti
                        
                     
                                     
                        
                            Príroda
                        
                     
                                     
                        
                            Zaujímavosti
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Wikipédia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Podivný nákup pušiek BREN
                     
                                                         
                       Čentéšov prípad je ponaučením
                     
                                                         
                       Mladí, príďte do Prahy, na stretnutie Taizé
                     
                                                         
                       Sexualizácia detí s podporou štátu – prevencia, deformácia - rodičia pozor
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Kampaň za posledné drobné len začne
                     
                                                         
                       nove automaty na parkovanie v centre Bratislavy
                     
                                                         
                       PPP áno, ale nie Podvodné, Predražené a Protiústavné
                     
                                                         
                       Debakel Siete?
                     
                                                         
                       MHD pre Žilinčanov „zadarmo“... hmmm... a to s kým bankujete?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




