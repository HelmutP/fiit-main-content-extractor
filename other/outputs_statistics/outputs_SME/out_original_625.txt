
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Effenberger
                                        &gt;
                zamyslenia
                     
                 Pravdivosť... 

        
            
                                    13.5.2008
            o
            22:14
                        |
            Karma článku:
                7.07
            |
            Prečítané 
            1334-krát
                    
         
     
         
             

                 
                    ...priťahuje...
                 

                  Čoraz častejšie sa stretávam s tým, že tí, ktorí sa hlásia ku kresťanstvu, teda ku Kristovi nežijú tak, ako by sa to od nich očakávalo. Zaujímavé je, že rôzne konflikty vytvárajú práve tí, ktorí by mali byť svetlom a kvasom tohto sveta. Nechcem kritizovať len tých druhých, samozrejme hovorím aj o sebe, pretože v prvom rade ja by som mal byť príkladom pre ostatných a svojím konaním, hovorením a myslením povzbudzovať druhých. No žiaľ, nie vždy som takýmto príkladom pre ostatných...a samozrejme ma to mrzí. Áno, je to kritika do „vlastných radov“. Veľakrát si musíme uznať, že s tým našim kresťanstvom nie je niečo v poriadku. Lepšie povedané – s prežívaním nášho kresťanstva. Formálne a povrchné prijímanie sviatostí – sv. prijímanie, sv. spoveď, birmovanie...podstatné a prvoradé sú krásne šaty (pre našu princezničku, ktorá musí byť najkrajšia), obleky pre mladých pánov, dostatok jedla na stoloch, okázalé fotografovanie, slabé a len nabiflené vedomosti, ktoré sa razom vyparia z hlavy, nieto ešte, aby zostali v srdci...Ale môžu za to tie deti? Samozrejme, že NIE. Zodpovednosť je na tých, ktorí s nimi prichádzajú denno-denne do styku a pripravujú ich na prijatie týchto sviatostí a taktiež im ukazujú model kresťanstva. Hlavnú úlohu majú rodičia, ale keď nepraktizujú nič z kresťanstva...prečo sa čudovať? To, čo nezvládli rodičia by mali aspoň z časti zachrániť katechéti...ale priznajme si – koľkí z nich naozaj žijú to, čo učia...? To, čo nezvládnu katechéti by mal zvládnuť kňaz, ale prakticky to nie je možné. Veď si len napríklad predstavte 80-člennú skupinu mladých ľudí vo veku od 13-17 rokov. Zvládnuť týchto mladých je niekedy nad ľudské sily. Ešte keď som bol v kláštore boli sme v jednej dedinke na pozvanie miestneho kňaza. Pripravoval takýchto mladých na birmovku, tak nás zavolal, že teda, aby sme sa k nim nejako priblížili. Tak sme nacvičili nejaké piesne (sólová a basová gitara + bongo – to teraz medzi mladými letí...) Počas hrania sa to ešte dalo, ale keď sme vyšli von a rozdelili sa na skupinky začalo to byť „zaujímavé“ Priznám sa, nedokázal som ich zaujať vôbec ničím. Možno keby som vytiahol nejakú fľašu alkoholu, cigarety, karty a začal by som sa s nimi baviť o sexe – možno by som zaujal. Keď som sa ich spýtal prečo vlastne idú na birmovku, takmer každý mi povedal, že ani nevie, že babka ich prihlásila a prinútila. Jednoducho som kapituloval...Ale tie deti za to nemôžu... je to naša chyba. Žijeme kresťanstvo iba veľmi povrchne a len v kostole. Som smutný z toho, keď na omši pri znaku pokoja vidím tie chladné pohľady, niektorí sa ani len neotočia hoci stoja tak blízko seba. Nie je to smutné? Dojíma ma, keď vidím, ako si babka s veľkou námahou kľakne pri premenení  na obe kolená a mladý muž (25 rokov) si kvokne a popritom stihne napísať sms-ku...Je to zahanbujúce. Keď takýto človek vyjde z kostola, má a musí reprezentovať kresťanstvo, ale ak on v kostole dokáže stvárať takéto veci, čo sa potom deje mimo kostola...? Tí, ktorí si chodia do kostola odsedieť tú hodinku – nech radšej ani nechodia. Možno by lepšie spravili, keby si zašli na kofolu. Mňa by nikdy nenapadlo ísť na operu, pretože to nie je moja krvná skupina. Bol by som tam úplne zbytočne – nebavilo by ma to. Ak neprijímaš to, čo sa v kostole hlása, tak načo tam chodíš? Načo ideš niekam, kde ťa to vôbec nebaví? Nie je to strata času? Tu naozaj nerozhoduje kvantita, ale kvalita. Darmo budú plné kostoly, ak sa s vierou nestotožníme, ak ju nezačneme žiť všade tam kam sa dostaneme. A tieto riadky platia v prvom rade pre mňa...      Pane, Bože, tvoj je deň a tvoja je noc; daj, nech v našich srdciach žiari slnko spravodlivosti, nech z nášho srdca vyžaruje teplo a svetlo pre tento svet. Nech sme tvojimi svedkami všade naokolo, aby podľa našich skutkov, podľa našich rečí, našich názorov a postojov bolo zrejmé, že sme tvoje deti, že sa na nič nehráme, že to s našim kresťanstvom myslíme vážne. Aby sme potom mohli vojsť do svetla, v ktorom prebývaš.       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (65)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Effenberger 
                                        
                                            Nebudú zle robiť a nebudú škodiť...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Effenberger 
                                        
                                            Adrenalínový šport...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Effenberger 
                                        
                                            Veľká noc 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Effenberger 
                                        
                                            Po dlhých mesiacoch...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Effenberger 
                                        
                                            Podpora OFM
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Effenberger
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Effenberger
            
         
        effenberger.blog.sme.sk (rss)
         
                                     
     
        


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    107
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1748
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            zamyslenia
                        
                     
                                     
                        
                            myšlienky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Bon Jovi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            My Posterous
                                     
                                                                             
                                            My WordPress
                                     
                                                                             
                                            My Twitter
                                     
                                                                             
                                            My web
                                     
                                                                             
                                            My Youtube
                                     
                                                                             
                                            My Facebook
                                     
                                                                             
                                            Bon Jovi
                                     
                                                                             
                                            Žive SK
                                     
                                                                             
                                            Živě CZ
                                     
                                                                             
                                            Mobilmánia SK
                                     
                                                                             
                                            Mobilmánia CZ
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




