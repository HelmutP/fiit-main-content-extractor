
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivona Mertusová
                                        &gt;
                patchwork
                     
                 Patchwork 2. 

        
            
                                    26.4.2010
            o
            11:08
                        (upravené
                26.4.2010
                o
                11:41)
                        |
            Karma článku:
                3.88
            |
            Prečítané 
            864-krát
                    
         
     
         
             

                 
                    Viete čo je SIGGY?
                 

                 
 flickr.com
   Je to vlastne „šitá vizitka", ktorú si patchworkárky s obľubou vymieňajú medzi sebou pri rozličných príležitostiach a keď ich nazbierajú dostatočný počet, môže vzniknúť deka alebo aj obrus na stôl. Navyše, je to aj trvalá spomienka na ostatné patchworkárky.   V Európe má siggy zvyčajne rozmer 14 x 14 cm a v strede je nejaký zaujímavý vzor. Americké bývajú menšie, a sú skôr vhodné na moderný quilt. Dajú sa z nich skladať rôzne vzory.   Ak by ste si chceli niečo také ušiť, postup pre európske siggy je veľmi jednoduchý a zvládnu ho aj začiatočníci. Pre stredy si vyberte nejakú svetlejšiu látku bez vzoru a na okraje nejakú peknú vzorovanú. Keďže siggy by malo prežiť častejšie pranie (ako súčasť nejakého výrobku), odporúča sa 100% bavlna, ktorú treba vopred predprať a vyzrážať. Okrem látok budete potrebovať permanentnú fixku na textil, aby sa nápis nevypral a nevybledol. Toto si treba dopredu vyskúšať, aby ste časom neboli nepríjemne prekvapení :-)   Vymyslite si obrázok do stredu siggy - môže to byť niečo kreslené, aplikácia alebo hoci aj výšivka, ak máte dosť trpezlivosti. Ja som si zvolila kresbu. Okolo obrázka patrí meno, mesto, krajina autora siggy a samozrejme dátum, kedy vizitka vznikla.   Postup pre americké siggy je takisto jednoduchý a potrebujete akurát základy angličtiny, aby ste si ho naštudovali. Tento vzor si môžete ušiť aj bez nápisov a použiť ho na deku - šije sa rýchlo a výsledok milo prekvapí.   Jeho malou modifikáciou získate zase trochu iný vzor - stačí našívané farebné štvorce zmenšiť a svetlý stred nechať väčší (prípadne u niektorých stred urobiť tmavši).   Ušili ste si zopár siggy? Je načase začať vymieňať a tešiť sa na nové prírastky do zbierky :-)   Príjemné tvorenie!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Rýchly obed - rajčinová polievka a syrovo - cibuľový sendvič
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Lukostreľba V. - užitočné stránky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Patchworková výstava v Záhorskej Bystrici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Hľadáte originálny darček?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Patchwork v Dunajskej Strede
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivona Mertusová
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivona Mertusová
            
         
        ivonamertusova.blog.sme.sk (rss)
         
                                     
     
        Milovníčka škótskych teriérov a nevyliečiteľná gurmánka. Okrem lukostreľby a biliardu sa venuje aj cudzím jazykom a patchworku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    53
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1901
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            škótsky teriér
                        
                     
                                     
                        
                            gastro
                        
                     
                                     
                        
                            na cestách
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            snooker
                        
                     
                                     
                        
                            lukostreľba
                        
                     
                                     
                        
                            patchwork
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Norbiho tajemství
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marína Mery Dobošová
                                     
                                                                             
                                            Ľubomír Mercery Pecho
                                     
                                                                             
                                            Tomáš Bella
                                     
                                                                             
                                            Martin Basila
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Gorila
                                     
                                                                             
                                            Update Slovakia
                                     
                                                                             
                                            Prevody kuchynské
                                     
                                                                             
                                            Prevody do metrického systému
                                     
                                                                             
                                            Slovenský lukostrelecký zväz
                                     
                                                                             
                                            1. Slovenský školský lukostrelecký klub
                                     
                                                                             
                                            PRIHLÁSENIe do blogu
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




