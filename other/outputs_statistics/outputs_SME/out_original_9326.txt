
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Zlúky
                                        &gt;
                Filmy
                     
                 Kino preview – leto 2010 – Part 1 

        
            
                                    21.6.2010
            o
            16:28
                        (upravené
                21.6.2010
                o
                17:22)
                        |
            Karma článku:
                2.20
            |
            Prečítané 
            847-krát
                    
         
     
         
             

                 
                    Keďže dnes je prvý letný deň tak sa poďme pozrieť na to čo si pre nás pripravil na hlavnú sezónu slovenský distribútor.  Pokiaľ má v jeden deň premiéru viacero filmov venovať sa budem vždy len jednému (lenivosť :)).
                 

                 
  
        24.6.2010 - The Crazies (Crazies), An Education (Škola života)    The Crazies   Horory a obzvlášť také kde sa to hemží zombie nie sú ani náhodou to čo vyhľadávam.  O to väčším prekvapením bol pre mňa najnovší počin režiséra Brecka Eisnera. V roku 2005 firma Paramount zariskovala a málo známemu režisérovi televíznych filmov dala do vrecka 130 miliónov dolárov aby im natočil dobrodružný film Sahara. Nakoniec z toho bolo slušné fiasko s celosvetovými tržbami cca 120 miliónov dolárov. Zdalo sa, že chlapec si už viac neškrtne.   S o dosť nižším rozpočtom (20 miliónov) mu dala šancu mladá a stále zaujímavejšia firma Overture films  a tak sa môžete tešiť na remake klasického hororu Georga A. Romera The Crazies.  Ja už som ho mal možnosť zhliadnuť a výsledok hodnotím kladne.  Veľmi solídna zábava prekračujúca priemer  súčasných „hororov", ktorá nerobí z diváka až takého dementa ako iné. Pre fanúšikov žánru takmer must see.       1.7.2010 - Twilight saga: The Eclipse (Twilight sága: Zatmenie), Mamas &amp; Papas, Oko nad Prahou    Twilight sága: Zatmenie   A je to tu decká. Druhé pokračovanie megaúspešnej megaságy od megaspisovateľky pre megamasu ľudí, ktorý jasajú pri každom pohybe megasterilného Pattinsona a megapatetickom prejave Taylora Lautnera (vďaka za Kristen Stewart i keď aj ona je tu doooooosť biedna). Štúdio Summit už je určite nadržané koľko prachov im prinesie švédska trojka v podaní vlkolaka, upíra a nevinnej dievočky a milióny fanúšikov po celom svete sa už nevedia dočkať kedy sa odbavia pri výmene fusaklov hlavných predstaviteľov.  Áno aj takýto je dnes filmový svet. Možno sa mýlim a režisér David Slade (vynikajúca Noc dlhá 30 dní) vtesná do filmu aj nejaké to prekvapenie.       8.7.2010 - I love you Phillip Morris, The Road (Cesta), Predators (Predátori)   Predátori   Asi by bolo vhodnejšie sa povenovať prvým dvom filmom ale nedá mi nechať voľné pokračovanie kultového Predátora len tak bez povšimnutia. Dlho dlho som nevedel, že sa vôbec niečo takéto chystá a pravdupovediac som sa hneď chytal za hlavu, že čo za magora to zas ide dodrbať tak dokonalé dielo.  Pokúsiť sa o to má Nimród Antal pod vedením Roberta Rodrigueza ako producenta. Nech rozmýšľam ako rozmýšľam nejaký pozitívny výsledok jednoducho nie je možné očakávať. Antal je pri všetkej úcte nula a Rodriguez asi s filmom veľa spoločného nemá. Aj trailer je zatiaľ úplne na nič. Jediným kladným bodom je teda zatiaľ len návrat do džungle tak ako to máme radi a nie nejaký severný pól a buranské mestečko v USA (Votrelec vs Predátor, Votrelec vs Predátor 2).       15.7.2010 - Shrek Forever After (Shrek: Zvonec a koniec 3D), Hot Tub Time Machine (To bol teda zajtra žúr)   Hot Tub Time Machine   „This years Hangover", „Best comedy in years" - aj takéto výroky nájdete na nete keď budete chcieť niečo bližšie o filme Hot Tub Time Machine, v doslovnom preklade To bol teda zajtra žúr.  A keďže aj túto premiéru už mám za sebou tak tu je pár teplých slov. Nič také ako je napísané vyššie sa nekoná. Očakávanie boli veľké a preto ma priemerný výsledok neuspokojuje. Nie je to zlé ale čakal som viac, oveľa viac. Nápadov s vrátením sa do minulosti tu už bolo a tak to chcelo niečo navyše. Osemdesiate roky sú na to ako vyšité ale tvorcovia to nedotiahli tam kam chceli. Vtipy sú na môj vkus až moc nútené a jeden z hlavných predstaviteľov nechutne prehráva a brutálne kazí dojem z filmu, naopak Craig Robinson je skvelý. Na letnú oddychovku to však môže byť dobrá voľba.       22.7.2010 - Furry Vengeance (Pomsta chlpáčov), Inception (Začiatok)   Začiatok   Jeden z najočakávanejších filmov roka sa pomaly blíži (keby bola nejaká tajná premiéra v Karlovych Varoch vôbec by som sa nenasral). Najvychytenejší režisér Christopher Nolan a najvychytenejší herec Leonardo Dicaprio, z ktorých ani jeden nevie čo to znamená podieľať sa na vyslovene zlom filme, to je prísľub fantastického zážitku a nepochybujem o tom, že to bude bomba. Mindfuck trailer hovorí za všetko:              Pokračovanie čoskoro......                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Zlúky 
                                        
                                            Aliancia za alianciu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Zlúky 
                                        
                                            Remember my name
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Zlúky 
                                        
                                            Filmy, ktoré ste možno nevideli – Take Shelter
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Zlúky 
                                        
                                            Filmy, ktoré ste možno nevideli – The Loved Ones
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Zlúky 
                                        
                                            Filmy, ktoré ste možno nevideli - Once Were Warriors
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Zlúky
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Zlúky
            
         
        zluky.blog.sme.sk (rss)
         
                                     
     
        Ak by ste náhodou chceli vedieť ako vyzerá skutočný flegmatik tak vám dám na mňa kontakt.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1224
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako sme (ne)vyrobili nový web UK
                     
                                                         
                       To čo zažiješ ťa posilní alebo...
                     
                                                         
                       Aliancia za alianciu
                     
                                                         
                       Akcia supermarketu: doktorský titul PhD. za päť mesiacov
                     
                                                         
                       J&amp;T, starajte sa o kamzíky a nevydierajte!
                     
                                                         
                       Viete, čo majú spoločné Igor Rattaj (J&amp;T) a Miroslav Trnka (Eset)?
                     
                                                         
                       Povolebný návrat Smeru k duchovnému bratstvu s Harabinom
                     
                                                         
                       Obvinený z podvodu na DPH ukázal prstom na Bučeka, Kubánka, Kopčíka
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Rumunskí podvodníci sú späť!
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




