

 Po zverejnení programu SaS sa ozvalo mnoho, často aj oprávnených, kritikov nielen na tému zmeny legislatívy v drogovej politike. Lenže práve v tejto oblasti sa asi naviac zamieňajú vyššie menované tri pojmy, čo vedie k rôznym dezinterpretáciám. Najkrajšou ukážkou je asi Mečiarov výrok: "Tak prvé, na autoritu pána Sulíka sa odvolávať nebudem. U mňa  ľudia, ktorí chcú zaviesť voľný obchod s drogami nemajú čo robiť. (NaTelo 11.4.2010)" Ako už ukázal Demagog.sk predo mnou, Mečiar v tomto prípade zle interpretoval zámery SaS. 
   
 Najprv je však treba vysvetliť rozdiely medzi legalizáciou, dekriminalizáciu a depenalizáciou. Mnohí považujú Holandsko za príklad legalizácie, niektorí ho označujú za krajinu, v ktorej sú dekriminalizované drogy. Avšak jediná krajina v Európe, ktorá dekriminalizovala drogy ako také (nielen marihuanu) je Portugalsko a o legalizácii sa v Európe hovoriť nedá vôbec. Dekriminalizácia znamená, že užívanie alebo držanie drog (v prípade SaS by to bola len marihuana) už nespadá pod trestné právo a nevyužívajú sa tresty, ako odňatie slobody alebo záznam v registri trestov, namiesto toho sú tieto činy považované len za niečo ako administratívne priestupky (?)  -administrative violations. 
   
 Oproti tomu depenalizácia ponecháva tieto činy pod trestným právom, avšak nevyužíva sankcie podobne ako pri dekriminalizácii. Takisto je dôležitá miera. Kým Portugalsko dekriminalizovalo všetko- aj heroín, kokaín a podobné, iné krajiny sa zamerali iba na mäkké drogy alebo čisto na marihuanu. Vačšina krajín však zaviedla len depenalizáciu, podobne ako sa to pred nedávnom udialo v Čechách, alebo ako to funguje v Nemecku, Belgicku a mnohých iných Európskych krajinách. 
   
 Pod dekriminalizáciu a depenalizáciu nepatrí voľný obchod, ani ľubovolný predaj drog komukoľvek a kdekoľvek. Legalizáciou sa navyše povoľuje aj výroba a obchodovanie s drogami, s podobným systémom, vrátane napríklad vekových obmedzení, ako to poznáme pri predaji alkoholu alebo tabakových výrobkov. (viď: správa o dopadoch dekriminalizácie drog v Portugalsku, kde okrem iného vysvetľujú i rozdiely medzi týmito pojmami) 
   
 Z toho, čo SaS píše vo svojom programe sa dá povedať, že určite nenavrhujú legalizáciu, pretože predaj, obchodovanie či dovoz má i naďalej ostať trestným činom, podľa programu však pravdepodobne presadzujú čiatočnú (len na marihuanu zameranú) dekriminalizáciu, avšak nie je to isté pretože nepíšu, či chcú konkrétne činnosti vyňať spod trestného práva (program). 
   
 Bez ohľadu na náš osobný názor na SaS alebo na navrhované zmeny v legislatíve by sme mali o tejto téme začať verejne diskutovať, pretože drogy boli, sú aj budú do istej miery problémom, ktorý treba nejako riešiť, ale táto diskusia bude mať zmysel len vtedy ak si ľudia, vátane tých v SaS, prestanú (samozrejme aj okrem iného) zamieňať jednotlivé pojmy a ich významy a prestanú si s tymito slovami spájať rôzne predsudky. Ak by SaS vo svojom programe nestriedali "haj-buj" slová legalizácia- dekriminalizácia, pravdepodobne by zabránili aspoň niektorým dezinterpretáciám. 
   
   
   

