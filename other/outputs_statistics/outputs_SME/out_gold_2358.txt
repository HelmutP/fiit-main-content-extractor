
 Prvý krát vtedy, keď som čakala v Petržalke na autobus a počas okamihu, kedy nebola veľká premávka, prechádzal cez štvorprúdovú cestu jeden, ujo, ktorý nemal obuté zimné topánky, ale iba poltopánky. A ako som sa tak zapozerala na toho uja a jeho topánky, celkom bolo vo vzduchu cítiť vlhkú hlinu, slnečné teplo a a niečo o čom sa domnievam, že tak vonia chlorofil keď  všetky rastliny rastú.

     A potom druhý krát vtedy, keď som išla zatvoriť balkónové dvere na intráku asi okolo pol šiestej večer. Vzduch voňal tak ako vždy u nás v Slávičom údolí na neskorú jar a drozdy spievali presne rovnako ako vždy večer keď prichádza jar. Ale bol to spev ešte akoby tlmený protihlukovou stenou. Fúkal južný vetrík a zore boli oranžovo-ružové.

     Tak som teda išla peši do Mlynskej doliny na divadelné stretko a bolo mi tak mäkko. Neviem presne vystihnúť čo znamená keď mi je mäkko, ale môžem povedať, že mi bolo veľmi jarne.

     Leto bolo schované vo voňavke môjho kamaráta. Voňala mi chatou na Hronci a drevom, trávou, čokoládou a únavou. 

     No a tak som si išla späť na intrák a na príjazdovej ceste k nemu, som mala chuť hopkať tak som hopkala a on mi povedal, že sa mu páči moja radosť. 

     A keď som sa pozrela do zrkadla, mala som jahodové pery.

 
