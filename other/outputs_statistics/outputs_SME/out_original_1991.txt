
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nikolaj Vyšnikov
                                        &gt;
                Slovensko
                     
                 Príručka mladého pravičiara 

        
            
                                    25.12.2009
            o
            13:53
                        |
            Karma článku:
                11.50
            |
            Prečítané 
            5754-krát
                    
         
     
         
             

                 
                    Všetci ich uznávame, lebo sú čerešničkou spoločnosti
                 

                Kvalitné vzdelávanie mladých pravičiarov predstavuje fundamentálny prvok ich vývoja. Už v útlom veku netreba zaháľať, lebo detičky majú tendenciu rozvíjať svoje city. Nie, to nemôže byť prirodzenosťou človeka. Za to môžu len a len komunisti, ktorí číhajú v každom rohu a ich jediným cieľom je kazenie našej mládeže (pozn. autora: zrejme pridávaním niečoho do pitnej vody). City treba poraziť. Deťom treba jasne vysvetliť, že takto sa proste nedostanú do neba. Hayekoanjeli ich vyženú do pekiel pekelných, ktorým šéfuje kto iný ako ... ťažko sa mi to vôbec vyslovuje ... Fidel Castro (pozn. autora: brrr).    V prvých rokoch života poslúži starý dobrý kapitalista Santa. Celá schéma obchodníckych sviatkov je veľmi šikovným ťahúňom pravice. Stanovuje jasné pravidlá: budeš počúvať – budú darčeky. Budeš sa celý život naháňať za peniazmi – tvoje deti budú mať viacej darčekov. Spoločne s propagandistickým tlakom reklamy a spoločnosti vytvárame armádu od konzumu závislej generácie. Pravičiarska utópia? Nie, tvrdá realita. Čím viacej ľudia prepadnú tejto mánii, tým viacej zarobia špekulanti v pohodlných kreslách s cigarou v jednej ruke a whiskey v druhej ruke.    Deti treba zlomiť čo najefektívnejšie. Musia dospieť do štádia, keď im nebudú vadiť masové hroby plné ľavičiarov pozabíjaných pravicovými eskadrami smrti, ale akonáhle niekto vysloví čo i len najmenšiu ľavicovú myšlienku, tak musia škrípať zubami. To chce silný žalúdok, aby niekto ignoroval zverstvá, ktoré vo svete vykonávala a vykonáva pravica. V tom lepšom prípade sa o nich mladý pravičiar ani nedozvie. Čím je človek menej informovaný, tým je lepším pravičiarom. V ignorantstve je sila.    Mladý pravičiar si musí dávať veľký pozor na to, čo číta. Dovolené smie mať len osvedčené pravicové denníky, týždenníky a televízne programy. Rádio nech radšej nepočúva. V poslednom čase je tam priveľa liberálneho odpadu. Každý správny pravičiar je šíriteľom pravdy. Celý svet raz pochopí, že ľudia ako Bush, Reagan, Pinochet atď. boli vlastne spasiteľmi, ktorí nás chceli ochrániť pred inváziou červených myšlienok. Pred ľuďmi, ktorí by jedli naše deti, otrávili polia a znefunkčnili stroje.    Zmyslom života mladého pravičiara sú diskusie. Vtedy ide aj hromadenie bohatstva bokom. Úspech závisí od dokonalého naučenia sa niekoľkých poučiek. Šikovný mladý pravičiar musí ovládať dve poučky pre každú tému. Bohate to stačí, lebo títo machri sú čerešničkou spoločnosti. Akonáhle však diskusia postúpi do väčšej hĺbky, v hlavách mladých pravičiarov dochádza ku system erroru, totálnemu preťaženiu a v tej chvíli vypomôžu už len osobné útoky (často postavené na sklone ku eštébáctvu). Najjednoduchšie je však označiť oponenta za komunistu, spraviť si čiarku na klávesnici a cítiť sa úžasne.    Tak nezabúdajte:    Ľudské práva sa porušujú len  v Číne a na Kube. Počty mŕtvych názorových oponentov sú iba štatistikou. Ku znečisťovaniu životného prostredia vôbec nedochádza. Rapídny nárast spotreby po celom svete je plne udržateľný. Je rozumné píliť konár, na ktorom sedíme. Médiá sa idú zodrať, aby nám poskytli pravdivé informácie. Autocenzúra je pojem zo science fiction. Chudoba je indikátorom lenivosti. Ľavičiarske knihy horia lepšie ako benzín. War is peace, freedom is slavery... Cisár nie je nahý. Má na sebe sako za tisíce eur a reťaze zo zlata.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (217)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Prečo končím s blogovaním
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Komunistická hrozba v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Legalizácia marihuany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Aj v Iraku rastú dubáky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Reforma manažmentu krajiny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nikolaj Vyšnikov
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nikolaj Vyšnikov
            
         
        vysnikov.blog.sme.sk (rss)
         
                                     
     
        Nesympatizujem ani s jednou slovenskou politickou stranou.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    41
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1574
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Zahraničie
                        
                     
                                     
                        
                            Zelená revolúcia
                        
                     
                                     
                        
                            foto
                        
                     
                                     
                        
                            Ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prečo kríž a nie radšej cesnak?
                                     
                                                                             
                                            Pred 60 rokmi sa v čase Povstania zrodil týždenník Nové slovo
                                     
                                                                             
                                            Rozprášená ľavica
                                     
                                                                             
                                            Posudzovanie vplyvov na životné prostredie je trápne
                                     
                                                                             
                                            VŠE – líheň nové generace
                                     
                                                                             
                                            Prvý verzus druhý pilier
                                     
                                                                             
                                            rozhovor - Rado Geist
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Lukáš Zorád
                                     
                                                                             
                                            Jano Topercer
                                     
                                                                             
                                            Peter Daubner
                                     
                                                                             
                                            Daniel Hromada
                                     
                                                                             
                                            Soňa Svoreňová
                                     
                                                                             
                                            Juraj Draxler
                                     
                                                                             
                                            Monika Martišková
                                     
                                                                             
                                            Jaroslav Hrenák
                                     
                                                                             
                                            Michal Feik
                                     
                                                                             
                                            Milan Vaňko
                                     
                                                                             
                                            Viliam Búr
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Kamil Kandalaft
                                     
                                                                             
                                            Jan K. Myšľanov
                                     
                                                                             
                                            Peter Weisenbacher
                                     
                                                                             
                                            Eduard Chmelár - aktualne.sk
                                     
                                                                             
                                            Eduard Chmelár - sme.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Al Jazeera
                                     
                                                                             
                                            CEPA   SPZ   VLK
                                     
                                                                             
                                            Guardian
                                     
                                                                             
                                            BBC NEWS
                                     
                                                                             
                                            Ekoporadňa
                                     
                                                                             
                                            Earthlings - VIDEO, en
                                     
                                                                             
                                            EurActiv.sk - všetko o EÚ
                                     
                                                                             
                                            Stiglitz o globalizácii - VIDEO, en
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




