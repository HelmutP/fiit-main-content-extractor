
 „Máš pravdu. Keď som sa s tebou naposledy takto normálne rozprával, mali sme asi dokopy v ústach desať zubov a našou hlavnou témou bolo ´Aký koláčik z piesku spravíme.´“
„Presne si to vystihol.“  zasmiala som sa „Kedy si sa vrátil domov ?“
„Prišiel som asi pred týždňom. Počasie ma milo prekvapilo. V Londýne bolo asi tak maximálne osem stupňov a tu mi dvadsaťosem totálne vyhovuje.“
„To si tu mal byť ešte v máji. Bolo neskutočne teplo.  Ty si už školu skončil ?“
„Áno, nechali ma odísť skôr.“
„A ja to mám ešte do stredy.“ vzdychla som si pri predstave, že musím ešte pár krát tak skoro ráno vstávať.
„To zvládneš.  Zvládala si to desať mesiacov, tak to predsa nevzdáš teraz .“ ten človek ma vždy dokáže optimisticky naladiť.
„Čo budeme robiť v lete ? Teda, ty sa kedy vraciaš domov ?“ radšej som zmenila tému. Nemám rada, ak sa všetko točí iba okolo mňa.
„Vraciam sa v polovičke augusta. A čo budeme robiť v lete ?!“ položil otázku sám sebe „To ani ešte presne neviem, ale my dvaja sa zabavíme. Ehm, teda traja.“ zasmial sa a ja som si až teraz všimla, že so sebou má aj svojho psa. Jeho anglická doga Salma bola aj môj miláčik. Poškrabkala som ju medzi ušami a ona sa mi za to odvďačila spokojným pohľadom.
„Vždy ťa mala radšej ako mňa. Asi by si sa o ňu mala starať ty.“ žartoval.
„Ale prosím ťa ! Akoby som doma nemala dosť zvierat.“ iba jedno. Brata.
„Nemáš zvieratá. Adam ?“ takmer okamžite uhádol.
„Presne tak. Mám dosť práce aj s ním a to má jedenásť.“ obaja sme sa zasmiali. „Kam vlastne ideš ?“
„Idem ťa odprevadiť domov. Vlastne, ideme ťa odprevadiť.“ Salma ho trochu potiahla dopredu a takmer spadol.
„Hovorím ti, určite sa nestratí ak ju necháš na chvíľočku sa vybehať.“  presadzovala som svoje. Aj ja som mala psa a vždy som ho nechávala bez obojku.
„Hej ale ona ... vždy si robí, čo chce. Ak ju raz pustím, asi sa už domov nevráti.“ usmial sa .
„Och muži ! Neprejdeme sa až na pláž ?“ navrhla so mu.
„Nerobí nám to problém.“
„Fájn . A dovoľ mi niečo vyskúšať, prosím.“ nahodila som ´angel face´.
„Som v nevýhode , lebo vieš , že to na mňa zaberá. No dobre, čo chceš ?“
„Hádzať jej drevo. Bez obojku.“ použila som najmilší úsmev, akého som bola schopná .
„Hm ....“ zdalo sa, že nad tým naozaj rozmýšľa , „Fájn.“ podal mi jej obojok.
„Ďakujem, ďakujem, ďakujem.“ hneď som jej obojok odopla a hodila drevo, ktoré som našla. Sama sa ku mne bez problémov vrátil aj s drevom, ktoré som jej dookola hádzala.
„Vidíš ?! Občas nezaškodí, dať na moje rady.“ hovorila som mu, kým Salma bežala po drevo až do vody.
„Áno , áno.“ zrazu bol pri mne a zobral ma na ruky. Bežal so mnou až do vody , chcel ma do nej hodiť, ale môj krik a kopance zmenili jeho názor.
„Toto mi už nikdy nerob !“ vykoktala som sa , pokým som mala záchvat smiechu.
„Budem na to myslieť.“ hovoril len tak. Chytil ma za ruku a v druhej držal obojok. Salma k nám pribehla, ale Lukáš za už neobťažoval zapínať jej ho.
„Ďakujem za pekný deň.“ povedala som mu, keď som už bola pred dverami nášho domu.
„To nestojí za reč.“ povedal a odkráčal so svojím psom. 

Zobudila som sa a mi tiekli slzy. Zase sa mi sníval ten sen. Sen , v ktorom Lukáš ešte stále žije.


El Barça
 
