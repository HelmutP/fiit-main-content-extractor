

    
    
 Včera popoludní prenikli na verejnosť tajné plány na oslobodenie dvoch švajčiarskych rukojemníkov zadržiavaných v Líbyi. Vzťahy vo vnútri švajčiarskej vlády sú napäté, ministerka zahraničných vecí Micheline Calmy-Rey (SP)[1] a minister financií Hans-Rudolf Merz (FDP)[2] sú v otvorenom spore. Merz v piatok obvinil svoju kolegyňu z plánovania riskantnej záchrannej operácie, pred jeho cestou do Líbye. Calmy-Rey nechápe útok kolegu zo Spolkovej rady, pretože samotná iniciatíva na záchranu rukojemníkov vychádzala s diskusie s ozbrojenými zložkami. Za svojim rozhodnutím si ministerka zahraničia pevne stojí. „Keď Max Göldi[3] objal svoju manželku na letisku v Zürichu, bolo už všetko v poriadku.“: povedala ministerka. Zajatí Švajčiari sa 14. júna vrátili do vlasti.  
  Ministerka zahraničia aj napriek jej ubezpečeniam, že šlo iba o krajné riešenie problému, čelí obrovskej vlne kritiky, odporcovia riskantného plánu žiadajú jej „hlavu“. Odmietajú tiež záchranu rukojemníkov použitím sily a chcú naďalej presadzovať diplomatické riešenie sporov s Líbyou. Iniciatívu ministerstva zahraničných vecí bude prešetrovať bezpečnostný výbor Národnej rady. 
 Riskantný plán v štýle Ramba  
  Podľa zverejnených informácií plánovalo švajčiarske ministerstvo zahraničných vecí oslobodiť svojich občanov pomocou špeciálnej jednotky Švajčiarskej armády. Pri úteku Švajčiari plánovali použiť tri možnosti úniku. Prvou možnosťou bola cesta vozidlami cez púšť do Nigeru alebo Alžirska, druhým plánovaným variantom bolo využitie diplomatických lietadiel a tretím plánovaným prostriedkom bolo využitie ponorky.  
 Reakcia Líbye 
  Podľa saudského denníka Ash-Sharq al-Awsat sa jeden z vysokých vládny predstaviteľov Líbye ohradil voči agresívnemu plánu Švajčiarska, ktoré nemá žiadne právo napadnúť líbyjské ciele. Obe strany sa zhodujú v tom, že takéto správy ešte zhoršia už tak dosť napätú situáciu. 
 Zdroj: http://www.tagesschau.sf.tv/Nachrichten/Archiv/2010/06/20/Schweiz/Libyen-sorgt-fuer-Zuendstoff-im-Bundesrat 
   
   
 
  
 

 
 [1] SP – Sociálno-demokratická strana Švajčiarska 
 
 
 [2] FDP – Liberálna strana Švajčiarska  
 
 
 [3] Jeden zo zadržiavaných švajčiarskych občanov 
 
 
   

