
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magdaléna Stýblová
                                        &gt;
                Nezaradené
                     
                 Koniec dobrý, všetko dobré... 

        
            
                                    24.5.2010
            o
            12:15
                        (upravené
                24.5.2010
                o
                13:05)
                        |
            Karma článku:
                2.48
            |
            Prečítané 
            624-krát
                    
         
     
         
             

                 
                    Koncert nabitý energiou.
                 

                   Krásne počasie, pokojná nedeľa a záverečný koncert festivalu Ars Organi. Kostolom sv. Ladislava v Nitre sa niesla neopísateľná atmosféra. Na jednej strane si uvedomujete, že fáza čosi pekného a hodnotného končí, na strane druhej si vychutnávate posledný koncert 3. ročníka tohto úžasného festivalu. Predstavil sa nám vynikajúci organista z Čiech, Pavel Kohout.    Ked' som sledovala pokrok českých hokejistov, bála som sa, že koncert ukončí v polovici a pôjde držať palce... Nestalo sa tak, a bravúrne nám predstavil diela J.S. Bacha, F.Liszta, W. Albrighta či K.J.Muldera. Interpretácia diel J.S. Bacha bola príjemná, nenútená a farebne veľmi pekná. Skladba Fantázia a fúga na B-A-C-H od F. Liszta je dokonalé dielo samo o sebe. Ked' sa k tomu pridá aj skvelé prevedenie, farebnosť registrov a atmosféra taká, že sa cítite ako v inom svete, potom niet čo dodať. Pri piane kostol ani nedýchal. Tie pocity sa, jednoducho, nedajú opísať, to treba prežiť. Dozneli posledné tóny a publikum si najprv vydýchlo a začalo nadšene tlieskať.   Záver koncertu bol humorný. William Albright skomponoval Sweet Sixteenths: A Concert Rag for Organ, ktorého interpretáciu sme mali možnosť počuť. Ako som sa dozvedela, mnohí šesťdesatnici melódiu poznali hádam ešte z dávnych prenosov krasokorčuľovania, no v prevedení organovom to bola novota. Ďalšia "rana" pre konzervatívnejších bola skladba Orange Fantasy od K.J.Muldera. Až úsmevná kombinácia známych melódii vo veľmi modernom prevedení. Mňa si posledné skladby získali. Síce som sa stretla aj s názormi, že novinky takéhoto žánru pre organ nie sú na mieste. Obávam sa, že to pramení zo slovenskej "nátury" ťažko prijímať novoty, a preto konzervatívcom na Ars Organi odpúšťam. Avšak, nič nemení fakt, že záverečný koncert bol skvelý, po všetkcýh stránkach.    Čo dodať na záver? Finis Coronat Opus. Záver festivalu nám dokázal svoje kvality, a pevne verím, že sa o rok stretneme. Ešte dlho budeme na tento ročník spomínať v dobrom. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Naivných. Koľko nás v skutočnosti je?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Kultúrne Slovensko časť 2. alebo ako sa mi rozum zastavil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            "Kultúrne Slovensko" časť 1.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Dovolenka inak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Zdravotníctvo šokuje
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magdaléna Stýblová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magdaléna Stýblová
            
         
        styblova.blog.sme.sk (rss)
         
                                     
     
        ponáhľajme sa milovať ľudí, pretože rýchlo odchádzajú...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    810
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na malomeštiakovej svadbe v SND
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




