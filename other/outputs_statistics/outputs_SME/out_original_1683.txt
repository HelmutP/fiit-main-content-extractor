
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Emília Katriňáková
                                        &gt;
                Občasník ženy
                     
                 Viete do koľkých dní musíte predložiť začiatok "OČRky"? 

        
            
                                    29.6.2010
            o
            22:51
                        |
            Karma článku:
                10.37
            |
            Prečítané 
            3768-krát
                    
         
     
         
             

                 
                    Myslím, predložiť ju svojmu zamestnávateľovi. Pretože ja to fakt neviem.
                 

                 Nikdy som sa nad tým nezamýšľala. Pri predchádzajúcich OČR, ten papier o začiatku zaniesol môjmu zamestnávateľovi manžel. Lenže teraz sa situácia skomplikovala. Manžel odcestovaný, ja sama s chorým dieťaťom. Čo robiť? Koľko mám času?   Nahodila som teda S.O.S. status na facebooku. Nech poradia. Odpovede sa hrnuli... "čo najskôr! Do 3 dní. Ešte v ten deň, tuším..."   Mne nestačí tušiť, ja to chcem vedieť na isto!   V prvý moment som sa na papiere vykašlala, musela som predsa ratovať dcéru. To viete, horúčky. Ale priznám sa, dobrovoľne a bez mučenia, stále mi vŕtalo hlavou. Čo ak niečo podcením, čo ak nedodržím lehotu a zamestnávateľ to vezme ako porušenie pracovnej disciplíny? Panebože, hotové katastrofické scénare!   "Zuza, nerob paniku, pošli to zajtra poštou", nechal sa počuť môj šéf v deň vystavenia OČRky.   Pche, ešte to tak, aby som sa s dieťaťom levitujúcim medzi vedomím a takmer bezvedomím trepala na poštu! Veď by ma vďační ľudkovia okamžite nahlásili sociálke, že hazardujem s jej zdravím...   Ako som malej menila zábaly, surfovala som po sieti a vyhľadávala. Hrabala som sa v zákone o sociálnom poistení, v zákonníku práce, nikde nič. O lehotách ani stopa. Isto, možno som nehľadala na správnom mieste. Nechala som si teda "vygoogliť" do koľkých dní doručiť žiadosť o ošetrovné. Vačšinou mi vyhodilo:   poistenec je povinný príslušnej pobočke preukázať skutočnosti rozhodujúce na vznik, trvanie a na zánik nároku na dávku, nároku na jej výplatu a jej sumu (po písomnej výzve Sociálnej poisťovne povinnosť preukázať tieto skutočnosti do 8 dní odo dňa doručenia výzvy, ak neurčila inú lehotu)    
príjemca dávky je povinný do 8 dní oznámiť príslušnej pobočke zmenu v skutočnostiach rozhodujúcich na trvanie nároku na dávku, nároku na jej výplatu a jej sumu    Ale ja potrebujem lehotu doručenia zamestnávateľovi...   Napísala som teda aj do internetovej poradne, kde mi ešte v ten deň odpísali: "zamestnanec pred uplatnením nároku na ošetrovné predloží svojmu zamestnávateľovi žiadosť o ošetrovné, ktorý žiadosť vypíše a podpíše potvrdenie zamestnávateľa. Zamestnávateľ musí predložiť doklady pobočke Sociálnej poisťovne do 3, až 5.dňa následujúceho kalendárneho mesiaca. Každý zamestnávateľ má svoje interné predpisy, ktoré musí dodržiavať zamestnanec, preto je potrebné na personálnom oddelení Vášho zamestnávateľa telefonicky nahlásiť spôsob doručenia tlačiva na uplatnenie si nároku na ošetrovné. Ak by ste boli dobrovoľne nemocensky poistená, tak  žiadosť o ošetrovné je potredné hneď nahlásiť pobočke Sociálnej poisťovne, ktorá vykonáva nemocenské poistenie. Zvolili ste správny spôsob, že ste aspoň telefonicky oznámili svojmu zamestnávateľovi, že máte OČR."   A keďže som občas Zuza, čo robí paniku... úpenlivo som sa počas noci modlila o zázrak, nech horúčky klesnú... aby som sa ešte dnes mohla trepať na poštu.   A teraz nebudem pokojne spávať, pri spomienke, že moja kámoška do dnešného dňa nedostala môj list, ktorý som jej poslala ešte keď som študovala na gymnáziu! Utešuje ma len istota, že si pošta dá záležať, pretože doporučený list zaslaný prvou triedou nemôže len tak zapotrošiť. Sú v ňom predsa moje peniaze!    A viete čo mi ešte nedá pokoj? Dňa 3.6.2010 mi sociálna poisťovňa poslala rozhodnutie, v ktorom mi priznáva ošetrovné. Schuti som sa zasmiala, pretože ten list mi prišiel v čase, keď už ošetrovné bolo pripísané na účet. Vŕta mi v hlave aj to, kde dodnes leží list - rozhodnutie o priznaní ošetrovného z úplne prvej OČR. Kebyže mi list z 3.6.2010 nepríde do schránky, ani neviem, že niečo také posielajú. Hoci, myslím si, že ide o zbytočné papierovanie. Základ je, že peniaze boli pripísané na účet! Koľko by toho stihli urobiť, keby nevypisovali zbytočné rozhodnutia. A ak ich vypíšu, prečo ich nezašlú... no čák? Ledaže ho zaslali, a lajdácka pošta nedoručila! Panebože, zasa ma chytá panika! Som ja ale ... idem radšej spať.   P.S.: Čestne prehlasujem, že si pri nástupe do práce naštudujem naše interné predpisy pojednávajúce o doručení toho nešťastného papiera, aby som nerobila paniku ako taká Zuza.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Emília Katriňáková 
                                        
                                            Otvorený list Európskej centrálnej banke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Emília Katriňáková 
                                        
                                            Somrák v trolejbuse
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Emília Katriňáková 
                                        
                                            Som nesvojprávna a on má frajerku a chce si ju vziať...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Emília Katriňáková 
                                        
                                            Truhlica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Emília Katriňáková 
                                        
                                            Mačky a psi a Playboy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Emília Katriňáková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Emília Katriňáková
            
         
        katrinakova.blog.sme.sk (rss)
         
                                     
     
        Jednoducho ja.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    167
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2021
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Panoptikum
                        
                     
                                     
                        
                            Občasník ženy
                        
                     
                                     
                        
                            Občasník matky
                        
                     
                                     
                        
                            Občasník manželky
                        
                     
                                     
                        
                            Čo ma páli
                        
                     
                                     
                        
                            Spomienky
                        
                     
                                     
                        
                            Súkromie
                        
                     
                                     
                        
                            Viera
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            hmmm...
                                     
                                                                             
                                            Diabol existuje...
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jozef Mikloško - Ako sme boli malí - odtajnené po 60 rokoch
                                     
                                                                             
                                            Jozef Kron - Ozveny ticha
                                     
                                                                             
                                            Nataša Tánska - Puf a Muf
                                     
                                                                             
                                            Elias Vella - Ježiš, môj uzdravovateľ
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Szidi Tobias
                                     
                                                                             
                                            SĽUK - Spievanky, spievanky
                                     
                                                                             
                                            Soundtrack ONCE
                                     
                                                                             
                                            Čanaky &amp; Podhradská - Deťom
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martin Grecula
                                     
                                                                             
                                            Ladislav Lencz R.I.P.
                                     
                                                                             
                                            Janko Urda
                                     
                                                                             
                                            Vlasta
                                     
                                                                             
                                            Danka Nemčoková
                                     
                                                                             
                                            pohladenie duše Šefčík
                                     
                                                                             
                                            Kathrin
                                     
                                                                             
                                            Vierka Mikulská
                                     
                                                                             
                                            Jozef Mikloško
                                     
                                                                             
                                            Mika Polohová
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Marcela Bagínová
                                     
                                                                             
                                            Riko
                                     
                                                                             
                                            my husband´s  blog
                                     
                                                                             
                                            Amigo R.I.P.
                                     
                                                                             
                                            Človečina
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




