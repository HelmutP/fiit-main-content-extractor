

 Uprostred čarokrásnej toskánskej krajiny, 40 kilometrov severozápadne od Sieny, sa na 334-metrovom kopci rozpriestiera mestečko San Gimignano s krásnymi kamennými vežami z 11.-13. storočia. Mestečko vzniklo v 10. storočí z pôvodne etruskej dediny z 3. storočia pred n.l. Veže boli obytné a zároveň plnili obrannú funkciu, jednalo sa o rodové veže. Neskôr bolo okolo mesta vybudované opevnenie, veže prestávali plniť pôvodnú funkciu, stavali sa aj iné obytné budovy a infraštruktúra. V roku 1353 vtedy významné mesto postihla epidémia moru, po ktorej význam mesta upadol. Dnešné San Gimignano žije z turistického ruchu, vína a šafránu, ktorý sa tu pestuje od stredoveku v biologicky najčistejšej kvalite. 
  
  
  
  
  
  
 S bratom Milanom sme vystúpili na najvyššiu - mestskú vežu za 4 eura, súčasťou vstupného je aj prehliadka mestského múzea. Na vrchole veže sú 3 zvony a výhliadková terasa cca 5x5 metrov s hrubým meter vysokým múrom okolo. 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 Z veže sme si vyhliadli najlepší kopec v okolí, z ktorého bude najkrašie viditeľná panoráma mesta. Po opustení mesta sme si na vytipovaný kopec vyšli a myslím, že sa nám to vyplatilo. 
  
  
  

