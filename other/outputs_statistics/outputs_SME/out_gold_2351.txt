

 Všimol si, že na blogu Martina Poliačika (tu), ktorý je člen republiblikovej rady strany Sloboda a Solidarita je viackrát uvedené celé logo tejto strany vo fotografii k článku, čo je v rozpore s kódexom blogera. 
 Martin Poliačik porušuje kódex blogera na blog.sme.sk v bode 6, ktorý sa venuje politikom, verejne činným osobám a kandidátom na verejné funkcie. 
 V bode 6, odseku 1 sa píše: 
 Je to váš osobný blog. Nepatria naň oficiálne vyhlásenia, logo strany, volebné slogany ani tlačové správy. Patria naň vaše osobné názory, komentáre, postrehy. 
 Dúfam, že po prečítaní tohto odkazu Martin Poliačik prestane porušovať kódex blogera a vedenie SME, resp. ľudia majúci na starosti sekciu blog.sme.sk budú dôslednejšie sledovať takéto politické porušovanie kódexu, ktoré sa s prichádzajúcimi voľbami môže znásobiť. 
 Alebo žeby takéto porušovanie kódexu bolo tichou dohodou medzi adminmi blog.sme.sk a členmi SaS povolené? 
   
 P.S. Členom SaS... zvykajte si na kritiku :) 
   

