
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Fotoreportáže
                     
                 Cigánska osada Jaruška 

        
            
                                    25.8.2009
            o
            18:00
                        |
            Karma článku:
                11.02
            |
            Prečítané 
            7016-krát
                    
         
     
         
             

                 
                    Po minuloročnom požiari chatrčí v cigánskej osade Jaruška, 400 metrov za obcou Úbrež, obec zabezpečila pohorelcom náhradné bývanie v 12 čiastočne zariadených prenosných domčekoch, osadených na prístupovej ceste k osade. Bol som zvedavý, v akom stave sú domčeky po ôsmych mesiacoch, nakoľko sú „vybývané", preto som sa vybral do osady spolu so starostom, komunitnou sociálnou pracovníčkou, jej rómskou asistentkou a fotoaparátom.
                 

                    Musím konštatovať, že som bol milo prekvapený. Domčeky nepoškodené, celkom slušne zariadené a okolo plotmi na cigánsky spôsob ohradené malé dvory.                           Cigánčatá sa veľmi radi fotografujú ...                           ... a predvádzajú.      Majú aj svojho holiča.      Starosta - zverolekár poskytol radu, čo si počať s boľavým zubom.      Išli sme sa pozrieť aj do starej časti osady.         Jedna z najstarších Cigánok Magda sa mi pochválila svojím chovom husí.      Pozvala ma aj do domu, že vraj môžem fotiť, čo chcem ...      ... tak som fotil.      V druhej izbe bez okien bola tma, fotil som s bleskom, ani som poriadne nevedel čo.      Až doma som zistil, že som nafotil Magdinho syna pri obedňajšej sieste po sociálnych dávkach.      Navštívil som aj najstaršiu Cigánku, 78-ročnú Boženu (najstarší Cigán má 58), tá ma ale do domu nepozvala.      Ešte som vyfotil 3 najstaršie Cigánky pod krížom a išli sme domov.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




