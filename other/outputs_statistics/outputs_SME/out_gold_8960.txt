

 Z pragmatických dôvodov som výsledky konsolidácie začlenil do 4 rôznych kategórií – úplna zhoda, 3 zo 4, polovičná zhoda, protichodné názory. Znamená to, že každý bod virtuálneho programu bol začlenený do konkrétnej kategórie poďla toho, koľko strán uviedlo daný bod vo svojom programe. Napríklad Konsolidácia verejných financií má oporu u všetkých štyroch strán. 
 Kategória Protichodné názory zastrešuje tie body programov, kde aspoň dve strany majú opačný názor na realizáciu takýchto bodov. Príkladom takéhoto bodu je Plošné zrušenie poľnohospodárskych dotácií. 
  
    
 Vládny program zostavený z aktivít úplnej zhody by bol krátky 
 Primárnym cieľom novej vlády SDKÚ-DS, SaS, KDH a MOST-HÍD by bola konsolidácia verejných financií. Strany sú za to, aby bol prijatý Ústavný zákon o fiškálnom pravidle pre hrubý verejný dlh. 
 Vládna aliancia by sa vo svojom programe sústredila na Reformu odvodov. Všetky strany uvádzajú rekonštrukciu II. piliera starobných dochodkov. 
 Podpora výstavby nájomných bytov pre mladé rodiny rezonuje vo všetkých volebných programoch.  
 Ďalším bodom programu Vládnej aliancie by bola revízia Zákonníka práce. Konkrétne kroky, na ktorých sa všetci zhodujú súvisia s obmedzením postavenia a právomocí odborov v prípadoch, ktoré spadajú do priameho hospodárskeho riadenia firmy (napr. určovanie začiatkov a koncov pracovnej smeny) a zrušiť povinnosti zamestnávateľov poskytovať im služby a priestory. 
 Bývalý predseda vlády, Róbert Fico sa obával, že nova vláda zastaví dialnice. Táto jeho obava nie je na mieste, lebo Diaľnice figurujú ako bod programu každej zo strán Vládnej aliancie. 
 Dôležitým bodom posilnenia transparentnosti a boja proti korupcii v programe novej Vládnej aliancie bude spustenie prehľadného centrálneho vestníka verejného obstarávania na internete, v ktorom bude môcť verejnosť jednoducho vyhľadávať všetky prebiehajúce zákazky podľa rôznych kritérií. S tým úzko súvisí povinnosť pre verejnú správu a samosprávu využívať transparentné elektronické aukcie. 
 Posledným bodom programu, ktorí uvádzajú všetky strany vo svojich volebných programoch by bolo zavedenie výhradne elektronických, resp. bezhotovostných pokút. 
 Protichodné názory strán Vládnej aliancie: 
 
 Zrušenie výnimiek pri dani zpríjmov azároveň vytváranie výnimiek pre novomanželov, rodiny sdeťmi, starobných dôchodcov, ... 
 Prenesenie platieb odvodov na zamestnancov alebo na zamestnávateľov 
 Zrušiť investičné stimuly bez náhrady alebo revidovať investičné stimuly pre investorov a malých astredných podnikateľov 
 Projekt širokorozchodnej železnice 
 Plošné zrušenie poľnohospodárskych dotácií. 
 Centrum kultúry a umenia alebo Ministerstvo Kultúry SR 
 Obmedziť kontakty na nevyhnutný diplomatický styk a neuprednostniť krátkodobé a často veľmi sporné komerčné výhody pred principiálnym postojom vo vzťahu k predstaviteľom totalitných režimov alebo hospodárska, vedecká a kultúrna spolupráca sČínou 
 Postavenie afinancovanie cirkví 
 Registrované partnerstvá 
 Dekriminalizácia ľahkých drog 
 Slovensko-maďarské vzťahy 
 
   
 Rozšírený program Vládnej aliancie - prevažná alebo polovičná zhoda 
 Spomenul som už, že keď jedna strana neuvedie konkrétny krok vo svojom volebnom programe ešte to neznamená, že bude proti takémuto kroku vystupovať. Uvediem teda ďalšie body virtuálneho programu Vládnej aliancie, ktoré majú v stranách väčšinovú alebo aspoň polovičnú podporu, t.j. aspoň dve alebo tri strany uviedli daný bod vo svojom volebnom programe 
   
 Verejné financie, dane a odvody 
 Tri zo štyroch strán sa zhodujú na zavedení Informačného minima o rozpočte verejných financií a štátnom záverečnom účte. Rovnaká zhoda panuje aj v znížení počtu členov vládneho kabinetu. 
 Dve zo štyroch strán navrhujú podrobné sledovanie čistého bohatstva Slovenskej republiky, teda rozdiel medzi celkovými aktívami a pasívami štátu. Zároveň navrhujú záviesť výdavkové stropy, čím bude vláda vždy nútená rozhodovať o tom, ako rozdelí vopred definovaný objem zdrojov medzi rôzne priority. 
 Polovica Vládnej aliancie ďalej navrhuje: 
 - vytvoriť samostatnú nezávislú inštitúciu na kontrolu vývoja verejných financií a informovanie verejnosti 
 - obmedziť schvaľovanie legislatívnych a iných návrhov s negatívnym dopadom na verejné financie 
 - žiadne nové dane, ak to nevyplýva z medzinárodných záväzkov EÚ 
 - nezvyšovať dane 
 - vytvoriť systém finančnej decentralizácie smerom k samosprávam s možnosťou zvyšovať atraktívnosť podnikateľského prostredia v regiónoch a obciach 
   
 Čo sa týka daní a odvodov, tri zo štyroch strán navrhujú zrušiť viac ako 40 výnimiek a znížiť dane, čo je v súlade s návrhom radikálne znížiť odvodovo-daňové bremeno. 
 Väčšina Vládnej aliancie navrhuje zjednotiť vymeriavacie základy a definície platiteľov zdravotného a sociálneho poistenia a základ dane z príjmu fyzických osôb. 
 Zhoda v programoch u troch zo štyroch strán panuje aj v zavedení jednotného miesta na výber daní a odvodov v reformovaných súčasných daňových úradoch a zavedení spoločného ročného zúčtovania dane z príjmu a odvodov. 
 Dve zo štyroch strán navrhujú systém jednoročných licenčných poplatkov pre Živnostníkov, ktorí by odbúral administratívu. 
   
 Demokratická spoločnosť a osobné slobody 
 Obmedziť poslaneckú imunitu navrhujú tri strany Vládnej aliancie. Ďalej navrhujú povinnosť posudzovať zákony, o ktorých rokuje parlament, z hľadiska ich dopadov na podnikateľské prostredie a vytváranie priestoru pre korupciu. Do tretice by chceli zaviesť elektronickú formu administratívnych úkonov medzi občanmi, podnikateľmi a orgánmi verejnej správy. 
 Polovica predstaviteľov Vládnej aliancie bude podporovať rýchle rozširovanie širokopásmového internetového prepojenia pre celé územie Slovenska. Taktiež navrhujú elektronický legislatívny proces a zákon o dobrovoľníctve, ktorý by mal pomôcť rozvíjať tretí sektor. 
   
 Boj proti korupcii, polícia a poriadok 
 Väčšina strán Vládnej aliancie sa zhoduje na týchto návrhoch: 
 
 povinnosť všetkých verejných organizácií zverejňovať všetky platné obchodné zmluvy. 
 zakázať orgánom verejnej správy uzatvárať zmluvy podliehajúce obchodnému tajomstvu  
 vytvoriť jednotný vzor majetkových priznaní pre všetky osoby s povinnosťou podávať majetkové priznania tak, aby majetkové priznania povinne obsahovali informácie o majetku verejným funkcionárom nielen vlastneným, ale aj užívaným.  
 
   
 Polovica strán chce prispieť do boja proti korupcii najmä dvomi opatreniami. 
 Uložiť Úradu pre verejné obstarávanie SR (ÚVO SR) povinnosť aktívne dohliadať na dodržiavanie zákona o verejnom obstarávaní aj na samosprávnej úrovni. V prípadoch, kedy bolo zistené porušenie zákona alebo preukázané nedostatky neboli odstránené v stanovenej lehote, bude ÚVO SR povinný voči nim bezodkladne iniciovať príslušné právne konanie. 
 Zaviesť do praxe nový internetový systém pre boj proti korupcii, s ktorým majú veľmi dobré skúsenosti napr. v Nemecku. Tento systém pridelí nahlasovateľovi korupčného prípadu jednoznačný, ale anonymný identifikátor, vďaka ktorému bude môcť udržiavať komunikáciu s orgánmi činnými v trestnom konaní počas celého vyšetrovania prípadu bez strachu z odhalenia jeho totožnosti. 
 Dve strany navrhujú rozšíriť využívanie kamerových systémov na zvýšenie bezpečnosti. 
   
 Súdnictvo a spravodlivosť 
 Program Vládnej aliancie by mal obsahovať body zamerané na zvýšenie kvality a odbornosti sudcov, keďže s týmto návrhom prichádza väčšina. 
 Zároveň by mal medzi prioritami figurovať Súdny manažment - opatrenia na zrýchlenie a zefektívnenie súdnych konaní. Dočkáme sa i povinného zverejňovania všetkých súdnych rozhodnutí na internete spolu s menami sudcov, ktorí sa na nich zúčastnili. 
 Súčasťou programu Vládnej aliancie by mohli byť aj dvojstranné návrhy zrýchlenia a skvalitnenia práce obchodných súdov ako i minimalizovanie obštrukcií v procesnom práve. 
 Polovica Vládnej aliancie navrhuje aj tieto opatrenia v súdnictve: 
 Zmeniť spôsob voľby Súdnej rady tak, že za členov bude môcť kandidovať aj bezúhonná osoba s vysokoškolským vzdelaním, ktorá nie je sudcom. 
 Zmeniť postavenie Súdenj rady, aby ani minister spravodlivosti, ani predseda Najvyššieho súdu, ani iná úzka skupina nemohla tento orgán ovládať. 
 Zriadiť telefónnu linku na hlásenie prípadov korupcie 
 Obchodný register aj konkurzný register (zoznam úpadcov) bude on-line na internete. 
 Podporiť občiansky monitoring súdov 
 Zverejňovať audiovizuálny záznam každého verejného súdneho procesu na internete. Takýto záznam bude zároveň aj prílohou k súdnemu spisu. 
 Zaviesť pravidelné komplexné hodnotenie sudcov, v ktorom sudcu nebudú hodnotiť len kolegovia 
 Zaviesť pravidlá proti zneužívaniu inštitútu doživotného menovania sudcov 
   
 Podnikateľské prostredie, hospodárstvo a eurofondy 
 Väčšina programov strán Vládnej aliancie sa zhoduje v dvoch bodoch súvisiacich s hospodárstvom 
 - Revidovať jednotlivé operačné programy v rámci Národného strategického referenčného rámca. Budovanie infraštruktúry neposkytovanej súkromným sektorom (napr. cesty, diaľnice, čističky odpadových vôd). 
 -  Podporovať rozvoj obnoviteľných zdrojov 
   
 Polovica strán prichádza s týmito návrhmi: 
 - zrušiť Živnostenský úrad a ešte predtým ho prepojiť s informačným systémom Obchodného registra 
 - zrevidovať kompetencie na kolektívne vyjednávanie medzi zamestnávateľmi a odbormi 
 - umožniť uzatvárať dobrovoľné vzájomné dohody medzi zamestnávateľmi a zamestnancami na riešenie operatívnych zmien pracovných podmienok, napr. úprava pracovného času, dočasné zvýšenie nadčasov, čerpanie voľna zamestnanca pri prekážke na strane zamestnávateľa 
 - zrušiť plošnú minimálnu mzdu a ponechať vyjednávanie o mzdách výlučne na dohodu zamestnanca a zamestnávateľa, resp. na kolektívne vyjednávanie 
 - jeden týždeň voľna pre otca do dvoch mesiacov od narodenia dieťaťa 
 - výnosy z predaja emisií využiť na zniženie energetickej náročnosti v domácnostiach 
 - sprivatizovať štátne podniky, napr. Cargo 
 - diverzifikovať naše energetické zdroje 
 - financovať výstavbu diaľníc z viacerých zdrojov vrátane štátnych dlhopisov, ktoré budú môcť kupovať aj dôchodkové správcovské spoločnosti (zdroje občanov uložené v 2. pilieri) 
   
 Kultúra, školstvo, veda a výskum 
 Školstvo, veda a výskum majú šancu pocítiť výraznú zmenu, keďže sa nova vládna štvorka zhoduje na viacerých návrhoch práve v tejto oblasti. Opatrenia, kde panuje zhoda troch zo štyroch strán" 
 
 spravodlivé hodnotenie žiakov prostredníctvom jednotných a štandardizovaných testov 
 dať školám slobodu vo výbere učebníc. Ministerstvo školstva bude viesť verejný zoznam učebníc, ktorých používanie je v súlade so Štátnym vzdelávacím programom 
 škola otvorená celý deň. Profesionálne vedenie mimovyučovacích aktivít a zvýšená motivácia škôl pre spoluprácu so základnými umeleckými školami či centrami voľného času 
 systém externého hodnotenia kvality slovenských škôl všetkých stupňov prostredníctvom nezávislej akreditovanej inštitúcie 
 účinné vnútorné aj vonkajšie formy sledovania kvality vysokých škôl 
 výrazna zmena pomeru financovania vedy a výskumu v prospech grantového systému  
 zjednotiť a sprehľadniť podmienky pre získanie grantov zo štrukturálnych fondov, znížiť ich byrokratickú náročnosť a zakotviť možnosť opravy formálnych chýb, aby tieto neboli dôvodom zamietnutia 
 odbremeniť žiadateľov o príspevky zo štátneho grantového systému od zbytočnej byrokracie a presunúť váhu pri hodnotení projektov od formálnych náležitostí ku kvalitatívnym parametrom. 
 zverejniť na internete všetky priznané granty v rámci Slovenskej republiky spolu s prihláškou, so súvisiacou kompletnou dokumentáciou a záverečnou audítorskou správou. 
 prizvať do grantových komisií významných nezávislých zahraničných vedcov a odborníkov a súčasne vyžiadať stanoviská renomovaných zahraničných grantových inštitúcií 
 
   
 V oblasti kultúry panuje zhoda v zrušení koncesionárskych poplatkov. 
   
 Konsenzus dvoch zo štyroch strán prevláda aj v týchto návrhoch pre základné a stredné školy: 
 
 Príjmy učiteľov zvyšovať rýchlejším tempom, ako budú rásť priemerné platy v slovenskej ekonomike.  
 Odstrániť zbytočnú byrokraciu 
 Finacovať všetky školy prostredníctvom systému normatívov, ako jediných príspevkov zo štátneho rozpočtu určených na vzdelávanie.  
 Všetky prostriedky na prácu s mládežou prerozdeľovať prostredníctvom navýšenia sumy na dnešné školské poukazy. Každé dieťa bude mať až do ukončenia strednej školy nárok na poukážku na ľubovoľnú voľnočasovú aktivitu. Túto si bude môcť uplatniť v ľubovoľnom športovom klube, centre voľného času či inej certifikovanej vzdelávaco-výchovnej inštitúcii. Certifikácie bude udeľovať ministerstvo školstva. 
 Rozšíriť športovanie na základných a stredných školách na jednu hodinu denne, aj s využitím popoludňajších hodín 
 
   
 Opatrenia, ktoré navrhujú dve zo štyroch strán v oblasti vysokého školstva: 
 - Do hodnotenia škôl zapojiť študentov 
 - Pre všetky tituly - od bakalárskych až po profesorské - zaviesť zákonný proces ich odobratia, ak boli získané podvodom 
 - Oddeliť financovanie vzdelávania na vysokých školách od financovania výskumu na týchto školách a vytvoriť transparentný a nekorupčný grantový systém na financovanie výskumu 
 -  Umožniť školám vyberanie školného a zabrániť nezákonnému vyberaniu školného na vysokých školách. 
 - Dať školám úplnú slobodu v nakladaní s príjmami z hospodárskej, či akejkoľvek inej činnosti podľa vlastného uváženia. 
 - Vnútorná organizácia vysokej školy a rozdelenie kompetencií medzi jej jednotlivé orgány musia byť vnútornou vecou každej vysokej školy 
   
 Veda a výskum by mali byť posilnené týmito návrhmi polovice predstaviteľov Vládnej aliancie: 
 
 Posilniť trend zvyšovania finančných objemov jednotlivých grantov na úkor počtu grantov a preferovať dlhodobejšiu realizáciu na 3 až 5 rokov 
 Presunúť prostriedky určené na aplikovaný výskum z rozpočtových kapitol rôznych ministerstiev pod Ministerstvo hospodárstva a všetky ostatné prostriedky pod Ministerstvo školstva 
 Fixovať prostriedky financovania inštitúcií a projektov základného a aplikovaného výskumu tak, aby mohli byť použité výlučne na vedu a výskum. 
 Podporu spoločných výskumných, inovačných a vzdelávacích aktivít realizovaných spoločne akademickou obcou a podnikmi. 
 
   
 V oblasti mediálneho trhu sa možno budú realizovať tieto opatrenia navrhované dvomi stranami: 
 
 Určiť percentuálnu časť HDP, ktorá bude schopná pokryť náklady na verejnoprávne médiá 
 Opraviť tlačový zákon, zrušiť právo na odpoveď a právo na dodatočné oznámenie  
 Nahradiť "povinnosť" vydavateľa zachovávať mlčanlivosť pri ochrane zdroja a obsahu informácií "bezvýhradným právom" na ochranu zdroja informácií 
 
   
 Sociálna politika 
 V sociálnej politike je hlavný dôraz kladený na rodiny, deti bez rodín a sociálne odkázané spoločenstvá. Väčšina Vládnej aliancie sa zhoduje v návrhu vytvoriť integrované školy, kde by pôsobil odborný personál pedagogicko-psychologických poradní a asistenti učiteľov s cieľom individuálne podporovať začlenenie detí do štandardných škôl. 
 Primárnym cieľom novej vlády by taktiež mali byť komunitné centrá, pričom o ich zriadenie bude môcť požiadať obec alebo akákoľvek iná entita - mimovládna organizácia, súkromník, firma a pod. Takéto centrá by boli financované z Europského Sociálneho Fondu (ESF) a príjmov z prevádzkovania materskej školy, voľnočasových aktivít a krúžkov. 
 Trojstranná zhoda panuje aj v otázke detských domovov a ich premeny na krízové centrá, kde budú deti len krátkodobo pred ich umiestnením do rodín. 
 Dve zo štyroch strán navrhujú urýchľovanie medzištátnej adopcie v prospech detí. Deti do 6 rokov by sa mali dostať výlučne do profesionálnej rodiny, pestúnskej starostlivosti alebo inej formy náhradnej rodiny. 
 Profesionálni rodičia by sa mali stať zamestnancami ÚPSVaR a nie detského domova. Podľa týchto strán je nutné podporiť tzv. špecializované pestúnstvo, t.j. pestúnov, starajúcich sa o znevýhodnené deti alebo deti so špeciálnymi potrebami. 
 Polovica strán rovnako navrhuje vytvoriť systém pracovníkov za finančnej podpory ESF pre prácu s rodičmi, ktorým deti neboli odobraté z dôvodov sexuálneho zneužívania, psychického alebo fyzického týrania dieťaťa. 
 V neposlednom rade by mohlo byť na programe dňa zjednotenie agendy reedukačných domovov ako súčasti náhradnej rodinnej starostlivosti jej presunutím pod MPSVR a zavedenie povinnej vnútornej i vonkajšiej diferenciácie reedukačných zariadení vytvorením ich jednotlivých typov. 
 Dve strany navrhujú zveriť dieťa do spoločnej, resp. striedavej výchovy oboch rodičov, ak je to v záujme dieťaťa a jeho potrieb. 
 Bodom programu polovice predstaviteľov novej vlády bude odstránenie prekážok, ktoré bránia vzniku a fungovaniu materských škôl a iných zariadení - bude vytvorený priestor pre výraznejší vstup súkromného a neziskového sektora. 
 Sociálne odkázané spoločenstvá by mali dostať pochôdzkárov na zvýšenie bezpečnosti. 
   
 Zdravotníctvo 
 Prienik programov strán Vládnej aliancie v oblasti zdravotníctva neexistuje, no polovica predstaviteľov navrhuje tieto 4 body: 
 
 Zadefinovať štandardy poskytovaných služieb vrátane základného balíka zdravotnej starostlivosti 
 Vytvoriť systém merania kvality zdravotnej starostlivosti 
 Zaviesť ochranný limit na spoluúčasť v zdravotníctve s maximálnou platbou pacienta (lieky, poplatky,...) 
 Nezávislosť Úradu pre dohľad nad zdravotnou starostlivosťou 
 
   
 Životné prostredie 
 Dve zo štyroch strán navrhujú jasné pravidlá medzi vlastníkmi lesov a ochranou prírody. 
 Polovica predstaviteľov Vládnej aliancie sa zameriava na stavebný zákon: 
 - Zaviesť trestnoprávnu zodpovednosť stavebníka za výstavbu bez právoplatných povolení alebo za nerešpektovanie ustanovení už vydaných povolení. 
 - Trestnoprávne zodpovední budú aj poskytovatelia regulovaných služieb pre výstavbu bez právoplatného povolenia. 
 - Stavebný úrad získa možnosť odpojiť čierneho stavebníka od vody alebo elektriny. 
 - Budu jasne zadefinované pravidlá pre zmeny stavieb pred dokončením. 
 - Zamedziť investíciám, ktoré by na úkor krátkodobého zisku obmedzenej skupiny obyvateľstva alebo konkrétnych podnikov spôsobili trvalé ujmy na životnom prostredí. 
 - Posilniť legislatívu o bezbariérovej výstavbe budov verejného určenia tak, aby bola záväzná pre všetky budovy využívané verejnosťou. 
   
 Zahraničná politika, obrana a bezpečnosť 
 Nosnými bodmi v zahraničnej politike podľa troch nových vládnych strán budú: 
 - Podporiť členstvo Chorvátska v Európskej únii 
 - Prehodnotiť význam a potrebu všetkých zahraničných zastúpení Slovenska a zrušiť tie, ktoré sú pre Slovensko zbytočné a neefektívne 
 Medzi prioritami nového Ministra Zahraničných vecí by ďalej mohli byť i návrhy dvoch strán Vládnej aliancie: 
 - Zabezpečiť, aby kritériami pri obsadzovaní pracovných pozícií boli odborná zdatnosť a morálna bezúhonnosť 
 - Odpolitizovať sporné otázky vo vzťahu s Maďarskom a preniesť ich do roviny hľadania vecných riešení. Zásadne zlepšiť vzťahy s Maďarskom je možné len dlhodobou čitateľnou politikou 
 - Posilniť po dohode s partnermi akcieschopnosť V4 vytvorením špecializovaných inštitúcií 
 - Spolupracovať pri formovaní spoločných politík EÚ s partnermi v rámci V4 a posilňovať postavenie SR, najmä pri diskusii o posilňovaní politiky východného partnerstva EÚ (Eastern Partnership), alebo o energetickej bezpečnosti a pri koordinácii poskytovania rozvojovej pomoci tretím krajinám 
 - Podporiť žiadosť Ukrajiny o vydávanie bezplatných schengenských víz a zjednodušiť migračnú politiku vo všetkých odôvodnených prípadoch 
 - Podporiť integráciu Turecka do európskych štruktúr, avšak len na úrovni špeciálneho pridruženého členstva s neúplnou účasťou v spoločných orgánoch EÚ, podľa modelu Európskeho hospodárkeho priestoru 

