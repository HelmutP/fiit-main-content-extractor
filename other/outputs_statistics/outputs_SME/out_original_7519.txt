
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Erik Kriššák
                                        &gt;
                Poézia
                     
                 Métis 

        
            
                                    29.5.2010
            o
            10:48
                        |
            Karma článku:
                12.54
            |
            Prečítané 
            307-krát
                    
         
     
         
             

                 
                    Rozum a cit trošku inak. Antika a dnešok. Alebo len dialóg. Vyberte si.
                 

                 Métis    Hovorí mi – nebásni!  Básnici už vyšli z módy.  Nemódni sú len blázni,  proti prúdu lodivodi...      Hovorím jej – neblázni!  Báseň aj titánov zrodí.  To, čo vkladám do básní,  nie je to, čo varia z vody      jednodňoví kuchári,  rýchlokvasné celebrity,  všehochuti gurmáni,  ktorých dielo sotva cítim.      Poet prevteľuje ríty  do reality, moja spanilá.  Prečo by si sa i ty  so mnou inak bavila?   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Zaklínač: Búrková sezóna – dvakrát do tej istej rieky nevstúpiš! Ale čoby!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Novembrová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Vtedy na Východe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Dôkaz
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Kriššák 
                                        
                                            Pod nebesami báseň nezaniká
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Erik Kriššák
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Do mňa sa omáčaj
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Nežná
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Erik Kriššák
            
         
        erik.blog.sme.sk (rss)
         
                                     
     
         Fanúšik komiksov, literatúry a hudby. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    458
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    353
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Texty
                        
                     
                                     
                        
                            Blogové venovania
                        
                     
                                     
                        
                            Básnické grify v praxi
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Pre deti
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O hudbe
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            Komiks
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Guy Gavriel Kay - Pod nebesy
                                     
                                                                             
                                            Fernando Báez - Obecné dějiny ničení knih
                                     
                                                                             
                                            Augusto Croce - Italian Prog
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Warhorse - Ritual
                                     
                                                                             
                                            Genesis - Selling England by the Pound
                                     
                                                                             
                                            Beggar's Opera - Raymond's Road
                                     
                                                                             
                                            Premiata Forneria Marconi - Photos of ghosts
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Branislav Haspra
                                     
                                                                             
                                            Lívia Hlavačková - pitva
                                     
                                                                             
                                            Silvia Lužáková
                                     
                                                                             
                                            Saša Grodecká
                                     
                                                                             
                                            Patrícia Šilleová
                                     
                                                                             
                                            Marek Sopko
                                     
                                                                             
                                            Martin Hajnala
                                     
                                                                             
                                            Jano Topercer
                                     
                                                                             
                                            Jaromír Novak
                                     
                                                                             
                                            Ema Oriňáková
                                     
                                                                             
                                            Vitalia Bella
                                     
                                                                             
                                            Janka Bernáthová
                                     
                                                                             
                                            Dana Janebová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Žalmani na Facebooku
                                     
                                                                             
                                            Žalman Brothers Band
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




