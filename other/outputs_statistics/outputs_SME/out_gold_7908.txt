

 V rámci E-Vote 2011 projektu, ktorý začali v Nórsku plánovať v roku 2008, budú môcť voliči voliť cez internet. Pilotný projekt sa týka 11 samospráv a zahŕňa približne 200 000 oprávnených voličov. Na nórskom riešení sú zaujímavé 2 veci: možnosť zmeniť voľbu a zverejnenie kódu. 
 Na voľbu cez internet bude mať volič istý čas a bude môcť svoju voľbu zmeniť a to dvoma spôsobmi. Buď cez internet alebo vo volebnej miestnosti štandardným hlasovacím lístkom. Autori projektu týmto sledujú zabezpečenie jedného z kľúčových atribútov - slobodná voľba a zároveň eliminujú kupovanie hlasov. 
 Predstavme si voliča, ktorý ide voliť doma cez internet. V jeho slobodnej voľbe mu môže niekto brániť a môže ho (do)nútiť hlasovať inak, ako chcel. Takýto volič si môže o niekoľko hodín opäť sadnúť k počítaču a svoje hlasovanie stornovať a jednoducho hlasovať znovu, slobodne. Ak by to možné nebolo, môže volič prejsť do štandardnej volebnej miestnosti, tam zrušiť svoju pôvodnú voľbu a hlasovať hlasovacím lístkom. 
 Možnosť meniť svoje rozhodnutie eliminuje kupovanie hlasov. Jednoducho kupujúcim sa kupovanie už neoplatí, pretože nevedia, či volič svoje hlasovanie nezmení a nebude hlasovať inak. 
 Druhou zaujímavosťou je, že celý kód aplikácie (volebného softvéru) bude zverejnený na internete a každý si ho bude môcť stiahnuť a preštudovať. 
 Celý svet sa vybral cestou k elektronickým voľbám. Dúfajme, že tento vlak Slovensku neujde. 
   

