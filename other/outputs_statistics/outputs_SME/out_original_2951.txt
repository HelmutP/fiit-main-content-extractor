
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mišo Pavlik
                                        &gt;
                biela
                     
                 Tri úsmevy 

        
            
                                    23.3.2010
            o
            12:08
                        (upravené
                3.4.2010
                o
                13:22)
                        |
            Karma článku:
                7.81
            |
            Prečítané 
            389-krát
                    
         
     
         
             

                 
                    Sedel som s nimi v kupé vlaku. Bol som ticho, a pozeral sa von oknom. Až keď ukončili svoj rozhovor, zbadal som sa, že sa usmievam tiež.
                 

                 "Mami, pozri sa!" oslovovalo malé dievčatko svoju matku. Mohla mať asi tak päť rokov, a v rukách držalo svoje prasiatko. Teda, poviem vám, z tohto dievčatka priam žiarila radosť. Ale aby som vás uviedol do deja, volám sa André, som francúzky dôstojník v zálohe, a práve som bol povolaný do služby. Píše sa rok 1928 a vlak v ktorom sedím si to ženie na juh k španielským hraniciam. Cestujem civilným vlakom až do Montpellier, kde sa pripojím k svojej jednotke. So mnou je tu mladá žena so svojou dcérou, ktorá v rukách drží hlinenú pokladničku v tvare prasiatka.   "Mami, aha ako sa usmieva". Dievčatko znova požadovalo matkynu pozornosť. Stále sa smialo, a ukazovalo svoje riedke zúbky. Vo vlasoch malo dve stuhy. Žlté. Aj jej šatičky boli živých farieb, a chvíľku mi toto dievčatko pripadalo akoby ju na zem zoslala Freyja, starogermánska bohyňa krásy a lásky. Mať tak jej problémy, a nemyslieť na vojnu, pomyslel som si.   Matka sa pozrela na dcéru, a opätovala jej úsmev. "Vieš čo je na tvojom prasiatku najkrajšie?" spýtala sa svojej dcérky. Očividne ju táto otázka zaskočila, chvíľku si svoje prasiatko prezerala a potom odpovedala "že sa usmieva?". "Presne tak Marion". Marion sa volala, mohlo ma to hneď napadnúť. Teraz som si už istý že pochádza od bohov. "Presne tak Marion, dokonca sa usmieva, aj keď mu nehádžeš žiadne peniaze" pokračovala v odpovedi. Pozerali na seba a usmievali sa.   Sedel som s nimi v kupé vlaku. Bol som ticho, a pozeral sa von oknom. Až keď ukončili svoj rozhovor, zbadal som sa, že sa usmievam tiež.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mišo Pavlik 
                                        
                                            Padám hore
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mišo Pavlik 
                                        
                                            Chrobák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mišo Pavlik 
                                        
                                            Pozrite sa, tu leží
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mišo Pavlik 
                                        
                                            Bláznivý príbeh o bláznovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mišo Pavlik 
                                        
                                            Strana 263
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mišo Pavlik
                        
                     
            
                     
                         Ďalšie články z rubriky próza 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        David Stojaspal 
                                        
                                            Dukelské zápisky 1944-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Igor Čonka 
                                        
                                            Ellis Parker Butler - Prasce sú prasce I
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Kto nezažil, neuverí. . . (46)
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Oni
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Odrazový mostík
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky próza
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mišo Pavlik
            
         
        misopavlik.blog.sme.sk (rss)
         
                                     
     
        neberte moje názory vážne, som len študent :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    574
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            biela
                        
                     
                                     
                        
                            čierna
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




