
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magda Kotulová
                                        &gt;
                Nezaradené
                     
                 Na výlete 

        
            
                                    27.5.2010
            o
            14:26
                        |
            Karma článku:
                7.95
            |
            Prečítané 
            1679-krát
                    
         
     
         
             

                 
                    Začiatok mája. Neskutočne krásny. Rozžiarený slnkom. Nádherná mladučká zeleň všetkého: stromov, kríkov, trávy, kvetov. Lahodila očiam a vyvolávala v ľuďoch túžbu, aby sa do nej ponorili či v lese, alebo aspoň v mestských parkoch.
                 

                     Sedela som za stolom a písala, keď zaznel povedľa telefón.   „No?... Prosím...“ ozvala som sa netrpezlivo, lebo som si práve zaumienila, že si pôjdem po dokončení textu ľahnúť a vystriem si aspoň na chvíľu boľavé nohy.   „Mami, nechcete ísť s otcom, s nami a s babami na výlet... k Morave... pod Devín...?  Budete si tam môcť aj sadnúť... Odpočiniete si, aj vaše nohy...“ sypala dcéra. Neúprosne ma vábila, aby som sa vzdala odpočinku a vyštartovala do májovej nádhery. A ešte k tomu - s novými vnučkami. Zvedela by som , ako dozrievajú, rastú...   „Oco, ideme s Katkou von... aj s babami?... Idú k Morave...“   Muž mi za chrbtom odpočíval. Vedľa neho sa váľali neprečítané noviny Neochotne zvolal:   „Teraz?... Chce sa ti?...“   „No, ani nie, ale aspoň trochu uvidíme jar a májovú prírodu...“ navádzala som ho.   „No dobre, povedz jej, že nech prídu po nás... Aspoň sa trochu rozchodíme...“ už tón jeho hlasu nasvedčoval, že sa s vyrušením vysporiadal.   „Katka, teda príďte po nás...“   „Kedy... o koľkej...?“ hlas v slúchadle zaznel trošku netrpezlivo.   „No ako vám to vyhovuje... My sa vyštafírujeme raz-dva...“   „Tak, o hodinu... Dobre?“ hlas už znel pokojne. Pochopila som, že dcéra mala strach, že nám  náhle vyrazenie do prírody bude robiť problémy a príprava tiež.   „Dobre...“ zašuchla som do telefónu a zložila slúchadlo do základne prístroja. Zavrčal, akoby bol psík.   „Tak, Vilo, vstávaj... prídu po nás, čo by dup...“   „Však idem...“ pomaly sa zviechaval z váľandy manžel.   Vychystali sme sa. Vybrala som z kredenca banány, čokoládky a čakala na kresle v izbe pripravená, kedy sa ozve zvonec, aby sme zbehli dolu. Na koniec som sa celkom tešila. Ideme konečne s nimi. Aspoň  uvidím dievčatká...   Keď dcéra zdola zazvonila, vybehli sme z dvier ako mladíci. Ale hneď sme zbadali, že naše kolená vŕzgajú a bolia ako hrom. Došmatlali sme sa k autu. Zbadali sme obe vnučky sedieť priviazané v sedačkách na sedadlách auta. Usmievali sa a kývali nám ostošesť oproti..   „Čaute, baby,“ vydýchla som, keď už som aj ja bola pripútaná k sedadlu. Otočila som sa, vypýtala si ich rúčky a bozkala im ich. Obe očili, čo to vyvádzam. Neboli na také nežnôstky ešte zvyknuté.   „Babka, ideme k Morave,“ pustila si hneď na špacírku jazýček Janka. Sypala zo seba, sypala vety. Pousmiala som sa jej zhovorčivosti. Celkom rozumnej, zlatej a dôverčivej.   Danka za nami len mlčky sedela a len nás skúmala očkami. Snažila som sa jej prihovárať a vyprovokovať k hovoru. Márne. O chvíľu Janka spustila pesničky. Celkom jej to šlo.   „Janka, a kde si sa naučila také pekné pesničky?“ pátrala som po učiteľovi spevu.   „No, mamina im vyspevuje, keď ich ukladá spať...“ vyhŕkol hrdo zať.   „Ah tak...“ odkrylo sa tajomstvo, „pekne spievaš Janka,“ chválila som potom.   Už som sa neobracala dozadu a nevypytovala Danky nič. Tá sa potrebovala osmeliť.   Dobre, že som ju k ničomu nesilila. O moment sa k rozospievanej Janke pridala aj ona. A tak cestou do Rakúska, k Morave, bolo počuť spev, smiešky a pohodu  z nášho rozbehnutého autíčka.   Keď sme prišli na miesto, museli sme sa, ak sme sa chceli dostať k rieke, prebrodiť lesnou cestičkou. Krásna zeleň upokojovala, ale hneď akoby vzrušovala oči. Po bokoch cesty bola nádherná mladučká pŕhľava. Upozorňovala som detiská, že pŕhli. Smiali sme sa spoločne na slovke „pŕhli“, lebo Janka ani Danka nevedeli toto neznáme slovko vôbec vysloviť. Učili sme ich, ako ho správne vyslovovať.  Detiská boli celé nadšené, keď ho zvládli. Ukazovali prstíkmi na žihľavu. Keď ju zbadali rozšantene vykrikovali: „Pŕhľava, pŕŕrhľava!“   Danka sa držala ako kliešť mojej ruky. Janka zasa dedkovej.   Keď som chcela Danku pustiť pri zúžení chodníka,  zaprosila: „Babka, ja sa bojím... Nechaj mi ruku...“ tak som jej rúčku pohladila a podržala vo svojej ďalej.   Naraz k nám zozadu dobehla Janka, s nejakým drúkom, konárom: „Babka, priniesla som ti palicu, aby sa ti lepšie išlo... aby si sa mohla opierať...“   Skoro som z nôh spadla. Taká milá, pozorná... Pobadala, že dedko si vybral z auta svoju barlu, čo mu pomáhala pri chôdzi, tak chcela, aby paličku som mala aj ja. Malá potvorka. Pozorná, milá... Srdce mi zajasalo. Sú zlaté. Začínajú naberať dobré spôsoby... Vypustila som Dankinu rúčku na moment zo svojej, zobrala palicu od Janky, pohladila hlavičku, zohla sa a pobozkala na líčko. „Ďakujem Janka... si pozorná...“   Malá víchrička už aj bola opäť u dedka. Nepočkala vlastne ani na moju vďaku. Chytila som opäť Danku za ruku. Prechádzali sme rozšafne lesom, rozštebotaným vtáčikmi-letáčikmi. O pár sekúnd sa pred nami zaligotala Morava.   „Už sme tu,“ šťastne zakričali oba drobce ako na povel.   „No krása,“ zavrnela som šťastím aj ja.  Potom som mrkla na práve dochádzajúceho, naraz rozkrívaného dedka, čo si pomáhal pri chôdzi opieraním sa o palicu.   Dedko ani nemukol. Hneď hľadal miesto, kde by zaparkoval. Deti už zhadzovali svetríky.  Brali do rúk vedierka s lopatkami z batohu, ktorý dosiaľ teperil ich ocino a vrhli sa do hry: s pieskom a s vodičkou z Moravy. Občas zazerali na vrčavú, hrmotavú plaviacu sa lodisko dolu Dunajom, občas na kajakárov, čo vedľa nás prefrčali, a občas na množstvo ľudí, čo sa húfne na slovenskej strane, v nedeľnej májovej pohode, presmŕdzali pod hradom Devín.   Pokojne, šťastne, v pohodičke, bačovali sme v samote, a predsa nie v samote - na rakúskej strane pri Devíne. Z jednej strany sme mali valiaci sa Dunaj a z druhej strany pomalšiu solídnu Moravu, ktorá sa stočila tesne k nám, aby sa opatrne poniže spojila s mocným kolegom Dunajom. Výlet sa konal. Rozbehnutý – fičal.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            U kaderníčky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Oprstienkovaná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Daniel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Keď zakape mobil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Registrácia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magda Kotulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magda Kotulová
            
         
        magdakotulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som mama štyroch detí, stará mama pätnástich vnúčat a dvoch pravnúčat. Príležitostná publicistka. Občas sa "niečo" pokúsim napísať, keď ma čosi nahnevá alebo urobí spokojnou. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    318
                
                
                    Celková karma
                    
                                                10.84
                    
                
                
                    Priemerná čítanosť
                    1470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kódex blogera na blog.sme.sk (aktualizovaný 10. 7. 2012)
                     
                                                         
                       Ocko náš
                     
                                                         
                       Príjemné výročia si treba  pripomínať
                     
                                                         
                       Lezúň
                     
                                                         
                       Garancia vrátenia peňazí. To určite!
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Vôňa ženy
                     
                                                         
                       Vodopády Slunj
                     
                                                         
                       Mladý, nervózny vodič a starká
                     
                                                         
                       Nikdy, nikdy, nikdy sa nevzdávaj
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




