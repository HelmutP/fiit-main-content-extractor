

 Táto tabuľka je ilustratívna a neobsahuje všetky životné procesy.  Cieľom je priblížiť najčastejšie životné situácie "od narodenia až po smrť". Má edukatívne napomôcť k pochopeniu viacerých funkcii verejnej administrácie a môže všeobecne vymedziť podstatné časti budúceho eGovernmentu 
 
 




 Skutočnosť 


 Služby   verejnej správy 


 Služby   vo verejnom záujme 




 Narodenie 


 Rodný list 
 Pas 
 Pobyt osoby 


 Životné poistky 




 Vzdelávanie 


 Potvrdenie o návšteve školy 
 Ukončenie štúdia 
 Dosiahnuté štúdium 


 Informácie o štúdiu 
 Zápis na školu 
 Štipendiá 
 E-learning 




 Zdravie 


 Informácie o zdravotníctve 
 Podnety na úrad pre dohľad nad zdravotnou   starostlivosťou 


 Zdravotné poistenie 
 Zdravotná karta 
 Návšteva lekára 
 Výber zdravotnej poisťovne 




 Doprava 


 Vodičský preukaz 
 Register motorových vozidiel 
 Lustrácia osôb 


 Zjazdnosť ciest 
 Rezervácie dopravy 
 Diaľničné poplatky 
 Mýto 




 Zamestnanie 


 Evidencia nezamestnaných 
 Živnosť 
 Obchodný register 
 Podpora nezamestnanosti 


 Hľadanie zamestnania 




 Bývanie 
 Kultúra 


 Kataster nehnuteľností 
 Stavebné povolenie 
 Územný plán - prístup do GIS systémov 


 Plná informatizácia 
 Kultúrne poukazy/informácie 




 Dane 
 Poplatky 
 Odvody 


 Daňové informácie a priznania 
 Odvody a Clá 
 Dôchodkové poistenie 
 Rodinné prídavky 
 Miestne dane a poplatky 


 Výber DSS 
 Zmluvné poistenie 
 Koncesionárske poplatky 
   




 Demokracia 


 Voľby 
 Referendá 
 Participácia na verejných veciach 
 Notári 


 Petície 
 Združovanie sa 
 Politická agitácia 
 Lobbing 




 Bezpečnosť 


 Trestné oznámenie 
 Oznámenie priestupku 
 Trestné registre (spisy) 
 Súdy 


 Komunikácia občanov s OČTK 




 Exitus 


 Výmaz/deaktivácia záznamov 


 Pohreb 





