
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Urda
                                        &gt;
                Cestovanie
                     
                 II - Dolina, dolina, 

        
            
                                    18.7.2010
            o
            14:01
                        (upravené
                24.7.2010
                o
                11:18)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            2556-krát
                    
         
     
         
             

                 
                    čo prejdem, to iná... - Furkotská dolina Pokračovanie predchádzajúceho článku Dolina. dolina, čo prejdem, to iná... - Mlynická dolina Predchádzajúci článok končil prechodom Bystrej lávky, tento bude pokračovať zostupom z Bystrej lávky dole Furkotskou dolinou na Štrbské pleso. Pretože úzkym miestom prechodu týmito dvomi dolinami je Bystrá lávka, kde sa z oboch strán treba reťaziť, je to akási jednosmerka, teda len prechod z Mlynickej doliny do Furkotkej, nie opačným smerom. Nikde som nevidel zákaz ísť opačným smerom, takže to je len odporúčanie. Zvláštnosťou týchto dvoch dolín je aj to, že kým voda Furkotského potoka sa vlieva do Váhu a tečie Dunajom do Čierneho mora, voda Mlynického potoka sa vlieva do Popradu a tečie Vislou do Baltického mora. Hranicou medzi povodiami je hrebeň Soliska.
                 

                 
  
   * Furkotská dolina je veľmi turistami obľúbená aj z toho dôvodu, že na chatu pod Soliskom sa možno vyviezť sedačkovou lanovkou od areálu FIS na druhej strane Soliska ku chate pod Soliskom, odkiaľ je pekný výhľad, a odtiaľ zostúpiť po modrej značke do doliny. V doline je zopár plies, z ktorých niektoré sú ukryté v kosodevine, takže z turistického chodníka ich nevidno. .  Pohľad z Bystrej lávky na furkotskú stranu, v pozadí majestátny Kriváň .  Reťazenie z Bystrej lávky do Furkotkej doliny .  Pohľad zdola na prechod Bystrá lávka z Furkotskej doliny .  Druhé najvyššie položené tatranské pleso - Vyšné Wahlenbergovo - 2157 m n. m. .  .  Pri predchádzajúcej návšteve Furkotskej doliny som prišiel až sem.  Ten chodník nad plesom vedie na hrebeň a Furkotský štít, nie je oficiálny.  Značkovaný chodník na Bystrú lávku a ňou do Mlynickej doliny sa stáča napravo. .  Pri Vyšnom Wahlenbergovom plese bol aj tento psík, vraj vyšiel po vlastných .  Od Vyšného Wahlenbergovho plesa dole dolinou .  Malé pliesko medzi dvomi Wahlenberovými plesami - Vyšným a Nižným .  Nžné Wahlenbergovo pleso s malým plieskom .  .  Nižné Wahlenbergovo pleso .  .  Medzi kosodrevinou ukryté plesá (to väčšie je Furkotské), z chodníka ich nevidno, toto je pohľad z výstupu na Solisko .  .  Pohľad Furkotskou dolinou dole .  .  Furkotský potok .  A opäť na Štrbskom plese *   
 .  .  Nakoniec zástupcovia tatranskej flóry. * Čo na záver? Kto to prešiel, istotne tú krásu oboch dolín ocení. Koľkí obdivujú krásy prírody v zahraničí,   a tu, na Slovensku, máme nádherné najmenšie svetové veľhory. . Poznamenávam, že chodník, tak ako aj mnohé iné vo Vysokých Tatrách, ale nielen vo Vysokých Tatrách, sú od 1. 11. do 15. 6. uzatvorené a kultúrny turista tento zákaz aj rešpektuje. Celoročne je povolený len výstup po vodopád Skok. A napriek tomu sa nájdu na nete články, kde sa turisti chvália, že trasu absolvovali v zakázanom termíne. Podobne kultúrny turista si neskracuje cestu mimo vyznačených chodníkov ani nejde na miesta, kam nevedie značený chodník, napr. tu na Furkotský štít. Láka to, keď je človek tam a zdolal už toľko, pridať ešte ten kúsok. Ale predsa len...   
 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Z Detvy do Hriňovej...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Znova z Handlovej do Prievidze
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Králická tiesňava a vodopád
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Zákopčianskymi kopanicami
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Hmly a spomienky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Urda
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Urda
            
         
        urda.blog.sme.sk (rss)
         
                        VIP
                             
     
         jednoducho dôchodca, jednou rukou na blogu, druhou nohou v hrobe so životnou zásadou podľa Diderota: 
 "Lepšie je opotrebovať sa, ako zhrdzavieť" 

                                    * * * 
Krásy Slovenska som propagoval i tu: 
http://fotki.yandex.ru/users/jan-urda/ stále živá stránka - http://fotki.yandex.ru/users/jujuju40/ 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    517
                
                
                    Celková karma
                    
                                                7.55
                    
                
                
                    Priemerná čítanosť
                    2324
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Detstvo - spomienky
                        
                     
                                     
                        
                            Škola - spomienky
                        
                     
                                     
                        
                            Práca - spomienky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Z Turca
                        
                     
                                     
                        
                            Staroba - spomienky
                        
                     
                                     
                        
                            Filatelia - svet poznania
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Nezamestnanosť - najzávažnejší problém na Slovensku
                     
                                                         
                       Môj 4% ný kamarát.
                     
                                                         
                       Straka
                     
                                                         
                       Zadarmo do Žiliny
                     
                                                         
                       Oneskorene k 17. Novembru ....
                     
                                                         
                       Komunálne voľby prerušené na 38 minút.
                     
                                                         
                       Inzerát
                     
                                                         
                       Koľko je reálne hodné euro?
                     
                                                         
                       Gruzínsko - cesta do kraja bez ciest
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




