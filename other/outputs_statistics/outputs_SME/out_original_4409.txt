
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Anna Mezovská
                                        &gt;
                Welcome to My View
                     
                 Katyň 

        
            
                                    12.4.2010
            o
            10:48
                        (upravené
                31.5.2010
                o
                21:02)
                        |
            Karma článku:
                6.52
            |
            Prečítané 
            1293-krát
                    
         
     
         
             

                 
                    „Trpezlivo čakám na Boha, ktorý ma zachráni, som závislý len od Neho.  On ma chráni,  je mojím útočišťom  a ja NIKDY nebudem porazený!" (Ps 62: 1-2 GNB)
                 

                 
  
   Poľskí dôstojníci, predstavitelia poľskej inteligencie, masové hroby, masaker, guľka do tyla, Rusi, Nemci, vojna, krv, nenávisť, nevinné obete, manželky a deti bez otcov a ešte mnoho ďalších slov mi prichádza na myseľ. Všetko to do seba akosi zapadá. Áno, stalo sa. Bolo to už dávno. Je to len minulosť. Minulosť, ktorá však ovplyvňuje našu prítomnosť, hoci si to vôbec neuvedomujeme. A možno aj uvedomujeme, len to nechceme pripustiť. Hlúpo si nahovárame, že to tak nie je. Žiaľ, pravda je však iná.   Tlačia sa mi do očí slzy, keď sa dozvedám, aké hrozné veci sa diali a dejú v dejinách ľudstva. Je to priam neuveriteľné, aký krutý dokáže byť človek. A my? My to proste berieme. Necháme sa unášať životom a nestaráme sa o ostatné veci. Nezastavíme ani na chvíľočku, aby sme popremýšľali nad tým, čo nás, či chceme, alebo nie, bezprostredne ovplyvňuje, čo nás zasahuje a pretvára našu budúcnosť. A to nezávisle od toho, či sa to stalo pred sedemdesiatimi rokmi alebo len teraz.   Človek sa dobrovoľne rozhodol pre zlo. Zatúžil ho spoznať. Chcel byť ako Boh, no netušil, do čoho sa rúti. A teraz by to možno chcel vrátiť späť, je však už neskoro. Slobodne sa rozhodol a nedá sa to odvolať. Túžba človeka po čomsi viac zasiahla všetky generácie a tragickým spôsobom sa nesie ďalej a ďalej.   A Boh? Čo robí Boh? Vraj je láska. Vraj nás miluje. Tak prečo dovolí, aby sa diali také hrozné veci? Prečo nezasiahne? Nevadí mu, že toľkí trpia? Prečo nezastaví kruté prelievanie krvi? Nevidí to? Nepočuje? Nie, On to veľmi dobre vidí a počuje, dal však človeku slobodnú vôľu a keďže je pravým gentlemanom, nikdy mu ju nezoberie...   Som vďačná Bohu, že tých nevinných poľských mužov až tak nemučili. Vpálili im odzadu guľku do hlavy a skôr, než stihla vyletieť čelom von, stál tam Ježiš so zástupmi tisícok anjelov. Mal roztvorené náručie a čakal. Čakal, aby mohol zachytiť zomierajúceho. Verím, že to tak bolo. Hoci fyzické telo padlo do jamy, ktorú neskôr zasypali hlinou, duša bezbranného odišla s Ježišom do Večnosti. Do neba, kde ho už nečakalo žiadne trápenie, ale naopak, radosť. A Láska!   Plač, ktorý sa rozliehal po Poľsku a okolitých štátoch, sa ani zďaleka nevyrovná tomu, ktorý zasiahol srdce samotného Boha. Celé nebesia plakali s Ním.  A ja si ani zďaleka nedokážem predstaviť, aké to bolo boľavé. Možno to raz zistím, keď zomriem a dostanem sa do neba. Stretnem tam mnoho, mnoho priateľov a medzi nimi budú určite aj títo hrdinovia z Katyňského lesa. Budú mi môcť porozprávať viac a to je jedna z vecí, prečo sa teším na smrť.   Tragická havária poľského prezidenta a jeho manželky, popredných predstaviteľov poľského spoločenského i duchovného života a prakticky celého velenia poľskej armády, ktorá sa udiala v sobotu 10. apríla 2010, má priamy súvis s masakrom v roku 1940 a neskôr. Nie je to žiadne náhoda. Tieto dve udalosti sú úplne prepojené. Havária je pokračovaním toho, čo sa začalo v Katyni...   Akoby jedna udalosť hovorila o druhej. Jedna nadväzovala na druhú. Jedna predpovedala druhú. A stalo sa to kvôli niečomu. Kvôli niečomu veľkému, inak by nezomrelo tak veľa dôležitých osobností. Zatiaľ netuším, čo to presne má znamenať, prečo to má až také silné prepojenie. Ale jednu vec isto viem, Boh to vie! Som presvedčená, že On nie je pôvodcom krvavých nešťastí! On len dovolil, aby sa stali, pretože ešte stále rešpektuje našu slobodnú vôľu. Dal Svoje slovo a aj ho dodrží!   Je tu len jeden detail, na ktorý veľmi často radi zabúdame. Boh vie o všetkom, má nadhľad. Keby sme sa obrátili na Neho a prosili Ho o pomoc, nemuselo by sa udiať to, čo sa udialo. Keby sme s Ním nadviazali kontakt ešte skôr, nedošlo by k toľkým tragickým udalostiam. Počul by naše volanie a zasiahol by, lebo  sme Ho o to prosili. Vystríhal by nás, upozornil. Poukázal na veci, ktoré sa s nami stále vlečú a ktoré treba dať do poriadku. Chránila by nás Jeho ruka a situácia by sa vyvíjala iným smerom. No my sme Ho odmietli...   Nečakané momenty posledných dní sa netýkajú len Poľska, ale aj nás. Celého sveta. A tak znova - naša minulosť má priamy súvis s našou prítomnosťou i budúcnosťou. Aby tomu tak nebolo, aby došlo k preťatiu prekliatí, dedičných pút, zviazaností, potrebujeme prísť pred Boha, padnúť na tvár a prosiť Ho o zľutovanie. Len skrze Neho môže dôjsť k uzdraveniu duše ľudstva! On je naša jediná nádej! Jediný zdroj múdrosti! Ak to tak neurobíme, potom sa nečudujme, keď príde nová pohroma.       annieMERCY©2010     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Nehodná lásky? (Skutočný príbeh)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            S deťmi na nákupe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Horehronské babky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Mačacia kráľovná
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Anna Mezovská
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Anna Mezovská
            
         
        mezovska.blog.sme.sk (rss)
         
                                     
     
        :-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1039
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




