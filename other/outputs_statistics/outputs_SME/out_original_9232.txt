
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Gogola
                                        &gt;
                Ľudia a lesy
                     
                 Včera som stretol dôchodcu 

        
            
                                    20.6.2010
            o
            7:52
                        (upravené
                20.6.2010
                o
                9:36)
                        |
            Karma článku:
                19.39
            |
            Prečítané 
            10163-krát
                    
         
     
         
             

                 
                    Nebudete mi veriť, ale včera (v sobotu 19. júna) som v Trenčíne na výstavisku stretol jedného dôchodcu. Tento muž bol kedysi idolom mnohých občanov Slovenska. Bol najpopulárnejším politikom, viedol najsilnejšiu politickú stranu, stál na čele vlády, má svoj podiel a) viny b) zásluh (škrtni, čo sa nehodí) na rozdelení Českej a Slovenskej federatívnej republiky. Po minulotýždňových parlamentných voľbách sa po ňom zľahla zem. Darmo po ňom novinári pátrali, akoby zmizol z povrchu zemského. A zrazu stál kúsok odo mňa, spolu s manželkou a nejakými dvomi príbuznými. Šedivý, zjavne unavený, zdal sa mi akoby prihrbený. Jeho charizma (ej, tú veru kedysi mal, bisťubohu!) sa kamsi vytratila. Kupoval v stánku paintballovú pušku. Asi pre niektorého vnuka.
                 

                 Nebolo to moje prvé stretnutie s týmto dôchodcom. V roku 2007 sme sa stretli na Dni stromu vo Vydrovskej doline pri Čiernom Balogu. Ako vtedajší hovorca štátnych lesov som bol moderátorom a spoluorganizátorom tohto (teraz nemachrujem, vážne) obľúbeného podujatia. Dôchodca prišiel na Deň stromu až popoludní, zjavne ho nezaujímal program. Chcel sa len stretnúť s generálnym riaditeľom (jeho meno sem nenapíšem, lebo si to ani nezaslúži). Bol som tak trochu nechtiac svedkom ich stretnutia.   "Tak ako, pán generálny, ako idú veci?" - opýtal sa dôchodca. Z jeho hlasu a dikcie vety bolo úplne jasné, kto je tu "gazda".   "Buďte bez obáv, presne ako sme sa dohodli." - odpovedal generálny riaditeľ, zjavne trochu v pomykove (i keď čítať jeho emócie z tváre bolo veľmi ťažké, vždy mal mimiku strnulú ako medveď :)   "Príďte do kúpeľov, porozprávame sa." - z dôchodcovho hlasu bolo jasné, že to nie je pozvanie, ale rozkaz.   "Prídem, určite prídem. Ďakujem za pozvanie." - snažil sa generálny riaditeľ trochu prekryť direktívnosť dôchodcovej repliky.   Bolo mi to smiešne i trápne zároveň. Chlap so štyrmi akademickými titulmi stál pred svojim politickým chlebodárcom ako paholok pred gazdom. Predposratý.   Takže tak. A zrazu je tu opäť. Starý dedko, vyhasnutý, bez iskry v oku, moce sa po výstavisku. Mám sto chutí podísť k nemu a podarovať mu jeden výtlačok knižky Nežná zelená (nosím ich pár stále so sebou, všade ju ktosi pýta). A povedať mu: "Pán dôchodca, prečítajte si, ako sa hŕstka zamestnancov vzbúrila proti politickej rabovačke v štátnych lesoch! I keď viem, že máte veľmi podrobné informácie, toto je pohľad z druhej strany barikády."   Chvíľu som koketoval s touto myšlienkou. No nakoniec som si povedal: Tento chlap si nezaslúži nič iné, len aby sme naň všetci zabudli. Definitívne ho vymazať zo svojej pamäti. Nech hrá paintball s vnukmi...   A ešte faktická poznámka na záver: Výsledky parlamentných volieb dávajú nádej, že vo veciach verejných ktosi urobí konečne poriadok. Pozorne sledujme kroky modro-oranžovej koalície a buďme pripravení reagovať na to, ak by sa vo vládnutí uberala nesprávnym smerom (iste viete, ktorý Smer mám na mysli :). Dúfam, že medzi lesníkmi je dosť slušných a schopných ľudi, aby vypoklonkovali tú spodinu, čo tam bačovala doteraz. Chcem veriť, že nový generálny riaditeľ (kto to bude?) zostaví nové vedenie z profesionálov, ktorí sú a ostanú morálne bezúhonní. Držme im palce, nebudú to mať ľahké.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Referendum–dum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Bubeníkove osudy III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Stopárov sprievodca Banskou Bystricou (1. časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Otvorený list budúcemu ministrovi pôdohospodárstva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Nežná zelená
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Gogola
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Gogola
            
         
        petergogola.blog.sme.sk (rss)
         
                                     
     
        Komárňan, domestifikovaný v Banskej Bystrici. Milovník prírody a dobrého vína. Fanúšik rockovej hudby, bubnov, bežiek a Monthyho Pythona.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Banská Bystrica
                        
                     
                                     
                        
                            Ľudia a lesy
                        
                     
                                     
                        
                            Zbrane
                        
                     
                                     
                        
                            Bubny a bubeníci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




