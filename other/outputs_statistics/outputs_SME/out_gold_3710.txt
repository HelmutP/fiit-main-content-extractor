

 Neupotrebiteľnosť 
 
 Pocítiac náhlu potrebu uháňam na záchod. Vybieham bleskorýchlo, sťa prápor za mnou veje rozbaľujúci sa toaletný papier. Vo dverách stretám dievčinu pekného výzoru. Zrýchleným krokom vchádzame každý do svojej kabínky. Ach, nie som ja - chlapec z dediny -  zvyknutý takto verejne sa upotrebovať a ešte k tomu v prítomnosti žien. Aj mi to moc nejde, aj sa mi od trémy chvejú kolená (uznajte, že to v takejto situácii nie je bohviečo).  

 Zato dievčaťom čosi ihrá, lomcuje, nepripúšťam si to najprv, potom si aj uši zapchávam. Azda bola potreba natoľko neumlčateľná? Alebo jej pánboh zabudol namontovať ovládač volume, márne sa stišuje - zvuk zipsu je len tichom pred búrkou? Či nepatrí k etikete nielen pri jedle nemľaskať? Až zimomriavky sa mi robia z toho jej orchestrálneho vystúpenia, čosi sa vo mne dvíha sťa mexická vlna. Pripomína sa mi potreba celkom iného kalibru než som pôvodne zamýšľal.   

 Na chodbičke sa opäť stretávame za zvuku splachovačov, obaja v lepšej nálade. Na znak mieru sa patrí vymeniť si toaletné papiere, no mier je len jednostranný, svoj kúsok Tento-hebkosti kŕčovito zvieram v paprčiach. 

 Červená sprchovaniu 

 V sprche si veci kladiem na radiátor, len tak sa preťahujem, načim sa začať vyzliekať. Samé ženské vlasy ležia na zemi. "Chválapánubohu," zhutujem; aj na záchodoch je chlpov veľa, ale oveľa kratších a vlnitých. (nedomnievam sa, že by mohli byť z nosa).    

 Na vlhkej dlážka ale ale strácam rovnováhu. Moja ruka siahne na radiátor - pred pádom sa uchránim, do čohosi mäkkého som sa však zahĺbil a nechce sa mi o tom do podrobna na tomto mieste rozprávať. Poviem iba toľko, že tam ležalo a na moju ruku sa tešilo, len pár centimetrov od mojich skôr odloživších odiedz, čo v reklame často vidíme a pritom je to nám chlapom celkom zbytočné. Dotyčná nebola  ani  zďaleka šľachtického pôvodu.  
 Kľúč 
 Nočné som vtáča, o druhej nadránom spisujem článok, ktorý nakoniec ani nedokončím. Otvoria sa dvere na niektorej zo susedných izieb a na celú chodbu sa rozľahne opilecké "Je tu nejaky funkčný pekný chlap?".  
 Poviem to rovno, ostal som verný. (Zamykáme sa zvnútra a ja som nevedel nájsť kľúč.) 

 -----------  
 Nesťažujem sa, nemodlikám o pomoc. Ani neviete, aké milé vedia byť stretnutia s dievčatami s toaletným papierom v ruke a na jazyku "dobré rano", rozhovory v uteráku, a pod ním len čo pánbhoh stvoril, nohavičky na spoločnom balkóne sušiace sa z včerajšej mužskej charizmy. Patrí to k ním ako rosa k ránu, pristane im to, zdá sa to byť pri pohľade na ne vrchoľne prirodzené a ľudské.  
 Len o jedno vás prosím - už mi nikdy netvrďte, že prasce vedia byť iba chlapi. 

 Kroch. Dohovoril som. 


