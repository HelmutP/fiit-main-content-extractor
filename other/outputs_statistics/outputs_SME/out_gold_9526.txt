

 I. Ad poctivosť 
   
 "Pokiaľ bude veľký počet amerických občanov vnímať politickú činnosť ako nečestnú a príliš nízku, aby sa jej venovali, nechajú správu štátu na profesionálneho politika a politickú mašinériu. V princípe niet dôvodu, pre ktorý by štandardy v politike či vláde mali byť akokoľvek nižšie než štandardy v podnikaní, školstve, občianskom sektore alebo akejkoľvek inej ľudskej skupine a možno dodať, že nie je ani za súčasných okolností zriedkavé nájsť vo vládnych úradoch toľko slušnosti a cti, ako v podnikoch alebo sociálnych skupinách. Štandardy v ktorejkoľvek ľudskej inštitúcii alebo organizácii závisia od ľudí, ktorí tie inštitúcie tvoria a spravujú. (...) Ak si občan zašpiní ruky účasťou na politike, stane sa tak iba preto, že už do nej vstupoval príliš slabý." 
 Harold Zink: Politics and Government in the United States. MacMillan: Toronto, 1951, 3. vydanie, s. 122-123 (preklad môj vlastný) 
   
 II. Ad kompetentnosť 
   
 "Akonáhle typický občan vstúpi na javisko politiky, znižuje sa úroveň jeho duševného výkonu. Argumentuje a analyzuje spôsobom, ktorý by v oblasti svojich skutočných záujmov sám uznal za infantilný. Stáva sa z neho znova primitív. Jeho myslenie je asociačné a emotívne." 
 J.A. Schumpeter: Capitalism, Socialism and Democracy, s. 280, citované podľa Pavel Holländer: Základy všeobecní státovědy. Aleš Čeněk: Plzeň, 2009, s. 255. 
   
   
 Pre vás, mojich sponzorov, to samozrejme nie je žiadna katastrofa, lepšie poctivý blbec, ako amorálny inteligent. A ak už nebudem stíhať ani to asociačné myslenie, ešte vždy ma môžu odložiť na Slovenský futbalový zväz. 
   
   
   

