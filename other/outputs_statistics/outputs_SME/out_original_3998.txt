
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Zbornák
                                        &gt;
                Nezaradené
                     
                 Čaro blčiaceho ohňa 

        
            
                                    5.4.2010
            o
            22:14
                        (upravené
                6.4.2010
                o
                0:05)
                        |
            Karma článku:
                2.83
            |
            Prečítané 
            419-krát
                    
         
     
         
             

                 
                    Len málo prvkov ľudského života prežilo technické zmeny uplynuvších desaťročí . Ešte nikdy sa náš životný štýl nezmenil tak prudko za taký krátky čas. Možno preto  sú malé návraty ku koreňom také lákavé a nezabudnuteľné. Oheň od nepamäti sprevádzal našich predkov. Ohnisko bývalo centrom domácností, oheň bol symbolom bezpečia a spoločníkom uprostred temnoty.  Dnes existujú praktickejšie spôsoby získavania svetla a tepla, no oheň stále láka svojou mysticitou.
                 

                 
  
        Hoci štatút „Pána ohňa" už nie je tým hlavným čo nás odlišuje od zvyšku zvieracej ríše, stále je lukratívnou súčasťou potuliek prírodou, či už krátkych opekačiek alebo riadnych čundrov. Ktorý správny odchovanec mayoviek môže zabudnúť na tie letné večery vo svetle plameňov ? Na zvuk gitary a slová piesní ? Na vôňu dymu a pečenej slaninky ?    Pre mňa sú to rozhodne tie najkrajšie spomienky. Mal som to šťastie, že som sa dostal medzi ľudí s podobným založením, a tak vzniklo niekoľko neskutočných akcií, keď sa bežný deň skončil na nečakanom  a často neznámom mieste v kruhu okolo ohňa a v atmosfére, ktorá sa slovami naozaj len ťažko opisuje.    Jedným takým prípadom bol aj výlet na Starhrad neďaleko Strečna, kam sme sa vydali so spolužiakmi zo Žiliny pred dvoma rokmi. Bol už síce október, ale babie leto ešte nedovoľovalo teplé oblečenie. Dnes už neviem ako to celé vzniklo, ale asi to bolo nudou na intráku a fajnovým počasím, že nápad vydať sa na čunder len tak, bez akejkoľvek prípravy a s letnými spacákmi zožal absolútny úspech. O hodinu neskôr sme sa už viezli osobáčikom smer Strečno. Boli sme vtedy prváci a v podstate sme sa navzájom vôbec nepoznali, ale už po ceste hore bolo jasné, že si pôjdeme „ do noty". Bol to možno práve blkot ohňa, ktorý nás v ten večer tak zblížil ( určite na tom mala zásluhu aj trocha slivovice, ale o tom nabudúce ). Večer sa nám zaspávalo veľmi príjemne, teda aspoň mne sa spí vonku na čerstvom vzduchu tak super, že mi nevadí ani bodrel pod spacákom, ktorý nad ránom začína nepríjemne pôsobiť na oblasť v okolí krížovej kosti. Horšie bolo skoré ranné vstávane, keďže v noci sa citeľne ochladilo a neskôr ešte nudná prednáška. Musím poznamenať, že už sme sa pred školou nestihli osprchovať a tak sme trošku voňali dymom. Táto spomienka jedna z vecí, ktorá nás doteraz spája, na ktorú často a radi spomíname.   A teraz konečne pointa celého tohto rozprávania. Moderný život má svoje súčasti, ktoré sú úplne super, ale z času na čas je veľmi príjemné zažiť niečo, čo bolo možno typickejšie pre staršie generácie, niečo čo možno nie je také štýlové ako squash, či dobrá disco ( nič v zlom ), ale aj niečo čo má v sebe to prehistorické čaro, o ktorom tak rád píše Jack London. Pretože aj s pomocou „ohnivej mágie" nám môže pomôcť zblížiť sa s ľuďmi okolo nás a tiež lepšie spoznať samého seba.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Zbornák 
                                        
                                            Martketing – The Modern Warfare
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Zbornák 
                                        
                                            Voličské oprávnenie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Zbornák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Zbornák
            
         
        zbornak.blog.sme.sk (rss)
         
                                     
     
        Svet vidím iba vo farbách. O čiernej a bielej som už veľa počul, ale ešte som sa s nimi nestretol.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    477
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O pravdu asi všeobecne nie je záujem
                     
                                                         
                       Ako homeopati úspešne liečia HIV, alebo ako sa v rozhlase zbláznili
                     
                                                         
                       Peniaze, sex a EÚ
                     
                                                         
                       Naftalan
                     
                                                         
                       Budhista
                     
                                                         
                       Ako začať behať alebo pomaly ďalej zabehneš (1. časť: Výbava bežca)
                     
                                                         
                       Behanie v zime - dobrý či blbý nápad?
                     
                                                         
                       na koľko sa ceníš?
                     
                                                         
                       Sokol v Bezákovi, Bezák v Sokolovi, obaja v nás
                     
                                                         
                       MacGyver v detskej izbe
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




