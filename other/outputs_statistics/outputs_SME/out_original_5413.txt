
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Babinová
                                        &gt;
                Súkromné
                     
                 Už som sa nehodila, no niekto tam hore pri mne stál - čásť prvá. 

        
            
                                    28.4.2010
            o
            11:14
                        (upravené
                28.4.2010
                o
                11:22)
                        |
            Karma článku:
                8.36
            |
            Prečítané 
            1938-krát
                    
         
     
         
             

                 
                    Hurá ide sa na výlet. To bude krásny deň. Mám rada ak okrem prechádzky,čo nie je tak časté, sa môžem povoziť v aute. To bude na dlhšie a moja rodina bude so mnou po celý čas. Po nejakom čase zastavujeme, neviem ako dlho sme šli, hlavne, že sme spolu. Berú ma na ruky a dávajú na zem. Fuj, to je špina! Hlina, kamienky a ešte aj fúka.
                 

                 Pozerám okolo seba, nepáči sa mi tu. Prečo stojíme a prečo mám zrazu čudný pocit? Pozerám sa na svoju rodinu, čakám na povel. No moment! Prečo si sadajú do auta a zatvárajú dvere? Veď ja som stále tu. „ Hej, počkajte!" - otvorím svoje malé ústa a tak rýchlo ako viem, bežím smerom k autu. Moje krôčky nie sú dlhé, ako by mohli, keď som tak malá, ale nevzdávam to. Bežím a svojou rečou sa snažím to auto zastaviť. Nestačím...auto je ďaleko a ja už bežať nevládzem. Je na mňa príliš rýchle.   No nič, vrátim sa na miesto a počkám. Keď ma budú hľadať, uvedomia si, kde ma nechali. Som naštvaná! Som maličká, ide ma ľahko prehliadnúť, ale toto sa ešte nestalo. Nikde ma nezabudli, až teraz. Fajn, keď prídu po mňa, budem hrať chvíľu urazenú, o to bude lepšia večera.   Začína byť tma, vietor fúka silnejšie, je mi zima. Cítim ako sa celá chvejem,nie je sa kde schovať. Schúlim sa do klbíčka a zaspávam.   Nespala som dlho,počujem zvuk auta. Hurá konečne! Auto prešlo, nebolo to naše. Po ňom ďalšie a ďalšie,neviem koľko ich bolo, ale naše som nevidela. To predsa nie je možné,už tu dávno mali byť! Dobre, nebudem trucovať, ale nech už som doma. Je mi zima, som hladná a okrem vetru začína i pršať. Hrbím sa, privieram oči pred vetrom a dažďom, tá hlina okolo mi nerobí dobre. Zaostrím zrak...niekto ide. Pohybuje sa rýchlym krokom a smeruje ku mne. Dvihnem noštek, vetrím. Nie, tak túto vôňu nepoznám. Už je blízko. Prihovára sa, skláňa sa a chcem sa ma dotknúť. Odskakujem. Nechcem, aby sa ma niekto cudzí dotýkal. Ten človek si nedá povedať. Hovorí na mňa a snaží sa ma dotýkať. Otočím sa a utekám. V kľučkovaní som naozaj špička. Pridám a som dosť ďaleko. Otočím sa. Ten divný človek tam stojí a stále na mňa pozerá. Po chvíli odchádza. Ide sa schovať pred dažďom, i ja by som už šla, ale naši ešte neprišli. Pomaly sa vraciam na miesto, kde som pôvodne sedela. Čakám.   Prešlo niekoľko dní. Som zmätená. Ten človek sa stále na mňa chodí pozerať. Som premočená, je mi zima a som hladná. Chcem ísť domov. Vonku je stále zima. Premýšľam.Ten človek zase prišiel.Už mám toho tak akurát dosť! Som hladná a nechce sa mi stále behať. Ešte som teda raz odehla do stredu poľa a čakám kým odíde.   Odišiel, to bola doba! Odchádzam i ja. Rozhodla som sa, že pôjdem našim naproti. Už nemôžem čakať. Spinkať sa tu nedá, okolo stále prechádzajú cudzie autá.Schovať sa nemám kam, naokolo je len pole a veľmi tu fúka. Rozhodla som sa ísť cestou, ako odchádzal a prichádzal i ten podivný človek, čo sa ma chcel dotýkať.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Nebyť dokonalá sa oplatí.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Kedy končia kamarátstva?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Nastavujem zrkadlo.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Už nehovorím dcére: " Neplač!"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Babinová 
                                        
                                            Vyznanie.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Babinová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Babinová
            
         
        evababinova.blog.sme.sk (rss)
         
                                     
     
         Som mama, manželka, dcéra a sestra, kamarátka. Jak pribúdajú roky, prichádzajú aj skúsenosti, ktoré by som nemenila. Som vďačná za všetko, čo mi život dal a budem vďačná aj za to, čo ešte prinesie. 

   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    40
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1645
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Pochod myšlienok
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




