
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Dunaj
                                        &gt;
                Nezaradené
                     
                 Funkčná rodina II. 

        
            
                                    12.6.2010
            o
            10:49
                        |
            Karma článku:
                3.16
            |
            Prečítané 
            502-krát
                    
         
     
         
             

                 
                    Poďme ešte k tým sľúbeným rodinným centrám, keďže dnes je posledný deň, kedy sa môžete rozhodnúť dať šancu novým perspektívnym ľuďom s odvahou ísť na politický pranier a vysporadúvať sa ďalšie štyri roky s tým, že médiá rozoberú každý šev na ich saku (bolo drahé? Kúpil ho v Parndorfe či v Miláne?...) a to, či remienok na hodinke je z kože krokodíla alebo muchy Tse-Tse.
                 

                   
 Rodinné centrum v širšom kontexte obsahuje niekoľko cieľových skupín: bezdetné ženy, bezdetné páry - snúbencov, mamy, otcov, tehotné ženy, deti od 0-6 rokov, ich súrodencov, starých rodičov, to znamená, že to nie je iba o mamkách s batoľatami, ktoré sa chcú stretnúť, lebo „materská“ určitým spôsobom izoluje. Do procesu výchovy vstupujú všetky aspekty rodiny, všetci členovia sa pod vedením odborníkov učia zaobchádzať s malým človiečikom, riešiť problémy, ktoré môže rodičovstvo priniesť a batoľa sa rozvíja v maximálnej možnej miere. Rozdiel medzi tradičnými materskými centrami je najmä v tom, že rodinné centrá majú aj v zahraničí inšitucionálny charakter, nevedú ho mamy z nadšenia ale zamestnáva odborníkov z oblasti pediatrie, psychológie, muzikoterapie, lektorov cudzích jazykov, alergológov, etc.etc... Mladé bezdetné páry sa môžu prísť taktiež vzdelávať v praxi a pozorovať páry s deťmi, dokonca v niektorých štátoch to majú ako povinnú súčasť prípravy na rodinný život. Tak ako sú v tehotenstve z preventívnych dôvodov zavedené povinné preventívne prehliadky a odbery v 15. týždni, tak by bolo možné zaviesť poukazy pre každú rodinu na návštevy, kurzy či prednášky v takýchto centrách.   Ďalšou zaujímavosťou z rodinných programov a politických téz ktorú som na radosť mojich pacientok aj na radosť svoju našiel, je legislatívna úprava firemných jaslí a babysitteriek. Dávnejšie som čítal o sociálnych vymoženostiach korporátnych amerických firiem, kde môže žena na materskej dovolenke pokojne zostať dva-tri mesiace a potom zveriť mimino kvalitnej babysitterke, ktorú zaplatí firma, prípadne väčšie batoľa umiestniť do firemných jaslí. ... s pocitom že po dôležitom rokovaní môže povedzme  nakojiť svoje dieťa o dve-tri kancelárske dvere ďalej alebo ho kedykoľvek skontrolovať, zakývať mu, absolvovať s ním spoločné obedy, etc. Legislatívnou úpravou by bolo možné dosiahnuť, aby zamestnávateľ dostal odvodové ale aj iné úľavy, či priamu podporu pri zriadení takéhoto zariadenia. Podľa Edity Pfundtner, ktorá sa dlhodobo zaoberá touto tematikou (ako inak, žena si musí najskôr sama preskákať materstvom aby zistila čo iným matkám chýba), by bolo možné upraviť aj určitý povinný percentil zamestnávaných žien po rodičovskej dovolenke vo firmách spĺňajúcich určité kapacitné kritériá.    Neplatenie alimentov je ošemetná téma a všetko čo sa doposiaľ dokázalo urobiť na tomto bojovom poli je odobratie vodičského oprávnenia v prípade chronických neplatičov. Samozrejme tak ako vo všetkých oblastiach, aj tu sú jednotlivci, ktorí chcú systém či platcu zneužiť, avšak v 90% prípadov ide o ženy, ktoré sú na tú drobnú paušálnu sumu určenú súdom ekonomicky odkázané. Toto sú témy na ktoré by žiadna vláda nemala zabúdať a už vôbec ich nekvalifikovať ako POPULizmy, ale ako nutnosť, pretože  sa týkajú POPULácie.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Keď nás tu bude 100 000 Jano sa oholí!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Manažérov denník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Najlepší kúzelnícky trik na svete!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Konkurz do zamestnania
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Dunaj 
                                        
                                            Funkčná rodina
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Dunaj
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Dunaj
            
         
        dunaj.blog.sme.sk (rss)
         
                                     
     
        Žijeme tak krátky život až mi je to smiešne ako sa staviame do roly toho, ktorý má vo svojom eLVéčku všetky planéty. Niekto je hore a niekto je dolu. Ten dolu je na ceste a potkýna sa o ďalších
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    694
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




