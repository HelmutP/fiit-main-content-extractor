
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Giač
                                        &gt;
                Nezaradené
                     
                 Babylon a homonymá. 

        
            
                                    7.6.2010
            o
            19:29
                        (upravené
                7.6.2010
                o
                19:36)
                        |
            Karma článku:
                3.64
            |
            Prečítané 
            914-krát
                    
         
     
         
             

                 
                    Homonymum -  slovo gréckeho pôvodu, svojou zvukovou podobou zhodné s iným slovom, ale majúce úplne odlišný význam. Cool - moderný džezový sloh z konca štyridsiatych rokov Toľko v Slovníku cudzích slov vydanom v roku 1983 vydanom Slovenským pedagogickým  nakladateľstvom. ....
                 

                 ... cool jazz [kúl džez] -u m. ‹a› hud. (v polovici 20. stor.) džezový sloh racionálne rozvádzajúci melodicko-harmonické výboje bopu, chladný džez vysvetľuje  novšie „Slovník cudzích slov (akademický)Druhé, doplnené a upravené slovenské vydanie, SPN 2005. Akademický slovník cizích slov, Academia Praha."   V Anglicko - slovenskom slovníku z roku 1967 od toho istého vydavateľa si môžeme prečítať nasledovné významy slova cool.   chladný, svieži, pokojný, triezvy, rozvážny, chladnokrvný, ľahostajný, neprívetivý, nevľúdny, bezočivý, nehanebný, odvážny, trúfalý.   Ak mi niekto povie, že som cool tak si myslím, že  v najlepšom prípade ma označuje za trpáka.  Už vôbec nie, za  rozvážneho  alebo odvážneho,   lebo na to sa u nás zvykol používať výraz trieda alebo klasa.  Aj  keď to druhé slovo sa tiež nenájde v slovenskom pravopisnom slovníku.  Nie som o nič múdrejší ani vedy, ak mi niekto vysvetlí význam slova tým, že ho používa mládež.  Tak na oplátku si ja o dotyčnom, dotyčnej myslím tiež,  že je cool, rozumej trpák, trpáčka.   Ak pán Boh potrestal  ľudstvo stavanie Babylonu zmätením jazyka,  tým že vznikli nové jazyky, tak pri angličtine si dal extra záležať.  Homoným ma neúrekom.  Že by sa tým zvýšila jej zrozumiteľnosť  sa chváliť nemôže. Slovenčina je živý jazyk.   Ako každý jazyk je  spoločenským  výtvorom.  Každý spoločenský výtvor sa dá zveľaďovať, obohacovať alebo domnelým obohatením znehodnotiť.  Preberaním cudzích slov sa dá slovenčina aj obohatiť aj znehodnotiť. Bezpochyby k obohateniu slúžia slová na pomenovanie javov vznikajúcich  v dôsledku spoločenského vývoja  v tom najširšom slova zmysle na ktoré slovenčina nemá vlastné výrazy, prípadne aj  v má v podobe ich kombinácii. (Koniec týždňa, elektronická  pošta.)  Prienik slov víkend alebo ímajl alebo jednoduchšie majl  asi znamená obohatenie jazyka, lebo aj rodený  Angličan nám porozumie, čo  myslíme slovami víkend alebo ímajl.   Používanie takých cudzích slov, ktoré nie sú odborné,  v bežnej reči, na ktoré má slovenčina vlastné výrazy,  považujem za neznalosť slovenčiny, alebo to čo sa voľakedy volalo malomeštiactvo.  Ale ak chce niekto slovenčinu znásilňovať  cudzími homonymami  ako napríklad slovom  cool,  dopúšťa sa podľa mňa znehodnocovania jazyka.  A ak niekto používaním slova cool chce iba nadbiehať „svetobežníkom" ,  mal by sa zamyslieť na obsahom pojmov národ a jeho identita.  Identita je síce tiež slovo cudzieho pôvodu, ale má svoj presný odborný význam. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Komunálne voľby prerušené na 38 minút.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Posledný a či ostatný?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Štyri metre štvorcové.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Starý Martin a Božie napomenutie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Giač 
                                        
                                            Paraglajdisti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Giač
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Giač
            
         
        giac.blog.sme.sk (rss)
         
                                     
     
        Vekovú hranicu, keď som mohol hovoriť, že už mám po mladosti a do dôchodku ďaleko, som už prekročil.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    70
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1079
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            O slovenčine
                        
                     
                                     
                        
                            Počítače
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




