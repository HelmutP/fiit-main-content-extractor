

   
 Vzchopila sa. Ešte nie je neskoro. Ešte žije. Musí ísť za ním. Musí ho vidieť, musí sa s ním stretnúť a hoci on už nerozpráva, ona chce dopovedať to, čo ostalo nedopovedané. 
 Cesta po diaľnici sa nekonečne vliekla. No o to väčšou rýchlosťou utekali spomienky. Prichádzali i odchádzali. A niektoré... aj ostávali. Boli mocné a zväčšovali istotu bezmocnosti. 
 Jedna z nich hovorila o chvíľach, keď jej bolo ako dievčatku ťažko. Vtedy prišiel on, so svojou mocnou dlaňou a vzal ju do náručia. Nepovedal toho veľa, ale ona vedela, že je to jej „ocko" a že to náručie sa stáva miestom bezpečia. 
 Zastavila sa aj pri chvíli jej životného rozhodnutia. Vedela, že musí od neho odísť. Nepovedal jej, aby ostala. No bolesť sa v tej chvíli dala krájať a zväčšovalo ju každé objatie bez slov. 
 Keď si spomínala na chvíľu, ako spolu zachraňovali „diplomovku" a robili všetko, aby ju odovzdali včas, usmiala sa. Bol to on, čo premýšľal aj za ňu, robil jej šoféra a opakoval: „Neboj sa, zvládneme to. Bude to dobré..." 
   
 Konečne bola doma. Spoločne s mamou sa vydala na cestu do nemocnice. Plná bolesti, strachu i nádeje, ale i vďačnosti za to, že ešte je šanca sa stretnúť, vidieť, pohladiť a objať... 
 Vstúpila do izby. Ležal na posteli. Sklenené oči zaliali slzy. Mozgová príhoda zasadila úder. Pravá strana tela vypovedala službu, rečové centrum sa snažilo o artikuláciu. 
 Boli spolu. Stačila táto chvíľa, aby pochopili, akí sú si vzácni a ako sa potrebujú. Nerozdeľovalo ich nič, ani politika, ani životné štýly. Boli spolu a to bolo podstatné. 
 Chceli sa objať. „Ockovské" objatie malo vždy svoju silu. Bolo prítomné pri každom príchode i odchode. Stalo sa rituálom... a tu zrazu. Skončilo pokusom. Ľavé rameno objímalo jej plecia, kým pravé patrilo chladnému severnému pólu a padalo pod posteľ. Cítila samotu, ponárajúcu sa do ticha. V ich očiach objavil zmätok, bolesť a otázka: Je to koniec? 
 Vzpamätala sa. Nadýchla sa a cez slzy sa rozhodla za oboch: Nevzdajú sa, ba začnú bojovať nanovo. Bojovať o život, o novú nádej, o silu pre ruku i pre nohu... budú bojovať o nové mocné objatie. 
 A bojujú. Stretávajú prehry i výhry. Nemocničný personál, ktorý vníma svojich pacientov ako handry i ako ľudí. Klopú na  inštitúcie, ktoré liečia len tých, ktorí o kvalitu svojho života prišli pri havárii, nie pri mozgovej príhode... A "papierovo" bojujú o výnimku, aby sa mohol liečiť tam, kde snáď iní majú prednostné právo... Mimochodom pýtajú sa či nášmu štátu ide o liečenie chorých všeobecne, alebo o to, akým spôsobom prišli k ochrnutiu? Či nie sú všetci chorí rovnocenní, najmä vo chvíli, keď potrebujú pomoc? 
 Berú do svojich rúk stále nové a nové zbrane, no tou najväčšou je neustále presviedčanie sa a radosť z toho, že sú spolu, istota, že sa majú radi, že túžia žiť. Vedia, že aj tento čas je pre nich darom od Niekoho zhora, od niekoho, ktorý dopustí, ale neopustí... 
 A jeho meno poznáte... Každá bezmocnosť sa nesie ľahšie, ak človek vie, že ju nenesie sám. 
   
   

