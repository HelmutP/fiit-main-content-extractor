

   
 Stará ošúchaná zhrdzavená a vŕzgajúca Škoda 105L by zastala kdesi na námestí pred množstvom ľudí. Vystúpil by som z nej upravený, zabuchol by som dvere, podišiel bližšie ku kamere a povedal: „Ja síce nemám na dobré auto, ale vy máte na dobrého politika.“ Usmial by som sa a dodal: „Voľte MaV.“ Zrazu by sa spoza kamery ozval hlas: „Hej, to znelo ako: ‚Voľte ma‘, takto to nemôže byť. Buď to vyslovíš zreteľne, alebo povedz Em á Vé.“ Zaškľabil by som sa v štýle akože-prepáč-pán-režisér a hrdo by som vyhlásil: „Voľte Em á Vé.“  Stmievačka. Zobrazilo by sa logo mojej strany (nemám o ňom konkrétnu predstavu), pomaly by sa približovalo, pod ním by sa zjavil slogan, ktorý by bol prečítaný krásnym hlasom Mareka Majeského: „Preukážte lásku strane Em á Vé a ona preukáže lásku vám.“ Dobré, nie? 
 Keď sa dajú voliť veselé kapurkové, prečo by sa nemali dať voliť milujúce? 
   

