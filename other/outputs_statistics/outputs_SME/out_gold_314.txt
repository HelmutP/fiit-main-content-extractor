

 

 
 

Nič sa nerodí úplne ľahko a žiaľ, na Slovensku nemáme takú tradíciu v písaní scenárov ako napríklad Česi, od ktorých sa snažili naši tvorcovia odkopírovať seriál. A tak som im dal šancu. Viac menej som si doteraz pozrel 48 častí, sem-tam som vynechal. Úctyhodný výkon, čo podľa môjho kolegu svedčí o mojej trpezlivosti, vytrvalosti a hlavne hrošej koži. A on tak trochu má aj pravdu. 
 
Ordinácia v ružovej záhrade má niekoľko fatálnych chýb a nedostatkov. V prvom rade úbohý scenár, ktorý dokáže predvídať aj môj 12-ročný synovec. Stereotypné scény, nudné postavy bez dynamiky, nejasná motivácia, absencia súvislostí, školácke dialógy plné klišé...údajne ich píšu mladí ľudia a takmer každý diel má iného scenáristu. Ak je to tak, už chápem. Takýto seriál sa totiž nedá robiť systémom fabrika, čiže zakaždým píše, dokonca nakrúca a vedie dramaturgiu niekto iný. Jednotlivé časti sú tak rozkúskované a nikto ich už nedá dokopy ani sekundovým superlepidlom. Chápem, že zopár ľudí ako režisér, scenáristi, kameramani či herci potrebujú stabilný príjem, ale takýmto spôsobom? 
 

Veľkým sklamaním sú pre mňa herci. Podľa mňa (každý to môže vnímať inak) stoja za to Milka Vášáryová, Emil Horváth, Táňa Radeva, Tomáš Maštalír, z tých mladších Zuzka Kanócz či Janko Alžbetkin. Prevažná väčšina ich kolegov je veľmi, veľmi nepresvedčivá. Rozchod nedokázali zahrať tak, aby ma rozrušil. Narodeninová oslava tínedžerky vyznela úboho. A najhoršie je to pri scénkach, kde sa majú rozčuľovať a hádať. Chvíľami mám pocit, že sme skutočne holubičí a mierumilovný národ, pretože sa ani vyhádať nedokážeme.   
 
Mám pocit, že postavy v tomto seriáli nežijú, ale o to častejšie afektujú, preháňajú, prípadne idú na pol plynu. Možno majú mladí scenáristi dodať seriálu moderný cool lifting, aby pritiahli mladú generáciu. No darí sa im práve opak. Mnohé scény vyznievajú trápne, až sa niekedy pristihnem, že podobne trápne sa cítim aj ja a držím hercom palce, aby to nepokašľali ešte viac a scéna sa už skončila. Nuž, zle napísanú scénu nezahráte dobre. Ak by boli texty kvalitnejšie, určite by aj seriál vyznel lepšie.  
 

Je mi z toho všetkého smutno. Tak som veril a dúfal. Tak rád by som zatlieskal, doprial tomuto seriálu a najmä ľuďom za ním úspech a uznanie... 
A možno je chyba vo mne a nie som typický televízny divák, ktorému je Ordinácia určená. Veď keď sa človek pozrie na stabilnú a nie celkom zlú sledovanosť, nemôže to byť až taký prepadák.  
So sebazaprením a dávkou zhovievavosti dám teda dnes večer ešte poslednú šancu Ordinácii v ružovej záhrade. Blíži sa jubilejná 50. časť, tak uvidíme, či budem oslavovať aj ja...
 
 
Foto markiza.sk 
 

