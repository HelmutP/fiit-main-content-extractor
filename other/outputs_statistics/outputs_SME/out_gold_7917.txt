

 Namiesto odpovede som skočil do najbližších potravín a nakúpil 12 balíčkov cukríkov. 
 "Tu je dvanásť balíčkov cukríkov," vravím mávajúc pred nimi práve kúpenými cukríkmi. Decká sa dočiroka usmiali. Majú totiž veľmi radi, keď je výsledkom súťaženia na etike sladká odmena :o) Teraz však prišlo niečo nečakané. Nezačala žiadna súťaž, ani bojová úloha. Jednoducho som im tie balíčky začal rozdávať. Zostali v rozpakoch a čakali, čo bude ďalej. "Každý balíček predstavuje nejakú vašu vedomosť, alebo zručnosť. Zo "svojho know - how balíčka" si však môžete vziať len jeden cukrík. Ak sa ale rozhodnete, môžete ponúknuť svoje poznanie spolužiakom. Každému prirodzene len raz :o)" V miestnosti to zašumelo a decká sa rozbehli po triede. O malú chvíľu mal každý z nich okrem svojho vlastného "poznania" aj jedenásť "poznaní" svojich spolužiakov. 
 "Ak svoju vedomosť a poznanie použijem len pre seba, má z neho úžitok len jeden človek. Ja. Ak ju však budem šíriť medzi ďalších a ďalších, ktorí ju budú používať, rastie jej význam. Inak povedané: Význam môjho "poznania" stúpa s množstvom ľudí, ktorým toto poznanie prinesie úžitok..." Mlčali. Asi preto, že mali plné pusiny poznania :o))) 
 V pondelok prišli Holíčania. Temer 40 detí sa rozdelilo na tri skupiny a moji malí šušníci ich postupne učili, ako robiť animované filmy v kuchyni, alebo ako pripraviť školský song podľa vlastného gusta. Po troch hodinách tvrdej driny sa unavení a šťastní rozlúčili so svojimi žiakmi, ktorí dnes znásobili význam ich poznania... 
   

