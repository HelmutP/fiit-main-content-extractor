

 Považská Bystrica ju už čo nevidieť bude mať a už by si pomaly mali hovoriť, že čo nás nezabije, to nás posilní... Nie že by bola Považská Bystrica v tomto jedinečná, ani u nás nie je, niečo podobné máme v Podtúrni, aj v Prahe slávny Nuselský most. 
 Nechcem tu obhajovať koncepciu prieťahu diaľnice  mestom, tá už je v Považskej Bystrici faktom, na ktorý si treba len zvyknúť a tešiť sa na rýchle spojenie a upokojenie situácie v meste. 
 Do Považskej Bystrice nás priviedlo rodinné stretnutie. Toto mesto a okolie nemám veľmi preskúmané, tak som sa na návštevu tešil a nesklamala ma, bola to síce rodinná oslava, ale doplnená turistikou po okolí, presne podľa našich predstáv a možností. 
 V Považskej Bystrici som kedysi dávno oslávil svoje 33. výročie v príjemnom prostredí medzi priateľmi v Kolibe niekde na konci mesta. Aj také bývali akcie Hifiklubu. Súľovské skaly sme už dvakrát prešli, ale ešte som nebol na Považskom hrade.  
 Do mesta  sme vošli bez problémov, na sídlisko Rozkvet sa dá našťastie odbočiť krátko po zídení z nedokončenej diaľnice. Prvý výlet nás viedol do Javorníkov, na Česko - Slovenskú hranicu. 
 Z Považskej Bystrice sme šli v dvoch skupinách a keďže malý Rišo, náš oslávenec ešte potrebuje kočík, tak s ním išli cez Lazy pod Makytou až na parkovisko pri rekreačnom zariadení Kohútka. My sme išli cez Hornú Maríkovú do osady Vlkov k osade Stolečné a odtiaľ pomerne strmým výstupom k horskému hotelu Portáš. 
  
 Na Portáši sme sa stretli na prvé občerstvenie. Po hraničnej čiare vedie pohodlná cesta, horský hotel Portáš leží na Českej strane. 
  
 Obrovské parkovisko neďaleko rekreačného zariadenia Kohútka je prístupné z českej, aj zo slovenskej strany. Z našej strane je potrebné prekonať jeden úsek cesty vo veľmi zlom stave, podobne aj pri ceste k osade Stolečné. 
 Cestou hore, úzkou cestou lesom som si hovoril, že nedajbože stretnúť nákladiak naložený drevom a vtom sa objavil za najbližšou zákrutou. Všetko dobre dopadlo, ale vidieť nad sebou kolísajúcu sa horu dreva, ktovie ako zaistenú, je pre mňa vždy horor. 
  
 Na Kohútke majú kohúta, ktorý kikiríkaním ohlasuje, že kuchár má jedlo pripravené na servírovanie. Príjemné prostredie, na okolí množstvo zjazdoviek a vlekov, aj neďaleko chaty sa stavia nová lanovka z českej strany. Tak som prvýkrát spoznal pohodu na Slovensko - Českom pomedzí... 
 Poďme teraz na hrad... 
  
 Na Považský hrad sa stúpa z Považského Podhradia, na vlastné nebezpečie, lebo hradné múry nie sú dostatočne zabezpečené. Výstup je tiež pomerne strmý, ale relatívne krátky a stojí za to nielen hrad, ale aj tie výhľady do okolia a na novú diaľnicu. 
  
 Stavba diaľnice od Vrtižeru pokračuje na estakáde vedľa Hričovského kanála. 
  
 Diaľničný privádzač Vrtižer v detailnom priblížení, tam to zatiaľ končí. Zaujímavo o Považskom hrade píše Peter Martinisko na svojom blogu a má tam aj záber na túto križovatku z roku 2006. Predpovedá skorý zánik hradu, v tom dúfam, že nebude mať celkom pravdu. 
  
 Z hradu, nad Považským Podhradím, most cez kanál, za ním diaľnica. 
  
 Detail konštrukcie estakády. 
  
 O kúsok ďalej ďalší privádzač a diaľnica vstupuje nad mesto, na pylóny. Toľko dovidieť z hradu. 
  
 Záber z auta za jazdy, stračia nôžka... 
 Manínska tiesňava... 
  
 Prechádzame Manínskou tiesňavou. 
  
 Na lúky nad dedinou Záskalie. 
  
 Pohľad na málo známu Kostoleckú tiesňavu. 
  
 S krásnymi útvarmi skál. 
  
 Mohutný skalný dóm.  
 Dva dni v príjemnom rodinnom prostredí medzi horami,  s pekným počasím ako na objednávku sa skončili. Na spiatočnej ceste, pred Trenčínom nás chytil prívalový dážď, ale už nám nemohol pokaziť náladu... Stierače stierali, kolesá sa točili... 
   

