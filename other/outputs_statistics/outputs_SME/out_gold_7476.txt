

 Slota a Belousovová dnes spolu kopú v jednom zákope, no boli časy, keď sa Anička a Janík poriadne osočovali, čoho dôkazom je aj tento odkaz na stránku. 
 Známe sú výrazy, akými Ján Slota častoval Annu Belousovovú - "hus", "šialená krava", i "stará dievka". 
 Anna Belousovová to na oplátku Jánovi Slotovi vrátila, označila ho za "mafiána", "slovenského zbohatlíka", či "buditeľa chorvátskeho pobrežia". 
 Ján Slota v tých časoch založil Pravú SNS, ktorá vznikla ako reakcia na situáciu v SNS pod vedením Anny Belousovovej, vtedy ešte Malíkovej. Ich rozdelenie spôsobilo neúčasť národniarov v parlamente v rokoch 2002 až 2006. Strany sa zjednotili 4. apríla 2005, keď obaja zjavne pochopili, že spolu to dokážu /vo voľbách 2002 mala SNS a Pravá SNS spolu okolo 6,9 %/. A dokázali. Spolu sa do parlamentu v roku 2006 dostali, sú vo vláde so Smerom a HZDS. Po niekdajších urážkach medzi nimi zrazu niet stopy. 
 Priatelia, ja na tom nechápem len jedno. Ako môžu tí dvaja hrať roky takéto divadlo? Belousovová a Slota totiž svoje pravé tváre zjavne ukázali práve v rokoch, keď trval ich spor. Vážne si tí dvaja myslia, že im obom uverím toto ich predstavenie? 

