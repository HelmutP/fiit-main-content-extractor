

 Slovenskí voliči tak dali jasný odkaz politikom, že maďarská karta už nie je smrtonosnou zbraňou s ostrými hranami. Zmenila sa na ošúchanú handru, ktorú možno akurát len s odporom vyhodiť. SNS zobrala hlasy kotlebovcom, aj keď tu možno predpokladať, že voliči boli pragmatickí, nie žeby sa im štítilo hajlovať. Ale hlasy SNS zobral Smer, ktorý síce dúchal do národnostnej pahreby, koľko mu len pľúcka stačili, ale v prejavoch bol výrazne umiernenejší. 
 Absolútnou bombou bol nehanebný výprask SMK a úspech ich konkurentov zdôrazňujúcich spoluprácu a spájanie. Voliči na juhu tak dali "svojim" politikom najavo, že nechcú byť čímsi extra, ale riadnymi občanmi tejto republiky s právom rozprávať, ako im huba narástla. Nechcú revidovať Trianon, nechcú sa sťahovať za Dunaj, nechcú rozdupať slovenskú svojstojnosť pod kopytami malých chlpatých poníkov. Konečne Slováci dostali veľmi zreteľný signál, ako celý ten cirkus vnímajú občania maďarskej národnosti. 
 Ako povedal ktorýsi z oslavujúcich - teraz je čas riešiť ozajstné problémy, nie vymyslené spiknutia. Nehrať do karát maďarskej vláde odvádzajúcej pozornosť od vnútorných problémov provokovaním susedov. Je čas, aby sa Jano a Anča zase rozhádali, rozpadli a zanechali po sebe iba niekoľko trápnych kapitol slovenskej histórie. 
 Rád by som veril, že Slovensko urobilo ďalší krok zo svojej neurotickej puberty a smerom k dospelosti. 

