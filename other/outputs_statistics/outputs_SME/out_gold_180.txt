

   
 
 
Každý rok sa mesto snaží obohatiť program vinobranie o niečo nové a to formou sprievodných podujatí. V tomto roku je to: 
 
 
- Pezinský permoník - medzinárodná výstava minerálov, fosílií a drahých kameňov, 
 
 
- Výstava vinohradníckej, záhradnej a komunálnej techniky, spojená s predvádzaním      (na priestranstve na rohu Trnavskej a Šenkvickej cesty (oproti replike sochy slobody) 
 
 
- Pekársku šou pôjdem pozrieť a ochutnať do stanu, ktorý patrí Cechu pekárov, ktorý je na námestí vo veľkoplošnom stane v rámci pekárskej šou a  ponúka chlieb a pekárske výrobky ako vhodnú potravinu k vínu. 
 
 
a už tradične - Fyzulnačka - medzinárodná súťaž vo varení fazuľovej polievky. Ta Fyzulnačka je super akcia, bo po navarení polievok v kotlinách môžu ľudia ochutnávať rôzne chute fazuľovej polievky. Tejto fyzulnačky sa osobne zúčastním (sobota od 9.00 do 16.00), pretože som bol už minule na tom a veľmi sa mi to páčilo aj chutilo. Podotýkam, že najlepšie je sa tam motať okolo 12.00 a 13.00 hod, bo neskôr  pri veľkom počte ľudí môžete obísť naprázdno. Viac o fyzulnačke na tejto linke: http://www.pezinok.sk/index.php?yggid=541 
 
 
Podrobný program akcií vinobrania si môžete pozrieť na webe mesta Pezinok: http://www.pezinok.sk/index.php?yggid=539 a ak by ste sa rozhodli prísť pozrieť do Pezinka, tak za zmienku stojí aj to, že u nás výnimočne na železničnej stanici zastavia vybrané  rýchliky. Viac o dopravnom spojení na tejto linke: http://www.pezinok.sk/index.php?yggid=540 
 
 
A ešte aby ste si vedeli predstaviť ako to tu vyzerá, tak pripájam pár fotiek z dnešného otváracieho dňa. Zajtra už budú ulice plnšie ako dnes. Takže zajtra sa vidíme... 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 Par fotiek stačí, zbytok treba vidieť na vlastní bulvy :-) 
 
 
 
 

