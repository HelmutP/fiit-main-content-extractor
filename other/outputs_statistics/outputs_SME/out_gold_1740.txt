

 Osoba sa snažila silou mocou dvere otvoriť, ale nešlo jej to. V duchu som sa modlil, aby sa jej to ani nepodarilo, aj keď neviem, ako mohli byť tie dvere zamknuté. To bolo jedno. Ja som nemohol nič robiť, nemohol som sa hýbať, myslel som si, že ma to učarovalo. Srdce mi bilo od strachu, až som skoro zinfarktoval. Zrazu sa tá osoba prestala dobýjať do izby, pozrela sa na mňa posledný krát a odišla. V tom som od strachu otvoril oči a ostal som zaskočený. 
 Všetko naokolo bolo rovnaké. Nerozumel som tomu, čo sa stalo. Snívalo sa mi? Ale všetko som videl. Bolo to úplne reálne. Videl som ducha? Démona? Mimozemšťana? Či iného netvora? 
 Vstal som a poprechádzal som sa po byte. Nikto tam nebol. Pozrel som sa mame do izby a ona spala. Zapol som teda počítač a s vedomím, že na všetko musí existovať racionálne vysvetlenie, som išiel hľadať odpovede. Verte, či nie, na internete odpovede nájdete (ak viete dobre po anglicky). Riadim sa mottom: „Strach sa dá zahnať jedine poznaním a pochopením." 
 Internetové fóra 
 Našiel som rozličné fóra, kde ľudia opisujú podobné zážitky, dokonca niektorí videli osobu s bielou maskou. Niektorí popisujú démonov, iní popisovali iba pocit, že „niečo" je v izbe a sleduje ich to. Popisovali rôzne zvuky v ušiach alebo „niečo" im sadlo na hrudník... 
 To ma zaviedlo ďalej a našiel som svoju odpoveď. 
 Diagnóza - Spánková paralýza (sleep paralysis) 
 Spánková paralýza je stav, keď je telo dočasne neschopné sa hýbať. Paralýza nastáva pri zaspávaní (hypnagogická) alebo pri zobúdzaní (hypnopompická). Osoba nemôže hýbať žiadnou časťou svojho tela a ma minimálnu kontrolu nad telom. Dokáže iba hýbať očami a dýchať. Fyziologicky je spánková paralýza spojená s paralýzou (ochrnutím), ku ktorému dochádza počas tých fáz spánku, keď máme živé sny (tzv. REM atónia). V REM fáze náš mozog účinne funguje, ale telo by malo "spať" a nehýbať sa. Spánková paralýza teda vlastne slúži na to, aby sa naše snové pohyby neprenášali na naše fyzické telo, teda aby sme si počas spánku neublížili. Keď sa zrazu zobudíme, tak mozog si myslí, že spí a ostane v paralýze. Počas spánkovej paralýzy môže dôjsť k halucináciám: Vidíme obrazy, alebo zvuky. Osoba si môže myslieť, že niečo stojí pri nich, alebo môže počuť čudné zvuky (ako ja na chodbe a potom som videl osobu, čo chcela vojsť do izby). Môže vidieť rozličné veci, osoby a má rôzne vidiny. Dokonca som mal aj pocit, že mi niekto chce ukradnúť dušu. Čítal som, že aj iní mali taký pocit, akoby ich niekto sledoval, ale v miestnosti nikto nebol. Často sa dostaví veľký strach. Najčastejšie ľudia popisujú pocit, ako by im niečo tlačilo na hrudník. Ťažko sa im dýcha. Paralýza môže trvať od niekoľko sekúnd až po niekoľko minút. 
 Neviem, koľko času ubehlo, no bolo to krátke. Ja osobne som si uvedomoval, že niečo nie je v poriadku, že som v akomsi polospánku.  Nerozumel som však, prečo sa nedokážem hýbať. Bola to zaujímavá skúsenosť a aspoň sa nemusím báť, že sa mi niečo desivého prihodilo. Viem, že nie som jediný, ktorému sa to stalo. 
 Na internetových fórach sa ľudia často pýtali, či videli duchov, alebo je to niečo iné, pretože tomu nerozumeli. Verte, že som vôbec nevedel rozoznať, či to bolo reálne, alebo iba sen. A keď som o tom nič nevedel, mal som strach... Neviem čo sa dá robiť, ale viem, že potrebujem menej stresu, viac pravidelného spánku a možno to už nezažijem. 
 Mýty a legendy v rôznych kultúrach 
 O spánkovej paralýze má takmer každá kultúra svoje vlastné legendy. Spánková paralýza je často nazývaná syndróm Starej čarodejnice, kvôli spoločným znakom, keď ľudia videli alebo vnímali prítomnosť starej čarodejnice. Tá vyvíjala tlak na hrudník alebo vyvolávala pocit dusenia. 
 V africkej kultúre, je spánková paralýza bežne označovaná ako "čarodejnica, ktorá jazdi na tvojom chrbte". 
 V japonskej kultúre sa spánková paralýza nazýva kanashibari (金縛り, doslova "zviazaný alebo upevnený kovom," z Kane "kov" a Shibari "viazať, zviazať, upevniť "). 
 V tureckej kultúre je spánková paralýza často označovaná ako "karabasan" ( "temný pritláčač"). Verili, že je to stvorenie, ktoré v spánku napáda ľudí, tlačí im na hrudník a vyráža im dych. 
 V Grécku a na Cypre ľudia verili, že spánková paralýza nastane, keď sa duch so zvieracou podobou snaží obeti ukradnúť reč alebo sedí na jej hrudníku a chce jej spôsobiť udusenie. Duch sa volal Mora, Vrahnas alebo Varypnas (grécky: Μόρα, Βραχνάς, Βαρυπνάς). 
 Toto boli informácie, ktoré som o tomto fenoméne našiel na internete. Doteraz som o ňom nepočul, no viem, že niet sa čoho báť. Ak vás v spánku prepadne nejaká príšera, tak sa neľakajte. Možno ste práve zažili spánkovú paralýzu. 
 A ak ma nabudúce navštívi muž s bielou maskou, tak si musím ísť odpočinúť, lebo to nedopadne dobre :-) 
 Vy ste sa už s tým stretli? 
 Zdroje a články: Slovenské: http://helar.wordpress.com/2007/12/16/ked-sa-pri-spanku-nemozeme-hybat/ 
 Anglické: http://www.associatedcontent.com/topic/46801/sleep_paralysis.html?cat=5 http://www.stanford.edu/~dement/paralysis.html 
 Wikipedia: http://en.wikipedia.org/wiki/Sleep_paralysis 
   
   

