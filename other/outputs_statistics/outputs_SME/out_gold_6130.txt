

 Ticho v spoločenstve májových vtákov za oknom. 
 Páči sa mi, keď so mnou jeho mamka vedie dôverný rozhovor. To, čomu sa hovorí rodinné tabu alebo rodinné tajomstvá. O čom sa členovia rodiny buď rozprávajú alebo o tom roky mlčia. Veci, ktoré rodiny buď zoceľujú alebo delia. Hovorila o tom večer pred spaním a potom šla spať. Jej slová som si pozliepala dokopy a vyskladala z nich malý rodinný obrázok. Takýto obrázok by som si v mojich predstavách mohla zarámovať a večer pre spaním položiť vedľa seba. 
 Potichu mu zašepkám, ako silno mi bije srdce. Započúva sa doňho a povie: "Fakt." 
 Ženské srdce veci nemôžu nechať tichobijúcim, ženské srdce musí cválať, musí sa cez neho prečerpať mnoho krvi za krátky čas. Musí premýšľať nad 13-ročným dievčatkom odpadnutým na ceste v nejakom deríriu tremens, ku ktorému on pristúpil s očami veľkými aké má vo vážnych situáciách. Aj ja som chcela niečo urobiť, pozrieť sa na tú priotrávenú tvár, niečo pochopiť, pozrieť sa do jej vnútorností a načúvať, o čom sa s ňou vtedy zhovára jej duša. Byť taká malí špiónka, veď ma poznáš. Veď vieš, že chcem vedieť všetko a hneď, že dokážem rýchlo utekať, poobede aj tri hodiny spať. Dnes v posteli vysokej až po parapetu okna, hrášková princezná. Spať tam a nikoho o tom neinformovať. Na vidieku, ďaleko, nech na mňa nepôsobí nič, len príroda a vlastná duša. 

