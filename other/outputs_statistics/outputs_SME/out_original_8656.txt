
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Foto Potočný
                                        &gt;
                Mýty bez legendy
                     
                 Nájde sa nás dosť do Mečiarovho doprovodu na smetisko dejín? 

        
            
                                    12.6.2010
            o
            18:01
                        (upravené
                13.6.2010
                o
                16:43)
                        |
            Karma článku:
                10.09
            |
            Prečítané 
            1526-krát
                    
         
     
         
             

                 
                    V dnešných parlamentných voľbách nejde o to, kto a s akým náskokom ich vyhrá. Naopak rozhodovať budú politickí trpaslíci tým, kto z nich sa cez päťpercentnú laťku prehupne. Pikantné je, že Vladimír Mečiar ako aktuálne najmenší trpaslík súčasnej politickej scény zohráva po dlhých rokoch opäť kľúčovú rolu. Niekto by sa azda nazdal, že nemáme výsledky volieb vo svojich rukách. Že nám prináleží iba oddane čakať, či náš milovaný Otec Zakladateľ laťku preskočí alebo nie. Mylne. V skutočnosti toto podujatie nerežíruje Len On ani jeho klub skalných ultrafans. Režisérsku taktovku máme pevne v rukách my ostatní.
                 

                 
 shooty.sk (okt.'02)
   Za tie roky ho všetci poznáme ako staré tretry. Uznávaní sociológovia zaoberajúci sa prieskumami verejnej mienky dokonca pred týždňom veľmi presne určili jeho potenciálnu výkonnnosť. Namerali mu hodnotu 116 816 voličov. Za posledný týždeň si ešte Betka M. z Kežmarku (77) uvedomila, že vláda vytopeným Východniarom nevedela akútne pomôcť a že súčasťou tejto vlády je aj On. Jozef K. z Čadce (69) hlasuje striedavo za SNS a HZDS, ale teraz svoju tradíciu poruší a zahlasuje opätovne za SNS. Čítal výsledky posledného prieskumu a nechce, aby jeho hlas prepadol. Božka z Vranova nad Topľou (81) sa zúčastnila predvolebného mítingu, ktorý v ich domove zorganizovala strana AZEN a tak staví vo voľbách na mladšie kone z rovnakej stajne. Ďalší traja dôchodcovia z Trnavského samosprávneho kraja naleteli Novej vízii Demokracie od Tibora Mikuša. Hoci tesne pred voľbami, prebehli ešte do jeho košiara. Anička S. zo Žiaru nad Hronom (89) žiaľ už nie je medzi nami. Prežila ťažký a predsa požehnaný život. Svojich päť detí vychovávala v povojnových časoch, dožila sa pätnástich vnúčat a dvanástich pravnúčat. Nech odpočíva vo večnom pokoji. Nemenovaný radový člen Hnutia prehral včera svoj dlhoročný vnútorný boj a priznal si, že celé tie roky žil v omyle. Vo voľbách bude dnes voliť po prvýkrát slobodne a podľa skutočného vedomia a svedomia. Jeho manželke sa tejto slobody zatiaľ nedostane. Ešte hodnú chvíľu potrvá, kým bude schopný priznať životný omyl aj pred ňou.   Volebná výkonnosť Otca Zakladateľa je teda známa na chlp presne. Do volebných úrn padne 116 808 lístkov z jeho menom. Tých najskalnejších zo skalných už naozaj nemá čo zastaviť pred vykonaním svojej občianskej povinnosti. Záleží teraz len od nás ostatných, ako vysoko nasadíme päťpercentnú laťku. Ak príde k voľbám menej ako 55 percent, čiže len okolo 2 339 030 voličov, budeme svedkami ako sa tento borec cez ňu prehupne. S ošuchom ale predsa. Ak príde viac, jeho volebný výsledok bude menej ako   116 808 : 2 339 030 = 0,0499 = 4,99 %   Ak od vytúženej volebnej účasti 2 339 030 odrátame 116 808 skalných dostaneme 2 222 222. Presne toľko statočných ochotníkov treba, aby sme mohli raz a navždy odprevadiť Mečiara na smetisko dejín. Záujemci hláste sa dnes do 22:00 vo svojej volebnej miestnosti. Vzhľadom na počasie, bude treba každý jeden hlas.       Je dokonané... (doplnené v nedeľu 13.6. o 9:09)   V prvom rade sa chcem ospravedlniť za pár numerických chýb v článku. Spomínaných 55 percent predstavuje v skutočnosti až 2 399 303 oprávnených voličov, čo by už samo o sebe bola laťka nastavená dostatočne vysoko. S rezervou. Dokonca dvojitou, keďže počet skalných sa nakoniec scvrkol ešte o pár tisíc na 109 480. Zrejme úderom extrémneho tepla.   Veľká vďaka patrí každému jednému z 2 419 905 dobrovoľníkov (2 529 385 zúčastnených bez 109 480 skalných) za to, že mu posunul laťku o ďalší mikrometer vyššie. Toto je ten moment, kedy ma napĺňa pocit vlastenectva. Som hrdým občanom štátu, kde vedia poslať zločincov a grázlov tam, kam patria. Pomaly ale predsa. Príbeh HZDS čiže Vladimíra Mečiara nech je pre nás mementom o neodvratnom osude strany zlodejov s autoritatívnym vodcom bez vnútornej plurality názorov.   Nuž tak... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Keď slovák Človeku je vlkom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            HC Slovan - skrátená Bratislava
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Ako si politici z novinárov vlastných kot-kódov spravili
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Zlodejova Republika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Kauza hala nezaháľa, a výsledky?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Foto Potočný
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Foto Potočný
            
         
        martinpotocny.blog.sme.sk (rss)
         
                                     
     
        Šport môžem vo všetkých jeho prevedeniach a vo všetkých jeho podobách, aj literatúru a dobrú hudbu. Rád diskutujem pri dobrom pive o komplexnosti histórie, o zakrivení priestoru a o tom, že jazyky a dialekty tvoria spojité zobrazenie definované na povrchu geoidu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Črevo slepô zapálenô
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Mýty bez legendy
                        
                     
                                     
                        
                            I taká je politika
                        
                     
                                     
                        
                            Sever proti Juhu
                        
                     
                                     
                        
                            Zleužitá štatistika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Problém hokeja je naozaj široký... (.týždeň - máj 2008)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jana Kamencayová (žvásty plné geniá..)
                                     
                                                                             
                                            Jozef Kuric (ruka čo nezoslabne)
                                     
                                                                             
                                            Mika Polohová (o nenápadných ľuďoch)
                                     
                                                                             
                                            Helena Sedlárová (myšlienkou fascino..)
                                     
                                                                             
                                            Michal Kravčík (lúska kvapky vody)
                                     
                                                                             
                                            Samuel Marec (o ničom a predsa niečom)
                                     
                                                                             
                                            Petra Jamrichová (tuctový človek)
                                     
                                                                             
                                            Viera Mikulská (prevažne nevážne)
                                     
                                                                             
                                            Andrea Fecková (usporiadaná pestrosť)
                                     
                                                                             
                                            Ján Babarík (aj s Berkym Jariabkom)
                                     
                                                                             
                                            Dominika Handzušová (hokejoumelec)
                                     
                                                                             
                                            Ľubo Randjak (nebojí sa pravdy)
                                     
                                                                             
                                            Lukáš Cabala (vnímavý týpek z Bošáce)
                                     
                                                                             
                                            Ivana Pajanková (hospic - z prvej ruky)
                                     
                                                                             
                                            Drogy (Vlado Schwandtner)
                                     
                                                                             
                                            Melisa (začína žiť odznova)
                                     
                                                                             
                                            Boris Kačáni (boľševikom na večné časy)
                                     
                                                                             
                                            Janette csC. (celkom slušná Cigánka)
                                     
                                                                             
                                            Martin Vlachynský (tuzemský filozofix)
                                     
                                                                             
                                            Jan K. Myšľanov (Zlé správy počkali...)
                                     
                                                                             
                                            Viliam Búr (Programátor, aktivista)
                                     
                                                                             
                                            Chen Lidka Liang (Čínske dievča)
                                     
                                                                             
                                            lenka jíleková (veci, ktoré žije)
                                     
                                                                             
                                            Richard Sulík (Fachman, Občan)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




