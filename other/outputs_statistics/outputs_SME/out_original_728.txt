
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viliam Búr
                                        &gt;
                Počítačové programy
                     
                 Inkscape - vektorová grafika 

        
            
                                    5.7.2008
            o
            9:00
                        |
            Karma článku:
                10.99
            |
            Prečítané 
            11965-krát
                    
         
     
         
             

                 
                    Vektorové obrázky si zachovávajú kvalitu pri úpravách ako zväčšovanie a zmenšovanie; nie sú "kockaté" ani sa nerozmazávajú. Inkscape je slobodný program na kreslenie vektorových obrázkov. Pracuje s formátom SVG (škálovateľná vektorová grafika) a umožňuje export do rastrových formátov, napríklad PNG a JPEG.
                 

                 Ak chceme na počítači spracovávať grafiku, existujú dva základné spôsoby. Rastrová grafika používa "maliarske plátno" zložené z daného počtu malých štvorčekov -- obrazových bodov. Ak je obrázok určený na počítačovú obrazovku, jeden bod obrazu zvyčajne zodpovedá jednému bodu obrazovky. Ak je obrázok určený na tlač, autor si určí požadovanú kvalitu tlače pomocou DPI ("dots per inch", čiže počet obrazových bodov na jeden palec), napríklad 100 DPI na farebnú tlač alebo 300 DPI na čiernobielu. Pri digitálnom fotoaparáte zase počet obrazových bodov zodpovedá technickým možnostiam a nastaveniam fotoaparátu.   Výhodou rastrovej grafiky je jej jednoduchosť. Jednou z nevýhod je nutnosť dopredu určiť počet obrazových bodov. Ak dáte málo, strácate tým z obrazu informáciu pod hranicou rozlíšenia; ak budete chcieť obrázok zväčšiť, bude "kockatý" alebo rozmazaný. Ak dáte veľa, bude obrázok zaberať v počítači veľa pamäte a bude sa pomalšie spracovávať.       Na prvom obrázku je červený kruh s hnedým okrajom v rozlíšení 15×15 bodov. Z pohľadu rastrovej grafiky to však nie je "kruh" ale iba "sústava farebných bodov". Ak tento obrázok zväčšíme na 150×150, dostaneme kockatý útvar na druhom obrázku. Na odstránenie kockatosti majú lepšie grafické editory vyhladzovacie algoritmy, príkladom je tretí obrázok. My však chceme to, čo je na štvrtom obrázku -- a pri vektorovej grafike by sme to aj dostali.   Vektorová grafika si nakreslené prvky pamätá ako geometrické útvary. Nakreslený kruh si teda môže zapamätať ako "kruh so stredom v bode X,Y a polomerom R, vyfarbený farbou F1, s okrajom hrúbky H a farby F2". Väčšinu z týchto čísel nepíšeme, ale zadávame pomocou myši. Takýto geometrický kruh môžeme zväčšiť aj stonásobne a stále bude okrúhly.   Výhodou vektorovej grafiky je, že sa nemusíme príliš starať o výsledné rozmery obrázku. Hodí sa na kreslené obrázky, nehodí sa na fotografie. Svet okolo nás sa zvyčajne neskladá z geometrických útvarov, hoci lepšie grafické editory si ich dokážu na fotografii "nájsť". Z technického hľadiska si musí autor programu premyslieť, aké geometrické útvary bude program podporovať a aké úpravy sa s nimi budú dať robiť. (Napríklad ak program pozná kruh, mal by poznať aj elipsu, pretože roztiahnutím kruhu vznikle elipsa; mal by vedieť urobiť aj kruhový a eliptický odsek a výsek.)      Na prvom obrázku je fotografia (zdroj: Wikipédia). Na druhom a treťom obrázku je táto fotografia automaticky prememená na vektorový obrázok s použitím 3 alebo 8 odtieňov šedej; linky odkazujú na súbory vo formáte SVG. (Použitý program vedel upravovať iba čiernobiele obrázky.)     Program Inkscape je určený na prácu s vektorovou grafikou. Môžete si ho zadarmo stiahnuť a nainštalovať na Windows, Mac, a Linux. Je preložený aj do slovenčiny. Obsahuje niekoľko návodov; tie už preložené väčšinou nie sú. Príklady obrázkov vo formáte SVG nájdete na webovej stránke Open Clip Art Library, ktorá je žiaľ dosť neprehľadná. Sústreďme sa však na to, čo s týmto programom môžeme nakresliť.   Pomocou nástroja "Obdĺžnik" môžeme kresliť obdĺžniky a štvorce, ktoré môžu mať zaoblené rohy. Pri každom geometrickom útvare si môžeme nastaviť farbu výplne, a farbu a hrúbku čiary.      obdĺžnik bez zaoblenia, symetricky zaoblený, asymetricky zaoblený   Pomocou nástroja "Kruh" môžeme kresliť kruhy a elipsy, ale aj kruhové výseky a odseky. Všetky útvary možno vypĺňať nielen jednou farbou, ale aj lineárnym alebo radiálnym farebným prechodom, prípadne vzorkou.        kruh vyplnený jednou farbou, lineárnym a radiálnym farebným prechodom; kruhový výsek a odsek vyplnené vzorkou   Pomocou nástroja "Mnohouholník" môžeme kresliť pravidelné mnohouholníky a hviezdy. Môžeme nastaviť počet uhlov a zaoblenie; pri hviezdach aj špicatosť.        sedemuholník; symetrická sedemcípa hviezda spojená dvoma rôznymi spôsobmi; asymetrická hviezda; zaoblená asymetrická hviezda   Tieto nástroje (a ešte niektoré ďalšie) sa hodia na vytváranie jednoduchých geometrických útvarov, ale na zložitejšiu grafiku to nestačí. Potrebujeme sa naučiť používať všeobecnejší útvar zvaný "cesta". Týmto útvarom dokážeme nahradiť všetky ostatné útvary (niekedy s drobnou nepresnosťou), a pomerne jednoducho sa s ním robia úpravy ako zväčšovanie, zmenšovanie, otáčanie, zošikmenie, ale aj zjednotenie, rozdiel, a prienik; výsledkom týchto operácií je totiž opäť cesta.   Cesta je oblasť ohraničená niekoľkými čiarami; rovnými alebo oblúkovými. Na oblúkové čiary sa používajú krivky tretieho stupňa. Z matematického hľadiska je krivka tretieho stupňa vyjadriteľná vzorcom, v ktorom sa vyskytujú nanajvýš tretie mocniny súradníc "x" a "y". Pre nás je však užitočnejšie vedieť, že takáto krivka je daná dvoma krajnými bodmi, a dvoma pomocnými bodmi. Pomocné body neležia na krivke, ale určujú, ktorým smerom krivka vychádza z krajného bodu a pod akým veľkým oblúkom sa ohýba.    rôzne spôsoby spojenia dvoch bodov: rovná čiara, tupý oblúk, ostrý oblúk, asymetrický oblúk   Dve cesty vychádzajúce z jedného uzla na seba hladko nadväzujú, ak prvé pomocné body na oboch stranách od tohoto uzla ležia na jednej priamke. Ak neležia na jednej priamke, vzniká roh.    rôzne spôsoby napojenia dvoch čiar: roh medzi dvoma rovnými čiarami, hladko nadväzujúce oblúky, oblúk hladko nadväzujúci na rovnú čiaru, roh medzi dvoma oblúkmi   Pri kreslení môžeme postupovať napríklad takto: Najprv si obrázok približne načrtneme pomocou jednoduchých nástrojov (obdĺžnik, kruh, mnohouholník, kreslenie rukou, text,...). Vyberieme útvar, ktorý chceme upraviť, a použijeme položku "Cesta | Objekt na cestu" v menu programu. Tým sa pôvodný útvar premení na cestu, a ďalej ho môžeme upravovať ako cestu -- presúvať vrcholy a pomocné body.       Nakreslíme si sedemcípu hviezdu. Premeníme ju na cestu a vymažeme niektoré vrcholy. Niektoré čiary zmeníme na oblúky. Výsledok vyfarbíme. Neviem síce, čo to je, ale je to pekné.   Ak Vás tento program zaujal, môžete si ho stiahnuť a začať experimentovať. Na internete nájdete ďalšie návody a inšpirácie. Zaujal ma napríklad návod na kreslenie postavičiek v štýle Pac-Man; tu je video ukazujúce pracovný postup:   

 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Mýliť sa menej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Milí katolíci, vychovali ste si Kotlebu, teraz ho riešte!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Bývalý minister Galko zvyšuje čítanosť blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Digitálne učivo - áno, ale poriadne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viliam Búr 
                                        
                                            Špongia 2011
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viliam Búr
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viliam Búr
            
         
        bur.blog.sme.sk (rss)
         
                        VIP
                             
     
        Programátor.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7854
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Medzinárodný jazyk
                        
                     
                                     
                        
                            Manipulácia a kulty
                        
                     
                                     
                        
                            Počítačové programy
                        
                     
                                     
                        
                            Postrehy a úvahy
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Webové diskusie treba moderovať (2)
                                     
                                                                             
                                            Webové diskusie treba moderovať
                                     
                                                                             
                                            Koniec diskusie
                                     
                                                                             
                                            Ako sa stráca dôvera
                                     
                                                                             
                                            Hurá, prázdniny!
                                     
                                                                             
                                            mor(ho); // počítačom [de]generovaná poézia
                                     
                                                                             
                                            Plošinovka za štyri dni
                                     
                                                                             
                                            Vodná mapa pre Wesnoth, vylepšená
                                     
                                                                             
                                            Reálne čísla v jazyku Java
                                     
                                                                             
                                            Celé čísla v jazyku Java
                                     
                                                                             
                                            Informačná diéta
                                     
                                                                             
                                            Od februára učím (tretia časť)
                                     
                                                                             
                                            Vodná mapa pre Wesnoth, uverejnená
                                     
                                                                             
                                            Vodná mapa pre Wesnoth
                                     
                                                                             
                                            	JavaScript pre začiatočníkov (štvrtá časť)
                                     
                                                                             
                                            JavaScript pre začiatočníkov (tretia časť)
                                     
                                                                             
                                            JavaScript pre začiatočníkov (druhá časť)
                                     
                                                                             
                                            Závislosť na počítači
                                     
                                                                             
                                            Od februára učím (druhá časť)
                                     
                                                                             
                                            Od februára učím
                                     
                                                                             
                                            Vydávanie slovenských kníh
                                     
                                                                             
                                            Wesnoth hľadá prekladateľov
                                     
                                                                             
                                            Hrdinovia
                                     
                                                                             
                                            JavaScript pre začiatočníkov
                                     
                                                                             
                                            Denná dávka socializmu
                                     
                                                                             
                                            Dobré a zlé správy o sedemnástom novembri
                                     
                                                                             
                                            Pole v jazyku Pascal (tretia časť)
                                     
                                                                             
                                            Pole v jazyku Pascal (druhá časť)
                                     
                                                                             
                                            Pole v jazyku Pascal
                                     
                                                                             
                                            Otec časti národa
                                     
                                                                             
                                            Cyklus v jazyku Pascal (tretia časť)
                                     
                                                                             
                                            Cyklus v jazyku Pascal (druhá časť)
                                     
                                                                             
                                            Cyklus v jazyku Pascal
                                     
                                                                             
                                            Hrach na stenu hádzať
                                     
                                                                             
                                            Uloženie obrázku z programu
                                     
                                                                             
                                            Dá sa úspech naučiť?
                                     
                                                                             
                                            Rekurzia
                                     
                                                                             
                                            Informatické dogmy
                                     
                                                                             
                                            Krok za krokom, blog za blogom
                                     
                                                                             
                                            Ako sa zbaviť reklamných letákov?
                                     
                                                                             
                                            Robert Kiyosaki – podnikateľ alebo rozprávkar?
                                     
                                                                             
                                            Moje obľúbené blogy
                                     
                                                                             
                                            Môj názor bez telefónneho čísla nemá cenu
                                     
                                                                             
                                            Kontroverzný návrh blogerského zákona
                                     
                                                                             
                                            Siete P2P
                                     
                                                                             
                                            Posledná vlna internetu
                                     
                                                                             
                                            Náhodný obrázok na webovej stránke
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Algorithmic Adventures (J. Hromkovič)
                                     
                                                                             
                                            Pavučinové pančušky (A. Adamová)
                                     
                                                                             
                                            Na samotu sa nezomiera  (A. Adamová)
                                     
                                                                             
                                            Na lásku sa nezomiera (A. Adamová)
                                     
                                                                             
                                            Gang Leader for a Day (S. Venkatesh)
                                     
                                                                             
                                            Z bludného kruhu (B. Baker)
                                     
                                                                             
                                            Gut Feelings (G. Gigerenzer)
                                     
                                                                             
                                            Labyrint světa a ráj srdce (J. A. Komenský)
                                     
                                                                             
                                            Čajka Jonathan Livingston (R. Bach)
                                     
                                                                             
                                            QED, Nezvyčajná teória svetla a látky (R. P. Feynman)
                                     
                                                                             
                                            Darwin's Dangerous Idea (D. Dennett)
                                     
                                                                             
                                            Rich Dad, Poor Dad (R. Kiyosaki)
                                     
                                                                             
                                            Sumerečnyj dozor (S. Lukjanenko)
                                     
                                                                             
                                            Dnevnoj dozor (S. Lukjanenko, V. Vasiliev)
                                     
                                                                             
                                            Nočnoj dozor (S. Lukjanenko)
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Pathetic Hypermarket Band
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Kunder
                                     
                                                                             
                                            Evka Cucurachi
                                     
                                                                             
                                            Slovak Press Watch
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Manuál mladého konšpirátora
                     
                                                         
                       Bajanov zimný štadión v Petržalke: na deti sa zabudlo, profituje iba investor
                     
                                                         
                       Sťažnosť pre Kaliňáka – odporúčanie pracovníčky polície BA III.
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Vymenuje Kiska Harabinovu kandidátku?
                     
                                                         
                       Bude Bratislava smradľavá?
                     
                                                         
                       Fínske školstvo v 20 bodoch
                     
                                                         
                       Agresori
                     
                                                         
                       Milý homosexuálne cítiaci Peter!
                     
                                                         
                       Mýliť sa menej
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




