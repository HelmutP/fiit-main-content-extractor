

 Jej mama, moja babka teda dovolila, aby Jožko máj postavil. Nezabudla im pripomenúť, že aby to bolo vo všetkej počestnosti. Do jesene mala byť aj svadba. Veď čo budú vyčkávať, lepšie aj tak nebude. Teta začala pracovať v obchode ako predavačka, Jožko bol traktorista. No dobrý párik to bol. Na pohľadanie. Všetko bolo narichtované ako sa patrí. Obe rodiny so svadbou súhlasili. Svine aj husi sa chovali. 
 Ráno babka vstala a kukla na ulicu cez okno v prednej izbe. Pred domom nič. A nikto. Lebo bolo zvykom, že mládenec si máj aj postrážil. Aby mu ho nejaký iný odkundes nezvalil. Nechcela vyvolať zvadu, nuž teda čakala. Teta sa zobudila, a ešte s neučesaným vrkočom poďho na ulicu skontrolovať máj.  
 Vrátila sa s plačom. Aj Maryša z dolného konca má, aj Kača, aj Terka majú. Len ja nič. Nezeberem si ho ja mama, veru nie. Nech si frajerku hľadá tam, kde si máj postavil. A tak aj bolo. Zo svadby veru zišlo. 
 Jožko si totiž večer vypil viac, ako bolo vhodné. Jeho kamaráti - a boli to veru dobrí kamaráti, ho však v štichu nenechali. Máj postavili za neho. Jožkova mater mala všetko v gánku nachystané, aj mašle, aj ozdobu, aj perníkové srdiečko. Aj pálenku. Veruže máj postavili. Krásny, vysoký, rovný. Šak ho aj dlho Jožko hľadal. Celé prvé aj druhé pole pochodil, lež sa mu zapáčil. Škoda len, že si pomýlili dom. Teda, číslo domu sedelo. Len boli o dedinu ďalej. A tak tetin krásny máj stál pred domom jednej vdovy. Márne sa tá čudovala, kto jej to, prečo jej to. Možno sa aj nádejala, že niekto by ju chcel. Jožko sa však nepriznal.  
 Ako áno, ako nie, svadba nebola. Teda v tom roku. Bola až o rok neskôr. V roku 1954 si totiž Jožko dal dobrý pozor a máj postavil tam, kde bol naozaj treba. A nechýbalo ani perníkové srdce, lebo jeho Anička mala o deň neskôr narodeniny. 
 To bolo vtedy, a takto je to dnes. 
   
   
 najprv ho posledného apríla silní chlapi postavia, podoprú a dobre upevnia 
   
   
   
   
   
  potom si ho môžeme vyfotiť -lebo je len jeden pre všetky dievky a ženy 
   
  kým si pripravia škôlkári svoj program 
   
 a potom sa už tancuje priamo na ulici 
  
  možno až do rána bieleho ... 
   
 Zmenilo sa veľa vecí, ale moja teta Anička stále oslavuje narodeniny. Túto nedeľu má krásnych 75 rokov. A keby ujo Jožko žil, možno by jej taký nejaký malý májik kdesi schopil. A keby náhodou aj nie, celkom určite by jej zahral na harmonike pesničku Anička dušička. Ako každý jeden rok. 
   

