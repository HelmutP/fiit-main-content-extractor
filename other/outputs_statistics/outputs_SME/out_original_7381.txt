
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Javurek
                                        &gt;
                Záhrada
                     
                 Tak beží čas 

        
            
                                    27.5.2010
            o
            9:40
                        (upravené
                27.5.2010
                o
                9:59)
                        |
            Karma článku:
                7.46
            |
            Prečítané 
            1974-krát
                    
         
     
         
             

                 
                    Čas je pojem vyjadrujúci plynutie, dej bez zastavenia. Len na fotografiách si ho vieme na chvíľu zastaviť. Tak som si zastavil čas na niekoľko momentov v prírode. ONA nie je bez človeka, je s nami v plnom význame, my patríme do nej aj s našimi výtvormi...
                 

                 Pri "konzervovaní" reality nie som si vždy istý, čo mi dá viac, či fotografický moment, alebo film. Tak sa stalo, že som prešiel od prvotnej fotografie, na dlhšie obdobie k filmu, k videu. Nová technika a jej možnosti ma vždy lákali.   Momentálne som naklonený viac k fotografickým momentom života. K tomu mi paradoxne pomohlo video, svojou možnosťou zastaviť jednotlivé zábery. Tým som uvidel fotografiu vo filme s nečakanými výrazmi. To ma inšpirovalo vrátiť sa k momentom času.   Priznávam aj to, že veľkú úlohu tu zohrala neuveriťeľná príťažlivosť digitálneho snímania, uchovávania a úpravy záberov. To, o čo sme sa niekedy pracne snažili v tmavej komore, sa dnes dá zvládnuť "ľavou zadnou"... Niekedy to až zvádza k extrémom, ale na druhej strane to dáva aj možnosť tvorby.   V tomto výbere mám niekoľko momentov, ktoré ma zaujali kdekoľvek, a tiež dokumentuje trvalú snahu fotografickou sériou napodobniť beh života.      Takto beží čas v poslednom čase a prináša májové dažde i potopy.      Nám priniesol čas aj geotermálny vrt s vodou z hĺbky 1800 m. Prispejeme ku globálnemu oteplovaniu, ale ak sa to vezme globálne, žiadna energia sa nestratí, ani nevytvorí, len sa premení a z "gea" zoberieme trochu tepla a pustíme ho to atmosféry...      Rozmnožili sa nám slimáci.      Je to jasné, skôr musela byť sliepka a až potom vajce. Tu je dôkaz zrodu nových sliepočiek.      Niektoré sa už ohrievajú v maminom perí.      Nevyhnutným obyvateľom ranča je mačka (kocúr).      S neustále sa rozširujúcim potomstvom...      Mama má chvíľu pre poobhrýzanie kostí, tak sa nedá vyrušovať.      Koník si myslí, že je u zubára. Mal som šťastie, že som včas uhol, inak by mi ukradol foťák...      Stabilný postoj je dôležitý.      V celku svojej konskej krásy.      Na rade je posunková reč...      ...a konská dynamika.      Aj v malom priestore sa vie rozbehnúť.      Isteže, má meno a začína sa na "A", podľa tradície. Volá sa Argon (ak si dobre pamätám)...      Zatiaľ čo ja blúdim po svete za prírodou, príroda sa nasťahovala ku mne domov. V ľudskej reči: čierna stavba. Hrozné. Tá lastovičia dvojica si to postavila bez stavebného povolenia a bez súhlasu susedov... Čo mám robiť, keď na mestskom stavebnom úrade moju sťažnosť neakceptovali? Mám sa sťažovať vyššie? A nedajBože až najvyššie???       Mak a červení Kméri...      Na záver niečo nezničiteľné a vraj zdravo pŕhľavé...       Dodatok:    Dopĺňam "strážcu ranča", na želanie Packy z diskusie...      Strážca ranča má práve dovolenku a v rámci vedľajšieho pomeru stráži "činžiak"  :)      Sledujem ťa....         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Vieme prví, len či pravdu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            November
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Moja prvá fantastika
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Papierové mestá
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Spisovatelia v Smoleniciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Javurek
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Javurek
            
         
        jozefjavurek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Neoznačené fotografie na blogu výlučne z vlastnej tvorby (c). Inak je všetko vo hviezdach...
 
"štandardný už tradičný bloger, s istou mierou poctivosti" (jeden čitateľ)
 
 


Click for "Your New Digest 1".
Powered by RSS Feed Informer

počet návštev:







 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    650
                
                
                    Celková karma
                    
                                                4.59
                    
                
                
                    Priemerná čítanosť
                    2950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Český raj
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Myšlienky, názory
                        
                     
                                     
                        
                            Blogovanie
                        
                     
                                     
                        
                            Technika a technológia
                        
                     
                                     
                        
                            Domácnosť
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            EÚ
                        
                     
                                     
                        
                            Hory, lesy
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




