

 " Dnes vidíme skutočne hrozivú skutočnosť, že najväčšie prenasledovanie Cirkvi nepochádza od vonkajších nepriateľov, ale zrodilo sa z hriechu vo vnútri Cirkvi ". povedal pápež novinárom na palube lietadla. 
 V posledných týždňoch mnoho vatikánskych predstaviteľov obviňovalo médiá z kampane proti Cirkvi, a jeden z vysokých predstaviteľov Vatikánu neváhal označiť správy o utajovaní sexuálneho zneužívania predstaviteľmi Cirkvi za "bezvýznamné klebety". 
 Aj na Slovensku mnohí horliví zástancovia nevinnosti Cirkvi pomenovávali celú kauzu ako "mediálnu paniku", ktorej cieľom je zdiskreditovať Cirkev. Autori inak znejúcich článkov či diskusných príspevkov boli označovní za rúhačov a tých, ktorí si riešia obviňovaním Cirkvi výlučne svoje osobné animozity a komplexy. Obhajcovia pápeža a Cirkvi v zahraničí, medzi nimi aj bývalý predseda talianskeho Senátu a občasný spolupracovník Benedikta XVI. taktiež nekompromisne odmietali údajnú snahu " poškvrniť biele rúcho cirkvi". Zdá sa však, že pápež pochopil, že poškvrnenie prichádza zvnútra, že vonkajšie vplyvy nemôžu viac ublížiť tomu, komu už bolo tragicky ublížené zvnútra. 
 Dúfajme, že slová pápeža preniknú do sŕdc veriacich, ktorí doteraz nechceli vidieť hroznú realitu - k tomu napokon vyzýva pápež aj predstavených cirkevného života. "Odpustenie obetí nesmie byť náhradou za spravodlivosť", doplnil pápež. Pápež cestuje do Fatimy, kde sa v roku 1917 zjavila trom deťom Panna Mária. Dôvodom jeho 4-dňovej apoštolskej cesty je 10.  výročie beatifikácie  fatimských pastierikov Hyacinty a Františka. 
 Novinárom dokonca povedal, že obsah takzvaného "tretieho fatimského tajomstva", ktoré zverejnili v roku 2000, naznačuje že by mohlo ísť nie tak o predpoveď atentátu na Jána Pavla II, ale o predpoveď  súčasného utrpenia pápežstva a Cirkvi v súvislosti so sexuálnym zneužívaním v jej radoch. Okolo tohto fatimského proroctva existuje veľa nejasností, viacerí pápeži ho odmietli zverejniť, hoci k tomu malo prísť v roku 1960. Neoverné zdroje naznačujú že v posolstve môže ísť práve o vnútroný rozklad Cirkvi  a zlyhania jej predstaviteľov, čo napokon, ako sa zdá, potvrdzuje dnes aj sám Benedikt XVI. 
 Zdá sa, že je to naozaj ešte len začiatok. 
   
   
   
   

