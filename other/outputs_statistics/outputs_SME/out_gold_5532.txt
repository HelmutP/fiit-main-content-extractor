

 Žobráci - to je v Indii kapitola sama o sebe. Kam sa človek pohne, všade stretá ženy v otrhaných šatách so spiacou ratolesťou v jednej ruke a s miskou na peniaze v druhej, alebo desaťročné deti prosiace o dolár, vraj na čapátí - chlebové placky. Vždy si pritom spomeniem na film Milionár z chatrče alebo na Dickensove romány - akému mafiánovi večer odovzdávajú svoj denný úlovok? 
 V Nainitale sme hneď prvý deň stretli chlapca, ktorý k nám natiahol ruku so slovami: 
 "Dajte mi päť rupií. Na čokoládu." 
 Rozosmiali sme sa. Toľká úprimnosť! Toto mesto je skutočne iné, keď sa tu namiesto o chlieb žobrá o čokoládu. 
 Večer sme príhodu spomenuli nášmu americkému kamarátovi Paulovi. Zasmial sa a spýtal sa: 
 "Dali ste mu na tú čokoládu?" 
 "Nedali. Detským žobrákom nič nedávame, nechceme ich v žobraní podporovať." 
 Paul potom spomenul príhodu zo svojho rodného Chicaga: 
 "Raz som videl u nás na ulici sedieť bezdomovca. Podľa výzoru ťažký alkoholik. Držal taký papier, ako žobráci občas mávajú, popisujú na ňom svoju srdcervúcu situáciu a prosia o peniaze. Tento mal na papieri len lakonický nápis: I need a drink - Potrebujem sa napiť." 
 "To je teda dobré! A čo, dal si mu niečo?" 
 "Už sa presne nepamätám, ale myslím, že áno." 
 Uvažujem, či sa skutočne nepamätá, alebo len nechce úplne otvorene priznať, že ho starý alkoholik svojou úprimnosťou dostal a on mu prispel na ďalšiu fľašku. 
 Malého čokoládového žobráka sme viackrát nestretli. Na druhý deň sme sa nechali odviezť rikšou okolo jazera a potom sme si kúpili vyhliadkovú plavbu malou loďkou. Ani sme po týchto radovánkach príliš netúžili, ale boli sme tam mimo turistickú sezónu, tak sme to urobili pre dobrý pocit - umožnili sme miestnym trochu si zarobiť. Vesliar na loďke sa nám svojou chabou angličtinou pochválil, že má štyri deti. Nechápem, z čoho ich mimo sezónu dokáže uživiť - lodiek tam na turistov čakalo aspoň štyridsať a za celý deň sme na jazere videli plávať len šesť z nich. Keď sme vystupovali na breh, dali sme mu štedré sprepitné. 
   

