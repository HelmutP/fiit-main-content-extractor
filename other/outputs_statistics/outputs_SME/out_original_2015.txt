
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Kamien
                                        &gt;
                Nezaradené
                     
                 Občas neverím, že žijeme v 21.storočí, alebo„Som pohan, upáľte ma!" 

        
            
                                    5.1.2010
            o
            16:56
                        (upravené
                5.1.2010
                o
                17:35)
                        |
            Karma článku:
                7.58
            |
            Prečítané 
            977-krát
                    
         
     
         
             

                 
                    Len pred pár chvíľami som sa dočítal, že v Írsku začal platiť zákon, podľa ktorého je rúhanie trestným činom a môžem zaň byť stíhaný (samozrejme v Írsku). Hneď na začiatok uvediem, že som podľa všeobecného ponímania ateista. Zdôrazňujem, podľa všeobecného ponímania.  Pretože nemyslím, že je potrebné vyznávať nejakú oficiálne uznanú vieru a nebodaj patriť k nejakej cirkvi o ktorých si myslím svoje. Môžem veriť v karmu, prírodu alebo vesmír...
                 

                     A prečo o to píšem? Lebo ma to prekvapilo, zaskočilo a na chvíľu aj mierne rozčúlilo a hlavne som sa potreboval vypísať a týmto spôsobom si uľaviť. Prekvapil ma tento pokus o návrat do stredoveku. Je zarážajúce, že v dvadsiatom prvom storočí sa môže v civilizovanej krajine takýto zákon dostať do parlamentu a byť schválený. Veď čo je rúhanie?  Niekde som našiel, že spočíva v tom, že sa proti Bohu vyslovujú - vnútorne alebo navonok - slová nenávisti, výčitky a provokácie, že sa o Bohu hovorí zle, že chýba k nemu úcta v reči a že sa zneužíva jeho meno. Rúham sa keď poviem, že podľa mňa Boh nie je, alebo prehlásim, že ak je, tak je krutý a zlý?  Možno áno a myslím, že mám právo na svoj názor. Ale nie v Írsku. Zákon podľa ktorého môžem byť postihovaný za svoje názory je od základu zlý a je veľkým krokom späť.  Je otázne či a ako bude uplatňovaný, ale už samotná jeho existencia  je veľkým varovaním. Veď práve náboženská zaslepenosť bola vždy najväčšou brzdou pokroku. Za všetko jeden výrok:   „Všetky veľké pravdy začínajú ako rúhanie." (George Bernard Shaw)   Kto bude  posudzovať čo rúhanie je a čo nie? Sudca? Kňaz? Íri majú svoje nové kladivo na čarodejnice. Ešte raz sa pýtam, čo so slobodou slova?   Čo myslíte, ktorý z týchto výrokov by bol v Írsku trestný?       „Viera a poznanie sú ako dve misky váh, čím vyššie je jedna, tým nižšie je druhá." Arthur Shopenhauer   „Náboženstvo znamená veriť, že všetko čo vieme nie je pravda." Mark Twain   „Ak nás história vedy niečo naučila, tak je to poznatok, že označenie našej nevedomosti nálepkou Boh nikam nevedie." Coyne   „Už len fakt, že ateizmus je neziskový na rozdiel od náboženstiev, o niečom svedčí." George Carlin   „Keby Kristus existoval tu a teraz, viem o jednej veci ktorou by určite nechcel byť... kresťanom." Mark Twain   „Je možné, že našou úlohou nie je uctievať Boha, ale stvoriť ho." Arthur C. Clarke   „Nezdá sa vám, že ste na imaginárneho kamaráta trochu pristarý?" Bill Maher   „Pre teba som ateista, pre Boha lojálna opozícia." Woody Allen   „Náboženstvo je dôvod prečo chudobní neútočia na bohatých. A je to skvelá vec na udržanie chudoby ticho." Napoleon Bonaparte   „Ľudstvo nebude slobodné, kým posledný kráľ nebude zaškrtený črevami posledného kňaza." Denis Diderot   „Mám rád vášho Krista, ale nemám rád kresťanov, vôbec mu nie sú podobní..." Ghandhi   „Ani krajina, ani ľudia ktorí sú otrokmi dogiem, nemôžu mentálne napredovať." Jawaharlal Nehru   „Našu neschopnosť sme nazvali Boh." Robert Ingersoll   „Keď si predstavím všetku tú bolesť, čo biblia spôsobila, nechcel by som nikdy napísať niečo podobné." Oscar Wilde   „Čo môže byť prehlasované bez dôkazu. Môže byť odmietané bez dôkazu." Christopher Hitchens   „Ten čo vymyslel Boha bol blázon, ten čo šíri jeho slovo darebák a ten čo ho uctieva barbar." Peryar   „Každého Boha zabil čas. Zabije aj vášho." Edgar Allan Poe   „Každý vnímavý a dôstojný človek musí považovať Kresťanskú sektu za horor." Voltaire   „Boh je vrah, keď chodí do vojny."  Marka Twain       Týmto článkom som sa nechcel dotknúť náboženského presvedčenia žiadneho človeka. Vyjadruje len moje názory a môj pohľad na prenikanie cirkvi aj do životov nás „ateistov". Zatiaľ v Írsku, ale tiež to možno začalo krížami na školách...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (75)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Kamien 
                                        
                                            Len malé zamyslenie...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Kamien 
                                        
                                            Vráťme futbal tam, kam patrí... do Anglicka!!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Kamien
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Kamien
            
         
        kamien.blog.sme.sk (rss)
         
                                     
     
        Prečo sem píšem? Možno potreba sa podeliť o niektoré názory a postrehy a možno iba potreba sa vypísať z toho, čo ma trápi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    754
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




