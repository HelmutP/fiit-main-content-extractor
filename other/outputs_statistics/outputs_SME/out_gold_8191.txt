

   
    Ako jeden z argumentov uvádzajú napríklad Veľpieseň, ktorá je plná rôznych zmyselností, alebo trebárs priamy pokyn Stvoriteľa, aby sa ľudský rod plodil a množil, či názor, že žena dostala krásu na to, aby ju muž obdivoval. 
   
    Veľpieseň bola napísaná pred niekoľkými tisícročiami, kedy sa ľudstvo nachádzalo na radikálne odlišnom stupni zrelosti, než je tomu dnes. Vývoj však v mnohých oblastiach života pokročil neuveriteľným spôsobom dopredu a malo by sa to celkom logicky týkať i oblasti morálnej a mravnej. Z toho pohľadu je potom doslova scestné v súčasnosti argumentovať tým, čo bolo napísané a aktuálne pred niekoľkými tisícročiami.  
   
    Proti výroku milujte sa a množte nemožno nič namietať. Iba že to milovanie musí byť čisté, ušľachtilé a ľudsky dôstojné! Nie plné živočíšnej zmyselnosti a niekedy až zvrhlosti!  
   
    Taktiež nemožno nič namietať ani voči ženskej kráse, iba musí byť prezentovaná čistým a ušľachtilým spôsobom, hodným bytosti zvanej človek. Ženská krása sa totiž v nijakom prípade nesmie stať nástrojom dráždenia zmyslov a tých najnižších pudov tak, ako je tomu dnes. Nesmie sa v mužoch vedome a cielene snažiť vyvolávať sexuálnu chtivosť.  
   
    Kristus na túto tému riekol: „Bolo vám povedané – Nescudzoložíš! Ja vám však hovorím, že každý, kto žiadostivo vzhliadne na ženu, už s ňou scudzoložil vo svojom srdci!“  
   
    To teda znamená, že už keď vo svojom vnútri prechovávame zvrhlé a nečisté myšlienky, už nimi s previňujeme voči príkazu Nescudzoložíš, za čo budeme braní Zákonmi Božími adekvátnym spôsobom na zodpovednosť! 
   
   Len sa pozrime sa z tohto pohľadu na dnešnú ženskú módu, na filmy, na internet, na časopisy! Sú to obrovské zdroje nečistoty, s ktorou sa so zištných dôvodov vedome pracuje. 
   
     Avšak odplata za toto všetko musí byť zničujúca! Úmerne zničujúca k špine, v ktorej žijeme, ktorú každodenne vytvárame a ktorou je zaplavený náš skrytý, vnútorný život. Len sa pozrime na súčasné záplavy! Živly tejto zeme sa už búria proti strojcovi a zdroju všetkej tej špiny – proti pozemskému ľudstvu! Ak to ľudia v tomto zmysle nepochopia a nezmenia sa, budú mocným živelným dianím zo zeme zmetení, ako nepoučiteľní a nenapraviteľní škodcovia.  
   
    Mnohí, či už materialisti, alebo aj kresťania si povedia, že všetko toto sú iba slová nejakého pomýleného fanatika, pričom oni sa so svojim umierneným postojom počítajú k väčšine, ktorá má predsa vždy pravdu. Veď také obrovské množstvo žien a mužov, ktorí schvaľujú smerovanie súčasnej módy, nevidiac v nej nič zlého, takýto obrovský počet ľudí sa predsa nemôže mýliť.  
   
    Ale opak je pravdou! Pri svojom povrchnom posudzovaní vecí totiž nikdy nebrali do úvahy známe podobenstvo o dvoch cestách. Z nich iba tá úzka, ktorú nachádzajú nemnohí, vedie do života, kým tá široká a pohodlná, po ktorej kráčajú zástupy, vedie do záhuby. Kto dokáže ešte aspoň trochu samostatne uvažovať, nech sa nad tým dobre zamyslí a nech sa prestane spoliehať na povrchný argument, že väčšina má predsa vždy pravdu.   
  
 
  L.L., sympatizant Slovenského občianskeho združenia pre posilňovanie mravov a ľudskosti  
 http://www.pre-ludskost.sk/ 
   

   
 
   

