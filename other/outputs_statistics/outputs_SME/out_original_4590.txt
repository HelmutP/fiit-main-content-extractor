
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Miroššay
                                        &gt;
                Súkromné
                     
                 Poklad 

        
            
                                    15.4.2010
            o
            9:50
                        |
            Karma článku:
                4.75
            |
            Prečítané 
            890-krát
                    
         
     
         
             

                 
                    Sedel na terase  domu a dolieval si z fľašky bieleho suchého vína, ktoré popíjal celý večer. Rozmýšľal nad životom a v tom sa zjavila ona. Bola krásna. Taká ako ju, poznal. Taká, akú ju obdivoval,  keď zaspávala a miloval, keď sa prebúdzala.  Pohľad do  jej očí mu spôsoboval  neuveriteľnú radosť, ale teraz i bolesť .
                 

                 
  
           „Vitaj slniečko!“ povedal, bez toho, aby sa na ňu pozrel a pri tom sa díval na slnko, ktoré zapadalo nad letným mestom.   „Ahoj!“ odpovedala so súcitnou tvárou a pokračovala: „Ako sa máš?“   „Nie tak dobre, ako ty“, odpovedal s ironickým úsmevom.   „Čo tu robíš?“   „Nič, sedím a rozmýšľam.“   „Nad čím?“   „Ani neviem. Nad životom, nad nami. Nad všetkým a zároveň nad ničím.“   „Prišiel si na niečo?“, sadla si na stoličku, ktorá bola vedľa neho a začala niečo hľadať vo svojej kabelke.   „Prečo si prišla ?“, odpovedal na otázku otázkou a porušil nepísané pravidlá príjemnej konverzácie.   „Len som ťa chcela vidieť. Vieš, že pre mňa stále veľa znamenáš.“   „To ma teší...“ , znova zaúradovala jeho irónia. Povedal to, lebo ani nevedel, ako má na to reagovať.   Zrazu jej vyšli z úst slová, ktoré nečakal: „Prepáč, ak som ti ublížila.“   „Pozri !“, odsrkol si z pohára, zamyslel sa a pokračoval: „Nehnevám sa na teba. Nemôžem, lebo srdcu nedokážeme rozkázať. Tak ako ja neviem rozkázať svojmu, ty zasa tvojmu. Bolo by odo mňa hlúpe a detinské, ak by som sa mal tváriť, že si mi ublížila. Veľmi to bolí, ale taká je neopätovaná láska . V živote je to proste tak, raz vyhráš, raz prehráš,  aj keď v láske neexistuje víťaz a porazený.“   „Nie?“, pýtala sa ho nechápavo a pri tom dobre vedela ako to myslí.   „No buď sú víťazi obaja alebo obaja porazení!“   „Vieš čo?“ , vstala zo svojej stoličky a prišla k nemu a vrúcne ho objala.  „Si úžasný človek!“   „Vôbec si to nemyslím.“ , oponoval jej a pokračoval: „Keby som bol taký úžasný, tak by si nikdy...“   „Pšššššttttt!“, nenechala ho dohovoriť a  pritlačila mu svoj prst na pery a obdarovala ho vrúcnym pohľadom.   „Musím už ísť...“, pokračovala a pozerala pri tom na svoj mobil.   „Stále niekam odchádzaš, už by si konečne mohla aj ostať.“, reagoval  smutne.   Nepovedala nič, len sa otočila a pomaly kráčala preč.   „Aspoň som vďaka tebe pochopil Alchymistu.“ , hovoril pri tom ako odchádzala a znova si odpil z vína.   „Si ho nepochopil, keď si ho čítal?“, otočila svoju hlavu a prstami si opravila svoju ofinu.   „Neviem, ten záver mi došiel až teraz. Takže nezabúdaj, že máš moju truhlicu s pokladom !“, žmurkol a usmial sa.   „Akým pokladom?“, naliehavo čakala na odpoveď, lebo sa veľmi ponáhľala.   „Veď vieš, čo bola jedna z posledných viet v Alchymistovi.“ , urobil krátku pauzu a pokračoval: „Tam, kde bude tvoje srdce, tam bude i tvoj poklad...“     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Miroššay 
                                        
                                            Chcete vedieť, ako skončí Šeherezáda ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Miroššay 
                                        
                                            Dievča na lavičke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Miroššay 
                                        
                                            Exkluzívne: Ako (možno) bude vyzerať denník SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Miroššay 
                                        
                                            Ako sa staré veci môžu zísť...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Miroššay 
                                        
                                            Nové logo pre VšZP
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Miroššay
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Miroššay
            
         
        mirossay.blog.sme.sk (rss)
         
                                     
     
        Študent VŠ zaujmajúci sa takmer o všetko.Väčšinu života s dobrou náladou..Na internete známy ako malypali a priležitostný webdesignér  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2287
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Internet
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Svetové udalosti
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Zamyslenia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            IL Blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Movies
                                     
                                                                             
                                            TA3
                                     
                                                                             
                                            .týždeň
                                     
                                                                             
                                            Medialne.sk
                                     
                                                                             
                                            CSS Beauty
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Odkaz Harabinovi: Nikto nie je nedotknuteľný
                     
                                                         
                       Modré z neba alebo „splnený“ sen na televízny spôsob
                     
                                                         
                       Cesta do otroctva
                     
                                                         
                       Včerajší deň
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




