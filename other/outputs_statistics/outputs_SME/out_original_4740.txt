
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Culák
                                        &gt;
                Postrehy a zaujímavosti
                     
                 Denníčky pre politikov? Prečo nie! 

        
            
                                    20.4.2010
            o
            20:23
                        |
            Karma článku:
                4.09
            |
            Prečítané 
            632-krát
                    
         
     
         
             

                 
                    Konečne je to tu.  Zo všetkých strán sa na nás milo usmievajú tváre uhladených politikov, ktorí sa predháňajú v tom, kto vybuduje v nasledujúcich štyroch rokoch zo Slovenska sociálnejší štát. Médiá prinášajú každú chvíľu tie najzaručenejšie prieskumy verejnej mienky, aj keď ich zakaždým niekto označí za zmanipulované a zámerne sa snažiace o jeho poškodenie. A z politiky sa po piatej runde stáva vďačná téma na debatu v miestnych pohostinstvách. Áno, voľby na Slovensku sú pred dverami...
                 

                 To, že je u nás pod Tatrami obvykle nízky záujem o veci verejné, je všeobecne známy fakt. Stačí sa pozrieť na percento účasti z rôznych volieb a referend, ktorých bolo i počas krátkej histórie samostatného Slovenska neúrekom. Zdá sa, že občan znechutený občasným a takmer iste nechceným pochybením úradníka vo vláde, vďaka ktorému vyletí hore pomysleným komínom trčiacim zo štátnej pokladnice ďalších zopár miliónov eur, stráca akúkoľvek nádej na lepšie a svetlejšie zajtrajšky.   Ja sa však ešte stále hlásim ku skupine dúfajúcich optimistov, ktorí veria, že celý náš parlament nie je skrz-naskrz prehnitý, akoby sa na prvý pohľad mohlo zdať a že sa v ňom i dnes sem-tam blysne nejaká dobrá  poctivá  duša. O tom ale, že tieto duše sú v menšine však nepochybujem už ani ja v tých najväčších návaloch optimizmu.   Ako však oddeliť zrno od pliev a zistiť, ktorý politik sa počas štyroch rokov v 150 – člennom zbore vyvolených venoval zmysluplným či dokonca užitočným záležitostiam, neskôr pretaveným do paragrafov? Uznávam, že v prípade niektorých jedincov je tá najužitočnejšia vec, ktorú môžu pre dobro Slovenska urobiť, ticho sedieť v poslaneckej lavici a hlavne ničoho sa nechytať. Všetkých však nemožno hádzať do jedného vreca spolu so známymi popolčekármi, emisionármi, garážnikmi, vinohradníkmi, špinavými práčmi a diaľničnými tunelármi. Čas od času predsa musí zvíťaziť zdravý rozum, ktorý nedovolí jedným šmahom čarovného prútika premeniť Slovensko na stredoeurópske Somálsko. Inak by nám už neostalo nič iné, len oprášiť valašky namiesto kalachov, kapce namiesto rýchlych malých člnov a prepadávať nákladné vlaky, pokiaľ by cez nás úplne neprestali jazdiť.   Vráťme sa však späť k zrnu a plevám. Mne by sa vcelku páčil systém, v ktorom by každý jeden poslanec musel povinne zadávať – povedzme raz za týždeň alebo mesiac – heslovite všetky veci, na ktorých cez toto obdobie vo svojom zamestnaní pracoval a s akým úspechom. Aspoň by som zhruba vedel, kto má v čom prsty.   Práca poslanca chvalabohu nesúvisí len s odieraním si zadkov v poslaneckých laviciach a s bavením sa na podarených výrokoch svojich vtipom nadaných kolegov. Do vlaku si občas kúpim jedny nemenované slovenské noviny, v ktorých som prednedávnom nad veľkým tučným nadpisom „Voľby v Hospodárskych novinách“ objavil nenápadný odstavček „Odpočet poslanca: Čo som urobil pre vás“. Počas každej svojej vlakovej výpravy tam tak nájdem iného poslanca hrdo sa hlásiaceho k svojej štvorročnej drine a čítaním jeho zásluh v najrôznejších komisiách, pracovných skupinách či výboroch si skracujem cestu plnú stavebných prác na trati. Táto rubrika sa mi natoľko zapáčila, že viedla k mojej úvahe o poslaneckých denníčkoch.   Tak ako ja, určite i množstvo iných ľudí nevie prísť väčšine poslancom NRSR ani len na meno (myslené doslovne), nieto ešte vedieť na čom pracujú. Jednoduché elektronické „poslanecké denníčky“ dostupné na jednej internetovej stránke by možno zväčšili povedomie bežného občana o práci jeho volených zástupcov a zvýšili celkovo záujem o politiku. Predvolebný program strán je totiž jedna vec a skutočná práca poslancov po voľbách vec druhá...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Procházka - Lipšic  (1:0)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Nechcem budovať silný sociálny štát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            [Foto] Večerné slnko na večernej tráve
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Hľadá sa dno. Pravicové.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            [Foto] Auschwitz I, Auschwitz - Birkenau
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Culák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Culák
            
         
        culak.blog.sme.sk (rss)
         
                                     
     
        Životné motto: Nič na na svete nie je čiernobiele.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4868
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Foto sekcia
                        
                     
                                     
                        
                            Postrehy a zaujímavosti
                        
                     
                                     
                        
                            Vojenské letectvo SR
                        
                     
                                     
                        
                            Vojenské letectvo ČS od 1945
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Bohatství národů (Adam Smith)
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Aliancia Fair-play
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Milan Barlog
                                     
                                                                             
                                            Peter Paulík
                                     
                                                                             
                                            Eduard Kladiva
                                     
                                                                             
                                            Stanislav Thomka
                                     
                                                                             
                                            Milan Hraško
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Veronika Bahnová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




