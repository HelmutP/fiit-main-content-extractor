
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Strapko
                                        &gt;
                Domáca politika
                     
                 Svätopluk - vzor pre Fica, Slotu a Mečiara 

        
            
                                    7.6.2010
            o
            10:05
                        (upravené
                1.3.2011
                o
                8:40)
                        |
            Karma článku:
                10.86
            |
            Prečítané 
            1431-krát
                    
         
     
         
             

                 
                    Včerajšia veľkolepá hradná show, ktorú live vysielala aj "nezávislá" verejnoprávna televízia bola klincom do rakvy, plnej trápnosti a nechutností, ktorými sme svedkami za posledné 4 roky existencie SR. Skovávanie sa národ, falošné velebenie najrôznejších historických postáv a udalostí, ktorými sa snažia súčasní vládcovia vyvolať v ľuďoch zdeformovaný pocit vlastenectva a národnej hrdosti je pre rozumne uvažujúcich ľudí na zaplakanie. Ale v prípade sochy Svätopluka išlo aj o niečo viac, pre súčasnú trojku je to jasný vzor...
                 

                 ...vzor stability. Práve stabilita je najpoužívanejším argumentom Fica v predvolebnej kampani. Už si ani nepamätám, koľkokrát to povedal na mítingu, ktorého som sa zúčastnil. To, čo tu vládlo pred rokom 2006 a to, čo by tu mohlo vládnuť po nadchádzajúcich voľbách považuje za zlepenec pravice a maďarských strán. Jasne ej to napísané aj na útočných billboardoch strany Smer.       Podľa legendy Svätopluk, keď zomieral, dal priniesť prúty a zavolal svojich synov. Povedal im, že keď budú držať spolu, nič ich neporazí. Bude sa im dariť aj v zlých časoch a podobne.   Prečo v tom vidím paralelu s konaním týchto troch predstaviteľov vládnych strán? Nestalo sa náhodou X krát počas 4 rokov, že aj keď jeden z nich spáchal niečo zlé, navzájom sa podržali? Jeden deň sa na seba hnevali, ale druhý deň boli zase za jedno.   Za jedno boli aj pri rozkrádaní štátu. Ušlo sa všetkým, aj keď to vyzerá, že Vladkovi trošku menej. Ale ten je najstarší a má už nejaký ten domček v Tepliciach, takže sa uskromnil.   Za jedno boli aj v otázkach slovensko-maďarských vzťahov. Burcovali a stále burcujú národ, aby sa bránil pred nepriateľom z juhu. Úporne sa bránia všetkým snahám južného suseda zabrať nám naše územie...   Takto by sa dalo písať úplne o všetkých činoch tejto trojky, pretože oni boli za jedno vo všetkom. Aj keď niekto z menších bratov (SNS, HZDS) vnútorne nesúhlasil, navonok sa podriadil Smeru...všetko v duchu stability.       A ľuďom to stačí, stačí im, že je stabilita.   - stabilita ekonomického úpadku - stabilita úpadku morálneho - stabilita zhoršovania susedských vzťahov - stabilita v raste nezamestnanosti - stabilita v raste deficitu  - stabilita v intenzite populistických výlevov - stabilita v realizácii podivných, netransparentných, predražených tendrov - stabilita v raste korupcie   atď, atď...   Ľudia, ktorí týchto "troch synov Svätopluka" volia majú asi radšej vládu stabilného lúpežného tria ako možno vládu menej kompatibilnú, ale vládu strán, z ktorej každá prináša aj nejaké vlastne rozumné nápady.   Preto výstavba sochy Svätopluka je úplne zaslúžená. Celé 4 roky sa niesli v duchu jeho predsmrtného odkazu. A to bolo treba voličom pripomenúť, preto to správne načasovanie - týždeň pred voľbami.   Na záver už len konštatovanie, že Svätopluk by na Fica, Slotu a Mečiara bol isto hrdý. Teda mám na mysli, že ako sa riadia jeho legendárnym posolstvom.  S troškou humoru sa stavím , že keby znova žil, dal by pre zmenu postaviť sochu práve on im... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Fico odchádza - podobne ako Mečiar aj Putin
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Komunálne voľby odhalili množstvo chýb - Môžu vás stáť budúci post!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Z vlastnej vôle, či bez - hlavne, že ideš preč!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Should I stay or should I go?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Strapko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Strapko
            
         
        strapko.blog.sme.sk (rss)
         
                                     
     
         Mám rád ľudí, aj keď často nedokážem pochopiť ich konanie. A napriek tomu, že som často moralista a skeptik, nikdy neprestávam hľadať pozitívne aspekty negatívnych javov. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    267
                
                
                    Celková karma
                    
                                                8.08
                    
                
                
                    Priemerná čítanosť
                    1523
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Domáca politika
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Z môjho života
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Škola
                        
                     
                                     
                        
                            Osobnosti, zaujímavosti
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Úradnícka vláda ako plán Ficovho návratu
                     
                                                         
                       Stránka katedry žurnalistiky radila, ako obísť Piano
                     
                                                         
                       Fico de facto pripustil Paškovu zodpovednosť, ale...
                     
                                                         
                       Anarchista odstúpil
                     
                                                         
                       Slovenský gympel, americká "high school"
                     
                                                         
                       Yankees go home! (desať minút s národniarom)
                     
                                                         
                       Medzi väzňami v Nairobi, alebo keď si myslíte, že vás už v živote máločo prekvapí.
                     
                                                         
                       Buď chlap
                     
                                                         
                       'Satanistický' festival? Meriame dvojakým metrom. A je to ešte horšie.
                     
                                                         
                       Slušne platený bočák
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




