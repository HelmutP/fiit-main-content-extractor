

 Volá sa to celé „Stačí otvoriť dlaň“. Myslím, že nás to hodne vystihuje. Príbehy mladých. Zrady a klamstvá. Svet ilúzií, ktorý ich zahaľuje do rúška nevedomosti... Žijú a predsa často krát len prežívajú... Aj o tom je toto divadlo, ale nielen. Je hlavne o mladých, ktorí dokázali venovať svoj čas nácvikom textu, tancov, spevu... a predovšetkým vzájomnej súhre pre niečo veľké. Niečo o čo sa oplatí snívať. Pre čo sa oplatí aj žiť... 
 Pre nás to bola radosť, láska, dávanie sa... nájdenie a dávanie krásy a plnosti života aspoň v čriepkoch, ale predsa tým, ktorí prišli a aj tým, ktorí nám fandia. 
 A preto vďaka. Im, ale aj každému jednému z vás, kto neláme palicu a nezhasína knôt... 
 Pekné dni. 

