

   
 Súznenie tiel, či prechádzky v daždi? 
 Pohľadov pár, či ruka čo dráždi? 
 Berie ti dych dotyk letmý, nežný? 
 Či líškanie zvodnej vyzývavej slečny? 
   
   
 Spojené srdcia - dve nádoby krehké,  
 Vášnivé objatia, či bozky vlhké? 
 Spoločné chvíle, tie chvíle sladké. 
 Prečo máme tie chvíle tak krátke? 
   
   
 Veštenie z dlaní, či dary z lásky? 
 Zvrhnutie faloše - zloženie masky? 
 Pre druhého zrieknuť sa - obetovanie? 
 Zmysel života snáď MILOVANIE ? 
   

