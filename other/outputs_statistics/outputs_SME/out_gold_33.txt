

 Kapušianský hrad (494 m) bol budovaný v rokoch 1410 až 1420 pôvodne na zvyškoch gotického hradu zničeného v roku 1312, o ktorom sa zachovalo len málo údajov. Postavili ho ešte starí Rimania na vrchu Zámčisko a to zo strategických dôvodov ako útočisko a obrana proti Sarmatom, ktorí vtedy vpadli na územie terajšieho Slovenska. V roku 1300 bol majetkom maglodského rodu. Vznik hradu je v ranom stredoveku spätý s pohraničnou obrannou funkciou tohto územia. Západne od neho sa vypínajú kopce Veľká a Malá stráž a aj samotný názov obce pod hradom, založenej pôvodne strážcami hraníc, poukazuje na strategickú polohu tohto miesta. Odtiaľto sa strážila rovnako hranica ako i obchodná kráľovská cesta, ktorá viedla z Prešova na sever smerom do Poľska.   
 Prvá zmienka o hrade pochádza z roku 1249 ako hrade Tobul s jeho prvým majiteľom pravdepodobne z rodu Maglód, ktorý vymrel v roku 1300, keď sa hrad stal aj kráľovským majetkom. V roku 1312 dal kráľ Karol Róbert hrad zbúrať a kastelán hradu sa dostal do zajatia. Po jeho vyslobodení jeho verným sluhom Jozefom Kohákom, prezývaným Kvaka, sa sám dostal do zajatia, za čo ho stihol trest utnutia oboch rúk. Kastelán sa svojmu vernému sluhovi zavďačil podarovaním celej dediny, ktorá dostala meno Kvakov. V roku 1347 daroval kráľ Ľudovít I. majetok Magloch Capy svojmu vychovávateľovi Petrovi Poharosovi za podmienky, že hrad už neobnoví. K prestavbe hradu došlo až po smrti Petra Poharosa v roku 1409, keď hrad pripadol kráľovi Žigmundovi. Hrad zväčšil a dobudoval o dve strážne veže. V roku 1440 kráľ Vladislav daroval hrad šľachticom z Rozhanoviec. K ďalším architektonickým zmenám hradu došlo v druhej polovici 16. storočia, kedy v rámci všeobecných príprav protitureckej obrany bola pevnosť vyzbrojená novou vojenskou technikou. Ďalšie dejinné udalosti, ako bocskayovsko-rákócziovské povstanie sa hradu nedotklo, ale v roku 1685 sa hradu zmocnil Imrich Thököly a v tom istom roku generál Schultz, ktorý hrad prinavrátil späť rodine Kapyovcov. V roku 1709 sa o hrad začal zaujímať Telekessy, veliteľ vojska Františka Rákócziho II. Hoci hrad hrdinsky bránil šarišský podžupan Valentín Usz, nedokázal ho ubrániť.  Kapušiansky hrad dal nakoniec Telekessy  podpáliť a úplne zničiť v roku 1709.  Keď boli ukončené vnútropolitické boje, majiteľka hradu Eva Gergelakyová, vdova po Gabrielovi Kapym, sa pokúsila v roku 1712 hrad ešte raz obnoviť. Avšak o tri roky neskôr bol hrad z rozhodnutia uhorského snemu odsúdený na zánik a jeho  majiteľka nútená hrad podpáliť, aby sa nemohol stať strediskom protihabsburgovského hnutia. Podobný osud postihl aj okolité hrady.  Kapušiansky hrad sa nachádza na Kapušianskom hradnom vrchu, nad obcou Kapušany. V roku 1980 bol vyhlásený za Prírodnú rezerváciu. Je zaujímavou archeologickou lokalitou s objavom neolitického sídliska a bukovohorskou kultúrou, halštatského pohrebiska, rímsko-barbarského sídliska, kostrového pohrebiska z doby sťahovania národov, slovanského sídliska z doby Veľkomoravskej ríše a sídliska z 10. - 11. storočia. 
 Návšteva Kapušianskeho hradu je dobrým námetom poznávania kultúrnohistorických pamiatok hradov a zámkov v okolí Prešova. 
   
  
 Mohutné obvodové hradby. 
   
  
 Hradom prechádza zelené turistické značenie.  
   
  
 Nádvorie hornej časti hradu. 
   
  
 Pozostatky gotickej veže. 
   
  
   
  
 Mliečnik chvojkový 
   
  
 Jedna zo strieľní hradu. 
   
  
   
  
 Horná časť hradu. 
   
  
 Najzachovalejšie fragmenty stavby. 
   
    
   
  
 Scenéria mohutných múrov stavby v hre svetla a farieb. 
   
  
   
  
   Pakost hnedočervený 
   
  
 Pohľad na Fintice. 
   
  
 Kapušany  
 Súvisiaci článok: www.hrady.szm.sk/kapusany 

