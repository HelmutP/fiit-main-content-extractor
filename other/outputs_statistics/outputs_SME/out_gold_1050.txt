

 
Včerajšia dvojhodinová diskusia na tému „Kam kráčaš, slovenská žurnalistika“ mala zábavné obsadenie: 
 
Drahoslav Machala (exnovinár promečiarovskej Republiky/Slovenskej republiky, dnes poradca R.Fica) 
Dušan Kerný (za Mečiarovej vlády šéf TASR, šéf spravodajstva STV, neskôr šéfredaktor Nového dňa) 
Pavol Dinka (šéf úradu štátny tajomník Ministra kultúry Hudeca za poslednej Mečiarovej vlády, od januára člen Rady  dozerajúcej nad elektronickými médiami)  
Eva Jaššová (psychologička, Slovenská akadémia vied, kandidátka do parlamentu za SNS, predtým Pravá SNS, súčasná členka Rady dozerajúcej na Slovenský rozhlas) 
Eduard Chmelár (vysokoškolský učiteľ)  
Zuzana Krútka (predsedníčka Syndikátu novinárov) 
Ján Budaj (exposlanec parlamentu, mal na starosti mediálnu legislatívu) 
 
Celá skupina sa v zásade zhodla, že naše médiá sú na tom na rozdiel od minulosti vo veľmi zlom stave, že za to môžu najmä súkromné médiá, lebo tamojší novinári sú v područí vlastníkov. Samozrejme nemal im kto oponovať, lebo novinári/vydavatelia z hlavných súčasných mienkotvorných médií chýbali, hoci práve oni by niečo o súčasnej žurnalistike vedeli. 
 
Najmä prví traja menovaní páni volali opakovane po objektivite a zastavení manipulácie. Páni, ktorí o objektivite niečo fakt musia vedieť, veď to ukázali vo svojej práci v minulosti. Aby som nezabudol, celú diskusiu „moderovala“ ďalšia hviezdna našej žurnalistiky, Milan Blaha (exSlovenská Republika, TV Luna, Nautik TV atď.) 
 
Samozrejme nik sa ani slovom nezmienil o tom, ako sa v súčasnom spravodajstve STV dezinformuje. Nie, problémom sú pravicové súkromné médiá, najmä tlač. Tá tlač, ktorá má hlavnú zásluhu na obrane miliárd korún daňových poplatníkov, na dôslednej kontrole správania sa a hospodárenia vládnej moci na Slovensku. Nič sme sa nedozvedeli ani o čiernych rokoch (pre krajinu aj žurnalistiku) v polovici 90-tych rokov. A opäť sa obhajovali „slávne“ 60.roky, hoci dnešné médiá podávajú o spoločnosti neporovnateľne viac užitočných informácií ako to obdobie trochu miernejšej propagandy. 
 
Na moje prekvapenie sa súčasných novinárov nezastala ani šéfka ich odborovej organizácie. Občas som súhlasil akurát tak s Chmelárom, a to pri pomenovaní konkrétnych nástrah v práci novinárov (práca s analytikmi, tlaky inzerentov, slabá ochota k sebakritike), hoci pre mňa je kontext úplne opačný: novinárčina u nás nikdy nebola lepšie robená ako dnes a hovoriť o mínusoch no mlčať o evidentných prínosoch súčasnej žurnalistiky k lepšiemu životu je hrubé zavádzanie. 
Nepočul som nikoho spomenúť otvorenosť médií k novým technológiám, k blogerom, k živým prenosom, k rozšírenej videoponuke, ku komunikácii s čitateľmi - to všetko kvalitu informácii zlepšuje.  
 
Nemá význam obšírne diskutovať, stačí si pár minút tej akože diskusie pozrieť. Na záver ešte kontrola faktov u jedného rečníka. Docent Eduard Chmelár spomenul prieskum o novinárskej korupcii: 
 
 
	Dovolím si tu uviesť jeden výskum, aby som nemoralizoval, a čísla hovoria za všetko. Ten výskum bol urobený pred pár rokmi, počas druhej Dzurindovej vlády, robil ho profesor Dean Kruckeberg, profesor z katedry Komunikačných štúdií Univerzity v Severnej Iowe.  
 
 
Áno, tá štúdia  vyšla v roku 2003. Pamätám sa na ňu, písal som o nej  krátko na SPW  (21.10.) s komentárom, že  „bral by som to s rezervou, ide o dosť hrubo stavaný index.“ 
 
 
	Ten výskum celého tímu sa týkal vyše 60ich krajín z celého sveta, reprezentatívny výskum na základe vyše 100 kritérií, čiže žiadna povrchnosť a týkal sa korupcie médií. 
 
 
 
	  
 
 
 
Začína Chmelárovo typické preháňanie. Prieskum v 66 krajinách sveta bol postavený na 8 kritériach. Prieskum meral pravdepodobnosť korupcie, nie jej skutočnú existenciu. A šlo čisto o mienkotvornú tlač (nie iné médiá). 
 
 
	Výsledky tohto výskumu boli uverejnené takmer všade, len nie na Slovensku. Respektíve aby som bol úplne presný, Pravda uverejnila jednu malú noticku, že najväčšia korupcia je v Číne.  
 
 
Už keď som o výskume prvýkrát písal na blogu, upozornil som, že o tom (s chybou) písala aj Národná Obroda. Podľa archívu mala prieskum na prvej strane s dokončením článku na strane 3. Dokopy 361 slov. Aby som bol úplne presný. 

 
 
	
	Povedala A, ale nepovedala už B. Tu je tabuľka, z ktorej vyplývajú veľmi zaujímavé zistenia. Slovensko patrí medzi krajiny s najvyšším stupňom korupcie v Európe. Nie v Európskej únii, hovorím v Európe.  
 
 
Opakujem, prieskum neuvádza stupeň korupcie, len pravdepodobnosť jej výskytu na základe istých kritérií, ako je gramotnosť obyvateľov, sloboda médií či existencia etických kódexov novinárov. Pokiaľ ide o Slovensko, celkovo skončilo na 31.-34. mieste zo 66 krajín. Európa mala v rebríčku 31 krajín (ak rátame aj Rusko), Slovensko v nej skončilo na 21.-23. mieste. Za Slovenskom skončilo aj také Česko, Slovinsko a Poľsko.  

 
 
	
	Horšie ako Bosna a Hercegovina, horšie ako Albánsko. 
 
 
Albánsko je hlúposť, to v prieskume ani nebolo. Bosna skutočne skončila tesne pred nami, dokonca aj pred Maďarskom. Čo tiež ukazuje na pochybnú kvalitu rebríčka. Podľa mňa upozorňovať akurát na Bosnu je navyše od veci, lebo v 2 z 8ich kritérií ani nebola hodnotená, lebo neboli prístupné dáta. Napríklad o korupcii v spoločnosti...  
 
 
	Ak si zoberieme čiastkové kritériá, napríklad čo sa týka plurality médií, Slovensko je na tom horšie ako Rusko! To kritizované Rusko, ktoré naše novinári berú každý deň do úst, keď...kritizujú stav žurnalistiky v tejto krajine.  
 
 
Pluralita ako kritérium v prieskume nebola. Bolo tam niečo príbuzné, a to miera konkurencie (competition), kde sme naozaj skončili za Ruskom. Lenže meradlom bol pomer obyvateľstva a počtu titulov mienkotvornej tlače. Nič o obsahu. Ak 10 novín píše to isté, je to pre tento index vyššia miera konkurencie ako keď existujú len 4 noviny s rozličnými názormi. Na základe toho niečo vyvodzovať o horšej pluralite médií u nás ako v Rusku je pochabé. Inak tento výskum dal Rusku za mieru konkurencie najvyššiu možnú známku, podobne ako ďalším 11im z celkových 66 krajín. Horší sú tu aj Fíni, Nemci či Američania... Nepochybne vhodne merané kritérium "plurality". 
 
 
	...ten citovaný prieskum hovorí, že Slovensko nemá ani jedno legislatívne opatrenie, na rozdiel povedzme od okolitých štátov, ktoré sa týka korupcie médií. 
 
 
Kritérium o efektívne aplikovanej protikorupčnej legislatíve v prieskume naozaj je. Lenže netýka sa médií, ale spoločnosti ako celku. A je merané jednoducho – výškou indexu vnímania korupcie CPI od Transparency International. A práve za ten index sme dostali nulu, nie za to, že máme nula protikorupčných opatrení v oblasti médií. A pokiaľ ide o okolité krajiny, nulu za toto kritérium dostali aj Česi, Poliaci a Ukrajinci. Maďari bod, Rakúšania tri. 
 
 
	Jednoducho tie čísla sú tak alarmujúce, že by sme si mali prestať zatvárať oči pred tým a myslieť si, že ak korupcia existuje v celej krajine, tak že žurnalistiky sa netýka. 
 
 
Veru tak,  ibaže by boli alarmujúcejšie klamstvá o prieskume, ktorý sa používa na popísanie súčasného stavu našej žurnalistiky.
 
 
Tak skúste si napĺňať právo byť informovaný pozeraním diskusnej relácie STV.  
 

