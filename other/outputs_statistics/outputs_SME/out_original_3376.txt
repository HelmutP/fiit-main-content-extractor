
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Sliacky
                                        &gt;
                Nezaradené
                     
                 Ako smrdí vesmírny prd? 

        
            
                                    26.3.2010
            o
            14:16
                        (upravené
                26.3.2010
                o
                17:25)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1808-krát
                    
         
     
         
             

                 
                    Vtáka poznáš po perí - človeka po reči. Už-už sa mi zdalo, že SaS prinesie do našej politickej kultúry trochu čerstvého vzduchu. Niečo ako jarný vánok. A tu zrazu: Prd! Dokonca vesmírny!
                 

                        Richard Sulík, šéf strany, ktorá si do názvu hrdo dala slovká sloboda a solidarita, sa o mladom bloggerovi, ktorý sa mu pokúsil oponovať, vyjadril, že jeho slová sú ako vesmírny prd. Nuž, neviem, čo ho vyhecovalo k takémuto prirovnaniu. Ekonomike, o ktorú išlo, nerozumiem. A neviem ani to, ako taký vesmírny prd vyzerá a ako smrdí.           Prd je mi napokon celkom sympatické slovo a v podstate neškodné, aj ja sa občas k nemu uchýlim. Teda aj k slovu... A ten, čo ma pozná, vie, že dokážem použiť aj ostrejšie, nepublikovateľné výrazy. Ale ja nie som politik, žiaden človek na výslní, žiadna celebrita, takže si to azda môžem dovoliť, hoci - máte pravdu - aj na mňa platí prvá veta z perexu tohto článku. Aj preto nechcem poučovať, len nesmelo vyjadrujem svoju mienku, že politik, ak chce byť v úcte tých, ktorých chce viesť, by mal mať naopak úctu ku všetkým, vrátane tých, ktorí nemajú rovnaký názor. Lebo takých, ktorí radi siahajú po vulgárnejších vyjadreniach, by sme už radšej na slovenskom politickom nebi nevideli, keďže zábavy, ktorú nám svojím ochotníckym divadelníctvom za drahé vstupné ponúkli doterajší „lídri“, už bolo vyše hlavy.       Už som spomenul, že som od Sulíkovej strany očakával nové vetry, ale nie v pejoratívnom zmysle. Mali by mať podobu jarného vánku a nie vôňu smetiska. Prečítal som si program strany a zdá sa mi celkom fajn, ale jedným dychom sa znova a znova musím opýtať: Kde je záruka, že to zase nebudú len kopy popísaného papiera alebo zaplnené internetové stránky a - ako hovoria naši bratia Česi - „skutek-utek“? Že to zasa nebude len to staré známe bla-bla-bla... Že sa takto popísaný papier stane použiteľným iba ak na zahladenie stôp po prde.       Spomenul som, že SaS a jej program ma zaujali v tom pozitívnom slova zmysle a keby som bol pre nich vhodným občanom (lebo už teraz - tak to cítim - nás škatuľkujú či kastujú - a zajtra kastrujú?), azda by som tejto strane aj pomohol v jej aktivitách. Pochybnosti vo mne začali klíčiť, keď som sa dočítal, ako sa pán Sulík snažil poučovať novinárku, aký titulok má použiť. Ešte väčšie pochybnosti mnou prenikli vtedy, keď som sa zadíval do zoznamu kandidátov SaS. Ale to je moja rýdzo osobná pochybnosť. A vesmírny prd a smradík okolo neho ma pomaly, ale iste, vedie k poznaniu, že SaS nie je žiaden zázrak na slovenskom politickom nebi, že sa nám derie na svetlo sveta len ďalší pokračovateľ mečiarovsko-dzurindovsko-ficovských metód či tradícií.       Tak to vidím ja! Ak sa v mojom pohľade mýlim a skutočnosť bude naozaj ružová, budem šťastný, ospravedlním sa a zložím klobúk pred tými, ktorí mne (nám) vrátia nádej v životaschopnú budúcnosť. Hoci pri mojom veku... Čo už? Zatiaľ mi chýba viera v zmenu a pesimisticky píšem: Aj v novembri ´89 sme si „sľúbili lásku“... A čo sa udialo? Pri všetkých úspechoch (lebo tie nemožno poprieť, tie naozaj boli) je z tej proklamovanej lásky naozaj iba obyčajný prd.       Ak som ešte nedávno chcel pridať svoj hlas ideám SaS, postupne sa presviedčam, že je to vlastne úplne jedno, ktorému čertovi upíšem svoju dušu. V našom štáte to beztak pekelne smrdí a zrejme bude smrdieť i naďalej. O tom, že je naša politická scéna smiešna, nás presvedčil Pali odkiaľsi zo Zemplína či zo Šariša. Ten sa aspoň dokázal zasmiať na vlastnej biede, hoci pri vyše 16-tisícovom vstupnom na politické ihrisko mám aj o tej biede vážne pochybnosti. Ale je to skvelý reklamný trik, ktorý nesmrdí prdom, ale vonia borovičkou či iným destilátom. Azda kvôli tomu nepríde o fúzy.       Viem, že som týmito vetami zabŕdol do osieho hniezda, lebo blog.sme sa stal riečiskom, ktorým sa k nám valia myšlienky o tom, ako to bude v budúcnosti na tom našom milom Slovensku vyzerať. Krásne svetlé zajtrajšky!Budú voňať jarným kvetom alebo smrdieť prdom, hoci nie práve vesmírnym?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (104)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Veľryba je hladná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Tak im treba?! ??? !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Alarm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Hanba!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Viete, čo má spoločné mandarínka s „pukaným“ vajcom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Sliacky
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Sliacky
            
         
        sliacky.blog.sme.sk (rss)
         
                                     
     
        59-ročný muž s energiou a dôverčivosťou mladíka.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    249
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1437
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




