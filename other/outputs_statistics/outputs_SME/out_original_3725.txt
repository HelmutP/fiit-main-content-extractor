
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Renáta Holá
                                        &gt;
                Myslím si
                     
                 Znalí i neznalí 

        
            
                                    31.3.2010
            o
            18:58
                        |
            Karma článku:
                5.31
            |
            Prečítané 
            1300-krát
                    
         
     
         
             

                 
                    Včera večer odznel v televíznych novinách návrh, aby ročné zúčtovanie zdravotných preddavkov vykonávali zdravotné poisťovne a nie samotní poistenci. „Výborný nápad," pomyslela som si poznajúc tie komplikované tlačivá na ročné zúčtovanie zdravotných preddavkov. My, poistenci, by sme zdravotnej poisťovni oznámili len základ dane za predchádzajúci rok a ročné zúčtovanie by zdravotná poisťovňa vykonala sama. Avšak, niektorí poistenci by na túto skutočnosť mohli i doplatiť . Ako?
                 

                 Keď som totiž nahlasovala živnosť na zdravotnej poisťovni, pracovník, ktorý moje oznámenie prijímal, mi hneď oznámil, že keďže som SZČO a poberám invalidný dôchodok, mám povinnosť odvádzať poistné mesačne 7% z minimálnej mzdy. Hneď mi aj vystavil poštovú poukážku na zaplatenie prvého poistného.   Mne sa to nepozdávalo, pretože som mala vedomosť, že prvý rok vykonávania živnosti nie som povinná odvádzať preddavky na zdravotné poistenie, a preto som si naštudovala  príslušné predpisy, kde som sa utvrdila, že nielen nie som povinná počas prvého roku odvádzať poistné, ale že sa ma dokonca ani netýka výška minimálneho poistného 7% z minimálnej mzdy pokiaľ som SZČO a ako poberateľ invalidného dôchodku zároveň i poistenec štátu.   Prišlo však na lámanie chleba v júni minulého roku, kedy som bola prvýkrát povinná podať ročné zúčtovanie zdravotných preddavkov za r. 2008. Keďže v r. 2008 som bola po celý rok invalidná a poberala som invalidný dôchodok, prvé tri mesiace zamestnaná a posledné mesiace roku zase SZČO, bolo to trošku zložitejšie. Pri preberaní tlačiva v zdravotnej poisťovni mi pracovníčka ponúkla pomoc: „Ak by ste prišli poobede so všetkými dokladmi , bude tu kontrolórka z krajskej pobočky, a tá by vám to ročné zúčtovanie spravila." Žiaľ, nešlo to, poobede som nemala čas navštíviť znova zdravotnú poisťovňu, a tak som v ten rok ročné zúčtovanie vykonala sama za pomoci internetového portálu. Vyšiel mi nedoplatok za r. 2008 a preddavky na poistné na r. 2009 mi vyšli nulové. Pri odovzdávaní ročného zúčtovania mi pracovníčkou zdravotnej poisťovne bolo oznámené, že moje ročné zúčtovanie skontrolujú a zašlú mi informáciu o č. účtu na zaplatenie predmetného nedoplatku.   Prešiel rok a dozrel čas na ďalšie ročné zúčtovanie. Keďže počas roku som žiadnu informáciu od zdravotnej poisťovne nedostala, dúfala som, že moje ročné zúčtovanie za r. 2008 skontrolovali a zrejme žiadne nedoplatky voči zdravotnej poisťovni nemám.   Dozvedela som sa, že známemu vlani bez problémov na pobočke zdravotnej poisťovne ročné zúčtovanie spravili, a teda v polovici marca tohto roku som sa zastavila v pobočke zdravotnej poisťovne s tým, aby som ich pokorne poprosila, či by mi nepomohli s ročným zúčtovaním za r. 2009.   „Nie je to našou povinnosťou," zdôraznila pani za počítačom, no napriek tomu si vyžiadala  a overila moje údaje. Keď však zistila, že som SZČO a zároveň poberám invalidný dôchodok, upozornila ma, že som nesprávne odvádzala preddavky na zdravotné poistenie, resp. som ich neodvádzala vôbec.  Presviedčala ma, že som mala odvádzať preddavky minimálne vo výške 7% z minimálnej mzdy, t. j. mesačne v r. 2009  v sume 20,68 eur, presne tak ako mi kedysi tvrdil ten pán, ktorý v pobočke už nepracuje.   Darmo som namietala, že ja mám vedomosť, že takýto poistenec výšku minimálneho preddavku nemá stanovenú. Vytiahla na mňa interný predpis - akúsi tabuľku pre výpočet výšky preddavku na poistné, kde mi zvýrazňovačom vyznačila kolónku, podľa ktorej SZČO, poberateľ inv. dôchodku, má min. sadzbu 7% z min. mzdy. Tak, a mala som to čierne na bielom zvýraznené žltou farbou.   „Viete, ja by som tú zárobkovú činnosť za týchto podmienok ani nevykonávala, pretože príjem z nej mám  nižší než vychádzajú tie zdravotné odvody," vravela som, no nerada by som mala voči vám nejaké nedoplatky."   „Viete, čo? Mohli by ste prísť budúcu stredu? Bude tu pani z krajskej pobočky a s tou by sme to dali do poriadku."   „Môžem," povedala som tentoraz a trpko oľutovala, že som si rok predtým na pani kontrolórku z krajskej pobočky čas nenašla.   Pravdaže doma mi nedalo a znova som si naštudovala zákon o zdravotnom poistení i príslušné tabuľky pre výpočet poistného a potvrdilo sa mi, že minimálnu výšku preddavku na zdravotné poistné nemám stanovenú, nakoľko som SZČO a zároveň poistenec štátu. Ak preddavok vychádza menej ako 3 eurá, SZČO, poistenec štátu,  preddavok neplatí vôbec.   Patrične vyzbrojená vedomosťami zo zákona o zdravotnom poistení  som sa však - vzhľadom na moju psychickú labilitu i tak s malou dušičkou a otázkou ako dopadnem - vybrala v stanovený deň na pobočku zdravotnej poisťovne.   Pani, s ktorou som hovorila pred týždňom ma hneď nasmerovala ku kontrolórke z krajskej pobočky. Požiadala som ju o pomoc pri ročnom zúčtovaní.  „ Ste SZČO a zároveň poberáte invalidný dôchodok? Aký bol váš základ dane za r. 2009?" zneli jej otázky. Nadiktovala som sumu, pani ju nahodila do počítača a o minútku už vytlačila moje ročné zúčtovanie zdravotného poistenia za r. 2009.   „Vyšli vám nulové preddavky. Nech sa páči, tu to podpíšte!"   Podpísala som, poďakovala a významne pozrela na pani z minulého týždňa, ktorá si ma aj tak nevšímala. Ešte som sa spýtala, či nemám nedoplatky z minulého roku. Pani chvíľu blúdila v počítači a potom mi oznámila: „To teraz necháme tak. Ešte to nemáte pracovníkmi skontrolované a uzavreté. Nateraz je hlavné, že ste odovzdali tohoročné ročné zúčtovanie."   Po dva roky mojich skúseností s ročným zúčtovaním zdravotných odvodov a s ich stanovením sa mi potvrdilo, že na pobočkách zdravotnej poisťovni robia ľudia znalí i neznalí. Vychádza mi však, že keby mi ročné zúčtovanie mal robiť „pán spred dvoch rokov", či „pani z minulého týždňa", zrejme by som -dôverujúc im ako kompetentným - zbytočne odvádzala mesačne zdravotnej  poisťovni  7% z minimálnej mzdy, čo činilo v r. 2009 mesačne 20,68 eura.   Nápad, aby za poistencov vykonávali ročné zúčtovanie zdravotných preddavkov samotné poisťovne, je  teda výborný, ale pracovníci pobočiek zdravotných poisťovní by museli byť na to dostatočne pripravení.  Ja som sa zatiaľ stretla na pobočke zdravotnej poisťovne s jediným takýmto pracovníkom a tým bola kontrolórka z krajskej pobočky, ktorej som dodnes za jej ochotu a prístup vďačná.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Sme rozdielni, tak aj naša liečba je individuálna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Aj na Slovensku cítiť potrebu reformy psychiatrie
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Pasívne čakanie nie je moja cesta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Funguje to po kvapkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Renáta Holá 
                                        
                                            Ide to aj bez antipsychotík?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Renáta Holá
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Renáta Holá
            
         
        hola.blog.sme.sk (rss)
         
                        VIP
                             
     
         blogujúca psychotička, dôverne poznajúca stavy depresie či mánie, snažiaca sa byť normálnou manželkou a matkou 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    174
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2359
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodičovstvo
                        
                     
                                     
                        
                            Psychika
                        
                     
                                     
                        
                            Z dovolenky
                        
                     
                                     
                        
                            Kde bolo,tam bolo...
                        
                     
                                     
                        
                            Drobnosti
                        
                     
                                     
                        
                            Práca
                        
                     
                                     
                        
                            Myslím si
                        
                     
                                     
                        
                            Pocitovky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




