

 TÉZY ZÁKONA O ŠTÁTNOM PODNIKU LESY SR 
 Dôvodová správa  
 Lesy na území Slovenska sú dôležitým zdrojom prosperity krajiny. Štát má povinnosť vytvárať podmienky pre uvážlivé hospodárenie vo všetkých lesoch, bez ohľadu na ich vlastníctvo a bonitu. Osobitnú zodpovednosť má štát za správu lesov vo svojom vlastníctve. Toto vlastníctvo je mimoriadne významné, a to nielen z pohľadu všeobecne prospešných úžitkov, ktorých hodnota nie je finančne doposiaľ spoločensky plne akceptovaná, ale aj z pohľadu komerčne atraktívnych komodít (drevo, poľovné revíry, chaty...). Práve táto lukratívna stránka vlastníctva lesov, mimoriadne dôležitá pre vyvážené plnenie všetkých funkcií lesov (sociálnych, ekologických a ekonomických) je pre podnik štátnych lesov potenciálnou hrozbou. Tá spočíva v tom, že ľudia zodpovední za riadenie podniku môžu byť vystavovaní tlaku vplyvných skupín, snažiacich sa uprednostniť vlastné záujmy pred celospoločenskými. Skúsenosti z podniku LESY SR, š.p., ukázali, že takéto nebezpečenstvo je reálne, a preto je potrebné ľudí poverených riadením podniku pred nim v budúcnosti chrániť.   
 1. Postavenie štátneho podniku LESY SR 
 1.1. Vzorové hospodárenie  
 Lesy sú dominantnou krajinotvornou súčasťou Slovenka. Ich všestranný význam bude  trvalo narastať. Štát má záujem na riadnom obhospodarovaní všetkých lesov Slovenska. Lesy, ktoré sú vo vlastníctve štátu, musia byť obhospodarované vzorovým spôsobom, a to aj v prípadoch, kde z hľadiska rentových pomerov ide o lesy stratové. (V tom je rozdiel oproti majiteľom neštátnych lesov, pre ktorých síce platia rovnaké zákony, no ktorí pre vzorové, spoločensky želateľné hospodárenie, často nenachádzajú dostatočné dôvody).    
 1.2. Výlučné spravovanie lesov vo vlastníctve štátu 
 Lesný majetok vo vlastníctve štátu je spravovaný jednotne jedinou na to určenou organizáciou LESY Slovenskej republiky, š.p. (ďalej len „LSR"), avšak s výnimkou lesov vojenských a štátnych lesov TANAPu. LSR sú zriadené štátom, pričom žiadna súčasť tohto podniku a ňou spravovaného štátneho majetku sa nesmie ani dočasne stať vlastníctvom inej štátnej alebo neštátnej organizácie. LSR je organizáciou, ktorej povinnosťou je komplexne hospodáriť na zverených lesných pozemkoch, a to po stránke lesníckej, ekologickej i etickej. 
 1.3. Vlastníctvo lesných pozemkov a lesných porastov    
 LSR nie sú vlastníkom lesných pozemkov a lesných porastov vo vlastníctve štátu, tieto môže vlastniť výlučne Slovenská republika. 
 1.4. Výkon hospodárskej činnosti  
 LSR vykonávajú hospodársku činnosť pomocou vlastných zamestnancov na základe pracovného práva a prostredníctvom externých subjektov na základe obchodného práva. LSR nesmú nahradiť hospodárenie na zverenom lesnom majetku postúpením práv tretím osobám. Prostredníctvom tretích osôb je možné zabezpečiť len samotný výkon činností vykonávaných v záujme realizácie lesnej výroby, a to na zmluvné obdobie nie dlhšie ako jeden rok.  
 1.5.  Odpredaj lesného majetku vo vlastníctve štátu v správe LSR 
 O odpredaji akéhokoľvek lesného majetku štátu (t.j. lesných pozemkov a lesných porastov) môže rozhodnúť jedine vláda SR, pričom bezvýhradnou podmienkou je, že súčasne s odpredajom sa musí vykonať hodnotovo rovnocenný nákup lesného majetku, a to minimálne v rovnakej, prípadne väčšej trhovej hodnote. 
 1.6. Odpredaj nehnuteľného majetku LSR   
 LSR vypracujú a trvalo vedú zoznam nehnuteľného nelesného majetku, ktorý: 
 a/  Je na podnikovom zozname historického lesníckeho dedičstva a  nemožno ho v žiadnom prípade odpredať. 
 b/  Je potrebný pre výkon riadneho lesného hospodárenia a možno ho odpredať len výnimočne, na základe súhlasu Dozornej rady LSR, a to za podmienky, že podnik súčasne získa trhovo rovnocenný, alebo hodnotnejší nehnuteľný majetok. 
 c/  Možno predať ako prebytočný. 
 1.7. Prenájom lesného majetku štátu a nehnuteľného majetku LSR 
 Lesné pozemky a lesné porasty nie je možné prenajať na účely výkonu správy lesa. Možno ich prenajať na iné účely (napr. poľovníctvo), a to za podmienky, že prenájom je trvalo pre štát finančne výhodnejší ako užívanie vo vlastnej réžii. 
 Nehnuteľný majetok je možné prenajať iba v prípade ak patrí medzi prebytočný majetok (kat „c" bodu 1.6.).     
 1.8. Narábanie so získanými prostriedkami z predaja a prenájmu 
 Takéto prostriedky možno použiť výhradne v prospech hospodárenia podniku, t.j. nemôžu sa stať súčasťou mimoriadnych odvodov smerujúcich mimo LSR.   
 2. Hospodárenie LSR 
 2.1. Základný princíp 
              Hospodárenie LSR musí byť vykonávané s maximálnou efektívnosťou tak, aby boli bez porušenia zákonov štátu, prírody a lesníckych zásad vytvárané zdroje pre spravovanie lesného majetku štátu. Hospodárska úspešnosť LSR sa nehodnotí dosiahnutými tržbami, ale účelne vynaloženými nákladmi v prospech lesa, krytými predovšetkým vlastnou hospodárskou činnosťou. LSR hospodária tak, aby celkové hospodárenie bolo vyrovnané, t.j. aby lesy, ktoré vplyvom rentových dôvodov poskytujú zisk, umožňovali primerané hospodárenie aj v lesoch, ktoré sú z rentových dôvodov stratové. 
             LSR hospodária so zreteľom na trvalosť využívania všetkých funkcií lesa spoločnosťou, pričom osobitne dbajú na: 
 
 Nezmenšovanie výmery lesných pozemkov  a lesných porastov 
 Zvyšovanie podielu jemnejších spôsobov hospodárenia 
 Zachovávanie množstva a kvality vodných zásob 
 Ochranu pôdy a ovzdušia 
 Zachovanie osobitne chránených území lesa, lesných živočíchov a rastlín  
 Ekologickú rovnováhu 
 Možnosti rekreačného využívania lesa  
 
 2.2. Podpora z verejných zdrojov 
 LSR hospodári na vlastný účet a vlastnú zodpovednosť.  Špecifické výkony súvisiace s verejnoprospešnými funkciami lesa (voda, pôda, rekreácia, protilavínová ochrana),           s významnými investíciami (lesné cesty, protipožiarne pásy,...) a so strategickými lesohospodárskymi opatreniami (likvidácia kalamity, prírode blízke hospodárenie...) sa vykonávajú s účinnou podporou verejných zdrojov.     
 3. Orgány podniku 
 3.1. Stanovenie orgánov podniku  
 Orgánmi podniku sú: 
 Dozorná rada 
 Predstavenstvo 
 Etická rada 
 Spoločenská rada 
   
 3.2. Postavenie orgánov LSR 
 Najvyšším orgánom LSR je Dozorná rada. Schvaľuje strategické,  koncepčné a zásadné dokumenty LSR. Vyberá a menuje členov Predstavenstva LSR. Kontroluje činnosť predstavenstva LSR. Na jej čele stojí predseda Dozornej rady. Rada rozhoduje hlasom nadpolovičnej väčšiny všetkých svojich členov. 
 Predstavenstvo je najvyšším výkonným orgánom LSR. Jeho členovia riadia podnik na vlastnú zodpovednosť v zmysle zákonov a stanov. Na jej čele stojí generálny riaditeľ. Za svoju činnosť sa predstavenstvo zodpovedá Dozornej rade. 
 Etická rada dbá na dodržiavanie Etického kódexu podniku. Koná v rámci vlastnej iniciatívy, ako aj na základe podnetov daných vo veci porušenia Etického kódexu.     
 Spoločenská rada je poradným orgánom LSR. Pozostáva zo zástupcov mimovládnych organizácií, občianskych združení, štátnej správy, vlastníkov neštátnych lesov, zástupcov drevárskeho priemyslu, prípadne iných nárokových skupín, ktorí formulujú želania spoločnosti vo vzťahu k správe lesného majetku štátu. Spoločenská rada nemá právne nástroje na presadenie svojich postojov, má sa však za to, že tlmočí názory odbornej i laickej verejnosti, pričom je vhodné, aby orgány LSR tieto názory pri svojej činnosti primerane zohľadňovali, resp. vysvetľovali prečo to nie je vhodné. Členovia Spoločenskej rady sú čestní funkcionári, ktorým z členstva nevyplývajú žiadne práva a povinnosti. Na čele stojí predseda spoločenskej rady, ktorého si volia sami členovia. 
 3.3. Kreovanie orgánov LSR 
 Cieľom metodiky kreovania orgánov LSR je plná odborná a morálna kompetentnosť ich členov s dôrazom na nezávislosť týchto orgánov na aktuálnej mocenskej štruktúre štátu. 
 3.3.1.  Menovanie Dozornej rady  
 Dozorná rada má sedem členov, vrátane predsedu. Funkčné obdobie členov Dozornej rady je 7 rokov. Členov Dozornej rady vymenuje prezident republiky na základe návrhov: 
 Ministerstva pôdohospodárstva SR, ktoré nominuje  šesť kandidátov, z ktorých prezident republiky vymenuje troch členov. 
 Zamestnancov LSR, ktorí prostredníctvom priamej tajnej voľby nominujú dvoch kandidátov, z ktorých prezident republiky vymenuje jedného člena. 
 Slovenskej lesníckej komory, ktorá nominuje dvoch kandidátov, z ktorých prezident republiky vymenuje  jedného člena. 
 Lesníckej fakulty TU Zvolen, ktorá nominuje dvoch kandidátov, z ktorých prezident  republiky vymenuje  jedného člena. 
 Zväzu spracovateľov dreva, ktorý nominuje dvoch kandidátov, z ktorých prezident republiky vymenuje jedného člena. 
 Nevyhnutnou podmienkou pre návrh kandidáta členstva Dozornej rady je vysoký morálny kredit a preukázateľné mimoriadne pracovné výsledky vo svojom odbore. Kandidát, ktorý je členom politickej strany sa musí vopred písomne zaviazať, že v prípade menovania si pozastaví stranícke členstvo, a to na celú dobu výkonu funkcie člena Dozornej rady. 
 3.3.2. Predstavenstvo 
 Predstavenstvo má troch členov, vrátane generálneho riaditeľa. Členov predstavenstva a generálneho riaditeľa menuje Dozorná rada, pričom rešpektuje výsledky ňou vypísaného  výberového konania. 
 3.3.3. Etická rada 
 Má 9 členov, ktorí sú menovaní Dozornou radou z radov renomovaných zamestnancov LSR. Dozorná rada menuje aj predsedu Etickej rady. Členstvo v Etickej rade je časovo neobmedzené. Dozorná rada môže rozhodnúť o odvolaní doterajších a vymenovaní nových členov Etickej rady. 
 3.3.4. Spoločenská rada 
 Má najviac 11 členov, ktorí si sami volia svojho predsedu. Na základe návrhu vysielajúcich inštitúcií ich menuje minister pôdohospodárstva SR. Môže tak odmietnuť urobiť len v osobitnom prípade ak by tomu bránili závažné okolnosti (strata občianskej bezúhonnosti). 
 3.4. Zánik členstva 
 Členstvo v orgánoch LSR automaticky zaniká smrťou a spáchaním úmyselného trestného činu. Členstva sa možno písomne vzdať. Členov Predstavenstva, vrátane generálneho riaditeľa, môže kedykoľvek odvolať Dozorná rada. Člena Dozornej rady môže odvolať iba prezident republiky, a to iba v prípade, ak by bola zjavne a mimoriadne závažným spôsobom ohrozená vážnosť tohto orgánu. Prezident tak koná na základe návrhu inštitúcie, ktorá člena nominovala, no môže tak konať aj z vlastného rozhodnutia. Na uvoľnené miesto vymenuje nového člena Dozornej rady postupom podľa bodu 3.3.1., teda na základe  predloženia predpísaného počtu kandidátov nominujúcej inštitúcie. Takto odvolaný člen už nikdy nemôže byť nominovaný ani vymenovaný na člena žiadneho orgánu LSR. Vo všetkých orgánoch LSR je prípustné opakovanie členstva bez časového obmedzenia.      
 3.5. Povinnosti orgánov podniku 
 3.5.1. Povinnosti Dozornej rady 
 a/ Menuje a odvoláva Predstavenstvo a generálneho riaditeľa, rozhoduje o pracovných     zmluvách členov Predstavenstva a generálneho riaditeľa.   
 b/ Kontroluje činnosť Predstavenstva. 
 c/ Schvaľuje stanovy LSR a rozhoduje o ich zmenách. 
 d/ Schvaľuje Etický kódex a rozhoduje o jeho zmenách. 
 e/  Schvaľuje ročnú uzávierku. 
 f/ Dáva Predstavenstvu súhlas na konanie vo veci predaja lesného majetku štátu (1.5.) a nehnuteľného majetku LSR (1.6.). 
 g/ Schvaľuje rozdelenie nehnuteľného majetku do troch základných kategórií (1.6.) a vopred odsúhlasuje návrh Predstavenstva na presun majetku v rámci uvedených kategórií. 
 h/ Odsúhlasuje obchodné zmluvy presahujúce štatútom stanovenú finančnú výšku. 
 3.5.2. Povinnosti Predstavenstva  
 a/ Riadi LSR na vlastnú zodpovednosť podľa platných zákonov a stanov podniku. 
 b/ Vedie podnik so starostlivosťou riadnych hospodárov, pričom všetky svoje právomoci a povinnosti vykonáva výhradne v prospech  LSR. 
 c/ Ak poruší svoje povinnosti, zaväzujú sa členovia Predstavenstva nahradiť spoločne vzniknutú škodu spôsobenú ich konaním, resp. opomenutím konania na ktoré boli povinní v zmysle tohto zákona, resp. ostatným všeobecne záväzných právnych predpisov.  
 d/ Povinne sa podriaďuje všetkým rozhodnutiam Dozornej rady. 
 e/ Zastupuje podnik vo všetkých interných i externých úkonoch, a to spoločne, pokiaľ stanovy neurčia inak. 
 f/ Povinne poskytuje Dozornej rade a Ministerstvu pôdohospodárstva SR na ich požiadanie akékoľvek informácie a zároveň ich bez požiadania vždy vopred informuje o všetkých dôležitých rozhodnutiach. 
 3.5.3. Povinnosti Etickej rady 
 a/ Získava poznatky o porušeniach Etického kódexu, vyhodnocuje ich a predkladá Predstavenstvu návrhy na ich riešenie. Akékoľvek podozrenie z porušenia Etického kódexu je dôvodom na povinnosť zahájiť do 30 dní rokovanie Etickej rady a vydanie jej stanoviska. 
 b/ Predkladá Dozornej rade návrhy na úpravu Etického kódexu. 
 3.5.4. Povinnosti Spoločenskej rady 
 a/ Má čestnú povinnosť poukazovať všestranného na akékoľvek okolnosti súvisiace s lesným majetkom štátu v správe LSR. Svoje návrhy tlmočí písomne. Raz do roka má právo zúčastniť sa spoločného rokovania  s Dozornou radou a Predstavenstvom. 
 b/ Má právo požadovať a získať od Predstavenstva i Dozornej rady akékoľvek informácie o spravovanom majetku štátu a verejne sa k ním vyjadrovať.    
 4. Iné dôležité ustanovenia 
 4.1. Kariérny rast  
 Kritériom pre kariérny postup v LSR sú výlučne pracovné výsledky, morálna a odborná zdatnosť a výkonnosť. Je zakázané spájať ktorékoľvek pracovné pozície s inými kritériami, osobitne kritériami politickými. Obsadzovanie v štatúte určených vedúcich funkcií je možné vykonať len na základe výberového konania. LSR uplatňujú systém hodnotenia zamestnancov s cieľom usmernenia a využitia ich schopností s prepojením na ich kariérny rast alebo zostup.     
 4.2. Práca s verejnosťou 
 Medzi štandardné povinnosti LSR patrí práca s verejnosťou s cieľom pravdivo, komplexne a trvalo vysvetľovať funkcie lesa a zmysel hospodárenia v ňom. Osobitná pozornosť je v tejto súvislosti venovaná lesnej pedagogike, lesníckej publicistike, lesníckym informačným kanceláriám a lesníckemu múzejníctvu.   
 4.3. Verejné súťaže   
 Prioritnou metódou pri obchodnej činnosti podniku sú verejné súťaže a aukcie, ktoré LSR vyhlasujú za účelom dosahovania  najnižších cien vstupov a najvyšších výstupov,  a to bez toho, aby tým bola ovplyvnená požadovaná kvalita. Trvalý dôraz sa kladie na využívanie súťaží pri predaji dreva a prenájme revírov.  
 4.4. Spolupráca so školami  
 LSR aktívne spolupracujú s lesníckym školstvom s cieľom ovplyvňovať odborné vedomosti, praktické schopnosti a charakterové vlastnosti študentov tak, aby spĺňali požiadavky na zamestnanca podniku. Osobitnú pozornosť LSR venujú podpore a získavaniu mimoriadne talentovaných študentov. 
 4.5. Lesnícky výskum  
 Podnik navrhuje témy lesníckeho výskumu, skúšobne overuje jeho predbežné výsledky a už osvedčené výsledky prakticky uplatňuje. 
 Epilóg 
 Zákon vypracovaný v intenciách navrhnutých téz prospeje efektívnosti a čistote riadenia štátneho podniku LSR, a tým prospeje všetkým občanom Slovenskej republiky. 
   
 Poznámka na záver: V období do parlamentných volieb chceme predložiť na odbornú diskusiu návrh zákona o štátnom podniku v paragrafovom znení. 
      
   

