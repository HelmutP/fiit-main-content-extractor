

 Zhromaždenie viedol Starosta mestskej časti Bratislava – 
Staré Mesto Andrej Petrek, moderoval ho Miroslav Švec a nieslo sa v konštruktívnom duchu. Prvá dôležitá informácia ktorá odznela je, že pripravený materiál, ktorý bol k dispozícii pred rokovaním je len úvodný materiál do problematiky a bude sa na ňom ďalej pracovať. 

 Je to hádam prvý krát, keď sa niekto pýtal občanov čo si 
myslia hneď na začiatku tvorby materiálu z ktorého asi bude všeobecne záväzné 
nariadenie a nie až tesne pred schválením takéhoto materiálu v mestskom 
zastupiteľstve kde je už akúkoľvek drobnú zmenu od občana je problematické presadiť a o zmene základných vecí sa nedá ani hovoriť. 

 Vo všeobecnosti možno povedať, že Staré Mesto pripravuje 
buď rozšírenie existujúcej zóny s platením parkovaním alebo vytvorenie novej 
zóny s platením parkovaním. Je to dôsledok toho, že Staré Mesto má zmluvu s 
Bratislavskou Parkovaciou Službou, ktorej prenajalo komunikácie v aktuálnej zóne 
s tým, že ak sa rozšíri spadá toto rozšírenie do zmluvy. 

 Predstava o cenách za parkovanie na ulici je 1 Euro za 
hodinu v centre a 0,5 Euro za hodinu v ostatných častiach Starého mesta. Pričom 
bude spoplatnený len čas od 08:00 do 16:00 v pracovných dňoch. 

 Domáci obyvatelia by mali mať možnosť zakúpenia paušálnej 
karty, ktorá by ich oprávňovala parkovať kdekoľvek na ulici v Starom Meste. 
Zatiaľ nie je stanovená cena (odhad 1000 - 5000 Sk) ani to či bude jedna karta 
za domácnosť alebo na dospelého obyvateľa z domácnosti v Starom Meste pokiaľ má 
žiadateľ vodičské oprávnenie. 

 V pláne je postupné zrušenie všetkých trvalých vyhradených 
parkovacích miest (vyhladzovacích stĺpikov) , okrem miest pre invalidov. 
 Zneprejazdnenie niektorých ciest a iné zmeny v doprave sa 
nepreberali, nakoľko je pri tom potrebná súčinnosť s Magistrátom a je nutne na 
počítači modelovať aký vplyv budú mať jednotlivé zmeny na dopravu, aby nedošlo k totálnemu dopravnému kolapsu. 

 Dá sa predpokladať, že ku kolapsu aj tak príde, keď sa 
uvedú do prevádzky rozostavené objekty, nakoľko sa investorom podarilo vyhnúť 
dopravnej štúdii. 

 Doteraz bol problém, že sa stavali nové objekty, ktoré 
nemali dostatok parkovacích miest pre 1-obyvateľov, 2-zamestnancov, 
3-navštevnikov a zákazníkov, 4-dodávateľské služby. Teraz sa stavajú objekty kde sa už väčšinou na tieto parkovacie miesta myslí, ale ešte sa nemyslí, že 
daný objem vozidiel musí do garáží mať možnosť vojsť aj ich opustiť a okolité a aj tranzitné dopravné komunikácie musia túto záťaž uniesť vrátane MHD.  
Taktiež sa momentálne nemyslí na miesta pre vystupovanie a nastupovanie ľudí pre 
prípad keď sa viacerí vezú v jednom aute a vodič robí ich rozvoz napríklad pri 
ceste do práce a podobne. 

 Počet vozidiel aj v Starom Meste pribúda nielen so stúpajúcou životnou úrovňou, ale najmä s výstavbou nových objektov. Veľa z tých čo vystúpili v diskusii uviedli, že v rodine má dve a viac aut. Z čoho následne vyplynula požiadavka na výstavbu parkovacích domov a ich nezisková prevádzka pre obyvateľov Starého Mesta. Bohužiaľ Staré Mesto nemá pozemky na ktorých by sa dala takáto výstavba realizovať. Má ich jedine Hlavné mesto a ich poskytnutie Starému Mestu je veľmi otázne. 

 Zaujímavá informácia padla aj o Bratislavskej Parkovacej Službe ako sa dostala k nevypovedateľnej zmluve. Prv si parkovane zabezpečovala mestská časť pomocou vlastného oddelenia. V roku 1996 vedúci oddelenia navrhol osamostatnenie tejto činnosti a vytvorenie k tomuto účelu s.r.o. kde by mala mestská časť 80% a on 20%, s tým, že by mestská časť dostávala peniaze za prenájom komunikácií a príslušného podielu zo zisku. Lenže pred hlasovaním o danom materiáli ako celku dal jeden poslanec (neviem kto) pozmeňovací návrh aby sa percentá otočili a tento návrh prešiel. Výsledok je taký, že Staré Mesto môže navrhnúť akúkoľvek cenu, ale platiť bude tá, ktorú povie BPS. 

 Občanov najviac zaujímalo: 

 Prečo keď niekto býva - nepodniká v pešej zóne má platiť 
iný poplatok za vjazd ak má oprávnenie na živnosť než keď nemá oprávnenie na 
živnosť. Tá istá otázka padla prečo má bývajúci platiť inú sumu ak je vlastníkom 
auta než keď ho má na leasing alebo z požičovne. Odpoveďou starostu bolo, že sa 
pripravuje zmena predmetného VZN a takéto nedostatky sa odstránia. 

 Prečo zmluva s BPS nie je verejne prístupná napríklad 
prostredníctvom internetu, nakoľko sa dotýka prenájmu obecného majetku. Starosta 
prisľúbil, že sa poradí s právnikmi a ak bude mužné uvedenú zmluvu zverejniť tak 
ju dá na internet. 

 Ja by som k tejto zmluve a iným lukratívnym zmluvám povedal 
len toľko, že v procese schvaľovania v mestskom zastupiteľstve tvorili verejne 
prístupný materiál na rokovanie, do ktorého má právo nahliadnuť každý, možno by 
stalo za to pohrabať sa v archíve a aktuálnu zmluvu poskladať. 

 Záver: 

 Perspektíva okolo parkovania v Starom Meste nie je ružová. Situácia sa bude zhoršovať a bohužiaľ sa bude zhoršovať aj prejazdnosť komunikácií. Jediné čo je pozitívne, že sa začalo nad touto situáciou zamýšľať. 
V prípade záujmu sledujte 
www.staremesto.sk kde by sa mal objaviť nový materiál a bude týždeň na jeho pripomienkovanie. 

