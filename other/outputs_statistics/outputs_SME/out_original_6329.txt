
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Novotňák
                                        &gt;
                Určite to poznáte.(Fejtóny)
                     
                 Kúpiť žene darček, to nie je len tak... 

        
            
                                    13.5.2010
            o
            9:04
                        (upravené
                12.5.2010
                o
                13:29)
                        |
            Karma článku:
                9.94
            |
            Prečítané 
            2480-krát
                    
         
     
         
             

                 
                    Páni, určite to poznáte. Blíži sa nejaký sviatok vašej manželky, priateľky, družky, alebo ste len niečo vyparatili a potrebujete si to vyžehli, alebo...no  skrátka potrebujete darček. Ale na to pozor! To nie je len tak.
                 

                 Vo všeobecnosti sa dajú darčeky pre ženu zhrnúť do štyroch základných kategórií:  Prvá kategória je stáročiami overená a osvedčená klasika. Sem patria šperky, kozmetika, kvety... V tomto prípade je úspech zaručený, veď už ste niekedy počuli o žene, ktorá by povedala, preboha , načo sú mi ďalšie náušnice, veď ja už nejaké doma mám. Alebo počuli ste niekedy o žene, ktorá by povedala, že nechce nový parfum, pleťový krém či šminky? O kvetoch ani nehovoriac, takže tu niet čo pokaziť.   V podstate bezproblémová sa dá nazvať aj kategória druhá a to sú darčeky, ktoré by sme mohlo označiť ako užitočné. Je síce pravda, že ak žena rozbalí darček a nájde tam kľučky na dvere do vášho spoločného práve prerobeného bytu asi veľmi od radosti skákať nebude, ale to ju prejde, keď  budú kľučky namontované a ona už nebude musieť otvárať dvere na záchode šraubovákom. Do tejto kategórie by sme mohli zaradiť napríklad aj taký nový výfuk na auto, hlavne ak ten starý odtrhla ona, keď si chcela skrátiť cestu z parkoviska cez obrubník.   Opatrnejší by som však bol pri tretej kategórií a to sú darčeky, ktoré kupujeme ženám, lebo ich chceme my. Tu si to už vyžaduje nejakú prípravu a určité komunikačné schopnosti. Neslobodno to zanedbať, lebo ak je vaša žena technický analfabet a dostane najnovšiu digitálnu zrkadlovku od svetoznámeho výrobcu fotoaparátov a k tomu zopár objektívov, polarizačné filtere a statív, je dobré mať celú záležitosť dôkladne premyslenú a naplánované nejaké odôvodnenie.  Aj na takú vŕtačku s príklepom a novotou sa lesknúce vrtáky môže pozerať s vypleštenými očami, preto je dobré mať po ruke pripravené nejaké vysvetlenie. Nikdy, opakujem nikdy prípravu neodfláknite s tým, že vás hádam niečo napadne priamo na mieste. Nenapadne, verte mi. Vtedy je už neskoro. A okrem toho neistý, koktajúci muž s kvapkami potu na čele nevyzerá veľmi dôveryhodne.   Najnebezpečnejšou kategóriou, a tu by som bol obzvlášť opatrný, je štvrtá kategória. Sem patria darčeky, ktoré sme zvolili len tak, na poslednú chvíľu, bez rozmyslenia a v rukách sklamanej ženy môžu  poslúžiť ako zbraň. Je pravda, že ako zbraň môže poslúžiť snáď každý predmet, ale pravdepodobnosť, že vás rozrušená žene trafí s pamätným tanierom k osemstopäťdesiatému výročiu prvej zmienky o vašom meste, ktorý ste jej dali k narodeninám je dosť malá. Uznajte. A o  rane reklamným vankúšikom s logom vašej firmy, ktorý jej zabalíte ako darček k výročiu svadby ani nehovoriac. V strehu však treba byť v prípade takej sady kutáčov na stojane ku krbu obzvlášť, ak žiadny krb nemáte, v prípade elektrického predlžovacieho káblu, či valčeka na cesto, lebo z takýchto darčekov sa môže vykľuť ten povestný bič, ktorý plieska na konci, tak bacha, aby  neplieskal na vašom chrbte.               ...a keď tak premýšľam nad tou štvrtou kategóriou, v hlave sa mi preháňajú myšlienky, vety sa prevaľujú jedna cez druhú, slová sa samé ukladajú na seba ako kocky lega a ja som si spomenul na môjho kamaráta Jana...        Jana vám raz napadlo  kúpiť Anči švihadlo. Cez švihadlo skoky zoštíhľujú boky, tak sa hádam poteší figúru si vylepší.       Švihadlu sa potešila, že po takom vždy túžila a teraz keď Jano pije Anča ho švihadlom bije a z ruky ho nepustí kým ju zlosť neopustí.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (144)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Novotňák 
                                        
                                            Sásovská dolina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Novotňák 
                                        
                                            Jardín de Cactus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Novotňák 
                                        
                                            Fundacion César Manrique
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Novotňák 
                                        
                                            Hrad Conwy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Novotňák 
                                        
                                            Dolinou Kordického potoka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Novotňák
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Novotňák
            
         
        novotnak.blog.sme.sk (rss)
         
                                     
     
        Raz som si všimol jeden plagát.Bolo na ňom "Choď a pozeraj sa!"Odvtedy idem a pozerám sa.





        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1815
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Určite to poznáte.(Fejtóny)
                        
                     
                                     
                        
                            Kopance od múzy
                        
                     
                                     
                        
                            Anglicko krížom krážom
                        
                     
                                     
                        
                            Lanzarote krížom krážom
                        
                     
                                     
                        
                            Slovensko krížom krážom
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Francúzska výchova v samoobsluhe
                                     
                                                                             
                                            Ako premeniť priekopu vedľa cesty na cyklocestičku
                                     
                                                                             
                                            Japonský zázračný spôsob skladania tričiek
                                     
                                                                             
                                            Rodený diplomat
                                     
                                                                             
                                            Ľudia,, toto musíte vidieť,,
                                     
                                                                             
                                            Alberobello. Medzi rozprávkovými domčekmi
                                     
                                                                             
                                            SaS ako urýchľovač vývoja slovenských dejín ? Pascalova stávka.
                                     
                                                                             
                                            Aj Rusi majú svoju „Monu Lisu“ – portrét M. Lopuchinovej (1797)
                                     
                                                                             
                                            Škrekot vtáčí
                                     
                                                                             
                                            Neviete, koho voliť? Poradíme!
                                     
                                                                             
                                            Prečo je Švajčiarsko bohaté?
                                     
                                                                             
                                            Ježiš bloguje - kreslený vtip
                                     
                                                                             
                                            Ruja starého uja
                                     
                                                                             
                                            „ máte sa? “ / s náznakom pointy /
                                     
                                                                             
                                            Ani ten komplex menejcennosti nemám poriadny!
                                     
                                                                             
                                            Kto chýba Cigánčatám?
                                     
                                                                             
                                            Rozprávky z Nepálu (1) - O kráľovstve, ktoré už nikto nechcel.
                                     
                                                                             
                                            Angličtina z telky het! alebo Jazykový Zákon Appreciation Societ
                                     
                                                                             
                                            Bicie z nahých zadkov žien
                                     
                                                                             
                                            SNS choďte do čerta!
                                     
                                                                             
                                            Tomuto neuveríte!
                                     
                                                                             
                                            Amaterizmus v STV.
                                     
                                                                             
                                            Pustím ťa dnu
                                     
                                                                             
                                            70 dní Hodvábnej cesty = 186 dopravných prostriedkov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            baranek.blog.sme.sk
                                     
                                                                             
                                            martinpollak.blog.sme.sk
                                     
                                                                             
                                            kubus.blog.sme.sk
                                     
                                                                             
                                            kikakovacikova.blog.sme.sk
                                     
                                                                             
                                            ulaherova.blog.sme.sk
                                     
                                                                             
                                            konarova.blog.sme.sk
                                     
                                                                             
                                            palolajciak.blog.sme.sk
                                     
                                                                             
                                            jaromirmichalnovak.blog.sme.sk
                                     
                                                                             
                                            konopaskova.blog.sme.sk
                                     
                                                                             
                                            peterchrenka.blog.sme.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




