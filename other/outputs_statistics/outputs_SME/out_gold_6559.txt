

 Ale poďme k veci: 
 Prečo cítim potrebu vyjadriť svoj postoj k rovnosti pohlaví? Jednoducho preto, lebo si myslím, že často ženy a muži nie sú vnímaní ako rovnocenní partneri. Rozumiem, spoločnosť v ktorej žijeme bola (a ešte stále je) veľmi dlho patriarchálna. Ženy nehrali nejakú významnejšiu „spoločenskú rolu". Starali sa o domov, deti. Okolnosti sa však zmenili, dnes si nikto (navonok) veľmi netrúfa deliť zamestnania alebo práce podľa pohlavia. Dnešné ženy sú emancipované a mužov k životu vraj niektoré (ako tvrdia) nepotrebujú. Naozaj? 
   
 Povedal by som, že ženy potrebujú mužov a muži zase potrebujú ženy. Akékoľvek vyvyšovanie jedného pohlavia nad druhé nemá opodstatnenie a v dnešnej, „vyspelej" spoločnosti by nemalo mať už ani miesto. Veď odlišnosť pohlaví nie je dôvodom na akúkoľvek diskrimináciu. Na druhej strane, ťažko možno očakávať, že pohlavie človeka môže byť jednoducho ignorované. Napriek tomu by ženy a muži mali byť rovnako hodnotení, rovnako rešpektovaní, mali by mať rovnaké možnosti a príležitosti. O tom nie je potrebné diskutovať. Nerád by som sa však dožil niečoho neprirodzeného... Tým chcem povedať, že muži by sa mali k ženám stále správať ako k ženám (a opačne), pretože rovnosť v živote neznamená rovnakosť. Rovnosť, to je úcta k rôznosti, úcta k osobitosti a úcta k odlišnosti... rešpekt k individualite každého človeka ako aj vyzdvihovanie predností jedného aj druhého pohlavia. Nemyslím, že je problém v „byť hrdý/á/ na to, že som muž/žena/" - každý sme sa tak narodili a hrdosť je v poriadku - ide iba o uznanie kvalít toho druhého a ocenenie jeho výnimočnosti a nenahraditeľnosti pre ľudskú spoločnosť. 
   
 Rád by som a vyhol otrepaným frázam, ale budúcnosť by mala nájsť obe pohlavia stáť na jednom stupienku, bez akýchkoľvek snáh o vyvyšovanie mužov na úkor žien, ako aj opačne. Muži by bez žien neprežili jedinú generáciu, ženy bez mužov takisto. Vážme si možnosť prežiť svoj život v role muža/ženy/, ale nezabudnime obdivovať a ceniť si opačné pohlavie pre jeho výnimočné vlastnosti, také nepodobné našim... Či muži alebo ženy, v prvom rade sme ĽUDIA. Ak by boli obe pohlavia rovnaké, život by stratil svoju krásu. 
   
   
 P.S.: Malá poznámka k nerovnosti pohlaví: 
   
 (V mnohých dnešných kultúrach a spoločnostiach  ženy stále nemajú ani zďaleka rovnoprávne postavenie. S tým sa však dá ťažko niečo robiť. Všetky kultúry idú svojou cestou a svojím tempom smerom k budúcnosti - možno v arabských krajinách bude trvať ešte niekoľko desať(sto?)ročí, kým budú ženy rovnoprávne s mužmi. Zasahovať do cudzích kultúr však nie je ideálne riešenie a môže urobiť viac škody ako úžitku. Určite treba ženám v takýchto krajinách pomáhať, ako sa dá a snažiť sa o zlepšenie ich postavenia. Niektoré veci ale jednoducho potrebujú čas. Myslím, že toto je jedna z nich.) 
   

