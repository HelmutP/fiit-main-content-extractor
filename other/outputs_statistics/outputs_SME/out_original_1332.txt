
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Drotován
                                        &gt;
                Média
                     
                 Najsledovanejšie televízie ignorujú kresťanskú Veľkú noc 

        
            
                                    10.4.2009
            o
            10:15
                        |
            Karma článku:
                9.06
            |
            Prečítané 
            2214-krát
                    
         
     
         
             

                 
                    Opäť tu máme Veľkonočné sviatky. Tie, ktoré sú podľa kresťanskej tradície najvýznamnejšími zo všetkých v roku. Mal by to byť čas, keď sa zamyslíme nad sebou a uvedomíme si, že tu nebudeme stále. Náš život však môže pokračovať, ak uveríme nádeji na večný život, ktorú nám dal Ježiš Kristus svojim zmŕtvychvstaním. Neberiem nikomu to, ak tomu neverí, ale nechápem, prečo tu nádej berie aj nám ostatným? Možno sa pýtate: „ako"? Napríklad aj tým, že o nej nehovorí.
                 

                 
Záber z filmu Umučenie Kristahttp://port.sk/picture/instance_4/29786_4.jpg
   Teoreticky, ak by ste sa dnes prebrali z dlhého spánku ako by ste prišli na to, že je Veľká noc? Mohol by Vám to niekto povedať. Keby ste boli náhodou sám a nemali pred sebou ani kalendár, tak kto? S veľkou pravdepodobnosťou by ste sa skôr či neskôr uchýlili k televízoru. Šanca, že by Vám tam niečo pripomenulo tieto sviatky a ich výnimočnosť je však minimálna.   Na Veľký piatok, deň, keď si ako veriaci pripomíname umučenie a smrť Ježiša Krista, sa nás najsledovanejšie televízie snažia presvedčiť, že je piatok ako každý iný. Jedna zabáva ľudí tak ako vždy reláciou Aj múdry schybí, druhá ponúka svoje akčné thrillery. Na Bielu sobotu je ich program preplnený zase komédiami. V čase veľkonočnej vigílie láka ľudí na Poštu pre teba aj verejnoprávna televízia. Pre druhú sa hrdinom Veľkej noci stal Zorro. „Trhni si!", odkazuje titul infantilnej komédie inej televízie pre tých, čo hľadajú nebodaj niečo s myšlienkou. Objavil niekto nejakú spojitosť so sviatkami? Ja teda ani s lupou nie. V minulosti bolo možné sledovať aspoň veľkofilm Umučenie Krista, tento rok ani to nie.   Viem, že globalizácia a západná kultúra dorazili už aj na Slovensko, ale patríme dnes už aj my medzi krajiny, kde je jedinou podstatou materiálny pôžitok? Sú duchovné hodnoty už minulosťou alebo sa nás len majitelia televízii a iných médií snažia presvedčiť, že to tak je? Nebude to náhodou tak, že vedia, že tieto hodnoty sú nepredajnými a tak nám radšej ponúkajú a niekedy až doslova vnucujú tie ich predajné? Premiér Fico s obľubou nadáva na média, že sú jednostrannými. V určitom zmysle však má žiaľ pravdu, a to že sú nositeľmi myšlienok ich majiteľov, konkrétne ideálov konzumnej spoločnosti. Zdá sa mi miestami akoby sa cielene snažili urýchliť čas, aby na nás mohli čoskoro zarábať. Niekedy akoby nedokázali názory veriacich už ani tolerovať. Od nás to však neustále vyžadujú.   O tom, že veľká časť obyvateľov SR stále uznáva tradičné hodnoty nás mohli presvedčiť aj ostatné prezidentské voľby. Pri ich rozhodovaní o vhodnosti či nevhodnosti jednotlivých kandidátoch mali pravdepodobne značnú váhu. Istá časť ľudí bola ochotná nevoliť kvôli nim resp. zvolili toho, koho mu vykreslili média a premiér ako človeka bližšieho k týmto hodnotám (Ivana Gašparoviča). Prečo sa média napriek tomu a aj iným štatistickým údajom stále snažia popierať hodnotový svet? Sú veriaci naozaj v takej menšine ako to berú televízie? Realita usporiadaných rodín a napríklad aj rady mladých čakajúcich na svätú spoveď pred kapucínskym kostolom v Bratislave ukazujú niečo iné. Nenechajme sa teda oklamať!   Prajem Vám príjemné a požehnané prežitie Veľkonočných sviatkov. Želám si, aby sa nám podarilo ďalej hľadať a nájsť skutočný zmysel života, aby sme sa nenechali odradiť tým, čo nám ponúka tento svet ako to pravé a jediné správne.   P.S.: Na článok nebolo možné počas Veľkej noci reagovať, pretože som nechcel narúšať pokojnú atmosféru týchto sviatkov diskusiou.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Ministerstvá a ich pobočky kupovali a predávali na poslednú chvíľu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Nezávislé osobnosti začali doplácať na Matoviča
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Televízny odpad namiesto serióznej politickej diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Okná od Eurojordánu, nikdy viac
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Neskutočne poddajný Smer alebo len paranoja CBR?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Drotován
            
         
        jandrotovan.blog.sme.sk (rss)
         
                                     
     
        Som Bratislavčan s vlastným pohľadom na veci, pochádzajúci z mnohodetnej kresťansky založenej rodiny.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2042
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Média
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Média v čase globalizácie (Tadeusz Zasępa)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ladislav Lencz
                                     
                                                                             
                                            Jozef Drahovský
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Viliam Búr
                                     
                                                                             
                                            Michal Drotován
                                     
                                                                             
                                            Peter Kunder
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Mediálne.sk
                                     
                                                                             
                                            imhd.sk
                                     
                                                                             
                                            Seriály.KINEMA.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




