
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Šimčík
                                        &gt;
                Subkultúry
                     
                 História Trojan Records 

        
            
                                    19.4.2010
            o
            7:56
                        (upravené
                23.4.2010
                o
                8:38)
                        |
            Karma článku:
                5.07
            |
            Prečítané 
            1689-krát
                    
         
     
         
             

                 
                    Populárnu hudbu posledných 50. rokov neformujú len na hudobníci ale často aj na vydavateľstvá. Niektoré z nich sa časom stali mamutími spoločnostiami, niektoré zanikli a niektoré nadobudli status kultové. Jednou z posledných menovaných je aj Trojan Records.
                 

                 
Duke Reid 
   Prvýkrát sa milovníci hudby mohli stretnúť s pojmom Trojan okolo roku 1967 kedy sa takto označovovala produkcia jamajského producenta a DJ Duka Rieda. Ten so soundsystéom naloženom na nákladnom vozidle Leyland Trojan cestoval napriek Jamajkou a usporiadával tanečné akcie.   O rok neskôr Trojan records sa stalo vydavateľstvom importujúce jamajksú reggae a ska hudbu do Velkej Británie, kde sídlila veľká karibská populácia. Kedže od prvej lode s emigrantmi  z  karibiku uplynulo už 20 rokov, černošká mládež v londýnskych štvrtiach bola relatívne dobre zžitá s belošku populáciu a zákonite začala jedna na druhú vplývať. Stret černošskej subkultúry Rude Boy a belošskej Mods mal za následok vznik raných skinheads. Práve rok 1969 kvôli boomu skinheads a prieniku niektorých reagge hudobníkov do rebríčkov je označovaný ako zlatý vek skinheads a veľká časť jeho antirasistickej vetvy na ňu nadväzuje (tradicionalisti).      Centrum tohto hnutia sa stalo vydavateľstvo Trojan, vydávajúc ska, reaggea, dub a neskôr aj dancehall. Na jeho platniach sa objavili John Holt, Lee Perry, Jimmy Cliff, The Wailers, The Pionneers, The Maytals, Harry J alebo z nich najúspešnejší Desmond Dekker.      Jediným belošským umelcom, ktorý bol vydaný na Trojane bol Judge Dread. Ten dokonca vyhral jamajskú hitparádu a keď následne robil turné po Jamajke, domaci boli prekvapený že je beloch. Kultové sa stali aj kompilácie Tighten Up s polonahými slečnami na obaloch.      Začiatkom 70 rokov sa reaggae hudba začína meniť. Tempo sa spomaľuje s texty sa stávajú výrazne politické a náboženské (rastafarianizmus), na čo Trojan so svojou apolitickou filozofiou nedokáže reagovať a upadá. Následné desaťročia ťažiskom katalógu stále ostaváju hudobníci z prelomu 60. a 70. rokov. Keď sa v druhej polovici 80. rokov  po rasistickom vplyve na skinheads, hnutie začína opäť očisťovať a vracať ku koreňom, logo Trojan Records - antická vojenská prilba -  sa stáva základom pre logo hnutia SHARP - Skinheadi proti rasovým predsudkom.      V roku 2001 sa Trojan Records znova ohlasuje, ked začína vydávať úspešne 3 diskové kompilácie Trojan Box Set, na rôzne témy mapujúc svoj katalóg. 40 rokov po založení vydavateľtsvo stále žije šíriac okrem dobrej hudby aj ideu multikulturárnosti a tolerancie, ktorá môže obohatiť na prvý pohľad úplne odlišné strany.                           Zdroje:   www.trojanrecords.com   en.wikipedia.org     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Ako Maťo Ďurinda reggae hral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O retre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O hmle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O nechcených vianočných darčekoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Keď jeden chýba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Šimčík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Šimčík
            
         
        simcik.blog.sme.sk (rss)
         
                        VIP
                             
     
        Agropunk, vraj aj Mod, stratený ako working class hero na východoslovenskom vidieku.  Rád chodím na futbal, koncerty, do pubu, kostola a z času na čas aj na disco :)

"Myslím, teda slon"


  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    186
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názory
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Subkultúry
                        
                     
                                     
                        
                            Británia
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Čo sa domov nedostalo
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Roger Krowiak
                                     
                                                                             
                                            Rudolf Sloboda - Do tohto domu sa vchádzalo širokou bránou
                                     
                                                                             
                                            Subculture - The Meaning Of Style
                                     
                                                                             
                                            F.M. Dostojevskij - Idiot
                                     
                                                                             
                                            NME
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Arctic Monkeys
                                     
                                                                             
                                            The Gaslight Anthem
                                     
                                                                             
                                            Slovensko 1
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            BBC Radio 2
                                     
                                                                             
                                            BBC Mike Davies Punk Show
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tatran Prešov
                                     
                                                                             
                                            Bezkonkurenčný Dilbert
                                     
                                                                             
                                            flickr
                                     
                                                                             
                                            discogs
                                     
                                                                             
                                            ebay.co.uk
                                     
                                                                             
                                            Banksy
                                     
                                                                             
                                            Kids And Heroes
                                     
                                                                             
                                            Roots Archive
                                     
                                                                             
                                            West Ham United
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            denník Guardian
                                     
                                                                             
                                            denník Pravda
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




