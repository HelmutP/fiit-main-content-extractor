

 Zapnem počítač a píšem blog. Vo chvíli, keď som takmer na konci, pripojenie zmizne a definitívne. Nič to, veď zajtra to snáď bude lepšie, zavolám dcére ako pokračuje s diplomovlou. Ale ani prezvoniť sa mi nedá, signál nemám ani na jedinú čiaročku. 
  Tak by som si mohla niečo pozrieť v televízii. Prepínam a prepínam, ale rýchlo ma to prestane baviť. Medzitým sa totiž spustil riadny lejak a ani jeden kanál nefunguje čisto. Istejšie bude všetko povypínať, zdá sa mi, že v diaľke počujem hrozivé hrmenie. 
 Zasiahol nás chvost strašnej búrky. Krúpy podierkovali aj tak dosť biedne listy rastliniek a dolu dvorom sa valí mútna voda. Východ je na tom určite horšie, temné olovené mraky na tej strane neveštia nič dobré. Naša pivnica je suchá a Ipeľ drží hrádza. Z tejto strany nám nehrozí nebezpečenstvo. 
 Ľudia v prestávkach medzi dažďovými vlnami vychádzajú skontrolovať svoje dvory a záhrady. So strachom pozerajú na tmavé nebo. Mnohí hovoria, že nič také ešte nezažili a že to má na svedomí sopka na Islande. 
 Čo sa to vlastne deje? Písala som už raz o tom, že len tak náhodou sa nič nestane. Určite aj na takéto extrémy existuje reálne vysvetlenie. Niečo som raz začula v televíznych novinách, ale nie od meteorológov, tí sa vyjadrujú  k problému opartne. 
 Zapálila som sviece presne tak, ako v časoch keď nebola televízia, internet, ani mobilné telefóny. Všade sa rozlieha hrobové ticho. Dokonca ani lietadlá nad nami nepočuť. Príroda im pristrihla krídla. 

