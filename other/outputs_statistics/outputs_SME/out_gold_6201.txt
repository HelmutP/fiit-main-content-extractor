

 Pitvy k stavu medickému patria. 
 Niekedy to človek v pitevni ustojí a cvičenie skončí, ani nevie ako. (Ľudské telo, napríklad pre mňa bolo, je a bude zdrojom fascinácie. Mozog zvlášť.). Inokedy to bolí už len keď vidí fotky. 
 Súdnym lekárom sa pod ruky dostane každý, u koho nie je príčina smrti jasná, kto zomrel chirurgom na stole (mors in tabula) alebo sa vyšetruje, či jeho smrť nenastala chybným lekárskym postupom, každý, kto z tohto sveta zišiel násilím, či už vlastnou alebo cudzou rukou. 
 Už nie je doma, vo svojom oblečení, špinavý. Na pitevnom stole to už je človek vytrhnutý z kontextu, nahé telo na pracovnom stole. Po prvom nafotení umytý asistentom. 
 Výsledok je niekedy zarážajúci. Mladý chalan, príjemná tvárička, akoby spal. Až na jeden detail. Že si strelil do spánku. Vôbec, tie obrázky ľudí tak podobných tým, čo nám ukazujú chirurgovia a intenzivisti, medik naučený, že zachránili sme... a potom prásk! Na tretej fotke zo série zranenie, ktoré mladý muž (iný prípad ako prvý spomínaný) určite nerozchodil. 
 Nech si hovorí kto chce, čo chce o rovnocennom prístupe, ak je onou mŕtvolou starý spitina (z ktorého vnútorností po otvorení ešte tiahne), nebolí to ani zbla tak, ako keď z pozostatkov kričí, že je to dieťa alebo mladý človek. Na celom tom nie je najhoršia tá bezkontextová mŕtvola na pitevnom stole. Je to ten príbeh, ktorý ju tam dostal. Niekedy pracne skladaný najlepšími odborníkmi. 
 Situácie niekedy tak kontrastné, až sa javia pretkané istým zvláštnym poetičnom, svet inou optikou, tak pokrivenou, že na konci je niekedy dúha. Púpavové pole, bezoblačná jarná obloha. Uprostred poľa malé lietadlo zaryté čumákom do zeme, v ňom mŕtvy pilot. "Keď som tam prišiel, okolo stáli policajti a fajčili jednu od druhej. Keby to videl Svěrák, natočí o tom film." 
 Scenáre smrti, niekedy absurdné, niekedy priam zbytočné. Také, aké napíše len život so smrťou. 
 Panoptikum nášho slovenského minisveta. Postavy, postavičky, mafiánske vraždy a po rokoch vykopávané mŕtvoly. Biele kone, veľké zvieratá aj malé ryby, v smrti si všetci rovní navzájom. Psychopatickí vrahovia, domáce násilie, znásilnenia, sebevraždy jednoduché aj prešpekulované. To všetko často impregnované alkoholom. "Ja nerozprávam, čo eskimáci. To Slováci." Všetky tie neuveriteľné veci, čo si ľudia dokážu navzájom porobiť.  Sfetovaný stopár, pitva vodiča: "Zasadil mu ranu do krku. Potom ďalších  stodeväťdesiatpäť." 
 "Za svoj život uvidíte toľko mŕtvol, že vám z toho bude zle." Už teraz dosť na to, že sa mi síce po prednáške nesnívalo o mŕtvolách, ale prisahám, že zlé sny som mala. A pár dní som z toho bola riadne vytretá (výraz pre mňa pomerne dobre vystihuje špecifický smutný stav sprevádzajúci stretnutie s niektorými prípadmi). 
 Pre toto všetko patrí súdnym lekárom môj veľký obdiv. Skladajú z kúskov tiel príbehy, sú tí poslední, ktorí nechajú mŕtveho prehovoriť. A nesú si so sebou celým svojim životom tie príbehy a mená, niekedy aj mŕtvolný pach, ktorý sa nie vždy dá hneď osprchovať. 
 Dnes v zjemnenom a cenzúrovanom svete plnom vône najnovšieho  dezodorantu, módnych šatočiek a večnej mladosti je toto iná lekcia do  života. 
 Memento mori. Pamätaj na smrť. 
 Carpe diem. Uži deň. 
   
 
 (výroky v úvodzovkách sú pomerne presné citácie zachytené a zapísané na prednáškach a stážach) 
   

