

 Možno je pravda, že po štyroch rokoch vlády mala máloktorá strana väčšiu podporu ako pred volebným obdobím. 
 Ale je pravda aj to, že tieto voľby neboli súťažou jednotlivých strán, ale zoskupení PRo-Fico a ANti-Fico. 
 Boli síce bitkou strán, ale boli vojnou zoskupení PRo-Fico a ANti-Fico. Bitku síce Fico vyhral. Z vojny však musí odísť ako porazený, pretože vládu jednoducho nepostaví a o to v tejto vojne išlo. 
 Tých jeho šesť percent plus treba skresať o šesť percent, ktoré stratila SNS a navyše o tie nemalé percentá, o ktoré prišiel ujo Vlado. 
 Takže úspech-neúspech. 
 Tak, ako pred rokmi nikto nevidel, že Fico sa netají tým, že mu nejde o nič iné, ako o účasť vo vláde a kradnutí-ako to ukazoval štyri roky, tak je tomu aj teraz. 
 Fico dokonca otvorene povedal, že mu nejde o nič, len o to, aby bol pri koryte. Otvorene sa vyjadril, ze pôjde do vlady s hocikým od Radičovej, na ktorú cez plagáty kampane hučal, že obrala invalidov, cez KDH, na ktorých cez plagáty svojej kampane poukazoval ako na pravicový zlepenec, až po Most, o ktorých sa vyjadroval, že je to to isté, čo SMK a treba spraviť všekto preto, aby SMK nemala opäť moc - čím poprel vlastné hodnotové presvedčenie a ešte si aj úplne nesofistikovaným spôsobom pred všetkými nasral do huby. Otvorene povedal, že ak vo vláde nebude, nech už bude vláda akákoľvek, bude robiť všetko preto, aby ju zničil a dostal sa do nej on. 
 Rovnako ako Fico, ani Belousovová /zastupujúca predsedu Divného Janka, ktorý bol zrejme niekde v divnom stave/ nezabudla podotknúť rozdielnosť názorov strán, ktoré podľa všetkého /ak žiadna z nich nepoĽaví/ budú tvoriť koalíciu. Áno, liberáli a konzervatívci, to sa trochu škrie, to je pravda. Ale iste nie tak, ako sa škrie 
 extrémna pravicová strana, ktorá hlása, že hranice nejakého štátu treba vymazať z mapy a podobne, s extrémne ľavicovou stranou, ktorá hlása znárodňovanie a zákaz zisku. 
 Určite, rozdiely v riešeniach sú, ale politika je o kompromisoch, o ktorých súdruh Fico nemá ani poňatia, keďže štyri roky sedel vo vláde s prisluhovačmi, ktorí olízali každé hovno, len aby ostali pri koryte, z ktorého sa sem-tam vysypalo aj dačo chutné. 
 Určite, liberálna pravica bude mať s konzervatívnou pravicou problémy, ale ide o ľudí, ktorí majú jasnú víziu a kde je vôľa pre dobrú vec, tam je aj cesta pre dohody. Fico však má skúsenosti len s tým, že kde je pažravosť po kradnutí, tam nie je priestor pre suseda. 
 Určite, bude to divočina, ale čo nič nestojí, za nič nestojí. 
 Rovnako ako Fico, ani Belousovová si nechcú /a možno ani nedokážu/ uvedomiť, že nasledujúca vláda nevznikne preto, aby ťahala krajinu svojím smerom, ale preto, aby napravila chyby a svinstvá terajšej diktatúry. A na tých sa zhodnú bez rozdielu všetci, ktorím neamputovali pamäť a zdravý rozum. 
   

