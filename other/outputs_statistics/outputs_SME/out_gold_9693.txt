

 Každý z nás si musí prejsť niečím, každý sa popáli, poplače si, trpí, prežije etapu, ktorá mu vždy zvraští tvár, a slzy mu buď zalejú oči a líca, alebo ten človek plače vo svojom vnútri...A to som si myslela, že aj ten môj osud je dosť trpký, ale pri týchto osobách viem, že nie, kráčam vpred, len ten súcit, ktorý k nim vnímam, je ľudský, pravý, a z určitej miery obdivuhodný....a v neposlednom rade úprimný.... 
   
 Absolútne si neviem predstaviť, že by som prišla o rodinu, rodičov, mamu, otca, brata, alebo budúceho partnera, zaťa...že by som niekedy trpela hladom, nedostatkom lásky...tieto „ potrebnosti“ sú základom mojej existencie a veľmi obdivujem ľudí, ktorí takýmto niečím prešli, vyplakali more sĺz, vo vnútri cítili nesmiernu bolesť a pociťovali strašnú stratu, ale predsa, pozviechali sa, a dnes sa dokážu smiať, žiť, rozprávať, rozdávať rady, nádej, dokážu ťa chápať, podporiť...dokážu Ti v dobrom poradiť, aj keď zdvíhajú ukazovák... 
   
 Saška, Teba, a aj Helenku mám nesmierne rada, a veľmi ma teší, že ma ten môj osud zahnal aj s Ninkou práve na našu pôdu detského ihriska, kde sme ako rodina...Vám obom prajem, už len navždy spokojnú tvár, úsmev, šťastie....Radosť z detí a zo života...a taktiež som rada, že istým spôsobom ja, tzv Helena strelená, vám zlepším občas náladu, mojími „ drístami“ . 
   
 Miško....Miško je Mickey, / Mikí/, mňa osobne si nesmierne zaujal duševným pokojom, život Ti veľa vzal, ale následne Ti veľa dal...Máš v sebe to, čo mnohým starším bude vždy chýbať...a napriek svojej mladosti, si nevšedne krásny vo vnútri. ( od teraz bude už len dobre, ver mi ) J na oplátku, aj ja Tebe vždy rada pomôžem, aby si sa smial plným dúškom, ako aj ja J a ako sa zahrá osud, k nám, to už nechajme na ten osud... 

