

 
 
   
 
    
 Narodil som sa v júni 1992 s diagnózou Aplasio radii congenita, t.j. s chýbajúcou radiálnou kosťou v predlakti pravej ruky. Bola nedeľa, pravé poludnie, vonku všetko kvitlo a voňalo. Ortopéd, ktorý prišiel na konzílium hneď po pôrode, mi amputoval palec na pravej ruke, ktorý bol nesprávnou polohou ochabnutý. Bez röntgenu a neurologického vyšetrenia mi svojím postupom spôsobil ďalšiu traumu a vážnu diagnózu – Aplasio policis – t.j. chýbajúci palec.  
 
 A tak sa začal život môj a mojej mamy, život ťažký, náročný a hlavne iný, ako majú moji rovesníci. Som 15-ročný chlapec s chýbajúcou radiálnou kosťou v predlakti pravej ruky a chýbajúcim palcom. Mám len ulnárnu kosť a štyri prsty.  
 V Prešove sa ma nechcel ujať žiadny lekár, nemali skúsenosti s podobným prípadom. Na odporúčanie sa ma ujal detský ortopéd v pražskej nemocnici Motole, kde som ako dvojmesačný podstúpil diagnostické vyšetrenie. Mama sa tak presne dozvedela, čo mi je a ako postupovať ďalej. Prvú operáciu stanovili na jún 1993, keď dovŕšim rok života.  
 1.1.1993 sa naše republiky rozdelili a moju operáciu vyčíslili na šesťmiestnu sumu. Bolo potrebné nájsť iné riešenie. 
 Po mnohých problémoch sa ma ujal MUDr. L. Košťál, známy plastický chirurg a odborník na ruku v Bratislave. V júni 1993 som napokon predsa podstúpil prvú náročnú operáciu – centralizáciu - Reinsekciu upona extenz. carpi ulnaris. Dovtedy bola moja ruka ako hokejka, zápästie bolo vytočené skoro v pravom uhle. Predlaktie mi vyrovnali a ja som začal novú etapu života, iný rozmer. Cvičili so mnou stále, aj dvakrát týždenne jazdili do Bratislavy na preväzy a kontroly. Iné deti sa lezením, pridržiavaním o stenu a nábytok učili chodiť, ja som však tieto fázy musel vynechať a naučiť sa chodiť za ruku so svojimi rodičmi.  
 V apríli 1994 mi primár Košťál urobil na tú dobu unikátnu operáciu, nahradil mi palec ukazovákom, teda urobil mi úchop. Bol som prvý v strednej Európe. Lekár sa snažil napraviť aspoň čiastočne chybu prešovského ortopéda, ktorý mi amputoval palec. Bola to dlhá a náročná operácia – opponen plastica sec. Buck – Gramcko, o ktorej bol natočený film a vyšli rôzne odborné články. Pooperačný priebeh i rehabilitácia boli veľmi náročné. Celkovo som bol s mamou v Bratislave na kontrole 38 krát!  
 To všetko si našťastie nepamätám, ale som vďačný svojej mame, že to všetko so mnou podstúpila a nikdy sa nevzdala. 
 Zdalo sa, že mi nič ďalšie nehrozí, ale pri mojej diagnóze sa predlaktie správa ako chce a rastom organizmu má tendenciu vracať sa do pôvodnej „hokejkovej“ polohy. Tak sa aj po čase stalo, i keď oveľa miernejšie. Ja som  
 vyrástol, predlaktie PHK vôbec. Predpoveď prim. Košťála, že bude rásť a ruka bude taká dlhá ako ľavá, sa nanešťastie nepotvrdila.  
 Ohyb v zápästí bolo potrebné opäť riešiť. Vhodného lekára, výborného špecialistu na ruku, našla opäť mama, a to sústavným študovaním mojej diagnózy a pátraním po nových postupoch v medicíne. Lekár je na oddelení plastickej, rekonštrukčnej a estetickej chirurgie FNL Pasteura v Košiciach. Volá sa MUDr. T. Kluka, PhD. Je to odborník, ktorý pracuje nielen s mäkkým tkanivom, ale aj kosťami a kostičkami v ruke.  
 Prijal ma a v roku 2001, keď som mal deväť rokov, mi zápästie opäť centralizoval a urobil osteotómiu ulnae. Operácia bola úspešná, výsledok výborný, dôveru k lekárovi sme získali obaja, ja aj mama.  
 Odvtedy som na oddelení v Košiciach častý hosť. Po dlhom zvažovaní a príprave som v šk. roku 2005/2006 podstúpil distrakciu (predĺžovanie predlaktia). Mali sme nádej, novú, že sa moja pravá ruka predĺži.  
 V septembri 2005 mi MUDr. Kluka preťal ulnárnu kosť a nasadil distraktor. Do apríla 2006 som si sám doma naťahoval mäkké časti ruky, podľa vopred stanovených pravidiel – 1 až 3 otáčky denne.  
 Tak ako všetko, čo súvisí s predlaktím mojej ruky, nezaobišlo sa to bez komplikácií. Celkove v procese distrakcie som podstúpil 4 operácie. Prežil som veľa bolesti, trápenia a úskalí. Často mne, i mame, tiekli slzy, ale s nasadením všetkých ľudských síl sme to prekonali. Predlaktie sa predĺžilo o 9,8 cm, čo je pri mojej diagnóze dosť.  
 Bol som šťastný, ale krátko. V mieste zrastu kosti sa mi vytvoril paklb. Rok po vybratí distraktora som nosil termoplastickú a termodynamickú dlahu, vyrobenú na mieru.  
 V septembri 2007 som podstúpil ďalšiu, zatiaľ poslednú, ôsmu operáciu. Bol mi odstránený paklb – resectio pseudoarthrosis oseosynthesis cum Kirschneri 4x. Do januára 2008, kým sa kosť zrástla, som mal v škole opäť individuálny plán, nakoľko mi v predlaktí trčali 4 drôty (klince), ktoré držali zrastajúcu sa kosť. Predlaktie je teraz rovné, ruka dlhšia! Mám opäť termoplastickú dlahu, budem rehabilitovať, ako vždy pôjdem do kúpeľov. Je to súčasť môjho života, ktorý nie je vždy veselý, ale ani vždy smutný. 
 Život ma naučil, že každá bolesť, každé trápenie, skončí. Že ma posilní a dodá odvahu bojovať ďalej. Veď okrem iného som pravák, to všetko som sa musel naučiť ľavou rukou. Som bojovník a nevzdám sa. Medicína sa rozvíja, sú nové možnosti, nové spôsoby liečenia. 
 Aj mňa ešte nejaká korekcia čaká. Ak sa rozhodnem, podstúpim ju. Dnes to ešte neplánujem.  
 Žijem plnohodnotný život s niektorými obmedzeniami. Nemyslím, že som menej šťastný než ostatní. Mám svoje hodnoty a veľké ciele. Idem za nimi pomaly, ale isto. 

