

 O DENNÍKU SME 
 A JEHO REDAKTOROVI 
 "Sionistické plátky nečítam. S hovnami sa nebavím."	SME 22. februára 1994   
 O SPÔSOBE SVOJHO VLÁDNUTIA 
 "Keď je niekto vo väčšine a je rozhodnutý hlasovať, môžete si navrhovať, s prepáčením, čo chcete, pretože rozhodovať budeme my, čo vládneme."	V NR SR 22. decembra 1994   
 O SEPAROVANÍ 
 V DOBROM SLOVA ZMYSLE 
 "My vychádzame Cigánom v ústrety. Sú to deti prírody a my im chceme vrátiť priestor na radovánky, úplne odlišné od našich. Aby mali pocit šťastia a prírody, na ktorú boli zvyknutí v časoch, keď kočovali. Nechceme ich nijakým spôsobom diskriminovať. Naopak, budú separovaní v dobrom slova zmysle."	NOVÝ ČAS 16. decembra 1996   
 O LE PENOVI AKO VODCOVI 
 "Som veľmi rád, že je medzi nami podľa môjho názoru svojím významom najlepší národniar a patriot Európy, predseda Národného frontu Jean-Marie Le Pen... Nech žije vodca európskych národných síl!" 
 Mestský úrad v Žiline 19. septembra 1997   
 O TANKOCH A BUDAPEŠTI 
 "My pôjdeme do tankoch a pôjdeme a zrovnáme Budapešť!" 
 Míting v Kysuckom N. Meste  5. marca 1999 

