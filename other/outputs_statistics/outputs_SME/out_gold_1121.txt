

 
Rok 2008 bol pre mňa v mnohých veciach prelomový. V mojom živote sa stali mnohé veci a dve z nich zapríčinili vznik tohto článku, alebo skôr fotoreportáže. Prvou z týchto dvoch vecí je, že som v máji konečne dostal nový foťák. Nie je to žiaden špičkový prístroj, vlastne to nie je ani len zrkadlovka, ale aj tak sa z neho teším. Druhá vec je, že som po dlhých rokoch opäť začal chodiť na túry do Vysokých Tatier. A teraz by som sa s vami rád podelil o niektoré z mojich fotiek. 
 
 
 
 
 
Na úvod pár nezaradených fotiek. Ostatné už budú zaradené po jednotlivých túrach.

 


 


 


 

Soliskový hrebeň a hrebeň bášt pri západe slnka. Fotené niekedy v júni z okna mojej izby (práve preto je na väčšej časti fotky obloha, nechcel som aby bolo vidno bytovky, ktoré sú oproti :D).

 


 


 


 

Líštička v lesíku na magistrále medzi Hrebienkom a odbočkou na Slavkovský štít. Bola dosť prítulná až mi to začínalo byť podozrivé. Aj kamarátku mala, ale spoločné fotky nejako nevyšli. =)

 


 


 


 

Tatry v júly, tesne po západe slnka, opäť fotené z mojej izby. Na fotke sú vrcholy Gerlachovského štítu, Bradavice a Slavkovského štítu.
 


 


 


 

Nasleduje séria snímok zo Studenovodských vodopádov, fotené koncom júla.

 


 



 


 



 


 


 
 
 
 


 


 


 

Pár snímok z výstupu na Jahňací štít 22.augusta:

 


 



 


 

Pekná kvetinka, ale vôbec netuším ako sa volá, pretože do rastliniek sa veľmi nevyznám. V pozadí sa týči Lomnický štít v strede a napravo Veľká Svišťovka.

 


 



 


 

Bielovodský potok :) Na fotku sa ale vopchal aj môj tieň :( Snažil som sa ho odstrčiť, ale nedal sa :D

 


 



 


 

Idilická krajinka fotená po ceste na Brnčalku. Na fotke su doliny (zľava): Veľká Zmrzlá dolina, Malá Zmrzlá dolina, Červená dolinka a štíty (zľava):Čierny štít (2434 mnm), Jastrabia veža (2137,3 mnm) (vysunutá dopredu), Kolový štít (2418,1 mnm). Hore na oblohe dokonca "svieti" mesiačik :)

 


 



 


 

Zrkadliaca sa hladina zeleného plesa.

 


 



 


 

Kvetinky síce veľmi nepoznám, ale Horec ešte rozoznám. Aspoň teda dúfam, že to je Horec :D

 


 



 


 

Zelené pleso a chata pri zelenom plese (bývalá Brnčalka).

 


 



 


 

Reťaze na "Kolový priechod"(také menšie sedielko po ceste na Jahňací štít).

 


 



 


 

Vrcholová fotka z Jahňacieho štítu. (Neni veľmi kvalitná, pretože som si nevšimol, že mám špinavý objektív). Nemá veľkú umeleckú hodnotu, ale mne sa páči, pretože je to moja prvá vrcholová fotka po dlhej dobe. Snímok urobil vtedy ešte neznámi turista (no o pár chvíľ už kamarát :) Milan, za čo mu chcem poďakovať.

 


 



 


 

Tak tu je, Jahňací štít (2229,6 mnm). Nie je veľmi vysoký, no cením si ho, pretože to bol prvý štít, na ktorý som vystúpil sám. Odfotiť som ho však mohol až po ceste dolu, lebo po ceste hore som ešte netušil, ktorý to vlastne je :D.
 
 

 
 

 
 

 
 

 
 

 
 
Fotky kamzíka (chudáčik mal zlomené parôžky), ktorého som stretol pri zostupe.
 
 

 
 
Záverečná fotka z túry na Jahňací štít. Neviem ako sa volá tento kvietok odborne, ale pre mňa je to Zvonček. :)
 
 
 
 
 
Fotky z prechodu Priečnim sedlom z 30. augusta.
 
 

 
 
Opäť kvetinka na úvod. A opäť Horec (dúfam).
 
 

 
 
Slnečné lúče prechádzajúce Prostredným hrotom (2441 mnm).
 
 

 
 
Malý vodopádik vytekajúci z Nižného Spišského plesa pri Téryho chate.
 
 

 
 
Samotné Priečne sedlo (2352,3 mnm). Pohľad z Malej Studenej doliny. Naľavo je Žltá veža 2385 mnm).
 
 

 
 
Môj samotný prechd cez priečne sedlo. Opäť fotka nevyniká "umeleckým dojmom", ale pre mňa osobne má zvláštnu hodnotu, keďže mi pripomína strach, ktorý som zažil pri prechode sedlom počas výchrice, ktorá ma tam zastihla. 
 
 

 
 
Veľký studený potok vo Veľkej Studenej doline. V hornej časti fotky vidno aj Popradskú kotlinu v opare.
 
 
 
 
 
Pokračujeme fotkami z túry na Východnú Vysokú a následného prechodu sedlom Prielom do Veľkej Studenej doliny.
 
 

 
 
Pohľad na Slavkovský štít (2452,4 mnm) z Tatranskej Polianky.
 
 

 
 
Opäť ružový kvietok, ktorého názov nepoznám. V pozadí Velický vodopád.
 
 

 
 
Velický vodopád a úbočie Kotlového štítu (2601,1 mnm).
 
 

 
 
Pekná rastlinka, ktorá je v mojej hlave nazývaná bodliak. :)
 
 

 
 
Odpočinok na brehu Dlhého plesa vo Velickej doline. 
 
 

 
 
Dlhé pleso s nádhernou farbou vody. V pozadí Velický štít (2318 mnm) a Poľský hrebeň (2199,6 mnm).
 
 
 
 
 
Tak takto to vyzerá, ak niekto zavesí foťák na klinec na Poľskom hrebeni. :)
 
 

 
 
Smerová tabuľa a samotná Východná Vysoká (2428,6 mnm). Na ľavo vidno aj sedlo Prielom (2290,4).
 
 

 
 
Vrcholová fotka z Východnej Vysokej. Veľmi rád by som Vám popísal aj štíty v pozadí, ale skutočne neviem rozoznať, ktorý je ktorý. Určite tam však sú Gánok (2458,8 mnm), Rumanový štít (2428 mnm), Zlobivá (2425,7 mnm), Vysoká (2560,2 mnm), Dračí štít (2523,1 mnm) a Ťažký štít (2520 mnm). Fotka bola fotená na samospúšť a tak aj vyzerá. :D
 
 

 
 
Na záver tejto túry opäť fotka Veľkého Studeného potoka, tentokrát však vyššie vo Veľkej Studenej doline. V pozadí Svišťový štít (2382,6 mnm).
 
 
 
 
 
Fotky z túry na Furkotský štít (2403,5 mnm) z 11.októbra.
 
 

 
 
Východ slnka fotený z električky, niekde na zákrute za Vyšnými Hágami.
 
 

 
 
Jesenná atmosféra. Fotené zo Štrbského plesa. Na fotke sú (pravdepobone, pri týchto štítoch si nikdy nie som istý, ktorý je ktorý) vrcholy v strede: (z ľava) Rysy (2503 mnm), Dračí štít (2523,1),  Vysoká (2560,2 mnm) a iné, nižšie vrcholy po krajoch. Ruku do ohňa by som ale za to nedal, ako som už napísal, nie som si istý a ak som názvy uvideol nesprávne, vopred sa ospravedlňujem.
 
 

 
 
Pohľad na inverzné počasie v Popradskej kotline, po ceste k vodopádu Skok.
 
 

 
 
Vodopád Skok.
 
 

 
 
Slnečné lúče prenikajúce cez hrebeň bášt, inverzné počasie v kotline a Nízke Tatry týčiace sa nad oblaky.
 
 

 
 
Štrbský štít (2381,4 mnm) fotený od Plesa nad Skokom.
 
 

 
 
Pleso nad Skokom, mraky nad kotlinou a Nízke Tatry v pozadí.
 
 

 
 
Vrcholová fotka z Furkotského štítu (2403,5mnm). Za mnou Hrubý vrch (2428,3 mnm), naľavo odomňa hrebene Západných Tatier. Táto fotka nebola odfotená mnou, ale chalanom, ktorého som stretol na vrchole, za čo mu ďakujem.
 
 

 
 
A na záver jesenné stromy v popredí, zasnežené vrcholky v pozadí.
 
 
 
 
 
Výstup na Slavkovský štít 18.októbra.
 
 

 
 
Východ slnka nad Starým Smokovcom. Keď vidím ten "les" občas mi ostane dosť smutno.
 
 

 
 
Ranné, jesenné slniečko predierajúce sa cez "pololes". Fotené po ceste na Hrebienok.
 
 

 
 
Čerstvo osvetlené vrcholky Prostredného hrotu (2441 mnm) a  Lomnického štítu (2633,8 mnm). Prosím ospravedlnte moju zlú kompozíciu, ale snažil som sa, aby na snímke nebolo vidno "pouličné" lampy Hrebienka. =:o)
 
 
 
 
 
Slavkovský štít (2452,4 mnm) odfotený zo "Štrbavého hrebeňa".
 
 

 
 
Kamzík v oblaku. Zimná srsť je už takmer úplne nahodená. ;)
 
 

 
 
Zaujímavý mrak a Tatranské vrcholy. Presne viem popísať len niektoré a nerád by som zavádzal. Najblžšie k objektívu je však s určitosťou Prostredný hrot (2441 mnm) a najvyššý je zase Lomnický štít (2633,8 mnm).
 
 

 
 
Lomnický (2633,8 mnm) a Kežmarský štít (2558,4 mnm), ale tiež mraky v oboch Studených dolinách.
 
 

 
 
Pravá tatranská pohodička, už takmer na vrchole Slavkovského štítu. Túto fotku milujem pre nádherne modré nebo a pre jeden z najkrajších pocitov na svete, ktorý mi pripomína. Odfotené kamarátom Mišom, ktorý ma pri tomto výstupe sprevádzal.
 
 

 
 
Ja spolu s kamarátom Mišom na vrchole Slavkovského. Na pravo odomňa sa týči Ľadový štít (2627,3 mnm). Snímok urobila milá pani, ktorú sme stretli na vrchole. 
 
 
Fotky z dvojvýstupu na Kôprovský štít (2367,3 mnm) a Rysy (2503 mnm) z 16.novembra.
 
 

 
 
Východ slnka nad oblaky v Popradskej kotline. Ospravedlňujem sa za zníženú kvalitu obrazu, ale fotka bola urobená z idúcej električky.
 
 

 
 
Najväčšie a najhlbšie pleso na Slovenskej strane Tatier - Veľké hincovo pleso. Nad ním Mengusovské sedlo (2307 mnm) a Hincova veža (2372 mnm). 
 
 

 
 
Opäť vrcholová salámička, pohodička. :) Nejdem sa tváriť, že viem popísať  vrcholy, pretože neviem.
 
 

 
 
Vrcholová fotka z Kôprovského štítu (2367,3 mnm) fotená na samospúšť. V pozadí na ľavo je Kriváň (2493,7 mnm), na pravo Západné Tatry.
 
 

 
 
Reťaze po ceste na Rysy.
 
 

 
 
Fotka, ktorá mi tak trošku pripomína Tibet. :D Inak sú to ale vlajočky pred Chatou pod Rysmi (2250 mnm) a Ťažký štít (2520 mnm).
 
 

 
 
Vrchol Rysov (2503,0 mnm), najvyššý bod Poľska. Fotku kazí podľa môjho názoru odporný betónový stĺp, ktorý tam nemá čo robiť. Opäť je na obrázku inverzné počasie, teda oblaky v kotline a nádherný slnečný deň nad nimi.
 
 
 
 
 
A záverečné fotky zo zimného výletu do Veľkej Studenej doliny z 20.decembra.
 
 

 
 
Veľký Studený potok a kopy snehu.
 
 

 
 
Opačný záber dolu do doliny, ktorej spodná časť bola v oblakoch.
 
 

 
 
Večerný Starý Smokovec, Popradská kotlina, ale aj samotný vysvietený Poprad.
 
 
 
 
 
Tak a je to. Fotiek mám doma o veľa viac, ale snažil som sa vybrať také, ktoré by mohli zaujať. Ak sa vám niektoré z nich páčia, prosím napíšte do komentárov ktoré. Rovnako však prosím aby ste napísali aj tie, ktoré sú podľa vás slabé. Vopred ďakujem ;)
 
 
PS: Dúfam, že ste si nemysleli, že poznám všetky názvy štítov, plies atď. a už vôbec nie nadmorské výšky. Približne 30% percent názvov som vyčítal z mapy, rovnako ako všetky nadmorské výšky. =)
 

