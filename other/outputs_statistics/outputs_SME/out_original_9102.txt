
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Petra Bartošová
                                        &gt;
                Nezaradené
                     
                 Balíček júnových kariet 

        
            
                                    18.6.2010
            o
            7:10
                        |
            Karma článku:
                2.45
            |
            Prečítané 
            511-krát
                    
         
     
         
             

                 
                    Plynú júnové dni, ktorých ráz by sa mal vznášať v podobe voňavého, očakávaného a roztopašného leta. Vonkoncom to však tak nie je. V chladné a nezvyčajne sivé májové dni som si povedala, že to je asi len tohtoročnou daňou, ktorú musíme ,,zaplatiť“, aby sme mali počasím vydarenejší jún. Celkom som tomu verila a nemájový, málo láskavý máj ma tým pádom nezlomil a nezarmútil. Veď mysľou sa plavila myšlienka: ,,....o to krajší bude jún....“
                 

                     Čas sa prevalil do svojho tohtoročného šiesteho mesiaca, no leto stále nie je samozrejmosťou. Povyskočenie teplôt potešilo moju osobu, no zároveň som vnímala to klasické ľudské nespokojné mrmlanie. Ani teraz som sa však nepostavila na stranu ,,klimatických kabátnikov“, moja láska k slnku, letu, ku každému lúču ostáva neochvejnou a vernou. Ani tohtoročný jún so sebou nepriniesol takt istoty vysokých teplôt. Dni sú premiešané, leto má svoje karty rozhodené prekvapivo a nerovnomerne.       Ihličkovým krokom som kráčala siedmeho júna. Sako mohlo ostať v skrini, teplota od rána pulzovala okolím, to bol deň mojich bakalárskych štátnic, karta hrejivého a priam dusného dňa. Jednoduchým žabkovým krokom som mierila k škole i dvanásteho júna. Bola sobota, slniečko pálilo o čosi viac, teraz však výnimočne v mieste môjho trvalého bydliska. Škola ku ktorej som plávala nebola mojou Almou Mater, bola mi úplne cudzou, nepoznanou, teraz však nadobudla úlohu akéhosi sprostredkovateľa – okrsok, volebná miestnosť...:)))       Ja prvovolič - môj ,,prvý hlas“. Pred štyrmi rokmi som v období volieb nemala dovŕšených osemnásť rokov, vtedy som si neuvedomovala ten diametrálny rozdiel môjho života, spôsobený časom. Obdobie volieb 2010 je časom, kedy je plnoletosť už zabudnutou, klasickou samozrejmosťou. Môj životopis je obohatený o nenápadný titul, štatút študenta je nateraz stratený, ISIC je už neplatný, jeho výhody pominuli a cestujem bez zliav. Vyhliadky budúcnosti majú neisté kontúry, môj odbor je kvôli problémom s garantom zrušený, modlím sa za prijatie na druhý stupeň štúdia na inej univerzite, no zároveň sa prehrabávam v ponukách práce.       Znova je sivo, upršane, plačlivo, prevládajú chladné karty. No, aspoň, že vo volebný deň bolo zažaté, deň v znamení prenádhernej, slnečnej, hrejivej karty.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Bartošová 
                                        
                                            Slnko, kde si sa stratilo???...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Bartošová 
                                        
                                            Len s(l)ama...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Bartošová 
                                        
                                            Elegantná dáma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Bartošová 
                                        
                                            Pomedzi naše prsty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Bartošová 
                                        
                                            Svet milujúcich kvetín
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Petra Bartošová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Petra Bartošová
            
         
        petrabartosova.blog.sme.sk (rss)
         
                                     
     
        Mojou existenciou sa mnohé potvrdzuje, mojimi reakciami mnohé vyvracia, mojimi vlastnosťami mnohé komplikuje, ale môj úsmev mnohé zjednoduší...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    472
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Slnko, kde si sa stratilo???...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




