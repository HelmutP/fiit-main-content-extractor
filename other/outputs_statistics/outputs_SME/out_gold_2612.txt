

 A tak som vysvetľovala, že stromy sú ich, ale zmysel a význam majú len vtedy, keď ostanú tam, kde sú doma; v lese, v horách. 
 Postupne som kúpila ďalšie stromy  pre najmladšieho, pre synovca Miška, ako dar pre môjho otca k narodeninám... 
 Sledujem Vlkov  a najmä Jura Lukáča už dlho. V niečom s ním nesúhlasím,  v niečom mu závidím a v niečom ma štve. V niečom ho obdivujem a v niečom by som sa mu chcela podobať, aj keď nikdy nebudem... 
 Obdivujem jeho schopnosť exaktne naformulovať víziu záchrany slovenských lesov, strategicky myslieť a predvídať, čo sa stane. Cením si ľahké pero, ktorým približuje  svoje túlačky lesom a vnímam jeho ťažké srdce na tých, ktorí nie sú schopní pochopiť, že aspoň malú časť prírody treba nechať na ňu samú. Že národné parky sú naším národným dedičstvom a nie národným tunelom pre lesákov, niektoré finančné skupiny alebo tých, ktorí svoj cit k vlasti deklarujú cez nástenky a  zákony. 
 Vlci sa osobne angažovali pri záchrane Tichej a Kôprovej doliny v Tatrách, v súdnych sporoch s nelegálne sa správajúcimi úradmi, pri ochrane národných parkov pred práškovaním jedovatými  látkami. 
 Keď Vlci viedli 52 súdnych sporov proti Ministerstvu životného prostredia, poslala som peniaze pre ich právničku. Keď vyše 200 žalôb, ktoré podali, viedlo k tomu, že Najvyšší súd si začal vyjasňovať s Európskym súdom stanovisko k Aarhuskému dohovoru (dohovor, ktorý - zjednodušene - zabezpečuje, že občania a organizácie sa môžu vyjadriť k veciam, o ktorých rozhodujú politici), opäť som aspoň trošku prispela. 
 Dnes nebudem posielať peniaze. Sadnem si a napíšem list. A ak budem v nedeľu 14. marca 2010 v Bratislave, prídem o tretej na Námestie SNP, kde sa začne verejné stretnutie proti navrhovanej  zonácii, podľa mňa skôr útoku na náš prvý národný park Vysoké Tatry. 
 Je mnoho vecí, ktoré už za nás  Vlci urobili. 
 Je čas, aby sme ich začali robiť sami. 
   
 Foto v článku: National Geographic ; internet 

