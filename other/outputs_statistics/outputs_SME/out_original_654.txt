
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Marušic
                                        &gt;
                Tunelovanie
                     
                 Mission Breakthrough alebo ako vystrielať tunel 

        
            
                                    24.6.2008
            o
            8:30
                        |
            Karma článku:
                10.93
            |
            Prečítané 
            13807-krát
                    
         
     
         
             

                 
                    Na razenie tunelov existuje množstvo metód. Razenie pomocou trhavín je jeden z najstarších spôsobov budovania podzemných stavieb a v dejinách bolo vyvinutých veľa postupov, ako pomocou nich tunel realizovať. V dnešnej dobe sa v našich zemepisných šírkach pri trhavinách používa takmer výlučne tzv. nová rakúska tunelovacia metóda, ktorú Taliani s Nemcami nazývajú technológia striekaného betónu a prisvojujú si ju tiež. Okrem toho sú známe napr. nórska tunelovacia metóda, vhodná ale do nerozpukaných nórskych hornín, japonská tunelovacia metóda, alebo z novších metódy Perforex, Adeco a ich modifikácie.
                 

                  Základné zásady novej rakúskej tunelovacej metódy (NRTM) sformuloval v roku 1948 profesor Ladislaus von Rabcewitz, trvalo však až do roku 1962, kým sa po rokoch skúmania odhodlal na konferencii v Salzburgu túto metódu verejne prezentovať. Jej základnou myšlienkou je pri budovaní využiť fakt, že horninový masív v okolí tunela je schopný stabilizovať samotný tunel podobne, ako pri stavaní hradov z piesku dokážete do hory piesku vydlabať dieru. Preto považoval za podstatné urobiť klenutý alebo oválny plynulý prierez, lebo ten tie sily od nadložia vo svojom okolí najlepšie prenesie, kdežto rohy sú najexponovanejšie miesta a prvé trhliny vznikajú vždy tam. Koniec-koncov, toto vedia stavitelia už od čias starovekého Ríma.                   Základ je zabezpečiť stabilný výrub pred zabudovaním definitívneho ostenia. Cieľom nie je nepripustiť žiadne deformácie, naopak, pokojne nechať práve vyťaženú dieru, nech sa pomaly zmenšuje.     Už koncom devatnásteho storočia sa totiž prišlo na to, že tunelovanie by nemalo byť o tom, ako vzdorovať obrovským tlakom hornín, ale ako sa tým obrovským tlakom vyhnúť. Pokiaľ pripustíte deformáciu, dôjde k redistribúcii napätí v oblasti nad vaším výrubom, kde vznikne tzv. horninová klenba - oblasť, kde samotný masív prenesie napätia od nadložia do strán. Na druhej strane, pokiaľ by ste čakali veľmi dlho, v rozpukanej hornine by vám začali vypadávať bloky skál zhora do výrubu a on by sa vám zavrel. Kumšt je teda v tom, kedy tú dieru správne podoprieť.           No ale zanechajme teóriu a poďme k samotnému postupu. Najprv treba vyvŕtať otvory pre nálože. To sa robí podľa nejakej vrtnej schémy. Vrtná schéma je taká alchýmia, že do nej poriadne nevidia ani tí, ktorí ju navrhujú. Počet, rozmiestnenie vrtov, ich dĺžka a sklon, veľkosť   náloží, ich časovanie závisí od takého množstva faktorov, že ideálna schéma je pojem tak  hypotetický, že je pomaly z krajiny snov, hoci každý strelmajster bude tvrdiť, že tá jeho. Zvláštnosťou je, že hoci v ťažšej geológii treba viac trhavín, keď si  ale zoberiete ich spotrebu v závislosti od meniacej sa geológie, jasne preukázateľnú súvislosť tam zvyčajne nezbadáte. To dosiahnete až keď začnete sledovať spotrebu jedného streľmajstra.                      Ako vidíte rozmiestnenie vrtov na čelbe nie je rovnomerné. V zásade sa dá povedať, že sa rozoznávajú vrty:     zálomové - bývajú v strede, najhustejšie pri sebe a najviac nabité. Odpaľujú sa ako prvé, aby vyrazili priestor v strede, kam sa môže odpáliť hornina zo strán.     prebierkové, alebo rozširovacie, ktoré sa odpaľujú po zálomových a rozpojujú horninu smerom do stredu   predobrysové a obrysové, ktoré bývajú najslabšie, aby sme nevyrazili zbytočne viac ako potrebujeme        Vŕtanie sa kedysi robilo ručne, v dnešnej dobe v tuneloch už výlučne strojmi s lafetami, ktoré sa veľmi často zamerajú a presne laserovo navádzajú na vŕtanie. Ručné vŕtačky sa používajú v štôlňach, kam by sa stroje nezmestili.                          Takto vyzerá odparkovaný stroj.                      Ako vidno, tvorcovia filmových smrtiacich robotov sa zrejme chodia inšpirovať do tunelov                          A tu už je stroj v plnom nasadení             Ako trhaviny sa používajú dva druhy. Buď klasické banské trhaviny alebo moderné viaczložkové. Klasické môžu byť sypké, poloplastické, alebo všeobecne známe a obľúbené plastické trhaviny. No a v nich sa nachádzajú také látky, ktoré všetci veľmi dobre poznáte - trinitrotoluén, dusičnan amónny, a rôzne trhacie želatiny na báze nitroesterov (nitroestery sú známejšie pod názvom dynamity). Veľkí priatelia životného prostredia, Švajčiari, ale zistili, že v rúbanine ostáva po nitroesteroch škodlivý spad, takže aj z ekologických dôvodov sa v dnešnej dobe ich používanie obmedzuje, hoci už ani zďaleka nie sú také citlivé ako kedysi. Väčšinu z nich môžete dokonca bezpečne zapáliť a oni vám normálne zhoria, vybuchujú iba pri iniciácii roznetkou. Moderné trhaviny sú tvorené dvoma alebo viacerými emulziami, ktoré sú výbušné až pri ich zmiešaní. Nemajú škodlivý vplyv na pracovníkov, vyznačujú sa vysokou manipulačnou bezpečnosťou, produkujú menej škodlivín, sú účinnejšie. Veľký boom zažili koncom deväťdesiatych rokov, bezprostredne po vynájdení a dnes sa v západnej Európe používajú prakticky iba tieto.                  Ružový valec je patróna sypkej trhaviny, Vľavo tie medené tyčinky sú rozbušky naplnené vysoko brizantnou trhavinou, ktoré aktivujú hlavnú nálož. Ružová šnúrka je moderná bleskovica - tenká rúrka naplnená vysoko brizantnou látkou, ktorá horí rýchlosťou 6000 m/s. Dúfam, že to neuvidí moja mama, lebo ma zabije, že jej také veci nosím domov ;-)       Po odstrele je nutné priestor dostatočne vyvetrať, čo môže trvať aj pol hodiny. Potom nastúpi na svoju robotu pásový tunelbager, ktorý vyčistí prierez do požadovaného tvaru (môže byť vybavený aj lopatou aj búracím kladivom) a prihrnie rúbaninu lopatovému nakladaču.                               Tu tunelbager práve razí núdzový výklenok                         V tieni oddychujúci tunelbager                            Lopatový nakladač naberá rozrušenú horninu                         Spolupráca nakladača a dampra                       Vyťažený materiál sa nakladá na obrovské dampre - nákladné autá schopné uniesť až viac než päťnásť ton.                        Krásavec zozadu                                        Krásavec spredu          Keď už to máme vyvezené, zabezpečíme steny primárnym ostením. To sa skladá vždy zo striekaného betónu, siete z betonárskej výstuže a v prípade potreby aj z oceľových oblúkov.                         Primárne ostenie tunela               Najskôr sa pribije sieť, umiestnia sa v správnej vzdialenosti oblúky a betón sa nastrieka. Tunelári sa ešte nezhodli, či je vhodnejšia mokrá zmes, kde sa strieka cez dýzu betón privezený v domiešavači, alebo suchá zmes, kde sa voda pridáva až tesne pred dýzou. Strieka sa zo vzdialenosti asi 1,5 m.                            Nákladné auto na mokrú betónovú zmes                     Ručné striekanie suchej zmesi        Pokiaľ sa jedná o horniny silne tlačivé, zabezpečuje sa výrub aj horninovými kotvami.                         Tu je vidno hlavu kotvy       Po zabudovaní primárneho ostenia je nutné sledovať deformácie - to je tiež jeden z pilierov novej rakúskej tunelovacej metódy. Ďalšie práce môžu pokračovať, až keď deformácie prestanú a masív sa znova nachádza v rovnovážnom stave.                     V žiari blesku sa odrážajú terčíky geodetických bodov, ktorými sa sledujú deformácie                      Za touto skrinkou je vrt, kde sú umiestnené prístroje na meranie tlakov v hornine za ostením         Pri veľkých tunelových prierezoch nie je technologicky možné raziť na celý profil, preto sa tento zvyčajne člení. Za normálnych okolností na vrchnú časť (kalotu), za ktorou sa v určitej vzdialenosti razí spodná časť (štrosa).                       Pred nami stena štrosy, hore už je vyrazená kalota.          Pokiaľ sú podmienky veľmi zlé, tak sa robia bočné predrážky, ktoré sa potom prepoja. Tomuto sa zvyčajne treba snažiť vyhnúť, lebo to nie je celkom v súlade s NRTM. Pre trhavinové razenie a vytvorenie horninovej klenby je ideálne, keď sa počas výstavby mení napätostný stav určitého bodu masívu iba raz. Keď prierez rozčleníme a vyťažíme na štyrikrát toto už nedodržíme a kvalita finálnej horninovej klenby už nie je taká, ako keby sme to robili na jeden šup.                   V obtiažnych geologických podmienkach sa najskôr spravia bočné predrážky a potom razí stred medzi nimi.        Častokrát je nutné aj rozšíriť prierez. Pri cestných tuneloch pre výstavbu zálivov, poprípade pri mestských tuneloch pre pripojenia iných komunikácií. Železničné tunely sa rozširujú iba výnimočne, a to v miestach, kde sa plánuje podzemné napojenie inej trate.                        Ďalším prvkom rozširujúci prierez sú záchranné výklenky v cestných tuneloch (v železničných sa od nich upúšťa, lebo v rámci prejazdného prierezu sa počíta aj s únikovým chodníkom). Toto ale nie je žiaden výrazný zásah do horninového prostredia. Niektorí uprednostňujú ich čo najskoršie vybudovanie, iní ich robia až vo veľkej vzdialenosti za čelbou, v zásade je to ale prakticky jedno.                     Stopy po tunelbagri, ktorý pripravil miesto na výklenok           Keď je hotové primárne ostenie začne sa ukladať izolácia. Predpokladá sa, že primárne ostenie sa časom rozpadne a stane sa ďaleko viac vodopriepustným ako bolo na začiatku. Izolácia zabráni priesakom vody do tunela a po nej voda stečie do drenáže v spodnej časti. Čiže takto zhotovený tunel pôsobí ako drén.           Pohľad na natiahnutú izoláciu                Drenáž v spodnej časti         Izolácia sa pripevní klincami o primárne ostenie, zvyčajne býva z fólií, ktoré sa zvárajú. O ňu sa tiež pripevní výstuž definitívneho ostenia.                     Vystuženie sekundárneho ostenia         Definitívne ostenie sa betónuje pomocou betónovacích vozíkov na koľajniciach.                 Betónovací vozík pred portálom                 Zvnútra sa cez kruhové otvory napĺňa betón. Postupne sa tie otvory zatvárajú a betónuje sa to z vyšších polôh. Je to nutné kvôli správnej konzistencii, pokiaľ by sa to betónovalo zhora od začiatku, než by ten betón stiekol, bol by značne nehomogénny. Zvyčajne sa to aj vibruje, aj keď v poslednej dobe sa používa aj tzv. samozhutňovací betón.                   Vozík v plnom nasadení         Ak sa jedná o dlhý cestný tunel, je nutné urobiť medzistrop pre vetrací systém.                     Debnenie medzistropu zospodu                          Betónovanie medzistropu zhora          V miestach výklenkov sa na debnenie dajú nástavce, ktoré ich vytvarujú. V miestach odstavných zálivov sa tiež môžu dať nadstavce alebo ak je ich v tuneli veľa, tak sa vyrobí samostatný vozík iba na ne.                         Čelo vozíka sa musí zašalovať drevom                   Jeden vozeň býva dlhý do 12 metrov, nech sa môže vyrektifikovať aj do zákrut. Niekedy sa postupuje o jeden krok s tým, že než zatvrdne, vozík sa posunie a pripraví na ďalšie betónovanie o kúsok ďalej. Alebo jednoducho idú dva vozíky za sebou s tým, že prvý betónuje každý druhý úsek a druhý vypĺňa medzery.       No ale pod zemou sme boli už dlho, takže občas sa treba pozrieť aj ako je vonku krásne. Ďakujem za pozornosť.                                           Poznámka: Tá výbušnina na fotke je v skutočnosti kus dreva v ružovom papieri s nápisom Permonex, rozbušky sú síce pôvodne pravé, ale sú prevŕtané a nemajú náplň. Rovnako bleskovica je iba ružová rúrka bez tej brizantnej náplne. Navyše je to všetko zreteľne označené ako atrapa .      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Zoči-voči policajnému zboru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Antigorilí volebný systém
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Vorderes Sonnwendjoch a Sagzahn
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            O zemetraseniach bez mýtov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Marušic 
                                        
                                            Ževraj ajťák je najlepší pre ženu? Ťažko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Marušic
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Marušic
            
         
        marusic.blog.sme.sk (rss)
         
                        VIP
                             
     
        Profesionálny tunelár
  "Slovo tunelář vymysleli bývalí kvazikomunisté jako součást své předvolební populistické kampaně."  Viktor Kožený  &lt;a href="http://blueboard.cz/anketa_0.php?id=571219"&gt;&lt;/a&gt;  Diskusia k ankete 

&lt;a href="http://www.blueboard.cz/shoutboard.php?hid=xhu0dnibsi42x66rek6cc9ivfspapi"&gt;ShoutBoard od BlueBoard.cz&lt;/a&gt;
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    197
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4420
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Prihodilo sa mi
                        
                     
                                     
                        
                            Tunelovanie
                        
                     
                                     
                        
                            Vodné diela
                        
                     
                                     
                        
                            Planéta Zem
                        
                     
                                     
                        
                            Historia est lux veritatis
                        
                     
                                     
                        
                            Slovenská sosajety (vážne)
                        
                     
                                     
                        
                            Odpočuté
                        
                     
                                     
                        
                            krížom-krážom
                        
                     
                                     
                        
                            64 polí
                        
                     
                                     
                        
                            Slopeme s monitorom
                        
                     
                                     
                        
                            Mosty
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Winston Churchill - Druhá světová válka
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sem som to nahustil
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tu som skoro furt a ničomu nerozumiem...
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




