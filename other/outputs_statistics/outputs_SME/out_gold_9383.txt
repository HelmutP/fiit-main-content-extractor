

 držím synka. 
 Práve krásne spinká 
 a vôbec nič netuší. 
   
 Nevie o tom, 
 že je z neho rarita. 
 Má aj otca, má aj mamu, 
 a predsa je sirota. 
   
 Sirôtka moja, 
 krehká, drobná, 
 dušu má ešte nebovú. 
 Voňavý lúč jesene 
 a iskrička v oku, 
 žuje už kôrku chlebovú. 
   
 Pane, prosím, zachráň ma, 
 ja nevládzem niesť svoj kríž. 
 A keď Ťa zavše o smrť prosím, 
 prosím, tie prosby nevyslyš. 
   
 Pre sirôtku moju o ocka prosím 
 a pre seba o múdrosť. 
 Bože, daj mi srdce múdre, 
 nech viem, čo správne je. 
 Nech si vraví kto chce čo chce 
 a nech sa čo chce udeje. 
   
 Pane, daj mi srdce veľké, 
 nech sa doň zmestí celý svet. 
 Nech je tam všetko pre sirôtku moju 
 a nech v ňom žiadnej zloby niet... 

