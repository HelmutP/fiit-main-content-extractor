
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniel Servanský
                                        &gt;
                Nezaradené
                     
                 Zvoľte si svoj "časopis" 

        
            
                                    27.6.2010
            o
            21:35
                        (upravené
                2.7.2010
                o
                6:50)
                        |
            Karma článku:
                4.78
            |
            Prečítané 
            503-krát
                    
         
     
         
             

                 
                    Poznám množstvo ľudí, ktorí radi čítajú časopisy. Množstvo informácií pod jednou obálkou v rukách, pohodlne si sadnete a listujete stránkami, aby ste našli zaujímavú informáciu, ktorá by vám spríjemnila chvíľu. Je ich naozaj neúrekom. Sú také, ktoré z princípu ani nevezmete do ruky, už len kvôli názvu, alebo zo zlej skúsenosti, keď ste si po ich prečítaní uvedomili, že ich obsah nestojí ani za deravý groš. Iné sú zase určené pre široký okruh čitateľov, a tak medzi množstvom údajov nájdete len malé množstvo takých, ktoré by stáli za prečítanie. Ďalšie ponúknu dobré články, no keď si uvedomíte, že veľká časť výtlačku pozostáva z reklám, stratíte chuť míňať na nich vaše úspory. Občas však udrie blesk z jasného neba a po zdolaní poslednej strany a zistení stručného obsahu budúceho čísla, dôjdete k záveru, že práve táto značka je pre vás ako ušitá. Dozviete sa z neho množstvo nových informácií, ktorými následne ohurujete svojich priateľov, nájdete tam aj skvelé fotografie a články o vašej práci, koníčkoch, snoch a tak ďalej, ktoré pre vás majú želaný prínos.
                 

                     Môj spolužiak Jozef, je veľký fanúšik kozmických rakiet, ktorý si často odbehne do múzea vesmírnej techniky alebo observatória, aby si obzrel historické modely družíc sputnik 1, sputnik 2, sputnik 3, kozmické lode Voschod, Sojuz, Progress a mnohé iné. Je jeden z mála ľudí, ktorí pre svoj koníček vedia obetovať všetok svoj voľný čas a ťažko nasporené peniaze. Pri jednej návšteve astronomického centra si na jednej zo stien, všimol reklamu na už dosť populárny a zaujímavý časopis o kozmických lodiach s názvom Vostok, a zo zaujímavosti sa rozhodol, že si v najbližšej trafike jeden zaobstará. Ešte toho dňa si ho naozaj kúpil, a treba uznať veľmi výhodnú cenu s ohľadom na počet jeho strán. Doma Vostok najprv listoval, potom začal čítať a nepostavil sa zo stoličky, kým ho nemal kompletne prečítaný spredu aj zozadu. Bolo to naozaj skvelé číslo, s obrovským množstvom zaujímavých článkov a fotografií, bez kadejakých reklám, na skvele vyzerajúcom kriedovom papieri. Pomocou tohto vydania sa okrem iného naučil napríklad, že Valerij Poljakov drží rekord 437 dní nepretržitého pobytu vo vesmíre, alebo o letoch na Mesiac, po ktorom si zaskákalo dokopy už 12 ľudí. Kúpil si tak ešte jeden alebo dve ďalšie vydania, po ktorých sa rozhodol pre možnosť predplatného.   Bolo to akurát obdobie, počas ktorého vyšli všetky známe mesačníky s neuveriteľnými zľavami na cenách predplatného. Mnohé ponúkali okrem výhodných cien, rôzne darčeky, súťaže o dovolenky v oblasti Bermúdskeho trojuholníka, ako hlavnou cenou a rôzne bonusy pre najvernejších čitateľov. Ten jeho ponúkal takisto skvelú súťaž, pričom hlavná cena bol výlet do vesmíru ruskou kozmickou loďou Sojuz TMA-10 a následný pol ročný liečebný pobyt v Medzinárodnej vesmírnej stanici ISS. Myšlienka na cestu do vesmíru ho každý deň zahriala pri srdci, a nie je tajomstvom, že značne ovplyvnila jeho rozhodnutie, zaplatiť predplatné. Predplatné bolo v tomto prípade mierne neštandardné, neplatilo sa za jeden, ale hneď za štyri roky, no čo nespravíte pri tak výhodnej cene a garancii skvelých článkov počas celého obdobia. Tak sa stalo, že každý deň v mesiaci mu prichádzal do schránky jeho obľúbený časopis o raketách, s výborne písanými článkami na kriedovom papieri, bez zbytočne veľkého množstva reklám, aké ste mohli nájsť v tých ostatných konkurenčných mesačníkoch. Čítal ho s radosťou, postupne si doma vyčlenil miesto kde si časopis Vostok odkladal, a na stenu si prilepil plagáty stanice ISS z exteriéru aj interiéru, aby sa po prípadnej výhre vedel na mieste výhry orientovať. Veru, až do dňa, keď prišiel k svojej poštovej schránke a vytiahol z nej svoje dlho očakávané vydanie časopisu o raketách. Nedočkavo ho otvoril na prvej strane, aby si pozrel obsah vydania, keď tu s hrôzou zistil, že kriedový papier, garantovaný na obdobie štyroch rokov, vydavateľ nápadne nahradil drapľavým papierom, nachádzajúcom sa v štádiu materiálu, po siedmom recyklovaní, s odôvodnením, že ide o šetrenie, spôsobené stúpajúcou cenou kužeľových kolíkov s vnútorným závitom, používaných v kozmickej lodi. Jozef tomu postupom času uveril, pretože v ďalších článkoch boli všetky ďalšie potrebné zmeny znižujúce kvalitu jeho predplateného časopisu Vostok, vierohodne vysvetlené a tak cez zaťaté zuby trpel degradáciu každého ďalšieho čísla, ktoré vytiahol zo svojej poštovej schránky. Trpel a zmieril sa aj so stále menším množstvom dôveryhodných a presných údajov, až kým neprišlo osudové februárové vydanie Vostoku, časopisu o kozmických haváriách a tajne financovaných vesmírnych letoch. V tomto čísle zistil Jozef a spolu s ním sto tisíce rôznorodých čitateľov krutosť reality. Na stránkach nenašli nič, čo by súviselo s obsahom prvých čísel, a okrem reklám na každej recyklovanej strane tu ostali iba pľuvance, ktoré im tam poslali šéfredaktori Vostoku so záverečným slovom redakcie: ,,Júnové číslo prinesie novinky ".       Možno si budete myslieť, že Jozef po tomto zážitku stratil dôveru vo vesmírne časopisy, ale opak je pravdou. Jozef žil vo vesmíre aj naďalej a nič ho v tom nemohlo zastaviť. V máji si totiž zapol svoj vlastnoručne zostavený počítač zo šrotoviska, a našiel si svoju novú lásku. Prostredníctvom sociálnej siete, sa dostal k stránke jedného internetového časopisu, kde našiel skvelé články a zaujímavosti, ktoré ešte vo svojom živote nikdy nevidel, a rozhodol sa. 12. júna si na štyri roky predplatil nový časopis o raketoplánoch a cestách za hranice našej galaxie, s názvom Space Shuttle. Prvé vydanie mu už niekoľko dní leží v poštovej schránke a on v izbe rozmýšľa, čo z internetového obsahu prejde do tlačeného vydania.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Servanský 
                                        
                                            Záručná lehota
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Servanský 
                                        
                                            Cesta k oblakom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Servanský 
                                        
                                            Hluční susedia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniel Servanský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniel Servanský
            
         
        servansky.blog.sme.sk (rss)
         
                                     
     
        To, čo tu všetci stvárame nemá chybu
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    820
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




