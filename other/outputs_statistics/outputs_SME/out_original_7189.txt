
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Ukropcová
                                        &gt;
                Nezaradené
                     
                 Rusko versus Česko 

        
            
                                    24.5.2010
            o
            16:05
                        |
            Karma článku:
                6.94
            |
            Prečítané 
            1117-krát
                    
         
     
         
             

                 
                       Česi prišli bez mien a odišli s menami. Iba dve hviezdne mená, ako Jágr a Vokoun, stačili českému reprezentačnému tímu nato, aby zdolali mašinériu zbornej. Ani celá súpiska zvučných mien nepriniesla rusom vysnívané  víťazstvo.  
                 

                     Prvá vlna nespokojnosti sa spustila ihneď po nevydarenej olympiáde, kedy Rusi stáli pred zlatou bránou do štvrťfinále, pred ktorou ich zastavilo práve slovenské mužstvo. Už vtedy si začali brúsiť zuby na zlatý kov v Nemecku. Favorizovaní Rusi vstupovali do zápasov suverénne. Aj keď im bolo uštedrených zopár ošuchov, do finále sa prebojovali s čistým štítom. Počas šampionátu nezaznamenali ani jednu prehru a všetky výhry dosiahli v riadnom hranom čase. Preto asi nikoho nezaskočilo Kovaľčukovo vyhlásenie, že si ide do Kolína po zlato.       Českí hokejisti si počíňali na tohto ročnom turnaji rovnako statočne, i keď pocítili ohrozenie na vlastnej koži hneď dvakrát. V základnej skupine podľahli Nórom a neuhrali ani súboj so Švajčiarmi v osemfinále. Po prvýkrát v histórii visela nad Českom hrozba, že sa neprebojujú medzi osem najlepších mužstiev sveta. Vtedy ešte na hokejový olymp určite nepomýšľali. Ale Česi ukázali svoju urputnú bojovnosť a po tom, čo prekonali najskôr Fínov a vzápätí Švédov, obe krajiny v samostatných nájazdoch, postúpili do finále. Či už s odretými alebo neodretými ušami, to už nikto nerieši. Celému svetu ukázali neoblomnú húževnatosť a vieru v seba samých.       Prvý gól, ktorý padol do ruskej bránky už po 20 sekundách otvorenia zápasu, môže kľudne vojsť do dejín ako Jágrov preplesk. Autorom gólu bol síce Klepiš, ale to by bez Jágrovho výpadového rýchleho naservírovania sotva bolo možné. Ohromenosť Rusov sa podpísala na ich nepremenené šance. Padli im dve žrďky a jeden neuznaný gól. Konečný stav 1:2  Rusi len veľmi ťažko rozdýchavali.       Rozplytvávať veci ako, na strane koho bola a nebola šťastena, si odpustím. Rusom jednoducho chýbalo to, čo našim západným susedom nebolo možné odoprieť. Na prvenstvo nestačí vždy suverénnosť hry či pevné odhodlanie. Treba mať i pokoru.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Ukropcová 
                                        
                                            Divé maky 93 / 190
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Ukropcová 
                                        
                                            Divé maky 92 / 190
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Ukropcová 
                                        
                                            Divé maky 90-91 / 190
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Ukropcová 
                                        
                                            Divé maky 88-89 / 190
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Ukropcová 
                                        
                                            Divé maky 84-87 / 190
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Ukropcová
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Ukropcová
            
         
        ukropcova.blog.sme.sk (rss)
         
                                     
     
        Veci dokáže zmeniť iba láska.. 



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    344
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Eric Clapton
                                     
                                                                             
                                            Christina Aguilera
                                     
                                                                             
                                            Lisa Fischer
                                     
                                                                             
                                            U2
                                     
                                                                             
                                            Bob Dylan
                                     
                                                                             
                                            Rolling Stones
                                     
                                                                             
                                            Amy Winehouse
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




