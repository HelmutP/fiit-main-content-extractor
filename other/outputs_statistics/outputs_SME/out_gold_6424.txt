

 Urobila som si v okolí malý prieskum - díva sa na ne dosť ľudí vo veku, no, mladosť preč, do dôchodku ďaleko. Obvykle to priznajú s malým zaváhaním a úsmevom, ale priznajú. 
 Prečo sa na ne rada dívam? Nuž, je to podobný pocit ako v detstve, keď som hltala Dobšinského rozprávky. A to sú teda pekné krváky! Čudujem sa, že som pri nich neutrpela nejakú traumu. Proti nim sú nemecké romantické filmy prechádzka ružovou záhradou (niekedy aj doslovne). 
 Poznám psychiatričku, ktorá sa po celodennej práci večer rada posadí ku knihe z červenej knižnice. Jej kritérium je: 
 "Len aby sa vzali!" 
 Chápem ju. Život býva dosť ťažký sám o sebe, tak prečo sa trochu neodreagovať? 
 Ja sa psychiatriou neživím, asi preto to mám posunuté od kníh smerom k filmom... 
 Lenže je tu háčik - zbavila som sa televízora. Úplne dobrovoľne a po zrelom uvážení, ale má to jednu nevýhodu - prišla som tým o nemecké romantické filmy. 
 Využívam teda občasné návštevy u rodičov, vždy tam stihnem aspoň jeden príbeh. Spoločne sledujeme, či vo filme bude presne ten istý útes, ktorý sme už videli vo všetkých ostatných príbehoch. Ale pozor: útes býva len u jednej autorky, už neviem ktorej. V ostatných nemeckých romantických filmoch sa útes nevyskytuje. 
 Raz bolo v programe pri jednom filme uvedené: kanadský romantický film. Natešene som si ho pustila. Ale bola som sklamaná, nemalo to ten šmrnc. Pokiaľ to presne nezodpovedá štruktúre 'stretnutie, jeden z nich bohatý, prekonanie prekážok, zásnuby alebo svatba', proste to nie je ono. 
   

