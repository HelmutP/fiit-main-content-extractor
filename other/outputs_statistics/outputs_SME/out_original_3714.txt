
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Szabo
                                        &gt;
                Právo a politika
                     
                 Kriminálnici k volebným urnám? 

        
            
                                    31.3.2010
            o
            23:40
                        (upravené
                1.4.2010
                o
                19:15)
                        |
            Karma článku:
                6.04
            |
            Prečítané 
            2131-krát
                    
         
     
         
             

                 
                    Nedávnou novelou volebných zákonov umožnil náš zákonodarný zbor v tohtoročných parlamentných voľbách rozhodovať spoločne s nami aj odsúdeným kriminálnikom. Popísalo sa toho mnoho, predovšetkým však z politického hľadiska a nikto nemal záujem sa zaoberať detailmi a úvahami o novele z právneho, či spoločenského alebo pragmatického?
                 

                 Táto téma sa odvíjala od skutočnosti, že na Slovensku bolo odsúdeným osobám vo výkone trestu odňatia slobody odopreté volebné právo. Naše volebné zákony priamo ustanovovali prekážku vo výkone práva voliť.  Súčasný stav je taký, že volebné právo je novelou priznané aj osobám vo výkone trestu odňatia slobody s výnimkou tých, ktorí spáchali obzvlášť závažný zločin. Obzvlášť závažný zločin je úmyselný trestný čin, za ktorý Trestný zákon ustanovuje trest odňatia slobody s dolnou hranicou trestnej sadzbu najmenej desať rokov. Trestné sadzby za jednotlivé trestné činy nájdete v 2. časti Trestného zákona (zákon č. 300/2005 Z. z.).  Článok som pre lepšiu prehľadnosť rozdelil do troch častí:   1. Skutočnosti, ktoré predchádzali schváleniu novely 2. Úvaha 3. Alternatívne riešenie     1. Skutočnosti, ktoré predchádzali schváleniu novely:  a) 6. október 2005 – Európsky súd pre ľudské práva: rozhodnutie Hirst proti Spojenému kráľovstvu Malo svoje počiatky na národnej úrovni v Spojenom kráľovstve. Navrhovateľ John Hirst bol v roku 1980 uznaným za vinného za spáchanie vraždy. Okrem iného bol uznaný na základe medicínskeho posudku za osobu so závažnou poruchou osobnosti, až do miery amorálnosti. Týmto bol pozbavený osobnej slobody na doživotie.  Representation of the People Act je britský právny akt z roku 1983, na základe ktorého bolo odňaté väzňom volebné právo. Hirst sa dovolával na súde (Divisional Court) porušenia jeho základných práv a nesúlad tohto aktu s Európskym dohovorom o ľudských právach. Hirstov žalobný návrh bol odmietnutý a sudca (Lord Justice) odôvodnil právny akt nasledovne:    „Páchané trestné činy, ktoré samé o sebe alebo spáchaním za akýchkoľvek priťažujúcich okolností vrátane osoby páchateľa s predošlým záznamom v trestnom registri, vyžadujú trest odňatia slobody, takýmto väzňom prepadá právo mať slovo pri určovaní spôsobu, akým sa má krajina spravovať, v rámci obdobia vo väzení. Trest zahŕňa viacero prvkov, než iba nútené zadržanie. Vyradenie zo spoločnosti znamená vyradenie z výsad spoločnosti, medzi ktoré patrí aj právo zvoliť si svojich zástupcov.“  Pokusy o odvolanie na národnej úrovni boli takisto zamietnuté. Vo veci sťažnosti Európsky súd pre ľudské práva rozhodol o porušení článku 3 Protokolu č. 1 Dohovoru (právo na slobodné voľby), že „strata volebného práva bola automatická, nevyplývala z rozhodnutia súdu, pričom posudzované obmedzenie sa vzťahovalo paušálne na všetkých väzňov bez ohľadu na dĺžku trestu, charakter alebo závažnosť spáchaného trestného činu a konkrétne okolnosti prípadu. Súd konštatoval, že takéto všeobecné a automatické obmedzenie považuje za vybočujúce z rámca voľnej úvahy, bez ohľadu na jej rozsah, a preto za nezlučiteľné s článkom Dohovoru.“  Pre Slovensko a iné členské štáty Rady Európy bol rozsudok veľmi významný, nakoľko 13 z nich, vrátane Slovenska, odopieralo väzňom volebné právo a v 12 štátoch ho určitým spôsobom obmedzovali.  b) 11. február 2009 - Rozhodnutie Ústavného súdu SR  Generálny prokurátor Dobroslav Trnka podal návrh na Ústavný súd na prelome rokov 2007/2008, v ktorom navrhoval zrušenie volebnej kaucie a argumentoval v prospech aktívneho, ale aj pasívneho volebného práva pre osoby vo výkone trestu odňatia slobody. Odvolával sa predovšetkým na články Ústavy SR a namietal nesúlad prekážky vo výkone volebného práva so základnými ústavnými a demokratickými zásadami. Zvláštne, že sa absolútne neodvolával na judikatúru Európskeho súdu pre ľudské práva a predovšetkým na významné rozhodnutie Hirst.  Nakoľko sme viazaní medzinárodnými zmluvami a zahraničné judikáty sa dotýkali rovnakého predmetu, súd bol de facto povinný vyhovieť a vyhlásiť ustanovenia zákonov za protiústavné, no iba v prípade aktívneho volebného práva a aj to iba vo vzťahu k voľbám do Národnej rady Slovenskej republiky a Európskeho parlamentu.  Rozhodol, že „v demokratickom a právnom štáte je neprípustné, aby bola akokoľvek veľká skupina občanov alebo čo i len jeden občan bez závažného verejného záujmu vylúčená z volieb a zároveň jej bolo odopreté na určitú dobu vykonávať jedno z jej ústavou garantovaných práv, ak k takému opatreniu neexistuje legitímny cieľ (ciele) a súčasne nie sú s jeho prípadným odstránením ohrozené iné dôležité verejné záujmy.“  Pri voľbách do orgánov samosprávnych krajov a orgánov samosprávnych obcí nevyhovel vzhľadom na ich značne obmedzený rozsah svojej normotvornej právomoci a územne obmedzenú pôsobnosť. Rovnako sa tieto voľby vzťahujú na trvalý pobyt a osoby vo výkone trestu odňatia slobody majú možnosť sa prihlásiť na trvalý pobyt v obci, v ktorej ma sídlo ústav na výkon trestu odňatia slobody. Vo svojom vyjadrení generálna riaditeľka Zboru väzenskej a justičnej stráže jasne uviedla, že zatiaľ nebol známy prípad, kedy by odsúdený požiadal o zmenu trvalého pobytu alebo prechodného pobytu, z dôvodu odňatia slobody. Z tohto hľadiska sa ani väčšina nemôže reálne zúčastniť na živote obce, a činnosť orgánov samosprávy obce nemajú na nich žiadny bezprostredný vplyv. Aj orgány samosprávneho kraja majú vplyv iba v značne obmedzenom rozsahu.  I keď nebolo rozhodnutie Ústavného súdu v krátkom čase prenesené do zákonov vo forme novely, osobám vo výkone trestu odňatia slobody bolo umožnené sa zúčastniť už vo voľbách do Európskeho parlamentu, v júni toho istého roku. Čo sa týka prezidentských volieb, v nich prekážka vo volebnom práve pre väzňov nebola nikdy stanovená.  c) 2009/2010 - Národná rada Slovenskej republiky a legislatívny proces  Vládny návrh doručený 12.11.2009 na pôdu Parlamentu novelizoval oba volebné zákony do NRSR a do Európskeho parlamentu. Dôvodová správa sa opierala o rozhodnutie Ústavného súdu a počiatočný návrh priznával aktívne volebné právo všetkým osobám vo výkone trestu odňatia slobody. Daniel Lipšic predložil dva pozmeňujúce návrhy, ktoré stanovovali limit na:  - zločin – jedná sa o úmyselný trestný čin, za ktorý ustanovuje zákon hornú hranicu trestnej sadzby prevyšujúcu päť rokov (viac ako päť), alebo - obzvlášť závažný zločin – dolná hranica trestnej sadzby minimálne desať rokov  Odôvodnil jeho rozhodnutie tým, že návrh nepovažuje za politický. Položil aj otázku s odpoveďou: „Kto sa zastane obetí trestných činov? Páchatelia majú svojich advokátov, obetí sa musíme zastať v parlamente.“  Jana Laššáková neskôr navrhla rovnaký návrh, ktorý aj uspel a opieral sa o hranicu obzvlášť závažných zločinov. Odôvodnila ho existujúcim verejným záujmom. Daniel Lipšic následne svoje návrhy stiahol.  Zákon bol schválený a podpísaný 15. februára prezidentom SR s účinnosťou k 15. marcu 2010.  2. Úvaha  Hranica, ktorá určuje prekážku volebného práva nám už je jasná, keďže sa odvíja od zákonne stanovenej trestnej sadzby trestného činu. Teraz identifikujme a opíšme bližšie niektoré trestné činy. Druhá časť Trestného zákona sa delí na jednotlivé hlavy. Každá jedna hlava predstavuje príbuzné spoločenské záujmy, ktoré sú chránené Trestným zákonom a ustanovuje v nich skutkové podstaty trestných činov/popisuje trestné činy, ktoré ohrozujú, alebo porušujú tieto záujmy. Jednotlivé hlavy sú zoradené v závislosti od prioritnej ochrany v demokratickej spoločnosti. Nižšie uvedené trestné činy sa nachádzajú v prvých troch hlavách z dvanástich, pričom nie sú predpokladom straty volebného práva:    § 159 – Neoprávnené odoberanie orgánov, tkanív a buniek a nezákonná sterilizácia  „Páchateľ, ktorý neoprávnene odoberie zo živej osoby orgán, tkanivo alebo bunku, alebo páchateľ, ktorý pre seba alebo iného taký orgán, tkanivo, či bunku neoprávnene zadováži.“  Páchateľ týmto odberom zasiahne do chráneného práva človeka na zachovanie jeho telesnej integrity a práva na nedotknuteľnosť osoby. Samozrejme je to ponímané v rovine protiprávnosti a nejedná sa o dobrovoľné darcovstvo.  § 161 – Neoprávnený experiment na človeku a klonovanie ľudskej bytosti  „Páchateľ, ktorý pod zámienkou získania nových medicínskych poznatkov, metód alebo na potvrdenie hypotéz, alebo na klinické skúšanie liečiv vykonáva bez povolenia overovanie nových medicínskych poznatkov a ohrozuje bezprostredne život a zdravie človeka.“  Lekársky experiment na živom človeku je takisto obrovským zásahom do jeho osobných práv a osobných slobôd, s nebezpečenstvom spôsobenia smrti.  § 165 – Ohrozovanie vírusom ľudskej imunodeficiencie  „Páchateľ, ktorý iného úmyselne vydá do nebezpečenstva nákazy vírusom ľudskej imunodeficiencie.“  Ľudská imunodeficiencia je patologický stav vyplývajúci z funkčnej poruchy niektorej zložky imunitného systému, napríklad vírus HIV. Osoba vedomá si svojej pohlavnej choroby prevádza pohlavný styk so zdravou osobou, prípadne inak ohrozuje nakazením (zatiaľ nevyliečiteľným) vírusom.  § 179 – Obchodovanie s ľuďmi  „Páchateľ, ktorý použitím ľsti, obmedzovania osobnej slobody, násilia a iných foriem donucovania dosiahne súhlas obete, na ktorú je iná osoba odkázaná, alebo páchateľ zneužije svoje postavenie, či bezbrannosť obete a tú prepraví, prechováva, odovzdá  na účel prostitúcie alebo inej formy sexuálneho vykorisťovania, vrátane pornografie, nútenej práce, otroctva a pod.“  Ešte stále sa jedná iba o zločin, ak je obeťou aj chránená osoba, t.j. dieťa, tehotná žena, osoba vyššieho veku (nad 60 rokov), chorá osoba atď.  § 180 – Obchodovanie s deťmi  „Páchateľ, ktorý v rozpore so zákonom zverí do moci iného dieťa na účel adopcie, jeho využívania na detskú prácu alebo na iný účel.“  Pokiaľ mu páchateľ nespôsobí ťažkú ujmu na zdraví, prípadne smrť, tak sa stále jedná o zločin, no pokúste si predstaviť tie psychické dôsledky a traumy dieťaťa po zvyšok jeho života.  § 199 – Znásilnenie  „Páchateľ, ktorý násilím alebo hrozbou bezprostredného násilia donúti ženu k súloži, alebo na to zneužije jej bezbrannosť.“  Pokiaľ nespôsobí ťažkú ujmu na zdraví, môže tento čin spáchať takisto na chránenej osobe, t.j. dieťaťu, tehotnej žene a pod., sa tento trestný čin kvalifikuje ako zločin. Som si istý, že v mnoho prípadoch bude mať znásilnená žena do smrti sťažený spôsob života, či už v partnerskom, alebo manželskom vzťahu.  § 200 – Sexuálne násilie  „Páchateľ, ktorý násilím alebo hrozbou bezprostredného násilia donúti iného k orálnemu styku, análnemu styku alebo k iným sexuálnym praktikám, alebo pri tom zneužije bezbrannosť obete.“  ------------------------------------------------------  Mnoho z týchto a iných trestných činov nie sú obzvlášť závažnými zločinmi, i keď sú spáchané napríklad z osobitného motívu – t.j., na objednávku, v úmysle verejne podnecovať k násiliu alebo nenávisti voči skupine osôb alebo jednotlivcovi pre ich príslušnosť k niektorej rase, národu, národnosti, farbe pleti, etnickej skupine, pôvodu rodu, alebo pre ich náboženské vyznanie, alebo z národnostnej, etnickej alebo rasovej nenávisti, či nenávisti z dôvodu farby pleti a podobne (§ 140 TZ).  Medzi hlavnú funkciu trestného práva patrí ochranná funkcia – trestné právo poskytuje ochranu spoločnosti pred spoločensky najzávažnejšími útokmi fyzických (neskôr aj právnických) osôb. Účelom trestu má rovnako (okrem iných) ochrannú funkciu – zabezpečiť ochranu spoločnosti pred páchateľom trestného činu, ako aj odradiť ostatných od páchania trestných činov a vyjadriť morálne odsúdenie páchateľa spoločnosťou.  Osobu, ktorú sme pozbavili osobnej slobody pre jej závažné a nebezpečné protispoločenské správanie, porušenie základných práv a chránime pred touto osobou zvyšok spoločnosti, zároveň tej istej osobe poskytneme možnosť rozhodovať o spoločnosti?  Osoba ktorá mala zväčša na výber, či dané spoločenské záujmy ohrozí, alebo poruší.  Osoba, ktorá obchodovala s ľuďmi, znásilnila bezbrannú ženu, zverila dieťa na detskú prácu, prípadne experimentovala na ľudskej bytosti?  Ktovie, aký názor by vyjadrila matka odcudzeného dieťaťa alebo druh znásilnenej ženy voči páchateľovi trestného činu, ktorý spôsobil „nehmotnú“ škodu s celoživotnými následkami, pritom by rovnakým hlasom rozhodoval o tom, kto má určovať ekonomické reformy, riešiť nezamestnanosť, efektívnosť súdnictva, či sociálne zabezpečenie, prijímať zákony a určovať, čo je zákonné a čo protizákonné ?  Európsky súd pre ľudské práva v bode 62 rozsudku Hirst proti Spojenému kráľovstvu spojil „volebné právo so záujmom človeka na živote spoločenstva, na správe ktorého sa prostredníctvom volieb podieľa.“ Aký záujem zastával v momente úmyselného páchania trestného činu? Aký záujem bude zastávať vo voľbách počas obdobia výkonu spravodlivého trestu za porušenie slobôd jeho úmyselným zavinením ?  Nedovolím si nespomenúť zákon o štátnom občianstve SR (zákon č. 40/1993 Z.z.). §7 ustanovuje podmienky udelenia slovenského štátneho občianstva. Udelenie je možné vtedy, ak má žiadateľ nepretržitý trvalý pobyt na území SR aspoň osem rokov, bezprostredne pred podaním žiadosti, je bezúhonný (nebol právoplatne odsúdený za úmyselný trestný čin atď.), nie je proti nemu vedené trestné stíhanie, vydávacie konanie, ani konanie o vykonaní európskeho zatýkacieho rozkazu a podobne. Ministerstvo je povinné rozhodnúť do jedného roka od podania žiadosti (t.j. spolu do deväť rokov). Získanie štátneho občianstva je pochopiteľne úzko prepojené so získaním aktívneho volebného práva. Suma sumarum priemerná osoba, ktorá tu osem rokov vedie riadny, možno až oproti ostatným slovenským občanom vzorný život, riadne platí dane, nemá prakticky dve volebné obdobia v dispozícii rozhodnúť o svojich zástupcoch, ktorí svojou politickou a legislatívnou činnosťou ovplyvňujú jeho každodenný život. Zaujímavý by bol názor aj týchto ľudí.  To, že zákon o spôsobe voľby prezidenta SR (zákon č. 46/1999) neumožňuje voliť Slovákom, ktorí sa nachádzajú v zahraničí, snáď ani nie je potrebné rozvádzať.  Polemiku o morálnosti, ktorá sa môže značne líšiť od jednotlivca k jednotlivcovi, by sme mohli viesť celé dni, mesiace a roky, no cieľom tohto článku je objektívne posúdiť zmieňované obmedzenie volebného práva z iného pohľadu.  3. Alternatívne riešenie  Obmedzenie volebného práva je v demokratickej spoločnosti prípustné. Mnohé dokumenty určujú hranice: napríklad dokument prijatý Európskou komisiou pre demokraciu prostredníctvom práva predloženého Parlamentnému zhromaždeniu Rady Európy v novembri 2002 (Code of Good Practice in Electoral Matters), všeobecný komentár k článku 25 Medzinárodného paktu o občianskych a politických právach a relevantná judikatúra Európskeho súdu pre ľudské práva.  V priemere sa tie hranice dajú identifikovať v pár bodoch. Obmedzenie:  - musí sledovať legitímny cieľ spočívajúci v dôležitom verejnom záujme - musí dodržiavať zásadu proporcionality – suspendovanie práva voliť musí byť primerané trestnému činu a trestu - musí byť ospravedlniteľné požiadavkami a je vylúčené, aby bol zbavený práv iba z dôvodu, že bol odsúdený a nachádza sa vo výkone trestu - nesmie byť automatické a paušálne vzťahujúce sa na všetkých väzňov bez ohľadu na dĺžku trestu, charakter a závažnosť spáchaného trestného činu - by malo vyplývať z rozhodnutia súdu  Status quo prinášajúce novelou striktne vymedzuje hranicu, ktorá sa odvíja od výšky trestnej sadzby. Svojím spôsobom sa jedná o automatické a paušálne odobratie volebného práva, i keď sa nevzťahuje na všetky osoby vo výkone trestu odňatia slobody a týka sa to činov so značnou závažnosťou.  Dávno neplatný trestný zákon spred 60 rokov (zákon č. 86/1950 Zb.) ustanovoval vedľajší trest zvaný „Trest straty čestných práv občianskych“. Odsúdený týmto trestom okrem iného stratil volebné právo. Vedľajší trest znamená, že popri treste odňatia slobody bude v odsudzujúcom rozsudku zahrnutý aj tento trest. V roku 1962 nadobudla účinnosť novela, ktorá trest zrušila a všetkým väzňom bolo odopreté právo voliť, prakticky až donedávna.  Kľúčová otázka by pravdepodobne mala odznieť: v čom by bol takýto trest spravodlivejší a vhodnejší, v porovnaní so súčasnou úpravou?  a) súčasné obmedzenie – je stanovené pre obzvlášť závažný zločin, takže sa vzťahuje na výšku trestnej sadzby ustanovenej priamo v Trestnom zákone za jednotlivé trestné činy. V praxi to znamená, že ak je osoba odsúdená za tento čin a rozsudok nadobudne právoplatnosť (už nie je možné podať riadny opravný prostriedok – odvolanie), súd vydá nariadenie výkonu trestu a odsúdený je prijatý do ústavu na výkon trestu odňatia slobody - automaticky je odsúdenému odopreté právo voliť v zmysle volebných zákonov a jediná možnosť, ako získať toto právo späť, je napadnúť právoplatný rozsudok mimoriadnymi opravnými prostriedkami – napadnúť a spochybniť spáchanie trestného činu, svoju vinu atď. Bez ohľadu na formu protispoločenského útoku, sa plošne vzťahuje na minimálnu zákonne ustanovenú desať ročnú trestnú sadzbu. Výška trestných sadzieb tiež nie je ustáleným a nemenným javom.  b) trest straty čestných práv občianskych – zaradil by sa spomedzi ostatné tresty. Ukladal by sa ako vedľajší, popri treste odňatia slobody.  Výhody vnímam v tom, že by bol daný trest udelený na základe rozhodnutia súdu. Bolo by na uvážení a zhodnotení súdu, či popri treste odňatia slobody by trest straty čestných práv občianskych sledoval legitímny cieľ spočívajúci vo verejnom záujme a bol by primeraný k spáchanému trestnému činu, keďže by bol ukladaný ad hoc ku každému prípadu.  Ako vedľajší trest by bol v rozhodnutí zastúpený bežným výrokom. K tomu, aby súd vyslovil výrok o strate volebného práva, bol by povinný dostatočne zdôvodniť, prečo si odsúdený popri zbavení jeho osobnej slobody spravodlivo zaslúži nepodieľať sa na rozhodovaní o správe jeho vlastnej krajiny. Prečo jeho konanie bolo natoľko závažné, aby sa mu zabránilo pristúpiť k volebnej urne. Zhodnotenie v individuálnej rovine presvedčenia, právnej logiky a právnej disciplíny sudcov, no ohraničené v ústavných medziach a rešpektovanej judikatúry. S týmto je spojená aj skutočnosť, že ako bežný výrok v rozhodnutí, by bol napadnuteľný opravným prostriedkom.  V praxi by mal odsúdený širšie dispozičné práva proti takémuto obmedzeniu a zásahu do citlivých ústavných práv, v porovnaní so súčasnou úpravou. Ak by nebol schopný podať relevantný opravný prostriedok voči výroku o svojej vine a treste odňatia slobody, prípadne by opravné konanie bolo pre neho neúspešné, ešte by mu stále zostávala možnosť argumentačne ustáť voči výroku o strate čestných práv občianskych.  V skratke: síce by po nadobudnutí právoplatnosti ostatných výrokov nastúpil na výkon trestu odňatia slobody, v opravnom konaní by bol úspešný voči výroku o strate čestných práv občianskych a volebné právo by mu nikdy nebolo odopreté. Samozrejme za predpokladu, že by mu to právo stálo za námahu.  Špecifické znenie ustanovenia tohto trestu by malo takisto podliehať odbornej diskusii. Bezpochyby by mali byť stanovené minimálne hranice, ako je napríklad úmyselné zavinenie u trestného činu, ukladať až pri trestoch odňatia slobody nad 5-6 rokov a iné. Trest straty čestných práv občianskych by nemusel nevyhnutne kopírovať dĺžku trestných sadzieb ostatných trestov, za ktoré by bol v rovnakom rozhodnutí odsúdený.   Príklad: Páchateľ je odsúdený na trest odňatia slobody na 8 rokov a na trest straty čestných práv občianskych na 4 roky, ktoré by zodpovedali jednému volebnému obdobiu NRSR.  Diskusia o novele sa v Parlamente absolútne neviedla. Tieto návrhy by si síce vyžadovali novelu Trestného zákona, avšak po dostatočnej diskusii vo výboroch a v samotnom zákonodarnom zbore ako celku, by sa mohlo dospieť k flexibilnejším záverom, pričom Trestný zákon sa novelizoval v obdobnom čase, kedy volebné zákony. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Szabo 
                                        
                                            Prezidentský vzdor a nominačné mantinely
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Szabo 
                                        
                                            Neústavné zadlžovanie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Szabo 
                                        
                                            Prezidentov právny relativizmus a Ficove protiústavné exkrementy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Szabo 
                                        
                                            Vláda práva a vláda právom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Szabo 
                                        
                                            Mocenská ruka Sovietov u kráľa prokurátorov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Szabo
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Szabo
            
         
        pavolszabo.blog.sme.sk (rss)
         
                                     
     
        Ešte stále mladý a na vlastnej ceste. Nevybral som sa po nej sám, no zotrvať na nej a stále poznať jej zmysel, je veľká výzva pre mňa, ako aj pre iných.  Človek, občan, právnik a teraz po škole študentom života.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2333
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Právo a politika
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




