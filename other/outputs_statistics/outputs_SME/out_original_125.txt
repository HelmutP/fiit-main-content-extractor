




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 Bavorsk? Al?beta "cis?rovn? Sisi" 
 
 Vydan? 11. 9. 2003 o 0:00 Autor: DARINA S?KOROV?
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (7)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 Sisi v skuto?nosti a jej sl?vna telev?zna podoba.	FOTO ? ARCH?V 
 

 Al?beta Bavorsk?, man?elka Franti?ka Jozefa I. Habsbursk?ho, prez?van? Sisi, mala v ?ivote takmer v?etko - postavenie, kr?su, bohatstvo, l?sku man?ela a det?, ale nikdy nebola spokojn?. Cel? ?ivot bl?dila, cestovala, h?adala - pravdepodobne samu seba. Nikdy sa jej to nepodarilo. V septembri 1898, pred 105 rokmi ju zavra?dil anarchista Luigi Lucheni. Prebodol ju upraven?m ostr?m piln?kom. 
 Al?beta poch?dzala z bavorsk?ho panovn?ckeho rodu Wittelsbachovcov. Za man?elku Franti?ka Jozefa ju nevybrali. Jej sestra Helena v?ak musela ust?pi? pred rozhodnos?ou cis?ra, ktor? asi jedin?kr?t zavzdoroval svojej matke - a v?nivo sa za??bil do Sisi. V tom ?ase mala p?tn?s? rokov. 
 Sisi l?sku op?tovala, sk?r v?ak i?lo o o?arenie tr?nom, poklonami a mo?nos?ou prezentova? svoju kr?su. Al?beta bola naozaj kr?sna, ale aj posadnut? svoj?m v?zorom. Nen?videla svoje tehotenstv?, lebo ni?ili jej ?os? driek, svoje deti milovala a op???ala pod?a n?lady. Z du?e nen?videla svoju svokru, cis?rovn? ?ofiu, ktor? chcela p?n?s?ro?n? nevestu usmer?ova?. 
 Franti?ek Jozef I. ?a?ko zn?al boj t?chto dvoch ?ien, ktor?ch osud tr?nu a skuto?n? politika absol?tne nezauj?mali. Al?beta v?ak do politiky zasahovala, jej vz?ah k Uhorsku prispel k rak?sko-uhorsk?mu vyrovnaniu. Ve?mi pravdepodobne preto, ?e uhorsk? magn?ti a najm? gr?f Andr?ssy jej nesmierne lichotili. Sprievodky?a v ma?arskom z?mku Habsburgovcov G?d?l?, ned? na Sisi dopusti? jedno kritick? slovo. 
 
 
 

 Aj ke? sa klebetilo o ich vz?ahu, je ist?, ?e Al?betin vz?ah k sexu bol sk?r negat?vny. Franti?ek Jozef sa na ?u vraj v?dy ?vrhal? s nesmiernou t??bou, ale jej potreby boli asi in?. Milovala inotaje v po?zii a rozhovoroch. Najpozoruhodnej?? bol jej vz?ah a nekone?n? rozhovory s ?udov?tom II. Bavorsk?m. ?ialen? ?udov?t, obdivovate? kr?sy a stavite? rozpr?vkov?ch z?mkov, jej povahe vyhovoval. Realita ani jedn?ho z nich nezauj?mala. Nevideli ju, alebo nechceli vidie?. Recitovali Heineho, ktor? Al?betu in?piroval k p?saniu b?sn? (?i sk?r plagi?tov), a hrali divadlo. Oby?ajn? ?prasce?, ako Al?beta nazvala v jednom liste vlastn? vn??at?, nemohli vst?pi? do ich sveta. 
 Al?betina choroba, ktor? jej umo??ovala uteka? z Viedne a zdr?ova? sa, kde chcela, by sa dnes lie?ila na psychiatrii. Pravdepodobne mala pr?znaky anorexie. Jej posadnutos? ?t?hlos?ou si v?imla aj man?elka anglick?ho ve?vyslanca. ?Na chudom hrdle m? vol?nik z ?ierneho fl?ru. Jej tv?r vyzer? ako maska.? Choroba jej umo?nila cis?ra aj vydiera?. Listy cis?rovi sa za??nali: ??alej si prajem,? a pokra?ovali po?iadavkami. Cis?r ich plnil, len ju prosil, aby myslela na ??boh? deti?. 
 Deti v?ak Al?betu sklamali. Najlep?ie sa spr?vala k M?rii Val?rii, Gizelu priam nen?videla, ale najviac ju zradil Rudolf - n?sledn?k tr?nu. Na?iel si milenku a nesk?r sp?chal samovra?du. Vyu?ila v?ak pr?le?itos? a nosila u? len ?ierne ?aty, ktor? ju e?te viac zo?t?h?ovali. 
 Aby ju man?el neob?a?oval, na?la mu za seba n?hradn??ku, hoci jej na ?om asi z?le?alo. Here?ka Katar?na Schrattov? tvorila zvl?tny bod trojuholn?ka ako ?priate?ka? cis?rskych man?elov. Cis?r u nej na?iel porozumenie, ktor? u Sisi nemal. 
 Sisi nebola slne?n?m stvoren?m, ako ju pozn?me z romantick?ho filmov?ho pr?behu s Romy Schneiderovou. Nebola ani rozv?nou a m?drou ?enou, ako ju zahrala Jana Hlav??ov? v zn?mej telev?znej inscen?cii. Bola ako ?bl?diaci Holan?an?, nikdy sa neuspokojila, ale krut? smr? si nezasl??ila. 
 Lucheni neuva?oval nad jej politickou anga?ovanos?ou a s ve?kou pravdepodobnos?ou by ho ani nezauj?malo, ak? ?lohu zohrala pri rak?sko-uhorskom vyrovnan?. Sta?ilo mu, ?e reprezentovala tr?n. Nevie sa, ?i ju sledoval dlh?ie, alebo ?i i?lo o zvl?tnu zhodu okolnost?. Ot?zkou je, ako cis?rovn? spoznal, ke? sa pohybovala bez v??ieho sprievodu. 
 Habsburgovci detaily Al?betinej smrti nikdy nezverejnili a presn? fakty o atent?te dodnes nie s? zn?me. Ist? je, ?e cie?om anarchistu bolo zni?i? reprezentantku moci. Zomrela len preto, ?e bola v ?eneve nechr?nen?m ?ahk?m cie?om. 
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (7)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? 16:00  Rube? kolabuje, rusk? vl?da m? mimoriadne rokova? 
 Jeden dol?r st?l v utorok poobede u? viac ako 78 rub?ov, razantn? zv??enie ?rokov nepomohlo. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 17:33  
Taliban zabil v ?kole v Pakistane vy?e sto ?ud?, v??inou deti
 
 Militanti vtrhli do vojenskej ?koly v policajn?ch rovno?at?ch. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  Cynick? obluda: Medzi?asom na opustenom ostrove 
 Stroskotanci maj? ve?k? ??astie, ?e s? dvaja. Lacn? vtipy na ?rovni ich autora. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  
Zav?dzaj?ca produktivita pr?ce
 
 Priemern? hodnoty s? pekn?m ?dajom, ale zl?m n?vodom pre hospod?rske politiky. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






