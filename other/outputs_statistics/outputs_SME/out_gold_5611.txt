

 Sľúb,že naše 
 
 slnko lásky nikdy nezhasne 
 
 a ja pokorím hory v diaľave 
 
 na vrchol položím 
 
 srdce svoje boľavé. 
 
 
 Neraz pichne ma pri ňom 
 
 keď len fotky si obzerám 
 
 keď mesiac deň zastrie 
 
 na hviezdy sa smutne pozerám. 
 
 
 Ďakujem Bohu,že som Ťa stretol 
 
 tak čistú a nevinnú 
 
 že iný Ťa nezviedol 
 
 Tvoj otec mi Ťa z bielym čepcom 
 
 pred oltár priviedol. 
 
 
 Sľúbil som vernosť Ti 
 
 a ja to dodržím 
   
 
 neresti pod zámok 
 
 neveru zahubím. 
 
 
 Ja oheň som Ty voda 
 
 ktorú milujem a zbožňujem 
 
 aj keď občas z koryta vylieva sa 
 
 náš oheň lásky nikdy nezhasí 
 
 a prameň ten zosilnieva. 
 
 
 Búrky čo život prináša 
 
 svetlom slnko zastrie 
 
 rukou hladím chrbát Tvoj 
 
 Ty cítiš sa slastne a štebot detí opodiaľ 
 
 čo viac chcieť 
 
 veď to je to pravé šťastie. 
 
 
 Sládkovič mal Marínu 
 
 ja zas Teba Erika 
 
 si studňou po ktorej prahnú moje pery 
 
 do lona Tvojho sa cely vkladám 
 
 keď Teba mám inú už nehľadám. 
 
 
 Jak oceán bezodný a vesmír nesmierny 
 
 je prameň lásky vo mne 
 
 aj keď občas unáša ma sopky hnev v noci aj vo dne 
 
 ja viem kde je prístav môj,ten stále nájdem 
 
 kde Ty si vo mne a ja v Tebe 
 
 tak veľmi Ťa láska moja milujem... 
 
 
   

