

 V inak celkom zaujímavom článku na portáli centrum.sk sa objavila nasledovná správa: 
 (...) 
 17:51 - Predseda SNS Ján Slota, ktorý pôvodne sľuboval, že účastníkov pochodu príde osobne opľuť, vydal stanovisko, v ktorom Dúhový PRIDE odsúdil. "Sme presvedčení, že takáto sexualita nepatrí na verejnosť. Demonštrácia nielenže ohrozuje normálne mravné hodnoty, ale vyprovokovala násilie a zvýšené náklady na prácu polície. SNS je rozhodne proti sprístupneniu inštitútu manželstva pre páry rovnakého pohlavia spolu s možnosťou adopcie detí. Manželstvo je a bude zväzkom muža a ženy a zasadíme sa za jeho jednoznačnú právnu definíciu," uviedol Slota. 
 (...) 
 ľahkovážený znevážený pán Slota, 
 dovoľte mi, aby som sa Vám týmto prihovoril. Budem stručný. Všetko násilie a všetky zvýšené náklady na prácu polície majú na svedomí presne tí ľudia, ktorí zastávajú názor váš. Ak je potrebné, aby mierumilovný pochod ľudí museli chrániť policajné zložky pred svorkami polovedomých poloľúdí, aby v záujme zachovania ľudských prirodzenopráv (ani nepíšem ústavných, lebo tá je vo vašich špinavých rukách, ale medzinárodne, ľudskoprirodzene garantovaných) - práva slobodne sa zhromažďovať, práva verejne prejaviť svoj názor - musela zasahovať štátna sila, tak asi nie je čosi so spoločnosťou v úplnom poriadku. 
 Áno, v úplnom poriadku nie ste vy, pán Slota. Navrhujem vám vami vyrečené, vypľuté, aplikovať na seba, samosaopľuť : 
 "Som presvedčení, že takáto osoba nepatrí na verejnosť. Nielenže ohrozuje normálne mravné hodnoty, ale vyprovokovala násilie a zvýšené náklady na prácu polície." 
 Kto nevníma rozdiel medzi demonštráciou za vlastný názor, postoj, určený výhradne ľudskému vnútru s demonštráciou násilne odsudzujúcou názory a postoje iných, je pre mňa nula. V živote, v diskusii, všade. 
 (...) 
 Smutné post-skriptum: A to je asi ten problém. Niet rozumnej trecej plochy, ktorou by pôsobili ľudia vzdelanejší, tolerantnejší, ľudia ľudskejší, na takýchto podľúdí, zakomplexovaných nevzdelancov atď atď atď. Riešenie vidím jediné, a toť také, že váš názor, pán Slota, by mal spôsobiť aj zvýšené náklady na vzdelanie, sociálne a psychologické poradenstvo a na budovanie interspoločenských vzťahov.  

