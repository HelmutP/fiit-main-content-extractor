

 
Nesmierna vinou sa previní rod Löwenskoldov; traja nevinní ľudia
zaplatia životom. Vždy musí byť niekto, kto volá o pomstu. 
Je ňou Marit, dcéra a verenica. Sú premárné životy, je premrhaná aj 
láska. Tak znie kladba. Traja Löweskoldovci musia zomrieť násilnou
smrťou; a ani láska nesmie byť naplnená. 
 
 
Charlotta Löwenskoldövá neverí na staré rodové povesti. A predsa
cíti, že čiasi ruka neustále bráni jej šťastiu. Charlotta je hrdá príslučníčka
starobylého rodu. Napriek silnému boju prehráva. 
Osud jej a jej blížnych určujú neznáme sily. Navonok však nič nebadať. 
Všetko ostáva iba na tušeniach. A možno sú to všetko iba povery. 
Švédski ľudia veria v Boha; Anna Svärdová je z Medstuby a tam sa ľudia
inak dívajú na svet vôkol. Vedia svoje a sú si vedomí, že človeku možno počarať,
aby v kostole videl svoju mŕtvu matku. 
 
 
Traja Löweskoldovci zomrú násilnou smrťou. A láska ostane nenaplnená. 
Lebo ľudia často nebývajú šťastliví.
 
 
Je tu množstvo zvratov a uzlov. Mnoho postáv a viacero generácií. 
Netradičný pohľad na udalosti. Na vzťahy. Čitateľ určite nevytuší
konečné rozuzlenie. 
Trilógia Löwensköldov prsteň je jedným z najznámejších diel
Selmy Lagerlerlöfovej. 
 

