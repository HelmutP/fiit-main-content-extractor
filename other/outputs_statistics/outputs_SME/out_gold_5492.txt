

   
 Podme teda k vykladom. 
   
 "Mucho" [mučo] znamena "vela". Toto slovo sa pouziva pred podstatnymi menami. 
 Napriklad: 
 mucho dinero [mučo dinero] - vela penazi 
 mucho cariño [mučo kariňo] - vela lasky 
 mucha alegria [muča alegria] - vela radosti 
 mchos problemas [mučos problemas] - vela problemov 
 muchas peras [mučas peras] - vela hrusiek 
   
 "Muy" [muj] znamena "velmi", pouziva sa pred pridavnymi menami a prislovkami. 
 Napriklad: 
 muy bueno [muj bueno] - velmi dobry 
 muy generosos [muj chenerosos] - velmi stedry 
 muy sencillas [muj sensijas] - velmi jednoduche 
 muy cansada [muj kansada] - velmi unavena 
   
 Co sa tyka lasky, vyjadrit mnoztvo je velmi dolezite. Kto si nespomenie na rozpravku "Sol nad zlato". V tedy sa slovo "mucho" pouziva az za podstatnym menom. Teda, ak chceme niekomu povedat "Velmi ta lubim" po spanielsky to bude takto "Te quiero mucho". 
 Te [te] - teba 
 quiero [kiero] - lubim (ja) 
 mucho [mučo] - vela 
   
 Ked dame vsetky slova dokopy, tak doslovny preklad by znel takto: "Teba ja velmi lubim". 
 Znie to akoby Alinko z Mafstory hovoril do uska Pipike ako ju velmi lubi. :-)  Ano, ano, priznam to som ich fanusik. 
   
 Priatelia, dufam ze ste sa zas trosku pousmiali a nieco naucili, alebo precvicili. 
 Hasta la victoria amigos. 
   

