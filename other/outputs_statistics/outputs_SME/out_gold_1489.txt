

 A nastala spoveď, pri ktorej ma mrazilo. Asi som vystihla jej slabú chvíľku, asi už mala toho všetkého divadla plné zuby, alebo potrebovala vypustiť paru... neviem. Všetko rozprávala s mrazivým pokojom. A ja som ju neprerušovala. 
 Vieš, ja neviem, či som nekompletná ja... ale nič nie je také ako sa Ti zdá. A nič nie je zďaleka tak ako som si predstvovala. Hej, nemáme sa zle. Vlado dobre zarobí. Aj handry si môžem kupovať, dopraje mi. Ale v poslednej dobe so mnou komunikuje ako s poslednou chuderou. Prstom prejde po poličke, že či viem, koľko je tam prachu. Šplechne mi do očí, že máme v dome bordel. Akoby som na to všetko mala byť len ja. On sa cíti len ako gazda, ktorý položí na stôl peniaze a nestará sa. Ani neviem, či ho ešte milujem. Vieš, tak vášnivo, bez príčiny. Už žijem vlastne iba pre deti. Aby mali kompletnú rodinu. Taká šťastná rodinka - ako z filmu. 
 Vieš, niekedy je to super! Príde v dobrej nálade domov a aj sám od seba pomôže... poumýva riad, uprace po deckách. Večer si dáme vínko a potom nádherný sex. Len týchto momentov je čoraz menej a menej. Nemyslím, si, že by mal inú... ale to ako so mnou komunikuje. Ako taký despota. Aj pred deťmi mi nadáva. 
 Furt mi povie... "nefajčím, do krčmy nechodím, za ženami nebehám... a ty sa ani neusmeješ!" Povedz mi, mala by si silu usmievať sa na niekoho, z koho si v strese, čo Ti zasa vykričí, že si urobila zle, alebo neurobila vôbec? 
 Opustiť ho nemôžem. Som na ňom závislá. Hlavne finančne. A vieš, svadba v kostole... ani pomyslieť na nejaký rozchod. Našich by to zabilo. A kde je vlastne Boh, keď mu to je fuk, ako sa na toto môže pozerať? 
 Keď zbadala v mojich očiach prekvapenie...  iba dodala: 
 Ja viem, stávajú sa aj horšie tragédie. Čo sa tu vlastne sťažujem. Deti sú ako tak zdravé. Mám strechu nad hlavou. Veď vlastne o nič nejde... len tá láska akoby kdesi odišla.  

