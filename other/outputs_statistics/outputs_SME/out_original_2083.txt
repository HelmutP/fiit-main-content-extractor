
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Belko
                                        &gt;
                Tipy a triky
                     
                 Poznáte Ribbon Hero do MS Office? 

        
            
                                    20.1.2010
            o
            1:20
                        |
            Karma článku:
                7.66
            |
            Prečítané 
            3476-krát
                    
         
     
         
             

                 
                    Doplnok do MS Office  - Ribbon Hero je aktuálne dostupný aj verejne. Prináša celkom zaujímavú funkcionalitu pre Word, Excel a PowerPoint. Môžete si vyskúšať do akej hĺbky ovládate jednotlivé aplikácie a výsledky zdieľať na facebooku.
                 

                 Po nainštalovaní a prvom spustení aplikácie sa zobrazí úvodná obrazovka s možnosťou pozrieť si základné inštruktážne video zo stránky www.microsoft.com/showcase .   Samotný doplnok spustíte cez záložku Home (Domov) v skupine Office Labs príkazom Ribbon Hero. V okne, ktoré sa zobrazí sú informácie o získaných bodoch za jednotlivé úlohy, ktoré vykonávate príkazom Play Challenge.     Spustená úloha sa zobrazí v prostredí aplikácie s inštrukciami a ukážkou ako má výsledok vyzerať. Vpravo v podokne úloh si môžete zobraziť aj tip ako postupovať ak Vás nenapadá správne riešenie. Niektoré úlohy majú aj viac krokov, ktoré treba spraviť aby ste získali požadovaný počet bodov. Pri ich vykonávaní sa zobrazujú v podokne kroky, ktoré máte úspešne spravené s animovanými balónmi a ozvučením :-) Z hlavného okna si môžete zobraziť aj pomocníka Office On-line k aktuálnej úlohe.     Ribbon Hero je prototyp a nie je k nemu aktuálne žiadna podpora. Funguje len s Office 2007 a novšími. Viac detailov je na stránke officelabs.com/ribbonhero.   Osobne si z pohľadu lektora myslím, že je to veľmi dobrý nápad na otestovanie základných znalostí používateľa. Uvidíme či pribudnú aj možnosti detailnejšieho nastavenia aplikácie ako je napr. zadať počet otázok, prípadne vybrať z dvoch úrovní obtiažnosti. Aktuálna verzia je v 1.0.0.3, ja mám nainštalovanú ešte pôvodnú zhruba spred mesiaca. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Máte radšej bežné školenie alebo webinár?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Analyzoval som podporovateľov R. Fica v Exceli 2013
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Microsoft vrátil do Outlooku 2013 funkciu, ktorú pôvodne odobral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Hromadne si zmeňte utajenie udalostí v Outlooku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Belko 
                                        
                                            Pripojenie pomocníka Office 2013 na internet (+video)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Belko
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Belko
            
         
        belko.blog.sme.sk (rss)
         
                        VIP
                             
     
        Môžete ho stretnúť ako lektora na počítačových školeniach, pri IT konzultáciách vo firmách, na letných terasách a v kaviarňach ako pozoruje dianie okolo seba, ale aj na potulkách po gréckych ostrovoch, pretože počítače nie sú jediné čo ho zaujíma.Ostrovné správy popisuje na osobnej stránke www.dovolenkar.sk Aktívne prispieva na svoj portál Tipy a triky v MS Office.. 
  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    344
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3490
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Tipy a triky MS Office 2013/20
                        
                     
                                     
                        
                            Externé tipy a triky MS Office
                        
                     
                                     
                        
                            SharePoint, spolupráca,Office3
                        
                     
                                     
                        
                            Tipy a triky
                        
                     
                                     
                        
                            Návody
                        
                     
                                     
                        
                            Stalo sa ...
                        
                     
                                     
                        
                            Office 2010/2013 Beta
                        
                     
                                     
                        
                            Microsoft KB články
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Portál Tipy-triky.sk v MS Office
                                     
                                                                             
                                            .
                                     
                                                                             
                                            Správa a optimalizácia siete a IT
                                     
                                                                             
                                            Dovolenkar.sk– dovolenkové informácie
                                     
                                                                             
                                            Windows User Group
                                     
                                                                             
                                            Google+ profil
                                     
                                                                             
                                            Jednoduché video návody
                                     
                                                                             
                                            letenky.dovolenkar.sk - LETENKY do celého sveta
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




