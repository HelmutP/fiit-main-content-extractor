

 
* 
 
Rodina Jesenských:

 
 

Janko JESENSKÝ, 1874-1945, spisovateľ,

 
 

Zora JESENSKÁ, 1909-1972, prekladateľka, spisovateľka a redaktorka 
 
 
Ivan STODOLA, 1888-1977, lekár a spisovateľ 
 
 
Jozef CÍGER-HRONSKÝ, 1896-1960, spisovateľ, redaktor, kultúrny pracovník 
a 
Štefan KRČMÉRY, 1892-1955, básnik, literárny historik a kritik, prekladateľ 
 
 
Maša HAĽAMOVÁ, 1908-1995, poetka, spisovateľka a redaktorka 
a 
manžel Ján PULLMAN, 1898-1956, lekár-internista a ftizeológ 

 
 
 
 
Július BARČ-IVAN, 1909-1953, spisovateľ 
 
 
František HEČKO, 1905-1960, spisovateľ a redaktor 
a 
manželka Mária JANČOVÁ-HEČKOVÁ, 1908-2003, spisovateľka pre deti a mládež, poetka 
 
 
Ján BODENEK, 1911-1985, spisovateľ 
 
 
Ján SMREK-ČIETEK, 1898-1982, básnik 
a 
Blanka SMREKOVÁ-ČIETEKOVÁ, 1910-2002, spisovateľka, poetka 
 
 
Vladimír ROY, 1885-1936, básnik 
 
 
Milan HODŽA, 1878-1944, politik, predseda vlády ČSR 
 
 
Ferdinand JURIGA, 1874-1950, politik, publicista 
 
 
Ferdinand ČATLOŠ, 1895-1972, generál, minister 
 
 
Jozef LETTRICH, 1905-1969, politik 
 
 
Ján BULÍK, 1897-1942, krajanský pracovník, predseda MS v Juhoslávii, odbojár (zahynul v KT Mauthausen), 
 
 
 
Juraj ANTAL, 1912-1996, lekár-fyziológ, univerzitný profesor, akademik 
 
 
Jozef PAJTÁŠ, očný chirurg, univerzitný profesor 
 
 
Martin BENKA, 1888-1971, maliar 
 
 
Fraňo ŠTEFUNKO, 1903-1974, sochár, výtvarný pedagóg 
 
 
Mikuláš GALANDA, 1895-1938, maliar, grafik a ilustrátor 
 
 
Ján GERYK, 1892-1978, etnograf, múzejný pracovník 
 
 
Janka GUZOVÁ-BECKOVÁ, 1917-1993, speváčka a zberateľka ľudových piesní 
 
 
Naďa HEJNÁ, 1906-1994, herečka 
a 
Vít HEJNÝ, 1904-1977, učiteľ, kultúrny pracovník 
 
 
Emil HORVÁTH, 1923-2001, herec 
 
 
Ján HRUŠOVSKÝ, 1892-1975, spisovateľ a novinár 
 
 
František KALINA st., 1919-1992, redaktor a prekladateľ 
a 
František KALINA ml., 1953-1999, výtvarník 
 
 
Andrej LETTRICH, 1922-1993, filmový režisér a scenárista 
 
 
Karol PLICKA, 1894-1987, hudobný folklorista, fotograf, kameraman, filmový režisér 
 
 
Hana MELIČKOVÁ-RAPANTOVÁ, 1900-1978, herečka a 
 
 
jej manžel Daniel RAPANT, 1897-1988, historik, archivár a univerzitný profesor 
 
 
Milan MITROVSKÝ, 1875-1943, maliar a spisovateľ 
 
 
Štefan NEMČOK, 1912-1996, maliar 
 
 
Karol NOVÁK, 1883-1948, zbormajster 
 
 
Ivan Štubňa, 1926-1994, maliar 
 
 
Eduard TVAROŽEK, 1920-1999, prekladateľ, esperantista 
* 
A nakoniec ešte niekoľko zaujímavých náhrobkov: 
   
 
   
 
   
 
   
 
  

 
 

 
 
 
 
 
 
Pri návšteve Martina sa nezabudnite zastaviť na Národnom cintoríne. 
 
 
 

