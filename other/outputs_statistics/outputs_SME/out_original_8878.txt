
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Bednár
                                        &gt;
                Bezpečnostná a obranná politik
                     
                 Čo čaká nového ministra obrany ? 

        
            
                                    15.6.2010
            o
            14:29
                        (upravené
                2.10.2014
                o
                17:03)
                        |
            Karma článku:
                3.69
            |
            Prečítané 
            495-krát
                    
         
     
         
             

                 
                    Rezort obrany nikdy nepatril medzi obľúbené. S výnimkou obdobia po voľbách 1998, keď po Mečiarovskom marazme sa stali zahraničnopolitické otázky kľúčovými. Nutnosť začleniť Slovensko do severoatlantických a európskych bezpečnostných štruktúr prinútila vtedajšiu vládu uskutočniť po roku 1999 reformu, ktorá viedla k vybudovaniu súčasnej profesionálnej Armády Slovenskej republiky. 
                 

                 Aj keď táto reforma bola veľmi úspešná a doporučovaná ako príklad pre ďalšie krajiny NATO-m, nebola táto reforma dostatočne podporená modernizáciou ozbrojených síl. Otázky modernizácie sa spolu s kritikou slovenskej účasti na ISAF-e stali základom vládneho programu dosluhujúcej vlády Róberta Fica pre tento rezort. Zatiaľ čo v otázke zahraničných misií došlo k naplneniu tohto vládneho programu presunutím našich síl z Iraku do Afganistanu, kde Slovensko bolo prinútené navŕšiť počty vojakov aby nestratilo tvár pred svojimi spojencami, zlyhala vláda Róberta Fica v otázke modernizácie. K tomuto zlyhaniu sa pridala celková nespôsobilosť riadiť tento rezort, ktorá sa prejavila neschopnosťou rezortu pravidelne aktualizovať hlavné dokumenty rezortu, pričom ako príklad môže poslúžiť viac ako ročné meškanie tzv. Modelu 2020, ktoré už nebude ani dokončené, respektíve neaktualizovanie stratégii a doktrín. V neposlednom rade, minulé vedenie rezortu zlyhalo v schopnosti uhájiť rozpočet rezortu pred drastickými škrtmi, pričom paradoxne tieto škrty najtvrdšie postihli rezort obrany ešte pred hospodárskou krízou. Nedostatok finančných zdrojov viedol k nutnosti ďalej reorganizovať ozbrojené sily aby bolo vôbec možné ďalej udržať rezort v akom takom funkčnom stave, čo spolu s už spomenutou chýbajúcou koncepciou vnieslo do rezortu ďalší chaos a neodborné zásahy. Z uvedeného vyplýva, že na nového ministra obrany čaká druhá najťažšia úloha v histórii samostatných ozbrojených síl Slovenskej republiky. Prvým krokom, ktorý podnikne nasledujúci minister obrany okrem obligatórneho oboznámenia sa so skutočným stavom rezortu, ktorý sa za posledné roky značne zneprehľadnil, bude kontrola zmlúv podpísaných predchádzajúcim vedením. Je naivné si myslieť, že po všetkých kauzách, ktoré posledné 4 roky postili rezort obrany bude v zmluvách, ktoré zostanú v rezorte všetko v poriadku. Aj keď sú minulé zmluvy určite dôležité pre peňaženku daňového poplatníka, obavám sa, že veľké množstvo prešľapov sa už sanovať nebude dať, a preto považujem za dôležitejšie aby sa sústredil na nasledujúce 4 roky. Preto by okamžite nové vedenie rezortu malo začať pracovať na tom, aby aktualizovalo základné koncepčné dokumenty a pripravilo dlhoočakávaný strednodobý plán rozvoja rezortu. Nosnými časťami tohto dokumentu by mala byť reforma velenia ozbrojených síl, ktoré by malo byť menšie a integrovanejšie a spracovať kvalitnú koncepciu dlhodobo zanedbávanej modernizácie. Dôležitou otázkou tejto koncepcie bude otázka, aké ozbrojené sily budeme potrebovať a aké chceme budovať. Bude zaujímavé sledovať ako sa Slovensko vysporiada so súčasnou diskusiou v NATO, či je potrebné ďalej preferovať expedičnú orientáciu ozbrojených síl alebo klásť väčší dôraz na klasické vojenské spôsobilosti ako reakciu na nárast mocenských ambícii Ruska a ďalších krajín, ktoré začínajú priamo ohrozovať aj hranice Slovenska. Zároveň by nové vedenie rezortu malo minimálne navrátiť pôvodný stav transparentnosti rezortu spred roku 2006 a reformovať proces obstarávania, aby sa neopakovali kauzy predchádzajúcej vlády. Najnáročnejšou úlohou novej vlády bude už niekoľkonásobne spomenutá potreba modernizácie. Nastupujúce vedenie rezortu nebude môcť odmietať žiadny spôsob obstarávania, dôležitým kritériom bude len efektívnosť, pričom nemyslím len efektívnosť finančnú. Rezort obrany nie je v pozícii, že môže zamietnuť ofsety len preto, že to znižuje daňové zisky Nemecku alebo Francúzku, ktoré dominujú v EDA, ale musí myslieť na to, že väčšina výzbroje ASR je zastaraná, nemôže zamietnuť PPP a PMP projekty len preto, že pravdepodobná pravicová koalícia za predchádzajúcej vlády proti nim bojovala a nemôže ani politikárčiť pri spoločnom obstarávaní vnútri štátu ale aj pri cezhraničnej spolupráci ale využiť každú príležitosť, pretože riešenie problému modernizácie už nebude možné ďalej odkladať. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladimír Bednár 
                                        
                                            Ministerstvo obrany – porušenie zákona alebo hrubá nekompetentnosť?
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladimír Bednár 
                                        
                                            Výberové konanie na automatickú pušku v ČR v kontexte zmluvy na dodávku CZ 805 BREN
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladimír Bednár 
                                        
                                            Kontroverzná téma
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladimír Bednár 
                                        
                                            C-27J Spartan - priveľa otázok
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladimír Bednár 
                                        
                                            Promptná reakcia?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Bednár
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Bednár
            
         
        vladimirbednar.blog.sme.sk (rss)
         
                                     
     
         Píšem tento blog od januára 2007. Prvý krát som publikoval v roku 1998 v časopise Apológia (vtedajšia verzia dnešnej Obrany). Tento blog bol citovaný v odborných médiach mimo Slovenska v Iráne, Číne, Česku a naposledy v časopise Národnej univerzity pre verejné služby, Fakulta vojenských vied a výcviku dôstojníkov, Budapešť, v článku "Zmeny bezpečnostnej a obrannej politiky na Slovensku v posledných 20 rokov" Člen SFPA. Bývalý člen SDKU-DS. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    90
                
                
                    Celková karma
                    
                                                4.75
                    
                
                
                    Priemerná čítanosť
                    1051
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ministerstvo obrany SR
                        
                     
                                     
                        
                            Spravodajské služby
                        
                     
                                     
                        
                            Obranné plánovanie
                        
                     
                                     
                        
                            Bezpečnostná a obranná politik
                        
                     
                                     
                        
                            Medzinárodná politika
                        
                     
                                     
                        
                            Scenáre
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Slovenský rozhlas
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivan Zich
                                     
                                                                             
                                            Jan Sýkora
                                     
                                                                             
                                            Peter Papp
                                     
                                                                             
                                            Miro Tropko
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Slovenská spoločnosť pre zahraničnú politiku
                                     
                                                                             
                                            Ministerstvo obrany
                                     
                                                                             
                                            Centrum pre európske a severoatlantické vzťahy
                                     
                                                                             
                                            Hitechweb
                                     
                                                                             
                                            Genezis
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Obrana SR - časť 1
                     
                                                         
                       Ako šli Sovieti kradnúť nápady do Ameriky
                     
                                                         
                       Nie je sudca ako sudca: Ktorých sa treba obávať?
                     
                                                         
                       Ministerstvo obrany – porušenie zákona alebo hrubá nekompetentnosť?
                     
                                                         
                       Škandinávsky road trip: holandský Gröningen, nemecké Drážďany a cesta domov
                     
                                                         
                       Výberové konanie na automatickú pušku v ČR v kontexte zmluvy na dodávku CZ 805 BREN
                     
                                                         
                       Po hradoch - 10. Topoľčiansky hrad
                     
                                                         
                       Povyhadzovať tých darmožráčov zo SAV!
                     
                                                         
                       Dáte svoje dieťa na gymnázium alebo odbornú školu?
                     
                                                         
                       C-27J Spartan - priveľa otázok
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




