
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Culák
                                        &gt;
                Foto sekcia
                     
                 3. národné letecké dni Piešťany 

        
            
                                    14.6.2009
            o
            19:05
                        |
            Karma článku:
                11.96
            |
            Prečítané 
            4998-krát
                    
         
     
         
             

                 
                    Cez víkend som sa zúčastnil jednej vcelku vydarenej akcie, ktorá sa uskutočnila v neďalekých Piešťanoch. Jednalo sa už o tretí ročník Národných leteckých dní, nad ktorými prevzali podľa očakávaní záštitu príslušníci našich najvyšších politických špičiek - Róbert Kaliňák, Jaroslav Baška a Ivan Gašparovič. Tohto dvojdňového podujatia som sa, žiaľ, mohol zúčastniť iba v sobotu.  Aj keď by mal byť program obidva dni približne rovnaký, prišiel som týmto napríklad o takú "chuťovku", akou je iba nedelný prelet E-3 Sentry s dvojicou stíhačov. Hold smola, nabudúci rok si to budem musieť zariadiť lepšie... V nasledujúcich riadkoch vám priblížim svoje subjektívne dojmy z  tejto akcie a pridám aj zopár urobených fotiek.
                 

                 Môj sobotný výlet začal pomerne krátkou cestou zo Źiliny do Piešťan. Využil som ponuku posilnených vlakových spojov, ktoré organizátori zabezpečili špeciálne pre toto podujatie a pohodlne som docestoval. Už počas cesty vlakom som začal pociťovať skvelú atmosféru leteckých dní. Kde-tu bolo počuť rozhovory prvých nadšencov o letectve a o výkonoch a zložitých funkciách ich fotoaparátov. Po našom priblížení sa k Piešťanom bola táto atmosféra umocnená aj niekoľkými preletmi jednej z akrobatických skupín ponad náš vlak, ktorá nás svojimi letmi v presných formáciách akoby pozývala na letecký deň.   Odvoz návštevníkov z vlakovej stanice na letisko a späť zabezpečovala bezplatná kyvadlová doprava. Preto hneď po zastavení vlaku sme nasadli do už pripraveného autobusu a bez zbytočných strát času sme zamierili na letisko. Pre zhustenú premávku tvorenú stovkami osobných áut snažiacich dostať sa na letiskové parkovisko cesta trvala dlhšie, na všetkých dôležitých uzloch však bola prítomná polícia, ktorá plynulo riadila  premávku.   Ku vstupnej bráne letiska som sa dostal približne o 10-tej hodine a krátko po zakúpení vstupenky sa dozvedám od jedného z moderátorov programu, že počet návštevníkov už prekročil číslo 11 tisíc! Budem preto veľmi zvedavý na oficiálny údaj o celkovej návštevnosti, ktorá by mala byť podporená i priaznivým počasím a sľúbenou účasťou strojov z jedenástich krajín sveta.   Oficiálne odštartovanie tretieho ročníka Národných leteckých dní začalo krátko po desiatej hodine otváracími príhovormi a ďalšími oficialitami, ktoré tradične sprevádzajú každé takéto podujatie. Ozvučenie areálu bolo výborne "zmáknuté" a preto som všetko zreteľne počul aj na väčšiu vzdialenosť od hlavného pódia. Mohol som sa tak venovať foteniu a prehliadke odstavenej techniky bez obavy, že mi niečo utečie. Začal som teda hneď z kraja a tu sú moje prvé fotografie:    (Na Národných leteckých dňoch boli vystavené aj niektoré exponáty patriace Vojenskému historickému múzeu Piešťany, ktoré ešte pred niekoľkými rokmi slúžili v našich ozbrojených silách. Ak to bolo pre niekoho málo, mohol využiť ešte možnosť voľného vstupu do tohto múzea počas celého víkendu. Na tejto fotke "pózuje" odstavený MiG-21.)    (Ďalší dnes už len múzejný exponát - bývalý slovenský Su-22.)    (Francúzske farby hájila na leteckom dni Mirage 2000D.)    (L159 Alca je ľahký podzvukový viacúčelový letún vyrábaný v susednej Českej republike. Okrem statických ukážok ho mohli diváci obdivovať aj v pútavých preletoch.)    (Momentálna pýcha českého letectva - jeden z prenajatých strojov Jas-39 Grippen.)    (Tak s rozpoznaním tohto stroja nemala problém určite drtivá väčšina návštevníkov leteckého dňa a pre mňa bol jedným z najkrajších exponátov. MiG-29 v slovenských farbách.)    (MiG-29 v pozadí s cisternou po dotankovaní paliva.)    (Univerzálny ťahač leteckej techniky Talet 30. Je vyrábaný slovenskou firmou WAY INDUSTRY, ktorá je okrem iného výrobcom aj odmínovacieho zariadenie Božena.)    (Obdiv vzbudzoval i americký transportný letún C-130E Hercules. Zanedlho po vytvorení tejto fotky bol zvedavcom umožnený vstup do jeho útrob, čo sa z diaľky dalo poznať podľa dlhokánskej fronty.)   Jedným z reklamných partnerov podujatia bola aj piešťanská letecká škola Seagle Air. Spolu s českou leteckou školou F-air tu vystavovali na obdiv svoje ĺahké cvičné lietadlá:            Hneď vedľa uvedených cvičných strojov leteckých škôl sa nachádzala ďalšia vojenská technika. Záchranársky Mi-17 zaradený do slovenského systému Search and Rescue v nápadnej čiastočne žltej kamufláži a transportný L-410 Turbolet českej výroby v slovenských farbách.            Po odfotení transportného L-410 sa Národné letecké dni oficiálne začali i na piešťanskom nebi. Prvá dvadsaťminútovka patrila výlučne slovenským strojom. Vzbudzovala obdiv u starších návštevníkov a neskrývané nadšenie u malých detí. V priestore letiska som zaregistroval tri veľkoplošné obrazovky, na ktorých bolo zobrazované to najdôležitejšie, čo sa práve dialo a kvalitné ozvučenie akcie som už spomínal v úvode. Toto spolu so zaujímavými informáciami od moderátorov programu, občas prerušovanými revom leteckých motorov, prezrádzalo profesionálnu prípravu celej akcie.   Najväčším zážitkom z tejto časti programu bol podľa reakcií môjho bezprostredného okolia prelet veľkých transportných lietadiel (Jak-40, Tu-154) a ohlušujúci rachot dvojice motorov MiGov-29. Pre silný bočný vietor, ktorý komplikoval život organizátorom i účinkujúcim, hrozilo, že sa neuskutoční plánovaný výsadok vojakov 5. pluku špeciálneho určenia zo Žiliny nad piešťanským letiskom. Nakoniec bol však zoskok povolený a vojaci vysadení z transportného L-410 "doplachtili" do miesta určenia pred hlavnou tribúnou s minimálnymi odchýlkami. Počas ich presného dosadania sprevádzaného potleskom divákov som si na chvíľu s fotoaparátom odbehol k ďalším exponátom piešťanského múzea. Tentoraz som si na mušku vzal americké nákladné vozidlá z obdobia druhej svetovej vojny:            Neďaleko týchto vozidiel sa nachádzalo zopár príslušníkov Slovenských ozbrojených síl pôsobiacich v medzinárodných silách EUFOR a tak mi to nedalo nevyfotiť si ich. :) Za nimi v pozadí fotografie sa rozprestieralo vojenské "mestečko" pozostávajúce zo stanov či pospájaných buniek pozakrývaných maskovacími sieťami (vľavo vzadu). Okrem priestorov pre zúčastnených vojakov sa tam nachádzali aj rôzne lákadlá pre bežného návštevníka, ako napríklad poľná pekáreň s možnosťou ochutnávky práve upečeného pečiva či regrutačné stredisko.      Po preskúmaní aj tejto časti areálu som sa s niekoľkými letákmi v ruke, určenými na neskoršie preštudovanie, vrátil k pristávacej dráhe, nad ktorou práve dokončieval pilot MiGu-29 svoju ukážku z vyššej pilotáže a vychutnával si ďalšie letové ukážky. Tie sa obecenstvu práve chystal predviesť český pilot práve štartujúceho Grippenu. Toho po sérii krásnych manévrov vystriedala mladá chorvátska akrobatická skupina Krila Oluje (Krídla búrky), prvý krát verejnosti predstavená iba v roku 2004. Vydarené a pohľadné manévre akrobatických pilotov lietajúcich na šiestich strojoch Pilatus PC-9 komentoval po anglicky chorvátsky generál a jeho výklad bol následne prekladaný do slovenčiny.   V ďalšej časti programu mali diváci možnosť vidieť vo vzduchu dve české L-159 pomocou pripravenej pirotechniky "útočiace" na letisko, rakúsky Eurofighter Typhoon a holandskú F-16 v nápadnej oranžovej kamufláži. Zrejme pre určitý časový sklz, do ktorého sa organizátori dostávali, bol počas letovej ukážky F-16 pred hlavnou tribúnou zároveň rozostavaný a okomentovaný protilietadlový raketový systém a ženijná jednotka.   Po tejto ukážke, kedy sa k štartu pripravovala netrpezlivo očakávaná americká obojživelná Catalina, som zamieril do severnej časti areálu, kde bola umiestnená už poskladaná protilietadlová jednotka a ženisti. Za burácania dvoch motorov Cataliny som sa pustil do fotenia, s ktorým som musel počas letových ukážok prestať, lebo môj fotoaparát fotenie rýchlych lietadiel vo výškach uspokojivo nezvládal. :)    (Odmínovací systém Božena 4 vyrábaný slovenskou firmou WAY INDUSTRY.)    (Božena sa zatiaľ predstavila vo svete ako úspešný výrobok spoľahlivo nasadzovaný vo viacerých častiach sveta.)    (Ženijná verzia Aligátora s možnosťou detekcie kovov a so súpravou pre podrobný ženijný prieskum. Aligátor je taktiež produktom slovenskej výroby.)    (Aligátor - ženijná verzia.)    (Interiér tohto obrneného vozidla bol veľkým lákadlom pre malých či veľkých návštevníkov.)   Ukážky raketovej protilietadlovej techniky z Nitry:                Medzitým čo som fotil ženistov a protilietadlovú obranu, začala pred hlavnou tribúnou ukážka zabezpečeného prevozu peňazí, na ktorý zo vzduchu dohliadala dvojica bojových vrtuľníkov Mi-24. Tento konvoj bol prepadnutý zamaskovanými ozbrojencami, ktorí boli však po krátkom čase úspešne zneškodnení. Následne dostával návštevníkov do údivu vynikajúci maďarský akrobat Zoltán Veres lietajúci na špeciálnom stroji Extra-300S. Ešte pred jeho vystúpením  však prebehol prepad historického  vlaku s "ozbrojenou" posádkou z klubov železničnej histórie z Bratislavy a Vrútok, na ktorý útočil pilot  lietadla Let C-11 (Jakovlev Jak-11) z Českej republiky. V tomto čase som bohužiaľ už musel pomýšľať na odchod a tak prelety slovenskeho L-39 a poľskej akrobatickej skupiny Orlík som sledoval iba cestou na železničnú stanicu.   Celkovo som mal z tohto príjemne stráveného sobotného dňa pozitívne pocity. Mal som radosť z pekného počasia, z množstva lietadiel až z 11-tich krajín (aj keď nevidenie srbského SOKO G-2 Galeb pre nedostatok času ma dosť mrzí) a z dobre vykonanej práce organizátorov. Tú však v mojich očiach znížilo nešťastné umiestnenie hlavnej tribúny a VIP parkoviska! Tí, ktorí boli na tejto akcii vedia, o čom píšem a ostatným sa to pokúsim vysvetliť.   Na takýchto akciách býva obvykle určená jedna letisková dráha a okolo nej sa z jednej strany za zábranami zhromažďujú diváci. Aj v Piešťanoch to bolo tak, ibaže zhruba od polovice línie vyhradenej divákom až do tri štvrte sa rozliehalo VIP parkovisko a hneď vedľa neho tribúna pre rečníkov, komentátorov a hostí. Pri letových ukážkach takéto rozmiestnenie neprekážalo nikomu, pretože letiace stroje bolo vidno dobre skoro vždy. Problém však nastal, keď sa konali "pozemné" ukážky, napr. rozloženie protilietadlového systému, bojové umenie vojakov, prepad kolóny prevážajúcej peniaze atď. Toto všetko sa konalo výlučne pred hlavnou tribúnou a veľká časť návštevníkov z toho nemala nič, pretože na takéto veci veľkoplošná obrazovka skutočne nestačí.   Na zlepšenie tejto situácie v budúcich ročníkoch by podľa mňa stačilo HLAVNE odsunúť nezmyselne postavené parkovisko, ktoré návštevníkom zaberá dobré miesta na sledovanie a presunúť hlavnú tribúnu oproti "diváckej línii" na druhú stranu letiskovej dráhy. Návštevníci by boli tým pádom menej roztiahnutí po dĺžke dráhy a videli by oveľa viac i z dynamických ukážok na zemi ako aj z diania na tribúne. Dúfam, že do budúcna sa toto zmení a ďalší ročník bude už len o príjemných zážitkoch.   A nakoniec mi nezostáva nič iné, ako aj týmto spôsobom vyjadriť svoju vďaku za inak výbornú prácu organizátorov  a skvelé výkony všetkých zúčastnených. ĎAKUJEM! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Procházka - Lipšic  (1:0)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Nechcem budovať silný sociálny štát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            [Foto] Večerné slnko na večernej tráve
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            Hľadá sa dno. Pravicové.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Culák 
                                        
                                            [Foto] Auschwitz I, Auschwitz - Birkenau
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Culák
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Culák
            
         
        culak.blog.sme.sk (rss)
         
                                     
     
        Životné motto: Nič na na svete nie je čiernobiele.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4868
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Foto sekcia
                        
                     
                                     
                        
                            Postrehy a zaujímavosti
                        
                     
                                     
                        
                            Vojenské letectvo SR
                        
                     
                                     
                        
                            Vojenské letectvo ČS od 1945
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Bohatství národů (Adam Smith)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Aliancia Fair-play
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Milan Barlog
                                     
                                                                             
                                            Peter Paulík
                                     
                                                                             
                                            Eduard Kladiva
                                     
                                                                             
                                            Stanislav Thomka
                                     
                                                                             
                                            Milan Hraško
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Veronika Bahnová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




