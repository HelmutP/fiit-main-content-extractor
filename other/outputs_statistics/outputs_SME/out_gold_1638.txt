

                                          POLOHA A TURISTICKÉ INFO 
 Ak ste už niekedy išli do Maďarska cez jeden z hraničných prechodov v Slovenskom Novom Meste, netreba vám nič vysvetľovať. Akonáhle dorazíte do Sátoralyaújhelyu, namierte si to na mestskú výpadovku a po cca 7-8 kilometroch jazdy po hlavnom cestnom ťahu dorazíte na predmestia Sárospataku. Stačí postupovať ďalej po hlavnej (Rákocziho) ulici, kým sa neocitnete v centre (spoznáte to podľa zhluku menších obchoďákov po vašej pravici a neďalekej veže farského kostola, ktorá sa pre zmenu objaví vľavo). Nájdite si dobré miesto na parkovanie a je to. 
   
 Ospravedlňujem sa za nižšiu kvalitu, ale musel som sa uspokojiť s týmto detailom na mapu mesta na jednom z infopanelov v centre. Šípka s chodidlami ukazuje ideálnu východziu polohu na Rákocziho ulici, hlavnej dopravnej tepne mesta. Do Starého mesta je to na skok, stačí zahnúť do Ul. kráľa Bela alebo Ul. sv. Alžbety Uhorskej a už ste pod hradbami, neďaleko farského kostola. Od neho už stačí ísť rovno za nosom, konkrétne ďalej po "Svätoalžbetskej" (59, 47), až k hradnému areálu (50). Pohľad na staré mesto a časť hlavnej ulice (po ktorých sa budeme pohybovať) si môžete pozrieť z vtáčej perspektívy aj na tejto fotke. 
 Sárospatak a jeho centrum, staré mesto a mestský hrad nájdete aj na Google Maps.  
   
                             HISTÓRIA, POPIS A STAVEBNÝ VÝVOJ HRADU   
 Lokalita rodiaceho sa mesta Sárospatak a jeho neskoršieho hradu bola v 11. storočí (a po väčšinu 12.) ešte kráľovským majetkom. Samotný Sárospatak alias Patak (Blatný Potok/Potok) sa v 14. a najmä v 15. storočí stal významným a bohatým regionálnym centrom. Od cca pol. 14. stor. sa delil so starým stoličným hradom v Zemplíne o funkciu správneho centra celej Zemplínskej stolice (župy). Sárospatak bol jej de facto metropolou, a tiež jediným sídlom v celej stolici, ktoré malo štatút a kompetencie slobodného kráľovského mesta. 
 Presný časový údaj o založení hradu nepoznáme, no je isté, že ho začali stavať relatívne skoro po tatárskom vpáde, niekedy koncom 50. rokov 13. storočia. Ako u mnohých hradov, aj u tohto bola najprv postavená obytná veža. Stála na mieste dnešného jadra hradu, tvoreného Vörös torony („Červenkavou vežou"), ktorá bola postavená až v 16. storočí. Pôvodná obytná veža z 13. storočia bola základom budúceho jadra hradu, o čom svedčia aj nálezy jej podzemných základov. V období jej dokončenia sa začalo s prácami na výstavbu niekoľkých obranných veží. Dokladá to darovacia listina comesovi Michalovi, synovi Ubula, z roku 1262, v ktorej bola potvrdená donácia práve budovanej veže (nachádzajúcej sa v okolí dnešného farského kostola). 
  
 Kráľ Belo IV. počas svojho pobytu na potockom hrade v roku 1250 spísal okrem iného aj list určený pre vtedajšieho pápeža Inocenta IV., v ktorom ho oboznámil aj s prebiehajúcou výstavbou nových hradov v povodí Dunaja. Bola to prvá návšteva Bela IV. v Sárospataku a dokázateľne vydal krátko nato nariadenie na výstavbu obytnej veže na hrade. Neskôr prišiel rad aj na vybudovanie viacerých menších obranných veží v jej blízkosti. Jedna z nich, stojaca v severnej časti hradu, bola sprvu nedokončená a v roku 1262 ju dostal na starosť comes Michal, syn Ubula, aby dozrel na jej dostavbu. Táto veža sa svojou veľkosťou a odolnosťou ani len neblížila kráľovskej obytnej veži. Michal však okolo roku 1285 začal s výstavbou svojho nového bývania v Kérsemjéne, takže na zverenú vežu nakoniec neprišiel rad. Neskoršie záznamy ju ani nespomínajú, pravdepodobne preto, lebo nikdy nebola postavená. Proti tejto hypotéze vypovedá Angieliniho pôdorys z roku 1603. Po roku 1300 vlastnil hrad istý čas rod Baksa (Bakša, resp. Bokša). Karol Róbert (už ako etablovaný uhorský kráľ) získal potocký hrad výmenou za sečovské panstvo v roku 1321. V kráľovských rukách bol celý komplex až do roku 1391, kedy ho Žigmund Luxemburský daroval Mikulášovi (Miklósovi) Perényimu, banátskému bánovi, spolu s mestom a mýtnymi právami. Potom, čo Perényi v roku 1429 zomrel a nezanechal dediča, poputoval mestský hrad naspäť do rúk koruny. V roku 1436 ho kráľ udelil ostrihomskému arcibiskupovi Jurajovi (Györgyovi) Páloczymu (Pavlovskému). 
 V roku 1440 zabrali hrad husitské (resp. bratrícke) vojská pod vedením Petra Aksamita. V roku 1459 ho získali späť spojené oddiely Rozgonyiho a Héderváryho (radcov Mateja Korvína) s pomocou jágerského arcibiskupa Blažeja, zvaného Maďar (Magyar Balázs). Mesto a hrad prešli do rúk pánov z Pavloviec (Pálóczyovcov). Kráľ Matej v liste z roku 1465 dal Ladislavovi z Pavloviec právo plne opevniť svoju kúriu v Potoku na spôsob hradu - pevnými kamennými hradbami a vežami. Tento šľachtický dvorec so zosilneným opevnením stál s najväčšou pravdepodobnosťou medzi dnešnými ulicami Ferenca Dobóa a Mátyása Szuhaya, v niekdajšom severnom cípe mesta. Potocký mestský hrad však v tomto období ešte dokázateľne patril do kráľovského vlastníctva. V neistých časoch po smrti Mateja Korvína ho však páni z Pavloviec nakoniec prevzali do svojich rúk a pristúpili k modernizácii a prestavbám hlavnej obytnej veže. Dvadsať rokov spokojného vlastníctva za dynastie Jagelovcov však nemali trvať dlho. Jedným zo šľachticov, ktorí prišli o život v bitke pri Moháči, bol aj Anton z Pavloviec, potomok Ladislava. Rod Pavlovských takto vlastnil miestne majetky prakticky až do chronologického konca uhorského stredoveku (1526). Hrádok Pálóczyovcov zanikol s najväčšou pravdepodobnosťou v roku 1528, kedy bolo za ťažkých bojov vážne poškodené aj mesto a hrad.  
  
 Po Páloczyho smrti zabral potocký hrad vtedajší sedmohradský vojvoda, Peter Perényi (Perínsky). Takto sa Sárospatak spolu s hradom stal dedičným lénom rodu Perényiovcov. Dopomohlo im k dosiahnutiu ich najväčšieho mocenského a majetkového rozmachu v 16. storočí. Uhorská koruna a korunovačné klenoty boli v post-moháčskom chaose dočasne presunuté a uschované v bezpečí neďalekého füzérskeho hradu. Smeli byť vydané Ferdinandovi I. Habsburskému len pod dohľadom zastupiteľov Sátoralyaújhelyu a Sárospataku. Prestavbu a modernizáciu hlavnej obytnej veže inicioval Perényi vo svojom liste košickej mestskej rade z roku 1534, v ktorom žiadal zástupcov slobodného kráľovského mesta o skúsených staviteľov. 
 Už predchádzajúci vlastník, Ladislav z Pavloviec (Pálóczy), bol kvôli chaotickej situácii po smrti Mateja I. Korvína (zač. 90. rokov 15. stor.) donútený pristúpiť k celkovej modernizácii hradu, vrátane obytnej veže. Do tejto éry stavebných úprav sa dá pravdepobne zaradiť aj nadpis nájdený na vrchnej časti rámu jedných pôvodne zamurovaných dverí, odkazujúci na rok 1506. Svojskou črtou Sárospataku medzi zemplínskymi hradmi je fakt, že z pôvodnej gotickej stavby sa do dnešného dňa zachovalo len minimum. Potocký hrad nikdy nemenil polohu, no zato v priebehu niekoľkých storočí výrazne zmenil podobu. Zďaleka najväčšou prestavbou prešiel v 16. storočí. Gotický donjon uvoľnil miesto novej štvorhrannej obytnej veži talianskeho typu, ktorá bola malým hradom sama o sebe. Od 18. storočia sa pre ňu vžil názov Vörös torony („Červenkavá veža"). Pristavaných bolo viacero obytných krídiel, ktoré utvorili dve úplne nové hradné paláce. Staršie opevnenia nahradili novšie renesančné fortifikácie s pravidelnejšími, geometrickejšími tvarmi. Dnešná Červenkavá veža bola teda založená najskôr v druhej polovici 15. storočia, ale s istotou ešte pred rokom 1534. 
  
 Väčšina Perényiho veľkorysých stavebných úprav sa uskutočnila v rokoch 1534 - 1541 pod vedením talianskeho architekta Alessandra Venada. Tieto boli prvým krokom k úplnej transformácii hradu zo stredovekej pevnosti na renesančnejší typ stavby. O rýchlom postupe prác svedčí aj fakt, že počas mierových rokovaní medzi kráľom Ferdinandom Habsburským a Jánom Zápoľským v roku 1537 už mesto a hrad obkolesoval novovybudovaný systém hradieb. Priestor okolo obytnej veže navŕšili a dovtedy prízemné strielne obranných chodieb sa ocitli pod povrchom zeme. Viditeľnými sa opäť stali až počas archeologického prieskumu a reštauračných prác na hrade, uskutočnených v druhej polovici 20. storočia. Výstavba paláca na podnet Petra Perényiho postupovala od severných častí opevnenia smerom k Vörös torony. Aj keď Perényi zomrel v roku 1542, palác krátko nato dostavali podľa jeho pôvodných predstáv, čo odráža aj názov jednej z jeho častí, zvanej „Perényiho krídlo" (Perényi szárny). Okolo Červenkavej veže bolo postavené opevnenie renesančného typu, konkrétne pravidelný hviezdicovitý mnohouholník s geometricky rovnými baštami. Na takto vzniknutý vnútorný hrad nadväzovali opevnenia vonkajšieho, ktorý mal približne obdĺžníkovitý pôdorys s rozmermi 400 x 200 m. Hradby boli v pravidelných rozostupoch vybavené zaoblenými rohovými baštami a suchou hradnou priekopou. V roku 1541 bola dokončená výstavba povyše Bodrogu stojacej Vodnej brány (Vízikapu), ozdobenej rodovým erbom Perényiovcov. 
 Po smrti Jána Zápoľského sa Perényi stal prívržencom Ferdinanda I. V roku 1542 si však Peter pozval na hrad viacerých vysokopostavených členov domácej šľachty a s mnohými uzatvoril tajné spojenectvo. Ferdinandova odozva na seba nenechalo dlho čakať. Panovník, rozladený už od dávnejších prípadov Perényiho paktovania s Turkami, odsúdil svojhlavého vojvodu na šesť rokov väzenia v Nových Zámkoch. Perényi zomrel krátko po návrate domov v roku 1548 a ohromné rodinné vlastníctvo zdedil jeho syn Gabriel (Gábor). Pokračoval vo výstavbe a dohliadal na ďalšie vylepšovanie opevnení a architektonické skrášľovanie komplexu. Gabriel Perényi zomrel bez dediča v roku 1567.  
  
 Hrad po odchode Perényiovcov získal vtedajší kráľ Maximilián Habsburský, syn Ferdinanda I. O správu sa starala Spišská komora. Kráľ si hrad dlho neponechal, už v roku 1573 daroval potocké panstvo za sumu 100 000 florénov príbuzným Istvána (Štefana) Dobóa z Ruskej, už nebohého tekovského župana a hrdinu obrany jágerského hradu pred Turkami. Príbuzenstvo reprezentoval Dobóov syn Ferenc (František) a jeho najbližšia rodina. Dobó mladší minul na údržbu a opravy hradu nemalé peniaze. O niekoľko rokov neskôr ohodnotil cisár Rudolf Habsburský potocký hrad už na rovných 240 000 florénov. Odkázal ho spolu s ďalšími majetkami Dobóovým potomkom, Žofii Perényiovej a jej dedičom. Po niekoľkých pokojných rokoch sa v 1605-om zmocnil hradu Štefan Bočkaj. Po porážke Bočkajovho povstania získala hrad v roku 1608 do zálohu rodina Dobóovho  dediča, Michala Lórántffyho. Prostredníctvom vena Lórántffyovej dcéry Zuzany sa hrad v roku 1617 ocitol vo vlastníctve Juraja I. Rákocziho. Ten spravil zo Sárospataku sídelné mesto svojho rodového panstva. 
  
 Práve k Rákoczimu sa viaže ďalšia významná fáza stavebného vývoja hradu a obdobie najväčšieho rozkvetu mesta v 17. storočí. Prvé menšie úpravy sa objavili už v rokoch 1617-1618, kedy východné krídlo hradného paláca získalo nadstavbu v podobe ďalšieho poschodia. V roku 1628 bol spevnený severný úsek mestských hradieb pri farskom kostole (uvidíte ho aj na fotkách). V rámci novej výstavby započatej v roku 1631 bolo pozdĺž vonkajších hradieb vybudovaných niekoľko nových bášt. Ukončila sa výstavba novej vstupnej brány a kazemát vnútorného opevnenia, obkolesujúceho Červenkavú vežu. V priestore pred mnohouholníkovou hradbou sa tiahol druhý prstenec obranných múrov a vzájomné prepojenie oboch častí spojovacími hradbami utváralo komplexný fortifikačný systém. V rámci vonkajších hradieb bola jedna z menších rohových obranných stavieb prestavaná na baštu (staro)talianskeho typu, s murivom hrubým 5 metrov a zaklenutými strielňami pre delostrelecké stanovištia. V rovnakom období napredovala aj výstavba radu delostreleckých kazemát v juhovýchodnej časti mestských hradieb. Severozápadná rohová bašta a vnútorná obranná chodba boli vybavené zaklenutými strielňami (chodba ich mala 15). Vnútri sa z osamotených otvorov o výške 110 cm a šírke 60-90 cm vetvili smerom vonku a dole tri až štyri strieľne. Ústili  v otvoroch vonkajších hradieb o šírke 20 cm, nachádzajúcich sa v približne metrovej výške nad pôvodnou úrovňou okolitého terénu. V roku 1642 bolo pridané jedno nové poschodie aj na južnom krídle hradného paláca a v roku 1647 bola dokončená honosná Lorántffyho loggia. V rokoch 1656 - 1658 prešla významnou úpravou samotná Červenkavá veža. Vybudovalo sa v nej viacero nových delostreleckých stanovíšť, úplne nové horné poschodie zakryté ihlanovitou strechou (a vybavené menšou batériou diel). V jednotlivých rohoch strechy boli postavené strážne vežičky, ktoré dodali stavbe jej charakteristický vzhľad, ktorý sa zachoval dodnes. Zo severovýchodnej rohovej bašty vyčnieva tzv. „pavlač Sub-Rosa" (Sub-Rosa Erkély), taktiež postavená niekedy v 17. storočí. Meno dostala podľa štylizovanej maľby ruže umiestnenej vo vrchole klenby, zastrešujúcej pavlač. Nepatrné zvyšky západného okraja vonkajších hradieb existujú v blízkosti meštianskych domov z 19. storočia na Rákocziho ulici. Približne v tejto oblasti sú ešte čiastočne rozpoznateľné ruiny tzv. „Levej bašty" (Oroszlán bástya), vybudovanej v rozmedzí rokov 1631 - 1648.  
  
 Po smrti Juraja Rákocziho zdedil hrad František I. Rákoczi. Jeho manželka, Helena Zrínska, sa spolu so svokrom Petrom Zrínyim podieľala v roku 1670 na sprisahaní palatína Wesselényiho. Vyšlo to však najavo a Františka Rákocziho sa podarilo zachrániť pred trestom smrti len vďaka kaucii v hodnote 400 000 florénov. Majetky Rákocziovcov boli zhabané a v roku 1671 obsadilo hrad v Sárospataku cisárske vojsko. O niečo neskôr sa na krátky čas stal majetkom Imricha Thökölyiho (prostredníctvom manželstva s Helenou Zrínskou), no onedlho nato ho obsadili cisárske oddiely pod vedením generála Capraru. Jedno z mnohých šľachtických povstaní nepokojného 17. storočia vypuklo v roku 1694 pod vedením Ferenca Tokaiho (Františka Tokajského), ktorému sa podarilo získať na istý čas aj potocký hrad. Povstanie potlačil a mesto vydobyl naspäť vojvoda Vaudemont, krvavo sa odplatiac vzbúrencom. Kráľ Leopold I. v roku 1702 nariadil vyhodiť hrad do vzduchu, keďže už stratil bezprostredný vojenský význam a jestvovali obavy, že padne do rúk ďalším potenciálnym povstaniam. Demolácia sa však veľmi nevydarila, vážne poškodené boli len obytné krídla a zničená jedna z rohových vežičiek Červenkavej veže. A ako by to nestačilo, o rok neskôr sa hradu zmocnili príslušníci posledného stavovského povstania v uhorských dejinách... 
  
 František II. Rákoczi štedro dotoval urýchlenú rekonštrukciu vnútorného hradu a s obľubou sa zdržiaval v Sárospataku. Počas jeho niekoľkoročnej okupácie tejto časti Uhorska sa miestni stavebníci postarali najmä o dôkladnú opravu kaštieľovej časti komplexu. V novembri 1708 sem knieža zvolal zemský snem. Za zradu dal odsúdiť na smrť Istvána Bezerédyho a jeho švagra Ádáma Botku, ktorých potom popravili na vnútornom nádvorí. 10. októbra 1710 sa Rákoczi vydal do Užhorodu a hrad zveril do rúk Antonovi (Antalovi) Esterházymu. O niekoľko dní však hrad obsadili bez použitia sily Rakúšania. Od tohto obdobia bol hrad vlastníctvom rakúskej pokladne/finančnej komory, kým ho v roku  1720 nezískal vojvoda Donát(o) Trautson, ktorý sa pustil do rekonštrukcie zanedbaného hradu. Po zvyšok svojej novovekej histórie bol hrad prideľovaný už len šľachte rakúskeho pôvodu. V roku 1737 spustošil mesto aj hrad ničivý požiar. V roku 1776 sa hradný komplex opäť dostal pod správu štátnej pokladnice a v roku 1806 sa jeho novým vlastníkom stal Augustín (Ágost) Bretzenheim, ktorý chcel dať zrekonštruovať budovu hradného paláca, no zabránila mu v tom predčasná smrť. V prestavbe pokračoval jeho dedič Ferdinand Bretzenheim. V 19. stor. sa uskutočnili tri významnejšie úpravy, konkrétne podľa plánov Henricha Kocha (1829), Jeana Romana (1841) a Győzőa Zieglera (1891). Od roku 1875 hrad dedične vlastnila rodina Windischgrätzových. V roku 1945 prešiel do rúk štátu a neskôr bol premenený na múzeum. V súčasnosti tu sídli pobočka Maďarského národného múzea v Budapešti.  
    
 Pohnutá a pestrá história hradu v Sárospataku svojským spôsobom odzrkadľuje aj dejiny niekdajšej Zemplínskej a Abovskej župy (v ich slovenskej aj maďarskej časti). Vystriedala sa na ňom väčšina rodov a historických osobností charakteristických pre toto územie, prežil ambiciózne začiatky v ére po tatárskom vpáde, rozkvet mesta za Anjouvcov v 14. storočí,  bratrícke nájazdy a okupáciu v 15. storočí, boje o moc v 16. storočí, kompletnú prestavbu v čase najväčšej slávy Perényvcov, stavovské povstania v 17. storočí, premenu na ospalé letné sídlo, národno-vlasteneckú ikonu a neskôr múzeum... 
 Skôr než sa vyberieme k hradu, pozrieme si (už tradične) jeho plán. Pôdorys som, na rozdiel od predchádzajúcich článkov, nevedel nikde zohnať... Takže sa musíme uspokojiť len s ofoteným obsahom infopanelov stojacich neďaleko brány do hradného areálu : 
   
 Celkový plán hradného komplexu a priľahlého okolia (všimnite si park a hospodárske budovy vo vonkajšom hrade a celkom v pozadí rieku Bodrog). 
  
 No, a tuhľa máte celkom fešný 3D-plánik vnútorného hradu. :-) Dúfam, že čísla sú ešte dobre viditeľné (ak nie, dajte mi vedieť a napojím na fotku link na verziu s väčšími rozmermi). 
 Vysvetlivky : 
 1. - pokladňa a informácie 
 2. - Vörös torony ("Červenkavá veža") 
 3. - vchod do Rákocziho expozície 
 4. - balkón Sub Rosa 
 5. - Konyha bástya (Kuchynská bašta) 
 6. - expozícia gotických kamenárskych prác 
 7. - expozícia renesančných kamenárskych prác 
 8. - Lorántffyho loggia 
 9. - WC 
 10. - kaviareň 
 11. - kazematy 
 12. - "tu stojíte"  
 Prípadne je tu aj tento kreslený pohľad na jadro hradu z vtáčej perspektívy, ktorý som našiel na nete : 
  
 Nuž teda, poďme.  
  
 Pokochajte sa staršími domčekmi z 19. storočia na Rákocziho ulici... 
  
 ...a zabočte na ulicu sv. Alžbety. O malú chvíľočku sa objavia zachované úseky mestských hradieb pri farskom kostole : 
  
  
 Toto bol kedysi okraj mesta Sárospatak... 
  
  
  
 Hradby, kedysi strategicky pristavané o okraj kostola, už dávno nahlodali vysekané prechody pre peších alebo automobily... 
  
 Mnohé z bývalých obranných múrov sa doteraz vinú naprieč záhradkami a zadnými dvormi rodinných domov... :-) 
  
 No, a tu máte farský kostol Nepoškvrneného Počatia v plnej kráse. Pôvodne neskorogotický kostol dokončený v roku 1537, čiastočne prestavaný v rokoch 1787 a 1802. Môžete si ho pozrieť aj tu. V čase vzniku tejto fotky práve prebiehali rekonštrukčné práce na jeho vonkajších stenách. 
  
  
 A ako vidíte na menšom pútači nad vchodom do lode - v inkriminovanom čase sa blížilo 800. výročie narodenia sv. Alžbety Uhorskej... 
  
  
 ...ktorá tu má aj sochu... ;-) 
  
  
 Staručké domčeky na ulici sv. Alžbety... 
  
 ...chodník vedúci k nábrežiu Bodrogu a prežívajúcim častiam vonkajších mestských hradieb... 
  
 ...bývalý župný úrad, dnes sídlo mestskej galérie.  
   
 Tak, a sme tu. :-) Vstupná brána do hradného areálu a bývalá hospodárska budova...  
  
  
  
 A teraz "kaštielne" krídla hradu z 18. a 19. storočia, prestavané z pôvodných krídel a traktov hradného paláca : 
  
  
  
  
  
  
  
   
 Vstúpte.  
   
 Pri vchode nás vítajú dve levie sošky z 19. storočia : 
  
  
  
 A sme na nádvorí... 
  
 Východné krídlo. 
  
 A pri ňom mierne vyčnievajúca bašta s týmto delom z cca 17. storočia (nie som si istý, či ide o repliku). 
  
  
  
 Letmý pohľad na prízemné časti Vörös torony. Ale ešte chvíľku vydržte, najprv dopozeráme nádvorie. ;-) 
  
 Pohľad na budovu starého skladiska za kazematami. 
  
 Pokračujeme v prechádzke nádvorím... 
  
  
  
 Lorántffyho loggia a schodisko so stĺporadím vedúce z nádvoria na horné poschodie Vörös torony. 
  
  
 A teraz už samotná Vörös torony v celej svojej majestátnej výške... :-)  
  
 Hodinový mechanizmus bol pridaný do jednej z vežičiek niekedy v 19. storočí. 
  
 To, že túto stavbu navrhol talianský architekt, je vidieť už na prvý pohľad. Vörös torony mi trochu pripomína budovy v Assissi... :-) 
  
  
  
  
  
 Vchod do expozície renesančných kamenárskych prác. 
  
  
 Mostík vedúci k hlavnému vchodu do Vörös torony... 
  
 ...a dobre zachovaný, no horšie čitateľný rodový erb. 
  
 Miesto, kde sa drsnosť múrov pevnosti snúbi s eleganciou obytných krídel... :-) 
  
 Vchod do expozície gotických kamenárskych prác. 
  
 No, a teraz vyjdeme na "zadný dvor" a prejdeme sa po kazematách vnútorného hradu... 
  
  
  
 Všímnite si prízemné časti a základy Vörös torony... 
  
 ...strielne a otvory pripomínajúce vetrací systém... 
  
 ...niekdajšiu kamennú nádrž, pravdepodobne na dažďovú vodu. 
  
 Masívne a mierne megalomanské ? Sedí vec. 
  
 Aby sme sa však nekochali len obytnou vežou... Z hradieb sú výborné výhľady po okolí - na vonkajšie prstence opevnení, obopínajúce hrad a staré štvrte mesta : 
  
  
  
  
 Na bašte sa nájde dostatok miesta pre stromy i relax... :-) 
   
  
  
 Dojem z hrdo sa vypínajúcej Vörös torony kazí len zlovestne nahlodaný roh jej strechy - miesto zaniknutej štvrtej vežičky, zničenej už spomínaným výbuchom v roku 1702.  
   
  
 Výhľady na Staré mesto a hradby však nie sú jediné panorámy, ktoré stoja zato - pohľad na brehy Bodrogu je fakt malebný : 
  
  
  
  
 Pekný dojem trochu kazia menej odborné konzervačné práce na niektorých múroch, vykonané v predchádzajúcich desaťročiach. :-/  
  
 Ale hlavu hore ! Prítomnosť Vörös torony si to priam pýta ! ;-) 
  
  
  
  
 Základy skladiska pristavaného k vnútornej strane kazemát. Vraj to mohol byť sklad munície... 
  
  
 "Ozubené" základy veže sa kŕčovito zahryzávajú do terénu...  
  
  
 ...ozdobné kry drzo rastú vo výklenkoch na okraji ochodze... 
  
  
  
  
 ...a Vörös torony sa mi nie-a-nie vojsť do hľadáčika kvôli svojej úctyhodnej veľkosti... :-)  
  
 Hľadíme smerom k miestu, kde sme začínali na nádvorí.  
   
 Z ochodze sa konečne otvoril poriadny pohľad na východné krídlo hradu...  
  
 ...a pomerne clonený, no stále viditeľný výhľad na doďaleka sa tiahnuce mestké hradby...  
   
 Zajtra sa vrátime naspäť na slovenskú stranu Zemplína, kde konečne začneme naše putovanie po málo známych alebo úplne zabudnutých hrádkoch - roztrúsených v divočine, po dedinách, ale i v dnešných okresných mestách. Nové putovanie začneme pri hrádku v Kazimíri. :-) 
 ---- 
 Fotografie : (C) Peter Molnár - december 2006  
 Použité zdroje :  
 http://www.sarospatak.hu/tortenelem/var/var.htm - dejiny hradu v Sárospataku (rovnaký text nájdete aj na paneloch vo vchode na nádvorie) 
   
 http://itthon.hu/sarospatak-ev-telepulese - pomerne obsiahly text o dejinách mesta a hradu 
   
 http://www.vendegvaro.hu/Vartemplom-Szeplotelen-Fogantatas-Sarospatak - krátky text o histórii farského kostola Nepoškvrneného počatia  
   
 http://nagyvofely.hu/sarospatak/templomok - kostoly Sárospataku 
   
   
 Autori obrázkov : 
   
 Plán hradného komplexu a jadra hradu v Sárospataku - infopanel pobočky Maďarského národného múzea v Sárospataku pri vstupnej bráne do hradného areálu 
   
 Kresba hradu z vtáčej perspektívy : http://www.var-webaruhaz.hu/termekek/grafikak/G00002.jpg 
   
 Fotka hradu a Starého mesta z vtáčej perspektívy : http://www.travelsinhungary.hu/pics/Sarospatak/sarospatak.jpg 
   
 1. kresba hradu v Sárospataku zo začiatku 20. storočia :  http://varwebaruhaz.blogter.hu/247850/a_sarospataki_var_udvara_1900-ban_kepeslapon 
   
 2. kresba hradu v Sárospataku zo začiatku 20. storočia : http://www.ace.hu/MNM/MH/709kiadv.html 
   
 3. a 4. kresba hradu v Sárospataku (19. stor.) : http://www.mek.oszk.hu/05700/05707/html/ 
   
 Staré fotografie a pohľadnice hradu v Sárospataku : http://sporisport.uw.hu/regi-fotokturiszt.html     (odporúčam pozrieť, pekná stránočka s množstvom starých fotiek zo SV Maďarska) 
   
 Maľba hradu v Sárospataku - autor : Thomas Ender 
   
   
 Použitá mapa : Infopanel s mapou mesta Sárospatak na Rákocziho ulici 
   

