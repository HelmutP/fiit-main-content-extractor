
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Šášky
                                        &gt;
                Futbal
                     
                 Hlavne sa nezbláznime! 

        
            
                                    15.6.2010
            o
            16:45
                        (upravené
                19.6.2010
                o
                14:20)
                        |
            Karma článku:
                2.31
            |
            Prečítané 
            323-krát
                    
         
     
         
             

                 
                    V poradí devätnáste majstrovstvá sveta vo futbale zažili prvý zápas slovenskej reprezentácie. Premiéra mala byť víťazná, mal sa streliť prvý a rýchly gól, nazbierať potrebné body i urobiť si skóre... Ostalo pri slovách.
                 

                 Slovenská reprezentácia správne neriskovala a snažila sa zápas s Novým Zélandom zvládnuť hlavne v defenzíve. Nešli sme za každú cenu do útoku a Nový Zéland sme sa zároveň snažili nepúšťať k ničomu. Reči o víťaznej premiére a rýchlom góle ostali nesplnené. Slováci nevyhrali a gól dali až v 5.minúte - druhého polčasu.   Tak či onak, na štadióne Royal Bafokeng a pri obrazovkách tam i doma panoval zo slovenskej strany smútok a z Nového Zélandu preradostené tváre. Aj to je obraz dnešnej remízy.   Zápas rozoberať určite netreba, každý kto chcel zápas videl. Do zápasu sme vstúpili opatrne, v Afrike sme hrali v chlade 7°C a okrem krásneho signálu pri priamom kope, kde Hamšík iba natiahol brankára, sme odchádzali do šatní s remízou 0:0. Tento dvojzmyselný výsledok sa menil po 50.minútach, kedy sa z nevšimnutého offside-u presadil hráč tureckého Ankaragücu a náš rozhodne najlepší hráč, Róbert Vittek.   Slováci sa síce snažili, avšak náskok zvýšiť nedokázali. Následne však prišli, aspoň pre mňa určite, NEPOCHOPITEĽNÉ striedania nášho hlavného trénera Vladimíra Weissa, ktorý na koniec zápasu vystriedal všetkých hráčov, ktorí by ešte vedeli zvýšiť náskok Slovenska. Záložníka Vladimíra Weissa mladšieho a útočníkov Stanislava Šestáka a Róberta Vitteka...   Jednoznačne najlepší hráči zápasu: Róbert Vittek a Vladimír Weiss, ktorí boli vždy tam, kde to bolo potrebné, nabehali neuveriteľné kvantum kilometrov, boli zo zápasu vytlieskaní, ale čo už. Od začiatku jednoznačne najhorší hráč zápasu Zdeno Štrba, ktorý si absolútne neplnil úlohy defenzívneho stredopoliara, ktorý mal nepresné nahrávky, pomalé rozohrávky a na konte i zbytočné fauly, ostal na trávniku nevystriedaný, a tak prišlo to, čo sa dalo očakávať...   Tretia minúta nastaveného času. Slováci sa rozhodli brániť jednogólový náskok, čo sa rozhodne nevypláca. Až prišiel center do šestnástky, ďalšia defenzívna chyba Zdena Štrbu a gól obrancu Nového Zélandu. Jedna - jedna...   Samozrejme, je zbytočné teraz rozoberať zápas a hľadať chybu a vinníka. Dá sa prstom ukázať na Štrbu i na striedania trénera Vladimíra Weissa. Dá sa tiež ukázať prstom na Šestáka, ktorý nedal gól, dá sa prstom ukázať na každého - i na mňa, fanúšika. Je to však kompletne zbytočné. Chybu spraví hráč v každom zápase, striedania nie vždy vyjdú podľa predstáv a gól nedal ani najlepší hráč planéty Messi, ktorý mal rozhodne viac príležitostí, ako náš Šesták...   Gól pol minúty pred koncom, v tretej minúte nadstaveného času, bol rozhodne chybou, ktorá na MS nepatrí. Je však teraz v rukách hráčov a trénerov, aby zápas čo najrýchlejšie zakopali pod zem, zabudli naň a sústredili sa iba na zápas medzi Slovenskom a Paraguajom...   Včerajší zápas (aspoň mne) ukázal, že Paraguaj nie je taký silný, ako vyzerá a Taliani azda všetkým ukázali, že prestarnuté mužstvo neznamená zlý futbal - práve naopak. Nedeľa, 20.jún. To bude pre nás dôležitý termín a treba sa sústrediť na to! A hlavne sa nepos... nezblázniť z toho, že sme nevyhrali nad Novým Zélandom. Stane sa. I v lepších rodinách...   SLOVENSKO DO TOHO! Kričíme naďalej. Dvere do osemfinále sú stále dokorán otvorené! Len treba hrať a podať ešte lepší, ako 100%-ný výkon!   Nový Zéland - Slovensko 1:1 (0:0) Góly: 50. Vittek (0:1), 93. Reid (1:1). ŽK: 42. Lochhead, 93. Reid - 55. Štrba Rozhodovali: Damon (JAR) - Ntagungira (Rwanda), Molefe (JAR). 23 871 divákov  Zostavy: Nový Zéland: Paston - Reid, R. Nelsen, T. Smith - Vicelich (78. Christie), Elliott - Bertos, Lochhead - Killen (72. Wood), Fallon, Smeltz. Slovensko: J. Mucha ml. - Zabavník, Škrtel, J. Ďurica, M. Čech - Zd. Štrba - Weiss ml. (91. Kucka), Hamšík, Jendrišek - Vittek (84. Stoch), Šesták (81. Hološko).  Štatistiky: GÓLY: 1 - 1 STRELY NA BRÁNKU: 2 - 3 STRELY MIMO BRÁNKU: 6 - 10 STRELY CELKOVO: 8 -13 DRŽANIE LOPTY: 52% - 48% ROHOVÉ KOPY: 3 - 10 OFSAJDY 0 - 1 FAULY 17 - 15  Niečo z úst aktérov: Róbert Vittek (útočník Slovenska, strelec jediného gólu SR a hráč zápasu /i podľa FIFA/): „V takomto dôležitom zápase nemôžeme dostať takýto gól... Tento okamih môže v konečnom dôsledku rozhodnúť o našom postupe či nepostupe zo skupiny. Žiaľ, namiesto troch bodov máme iba jediný. Zápas sme si prehrali sami.” 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Pekarík potešil mladých slovenských futbalistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Prišlo na moje slová, komentovať bude Slávo Jurko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Keď aj futbalový fanúšik pociťuje hrdosť, že je Slovák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Weiss nadáva na fanúšikov, fanúšikovia na Weissa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Šášky
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Šášky
            
         
        sasky.blog.sme.sk (rss)
         
                                     
     
        Večne meškajúci maniak do športovej novinárčiny, s extrémne vyvinutým zmyslom pre ironické a sarkastické poznámky, predseda vlastnej kvázipolitickej strany SPI a človek prácu vykonávajúci vždy na 100% (pondelok 12%, utorok 23%, streda 40%, štvrtok 20% a piatok 5%...)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    139
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    980
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            MS v hokeji 2011
                        
                     
                                     
                        
                            MS v hokeji 2012
                        
                     
                                     
                        
                            Iné športy
                        
                     
                                     
                        
                            Z môjho nudného života
                        
                     
                                     
                        
                            Televízor
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mono pohľad na to čo vidím keď nevyzriem natoľko,
                     
                                                         
                       3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




