
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Hamada
                                        &gt;
                Ostatné
                     
                 Pár slov o Protivných babách a spol. 

        
            
                                    30.5.2010
            o
            17:57
                        (upravené
                30.5.2010
                o
                18:06)
                        |
            Karma článku:
                3.58
            |
            Prečítané 
            917-krát
                    
         
     
         
             

                 
                    V akom asi stave musí byť mladý muž, aby si pozrel v pondelok večer a ešte k tomu sám tínedžerskú komédiu s Lindsay Lohan? Čo vôbec potrebuje k tomu, aby sa  k niečomu romanticko-zábavnému posadil? Aspoň pár dojmov...
                 

                     Asi v takom, v akom si ťažký nefajčiar zapáli šialene príležitostnú cigaretu. Priateľka za siedmimi vŕškami, spolubývajúci na priváte nehanebne spravil všetky skúšky v priebehu zápočtového týždňa (to by si zaslúžilo samostatný článok) a s postarším domácim sme vyčerpali športom a počasím väčšinu tém počas večere.   Tak som sa uchýlil k televízii. Avizovaná relácia hneď po počasí a športe: Protivné baby. „Mhm, to musí byť lahôdka, filmík pre fajnšmekra," pomyslel som si. Vyplýva to hneď z názvu, no nie? Tak som sa do tej lahôdky s ironickým úškrnom zapozeral. A veru dopozeral som to až do konca, párkrát som sa dobre pobavil a tie baby boli síce protivné, ale na druhej strane celkom znesiteľné. Skrátka, príjemne strávený večer.   O pár dní neskôr som si otvoril Daily Telegraph kvôli príprave na hodinu angličtiny. Našiel som tam krásny článok, v ktorom autorka vysvetľuje mužské postoje k dievčenským filmom, tzv. chick flick-om. Vraví, že muži sa nechcú pozerať vo filmoch na problémy, ktoré sami prežívajú vo svojich vzťahoch. Podľa jej osobného prieskumu, tento typ filmov má u mužov úspech ak spĺňa nasledovný vzorec: vysoký mužský hrdina, ktorého dominantnou vlastnosťou je chladnokrvnosť a podobne vysoká ženská hrdinka, ktorej dominantnou charakteristikou je...atraktivita.   Neviem, či sa to dá takto zovšeobecniť, no niečo na tom asi bude. Mojou najobľúbenejšou romantickou komédiou je dlhodobo vianočná story Láska nebeská. Tam je tých hrdinov síce viac, no sú tam viac-menej cool chlapíci (odhliadnuc od Hugh Granta) a tiež zopár atraktívnych herečiek. A čo je hlavné, tejto komédii nechýba inteligentný humor a popri láske aj láskavosť. Trošku z iného súdka je ďalšia stálica, hoci neviem, či spadá do kategórie romantických komédií - slávna My Fair Lady. Možno miešam jablká z hruškami, no aj tu je cool gentleman profesor Higgins a nádherná charizmatická Audrey Hepburn s kytičkou fialiek a strašným akcentom. Keď k tomu pridáme úžasnú hudbu, je z toho film, ktorý si pozriem kedykoľvek. Alebo taká čarovná Amélia z Montmartru...Podobne zvodných filmov je však naozaj poriedko a preto aj môj súkromný archív tvoria väčšinou filmy iného žánru.   Natočiť skutočne dobrú romantickú či tínedžerskú komédiu dá zrejme naozaj veľa práce. Láska je naozaj všade okolo nás a ponúka mnoho inšpirácií. Asi je však ťažšie uložiť tie inšpirácie na plátno tak, aby zneli romanticky a zábavne zároveň. Príliš často sa asi stretávame skôr s námetmi na drámy ako na komédie. Napriek tomu tomuto žánru fandím. A teším sa, kedy si zas budem môcť so spriaznenou dušou, pri zapálenej sviečke a fľaške dobrého vína niečo povzbudivé pozrieť..     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Hamada 
                                        
                                            Ide to aj inak alebo o padákoch trochu pozitívnejšie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Hamada 
                                        
                                            Dojmy z Banja Luky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Hamada 
                                        
                                            Stará známa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Hamada 
                                        
                                            O kráse v ružovom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Hamada 
                                        
                                            O nemorálnej reklame
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Hamada
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Hamada
            
         
        hamada.blog.sme.sk (rss)
         
                                     
     
        Som dieťa, brat, vnuk, synovec, bratranec, priateľ, BO, animátor, asistent, milovník vidieka, hudobník, herec, mudrlant, filmový fanúšik...Mám rád život..hoci často prináša aj zlé veci, mám ho rád..veď je ako Forrestova bonboniera. Veľmi mám rád ľudí okolo seba, svoj rodný kraj. V tom všetkom väzí obrovské bohatstvo, ktoré si tak často neuvedomujeme
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Na cestách
                        
                     
                                     
                        
                            Boh a ja
                        
                     
                                     
                        
                            Ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Terry Pratchett - Čarodějky na cestách
                                     
                                                                             
                                            Friedrich A. Hayek - Cesta do otroctví
                                     
                                                                             
                                            Tomáš Halík - Stromu zbývá naděje
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Ludwig van Beethoven
                                     
                                                                             
                                            Fryderyk Chopin
                                     
                                                                             
                                            Franz Liszt
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Robo
                                     
                                                                             
                                            Zuzana Hanzelová
                                     
                                                                             
                                            Hermanka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            darcovstvo krvi
                                     
                                                                             
                                            najlepšia stránka pre filmových fanúšikov
                                     
                                                                             
                                            najlepší slovenský denník (podľa mňa)
                                     
                                                                             
                                            naše pastorko :)
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Štyri a pol zemiaka
                     
                                                         
                       Som zdravotná sestra. Personál v slovenských nemocniciach? Otrasný
                     
                                                         
                       Demagógia alebo čoho všetkého sa vieme dopustiť
                     
                                                         
                       Ako uspokojiť J&amp;T aj voličov
                     
                                                         
                       Vodákov sprievodca Vltavou, Hronom a inými žaburinami II.
                     
                                                         
                       Čierna huba sa nemá voziť električkou!
                     
                                                         
                       Skúška
                     
                                                         
                       O mužsko-ženských priateľstvách
                     
                                                         
                       Naozajstné ruské ženy.
                     
                                                         
                       Prečo sa chce Fico podobať na Mečiara?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




