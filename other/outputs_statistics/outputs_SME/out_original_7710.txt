
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kluvánek
                                        &gt;
                Nezaradené
                     
                 Zaostrené na atómy 

        
            
                                    1.6.2010
            o
            8:00
                        (upravené
                9.6.2010
                o
                8:05)
                        |
            Karma článku:
                8.57
            |
            Prečítané 
            2544-krát
                    
         
     
         
             

                 
                    Hoci dnes už nikto nepochybuje o existencii atómov, ešte koncom 19. storočia Ludwig Boltzmann dokazoval teoreticky ich existenciu. Vývoj vo fyzike nakoniec porazil aj tých najzarytejších odporcov atómov a v súčasnosti je to už takmer 55 rokov odkedy máme možnosť vidieť ich „na vlastné oči".
                 

                 Fyzika pracuje v súčasnosti s mnohými natoľko abstraktnými konštrukciami, že si ich nedokážeme predstaviť (napríklad zakrivený viacrozmerný priestor), alebo sme úplne rezignovali na možnosť ich niekedy vidieť (kvarky). Dokonca sa môžeme pýtať, do akej miery sú popisom reality a do akej miery predstavujú len prostriedky, ktorými „vnucujeme" nami skonštruovanú logickú štruktúru prírode. Dlhé stáročia boli aj atómy takouto abstraktnou hypotézou. Jej zástancami boli mnohí význační chemici a fyzici: Boyle, Bernoulli, Lomonosov, ... Moderná atómová teória začala svoju existenciu v roku 1803 známou prácou J. Daltona o stálych zlučovacích pomeroch. Dalton v nej spojil hypotézu atómov s chemickými reakciami a stechiometriou. K ďalším výrazným úspechom atómovej teórie určite patrí periodická tabuľka zverejnená D. I. Mendelejevom v roku 1869 či práce z oblasti štatistiky od velikánov ako J. C. Maxwell a L. Boltzmann. V roku 1897 objavil J. J. Thomson prvú subatomárnu časticu - elektrón. Definitívne potvrdenie atómov prišlo v rokoch 1905 - 1908 prácami A. Einsteina, M Smoluchowského a J.-B. Perrina o Brownovom pohybe[1]. Povzbudení objavmi atómového jadra (E. Rutherford, 1911) a difrakcie RTG žiarenia na kryštáloch (M. von Laue, 1912) začali vedci pracovať na metódach umožňujúcich atómy aj vidieť.   Prečo zostali atómy tak dlho skryté?    Odpoveď na otázku v podnadpise je jednoduchá: lebo sú veľmi malé. Rozmery atómov sú totiž na úrovni desatín z miliardtiny metra (okolo 0,2 nm). Ak by sme napríklad zväčšili jablko na rozmer Zeme, atómy by mali veľkosť približne 1 mm. Na ich zobrazenie je tak potrebné vymyslieť prístroj s naozaj dobrým zväčšením, čo vedcom trvalo až do polovice 20. storočia.   V roku 1590 holandskí optici, bratia Janssenovci, vyrobili optický mikroskop, čím spôsobili revolúciu v mnohých vedných odboroch. Najdôležitejší parameter mikroskopu, jeho zväčšenie, vypočítame  jednoducho vynásobením dvoch čísel - zväčšenia objektívu a okuláru. Mohlo by sa zdať, že zlepšovaním optických vlastností okuláru a objektívu dosiahneme prakticky ľubovoľné zväčšenie. Bohužiaľ to takto nefunguje. Zostrojiť by sme naozaj mohli optický mikroskop s ľubovoľným zväčšením, no jeho rozlišovacia schopnosť by bola obmedzená takzvaným Abbého ohraničením. Odlíšiť totiž od seba môžeme len detaily približne na úrovni 1 vlnovej dĺžky žiarenia, ktoré mikroskopom prechádza. Za to nesú zodpovednosť vlnové vlastnosti svetla a nepomôže nám nijaké zväčšenie. Hoci by sme modrým svetlom (vlnová dĺžka = 400 nm = 400 miliardtín metra) mohli uvidieť zhruba dvakrát menšie detaily ako svetlom červeným (750 nm), je to stále veľmi málo na atómy s rozmermi na úrovni 0,2 nm.   Ďalšie zmenšovanie vlnovej dĺžky použitého žiarenia sa pri mikroskope nevyužívalo. Prakticky v rovnakom čase, ako sa začala venovať pozornosť skúmaniu elektromagnetického žiarenia s menšími vlnovými dĺžkami, totiž prišla kvantová mechanika a s ňou úplne nové možnosti. Podľa nej majú všetky častice aj vlastnosti vlnenia. Môžeme ich charakterizovať tzv. de Broglieho vlnovou dĺžkou, nepriamo úmernou ich hybnosti - čím ťažšia a rýchlejšia častica, tým menšia je vlnová dĺžka jej de Broglieho vlnenia.   Prirodzene vznikla aj myšlienka využívať vlnové vlastnosti častíc v mikroskopii. V roku 1931 M. Knoll a E. Ruska zostrojili prvý elektrónový mikroskop. Elektróny sú v týchto strojoch urýchlené pomocou elektrického poľa a fokusované magnetickými šošovkami. Zaznamenávajú sa elektróny odrazené od povrchu skúmanej vzorky, prípadne sekundárne elektróny, vznikajúce vplyvom dopadajúcich elektrónov v povrchových vrstvách atómov. Dosiahnuté zväčšenia boli z hľadiska staršej svetelnej mikroskopie nepredstaviteľné (až niekoľko 100 000 krát), no obmedzenie o polovičnej vlnovej dĺžke platilo aj v týchto prístrojoch. Navyše zdanlivo jednoduchá možnosť skracovania vlnovej dĺžky zvyšovaním rýchlosti elektrónov narážala, okrem technických, aj na problémy s deštrukciou vzoriek (pri istých energiách elektrónov dochádza k poškodeniu, odpareniu, vzoriek). V súčasnosti je možné dosiahnuť atómové rozlíšenie aj týmito prístrojmi. Existuje totiž technológia odstraňujúca sférickú aberáciu magnetických šošoviek. Ale možnosť vidieť atómy nám prvý krát poskytol iný typ mikroskopu.   Autoemisný mikroskop   Len 6 rokov po úspechu Knolla a Ruska vyvinul E. W. Müller nový typ mikroskopu - autoemisný mikroskop (Field-Emission Microscope). V tomto prístroji je veľmi ostrý kovový hrot pripojený na vysoké záporné elektrické napätie - čím ostrejší hrot, tým vyššie elektrického pole vzniká v jeho okolí. To spôsobuje uvoľnenie elektrónov, ktoré pokračujú od hrotu po priamej dráhe a dopadnú na pólkruhové fosforové tienidlo. Tam ich dopad spôsobuje viditeľné bliknutia (ako v klasickej televíznej obrazovke). Zaznamenaný obraz odráža pravdepodobnosť emisie elektrónov z materiálu a je zväčšený v rovnakom pomere ako je pomer polomerov fosforového tienidla a kovového hrotu. Extrémne ostré hroty umožňovali dosiahnuť až miliónnásobné zväčšenia. Princíp elektrónového autoemisného mikroskopu bol veľmi jednoduchý. Jeho výhodou bolo, že nepotreboval nijakú dodatočnú optiku. Nevýhodou bola potreba vysokého vákua, možnosť použiť len kovové, veľmi ostré, vzorky a priestorové rozlíšenie nie menej ako 2 nm. K obmedzeniu rozlíšenia prispievali najmä difrakcia elektrónov a ich tepelný pohyb (elektróny mali zložku rýchlosti kolmo na smer k tienidlu).      Obr. 1 Schéma FIM: atóm He, polarizovaný silným elektrickým poľom, je pritiahnutý ku kovovému hrotu vzorky. Na povrchu hrotu dôjde k jeho ionizácii a vplyvom poľa je urýchlený k obrazovke. Stopa po jednom ióne by bola slabá, takže sa používa ešte mikrokanálový zosilňovač, spôsobujúci spustenie celej kaskády elektrónov po dopade jedného He iónu. Vpravo je vidieť typický obraz hrotu. Svetlé body predstavujú obraz jednotlivých atómov (http://www.nims.go.jp/apfim/fim.html).   K dosiahnutiu atómového rozlíšenia dospel Müller v troch krokoch. Najprv v roku 1951 nahradil ľahké elektróny oveľa ťažšími iónmi vodíka H. Tým potlačil difrakciu. Autoemisný iónový mikroskop (FIM - Field-Ion Microscope) využíval adsorpciu (naviazanie) atómov H na povrch ostrého hrotu vplyvom kladného napätia. Adsorbované atómy boli silným elektrickým poľom ionizované. Vznikli tak kladné ióny, ktoré boli od povrchu „odtlačené" a pole ich urýchlilo smerom k obrazovke, kde zanechali svetelnú stopu (Obr. 1). Takýto prístroj už dokázal rozlíšiť jednotlivé atómové vrstvy v ostrom hrote. Ďalším vylepšením rozlišovacej schopnosti bolo ochladenie hrotu na teploty 20 - 100 K. Vďaka tomu sa znížila energia tepelného pohybu atómov a teda aj zložka ich rýchlosti kolmá na smer k obrazovke. Nakoniec ešte Müller nahradil atómy vodíka héliom He. Tie sú ťažšie a k ich ionizácii dochádza vplyvom vyššieho elektrického poľa, čo dodatočne zvyšuje rozlíšenie prístroja. Spolu s kolegom Bahadurom tak boli schopný 17. októbra 1955 získať obrázok so stopami po atómch volfrámového hrotu. Ľudstvo tak malo po prvý krát možnosť vidieť atómy takmer na vlastné oči (Obr. 1 a 2).      Obr. 2 Hrot tunelového mikroskopu zovrazený pomocou FIM. Na fotografii vidieť jednotlivé atómy volfrámu. (http://aip.org/pnu/2006/split/788-2.html)    Hoci sú možnosti FIM obmedzené tvarom a materiálom vzorky (len ostré hroty z určitých kovov), výskumníci ho dokázali využiť na pozorovanie prekvapivo širokej triedy fenoménov: od dekompozície zlúčenín, cez správanie sa prímesových atómov, difúziu jedného atómu po povrchu hrotu až po rôzne mechanizmy vzájomných reakcií atómov. V roku 1967 skombinovaním FIM a hmotnostného spektrometra (čas trvania letu iónov z hrotu do detektora určí ich hmotnosť) vytvoril Müller tzv. atómovú sondu. Pomocou nej možno vytvárať chemické mapy povrchov s atómovým rozlíšením.   FIM a atómová sonda sa ukázali ako veľmi užitočné nástroje na získavanie informácii z mikrosveta. No v súčasnosti už väčšinou poznáme fotografie atómov z úplne iných typov mikroskopov. Tieto stroje „novej generácie" sú schopné nielen atómy zobrazovať, ale môžu s nimi aj manipulovať.   Tunelový mikroskop   Tunelovanie je termín označujúci kvantový proces vznikajúci vďaka tomu, že častice majú nenulovú pravdepodobnosť prekonávať oblasti, cez ktoré by sa podľa klasických zákonov nemohli dostať. Elektrón dokáže prejsť cez potenciálovú bariéru (môžeme si ju predstaviť ako kopec), hoci na jej prekonanie nemá, podľa klasických predstáv, dostatok energie. Je to akoby lopta prešla skrz kopec vďaka tunelu (odtiaľ názov javu), ktorý šťastnou náhodou trafila, pričom energia jej postačovala len aby sa dokotúľala do polovice kopca. Pravdepodobnosť takéhoto prechodu exponenciálne klesá s výškou potenciálovej bariéry a jej  hrúbkou.   Teoretické princípy skenovacieho tunelového mikroskopu (Scanning Tunneling Microscope - STM) boli známe už v roku 1956[2], no až v roku 1981 boli definitívne prekonané všetky technické problémy a G. Binnig s H. Rohrerom skonštruovali prvý STM. O dôležitosti ich objavu svedčí aj fakt, že už v roku 1986 zaň dostali Nobelovu cenu.   Pri STM sledujeme malý prúd tunelujúcich elektrónov prechádzajúcich medzi skúmanou vzorkou a ostrým, najčastejšie volfrámovým, hrotom. Hrot je zaostrený na maximálnu možnú mieru (hrot hrá úlohu otvoru - viď poznámka 3). Ideálne ostré sú hroty s jediným atómom na vrchole (Obr. 2). Majú rozmer približne 0,2 nm, čo je aj hranicou rozlišovacej schopnosti tunelového mikroskopu. S STM je tak možné dosiahnuť rozlíšenie 0,2 nm, pričom vlnová dĺžka tunelujúcich elektrónov je 1 nm. Vidíme, že Abbeho obmedzenie v tomto prípade neplatí.      Obr. 3 Povrch platiny s atómovým rozlíšením získaný pomocou STM (Don Eigler, Laboratóriá IBM).   Hrot sa pohybuje 1 - 2 nm nad povrchom vzorky. Pri tejto vzdialenosti je pravdepodobnosť tunelovania elektrónov dostatočne vysoká a tunelový prúd dosahuje veľkosť niekoľko nanoampérov. To je v súčasnosti dobre merateľná hodnota. Tunelový prúd je silne závislý od vzdialenosti medzi hrotom a vzorkou. Pohyb hrotu ponad vzorku (prípadne pohyb vzorky pod hrotom) je zabezpečený pomocou piezoelektrických kryštálov[3]. Aby sa predišlo nebezpečenstvu hroziacemu pri náraze hrotu na nerovnosti vzorky a aby sa tunelový prúd nemenil vplyvom nerovností v príliš veľkých hodnotách, pracuje STM v režime konštantnej úrovne tunelového prúdu vo všetkých bodoch vzorky. Zabezpečené je to spätnou väzbou, napojenou na ďalší piezoelektrický kryštál. Zmena napätia na tomto kryštále zabezpečuje posun hrotu vo vertikálnom smere, čím udržuje hrot v takej polohe, aby tunelový prúd bol konštantný. Konštantná hodnota tunelového prúdu znamená konštantnú vzdialenosť medzi hrotom a vzorkou. Zo zmien napätia na piezokryštále určujeme vertikálne posunutie hrotu a tým aj vertikálne zmeny vo výške povrchu vzorky. Z týchto údajov sa potom zostavuje jej reliéf - získavame 3D mapu povrchu vzorky.   Vzniknutá "mapa" je zobrazením plochy konštantnej pravdepodobnosti tunelovania elektrónov. Okrem samotnej topografie povrchu (čiže priestorového rozloženia atómov) na ňu vplýva aj zmena elektrónovej hustoty vyvolaná inými vplyvmi. Pri povrchu, tvorenom atómami rovnakého druhu, takáto mapa veľmi dobre vystihuje skutočnú topografiu vzorky (Obr. 3). Pri povrchoch z rôznych druhov atómov je výsledný obraz ovplyvnený aj silami vzájomného pôsobenia medzi atómami. Pri interpretácii získaných máp treba preto postupovať opatrne[4]. Výhodným pomocníkom sa stávajú počítačové simulácie, umožňujúce porovnávať naše teoretické predpoklady s experimentom.      Obr. 4 Autori tento obrázok nazvali The Beginning (Začiatok). Z 35 atómov xenónu zostavili na povrchu niklu pomocou STM reklamu na IBM (D. M. Eigler, E. K. Schweizer, Nature 344, 524, 1990)   Pomocou STM je teda možné rozlíšiť jednotlivé atómy (Obr. 3 - 5). Dôležitosť STM je však aj v tom, že sa dá použiť ako veľmi jemný nástroj na prenášanie atómov. Dokážeme tak zostavovať štruktúry z jednotlivých atómov. Výsledky týchto pokusov, slúžiacich rovnako vedeckým aj reklamným účelom, sú na obrázkoch 4 a 5. Navyše STM sa stal prototypom zariadení pracujúcich na mierne odlišnom princípe, ktoré odstraňujú jedno obmedzenie FIM a STM mikroskopov.        
 Obr. 5 Atómy železa na povrchu medi. Vlnky vo  vnútri sú stojaté vlny hustoty pravdepodobnosti povrchových elektrónov. Takže vlnové vlastnosti častíc naozaj existujú! (M.F. Crommie, C.P. Lutz, D.M. Eigler, E.J. Heller, Surface Rev. and Lett. 2 (1), 127, 1995).    Silový mikroskop    Binnig po svojom úspechu s STM pokračoval vo výskume a spolu s dvojicou Ch. Gerber a C. F. Quate prišli v roku 1986 s novinkou - atómovým silovým mikroskopom (Atomic Force Microscope - AFM, niekedy tiež scanning force microscope). Podobne ako v STM aj tu sa používa zaostrený hrot presúvajúci sa ponad vzorku (Obr. 6), no nemeria tunelový prúd ale sily pôsobiace medzi hrotom a vzorkou (Obr. 6). Aby sme dosiahli atómové rozlíšenie, musí byť hrot zaostrený na atómovej úrovni, takže meriame hodnoty síl medzi atómom hrotu a atómom vzorky. Tieto sily sú veľmi malé: 10-7 N pre iónové sily, 10-11 N pre Van der Waalsove sily a 10-12 N pre slabšie druhy síl. Veľkosť síl sa meria z výchylky kovového pásika, na ktorom je hrot upevnený. Hrot musí bzť veľmi ľahký (okolo 10-10 kg) a pásik veľmi tenký - tenší ako alobal. Aj napriek extrémnym rozmerom sú výchylky pásika tak malé, že pôvodne sa merali pomocou samostatného STM. To by sa mohlo zdať na prvý pohľad dosť samoúčelné. AFM však odstraňuje zásadnú nevýhodu STM a FIM - nie je obmedzený na elektricky vodivé vzorky. V súčasnosti sa používajú aj iné systémy na meranie posunutí kovového pásika. Pomocou kapacitnej metódy, röntgenového interferometra či systémov SQUID (využívajú supravodiče) je možné merať výchylky na úrovni milióntin nm[5]. V praxi je často využívané meranie posunutia pomocou laserového lúča. Ten sa  odráža od pásika a dopadá na fotodiódu rozdelenú na polovicu. Z rozdielu signálov z jednotlivých častí fotodiódy je možné určiť posunutie pásika.      Obr. 6 Schéma AFM.   Hoci je rozlišovacia schopnosť AFM teoreticky obmedzená len rozmermi hrotu (rovnako ako pri STM), cesta k rozlíšeniu jednotlivých atómov bola pre tento prístroj kľukatejšia ako v prípade STM. Je totiž podstatne ľahšie merať prúd na úrovni nanoampérov ako sily rádu 10-10 - 10-12 N (list papiera formátu A4 má tiaž 15 miliónov až miliardu krát väčšiu). Samotný pôvod týchto síl je podstatne zložitejší ako pôvod tunelového prúdu, čo tiež sťažuje analýzu získaných údajov.   V prvých obrázkoch s atómovým rozlíšením získaných pomocou AFM nebolo možné rozoznať žiadne bodové defekty alebo zlomy. Merané sily boli asi 100 krát väčšie ako sa očakávalo pre Van der Waalsovské sily medzi povrchom vzoriek a hrotom. AFM totiž pracoval v kontaktnom móde. Pri ňom je hrot ťahaný po povrchu vzorky a spätnou väzbou udržovaný na hladine konštantnej sily. Vzdialenosť medzi atómami hrotu a povrchu je tak malá, že dochádza medzi nimi k chemickej väzbe. Vytvárajú sa pritom mnohé parazitné „minihroty", čo väčšuje pôsobiacu silu - meriame priemernú silu od desiatok hrotov, a znemožňuje rozlíšenie porúch ako sú defekty a zlomy. Ďalšou nevýhodou kontaktného módu je časté poškodenie vzoriek hrotom.      Obr. 7 Červená krvinka zobrazená pomocou AFM.   Výrazný pokrok nastal s príchodom nekontaktnej metódy. Pôsobením spätnej väzby pri nej hrot s pásikom kmitajú vlastnou frekvenciou (približne 100 kHz). Amplitúda kmitov je pomerne veľká - asi 20 mn. Keď sa hrot priblíži ku vzorke, dochádza vplyvom pôsobiacich síl k zmene frekvencie kmitania hrotu. Pomocou spätnej väzby je hrot udržiavaný na hladine rovnakého frekvenčného posunu. Takže získavame obraz konštantného frekvenčného posunu. Hrot sa  navyše nepribližuje tak tesne ku vzorke ako pri kontaktnej  metóde a nehrozí preto vytvorenie väzby medzi atómami hrotu a vzorky ani jej poškodenie. Navyše AFM je v tomto móde citlivý na zmenu veľkosti síl, čiže meria naozaj len sily meniace sa na atómových vzdialenostiach.   Interpretácia získaných výsledkov je komplikovanejšia než pri STM. Hrot vykonáva zložité anharmonické kmity a nepomáha ani komplexná podstata pôsobiacich síl. Často je potrebné pre získanie správnej predstavy o atómovej štruktúre vykonať ešte komplementárny experiment prípadne počítačovú simuláciu.      Obr. 8 Povrch diskety zobrazený AFM s obyčajným hrotom (vľavo) a s hrotom citlivým na magnetické sily (vpravo) (T. J. McMaster et. al. University of Bristol).   Našťastie má AFM aj množstvo predností. Ako sme už spomínali, nie je obmedzený len na vodivé vzorky, čím sa pred ním otvára široké pole pôsobnosti v oblasti biológie (Obr. 7). Dokáže tiež pracovať v rôznych podmienkach od ultravysokého vákua až po obyčajný vzduch. To opäť nahráva jeho využitiu v biológii, pretože umožňuje skúmať preparáty v prirodzenom prostredí. Lákavá je aj možnosť skúmať pomocou AFM rôzne druhy silového pôsobenia. Môžeme merať Van der Waalsove sily medzi vzorkou a hrotom. Keď použijeme nabitý hrot, môžeme zmerať sily elektrostatického pôsobenia. Dokonca sme schopní merať magnetické sily pomocou hrotu s vlastným magnetickým momentom. Magnetické hroty umožňujú napríklad zviditeľniť spôsob zaznamenávania informácií na magnetické médiá (Obr. 8).   Zostrojený bol aj teplotný mikroskop. Jeho hrot je vyhotovený z volfrámu a niklu, ktoré sú oddelené izolujúcou vrstvičkou po celom povrchu hrotu, okrem samotnej špičky. Druhé spojenie volfrámu a niklu je v oblasti mimo hrotu a je udržované pri známej teplote. Vďaka rozdielu teplôt vzniká medzi spojmi kontaktné napätie a jeho veľkosť umožňuje merať teplotu hrotu. Je to určite najmenší teplomer na svete. A hoci tento hrot sa nedá „zastrúhať" na menej ako 30 nm, jeho využitie v biológii môže byť veľmi zaujímavé.      
    [1] Paradoxne tak vyznieva samovražda nešťastného Boltzmanna v roku 1906. Okrem psychickej lability ho k nej priviedli aj problémy s odporcami teórie atómov v čase, keď už bolo prakticky všetko rozhodnuté.   [2] V roku 1956 sa objavilo vo výskumnom ústave amerického vojnového námorníctva zdanlivo jednoduché riešenie ako prekonať Abbeho ohraničenie. Skúmaný objekt bol umiestnený tesne za kruhový otvor vytvorený nepriehľadnou clonou. Svetlo, prechádzajúce týmto otvorom, odrazené od skúmaného objektu (prípadne ním prechádzajúce) bolo následne analyzované. Clona sa posúvala ponad skúmaný objekt, pričom sa vykonávala jeho analýza. Takýmto skenovaním sa vytvárala mapa povrchu. Rozlíšenie bolo pritom obmedzené len veľkosťou otvoru v clone.   [3] Takéto kryštály sú známe tým, že menia svoje rozmery vplyvom prítomnosti elektrického poľa. Takže zmenou napätia na hranách kryštálu dokážeme meniť jeho rozmery, pričom táto zmena môže byť až na úrovni desaťtisícin nm.   [4] STM je totiž viazané na elektrónovú štruktúru vonkajších (valenčných) elektrónov a tie sú v tuhých látkach (a špeciálne v kovoch) značne delokalizované. STM vytvára obraz plochy konštantnej pravdepodobnosti tunelovania. Keďže tunelovania sa prednostne zúčastňujú elektróny v blízkosti Fermiho hladiny, získaný obraz je vlastne priestorovým obrazom Fermiho hladiny v kove. Otázkou pri interpretácii STM preto je, nakoľko sú výrazné črty v tvare Fermiho hladiny v korelácii s polohami atómov.   [5] Takmer slepé púštne škorpióny druhu Paruroctonus mesaensis dokážu, vďaka citlivým bunkám na konci nôh, zaznamenať otrasy pôdy na úrovni 0,1 nanometra. Z rozdielov v amplitúde otrasov, zaznamenanej každou z ôsmich nôh, vedia určiť smer ku koristi [W. Sturzl, R. Kempter, J. L. van Hemmen, Phys. Rev. Lett. 84, 5668 (2000)]. Aj niektoré druhy hmyzu dokážu registrovať takéto otrasy.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Zákernosti novej kategorizácie liekov, alebo ako ušetriť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Paradox dvojčiat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Všeobecná teória relativity – deväťdesiatnička s tuhým koreňom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Bezpečnosť cestnej premávky z hľadiska fyziky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kluvánek 
                                        
                                            Prečo veríme špeciálnej teórii relativity?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kluvánek
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kluvánek
            
         
        kluvanek.blog.sme.sk (rss)
         
                                     
     
        Hlava rodiny, občas sa venujúca aj iným radostiam (práca, šport, fyzika, knihy,...), výsledky ktorých niekdy naberajú elektronickú formu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5239
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




