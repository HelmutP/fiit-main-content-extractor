

 Hudba 
 - Stan, pačia sa ti Arcitic Monkeys? - pýta sa ma asi 60 ročný kolega v práci 
 - Poznám iba zopár songov z rádia - 
 - Keď mi donesieš CD, tak ti ich napálim -  
 Hoci sa vraví že Slováci sú spevavý národ, po príchode do Británie som trošku zmenil názor. Hoci Angličania narozdiel od nás nedokážu zaspievať niekoľko sto rokov staré ľudové piesne ako Slováci, ich hudobný vkus je omnoho rozvinutejší. Takmer každý má  ho vysoko vycibrený  gigantickou domácou zbierku CD alebo platní, dokáže sa baviť o hudbe a hudobníkoch aj generáciu mladších. Navyše mladé kapely  ktoré som videl v puboch a kluboch dokázali byť tak úžasne svojské a zároveň s potenciálom preraziť, s čím som sa na Slovensku žiaľ nestretol. 
 Futbal 
 Futbal je u Britov témou rozhovoru č.1.  Niekedy však mám pocit je, že je to vďačným predmetom rozhovoru ľudí, ktorí si často nemajú čo povedať. Diskutéri sa zväčša rozprávajú o svojich kluboch spôsobom ako keby boli ich členmi alebo majiteľmi. Napr. 
 - Keď budete hrať  takto aj naďalej, budete mať problémy - 
 - Potrebujeme nejakého dobrého útočníka a bude to v pohode- 
 Kopu kolegov síce v zivote nikdy neboli na štadiónoch Anfield alebo Old Trafford, doma nemajú káblovku aby sledovali ich zápasy, no každý z nich na začiatku sezóny si kúpi nový dres a po celý jej zvyšok sa bude s ostatnými doberať. Príslušnosť ku klubu sa dedí z pokolenia na pokolenie a jeho zmena je zriedkavá. Podľa mňa to súvísi s  definovaním nejakej identity, ktorú stratili keď ich priemyslená revolúcia vyhnala z tradičných rodinných komunít do anonymity miest. 
 Ústretovosť 
 Na cestách, uliciach, obchodoch a úradoch sa zväčša stretyávam s ľudmi, ktorí  ochotní pomôcť a za každú maličkosť sa odmenia úsmevov. Teda žiadni slovenskí snehuliaci s pohľadom do zeme. Pamätám si môj prvý nákup v UK, kedy mi spadol pri pulte v Sommerfielde košík a pol predajne mi pomohlo pozberať veci nazad. Červenal som sa ako čerešňa... :) 
 Pubová kultúra 
 Satinský s Lasicom tvrdili, že po vypití veľkého množstav alkoholu dochádza k zmene osobnosti - smerom k protipólu. Táto téoria sa tu potvrdila. Opití Angličania slušnosť a ústretovosť po vypití alkoholku nahrádzajú agresivitou a arogantnosťou. Kým na Slovensku policajné dodávky s mrežami na čelnom skle sa používajú iba pri demonštráciach a nepokojoch, tu patria k bežným víkednovým javom. Polícia totiž nestíha na obyčajných autách zvážať opitých a výtržnikov. Obvzlášt zaujímavý jav je  fenomén ladettes. Vyskytol sa v polovici 90. rokov a v krátkosti sa jedná o preberanie staromládeneckého štýlu života dievčatami. Ide teda o nadmerné pitie, bitkarstvo, sexuálna promiskuita... 
 Sukne 
 Kým slovenské dievčatá sa bránia sukien ako čert kríža, anglické ich milujú. Nosia ich dievčatá rôznych typov tela a prítažlivosti, veku a za každého počasia. Ide ti o jednoduchú matematiku, ktorú každá rýchlo pochopí: 
 chalpcom sa páčia sukne + chcem sa páčiť chlapcom =  nosím sukňu aby som sa páčila. 
 Dievčatá boli práve jedným z mojich najväčších prekvapení po príchode na Ostrovy. Človek, ktorý povedal že Britky sú škaredé tu pravdepodobne nikdy nebol. Niesú totiž škaredé, iba iné. 
   
 Tento článok možno vyznieva britofilne ale Británia pre Slováka nemusí byť iba nejakou škaredou a divnou destináciou na zarobenie balíka peňazí alebo naučenie sa jazyka. Síce profesne som tu stále na dosť nízkom stupni, nežijem život nejakého gastarbeitera z ubytovne. Británia sa mi páči, na Slovensko a korene nezabúdam, to dobré tu absorbujema a keď sa raz vrátim domov posnažím sa to odovzdať okoliu. 
   
   

