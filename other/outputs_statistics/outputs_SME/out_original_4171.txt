
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zdena Zvadova
                                        &gt;
                fotky
                     
                 Islandskými cestami 

        
            
                                    8.4.2010
            o
            15:27
                        (upravené
                8.4.2010
                o
                16:16)
                        |
            Karma článku:
                9.27
            |
            Prečítané 
            1470-krát
                    
         
     
         
             

                 
                    Kde to som? Zaujímavé. Nikde nič, len hory studenej lávy. Asi bude pravda, že tu stromy nerastú. Chce sa mi spať. Nespi, aby si nič neprepásla. Nebudem. Zúfalo pozerám, či sa niečo zmení. Dlho, dlho nič. Oči sa mi zatvárajú, ale v tom zbadám súsošie. Ľudské ruky pretvorili jednotvárnosť v umelecké dielo. Neskôr ďalšie a ďalšie. Cestou naspäť si ich odfotím.    
                 

                    Vošli sme do nejakého mestečka, je krásne, je čisté. A vidím stromy! Zdroje asi boli zastaralé. Prví pasažieri vystupujú. Už sa mi nechce spať. Kopírujem do mozgu každý roh domu, každú uličku. Híkam ako dieťa na Vianoce.   Neskôr som sa dozvedela, že ihličnany tu sadia dôchodcovia ako hobby a že pravé islandské stromy siahajú asi do pol lýtok. V islandskom lese sa nemôžeš stratiť.           Hallgrimskirkja, najväčší islandský kostol.                Valí sa para, cítiť síru, čakám, čo sa stane. Výbuch.             Gullfoss, jeho divokosť mi naplnila srdce vášňou.             Tu mala byť Polárna žiara. Ale fotiť hviezdnatú oblohu bola tiež zábava.          Modrá lagúna. Kým sa telo máča v teplej vode, studený vietor uši reže.                         Kone, ako ich pred 1000 rokmi poznali Vikingovia.          Krátky výletík, nabudúce ho predĺžime a snáď bude aj Polárna žiara. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdena Zvadova 
                                        
                                            Človek človeku človekom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdena Zvadova 
                                        
                                            Medzi severom a juhom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdena Zvadova 
                                        
                                            Kultúra je kultúra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdena Zvadova 
                                        
                                            V lotosovom kvete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdena Zvadova 
                                        
                                            Celebritou na pár týždňov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zdena Zvadova
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zdena Zvadova
            
         
        zdenazvadova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Nie som náročný divák, mám len rada dobré filmy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    84
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    907
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            moje diamanty
                        
                     
                                     
                        
                            i cesta muze byt cil
                        
                     
                                     
                        
                            zaveršujeme si
                        
                     
                                     
                        
                            fotky
                        
                     
                                     
                        
                            zo života
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Gerta Schnirch
                                     
                                                                             
                                            Biela Masajka
                                     
                                                                             
                                            hudba
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Branik
                                     
                                                                             
                                            Peter Balko
                                     
                                                                             
                                            Eduard Grecner
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




