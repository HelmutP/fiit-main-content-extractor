
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Veronika Jánošíková
                                        &gt;
                Recenzie
                     
                 Basketbalové denníky (recenzia) 

        
            
                                    21.5.2010
            o
            21:45
                        (upravené
                22.5.2010
                o
                10:49)
                        |
            Karma článku:
                2.74
            |
            Prečítané 
            436-krát
                    
         
     
         
             

                 
                    Život bez problémov, život s budúcnosťou, cesta z ktorej sa zjavne nedá vybočiť. No nie je to tak. Na pohľad nevinná ukážka čo dokáže zmena vedomia. Veď je to len zábava, no meniaca sa na peklo. Zábava končí prvým absťákom, a tým aj pokora, čistota, rešpekt i slušnosť. Žiadne zábrany, a na dlani len cesta do pekla. Cesta, s ktorou by ste sa nikdy nevydali. Predtým.
                 

                     Jimov život pripomína klasické tínedžerské nažívanie. Školák ktorý sa popri škole venuje basketbalu, voľné chvíle trávi s kamarátmi, s partiou. Prišli cigarety, tráva, trocha toulénu,  pocítiť  ,,byť mimo". Zavrieť oči, byť na streche nahý a onanovať pod nebom a hviezdami. Ovoňať slobodu z bezprostrednej blízkosti a nepozerať dopredu čo sa môže stať. Jimov kamarát umiera a nik tomu nezabráni. Videl ho skákať s ním na kôš, zabávať sa vonku s chalanmi a zrazu leží pred ním mŕtvy a nič už nevie zmeniť. Bolesť ho páli na hrudi a nikto nevie pochopiť to čo cíti. A tak nehľadá niekoho, ale nájde niečo. Niečo čo ho pochopí, pomôže, vypočuje, uvoľní, no pýta si zato krutú daň. Vyhadzov zo školy, z basketbalového krúžku, z domova. Jim sa ocitá na ulici len s ťažkou závislosťou a čoraz horším zdravím. Už nemá nikoho. Stratil aj samého seba. Cestou šťastia, no najmä porážky ho sprevádza denník, kde si píše myšlienky, pocity, zážitky ktoré prežíva.   Film podľa pravdivej udalosti bol natočený v roku 1995 režisérom Scottom Kalvertom. V úlohe Jima si zahral famózny Leonardo di Caprio, ešte vo svojom rannom veku. Svojej úlohy sa chopil bravúrne, sťa by danú situáciu sám už zažíval. Scény utrpenia a zvýjania sa v bolestiach, chytili za srdce asi každého. Jimov srdcervúci rev, plný sklamania a zúfalstva priam bodal do ľútosti. Občas som mala pocit akoby bol zvuk zosilnený, no bol to len efekt zúfalého revu na pôde ticha. Fázy strihu boli v každom prípade na svoju dobu dosť alternatívne, odlišné. Časový posun často predstavovali krátke zábery, končiace tlmením svetla. S kamerou sa ale v tomto filme výborne pohralo. Časté zábery priamo ,,en face", kamera rovno oproti hercom. Pohybuje sa s nimi, a často badáme aj ako sa točí okolo nich. Takýto záber viac navodzuje pre nás zdrogovaný stav a dokážeme tak viac pochopiť Jimove prežívanie. Hudobná zložka bola nevtieravá, takmer ju ani nebolo badať. Ničím výrazným sa neprejavila, no osobne ma potešila scéna Jimovho posledného zápasu, kde zaznela pieseň od The Doors. Táto psychedelická kapela totiž výborne rezonovala s Jimovým daným stavom.   Basketbalové denníky sa však pridržiavajú celý čas Jimových zápiskov. Sú ako nepriama šnúra po ktorej plynie dej a o ktorú sa môže aj oprieť film. Rozprávačom filmu je Jim, čítajúc svoj denník, čím sa prelamuje film s literárnym umením a dáva mu aj silnejšiu atmosféru. Chvíle keď mu kamaráti nahlas čítajú jeho strany, je jeho zúrivosťou dokazovaná dôležitosť stránok na ktoré si kladie svoju dušu. Kto by aj chcel, aby bola naša duša obnažená. Pre Jima majú denníky podobu akejsi filtrácie vnútra. No časom mu to je málo.   Zaujímavým momentom a linkou filmu sa stáva Jimov vzťah so svojim trénerom. Na prvý pohľad to vyzerá ako prirodzená náklonnosť, takmer priateľstvo. No keď sa Jim dostane do závislosti, jeho tréner sa ukáže v pravom svetle, a začne mu ponúkať peniaze za sex. Jim však ešte nie je tak na dne, aby to prijal a z celej situácie mu je zle. Paradoxom je scéna, keď Jim prejde na heroín a nemá peniaze. Nechá sa za peniaze ukájať na verejných záchodov, kde sa objaví jeho prelud, chlípni tréner. Jim sa stáva tým, čo sa mu hnusilo. Podobnou linkou je Diana, miestna feťáčka a pobehlica, ktorej sa zo začiatku chlapci zo školy vysmievajú. Postupom času, sa stávajú takmer rovnakými osobami. Dokonca nadíde chvíľa, keď sa ona vylieči, dá dokopy a Jim od nej ponížene pýta pár drobných. Opäť sa stretávame s podobným motívom.   Zaujímavou a čímsi záhadnou postavou filmu sa stáva Reggy. Vysoký černoch ktorý Jimovi robí spoluhráča pri koši vo voľnom čase. V časoch Jimovej krízi, keď hrozí že umrie sa Reggy stáva jeho záchrancom a dostane ho z toho. Samozrejme, len na chvíľu, čím chceli tvorcovia pravdepodobne ukázať, že ťažko závislému človeku, už nevraví nič priateľ či jeho pomoc. Vidí už len drogu.   Film sa stal spolu s európskym Trainspottingom akýmisi zakladateľmi drogových filmov v kinematografii. Basketbalové denníky sú zaujímavé aj tým, že na začiatku filmu by len málokto vedel povedať, akým smerom sa film vydá. Podľa názvu by určite mnohý povedali, že to bude ,,športový" film pre tínedžerov. Potešilo ma, že film sa vydal inou, neočakávanou cestou. Tvorcovia si však neodpustili ,,happy end". Udalosť keď Jima zatvoria do basy a tam prestane s drogami však vyznela skôr ako skratka na rýchlejšie dokončenie filmu. Treba však brať do úvahy aj to, že sa samozrejme museli pridŕžať skutočnej udalosti podľa ktorej bol film natočený. Film básnika Jima Carrola končí jeho vlastnou čítačkou pred publikom, ako znak že sa poučil zo svojho skazeného života a chce sa venovať písaniu.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Veronika Jánošíková 
                                        
                                            Poznala som, no už nepoznám
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Jánošíková 
                                        
                                            Na zdra vie !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Jánošíková 
                                        
                                            latentná búrka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Jánošíková 
                                        
                                            Dematerializácia umenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Jánošíková 
                                        
                                            Lou Reed &amp; The Velvet Underground, - prechod k postmoderne
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Veronika Jánošíková
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Veronika Jánošíková
            
         
        veronikajanosikova.blog.sme.sk (rss)
         
                                     
     
         "Vstúpte do tohoto hrozného chaosu ktorý mám v hlave!" 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    38
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    899
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Riadky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Poviedky
                        
                     
                                     
                        
                            Rozprávky
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Poznala som, no už nepoznám
                     
                                                         
                       Na zdra vie !
                     
                                                         
                       latentná búrka
                     
                                                         
                       Útek doby
                     
                                                         
                       Príbeh vločiek
                     
                                                         
                       Markíza ukázala v správach značku blízku neonacistom
                     
                                                         
                       Špinavému feťákovi sa ošetriť nedám
                     
                                                         
                       Čo zmení jeden potrestaný vajda?
                     
                                                         
                       Keby bolo keby...
                     
                                                         
                       Premiérova fľaša, Čaplovičova torta a vegetariánsky mäsožravec
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




