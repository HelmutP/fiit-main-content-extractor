
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Urda
                                        &gt;
                Cestovanie
                     
                 Filatelisti-dôchodcovia... na Choči 

        
            
                                    9.6.2010
            o
            10:01
                        (upravené
                24.7.2010
                o
                23:21)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1618-krát
                    
         
     
         
             

                 
                    ...alebo dôchodcovia-filatelisti? Oboje, a k tomu treba pridať ešte ďalšiu vlastnosť: nadšení obdivovatelia krás slovenskej prírody - turisti. Tohto roku sa uskutočnil v poradí už piaty ročník turistického výstupu filatelistov na niektorý z krásnych slovenských vŕškov. Po predchádzaúcich výstupoch na Kriváň, Ďumbier, Sitno a Poľanu tentoraz bol na rade Veľký Choč na rozhraní Liptova a Oravy. Opisovať Choč, súvislosti spojené s osobnosťami, nadchýňať sa tu zážitkami - to nemá význam. Kto ste tam boli, poznáte to z vlastnej skúsenosti, ďalší máte zážitky z iných podobných výstupov, no a tí, čo tomu neholdujete, iste si to viete predstaviť z nasledovných záberov. A tak teda zopár našich nadšencov dôchodcov-filatelistov-turistov ten Veľký Choč navštívilo. Zišli sa tu večne mladí z Topoľčian, Nitry, Piešťan, Bratislavy a Martina. My, starší, hľadáme to, čo nás spája, nie to, čo nás rozdeľuje:-) A spájajú nás naše spoločné lásky: filatelia, obdivovanie krás vo všetkých podobách, večná mladosť...
                 

                 
  
   * Nádherné slnečné sobotné ráno.  Takto to vyzeralo na krpelianskej priehrade. Normálne plná labutí, kačíc, volaviek...,  teraz po nich ani vidu, ani slychu, ale zato tam plávajú takéto kúsky dreva   Nad Kraľovanmi prízemná hmla   V Ružomberku si prišli na svoje milovníci historických vozidiel   Sprievod historických áut a motocyklov išiel aj cez Valaskú Dubovú, ktorá bola východiskom na túru   Starí dobrí známi ešte z predchádzajúceho výstupu - krásne drevorezby ľudového umelca   Jeden záber pri výstupe   Pohľad späť na druhú stranu doliny   No neláka to človeka postáť tam hore a nadchýňať sa pohľadmi navôkol?   Na tejto poľane sa stretávajú turistické chodníky  z Vyšného Kubína, Jasenovej, Valaskej Dubovej a Likavky, len výstup z Lúčok je z opačnej strany Choča     To je už tesne pod vrcholom Veľkého Choča - typický zástupca jarnej flóry - horec Clusiov                   Mať tak krídla! Ale aj bez nich je ten pocit nádherný   Spoločný obrázok z vrchola       No a spoločné posedenie v Jánošíkovej krčme na filatelistov trochu nezvyklé,  nie pri malých obrázkoch známok, spomínanie na spoločných známych, rozoberanie spoločných záujmov...   Posledný pohľad na Sokol nad Valaskou Dubovou 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Z Detvy do Hriňovej...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Znova z Handlovej do Prievidze
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Králická tiesňava a vodopád
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Zákopčianskymi kopanicami
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Hmly a spomienky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Urda
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Urda
            
         
        urda.blog.sme.sk (rss)
         
                        VIP
                             
     
         jednoducho dôchodca, jednou rukou na blogu, druhou nohou v hrobe so životnou zásadou podľa Diderota: 
 "Lepšie je opotrebovať sa, ako zhrdzavieť" 

                                    * * * 
Krásy Slovenska som propagoval i tu: 
http://fotki.yandex.ru/users/jan-urda/ stále živá stránka - http://fotki.yandex.ru/users/jujuju40/ 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    517
                
                
                    Celková karma
                    
                                                7.55
                    
                
                
                    Priemerná čítanosť
                    2324
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Detstvo - spomienky
                        
                     
                                     
                        
                            Škola - spomienky
                        
                     
                                     
                        
                            Práca - spomienky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Z Turca
                        
                     
                                     
                        
                            Staroba - spomienky
                        
                     
                                     
                        
                            Filatelia - svet poznania
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Nezamestnanosť - najzávažnejší problém na Slovensku
                     
                                                         
                       Môj 4% ný kamarát.
                     
                                                         
                       Straka
                     
                                                         
                       Zadarmo do Žiliny
                     
                                                         
                       Oneskorene k 17. Novembru ....
                     
                                                         
                       Komunálne voľby prerušené na 38 minút.
                     
                                                         
                       Inzerát
                     
                                                         
                       Koľko je reálne hodné euro?
                     
                                                         
                       Gruzínsko - cesta do kraja bez ciest
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




