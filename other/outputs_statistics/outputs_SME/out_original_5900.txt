
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Kohutiarová
                                        &gt;
                Nezaradené
                     
                 Nevezmeš meno puberty nadarmo 

        
            
                                    5.5.2010
            o
            13:03
                        (upravené
                5.5.2010
                o
                13:18)
                        |
            Karma článku:
                9.23
            |
            Prečítané 
            2076-krát
                    
         
     
         
             

                 
                    Nálada pokleslá, čas pokročilý, výsledok mizivý, optimizmus žiadny. V hlave sa mi pripomína výrok ktoréhosi amerikánskeho veľčloveka o šiestich teóriach výchovy, ktoré parádne spľasli na počet nula, keď mal... šesť detí.
                 

                My sme na tom taky tak.  Ale asi je to jedno, koľko tých pubertiakov sa premelie životom.   I keď vraj, niektorí sú celkom neškodní, a oní rodičia si zväčša ten efekt pripisujú sebe. Aj mne by sa seba sa tak dobre škrabkalo, ale - nedá sa.   Zádrapka včera boli - klasické riady. Večierka, predĺžená podľa želania našich gymnazistov, ubiehala veselo, až tak, že ich ani netrklo - hlavne slečnu dcéru - že ono to má v dohode aj písmeno b - komplet poctivo spratanú kuchyňu.   A keď došlo na lámanie chleba a zistenie skutkovej podstaty, to vyrastené dieťa inokedy s veľmi vážnymi problémami zo zaspávaním neuveriteľne rýchlo zmizlo do hajan.   Meine nerven!   Sedeli sme na posteli ako dvaja pritvrdlí truhlíci, môj drahý soptil spravodlivým otcovským hnevom a ja som zúfalo vedľa neho čušala ako zmoknutá slepica. V hlave tisíc variácií,kombinácií, rovníc a nerovníc, prečo????   Nic naplat, z naplánovaného romantického večera v náručí bola figa rozmrzelá, a nadôvažok som si v pracovnej pošte našla - neuveriteľná súhra náhod - citát od Marka Ebena o puberťákoch, ktorí jednoducho experimentujú s nemožnosťou svojich rodičov, aby sa raz mohlo experimentovať na nich...   V tej chvíli by to Marek Eben schytal, aj keď ho celkom rada vidím a uznávam.   Keď si človek pripadá ako blb neschopný výchovy, komunikácie, dohody a rešpektu, to je na odpis. Aspoň v tom okamihu.   Ako to krásne okomentuje vždy naša pani doktorka, keď sa bavíme o deťoch: "Tak dlho ich človek piple, aby mal s nimi dobrý vzťah, aby všetko klapalo, dáva sa... a príde puberta a všetko je v keli." K tomu patrí patričný povzdych. Môj i jej.   Nejak sa utešujem, že to prežijeme, lebo to prežili všetci, a počítam roky, koľko ešte to potrvá... a dúfam, že tak, ako ju to skoro chytilo, tak ju to skoro prejde... a dúfam, že dovtedy budeme bez infartku a ostane nám napriek našej trápnosti z jej pohľadu a jej nezodpovednosti z nášho pohľadu dobrý vzťah.   Nuž teda - puberta u nás skúša všelijaké výbušniny, (a je k podiveniu, na čo všetko zle reaguje), ale inak sa máme radi.   Len keď náhodou budete mať voľné víza pre tetu Bertu do nejakej exotickej destinácie bez možnosti návratu, hlásime sa... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Rada volím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Tato
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Môj chlap sa skloňuje podľa vzoru hrdina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Hmmm, nič nebanujem...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Neberte nám školu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Kohutiarová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Kohutiarová
            
         
        kohutiarova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Divožienka. Manželka svelého muža. Mama 7 krásnych originálov. Človek, ktorý neprestáva snívať, smiať sa, vidieť veci inak, ako sú na povrchu.Občas to bolí iných i mňa - nie je to predsa "normálne".
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    205
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2172
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pastoračný plán Katolíckej cirkvi na Slovensku 2007-2013
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetko o grantoch
                                     
                                                                             
                                            John a Stasi Eldredge: Očarujúca
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            rádio Lumen
                                     
                                                                             
                                            ticho
                                     
                                                                             
                                            Spirituál Kvintet
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            David Kralik
                                     
                                                                             
                                            Ivanka Iviatko
                                     
                                                                             
                                            Juraj Drobný
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            o zahradach a stavbe domu /hadajte, kvoli comu/
                                     
                                                                             
                                            o deťoch a rodine
                                     
                                                                             
                                            MC Bambulkovo
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                                                         
                       Môj chlap sa skloňuje podľa vzoru hrdina
                     
                                                         
                       Čo už, 50tka
                     
                                                         
                       Pochopila som ...
                     
                                                         
                       Učitelia nevydržia štrajkovať dlhšie ako tri dni...
                     
                                                         
                       Moja spoveď alebo cesta hľadania partnerského šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




