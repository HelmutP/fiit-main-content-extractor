
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Miloslav Smrek
                                        &gt;
                Nezaradené
                     
                 Prejavy kliatby Svätoplukových synov na otvorenej scéne 

        
            
                                    6.6.2010
            o
            23:05
                        |
            Karma článku:
                10.50
            |
            Prečítané 
            1089-krát
                    
         
     
         
             

                 
                    Práve som dopozeral priamy prenos z odhalenia sochy kráľa Svätopluka na Bratislavskom hrade a je mi smutno. Veľmi smutno na duši, že ani toto symbolické pripomenutie si 1500 ročnej existencie Slovákov na území Nitrianskeho kniežatstva a Veľkej Moravy sa nezaobišlo bez hrubého a v absolútnej miere možného privlastňovania si histórie Slovákov, politickým zoskupením strán súčasnej vládnej koalície, presnejšie stranou SMER - SD. Počínajúc vyhlásením zámeru postaviť na nádvorí Bratislavského hradu sochu kráľa Svätopluka až po jej odhalenie, bola z tejto svetodejinnej udalosti - minimálne 1500 ročnej existencie Slovákov na tomto území - výhradná politická agitka strany SMER - SD.
                 

                 Pýtam sa teda vás, vážení predstavitelia strany SMER - SD, i Vás pán prezident, akým právom ste vylúčili z účasti na tejto, národnú hrdosť Slovákov tak umocňujúcej udalosti, dosť podstatnú časť Slovákov ? A to zjavne len tých, ktorí sú iného politického zmýšľania ako je vaše ? Pýtam sa vás, prečo ste neumožnili do základov sochy kráľa Svätopluka uložiť aj prsť zeme z Bratislavského kraja ? Je to snáď pomsta všetkým obyvateľom BSK len preto, že si na jeho čelo zvolili člena opozičnej politickej strany ?  A ďalej, či si nezaslúži byť v základoch sochy kráľa Svätopluka prsť zeme z obce Černová, rovnako ako sa tam uložila prsť zeme z pamätníka SNP ?   Prečo takto rozdeľujete Slovensko a jeho obyvateľov aj tam, kde je to nielenže neprípustné, ale dokonca škodlivé pre ďalší osud štátu i národa ?   Naviac, dôstojnosť chvíle narušili všetci slávnostní rečníci (najmenej prezident, najviac predseda NR SR) v danej chvíli neopodstatnenou a naprosto nevhodnou polemikou s kritickými názormi na postavenie sochy kráľa Svätopluka na nádvorí Bratislavského hradu, ba dokonca aj s názormi, popierajúcimi vyše tisícročnú existenciu Slovákov na tomto území.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Občania druhej kategórie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Aký prívlastok dať štátu, ktorý neprávom  zadržiava peniaze
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Farizejstvo, či úprimný záujem o týrané zvieratá väčší,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Nájde sa v slovenskom parlamente „bohatier boží,
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Miloslav Smrek 
                                        
                                            Patrí kontroverzná osobnosť na čelo riešiteľských tímov?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Miloslav Smrek
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Miloslav Smrek
            
         
        jozefmiloslavsmrek.blog.sme.sk (rss)
         
                                     
     
        Som dôchodca, ktorý sa neprestal zaujímať o dianie v našej spoločnosti,o morálne aspekty správy vecí verejných, o osud tohto štátu a jeho obyvateľov z hľadiska možných dôsledkov z politických nominácií na všetky vedúce funkcie v štátnych orgánoch a inštitúciach, obrazne povedané "štátnymi tajomníkmi počínajúc a upratovačkami končiac". Vo voľných chvíľach, ktorých dôchodca nemá nikdy nadostač, skúšam fotografovať a pre vlastné potešenie kompilovať hudobné CD prevážne zo slovenských ľudových piesni a pravoslávnych chrámových zborov.  

moje fotky na SME.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    49
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1134
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




