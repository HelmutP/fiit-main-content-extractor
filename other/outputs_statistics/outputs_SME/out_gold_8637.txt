

 Bratislava 7. februára (TASR) - Parlament by dnes mal schváliť
svoje vyhlásenie k Zákonu o Maďaroch žijúcich v susedných krajinách.
Stane sa tak až vtedy, keď sa skončí rozprava o tomto bode programu
54. schôdze. Na svoje vystúpenie čakajú šiesti poslanci HZDS, dvaja
členovia klubu Pravej SNS a dvaja nezávislí zákonodarcovia. 

 Rozprava bola limitovaná dvanástimi hodinami, čo znamená, že
každý člen parlamentu mal na vystúpenie 4 minúty 48 sekúnd. Najväčší
poslanecký klub HZDS má tak k dispozícii pre členov 3 hodiny 26 minút
24 sekúnd a najmenší klub PSNS 38 minút 24 sekúnd. 

 Poslanci by sa mali dnes podľa harmonogramu schôdze dostať aj
k legislatívnym bodom, keď vládny kabinet pripravil na prvé čítanie
návrh ústavného zákona o bezpečnosti štátu v čase vojny, vojnového
stavu, výnimočného stavu a núdzového stavu. Na to nadväzujú zákony SR
o obrane SR, ozbrojených silách, brannej povinnosti a vojenskej
povinnosti. Týmto návrhom zákonom bude predchádzať prezentácia návrhu
Ozbrojené sily SR - model 2010. 

 
*pm som 

