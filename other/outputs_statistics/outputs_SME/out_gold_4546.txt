

 
 
 Kolumbijská modelka Angie Sanclemente Valencia takouto sirénou moderného sveta pravdepodobne je. Z pódií módy a súťaží krásy sa preorientovala na prácu, vďaka ktorej si môže pestovať svoj predražený vkus a životný štandard. Od originálnej a srdce drásajúcej vety, „chcem svetový mier“, prešla k činom, k pašovaniu drog. O prevoz sa nestarali bodyguardi, esbeeskári alebo chlapíci s jemne sicílskym prízvukom ako z románu o Al Caponom, ale jej kolegyne – modelky. Vskutku geniálna myšlienka. Valencia vedela, čo robí. Kto iný by lepšie preniesol pozornosť strážcov z tašiek na seba? Nik! Stačilo si predstaviť, že letisková hala je mólo. Hlava sebavedome hore, podmanivý úsmev, pôvabná chôdza. Sanclemente Valencia vraj svojím kuriérkam radila, aby „boli vždy šarmantné, milé a priateľské ako stelesnení anjeli“. A ony také pravdepodobne boli. 
   
 Ani krava však nedojí večne. Tak to bolo aj s týmto desaťcentimetrovým podpätkovým gangom. Na letisku v Buenos Aires našli v batožine jednej z kuriérok 55 kíl kokaínu. Tá asi podcenila tréning modelingovej chôdze. Nech je ako chce, faktom ostáva, že na našu kolumbijskú sirénu Angiu Sanclemente Valenciu bol vydaný medzinárodný zatykač. Tak, ako každá vražedne inteligentná a krásna zbraň, ani Valencia nečakala, kým ju prídu zatknúť. V nádejí, že sa vyhne pochmúrnemu väzenskému oblečeniu bez módnych doplnkov, zmizla pred niekoľkými mesiacmi bez stopy, alebo priliehavejšie povedané, bez klopkajúcej stopy. 
   
 Nápad to bol vskutku nevšedný a brilantný. Zlyhal, čo už! Možno však už dnes Valencia niekde chystá nový fenomenálny a pritom šialený plán. 

