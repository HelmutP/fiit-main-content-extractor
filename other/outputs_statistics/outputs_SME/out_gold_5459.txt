

 Dostal som mail. Hlúpy mail. O pomoci Grécku. Od neznámeho autora, ktorý vôbec nič nepochopil. Nedá mi nereagovať. Dokonca som si kvôli nemu založil blog (ten som si už chcel založiť dávno, aby som mohol písať o tom, v akom krásnom svete žijeme, ale prinútil ma k tomu až zmienený mail). Jeho znenie : 
 ----------------------------------------------------------------------------------------------------------------------------------------- 
 
 
 Pomáhame krajine, kde: 
 *   minimálne odstupné predstavuje 24 platov  *   odstupné v štátnej správe je až 100 platov  *   štátni zamestnanci ročne dostávajú 14 až 16 platov  *   štátni zamestnanci majú príplatok pri dodržaní pracovného času  *   do dôchodku sa odchádza v 61 rokoch  *   dôchodok predstavuje až 100 % predchádzajúceho platu  *   slobodné dcéry generálov dostávajú penziu  *   vojakom štát uhrádza 8 leteniek ročne  *   na úradoch sa pracuje maximálne do 15.00 hod.  *   vláda falšuje účtovníctvo, aby zakryla dlhy    Čo by sme mohli mať, keby Grécku nedali pôžičku:    *   57 € do vrecka každého Občana  *   približne 15 km diaľnic v náročnom teréne  *   zníženie dane na benzín a naftu o 10 centov  *   každý 130. občan SR by dostal novú Škodu Fabia  *   všetky deti na ZŠ by mohli ísť 4 roky po sebe do letného tábora 
   
 ----------------------------------------------------------------------------------------------------------------------------------------- 
   
 To, čo bude nasledovať, berte s rezervou. Je to samozrejme pritiahnuté za vlasy, ale... 
   
 Aká by nakoniec bola (pravdepodobne) realita: 
 
   
 V skutočnosti na tejto našej "nezištnej" pomoci, na ktorú veľa ľudí frfle, pekne zarobíme na úrokoch.  Ale to nie je to podstatné. Podstatné je to, že ak by sme náhodou chceli týchto 300 mil. euro použiť "pre ľudí, pre Slovensko", tak tí naivní ľudia, čo tu napísali, čo všetko by sme za ne mohli mať, by boli veľmi prekvapení, ako by to vlastne dopadlo. Lebo realita by mohla byť aj takáto:    * nejakí ožrani by odrazu lietali na svojích nových stíhačkách F16  * nejakí šermiari by stavali nové sídla Elektra II - MMX  * nejaké kvádre (alebo spravne kádre?) by vytvárali  spletité siete garážových a podpivničných firiem (samozrejme s poctivým úmyslom  pestovať hrozno a vyrábať víno) 
 * 3 km 10-násobne predraženej diaľnice na rovinke s nekvalitným asfaltom, ktorý by musel byť menený každý rok (a záplaty robené každý mesiac) 
 * každý konkrétny Občan (rozumej zo správnej strany) by dostal limuzínu za 200 000 eur aj so šoférom, ktorý by na nej chodil za Občanom do krajín, kde by Občan letel lietadlom 
 
 * a takto by sme mohli pokračovať, ale píšem to neskoro v noci a za 3 dni idem na výlet, tak aby som ho nezmeškal, radšej tu skončím 
 
   
 Niektorí ľudia si neuvedomujú, že za túto situáciu v Grécku môžu grécki politici neskutočne podobní tým, ktorí momentálne vládnu u nás, na Slovensku. Tí grécki tiež sľubovali sociálny štát a blahobyt a to všetko samozrejme zadarmo, stačí ich len zvoliť a raj na zemi príde. 
 Ľudia sa nad touto pôžičkou rozčuľujú, ale to, že tá finančná pomoc Grécku nie je  ani len tretina peňazí, ktoré sa zrejme prešantročia len na PPP projektoch (a to  ani k tomu nerátam ďalšie veľmi závažné kauzy typu Emisie, Popolček,  Nástenkový tender, Mýto a mnoho, mnoho ďalších), to si nejako vôbec neuvedomujú (najmä istých konkrétnych 40% voličov). 
 
 Pre mňa je tá pôžička Grécku asi momentálne celkom dobrá investícia, lebo  tieto peniažky budeme mať relatívne bezpečne uložené mimo dosah zlodejských rúk našej skorumpovanej vlády. Nie je to veľa, ale aspon dačo pred nimi  uchránime. P.S.: Škoda, že tomu Grécku požičiavame len tých 300 mil. Ja by som bol  radšej, keby sme si takto uchránili o hodne viac :) 
 
 

