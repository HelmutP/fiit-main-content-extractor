

  
   
    V protiklade s ľuďmi, snažiacimi sa hľadať vinu za všetko len a len vo svojom okolí si uveďme slová istého starého človeka, ktorý pri pohľade na svoje zatopené pole skonštatoval: Sme zlí a preto nás Boh tresce! 
   
    Tieto slová prostého a jednoduchého človeka sú omnoho bližšie k pochopeniu skutočných príčin záplav, ako erudované formulácie meteorológov, vodohospodárov, krízových protipovodňových štábov, starostov, alebo primátorov, ktoré hovoria vždy iba dôsledkoch, ale nikdy nie o skutočných príčinách. Tieto slová totiž nehľadajú vinu v niekom, alebo v niečom inom okolo nás tak, ako sú na to ľudia vo všeobecnosti zvyknutí, ale hľadajú ju tam, kde naozaj je – v nás samotných!  
   
    Áno, jedine my, ľudia, sme vinní za to, čo sa deje! Podobný uzáver však bude s veľkou pravdepodobnosťou niečím, čo človek dneška asi veľmi ťažko pochopí a to z jednoduchého dôvodu – z nedostatku pokory a s ňou spojenej, zdravej sebareflexie.  
   
    Určite sa teraz mnohí spýtajú: Ako môžem ja osobne za to, že došlo k záplavám? Odpoveď už raz zaznela: Sme zlí a preto nás Boh tresce! V týchto slovách je naozaj vystihnutá podstata pravdy i keď to bude treba predsa len trochu spresniť.  
   
    V skutočnosti nás Boh vôbec netresce, pretože na to je príliš vznešený. Tvorcovi univerza však stačilo vložiť do fungovania stvorenia jeden jediný dokonalý Zákon, na základe ktorého sa my sami trestáme, alebo odmeňujeme. Je to Zákon spätného účinku – zákon akcie a reakcie! 
   
    Aká je akcia, taká je reakcia!  Čo kto zaseje, to aj zožne! Takto jednoducho, prosto a dokonalo to funguje. 
   
    Ak konáme dobro, vráti sa nám dobro a ak konáme zlo, vráti sa nám zlo. V účinkoch jedného a toho istého Zákona je teda zahrnutá všetka odmena i trest, je v ňom zahrnutá veľká a dokonalá Spravodlivosť Božia.  
   
    A teraz sa vráťme k povodniam, ktoré sú taktiež iba dôsledkom spomínaného Zákona. Z poznania jeho účinkov možno veľmi jednoducho vydedukovať, že ak ľudí postihlo niečo mimoriadne negatívneho, museli si to zapríčiniť oni sami svojou vlastnou negativitou. Čo kto totiž zaseje, to aj zožne! Ak ľudia sejú zlo, musia byť zlí, lebo podľa účinkov Zákona spätného účinku to jednoducho inak nie je možné!  
   
    V čom konkrétne sú však ľudia takí zlí, keď ich trestajú rôzne povodne, zemetrasenia, veterné smršte, či iné katastrofy?  
   
    Naozaj iba nedostatok pokory a zdravej sebareflexie nám znemožňuje jasne vnímať, v čom je ľudstvo zlé, ba čo viac, doslova zvrhlé a skazené!  
   
    Poďme teda rad za radom a pozrime sa napríklad na internet. Je plný zmyselnosti, zvrhlosti, nízkosti, špiny a duševnej prázdnoty. Pozrime sa na filmy, plné násilia, hrubosti, vulgárností a sexuálnych zvráteností. Pozrime sa na najčítanejšie bulvárne noviny a časopisy, plné ohovárania, vyzývavej telesnosti a prázdnych senzácií. Pozrime sa na ženskú módu, ktorej ideálom je byť sexy a ktorá je vytváraná s úmyslom čo najrafinovanejšieho dráždenia zmyslov. Pozrime sa na už úplne „normálne“ používanie vulgarizmov v bežnej, hovorovej reči, na nevraživosť, ohováranie a neprajnosť, tak veľmi rozšírené medzi ľuďmi. Ďalej na duchovnú prázdnotu a tupú honbu za hmotou, na egoistickú snahu získať čo najviac pre seba i na úkor druhých, na neprekonateľnú nenávisť, ústiacu do terorizmu, na snahu ovládať a zneužívať iné národy a tak ďalej a tak ďalej.  
   
    Takéto je teda prostredie, v ktorom žijú ľudia na zemi! V týchto dimenziách sa pohybuje všetko ich myslenie a cítenie! No a svojim nečistým myslením a cítením,  nízkym a nečistým vnútorným životom väčšiny obyvateľstva našej planéty sú neustále a každodenne produkované obrovské mračná negatívnej mentálnej a citovej energie. Je to doslova oceán neuveriteľnej nízkosti, špiny, malosti a úbohosti, ktorý zoviera a pevne obopína celú našu planétu. Oceán, do ktorého sa stále, dňom i nocou valia nové negatívne prúdy ľudského cítenia a myslenia! Desivý oceán zla, vytvoreného ľuďmi, ktorého hladina už kulminuje a v spätnom účinku, prostredníctvom rôzneho negatívneho prírodného diania postihuje skazené ľudstvo. Živly zeme, vedené inteligenciu prírody, vrhajú ľuďom naspäť do tváre všetko nimi samotnými vytvorené zlo! Toto je hlavná príčina súčasných i budúcich prírodných katastrof!  
   
    Ak im chcú ľudia zabrániť, pomôže iba jediné: Radikálny obrat v spôsobe myslenia a cítenia! Ak totiž chceme, aby sme dobro žali, musíme ho aj siať! Tento Zákon nepustí!  
   
    Ako na to? Každý z nás by mal v prvom rade očistiť svoj vlastný, osobný vnútorný život! Mal by vedome dbať na čistotu a ušľachtilosť svojho myslenia a cítenia! Mal by od seba nekompromisne odvrhnúť všetku špinu, nízkosť a úbohosť, ktorá naňho dolieha z filmov, časopisov, internetu, módy, atď.  
   
     Honbu len a len za čisto svojim osobným prospechom by mali ľudia nahradiť snahou po pomoci iným – snahou po prospechu všeobecnom a to v duchu zásady: Nikdy nerob iným to, čo nechceš, aby iní robili tebe! Alebo inak povedané: Ako chceš, aby sa iní chovali k tebe, tak sa chovaj aj ty k nim! 
   
    Nie je to nič nového! Sú to princípy, ktoré poznáme už stáročia! Princípy, ktoré sme však nikdy nezrealizovali, ale naopak, vždy sme kráčali a doteraz kráčame proti ním. Práve preto dnes žneme to, čo v prenesenom slova zmysle možno nazvať Božím trestom.  
   
    Nie je to však nijaký trest Boží! Je to iba žatva toho, čo sme ako ľudia vždy siali a čo ešte doteraz sejeme! Žatva, ktorej trpké plody budeme požívať dovtedy, kým sa nezmeníme!  
   
    A ak pozemské ľudstvo nebude vo svojej materialistickej slepote schopné spoznať skutočné príčiny, zodpovedné za všetko, čo ho stretá, príčiny, skrývajúce sa v ňom samotnom, živly prírody budú čoraz viacej stupňovať silu spätných účinkov a to až k neznesiteľnosti, aby nakoniec zmietli zo zemského povrchu všetkých tých, ktorí i napriek mnohým upozorneniam a výstrahám zostali do poslednej chvíle iba nepoučiteľnými škodcami, vytvárajúcimi vo svojom myslení a cítení len a len zlo.  
   
 M.Š. priaznivec stránky : http://ao-institut.cz/ 

