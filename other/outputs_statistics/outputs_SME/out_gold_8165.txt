

 V utorok 8. Júna o 16.30 budú v Bratislave pochodovať ľudia, ktorí sa nechcú prihliadať na vyostrujúce sa slovensko-maďarské napätie a netoleranciu v našej spoločnosti. Pochod tolerancie sa symbolicky začne v Medickej záhrade pri soche najväčšieho maďarského básnika Sándora Petőfiho a skončí pri soche gen. Milana Rastislava Štefánika, jedného zo zakladateľov novodobej slovenskej štátnosti, v komplexe Eurovea. Na začiatku i na konci podujatia bude krátky kultúrny program vrátane príhovorov. Toto podujatie je nepolitické! 
 A ja len môžem dúfať, že názov tohto článku je v súlade s jazykovým zákonom :) 

