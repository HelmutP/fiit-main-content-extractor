
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Gaborčík
                                        &gt;
                -
                     
                 Znásilňovaní mocou! 

        
            
                                    5.2.2010
            o
            21:41
                        (upravené
                15.2.2010
                o
                19:47)
                        |
            Karma článku:
                10.96
            |
            Prečítané 
            1548-krát
                    
         
     
         
             

                 
                    Kto je znásilňovaný mocou? V  dnešnej, modernej dobe je to čoraz viacej   obyčajný človek. Občan tohto, rád  by som povedal, sociálneho  a právneho štátu. Nemôžem, aj keby som chcel. Realita týchto dní hovorí o niečom inom. Sme vazalmi arogancie moci - jednotlivca, skupín, resp štátnych a vládnych inštitúcii. Za tým všetkým stojí moc peňazí a " kráľovstva krivých zrkadiel "- klamstva a  podvodov. Na úkor nás, čo sa snažíme žiť slušne, čestne a zodpovedne.
                 

                 Verím, že  je nás ešte vždy väčšina. To, čo vidíme okolo seba, je kríza  hodnôt a morálky, či si to chceme priznať alebo nie. Z toho pramení korupcia, klientelizmus a túžba zbohatnúť na úkor iného. Tzv. kauzy, prešlapy, porušovanie zákonov, ako aj mravných a etických noriem   sú už iba dôsledkom marazmu celej spoločnosti. Profit-zisk diktuje priamo, či nepriamo   cenu práce, tovaru a služieb. A predovšetkým determinuje  bohatstvo jednotlivca , či korporácie. No, nikdy by nemal určovať cenu života a ľudskej dôstojnosti. Preto máme rozum a slobodnú vôľu.   Na Slovensku sa zvlašť dobre darí mnohým aspektom a javom, ktoré do civilizovanej a kultúrnej spoločnosti nepatria. Je to široká škála-  ktorá vedie od podvodov, prania" špinavých peňazí," politickej prostitúcii", rozkrádania až ku skorumpovaniu niektorých štátných  úradnikov, ktorí by mali dohliadať na dodržiavanie zákonov,  resp. občianských slobôd. Vo väčšine prípadov  za tým všetkým stoja  tie isté záujmové skupiny, tzv. podnikateľov a sponzorov pod rôznymi logami firiem. V ich hľadáčiku  sú politici,či poslanci, nielen politických strán a hnutí , ale aj vytipovaní úradníci zo štátnej, či verejnej správy. Ide o  ľudí, ktorým boli delegované určité právomoci. Laicky povedané, ktorí  majú vplyv na určité rozhodnutia a majú tú správnu pečiatku. Najdôležitejšie pre týchto " žralokov" je, či sa títo dajú kúpiť, resp. či  sú vydierateľní cez svoje prešľapy v práci , či v súkromí. Potom to už ide. Peniaze robia rýchlo nové peniaze, určitá skupina bohatne a zveľaduje svoj podvodom nadobudnutý majetok. Majú informácie z prvej ruky o tendroch, výberových konaniach, štátných zákazkach atdˇ. Kúpia si členov komisii, dostanú sa k svojmu "biznisu"a tak sa im majetok utešene zveľaduje. Nikto nič, ako je u nás zvykom, nevidí, nik neprotestuje. Polícia je radšej slepá a na súdoch sa  spravodlivosti nedovoláš. Obyčajný človek je týmto stavom znechutený. Podnikavcom to len a len vyhovuje. Nie je žalobca, nik neprotestuje, tak si čoraz viacej dovoľujú a skúšajú hranicu ľudskej hlúposti a trpezlivosti. Skoro všetko je u nás založené na  poskytnutí protihodnoty, či nejakej  výhody. Niečo za niečo. Zašlo to tak  ďaleko , že sa " šijú zákony" na mieru pre určité záujmové skupiny. Títo rýchlokvasení zbohatlíci si kupujú miesta na kandidátkach politických strán,   externe  doštudujú na našich" skvelých" vysokých školách a stávajú sa vážení občania - "biele goliere"v politike. Ak to nestihnú, tak  za nich vyštudujú ich deti, obyčajne na súkromných vysokých školách. Kruh sa uzatvorí a tak to ide dookola.   Z niektorých sa stávajú"celebrity" - Smotánka je toho dôkazom , iní  radi ukazujú svoj  majetok verejnosti  na obdiv  - auta, ranče, hodinky. Takto prezentujú svoju moc a bohatstvo... " Znásilňujú"nás ľudia bez morálky, bez zásad. Chcú určovať, čo je pre plebs dobré a čo zlé, ako by sme mali žiť, t.j. vnucujú nám určitý životný štýl. Manipulujú nás v mene svojej moci a peňazí. To všetko na úkor psychiky a peňaženky obyčajného človeka. Ten toho začína mať plné zuby!   Teraz, znova po dlhšej  dobe, sa objavuje staronový fenomén  arogancie moci. Občan už nie je  "znásilňovaný "iba agresívnou reklamou v televízii, či v obchode, ale aj silovými zložkami štátnej moci. Polícia by mala odmietnuť plniť svoju represívnu úlohu, ak je nad rámec zákona. Nemôže iba slepo plniť rozkaz nadriadeného šplhúňa. Polícia aj preto postupne stráca svoj kredit. Popri tom viackrát, až smiešne pochybila. Je chybou, ak ide o politickú moc zaťahovať do predvolebného boja štátne inštitúcie. Stačí sa pozrieť, čo sa deje na Ukrajine. Aby bola akceptovaná spravodlivosť, nestačí iba striktne dodržiavanie zákonov, bez morálno-etických princípov. Tento rozmer absentuje  v politike aj v našej spoločnosti. Pritom  sa dementne čudujeme, kam to vlastne spejeme!! Kto to porušuje? Často tí, ktorí by mali byť vzorom a dbať na dodržiavanie zákonov, etiky aj morálky. To, čo vyvádzajú Harabínovi sudcovia, je výsmech, patriaci nám občanom tejto krajiny. My im priplatíme, lebo si to oni sami prisúdia, na ich vysoké platy, z našich daní!! Nechceli vykonávať funkciu sudcov  špeciálneho súdu, lebo sa báli previerok a  mafie,  t.j. často svojich" známych". Teraz im  závidia." Vďaka vám," najvyšší ústavní činitelia , právnici - Fico, Gašparovič aj Mečiar, Kaliňak, že ste dovolili  svojmu bývalému súdruhovi, aby dehonestoval súdnictvo a postaral sa o to , že je až taký "nezávislý" na zákonoch. Fakticky, nemáte na neho dosah a skoro každý sudca si  robí, čo chce. V jednej veci je desať názorov  na výklad paragrafu zákona. Harabin vás  riadne dobehol a vypálil vám rybník. Bude  tu aj po voľbách šarapatiť, zavádzať a "zarábať." "Pop"sa môže smiať. Ste fakt asi dobráci, babráci-česť práci! Kedy očistíte tento  rybník, plný bahna od plevelných rýb? Som zvedavý, ako dopadne zákon zákonov: "O preukazovaní pôvodu nadobudnutého majetku" v druhom čítaní,  či sa bude dať prakticky použiť. Aspoň budeme vedieť, ako sa my, občania, zariadime pri hlasovaní vo voľbách!! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Súčasná Matica slovenská-pýcha,či hanba národa?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Prezident nemôže perliť v parlamente.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Aj nás desí zloba p.prezident Ivan "hrozný"!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Mlčanie "ružových" jahniatok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Gaborčík 
                                        
                                            Kto utrie soplíky Ficovi?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Gaborčík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Gaborčík
            
         
        gaborcik.blog.sme.sk (rss)
         
                                     
     
        Spišiak - nie celkom "slepý". Súkromný vetrinárny lekár so záľubou komentovať politické dianie formou článkov resp. básní. Iné záujmy - šport, história,varhany, šachy, príroda.
Obľúbená pieseň: Goraľu, czi či ne žaľ. Pod úbočou...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1814
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Kačani
                                     
                                                                             
                                            Hubeňová
                                     
                                                                             
                                            Jurzyca
                                     
                                                                             
                                            cerveň
                                     
                                                                             
                                            Marec
                                     
                                                                             
                                            Hlina
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




