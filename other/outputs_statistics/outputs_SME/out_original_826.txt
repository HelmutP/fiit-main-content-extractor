
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lívia Pongrácová (Luptáková)
                                        &gt;
                Taká Adams family
                     
                 Deťom dáme divné mená... 

        
            
                                    29.8.2008
            o
            12:55
                        |
            Karma článku:
                9.41
            |
            Prečítané 
            3642-krát
                    
         
     
         
             

                 
                    „...slovanské mená. Alebo mená po predkoch, ale to už radšej tie slovanské. Alebo jednému dieťaťu tak a druhému tak. Prvorodený syn sa ale bude volať po mojich predkoch. Jednoducho mu meno vyberiem ja,“ vyhlásil jedného dňa môj drahý a na mňa z toho prišla mierna nevoľnosť. Historik sa nezaprie. „A s kým chceš mať tie deti?“ to bol môj protiútok.
                 

                 
  
  Odjakživa prikladám menám význam hádam aj väčší, ako si zaslúžia. Ako malá som si vymýšľala imaginárnych kamarátov nie preto, že by som sa cítila sama a túžila po spoločnosti, ale preto, aby som mohla niekoho nejako nazvať. Dať mu meno - také, aké sa mi v ten deň páčilo. Vymýšľala som mená aj pre seba a ostatných členov rodiny. Keď som mala štyri roky, snažila som sa všetkých presvedčiť, že sa volám Adliatka Hotanová. Bohužiaľ, nik si tento pseudonym nevedel zapamätať (len ja si ho pamätám doteraz). Moja fantázia nepadla na úrodnú pôdu a tak som sa vzdala a moje ďalšie akože-mená boli inšpirované už len kalendárom. No ešte dodnes občas nenápadne sledujem ľudí a hádam, ako sa volajú, alebo aké meno sa komu hodí. A teraz mám dovoliť niekomu aby dal môjmu (síce zrejme aj svojmu) dieťaťu meno Matej alebo Rasťo? To teda nie.    Už som skoro začala premýšľať nad hľadaním nového potenciálneho otca. Našťastie len skoro. A keďže zatiaľ nijaký možný nositeľ mena nie je na ceste, moja polovička usúdila, že na takúto debatu je priskoro a odmietol ďalej diskutovať. No mne to vŕtalo v hlave. Stále vŕta. A tu nastal ten zlom. Pretože práve toto je osvedčený ťah ako presvedčiť moju tvrdú hlavu. Povedať, len načrtnúť a nechať tak. A nechať, aby mi to vŕtalo v hlave. Nechať ma premýšľať bez nátlaku. A ja som premýšľala a pritom som si asi šesťkrát prelistovala kalendár a rôzne internetové fóra a stránky s menami. Aj s menami slovanskými. Hlavne s tými.     Veď vlastne nie sú až také zlé. Niektoré sú celkom pekné. Dokonca aj v kombinácii s partnerovým zvláštnym priezviskom. Napríklad Mojmír (pôvodný význam môj mier, môj svet). Alebo Kvetoslava. Alebo Vladimír (vládni svetu, veľký vládca). Či Ľubomíra. Aj Lada (podľa mena slovanskej bohyne krásy), ale tak sa už volá sesternicina dcérka a staré mamy (a nielen tie) dosť krútili hlavami. Ale ani Tichomír (s pôvodným významom tichý mier) nie je zlé meno. Ani Bronislava a Branislav (brániaci slávu, slávni obrancovia) (ale nie Branislava a Bronislav!). Našla som dokonca aj také, o ktorých som doteraz ani nevedela, že existujú – Borimír (bojujúci o mier) alebo Radana (variant mena Radovana).      Neviem, či sa mi tieto mená budú páčiť viac ako Jonáš, Jeremiáš, Ondrej, Hana, Petra a Paulína. A Cyril a Cecília pre dvojičky. Ostatne, tieto mám rada už dlhší čas. A pre staré mamy (a nielen tie) to budú mená rovnako podivné, ako tie s rýdzo slovanskými koneňmi (i keď ktovie...). Takže je to vlastne jedno. Hlavne keď je mi dopredu jasné, že budúci otecko práve s týmito menami súhlasiť nebude (možno okrem toho Mojmíra) a že nás konzultácie ohľadom mien budú stáť ešte veľa prebdených nocí a zrejme sa nevyhneme ani početným výmenám názorov. Aspoň že starý otec by bol rád. Ten bol Dionýz. Prastarký Karol Koloman (bežne mali aj viac mien, vlastne ja som tiež podľa krstného listu okrem Lívie ešte aj Viera) a jeho otec Baltazár. Je to už akási Pongrácovská tradícia – nezvyčajné mená.  A vyzerá to tak, že v nej budeme pokračovať.    Zas mám chuť, ako keď som mala štyri roky, niekoho nejako nazvať. Dať mu meno – také, aké sa mi práve páči. Napríklad Ratibor alebo Slávka. Len Rastislava doma určite mať nebudeme (bez urážky, vec vkusu a o tom našom vkuse už po tomto článku aj tak možno pochybovať).    Nevyriešilo sa nič (vlastne ani nebolo čo), len som si trochu popremýšľala a vymyslela kompromis. Slovanské mená sú krásne (minimálne tak krásne - ak nie ešte krajšie - ako tie nemecké, hebrejské a latinské). Ak raz budeme mať deti, určite ich naučíme, aby boli hrdé na svoj pôvod. Je na čo byť hrdý. Minimálne meno bude toho symbolom.            Viac o menách a o ich pôvode:     www.fodor.sk    http://slovnik.dovrecka.sk/etymologicky-slovnik-mien     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (91)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lívia Pongrácová (Luptáková) 
                                        
                                            Ako som bola nikto, aby som dokázala, že som niekto I.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lívia Pongrácová (Luptáková) 
                                        
                                            Ako sa v Londýne hľadá bývanie alebo kto je väčší blázon
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lívia Pongrácová (Luptáková) 
                                        
                                            Uvedomené vo vlaku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lívia Pongrácová (Luptáková) 
                                        
                                            25.11., 14.2. a potom už stále...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lívia Pongrácová (Luptáková) 
                                        
                                            Keď sa nepýtaš, nedostávaš odpovede
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lívia Pongrácová (Luptáková)
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lívia Pongrácová (Luptáková)
            
         
        pongracova.blog.sme.sk (rss)
         
                                     
     
        Já nejsem já a nejsem ani nikdo jiný.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1498
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Taká Adams family
                        
                     
                                     
                        
                            Rubrika spoločenská
                        
                     
                                     
                        
                            Zo spálne
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




