
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniela Nemčoková
                                        &gt;
                postrehy
                     
                 Psi brešú a karavána ide ďalej 

        
            
                                    24.5.2010
            o
            9:36
                        |
            Karma článku:
                8.77
            |
            Prečítané 
            1441-krát
                    
         
     
         
             

                 
                    Toto hovorieval starý otec jednej mojej kamarátky vždy, keď išiel proti prúdu a ľudia ho za to odsudzovali. Nedbal o to, lebo bol presvedčený, že koná správne... Táto kamarátka si to vzala za svoje a priznám sa, že aj mne sa zapáčilo...
                 

                 Aké jednoduché je povedať o druhom, že koná zle! Vtedy zažiarime v lepšom svetle, pretože my sme iní, my by sme tak nikdy nekonali!   „Najlepšie" vychovávajú deti tí, ktorí žiadne nemali, pretože teóriu ovládajú perfektne! Tiež som vedela presne, ako budem deti vychovávať, ale prax je často iná.   Napríklad: Vedela som presne, koľko času im dovolím stráviť pri TV. Ale keď som bola unavená po „nočnej šichte", radšej som ich posadila k telke, aby som mala aspoň chvíľku na oddych... Chcela som byť neoblomná. Ale keď dieťa začalo prosíkať a urobilo všetko preto, aby to malo, dovolila som to... Nikdy som nechcela na deti kričať, a predsa sa mi to stalo, keď pohár pretiekol... Nechcela som im dovoliť jesť v obývačke, ale sama som to robila, takže som od zákazu napokon upustila... et cetera... Každý, kto neskúsil, ako je to byť viacnásobnou matkou, ma môže odsudzovať...    Istá mama siedmich detí povedala: „Keď som nemala deti, mala som sedem teórií, ako ich vychovávať. Teraz, keď mám sedem detí, nemám žiadne teórie."   Poznám človeka, ktorý v živote prešiel peklom, lebo vyrastal bez rodičov. Urobil veľa chýb... napokon prišiel aj o svoju rodinu(ženu, deti)... Teraz ho to všetko mrzí a chce sa zmeniť. Ľudia ho však poznajú ako zlého človeka a odsudzujú ho... Ale toto je farizejské! Farizej sa cíti byť lepší ako mýtnik. Ale len mýtnik pozná cenu odpustenia... Nemali by sme aj my odpúšťať? Alebo sa aspoň snažiť pochopiť iných? Kto z nás prešiel tým, čím on? Kto z nás by z tohto pekla vyviazol živý a posilnený s ochotou zmeniť sa! Toto je väčšie hrdinstvo ako žiť si pokojný, ničím ohrozený bezúhonný život, keď ti akoby šťastie padalo z neba! Mne osobne takýto ľudia majú viac čo povedať.   Indiánske príslovie hovorí: „Ak chceš pochopiť iného človeka, prejdi v jeho topánkach aspoň míľu."   Keby sme počúvali ľudí, mohli by sme dopadnúť ako otec so synom, ktorí viedli somárika... Čokoľvek urobili (či sedel na ňom otec, syn, obaja alebo nikto), vždy sa našiel niekto, kto to skritizoval. Otec už len skonštatoval veľkú pravdu: „Ľuďom nevyhovieš."   Počúvam radšej rady priateľov, ktorým verím. Ale ani podľa nich sa neriadim... Kompas, ktorý ma vedie, je vo mne. Je to moje svedomie, ktoré ma ešte nikdy nesklamalo. Skôr som sa sklamala, keď som príliš dala na druhých... Často som bola zmätená, lebo jedni hovorili tak a druhí onak. Pritom stačilo, aby som sa stíšila a počúvala seba samu. Tam som našla správnu odpoveď. Hovorí sa tomu aj - počúvať srdce... Dokonca aj keď som stála pred vážnymi rozhodnutím, tú odpoveď som tam vždy našla, darmo som hľadala inde, mohla som preštudovať horu kníh, poučných, múdrych... Ale tá konečná odpoveď bola vo mne! Vždy je tam! Stačí dobre načúvať!   A pre toho, čo nepochopil nadpis, pripájam toto vysvetlenie:   „Ak sa ti zdá tvoja cesta ťažká, ale si presvedčený, že je správna, drž sa jej a nedbaj nato, čo si myslia iní." 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Oplatí sa bojovať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Prípad rozviazaných šnúrok a poľského kamionistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Kráska z internetu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            Bolesť - nepríjemná spoločníčka, dobrá učiteľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Nemčoková 
                                        
                                            "Dnes slnko vyšlo aj pre mňa"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniela Nemčoková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniela Nemčoková
            
         
        nemcokova.blog.sme.sk (rss)
         
                                     
     
        Som obyčajná žena-matka veľkej rodiny.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    117
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            deti
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            osobné príbehy
                        
                     
                                     
                        
                            príbehy iných
                        
                     
                                     
                        
                            socializmus
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Huljak
                                     
                                                                             
                                            Adriana Bagová
                                     
                                                                             
                                            Pavol Biely
                                     
                                                                             
                                            Ivana O Deen
                                     
                                                                             
                                            Marcela Bagínová
                                     
                                                                             
                                            Marek Ševčík
                                     
                                                                             
                                            Mária Kohutiarová
                                     
                                                                             
                                            Emília Katriňáková
                                     
                                                                             
                                            Peter Zákutný
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Oplatí sa bojovať
                     
                                                         
                       Babka, neseď stále za tým Facebookom
                     
                                                         
                       Prípad rozviazaných šnúrok a poľského kamionistu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




