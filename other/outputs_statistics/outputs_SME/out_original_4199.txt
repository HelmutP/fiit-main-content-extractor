
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Iveta Adamová
                                        &gt;
                femme fatale
                     
                 Pátranie po voličovi 

        
            
                                    8.4.2010
            o
            23:54
                        (upravené
                9.4.2010
                o
                0:46)
                        |
            Karma článku:
                5.70
            |
            Prečítané 
            462-krát
                    
         
     
         
             

                 
                    Keď sa slovenské strany po štyroch rokoch prebudili vo vojnových zákopoch, postavili sa k boju zodpovedne. Prv než si zadefinovali vlastný program, pustili sa do pátrania (sociologických štúdií) po výskyte ľudí, ktorí sú ochotní veriť na bociana, BMG a bilboardový populizmus,  v marketingovom slangu povedané „definovania cieľových skupín“
                 

                     Až si našli  a vo vedeckých laboratóriách definovali tzv. „nášho voliča“. Následne sa v médiách premlelo zopár vyjadrení: „Náš volič je stabilný, volič strany XY je nedôveryhodný, voliči tejto vekovej skupiny sú nedisciplinovaní...“  Z prieskumov sa dalo zreteľne pochopiť, že majitelia skupiny stabilných voličov sa v zákope pokojne môžu obrátiť na druhý bok. „To je druh, ktorý musí vyhynúť, aby strana padla“, pošepkala sociologička so strachom, aby ju niekto nepočul. „Nebojte sa, nemám zapnutý diktafón, presviedčala som ju. Ja len hľadám na laické preskúmanie stabilného, nedôveryhodného a nedisciplinovaného voliča...chcem vedieť kto sú, kde sa ukrývajú, čo jedia a či majú vysokorýchlostný internet. A vôbec, ako sa k nim dostali všetci tí slovenskí sociológovia a kde ich ukrývajú? Pustia ich pred voľbami? Ako ich skúmali? A čo ak ich nepustia? Kto bude potom voliť? .... skamenela a už už chcela pýtať môj výpis z registra trestov no vtom vošiel jej kolega. Taktiež v bielom plášti s päťciferným titulom...Vyzeral vážne, v ruke držal hárky s nápisom „Vyjadrenie vedeckej obce k mestám“ a turecký med.   „Viete, ľudia žijú rýchlo“, začal zmierlivo. Mnohí sa k svojej príslušnosti boja priznať. Niektorí sa natoľko obávajú inkvizície že sa boja priznať aj k slušnosti. Napriek tomu máme štatistické metódy a merania z uplynulých ročníkov ktoré jasne ukazujú účasť, správanie a politologické prognózy, mieru ich naplnenia, a správanie sa jednotlivých skupín. Voliči sa obmieňajú, starnú, aha tu v bode A vidieť entuziazmus vidieť to aj na tejto krivke, vidíte ako krásne ide hore? Tu sa nám prelína s demografickou krivkou, ktorá pokrivkáva pri poklese zamestnanosti a najhoršia konfrontácia je s apatiou voliča. Takýto druh sme museli hľadať až na Facebooku!“ Zvedavo som sa pritiahla bližšie.“ Tu! – vykríkol nahnevane. „V tomto bode G – jasná skepsa! Priam pochybnosť o jeho existencii!“ ....už ma nepotrebovali. Kolegyňa sa doňho pustila aby napadla jeho empirické fakty a ani nezaregistrovali kedy som sa vytratila z vedeckej kancelárie.   Sklamane som odchádzala....bez voliča, bez predvolebného upomienkového predmetu....smutne som pozrela na politický bilboard pri ceste s nástojčivým nápisom „Sme s Vami!“ „I s duchom tvojím“, odvetila som potichu... stabilne-nedôveryhodne a nedisciplinovane...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Adamová 
                                        
                                            Nemá každý pravdu, čo má prácu a nemá každý prácu čo má pravdu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Adamová 
                                        
                                            V januári u pediatra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Adamová 
                                        
                                            Pitva chatera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Adamová 
                                        
                                            Motýle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Adamová 
                                        
                                            Nos ako nástroj na všetko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Iveta Adamová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Iveta Adamová
            
         
        ivetaadamova.blog.sme.sk (rss)
         
                                     
     
        Odkedy som na tomto svete, mám pocit, že svetčania (či skôr svetci?) sú tak trochu blázni....
Na môj dotaz, nech mi dá 5 logických dôvodov, prečo by mal žiarliť, mi napísal toto: 

"1. žiarliť je ľudské =&gt; ergo žiarlim aj ja
2. si rozkošná =&gt; je dopyt .. ak je dopyt treba žialiť
3. včera si tvrdila že ťa zanedbávam 
4. ryšavé treba obzvlášť podozrievať
5. lebo sa ma pýtaš na logické dôvody a pritom je jasné že sa nežiarli z logických dôvodov."



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    937
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            femme fatale
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            obchodnú literatúru
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            srdce...a márne sa ho snažím umlčať
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.zuzanakapisinska.com
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




