
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Jankes
                                        &gt;
                Fototúlačky
                     
                 Ako som nevyšiel na Suchý 

        
            
                                    30.5.2010
            o
            23:42
                        (upravené
                30.5.2010
                o
                23:47)
                        |
            Karma článku:
                10.25
            |
            Prečítané 
            2037-krát
                    
         
     
         
             

                 
                    Po dlhom čase voľná nedeľa. Žena odišla na služobnú cestu do Bratislavy. Mám tisíc vecí, ktoré potrebujem urobiť, ale nakoniec sa rozhodnem vybehnúť na chatu na Magure.
                 

                 Pekne. Pozeral som predpoveď počasia na nete a podľa nej by malo dnes pršať. Zatiaľ to tak nevyzerá. Známou cestičkou stúpam pomaly hore.               Príjemný výstup. Chata je už nadohľad.      Síce sa trochu zamračilo, ale na dážď to zatiaľ nevyzerá. Po dobrom čaji na chate sa rozhodnem vyjsť až na Suchý. Je to ešte asi hodina a pol šľapania. Prechádzam okolo kvitnúcich čučoriedkových kríkov.      Obdivujem ako sa ako zo zámotku vymotávajú listy paprade.      Pomedzi posledné stromy sa objavuje prvý krát Suchý.      Na druhej strane vykukuje Malý Fatranský Kriváň s posledným fliačikom snehu.      Krajina okolo sa pomaly mení. Dostávam sa do oblastí nad pásmom lesa.  Mám takú krajinu rád.         Oblakov však začína pribúdať.      Keď sa dostanem do sedla pod Suchým, prvý krát zahrmí.      Radšej sa otočím. Nerád sa mocem po hrebeni v búrke. Ešte posledný obrázok Malého Kriváňa.      Keď som sa vrátil na chatu, začalo riadne pršať.      Na Suchý som dnes nevyliezol, ale neľutujem. Bola to dobrá príprava na vysokohorské túry, ktoré by som chcel absolvovť, keď skončí 15. júna uzávera v Tatrách. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Neverím, že sa tento rok preveziem po diaľnici medzi Dubnou Skalou a Turanmi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Štyri hodiny v Tatrách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Čaká sa tu dlho?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Bociany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Možno posledný sneh
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Jankes
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Jankes
            
         
        jankes.blog.sme.sk (rss)
         
                        VIP
                             
     
        Raz na to možno prídem.


  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    315
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1632
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovník cudzích slov
                        
                     
                                     
                        
                            Rozhovory
                        
                     
                                     
                        
                            Moje knihy
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Autozážitky
                        
                     
                                     
                        
                            cudzie perie
                        
                     
                                     
                        
                            Fototúlačky
                        
                     
                                     
                        
                            Kuchárska kniha pre úplných za
                        
                     
                                     
                        
                            mamonár
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            80 strán
                        
                     
                                     
                        
                            S kreslenými vtipmi
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ivan Štrpka: Rukojemník
                                     
                                                                             
                                            Pavel Hirax Baričák: 666 anjelov
                                     
                                                                             
                                            Ivan Jančár - Lee Karpiscak: Koloman Sokol 100 nepublikovaných d
                                     
                                                                             
                                            Inspire - magazine with a difference
                                     
                                                                             
                                            Zberateľ alebo zmluva s diablom
                                     
                                                                             
                                            Milan Kundera: Směšné lásky
                                     
                                                                             
                                            Egon Bondy: Mníšek, Vylitý nočník
                                     
                                                                             
                                            Rudolf Sloboda: Krv
                                     
                                                                             
                                            Rasťo Piško: Bohémska kolonáda
                                     
                                                                             
                                            Barry Miles: Charles Bukowski
                                     
                                                                             
                                            Dušan Mitana: Koniec hry - prvé vydanie
                                     
                                                                             
                                            Egon Bondy: Filosofické eseje sv.4
                                     
                                                                             
                                            Ivan Kadlečík: Škoda knižke nerozpredanej ležať
                                     
                                                                             
                                            Rudolf Sloboda: Rozum
                                     
                                                                             
                                            Rudolf Sloboda: Z denníkov
                                     
                                                                             
                                            Pierre Restany: Hundertwasser
                                     
                                                                             
                                            Liv Ulmann: Choices
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            London grammar
                                     
                                                                             
                                            Katarzia
                                     
                                                                             
                                            Korben Dallas
                                     
                                                                             
                                            Aminata Kamissoko
                                     
                                                                             
                                            Herbie Hancock Feat Corinne Bailey Rae - River
                                     
                                                                             
                                            Iva Bittova + Vladimír Václavek - Zvon
                                     
                                                                             
                                            Adele - Someone Like You (on 'Later Live with Jools Holland') -
                                     
                                                                             
                                            Grzegorz Karnas feat. Tomasz Szukalski
                                     
                                                                             
                                            Scott Dunbar - Sober
                                     
                                                                             
                                            Offret - Sacrificatio (1986) Andrei Tarkovskij / Part 1 / Eng. S
                                     
                                                                             
                                            Bach - Julia Hamari - Matthäus Passion - Erbarme dich
                                     
                                                                             
                                            Vladimír Václavek - Sen
                                     
                                                                             
                                            Lightnin' Hopkins - Goin' Back Home
                                     
                                                                             
                                            AG flek - Carpe diem
                                     
                                                                             
                                            Fred McDowell: Keep Your Lamp
                                     
                                                                             
                                            Seban Rozsa Buntaj - Come Together
                                     
                                                                             
                                            Scott Dunbar, ArtsWells Festival, Part 3
                                     
                                                                             
                                            Lenka Dusilová - Za vodou
                                     
                                                                             
                                            Stand By Me | Playing For Change | Song Around the World
                                     
                                                                             
                                            KRZAK - Blues dla nieobecnych
                                     
                                                                             
                                            Scott Dunbar One Man Band
                                     
                                                                             
                                            Bittová, Godár - Lullabies
                                     
                                                                             
                                            Vlasta Třšešňák - Atlant
                                     
                                                                             
                                            Sting - Shape Of My Heart
                                     
                                                                             
                                            Knopfler &amp; Clapton - Same old blues
                                     
                                                                             
                                            Tore Down - Chris Shepherd Band
                                     
                                                                             
                                            Iva Bittova - Uspavanka
                                     
                                                                             
                                            Chuck Beattie Band &amp; Kenny Dore
                                     
                                                                             
                                            Blind Boys of Alabama - Satisfied Mind
                                     
                                                                             
                                            'God Moves On The Water' BLIND WILLIE JOHNSON
                                     
                                                                             
                                            Eric Clapton Crosroads
                                     
                                                                             
                                            Suni Paz Performs Tierra Querida- Bandera Mia/Smithsonian Folkwa
                                     
                                                                             
                                            Guy Forsyth Band - Taxi
                                     
                                                                             
                                            Drum and Bass (Impressive street performer)
                                     
                                                                             
                                            Rihanna feat. Justin Timberlake rehab
                                     
                                                                             
                                            Justin Timberlake Vs Eminem
                                     
                                                                             
                                            Rupa &amp; the April Fishes "Une americaine a Paris"
                                     
                                                                             
                                            Audition (3)
                                     
                                                                             
                                            Fleret: Zafukané
                                     
                                                                             
                                            ČECHOMOR - Karel Holas - Rehradice
                                     
                                                                             
                                            Michal Prokop - Snad nám naše děti prominou..
                                     
                                                                             
                                            Michal Prokop - Kolej Yesterday
                                     
                                                                             
                                            Vladimír Mišík Variace na renesanční téma ( Večernice)
                                     
                                                                             
                                            Vladimir Mišík - Stříhali dohola malého chlapečka
                                     
                                                                             
                                            Knocking on Heaven's Door by Bob Dylan
                                     
                                                                             
                                            Prežijem
                                     
                                                                             
                                            Rachmaninov mal veľké ruky
                                     
                                                                             
                                            Kapela ze Wsi Warszawa - Live!
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Dášena
                                     
                                                                             
                                            zina veličková
                                     
                                                                             
                                            Anna Strachan
                                     
                                                                             
                                            Videoblog: Tanec je všetko
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Ivan Kadlečík
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Malá Fatra zo Strečna.
                     
                                                         
                       Stehno veľryby
                     
                                                         
                       Robert a jeho úžasný život v bubline
                     
                                                         
                       Konečne letím, alebo z Viedne do Quita (Ekvádor - 1. časť)
                     
                                                         
                       Suť
                     
                                                         
                       V úzkosti zo smrti
                     
                                                         
                       Päť užitočných otázok o Modrom z neba
                     
                                                         
                       Delová guľa Ruda Slobodu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




