
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Dedinský
                                        &gt;
                Nezaradené
                     
                 Kam sa stratila úcta k dievčatám a ženám? 

        
            
                                    18.9.2008
            o
            0:44
                        |
            Karma článku:
                13.57
            |
            Prečítané 
            7179-krát
                    
         
     
         
             

                 
                    Tento článok mnohý z Vás zrejme nedočítajú do konca, lebo v ňom spoznajú samých seba. Ale práve najmä pre tých je písaný. Už dlhší čas si totiž dosť podrobne všímam správanie sa chalanov k babám, mužov k ženám, kolegov ku kolegyniam, partnerov k partnerkám a zisťujem že ich správanie sa k sebe nieje na takej úrovni, akú by si osoby nežnejšieho pohlavia zaslúžili. Preto som sa rozhodol poukázať na niektoré základné etické chyby a nedostatky správania sa k ženám, s ktorými sa každý z nás najčastejšie stretáva. Pokúsim sa ich teraz v niekoľkých bodoch zhrnúť, a prípadne napíšem ako ich napraviť. Verím že po ich prečítaní si mnohý z Vás uvedomia svoje chyby vo vzťahu k vaším dievčatám či manželkám, a dúfam že aspoň niektorým z Vás tento článok pomôže v tomto smere vylepšiť váš vzťah.
                 

                  Nedostatok krásy – základná chyba, ktorou sa dá dievčaťu či žene ublížiť. Nikdy vašej partnerke nevyčítajte jej telesné nedostatky. A najmä nie na verejnosti! Veľmi by ste jej tým ublížili. Dievčenské srdce je v tomto smere veľmi zraniteľné. Každé dievča sa trápi kvôli svojej postave, každá chce byť pekná a príťažlivá, každá sa chce páčiť svojmu okoliu a najmä svojmu partnerovi. Ale nie každá dostala pri narodení do vienka dokonalú krásu. Takmer každá by chcela mať dlhé husté vlasy, štíhlu či ináč ideálne tvarovanú postavu. A máloktoré dievča môže za to, že má malé prsia, alebo veľký zadok. Samozrejme, niektoré veci sa dajú ovplyvniť správnym a zdravým stravovaním, alebo pravidelným športovaním. No niektoré veci sú dané geneticky a bohužiaľ sa s tým nič nedá robiť. Preto treba brať dievča také aké je a mať ho rád aj s jeho drobnými chybičkami krásy. A ak Vám predsa len na vašej nežnejšej polovičke niečo po telesnej stránke vadí, tak jej to povedzte jemne, s citom a hlavne ju podporte v každom jej navrhnutom riešení. Či už s ňou začnete chodiť do posilovne, alebo na plaváreň, masáže, prípadne jej ako darček objednáte omladzovaciu kúru na obnovu v kozmetickom štúdiu. A nezabúdajme na to, že škaredé ženy neexistujú. Dievča, ktoré sa mne môže zdať škaredé, sa inému chalanovi môže zdať neodolateľne pekné. Dievčatá bolo stvorené, aby skrášlili tento svet. Priznajme si, že každého chalana i muža poteší pohľad na pekné dievča, a každú ženu poteší, ak vedia muži ich krásu objektívne zhodnotiť a patrične oceniť.        Nedostatok nehy a jemnosti – chyba, ktorá pramení z nás, mužov. Tým že my sme „tvrdý“, tak často krát zabúdame na to, že ženy také nie sú. Klasický príklad: na križovatke nám druhé auto nedá prednosť a len prudké brzdenie nás zachráni od havárie. Ak v ňom sedí muž, tak ho „vytrúbime“ otvoríme  okienko a šťavnato mu vynadáme, aby si uvedomil svoju chybu. Ale ak v tom aute sedela žena /najmä ak je mladá a neskúsená/, tak nakričať na ňu by sme v žiadnom prípade nemali – len ju tým ešte viac vystrašíme, zmätieme a vynervujeme, čo bude mať za následok že ak bude pokračovať v jazde, tak z nervozity bude robiť ďalšie chyby. Toto isté platí aj pri všetkých podobných situáciách. Keď spraví dievča niečo zlé, tým že na ňu bezdôvodne nakričíme sa nič nezmení. Radšej si s ňou treba v kľude sadnúť a chybu vysvetliť. Veľa chlapov kričí na baby bezdôvodne a vynadá im za hocijakú drobnosť, len preto, aby ukázali kto je tu pánom domu. Týmto krikom sa snažia získať si rešpekt, čo sa im niekedy aj podarí. Mnohé dievčatá sa radšej proti svojej vôli prispôsobia, len aby bol kľud v dome a aby bol partner spokojný. A to isté platí aj o situáciách, ktoré nastanú v škole či v zamestnaní. A vrchol je, ak muž svoju ženu zbije. Štatisticky každá piata z vás zažije počas svojho života bitku či inú formu domáceho násilia. Takže milé dámy, keď sa najbližšie spolu stretnete, porovnajte si partnerov a skúste si tipnúť, ktorá z Vás to bude. Je to smutné, ale je to tak. Dievčatá sú bohužiaľ slabšie a v prípade bitky sa silnejšiemu partnerovi málokedy ubránia. A keďže sa hanbia vyjsť s týmto problémom na verejnosť, tak sa tento problém potom skryto vlečie ďalej. Väčšina z vás si totiž myslí, že ak Vám Váš partner dal facku či bitku len raz, tak vy mu to odpustíte a život ide ďalej. Lenže toto je veľký omyl! Ak muž čo i len raz stratí zábrany a udrie Vás, dokáže to potom už kedykoľvek bez zábran zopakovať.       Nedostatok úcty – úcta k dievčaťu by mala byť samozrejmosťou v každej situácii. No veľa chalanov či mužov na ňu zabúda. Úctou teraz nemám na mysli také samozrejmosti akými sú otvorenie a podržanie dverí, podanie čižiem, podržanie kabáta, prenechanie miesta na sedenie v autobuse, atď. Pod slovom úcta sa toho skrýva omnoho viac. Každý si myslí, že s úctou sa treba správať len k tomu svojmu dievčaťu. Ale čo tie ostatné? Tie ktoré zrovna nikoho nemajú? Alebo kamarátky Vašej priateľky, ktoré boli alebo idú niekam s Vami? To že s nimi nechodíte, lebo ste šťastne zaľúbený či ženatý, neznamená že by ste sa k ním mali správať menejcenne. Sú to rovnocenné jemné ženské bytosti, také isté krehké a zraniteľné ako aj Vaše dievča. Takže podľa toho by ste s nimi mali aj zaobchádzať.  Ak vonku prší je samozrejmosťou bez váhania odviezť každé dievča až pred vchod. V prípade tmy či neskorej nočnej hodiny nenechať žiadne dievča ísť domov samé, ale odprevadiť ich. V prípade sviatku negratulovať len sms-kou ale prísť s bonboniérou a kvetmi /pokiaľ ona, alebo vy nemáte extrémne žiarlivého partnera, ktorému by to mohlo prekážať/. Veľa chalanov a mužov si myslí, že úctivo sa majú správať len v prítomnosti svojej partnerky, a potom keď sa stretnú v krčme či na futbale, tak o svojich polovičkách rozprávajú chladne a bezducho, ako keby to bol nejaký spotrebný tovar. Z toho vidno že patričná dávka úcty tu stále chýba. Ak predsa mám niekoho rád, tak aj v jeho neprítomnosti by som mal o ňom hovoriť slušne a len v dobrom.        Nedostatok ohľaduplnosti – Dievčatá treba vždy rešpektovať, či už v ich názoroch, alebo požiadavkách. Každé dievča má svoje zásady, ktoré sú pre ňu smerodajné a vystihujú jej charakter a povahu. Či už sú to názory na predmanželský sex, potraty, vieru, drogy, alkohol, atď. Tieto názory treba vždy rešpektovať a ak má chalan na tieto veci iný názor ako jeho dievča, treba to riešiť s ohľaduplnosťou a v žiadnom prípade ani silou ani nátlakom. Nieje správne tvrdohlavo si dupnúť nohou „a bude po mojom“ alebo „môj názor je lepší, tak sa mi musíš prispôsobiť“. Dievča nikdy „nemusí“. Dievča buď chce alebo nechce, môže alebo nemôže. Ale nikdy nesmiete svoje dievča, alebo svoju ženu, nasilu nútiť do niečo, čo je proti jej zásadám. Každé dievča vie samo, čo „musí alebo nemusí“ a preto ak ju aj napriek tomu budete nútiť, tak sa jej to ešte viacej sprotiví. Veľká časť žien či dievčat nevydrží nátlak a partnerovým či manželovým požiadavkám podľahne, aj keď je to proti jej vôli. Odôvodní si to tým, že „je to môj chalan / manžel, a preto ho musím nakoniec poslúchnuť“. Lenže do vzťahu idete predsa ako rovnocenný partneri, a Váš partner Vám nemá právo rozkazovať a ani vás nasilu nútiť do niečoho, čo je proti Vášmu svedomiu či proti iným pre Vás dôležitým zásadám. O všetkom sa predsa dá v kľude porozprávať a v normálnom vzťahu sa dá vždy vytvoriť kompromis prijateľný pre obe strany.        Takže toľko o základných zásadách úctivého správania sa k ženám. Verím že viacerí z Vás v tomto texte našli sami seba a uvedomili si svoje chyby, a dúfam že sa ich úspešne pokúsia napraviť. Ani ja niesom dokonalý a priznám sa že tiež sa k dievčatám nesprávam vždy s takou úctou akú si zaslúžia, ale snažím sa zlepšiť. Snaha sa cení. Každý kto sa snaží zlepšiť svoj vzťah k druhému pohlaviu je na dobrej ceste za šťastím. Treba mať vytrvalosť a pevnú vôľu a úspech sa dostaví. Niet krajšieho ocenenia ako úprimná vďaka Vašej partnerky, radosť v jej srdci a úsmev na jej tvári.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (286)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Dedinský
            
         
        jurajdedinsky.blog.sme.sk (rss)
         
                                     
     
        Študujem geografiu na prírodovedeckej fakulte a snažím sa žiť naplno - veď žijeme len raz.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7179
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Anselm Grún - Buď dobrý sám k sebe
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Desmod, IMT Smile, Bon Jovi
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




