
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Oriešok 

        
            
                                    10.5.2010
            o
            8:49
                        (upravené
                10.5.2010
                o
                10:31)
                        |
            Karma článku:
                7.23
            |
            Prečítané 
            1127-krát
                    
         
     
         
             

                 
                    muškátový, všestranne používaný... Okorení, polieči, prevonia, vaše chuťové poháriky si ho obľúbia...
                 

                 
  
   Stálozelený strom, dožívajúci sa aj sto rokov, s krajinou pôvodu Indonézia, ponúka zaujímavé oriešky a kvety. Volá sa muškátovník pravý, Myristica fragrans. Pre informáciu uvediem, že oriešky, ako také, nie sú plody, ale jadrá semien plodu. Semienko je obalené v červenej čipkovanej šupke, ktorej sa hovorí muškátový kvet.   Pamätám si, že stará mama mala medzi koreninami odložené muškátové oriešky. Vždy boli  uzatvorené vo fľaštičke. Teraz už viem, že preto, lebo ináč by rýchlo stratili  svoju typickú  arómu. Ako malé dievčatko som tú fľaštičku rada brala do rúk a hrkala som si s orieškami, občas som fľaštičku pootvorila  a privoňala. V kuchyni starej mamy bol používaný dosť často. Obľúbenosť získal aj v kuchyni mojej maminky a tak je samozrejmé, že pokračujem v tradícii.   Ide o veľmi výraznú chuť, preto s dávkovaním musíme byť opatrní, navyše väčšie množstvá by mohli podráždiť žalúdok. Oriešok obsahuje veľké množstvo esenciálnych olejov, z ktorých asi 4% tvorí zaujímavý myristicín, ktorý pôsobí halucinogénne. Jemnejšou alternatívnou orieška je jeho kvet. Veľmi citlivým jazýčkom možno postačí práve on.   Oriešok je vhodné strúhať na špeciálnom strúhadle a odporúča sa pridávať ho tesne pred dovarením, nakoľko v horúcom prostredí stráca na svojej sile. Jeho použitie je všestranné, môže vhodne dochutiť polievky, či už mäsové, alebo zeleninové, zaujímavú chuť pridá svetlým omáčkam, mletému mäsku, či zemiakovej kaši, karfiolu, špenátu, ale aj syrovému fondue. Ani milovníci sladkostí si ho nemusia odoprieť. Môže sa pridať do krémov, kompótov, vianočného pečiva, sirupov.  Jeho stopu nájdeme aj v nejednom likéri. V dobrých kaviarniach iste nájdete v ponuke ľadovú kávu s muškátovým orieškom.   Muškátový oriešok je využívaný aj ako liečivka. Podporuje trávenie, pôsobí proti plynatosti, proti hnačke a je účinným prostriedkom proti nevoľnosti.   Hojne sa používa v homeopatii.   Zaujímavý indický recept proti hemoroidom znie: do bravčovej masti nastrúhať malé množstvo muškátového orieška.   Muškátový oriešok však nájdete aj v sprchovacom géle (nemenovaná kozmetická spoločnosť nedávno uviedla na trh regeneračný sprchový gél s višňou a muškátovým orieškom), je súčasťou viacerých kozmetických prípravkov (napr. peelingov). Veľké uplatnenie našiel aj vo svete vôní, siahajú po ňom svetoznáme parfumérske spoločnosti a pridávajú ho do svojich voňavých skvostov, páni sa môžu stretnúť s náznakom jeho vône vo vode po holení, jeho kozmeticko - liečivý účinok sa prejaví aj vo forme vlasového tonika.   Moja zbierka korenín obsahuje aj niekoľko muškátových orieškov. Mám ich uložené vo fľaštičke so zabrúseným uzáverom, aby som čo najdlhšie udržala jeho krásnu arómu.   Kedysi sa oriešok využíval ako amulet proti zlým silám.   Ak ste ho ešte neskúsili a nepoznáte jeho chuť, pritom  ste zvedaví a radi skúšate čosi nové, tak zopakujem na záver malú, ale dôležitú radu - používajte oriešok len v malom množstve a pridávajte ho v závere varenia.   Oriešok je dirigent, ktorý spustí na vašom jazyku zaujímavý koncert chutí...       PS: Niekedy sa mi zdá, že pre bohatú ponuku exotických korenín súčasného trhu, trochu zabúdame na tie tradičné. Preto som sa rozhodla občas niektoré z nich pripomenúť.   foto: zdroj internet                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (58)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




