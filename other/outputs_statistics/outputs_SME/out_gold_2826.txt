

 Terminológia: 
Na osobnú dopravu je potrebné pozerať z viacerých uhlov 
pohľadu. 
Existuje doprava verejná a neverejná, 
existuje doprava individuálna a hromadná 
existuje doprava pravidelná a nepravidelná, 
existuje doprava mestská, prímestská, diaľková. 
Kategórii by mohlo byť ešte viac ale toto sú tie základné, 
ktoré na bežné veci postačujú. 

 Napríklad električka v Bratislave z jedného konca mesta na 
druhý je doprava verejná, hromadná, pravidelná a mestská zvyčajne označovaná ako MHD. Firemný autobus, ktorý zváža zamestnancov z okolitých dedím do podniku je doprava neverejná, hromadná, pravidelná a prímestská. Napríklad jazda svojim autom do divadla alebo práce a naspäť  je doprava neverejná individuálna, nepravidelná a mestská, zvyčajne označovaná IAD. Napríklad také taxi z hotela na letisko doprava verejná, či individuálna alebo hromadná bude záležať od veľkosti vozidla, nepravidelná a mestská. 

 Prečo vôbec existuje MHD? 
Verejná, mestsá, pravidelná hromadná doprava, ktorú zvyčajne označujeme existuje z dvoch hlavných dôvodov: 
1, je to požiadavka na dopravné služby od obyvateľov skoncentrovaných na jednom mieste. 
2, je to spôsob riešenia dopravy v prípade nedostatočnej priepustnosti dopravnej infraštruktúry v obciach. 

 Čo je nutne potrebné aby MHD existovala? 
MHD, prípadne iné formy hromadnej dopravy môžu existovať a voziť cestujúcich pokiaľ sú splnené nasledovné podmienky: 
1, zastávky pre nastúpenie a vystúpenie cestujúcich 
2, cestná infraštruktúra pre jazdu vozidla 
3, vozidlo - dopravný prostriedok na prevoz cestujúcich 

 Súperenie MHD a IAD? 
Väčšina ľudí  si mysli že MHD a IAD nutne musí medzi sebou súperiť o cestnú infraštruktúru. Nie je to pravda. Súperenie o dopravnú infraštruktúru je jedine v prípade, že dopravná infraštruktúra je spoločná, prípadne križujúca sa a nepostačuje všade alebo aspoň jednom kritickom uzle; pre oba druhy dopravy. v čom MHD a IAD skutočne súťažia to sú obyvatelia a 
návštevníci mesta pre ktorý druh dopravy sa rozhodnú, samozrejme ak majú oba 
druhy dopravy k dispozícii. 

 Kto môže vykonávať verejnú hromadnú dopravu v mestách? 
Veľa ľudí si myslí, že jedine obec, lebo je všeobecný názor, že MHD musí byť dotovaná. Lebo je za každých okolnosti stratová a MHD žije len na základe objednávky dopravy vo verejnom  záujme, ktorá sa premieta do priamych a nepriamych dotácii obecného dopravného podniku. 
 
Lenže príklady z viacerých miest Slovenska ukazujú, že v hromadnej doprave začali podnikať aj súkromné subjekty a nie zo stratou. Niekde 
sú to klasické MHD linky,  niekde kombinácie prímestského autobusu a MHD a niekde MHD spojenie rôznych obci. 
 
Boje medzi obecnými dopravnými podnikmi a súkromnými dopravcami je oveľa ostrejší ako boj medzi MHD a IAD. Zo znalosti reálny prípadov viem, že najprv chceli od súkromných dopravcov poplatky za každé zastavenie na zastávke. Keď to súkromní dopravcovia akceptovali, tak došlo k zvýšenú ceny za zastavenie a nakoniec k zrušeniu možnosti zastaviť na zastávke. V praxi to viedlo k tom, že na autobusové zastávky sa dávali dopravne značenia zákaz zastavenia okrem dopravného vozidiel podniku xy. 

 Prečo sa mi nepáči obecný monopol na MHD? Monopolné riešenia sú vo všeobecnosti zlé. Na jednej strane cena za ich služby rastie a efektivita klesá. z praxe je zrejme, všade tak kde bol uvoľnený trh do konkurenčného prostredia došlo k výraznému nárastu kvality a poklesu ceny. Stačí sa pozrieť na telekomunikačný trh. 

 Ako vytvoriť konkurenčné prostredie v MHD? 
1, Treba oddeliť správu a prenájom zástaviek MHD od obecného dopravného podniku a stanoviť poplatok za zastavenie na zastávke. Samozrejme, že poplatok sa môže v závislosti na konkrétnej zástavke a čase v dni 
meniť ale musí byť pre všetkých rovnako vysoký. 

 2, Treba spoplatniť cestnú infraštruktúru. Momentálne sú spoplatnené len niektoré cesty a to len vďaka elektronickému mýtu, ale tento systém je potrebné rozšíriť na všetky cesty a pre všetky vozidla vykonávajúcu 
hromadnú dopravu. 

 3, Zmeniť spôsob objednávania  dopravy vo verejnom záujme. Momentálne to funguje tak, že financie dostane obecný dopravný podnik ako sumu na zabezpečenie dopravy. Zmeniť to treba tak, aby boli objednávky vo verejnom záujme adresné. Napríklad pre deti, dôchodcov, dopravu z alebo na určité mesto, alebo na dopravu v určitom čase alebo kombinácie. Čiže objednávali/dotovali by 
sa konkrétne jazdy, konkrétny cestujúci a nie dopravca ako taký. 

 V akej výške dotovať MHD? 
Veľa ľudí si myslí, že dotácie do MHD pochádzajú od niekoho 
tretieho a mali by byť čo najvyššie. Lenže nie je to pravda. Dotácie pochádzajú s rozpočtov miest, v niektorých prípadoch VÚC a štátu. Lenže ani tam nikto peniaze nenatlačil ale ide o prerozdelenie peňazí vybraných na daniach a podobne. v princípe sa  ľudia môžu legitímne dohodnúť či všetko bude platiť cestujúci zo svojho, alebo obyvatelia cez svojich zástupcov prerozdelia z daní a poplatkov takú časť, že pokryjú náklady na MHD paušále a cestujúci bude platiť len symbolickú sumu. 

 Ako by sa malo platiť za jazdu MHD? 
Základný poplatok by mal byť za osobokilometer s tým, že prevádzkovateľ môže stanoviť okrem základného spôsobu aj iné spôsoby ceny lístka. v konkurenčnom prostredí sa medze kreatívnosti nekladú. Samotná platba by mala byť bezhotovostná a to či už kreditná, debetná, účtovaná ihneď alebo na konci mesiaca a podobne. Dnešná technika umožňuje najrôznejšie spôsoby účtovania a aj platby za dopravu v prostriedku MHD. Dnes sú možnosti evidovať a účtovať - platiť nielen pri nastúpení do vozidla ale aj pri vystúpení a platiť tak len skutočnú vzdialenosť. Napríklad k tomuto 
účelu sa dajú použiť bezkontaktné čipové karty, keď sa cestujúci prezentuje 
priloží kartu k snímaču pri nastúpení do vozidla a potom pri vystúpení. Alebo dá sa použiť aj cyklické skenovanie kariet prítomných vo vozidle. Ak karta sa vo vozidle prestane nachádzať, znamená to že cestujúci vystúpil. Technologické 
možnosti sú priam neobmedzené. 

 Kto má rozhodovať kde a kam bude jazdiť MHD? 
Na rozhodovanie odkiaľ kam ma jazdiť MHD, majú mať vplyv 
dve veci. Práva je záujem cestujúcich, ktorý si ju platia a druhou sú objednávky vo verejnom záujme. Mohlo by sa zdať, že ide o totožnú požiadavku ale nejde. Prerozdeľovanie prostriedkov vybraných od obyvateľstva sa často robí korupčným spôsobom a proti záujmom občanov. Čím sa prerozdeľuje vyššia suma tým je vyššia korupcia a v našej spoločnosti to platí doslova všade.  

 Ekológia MHD a IAD. 
Nie je pravda, že MHD je ekologický a IAD je neekologická. Môžme porovnávať len reálne exhaláty a dopady na životné prostredie k prepravný výkonom. Pravdepodobne najlepšie dopadnú vozidlá na elektrický pohon, ale u oboch kategórii. Je pravda, že v súčasnosti sa u MHD oveľa ľahšie a účinnejšie zavádzajú ekologické riešenia - električka, trolejbus čo v značnej miere otáča ekologický pohľad v prospech MHD. 

 Preferencia MHD áno alebo nie? 
Odpoveď je jednoznačne áno. Lenže z rozumom. Bratislavčania majú v živej pamäti preferenciu električiek vychádzajúcich z tunela, ktorá spôsobila dopravný kolaps. Autám ale aj náhradným autobusom MHD zostala na 
semafore svietiť červená, lebo električka zostala stáť v tuneli na snímači. a tak preferencia prednosti MHD zablokovala aj samotnú MHD. Viac v článku z tej doby. 

 V zahraničí sa to robí tak, že nejde len o preferenciu zelenej pre MHD na niektorých križovatkách ale preferenciu MHD v rámci celého 
systému riadenia dopravy. Najprv sa načasuje jazda za normálnych okolnosti a z nej sa spraví jazdný plán. Ak vozidlo ide pola tohto plánu tak preferenciu nedostane. Ale ak mešká tak ju dostane. Samozrejme, že sa berie v úvahu aj či vozidlo MHD môže pokračovať ďalej. Zbytočná je preferencia jazdy cez križovatku keď na zastávke za ňou stoja z nejakého dôvodu predchádzajúce vozidla MHD.  
 Čo sa týka mojej kritickej poznámky k návrhom obmedzovať IAD aj keď to dopravná situácia nevyžaduje s cieľom nútiť ľudí používať MHD, poviem len toľko. Dajte si príslušné otázky do Google a nejdete ich tam. 


 Prečo ľudia majú prostriedok IAD aj keď často chodia MHD? 
IAD predstavuje určitú slobodu. Na rozdiel od MHD ide aj vtedy keď MHD nejde, ide na také miesta kam MHD nechodí, ide bez prestupovania, odvezie aj náklad a keď ide plno obsadené auto tak je aj lacnejšia ako MHD a to 
nehovorím o odolnosti voči počasiu a pohodlí a pocitu bezpečnosti. Značná časť ľudí keď si zvykne na IAD, tak je malá pravdepodobnosť návratu výlučne k MHD. Väčšina ľudí sa operatívne rozhoduje, ktorý druh dopravy v akom prípade využije. Navyše ak je v rodine niekto telesne postihnutý tak návrat k MHD je prakticky nemožný. Preto si myslím, že spoliehať sa len na MHD a nemať nič v zálohe je zlé riešenie. 

 Prečo v niektorým metropolách návštevníci odstavujú IAD a jazdia MHD? 
Pomerne často cestujem do Prahy. Samozrejme si objednávam 
hotel aj s parkovaním v blízkosti metra. Zvyčajne zostane zaparkované a využívam ich MHD. Prečo tak robím a nielen ja. Prečo množstvo vodičov odstaví svoje vozidlá na záchytných parkoviskách? Je to z jednoduchého dôvodu,  MHD je tam rýchlejšie ako jazda autom.  

 Ako budovať mestá aby nebol problém s dopravou? 
Nové projekty vo svetových metropolách ukazujú na to, že najlepšie je vtedy keď nároky na dopravu sú minimálne. Znamená to budovať mestské štvrte tak, že v rádius 500 metrov pokrýva  všetko čo obyvateľ potrebuje: bývanie, prácu, obchody, relax, služby. Vtedy postačuje chôdza peši, prípade jednoduchá vozítka. Horšie je to ak mesto postavené je a obyvatelia musia každý deň cestovať z jedného konca mesta na druhý z prácou, zábavou nákupmi, športom a podobne. Ak je dopravná infraštruktúra dostatočná tak problém 
nie je. Ak sa vytvárajú občasné preťaženia tak sa to dá riešiť operatívnym 
smerovaním dopravy alebo je rozložením na dlhší časový úsek. Už nie je nutnosť 
aby všetky podniky začínali pracovať ráno o 7:00 a končil o 8,5 hodiny neskôr a obyvatelia ako stádo sa presúvalo v stanovený čas tam a naspäť. Dnes je už veľká rôznorodosť začiatku a konca pracovnej doby a táto rôznorodosť sa bude ďalej zvyšovať, plus bude pribúdať práca doma prostredníctvom internetu. Ak nič nepomáha, tak jedna z možností je vybudovať nosný dopravný systém pre MHD, ktorý nebude kolízny z doterajšími druhmi dopravy. 

 Bratislava - prípad nekoncepčného riešenia. 
Bratislava má z hľadiska dopravy špecifické miesto. Skúste 
návštevníkovi napríklad z Prahy povedať kde je záchytné strážené parkovisko, kde má odstaviť svoje auto a využívať rýchlejšiu MHD. Jednoducho v Bratislave nie je jediné záchytné parkovisko a nie je tu ani rýchlejšia MHD. Ďalšia vec, ktorá len zhoršuje dopravnú situáciu je stavba nových objektov bez dostatočných parkovacích kapacít a aj samotná výstavba je napojená do dopravne preťažených komunikácií. Výsmechom je povolenie stavby s tým, že napojenie na dopravnú infraštruktúru sa bude riešiť až pri kolaudácii stavby. 

 Statická doprava v Bratislave. 
V Bratislave miznú možnosti na parkovanie, na ich miestach sa stavajú objekty kladúce väčšie požiadavky na parkovania a aj dynamickú dopravu. Navyše fungujú aj organizovane skupiny, ktoré spoplatnia v prospech seba aj parkovanie na mestských alebo cudzích plochách. Ak niekto niekde zriadi parkovisko alebo parkovací dom, tak spravidla v širšom okolí zakáže nielen stáť, ale aj zastaviť a parkovanie počíta hneď za načatú hodinu, hoci vozidlo tam zastaví len ma čas do 5 minút. Dnešné technológie umožňujú aj účtovať parkovanie aj po sekundách a mestá majú možnosť vydať aj všeobecne záväzné nariadenia na riešenie statickej dopravy. Lenže to čo sa robí sú vo väčšine aktivity ako na parkovaní a odťahovaní aut čo najviac zarobiť a v prospech koho majú vybrané peniaze ísť. 

 Čo robiť v Bratislave? 
Postavenie diaľnice D4 čo je nultý obchvat Bratislavy, trochu zníži objem 
vozidiel v meste asi na dnešnú úroveň. (kým ho postavia tak počet vozidiel ešte výrazne stúpne). Väčšina mestských trás je pomerne dobre prejazdných. Aby mesto zostalo prejazdne naďalej, treba namiesto úrovňových križovatiek budovať podľa možnosti mimoúrovňové križovatky. 
 Je potrebné obmedziť výstavbu bez dostatočného napojenia na 
dopravnú infraštruktúru. Všetky veľké stavby by mali mať v sebe aj terminál pre MHD. Momentálne je ovale väčší problém v statickej doprave, kde kýcha množstvo parkovacích kapacít. Navyše aj tam kde sú, respektíve boli, aké také parkovacie kapacity. Tam boli tieto parkoviská mestom predané investorom a postavili sa na nich stavby z nedostatočnými parkovacími kapacitami pre seba a nie pre doteraz tam parkujúcich. Myslím si, že tak ľahko nedôjde k zbúraniu takto postavených objektov a vyvodeniu osobnej zodpovednosti voči predstaviteľom mesta a mestských častí, ale do budúcne treba zastaviť zahusťovanie existujúcich sídlisk. 
 Pri nových výstavbách musia investori vybudovať alebo 
prispieť na cestnú infraštruktúru a to nielen na samotnú cestu ale aj zástavby a terminály MHD. Dostatok parkovacích kapacít, nielen pre obyvateľov ale aj pre návštevy a služby je nutnosť, bez ktorej by sa nemala povoliť, žiadna stavba vyžadujúca dopravnú obsluhu. 


 Ako sa bude meniť MHD v budúcnosti? 
Budúcnosť MHD nie je čierna. Verejná doprava je jeden z 
fenoménov voľného pohybu osôb a bude sa ďalej rozvíjať. Bude sa bude meniť výzor miest, spôsob práce a život ľudí. Podľa mňa pribudne viacero individuálnych požiadaviek na MHD. Pribudnú požiadavky aby MHD jazdila čase v ktorom dnes nejazdí, pribudnú požiadavky aby jazdila do miest kde doteraz nejazdí. Pribudnú požiadavky na expresnú MHD, ktorá nebude stáť na každej zástavke. Bude to smerovať k zničeniu počtu miest vo vozidlách MHD, k zvýšeniu komfortu. MHD bude akceptovať individuálne požiadavky na zmenu cestovného plánu a pribudne elektronické objednávanie trasy a termínu jazdy. Celkovo to bude smerovať k vozidlám MHD bez vodičov a nasledovať bude praktické splynutie všetkých dopráv do jedného riadiaceho systému. Na druhej strane pribudnú rýchle nekolízne komunikácie spájajúce rôzne časti mesta napojené na mimo mestskú dopravnú sieť, čím sa výrazné skráti čas strávený pri doprave a stúpne jej komfort. 

 Záver: 
MHD je jednou z foriem služieb pre obyvateľov a návštevníkov miest a bude fungovať tak ako ju budú využívať. Ako ju budú využívať to záleží od jej kvality, možností a ceny. Každé mesto musí riešiť dopravu v predstihu, už pri územných rozhodnutiach a nie až potom, keď veľa priestoru pre dopravu nezostane. Aj u MHD je potrebne zrušiť umelo udržiavaný monopol a umožniť v nej konkurenciu. 

