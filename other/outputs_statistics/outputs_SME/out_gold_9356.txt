

 Podľa grafu zobrazujúceho percentuálnu výhodu podľa mesačného príjmu je vidieť, že solidarita odvodového bonusu sa končí pri 1400€, kde krivka opäť začína vzrastať až k takmer 25% pri 2900€ a potom pomaly znovu klesá. Či jednotlivci s príjmom 2900€ mesačne sú tí, ktorí odvodový bonus potrebujú najviac, je na zamyslenie. 
  
 (kliknite na obrázok pre zväčšenie) 

