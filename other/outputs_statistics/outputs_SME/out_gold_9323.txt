
 Jupiterova rodina prevádzkuje bazár, kde leží aj hlavný stan pátračov. Sídlo tvorí starý nepojazdný príves obsypaný nepotrebnými vecami, takže stan nie je z vonku vidieť. Existuje k nemu niekoľko dômyselných tajných vchodov ( napr. zelený tunel dva, štyri atď.) K vybaveniu prívesu patrí telefón, fotokomora, kartotéky a dielňa, v ktorej Jupiter montuje zariadenia, väčšinou z vyradených položiek na smetisku, ktoré pomáhajú vyšetrovateľom v detektívnej práci. 
 
Vizitka, ktorú používajú Traja pátrači vo svojich príbehoch: 
 
TRAJA PÁTRAČI 
VYPÁTRAME VŠETKO 
??? 
Prvý pátrač – Jupiter Jones 
Druhý pátrač – Peter Crenshaw 
Záznamy a výskum – Bob Andrews 
 
Traja pátrači sa stretávali s týmito postavami: najčastejšie to bola postava Alfreda Hitchcocka, ktorého si získali dôveru. Poveroval ich novými prípadmi a napokon ich zdokumentoval. Naposledy spolupracovali pri „Záhada žraločieho útesu“. Ďaľšou postavou bol fiktívny detektívny spisovateľ, ktorý prijal „sponzorstvo“ od príbehu „Záhada prázdneho hrobu“. 
 
Medzi ďaľšie postavy príbehov je nutné spomenúť britského šoféra Worthingtona, ich odvekého nepriateľa rovesníka Skinnera Norrisa, strýka Titusa a tetu Matildu,  ich bavorských  pomocníkov Hansa a Konráda, náčelníka polície v Rocky Beach šerifa Reynoldsa, s ktorým mali prvýkrát česť sa stretnúť v „Záhada zeleného strašidla“, a napokon francúzskeho zlodeja obrazov Victora Hugenaya, ktorý sa objavil v dvoch knihách ( „Záhada zajakavého papagája“ a „Záhada vreštiacich hodín“ ).  
 
Spisovatelia Troch pátračov: 
 
Pôvodný autor Robert Arthur napísal v roku 1964 prvý príbeh „Tajomstvo strašidelného zámku“. Veľká popularita si vyžiadala napokon 10 kníh, ktoré písal až do svojej smrti. 
 
William Arden, vlastným menom Dennis Lynds bol prvým, kto nasledoval Roberta Arthura, hoci jeho prvý príbeh „Záhada stonajúcej jaskyne“ prišiel na svet ešte pred smrťou stvoriteľa Troch pátračov. Celkovo napísal 13 kníh vrátane poslednej knihy pod záštitou Alfreda Hitchcocka ( „Tajomstvo žraločieho útesu“). 
  
Mary Virginia Carey - jej prvá kniha nazvaná „Záhada horiacich stôp“ vznikla v roku 1971 a odvtedy za 16 rokov napísala 15 kníh, teda viac ako ktokoľvek iný. V jej knihách sa často objavujú paranormálne javy, ktorými bola fascinovaná. 
  
Nick West, vlastným menom Kinn Platt sa zaslúžil o pridanie dvoch kníh „Záhada kašľajúceho draka“ a „Záhada nervózneho leva“. 
 
Marc Brendel sa podielal na sérii tromi knihami, ale boli rovnako významné. Hlavne kniha, kde sa ponárame do minulosti Jupitera Jonesa ( Mystery of the Rogues´ Reunion ), ktorá však ešte nevyšla v slovenčine. 
  
Časom sa pridali spisovatelia hlavne z Nemecka a Rakúska: André Marx, Ben Nevis, Brigitte-Johanna Henkel-Waidhoferová,  Kari Erlhoff, Astrid Vollenbruchová, Marco Sonnleigner, André Minninger a Katarina Fischerová. Séria začínala v originály označením „Die Drei ???“. 
 
Kompletná séria Troch pátračov chronologicky vydaných v slovenčine: ( zatiaľ 60 titulov )
 
1.Tajomstvo strašidelného zámku(The Secret Of Terror Castle 1964)Robert Arthur 
2.Záhada zajakavého papagája(The Mystery Of The Stuttering Parrot 1964)Robert Arthur 
3.Záhada šepkajúcej múmie(The Mystery Of The Whispering Mummy 1965)Robert Arthur 
4.Záhada zeleného strašidla(The Mystery Of The Green Ghost 1965)Robert Arthur 
5.Záhada strieborného pavúka(The Mystery Of The Silver Spider 1967)Robert Arthur 
6.Záhada miznúceho pokladu(The Mystery Of The Vanishing Treasure 1966)Robert Arthur 
7.Tajomstvo Ostrova kostier(The Secret Of Skeleton Island 1966)Robert Arthur 
8.Záhada kašľajúceho draka(The Mystery Of The Coughing Dragon 1970)Nick West 
9.Tajomstvo pruhovanej mačky(The Secret Of The Crooked Cat 1970)William Arden 
10.Záhada ohnivého oka(The Mystery Of The Fiery Eye 1967)Robert Arthur 
11.Tajomstvo vreštiacich hodín(The Mystery Of The Screaming Clock 1968)Robert Arthur 
12.Záhada hovoriacej lebky(The Mystery Of The Talking Skull 1969)Robert Arthur 
13.Záhada horiacich stôp(The Mystery Of The Flaming Footprints 1971)Mary Virginia Carey   
14.Záhada nervózneho leva(The Mystery Of The Nervous Lion 1971)Nick West 
15.Záhada dopingového bežca(Die drei ??? - Dopingmixer 1994)Brigitte Johanna Henkel-Waidhoferová   
16.Zahada smejuceho sa tieňa(The Mystery Of The Laughing Shadow 1969)William Arden 
17.Záhada cirkusovej tanečnice(Die drei??? - Tatort Zirkus 1993)Brigitte Johanna Henkel-Waidhoferová 
18.Záhada zmenšujúceho sa domu(The Mystery Of The Shrinking House 1972)William Arden 
19.Tajomstvo fantómovho jazera(Secret Of Phantom Lake 1973)William Arden 
20.Záhada bláznivého maliara(Die Drei ??? und der verruckte Maler 1993)Brigitte Johanna Henkel-Waidhoferová 
21.Záhada spievajúceho hada(The Mystery Of The Singing Serpent 1972)Mary Virginia Carey 
22.Záhada mesta duchov(Die Drei ??? – Geisterstadt 1995)Brigitte Johanna Henkel-Waidhoferová                             
23.Záhada horskej príšery(The Mystery Of Monster Mountain 1973)Mary Virginia Carey 
24.Tajomstvo podzemných sarkofágov(Die Drei???- Geheimnis der Särge 1996)Brigitte Johanna Henkel-Waidhoferová 
25.Záhada mátožného zrkadla(The Secret Of The Haunted Mirror 1974)Mary Virginia Carey 
26.Záhada horiaceho meča(Die Drei??? - Das brennende Schwert 1997)André Marx 
27.Záhada tancujúceho diabla(The Mystery Of The Dancing Devil 1976)William Arden 
28.Záhada čierneho havrana(Die Drei??? - Die Spur des Raben 1997)André Marx 
29.Tajomstvo žraločieho útesu(The Secret Of The Shark Reef 1979)William Arden 
30.Záhada neviditeľného psa(The Mystery Of The Invisible Dog 1975)Mary Virginia Carey 
31.Záhada prázdneho hrobu(Die Drei??? - Das leere Grab 1997)André Marx 
32.Záhada planúcich brál(The Mystery Of The Blazing Cliffs 1981)Mary Virginia Carey 
33.Záhada Purpurového piráta(The Mystery Of The Purple Pirate 1982)William Arden 
34.Tajomstvo mesačného ohňa(Die drei??? – Feuermond  2005)André Marx 
35.Záhada podmorského obra(Die drei??? - Menterei aufhoher See 1998)André Marx 
36.Záhada magického kruhu(The Mystery Of The Magic Circle 1978)Mary Virginia Carey 
37.Záhada hlasov z podsvetia(Die drei??? - Stimmen aus dem Nichts 1997)André Minninger                           
38.Záhada unesenej veľryby(The Mystery Of The Kidnapped Whale 1983)Marc Brandel 
39.Záhada diabolských kariet(Die Drei??? - Die Karten des Bôsen 1998)André Minninger 
40.Záhada Vlčej tváre(The Mystery Of The Scar-Faced Beggar 1981)Katharina Fischerová 
41.Záhada tajomného odkazu(The Mystery Of The Dead Man's Riddle 1974)William Arden 
42.Záhada strašiaceho ducha(The Mystery Of The Sinister Scarecrow 1979)Mary Virginia Carey 
43.Záhada cesty hrôzy(The Mystery Of The Trail Of Terror 1984)Mary Virginia Carey 
44.Záhada stonajúcej jaskyne(The Mystery Of The Moaning Cave 1968)William Arden 
45.Záhada horiacej veže(Die drei??? - Feuerturm 1999)Ben Nevis   
46.Záhada bezhlavého koňa(The Mystery Of The Headless Horse 1968)William Arden 
47.Záhada diabolskej hudby(Die drei??? - Musik des Teufels 1998)André Marx 
48.Záhada smrtiaceho talizmanu(Die drei??? - Im Bonn des Voodoo 1998)André Minninger 
49.Záhada vesmírnej lode(Die drei??? - Geheimsache Ufo 1998)André Marx 
50.Záhada omamného jedu(Die drei??? - Dreckiger Deal 1996)Brigitte Johanna Henkel-Waidhoferová 
51.Záhada hotelového prízraku(Die drei??? - Spuk im Hotel 1994)Brigitte Johanna Henkel-Waidhoferová 
52.Záhada močiarneho postrachu(Die drei??? - Schrecken aus dem Moor2005)Marc Sonnleitner 
53.Tajomstvo kaňonu duchov(Die drei??? – Geister-Canyon 2005)Ben Nevis 
54.Tajomstvo pokladu mníchov(Die drei??? – und der Schatz der Mönche 2002)Ben Nevis 
55.Záhada labyrintu bohov(Die drei??? – Labyrinth der Götter 2000)André Marx 
56.Záhada siedmich brán(Die drei??? – Die sieben Tore 2002)André Marx 
57.Záhada tajomného dedičstva(Die drei??? – Das düstere Vermächtnis 2004)Ben Nevis 
58.Záhada troch plachetníc(?)Ben Nevis 
59.Záhada hmlistej hory(Die drei??? – und der Nebelberg 2002)André Marx   
60.Tipy a triky troch pátračov(Die drei ??? verraten Tips und Tricks 1998)André Marx, Kosmos-Krimi-Clubs 
 
“Die Drei ??? Kids” 
 
Príbehy Troch pátračov sú neustále vyhľadávané, takže sa chŕlia nové a nové príbehy. V súčasnosti sa ich počet blíži k stovke titulov. V roku 1999 sa začala písať nova séria knižiek “Die Drei ??? Kids”, ktorej počet sa blíži k štyridsiatke. Sú venované deťom do 10 rokov a vymýšľajú ich predovšetkým Ulf Blanck a Boris Pfeiffer. 
 
Ako je to s filmom ? 
 
Túto záhadu by mala taktiež vyriešiť slávna trojka… Je neuveriteľné, že po atraktívnych námetoch za tie roky nikto z producentov nesiahol v primeranej miere. Doteraz sa natočili dva televízne celovečerné filmy a jeden videofilm, ktorý je však skôr komédiou. 
 
Prvou lastovičkou bol v roku 2006 v koprodukcii Nemecka a Južnej Afriky ( tam sa príbeh odohrával ) “Three Investigators And The Secret Of Skeleton Island”, vysielaný Českou televíziou pod názvom “T3I: Traja pátrači a Tajomstvo ostrova kostlivcov”. O tri roky neskôr v tej istej produkcii natočili “ Three Investigators And The Secret Of Terror Castle”. Tretím do partie je videofilm z roku 2006 “Die Drei ??? und der Super-Papagei”. 
 
Záverom… 
 
Pred 46 rokmi sa začala písať história útlych knižiek a jej záver je v nedohľadne. Predpokladám, že Traja pátrači sú nezničiteľní a ešte dlho budú zabávať násťročné deti svojimi záhadami. Žiadna záhada nie je pre nich nevyluštiteľná. Lebo ako si hovoria : Vypátrame všetko.  


 


 
