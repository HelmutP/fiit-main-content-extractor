

 Po prehratej vojne bolo Španielsko nútené prenechať Gibraltár Spojenému kráľovstvu.  Bolo to v roku 1713.V rokoch 1976 a 2002 sa na Gibraltare konali referendá o pripojení k Španielsku, no väčšina obyvateľstva si želala zotrvať ako britská kolónia. Podľa štatistiky z roku 2001 tvoria miešanci pôvodného španielskeho obyvateľstva, s Arabmi, Britmi, janovskými Talianmi až 83,22 % obyvateľstva. Tí sa nechcú považovať za Španielov a želajú si zachovanie terajšieho stavu. Samotní Briti sú v štatistike zastúpení s necelými 10-timi %. Španielčina je na tomto výbežku počuť naozaj často, no problém s angličtinou sme nezaznamenali. Keď som sa od staršej pani chcel dozvedieť, kde je najbližšie nákupné centrum, či obchod, opýtal som sa, či vie po anglicky. S mierne rozhorčeným výrazom v tvári ma ubezpečila, že angličtinu ovláda dokonale. Raz darmo, boli sme na britskom území. 
  
 Vstupná hraničná časť. To, že sme na britskom území, dokazovala nie len vlajka Veľkej Británie, ale aj typická červená telefónna búdka.  
  
 Pohľad na vápencovú skalu vysokú 426 m nad hladinou mora. Hlavná cestná komunikácia spájajúca Španielsko s Gibraltárom. Zelená na semafóre naznačuje, že Vás lietadlo na tejto ceste nezrazí. Komunikácia sa totiž križuje s letiskom.  
   
 (Zdroj: wikimedia.org) 
   
 Pohľad na letisko zo skaly. My sme boli svedkami pristátia vojenského lietadla. Na oboch stranách cesty, ktorá pretína letiskovú dráhu sú semafóry so závorami. Je to trochu netradičné riešenie, no vzhľadom na obmedzenú rozlohu Briti veľa možností nemali.  
   
 V hornej časti fotografie Španielsko, v strednej letisko a v dolnej časti cintorín na Gibraltáre.  
   
 Ceľkový pohľad na letisko. (Zdroj: pf.blog.pravda.sk)  
  
 Sklady, prístav a početné nákladné lode sú vidieť takmer z každej strany. 
  
  
  
  
 Pohľad na skalu z opačnej strany. Skala je prístupná niekoľkými cestami. Atraktívna nie je len výhľadom na sever Afriky, ale aj múzeom a chodbami vybudovanými ako útočisko pred obliehaním Gibraltáru.  
    
 Cesta lanovkou je jednou z možností. 
   
 My sme zvolili chôdzu, takže bolo takmer isté, že kontakt s jedinou európskou kolóniou opíc (makak-sylvanus) bude naozaj tesný. Gibraltár navštívi ročne okolo 7 miliónov turistov, takže opice sú na ľudí zvyknuté. Ak máte na chrbáte batoh, vyskočia naň a snažia sa dostať k jeho obsahu. Keďže so slovenským batohom asi veľa skúsenosti nemala, obišla naprázdno.  
   
 Ak neponúknete nič pod zub, využijú aspoň otvorené okno a zatrúbia. Ako malé deti. 
   
   
   
 Opice síce drzé boli, no agresivitu na ľudí som nepostrehol. Ak tak, len medzi sebou. 
   
 Vchod do chodieb (The Great Siege Tunnels) vybudovaných od roku 1779 do 1783. Slúžili ako útočisko pri obliehaní Gibraltáru. Dnes sú prístupné verejnosti po zaplatení niekoľkých libier (ak si dobre spomínam tak 8). Mal som pocit, že takmer celá skala je prevŕtaná podobnými chodbami, ktoré boli využívané aj počas ll. svetovej vojny.  
  
 Keď som nazrel cez prvé masívne dvere, skríkol na mňa britský vojak (figurína) s tým, aby som odstúpil od dverí. Pri pohľade na jednotlivých vojakov som mal pocit, že sa aspoň na okamih ocitám v 18. storočí pri obliehaní strategického výbežku. V jednotlivých "miestnostiach" sú zobrazené náročné a tvrdé podmienky, v akých musela armáda fungovať.  
  
  
  
   
  
 V kontraste s tvrdými podmienkami vojakov je vidieť aj dôstojníkov pri stolovaní. Víno, kvalitné jedlo, sluha a k tomu anglická elegancia. 
  
 11. marca 1780 Guvernér prikázal, že okrem chleba má každý vojak obdržať nasledovnú dávku jedla na mesiac. Počas najväčšieho obliehania boli tieto množstvá zredukované na polovicu. 
  
 Plátok bravčového alebo hovädzieho mäsa, plátky rybieho mäsa, hrozno, hrach, maslo, ryža, ovsené vločky, pšeničné zrná... Aj napriek vojenským podmienkam dostávali výživnú a kvalitnú stravu.  
   
 Pri lepšom pohľade je v skale vidieť otvory, v ktorých sa nachádzali (a stále nachádzajú) delá. Takýchto otvorov je možné vidieť desiatky. Je to jednoducho strategické miesto, a to si vyžadovalo dômyselnú ochranu. 
  
   
   
   
   
  
 Výhľad na mesto pod dohľadom kanónov z ll. svetovej vojny. 
   
 Toto sú zvyšky pôvodného mesta Gibraltár po obliehaní. Zostali len ruiny prístupné pre turistov. Jedna z vecí, ktoré by sme si od Angličanov a Škótov mohli prebrať. Aj zvyšky významnejších domov, obytných komplexov či ruiny hradov majú atraktívne spropagované a udržované. Na Slovensku máme častokrát problém osadiť aspoň informačnú tabuľu alebo smerník k významnej kultúrnej pamiatke. 
  
   
   
   
   
 Starček snažiaci sa aj počas vojny predať aspoň časť svojej úrody. 
   
   
 O niekoľko metrov nižšie sa nachádza už len malá časť s pôvodného opevnenia s vežou. Pohľad na mesto je aj z tejto lokality príjemný. 
   
   
 Vchod do veže. V nej sa okrem nových schodov nenachádza nič. 
   
 Schádzame do starého mesta...  
   
   
 Obchodné reťazce známe aj z Veľkej Británie. Okrem vlastnej meny (Gibraltarská libra), je samozrejme možné platiť britskou librou a eurom.  
   
  
   
   
 To, že sme na britskom území je vidieť aj z niektorých balkónov. Britská vlajka a vlajka Gibraltáru. 
 Dojmy z tohto malého územia sú určite dobré. Nie je to síce destinácia na niekoľkodňové vysedávanie pri mori, no rozhodne má na 2 - 3 dni čo ponúknuť. Obzvlášť pre ľudí zaujímajúcich sa o históriu a vojny. Gibraltár je v podstate jedna veľká pevnosť. Vidieť to takmer na každom kroku. Po obvode poloostrova je možné prechádzať okolo zvyškov opevnenia, či okolo ešte stále rozmiestnených kanónov mieriacich na moderné prepravné lode. 
   

