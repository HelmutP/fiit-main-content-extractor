
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabo Németh
                                        &gt;
                Súkromné
                     
                 Filozofia hry slovenských hokejistov na ZOH 2010 

        
            
                                    28.2.2010
            o
            12:34
                        (upravené
                3.3.2010
                o
                9:01)
                        |
            Karma článku:
                2.69
            |
            Prečítané 
            639-krát
                    
         
     
         
             

                 
                    Hokej tá neskutočná hra tvrdých chlapov, ktorá nenechá chladným, ani jedného muža
                 

                 Hra, ktorá je užasná a krásna, rýchla vyjadrujúca statočný boj za čo najlepšími výsledkami. Som neskutočne rád, že naši chlapci - hokejisti patria do kategórie borcov. Ukázali, že bojovať vedia. Niekedy však boj nestačí. Výsledok je rozhodujúci.  A ten  je pre nás veľmi nepríjemným vysvedčením. Semifinálový zápas - Slovensko - Fínsko zamrazil nás všetkých. A netreba ani pripomínať, že Fínsko je profesionálnejšie, herne na tom je oveľa lepšie. Každý team má svoje slabiny. A my sme ich odhalili, keď sme držali zápas v rukách a s Fínmi sme vyhrávali 1:3, ale to čo sa v poslednej tretine udialo to by neuveril nik. Tak vypustiť zápas, keď sme na koni a nedotiahnúť ho do víťazného konca je  príliš trestuhodné. Celá filozofia nášho mužstva akoby zlyhala. Čo sme si horko-ťažko vybudovali, sme stratili za pár okamihov.   Legenda slovenského hokeja Jozef Golonka vyslovil raz pre športové spravodajstvo televízie Markízy myšlienku: - Niekedy mám pocit, že naši chlapci nehraju koncentrovane  celých šesťdesiat minút. Áno to sa potvrdilo.  Poslednú tretinu hoci sme bojovali ako levy, dovolili sme, aby sa zápas obrátil v prospech samotného Fínska. Dal sa očakavať súboj na dračku, plný driny, víťazstva, boja do posledného dychu. Stačila nekoncentrovať, zaváhanie a bolo to tu. Šťastie sa pridalo  na hokejky fínských hokejistov. Rešpekt, to je to, rešpekt pred súperom nám zviazal ruky a  to poznačilo celý priebeh. Aký rešpekt mali naši hokejisti? Ak  naši chlapci  majú rešpekt, tento boj je už iný. Nie je tak húževnatý, tak bojovný, ctižiadostivý pokoriť súpera a dosiahnúť víťazný výsledok. Ďaľší fenomén   zbytočné fauly, ktoré oslabujú naše sily a celý team. Skúsenosť mužstva by mala v takýto zápasoch byť a vravieť jasnou rečou:-  víťazstvo -. Žiaľ to staré známe sa potvrdilo: Nedáš, dostaneš. Pripravili sme sa sami o bronz a to  bolí o to viac. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky a čas
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Plač kvetov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Nočná návšteva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Spomienky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabo Németh 
                                        
                                            Dušičková
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabo Németh
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabo Németh
            
         
        gabonemeth.blog.sme.sk (rss)
         
                                     
     
         profesionálny knihovník, básnik, spisovateľ, od roku 2006 člen Spolku slovenských spisovateľov. Vydal básnické zbierky: Za súmraku, Slnko nad básňou, Rodinný album. Venuje sa duchovnej poézii. Chce byť pokračovateľom katolíckej moderny. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    917
                
                
                    Celková karma
                    
                                                2.76
                    
                
                
                    Priemerná čítanosť
                    335
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            denná čítannosť
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            A300
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            len a len dobré knihy
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                            o politike  vôbec
                                     
                                                                             
                                             
                                     
                                                                             
                                            denník Sme všetky články
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Skôr než zaspím od S.J. Watson
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Queen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            blog spisovateľky Miroslavy Varáčkovej
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetky dobré a kvalitné blogy na Sme.sk
                                     
                                                                             
                                            facebook
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Muž z kríža
                     
                                                         
                       Spomienky a čas
                     
                                                         
                       Jesenné dotyky
                     
                                                         
                       Plač kvetov
                     
                                                         
                       Nočná návšteva
                     
                                                         
                       Spomienky
                     
                                                         
                       Na prechádzke
                     
                                                         
                       Dušičková
                     
                                                         
                       Ranný obraz
                     
                                                         
                       Po stopách vlastného príbehu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




