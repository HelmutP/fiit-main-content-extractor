

 
Pred časom som sa stretol s názorom, že by nebolo zlé občas zaradiť
aj bežnejší tradičnejší recept. Jednak si prídu na svoje aj kuchárske
'embryá'  a zároveň je v tom aj šanca, predsa len aj do tradičného
receptu vniesť niečo netradičné a prezentovať to.
 
 
Jedno z tradičných jedál, ktoré sa v našej medvedej rodinke robí trochu inak, je lečo.
 
 
Potrebujeme:
 
 
1kg
paprík, 600g rajčín, 6 vajíčok, jednu veľkú cibuľu, tri strúčiky
cesnaku, 200g slaniny, 300g klobásy a predstavte si 100g bryndze.
 
 
Lečo ochutíme čiernym korením, rascou, tymianom, mletou paprikou a soľou. 
 
 

 
 
Tak a ideme na to:
 
 
Najprv zo všetkého si zavoláme
kúzelného deduška. Ten urobí: "Simsalabim." A hneď je všetko nakrájané.
Pokiaľ sa kúzelní deduškovia  z nejakých dôvodov minuli vo
všetkých obchodoch, tak ešte ostáva rodina. A len skutočne poslednou
možnosťou je vziať nožík a čistiť a krájať a čistiť. 
 
 
Slaninu
na kocky, klobásu pozdĺžne rozrezať a potom na pol krúžky. Papriku na
slížiky a dávať pozor, aby sa nám do leča nedostala extra pálivá
paprika, ktorá by nám mohla lečo úplne znehodnotiť. Pre priostrenie
chute leča je istejšie pridať pálivej papriky, alebo chilli. Takto si
môžeme ostrosť riadiť a nenechávať to na náhodu. 
 
 
Cibuľu
nakrájame na menšie kocky, rajčiny na väčšie kocky, cesnak očistíme a vajíčka vyšľaháme, alebo aj
nevyšľaháme a vmiešame ich do leča nevyšľahané, čím získame inú
štruktúru, ktorú niekto obľubuje. Ja však poctivo šľahám.  
 
 

 
 
A ideme variť:
 
 
Najprv
poriadne popražíme slaninu tak, aby pustila masť. Do nej potom vysypeme
cibuľu a pretlačíme cesnak. Od vône sa môžeme aj zblázniť. Ja keby som
si hneď nedal do úst kúsok surovej slaniny a nezajedol chlebíkom, tak
už som v klietke v blázinci.:)
 
 
Keď cibuľka spriesvitnie, vysypeme do zmesi cibule a slaniny papriku
a prichádza čas na koreniny. Trochu čierneho korenia a rasce ale najmä
tymian. To je tá tajná a zázračná ingrediencia, ktorá chuť leča
poriadne zmení a podľa mňa rozhodne k lepšiemu. Pre farbu pridáme ešte
mletú červenú papriku a ak chceme, tak aj pálivú. Nezabudneme posoliť,
ale so soľou nepreháňame. 
 
 

 
 
Zakryté
podusíme 15 minút a nasypeme rajčiny. Znovu podusíme 15 minút a pridáme
klobásku. Do leča sú výborné klobásy typu paprikovej, inoveckej,
bravčovej či grilovacej a podobne. Viac údené domáce klobásy dávam len
vtedy, keď
nemám inú možnosť. Chcem však, aby klobása mala nejakú vnútornú mäsovú
štruktúru, ale nebola príliš údená. Dnes som mal bravčovú grilovaciu,
ktoré bola ideálna. Točená či špekačky sú podľa mňa nevhodné. 
 
 

 
 
Lečo pustilo veľa šťavy. Preto ho ďalej varíme odokryté. Dáme si
záležať, aby sa prevažná väčšina šťavy vydusila. Výsledok by mal byť
šťavnatý ale nie tekutý. Pokojne to môže trvať ďalšiu 3/4 hodinku i
viac. 
 
 

 
 
Na
úplný záver dusenia prichádza ďalšia supertajná ingrediencia. Do
lahodne voňajúceho dusiaceho sa pokrmu vmiešame 100 g bryndze. Krásne
sa nám rozíde, nemusíme sa báť hrčiek. Bryndza dá leču famóznu príchuť
a vôňu.
 
 
No a na záver prichádzajú vajíčka. Rozšľahané či nerozšľahané
poctivo do leča vmiešame a lečo ešte krátko povaríme. Podľa potreby
dosolíme. 
 
 

 
 
Na obrázku vidíme, že lečo vyzerá výborne. Skvelo chutilo aj voňalo.
Nabudúce ešte trochu dlhšie podusím, aby šťava neostávala ani po
okrajoch, ale to už je samozrejme na vkuse každého z nás. 
 
 

 
 
Podávame s čerstvým bielym chlebíkom, alebo varenými zemiakmi. Po splave sa vám ozveme.
 
 
Dobrú chuť zo splavu, želajú medvede.:)
 
 
Poznámka:  
 
 
Čo tak dať do základu prerastenejšiu slaninu, ďalej postupovať obdobne, ako som postupoval ja, len pridať viac mletej červenej papriky, dobre podusiť a vynechať vajcia.  A správne maďarské lečo je na svete. 
 
 
 
 
 
tenjeho 
 
 
 
 

