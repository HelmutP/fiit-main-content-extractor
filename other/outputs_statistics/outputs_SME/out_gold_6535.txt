

    
 Spoznal som pár milých mokasín. Jedného dňa však ľavá mokasína pochovala svoju družku, svoje zrkadlo, keď ich majiteľovi odtrhlo nohu pri pracovnom úraze. Od tejto udalosti svet prestal byť farebný. Ľavá mokasína strávila zvyšok života hľadaním páru. Ale dajte dohromady mokasínu s teniskou alebo s čižmou. Na prvý pohľad vtipné, milé, ale po čase nefunkčné.  
 O poschodie nižšie býval pár starých športových tenisiek Jedna z páru si vzala do hlavy, že samej jej bude lepšie. Že už viacej nechce robiť to čo druhá, živiť sa len športovými ponožkami, ktoré nanajvýš smrdia viacej ako ostatné, nechce už viac kopať do lopty. Nevydržalo to dlho. Osamotená teniska trávila bezsenné noci v rozhovoroch s ľavou mokasínou. Dokonca sa spriatelili. Ale napriek tomu sa cítila sama, bola zvyknutá a mala rada keď počula svoju ozvenu a keď sa kedykoľvek obrátila, bol tam jej pár. Druhá, tá ktorá sa rozhodla byť single si spočiatku samotu užívala. Ale nič netrvá večne a prírodné zákony neoklamete. Tak rýchlo ako sa pár rozdelil, tak rýchlo sa párom opäť stal.  
 Dva príbehy dvoch párov. Jeden so šťastným a druhý s nejasným koncom niekde na ceste k hľadaniu samého seba. Ak máte svoj pár, držte si ho. Život je ako pexeso. Hľadáte pár. Ale aj v tejto hre je ich počet obmedzený.  
   

