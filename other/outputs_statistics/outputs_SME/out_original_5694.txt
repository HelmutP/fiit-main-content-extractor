
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniela Mužíková
                                        &gt;
                Nezaradené
                     
                 Makadamové jadro 

        
            
                                    2.5.2010
            o
            21:07
                        (upravené
                16.5.2010
                o
                9:05)
                        |
            Karma článku:
                4.17
            |
            Prečítané 
            791-krát
                    
         
     
         
             

                 
                    U nás je po novom moderné, mať svoj vlastný „biznis“. Je v podstate úplne ukradnuté (a to tak kolegom ako konečným „spotrebiteľom“) o aký biznis vám vlastne ide a aké má dôsledky. Dôležité je, že nejaký ten kšeft, obchod, či obchodík vôbec máte. Ak si totiž dnes žiadne „podnikanie“ nerozbehnete, može sa vám stať, že vám niekto preberie know how a vy ste, ľudovo povedané, v háji zelenom a to doslova, pretože si viac v branži neškrtnete bez toho, aby ste boli označený za plagiátora, prípadne – v lepšom prípade, za zlodeja, či inak vychytralého papaláša.
                 

                      Po takomto fatálnom autopitevnom výmaze z obchodného registra „zákazníkovho“ povedomia sa možete ísť teda rovno pásť do toho svojho hája a popri meditáciách a makrobiotickej diéte (na inú Vám neostanú prostriedky, ani voľa) chytro začať premýšľať, ako rozbehnúť nový biznis.    Tieto fakty sa nedajú čítať len na titulkách časopisov, či iných zvláštnych periodík, no vidí a ovláda ich i každé priemerne inteligentné dieťa. Stačí, ak so svojim potomkom vojdete ráno do triedy a pár minút zotrváte pri dverách, aby ste mohli pozorovať, čo sa bude diať. Je dosť možné, že uvidíte, ako syn, či dcéra pred začatím prvej hodiny skontroluje svoju desiatu a vymení si ju s niekým, koho mamka nedáva medzi chlieb uhorku. Prípadne uvidíte, že svoju desiatu dáva o desať kíl presvedčivejšiemu spolužiakovi, pre ďalšiu plynulú obchodnú spoluprácu medzi nimi. Ak sa však váš potomok už naučil v biznise dobre chodiť, a teda ho neuvidíte robiť ani jedno, môžete pokojne odísť do práce, je totiž pravdepodobné, že v priebehu prestávky predá svoju domácu úlohu za šunkový sendvič menej zdatného spolužiaka.    Takýto systém sa mi páči, buduje totiž silné osobnosti a zdravú konkurenciu, ktorá nás núti zlepšovať sa v každej sfére života. Problémové je, že tí, ktorí by mali ísť príkladom používajú najviac bočných cestičiek a kľučiek, aby aj za cenu ľudských hláv vymlátili čo najvyšší zisk. Áno, sú to samozrejeme naši „milovaní“, predbiehajúci sa v poslednej dobe o víťaza titulu „najsociálnejší milovaní Slovenska“. Rovnako ako teraz. Káždý z nich si varí svoj v biznis v predvolebnej kašičke a my, ako podstiví zákazníci hltáme horúcu kašu aj so všetkými prídavnými zmesami.       Najsmutenejšie na tejto patetickej snahe našich milovaných je, že si temer všetci sami zarývame do podnebia háčik, a keď drží dosť pevne, zaťaháme za silon, aby nás za to natrhnuté ďasno vytiahli na súš. Všetci sa chceme mať skvele a aj napriek tomu nechávame rozhodovať ľudí, vďaka ktorým platia zákony, zväčšujúce len objemy peňazí, odplavujúcich sa z peňaženiek do dier v site štátneho rozpočtu. To mi evokuje názor, že my Slováci predsa nechceme odísť z pieskoviska a tak sa aj s veľkou časťou EU hráme „Hru na podolympský dlh“. Zvyšujúcim sa dlhom si spokojne zaklincuvávame rakvičku a sypeme na hruď piesok, aby sme si zvykali na grécke pomery (nie však podnebné, ale ekonomické).    Hádam najdesivejším prvkom tohoto trendu je množstvo ľudí, dôverujúcich týmto obchodníckym nezmyslom. „Babky demokratky“, hulváti imitujúci národovcov,  vrstva lamentujúca nad padlým socializmom aj rádoby pravičiari obracajúci sa na menej krvavý východ sú proste tu – na nich som si už akosi zvykla. Šokuje ma však iná vec. Niektorým mladým už stihli naše pomery, resp. niektoré inštitúcie (bohužiaľ vrátane výchovnovzdelávacích) urobiť lobotómiu a ja si pripadám ako v komiksovom vydaní „Preletu nad kukučím hniezdom“, dokonca nám nechýbajú ani myšlienkové bubliny (máme predsa bilboardy).    Denne sa rozprávam s ľuďmi a začínam trpieť paranojou z vlastného tieňa. Mnoho mladých talentov, intelektuálov a všemožných skupín obyvateľstva žije blízko, či ďalej, no hlasy tých rozmuných sú z rôznych príčin ticho, respektíve šeptajú, no na pódiách ich hlasy často nepočuť. Namiesto kriku sa ozýva len šum hlasovacích lístkov, ktoré po sčítaní vykazujú čísla podobné tomu, akoby ich zaškrtávali šimpanzy, nie títo inteligenti... Kde to žijeme?    O celej situácii hovorí dokonale môj zážitok. Minule večer som cestou večerným autobusom mala možnosť vidieť skvostný obraz mesta. Luna v splne nad nevkusným červeným kovovým nápisom „Autobusová stanica“, osvetleným svetlom režimom zabudnutých lámp. A presne taký je aj náš štát. Plný talentov a šikovných mladých ľudí, ktorí sú utopení v predvolebnej propagande na štýl pompézneho prvomájového pochodu, usporiadaného 20 rokov po páde komunizmu. A vtedy som si uvedomila, v čom majú tie mladé zblúdilé duše problém. Tie lampy osvetľujúce plastiku na stanici sú ako lampa svietiaca na makadamové jadro krajiny. Problémom je, že makadamovému podkladu sa dostalo zatiaľ iba štrkovej prikrývky.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Mužíková 
                                        
                                            "Problém? Kde?" Opýtal sa chlapík a dlaňami si chránil tvár
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Mužíková 
                                        
                                            1984 a dilema prepychu menom sloboda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Mužíková 
                                        
                                            Otáznik
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Mužíková 
                                        
                                            Komentár: Predvolebné slepačenie po slovansky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniela Mužíková 
                                        
                                            "Moje city dajte pokojne do tamtej krabice!" povedal smutný klaun
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniela Mužíková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniela Mužíková
            
         
        danielamuzikova.blog.sme.sk (rss)
         
                                     
     
        Som človek na dlhšie vysvetľovanie, ale za všetko hovorí úryvok básne: "Zhliadol som zrkadlo sveta, zľakol sa a rýchlo daroval komusi voňavé kvety. Dal som ich do rúk úprimnému človeku, ..."
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    9
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    823
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




