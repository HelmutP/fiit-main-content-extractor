
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Buno
                                        &gt;
                Nezaradené
                     
                 Prečo chcú muži sex a ženy lásku?! 

        
            
                                    17.3.2010
            o
            10:38
                        |
            Karma článku:
                9.69
            |
            Prečítané 
            3251-krát
                    
         
     
         
             

                 
                    Mnohé matky nevedomky vedú svojich synáčikov k tomu, aby boli mizernými partnermi a manželmi. Chlapec si zvykne na to, že matka ho bude milovať bez ohľadu na to, čo urobí alebo neurobí. Zvykne si, že po sebe nemusí upratovať a nemusí sa mami spytovať, aký mala deň. Nemusí ju pozývať na večere, dokonca sa s ňou nemusí ani slušne rozprávať. Matka naučí synáčika, že láska k žene môže byť jednosmerná ulica a že nemusí urobiť vôbec nič, a ona aj tak bude šťastná...
                 

                 Žiaľ, v mnohých prípadoch je to naozaj tak. A hovorím to ako muž, chlap. Keď sa potom v partnerskom/manželskom vzťahu zníži hladina hormónov (a tá sa zníži vždy), vyprchá aj romantika, vášeň a sexu bude menej. Synáčik nemusel mame neustále dokazovať, že ju ľúbi, tak prečo to zrazu vyžaduje jeho partnerka? Prečo ho neustále núti prejavovať svoju lásku rôznymi spôsobmi, najlepšie romantickými, originálnymi, netradičnými? Prečo tak strašne potrebuje počuť, že ju ľúbim...keď ja chcem hlavne sex? pýta sa nejeden chlap.  Aj o tomto – najmä o tom – píšu Allan a Barbara Peasovci vo svojej novinke s krásne výstižným názvom „Prečo chcú muži sex a ženy potrebujú lásku“. Peasovci nie sú v tejto oblasti nováčikmi – sú to najúspešnejší súčasní autori zaoberajúci sa vzťahmi, doteraz napísali 15 bestsellerov, ich tvorba sa stala témou deviatich televíznych seriálov a úspešného filmu, ktorý videlo viac ako sto miliónov divákov. Majú za sebou neuveriteľný úspech.  Za ním stojí tvrdá drina, štúdium, analýzy, testy a diskusie s tisíckami ľudí.   Kniha je rozdelená veľmi prehľadne na množstvo pútavých kapitol. Začínajú sexom v mysli, rozoberajú, ako Hollywood zmenil naše vnímanie a prečo to majú muži v 21.storočí ťažšie. Nasleduje veľmi zaujímavá kapitola, ktorá poteší najmä mužov – Čo vlastne ženy chcú. A ozaj, vedeli ste, že s bohatými mužmi ženy dosahujú viac orgazmov? Že ako je to možné a v čom je to tajomstvo?   Inšpirujúce sú napríklad kapitoly o tom, ktoré veci chcú ženy najviac od mužov a čo im na chlapoch imponuje. Ktorých 15 faktov o mužoch ženy nechápu. A ako znie 12 právd o ženách, ktorým väčšina mužov nerozumie. Kniha je skvelou a zábavnou oddychovkou, ale zároveň je plná zaujímavých faktov, rád a odporúčaní, ktoré inšpirujú a napovedia, ako urobiť vzťah ešte lepším.  Prečítajte si ukážky z kapitoly „Otvorene o sexe a láske“.  P.S. Daily Record napísal duchaplne, že vo svete, kde sa až polovica manželstiev končí rozvodom, je suma 9,21€ za skvelé rady a iný pohľad určite nižšia ako volanie právnikom... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (115)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Ako šli Sovieti kradnúť nápady do Ameriky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Marián Gáborík. Inšpirujúci príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Buno
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Buno
            
         
        buno.blog.sme.sk (rss)
         
                        VIP
                             
     
         Robím to, čo milujem...po dlhých rokoch v médiách som sa vrhol na knihy a vo vydavateľstve Ikar mám všetky možnosti a príležitosti, ako si ich vychutnať naplno. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    297
                
                
                    Celková karma
                    
                                                7.34
                    
                
                
                    Priemerná čítanosť
                    2448
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nesbov SYN sa blíži. Pozrite si obálku a čítajte 1.kapitolu
                                     
                                                                             
                                            Zažite polárnu noc (nielen) s Jo Nesbom!
                                     
                                                                             
                                            Jony Ive - Geniálny dizajnér z Apple
                                     
                                                                             
                                            Konečne prichádza Láska na vlásku so Celeste Buckingham!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Tretí hlas
                                     
                                                                             
                                            Mandľovník
                                     
                                                                             
                                            Dôvod dýchať
                                     
                                                                             
                                            Skôr než zaspím
                                     
                                                                             
                                            Tanec s nepriateľom
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            M.Oldfield: The Songs of Distant Earth
                                     
                                                                             
                                            Ludovico Einaudi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Severské krimi
                                     
                                                                             
                                            Knižný svet na Facebooku
                                     
                                                                             
                                            Titulky.com
                                     
                                                                             
                                            Jozef Banáš
                                     
                                                                             
                                            Táňa Keleová-Vasilková
                                     
                                                                             
                                            BUXcafé.sk všeličo o knihách
                                     
                                                                             
                                            Noxi.sk
                                     
                                                                             
                                            Tyinternety.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Šup, šup, najesť sa. A utekať!
                     
                                                         
                       Ako šli Sovieti kradnúť nápady do Ameriky
                     
                                                         
                       Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                     
                                                         
                       Marián Gáborík. Inšpirujúci príbeh
                     
                                                         
                       Knižný konšpiračný koktail alebo čože to matky na severe miešajú deťom do mlieka?
                     
                                                         
                       Paulo Coelho
                     
                                                         
                       Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Tánina pekáreň
                     
                                                         
                       Malala - dievča, ktoré rozhnevalo Taliban
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




