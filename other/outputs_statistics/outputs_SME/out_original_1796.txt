
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lýdia Koňárová
                                        &gt;
                Nórsko
                     
                 De Syv Sostre, Sedem sestier 

        
            
                                    7.6.2010
            o
            9:47
                        (upravené
                19.2.2011
                o
                13:44)
                        |
            Karma článku:
                4.38
            |
            Prečítané 
            1035-krát
                    
         
     
         
             

                 
                    Do Geirangerfjordu sa samovražedne strmhlav vrhá zopár vodopádov. Najväčšiu slávu žne salto mortale De Syv Søstre, Siedmych sestier. Skageflå, najbohatšiu farmu s tromi horskými pastvinami a najväčším stádom kôz, opustili v roku 1916. Tak ako Knivsflå, visí nad fjordom vo výške 250 metrov.
                 

                           Geirangerfjord sa lahodne vlní okolo kolmo sivomodrých brál ako indická tanečnica v dĺžke pätnásť kilometrov.           Pozdĺž fjordu sa na skalách tiahnu prilepené lastovičie hniezda, opustené farmy Megardsplassen, Horvadrag a Matvik. Právo dediť pozemky mal vždy len starší syn a tí mladší sa museli postarať sami o seba. Hľadali pozemky kade tade.                    Život na takej farme bol mimoriadne írečitý. Nechápem, ako sa na tých kamenných strminách dokázal pásť dobytok, kozy a ovce. Asi to bol nejaký kamzíči klon.            Keď mamy kaskadérky odchádzali ráno na strmé pole, vyklčované v hustom lese, priväzovali svoje malé deti doma povrazom k stolu.                    Dolu z usadlosti sa liezlo k fjordu po povrazových rebríkoch. Vraví sa, že keď nechceli platiť dane kráľovským financom, rebrík vytiahli.            Skageflå, Knivsflå a Blomberg mali šťastie a zrekonštruovala ich spoločnosť Storfjordens.            Propagáciu si vzala na starosť sama kráľovná Sonja Haraldsen. Svoje sedemdesiatiny v roku 2007 oslávila na jednej z nich. Bola zhovievá. Kamošky kráľovné a princezné nemuseli visieť a šplhať po lane, objednala im vrtuľník.                  Do fjordu sa samovražedne strmhlav vrhá zopár vodopádov. Najväčšiu slávu žne salto mortale De Syv Søstre, Siedmych sestier.            Z brál oproti s nimi flirtuje Wooer, Nápadník.            Láka ich na Brudesløret, Nevestin závoj, rafinovane utkaný z vodných kvapiek a slnečných diamantov.                   Pesimistom pripomína fľašu a poeticky mu nadávajú Alkoholik. Vraj chlastá, lebo roky ho žiadna sestra nechce.            Realisti zhodnotili jeho šance prezývkou Friar, Mních.            Domáci ho prezývajú Friarfossen, Vodopád Mních, Geitfossen, Vodopád Koza alebo Skageflåfossen, Vodopád pri Skageflå.                  Nápadník meria štyristoštyridsať metrov, staršia sestra tristo a najmrňavejšia dvestopäťdesiat.            Usadlosť Knivsflå, visiacu 250 metrov nad fjordom, opustili v roku 1898 pre padajúce skaliská. Pešo od lodnej zastávky na fjorde sa k nej dostanete za polhodinu až hodinu.        Dobytok z nej vyháňali na horskú pastvinu v päťstometrovej výške.                    Blomberg v 450 metrovej výške je najkrajší a najzachovalejší zo všetkých. Má výhľad na všetky tri vodopády. Pešia túra od lodnej zastávky trvá hodinu.            Skageflå, najbohatšiu farmu s tromi horskými pastvinami a najväčším stádom kôz, opustili v roku 1916.        Visí vo výške 250 metrov. Pešo z Geirangeru k nej kráčate dve až štyri hodiny, od lodnej zastávky šplháte takmer kolmo ako Spiderman polhodinu.                   Magdalene Thoresen, svokra Henrika Ibsena o Skageflå povedala:       “Tento fjord je obkľúčený horami, najstrmšími z celého západného pobrežia.     Je veľmi úzky, na kolmých zrázoch neposkytuje priestor na pobrežné osídlenie, rozoklaný povrch vystupuje priamo z vody.        Peniace vodopády skáču do fjordu zo zubatých štítov.                   A predsa je tam zopár usadlostí, jedna alebo dve s riskantným prístupom po chodníkoch, kľukatiacich sa nad strmými priepasťami a mostoch, vtlčených do skál železnými klinmi a obručami.        Sú najpresvedčivejšími svedectvami o pozoruhodnej sile a vynaliezavosti, s akou výzvy prírody prekonával človek.“     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Tajná láska poetky Wallady, poslednej omejadskej princeznej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Zo Smrtnej hory fúka na Trygve Gulbranssena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Pizza Clandestina s Vitom Corleone
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Ako Pavol Jantausch vzdelaním národné sebavedomie podporoval
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Diego Velázquez - Las Hilanderas
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lýdia Koňárová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lýdia Koňárová
            
         
        konarova.blog.sme.sk (rss)
         
                                     
     
        ochranca prírody a pamiatok
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    54
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3598
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Chansons
                        
                     
                                     
                        
                            Bretónsko
                        
                     
                                     
                        
                            Capri
                        
                     
                                     
                        
                            Korzika
                        
                     
                                     
                        
                            Normandia
                        
                     
                                     
                        
                            Nórsko
                        
                     
                                     
                        
                            Paris
                        
                     
                                     
                        
                            Prado
                        
                     
                                     
                        
                            Pro veritate cum caritate
                        
                     
                                     
                        
                            Sardínia
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Škótsko
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Thassos
                        
                     
                                     
                        
                            Wien
                        
                     
                                     
                        
                            Zámky na Loire
                        
                     
                                     
                        
                            P&amp;#234;le-m&amp;#234;le
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Granada, Boabdil a kráľovná Aisha
                                     
                                                                             
                                            Meč z Toleda, tajný sen každého kráľa
                                     
                                                                             
                                            Diego Velázquez - Los Borrachos
                                     
                                                                             
                                            Vodný výťah pre lode vo Falkirku
                                     
                                                                             
                                            Siracusa, jedno z najslávnejších miest starovekého Stredomoria
                                     
                                                                             
                                            Córdobsky emirát a kalifova mešita
                                     
                                                                             
                                            Zámok Cheverny a Mona Lisa v oranžérii
                                     
                                                                             
                                            Zámok Chambord a dvojité schodisko Leonarda da Vinciho
                                     
                                                                             
                                            Zámok Ussé a Kráska spiaca v lese
                                     
                                                                             
                                            Zámok Blois a prekliaty básnik François Villon I.
                                     
                                                                             
                                            Zámok Blois, kabinet s jedmi a kredenc II.
                                     
                                                                             
                                            Aliki a pristávacia dráha pre mimozemšťanov
                                     
                                                                             
                                            Florencia, Catherine de Medicis a zámok Louvre
                                     
                                                                             
                                            Bayeux, Odon a tapiséria kráľovnej Matildy
                                     
                                                                             
                                            Perníkové chalúpky v Kerascoete
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




