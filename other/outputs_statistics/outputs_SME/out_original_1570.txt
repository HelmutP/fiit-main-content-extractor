
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Dzurindov mačkopes 

        
            
                                    31.7.2009
            o
            11:05
                        |
            Karma článku:
                14.97
            |
            Prečítané 
            15008-krát
                    
         
     
         
             

                 
                    V Nitre so SMERom, v Bystrici proti SMERu, v Trenčíne s HZDS, v Žiline proti HZDS, v Trnave s KDH, v Košiciach proti KDH, v Bratislave s SMK, v Nitre proti SMK... Aj takto vypočítavo sa správa strana, ktorej predseda Mikuláš Dzurinda tvrdí, že je založená na hodnotách. Momentálne je však vidieť len jedného veľkého mačkopsa.
                 

                 Deň po voľbách, kedy sa SaS celkom slušne nedostala do Europarlamentu, nás bývalý premiér Mikuláš Dzurinda obvinil, že oslabujeme pravicu. Napriek tomu, že to nebola pravda (aj keby všetky hlasy SaS získala SDKÚ, mala by pravica naďalej šesť poslancov v EP), nemáme záujem pravicu rozbíjať. Práve naopak, všetci, ktorí len trochu ctia slušnosť a zdravý rozum, sa musia spojiť, lebo dnešná takzvaná ľavica, je pre Slovensko hotová pohroma.   Preto sme prejavili záujem ísť v najbližších voľbách - to je voľbách do VÚC - spolu s SDKÚ a prípadnými inými stranami v koalícii. Konkrétne v Bratislave sme SDKÚ navrhli, že sa pridáme k existujúcej koalícii SDKÚ, KDH, SMK a OKS. Bratislavská VÚC bude mať 44 poslancov, tieto strany teda postavia 44 kandidátov a rozdelia si ich takto:      SDKÚ (18 kandidátov)    KDH (15 kandidátov)    SMK (6 kandidátov)    OKS (5 kandidátov)         SaS bola v bratislavskom kraji v posledných voľbách (v júnových Eurovoľbách) s výsledkom 10,24% treťou najsilnejšou stranou, KDH získala 8,51% a SMK spolu s OKS/KDS získali 8,19% hlasov, čiže tiež menej ako my.   Samozrejme že si uvedomujeme, že máme za sebou len jedny voľby a preto sme mali snahu prezentovať triezve postoje, čo v tomto prípade znamenalo, že sme požadovali šesť miest na kandidátke a nové rozdelenie mohlo byť:      SDKÚ (16 kandidátov)    KDH (13 kandidátov)    SaS (6 kandidátov)    SMK (5 kandidátov)    OKS (4 kandidátov)     Mali by sme teda menej než polovicu mandátov ako KDH a to napriek lepšiemu volebnému výsledku v posledných voľbách a mali by sme o tretinu menej mandátov ako SMK a OKS/KDS, tiež pri lepšom výsledku ako tieto tri strany spolu.   Sme presvedčení, že požadovať 6 zo 44 mandátov je pre stranu s tretím najlepším výsledkom primerané, no bohužiaľ to takto nevnímajú naši takmer koaliční partneri a návrh odmietli. Nie sme z toho zúfalí, pre nás by to bol dosť veľký kompromis a tento článok píšem len z jedného jediného dôvodu:   Keďže sa pri voľbách do VÚC jedná o väčšinový volebný systém, môže sa stať, že ostatné pravicové strany stratia mandáty a už teraz dôrazne odmietame akékoľvek obvinenia z nedostatočnej snahy o spoluprácu.   Čakal som veru viac politickej zrelosti. Tiež som čakal viac zásadovosti. Mikuláš Dzurinda totiž opätovne vyhlásil, že SDKÚ so SMERom vládnuť nebude, lebo by to bol mačkopes. To, čo platí možno pre Slovensko, neplatí pre Nitriansky kraj, lebo ten zjavne nie je na Slovensku, ale asi na Marse. V Nitrianskom kraji ide SDKÚ do koalície práve so SMERom a tam už nikomu nevadí, že "na Slovensku panuje veľká politická nekultúra namiesto slušnosti, je tu veľa arogancie, násilia, namiesto dialógu, vládne populizmus namiesto zodpovednosti, nacionalizmus namiesto vlastenectva, korupcia a netransparentnosť, lobizmus oproti transparentnému správaniu sa" (citát M. Dzurinda).   Ako dôvod koalície so SMERom uvádza SDKÚ nutnosť slovenskej koalície proti SMK, nemá ale najmenší problém s tou istou SMK ísť do koalície v susednej župe. Naopak, kúsok na sever, v Trenčíne, je všetko inak. Tam bude SDKÚ koalovať s HZDS a tak ďalej. Nuž, zásadovosť vyzerá inak a preto som ja osobne celkom rád, že SaS možno ako jediná strana bude na poslancov VÚC vo všetkých krajoch dôsledne kandidovať sama.   To sa však netýka županov. Keďže za župana bude zvolený len kandidát s najväčším počtom hlasov,  nemá zmysel, aby sme stavali vlastných kandidátov na županov a preto podporíme toho, kto nám je blízky. V Bratislave je to Pavol Frešo, v Bystrici Jozef Mikuš a v Košiciach Ján Süli, všetci SDKÚ. V ostatných krajoch uvidíme.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (171)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




