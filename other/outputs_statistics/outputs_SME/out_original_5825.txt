
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Kužma
                                        &gt;
                Nezaradené
                     
                 Dali by ste Grékom aj zo svojho? 

        
            
                                    4.5.2010
            o
            12:31
                        (upravené
                4.5.2010
                o
                20:16)
                        |
            Karma článku:
                6.85
            |
            Prečítané 
            840-krát
                    
         
     
         
             

                 
                    Musíme myslieť na naše deti, nemôžeme len tak rozdávať peniaze krajinám, ktoré sú ochotné urobiť len veľmi málo preto, aby ich boli schopné vrátiť. Musíme myslieť aj na ich deti, lebo nie je nič horšie, než niekoho podporovať v tom, že míňa viac ako je schopný vyrobiť. Takýto návyk vedie do záhuby.
                 

                     Predstavte si, že máte príbuzného, ktorý napriek tomu, že pochádza zo skromnejších pomerov akoby úplne zabudol, aký život viedol pred pár rokmi. Jeho životný štandard je vysoko nad skutočné možnosti. Typickým príkladom sú luxusné dovolenky každý rok. V zime lyžovačka v Alpách a v lete Karibik... Už i Vaše deti vo svojej nevinnej úprimnosti niekoľkokrát spomenuli, ako si len dobre tí príbuzní žijú. Prečo sa my nemáme tak dobre ako oni. Majú väčšie auto a deti nosia do školy značkové oblečenie. Možno Vám niekedy aj príde ľúto, že nedokážete svojim deťom vysvetliť, čo to znamená žiť v rámci svojich možností. Svoje výdavky si pozorne strážite a snažíte sa  ich čo najlepšie rozdeliť medzi najdôležitejšie potreby rodiny.   O to viac Vás prekvapí, keď sa Vám príbuzný posťažuje, že si na to všetko požičiaval. Teraz je v situácii, že pôžičky nevie splácať a ďalšie peniaze si už nemá od koho požičať. Dokonca ani v banke za vysoký úrok. Preto Vás osloví, či by ste nemohli Vy zobrať od banky pôžičku a potom mu chýbajúce peniaze požičať. Argument má jasný. Veď pomáhať sa má. A tá hanba, len si predstavte, žeby sa všetci dozvedeli, že u Vás v rodine niekto skrachoval a vy ste mu nepomohli.        Na rovinu podarenému príbuznému vysvetlíte, že má začať žiť skromnejšie. Míňať menej, než zarobí, aby mohol svoje dlhy posplácať. To sa ale nedá, povie príbuzný, manželka i deti si zvykli na takýto život, majú svoje nároky, z ktorých už dnes nie sú ochotní poľaviť. Toto Vás už naozaj nasrdí.    Čo robiť? Dobre viete, že požičané peniaze by ste už s najväčšou pravdepodobnosťou nikdy nevideli. Niekedy je ťažké povedať nie, ale urobiť to v takomto prípade je jediné správne riešenie. Ak nevie splácať svoje záväzky a nechce zmeniť život, tak nech mu exekútor siahne na jeho majetok. Bude to riešenie lepšie nielen pre mňa, ale do budúcna aj pre moje a jeho deti.    Takto by sa zachovala určite väčšina z nás, ak ide o vlastné peniaze. Ale i peniaze Slovenskej republiky sú naše vlastné peniaze, preto s nimi musíme narábať rovnako. Musíme myslieť na naše deti, nemôžeme len tak rozdávať peniaze krajinám, ktoré sú ochotné urobiť len veľmi málo preto, aby ich boli schopné vrátiť. Musíme myslieť aj na ich deti, lebo nie je nič horšie, než niekoho podporovať v tom, že míňa viac ako je schopný vyrobiť. Takýto návyk vedie do záhuby. Je rovnako ťažké povedať i v tomto prípade nie, ale je to jediné správne riešenie.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Kužma 
                                        
                                            Fakt by rodina s deťmi nepocítila 37% zdanenie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Kužma 
                                        
                                            Okradli aj Vás a páchateľovi sa nič nestalo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Kužma 
                                        
                                            Pán minister Lipšic, prestaňte klamať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Kužma 
                                        
                                            Prečo je pre nezamestnaného z osady mzda 600 eur málo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Kužma 
                                        
                                            Diskriminuje škola v Šarišských Michaľanoch Rómov?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Kužma
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Kužma
            
         
        stefankuzma.blog.sme.sk (rss)
         
                                     
     
        Volám sa Štefan Kužma. Dlhoročne sa venujem ekonomickým, sociálnym témam.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6015
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




