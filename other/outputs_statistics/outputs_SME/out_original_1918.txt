
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Iveta Rajtáková
                                        &gt;
                ja tomu nerozumiem
                     
                 Čo nás omína 

        
            
                                    2.12.2009
            o
            8:30
                        |
            Karma článku:
                7.43
            |
            Prečítané 
            1858-krát
                    
         
     
         
             

                 
                                         Priznávam, máme doma televízor. Priznávam, z času na čas televíziu pozerám. Priznávam, Smotánka je po Vraždách v Midsomeri mojím najobľúbenejším televíznym programom a viem aj to, kto vypadol v poslednom kole Superstar. Priznávam, nerozumiem ľudom, pre ktorých je zábavný televízny program dôvodom na moralizovanie akéhokoľvek druhu. 
                 

                                                        Povedzte, čo môže byť zábavnejšie, ako keď sa dozviem z televízie a so mnou nemalý počet obyvateľov Slovenska, že bývalý frajer frajerky, o ktorej ani len netuším, kto to je, má už novú frajerku, ktorá však bola na párty s kámoškou, ktorá možno chcela byť frajerkou frajera inej frajerky . Aby zábavy nebolo málo, priebežne o vývoji vo frajerovaní informujem Juraja, ktorý ma zatiaľ nikdy nesklamal a zakaždým sa ma opýta: „A to je to ? "                                                Frajerov frajeriek vystrieda manželka známeho slovenského básnika ( a vraj tiež známa spisovateľka), ktorá s vážnou tvárou vysvetľuje, že na párty kámoška neprišla pretože, jej prišla nejaká návšteva z kadesi z Tramtárie a pretože je to z Tramtárie a nie spoza roha, tak jej to odpustí. Tak to nám odľahlo, pretože, keby jej to neodpustila, v priamom prenose by sme zažili srdcervúce roztrhnute smotánkovského priateľstva  a toto je predsa len zábavná relácia a žiadna Pošta pre teba. ( Či tam sa vlastne putá v priamom prenose zväzujú uzlíkom 15 minút televíznej slávy, to musí vydržať !).                                         Chceme či nechceme, dryják, je tu. Sympatická politička zapadne medzi frajerov a frajerky a frajerovanie a nefrajerovanie, pretože celému národu zvestuje, že ju Janko opustil. Dramatický vrchol však príde až vtedy, keď si vypočujeme ako sa lieči smútok nad tragickou, nie, nebudem pokračovať...........                                         Tak, a možeme sa vrátiť k frajerom a frajerkám. Ešte niekoľko dôležitých informácii o dosiahnutom stupni vzdelania bývalých a súčasných frajeriek jedného frajera a už len prekráča moderátorka obrazovkou. Zábava nekončí ani v tomto bode, pretože moderátorka ( a človek by si myslel že jej mali prirásť k nohám ) sa pohybuje v topánkach s vysokým opätkom ako Golem na chodúľoch. Rozlúči sa tým zvláštnym jazykom, ktorý nemôže byť slovenčinou, pretože v ňom chýba hláska „ ľ "a je koniec. Zasa o týždeň.                                         Musím priznať, že v predchádzajúcich riadkoch som spomenula highlighty z viacerých vydaní tejto mimoriadne vydarenej relácie, pretože, aj keď naďalej trvám na tom, že Smotánku milujem, milujem aj mnoho iných vecí na tomto svete a na túto lásku mi vyjde čas len kedy - tedy.                                        Čo však nestihnem vidieť, to si prečítam v periodicky sa objavujúcich a opakujúcich článoch na tému  - aké hrozné to zasa bolo, čo za zberba sa tam zasa predvádzala a akým právom.                                                                                 Alebo Superstar. Srdcovka  môjho muža. Na dôvody som sa nepýtala, ale pretože bývame v jednom byte, stihnem sa popri iných činnostiach, dozvedieť o Superstar všetko podstatné. Ak veľmi kvičia, asi tam je teraz Šmajda. Keď porotcovia namiesto hodnotenia práve odspievanej pesničky hovoria o semifinále, asi  zasa Bagárová netrafila noty.                                                   Ako je Smotánka už dlhé roky najvydarenejšou zábavnou reláciou televízie  Markíza, tak sú všetky tie moralistické povyky sviežou ozdobou iných médii. Odľahčia každý rozhovor a zapĺňajú skromné miesto opomínaného humoristického žánru v novinách a na itnernete zvlášť.                                       Existuje aj iné vysvetlenie - autori týchto lamentov sa poburujú naozaj. Zle pošťukali tlačidlá na diaľkovom ovládači a myslia si že pozerajú priamy prenos z odovzdávania cien Česká hlava, alebo záznam z odovzdávania Cien Thálie. Možno sa chceli došťukať k Humanitnému činu roka, či vidieť benefičný koncert Úsmev ako dar alebo sa oboznámiť s výsledkami zbierky na Deň narcisov. Alebo sa chceli dozvedieť, kto dostal ocenenie Slovak Gold a namiesto toho vidia zlatý prach  rozfukovaný Erikou Judínyovou. To naštve, to chápem.                                      Hej a ešte tá Superstar. Zatiaľ všetky vážne morality na túto tému síce začali tým, že „ náhodou som 5 minút pozerala " pokračujú niečím ako, že bahno, odpad televíznej zábavy, kvalifikovane zhodnotia tie trápne výkony                 „ rýchlokvasených hviezdičiek " a cez nemilosrdnú kritiku nekvalifikovanej poroty sa dostanú k tomu, že všetci sú síce strašní, ale ten a ten je predsa len strašný menej a porota a diváci sú slepí a hluchí, že to nepočujú a nevidia a nevedia. A samozrejme, občas to je potrebné preložiť stonom , že iné osobnosti sú superstari a zaslúili by si byť v telke.                                      Korunou týchto serióznych úvah na tieto, uznáte naozaj seriózne témy ( veď, čo už môže byť vážnejšie ako to, kto si zaslúži byť v Smotánke a kto je SUPERSTAR ) je pobúrenie nad tým, akože si tvorcovia relácie mohli dovoliť dať týmto nímandom zaspievať nejakú pesničku, ktorá nepatrí do popu ani do iného braku, ale tvorí jeden z najžiarivejších klenotov  hudobného umenia od počiatku sveta. Vo svete kde sa prorok Mohamed zobrazuje v najrôznejších karikatúrach, Panna Mária ako ľahšia ženština a kresťania sa definujú ako uctievači mŕtveho muža na kríži, je naozaj svätokrádežou, keď v zábavnej relácii niekto zaspieva peknú pesničku. A to si z nej ani nerobí zábavu, ale snaží sa, čo mu hrdlo stačí.                                       Jednoducho, zábava bez konca. Občas ma len napadne, že by som sa mohla, čo i len na zlomok sekundy ocitnúť v hlave človeka, ktorého duševným svetom otrasie 5 minút televíznej zábavy . Strasiem sa od hrôzy.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (49)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ženy nevedia čo chcú
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            V Slovenskej sporiteľni alebo v Kocúrkove?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ad: Rodič a rodič alebo ukážka hrubozrnnej demagógie par excellence
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Súmrak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Buďte ako deti. Apdejt
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Iveta Rajtáková
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Iveta Rajtáková
            
         
        rajtakova.blog.sme.sk (rss)
         
                                     
     
        Som Iveta Rajtáková.Asi to podstatné, čo sa dá o mne povedať.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    132
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2336
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Služby
                        
                     
                                     
                        
                            Poézia alebo pocta Sufenovi
                        
                     
                                     
                        
                            Svet nie je children friendly
                        
                     
                                     
                        
                            ja tomu nerozumiem
                        
                     
                                     
                        
                            Videli sme
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                                     
                        
                            Boli sme v...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Z iného sveta
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog Iveta Rajtáková
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




