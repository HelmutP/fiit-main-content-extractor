
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Hlina
                                        &gt;
                Nezaradené
                     
                 Prežil som hrozný víkend. 

        
            
                                    16.6.2010
            o
            10:25
                        (upravené
                16.6.2010
                o
                13:47)
                        |
            Karma článku:
                16.20
            |
            Prečítané 
            3813-krát
                    
         
     
         
             

                 
                    Prežil som hrozný víkend. Zo soboty na nedeľu som celú noc nespal, preto som bol celú nedeľu unavený a na večer som ešte chytil dobrú bolesť hlavy.
                 

                     Nespal som, lebo som pozeral výsledky volieb, ktoré sa doplňovali neskutočne pomaly. Čakal som, ako dopadne Slota. Bol to napinák do poslednej chvíle. Kde bolo tých posledných cca 2000 voličov, ktorí mu pomohli preskočiť. Slota sa dal poraziť. Dokonca, keby nebol prišiel „kyslík" v pravú chvíľu z Budapešti, tak by nám už mával. Takto ešte štyri roky bude pôsobiť toxicky, jeho blbé reči budú atraktívne pre novinárov, a zároveň budú ovlažovať vyschnuté hrdlá slovenských a maďarských kriklúnov.   Som rád, že taktika „Páľte po vlečných lodiach" sa naplnila. Veľký Ficov krížnik sa síce pomaly mení na lietadlovú loď  /... a keď pozbiera to, čo ostalo po Mečiarovi, sa ňou aj stane/, ale bez vlečných lodí stojí a nemá ju kto ťahať.   Pri čakaní na výsledky som videl ľudskú radosť Radičovej, ale aj rozvážne pokyvovanie hlavou starého lišiaka Dzurindu. Kamery zachytili sympatickú eufóriu SAS  a Most/Híd.  Maďari sú v pohode. So spevom je to síce slabšie. Ešte treba zistiť, či vedia hrať futbal. Možno si nakoniec predsa len Matica zahrá futbal s Csemadokom. Tragikomicky pôsobil ten mladý pánko z HZDS, ktorý niečo splietal o tom, že do parlamentu sa dostanú.   Prežil som hrozný víkend. Nespať celú noc ako pubertiak je už pre mňa dosť náročné, ale viete: Mám rád túto krajinu. Trochu si oddýchnem a potom, keďže Mám rád toto mesto ....     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (48)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Čo sa Penta v Číne nenaučila
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Zrušiť zdravotné odvody je nebotyčná hlúposť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Nočné rokovanie bude aj pred Paškovou vilou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Smer zlegalizoval nezákonné billboardy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            O slovenskej žurnalistike rozhodne Shooty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Hlina
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Hlina
            
         
        alojzhlina.blog.sme.sk (rss)
         
                                     
     
        Alojz Hlina - nezávislý poslanec NR SR, občan Slovenska, občan mesta Bratislava, oravský bača. www.alojzhlina.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    518
                
                
                    Celková karma
                    
                                                8.94
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak kvôli tomu vyhodili Čaploviča?
                     
                                                         
                       Politický nekorektné úvahy k Dňu Európy a Dňu víťazstva
                     
                                                         
                       Hanebná nominácia na ústavného sudcu
                     
                                                         
                       Stane sa  SMER chrapúňom  roka?
                     
                                                         
                       Košičania, budete svietiť ako baterky?
                     
                                                         
                       Simpsonovci a Hlinovo hrdinstvo
                     
                                                         
                       Ako udržiavajú P.Paška a R.Kaliňák krehkú rovnováhu?
                     
                                                         
                       Môžete ísť do kostola, ale omša nebude
                     
                                                         
                       Keď SMER zabezpečuje koledy
                     
                                                         
                       Študenti, ako chutila držková u premiéra?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




