
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Rafael je skvelý, ale... 

        
            
                                    29.5.2010
            o
            18:35
                        |
            Karma článku:
                4.59
            |
            Prečítané 
            1100-krát
                    
         
     
         
             

                 
                      nedá sa nič robiť, nemám ho rád! Na svete to už býva presne tak, že človek človeku celkom jednoznačne vyhovuje, alebo aspoň vôbec nezavadzia a zasa naopak, už čo len spomienka evokuje nechuť, alebo vyvoláva priamo vyrážky a ani to nevystihuje silu prežitého. Musím sa však vyznať, že nehovorím o anjelovi Rafaelovi, ktorý je jednoznačným symbolom božskej dokonalosti v biblických príbehoch a už vôbec nie o tom „eseneskárskom“ Rafajovi, cez ktorého činy sa aj slovenská politika stáva v mnohom legendárnou, aj keď to naozaj nie je žiadna sláva, skôr primitívny podvod, ale chcem vám porozprávať o španielskom tenistovi Rafaelovi Nadalovi, pre ktorého sa mi nedarí ani hlboko v  duši nájsť pekné miestečko. V tom je s tým našim politikom narovnako, ale keďže je to naozaj známa osobnosť a skutočná celebrita, tento fejtónik si zaslúži!
                 

                     Spravodlivo, Rafael Nadal je mimoriadny tenisový talent, jeden z najonakvejších svetových hráčov súčasnosti i histórie a keď sa na chvíľu stal rebríčkovou jednotkou, bol som jednoducho nešťastným tenisovým fanúšikom. Doznávam, že je to asi v tom, akým dojmom na mňa pôsobí a rovnako tak bezpodmienečne pripúšťam, že obdivujem skôr brunetky než blondínky, mám radšej tatársky biftek než sekanú a aby to bolo spravodlivé, tak v mojom nápojovom lístku by pokojne mohla absentovať ponuka noblesnej škótskej či bourbonu!   Rafael Nadal je pre mňa synonymom mnohých najúžasnejších tenisových daností. Je fyzicky dokonalý a hoci nie je žiadnym naozajstným Adonisom – len si predstavte tie nešťastne „prerastené“ lýtka – dá sa jeho muskulatúra skôr obdivovať než haniť. Má danosť bojovať o česť a víťazstvá zo všetkých svojich síl presnou a mocnou ľavačkou a ešte aj mimika, reč tela a samozrejme všetky zvukové prejavy s tým súvisiace ho nominujú do tej skupina hráčov, o ktorých sa dajú skladať ódy. Je jednoducho úžasný a predsa aj v tej dokonalosti je kaz, defekt, porucha a zádrhel, s ktorými sa nedokážem pokonať a naozaj nikdy sa nemôžem stať jeho fanúšikom.   Za kaz v jeho prejavoch na tenisovom kurte považujem predovšetkým jeho grimasu pri podaní, teda pri uvedení loptičky do hry. Telo sa pohybuje tak akosi roboticky, nie je to plynulé, hoci naozaj dynamické, ale problémom sa pre mňa stala zlostne ohrnutá horná pera, vycerené zuby a hoci je to iba okamih na televíznej obrazovke, kde tohto tenisového génia vídavam najčastejšie, stačí to k identifikácii jeho nenávisti k súperovi. Možno podvedomej, ale je to tam, viem to a šport takúto nechutnosť jednoducho netoleruje.   Defektom v Nadalovom hernom prejave je pre mňa vražedný pohľad adresovaný tej druhej strane a čo je najhoršie, býva realizovaný veru aj vo chvíli uznania svojich vlastných cností, lebo napríklad zovretá päsť signalizuje radosť a cez ústa sa na svet derie pokrik „vamos“, tie oči  však celému svetu oznamujú, že najdôležitejším citovým prejavom je nekontrolovateľný odpor k súperovi.   Poruchou v prejavoch Rafaela Nadala pri hre je danosť všetko na svete využívať, či vlastne zneužívať vo svoj prospech. Nikdy sa nesnaží dať hre prirodzený rytmus, naopak, je režisérom i realizátorom rôznych odkladov, prerušení ba celých „mizanscén“, ak mám popísať tenisovú hru ako naozajstné predstavenie pre verejnosť a všetky tie klepnutia loptičkou, uhládzanie si vlasov, naprávanie čelenky a kontrola všetkého, na čo si len spomeniete slúži jednoznačne k tomu, aby sa súper začal smažiť a opekať v ohníčku pochybností a nestability. Občas to rozhodcovia postrehnú, ba už som niektorých videl na to aj zareagovať, ale španielsky tenista má v tomto prípade na sebe naozajstné brnenie nezáujmu. Pre mňa je v tej chvíli nesympatickým „donom z histórie“ a hoci sa Don Quijotte i Sancho Panza uvádzajú skôr v dobrom, môj zážitok je vždy negatívny.   No a celkom nakoniec som si nechal zádrhel v činoch Rafaela Nadala! Je mimoriadny, vo svete tenisu jedinečný a čo ako som sa snažil pochopiť, prečo je už toľké roky neodstrániteľný, na nič som neprišiel. Rafael sa vlastne nepretržite škriabe v zadku, alebo po ňom!   Na rôzne spôsoby, mocne, jemne, ako keby náhodou a neúmyselne, so zaľúbením, z nevyhnutnej potreby, no v každom prípade vlastne permanentne! Vždy znova a znova som hľadal príčiny. Najskôr to mohlo byť nevhodnou bielizňou, potom nechutnými šortkami, čo pripomínali dlhočizné spodky našich dedov zo zimných mesiacov roka, ale zmeny v kostýmovom vybavení tohto tenistu to už dávno vylúčili. Rovnako tak už nepochybujem ani o kvalite textilu, z ktorých je jeho dres vyrobený, som si istý, že dnešná medecína už problémy s parazitmi v konečníku ľudí vyriešila  naozaj dávno a tak mi vlastne zostáva iba myšlienka na niečo také plytké, ako je sebauspokojovanie. Hanba mi, ale napísať som to musel, aby ste uznali, že chcem byť jednoducho spravodlivý!   A ešte mi napadla ďalšia, asi jediná možnosť tento zádrhel odstrániť – poradiť Rafaelovi Nadalovi návštevu vo voľajakej psychologickej poradni, kde sa pod dohľadom odborníkov takmer všetky zlozvyky odstránia celkom spoľahlivo. A ak by to malo byť dôslednou reparáciou uvedených nedostatkov, ten známy a mne neobľúbený španielsky tenista by sa mohol stať tenisovou jednotkou aj zajtra a bolo by to bez protestov. Dúfam, že mu tento fejtónik voľakto preloží, alebo mu o ňom porozpráva, lebo miesto v tenisovej histórii by sa mu viac zaligotalo.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (31)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




