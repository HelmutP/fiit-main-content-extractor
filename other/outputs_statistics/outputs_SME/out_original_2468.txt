
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marian Jánoš
                                        &gt;
                Politické komentáre
                     
                 O Sulíkovi, alebo rozdiel medzi manažérom a lídrom 

        
            
                                    6.3.2010
            o
            1:16
                        |
            Karma článku:
                15.45
            |
            Prečítané 
            9501-krát
                    
         
     
         
             

                 
                    Pozrel som si DeFacto Belousová vs. Sulík. Je mi smutno. Už viackrát ma svrbeli prsty s úmyslom napísať zasa o nejakej politickej téme, ale vždy ma nakoniec odradila všeobecná presýtenosť priestoru podobnými článkami. Dnes urobím výnimku.
                 

                 SaS sledujem už od začiatku a obdivujem Richarda Sulíka ako dokázal z ničoho vybudovať novú liberálnu stranu. Nemal žiadne celebrity, vlastnú televíziu, ba dokonca aj jeho samotného málokto poznal. Dnes sa zdá, že je pravdepodobné, že to dokáže a prekročia tých zapeklitých 5%.   Ale tak ako Sulíkove manažérske schopnosti SaS pomáhajú, jeho vodcovské ju potápajú dole. Neexistuje aby nestačil Belousovej v diskusii a aby ho ona poučovala z použitia falošného argumentu ad populum („všetci si myslia..."). To je veľká hanba. Opäť pôsobil nepresvedčivo, neverbálna komunikácia veľmi slabá, ako strémovaný stredoškolák čo odpovedá pred tabuľou. A učiteľka Belousová ho poučuje o jeho nedostatkoch.   Manažéra nasledujú ľudia na základe jeho pozície v organizácii. Má mať dobré technické zručnosti, vie dobre organizovať, vyjednávať a komunikovať. Lídra nasledujú ľudia dobrovoľne. Vie ich zapáliť pre svoje myšlienky, pretože sám horí, je presvedčivý, má charizmu, vie si obhájiť hodnoty ktoré presadzuje.   Sulík je dobrý manažér, ale nie líder. Málokedy sa toľko vzácnych schopností stretne v jednej osobe. Čím skôr to sám pochopí, tým lepšie pre SaS. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (224)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Jánoš 
                                        
                                            Blogolock
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Jánoš 
                                        
                                            Nachmelené fakty o sankciách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Jánoš 
                                        
                                            Uznávať Pentaboys?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Jánoš 
                                        
                                            Neprimerané zisky emitentov stravných lístkov škodia slovenskej ekonomike
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Jánoš 
                                        
                                            Zastavme trend totálneho rodičovstva!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marian Jánoš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marian Jánoš
            
         
        marianjanos.blog.sme.sk (rss)
         
                        VIP
                             
     
        Fanúšik vedeckého skepticizmu
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    99
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6309
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kontroverzné témy
                        
                     
                                     
                        
                            Politické komentáre
                        
                     
                                     
                        
                            Pohrebisko povier a šarlatánov
                        
                     
                                     
                        
                            Diskusia namiesto diss-kecie
                        
                     
                                     
                        
                            Chrumkavá veda
                        
                     
                                     
                        
                            Mix
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Moje oblubene songy (Update 20.9.2009)
                                     
                                                                             
                                            Popoluska ci Shakira? LOL :)
                                     
                                                                             
                                            Uz maju dokaz! Neostava nam nic ine, ako uverit...
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            O vesmire
                                     
                                                                             
                                            vsetko mimoriadne
                                     
                                                                             
                                            O zdravotnictve
                                     
                                                                             
                                            Viliam Bur
                                     
                                                                             
                                            SPW
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            e-bay
                                     
                                                                             
                                            Shooty
                                     
                                                                             
                                            134. najvacsi cech :)
                                     
                                                                             
                                            kalerab
                                     
                                                                             
                                            vesmirna drubez
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




