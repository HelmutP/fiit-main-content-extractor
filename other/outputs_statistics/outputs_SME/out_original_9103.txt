
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Menyhertova
                                        &gt;
                Nezaradené
                     
                 Farebný svet 

        
            
                                    18.6.2010
            o
            7:23
                        (upravené
                18.6.2010
                o
                8:11)
                        |
            Karma článku:
                4.59
            |
            Prečítané 
            561-krát
                    
         
     
         
             

                 
                    Zavše mám pocit, že som už veľká. V takýchto prípadoch  stačí vojsť do detskej ibičky, hneď sú z nás dospelákov malí krpci. Alebo aj taká škôlka......
                 

                 Včerajšia návšteva materskej  škôlky na Budapeštianskej 3 v Košiciach ma presvedčila, že byť učiteľkou v MŠ nemôže byť hocikto. Nieje to iba povolanie, ale aj poslanie. Nefalšovaný úsmev, záujem o každé jedno dieťatko, v neposlednom rade láska. Moja dcéra k tetám učiteľkám priľnula ako magnet. Nečudujem sa..... Som im ale nesmierne vďačná, že za ten čas, ktorý som ja s dcérkou nemohla byť, ma zastúpili veľmi dobre. Až pridobre, povedala by som.   Škôlka končí, začína sa škola..........to bolo námetom pre včerajšiu rozlúčkovú slávnosť so školkou. Bolo to dojímavé. Plakala som. A nielen ja. Všade, kam som dovidela cez rozmazanú maskaru som videla slzy v očiach rodičov. Aj oteckov. Niektorý verejne, iný len tak do kútika, aby to nikto nevidel.  Pani učiteľky sa nehanbili, slzičky sme videli všetci. Ale také úprimné.   Byť v triede medzi krpcami je fakt zážitok. Všetko sa mi tam páči. Ešte aj tie maličké stoličky, všade výtvory "domácich". Na stenách visia najkrajšie práce, prípadne ocenenia, na chodbách sú spomiekové tablá predchádzajúcich škôlkarov. Celý ich svet je taký pekný, útulný, hravý, ale najmä farebný. Hlavne o tú farebnosť ide......podľa detí je to väčšinou tak, že čo nieje farebné, tak sa vlastne neráta. Alebo je to určené pre dospelákov. Detičky dostali prekrásnu tortu, pohostenie bolo nádherne naaranžované. Prvý krát ale v histórii MŠ dostali aj osvedčenie o ukončení predprimárenho vzdelania. Zase nejaký vtip, myslela som si. Zistila som, že ide o originál tlačivo, podobné prváckemu vysvedčeniu. Pani riaditeľa nám rodičom prízvukovala, aby sme si tento dekrét uchovali, pretože pri nástupe do školy sa ním musíme preukázať. Pridala aj príhodu, ktorá ma hneď chytila za srdce:   Pred dvoma dňami, ako som tieto osvedčenia chystala, za mnou do riaditeľne prišiel jeden žiačik. Ako sa mám, a že čo to vyrábam. Odpovedala som, že je to osvedčenie, a je to pre ich celú triedu. Chvíľu sa díval a zahlásil, že nieje pekné, lebo nieje farebné. Potom som vysvetlila, že je to vlastne taký doklad, ten nemusí byť vždy pekný. Na to malý chlapček zahlásil: "to nevadí, veď ja si ten štátny znak doma vyfarbím!"   Ďakujem pani učiteľky za krásny farebný svet, ktorý ste v tomto našom dospeláckom utvorili pre naše deťúrence..........     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Mužolapka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Rytmus a jeho kecy....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Závisť, alebo prehnané ego?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O  cintoríne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O delení ministerstiev...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Menyhertova
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Menyhertova
            
         
        menyhertova.blog.sme.sk (rss)
         
                                     
     
        som človek s chybami.....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    971
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Výnimočná
                                     
                                                                             
                                            Timea Keresztényiová :Sama mama
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lara Fabian
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




