
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rado Tomek
                                        &gt;
                Hudba
                     
                 Nedeľný hudobný výlet s Lonely Drifter Karen 

        
            
                                    14.5.2010
            o
            11:30
                        |
            Karma článku:
                2.02
            |
            Prečítané 
            732-krát
                    
         
     
         
             

                 
                    Chcete navštíviť kabaret ako z Weimerskej republiky? Alebo sa na chvíľu premiestniť do parížskej kaviarne či viedenskej cukrárne so Sacher tortou na tanieri? Nemusíte si hneď kupovať letenky ani lístky na autobus či nebodaj cestovať v čase. Atmosféru týchto a mnohých iných kútov Európy vám ponúkne koncert medzinárodného zoskupenia Lonely Drifter Karen už túto nedeľu v bratislavskom Nu Spirit Bare.
                 

                 Koncert je súčasťou série NouveauNu Nights, ktoré v spolupráci s Nu Spirit pripravuje agentúra Vresk. Ich cieľom je spríjemniť fanúšikom dobrej alternatívnej hudby čakanie na ďalší ročník rovnomenného festivalu plánovaný na október.      Začiatky Lonely Drifter Karen sa datujú do roku 2006, keď sa viedenská rodáčka Tanja Frinta presťahovala do Barcelony. Tam sa stretla s talianskym bubeníkom Giorgiom Menossim a španielskym klávesákom Marcom Meliá Sobreviasom. Už na jar 2008 medzinárodné zoskupenie vydáva svoj prvý album, ktorý sa stretol s veľmi priaznivým ohlasom kritikov a publika. V marci triu vyšlo druhé CD Fall of Spring, ktoré skupina na aktuálnej koncertnej šnúre predstavuje.   Zvuk Lonely Drifter Karen sa nedá zaškatuľkovať. Pripravte sa na flamenco, kabaret, avant-pop a mnoho ďalších štýlov, ktoré divákovi ponúknu naozaj nevšedný hudobný zážitok. Nenechajte si ho ujsť a rezervujte si miesto na 0905/865566. Kapacita "starého" Nu Spirit Baru, ktorý nájdete na Medenej 16,  je totiž limitovaná.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Nebojte sa japonskej scény alebo pozvánka na stredu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Prečo sa teším na Hot 8 Brass Band (alebo Nebojte sa dychovky)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Ako skoncovať so strachom z hip hopu: Lekcia 2, Dámy za mikrofónom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rado Tomek
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rado Tomek
            
         
        tomek.blog.sme.sk (rss)
         
                                     
     
        som ekonomicky novinar a okrem toho aj DJ Kinet, promoter, majitel bratislavskeho Nu Spirit Baru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    102
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1408
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Nebojte sa japonskej scény alebo pozvánka na stredu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




