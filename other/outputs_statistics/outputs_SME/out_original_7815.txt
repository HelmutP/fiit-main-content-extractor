
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Fořt
                                        &gt;
                Nezaradené
                     
                 SaS: Snaha o transparentnosť v politike. 

        
            
                                    1.6.2010
            o
            23:01
                        (upravené
                2.6.2010
                o
                9:38)
                        |
            Karma článku:
                10.15
            |
            Prečítané 
            1828-krát
                    
         
     
         
             

                 
                    Volebná kampaň vrcholí. Začal jún - mesiac volieb do NRSR. Nachádzame sa v tradičnej fáze očierňovania. Veľa ľudí práve táto časť znechucuje až odrádza, nielen od voľby tej ktorej strany, ale aj od samotnej účasti na voľbách. Je to ale zároveň aj obdobie testov sľubov politických strán o odvolávaní "do troch minút".
                 

                 Do politiky som vstúpil nedávno. Stal som sa priamym svedkom diania, ktoré som predtým vnímal len zvonku z dostupných médií.Som účastníkom udalostí.   Spolupodieľam sa na tvorbe toho, čo sa stane.Som súčasť SaS.   Chcem sa s vami podeliť o svoje pocity a doterajšie skúsenosti v otázke, či je možné do výberu kandidátov v politike vniesť transparetnosť.Rozhodnutia, ktoré už máme v SaS za sebou, ako aj to, ako ich preverila prax.   SaS si dala ambiciózny cieľ. Podstatne zvýšiť štandard transparentnosti svojich kandidátov oproti doterajšej praxi, keďže najmä systematická korupcia ako aj osobné zlyhania vo verejnej sfére sú chronickým morom, ktorý kvári slovenskú politickú scénu nepretržite od novembrovej revolúcie.Cynik či realista by snáď povedal, že naivná snaha, márny vopred prehraný boj.   Kritéria na výber kandidátov sú postavené v SaS vysoko nad zákonné požiadavky.SaS sa zaraďuje medzi strany s najprísnejšími kritériami.K nárokom kladenými na uchádzača patrí napríklad nulová tolerancia k predchádzajúcemu členstvu v KSS/KSČ či vedeniu vo zväzkoch ŠTB, uchádzači nesmú mať podlžnosti voči štátu a musia sa preukázateľne živiť poctivým spôsobom. Nesmú mať záznam v registri trestov a ich doterajšie aktivity a minulosť nesmie vrhať podozrenie z účasti na tunelovaní štátu alebo spolupráce s osobami, ktoré sa na ňom podielali.Nielen že nepochybili, ale aj niečo dosiahli.Dôležitá je otvorenosť a súhlas s vystavením sa zvýšenej verejnej kontrole novinárov, politických konkurentov, susedov, tretieho sektora, verejnosti - kohokoľvek napríklad zverejnením svojich podrobných majetkových a príjmových pomerov na www.politikaopen.sk ešte pred konaním volieb, v ktorých kandidát kandiduje. K.O. kritérium je súlad s hodnotami a princípmi, na ktorých je SaS postavená, a ktoré chce v politike presadzovať. Oportunizmus vyznačujúci sa v prelezení rôznych politických strán jednotlivcom v minulosti o takom súlade s hodnotami nesvedčí.Výsledok tejto politiky sa odzkadľuje v uprednostnení kvality pred kvantitou členskej základne.   Požiadavky na uchádzača sú verejné a sú uchádzačom známe ešte pred podaním prihlášky. Každý uchádzač tak prechádza svojim vlastným vnútorným testom.Väčšina záujemcov všetky tieto kritéria nespĺňa a od svojho zámeru odstúpi.Tí ostatní prihlašku podajú. Zaujímavé je, že v súčasnosti sa asi 3 zo 4 takto podaných prihlášok v internom preverovaní SaS zastavia a len 1 prejde. Takto preverený kandidát robí čestné vyhlásenie o skutočnostiach, ktoré sú relevantné pre rozhodovanie SaS a boli predmetom preverovania. Aj keby sa teda v priebehu interného preverovania neodhalili šrámy na povesti a mene uchádzača,uchádzač sa k nim prihlási v čestnom prehláseni vedomý si následku, ak by sa v budúcnosti preukázal opak. Následne nastupuje druhý stupeň kontroly a tou je tá verejná. Kandidáta tak preveruje verejnosť, možno aj SIS, a to ešte skôr, ako kandidát prejde voľbami, v ktorých by mohol byť v prípade úspechu zvolený. Osobne sa veľmi teším tomu, že cielené vystavenie kandidátov SaS zvýšenej verejnej kontrole už viedlo k svojmu ovociu. Ešte pred voľbami sa v dvoch prípadoch odhalil nesúlad s vnútornými kritériami SaS. Proste systém zafungoval a preukázal svoju životaschopnosť.   Na SaS bolo následne dostáť sľubu danému verejnosti. A ja som rád, že SaS obstála. Obaja títo kandidáti už ďalej nekandidujú. SaS sa ich nesnažila kryť.Naopak.Rozhodnutie bolo rýchle a plne v súlade s hodnotami SaS. Systém splnil svoju úlohu, nepustil kandidáta ďalej. Zámer sa naplnil.   Transparentnosť chápeme v SaS široko.Nielen v transparentnosti kandidátov, ale aj v transparentnosti konkrétneho volebného programu, transparentnosti financovania strany bez externých tieňových sponzorov a transparentnosti v tom, s kým  určite nebudeme rokovať o povolebnej koalícii či v transparentnosti priamej komunikácie s voličmi.   12.6 budem vstávať spokojný. V čase otvorenia volebných miestností si budem môcť povedať, že sme odviedli kus dobrej roboty pre jeden z dôležitých cieľov SaS:    Transparentnosť. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (37)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Preniesť sa ponad seba a zažiť kus večnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Aky je odkaz bilbordu Aliancie za rodinu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Couchsurfing, moja vlastná 6 ročná skúsenosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Drobné prekvapenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Kabaret: milujem cesty vlakom 2. triedy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Fořt
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Fořt
            
         
        fort.blog.sme.sk (rss)
         
                                     
     
        Mám rád slobodu, je to moja životná filozofia a môj životný kompas. Som pozorovateľ sveta. Fascinujú ma neviditeľné veci, ktoré hýbu dušou človeka. Zaujímam sa o happyológiu a dlhovekosť. Som couchsurfer. Obohacujú ma stretnutia. 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2977
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Karol Kaliský
                                     
                                                                             
                                            Samuel Genzor
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       6 tipov pri výbere osobného trénera
                     
                                                         
                       Gladiátor HD tréning
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




