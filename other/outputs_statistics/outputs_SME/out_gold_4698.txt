

 Avšak každá správna rocková skupina musí mať aj člena, ktorý by ovládal hudobný nástroj, zvaný bubny. Preto sa už menovaný M. Mokráň rozhodol, že on bude ten, ktorý skupinu zachráni a naučí sa hrať na bubny. Posledným problémom pre novovzniknutú skupinu bolo vyriešiť otázku priestorov. Aj toto však členovia racionálne vyriešili a rozhodli sa skúšať v priestoroch pivnice jedného z nich. 
 Skupine už teda nič neprekážalo v tom, aby mohli skúšať a pripravovať skladby pre svojich fanúšikov. Už v auguste 2009 sa konal ich prvý koncert, kde sa skupina stretla s mnohými priaznivcami a vyznávačmi ich štýlu, teda alternatívneho rocku. 
 Schussdefekt dúfa, že fanúšikov bude stále len pribúdať a oni budú môcť onedlho vydať svoje prvé CD. Navyše si túto skupinu môžete vypočuť už onedlho, teda 24.04.2010 v klube v priestoroch futbalového štadióna v Zlatých Moravciach. 
 Viac informácií o skupine Schussdefekt sa dočítate na webovej stránke www.schussdefekt.szm.sk 
   
   

