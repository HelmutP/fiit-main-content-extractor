
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Hlina
                                        &gt;
                Nezaradené
                     
                 Občan  Hlina pozýva spisovateľa Hvoreckého 

        
            
                                    3.4.2010
            o
            19:38
                        (upravené
                3.4.2010
                o
                20:32)
                        |
            Karma článku:
                14.61
            |
            Prečítané 
            4651-krát
                    
         
     
         
             

                 
                    Keď som bol prvý krát v parlamente lepiť nálepku „Zákaz vstupu pod vplyvom alkoholu", tak som tam hľadal poslanca Slotu. Bolo mi povedané, že treba volať poslanca Púčika. Volal som teda Púčikov sekretariát. Po chvíli prišla taká milá pani, ktorá pôsobila vystrašene a zmätene, čo potvrdila aj jej prvá otázka: „ A Vy ste ako kto?"
                 

                  Moja odpoveď , že som občan, občan a ešte aj trochu sused od vedľa mi v ničom nepomohla. Zdá sa, že byť občanom, špeciálne byť občanom v našom parlamente nič veľké neznamená. Hľadať Slotu alebo iného poslanca v parlamente môže v dnešnej dobe len iná „kapacita", a nie len nejaký občan.   Treba sa pokúsiť  aspoň čiastočne vrátiť starú zašlú vážnosť občanovi. Vážnosť, ktorej sa občan tešil už v starovekom Grécku. Vážnosť občanovi, ktorému dnes už ruže nekvitnú a stáva sa pomaly existenčne ohrozeným druhom.   Existenčne ohrozeným nie v tom zmysle, že na to, aby niekto mohol byť občanom, musí byť aj agentom niekoho alebo niečoho, a zákonite musí byť niekým platený, ale existenčne ohrozený v tom zmysle, že je ohrozeným druhom zapísaným v červenej knihe, druhom  odsúdeným na vymretie a špeciálne práve na Slovensku.   Skúsime v sobotu robiť Občianske senáty na zaujímavú tému, so zaujímavými hosťami, kde občania, ktorí sa budú chcieť zúčastniť senátu, budú srdečne vítaní.   V sobotu 10.4.2010 od 16,00 bude Občiansky senát v priestoroch bývalého kina Praha na námestí SNP 8 / v súčasnosti to je veľká bratislavská reštaurácia, názov uvádzať nebudem, aby to nebolo posudzované ako reklama/.   Mám pocit, že ako hosť by mohol byť zaujímavý spisovateľ Hvorecký, ktorého týmto pozývam a verím, že pozvanie prijme.   Aká bude téma a koho ďalšieho pozveme bude závisieť od toho, či spisovateľ Hvorecký prijme pozvanie. V prípade, že áno, budem informovať.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (84)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Čo sa Penta v Číne nenaučila
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Zrušiť zdravotné odvody je nebotyčná hlúposť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Nočné rokovanie bude aj pred Paškovou vilou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Smer zlegalizoval nezákonné billboardy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            O slovenskej žurnalistike rozhodne Shooty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Hlina
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Hlina
            
         
        alojzhlina.blog.sme.sk (rss)
         
                                     
     
        Alojz Hlina - nezávislý poslanec NR SR, občan Slovenska, občan mesta Bratislava, oravský bača. www.alojzhlina.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    518
                
                
                    Celková karma
                    
                                                8.94
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak kvôli tomu vyhodili Čaploviča?
                     
                                                         
                       Politický nekorektné úvahy k Dňu Európy a Dňu víťazstva
                     
                                                         
                       Hanebná nominácia na ústavného sudcu
                     
                                                         
                       Stane sa  SMER chrapúňom  roka?
                     
                                                         
                       Košičania, budete svietiť ako baterky?
                     
                                                         
                       Simpsonovci a Hlinovo hrdinstvo
                     
                                                         
                       Ako udržiavajú P.Paška a R.Kaliňák krehkú rovnováhu?
                     
                                                         
                       Môžete ísť do kostola, ale omša nebude
                     
                                                         
                       Keď SMER zabezpečuje koledy
                     
                                                         
                       Študenti, ako chutila držková u premiéra?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




