
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľudovít Kaník
                                        &gt;
                DÔCHODKY
                     
                 Slotove plané reči a jeho cigáni 

        
            
                                    16.4.2010
            o
            11:21
                        |
            Karma článku:
                10.78
            |
            Prečítané 
            2757-krát
                    
         
     
         
             

                 
                    Rómovia, cigáni. Slotova obľúbená téma. Silné reči a nič viac. Pritom jasné,  podľa niekoho tvrdé, ale fungujúce pravidlá tu už boli. Tomanová ich vyhlásila za nútené práce.
                 

                 Pred voľbami sa vždy vyroja  experti na oblbovanie národa so zaručenými návodmi na riešenie problémov.  Jeden z evergreenov je riešenie rómskeho problému.  A najlepší expert  je, ale iba pred voľbami a iba rečami, Slota. Čo sa pamätám, vždy sa rád zapísal do povedomia svojej komunity výrokmi typu: „malý dvor a veľký bič na nich" a podobne. Silné reči, pred voľbami, žiadne zmysluplné skutky po nich.   Najlepší sú títo experti vtedy, keď navrhujú, ako zapojiť rómov do práce pre obec,  inak nebudú sociálne dávky.  Pritom toto všetko je nielen dávno vymyslené, ale bolo to aj zrealizované.  Systém aktivačných prác, ktoré som zaviedol do sociálneho systému počas predchadzajúcej vlády bol účinným nástrojom na to, čo Slota  objavuje siláckymi rečami teraz. Pritom 4 roky SNS sedí vo vláde a všetko, čo v tejto oblasti dokázali je motivovať k pôrodnosti ako zdroja príjmov tie  najchudobnejšie komunity. Veď, čo iné je zavedenie vysokého príspevku na tri narodené deti bez akéhokoľvek kriteria zamestnania aspoň jedného z rodičov?   Aktivačne práce, ktoré som ako minister zaviedol, vtiahli do práce tisíce ľudí, medzi nimi veľku časť rómov, ktorí roky nepracovali.  Mnohým sa to zdalo tvrdé, ukázalo sa, že je to účinné. Mestám a obciam ich práca vytvorila množstvo hodnôt, ktoré by inak bolo treba draho platiť.  Ministerka Tomanová označila tento systém za nútené práce a snažila sa ho obmedziť ako sa len dalo. Keďže nič lepšie nevedela vymyslieť napriek tomu, že to za nútené práce považovala, tento systém, aj keď  obmedzene, funguje do dnes.  Je a bolo na schopnosti a šikovnosti starostov a primátorov ako dôkladne dokázali tento systém využiť, koľko práce vykonať, koľko sociálne odkázaných a rómov zapojiť do práce v prospech všetkých.   Aktivačne práce, znamenali náhradu práce, kým sa trvalá práca nenašla. Udržiavanie a získavanie pracovných návykov u tých, ktorí ich buď nikdy nemali, alebo stratili. A hlavne, bolo napĺňaním princípu: „Ak potrebuješ pomoc, spoločnosť Ti má pomôcť v rámci svojich možností, ale ty to musíš spoločnosti vrátiť. Svojou snahou, prácou, aktivitou. Znamenali  návrat k pravidlám, poriadku a uvedomeniu si, že práca, nech je akákoľvek ,je základná cesta k zlepšeniu svojho hmotného postavenia".   Tento systém treba znovu vrátiť a široko využívať.  Zlepšovať a rozvíjať. Medzitrh práce, ktorý máme vo volebnom programe je ďalším stupňom a opatrením, ktorý môže výrazne zlepšiť situáciu najmä v oblasti dlhodobej nezamestnanosti špeciálne ľudí  s nízkou kvalifikáciou alebo bez kvalifikácie.   Nie však Slotove plané silácke rečičky, ani Tomanovej a Ficove socialistické nezmysly.       Ľudo Kaník  exminister PSVaR   Kandidát do NRSR za SDKÚ-DS     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (72)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Nestačí prejsť do 2. kola, treba zvíťaziť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Fico sa sám usvedčil z manipulovania štatistík
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Kam sa podelo 400 miliónov pri transakcii súvisiacej s SPP?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Čo môžete naozaj čakať od smeráckeho župana?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Ktože to obviňuje z napomáhania Smeru? Ich koaličný partner? :-)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľudovít Kaník
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľudovít Kaník
            
         
        ludovitkanik.blog.sme.sk (rss)
         
                                     
     
        Poslanec NRSR za SDKÚ-DS. Exminister práce, sociálnych vecí a rodiny SR a dlhoročný predseda DS , ktorá sa pod jeho vedením po dlhoročnej spolupráci s SDKÚ integrovala na politickú stranu SDKÚ-DS, zodpovedný za reformu sociálneho systému a trhu práce. Pripravil a zrealizoval reformu zákonníka práce, dôchodkovú reformu, reformu systému sociálnych dávok a boj s čiernou prácou. Dnes aktívny člen SDKÚ - DS a zakladajúci člen občianskeho združenia SPORITEĽ na ochranu záujmov občanov, účastníkov dôchodkového sporenia v II. pilieri dôchodkového systému. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2934
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            DÔCHODKY
                        
                     
                                     
                        
                            PRÁCA
                        
                     
                                     
                        
                            RODINA
                        
                     
                                     
                        
                            POLITIKA
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nestačí prejsť do 2. kola, treba zvíťaziť
                     
                                                         
                       "Manažér" Paška
                     
                                                         
                       Fico sa sám usvedčil z manipulovania štatistík
                     
                                                         
                       Kam sa podelo 400 miliónov pri transakcii súvisiacej s SPP?
                     
                                                         
                       Máme sa tváriť, že čierne stavby a krádeže pozemkov nevidíme?
                     
                                                         
                       Na čo prišli na ministerstve práce
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




