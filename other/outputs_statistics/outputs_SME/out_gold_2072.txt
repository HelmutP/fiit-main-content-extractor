

 Taká normálna domáca post-novorčná party v jednom z malebných domčekov vo Woodstocku. Ľudia postávajú v obývačke s pohárikmi vína v rukách, občas im popod nohy prebehnú jačiace ratolesti, pojedajú pekne naaranžované prevažne celozrnné koláčiky, či organickú zeleninu a vedú zdvorilé konverzácie. 
 Pani je sympaticky a inteligentne vyzerajúca šesdesitnička, očividne hrdá na svoju hippie mladosť. Prezradím jej, že som v mestečku nová a porozpráva mi, že ona tu vyrástla, dokonca sa ako šestnásťročná zúčastila pred 40 rokmi TOHO Woodstock koncertu a poradila mi, kde nájdem na okolí zaujímavé koncerty a kultúrne akcie pre deti. Medzičasom samozrejme zachytila môj prízvuk a opýtala sa odkiaľ som. 
 Odpovedám „Slovakia" a vidím onen prázdny pohľad, z ktorého je jasné, že nemá predstavu kde tá krajina asi tak bude. Odhadujem tiež, že asi nebude mať záujem o priveľa detailov a tak jej dávam jednoduchšiu orientáciu: „To je v strednej Európe. Moje rodné mesto je iba nejakých 35 míl východne od Viedne..." 
 Podľa mojich skúseností toto obyčajne funguje celkom pekne, lebo väčšina vzdelaných Američanov sa aspoň snaží predstierať, že vedia, kde je Viedeň. Táto pani ma ale zaklincovala: 
 „Fíha, tak blízko? To som nevedela. Tak to ste v tej vašej krajine celkom civilizovaní, však?" 
 Nestrácam úsmev a odpovedám okamžite: „Áno, nemusíte sa báť, nevycikám sa tu na koberec, či niečo podobné". 
 Dáme v tom momente došlo, čo povedala, očervenela a začala sa ospravedlňovať, že to tak nemyslela. Po ďalších dvoch pohároch vína sme nakoniec v príjemnej atmosfére zvládli aj ten zemepis a históriu... 
 Len mi to tak v hlave vŕta. Keď podľa tejto teórie civilizácia končí pri Viedni, tak keby som náhodou nevyrástla v Bratislave, ale povedzme v Poprade ako moja mama, tak by som asi pre dobré meno Slovenska musela minimálne pohrýzť pani domácu a zožrať granule psovi z misky... 
  
   

