

 
Tvarohový koláč s čokoládou a jahodami
 
 
Tento jednoduchý a efektný koláč sa hodí vždy, keď potrebujete za pár minút pripraviť delikátnu sladkosť. Spojenie tvarohu, čokolády a jahôd stojí za to a námaha je naozaj minimálna. 
 
 
Čo budeme potrebovať
 
 
Jeden balíček lístkového cesta. Pol kilogramu mäkkého tvarohu, 2 decilitre mlieka, 3 vajcia, čokoládový pudingový prášok, pár jahôd, 50 gramov horkej čokolády a kryštálový cukor. 
 
 

 
 
A čo s tým urobíme  
 
 
Lístkové cesto si môžeme pripraviť doma, alebo ho môžeme kúpiť hotové a rozvaľkať na veľkosť plechu. Prípadne, a to bude úplne najjednoduchšie a najrýchlejšie, ho môžeme kúpiť už rozvaľkané a stočené s papierom na pečenie. Stačí ho rozbaliť a potom rovno s tým papierom šupnúť na plech. Rozvinuté cesto na plechu popicháme vidličkou.  
 
 

 
 
Do misky si vysypeme mäkký tvaroh, pridáme vajcia, kakaový puding, mlieko a najmenej tri kopcovité lyžice cukru. Ak máme radi sladšie, tak viac.
 
 

 
 
Metličkou všetko poriadne premiešame, aby sa suroviny pekne spojili a boli hladké.
 
 

 
 
Tvarohovú hmotu vylejeme na cesto a ozdobíme nakrájanými jahodami a kúskami nalámanej horkej čokolády.  
 
 

 
 
Pečieme v stredne vyhriatej rúre asi 40 až 45 minút. Koláč je upečený, keď do neho zapichneme drevené špáradlo a po vytiahnutí zostane špáradlo čisté. 
 
 

 
 
 
 
 
Sypaný tvarohový koláč
 
 
Bol som si úplne istý, že tento recept musí poznať každý, takže ma prekvapilo koľko ľudí o ňom vôbec nevedelo. Je to ten najjednoduchší koláč na svete a upečie ho naozaj aj človek, čo v živote nepiekol. A na rozdiel od predchádzajúceho receptu, nebudeme potrebovať žiadne polotovary.  
 
 
Čo budeme potrebovať
 
 
Na cesto: 3 hrnčeky polohrubej múky, 1 hrnček kryštálového cukru, 1 balíček kypriaceho prášku do pečiva.
 
 
Na náplň: 3/4 kila jemného tvarohu, 3 vajcia, 1 vanilínový cukor, lyžicu cukru a hrsť hrozienok namočených v rume.
 
 
K tomu ešte polovičku 125g masla. 
 
 

 
 
Do jednej misy nasypeme všetky suché prísady na prípravu cesta. Do druhej všetky prísady na prípravu náplne, okrem hrozienok.
 
 

 
 
Suché prísady poriadne premiešame vidličkou, aby sa všetko so všetkým premiešalo. Prísady na náplň zmiešame metličkou.
 
 

 
Plech vymažeme maslom.
 

 
 
A  teraz príde tá skvelá vec. Na pomaslovaný plech nasypeme polovicu suchej zmesi. Najlepšie to ide s polievkovou lyžicou, aby bola vrstva rovnomerná.
 
 
Na to opäť polievkovou lyžicou, rozvrstvíme všetku tvarohovú náplň, ktorú posypeme zmäknutými hrozienkami.
 
 
Na tvaroh s hrozienkami rozsypeme druhú polovicu múky s cukrom. Maslo nakrájame na tenké plátky a rozložíme po celom povrchu koláča. 
 
 
To je naozaj všetko, žiadne cesto, žiadne miesenie, žiadne špinavé ruky. A teraz šup s tým do trúby. 
 
 

 
 
Koláč sa pečie asi 40 - 45 minút v stredne vyhriatej rúre. Najprv sa maslo roztopí a vsiakne do múky, kde vytvorí cesto. Koláč je hotový, keď je krásne zlatkavý na celom povrchu. Pred krájaním ho musíme nechať celkom vychladnúť, aby sa jednotlivé vrstvy spojili. 
 
 

 
 
Koláče sú skvelé aj samé o sebe ale s trochou šľahačky sú celkom neodolateľné.
 
 

 
 

 
 
Dobru chuť.
 
 
 
 
 

