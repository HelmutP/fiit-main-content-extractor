

 
Nakoľko Ivan obľubuje terénnu jazdu na bicykli, pre náš kultúrno-sakrálny výlet sme odsúhlasili trasu Záhorská Bystrica - Stupava - Marianka. Veru, po jej absolvovaní som bol rád, že som nedostal defekt, hoci jej prvá polovica viedla po miestnych komunikáciách. 
Keď som sa obzrel po asi pol hodine cesty za seba uvidel som ako sa za nami postupne stráca mestská časť nášho hlavného mesta - Bratislava - Záhorská Bystrica, uvidel som takúto panorámu. 
 

	
		
			 

		
		
			
			 
			 Pohľad na bratislavskú mestskú časť Záhorská Bystrica 
			Вид района города Братислава - Загорска Быстрица 
			 
			
		
	

 
A po pravej strane zase Pajštún. 
 

	
		
			
			 
			  
			 
			
		
		
			
			 
			Zrúcaniny hradu Pajštún pri Stupave 
			Развалины града Пайштун близко города Ступава  
			 
			
		
	

 
Aj keď bolo počasie na neznesenie, veselo sme pokračovali do Stupavy, kde nás už čakala jej mestská časť Mást. Kedysi to bola časť obce Borinka, ktorá sa prvýkrát spomína v roku 1377. V 16. storočí ju začali postupne obývať chorvátski prisťahovalci, čo dokumentuje aj pamätník padlým vojakom v prvej svetovej vojne. Mimochom, v roku 1715 tu žilo 749 obyvateľov, č už je celkom slušná slovenská dedina. 
 
 
Začiatkom 18. storočia v roku 1701 tu bol postavený barokový kostol zasvätený sv. Šebastiánovi 
 

	
		
			 

		
		
			
			 
			Rímskokatolícky Kostol sv. Šebastiána v mestskej časti Stupava-Mást 
			Римокатолическая церковь св. Шебастияна в районе города Ступава-Маст 
			 
			
		
	

 
Druhá sakrálna pamiatka v meste - stupavský rímskokatolícky Kostol sv. Štefana, uhorského kráľa, - pochádza zo 14. storočia a pôvodne bol vybudovaný ako hradný kostol blízkeho zámku v Stupave. Prvá písomná zmienka o kostole pochádza z roku 1390. v 15. storočí slúžil obyvateľom ako útočisko v časoch vojen. Pôvodný vzhľad kostola sa dodnes nezachoval. V roku 1764 sa začalo s jeho rekonštrukciou. Kostol nadobudol väčší rozmer, pristavané boli aj bočné krídla a kaplnky na severnej a južnej strane. Pred kostolom sa nachádza trojičný stĺp z polovice 18. storočia. 
 

	
		
			
			 
			 
			 
			 
			 
			 
			 
			  
			 
			
			 
			 

		
		
			
			 
			 Rímskokatolícky Kostol sv. Štefana, uhorského kráľa 
			Римокатолическая церковь св. Штефана, венгерского короля 
			 
			
		
	

 
Spomínaný pamätník padlým vojakom zo Stupavy v prvej svetovej vojne sa nachádza pred kostolom. 
 

	
		
			  
			 
			 
			 
			
		
		
			
			 
			Pamätník padlým Stupavčanom v prvej svetovej vojne 
			Памятник павшим Ступавчанам в первой  мировой войне 
			 
			
		
	

 
Stupavu začiatkom 18. storočia preslávili svojou keramikou habáni, predovšetkým rod Putzlovcov, ktorí tu v roku 1714 založili dielňu. V tradícii pokračoval Ferdiš Kostka (1878 - 1951), ktorý sa stal prvým národným umelcom v Československu, a diela ktorého možno obdivovať v jeho rodnom dome v Stupave, ale aj v zbierkach múzeí v Bratislave i v zahraničí. 
 
 
Okrem budov sa v Stupave nachádza kalvária a anglický park, ktorý patrí z hľadiska výskytu vzácnej flóry k najvýznamnejším parkom Slovenska. Pôvodne bol v parku umiestnený grófsky pivovar, mlyn a niekoľko rybníkov. Nám sa ho však nepodarilo odfotografovať, nakoľko je súčasťou stupavského domova dôchodcov (bývalý kaštieľ), do ktorého je vstup cudzím osobám zakázaný. 
 
 
Zato stupavský domov dôchodcov (na fotografiách dole) zase patrí medzi najvýznamnejšie architektonické pamiatky mesta. Spoločne s anglickým parkom tvorí súčasť historickej Stupavy. Pôvodne na jeho mieste stál vodný hrad a patril ku kráľovským hradom. Pálffyovci ho však v 17. storočí prestavali na ranobarokový opevnený kaštieľ. Po tom ako sa dostal do rúk rodu Karolyiovcov dostal kaštieľ podobu romantického štýlu s rokokovými znakmi. Požiar však kaštieľ znehodnotil a následná rekonštrukcia jeho pôvodnú podobu úplne zmenila. 
 

	
		
			 

			 
			
 
		
		
			
			 
			 Bývalý stupavský kaštieľ, dnes domov dôchodcov 
			Бывший ступавский замок, сегодня дом пенсионеров  
			 
			
		
	

 
Plní zážitkov z tohto krásneho tichého mesta a posilnení pálivou paštékou, rožkami a horalkou z miestneho obchodného domu sme sa vybrali do najznámejšieho a najstaršieho pútnického miesta západného Slovenska a Uhorska - Marianky. 
 
 
Táto malá dedinka pod úpätím Malých Karpát vznikla v roku 1367, keď ju uhorský kráľ Ľudovít I. Veľký daroval kláštoru paulínov, ktorý sám založil. V 16. a 17. storočí to bolo centrum paulínov v Uhorsku. Vyvinulo sa tu pútnické miesto a prvé legendy o ňom sú známe počnúc 17. storočím. Jedna z legiend o tom hovorí nasledovné. 
 
 
	 
	Stalo sa to v roku 1330, že keď jeden slepý žobrák, ktorý sa vrúcne modlil v thálskom lese, počul hlas z neba, ktorý mu naznačil, že tam neďaleko je prameň, kde sa nachádza zázračný obraz. Ak by si vodou z tohto prameňa umyl oči, mal by hneď vidieť. Len čo si žobrák umyl oči touto vodou, otvorili sa mu oči a začal vidieť. Z prameňa vybral sošku Panny Márie, ktorá tam ležala, nechal zhotoviť drevený stĺp, postavil k nej obraz Panny Márie, často sem chodieval a pred touto sochou sa vrúcne modlil. 
	 
 

	
		
			  
			 
			 
			 
			
			 
			  
			 
			 
			 
			
		
		
			
			 
			Najstaršie pútnické miesto v Uhorsku a na Slovensku sa nachádza v obci Marianka 
			Самое старое скитальческое место в Австро-Венгрии и Словакии находится в деревне Марианка 
			 
			
		
	

 
Kolorit dedinky dotvára cisterciánsky kláštor (bývalý kaštieľ), rímskokatolícky Kostol Narodenia Panny Márie z roku 1377 a niekoľko kaplniek. 
 

	
		
			
			 
			 
			 
			 
			 
			 
			 
			  
			 
			
			 
			
			 
			 
			 
			 
		
		
			
			 
			Bývalý kaštieľ v obci Marianka 
			Бывший замок в деревне Марианка 
			 
			
			 
			
			 
			Rímskokatolícky Kostol Narodenia Panny Márie 
			Римокатолическая церковь Рождества Деви Марии 
			 
			
		
	


