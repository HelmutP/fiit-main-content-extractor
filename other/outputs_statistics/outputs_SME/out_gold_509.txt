

 
O jeho živote toho ale veľa
nevieme, dokonca je pomerne diskutované aj jeho zaradenie medzi priamych
predkov človeka múdreho (homo sapiens).
Vytvoril tzv. olduvajskú industriu, využíval jednoduché, pomerne primitívne
kamenné nástroje, ktoré si vyrábal na mieste použitia. Neovládal reč ani
nepoužíval oheň, bol to taký typický medzistupeň medzi človekom
a zvieraťom. Vyššiu úroveň dosiahol homo
erectus, náš úspešný predok, ktorý už vyrábal dômyselné kamenné nástroje.
Opracovával ich s cieľom dať im ten najvhodnejší tvar a nosil si ich
so sebou. Bol to druh, ktorý sa rozšíril z Afriky do Európy aj Ázie. Druh,
ktorému zrejme prináleží vynález reči, aj keď to so stopercentnou istotou
nebude známe nikdy, ovládnutie ohňa a mnoho mnoho ďalšieho. Vydláždil
intelektuálnu cestu pre človeka dnešného typu. A to je z pozície
hľadania odpovede na našu otázku to najdôležitejšie. Ťažko možno hádať, kedy
došlo u človeka k vývinu reči, a čo za reč už možno pokladať
a čo ešte nie. Ale je pomerne pravdepodobné, že homo erectus sa k nej minimálne na sklonku svojej existencie
dopracoval. A ovládanie reči malo ďalekosiahle následky na každého jedinca
aj na celú vtedajšiu spoločnosť. Ruka v ruke šlo s vývinom
analytického myslenia a snahou pochopiť vonkajší svet a udalosti
v ňom.
 
 
 
 
Vďaka reči a mysleniu mohol človek vzpriamený prvýkrát prežívať sny, hoci jeho úroveň vedomia zrejme nebola na takej vysokej úrovni, aby si ich mohol dlhšie pamätať
a rozprávať sa o nich. Určite každý z nás vie, aké fantastické a
iracionálne sny dokážu byť. Fantastické a iracionálne a pritom také
realistické, že ešte pár okamihov po prebudení si človek nie je na istom, čo je
realita a čo je len sen. A to sa deje v súčasnosti modernému
vzdelanému človeku, kedy sen nie je pre nikoho záhadou a veda už dávno odhalila
jeho zákutia a príčiny. A teraz si predstavte, čo taký zážitok musel
urobiť s rodiacou sa mysľou človeka vzpriameného... Boli to prvé záblesky
rodiacej sa alternatívnej reality,
ktoré tento druh prežíval a celkom iste im nerozumel a bol
z nich zmätený. Ale sny neboli jediným novým fenoménom, s ktorými sa
človek v dávnych dobách musel naučiť žiť. Boli tu tiež rôznorodé prírodné
javy od blesku, cez dážď až k Slnku a hviezdnej oblohe, s ktorými si
jeho rozvíjajúci sa rozum musel dať rady. Počas 600 miliónov rokov vývoja
života na Zemi sa žiaden makroskopický živočích nedostal do tejto neznámej
neprebádanej oblasti, nekládol si otázky prečo
sa veci dejú tak ako sa dejú a nehľadal na ne odpovede. Svet musel počkať až na
príchod rodu Homo, na príchod človeka
vzpriameného, ktorý sa začal na svet pozerať úplne inak. Tu však už zástupcu tohto
druhu trochu preceňujeme. Ťažko predpokladať, že dosiahol až takú úroveň, že sa
zaujímal o príčiny prirodzených procesov vo svojom okolí. Skôr ich iba
pasívne prijímal a rozprával sa o nich medzi druhmi z tlupy
a prípadné poznatky využíval na uľahčenie lovu a zaobstaranie jedla.
To bolo všetko, čoho bol schopný. Ale stačilo mu to na dlhú, jeden a pol milióna
rokov trvajúcu úspešnú existenciu. 
 
 
 
 

Ak sa chceme v našom bádaní
posunúť ďalej, musíme pokročiť do ďalekej budúcnosti, približne do nástupu
predposlednej doby ľadovej 190.000 rokov pred súčasnosťou, kedy Európu už
tisícročia ovládal mohutný druh človeka, homo
neanderthalensis (obr. vpravo, pravdepodobne sa tu vyvinul z potomkov homo erecta) a v Afrike sa
objavil celkom nový druh - homo sapiens.
Druh anatomicky úplne moderného človeka, akým sme aj my dnes. Tieto dva druhy
človeka zviedli súboj, ktorý je pre otázky dnešného duchovného života človeka
minimálne inšpirujúci, ak nie priamo ukážkový. Bol to súboj intelektu. Súboj
rôznych úrovní vedomia.
 
 
 
 
Územie dnešného Blízkeho východu
predstavovalo rozhranie, kde došlo k prvému stretu týchto dvoch druhov. Bolo to
na konci tejto doby ľadovej, asi 130.000 rokov pred súčasnosťou. Klíma sa
menila, teplota stúpala a homo
sapiens sa mohol šíriť cez toto územie z Afriky do celého sveta.
A aj tak urobil. Skupiny smerujúce na Európu sa tak dostali do kontaktu
s človekom neandertálskym - a muselo to byť pre nich riadne
prekvapenie. Vidieť tvora tak podobného a zároveň tak rozdielneho. Dopredu
vystupujúca tvár, ustupujúce čelo, výrazné nadočnicové oblúky, mohutné telo
a krátke končatiny, vydávajúci zvláštne zvuky, ktorým nerozumeli.
V období priaznivej klímy telesne útlejší človek rozumný pravdepodobne
zatlačil človeka neandertálskeho hlboko do európskeho vnútrozemia. Možná
fyzická prevaha neandertálcom nestačila. Človek rozumný totiž oplýval čímsi, čo
neandertálcom chýbalo. Úžasne rozvinutou mysľou, abstraktným myslením,
schopnosťou podrobne sa dorozumievať a plánovať. Mal vedomie na vyššej
úrovni. Plánoval stratégiu lovu, aj samotnú hierarchiu spoločnosti. Tá je možná
vďaka používaniu symbolov, ktoré určujú status jedinca v skupine. Vďaka
rozvrstveniu spoločnosti mohol človek rozumný energiu, ktorú neandertálci
míňali na neustále súťaženie o dominanciu v tlupe, použiť
zmysluplnejšie. To bola ohromná intelektuálna výhoda nášho druhu pred
neandertálcami. Zároveň mali naši predkovia hlbokú vieru v ríšu mŕtvych
v podsvetí, ako dokladajú bohato zdobené hroby z tohto obdobia.
Nebohý neandertálec si naproti tomu musel na onom svete vystačiť sám so sebou,
jeho druhovia mu na túto cestu nepribalili nič. Počas tisícročí, keď sa tieto
dva druhy dostávali do kontaktu, sa museli neandertálci úprimne diviť, čo za
cirkus to tí moderní ľudia pri pochovávaní druhov vystrájajú. Ich mozog také
abstraktné prejavy nezvládal vytvoriť, a preto ani pochopiť. Ich mozog
a ich vedomie, boli jednoducho iné.
 
 
 
 
 
 
Predstava človeka neandertálskeho. Bez ríše mŕtvych, duchov a bohov. Stačil si sám so sebou. 
 
 
 
 
 
 
Človek rozumný, náš predok, žijúci pred desiatkami tisícročí. Nad svojim konkurentom zvíťazil zrejme aj vďaka svojmu mozgu a vedomiu, ktoré vytváral. Tento mozog však prinesie ešte veľa prekvapení do jeho života a spoločenstva... Otvorí pred ním fascinujúcu alternatívnu realitu. 
(primates.com) 
 
 
Spolu s ríšou mŕtvych zrejme človek rozumný uctieval aj rôzne
božstvá sídliace kdesi „hore", odkiaľ svieti Slnko a padá blahodárny
dážď. Ako neskôr uvidíme, tieto silno iracionálne prvky v ich/našom
myslení sú neodbytnou súčasťou ich/našich myslí, sú napevno inštalované
v našich mozgoch. Aby sme nešteklili zástancov kreacionizmu, napíšme
radšej - vyvinuli sa v našich mozgoch, ale v mozgoch iných druhov už
nie. Naše mozgy nepredstavujú nejaký vytúžený cieľ, konečnú zastávku na ceste
k dokonalosti. Sú aj inak usporiadané mozgy, iné mysle a môžu byť
celkom úspešné, ako to ukázali neandertálci, hoci schopnosť abstraktného
analytického myslenia je určite obrovská výhoda. Daňou za tieto schopnosti sú
iracionálne prvky v našom vedomí a myslení. Avšak neznamená to, že
neandertálci boli v niečom horši ako náš druh, neboli menejcenní. Zreteľne to ukázali na konci tejto doby medziľadovej,
asi pred 80.000 rokmi, kedy sa človek rozumný nedokázal so svojím
intelektom, hierarchickou spoločnosťou ani intenzívnym vzývaním bohov
vyrovnať so zhoršujúcim sa podnebím a skupiny človeka neandertálskeho ho
vytlačili späť až do Afriky. Blízky východ sa stal opäť doménou neandertálcov.
Tentokrát ale naposledy. Z dnes ešte neznámych príčin sa ešte počas
zaľadnenia kontinentálnej Eurázie homo
sapiens vrátil a neandertálci definitívne zmizli z dejín. Nemuselo
to ale nesporne znamenať výhru intelektu a abstraktného myslenia nad
neandertálcami, neveriacimi v bohov a duchov. Podmienky totiž tieto druhy
nemali rovnaké. Kým náš druh prežíval v pomerne príjemnej klíme,
v zmrznutej Európe z nejakého dôvodu (táto doba ľadová mohla byť
napríklad o niečo krutejšia) bolo málo potravy a zdecimovaní
neandertálci neprežili boj s dobre zabezpečenými ľuďmi nášho druhu. Toto
obdobie spadá  70.000 až 40.000 rokov do
minulosti. Vyhynutie človeka neandertálskeho sa odhaduje do obdobia približne
35000 rokov pred súčasnosťou, ešte v čase pevného ľadového zovretia
Európy.
 
 
 
Človek rozumný disponoval takým
istým mozgom, ako máme my, ovládal zložitú reč, vedel analyticky myslieť
a predstavovať si abstraktné javy a navzájom si ich medzi členmi komunity
opisovať. Jediný rozdiel medzi ním a nami sú desiatky tisíc rokov
technického rozvoja. Ten bol možný práve vďaka jeho mozgu, ktorý mu umožňoval toto abstraktné
myslenie a presadenie sa v boji o prežitie. Avšak tento zložitý
mozog nebol len nástrojom pokroku a rozvoja, ale priniesol nášmu druhu
rôzne „horúce chvíľky“. Spolu s dobre známou racionálnou stránkou sa
rozvíjala aj jeho podvedomá, iracionálna podoba, ako jeho nedeliteľná súčasť. Jeho komplexnosť
a schopnosti pokročili natoľko, že si s nimi dlhé tisícročia sa
vyvíjajúca kultúra druhov rodu Homo
svojím spôsobom nevedela dať rady. Posun nastal až v prítomnosti.
O čom tu hovoríme?
 
 
Už sme spomínali, že
pravdepodobne už homo erectus ako
prvý zástupca človeka používal akú-takú reč a prežíval
sny. Sny však nie sú výsadou človeka, pravdepodobne ich prežívajú aj ostatné cicavce. Od človeka ich ale delí fakt, že si ich nedokážu zapamätať, na ich základe konať, rozhodovať sa, v dlhšom časovom horizonte nad nimi uvažovať. Sny, teda náhodné procesy neurónov v spánku,  u človeka nepredstavujú len nejaké
zvláštne premýšľanie počas spánku, ktoré si pri troche šťastia zapamätáme. Sú
oknom do mysle človeka, do jeho vedomia. Samotný spánok predstavuje
v podstate iný stav vedomia, ako je ten známy z bdenia. V knihe
Myseľ v jaskyni je proces prechodu medzi bdelým vedomím a spánkom
opísaný takto:
 
 
„V bdelom stave vedomia sa zaoberáme riešením
problémov, obvykle ako reakciou na vonkajšie podnety. Keď sa od týchto podnetov
odpútame, začnú prevládať iné druhy vedomia. Najprv sa snažíme riešiť problémy
v štádiu realistickej fantázie. ... Táto realistická fantázia postupne
prechádza k fantáziám skôr autistickým, ktoré berú menší ohľad na
skutočnosť. V stave nazývanom reveria naše myslenie stále viac stráca smer
a jeden mentálny obraz (predstava) nestrieda druhú v naratívnom slede.
Postupne nasleduje zaspávanie, ktorému môže predchádzať hypnagogické stavy
(hypnagogické halucinácie). Konečné štádium snívania pozostáva zo sledu
naratívnych obrazov. Prinajmenšom sa nám javia ako príbehy, aj keď je pravdou,
že veľa si k ním pridávame. Behom fázy známej ako REM, ktorá predchádza
hlbokému spánku, náhodná činnosť neurónov v mozgu vytvára mentálne obrazy,
ktoré, ako všetci vieme, sú veľmi bizarné: jeden za mení v druhý, môžeme
padať, lietať, utekať so všetkými sprevádzajúcimi pocitmi.“ (Lewis-Williams,
D.: The mind in the cave).
 
 
 
 
 
 
 
 
 
Schopnosť abstraktne myslieť, rozprávať artikulovanou rečou a snívať počas spánku, otvorila našim priamym predkom úplne nový svet. Každá noc bola zároveň vstupom do akéhosi zvláštneho alternatívneho sveta, plného nečakaných dobrodružstiev. Dokonca sa tu dali stretnúť mŕtvi druhovia, ktorí už boli mesiace pochovaní. Dalo sa ich dotknúť, zhovárať sa s nimi. Boli to zážitky priamo podmienené vnútorným usporiadaním neurónov ich mozgov. Tieto zážitky náš druh aj všetko s nami späté natrvalo poznačili. Prví zástupcovia nášho druhu na Zemi neboli na niečo také pripravení a nerozumeli, čo to má znamenať. A ako už celkom moderní ľudia, začali hľadať vysvetlenie týchto javov, ktorým dennodenne čelili. Ich vysvetlenia sa samozrejme nemohli opierať o poznatky aké máme my dnes. Preto neprekvapuje, že tieto snové ilúzie pokladali za zvláštny prejav iného, ale reálneho sveta - sveta duchov mŕtvych, neskôr bohov a mnoho ďalšieho, neznámeho. Ale to už trochu predbieham. Samotné sny sotva mohli stačiť na to, aby človek podľahol predstavám o inom svete, ktorý možno navštíviť len v noci, kedy sa nijako fyzicky nehýbe. Lenže "dôkazy" o jeho skutočnej existencii prišli aj z inej strany. A to už nikto v dobe kamennej nezostal na pochybách...
 
 
 
 
 
 
(pokračovanie 12.3.2008...09.00)
 
 
 
 
 
 
 

