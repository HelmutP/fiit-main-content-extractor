
 Pán PhDr. Michal Horský mali by ste vrátiť diplom magistra, archivára. Ako historik by ste totiž mali vedieť, kam podľa základov politológie vedie potieranie funkcií aj malých strán v demokracii. Osviežim vám trochu pamäť zastretú viac než platonickou láskou k Ivete Radičovej. 

Aj v čase Slovenského štátu okrem dvoch menšinových strán existovala jediná všetko rozhodujúca strana, nikdy neboli slobodné voľby. Aj ústredný výbor strany bol len poradným orgánom Vodcu. Na miesta volených orgánov boli osoby menované. 

Dr. Jozef Tiso hovoril: " Strana je národ a národ je strana. Národ prostredníctvom strany hovorí, strana miesto národa myslí. Čo národu škodí, to strana zakazuje a pranýruje."    
(Organizačné zvesti HSĽS 1943/3, podľa NAŠE STANOVISKO 1997, s. 24) 

Pán Horský mali by ste vedieť čosi aj o nevyhnutnosti politických inštitúcií pre demokraciu: akými sú funkcie volených činiteľov, slobodných, spravodlivých a častých volieb nevyhnutných pre rovnosť pri hlasovaní, čosi by vám mala hovoriť aj sloboda prejavu aj malých strán, prístupe k alternatívnym zdrojom informácií pre občanov, prečítajte si aspoň letmo čosi o slobode zhromažďovania a združovania a občianskej spoločnosti... a o vedúcej úlohe strany za totality u nás ste iste počuli.

Pán Horský prečítajte si šlabikárové princípy demokracie, a zamyslite sa, čo je to vlastne tá demokracia? Hovorí sa v nich aj o tom, že „každá politická väčšina je dočasná a žiadna väčšina nepodnikne kroky, ktoré by likvidovali opozičnú menšinu. Účelom vlády sa nesmie stať zabráneniu opozícii podieľať sa na akomkoľvek budúcom vládnutí.“

Pán Horský mali by ste vedieť a možno ste o tom aj čo-to počuli, že malé strany, ako je na Slovensku aj strana Slobodné Fórum, má v súčasnosti význam. Ten zákonite vzrastá najmä vtedy, keď sa rozdiel medzi opozíciou a koalíciou stáva zanedbateľným. Tým by mohla mať aj takáto strana značnú možnosť, na svoje pomery, určovať fungovanie vývoja ďalšieho politického systému. A to vám zjavne nevyhovuje.

Z toho ste pán Horský hysterický vy - a vaša kandidátka. Aj vy, aj ona dennodenne „vykrikujete“ svoje tzv. pravdy o malých šanciach strany Slobodné Fóru a jej kandidátky na prezidentku. Ste čoraz ustráchanejší z rastu preferencií Zuzany a jej strany.

Pán Horský, vráťte diplom, vzdajte sa titulu „politológ“ a nastúpte do volebného tímu tej „pravej pravice“. Nebudete tak robiť hanbu ostatným slovenským politilógom, čo majú aspoň zápočty zo základov politológie a princípov demokracie. 
 
