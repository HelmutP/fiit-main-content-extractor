

      Možno poznáte príbeh o niekoľkých zelených žabkách, ktoré sa zúčastnili pretekov v lezení na vysokú vežu. Hneď po štarte na nich začali pokrikovať diváci vety typu : „ To nezvládnete !“  „ To je nemožné vyliezť na takú vysokú vežu !“  „ Nemáte šancu to zvládnuť !“  a podobne.  Ako žabky liezli na vežu, postupne jedna po druhej padali na zem a vzdávali sa ďalšieho boja. Jedna žabka však bola vpredu s obrovským nádskokom. 
  
      Diváci sa čudovali,že jej to tak dobre ide a ešte viac na ňu kričali, že to nezvládne.  
 No žabka ďalej pokračovala v lezení a prekonávala prekážku za prekážkou. Nakoniec ostala na veži ako jediná a dostala sa na úplný vrchol. Všetci sa divili, ako je to možné,že to s takou ľahkosťou zvládla. Vypytovali sa jej na tajomstvo jej úspechu, no ona neodpovedala. Táto žabka, ktorá napriek zlým okolitým vplyvom preteky vyhrala bola totiš hluchá . 
       
      Každý z nás má nejaký cieľ. No väčšina z nás sa ho vzdá po prvom zlyhaní, alebo sa ani nepokúsi o jeho dosiahnutie. Vo väčšine prípadov nás odrádzajú reči ľudí, ktorí tak isto zlyhali a vytvorili si to známe motto : 
  „ To sa nedá“   
       
      Skúsme si preto zobrať príklad od žabky z príbehu, ktorá nepočula tie hlúpe reči divákov a s ľahkosťou vyliezla na vysokú vežu a vyhrala preteky. Nepoddávajme sa rečiam neúspešných ľudí, ktorí už neveria sami v seba. Radšej sa poučme z ich zlyhania a lezme ďalej za svojim cieľom až na vrchol.  

