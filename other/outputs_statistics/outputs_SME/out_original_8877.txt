
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Rokujeme, ponúkame! 

        
            
                                    15.6.2010
            o
            14:19
                        (upravené
                15.6.2010
                o
                14:26)
                        |
            Karma článku:
                7.68
            |
            Prečítané 
            913-krát
                    
         
     
         
             

                 
                      Dnešok bol aj medzi radovými občanmi celkom hektický. Už je jasné, kto je s výsledkami volieb spokojný a kto ich nechce akceptovať ani za svet, lebo ktohovie, čo to bude znamenať v praxi a presne v takomto rozpoložení som sa dostal do prvého verejného sporu s rovesníkom, čo patril voľakedy k nomenklatúrnym kádrom okresu a keď som sa do Prešova presťahoval ja, teda po zamatovej revolúcii, už ho s veľkou dávkou fantázie a tak trošku aj odvahy zašívali na pracovných pozíciách celkom nevysvetliteľnej opodstatnenosti. To viete, čas pred odchodom do penzie je zložitá doba a kamarátov bolo ešte vždy dostatok. Pred voľbami sme sa stavili, kto vyhrá a predstavte si, že na tom našom námestíčku pred veľkým marketom a lekárňou sme si to vysvetľovali. Spočiatku pokojne, potom s ohľadom na temperament hlasnejšie a napokon nás bolo počuť poriadne ďaleko. Okrem magistry z lekárne nás tam z ničoho nič bolo veľa!
                 

                     Argument s 35% získaných hlasov SMER-u som zmietol z reálneho sveta počtom mandátov pravicových strán a ich dohodou, čo napokon môj protivník akceptoval, ale jeho protiútok mal byť ničivý – vraj sú aj proti mne, ani mne nedajú vianočný dôchodok, či aspoň to prilepšenie a Fico by vraj dal! Sľúbil to! Moja argumentácia bola síce jasná, no naozaj skeptickejšia, lebo som sa ho spýtal odkiaľ na to vziať, keď je sociálna poisťovňa v dlhoch, prakticky insolventná a táto vláda sa správa nezodpovedne, ako tá grécka a kto to chápe, ten ani nič nečaká , keď mi pomohla naša lekárnička. Osôbka komunikatívna, s dôverou u občanov a hoci ma ešte včera informovala o tom, že veľa kupcov u nej viac nadáva na výsledok volieb než ich chváli, tentoraz nás ohromila správou, čo sa rozšírila z verejných médií o novom ťahu Roberta Fica. Vraj dáva KDH post premiéra, polovicu ministerských postov podľa výberu a ešte aj peniaze od finančníkov, čo už tu poletovalo ako sladká pasca na lakomých už odvčera. Teraz to počula na vlastné uši!   A bolo po škriepke, môj rival zhasol ako sviečka vo vetre, lebo doteraz môj argument o využívaní finančnej loby v tejto strane a pri realizácii jej záujmov tvrdohlavo odmietal, no takáto novinka ho celkom zničila. No koniec, skonštatoval, ak je to pravda, tak potom tomu rozumiem presne tak, ako to vysvetľuješ, sú v tom prachy pre všetkých, je to kšeft a do jeho záchrany treba obetovať naozaj všetko! Ospravedlňujem sa, prepáčte a bol koniec tohto celkom zvláštneho predstavenia.   To viete, doma som okamžite hľadal, počúval a sledoval všetko v tom naozaj širokom mediálnom rozmere a fakt, znelo to odvšadiaľ! Fico dáva, sľubuje, nič nevzdal, naopak, využije všetok čas, ktorý mu Gašparovič ponúkol, aby dosiahol svoj cieľ. Vybral si takticky správne, veď ani nie tak dávno nebyť Radičovej otvorene položenej podmienke by Figeľ ešte v mnohom váhal a buďte spravodlivý, takáto ponuka poriadne zamáva aj inými osobnosťami, než je Figeľ a ostatní členovia KDH na ktorých by to malo dosah! Taký mladý politológ  sa dokonca neváhal rozohniť, veci prežíval ako pokušenie ktoréhokoľvek svätého, čo s vlastným svedomím musel bojovať na život a na smrť, a potom už mi bol aj smiešny. Veď to je iba fantasmagória, aj Figeľ, aj Radičová, Sulík a aj Bugár svoje rozhodnutie nerokovať o zostavení vlády s Ficom predniesli verejne, pred celým národom, dali odkaz a som si istý, že žiadne peniaze sveta a ani žiadna iná výhoda či mimoriadny bonus na tom nezmení nič!   Opak je pravda! Ak sa o to Fico naozaj usiluje, ak takýto pokus realizoval, celému Slovensku prezentoval podstatu svojho vládnutia! Je v tom neuveriteľný balík peňazí a výhod, úžasný kšeft a stojí za to vykonať čokoľvek, aby sa to za nič na svete neskončilo! Naozaj všetko, od osobnej cti až po prestíž, lebo balík peňazí je jednoducho nad všetkým! Veď je to zatiaľ anonymné, nikto to o nás nevie, tak vydržať a nevzdávať sa!   Ten čas, čo prezident Gašparovič dal premiérovi Ficovi na pokus o zázrak sa pre mnohých spoluobčanov stane dobou neistoty, lebo človek je naozaj tvor krehký a omylný a odolávať pokušenie, to je naozaj veľmi dôležitá cnosť. Ale hovorí sa aj ináč, kto verí a dúfa, ten sa dočká, takže vydržať! Už to tu znelo viackrát, je to pekné zaklínadlo a dodáva to silu, takže to pripomeniem ešte raz. Vydržať, ten čas príde a naviac, poučenie dostanú aj tí, čo verili účelovosti a klamstvu. Byť pri moci to nie je len zodpovednosť, ale veru aj možnosť nabaliť sa a v mene takejto možnosti sa robí naozaj všetko! Slováci, na to sa nesmie zabúdať.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




