
 Keď som chodil na strednú školu, prežil som veľmi pekný vzťah so svojou priateľkou. Prežil som veľmi krásne obdobie zaľúbenia, vzájomného spoznávania a lásky. Spoločný vzťah sme si budovaliu viac ako jeden a pol roka až do chvíle, keď som sa rozhodol odpovedať na duchovné povolanie a stal som sa kňazom.
Počas tohto obdobia som sa veľa zamýšľal nad tým, vďaka čomu sa nám podarilo vybudovať taký pekný vzťah. Bolo to tým, že náš vzťah sme prežívali spolu s Bohom, ale aj tým, že sme sa o všetkom dokázali otvorene porozprávať a to najmä vtedy, keď sme v našom vzťahu prežívali krízu.
Na druhej strane som premýšľal, prečo sa vtedy mojim priateľom niečo tak pekné a obohacujúce nepodarilo vytvoriť. Už vtedy som zistil, že väčšina zaľúbení a lások sa končila väčšinou po pol roku, prípadne po roku. Akú úlohu v budovaní vzťahu má čas? Prečo to niektorým vyjde a niektorým sa vzťahy rozpadávajú ako na bežiacom páse?
Keď som neskôr študoval psychológiu, našiel som odpovede na veľa svojich otázok a pochopil som veľa zo zákonitostí vzájomného vzťahu dvoch mladých ľudí. Chcel by som sa so skúsenosťami podeliť so všetkými, ktorí sa zaľúbili a majú zrazu pocit, že v ich vzťahu niečo nie je v poriadku a nedokážu pochopiť prečo.
Určite im neposkytnem odpoveď na všetky otázky, ale možno sa aj vďaka týmto zopár riadkom dokážu na problém pozrieť aj z druhej strany. Ak to už aj nezachráni rozpadajúci sa vzťah, verím, že možno ten budúci bude lepší. Ak pomôžem čo len jednému človeku, neťukal som do počítača tieto riadky nadarmo.

Vzťah je cesta cez púšť...

Je to cesta cez púšť s niekým, koho mám rád, je to vždy cesta z jednej oázy do ďalšej - väčšej a krajšej. Smutné na tejto pravde by mohlo byť azda iba to, že do ďalšej oázy je potrebné prejsť cez nehostinnú púšť.
S tým sa však už nedá nič urobiť, na púšti to už tak chodí. Každá cesta cez púšť je spojená s rizikom, je to cesta iba pre odvážnych a pripravených. Každý vzťah, ktorý sa má rozvinúť, musí prejsť určitú cestu. Zastať na mieste  a to najmä medzi oázami, vždy značí najskôr vyčerpanie zásob vody a jedla a potom prichádza koniec. V prípade cestovateľov po púšti je to koniec života, v prípade cestovateľov v láske je to koniec vzťahu.
Je potrebné dobre sa pripraviť a vziať si na cestu ťavy, ktoré sú dobre "natehlované". Neviete, čo je to natehlovaná ťava? Tento výraz je z jedného vtipu:

Skupina turistov sa chystá na cestu cez púšť. Táto cesta im má trvať tri týždne. Idú preto do požičovne tiav a objednajú si ťavy. Vedúci požičovne im hovorí:
- Tieto ťavy vydržia obvykle dva týždne bez vody, ak chcete, aby vám vydržali tri týždne, musíte ich dobre napojiť a aj natehlovať.
Turisti nevedia, čo je to natehlovať a keďže sa hanbia opýtať, vydajú sa na cestu. Po dvoch týždňoch im ťavy na ceste podochnú a oni si horko - ťažko zachránia holé životy. Vrátia sa do požičovne tiav a idú ich reklamovať:
- Ako je možné, že vydržali iba dva týždne, keď mali vydržať tri?
- A natehlovali ste ich?
- Keď my nevieme, čo je to natehlovať...
- To sa robí takto. Napájate ťavu, a keď vidíte, že už prestáva piť, zoberite dve tehly a z obidvoch strán ju tresnete po hlave. Ťava sa tak zľakne, že od strachu urobí hup-hup-hup, nasaje viac vody, a takto natehlovaná vydrží aj tri týždne.

Keby sa turisti nehanbili opýtať, boli by so správne natehlovanými ťavami prešli celou púšťou až do najkrajšej oázy na druhej strane. Vo vzťahu s druhým človekom je tiež potrebné prejsť cez tri oázy. Tú prvú, oázu zaľúbených, som nazval oázou ružových okuliarov. O nej a o prvej kríze v budúcom článku... (to be continued) :) 
