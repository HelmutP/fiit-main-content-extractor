

   
   Mám psa,ktorý už mal byť na druhom svete.Chradol,slepol,hluchol,proste odchádzal zo života. 
   Po odchode do dôchodku som s ním viac začala komunikovať,ubezpečila som ho,že len na chvíľu si odskočím,zatiaľ môže pokojne spať.Jedným slovom, stále som ho ubezpečovala ,že s ním rátam,je súčasťou života.Viditeľne sa zotavil,bude mať 15 rokov,dobre vyzerá,dúfam ,že sa bude zabávať s mojím prvým vnúčaťom. 
   Poviete si ,je tam toho,nejaký ratlík.Pokojne mi pridáte tisíce takých osudov psíkov,rozmaznávaných unudenou paničkou. 
   Ja som si ale uvedomila,že tak isto možno,aj ked je to na pohľad urážlivé,skladať pozornosti,ohľaduplnosti,zaradovanie do aktivít ,pomôcť človeku. 
   Ak vidím potulovať sa bezcielne bezdomovcov,starých ,odložených ľudí v bohatých penziónoch,vidím obraz svojho psíka,ked mal vždy čo jesť a nejedol,mal teplý brloh a nespal. 
   A stačí tak málo.Poskladať mozaiku bývalých dobier,ktoré v minulosti hýbali životmi týchto nepotrebných. 
   Zas si poviete,ako mohli takto skončiť,mohli predsa aspoň niekedy vo svojich dobrých časoch myslieť na budúce aktivity,aby ich držali pri živote... 
   Vrátim sa ku psíkovi.Po celý čas jeho návratu do života som používala láskavosť,pozornosť ,zdôrazňovala som jeho užitočnosť ako strážcu,skladala som do pomyselnej mozaiky pozitívne ,zdôrazňujem, len pozitívne pohnútky,aby som ho prebrala k životu.Podarilo sa.  
   
   

