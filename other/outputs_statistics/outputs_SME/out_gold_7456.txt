

   
   
   
 Človek radšej skúma vesmír 
 než sám seba... (Hemingway) 
 ***  
 Výrok je pravdivý 
 som o tom presvedčená 
 je ľahšie na druhých 
 hľadať chyby 
 iným vytknúť vinu 
 skritizovať činy 
 (nepriznať kus vlastnej viny) 
 Zametať pred vlastným prahom 
 činnosť je náročná 
 ťažké je 
 byť prísny k svojej maličkosti 
 premôcť zlobu 
 čo pochádza zo slabosti 
 radšej neskoro ako nikdy 
 uvedomiť si 
 že človek je 
 vlastne tým 
 za čo sa hanbí 
 ***  
   
 T. Baťa: Nebojme sa ľudí, bojme sa seba... 
 Lamartine: Utópie sú často iba predčasné pravdy. 
 J. McCain: Človek sa nemôže sám pred sebou ukrývať po celý život.  
   
   

