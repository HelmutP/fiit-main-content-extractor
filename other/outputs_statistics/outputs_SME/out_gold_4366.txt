

 Každý verejný prejav, či už ústny alebo písaný, by mal byť z obsahového, formálneho i jazykového hľadiska na primeranej úrovni. Platí to aj o textoch publikovaných na internete. Každé dielo vypovedá o svojom pôvodcovi... 
 Prvým predpokladom správnej komunikácie je zrozumiteľnosť. Náš rečový prejav by mal byť preto jasný a skôr stručný ako rozvláčny. Z vlastnej skúsenosti môžem povedať, že pri písaní článku je lepšie dať svoje slová najprv na papier a až potom ich prepísať do počítača. 
   

