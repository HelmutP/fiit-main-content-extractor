
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Písal program, kandidoval za SaS. Pre STV nezávislý politológ. 

        
            
                                    22.2.2010
            o
            18:40
                        (upravené
                23.2.2010
                o
                12:28)
                        |
            Karma článku:
                12.64
            |
            Prečítané 
            21959-krát
                    
         
     
         
             

                 
                    STV pojem konflikt záujmov naozaj nič nehovorí. (Aktualizované o reakciu SaS)
                 

                 
Župný kandidát SaS komentuje v STV počas kampane župné voľby ako politológ.reprofoto stv.sk
   Minulý utorok STV v príspevku analyzovala prieskum volebnej mienky, kde k najväčším zmenám došlo pri preferenciách pre SDKÚ a SaS. Magda Želeňáková nechala práve tieto dve strany komentovať politológovi Univerzity Mateja Bela Martinovi Klusovi. Včera vo svojej hlavnej diskusnej relácii STV túto reportáž aj Klusove vyjadrenia zreprízovala.   STV má na výber politológov tradične veľmi dobrý nos, a tak neprekvapí, že aj spomínaný Martin Klus je v konflikte záujmov, o ktorom STV divákom nič nepovedala. Je spoluautorom dlhopripravovaného programu SaS, alebo ako to nazvala SaS, tímlídrom pre oblasť vnútornej bezpečnosti.            Za posledné štyri mesiace použila Klusa ako nezávislého politológa na hodnotenie domácej politiky STV sedemkrát, väčšinou vo forme rozhovorov pre večernú reláciu Správy a komentáre. Famóznym bol aj výber Klusa pre rozhovor o prvom kole župných volieb pár hodín pred koncom kampane v novembri minulého roka:   Maroš STANO, moderátor -------------------- Už vo štvrtok o siedmej hodine predpoludním, teda pozajtra, sa končí predvolebná kampaň. Aj o jej úrovni a pôsobení sa budeme rozprávať s politológom Martinom Klusom. Dobrý večer do Banskej Bystrice.  Martin KLUS, politológ -------------------- Dobrý večer prajem.  Maroš STANO, moderátor -------------------- Ak sa pozrieme na úroveň kampane, oslovila podľa vás voličov, aby mohla zvýšiť účasť, aby výber poslancov a županov bol lepším odrazom priazne voličov?  Martin KLUS, politológ -------------------- No treba povedať úprimne, že táto kampaň nemala mimoriadne mobilizačný charakter. Skôr zostala v takej rovine tých tradičných predvolebných sľubov jednotlivých politikov a politických strán tak, ako sme na to boli zvyknutí aj v predchádzajúcich voľbách do vyšších územných celkov.    Maroš STANO, moderátor -------------------- Vy ste postrehli aj nejaké vzdelávacie momenty, vysvetlenia, prečo ísť voliť alebo kampaň vo všeobecnosti sa niesla skôr v znamení tých sľubov jednotlivých kandidátov? Teda hovorili ste, že nie je edukatívna, ale predsa len, skúste.  Martin KLUS, politológ -------------------- V minimálnej miere. Samozrejme, zvyčajne keď, tak na internete, ale pokiaľ mám možnosť sledovať bilbordy alebo niektoré iné propagačné materiály jednotlivých kandidátov, tak sa na účasť zameriavajú skutočne len veľmi triezvo.   atď...   Čo sa diváci od moderátora - či samotného politológa - nedozvedeli, že sám Klus v tom čase kandidoval do župného parlamentu v Banskej Bystrica ako kandidát za SaS! (zvolený nakoniec nebol)  Klus mi napísal, že napriek kandidatúre a (bezplatnej) práci pre SaS sa cíti ako politológ nestranný:   Obe tieto ponuky [kandidovať do VÚC a tvoriť program strany], som... považoval za príležitosť získať skúsenosti zo spojenia akademickej teórie a politickej praxe. Zásadnou podmienkou takejto spolupráce pritom z mojej strany od samého začiatku bolo, že sa nestanem členom strany a že budem môcť naďalej v rámci akademickej obce, ale aj masmédií vystupovať plne nezávislé, bez odkazu na moje odborné aktivity v strane.   Tak neviem, či naozaj chceme, aby nám v médiách vystupovali takíto "nestranní" politológovia.   Update: Reakcia SaS (zdôraznenie boldom je moje):   Martin Klus nie je členom strany Sloboda a Solidarita (SaS), avšak pôsobí na poste tímlídra pre vnútornú bezpečnosť. Nie je kandidátom SaS na poslanca NR SR, ale je pravda, že za stranu kandidoval vo voľbách do VÚC.  SaS nemala informácie o vystupovaní M. Klusa v spravodajských a publicistických reláciách STV v úlohe nezávislého politológa. "Určite nie je správne, ak sa človek spätý s konkrétnou politickou stranou vyjadruje ako nezávislý odborník, " povedal predseda SaS Richard Sulík.  Po vzájomnej dohode M. Klus už nebude zastávať post tímlídra SaS pre vnútornú bezpečnosť.    PS Pokiaľ ide o spracovanie programu SaS v médiách, bol som sklamaný. Markíza ešte výslovne povie, že chcú postupne predstavovať programy všetkých strán. A potom reportérka Martina Dratvová minie takmer tretinu príspevku na informáciu, že zostavovateľ programu má ako koníčka šamanizmus. Mária Miháliková z Pravdy sa zase sústredila na dekriminalizáciu marihuany (titulok Sulíkovci vábia voličov na dym z marihuany), hoci na spoločnosť ďaleko väčší dopad by malo zavedenie odvodového bonusu.   Ak chceme aby strany naozaj pracovali na svojom programe a aby sa voliči podľa programov čo najviac aj rozhodovali, tak prístup informovania Markízy či Pravdy v prípade SaS je s tým presne v protiklade – voličov láka na emócie, nie rozumový pohľad na strany.       Nové diaľnice:  Počas premiérových PR aktivitách po ministerstvách (samé pochvaly, čo všetko sa dosiahlo atď.) tradične poslušne robila nadprácu STV. Pri téme diaľnic sa však zmýlila aj TA3.   STV, 148 kilometrov nových ciest, Marek Zgalinovič V programovom vyhlásení sa vláda zaviazala postaviť 100 kilometrov nových ciest, napokon je to o 48 kilometrov viac.   TA3, Za diaľnice zaplatíme viac, Daniel Horňák Rezort dopravy naplnil programové vyhlásenie vlády... Za 4 roky postavil takmer 150 Km rýchlostných ciest na diaľníc.    Tých 150km znamená počet otvorených úsekov, nie postavených. Ako Pravda výborne spočítala koncom minulého roka, 65km z daného počtu už bolo prevažne postavených za minulej vlády, a ďalších 40km čiastočne rozostavaných. Úplne novo postavených je len zhruba 40km.    STV aj TA3 tak určite bežného diváka uviedli do omylu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (135)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




