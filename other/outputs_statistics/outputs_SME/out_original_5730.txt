
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rado Tomek
                                        &gt;
                Nezaradené
                     
                 Nu Spirit of talkshow so Šarkanom a Tomášom Hudákom 

        
            
                                    3.5.2010
            o
            13:15
                        |
            Karma článku:
                4.58
            |
            Prečítané 
            1572-krát
                    
         
     
         
             

                 
                    Talkshow. Prvýkrát som som sa s týmto žánrom stretol v polovici 90. rokov počas môjho pobytu v New Yorku. Vždy som obdivoval ako hostiteľ s ľahosťou dokáže viesť vtipnú debatu na rôzne témy s rôznymi hosťami. Dnes viem, že je to jeden z najťažších zábavných formátov, kde treba skĺbiť dôslednú prípravu so schopnosťou pohotovo reagovať a improvizovať.  Preto som veľmi rád, že v našom Nu Spirit Clube sa svoju talkshow rozhodli robiť hneď dvaja hostitelia: Peter Novák alias Šarkan a smečkársky Tomáš Hudák.
                 

                 Prvého z nich môžete na vlastné oči vidieť už dnes. Zoznam Šarkanových hostí zahŕňa snáď všetky sféry slovenského života: biznis, šport, kultúru. Na doterajších dvoch offlinoch sa miešal (a popíjal) špeciálny drink, ktorý pripravil svetovo uznávaný slovenský barman Stan Vadrna, diskutovalo o hokeji s trénerom Jánom Filcom a k tomu všetkému spieval Martin Chodúr (a minulý mesiac aj Adela Banášová).    Dnes si to Šarkan rozdá s úspešným profesionálnym boxerom Tomim KID Kovácsom, ktorého neskôr na gauči vystrieda Rišo Rybníček, riaditeľ štátnej televízie v časoch, keď sa ešte dala pozerať.  Hudobný svet bude reprezentovať saxofonista Radovan Tariška.   Tomáš Hudák patrí medzi kľúčových ľudí zábavy na webe Sme, a jeho pravidelný Svet Tomáša Hudáka patrí medzi najsledovanejšie videá na stránke. Tomáš však zatúžil po priamom kontakte s publikom. A tak zabudnite na Markoviča a spol. Nová éra politickej talkshow sa volá Bez Potlesku. V jej poslednom vydaní som sa napríklad dozvedel, že známy aktivista Alojz Hlina začal svoju kariéru ako vykupovač dobytka na oravskom vidieku.      Obom protagonistom našich talkshows strašne držím palce. A to nielen preto, že nám ponúkajú možnosť naplniť klub v inak neatraktívny pondelok. Som rád, že talentovaní ľudia, ktorí na rozdiel od takého Jay Lena nemajú k dispozícií celý štáb asistentov a scenáristov, majú priestor na realizáciu svojej vízie.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Nebojte sa japonskej scény alebo pozvánka na stredu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Prečo sa teším na Hot 8 Brass Band (alebo Nebojte sa dychovky)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Ako skoncovať so strachom z hip hopu: Lekcia 2, Dámy za mikrofónom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rado Tomek
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rado Tomek
            
         
        tomek.blog.sme.sk (rss)
         
                                     
     
        som ekonomicky novinar a okrem toho aj DJ Kinet, promoter, majitel bratislavskeho Nu Spirit Baru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    102
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1408
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Nebojte sa japonskej scény alebo pozvánka na stredu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




