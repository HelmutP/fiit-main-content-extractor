

 Tu si cucnú, tam si cucnú, ale sem tam ich aj plesnú a zabijú, či vtáky ich zhltnú za letu, lebo letia rýchlosťou iba jeden, nanajvýš dva kilometre za hodinu. Tí ktorí to prežijú,  denne spravia aj 10km. Pri nedávnej prechádzke okolo jazera ich bolo také množstvo, že cez ne pomaly nebolo vidieť. 
   
 Najväčšiu radosť z toho mali vtáky a pavúky. Bicyklisti vzhľadom na ich výskyt necerili zuby a rybári si dali aj klobúky. 
   
 Vždy, keď sme si komáre chceli odfotiť, ony tie potvory sa stiahli. Fotoaparát sa stal účinným prostriedkom na ich odpudenie. Pár fotiek sa nám predsa len podarilo. Našu najväčšiu radosť vyvolal pohľad na pavučinu, kde tých smradov bolo pochytaných neúrekom.  V tej chvíli som pochopil text pesničky od O.B.D. : ...sú to komáre, nikto ich nemá rád... 

