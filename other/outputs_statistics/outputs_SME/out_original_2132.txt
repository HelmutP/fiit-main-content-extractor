
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 TA3 sa skrytej reklamy nechce vzdať 

        
            
                                    26.1.2010
            o
            18:09
                        |
            Karma článku:
                13.17
            |
            Prečítané 
            21305-krát
                    
         
     
         
             

                 
                    Niečo o televízii prezrádza aj výber prezentovaných hostí na plese
                 

                 
SME: Kto chce byť v TA3, musí platiťsme.sk
   Len čo som dopísal minulý článok o TA3, ozvali sa mi čitatelia, že táto televízia stále podmieňuje výskyt niektorých firiem objednaním si reklamy (tzv. biele a čierne zoznamy), že v správach stále vidieť skrytú reklamu, či už cez rozhovory alebo reportáže. Tieto praktiky potvrdili v minulých rokoch či už licenčná rada, alebo bývalí pracovníci TA3 a dotknuté firmy. TA3 celý čas nelegálne aktivity popierala (resp. rovno klamala) a kritikov sa snažila očierniť.  Minulý týždeň TA3 odvysielala „reportáž“, že dostala (kúpila ?) šesť nových Mercedesov. Meno značky ani raz nepadlo otvorene („prestížna automobilová spoločnosť“), a ani jej zástupca Andrej Glatz nebol v titulku firmou označený. V prepise by tak človek naozaj nevedel, aké autá televízia získala. Ale z obrázkov príspevku bolo úplne jasné, o akú značku ide. Logo Mercedesu bolo opakovateľne v zábere, najviac pri rozhovore so samotným riaditeľom TA3. Nechýbal dojemný záber na riaditeľa automobilky a televízie, ako si podávajú ruku aj kľúče (ako keď niekto vyhráva v tombole). Sotva je náhodné, že v súvislosti s novými autami zaznel v príspevku až štyrikrát v rôznych formách prívlastok "bezpečný."   Šéf Mercedesu Andrej Glatz je inak pre TA3 veľkou celebritou už dávnejšie. Býva častým respondentom na oslavách výročia TA3, ale aj ako respondentom v relácii Firmy, ktorá často zaváňa reklamou. Nechýbal ani vo výbere hostí do reportáže pre hlavné správy z tohtoročného plesu v Opere (podobne ako ďalšia postava, ktorú TA3 dlhoročne preferuje, Juraj Lelkes, šéf poisťovne Kooperativa). Pravda, Glatz bol aspoň v organizačnom výbore plesu, čiže aký taký dôvod na prezentáciu aj mohol byť, hoci žiadna iná televízia si ho do spravodajstva nevybrala.  Lenže v spomínaných čiernych a bielych zoznamoch firiem či automobiliek pre autosalón pred 2-3 rokmi bol aj Mercedes preferovanou značkou, s odkazom na kontakt Andrej Glatza. Neprekvapí, že v historickom kontexte je Mercedes (resp. materský DaimlerChrysler) jeden zo štedrejších inzerentov v TA3. A podobne spomínaná Kooperatíva.   Vzťahy televízie s inzerentmi môžu byť divákom ukradnuté, pokiaľ nezačínajú tlačiť na reportérov, ako môžu robiť spravodajstvo, prípadne kto sa v ňom môže alebo nemôže ukázať. Horeuvedená skrytá reklama na dodávateľské produkty je nevídaná. Prídu príspevky, kto TA3 dodáva čiernu techniku, upratovacie služby, kancelársky papier???   ...Na reportáž sa nechala zneužiť jedna z novších redaktoriek TA3 Katarína Gazdíková.  Že táto skrytá reklama v televízii nebola náhodne, ukazuje aj iný príspevok z minulého týždňa – 9-minútový rozhovor s manažérom Dexia banky zaradený do poobedňajších správ. Televízii zjavne nevadilo, že po dvoch minútach určite znudila či odstrašila väčšinu svojich divákov. Moderátorka sa hosťa len máločo zmysluplné pýtala, ale hlavne mu nadhadzovala PR loptičky.   Dexia banka je dlhoročným partnerom medzinárodných inštitúcií, môžete nám spomenúť niektoré projekty, ktoré ste realizovali už v minulosti? ... Určite sa oplatí spomenúť aj aktivity Dexia banky v oblasti životného prostredia a trvale udržateľného rozvoja: ... Spomeňme si ešte, Dexia banka je známa svojimi aktivitami v oblasti projektov verejnoprospešných prác, čo sa zrealizovalo v tom uplynulom roku 2009?   Zábavným vrcholom celej show spravodajskej televízie bola táto otázka:   Dexia banka, ako sme už aj trošku tak načrtli, je lídrom v poskytovaní úverov samosprávnym krajom. Ako sa vyvíja tento segment a aké postavenie v ňom má teda Dexia banka, aj keď to sme už teda tak trochu načrtli, že je tým lídrom.   TA3 už za skrytú reklamu v nedávnej minulosti dostala tresty od licenčnej rady. Podľa televízie šlo o štandardnú žurnalistiku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (95)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




