
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Bredschneider
                                        &gt;
                čo život priniesol
                     
                 Analýza podomového predaja 

        
            
                                    23.7.2010
            o
            7:49
                        (upravené
                23.7.2010
                o
                8:34)
                        |
            Karma článku:
                6.55
            |
            Prečítané 
            1038-krát
                    
         
     
         
             

                 
                    Bol som na prezentácii hrncov...
                 

                        Všeličo sa mi prehnalo hlavou - zájazdy dôchodcov, ovčie rúna, nátlak...Nakoniec som však našiel svetlo nad mojou hlavou - blik. Rozhodol som sa, že nebudem nikomu kaziť radosť - hlavne kamarátovi čo už dnes robí palacinky na novej panvici.    Baví ma  psychológia a keď sa ponúklo predstavenie tej „čiernej", povedal som si, prečo nie? Možno sa niečomu priučím. Naštudoval som si všeobecné taktiky efektívnych predajcov - vyspovedal som kolegov, ktorý mali skúsenosti s podobným podujatím a večer som bol pripravený rozohrať partiu s nič netušiacou reprezentantkou hrncov.    Po zoznámení som zosmutnel. Prišla bojazlivá žena. Už som myslel, že ju nechám na pokoji. Teta však v úvode direktívne zakázala jesť tyčinky.  Akoby do mňa štuchla. Od tej chvíle som spustil zamatovú jemnú  slovnú prestrelku.   Moja taktika striedania uštipačných poznámok a chválenia jej produktu vytvorila zo mňa akého nedefinovaného „vtipálka" a musím priznať, že som pobavil seba i celú skupinku na návšteve. ( čo nebol vôbec môj cieľ)    Spustila naučené básničky, ktoré v publiku mali vytvoriť pocity previnenia. Vytvorila atmosféru učiteľského neosobného prístupu medzi  7 ľuďmi. Pretáčala stránky na svojej prenosnej prezentačnej tabuly a myslím, že nikoho v miestnosti neočarila. Čakal som kedy ma predavačka odpíše pod čiernu zem, zhodí pred kamarátmi ( tak to má v popise práce) to sa však vôbec nestalo. Iba som ju rozhodil, znechutil a rozčaroval. Musím podotknúť, že veľa práce mi to nedalo, pretože už prišla s tvárou, z ktorej by skyslo aj mlieko J   Nemali by náhodou efektívny predavači chodiť na akcie nabudení? Tešiaci sa na niečo? ( napríklad, že spoznajú nový recept, nových ľudí a podobne?) Jej to nič nehovorilo a pokračovala si vo svojom nudnom programe a pri hrncoch.   Podarilo sa jej pokaziť guláš tým, že tam dala pikantnú papriku pričom nikto mexický guláš nechcel. Nespýtala sa skoro na nič. Nezaujímalo ju ako sa voláme, čo robíme, či varíme...nič. Keby som ju nebol neslušne prerušoval a neskákal do jej výkladu, tak by nám asi predviedla nudnú hodinovú prednášku o jej predražených hrncoch.   Hovorila všeobecne o štvorčlennej rodine a vôbec som sa nenašiel v tom čo prednášala. Tešil som sa ako si slovne „zapinkám" a kreatívne pohrám s profesionálkou a výsledkom bola hra so „zdochýnajúcim škrečkom", hlavne pri podávaní ruky to bola veľmi slizká skúsenosť.   Tá žena predviedla krásny príklad toho ako sa nepredáva. Aj taký dobrý tovar ako boli tie hrnce nikoho nezaujali. A po mojich poznámkach, ktoré pôvodne neboli plánované na rozbitie jej autority a kvalite výrobkov, v konečnom dôsledku zapôsobili presne tak ako ona nechcela.   Jediné čomu som uveril, bol jej smutný odchod z bytu. Nepredala...myslím, že zase...ani prvý, ani posledný raz....    Týmto postrehom si nechcem robiť srandu z ľudí, ktorý predávajú čokoľvek. Je to ťažká práca - vchádzať k cudzím ľuďom, do cudzieho prostredia. Nezávidím a skláňam sa pred všetkými čo majú na to talent, vedia si v tej práci nájsť čo ich baví a že majú úspech. Som si však istý, že keby prišla talentovaná osoba, mal by som čo robiť, aby som nekúpil.   A aj keby som nekúpil hneď, vytvorila by vo mne túžbu vlastniť to. Žiaľ alebo chvalabohu to na tej mojej akcií z hrncami nevyšlo a ja žijem ďalej bez platenia mesačných splátok, aj keď bez hrncov a nezdravo....           
              
        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Bredschneider 
                                        
                                            Príbeh vody: Hookus-Pookus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Bredschneider 
                                        
                                            Príbeh vody:pitné puzzle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Bredschneider 
                                        
                                            Príbeh vody
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Bredschneider 
                                        
                                            Webové kamery
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Bredschneider 
                                        
                                            Nikto ma neopľul pre šťastie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Bredschneider
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Bredschneider
            
         
        bredschneider.blog.sme.sk (rss)
         
                                     
     
        spisovatelia kedysi mali absint, whiskey a heroin. Ja mám google:-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    126
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1017
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            zákulisné poznámky
                        
                     
                                     
                        
                            jedenkrát nestačí
                        
                     
                                     
                        
                            videl som v kine
                        
                     
                                     
                        
                            Slovenská generácia Y
                        
                     
                                     
                        
                            internetový nasledovník
                        
                     
                                     
                        
                            Príbeh vody
                        
                     
                                     
                        
                            moje úvahy
                        
                     
                                     
                        
                            zážitky z ciest
                        
                     
                                     
                        
                            modelové situácie
                        
                     
                                     
                        
                            čo život priniesol
                        
                     
                                     
                        
                            psychologické
                        
                     
                                     
                        
                            fikcia
                        
                     
                                     
                        
                            projekt VERSUS
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            dojímavé
                                     
                                                                             
                                            veselé
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            obľúbený časopis: profit
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            30 seconds to Mars
                                     
                                                                             
                                            hudbu sveta
                                     
                                                                             
                                            Noisettes
                                     
                                                                             
                                            Gossip
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Patrik Konečný
                                     
                                                                             
                                            Mária Lazarová
                                     
                                                                             
                                            Nora Slížová
                                     
                                                                             
                                            kamikatze
                                     
                                                                             
                                            Marek Kopča
                                     
                                                                             
                                            Andrej Valentiny
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            google blog
                                     
                                                                             
                                            blogy po španielsky
                                     
                                                                             
                                            španielčina
                                     
                                                                             
                                            wikipedia
                                     
                                                                             
                                            blogy študentov angličtiny
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




