

   
 Matej Nedorolík  pochádza z Banskej Bystrice. Väčšinu svojho detstva trávil na Donovaloch kde jeho rodina vlastní krásnu drevenú chalupu. Už ako trojročný sa naučil lyžovať, keď mal trinásť rokov prvý krát sa postavil na požičaný snowboard. Ako si zadovážil svoj prvý vlastný snowboard, tento šport ho úplne pohltil. Momentálne je jedným z popredných snowboardistov na Slovensku a sponzoruje ho firma Burton snowboards, ktorá je tu od zrodenia tohto športu. Matej však nie je len snowboardista, taktiež je študentom Medzinárodných vzťahov v Banskaj Bystrici. Je to priateľský a cieľavedomý človek, ktorý presne vie čo chce. Jeho láska, dravosť a sebakontrola mu dopomohla k niekoľkým výborným výsledkom na snowboardových podujatiach, taktiež sme mohli vidieť desiatky jeho fotiek v časopisoch Boardlife a Tasty ktoré sa venujú snowboardingu. Snowboardu sa venuje siedmu sezónu a rád sa s Vami podelí o jeho skúsenosti. 
   
   
 Ahoj Matej, mohol by si sa nám na začiatok predstaviť a povedať pár základných údajov ? Čomu sa momentálne venuješ ? 
   
 Ahoj, volám sa Matej Nedorolík. Mám 20 rokov, som z Banskej Bystrice. Študent Medzinárodných vzťahov, momentálne už tretí ročník. Vyrastal som na Donovaloch, väčšinu svojho detstva. Momentálne sa však musím venovať škole a iným povinnostiam, takže väčšinu času trávim v Banskej Bystrici 
   
 Vravíš že si vyrastal na Donovaloch známom lyžiarskom stredisku. Venuješ sa teda nejakému zimnému športu ? 
   
 V detstve som sa venoval najprv futbalu, štrnásť rokov. Je to fajn hra, ale predsa kolektívna a bez dobrej partie to nemôže fungovať a preto som sa začal venovať individuálnemu športu, ktorým je snowboarding. 
   
 Prešiel si od futbalu k snowboardingu, to je dosť zaujímavé. Môžeš nám bližšie popísať ako si dostal od letného športu k zimnému ? 
   
 Na chate som mal kamaráta, ktorý mi ukázal snowboard a zapáčilo sa mi to. Dovtedy som poznal iba lyže. Mamka mi jedno poobedie požičala snowboard z požičovne a po prvom obutí sa som vedel, že je to niečo čo ma bude asi dlhšie sprevádzať mojim životom. Potom prišla zhoda okolností, nepriaznivé podmienky vo futbalovom klube a povedal som si, že snowboarding bude u mňa číslo jeden. 
   
 Učil si sa snowboardovať sám ? Ako dlho ti trvalo kým si sa naučil robiť prvé oblúky ?  
 Áno, sám. Považoval som to za výzvu, že sa to musím naučiť a nenechám sa tou jednou doskou poraziť. V podstate je to s tým tak aj doteraz. Nuž a oblúky som sa naučil robiť hneď v prvý deň, lebo ma to strašne bavilo. 
   
  Keďže ťa to bavilo, videli to aj tvoji rodičia a kúpili ti snowboard, alebo aká bola tvoja cesta k prvému snowboardu ? 
   
 Na svoj prvý snowboard som si musel zarobiť. Teda nie úplne všetky peniaze, ale väčšinu áno. Rodičia mi totiž chceli kúpiť nové lyže, ale ja som povedal že radšej snowboard. No vtedy ešte nevedeli, že sa kvôli tomu vykašlem na futbal. 
   
 Ako to teda pokračovalo, dostal si sa do nejakej partie ktorá už predtým snowboardovala? Ako dlho už snowboarduješ ? 
   
 Jazdím teraz siedmu sezónu a zo začiatku som jazdil s tatkom, lebo s ním som sa chodil aj lyžovať. No potom som zistil, že aj moji spolužiaci snowboardujú, tak sme začali chodiť na hory spolu a vydržalo nám to doteraz. 
   
 Boli to spolužiaci ešte zo základnej školy, alebo už zo strednej ? Bavilo ťa zlepšovať sa chcel si cielene dosiahnuť viac v tomto športe alebo to všetko prišlo samo ? 
   
 Chodil som na osemročné gymnázium, takže v podstate zo strednej. A stále to je tak ako na začiatku, že so sa nechcel nechať poraziť doskou, čiže som s ňou súperil, čo mi vydržalo až dodnes. No neviem či príde deň, keď si budem môcť povedať že už som ja porazil dosku a nie ona mňa, o tom naozaj pochybujem. 
   
 Takže snowboarding je pre teba akási výzva. Prišli s ňou aj ďalšie, napríklad začal si chodiť na súťaže, alebo robiť niečo konkrétnejšie v tomto športe ?  
   
 Áno, začal som chodiť na súťaže, ale akosi ma to prestalo baviť, lebo na súťažiach to nie je o jazdení, ale skôr o strese a tak ďalej. J mám rád pohodu s kámošmi na horách. Proste pár ľudí nejaká prekážka, skok alebo zábradlie na ulici a všetko s kľudom si pripraviť. Pokiaľ toto nemám necítim sa dobre a ani sa mi dobre nejazdí. 
   
 Píšeš o zábradlí a skokoch, môžeš nám priblížiť čo si pod tým predstaviť ? Čo rád robíš na snowboarde ty? 
   
 Skok by mal každý vedieť ako vyzerá, zábradlie je jednoduchá vec, presne tie ktoré sú popri schodoch v mestách. No a ja jazdím úplne všetko, lebo všetko zábava a hlavne o tej je snowboarding. 
   
 Mnohí ľudia tento šport spájajú s drogami a ľuďmi čo sú rozdrapený a vulgárny. Čo si o tom myslíš ? 
   
 To je hlúposť. Lebo to či niekto berie drogy vôbec nezáleží na tom čo robí, ale s akými ľuďmi sa stýka. Práve to, že my robíme snowboarding nám nedáva čas na to aby sme sa venovali veciam ako sú drogy, lebo väčšina mladých ľudí začne drogovať takpovediac „Z dlhej chvíle“. 
   
 Vráťme sa späť k tomu vývoju. Začal si jazdiť s kamarátmi po horách, došli súťaže. Kto ti to všetko hradil ? Je to nákladný šport ? 
   
 Ja som mal tú výhodu, že máme chatu priamo na Donovaloch a rodičia tam vždy chodievajú na víkend, čiže ma brávali so sebou. Lístok som si nemusel kupovať, mal som to dobre premyslené. Keď som občas išiel na nejakú súťaž, tak ma rodičia podporili. Samozrejme som si privyrábal aj ako inštruktor. 
 Takže ty si mal isté výhody. No keď sa na to pozrieme z pohľadu futbal - snowboard je to nákladnejšie ?  Čo všetko obnáša taká výstroj a koľko približne musí človek investovať do dobrej výstroje ? 
   
 Tak určite je to nákladnejšie ako futbal. Dobrá snowboardová výstroj stojí aj tých 50000 Sk, teda ak máme na mysli komplet veci aj hardware aj oblečenie. No ale ja keďže nepochádzam z majetnej rodiny som sa dokázal zmestiť do tých 15000 Sk. 
   
 50000 Sk nie je málo. Takú výstroj si určite každý snowboardista, ktorý chce tento šport robiť na profesionálnej úrovni dovoliť nemôže. Existuje aj v tomto športe sponzoring ako je to aj v iných športoch ? 
   
 Samozrejme, je to veľmi rozmáhajúci sa biznis ,takže veľa jazdcov sa týmto športom bez problémov živí. 
   
 Poznáš osobne niekoho kto sa týmto športom živí? Je  možné živiť sa týmto športom aj u nás na Slovensku ? 
   
 Poznám jedine Matúša Hubku osobne, ktorý bol istý čas profík a dostával mesačný plat, ale odvtedy už nikto nedosiahol túto métu vo freestyle snowboardingu. No a na Slovensku je to veľmi zložité, totiž ak chce jazdec byť profíkom mal by jazdiť hlavne viac v zahraničí, lebo na SR také podmienky nemáme, no a to je zase otázka financií. 
   
 Ako si spomínal podporovali ťa aj rodičia a niečo si si aj zarobil sám.  Ako je to s tebou teraz ohľadne vecí na snowboard ? 
   
 Teraz som už vyrástol a musím sa o seba postarať viac sám, aj keď isteže tá podpora zo strany rodičov stále je, za čo som im nesmierne vďačný, lebo bez nich by som sa tomu nemohol venovať. No momentálne ma podporuje firma aj Burton. Od nich dostávam všetky veci, či už hardware alebo doplnky a oblečenie. 
   
 Ako si sa k takejto podpore dostal? Čo všetko musíš pre to urobiť aby si bol podporovaný ? 
   
 Úplnou náhodou. Mal som šťastie, že si ma na Dachsteine ( rakúsky ľadovec ) všimol šéfredaktor časopisu Boardlife. Potom ma vzali na nejaké fotenie, ocitol som sa v časopise na internete a bolo to. Takáto podpora však nie je nič jednoduché. Jazdec musí za to čo dostáva oplácať sponzorovi vo forme kvalitnej prezentácie daných výrobkov. Musí byť viditeľný na fotkách, videách súťažiach a podobne. 
   
 Spomenul si Dachstein. Aké sú u nás podmienky pre tento šport je potrebné chodiť trénovať do zahraničia ? 
   
 Ak chceš jazdiť na svetovej úrovni, u nás nemáš šancu sa na ňu vypracovať. Musíš chodiť von, aspoň do rakúska určite.  
   
 Navštevuješ často do Rakúsko ? Približne koľko mesiacov do roka stráviš na snehu ? 
   
 V zime sa snažím čo najviac. No keď u nás zmizne sneh, tak väčšinou chodíme na jar a na jeseň na nejaký ľadovec ešte si užiť ten posledný respektíve prvý sneh. 
 Čo robievaš cez leto a pred sezónou ?  Pripravuješ sa na zimu ?  
   
 Áno chodievam pred sezónou do posilňovne a cez leto jazdím na skejte. Teraz som posledné dve letá trávil v Dánsku a tam som aj surfoval. 
   
 Čiže máš rád adrenalínové športy. Prečo hlavne robíš snowboarding a čo ti to dalo do života ?  
   
 V snowboardingu som sa našiel. Väčšina ľudí sú v dnešnej dobe materialistami, ale mňa snowboarding naučil pravý opak. Človeku totiž peniaze môžu ukradnúť, ale tie zážitky čo človek zažije s kámošmi na horách mu nezoberie nikto. 
   
 Máš nejaký plán čo bz si chcel ešte v tomto športe dosiahnuť ? Okrem pozitývnych stránok má snowboarding aj svoje negatíva ? 
   
 Chcem hlavne jazdiť dovtedy kým to bude možné, nejaké konkrétne ciele nemám a ani som nikdy nemal. Učiť sa nové triky, to je samozrejmé. Negatívna stránka snowboardingu je možno iba tá, že široká verejnosť to stále vníma ako nejaké šialenstvo na doskách. 
   
 Je to adrenalínový šport, K nim partia väčšinou aj zranenia, platí to aj pri tomto  
 športe, ak áno mal si niekedy vážnejšie zranenie ? 
   
 Samozrejme, ale pre mňa je to ako každý iný šport. V každom športe hrozia zranenia, len otázka je v ktorom viac a v ktorom menej. Ja som mal nejaké pomliaždeniny a podobne, tomu sa človek nevyhne. 
   
 Vidíš budúcnosť snowboardingu na Slovensku ? Začína sa to aj u nás rozmáhať, i keď ľudia stále nie sú zvyknutý na tento šport ? 
   
 Začína sa to celkom slušne rozbiehať. Ale všetci vieme, že globálne otepľovanie je neúprosné, 
 takže sa veľmi obávam toho, že to nebude ľuďmi, ale prírodou, ktorá bude brzdou tohto športu. 
   
 Mimo rozhovor si písal, že zajtra máš skúšku v škole. Stíhaš robiť snowboarding a popritom aj študovať ? 
   
 Samozrejme, keď človek niečo veľmi chce vždy sa to dá. A snowboarding chcem najviac, čiže si to viem vždy zariadiť tak, aby bol čas na všetko. 
   
 Vravel si, že chceš snowboardovať čo najdlhšie. Plánuješ sa nejako uplatniť v športe, alebo je to tvoj koníček a hlavná je škola ? 
   
 Ono človek nikdy nevie čo bude, to je na živote pekné. Určite by som chcel robiť niečo v súvislosti so snowboardingom a byť čo najviac na horách, ale faktom je, že nie vždy sa dá robiť to čo chceš a musíš sa prispôsobiť podmienkam. 
   
   
   
 Aký máš pohľad na snowboarding ty ho berieš ako šport alebo ako istý druh cesty životom ? Čo by si poradil človeku, ktorý chce tento šport robiť či už závodne alebo ako koníček ? 
   
 Poradil by som toľko, že nech sa nepozerá na iných, nech si ide svojou cestou a robí to čo sám chce a čo sa mu páči. Keď chce závodiť nech závodí, keď chce šliapať deň na horu a dať si jednu jazdu, nech to spraví, ale hlavne nech to robí od srdca. 
   
 Chcel by si na záver rozhovoru niečo odkázať, niekomu sa poďakovať ? Odkázať nejaké tvoje obľúbené motto ? 
   
 Určite by som sa chcel poďakovať rodičom, ktorí sú pre mňa najviac a nesnowboardoval by som keby nebolo ich.  Kámošom, ktorí vždy stoja pri mne  a dievčatám, za to že sú pekné a je ich tak veľa. Odkazujem moje motto: „ Nesúťaž, vyhrávaj“. 
   
 Ďakujem ti za rozhovor a želám ti veľa šťastia v napredovaní. 
   

