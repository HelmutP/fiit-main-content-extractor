
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 V TA3 všetko po starom? 

        
            
                                    19.9.2008
            o
            14:41
                        |
            Karma článku:
                11.25
            |
            Prečítané 
            4940-krát
                    
         
     
         
             

                 
                    Keď vychádzajú predpovede o budúcnosti
                 

                 
 flickr - Willy Volk
    V pondelok či utorok tento týždeň zapínam telku, čakám na správy TA3. Musím počkať, beží ešte reklama. Ale čo, je na cestovnú kanceláriu BUBO. Pripomína mi slová kolegu, že v publicistickej relácii Firmy  videl týždeň predtým PR rozhovor s obchodným riaditeľom tejto cestovky. Zaujímavé, myslím si, a spomeniem si aj na jún tohto roku, keď mi zástupca jednej firmy povedal, že v balíku možnosti inzercie dostali od TA3 aj PR rozhovor v „publicistike.“ Nasledujúci deň prebehnem zoznam inzerentov a archív TA3 – BUBO za posledný rok a pol neinzerovala, a ani nemala akúkoľvek zmienku v tejto televízii. Aká to  náhoda, že im to teraz vyšlo v rovnakom čase. A ako vyzeral ten rozhovor, ktorý televízia nazvala „Cestovanie o úroveň vyššie“? Vyberám:      Pavel FELLNER, obchodný riaditeľ BUBO Travel Agency  --------------------  Presne, to je ten dôvod. My dávame alebo snažíme sa dávať našim klientom intenzívnejšie zážitky. Je to vlastne aj náš slogan celej cestovnej kancelárie. Klient, ktorý s nami cestuje, nielenže je teda na jednom mieste, ale spoznáva kultúru, kulinárstvo, ľudí, pamiatky, prírodné úkazy a keď tú krajinu vlastne celú vidí, obsiahne, tak tie zážitky, ktoré z toho má, z toho cestovania, sú absolútne iné ako len ležať na pláži povedzme dva týždne, aj keď v nádhernej krajine.    Ján LEŠUNDÁK, moderátor  --------------------  K tomu, čo ste teraz rozprávali, máme aj pripravené obrázky. To nám môže aj réžia pustiť. My si zatiaľ teda môžeme povedať, že čo je teda podľa vás vôbec najdôležitejšie pri podnikaní na poli cestovných agentúr.    Pavel FELLNER, obchodný riaditeľ BUBO Travel Agency  --------------------  Jednoznačne spokojný klient. Na konci každého zájazdu naši klienti odovzdávajú ankety, kde píšu, ako sú spokojní so sprievodcom a s kanceláriou. Ako sa im páčilo na zájazde. A môžem sa pochváliť, že viac ako deväťdesiatšesť percent našich klientov hodnotí zájazdy alebo cestovnú kanceláriu spokojnosťou až nadšenosťou. Takže myslím, že to je absolútne jednoznačný názor.    Ján LEŠUNDÁK, moderátor  --------------------  Práve teraz diváci vidia, kam sa s vašou kanceláriou môžu dostať a je to vlastne o tom, čo ste spomínali, že im ponúkate zážitky a naozaj skvelé cestovanie. Poďme teraz ale na ďalšiu tému.  ....  Ján LEŠUNDÁK, moderátor  --------------------  Teraz prejdeme k niečomu inému. O pár dní, čiže dvadsiateho piateho septembra, otvoríte cestovateľské centrum, tak sa to volá. Môžeme to nazvať z vášho pohľadu novinkou a štandardom?    Pavel FELLNER, obchodný riaditeľ BUBO Travel Agency  --------------------  Pre nás už je to štandard, ale pre veľa ľudí to bude novinka. ...  ...  Ján LEŠUNDÁK, moderátor  --------------------  Povedzme si ešte na záver, blíži sa nám zimná sezóna, čo to znamená pre vašu agentúru cestovnú?    Pavel FELLNER, obchodný riaditeľ BUBO Travel Agency  --------------------  Znova je to veľký rozdiel oproti klasickým cestovným kanceláriám. Nám v podstate začína zimná sezóna v polovici októbra, kedy všetky hurikány, ktoré teraz sú na Kube, zaniknú a opäť tam bude nádherne a začne sezóna, od tej polovice októbra až do konca marca je práve v týchto krajinách, v strednej Amerike, v celej juhovýchodnej Ázii, ale aj v ďalších krajinách top sezóna. Takže Mexiko, Kuba, Thajsko. Tam sme zároveň aj najväčšími predajcami na Slovensku, takže to sú také tri oblasti, kde teraz budeme mať stovky, stovky klientov.    Ján LEŠUNDÁK, moderátor  --------------------  Ďakujem pekne za rozhovor.    Pred pár dňami si Mediálne.sk všimlo, že v oslavnej narodeninovej reportáži TA3  chváli televíziu aj náš prezident, tak ako rok predtým. A že tam vystupovali aj zástupcovia podnikateľov:     Spomedzi respondentov získali vo výročnej reportáži slušný priestor riaditelia distribútorov automobilov. Gratuloval riaditeľ Daimler Chrysler Automotive Andrej Glatz, riaditeľ BMW Group Peter Kramár či riaditeľ divízie Seat Import Ľudovít Ujhelyi. Vystúpili aj šéf najväčšieho mobilného operátora Orange Pavol Lančarič a druhej najväčšej poisťovne Kooperativa Juraj Lelkes.      V reportáži teda vystupovali najmä ľudia, ktorí zastupovali top inzerentov v TA3 – Seat/Volkswagen, Kooperatíva, BMW, DaimlerChrysler, Orange. Čo sú podľa údajných tajných zoznamov TA3  z marca tohto roku s výnimkou Orange práve firmy a značky v TA3 spravodajstve či publicistike pretlačované (na "bielom" zozname získanom mediálne.sk je aj usporiadateľ ostatných troch narodeninových osláv televízie hotel Crowne Plaza).    Zaujímaví boli v reportáži aj zástupcovia fanúšikov TA3 z strany odbornej verejnosti - neurochirurg Juraj Šteňo a rektor Ekonomickej univerzity Rudolf Sivák. Aká náhoda, že z pár stoviek hostí boli citovaní práve oni – presne ako v reportáži z oslavy narodenín TA3 v hlavných správach pred rokom, resp. aj v roku 2006 (v prípade Šteňa).    Obzvlášť citovanie Siváka ma neprekvapilo – v júni, keď som skúmal existenciu bielych a čiernych zoznamov, mi novinársky zdroj z TA3 spomínal, že aj Sivák je na zozname ľudí, ktorých bolo treba v TA3 oslovovať prednostne, kde sa dá. Podľa archívu bol Sivák hosťom vo veľkom rozhovore na TA3 za posledný rok dvakrát (relácia Manažment) a raz hosťom online diskusie  na ta3.com. Televízia o známom prípade plagiátorstva na EÚ  (Marián Tkáč) divákov neinformovala.       Zdá sa, že desiatka ľudí, ktorá mi v máji a júni rozprávala o prepletení osobných a obchodných záujmov s vysielaním TA3 vedela nielen pekne vysvetliť vtedajšiu minulosť na obrazovke, ale aj budúcnosť.       Mimochodom, Rada pre vysielanie a retransmisiu by podľa programu  mala budúci týždeň rozhodnúť, či TA3  v dvoch príspevkoch spred pol roka (!) porušila zákon a odvysielala skrytú reklamu (SkyEurope, OVB).     TA3 oficiálne opakovane odmieta, že by sa v jej vysielaní dialo niečo nekalé.           Najrýchlejší v únii: Ľuboš Jančík zo SME včera napísal, že sme stále najrýchlejšie rastúcou ekonomikou v Európskej únii (pozri infografiku v článku). Podľa Eurostatu  sme ale boli v druhom štvrťroku 2008 už druhí za Rumunskom (medziročne 7,6% verzus rumunských 9,3%).     Mŕtvi na cestách: Dušan Tokarčík v relácii TA3 Čierny Peter v príspevku o tragédii v Chorvátsku na konci povedal:       Stojí však za to zamyslieť sa, prečo si slovenské cesty za uplynulý rok vyžiadali viac ako päťstopäťdesiat obetí na životoch.    V 2007 alebo bolo podľa polície  627 mŕtvych, rok predtým 579.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




