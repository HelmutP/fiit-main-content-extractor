
 Trať Humenné – Łupków, ktorú  preslávil Hašek vo svojom Švejkovi, bola otvorená v roku 1874 a jej súčasťou je zaujímavý Łupkowsky tunel, ktorým v tej tunelovej tme, kdesi v jeho útrobách, vedie poľsko-slovenská hranica.  Łupkowským priesmykom sa pravidelne preháňali vojská rôznych národov, ktoré pri každom svojom prechode vyhadzovali železničný tunel  do vzduchu.  Keď túto pôvodne rakúsko-uhorskú trať po štvrtýkrát opäť v roku 1999 otvorili, bol som nadšený. Železnica predstavovala najjednoduchší prechod zo severovýchodného Slovenska do poľských Biesczad. 

Začal som trať intenzívne využívať a spolu so Skúkamom sme sa stali pravidelnými cestujúcimi v malých červených vagónikoch tohto poľsko-slovenského spojenia. Cieľovou stanicou bola väčšinou dedinka Komancza, malebná rusínska  obec, druhá zastávka po prechode hranice.

 Jediná vec, ktorá trochu komplikovala vikendy poznávania poľskej strany Bukovských vrchov, bol chudák Skúkam. Bolo to ešte pred vstupom do Európskej únie. Skúkam bol pre colníkov vec a nie „občan“ únie ako dnes, keď vlastní pas s fotografiou z profilu, na ktorú je patrične hrdý. Pred každým prechodom som musel vybaviť potvrdenie od veterinára, že v okolí Skúkamovho bydliska nie sú žiadne zákerné choroby a že je riadne očkovaný proti besnote. 

Veľmi často sme cestovali vláčikom sami dvaja a tak nám poľskí colníci z nudy venovali neprimeranú pozornosť. Hneď za tunelom je zastávka Nowy Łupków, kde pri nástupe alebo vystupovaní prezerali Skúkamove dokumenty a môj pas. Celkom ma to nebavilo a zdržovalo, ale Skúkam má veľkú charizmu a dokázal sa na nich usmievať aj cez košík, ktorý musel mať povinne  nasadený. Práve jeho charizma nám veľakrát umožnila prejsť hranicu aj keď na našich dokumentoch neboli úplne všetky pečiatky. 

Jedno krásne, slnečné októbrové popoludnie som sa v nedeľu vracal z poľských kopcov, kde som bol popozerať ako vyzerajú poľské jelene.  Víkend sa vydaril, mal som veľmi dobrú náladu a jedine čo mi ju kazilo, bolo pomyslenie na pondelok v práci.

Na stanici Nowy Łupków sme nastúpili do vlaku, vybalil som termosku, nalial Darjeeling a čakal na povinnú colnú a pasovú kontrolu. Ta začala hneď a prebiehala veľmi rýchlo, keďže v celom vlaku sme boli traja.

Ja, Skúkam a sprievodca.

„Proszę mi pokazać pana dokumenty! “, ozvalo sa z chodbičky. Bodrý pasový úradník skontroloval pas a pustil ku mne colníka.

„Ładny piesek, ładny, jak się nazywa ?“ otravoval uniformovaný ujo.

„Skúkam“, snažil som sa byť stručný.

„Pan tu do nas chodzi często, to co pan u nas robi?“

„Viete, ja pašujem psov, toto je napríklad kus za 200 000 korún“, povedal som veselo zjavnú blbosť, na ktorej sa všetci pohraničiari, vrátane našich, zasmiali, kontrola skončila a ja som pokračoval v pití čaju. 

 Výpravca zatiaľ na peróne nebol, aj keď sme už asi päť minút meškali, chcel som sa ešte so Skúkamom pred cestou vlakom prebehnúť, ale náš colník ma vrátil naspäť. Mal pravdu, po kontrole už treba sedieť vo vlaku.

Sedeli sme, sedeli, päť minút meškania sa zmenilo na desať, potom na trištvrte hodinu a vlak stále stál.

Sprievodca nevedel o príčine meškania nič a tak prišiel po čaji na rad olovrant. Rozbalil som si chlieb s maslom, pozerám von oknom a tam som zazrel najväčšie nasadenie poľskej armády aké som kedy videl.

Ak mám pravdu povedať, poľskú armádu v nasadení som videl jedine v slávnom seriáli Štyria tankisti a pes, ale toto, čo sa dialo za oknom, bolo tiež zábavné.

 Asi dvadsať poľských vojakov vyskočilo za jazdy z terénnych áut a vytvorilo na peróne rojnicu. Skupina ďalších desiatich sa rozdelila na dve skupiny po piatich a tieto dve komandá naskočili na začiatok a koniec vlaku. Z vlaku sa začali ozývať akési výkriky, ktorým som nerozumel a keďže sme sedeli uprostred, trochu sme si na vojačikov museli počkať.

Ďalšia konverzácia prebiehala rýchlo, tvrdo a v podstatne inom tóne ako pri kontrole dokumentov. Ničomu som nerozumel, len som plnil príkazy. Zostaňte sedieť, sedel som. Nesmejte sa, nesmial som sa. Odovzdajte doklady, odovzdal som. Nasaďte tomu psovi košík, nasadil som. 

Jeden chlapík zmizol kdesi s mojim pasom a Skúkamovými pečiatkami. Sprievodca a pohraničiari na tichú otázku, čo sa deje, len bezradne kývali plecami. Meškali sme už vyše hodiny a moja dobrá nálada odišla aj s mojim pasom kdesi preč.  

Po hodine a pol, keď už boli všetci naštvaní, odkiaľsi sa vynoril neohrozený poľský vojak a doniesol mi doklady. Bez vysvetlenia odvelil vojakov a vlak sa pohol do Medzilaboriec. A v tom slávnom Łupkówskom tuneli mi jeden náš colník porozprával čo sa vlastne stalo.

Po kontrole dokladov odišli colníci a pasováci  na stanicu a dávali si tam v kancelárii kávu. Jeden z nich začal rozprávať o Skúkamovi a o tom, ako som si robil žarty z pašovania psov za 200 000 korún. Akýsi prísediaci policajný úradníček si dal v hlave dokopy môj žart a nejakú informáciu o zlodejovi luxusných psov, ktorá visela u nich na nástenke a zavolal nadriadeným. 

Ostražití nadriadení páni vydali okamžitý príkaz „zločinca“ zaistiť, dôkladne preveriť a v prípade potreby internovať. Preto to komando a preto tá dlhá kontrola dokumentov.

Zachránil ma poctivý policajt, ktorý si nielen overil pravosť môjho pasu u našej polície, ale aktívne zavolal aj Skúkamovmu veterinárovi, ktorého číslo našiel medzi pečiatkami. Ten vedel, že mi pravidelne vystavuje potvrdenia na výjazdy do Poľska a policajta dokázal presvedčiť, že som to ja a Skúkam.

Vždy keď idem cez Łupkowský priesmyk, tak si spomeniem, že s policajtmi sa nežartuje. A zvlášť nie v dedine Nowy Łupków, kde je veľká poľská väznica pre nebezpečných recividistov.

Skúkam to vždy vedel a pri pasovej a colnej kontrole sa usmieval.
 
