

 -Som smutný z toho, že ľudia si v našej krajine nemôžu povedať vlastný názor. 
 -Som smutný z toho, že 20 rokov po komunizme nedokážeme mať v sebe ani kus tolerancie. 
 -Som smutný z toho, že 65 rokov po vojne sa nájdu ľudia, ktorý bezhlavo veria chorej ideológii. 
 -Som smutný z toho, že 600 rokov po stredoveku, dokáže človek za odlišný názor hádzať kamene do ľudí. 
 -Som smutný z toho, že si niekto radšej nájde čas na to aby hádzal kamene do iných ľudí, namiesto toho, aby rozvíjal svoje talenty, ktoré má. 
 -Som smutný z toho, že práve tý ľudia čo nám najviac rozprávajú o vlastenectve a hrdosti k Slovensku, tak aj najviac to milované Slovensko okrádajú. 
 -Som smutný z toho, že ľudia veria populistom. 
 -Som smutný z toho, že na Slovensku môže byť dealer odsúdený na dlhšie ako mafiánsky boss. 
 -Som smutný z toho, že na hrdosť potrebujeme zákon. 
   
 Ako môžem mojim priateľom zo zahraničia s úsmevom rozprávať o Slovensku, keď ma toho toľko na vlastnej krajine trápi? 
   
 Našťastie to nie je tak zlé ako to na prvý pohľad vyzerá a predsa len môžem byť ja, ale tak isto vy všetci na niečo hrdý. 
   
 Môžeme byť hrdý na to, že sú tu ľudia, ktorý to napriek tomu nevzdávajú! 
 Môžeme byť hrdý na to, že sú tu ľudia, ktorý veria že sa s tým dá niečo robiť a dokážu ísť za tým. 
 Môžeme byť hrdý na to, že nie som sám čo si toto myslí. 
 A môžeme byť radi, že sa k týmto ľuďom stačí pridať. 
   
 Môžeme začať už len zmenou postoju. 
   

