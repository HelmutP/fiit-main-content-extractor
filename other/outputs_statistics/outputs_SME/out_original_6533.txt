
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Récky
                                        &gt;
                Nezaradené
                     
                 Tisíc a jedna noc s dvomi manželkami v Katare. 

        
            
                                    15.5.2010
            o
            9:26
                        (upravené
                21.5.2010
                o
                6:58)
                        |
            Karma článku:
                7.57
            |
            Prečítané 
            1938-krát
                    
         
     
         
             

                 
                    Kto by bol povedal, že osoba ako ja sa ocitne v Katare. Možno vás prekvapí, prečo sa tomu čudujem, ale to ma nepoznáte. Nerád vychádzam z mojej izby, často mám problém zájsť si čo len do chladničky po jedlo. To radšej zostanem hladný. Preto, keď som sa pred mesiacom ocitol na prestupnom letisku v Dubaji, vedel som že je zle.
                 

                    Colník, inak mimo služby určite  priateľský človek, sa ma s výrazom odpozeraným z „Mlčania jahniat" pýta,  čo to mam za fľašku v kufri. Hmmm, klamať sa neoplatí, na starú  medicínu, slúžiacu na mazanie mojich kĺbov to asi neuhrám. S pokorou  počúvam, čo už dávno viem - žiaden alkohol do krajiny. Navonok  s trpiacim výrazom, no s úsmevom v mysli sa vraciam k Alpe schovanej  k ponožke. Plán vyšiel! Bude to síce iné ako zhabaná sedem decka  Fernetu, ale aspoň niečo na abstinenčné príznaky.       Do hotela sme prišli v noci. To, že je  noc sme zistili hlavne podľa mesiaca. Ak si niečím v Katare môžete byť  istý, tak tým, že svietia vždy, všade a so všetkým čo majú k dispozícii.  Pár slov o hoteli. Slovo „prezamestnanosť" v ňom a aj v tejto  krajine nadobúda nové rozmery. Netuším, ako to robili ale vychádzalo im  to presne - keď bolo na raňajkách 15 hostí, 18 čašníkov bolo v pozore.  Na konci nášho pobytu bol hotel plný, vďaka nejakému prachu vo vzduchu.  Ľudia nemohli odletieť, lebo mali na ten prach alergiu či čo. Vo  vrcholných chvíľach s na večeri objavilo približne 80 ľudí a....zas tam  mali viacej personálu ako hostí! Je síce pravda, že polka z nich boli  prezlečení rúm klíneri, tréneri z posilky a otvárači hlavných vchodových  dverí, ale....dokázali to. Tomu hovorím úroveň služieb! Ešte jednu  peknú vec musím spomenúť - všetok personál, vždy keď ma stretol sa  opýtal „Ako sa máte?".  Možno sa to nezdá ako nič  výnimočné, ale keď sa vás 40 krát za deň opýtajú, ako sa máte, tak ...      Na druhý deň sme sa rozhodli, že si  spravíme menšiu exkurziu mesta. Keď sme v centre po pol hodine  nenatrafili ani na jedného chodca, začalo to byť trochu divné. Zato však  každých 5 minút sa pri nás pristavilo auto s tým, či nás majú niekam  odviesť. Ja a moje dve manželky (vysvetlím neskôr) sme usúdili, že  stačilo a so značne zrýchleným krokom sa pobrali späť do hotela. Dostalo  sa nám vysvetlenia, že tu má auto každý a keď nás videli ísť peši  usúdili, že sa nám to naše pokazilo a potrebujeme odviezť. Takže  rozpoznávajúci znak - jediní ľudia, chodiaci v Katare peši, sú turisti.  Druhá možnosť je, že  nie sú turisti, a pred chvíľou sa im pokazilo  auto. Aké proste.      Ďalšiu kultúrnu facku som dostal ešte v  ten istý deň.  Pri vstupe do nákupného centra začnete  ľutovať, že ste si so sebou nevzali sveter. Je síce pravda že vonku je  34 v tieni, zato vo vnútri je tak 18 stupňov, a to všade. A obzvlášť na  ľadovej ploche uprostred. Môj prvý odhad bol, že je to ľad umelý. No keď  naň po chvíli vyšla rolba, bolo jasné, že je skutočný.   Za vrchol architektonickej kreativity považujem u nás v Nitre  presklený stánok na autobusovej stanici o rozmeroch 1x1,5 m, kde je  vidieť, ako tety robia cesto na langoše a následne aj langoše. Preto si  asi viete predstaviť, čo so mnou spravila ľadová plocha uprostred  nákupného centra.          O farebných zvieratkách, benzíne,  Katarčanoch, mojich dvoch ženách a o vode nabudúce. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Récky 
                                        
                                            Vzorec na úspešnú social game
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Récky 
                                        
                                            Krutá pravda o Farmville
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Récky 
                                        
                                            Ako si Facebook podal Nestlé
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Récky
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Récky
            
         
        jurajrecky.blog.sme.sk (rss)
         
                                     
     
        Even with my eyes wide open,
I can´t see a thing
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4488
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




