

   
   Z rozhlasu sa šíri zbožné volanie mladej speváčky,netreba ani rozumieť a porozumieme.Je to túžba po sladkom mámení,spomienka naň,pre iného len  bolestivé škŕkanie,ktoré vypína.Je blízko,naháňať sa za ňou je niekedy daromné,nevieme odhadnúť silu,ktorou nás vie ovládnuť,aź ked je po nej ,cítime smútok,plakať je zbytočné,nevráti sa. 
   Aj tak je nemysliteľné bez nej žiť.Viacerí podceňujú jej potrebnosť,tvária sa ,že aj bez nej sú v rovnováhe,dokonca jej cielene škodia,aby sa jej zbavili.Neváhajú sa zbabelo dívať na trápenie svojho partnera,len aby ho nejak presvedčili,že je preč.Radšej sloboda,nové dobrodružstvá,len žiadne záväzky.Potom príde štyridsiatka a v byte je prázdnota,šuští televízor,fučí počítač a čo ďalej... 
   Ale ona čaká,pozná svoju hodnotu,neodmysliteľnosť pre život,mládenci,nebojte sa ,má milióny rokov a vy ju neprevalcujete,čaká na vás ,ak sa jej vyhnete dnes,zajtra si na vás počká možno v prefíkanejšej podobe.Zatiaľ  vám mohol rásť v kočíku jej dar,to malé klbko šťastia,ktoré si vás získa jedným zívnutím a svet sa vám rozžiari aj iným svetlom ako počítačovým . 
   Uhádli ste,áno, je to láska,všeobjímajúca,všetko pretrpiaca,obetujúca nielen čas,ale aj silu,zostarnete,ale nie od vyčerpania za mamonou.Budete môcť bilancovať jej účinky v podobe svojho nástupcu. 

