

 Každopádně, mně je vždy vzletně nad každým novým obrázkem Lubice Litnerové na Facceboku . Její roztančené domy mi dávají možnost se povznést nad uplivanou, zavajglovanou, vulgární še´d městských i  venkovských ulic:brrrr!  
 To raději s ní ulétám coby ten motýl, ta jiřička a zbožně zhlížím na ten Boží svět z ptačí perspektivy. Je barevnější  a veselejší. 
  Domečku, domečku,vzhůru na kopečku  cestičku k tobě vyšlapávám ,  máš  červený čepeček z věžičky stavěný,  znáš moje tajemství co v tebe  vkládám:  zacinká zvonec, roznese do světa, 
 kdeže se právě ti  zpovídám z kopečku poběží jeho cinkání, do oblak poletí 
 co já ti  zazpívám. 
 její roztančené ulice jsou pro mě právě tím impulzem přeletů nad měvesničkami a rozprávěnek se zdánlivě mrtvými ulicemi z pohledu přiškrceného sevření pozemských asfaltů a poplivaných a zavajglovaných chodníků: brrrr. 
 To raději  nazpět do obrázků a slovních hříček. Třeba si zahrát na 50 slovíček. 
 Pes a páníček 
 panička seděla na lavičce, pes pod lavičkou 
 panička sledovala krajinu 
 pes snil 
 Byl to horský pes, tak snil 
 o ledové zimě 
 Panička hleděla do barevné krajiny 
 údolí pod lesem 
 a snila o pláži  plné ohřátého písku 
 a mořském vlnobití 
 Vtom zavolal páníček 
 "Mám houby !" 
 Jediný pes zapomněl 
 na své sny. 

