
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Šubová
                                        &gt;
                Privat
                     
                 Chudoba cti netratí 

        
            
                                    17.4.2010
            o
            9:43
                        (upravené
                17.4.2010
                o
                20:19)
                        |
            Karma článku:
                24.35
            |
            Prečítané 
            10748-krát
                    
         
     
         
             

                 
                    Netratí???  "Chudoba cti netratí" bolo obľúbené príslovie mojej babky.  Vedela o čom hovorí. Na začiatku minulého storočia ich 14 člennú rodinu  živil najprv plat pomocného robotníka v železiarňach a potom už ani ten,  lebo živiteľ a ploditeľ zomrel na španielsku chrípku, takže rodina  nedostala ani penziu. Babka škole moc nedala, ale od štrnástich rokov sa  chtiac-nechtiac starala o seba sama.
                 

                 V romantizujúcich predstavách môjho detstva chudobný šuhaj svojou múdrosťou, šikovnosťou a pracovitosťou získal princeznú a mne sa slovo "chudobný" spájalo so  skromnosťou, hrdosťou a schopnosťou prekonávať prekážky - na rozdiel od tých "bohatých", ktorí boli v svojej lenivosti odkázaní na prácu "chudobných". V rovnostárskej spoločnosti konca osemdesiatych, kde všetci mali "obývačku Stella" bol naozaj problém pochopiť, čo je chudoba - okrem pravidelných dodávok obrázkov černošských a indických slumov z televízie.   Prvú trhlinu dostala táto ilúzia v rozhovore s babkiným bratom, ktorý mi vysvetľoval, prečo "chudobní vstupovali do komunistickej strany - aby nemuseli pracovať a mali sa ako grófi". Nuž ak boli naozaj pracovití, prečo nechceli pracovať a ak boli naozaj múdri, ako mohli uveriť, že ich grófsky život nebude treba financovať? Vtedy som dostala argument v podobe facky za drzosť a na túto epizódu rýchlo zabudla. Pripomenula sa mi až neskôr v reálnom kapitalizme v bohatej krajine, kde si rodina "na sociálke" bez problémov kúpila auto a ľahko si spočítala, že je výhodnejšie nepracovať ako pracovať.   A potom som sa stretla s chudobou v reále. S ľuďmi žijúcimi v chatrčiach, či zničených bytoch, žobrajúcich drobné, ľudí bývajúcich bez vody a elektriny. Ale márne som medzi nimi hľadala toho šuhaja z rozprávok, ktorý svojou šikovnosťou menil svoj život. Stretla som sa s natiahnutými rukami, do ktorých neslobodno vložiť pracovný nástroj, s maximálnymi znalosťami svojich práv - bez vedomosti o tom, že existujú aj povinnosti, so závisťou až nenávisťou namiesto popisovanej hrdosti a miliónom dôvodov kto a prečo môže za to, "veď pozrite ako žijeme".  Na otázku prečo na stole neleží chlieb a maslo, ale najdrahšia čokoláda a fľaša vodky, prečo je byt plný smetí, ktoré nikto nevynesie, prečo cez okná nie je vidieť, keď obaja sú celý deň doma, prečo má dieťa deravé punčocháče a špinavé tričko, hoci voda tiekla a nite sú voľne dostupné, som veru rozumnú odpoveď nedostala. Prečo odmietli "obnosené" oblečenie, že nie je "značkové", prečo si celé týždne nenavarili, prečo celé dni trávili pred televízorom... Po týchto susedoch som si začala túto vrstvu všímať. Bohužiaľ stále sa rozrastá. Ľudia, ktorí nechcú robiť nič pre to, aby zlepšili svoj život. Sú v podstate nespokojní, zúrivo závidia "bohatým" - hoci vo vedľajšom byte, ale za ich "mizerný život" môže každý iný, len nie oni...   Naša babka sa asi v hrobe otáča. Ona, ktorá roky "držala dom" bez tečúcej vody a kúrenia, ktorá prešívala v ruke dcéram šaty, "aby to nebolo vidieť, že sú obrátené", ktorá dokázala vyčarovať obed pre päťčlennú rodinu zo zemiakov, kapusty a prézlí. Ona, ktorá ma naučila vážiť si potraviny "kto vyhadzuje chlieb ako by peniaze vyhadzoval", "držať kasu" - "kto vyjde z mála, vyjde potom aj s veľa" a ktorá ma naučila, že chudoba cti netratí. Ak ju teda samozrejme má...   Na pamiatku mojej babky, ktorá by mala v týchto dňoch 99 rokov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (71)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Od inakiaľ XVIII
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Domov sú ruky, na ktorých smieš plakať...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Príspevok z kuchyne k výročiu Veľkého Novembra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Osudové okamžiky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Veď nejde o život
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Šubová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Šubová
            
         
        subova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Nealternatívna mamina

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    585
                
                
                    Celková karma
                    
                                                9.02
                    
                
                
                    Priemerná čítanosť
                    1919
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nie súkromné
                        
                     
                                     
                        
                            Pod bielym krížom
                        
                     
                                     
                        
                            Služobne
                        
                     
                                     
                        
                            Damals
                        
                     
                                     
                        
                            Privat
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            o rakovine pri pive
                                     
                                                                             
                                            moja videovizitka
                                     
                                                                             
                                            Recenzia na Pána Prsteňov 1977
                                     
                                                                             
                                            Epitaf
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Falco
                                     
                                                                             
                                            Karel Kryl
                                     
                                                                             
                                            Daniel Landa
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Apollo
                                     
                                                                             
                                            Lujza
                                     
                                                                             
                                            fan
                                     
                                                                             
                                            Hliva
                                     
                                                                             
                                            Kar-ma
                                     
                                                                             
                                            waboviny
                                     
                                                                             
                                            štvorica
                                     
                                                                             
                                            vlčko
                                     
                                                                             
                                            stranger
                                     
                                                                             
                                            štvorlístok
                                     
                                                                             
                                            germa píše
                                     
                                                                             
                                            Maroško bloguje
                                     
                                                                             
                                            Ywka
                                     
                                                                             
                                            Čára
                                     
                                                                             
                                            Halúzky zo života
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Varíme s medveďom - Rok v kuchyni
                     
                                                         
                       Domov sú ruky, na ktorých smieš plakať...
                     
                                                         
                       Okolo nedeľného obeda
                     
                                                         
                       Žiť si na morskej MHD
                     
                                                         
                       Tak vchádza svetlo je kanadská severská krimi
                     
                                                         
                       Nechceme podnikať, chceme pracovať pre euroinštitúcie
                     
                                                         
                       Grabstein - nem. náhrobný kameň
                     
                                                         
                       O čase a bytí
                     
                                                         
                       Bratislavčanka
                     
                                                         
                       Ľudia nechápu.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




