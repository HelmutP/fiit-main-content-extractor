
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Dve štatistiky, ktoré médiá neprávom používajú voči vláde 

        
            
                                    30.4.2010
            o
            18:55
                        (upravené
                5.5.2010
                o
                14:34)
                        |
            Karma článku:
                13.97
            |
            Prečítané 
            12409-krát
                    
         
     
         
             

                 
                    Deficit verejných financií nie je za Fica rekordný, podobne ako nedôvera  voči súdom za Harabina
                 

                 
 ilustr. obr. flickr- Alpha Auer, aka. Elif Ayiter
   Kritizovať vládu za nezodpovednú rozpočtovú politiku či zhoršenie stavu v súdnictve je košér, ale v niekoľkých prípadoch sa objavujú ako podporné argumenty dve nekorektné čísla.  Najprv k deficitu:   .týždeň, Peter Schutz, súčasné číslo    Dramatické svedectvo, ako vláda gazdovala v roku 2009, dotvárajú dva údaje. Historicky rekordný deficit prekonal aj posledné revidované očakávanie...   Hospodárske noviny, Ficova vláda zlomila ten najhorší rekord, Ján Kováč   Vláda Roberta Fica si v krízovom roku 2009 pripísala dve neslávne prvenstvá. Najskôr zvýšila výdavky verejnej správy o vyše desať percent, čo je viac, ako ktorákoľvek iná krajina Európskej únie. Následkom toho "vyrobila“ najvyšší deficit v dejinách Slovenska, vyše štyroch miliárd eur.    TREND, Martin Jaroš a ČTK (reč je o štátnom rozpočte, v článkoch vyššie o celkových verejných financiách)   Na volebný rok 2010 vláda premiéra Roberta Fica (Smer-SD) naplánovala rozpočet s rekordným schodkom 3,75 miliardy eur.   Novinári berú ako rekord najvyššie nominálne číslo deficitu. Porovnanie nominálov ale nemá veľký praktický význam. Prečo potom tiež nenapíšu, že minulý rok boli historicky najvyššie priemerné platy v SR? Rekordné dôchodky? A že tento rok budú padať ďalšie rekordy vo výške príjmov? Deficity je vhodné porovnávať k sile ekonomiky, k HDP. Z pohľadu finančnej stability a schopnosti splácať je dobré volať deficit rekordne vysoký vtedy, keď je aj najvyššie riziko problémov.  Náš minuloročný deficit 6,8% bol historicky nadpriemerne  vysoký, to platí, ale nie rekordný. A rovnako nie je rekordný plán na budúci rok. V rokoch 1993, 2000 a 2002 bol deficit vyšší – 7,2%, , 12,3%, resp. 8,2% HDP. Viac ako emocionálne hrať na „rekordnú“ nominálnu výšku je podstatné riešiť, že rastie dlh štrukturálny (očistený o ekonomický cyklus) a že u politikov nevidno návrhy na jeho znižovanie.  Pri súdnictve sa zase šermuje nízkou mierou dôvery, či už to robia novinári alebo citovaní experti:  Pravda, Ľuboš Kostelanský, Justíciu zahalil tieň Štefana Harabina   Vláda Roberta Fica urobila v porovnaní s vládou Mikuláša Dzurindu najväčší krok späť v oblasti justície. Éra ministra za HZDS Štefana Harabina nepriniesla ľuďom to, čo od súdov očakávali.... Vlaňajší prieskum Európskej komisie ukázal, že súdnictvu dôveruje iba 30 percent Slovákov. Priemer EÚ je 48 percent.   aktuálne.sk, Iniciatíva Za otvorenú justíciu chce ozdraviť súdnictvo, Jakub Filo   Signatári upozorňujú, že dôveryhodnosť súdnictva medzi občanmi klesla na historické minimum. "Súdnictvo je vo fáze, keď dôvera je nižšia ako 30 percent," vyhlásila Javorčíková.   Áno, minulý rok bolo nameraných 29%. Ale ako ukázala analýza k tomu Eurobarometru, tá nízka dôvera k súdnictvu tu bola už za Lipšica. Vôbec žiadnu zmenu v dôvere pri prechode z Lipšica na Žitňanskú a na Harabina ten prieskum (napodiv) neukazuje.      Kritiku v médiách potrebujeme, ale s korektnými argumentami.   Titulok dňa: „Kyjev dal Krym priľahko,“ znie včerajší titulok k SME článku, v ktorom Mirek Tóda neutrálne vysvetľuje kontext prenájmu krymskej námornej základne Rusku. Prečo teda titulok automaticky súhlasí s názorom opozície? Nikde v článku nie je rozobraté, či cena bola primeraná alebo nie.  Titulok dňa 2: „SNS a SDKÚ sú v Bratislave rovnako silné,“ hlásalo v piatok SME.   Pritom šlo len o to, že:      ... prekvapivejšia je podpora SNS v Bratislavskom kraji, kde žije zhruba 11 percent slovenských voličov. Až 22 percent zo sympatizantov SNS pochádza práve z tohto kraja. Je to teda rovnako ako v prípade SDKÚ, ktorá má tradične najsilnejšie zázemie v hlavnom meste.   Keďže ale SDKÚ je celkovo v národných preferenciách 1,5 až 2krát silnejšia ako SNS, rovnako bude silnejšia aj v Bratislave. V SME si zrejme mizerný titulok uvedomili, lebo online už článok premenovali na „Bratislava je aj baštou SNS, nielen SDKÚ.“   Družba TA3 a Martinákovej pokračuje?: TA3 sa s političkou Zuzanou Martinákovou mali dlhodobo podozrivo zadobre (pozri moje porovnania v roku 2008). Čitateľ ma upozornil, že tomu nemusí byť koniec. Reportérka Jana Pišteková v minulotýždňovom 1,5 minúty dlhom príspevku dala priestor len predstaviteľom Martinákovej strany. Pritom šlo o tvrdý útok na SDKÚ a jej programové ciele, no o názor z druhej strany spravodajská televízia záujem nemala. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (66)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




