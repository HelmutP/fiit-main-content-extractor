
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Satko
                                        &gt;
                Nezaradené
                     
                 SS11. Starý chleba. 

        
            
                                    17.5.2010
            o
            17:18
                        (upravené
                20.12.2013
                o
                18:55)
                        |
            Karma článku:
                21.68
            |
            Prečítané 
            6324-krát
                    
         
     
         
             

                 
                    Obchodík s regálom na chlieb za pultom, hliníkovými kanvicami s mliekom a v sáčkoch z nebieleného  papiera muka. Volali to „U Eriky" podľa krstného mena predavačky.
                 

                 Keď som došiel zo školy, babka mi dala peniaze a poslala pre chleba. Veľakrát ho nemali a musel som isť ešte raz. Niekedy som mal šťastie a bol som pri tom keď poobede naviezli druhu várku. Obdivoval som vodiča a jeho závozníka, ako nacuvali zadnými dverami skriňového garanta k dverám obchodu. Keď otvorili dvere zavoňalo v celom obchode dobro. Vždy sa niekto z dospelých našiel a pomohol im vytvoriť reťaz a po dvoch bochníkoch chlieb hádzať k pultu, kde ho Erika odoberala a ukladala do regálov. Sníval som o tom, že aj ja budem raz  stať v reťazci, chytať a hádzať chlieb. Horúci, voňavý, rascový a vyrážkový. Keď som sa s nim ako slimák pomaly vracal k babke, okusoval som popraskanú kôrku, odtrhával ju prstami z konca. Stláčaním som skúšal, kde je pod kôrkou veľká bublina, aby som ju odhalil a odštibral. V zime som si ho prikladal pod bundu na hruď aby ma zohrial a dával pozor, aby mi naň sopeľ netiekol . Doniesol som ho k babke taký poobhrýzany, až mi bola hanba jej ho podať. „Kde si bol tak dlho? Teba pre smrť poslať!" hrešila ma. Ale potom, duša dobra, ho prežehnala, odkrojila ten  dokaličený koniec, natrela vrstvou masti. Ja som si masť posypal kryštáľovým cukrom. Topiacu masť som si olizoval na zápästí aby mi netiekla za rukáv a krehký stred krajca sa ako naschvál  stláčal pod čerstvou a chrumkavou kôrkou, ktorá sa húževnato bránila odkusnutiu.  Až sa mi gundže robili za ušami.   Obchodík zbúrali, pekáreň zmenili na hotel a babka ma čaká vo večnosti. Svet a chlieb sa zmenili, len vo mne zostala chuť a vôňa toho starého pezinského chleba a tváre ľudí, s ktorými som ho jedával. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (105)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Satko 
                                        
                                            Kronika duší - kompletka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Satko 
                                        
                                            Kronika duši - návod na obsluhu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Satko 
                                        
                                            Záhadní Cyril a Metód
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Satko 
                                        
                                            Zmizík
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Satko 
                                        
                                            Miklošove stroncium.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Satko
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Satko
            
         
        satko.blog.sme.sk (rss)
         
                                     
     
         antik.pk@gmail.com 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2389
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




