

 Neuveriteľné je, že o zodpovednosti nám tu ide rozprávať človek, ktorý podpisoval svojho straníckeho šéfa v parlamente a pritom vravel, že nič také nerobil, aj keď ho novinári nasnímali kamerou. 
 O zodpovednosti a slobode nám tu ide rozprávať predstaviteľ SNS, ktorá na jednej strane má plné ústna národných záujmov a výchovy k vlastenectvu a na druhej strane jej ministri boli odvolaní za emisné kšefty , nástenkové tendre a iné aktivity  - všetky asi v záujme ochrany národných záujmov a v prospech nás všetkých - ako radi vravia na svojich tlačovkách. 
 Dôvody, pre ktoré túto zmenu SNS navrhuje, je napr. slabá vymožiteľnosť práva, nepotrebné študijné odbory na univerzitách alebo nekorektní novinári. 
 Takže k tým 3 bodom: 
 Vymožiteľnosť práva - pán Rafaj asi zabudol spomenúť, že za celé 4 roky jeho strana v tejto vláde neurobila nič preto, aby sa vymožiteľnosť zlepšila, práve naopak. Rešpektovala rozdelenie rezortov a vo svojich si robila rozkrádačky - čo ich tam napr. po voľbe Štefana Harabina za predsedu NS a pod, veď to je rezort HZDS. Ani keď poslanci SNS mohli ukázať ako si vážia svoju zodpovednosť ako člena parlamentu a morálku, tak nehlasovali za vydanie poslanca Kvorku zo SMERu na trestné stíhanie - toľko k vašej zodpovednosti páni z SNS. 
 Výber študijného odboru je slobodným rozhodnutím každého študenta - žiaľ, je faktom, že v súčasnosti ide viac o titul než o vzdelanie. VŠ v každej väčšej dedine tomu len napomáha, ale každý študent si sám vyberá školu a odbor podľa svojich preferencií a štát mu nemá čo do toho zasahovať. Veď aj keď mladý človek ukončí VŠ, musí sa sám obracať aby našiel uplatnenie a je úplne jedno či v odbore ktorý študoval alebo niekde inde - záleží na jeho schopnostiach. Ministerstvo školstva bolo posledné 4 roky rezortom SNS a minister Mikolaj tento stav nijako nezlepšil. 
 Nekorektní novinári - SNS by asi potom navrhla zriadiť komisiu, ktorá by na základe svojho mandátu rozhodovala, ktorí novinári píšu korektné články a ktorí by nemali publikovať. Neviem čo viac môže pripomínať cenzúru v totalite. 
 Osobne som rád, že podobné úlety pána Rafaja a celej SNS zostávajú nateraz len teoretickým želaním ich tvorcov a majú ešte ďaleko k presadeniu. 
 Pán Rafaj, podobne ako vlastenectvo, ani zodpovednosti cez zákon nikoho nenaučíte, to buď človek cíti a má v sebe, alebo jednoducho nemá. Sami ukazujete, kde je tá vaša zodpovednosť a ako účelovo ju používate, rovnako ako to vaše vlastenectvo. 
 Prajem Vám páni z SNS, aby ste sa raz prebrali. 
   

