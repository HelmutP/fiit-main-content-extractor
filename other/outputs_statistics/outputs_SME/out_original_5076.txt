
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Theology Today
                     
                 Vatikán: Reforma ako pokánie? 

        
            
                                    23.4.2010
            o
            7:30
                        (upravené
                24.4.2010
                o
                0:30)
                        |
            Karma článku:
                10.58
            |
            Prečítané 
            3308-krát
                    
         
     
         
             

                 
                    Obhajoba a vysvetľovanie okolností jednotlivých prípadov pedofílie či lepšie povedané zneužívania mladistvých duchovnými rímskokatolíckej cirkvi naráža hneď na niekoľko úskalí. Argumentácia, že podobné škandály sa dejú aj v inom ako cirkevnom prostredí, je šikovným úhybným manévrom.
                 

                 
Falbo: Jeden svet, jeden Boh...archív autora
       Prípady sexuálneho zneužívania v inom prostredí sa určite diali, no nie sú známe kroky týchto inštitúcií, ktorými by sa snažili prípady pedofílie utajiť, zakryť a systematicky tak docieliť ich utajenie pred verejnosťou a orgánmi činnými v trestnom konaní. V škandále, ktorý momentálne sužuje mocenské špičky rímskokatolíckej cirkvi ide totiž o to, že prípady zneužívania mladistvých v cirkevných inštitúciách boli cieľavedome a systematicky utajované a riešené len podľa vnútorných smerníc rímskokatolíckej cirkvi. Označenie týchto prípadov pápežským listom motu proprio za pápežské tajomstvo bolo implicitne jeho adresátom - biskupom -  jasné, že nikto iný prípady nesmel prejednávať. Nielen miestny diecézny biskup v právomoci ktorého také prípady boli dovtedy. O civilnom súde ani nehovoriac. Ak biskup mal o kriminálnom konaní svojich podriadených kňazov vedomie, nebol žiadnym vnútorným predpisom viazaný oznámiť zločin či podozrivé konanie civilnej autorite. Naopak. Viete si predstaviť, žeby biskup X.Y., ktorého menoval pápež J.R. či jeho predchodca K.W. porušil toto „pápežské tajomstvo" a utekal by za strážmajstrom  na miestnej policajnej stanici aby mu toto „pápežské tajomstvo" prezradil?   Adresná zodpovednosť   Jadrom problému momentálne je, že Vatikán nechce vyvodiť zo systémového zatajovania škandálu historických rozmerov adekvátnu a adresnú zodpovednosť. V priemerne fungujúcej organizácii rozmerov a významu rímskokatolíckej cirkvi by bolo vypočutie legitímneho volania po štrukturálnych a personálnych zmenách jedinou cestou riešenia vzniknutej situácie. To, že pre bežného človeka nie je jednoduché vidieť potrebu reformy štátu nazývanom Vatikán alebo Svätá stolica, nemá nič spoločne s tým, či sa cirkev chce alebo nechce reformovať. Ona sa reformuje už od svojho počiatku. Teológovia nazývajú cestu cirkvi dejinami latinským pojmom ecclesia semper reformanda - sústavne sa reformujúca cirkev. Stačí ak sa pozrieme na podobu cirkvi v prvom storočí, v 15. storočí a v 21. storočí. Okrem k modernizácii či k tzv. „zdnešneniu" (tal. aggiornamento) dotlačeného katolíckeho kresťanstva je tu aj veľa skutočne radikálne či menej radikálne reformovaných kresťanských komunít, v ktorých sa odovzdáva kresťanská tradícia a sú životaschopné aj mimo spoločenstva s Rímom.    Vatikán naveky...?   Aj preto je Vatikán len prechodným štádiom a pomerne mladou historickou epizódou jej podoby v čase a priestore. Kresťanstvo je väčšie ako Vatikán. Zodpovední a osobne zrelí veriaci vedia svoju vieru žiť aj mimo jeho barokového objatia. A pateticky musím povedať aj to, že Boh má väčšiu moc ako bývalý prefekt kongregácie, ktorá nesie hlavnú zodpovednosť za to, ako sa tieto škandály desaťročia "prekrývali a tajili". Prefektom tejto kongregácie bol dlhé roky práve Jozef Ratzinger.    Mea culpa 2000   Ak dobromyseľní veriaci či cirkvi naklonení analytici nevnímajú ako čudný fakt, že sa cirkev o 400 rokov ospravedlní za to, ako sa dnes správa, je to ich legitímne rozhodnutie. Necítia sa poškodení, preto nemajú na náprave škody a zmene pomerov, ktoré k nej viedli záujem. Solidarita s obeťami v duchu pavlovskej solidarity zdravých častí tela s jeho trpiacimi časťami im zrejme veľa nehovorí. Takéto oneskorené „ospravedlnenie"sa „konalo" napr. aj v prípade rektora Karlovej univerzity Majstra Jana Husa, či pri spätnom hodnotení teologického rozmeru zodpovednosti oboch strán v prípade Luterovej reformácie.  Je to však v dnešnej dobe už eticky neakceptovateľné.    Problémy je potrebné riešiť v čase a priestore, ktorý ešte dáva možnosť na nápravu krívd, nielen na alibistické: "prepáčte!" ako sa to udialo v roku 2000. Bolo to síce chvályhodné, no život upáleným, perzekvovaným a zničeným obetiam to už nevrátilo. Vedia sa takíto obrancovia akože „dobrého" mena cirkvi vžiť do kože zneužitej obete či jej rodičov?  Vedia si napr. nasimulovať danú situáciu a žiť s vedomím faktu zneužitia pár týždňov? Aký text by asi potom títo obrancovia skamenelej cirkevnej administratívy následne napísali na danú tému..?    Lojalita cirkevných aristokratov...?   V absolutistickej monarchii sú všetci šľachtici vazalmi absolutistického monarchu. Preto dostali svoje léna. Nič viac a nič menej neplatí ani o monarchovi Vatikánskeho štátu a systéme, ktorý vedie, bráni a ospravedlňuje. Práve preto ho musia všetci jeho nominanti brániť aj vlastným telom. Žiaľ iná interpretácia sa momentálne neponúka. Rozbíjanie celého tohto smutného balíka na tzv. drobné pochybenia jednotlivcov môže ešte kde tu zaujať a odpútať pozornosť od podstaty problému. No v zásade je to len  leštenie zakalenej šošovky nevidiaceho oka, ktoré nikomu nepomôže. Tí ktorí toto usporiadanie vykonávania moci v rímskokatolíckej cirkvi neústupčivo obhajujú majú snáď konečne na stole tisíce dôkazov potvrdzujúce slová lorda Actona: Každá moc korumpuje. Absolútna moc korumpuje absolútne. Politicky nekorektné vyjadrenie Leonarda Boffa o tom, že pápež si mýli Bavorsko s Vatikánom a Vatikán s celým svetom je mrazivý bonmot, ktorý je k pravde bližšie ako sa na prvý pohľad zdá. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (344)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




