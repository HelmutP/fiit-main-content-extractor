
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladi Neuschlová
                                        &gt;
                Za hranicami...
                     
                 Volunteering alebo dobrovoľníctvo v Afrike III. 

        
            
                                    6.5.2010
            o
            22:32
                        (upravené
                7.5.2010
                o
                10:43)
                        |
            Karma článku:
                7.75
            |
            Prečítané 
            2097-krát
                    
         
     
         
             

                 
                    Rada by som nadviazala na moje predchádzajúce 2 články. Z prašných ulíc okrajových štvrtí v Nairobi, späť do škôl plných malých usmiatych tvárí, kde i tie najväčšie problémy doma vo vyspelom svete už nevyvolávajú strach či pochybnosti. Lebo pokiaľ je človeku dopriate zdravie, má prácu ktorá ho baví a vďaka nej pravidelný príjem na účet, a hlavu a srdce na správnom mieste.... niet sa na čo sťažovať!
                 

                 
  
   Chcem sa ospravedlniť ak tí, ktorí mali chuť si prečítať ďalšie pokračovania môjho krátkeho dobrovoľníckeho príbehu museli dlhšie čakať. Na písanie musíte mať chuť, priestor a čas, v hlave jasno a tiež vôľu deliť sa s tým, čo nosíte v sebe. A aj keď od septembra 2009 ubehlo už skoro 9 mesiacov, nie je pomaly dňa, kedy by sa do môjho bežného dňa nevkradla aspoň jedna malá myšlienka na Afriku.. pretože ak raz získate takú skúsenosť, už nikdy niet cesty späť. Aj napriek tomu, že si tiež rada doprajem niečo na seba v jednom z tých moderných nákupných centier, alebo bookujem ďalšie výlety po Európe, či rozmýšľam ekonomicky a manažérsky minimálne 10 hodín denne, plánujem a rozhodujem o veciach dôležitých či bezvýznamných, aj tak si svoju Afriku nosím denne v sebe. Spája sa to s vďačnosťou, ktorú už častejšie aj vyslovím, či dám pocítiť ľuďom na ktorých mi záleží. Prelína sa to s pocitmi šťastia, ktoré mám, lebo viem, že môžem... ísť kam chcem, tešiť sa s z jarného slniečka, klebetiť s babami pri dobrom víne, zahrabať sa do postele s ďaľšou motivačnou knihou a nerobiť vôbec nič. Alebo len vypnúť všetky spotrebiče, otvoriť okno v prenajatom byte dokorán, a počúvať vonku večerný dážď...      Cesta do školy, v ktorej boli deti opustené i tiež zo slabších rodín (tak by sme to ponímali my, v skutočnosi pokiaľ si rodičia uvedomovali, že dieťa potrebuje vzdelanie a neposielali ho od rána do večera žobrať a zháňať si stravu, tak títo rodičia patrili ešte k tým gramotnejším a ako-tak zarábajúcim) trvala vždy asi pol hodinu. Lebo ja som chodila obkľukou - bála som sa ísť skratkou, kde by som v priebehu pár sekúnd mohla skončiť v jednej z tých plechových búd, a už by som bola fuč. Možno to boli len predsudky, ale na tej skratke zabili iné domáce dievča, a ja som jednoducho nemala chuť zbytočne riskovať. Načo, nepotrebovala som si nič dokazovať. Občas na mňa domáci hovorili, najčastejšie ponúkali nejaký tovar, inokedy chceli len pozdraviť. Po pár dňoch som si aj sem-tam niečo kúpila, nejaký banán, pomaranč, či vlastnoručne robené hranolky za 10 shilingov. To bolo asi 5 centov myslím. Keby videla ten proces prípravy naša hygienická stanica, ktorá vás naháňa i za každú chýbajúcu čiarku na popise výrobku, asi by onemeli (občas by som im to priala, predtým ako niečo vyslovia, zamyslieť sa nadtým, aké ťažké je pre dnešných pidi-podnikatelov prežiť, ale to je iná téma). Ale chutilo, ono je vždy paradoxom, že v takýchto krajinách ten žalúdok strávi čokoľvek, a nič sa nedeje, a doma zájdete do reštiky na rýchly obed, a vzpamätávate sa z toho pár dní :).      Pri príchode do školy sa mi vždy trošku uľavilo. Myslím, že to bolo aj tou energiou, ktorá tam bola, pochádzajúc od detí, učiteľov, či i nás "dobrovoľníkov", keďže v každom z nás musí byť aj kus srdiečka, keď sa rozhodnete pre takéto dobrodružstvo. Vyučovanie väčšinou bežalo od pol ôsmej, aj keď predtým sa detičky v kostole (nazvime tento ďalší skromný drevený príbytok s plechovou strechou stánkom Božím) každé ráno pomodlili. Pre tých, ktorý doteraz nečítali ostatné 2 časti tohto príbehu - škola a sirotčinec boli vo vlastníctve manželského páru, dvoch pastorov. V sirotčinci bolo vtedy 46 detí, do školy ich chodilo spolu okolo 100.  Nechodím doma do kostola.. nemám potrebu, ale napriek tomu si myslím, že svojho Boha si nosí každý v sebe. Občas som si šla sadnúť sem na lavicu, v tichu rozmýšľala, alebo pozorovala ženy postihnuté na AIDS, ako tu vytvárajú nádherné náušnice a náhrdelníky, pričom korálky robili z časopisov. Už menej sa mi páčilo, že sa toto všetko predávalo veľmi predražené väčšinou nám "dobrovoľníkom", ale vravím si prečo nie, keď je zákazník ochotný zaplatiť. Aj ja mám jeden na pamiatku. A v konečnom dôsledku tieto ženy iný zdroj príjmu nemali, ako HIV pozitívne mali taký čudný náramok, ktorý na ich chorobu upozorňoval, a normálnu prácu by si nezohnali. Takto mali asi väčší príjem ako priemerný Afričan - ale kto by im závidel. Inak som sa dozvedela, že i AIDS sa už sa vlastne dá liečiť. Len si tú liečbu môže málokto dovoliť, keďže vírus stále mutuje, a pacient potrebuje neustále iné lieky. Ono človek počul všeličo, však na to sú tam tie večeri s ľuďmi z celého sveta, keď sa zídete "doma" v jednej spoločenskej miestnosti, a buť si osamote čítate knižku a počúvate svoje MP3 (či tam už skoro každý mal Ipod-a :)), alebo sa zapojíte do dlhých diskusií na hocijakú tému... a vtedy je jedno či ste z Austrálie, Ameriky, zo Škótska či Slovenska... Sme spolu v Afrike, a máme niečo spoločné. Každý máme isto svoj vlastný dôvod prečo Afrika a prečo v tomto okamihu, ale napriek tomu existuje nejaká malá súdržnosť, aj keď som si vždy uvedomovala, že pravdepodobne už nikoho nikdy v živote neuvidím. Možno tak cez Facebook :).           Vyučovanie beží, v triedach je 7-20 detských hlávok, jedni sa usmievajú, iní mračia. Ale každý, každý chce, aby sa na neho na chvíľu uprela pozornosť. Vtedy je niečím výnimočný, vtedy je vnímaný individuálne, vtedy sa učiteľ venuje len jemu.. V najstaršej triede cca. 11-13 ročných žiakov je len 7 detí, v tej predškolskej asi 15. Mne sa viac páčilo u tých starších, keď som skúšala upútať ich pozornosť, keď sa pýtali, keď som im chcela trošku po svojom vtlačiť do hlávky, že ONI raz budú rozhodovať o svojom osude, a jedine vzdelanie im otvára všetky dvere k tomu. Už som raz písala, že tu v priemernej Afrike, rozdiel medzi životom v tej najväčšej chudobe (ktorú si vlastne ani my nedokážeme predstaviť) a tým o niečo lepším, tú priepasť spája jedine vzdelanie, a raz denne niečo teplé do žalúdka, aby človek najskôr prežil. Tieto deti majú teda lepšiu štartovaciu dráhu, ako iné milióny detí po celej Afrike. Áno, milióny.   Keďže som bola v škole, ktorá mala dosť učiteľov, dohodla som sa s riaditeľkou, že nemám chuť zasahovať do bežného vyučovania, a zastupovať týchto expertov. Bolo by to k ničomu, a ja som chcela tieto deti obohatiť o niečo iné. Matematika, svahilstina, anglictina.. na to všetko mali svojich učiteľov. Trošku prísnych na môj vkus, ale v každom z nich som aj tak zahliadla aspoň raz dojatý úsmev, väčšinou keď sledovali deti cez prestávky, a nikto sa nesústredil na nich. A ja rada pozorujem, a vnímam veci okolo seba.. také, aké sú, keď sa za nič iné neprezliekajú. Poprosila som o pár polhodiniek s každou z 3 najstarších tried, pričom sme si dohodli jasný rozvhr. Už tu na mňa kukali trochu inak, keďže som sama zažiadala o tento plán. U iných "dobrovolníkov" to bolo často tak, že ich riaditeľka poslala do nejakej triedy, učiteľ sa z donútenia usmial, prestal robiť to čo mal v pláne, a prenechal priestor "bielym". Tak to bolo v mojej škole, kde bol dostatok učiteľov. Samozrejme boli školy, kde chýbali učitelia, a tam bola každá inteligentnejšia hlavička prínosom - dostala do ruky min. 10-ročnú školskú učebnicu, pochádzajúcu asi odniekiaľ z Ameriky alebo Anglicka, a vyučuj.          Počas týchto polhodiniek, som prišla s témami ako čas, trochu zemepisu, filozofie či etiky, tvorivé myslenie, hry a výtvarná. Aj pre mňa bolo zo začiatku nepochopiteľné, prečo 6-ročné deti pchajú penovú kocku do otvoru na trojuholník (a to už ovládali pár slov po francúzsky). Predstavte si naše 1-3 ročné dieťa, koľkými inteligetnými hračkami ho obklopujeme. Tu začína ten rozdiel.    Dobrá angličtina, bystrá matematická hlavička.. ale čo to znamená čas? Koľko hodín ma vlastne deň? A ako si ho mám naplánovať, ak mám byť o 8 hodine v práci, predtým musím zaviesť mamu do nemocnice, a na to potrebujem 30 minút, o 13 hodine musím skočiť na poštu, ... takto som im vymýšľala hry. Snažila som sa byť praktická, ale aby si z toho času stráveného so mnou aj niečo v hlávke odniesli. Čo je to Afrika, koľko nás (ich) Keňanov vlastne je, aké štáty sú v okolí, čo je to Európa, Amerika... Napodobňovali sme zvieratá, hádali hádanky, nechala som každého hovoriť pred triedou, a užívala si to. Pol dňa som vystrihovala z časopisov písmenká a obrázky, rozdávala farebné papiere a lepidlá, voskovky.. a po škole sme si lepili naše mená. Najviac času zabralo to, že oni výsledky svojej práce chceli mať nalepené na stenách tried. Tak som potom asi hodinu len oblepovala triedy, počúvala krik a radosť a smiala som sa s nimi.       Veľmi sa mi do duše počas vyučovania zaryla jedna myšlienka... Rozprávali sme sa o ľuďoch, pýtala som sa v každej triede, čo nás spája a čo nás rozdeľuje. Automaticky si to deti pretransformovali na "bielych" a "čierny", ale niet im čo vyčítať.. občas treba upustiť od svojich upätých predstáv, treba nechať veciam voľný priebeh, treba vnímať a počúvať. Začali sme tým, čo nás rozdeľuje... To sa asi aj bežnom živote ľahšie, hľadá. Hľadáme rozdiely, nové riešenia, svoje nápady prezentujeme ako tie najlepšie... namiesto toho aby sme si častejšie spolu sadli, a prestavili jednotné riešenia, či aby sme sa navzájom podporovali.  Iná farba pokožky, vlasy, jazyk, kultúra.. snažila som sa im vysvetliť prečo niekde nejedia psov, alebo rozdiely v obliekaní... išlo to. Keď sme začali menovať to čo nás všetkých ľudí spája, ostala som prekvapená vo všetkých troch triedach. Každná z nich, mi totiž na 2. či 3. mieste menovala "smrť". Deti toto slovo vyslovovali s ľahkosťou, prirodzenosťou, žiadny strach, či smútok. Neviem, prečo ma to tak zasiahlo. Asi preto, že považujem za smutné, že sa týchto úžasných, ešte ničím nezkazených bytostí smrť tak bytostne a denne dotýka. Vidia smrť doma i na uliciach, mnohé z týchto sirôtkov nemajú už ani jedného rodiča, smrť je tu na dennom poriadku, choroby, či hlad.. často ale aj ľudské násilie, prežije ten silnejší.      Písali sme eseje. Vyhlásila som na víkend súťaž, a vyzvala deti, ktoré majú chuť urobiť niečo naviac (okrem domácich úloh), aby mi napísali o svojich snoch. Napriek tomu, že som vysvetlila rozdiel medzi "snom" ako prianím, a "snom" ktorý sa nám môže snívať v noci, aj tak som čítala oboje. Opäť ma trochu pichlo, keď som čítala, ako sa niektorým deťom sníva, ako im vykrádajú príbytok, či zabíjajú rodičov, alebo ako padajú zo stromov, ale už bolo na čase začať to brať ako fakt. Skutočný život je tu iný, ako si ho ja prežívam doma. A dúfam, že moje deti rovnako budú mať tú šancu, ktorú som mala ja. Ostatné sny boli o kráľoch, a bohatstve, a tiež veľa o budúcom povolaní.. či už manželky, alebo lekára. Bolo to milé čítanie. Myslím, že to mám ešte niekde schované doma.   V sobotu sa do školy nemuselo.. ale ja som mala chuť. Pozrieť, čo tieto deti bez rodičov robia počas voľna... Pranie, čistenie izieb, a žiadny iný program. Len 1 kuchárka a na pár hodín učiteľ na 46 detí. Z ktorých každé by si zaslúžilo len chvíľku byť v pozornosti jednej osoby, ktorá by im dala trochu lásky. Len jemu, pár minút len ja a ty. Nejde to. Obklopia vás ihneď so slovami "teacher", tie mladšie sa vás všade začnú dotýkať, staršie najskôr kukajú s nedôverou, čo tam robím v sobotu. Tie ľady prelomí pár pomarančov. Cestou sem som kúpila asi 20 pomarančov, však nech aj tieto deti majú sobotu. Šúpem ich spolu s kuchárkou, a najskôr nechápem, že sa ma pýta, že či to robí dobre... šúpanie pomarančov. Bože veď sme v Afrike! Môže mať okolo 40 rokov, a nikdy v živote nešúpala pomaranč. Rozdeľujeme ich na menšie a menšie časti, deti sa stavajú do 2 radov, a ja som si myslela, že budem mať z tohto radosť a dobrý pocit. Smiešne.. cítim sa hrozne, pri každom tom detskom "thank you" keď im podám trochu pomaranča, nedokážem sa asi ani poriadne pozrieť do ich láskavých očí, a rozmýšľam koľko ovocia tak do roka asi v mojej rodine vyhodíme, lebo sa proste pokazí, alebo prečo som kúpila 20, a nie 46 pomarančov, aby sme ich tu nemuseli deliť... však stáli smiešnu cenu. Hanbím sa. Som v Afrike a tieto deti jedia pomaranč (vlastne kúsok pomaranča) prvý krát v živote. Inak ich strava spočíva z ryže, zeleniny allias buriny, a občas nejakých placiek. Voda, ktorá naprší do nádrže.      Neodcudzujem, len som písala ako to bolo, a aké mal človek pritom pocity. A možno si niekto z toho zoberie aj niečo pre seba, a možno vôbec nie. Neviem... ale ak pôjdete, buďte pripravení sa rozdať, tisíc krát sa vám to vráti.. a kúpte 46 pomarančov :). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Vladi Neuschlová 
                                        
                                            Prečo občas cestovať sám...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladi Neuschlová 
                                        
                                            Urob si kavu, alebo caj...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladi Neuschlová 
                                        
                                            When a man loves a woman...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladi Neuschlová 
                                        
                                            Čarovný okamih...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladi Neuschlová 
                                        
                                            Cestou po Skotsku pokusala som sa zastavit....
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladi Neuschlová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladi Neuschlová
            
         
        neuschlova.blog.sme.sk (rss)
         
                                     
     
         Thousands of candles can be lighted from a single candle, and the life of the candle will not be shortened. Happiness never decreases by being shared... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1243
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Za hranicami...
                        
                     
                                     
                        
                            Z logistiky...
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Buď chlap
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




