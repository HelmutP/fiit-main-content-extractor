
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Trnovec
                                        &gt;
                Nezaradené
                     
                 Deň rodiny 15. máj 

        
            
                                    15.5.2010
            o
            13:15
                        (upravené
                15.5.2010
                o
                14:50)
                        |
            Karma článku:
                5.35
            |
            Prečítané 
            434-krát
                    
         
     
         
             

                 
                    Rodina má právo na spoločný život. Deň rodiny má upozorňovať na iniciatívy podpory rodinného života. Nestačí ísť na námestia a rozdať cukríky a balóny s predvolebnými heslami. Iniciatíva WORK-FREE SUNDAY (Za nedeľu bez práce) má prvý veľký úspech za sebou. 24. marca 2010 sa konala v Európskom parlamente v Bruseli prvá Európska konferencia na ochranu nedele bez práce.
                 

                     Konferencia bola pripravená na základe iniciatívy poslanca Európskeho parlamentu Thomasa Manna, viceprezidenta Výboru pre zamestnanosť a sociálne veci Európskeho parlamentu.   Hlavným rečníkom na konferencii bol komisár pre sociálne záležitosti László Andor, ktorý predložil výzvu, ktorá bude za krátko k dispozícii v rozličných jazykoch, aby ju mohli občania EÚ podpísať. Podpisy sú potrebné na podporu iniciatívy, ktorá sa predloží Komisii. Nová Lisabonská zmluva umožňuje takéto iniciatívy občanov, pre ktoré je potrebné zhromaždiť viac ako pol milióna podpisov. Cieľom Iniciatívy Za Nedeľu bez práce je doložiť žiadosť vyše milióna podpisov. Je vôbec prvou, ktorá využíva nové možnosti a podporuje občianske iniciatívy v EÚ.   Zo Slovenska sa konferencie zúčastnili poslankyňa EP Anna Záborská, ktorá už 2.2.2009 predložila iniciatívu spolu s poslancami Martinom Kastlerom, Jean Louis Cottignyom, Patrizio Toia a Konradom Szymańským na ochranu pracovného voľna v nedeľu ako nosného prvku európskeho sociálneho modelu a súčasti európskeho kultúrneho dedičstva.   Ďalším účastníkom zo Slovenska bol predseda Aliancie Za Nedeľu Peter Novoveský, ktorý hovoril o nerešpektovaní voľnej nedele v nových krajinách EU do tej miery, že sa bežne porušuje Zákonník práce. Zamestnávatelia využívajú nezamestnanosť na vytváranie nátlaku na zamestnancov, ktorí „dobrovoľne" pracujú v nedeľu.   Predseda Klubu mnohodetných rodín Stanislav Trnovec, hovoril o trvalom pracovnom čase bez dovoleniek a voľných nedieľ, ktorý majú matky a rodinný opatrovatelia starajúci sa vo vlastných rodinách o deti, bezvládnych, invalidov, chorých a starých. Túto prácu vykonávajú bez riadneho ohodnotenia, bez sociálnych istôt. Rodiny s jedným príjmom (zárobkom) sú vystavené oveľa väčšiemu riziku chudoby, ako sú ostatní občania. Aspoň jeden voľný deň, nedeľa, utužuje rodinné zväzky. Plénum informoval o akcii INVISIBLE WORK DAY - NEVIDEĽNÝ PRACOVNÝ DEŇ.   Konferencie NA OCHRANU NedeľE bez práce sa zúčastnilo asi 400 osôb. Organizátori vytvorili veľký priestor na diskusiu. Predstavitelia rozličných organizácií predniesli rôzne argumenty Za Nedeľu bez práce ako zdravie a bezpečnosť pracovníkov, zosúladenie pracovného a rodinného života, ochrana malých a stredných podnikov. Je zaujímavé, že podporné stanovisko iniciatíve Za Nedeľu bez práce vyjadrili predstavitelia celého spektra zastúpeného v Európskom parlamente.   Konferencie sa zúčastnilo asi 60 predstaviteľov z postkomunistických krajín. Viacerí z nich sa aktívne zúčastnili diskusie. Boli z rôznych organizácií, pričom veľa ich bolo na vlastné náklady. Predseda KMR upozornil organizátorov na túto skutočnosť. Účasťou by sa podporila iniciatíva.   Ministerstvo práce sociálnych vecí a rodiny SR vykonalo analýzu zákonov. Potvrdilo sa, že Slovensko je veľmi dobre legislatívne pripravené pre dodržiavanie voľnej nedele. Problém je ten, že Zákonník práce sa obchádza a sa dôsledne nedodržiava.   Informácie o konferencii sú na www.workfreesunday.eu, kde sa dá iniciatíva podporiť.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Je ešte Rakúsko katolícke?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Nepôjdem voliť! Prečo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Strata zdravého rozumu, alebo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Diera v železnej opone
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Trnovec
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Trnovec
            
         
        trnovec.blog.sme.sk (rss)
         
                                     
     
         Som slobodný človek. Sloboda je okrem života najfantastickejší dar, ktorý som dostal. Denne stojím v nemom úžase pred zázrakom života, tohto tajomstva sa neviem nabažiť. Každý deň je DAR, za ktorý nestíham ďakovať. Predseda Klubu mnohodetných rodín, viceprezident FEFAF, člen administratívneho výboru COFACE. Teda písať budem o rodine. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    133
                
                
                    Celková karma
                    
                                                5.58
                    
                
                
                    Priemerná čítanosť
                    749
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Je ešte Rakúsko katolícke?
                     
                                                         
                       Nepôjdem voliť! Prečo?
                     
                                                         
                       Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                     
                                                         
                       Strata zdravého rozumu, alebo?
                     
                                                         
                       Ako trestáme korupciu: Za miliónové tendre podmienka, za 10 kíl ryže 5 rokov natvrdo
                     
                                                         
                       Zmena ústavy je dobrou správou, cena je však vysoká
                     
                                                         
                       EÚ a tradičné hodnoty
                     
                                                         
                       Zajtra hrozí sedem kresiel pre SMER a s ním aj útok Bruselu na posledné zvyšky suverenity
                     
                                                         
                       10 dôvodov, prečo má Slovensko nádej
                     
                                                         
                       Kto je chudobný?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




