
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Foto Potočný
                                        &gt;
                I taká je politika
                     
                 Prieskum verejnej domnienky od Median.sk 

        
            
                                    22.4.2010
            o
            15:45
                        (upravené
                22.4.2010
                o
                16:50)
                        |
            Karma článku:
                11.53
            |
            Prečítané 
            3005-krát
                    
         
     
         
             

                 
                    Voličské preferencie sa časom menia. Reagujú na vážnejšie slová a skutky politikov, ich popularitu, predvolebné sľuby, šnúry mítingov či medializované škandály. Ale ako si máme vysvetliť, že dve agentúry zverejnia diametrálne odlišné výsledky krátko po sebe? Zmenila sa verejná mienka tak výrazne za krátky čas, alebo sú agentúry vo svojich prieskumoch ozrutne nepresné?  Dá sa im vôbec veriť? Dá sa vôbec veriť štatistike?
                 

                 
 shooty.sk (jún 2006)
   Prieskum agentúry je vlastne akýmsi zrkadlom spoločnosti. Výsledok prieskumu nie je identický s verejnou mienkou samotnou, je len jej odrazom v zrkadle nastavenom agentúrou. Ak by sme chceli zrkadlo, čo ponúka absolútne verný a ostrý obraz, museli by sme sa opýtať všetkých oprávnených voličov. To je žiaľ časovo a finančne extrémne náročné. Takýto prieskum sa preto koná len raz za štyri roky a aj to na účet štátu. Dá sa však hocikedy nastaviť verejnej mienke zrkadlo zahmlené, v ktorom bude obraz mierne rozostrený. Je to síce za babku ale nemôže to robiť hocikto. Treba k tomu okrem tej babky aj filipa, precíznosť a silný charakter. Inak sa veľmi jednoducho stane, že zrkadlo bude nielen zahmlené ale aj pokrivené a obrazu v ňom bude okrem ostrosti chýbať aj vernosť.   V krátkom časovom období uskutočnili tri renomované agentúry štyri prieskumy. Sú to štyri rozostrené obrazy takmer identickej verejnej mienky, ktoré sa ale navzájom výrazne podobajú. Ich prekrytím dostaneme obraz, ktorý som zverejnil v predposlednom stĺpci tabuľky. Pomedzi to zverejnila agentúra Median.sk obraz tej istej verejnej mienky, ktorý sa ale už na prvý pohľad líšil od oných štyroch. Verejná mienka tu má hruď a dvojkríž na nej o niečo viac nafúknuté. To sa dá ešte bez problémov pochopiť ako reflexia vlasteneckého zákona. Ak by sme ale mali nájsť päť rozdielov, spomenutá veľkosť hrudníka by bol ešte najmenším z nich.     
 








     1.MVK 2.Focus 3.Polis 4.TA3 (F) Priemer 1.-4. 5.Median     Účasť v % 73,9 66,8 64,1 70,5 68,8 31,4+31,1 + 9,4?   
z počtu 
 1 041 
1 077 
 1 280 1 040   1 495 
respond. 
   v období    
 22.2.–1.3. 2.–8.3. 13.–16.3. 7.–13.4.   1.–28.3.     Smer-SD 37,1 38,4 38,0 36,8 37,6 44,0 (%)   SDKÚ-DS 12,8 14,3 12,7 13,4 13,3 13,1 (%)   KDH 12,7 9,7 13,4 8,6 11,1 13,1 (%)   SNS 4,9 6,3 5,2 8,6 6,3 7,0 (%)   ĽS-HZDS 6,6 5,4 4,4 5,4 5,5 6,1 (%)   SMK 6,0 5,2 5,9 5,1 5,6 3,9 (%)   Most-Híd 5,2 6,9 6,7 5,1 6,0 2,3 (%)   SaS 9,2 8,6 8,0 11,5 9,3 4,3 (%)      Ako prvé by do očú udrela asi veľkosť úst, pažeráka, žalúdka, žlčníka a vôbec celej tráviacej sústavy. Všetko z menovaného je v tomto obraze natiahnuté Smerom do šírky, výšky a hĺbky takmer o pätinu. Výrazný rozdiel je aj v dolnej časti obrazu a síce vo veťkosti chodidiel. Kým členky sú scvrknuté na čosi vyše dvoch tretín, klenba skrátená hádam na tretinu. Na záver si nemožno nevšimnúť ani nie polovičné čelo, keďže všetky posledné obrazy milej, i keď nie veľmi fešnej verejnej mienky ho mali... ako to povedať férovo... neprehliadnuteľne vysoké.   Všetko teda nasvedčuje tomu, že agentúra Median.sk nám cez tlačové agentúry a nekritický verklík médii (česť výnimkám) nedávno ponúkla výrazne neostrý ak nie priam deformovaný obraz. Ak pripustíme, že zrkadlo Medianu bolo pokrivené, pravdepodobne sa tak stalo vďaka kombinácii týchto troch (viac či menej závažných) faktorov: nekonkrétnosť mapovanej situácie, sugestívnosť ankety či nedostatočná reprezentatívnosť vzorky.   Nekonkrétnosťou mám na mysli napríklad nejednoznačne definovanú otázku. Väčšinou je jej znenie v zmysle: Ak by sa voľby konali tento víkend... Účelom takto postavenej otázky je, pokúsiť sa osloveného prinútiť, aby teraz učinil rozhodnutie na základe aktuálneho stavu spoločnosti, vlastného vedomia a svedomia. Aby sa nestalo, že niekto odpovie zo zotrvačnosti tak ako by bol odpovedal pred pol rokom alebo nebodaj pred 4 rokmi v posledných voľbách. Pre názornosť uvádzam fiktívny príklad nie veľmi šťastne postavenej ankety.   1) Keby zajtra boli voľby do Národnej rady SR, zúčastnili by ste sa ich?      určite áno   skôr áno   skôr nie   určite nie   neviem     2) Pokiaľ by ste sa ich zúčastnili, akú stranu alebo hnutie by ste volili?      Smer-SD   KDH   SNS   ĽS-HZDS   SDKÚ-DS   SMK   SDĽ   Slobodné Fórum   ANO   SOP   Paliho Kapurková   Most-Híd   SaS   ...     U niekoho takto postavená anketa môže vyvolať pocit, že má zahlasovať aj keď sa vlastne vôbec nerozhodol, či vôbec pôjde voliť. Anketa ho k tomu nenúti keďže môže odpovedať aj niekde medzi áno a nie. Iný by zase v takejto ankete nehlasoval za nikoho, lebo zrovna zajtra má dôležitú služobnú cestu. I keď presne vie koho bude voliť vo voľbách. Za istých okolností môže byť problematické aj to, že v inej skutočnej ankete Medianu trvá zber odpovedí celý mesiac, a teda že časť voličov zachytia ovplyvnených nejakou  novou spoločenskou klímou alebo fenoménom.   Sugestívnosť ankety nastáva vtedy, ak je odpoveď osloveného  výraznejšie ovplyvnená anketárom či samotnou anketou. Problémom v onej ankete môžu byť napríklad sugestívne zoradené možnosti odpovede na druhú otázku, prípadne nebodaj sugestívnejší prístup zle vyškoleného anketára. Už vôbec si neviem predstaviť, akým smerom takto postavená anketa ovplyvní maďarsky hovoriaceho spoluobčana, ktorý nevie takmer vôbec po slovensky.   Zabezpečiť dostatočnú reprezentatívnosť vzorky je zo všetkého najťažšie. Treba zabezpečiť, aby relatívne malá vzorka, niečo vyše tisíc oslovených, proporcionálne verne reprezentovala všetkých občanov Slovenska, starších ako 18 rokov. Spomenuté deformácie obrazu verejnej mienky od Medianu naznačujú, že ich zrkadlo mohlo byť výraznejšie pokrivené a odchýlené od vzdelanejších ľudí smerom na vidiek, od mladších smerom k starším a od juhu smerom na sever. Príklad ukážkovo pokriveného zrkadla aj keď úplne iným smerom nájdete na Facebooku.   Nadôvažok sa pokúsim dodať svojim doterajším slovám ešte trochu váhy malou úvahou o miere ostrosti obrazu. Šikovní matematici tým určujú pravdepodobnú veľkosť chyby, ktorej sa dopustili. Čím väčšia je reprezeatívna vzorka tým menšia vychádza chyba, rozumej ostrejší obraz. Pri vzorkách rádovo tisíc respondentov sa pracuje s takzvaným 95 percentným intervalom spolahlivosti. Mnoho z nás sa mylne domnieva, že podľa agentúry Focus by voľby v druhý marcový týždeň dopadli veľmi blízko novinármi medializovaných čísel. V skutočnosti agentúra Focus tvrdí, že s 95 precentnou pravdepodobnosťou by boli výsledky prípadných volieb vrámci nimi vypočítaných intervalov spoľahlivosti.         2.Focus výsledky pre media  
2.Focus 95% inter. spoľahlivosti 
 5.Median   Účasť  v % 66,8   64,1   
z počtu 
 1 077   1 495   v období    
 2.–8.3.   1.–28.3.   Smer-SD 38,4 34,8 - 41,9 
 44,0   SDKÚ-DS 14,3 11,8 - 16,9 
 13,1   KDH 9,7 7,5 - 11,9 
 13,1   SNS 6,3 4,5 - 8,0 
 7,0   ĽS-HZDS 5,4 3,8 - 7,1 
 6,1   SMK 5,2 3,6 - 6,8 
 3,9   Most-Híd 6,9 5,0 - 8,7 
 2,3   SaS 8,6 6,5 - 10,6 
 4,3     Ak by sme si takéto 95 percentné intervaly spoľahlivosti vypočítali aj pre ostatné tri prieskumy (MVK, Polis a TA3 od Focusu), zistili by sme, že Median sa svojimi odhadmi pre strany Smer, SaS a Most nezmestí do žiadneho z intervalov zvyšných štyroch prieskumov. Inak povedané agentúry MVK, Focus a Polis sa zhodujú v tom, že agentúra Median.sk je v prípade marcových predvolebných preferencii Fica, Sulíka a Bugára riadne mimo.   Mark Twain bol veľkým popularizátorom teórie o troch druhoch  klamstva: "Klamstvá, hrozné klamstvá a štatistika". V skutočnosti mal už vtedy na mysli klamstvá, hrozné klamstvá, manipuláciu a hrozne urobenú či zmanipulovanú štatistiku.   Nuž tak...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Keď slovák Človeku je vlkom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            HC Slovan - skrátená Bratislava
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Ako si politici z novinárov vlastných kot-kódov spravili
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Zlodejova Republika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Foto Potočný 
                                        
                                            Kauza hala nezaháľa, a výsledky?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Foto Potočný
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Foto Potočný
            
         
        martinpotocny.blog.sme.sk (rss)
         
                                     
     
        Šport môžem vo všetkých jeho prevedeniach a vo všetkých jeho podobách, aj literatúru a dobrú hudbu. Rád diskutujem pri dobrom pive o komplexnosti histórie, o zakrivení priestoru a o tom, že jazyky a dialekty tvoria spojité zobrazenie definované na povrchu geoidu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    57
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Črevo slepô zapálenô
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Mýty bez legendy
                        
                     
                                     
                        
                            I taká je politika
                        
                     
                                     
                        
                            Sever proti Juhu
                        
                     
                                     
                        
                            Zleužitá štatistika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Problém hokeja je naozaj široký... (.týždeň - máj 2008)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jana Kamencayová (žvásty plné geniá..)
                                     
                                                                             
                                            Jozef Kuric (ruka čo nezoslabne)
                                     
                                                                             
                                            Mika Polohová (o nenápadných ľuďoch)
                                     
                                                                             
                                            Helena Sedlárová (myšlienkou fascino..)
                                     
                                                                             
                                            Michal Kravčík (lúska kvapky vody)
                                     
                                                                             
                                            Samuel Marec (o ničom a predsa niečom)
                                     
                                                                             
                                            Petra Jamrichová (tuctový človek)
                                     
                                                                             
                                            Viera Mikulská (prevažne nevážne)
                                     
                                                                             
                                            Andrea Fecková (usporiadaná pestrosť)
                                     
                                                                             
                                            Ján Babarík (aj s Berkym Jariabkom)
                                     
                                                                             
                                            Dominika Handzušová (hokejoumelec)
                                     
                                                                             
                                            Ľubo Randjak (nebojí sa pravdy)
                                     
                                                                             
                                            Lukáš Cabala (vnímavý týpek z Bošáce)
                                     
                                                                             
                                            Ivana Pajanková (hospic - z prvej ruky)
                                     
                                                                             
                                            Drogy (Vlado Schwandtner)
                                     
                                                                             
                                            Melisa (začína žiť odznova)
                                     
                                                                             
                                            Boris Kačáni (boľševikom na večné časy)
                                     
                                                                             
                                            Janette csC. (celkom slušná Cigánka)
                                     
                                                                             
                                            Martin Vlachynský (tuzemský filozofix)
                                     
                                                                             
                                            Jan K. Myšľanov (Zlé správy počkali...)
                                     
                                                                             
                                            Viliam Búr (Programátor, aktivista)
                                     
                                                                             
                                            Chen Lidka Liang (Čínske dievča)
                                     
                                                                             
                                            lenka jíleková (veci, ktoré žije)
                                     
                                                                             
                                            Richard Sulík (Fachman, Občan)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




