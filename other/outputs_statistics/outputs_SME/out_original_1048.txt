
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Čo je nové na sme.sk
                                        &gt;
                Nezaradené
                     
                 Zlatý fond má dva roky (Prečo žiaci nenávidia elektronické knihy) 

        
            
                                    5.12.2008
            o
            11:30
                        |
            Karma článku:
                11.01
            |
            Prečítané 
            18298-krát
                    
         
     
         
             

                 
                    Milovníci slovenskej literatúry nájdu na internete 700 diel, ktoré si môžu voľne stiahnuť  do svojho počítača. Dobrovoľnícky projekt, ktorý im ich sprístupnil, oslavuje druhé narodeniny.
                 

                         BRATISLAVA. Slovenská literatúra sa číta aj na internete. Už dva roky najvýznamnejšie diela slovenských spisovateľov na webe sprístupňuje projekt Zlatý fond denníka SME. Najčítanejšie dielo - báseň Mor ho! básnika Sama Chalupku  si do počítača dosiaľ stiahlo viac ako 72-tisíc používateľov. Nasleduje Žltá ľalia  Jána Bottu, ktorú si do počítača stiahlo 56-tisíc internetistov a Marína  Andreja Sládkoviča  s 32-tisíckami stiahnutí.     Cieľom projektu, ktorý vznikol 6. decembra 2006 ako spolupráca denníka SME s Ústavom slovenskej literatúry SAV, je sprístupniť na internete najväčšie literárne diela slovenskej literatúry - ľudia si ich môžu voľne čítať alebo stiahnuť do počítača zo stránky http://zlatyfond.sme.sk. Medzi voľne dostupnými dielami nechýbajú diela od Janka Jesenského, Martina Kukučína, či Andreja Sládkoviča. Okrem literárnej klasiky postupne pribúdajú aj súčasní autori - medzi prvými sa na stránkach objavili Rudolf Dilong, Ján Stacho, Lýdia Vadkerti-Gavorníková alebo Ján Ondruš.       Prejsť na zoznam všetkých diel (700)          Viac diel ako inde v Európe  Tempo pribúdania voľne dostupných diel je dokonca vďaka Zlatému fondu vyššie ako v iných krajinách Európy. Európska sekcia  projektu Gutenberg.org obsahuje spolu 542 uverejnených diel vo všetkých európskych jazykoch, Zlatý fond denníka SME má len zo Slovenska viac ako 700 diel.     Každý mesiac projekt zverejní na svojej stránke niekoľko desiatok diel, na ktorých pracujú dobrovoľníci z celého Slovenska.     „Snažíme sa nepridávať denne viac ako tri nové diela, aby sa každé z nich stihlo ohriať aspoň chvíľu na hlavnej stránke. Pribúda ich však toľko, že mám na stole vyše sto dosiaľ neuverejnených diel a zoznam stále len narastá," hovorí Tomáš Ulej, koordinátor projektu Zlatý fond denníka SME.     V lete, keď mali dobrovoľníci viac voľného času, zdigitalizovali v priebehu jedného mesiaca viac ako osemtisíc strán textu. Bežne sa v Zlatom fonde v priebehu jedného mesiaca sprístupní šesťtisíc strán textu. „Tempo je také pekelné, že už ani nestíhame skenovať," hovorí Ulej.     Komunita milovníkov literatúry  Projektu sa za dva roky svojej existencie podarilo vytvoriť komunitu približne stovky Slovákov, ktorí chcú knihy dostať zo zaprášených knižníc na internet. Sú medzi nimi všetky vekové kategórie ľudí - študenti aj učitelia, tínedžeri aj dôchodcovia, mamičky na materských dovolenkách alebo dokonca ľudia zo zahraničia. „Som Mexičan. Som matematik. Študujem slovenčinu (nie je ľahký). Fotografovať veľmi sa mi pači," píše vo svojom profile na Zlatom fonde digitalizátor Agustin Murillo Lopez  z Mexika, ktorý dosiaľ zdigitalizoval 155 strán textu. Medzi inými pracoval aj na zbierke Letorosty  od Pavla Országha Hviezdoslava.     Najaktívnejší digitalizátori majú na konte každý  už niekoľko desiatok tisíc strán zdigitalizovaného textu. Zlatý fond je pre nich nielen spôsobom, ako zmysluplne vyplniť svoj voľný čas, ale aj priestor, ako si medzi sebou vytvárať priateľstvá, alebo si radiť v chúlostivých životných situáciách.     „Niekedy si pripadám, že niektorým digitalizátorom okrem radenia s korigovaním slúžim aj ako psychoterapeut," smeje sa správkyňa Viera Studeničová, ktorá má v projekte okrem iného na starosti prácu s menej skúsenými s digitalizátormi. „Je krásne, že sa ľudia podelia aj o svoje problémy a nie je to len o nejakom korigovaní," hovorí.       Mapa digitalizátorov v Zlatom fonde.     Zdochnite, kto to má čítať!  O knihy je veľký záujem - stránku v októbri navštívilo viac ako 40.000 tisíc unikátnych návštevníkov, najčítanejšie knihy boli dosiaľ stiahnuté každá niekoľko desaťtisíckrát. Do e-mailovej schránky projektu mieria desiatky listov od spokojných čitateľov - sú medzi nimi okrem iného aj nevidiaci, ktorí sa vďaka projektu môžu dostať ku knihám, ktoré doteraz nikto neupravil pre ich špeciálne čítačky. Projekt oceňujú aj zahraniční Slováci a učitelia základných, stredných i vysokých škôl.     Jediná skupina obyvateľstva, ktorej sa Zlatý fond občas nepozdáva, sú žiaci základných a stredných škôl. „Často nám píšu, že ich učiteľka núti stiahnuť si z našej stránky dielo a prečítať si ho. Doteraz sa zrejme mohli vyhovárať, že ho v knižnici nezohnali," hovorí Ulej. „Vďaka Zlatému fondu už neexistuje výhovorka typu ´knihu si už niekto v knižnici požičal´. K literatúre sa môže dostať každý - odkiaľkoľvek na svete," dodáva.     Z e-mailov, ktoré nám chodia  „Kto to má všetko čítať? Je to príliš dlhé..... pošlite mi do stredy referát, potrebujem ho na slowinu. Vďaka xxxxxxx@azet.sk"   (žiak strednej školy)     „vi si to fakt ako magori čitate??? diki za pozornost heh..!:§"   (anonymný návštevník stránky)     „tak to fakt jakoze diki. teraz musime vsetko citat, bo ste to tu vivesili. zdochnite, vsetci ako ste"   (žiak základnej alebo strednej školy, ktorému pravdepodobne prekáža fakt, že kvôli Zlatému fondu sa už nemôže vyhovárať, že nezohnal knihu v knižnici)     „opravte si to - pise sa restaUrácia a nie restaVrácia, lameri!"   (z pripomienky k dielu Jána Kalinčiaka)      Ako prebieha digitalizácia kníh        Kniha sa oskenuje a pomocou špeciálneho programu sa v nej rozpoznajú písmená. Výsledkom je text, ktorý obsahuje množstvo chýb - zlé rozpoznané písmená („Pcter" namiesto „Peter", „dˇakujem namiesto ďakujem" ap.), zlepené riadky, chyby vo formátovaní a podobne.   Digitalizátori trikrát po sebe text porovnajú s pôvodnou predlohou.   Dielo sa vloží do špeciálneho XML formátu, z ktorého sa už priamo pridá na server - okrem bežného čítania na webe je k dispozícii aj na stiahnutie vo formátoch ako Microsoft Word, alebo Adobe Acrobat. K dispozícii sú tiež formáty špeciálne určené na čítanie na mobiloch a vreckových počítačoch.       Zaujímavosti o Zlatom fonde       Pri rýchlosti čítania 200 slov za minútu by prečítanie celého obsahu Zlatého fondu zabralo mesiac.    Len samotné diela v rôznych formátoch zaberajú na disku 1,3 GB, teda skoro dve plné CD.   Do projektu sa doteraz aktívne zapojilo viac ako 200 dobrovoľníkov, najaktívnejšia digitalizátorka (Viera Studeničová - http://zlatyfond.sme.sk/digitalizator/228/Viera-Studenicova/) spracovala 30.000 strán písaného textu.   Len v októbri 2008 si používatelia do svojich počítačov vďaka Zlatému fondu stiahli 68-tisíc diel slovenskej literatúry. Spolu sa do slovenských domácnosti už dostalo viac ako 300.000 slovenských elektronických kníh.   Digitalizátori sú za svoju prácu čiastočne odmeňovaní poukážkami na nákup kníh v internetovom obchode Martinus - jednu päťstokorunovú poukážku dostanú za každých 400 zdigitalizovaných strán textu. Okrem iného tiež majú právo na polovičnú zľavu na predplatné denníka SME.       Najpopulárnejšie knihy       Samo Chalupka: Mor ho!   Ján Botto: Žltá ľalia   Andrej Sládkovič: Marína   Samo Chalupka: Branko   Ján Botto: Smrť Jánošíkova   Martin Kukučín: Keď báčik z Chochoľova umrie...   Andrej Sládkovič: Kratšie básne   Samo Chalupka: Turčín Poničan   Jozef Gregor Tajovský: Maco Mlieč   Andrej Sládkovič: Detvan   Samo Chalupka: Valibuk   Pavol Dobšinský: Prostonárodné slovenské povesti (Tretí zväzok)   Hugolín Gavlovič: Valaská škola mravúv stodola   Pavol Dobšinský: Prostonárodné slovenské povesti (Prvý zväzok)   Samo Chalupka: Sen   Pavol Országh-Hviezdoslav: Hájnikova žena   Ján Botto: Orol   Jozef Gregor Tajovský: Mamka Pôstková   Martin Kukučín: Rysavá jalovica   Janko Jesenský: Malomestské rozprávky           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (46)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            SME hľadá redaktorov, online editorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Prečo pod niektorými článkami nie je možnosť diskutovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Sme.sk hľadá HTML/CSS kodéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Vitajte na úplne nových blogoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Nová titulná stránka SME.sk: Viac správ, viac čítania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Čo je nové na sme.sk
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Čo je nové na sme.sk
            
         
        novinky.blog.sme.sk (rss)
         
                                     
     
         Novinky, zlepšenia a vôbec všetko, s čím sa chceme podeliť. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    153
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6917
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Novinky na sme.sk
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




