
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dávid Králik
                                        &gt;
                Etika u mňa v triede :o)
                     
                 Dávid, čo blbneš? 

        
            
                                    13.5.2010
            o
            18:50
                        |
            Karma článku:
                21.79
            |
            Prečítané 
            9124-krát
                    
         
     
         
             

                 
                    "Open your books and..." "Dávid, čo blbneš? Veď máme matematiku!" ozval sa ma prekvapene ktorýsi z mojich šušníkov.
                 

                 "Sorry?" nechápavo sa usmievam a ležérne pokračujem v zadaní­ projektu na dnešnú hodinu. (Prirodzene v anglickom jazyku :o) Od tej chví­le som už neprehovoril ani slovko po slovensky. V rámci zadania som po anglicky oznámil deckám, že ten, kto prvý správne splní­ úlohu, zí­ska veľkú čokoládu. Potom som ich nechal pracovať. Občas som im trochu pomáhal a súčasne komentoval ich prácu (ako inak, v angličtine :o).   Hodina pomaly končila a tak som, k nemalému úžasu mojich prďolákov, vytiahol sľúbenú čokoládu. Víťazom sa stal ktosi celkom náhodne (k prekvapeniu všetkých spolužiakov i seba samého :O). Vyfasoval teda čokoládu i s vysvetlením. No tentokrát v rýdzej slovenčine:   "Pred pár dňami ste sa ma pýtali, načo vám bude v živote angličtina a prečo sa ju vlastne musí­te v škole učiť. Viete, podmienky na dnešnej hodine boli celkom regulérne a férové, no i tak nevyhrali najlepší­ z vás (ktorí mimochodom už dnes dokážu samostatne urobiť úplne fenomenálne veci). Prečo? Skrátka boli znevýhodnení­ svojou neznalosťou cudzieho jazyka. Dnes išlo len o čokoládu, nad ktorou pokojne môžete mávnuť rukou... No čo ak raz v budúcnosti nastane podobná situácia vo firme, kde budete pracovať? Čo ak niekto iný urobí miliónový kontrakt namiesto vás len preto, že neovládate cudzí jazyk?" 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Redakcia SME sa musela zblázniť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            To najlepšie nakoniec...už dnes
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Ignoranti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Čo už by mi len mohol ponúknuť prepážkový zamestnanec Tatra Banky?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Možno máte dieťa vo veku 9 až 99 rokov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dávid Králik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dávid Králik
            
         
        davidkralik.blog.sme.sk (rss)
         
                                     
     
        Bývalý (m)učiteľ na I. stupni ZŠ, ktorý sa platonicky priatelí s Jonathanom Livingstonom, so Siddhárthom a hobitmi. Znamenie vodnára ho predurčuje k uletenosti všetkými smermi - hudba, literatúra, šport ...  




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    930
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3168
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja malá fikcia
                        
                     
                                     
                        
                            Viera ako ju cítim
                        
                     
                                     
                        
                            Aj také sa mi stalo
                        
                     
                                     
                        
                            Moje malé postrehy
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Život je krásny
                        
                     
                                     
                        
                            Malé gramatické okienko
                        
                     
                                     
                        
                            Logopedické okienko
                        
                     
                                     
                        
                            Rozprávky (aj) pre dospelých
                        
                     
                                     
                        
                            Milovaťžofiu
                        
                     
                                     
                        
                            Nedeľná ... veď viete : o )))
                        
                     
                                     
                        
                            Biblia očami laika : )
                        
                     
                                     
                        
                            ...cestou ma vanie...
                        
                     
                                     
                        
                            šerm
                        
                     
                                     
                        
                            moje hudobné výlevy
                        
                     
                                     
                        
                            Etika u mňa v triede :o)
                        
                     
                                     
                        
                            Moja poviedkovňa
                        
                     
                                     
                        
                            Pracujem ako divý :o)
                        
                     
                                     
                        
                            baška
                        
                     
                                     
                        
                            (pre mňa) zaujímaví ľudia
                        
                     
                                     
                        
                            na slovíčko
                        
                     
                                     
                        
                            Nápadník
                        
                     
                                     
                        
                            Súťaž krátkych videí
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Veľmi (ne)chutná hmyzia knižka
                                     
                                                                             
                                            Niektoré rany sú príliš hlboké a nikdy sa skutočne nezahoja
                                     
                                                                             
                                            (pre mňa) Môj najlepší rozhovor
                                     
                                                                             
                                            Logopédia už aj vo vašej škôlke? Prečo nie :)
                                     
                                                                             
                                            Have a nice Day(vid:)
                                     
                                                                             
                                            Kam odchádzam zo školstva
                                     
                                                                             
                                            Chceli by ste ísť do neba, potom vedzte...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Najlepšia logopédka v Rači/ na svete
                                     
                                                                             
                                            FELIX
                                     
                                                                             
                                            iní
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




