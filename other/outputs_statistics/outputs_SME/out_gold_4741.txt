

 A preto aj vďaka tomu býva naša imnutia narúšaná vonkajšími vplyvmi a bývame častejšie nachladnutí alebo chorí ako obyčajne. 
 Dostal sa ku mne recept na jeden zázračný nápoj, ktorý pomáha v 99% prípadoch (ktoré poznám). Úspešne vyliečil rozmáhajúcu sa nádchu, ktorá predzvestovala škaredú chrípku. 
 Takže nech sa páči, rada by som sa s vami o tento recept podelila: 
 Liter vody a 1 polievkovú lyžicu mletého zázavoru necháme spolu variť pri miernom ohni 7 minút. 
  
 Následne na to výluh odstránime z plynu. Pridáme 3-4 lyžice medíku, 2 lístky mäty- alebo mätového čaju. V našom prípade dávame práve čaj. Pridáme 5 lyžíc citrónovej šťavy a štipku mletého korenia. Na korenie pozor, raz sa nám podarilo ho tam dať viac, ako je únosné. Najbližších 30 minút som mala o zábavu postarané :-/ (štípali ma uši ako bláznivé). 
  
 Vyzerá to trošku odpudivo a chuť tiež nie je zrovna najlepšia. Pokiaľ vám ale lezú na nervy všetky tie práškové rozpustné nápoje na nádchu, nech sa páči návod na zmenu :-) 
 Je iba na vás, ako si to namiešate, môžete si pridať viac citrónu, ak to chcete mať kyslejšie, príp. viac medu, ak máte radi sladké. 
 V každom prípade prajem všetkým, aby tento nápoj v najbližšej dobe nepotrabovali :-) 
 Robievate si doma podobné nápoje aj vy? 

