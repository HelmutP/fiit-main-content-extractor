

 
Lenže po krátkom čase príde stav, že „piker“ ( od slova PIKO – pervitín) sa zmení z predátora na korisť. Všetci ho prenasledujú, je sledovaný, každý do neho vidí a chce mu ublížiť. Cíti sa ohrozený a zbytočne ho ukľudňujú všetci naokolo, ktorí piko neberú. Podľa neho je jednoducho ohrozený, všetci sa proti nemu sprisahali a on v takej chorej realite žije sám a bezmocný. 
 
 
Mal som tu chlapca, ktorý skalopevne veril, že ho prenasleduje bývalý guvernér Národnej banky Slovenska Vladimír Masár. Nijako si to nenechal vyhovoriť a bol taký presvedčivý, že už som tuším chcel volať tomu guvernérovi, nech ho láskavo nechá na pokoji. Nakoniec "som ho zachránil" a odviezol ho do Pezinka k pánu primárovi Jánoškovi , nech ho ukryje na svojom uzavretom oddelení za silné mreže, čo tam majú. Až tam sa konečne ten chlapec cítil bezpečne a skoro vybozkával  pána primára aj mňa. 
 
 
Druhého z mojich chlapcov zase v Pezinku na pravé poludnie  naháňali mŕtvoly. Hádzal sa o zem, kopal okolo seba vrešťal a bránil sa. Ľudia okolo neho zavolali políciu a tá ho odviezla hádajte kam?! No predsa primárovi Jánoškovi a ten ho zase schoval do bezpečia svojho uzatvoreného oddelenia. 
 
 
Každé z mojich detí, čo bralo piko sa dostalo do podobných stavov. Niektorí chodili štvornožky popod okná v izbe, lebo vonku čakali mafiáni s odstreľovacími puškami. Iných zase prenasledovali kukláči, mnohých sa pokúšali zabiť susedia rôznymi rafinovanými spôsobmi a iní mali zase paniku z najrôznejších sprisahaní a komplotov svojich spolužiakov a kamarátov. 
 
 
Lenže to sú pomerne jednoduché stavy, ktoré vo väčšine prípadov ustúpia a je pokoj. Potom už taký človek znovu chápe realitu a keď si to piko už viace  nedá, môže mať pokoj od takých vecí už natrvalo. Ak sa im v tých paranojách nič nestalo a ani oni nič nespôsobili, stačí ak dajú 500 korún na omšu, že tak dobre obišli. 
 
 
Sú však aj horšie prípady. Zažil som už všeličo a myslel som si, že v tejto oblasti ma  nič neprekvapí. Lenže svet je pestrý a keď si myslím, že už som všetko zažil, zrazu to neplatí . 
 
 
Nikdy som ešte nepočul, že by niekoho prenasledovala polovica obyvateľov Pezinka a úplne všetci novinári, finančné skupiny a iné presne nešpecifikované mátohy. Pokiaľ to boli skupinky, Maďari, EU, tak to som ešte chápal. Na také stavy stačí pouličný pervitín, no teraz asi prenikol  na náš trh nejaký silnejší, keď tie paranoje sú až také drsné a trvajú nekonečne dlho. 
 
 
Myslím, že pán primár Jánoška by si mal zriadiť ešte jedno oddelenie a zosilniť mreže, lebo tu už končí sranda. 
 

