
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Libor Lukáč
                                        &gt;
                Nezaradené
                     
                 Otázka zmiernej obety Krista 

        
            
                                    25.3.2010
            o
            18:03
                        |
            Karma článku:
                1.70
            |
            Prečítané 
            268-krát
                    
         
     
         
             

                 
                       Je načase, aby sme sa v tejto veci už raz pokúsili uvažovať jasne a neovplyvnene! A začnime od samotného významu pojmu zmierny, ktorý je odvodený od slova zmierenie. V našom konkrétnom prípade ide o zmierenie ľudí s Bohom.
                 

                   
         Ak hovoríme o potrebe zmierenia znamená to, že pred tým muselo dôjsť k určitému rozkolu, alebo odklonu. K odklonu ľudí od Stvoriteľa a od jeho Vôle. Ľudia totiž prestali rešpektovať Vôľu Božiu a začali sa riadiť iba svojim vlastným chcením, iba svojou vlastnou vôľou. Keďže ale ľudská vôľa nie je ani zďaleka dokonalá, muselo riadenie sa ňou celkom zákonite prinášať iba čoraz väčšie problémy a nedostatky, prejavujúce sa narastaním rôznych druhov zla a negativity. Zlo a negativita začali stále viacej a viacej prerastať ľudskou spoločnosťou a to až do takej miery, že celá civilizácia začala smerovať k zániku.           Aby nedošlo až k najhoršiemu, bolo treba opäť zmieriť ľudí s Bohom a to tým, že im bude znovu ukázaná Vôľa Najvyššieho, podľa ktorej majú žiť a podľa ktorej majú riadiť svoje chcenie. Iba toto jediné totiž mohlo zastaviť začínajúci úpadok, smerujúci ku chaosu a zničeniu! Toto predstavovalo a doteraz predstavuje jedinú správnu cestu k duchovnému, ale i k pozemskému vzostupu a napredovaniu.           Vrcholným aktom nevyhnutnej zmiernej obety Boha s ľuďmi bol pozemský príchod jeho Syna, ktorý dobrovoľne zostúpil až do hmotného, pozemského tela a to preto, aby sa týmto spôsobom stal Stvoriteľ ľuďom zrozumiteľným v čo najväčšej možnej miere. Aby ústami svojho Syna ukázal ľudstvu svoju Vôľu, ktorou sa má riadiť a podľa ktorej má žiť, ak chce byť šťastné.           Tvorca sa teda obetoval v osobe Ježiša Krista a prišiel na zem medzi ľudí. Je to obeta, nad ktorú niet väčšej! Časť živého Boha dobrovoľne opustila nepredstaviteľnú velebnosť a nádheru súcnosti Božej a zostúpila až k ľuďom do hmotnosti, dobrovoľne sa podriadiac všetkým prírodným zákonom, platným pre túto hmotnú časť stvorenia. To je obeta, ktorú ľudia nebudú môcť nikdy plne pochopiť! Bola podstúpená preto, aby došlo k zmiereniu ľudí z Bohom a to tým, že sa im znovu prinesie a ukáže poznanie jeho Vôle, pretože iba život podľa Božej Vôle mohol ľudstvo odvrátiť od záhuby a nasmerovať ho na správnu cestu.          A teraz uvažujme: Mohlo dôjsť k zmiereniu ľudstva so Stvoriteľom tým, že ľudia jeho Syna zavraždili? Veď touto vraždou v skutočnosti iba vyjadrili, že nechcú rešpektovať Vôľu Božiu a nechcú sa ňou riadiť, ale  naopak, chcú žiť aj naďalej iba podľa svojej vlastnej vôle. Touto vraždou sa odklon ľudí od Stvoriteľa nijako nezmenšil, ale naopak, ešte viacej prehĺbil! A je naozaj vrcholom všetkého, keď sa práve tento hanebný čin považuje za akt zmiernej obety ľudí so Stvoriteľom.           Táto vražda bola výsmechom ľudí voči Tvorcovi, ktorému, cítiac sa istí na pôde hmotnosti, nenávistne zavraždili jeho Syna. Syna, ktorý vzal na seba hmotné telo, čím sa dobrovoľne podriadil všetkým zákonom hmotnosti a to i s možným rizikom toho, že môže byť v nepochopení a nenávisti zavraždený.           Kristus sa dobrovoľne vydal ľuďom do rúk a iba na ich slobodnej vôli záležalo, či príjmu jeho osobu a učenie, čím by došlo k zmiereniu ľudstva s Bohom, alebo ho odmietnu a zavraždia a tým ešte viac prehĺbila svoj odklon od Stvoriteľa.           Kristus teda neprišiel zomrieť na kríži! Prišiel, aby ľuďom priniesol poznanie o nevyhnutnosti rešpektovania Vôle Božej a tým im dal šancu zmieriť sa s Najvyšším. V jeho príchode na zem, sem k nám do hrubej hmotnosti, pod účinky pozemských zákonov spočívala oná veľká zmierna obeta, podstúpená Svetlom pre ľudí.           Touto zmiernou obetou teda v nijakom prípade nie je Kristova vražda na kríži, ktorá je v skutočnosti aktom odporu a vzbury voči Stvoriteľovi! Vzbury voči jeho Vôli a voči jeho Poslovi! Niečo takéhoto nemôže v nijakom prípade privodiť zmierenie, ale naopak, ešte väčšiu nezmieriteľnosť!           Zo všetkých učeníkov bol Ježišovi najbližší práve Ján a to preto, lebo najlepšie chápal jeho slová. V rozhovore s Pilátom, ktorý sa týkal podstaty Kristovho príchodu na svet Ján zaznamenal tieto Ježišove: „Ja som sa narodil na to a na to som prišiel na svet, aby som vydal svedectvo o pravde.“ Čiže na to, aby ľuďom ukázal, ako správne žiť podľa Vôle Božej! Nie na to, aby zomrel na kríži!          Toto tvrdenie potvrdzujú i ďalšie Kristove slová: „Moje kráľovstvo nie je z tohto sveta. Keby bolo moje kráľovstvo z tohto sveta, moji služobníci by sa bili, aby som nebol vydaný do rúk Židom. Lenže moje kráľovstvo nie je z tohto sveta“          „Moji služobníci by sa bili, aby som nebol vydaný do rúk Židom! Lenže moje kráľovstvo nie je z tohto sveta“ Áno, Kristus sa dobrovoľne vydal do rúk ľudí, aby im mohol zvestovať Vôľu Najvyššieho. Jedine v poznaní a dodržiavaní tejto Vôle totiž tkvie záchrana a lepšia budúcnosť ľudstva! Toto jediné mohlo a môže zmieriť ľudí z ich Tvorcom!           V tomto Kristovom príchode na zem v ľudskom tele, so všetkými rizikami, ktoré to prinášalo tkvie i oná, toľko krát spomínaná, veľká zmierna obeta Svetla pre ľudí. Zmierna obeta, za ktorú nemožno v nijakom prípade považovať Vykupiteľovu surovú vraždu na kríži! Podstata vykúpenia preto nemôže nijakým spôsobom súvisieť s touto vraždou! Vykúpenie spočíva iba v živote podľa Božej Vôle, ktorej stratené poznanie prišiel Kristus ľuďom zvestovať.          L.L., sympatizant Slovenského občianskeho združenia pre posilňovanie mravov a ľudskosti    http://www.pre-ludskost.sk/                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Televízia – hriech náš každodenný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Iný pohľad na katastrofu v Japonsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            O radosti a bolesti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Zahynieme na devalváciu slova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Katastrofa v Japonsku – hlas Matky prírody!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Libor Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Libor Lukáč
            
         
        liborlukac.blog.sme.sk (rss)
         
                                     
     
        Snažím sa byť človekom s vlastným pohľadom na realitu. Emailový kontakt: libor.lukac@atlas.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    504
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




