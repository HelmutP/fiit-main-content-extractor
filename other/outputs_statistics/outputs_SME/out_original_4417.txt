
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Mečiar
                                        &gt;
                Politika
                     
                 Sú elektronické voľby hrozbou? 

        
            
                                    12.4.2010
            o
            13:05
                        |
            Karma článku:
                8.44
            |
            Prečítané 
            1452-krát
                    
         
     
         
             

                 
                    Jaroslav Daniška sa na webe .týždňa venuje časti volebného programu SDKÚ-DS a to zámeru presadiť elektronické voľby. V akomsi „konzervatívnom kŕči" napísal viacero pozoruhodných úvah. Odmietanie elektronických volieb z dôvodov, ktoré uvádza, je absurdné.  
                 

                 Jaroslav Daniška píše: „Elektronické voľby doteraz žiadna konzervatívna strana nepresadzovala. Nepatrí to k témam pravice. O voličoch, ktorí by sa zúčastnili elektronických volieb a nezúčastnili klasických volieb, totiž možno predpokladať niekoľko vecí. Po prvé, politika ich príliš nezaujíma. Keby ich zaujímala, našli by si cestu aj do volebnej miestnosti a volili by. Keďže ich politika nezaujíma, nesledujú ju, a teda ani nevedia povedať rozdiel medzi - povedzme - Ficom a Radičovou. Z toho vyplýva, že sa rozhodujú inak. Ovplyvnia ich napríklad celebrity, nové sľuby, marihuanové či homosexuálne témy.„   Hovoriť paušálne o voličoch, ktorí by sa zúčastnili elektronických volieb ale klasických nie, že ich politika nezaujíma a neorientujú sa v nej, je nezmysel. Môže ísť o voličov, ktorí sa v čase volieb nachádzajú na univerzite v zahraničí, ďaleko na dovolenke, doma na lazoch kilometre od volebnej miestnosti atď. Môžu byť v politike oveľa viac zorientovaní, ako voliči, ktorí v živote neopustili rodnú viesku a budú voliť klasicky, rozumej v obleku a bielej košeli. Aj autor tohto blogu by volil elektronicky, lebo je lenivý. Navyše, výsledky doterajších klasických volieb nenaznačujú príliš veľkú zorientovanosť niektorých skupín voličov.   Elektronických voličov by vraj ovplyvnili celebrity a marihuanové či homosexuálne témy. Po prvé: nie je to pravda. Po druhé: kto a čo ovplyvňuje klasických voličov? Po tretie: keby aj? Nepovažujem za správne voliť podľa celebrít alebo marihuanových či homosexuálnych tém, ale tento prístup nebudem nikomu odopierať.   Jaroslav Daniška ďalej píše: „A potom je tu ešte iná hrozba s ktorou sa SDKÚ zahráva. Mladí v Rakúsku dnes vo zvýšenej miere volia Stracheho extrémistov, v Maďarsku antisemitský Jobbik. Skúsenosti v strednej Európe teda velia byť opatrní. Nacionalisti dokážu medzi mladými oslovovať ľahko. Antiglobalisti a kadejakí novodobí zväzáci tiež. Stačí kliknúť a hlas sa počíta."       Extrémisti získavali hlasy aj doteraz v klasických voľbách, však? Obava, že napr. študenti na zahraničných univerzitách budú voliť extrémistov, nie je na mieste.   Z textu J. Danišku vanie aj čosi iné. Ako by chcel povedať, aby sme neuľahčovali účasť na voľbách hocikomu, je to nebezpečné! Môžu uspieť extrémisti. Čo potom s hrozbou, že v klasických voľbách uspejú nacionalisti (ako v roku 2006 a možno aj 2010 u nás) alebo komunisti (ako možno o mesiac v ČR)? Čo keby sme volebné miestnosti dali na 15. poschodie v budove, kde nefunguje výťah? To by odradilo starších voličov, ktorí volia komunistov. Tak?   Elektronické voľby skôr či neskôr prídu a netreba sa ich obávať. Tak ako sme zliezli zo stromov, začali používať príbor, telefonovať, či vysedávať za počítačmi, tak budeme aj voliť elektronicky. A každá konzervatívna duša by mala zostať pokojná, nijako totiž nezradí svoje presvedčenie. Konzervatívny neznamená odmietajúci všetko nové, čo ľudstvo vymyslí. Konzervatívne strany a politici už dávno majú svoje internetové prezentácie (viď www.klaus.cz alebo www.mydavidcameron.com).   Mimochodom, časopis .týždeň si čitatelia nájdu aj v novinových stánkoch, tak načo internetová verzia?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mečiar 
                                        
                                            Pred desiatimi rokmi začalo otváranie Slovenska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mečiar 
                                        
                                            O nezávislých a tých druhých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mečiar 
                                        
                                            KDH mieri na národnostné menšiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mečiar 
                                        
                                            Treba radikálne znížiť počet obcí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mečiar 
                                        
                                            Maďar Slovákovi neverí. A naopak.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Mečiar
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Mečiar
            
         
        meciar.blog.sme.sk (rss)
         
                                     
     
         NOVA. Zástupca primátora v Šali. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    56
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1847
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




