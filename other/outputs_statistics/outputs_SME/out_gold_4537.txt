

 
Livin’ Blues (nemám to overené, ale údajne je názov inšpirovaný výbornou holandskou skupinou Livin’ Blues) vznikol na troskách festivalovej prehliadky Rača Blues v roku 1993 a odvtedy sa koná každý rok, vždy v marci. Za 18. ročníkov na ňom vystúpili hádam všetci, čo na Slovensku majú s blues aspoň okrajovo čo dočinenia. Od polovice 90. rokov som sa síce tváril ako bubeník, ale na festivale som sa ocitol až v roku 2001, v príležitostnom zoskupení P. Csonka &amp; A. Band. Skupinu viedol Peter Csonka, ktorého všetci prezývali Maco, úžasný gitarista, ktorý nás tohto roku navždy opustil. Bolo mi cťou s ním hrať a jemu predovšetkým vďačím za to, že hrám v Žalman Brothers Band. 17. marca v hornej „akustickej“ sále DK Lúky som si iba za pomoci rytmičáku, hajtky a páru metličiek odbil svoju festivalovú premiéru. Maco si to užíval, moderoval, komentoval a nikdy nezabudnem, ako som počas predstavovačky hral nejaké rôzne akcentované paradidle na rytmičák a odpovedal do mikrofónu na jeho otázky. Maco, samozrejme, presviedčal publikum, že sme to mali nacvičené, ale pravda je, že nič také sme nacvičené nemali. V ten istý večer hrala v hlavnej sále zmienená skupina Žalman Brothers Band a bol to Maco, kto Petrovi Žalmanovi odporučil naverbovať do zostavy Maťa Výbocha a moju maličkosť. Fičíme spolu dodnes, nuž, Maco, vďaka.
 

 
So Žalman Brothers Band som následne na festivale vystúpil štyrikrát (16.3.2002, 15.3.2003, 24.3.2007 a 14.3.2009), z toho raz na hlavnom pódiu (v roku 2007). A to radšej nespomeniem, že v roku 2008 sme boli na programe, ale sme nevystúpili. Vtedy hornú sálu organizoval Maco a chcel, aby sme na záver niečo zahrali, ale vzhľadom na to, že sme tam boli bez nástrojov, iba tak, a časový plán vystúpení zlyhával, nakoniec sme to vzdali. Dnes si myslím, že je to škoda, ale vtedy som to veľmi neľutoval. Inak, z našich vystúpení mám dobrý pocit, v roku 2002 sme mali v zostave hosťujúceho huslistu Peťa Kolárociho a harmonikára Petra Horvátha, inak známeho ako Romi a bolo to výborné spestrenie. V 2003 zasa vypomohol Berco na saxofón a Martin „Marta“ Zúrik so spevom. Tuším sme vtedy boli aj odfotení v tlačenom vydaní SME, ale neviem to naisto, možno to bolo pri inej príležitosti.  
 

 
Tým moja účasť ešte neskončila, niekedy by bolo zaujímavé pochváliť sa, aký je to pocit, byť žiadaný. Občas som sa nechal nalákať na výpomoc rôznym skupinám a v roku 2006 som tak mohol absolvovať ešte jednu hornosálovú exhibíciu, tentoraz so skupinou Strempek Križan Blues Band. Pokiaľ si pamätám, aj toto vystúpenie sa vydarilo.
 
 
Na záver pripájam niekoľko skladieb (ospravedlnte rôznu kvalitu provizórnych nahrávok), ktoré tam odzneli.
 

