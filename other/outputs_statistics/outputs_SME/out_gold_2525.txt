
 Ale nakoniec - prečo nie!! 
 
Niektorých vecí je v živote málo, aj keby ich bolo koľkokoľvek -  a úsmev medzi ne určite patrí. 
 
 
 
Predlžuje vraj život - a tomuto ja rád verím :-) 
 
 
 
Niekedy stačí málo - pozrieť sa na niečo pekné s niekým, s kým je nám dobre...... 
 
 
 
...a hneď sa uškŕňame :-)) 
 
 
 
Ale - s tým dnešným dňom je to nejaké čudné..... 
 
 
 
...pretože niekde sa uvádza, že ten deň pripadá na 03/10, inde zas, že na 07/10. 
 
 
 
....nuž ak je pravda (ako obvykle) niekde uprostred, tak potom je to naozaj dnes :-))) 
 
 
 
Nuž ale - nech je úsmev akýkoľvek, dokonalý, alebo štrbavý, široký, alebo úškľabkovitý..... 
 
 
 
...určite sa oplatí! Hlavne nech je úprimný - a ak vám to vydrží, možno sa dostanete aj na bankovku :-)))) 
 
 
 
Hlavne ráno v dopravnej špičke v MHD je mrzutcov kopec - ale načo si kaziť deň, no nie? :-) 
 
 
 
Takže - keep on smiling! :-)) 
 
 
 
A nezabudnite sa usmievať :-))) 
 
 
 
Foto: ja a Dominika  
