

 Kauzu konkurzu spoločnosti Rašelina Quido odštartovali koncom mája tohto roku informácie o vražde banskobystrickej advokátky a správkyne konkurznej podstaty firmy Gabriely Matuškovej a únose podnikateľa Branislava Prieložného. 
 Polícia únos aj vraždu nafingovala, aby dolapila Tobiáša L., ktorý si to mal objednať. 
 Tobiášovi L. kedysi  celá spoločnosť Rašelina Quido patrila. Vraždu aj únos si mal objednať preto, aby celý konkurz zmaril. 
 V súvislosti s objednaním vrážd sa už objavujú informácie, že celá vec bola naňho narafičená istou skupinou, ktorá sa snaží ovládnuť jeho bývalú firmu. 
 Prevzatie zhruba 300-tisíc korún preto vraj nemala  byť platba za vykonanú vraždu, ale len pôžička pre jeho bývalého spoluväzňa. 
 Pôvodne si mal objednať vraždu šiestich ľudí a suma 300-tisíc je za niečo také podľa viacerých expertov veľmi nízka. 
 Tobiáš L. sa dnes okrem toho stále súdi so štátom o náhradu škody vo výške niekoľko sto miliónov,  ktorá mu mala byť spôsobená rozhodnutím štátneho orgánu, čo mu malo znemožniť podnikať. 
 (wm, mož, rk) 

