

   
  
  
  
   
   
 

 
 sssssss 
 sssssssss 
  
  
  
   
 Exaktná definícia slová Génius. 
 Úvodom mojej exaktnej definície obsahu slová génius, uvádzam výroky slávnych osobností  ľudstva vyslovené na adresu géniov. 
  
 1.  Keď  sa na svete objaví ozajstný génius, spoznáte to podľa toho, že sa proti nemu spoja všetky hlúpe hlavy. 
 (Jonathan Swift )  
 (1667 - 1745) 
  
 2.  Génius je človek ako každý iný. Lenže je iný. 
 (Honoré de Balzac) 
  
 3.  Talent využíva príležitosť. Génius ju vytvára. 
 (Jean Paul) 
  
 4.  Génius podľa Kanta je príroda prejavujúca sa v človeku ako rozum. 
  
  
 5.  Génius sa neriadi nijakým pravidlami, génius sám vytvára pravidlá.  
  
 6.  Genialita je schopnosť vytvárať idey presahujúce hranice abstraktného myslenia. 
  
 7. Podľa Junga je génius zvestovateľom nových hodnôt, ktoré reagujú na pálčivé problémy tej ktorej doby, ktoré sú obsiahnuté v mylnom kolektívnom vedomí ľudstva, a génius na ne prináša revolučné riešenie. 
  
 Z tu uvedených definícii slová génius vyzdvihujem Kantovu definíciu v znení:  
  
 "Génius je príroda prejavujúca sa v človeku ako rozum." 
  
 Hore uvedenej definície slová génius, majú jedného spoločného menovateľa v tom, že oni svoje argumenty nedokladajú opisom hodnoverných skutkov konkrétnych géniov, ale iba všeobecnými fázami. 
 Keďže ja sám (pri všetkej mojej, pritom ničím neopodstatnenej, no napriek tomu nekončenej skromnosti, až hanblivosti) považujem sa za génia, (ja som rozhodne géniom?!) aj v zmysle hore uvedených neexaktných definícii slová génius, preto ja môžem oprávnene podať prvú adresnú a preto aj exaktnú definíciu obsahu slová génius. 
  
 Na úvod uvediem tie vlastnosti, ktoré z mojich skúsenosti, s genialitou človeka, nemajú nič spoločného. 
  
 Génius, nemusí mať fenomenálnu pamäť, nemusí mať vysoké IQ, nemusí byť vysokoškolsky vzdelaný a nemusí ovládať oficiálnu matematiku, ani fyziku na najvyššej úrovní. Skôr naopak. 
  
 Génius nesmie by vysokoškolsky vzdelanými jedincom, lebo vysokoškolským vzdelaním (relativistickou fyzikou a diferenciálnou matematikou) by sa zákonito otupila jeho prirodzená, vrodená absolútna genialita.  
 (Otupil by sa prirodzený rozum génia, lebo príroda nie je ani ralatívna ani diferenciálna.) 
  
 Genialitou rôzneho stupňa je obdarená väčšia časť ľudstva (nie každý jedinec) už po narodení. Väčšina ľudí už sa rodí so zmyslom pre rozoznanie dobra a zla, ako aj so zmyslom pre rozoznanie pravdy od klamstva a to bez toho, aby sa oni najprv museli naučiť čo je dobro a čo je zlo a čo je pravda a čo klamstvo. 
 Keď však človek dospeje a začne sa dotýka reality života, keď začne chodiť do školy, tak sa táto jeho prirodzená, vrodená (natürlich) genialita deformuje tým, že spoločnosť a učitelia mu začnú systematicky vnucovať (vtĺkať do hlavy) také pravdy, ktorých zmysel nie je ani jemu, ani spoločnosti (ale ani jeho učiteľom) jasný a preto ani pochopiteľný. 
 V tom prípade mladý človek buď rezignuje na svoju vrodenú genialitu, (prestante o veciach analyticky uvažovať) a začne všetky, pre neho nepochopiteľné veci, v slepej viere, bez akceptovania svojho vrodeného svedomia a vedomia, teda ignorovaním svojho vrodeného ľudského génia,  mechanicky považovať za pravdy. 
 Toto moje konštatovanie dokazuje aj tá skutočnosť, že najhoršie známky z vyučovaných predmetov na školách a univerzitách na celom svete, dosahujú sa v predmetoch matematika a fyzika, ako aj to, že v týchto vedných disciplínach sa vývoj temer umŕtvil. 
 Keď niekto niečomu analyticky nerozumie, tak to nemôže ani zdokonaľovať, ani inovovať.  
 Pritom genialita človeka by sa mala prejavovať práve v zdokonaľovaní zákonov fyziky, ako aj v zdokonaľovaní zákonov matematiky. To preto, lebo tieto vedné disciplíny primárne tvoria piliere prírodnej filozofie a sekundárne aj piliere spoločenskej, čiže humanitnej filozofie ľudstva. 
  
 A. Genialita jedinca spočíva v tom, že jeho prirodzenú, vrodenú genialitu, nikto a nikdy nedokáže deformovať a to ani všeobecne uznávanými (vedeckými, náboženskými, politickými, národnostnými, či reklamnými) klamstvami. 
  
 B. Génius nikdy nesúhlasí s tým, čo mu nie je úplne jasné a čo si nedokáže exaktne, teda experimentálne overiť. 
  
 C. Génius je človek, pre ktorého poznanie pravdy a šírenie pravdy je jediným zmyslom jeho života.  
  
 D. Génius pre poznanie pravdy zasväcuje celý svoj život tým, že od detstva haľdá pravdu všetkými možnými dostupnými prostriedkami a kým pravdu nenájde, tak to hľadanie pravdy nikdy neukončí. 
  
 E. Génius sa vzdeláva sám, lebo v omyloch ľudstva žijúci učitelia ho pravdu naučiť nemôžu. 
  
 F. Génius je ten človek, ktorý svoju cestu za hľadaním pravdy ukončil autorstvom nového, revolučného prírodného (fyzikálneho) objavu, čiže autorstvom nového zákona pohybu hmoty. 
  
 G. Génius je človek, ktorý z fyziky a matematiky vie viacej, ako všetci nie geniálni fyzici a matematici na celom svete dokopy.  
   
 x 0= 0 
 Táto krava je geniálnejšia ako všetci matematici sveta, lebo ona vie ako sa má správne násobiť nulou, pričom matematici sveta nie !!! 
  
  
 H. Človek bez matematického a fyzikálneho rozhľadu, nikdy nemôže byť géniom. 
  
  
 Génius sa môže dostať na úroveň nie geniálnych jedincov ľudskej spoločnosti, iba pomocou alkoholu, drog a gemblerskej závislosti a ženskej márnivosti. 
  
 Z mojich definícii slová génius vyzdvihujem nasledovnú definíciu v znení: 
 Génius je človek, ktorý svoju cestu za hľadaním pravdy ukončil autorstvom nového, revolučného prírodného objavu, čiže autorstvom nového zákona pohybu hmoty. 
  
 Súhrnnou, Kant - JÁRAYovou definíciou slová génius je potom nasledovná definícia: 
  
  
  Génius je príroda prejavujúca sa v človeku ako rozum, ktorý dovedie človeka k autorstvu nového zákona pohybu hmoty. 
   
  
  
  Túto mnou zadefinovanú definíciu obsahu slová génius, teraz dokážem exaktným spôsobom na analýze opisu rovnomerného pohybu telesa po kružnici, lebo práve v tomto opise pohybu hmoty, sú zhutnené totálne omyly (dristy) matematiky a fyziky, ako aj totálne omyly celého ľudstva. 
   
 Ahjaľa opis rovnomerného pohybu telesa po kružnici, tak ako ho opisujú nie geniální matematici a fyzici:  
  
  
 a ako ho opisuje geniálny matematik a fyzik, pritom nesmierne skromný človek menom:  
  
  
         
  
 Rovnomerný pohyb telesa po kružnici je tak pohyb, pri ktorom sa nemení veľkosť rýchlosti pohybu telesa, ale pri ktorom mení sa iba smer pohybu telesa. 
 Problém spojený s opisom rovnomerného pohybu telesa, opísaní nie geniálnymi učiteľmi matematiky a fyziky spočíva v tom, že oni nepoznajú zákon takej sily, ktorá by mohla fungovať aj bez toho, aby zrychlovala pohyb telies, teda bez toho, aby nemenila rýchlosť telesa na ktoré pôsobí, pričom na teleso pohybujúce sa rovnomerne po kružnici, pôsobí práve takáto, pre nich zatiaľ neznáma, záhadná - tajuplná nezrýchľujúca sila. 
 A práve v tom, akým matematickým a fyzikálnym spôsobom, nie geniálni matematici a fyzici dokážu zdeformovať, znásilniť im jedinú známu, Newtonovú zrýchľujúcu silu, na nezrýchľujúcu silu, (na nie Newtonovú silu) spočíva top degenerované myslenie nie genialnych matematikov a fyzikov celého sveta a následne aj top degenerovanej, myslenie celého ľudstva. 
 (Už len preto, že proti nie geniálnemu opisu rovnomerného pohybu telesa po kružnici, pomocou nie zrýchľujúcej sily, nikto na svete nevznáša protest, že s tým nie geniálnymi opisom rovnomerného pohybu telesa po kružnici, každý jeden človek na svete so slepou oddanosť súhlasí.) 
 Pokračovanie na nasledovnej web stránke: 
 http://jaray.blog.sme.sk/clanok.asp?cl=212707&amp;bk=1160    
 eeeeeeeeeeee 
  
  
 Pre prípad, že by táto stránka skolabovala, stačí kliknuť na: http://jaray.bigbloger.lidovky.cz/clanok.asp?cl=113469&amp;bk=14334   
   
   
  
 Diskusia je možná aj na nasledovnej adrese: 
  http://jaray.blog.sme.sk/clanok.asp?cl=212953&amp;bk=72279  
   
  
 Manifest Génia Alexandra Járaya. 
  Môj viacročný boj s relativistickou, nie geniálnou vedecko akademickou obcou za to, aby ona  začala akceptovať objektívnu realitu prírody, pohol sa o kus dopredu. Dôvodom k tomu bolo moje poznanie, že z čoho konkrétneho pramení ich odpor k obsahu môjho historického objavu, pod názvom: 
  „Princíp absolútneho pohybu hmoty“. 
 Ja dnes už hodnoverne môžem povedať, že iba ja ako jediný človek na svete prišiel som na to, že v čom spočíva osudový omyl ľudstva. Osudový omyl ľudstva spočíva v tom, že nikomu z ľudí na svete nie je dná schopnosť uvažovať a diviť sa  nad tým, že ak „Zákon kauzality“ prikazuje každý proces prírody opisovať výlučne iba nerovnosťou, (lebo sila reakcie v zmysle „Zákona kauzality“ vzniká s časovým sklzom na podnet sily  akcie  a preto musí byť a aj je vždy menšia od sily akcie) potom ako je možné, že všetky fyzikálne zákony sú vyjadrené výlučne rovnicami, teda prečo všetky fyzikálne zákony ignorujú „Zákon kauzality“. 
 Moja odpoveď je nasledovná. 
 Relativistická fyzika Galilea a Newtona, stačila zdegenerovať myslenie ľudstva do takej miery, že ľudstvo už nedokáže akceptovať „Zákon kauzality“ tvrdiac, že aj bez jeho akceptácie svet napredoval v pred a aj v budúcnosti bez jeho akceptácie bude napredovať v pred. Na druhej strane sami fyzici musia konštatovať, že akceptáciou základného zákona prírody, „Zákona kauzality“, všetka ich múdrosť by sa zrazu premenila na ich totálnu tuposť. No a to, v žiadnom prípade nie je v ich osobnom záujme. To je výlučne v záujme celého laického ľudstva, ktoré zatiaľ o aplikáciu „Zákona kauzality“ v praxi nestojí. Nemá ani šajnu o čo v tejto kauze ide. 
 Takže môj boj za pravdu sa už veľmi zjednodušil. Ja už nemusím bojovať za veci ktoré laickej verejnosti nie sú známe. Ja už musím bojovať iba preto, aby vedci celého sveta brali v úvahu ten zákon prírody, ktorý oni sami učia, čiže „Zákon kauzality“. 
 V deň keď fyzika začne akceptovať jej vlastný zákon, číže „Zákon kauzality“ v praxi, skončí sa éra relativistických fyzikálnych, politických i morálnych bludov a súčasne začne sa éra absolútnej pravdy, éra  absolútneho pohybu hmoty.    
 V presadení „Zákona kauzality“ do praxi nikdy neustanem. To je mojím životným poslaním. A na viac,  boj s armádou štátom platených, nie geniálnych, vedomých či nevedomých ignorantov „Zákona kauzality“ je veľká zábava, ktorá ma privádza do stavu Nirvány, do stavu vrcholnej blaženosti. 
 Napraviť svet je teoreticky veľmi jednoduché, stačí prinútiť vedcov a politikov aby oni v každodennej praxi akceptovali Ústavou SR chránený „Zákon kauzality“.  
 Napraviť svet je v praxi temer nemožné, lebo vedci a politici majú na háku akceptáciu „Zákona kauzality“. Akceptáciu „Zákona kauzality“ môže zabezpečiť iba hnev laického ľudu a to v revolúcii pravdy. 
 Keď občanom SR vyhovuje život v prírodných, politických a morálnych bludoch, ja ich nemôžem nasilu z tých bludov vyslobodiť, lebo to ani nechcem. Ja môžem len jedno a to pripravovať revolúciu absolútnej Kauzálnej pravdy pre prípad, že občania SR už budú mať dosť so života v bludoch a v tejto veci nikdy neustanem !!! 
 Tak mi Boh pomáhaj. 
  
   
   
   

