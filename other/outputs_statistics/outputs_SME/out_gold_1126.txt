

 
      Predpoveď je určená pre územie SR, najnižšie hodnoty sú určené pre doliny a kotliny stredného a severného Slovenska, najvyššie pre juhozápad SR a okolie Bratislavy.  Najbližší týždeň okrem pondelka bude v znamení tuhých mrazov, druhá januárová dekáda sa prejaví zmiernením mrazov a  zrážkovo, hlavne na juhu Slovenska. Tlakovú výš vystriedajú frontálne systémy s tlakovou nížou.  Predpoveď na 10 až 15 deň berte ako vyhliadku.:-D  
 
 
 
 
 

 
  
 
 
 
 
nedeľa 4.1.MAX  -8 až -1°C 
 
 
                   MIN -16 až -8°C (sneženie, hlavne sever) 

 
 
pondelok 5.1. MAX  -5 až -1°C (sneženie 3-10cm) 

 
 
                        MIN -8 až -4°C 

 
 
utorok 6.1.MAX -10 až -4°C 

 
 
                   MIN -17 až -10°C, kotliny -23 až -18°C 

 
 
streda 7.1.MAX -11 až -4°C  
 
 
                   MIN -17 až -12°C, kotliny -23 až -18°C 

 
 
štvrtok 8.1.MAX -10 až -3°C 

 
 
                   MIN -17 až -11°C, kotliny -23 až -18°C 

 
 
piatok 9.1.MAX -9 až -2°C (inverzia) 

 
 
                   MIN -15 až -8°C, kotliny -20°C 

 
 
sobota 10.1.MAX -8 až -3°C 

 
 
                   MIN -16 až -8°C, kotliny -20°C 
 
 
nedeľa 11.1.MAX -4 až 2°C 

 
 
                   MIN -10 až -3°C(trvalejšie sneženie) 

 
 
pondelok 12.1.MAX -3 až 3°C 
 
 
                   MIN -6 až -2°C (sneženie) 

 
 
utorok 13.1.MAX -4 až -2°C 

 
 
                   MIN -12 až -5°C 
 
 
streda  14.1.MAX -2 až 3°C 

 
 
                   MIN -8 až -2°C 
 
 
štvrtok 15.1.MAX -3 až 2°C 

 
 
                   MIN -10 až -3°C 

 
 
piatok 16.1.MAX -5 až 1°C 

 
 
                   MIN -14 až -5°C 
 
 
sobota 17.1. MAX -5 až 1°C 

 
 
                   MIN -15 až -6°C 
 
 
nedeľa 18.1. MAX -2 až 4°C 

 
 
                   MIN -10 až -3°C
 
 
zdroje: http://www.meteogroup.co.uk/uk/home/weather/latest_model_forecasts/gfs_popup/archiv/Europe/t2m/2009010312/nothumb/on/372/ch/3b05351f94.html
 
 
http://www.meteociel.fr/modeles/gefs_cartes.php?code=0&amp;ech=120&amp;mode=2
 
 
http://www.meteociel.fr/modeles/gefs_cartes.php?ech=120&amp;code=0&amp;mode=4
 
 
http://www.shmu.sk/sk/?page=1&amp;id=meteo_tpredpoved_skcele&amp;area=cele&amp;ts=1231200000
 
 
http://www.shmu.sk/sk/?page=1186#tab
 
 
http://www.wetterzentrale.de/pics/avnpanel1.html
 
 
http://www.freemeteo.com/default.asp?pid=15&amp;la=1&amp;gid=3059096
 
 
a iné  
 

