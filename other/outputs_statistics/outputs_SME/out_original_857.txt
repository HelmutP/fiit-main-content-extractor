
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Fandel
                                        &gt;
                Nezaradené
                     
                 Katyň 

        
            
                                    12.9.2008
            o
            20:56
                        |
            Karma článku:
                10.02
            |
            Prečítané 
            2437-krát
                    
         
     
         
             

                 
                    Na poľský film Andrzeja Wajdu Katyň som sa veľmi tešil. Moje očakávania po zhliadnutí filmu boli naplnené. Do deja filmu Katyň som sa nechal vtiahnuť tak intenzívne, že som sa na takmer dve hodiny cítil byť Poliakom. Ktorý v celej hĺbke chápe zmysel otázok: Pre čo sa vlastne oplatí žiť, keď je okolo toľko zla? A komu možno veriť?
                 

                O postrieľaní viac ako 20.000 poľských dôstojníkov a príslušníkov poľskej inteligencie v Katyňskom lese počas druhej svetovej vojny som už veľa počul, aj veľa čítal. Najskôr ešte za socializmu, kedy komunisti v Československu dávali (po vzore Sovietskeho zväzu) vinu za túto genocídu Poliakov Nemcom. Už vtedy mi však môj medzičasom nebohý starý otec potichu hovorieval, že Poliakov povraždili Rusi. Film Katyň rozpráva práve o tejto historickej traume v rusko – poľských vzťahoch. Pred jeho začiatkom som sa obával, či Katyň nebude príliš psychologickým pohľadom na doráňanú poľskú dušu. Nič také sa nestalo. Aj keď psychologické poryvy človeka majúce podobu jeho boja so sebou samým („človek sa nemôže skloniť pred nepriateľom, len pred samým sebou“), vyrovnávania sa so zlom či rozhodovania sa v krízových životných etapách sú vo filme veľmi intenzívne prítomné. Paradoxne, vec, ktorú filmu asi najviac vytýkali niektorí recenzenti (podľa kritikov komplikovaný scenár brániaci divákovi vytvoriť si k postavám vzťah a citovú väzbu, čím divák zostáva nezaujatým pozorovateľom), mne vôbec nevadila. Na film Katyň som nešiel v očakávaní nejakého sladkého, lacného príbehu. Vraždy Poliakov v Katyňskom lese predstavujú strašnú udalosť v poľských dejinách. A preto som čakal to, čo mi Wajda (ako nezaujatému pozorovateľovi) v Katyni predložil.  Mrazivý, v istých momentoch naturálne pôsobiaci film. Ktorým sa tiahne ponurá nálada. V ktorom z ľudí kričí neistota po tom, ako si ich krajinu na začiatku vojny doslova rozparcelovali Nemci a Sovieti. Po ktorého pozretí je človeku doslova na grc z toho, ako sú (nielen) malé národy v historickom zrkadle iba hračkou v rukách veľmocí. A ľuďom z týchto malých národov potom nezostáva nič iné, len voliť medzi neslobodou a smrťou. Vo filme je veľmi veľa obrazovo silných scén. Hneď prvá, keď z jednej strany ženie poľských civilistov na most strach z útoku Nemcov. A z druhej strach z útoku Rusov. Nemajú kam ujsť. Takisto scéna, kedy sa hlavná ženská postava lúči so svojím mužom, poľským dôstojníkom, pred jeho transportom do zajateckého tábora v Rusku. Ten jej manžel možno ešte môže ujsť. Ale nechce zradiť svojich kamarátov. Verí/?/, že sa vráti. Silná a veľa hovoriaca je aj scéna podania si rúk nemeckých a ruských dôstojníkov. Odvlečenie profesorov univerzity priamo z auly do pracovného tábora. Vianočná modlitba a spev poľských  dôstojníkov a generálov v zajateckom tábore, po tom, ako vyšla na oblohu prvá hviezda. Scéna, ako si muž hlavnej ženskej postavy píše v zajatí denník. Ktorý sa po jeho smrti, po vojne, dostane do rúk jeho manželky. A tá vlastne až vtedy, pri pohľade na prázdne stránky po poslednom zápise do denníka tesne pred smrťou svojho manžela, pochopí a pripustí, že jej druhé ja už nežije. No a drsné sú samozrejme aj scény, kedy mestský rozhlas vyhlasuje mená ľudí, poľských dôstojníkov, ktorí boli na katyňskom zozname. A po vojne zase (klamlivo) hlási, že tie popravy spáchali Nemci. Alebo posledná asi desaťminútovka, v ktorej Wajda  v naturálnych obrazoch ukazuje popravy poľských vojakov. Ich vyťahovanie z auta, ktoré ich priviezlo do Katyňského lesa. Smrť v očiach pri pohľade na masový hrob. Zviazanie rúk. A poprava strelou do tyla. Na záver je jama s telami Poliakov zahrabávaná... Katyň je strašný film, po ktorom mi bolo veľmi smutno. To smutno zosilňuje aj záverečný obraz na tmavé plátno, nasledujúci po zasypaní masového hrobu. Diváci počujú len smutnú, vážnu hudbu. A rozmýšľajú, v akom svete žili naši predkovia. A v akom svete žijeme aj my. Slobodnom? Či len naoko slobodnom? Môže byť vôbec aj malý národ niekedy slobodný? Naozaj slobodný? Nie je to často iba virtuálna sloboda? Nie je iba dočasná? Nie sme iba bezvýznamnými figúrkami na šachovnici sveta, v ktorej priebeh hry, vrátane rozhodovania o našom bytí či nebytí, určujú iní?

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (44)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Zbraň na votrelcov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            ...až na hranicu úzkostnej tmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Tolerantný je len ten človek, ktorý je za registrované partnerstvá?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Učebnicový dvojaký meter z Haagu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Fandel 
                                        
                                            Dav ľudu vydal Krista na kríž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Fandel
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Fandel
            
         
        fandel.blog.sme.sk (rss)
         
                                     
     
         Obyčajný človek 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    110
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1617
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




