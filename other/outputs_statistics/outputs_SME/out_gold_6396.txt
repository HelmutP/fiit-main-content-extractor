

  
    
    
    
 Slovensko je najchudobnejším členom eurozóny. Sme chudobnejší ako Gréci, ktorí dlhé roky žili vysoko nad pomery, míňali viac než majú a dnes nevedia, ako svoje nahromadené dlhy budú splácať. 
 Navyše, aj my sme v rastúcich problémoch - vinou neschopnej Ficovej vlády, ktorá nedokázala na príchod krízy zareagovať, narastá zadlženie Slovenska rekordným tempom, a ak bude týmto tempom pokračovať, o pár rokov dopadneme rovnako ako Gréci. 
 A napokon, ak sa pozrieme na príspevky jednotlivých krajín eurozóny na pôžičku pre Grécko a prerátame ich ako podiel priemernej mzdy v každej krajine, výsledok je, že príspevok Slovenska je oveľa vyšší než v prípade ostatných členov eurozóny (viac v tabuľke). Je to dôsledok toho, že na Slovensku sú nízke platy, ale presne to ukazuje, že chudobnejší idú prispievať bohatším, pričom schopnosť Grécka splatiť dlhy, nech by teraz šetrili akokoľvek, spochybňujú aj tí najrenomovanejší ekonómovia. 
 Nemám nič proti princípu solidarity, rešpektujem záväzky Slovenska voči okolitému svetu a chápem dilemy, pred ktorými stoja ekonómovia aj štátnici v iných krajinách. To však neznamená, že musíme súhlasiť s akoukoľvek pomocou Grécku, keď navyše sami dnes kráčame jeho smerom. 
 Je čas na zmenu. 
   
   
   
 Tabuľka: Podiel pôžičky Grécku na priemernej mzde danej krajiny 
 
 Belgicko   0,19% 
 Cyprus   0,20% 
 Fínsko   0,18% 
 Francúzsko   0,22% 
 Holandsko   0,14% 
 Írsko   0,17% 
 Luxembursko 0,24% 
 Malta   0,33% 
 Nemecko   0,16% 
 Portugalsko   0,28% 
 Rakúsko   0,17% 
 Slovinsko   0,31% 
 Slovensko   0,43% 
 Španielsko   0,23% 
Zdroje: Eurostat, ECB, IMF, ILO
 

