

                               Spravodajský príspevok o slovenčine na školách pre menšiny. 
                                Viac gramatiky, či viac konverzácie ? 
                                Názory sa rôznia, podľa kľúča  - pozri, kto to hovorí. Dvaja žiaci, jedna učiteľka a László Szigeti unisono tvrdia -  gramatiky menej, chceme viac konverzovať. Hovoria po slovensky a až na jednu chybičku ( niet sa čo čudovať, mladá slečna v telke ešte zrejme nebola a tréma vie urobiť aj horšie veci ako pomýliť jednotné a množné číslo) aj GRAMATICKY správne. 
                                 No a kto je na druhej strane ? Minister školstva. Protivník najťažšieho kalibru. Teraz zaujme ministerský, odborný a pretože sa zrejme neučil slovenčinu v škole pre národnostné menšiny, aj brilantnou slovenčinou vyjadrený názor. Všetci pochopia, ako sa dá svedomitým štúdiom slovenskej gramatiky dopracovať ku kultivovanému a bezchybnému jazykovému prejavu. Určite to deti zo škôl pre národnostné menšiny zaujme, a ak sa doteraz učili gramatiku s odporom, všetko sa zmení, pretože budú chcieť rozprávať po slovensky tak dokonale, ako pán minister ŠKOLSTVA.    
                              Minister školstva má na celú vec opačný názor. Všetkým to jasne a zrozumiteľne vysvetlí: 
                     
                              „ Nemôžte KONZERVOVAŤ,  pokiaľ nepoznáte gramatiku. 
                     
                              Šok. Všetci čakali, že to bude o slovenčine, a on hovorí o zaváraní. 
                              Keď deti zo škôl pre národnostné menšiny pochopia , že slovo  „môžte" je podivným patvarom slovesa „ môžete ", budú sa musieť vysporiadať zo zásadnou otázkou: 
                             
                               Ako môže dobrá znalosť slovenskej gramatiky ovplyvniť zaváranie?  
                               
                              Keďže školy pre národnostné menšiny a osobitne tie, pre tú maďarskú sa nachádzajú predovšetkým na juhu Slovenska, známeho svojou hojnou produkciou zeleniny a ovocia, pre mnohé z nich je to perspektívne otázka existenčná. 
                               Veď uznajte, ako sa môže Maďar z juhu Slovenska zmieriť z osudom, že si tie uhorky, čo vypestuje nebude môcť ani len naložiť, ak nebude vedieť správne použiť prechodník slovesa zavárať a príčastie minulé trpné slova nakladať ? 
                                ( Je síce pravdou, že moja mama, ktorá v gramatike tiež nevyniká, robí nakladané uhorky, ktoré ju preslávili široko-ďaleko, ale ona chodila do slovenskej školy a tam gramatika pre zaváranie až taká potrebná nie je. Vo všeobecnosti my, štátotvorný národ, tú gramatiku až tak vážne neberieme, čoho dôkazom je práve minister školstva, ale reč bola o menšinách , nie ? ) 
                               Ak by som bola školáčkou v škole pre národnostné menšiny, lúskala by som tú gramatiku dňom i nocou. A keď by som ju dolúskala, konzervovala by som a konzervovala a..................... 
   
   
           

