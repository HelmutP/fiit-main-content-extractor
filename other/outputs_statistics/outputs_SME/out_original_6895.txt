
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Kristína Kováčiková
                                        &gt;
                Súkromné
                     
                 Anjel Pána 

        
            
                                    20.5.2010
            o
            15:00
                        |
            Karma článku:
                9.61
            |
            Prečítané 
            2412-krát
                    
         
     
         
             

                 
                    Dvanásť hodín. Babka sa nahlas modlí. Slíže dlhé pol metra mi okrem taniera nechtiac šplechne do malinovky. "Tomu nič neni, to vypiješ," vyberá ich hrubými prstami z pohára. "Zdravas Maria..." recituje ďalej. Na toto z detstva si spomeniem, keď počujem ostrovné zvony udrieť dvanásťkrát, na babku so šedým zákalom v oku, rovnako šedá je teraz obloha, rovnako zakalené je aj more.
                 

                    Ráno pršalo, miestne babky si ako obyčajne spievali v kafići, lebo došla penzia. Hovorím tomu malá omša v kafići, chudák Poliak bude tú oficiálnu večernú zase slúžiť sám pre seba. V nedeľu utrúsi čosi o telenovelách, ktorým dávame prednosť pred Bohom. Päť a pol človeka opúšťa brány kostola, evanjelizácia dediny musí čakať do najbližšej nedele. Netuší o malej omši. Ani o tom, že telenovelové seanse sa konajú doobeda, hneď po nej. Susedy sa stretnú, aby komentovali reprízu. Okrem iného.   "Samota je ubíjajúca," povedala mi pri takej príležitosti Ruža, na krku vrásky nazbierané za vyše osemdesiat rokov. Uprela na mňa pohľad, v ktorom som sa takmer utopila. Neviem, prečo som uhla. Asi že nechcem vidieť, ako sa mi raz modré oko zmení do šeda.   Keď sa rozhovorí vdovica, tie vydaté na chvíľu zabudnú, že sa majú hnevať na ubúdajúce litre z demižóna. Spomínajú na časy, kedy čakali na svojich milých, plaviacich sa bohviekde. Keď dorazia  zo seansy domov, utrú prach zo zažltnutej svadobnej fotografie.   Kým Janko spí, obzerajú si ho, ale nevidia ho, lebo myslia na svojich Jankov. Keď sa zobudí na hlad, každá by ho chcela nachovať.   "Najprv nám pijú mlieko a potom krv," povie jedna za všetky. Rozpráva skôr sebe ako nám, ako si každý deň želala, nech už je väčšie, nech už je len väčšie... nech to len rýchlo prejde. A prešlo tak rýchlo, že dnes by si priala, aby bol vtedy zastal čas.   Ja viem, že Janka mám, aby som sa naučila žiť prítomnosť.   Nemáme svadobnú fotografiu, nebudem mať z čoho utrieť prach, keď raz na to príde.   Odchádzam. Dvanásť hodín.   Kostolné zvony zvonia Anjel Pána. V dedine nie je živej duše. Kafić je zatvorený, zatvorené sú aj okenice domov, poľa ktorých prechádzam. Z poniektorej kuće besnie rádio na plné gule, neklamný znak, že nikoho nemá doma. Ktosi kdesi píli. A to žlté lietadlo od požiarnikov mi krúži nad hlavou. Takmer Beethovenova symfónia.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristína Kováčiková 
                                        
                                            Čo si za mater. Test pred železiarstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristína Kováčiková 
                                        
                                            Žiť si na morskej MHD
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristína Kováčiková 
                                        
                                            Medzitým na ostrove
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristína Kováčiková 
                                        
                                            Zubárova žena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristína Kováčiková 
                                        
                                            Právnické eso?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Kristína Kováčiková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Kristína Kováčiková
            
         
        kikakovacikova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Nespí
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3919
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            chorvátskové
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Hyperaktívne deti, retardovaní rodičia
                                     
                                                                             
                                            Prečo muži ignorujú výročia
                                     
                                                                             
                                            Materská na ostrove
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Sprievodca pôrodnicami
                                     
                                                                             
                                            Rodinka
                                     
                                                                             
                                            Tehotenstvo
                                     
                                                                             
                                            Babetko
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




