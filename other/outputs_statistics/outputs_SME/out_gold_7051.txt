

 1. Z akého dôvodu polícia SR nerozohnala neohlásené protizhromaždenie Slovenskej pospolitosti resp. ĽS-NS, ale dovolila im zhromaždiť sa v relatívne veľkom množstve? 
 2. Z akého dôvodu polícia nezabezpečila riadne ohlásenú trasu pochodu a dovolila neohlásené protizhromaždenie ĽS-NS? 
 3. Z akého dôvodu neupozornila členov a sympatizantov protizhromaždenia Slovenskej pospolitosti resp. ĽS-NS, že na daný čas a dané miesto nemajú riadne ohlásené zhromaždenie a zároveň ich neohlásené zhromaždenie priamo zasahuje do priestoru riadne ohláseného zhromaždenia resp. pochodu? 
 4. Z akého dôvodu bolo umožnené viacnásobne napadnutie účastníkov riadne ohláseného zhromaždenia dokázateľne tou istou skupinou ľudí? 
 5. Z akého dôvodu bolo vôbec umožnené zhlukovanie osôb, ktoré mali viditeľne znaky, ktoré ich jednoznačne identifikovali ako členov Slovenskej pospolitosti resp. ĽS-NS? Nie je úlohou polície predchádzať možným problémom už v zárodku a nie až a posteriori? 
  
 Z akého dôvodu policajti na fotografii nerozpúšťajú neohlásené zhromaždenie, ktoré zasahuje do priestoru riadne ohláseného zhromaždenia a iba tak postávajú? 
 Tieto otázky dávam osobe, ktorá je po každej stránke zodpovedná za činnosť polície SR - ministrovi vnútra Róbertovi Kaliňákovi. 
 Nech si už myslíme o požiadavkách účastníkov dúhového pochodu čokoľvek (o tom je práve sloboda - jeden človek môže súhlasiť, iný nie), mali v každom prípade riadne ústavné právo na zhromaždenie a slobodu prejavu. A toto ústavné právo im bolo de facto uprené. Môžeme polemizovať, či bolo vhodné zvolávať podobné zhromaždenie tri týždne pred voľbami, kedy určité skupiny (SNS a ĽS-NS) využijú každú možnosť na sebaprezentáciu. Zaujímavá by tiež bola štúdia o motivácií členov Slovenskej pospolitosti narúšať ústavné práva občanov (žeby latentná homosexualita?). 
 Jedna vec je však istá, slovenská polícia opäť ukázala, že nedokáže ochrániť ani vlastných občanov. 
   

