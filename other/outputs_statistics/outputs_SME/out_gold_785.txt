

 
V zásade sa dá k prieskumom verejnej mienky (ale aj iným štatistickým údajom)pristupovať kriticky v dvoch odlišných rovinách.
 
 
	 Ako ku svojim výsledkom prišli 
	 Ako svoje výsledky vyhodnocujú a čo tým vyhodnotením sledujú 
 
 
Najprv k prvému bodu. 
 
 
Pred časom ma pri jednom nákupnom centre oslovil pán s anketovým papierom a položil mi otázku, aké vitamínové prípravky užívam. Keď som na neho nechápavo pozrel a povedal, že žiadne, tak mi povedal:"Tak to nie ste vhodný do našej ankety." Je zjavné, že dôležité je vybrať správnych ľudí, aby výsledok mal nejakú výpovednú hodnotu. Celé sa to krúti okolo známeho pojmu reprezentatívna vzorka. Pokiaľ chcete skúmať len tých, ktorí si tie šumivé celaskony kupujú, tak jeho otázka mala význam. Aj v tom prípade by ale skúmal len ľudí, ktorí by mali čas a boli ochotní na jeho otázky odpovedať - tiež dá pochybovať o tom, či by bola vzorka reprezentatívna. Prvé úskalie teda je, koho si ako respondenta vybrať. 
 
 
Každý zrejme pochopí, že nie je jednoduché vybrať ľudí tak, aby v malej vzorke boli zastúpené všetky kategórie vzniknuté z rôznych kombinácií spoločných znakov a ich zastúpenie zodpovedalo zastúpeniu celej spoločnosti (napr. vek: 0-15, 15-25 atd.; vzdelanie: základné, stredoškolské, vysokoškolské; bydlisko: dedina do 1000 obyvateľov atd., počet členov domácnosti: 1,2,3,4,5 a viac. Všetky tieto kritériá možno skombinovať a v každej kategórii by sa mal niekto nachádzať). Zjavne teda nepôjde o náhodný výber ale o výber cielený. Samozrejme pre organizáciu, ktorá prieskum vykonáva je lacnejšie osloviť tisíc ľudí napr. pri vstupe do divadla ako sledovať v databáze u každého potenciálneho respondenta, koľko má rokov, kde býva a pod. (Aj keď sa mi stalo, že som raz zdvihol telefón a niekto sa pýtal, či je v domácnosti niekto vo veku 35-45 rokov. Každopádne je to viac roboty ako si kúpiť lístok na vlak a spríjemňovať náhodným cestujúcim cestu otázkami.) O to zaujímavejšie je zistenie, že pri prezentácii výsledkov nezverejňujú ako sa k výsledkom dopracovali (uvedú, že reprezentatívna vzorka, nepochvália sa ale, ako k nej prišli. Zrejme je to modernou terminológiou povedané how-know:), ale výsledky samotné. Problém čislo jedna by sme teda definovali ako výber vzorky, ktorá má zastupovať celok.
 
 
Tiež si spomínam, že sme sa ako stredoškoláci zúčastnili celá trieda dopravného prieskumu na území Bratislavy. Ja som vtedy sedel pri Bajkalskej a robil čiarky, boli ale aj spolužiaci, ktorí na menej frekventovaných cestách zapisovali značky. Pôvodným zámerom bolo, aby sa zistilo, kde sa náhodne vybrané autá pohybujú a vyskytujú - dalo by sa podľa toho zovšeobecniť, medzi ktorými mestskými časťami je najintenzívnejšia doprava a naopak. Zvrhlo sa to až vtedy, keď si ľudia začali tie ešpézeky vymýšľať (aby mali menej roboty) a vrchol bol, keď si spolužiak vymyslel auto s medzinárodnou poznávaciou značkou Kazachstanu a zatelefonoval druhému, nech si ho napíše do toho hárku tiež. Ďalší problém by sme teda pomenovali ako poctivosť ľudí, ktorí prieskum robia a správne zaznamenanie výsledkov.
 
 
Tretím problémom, ktorý sa týka samotného vykonávateľa prieskumu je to, že si výsledky kompletne vymyslí, sledujúc tým nejaký cieľ. (Napríklad, že mu niekto za výsledky zaplatí) 
 
 
Pred nedávnom ma v Tullne zastavila istá slečna, že chce so mnou urobiť krátku anketu o nakupovaní a parkovaní v ich meste. Medzi nie veľmi zaujímavými rakúskymi ženami bola mimoriadne sympatická, tak som povedal, že môžeme. Problém nastal až vtedy, keď som jednej otázke nerozumel. Pochopiteľne som nechcel vyzerať ako taký tĺk, tak som odpovedal, že áno. Vôbec netuším na akú otázku. Ono nepochopenie otázky znie ako nejaký marginálny problém cudzincov, keď však niekto položí otázku typu:"Ste proti názoru amerického prezidenta, že netreba zvyšovať počet vojakov v Afghanistane?" a odpoviem áno, tak som proti zvyšovaniu alebo nie? Inak povedané, ak je otázka komplikovane formulovaná, respondenti ju nemusia správne pochopiť. 
 
 
Ďalšia vec je, že ľudia klamú. Isto ste si všimli, že niektoré politické strany majú vo voľbách vždy lepší výsledok ako v prieskumoch. (Aby som nerobil propagandu nemenujem, každému je ale asi jasné, že ich voliči sa boja nahlas priznať, koho budú voliť.) V prieskume trhu tiež nikto neprizná, že si ide kúpiť novú telku, lebo susedia majú tiež, ale uvedú, že chcú lepší obraz.  
 
 
Často sa stáva aj to, že ľudia myslia na niečo iné ako v skutočnosti chcú. Jednoducho niečo povedia, v realite sa ale nakoniec rozhodnú ináč. Keď si to stále neviete predstaviť, spomeňte si ako ste naposledy nakupovali v hypermarkete a koľko ste kúpili vecí, ktoré ste nemali pôvodne v pláne. Niektorí autori zaoberajúci sa štatistikou uvádzajú, že tento rozpor vzniká hlavne z toho, že ak príde domov dotazník, jeho vyplnenie ostane na muža, ale pri rozhodovaní má posledné slovo žena a pred definitívnym rozhodnutím ho niekoľkokrát zmení. Koniec-koncov koľko chlapov stretnete v obchode so zoznamom nákupu od svojej ženy a koľko žien s papierikom od muža? Podobne pred voľbami v prieskumoch vám ľudia ochotne povedia svojho kandidáta a povedia, že pôjdu voliť, keď je však v deň volieb slnečné počasie, nájdete ich na kúpalisku a nie pri volebných urnách. Čiže ľudia neúmyselne uvedú niečo iné, ako sa nakoniec rozhodnú.
 
 
Proti týmto skresľovaniam reality je bežný človek prakticky bezbranný. Jedinou obranou by bolo spätne skontrolovať, ako sa prieskumy líšia od skutočných výsledkov a porovnať jednotlivé agentúry, ktoré ich robili. Toto sa samozrejme dá iba u niektorých druhov, napríklad pri voľbách, keď je vysledok celkovej populácie známy. Presnosť správne realizovaných prieskumov sa podľa literatúry 1,2 pohybuje okolo dvoch percent hore-dole (Pri podrobnejšom stanovení by už vynaložená nadpráca nebola adekvátna dosiahnutému spresneniu výsledku). Ak je to viac, agentúra jeden z možných problémov neeliminovala správnou metodikou (Poctivosť prieskumníkov môže kontrolovať sama. Pre kontrolu odpovedí dávajú psychológovia do série otázok také, ktoré by ukázali, že respondent odpovedá nesústredene, náhodne tipuje alebo klame tým, že na podobné otázky odpovedá rozdielne).  Podľa odchýlky od skutočnej hodnoty dokážete spoznať, kto robí prieskum poctivo a pedantne a kto ledabolo. A to už nebude náhoda. Potom neostáva než veriť prieskumom iba na základe skúseností s ich realizátormi.  
 
 
Toľko k problémom prieskumu, teraz prejdem k interpretácií. Tu je nutné byť ostražitý, výsledok prieskumu sám osebe nie je klamstvom, hovorí pravdu, častokrát sa ale snaží vztiahnuť výsledok na niečo iné, poprípade prekrútiť realitu podľa toho, čo dotyčný chce dosiahnuť. 
 
 
Obzvlášť efektívne sa dá kúzliť s percentami. Ak by som bol predseda JRD a zvýšil by som  produkciu cukrovej repy zo 100 ton na 700 ton, poslal by som na okres údaj, že sme zvýšili produkciu o 600% a bol by zo mňa hrdina socialistickej práce. Ak by ale moje družstvo bolo veľké a zo 100 000 ton by som zvýšil na 110 000 ton, isto by som napísal do novín, že naše družstvo dokázalo zvýšiť produkciu o 10 000 ton a percentá by som zamlčal. Čo mám informácie od učiteľa na strednej odbornej škole, na jej úrovni vzdelania samotným percentám rozumie máloktorý študent. (Alebo ako on hovorí: Blbcov je neskutočne viac, ako si len dokážeš predstaviť.) Bez absolútnych hodnôt aj takýmto mimoriadne priehľadným trikom s percentami oblbnete skoro všetkých. Neberte preto nikdy za smerodajné údaje, ktoré sú iba v percentách. Alebo iný príklad. Z prieskumu vyplynie, že určitá časť dôchodcov nie je spokojná s výškou svojho dôchodku. Ak by som bol minister, tak spravím vyhlásenie:"Iba 16,667% dôchodcov nie je spokojných s dôchodkami." Ak naopak opozičný poslanec, tak vykríknem: "Až každý šiesty dôchodca je nespokojný." Zrejme väčšina z vás cíti ten psychologický rozdiel, hoci sa jedná o to isté číslo. 
 
 
Ešte dve krátke poznámky k percentám. Ako bolo spomenuté, presnosť prieskumov verejnej mienky sa pohybuje okolo 2 percent. Je teda absolútne pošetilé živo diskutovať o poklese preferencií jednej strany o 0,5 %. Rovnako zaváňajú amaterizmom prieskumy, uvádzajúce výsledky na stotiny (alebo nebodaj tisíciny) percenta.   Predsa len štatistika je veda premieňajúca presné údaje na nepresné čísla.
 
 
Veľmi podobné je to s priemerom. Ak sa nechcete stať obeťou demagóga, tak ignorujte akýkoľvek údaj spojený s priemerom. Nádherný príklad je dookola omieľaná priemerná mzda. Predstavte si, že priemerná mzda v dedine je 1000 eur. Manažér jedného podniku zarába mesačne 4000 eur. Na to, aby bola priemerná mzda 1000 eur musí na tohoto manažera pripadať šesť ľudí pracujúcich za 500 eur. (Tu sa dopúšťam značného skreslenia ja. Chcem však ukázať, že v celku s nízkym príjmom vie jedna "uletená" hodnota značne ovplyvniť priemer.) To značí, že v našom príklade šesť ľudí zo siedmych túto mzdu nedosahuje. Čiže darmo ja budem hovoriť, koľko bochníkov chleba alebo litrov benzínu si človek z tohoto príkladu za priemernú mzdu môže kúpiť, keď väčšina ľudí túto mzdu nemá. (A ani v realite nemá. Plat je totiž zdola ohraničený minimálnou mzdou, zhora ale nie.) Jednoducho priemerná mzda nie je údaj, ktorý by vypovedal, koľko zarába „priemerný človek", lebo astronomické platy malej skupiny ľudí, ju „ťahajú" smerom hore. Že ju tak sverepo všade citujú má dve príčiny - vyvoláva pocit, že priemerný človek sa má lepšie ako v skutočnosti (to je určite hlavný dôvod) a sú od nej odvodené platy ústavných činiteľov. Pokiaľ by priemerná hodnota mala o niečom vypovedať, mali by byť hodnoty rozmiestnené okolo nej súmerne. A nielen to, mali by byť rozmiestnené v blízkosti toho priemeru. Keby ste mali nohy v teplote -30°C a hlavu v prostredí s teplotou 70°C, nehovorili by ste, že sa máte dobre, hoci v priemere by vonkajšia teplota bola 20°C.
 
 
Ďalšou rafinovanou fintou je porovnávať výsledky, ktoré spolu nesúvisia. Veľmi otrepaný príklad je, že najnebezpečnejším povolaním je žiak základnej školy a najbezpečnejším pápež. Vymyslím si, že prieskum nám dal výsledok, že priemerný vek úmrtia školáka je desať rokov, kdežto pápeža povedzme 75. Jedná sa o tú istú vec (priemerný vek úmrtia), dúfam však, že je každému jasné, že to výberové kritérium (povolanie) je závislé od veku. Školákom je človek od šiestich do štrnástich rokov, pápežom sa ťažko niekto stane štrnásťročný a je ním do svojej smrti. Nehovoriac o tom, že je utrhnuté od reality porovnávať vek úmrtia, ktorý očividne so zamestnaním nesúvisí - úmrtie školáka (rovnako pápeža) ťažko považovať za pracovný úraz alebo za následok choroby z povolania. Podobne by mohol policajný zbor ukázať štatistiku kradnutia kočov za posledných sto rokov a na nej ukázať, že sa výrazne znížil počet odcudzení týchto dopravných prostriedkov a považovať to za svoj úspech v boji so zločinom. Toto teraz vyzerá ako pomerne primitívna finta, pokiaľ však niekto takto začne hodnotiť zložitejšie javy, pri ktorých je príčin veľa a nie sú bez zamyslenia všetky zrejmé, tak vám bez problémov vymyje mozog tým, že si vyberie iba tie, ktoré sa mu hodia, poprípade to zdôvodní niečím, čo príčinou ani nie je. Ako povedal Benjamin Disraeli: "There are three kinds of lies: lies, damned lies and 
statistics." Existujú tri druhy lží: klamstvá, totálne klamstvá a štatistiky. 
 
 
Veľmi častým a mimoriadne obľúbeným prvkom je strašenie budúcnosťou. Niekto zverejní nejakú štatistiku, na základe ktorej nejakým záhadným spôsobom odvodí ďalší priebeh - a panika je na svete. Fakt neviem, kto má z toho úžitok, ale pravdou je, že človek, ktorý má strach, prestane konať rozumom a začne sa správať stádovito. (Príkladov z histórie by ste našli bezpočet, nakoniec fašizmus aj komunizmus boli založené len na strachu.) Spomeňte si na strach z roku 2000, keď mal byť kvôli dvom nulám koniec sveta, spomeňte si, ako pár údajov o pravdepodobnosti šírenia vtáčej chrípky na človeka malo pomaly vyhubiť ľudstvo, rovnako rýchlosť šírenia AIDS, eboly... Ak astronómovia vypočítajú pravdopodobnosť zrážky s nejakým telesom na 1:30 000, obehne to všetky noviny. Vždy tam však pri predpovedi chýba zohľadnený jeden fakt - že existujú protiopatrenia a to nové ochorenie sa nebude šíriť tak rýchlo, ako na začiatku, keď sa proti nemu nevedelo bojovať. Navyše aj u iných javov sa časom takmer vždy vyskytnú okolnosti, ktoré podmienky radikálne zmenia. Rôzne ekologické hnutia radi strašia zásobou fosílnych palív, ktorá má vydržať napríklad už iba 20 rokov. Neuvedú ale, že za tých dvadsať rokov sa stále môžu nájsť nové náleziská, ale sa aj posunie dopredu technológia získavania surovín - stanú sa zaujímavými zásoby, ktoré v súčasnosti nie sú pre nízku efektivitu využívané. 
 
No a úplne na záver jeden grandiózny podvod - grafy. Zatiaľčo text čitateľ často preskočí, farebný graf podvedome každého zaujme. To by bolo na celý článok, za všetky uvediem jeden obrázok v ilustračnom príklade a jeden citát.  
 
"Grafy sestavené z panáčků, stromečků, strojčeků, domečků nebo dokonce jásajících či smutných dětiček, jistě podsouvají něco, co je ve skutečnosti ůplně jinak. Doporučuji pokochat se jejich výtvarným provedením (pokud za kochání stojí) a pak celé svěží dílko i s doprovodným textem zahodit." 2
 
 
Aby ste si vedeli lepšie predstaviť, o čom bola reč, pripravil som ilustračný príklad, kde sú dva pokrivené pohľady na tie isté fakty. 
 
 
  
 
 
Nepochybujem, že štatistické dáta sú pre život dôležité, hovoria nám veľa o okolitom svete a na rozdiel od iných metód, prehľadne, spoľahlivo, zrozumiteľne a rýchlo sa šíria medzi verejnosťou. Práve pre tieto vlastnosti sú však veľmi často zneužívané. Ako si na také dezinformácie a manipulácie dávať pozor, sa vám snažil ukázať tento článok. V prvom rade by ste si mali všimnúť, či sú výsledky zverejnené celé, alebo iba vybraná časť z nich. A závery si skúsiť urobiť samostatne a nenechať si nahovoriť názor autora, poprípade si ním iba pomôcť. Rozhodne by som ale nerealizoval heslo z nadpisu - že nemožno veriť ničomu.
 
 
 
[1] BURGER, E. - STARBIRD, M.: Wie man den Jackpot knackt, Rowohlt Verlag, Reinbek bei Hamburg, 2007 
[2] MAREŠ, M.: Slova, která se hodí, Academia, Praha, 2006   
 

