
 Najprv sa stručne pozrime na spisovateľovu životnú púť. Edgar Horatio Wallace sa narodil 01.04.1875 v londýnskom Greenwichi hereckému páru Polly Richards a Richardovi Horatiovi Edgarovi Marriottovi. Novonarodené dieťa matka odložila do sirotinca, kde ho po 9 dňoch adoptoval ako svoje jedenáste dieťa nosič na londýnskom rybom trhu Dick Freeman. 
Už v 11- tich rokoch po absolvovaní štyroch tried opustil školu a začal pracovať. Spočiatku sa živil podradnou prácou – roznášal mlieko, noviny, predával kvety, topánky, pracoval na stavbách, v tlačiarni, ale aj jako poslíček a kuchár na mori. V 18 – tich rokoch vstúpil do britskej armády a v rokoch 1893 – 1896 slúžil v jednotke Royal West Kent Regiment. Následne ho odvelili do Južnej Afriky, kde pôsobil v sanitnom oddiely. V tom čase sa pokúšal o prvú literárnu tvorbu. V jeho úsilí mu pomáhal reverend Caldecott z Kapského města. Mladý Wallace začal prispievať do novín a časopisov a napísal básne s vojnovou tématikou pod názvom “The Mission That Failed”, ktorú vydal v roku 1898. Po prepustení z armády na druhý rok začal pracovať ako dopisovateľ pre tlačovú agentúru Reuters a pre londýnsky denník Daily Mail. Svojími článkami o brutalite britskej armády behom druhej búrskej vojny natoľko popudil ich velitel maršála Kitchnera, že mu zakázal pracovať ako vojnový spravodajca. 
V roku 1901 se oženil s Yvy Caldecottovou, začal pracovať v novinách Rand Daily Mail v Johannesburgu a roku 1902 odišiel do Anglicka.
Svoj prvý román „The Four Just Men“ o skupine mužov berúcich zákon do vlastných rúk vydal vo vlastnom náklade v roku 1905. Kniha mala úspech, ale kvôli zlej propagácii skončila pre autora finančným neúspechom. Od vydania románu „Sanders of the River“ v roku 1911, prvného zväzku jeho detektívnej série z Afriky s komisárom Sandersom, jeho zástupcom poručíkom Bonesom a domorodým náčelníkom Bosambom, už prežíval len literárne úspěchy.
V roku 1918 skončilo jeho prvé manželstvo rozvodom. V roku 1921 se oženil druhýkrát so svojou o 23 rokov mladšou sekretárkou Violet.
Najslavnejší román „The Green Archer“ napísal v roku 1923 a vďaka nemu sa stal najvydávanejším autorom svojej doby. Jeho produktivita bola obrovská (vydal 175 kníh a napísal 24 divadelných hier). Denne pracoval mnoho hodín, každú polhodinu vypil šálku čaju a nepretržite fajčil cigarety.
Pracoval aj ako režisér. V roku 1929 natočil kriminálku „Red Aces” a na budúci rok “The Squeaker”. Pre britský film napísal v roku 1931 scenár „The Hound of the Baskervilles“, podľa románu sira Arthura Conana Doyleho. Začiatkom roku 1932 začal pracovať na scénári pre americký film „King Kong”. Spracoval však len námet, lebo 10.01.1932 zomrel na ceste do Hollywoodu na zápal pľúc.

  Predvojnové filmy natočené v Nemecku: 

Der große Unbekannte (1927) r. Manfred Noa 
Der rote Kreis (1929) r. Frederic Zelnik 
Der Würger (1929) r. neznámy
Udavač, Der Zinker ( 1931 ) r. Karel Lamač, Martin Frič 
Maska, Der Hexer ( 1932 ) r. Karel Lamač, Martin Frič ( filmový materiál sa nezachoval )
Der Doppelgänger (1934) r. E.W.Emo

  Povojnová produkcia: 

Der Frosch mit der Maske (1959) r. Harald Reinl ( postavu detektíva hral Siegfried Lowitz )
Der rote Kreis (1960) r. Jurgen Roland ( Karl-Georg Saebisch ) 
Der Rächer (1960) r. Karl Anton ( Heinz Drache ) 
Die Bande des Schreckens (1960) r. Harald Reinl ( Joachim Fuchsberger ) 
Zelený lukostrelec, Der grüne Bogenschütze (1961) r. Jurgen Roland ( Klausjurgen Wussow ) 
Mŕtve oči Londýna, Die toten Augen von London (1961) r. Alfred Vohrer ( Joachim Fuchsberger )
Das Geheimnis der gelben Narzissen (1961) r. Ákos Ráthonyi ( Joachim Fuchsberger )
Peňazokaz, Der Fälscher von London (1961) r. Harald Reinl ( Siegfried Lowitz )
Das Rätsel der roten Orchidee (1962) r. Josef von Báky ( Joachim Fuchsberger )
Die Tür mit den sieben Schlössern (1962) r. Alfred Vohrer ( Heinz Drache )
Hostinec na Temži, Das Gasthaus an der Themse (1962) r. Alfred Vohrer ( Joachim Fuchsberger )
Der Fluch der gelben Schlange (1963) r. Franz Josef Gottlieb ( Joachim Fuchsberger )
Der Zinker (1963) r. Alfred Vohrer ( Heinz Drache )
Der schwarze Abt (1963) r. Franz Josef Gottlieb ( Joachim Fuchsberger )
Indická šatka, Das indische Tuch (1963) r. Alfred Vohrer ( Heinz Drache )
Todestrommeln am großen Fluß (1963) r. Lawrence Huntington ( Richard Todd ) anglický film
Zimmer 13 (1964) r. Harald Reinl ( Joachim Fuchsberger )
Die Gruft mit dem Rätselschloß (1964) r. Franz Josef Gottlieb ( Harald Leipnitz ) 
Der Hexer (1964) r. Alfred Vohrer ( Joachim Fuchsberger )
Das Verrätertor (1964) r. Freddie Francis ( Edward Underdown ) v koprodukcii s Anglickom
Sanders und das Schiff des Todes (1965) r.Robert Lynn ( Richard Todd ) anglický film
Neues vom Hexer (1965) r. Alfred Vohrer ( Heinz Drache )
Tajomný mních, Der unheimliche Mönch (1965) r. Harald Reinl ( Harald Leipnitz )
Das Rätsel des silbernen Dreieck (1966) r. John Llewellyn Moxey ( Christopher Lee )
Der Bucklige von Soho (1966) r. Alfred Vohrer ( Gunther Stoll ) 
Znamenie trojuholníka, Das Geheimnis der weißen Nonne (1966) r. Cyril Frankel ( Stewart Granger ) v koprodukcii s Anglickom
Die blaue Hand (1967) r. Alfred Vohrer ( Harald Leipnitz )
Der Mönch mit der Peitsche (1967) r. Alfred Vohrer ( Joachim Fuchsberger )
Der Hund von Blackwood Castle (1968) r. Alfred Vohrer ( Heinz Drache ) 
Im Banne des Unheimlichen (1968) r. Alfred Vohrer ( Joachim Fuchsberger )
Der Gorilla von Soho (1968) r. Alfred Vohrer ( Horst Tappert ) 
Der Mann mit dem Glasauge (1969) r. Alfred Vohrer ( Horst Tappert ) 
Das Gesicht im Dunkeln (1969) r. Riccardo Freda ( Gunther Stoll )
Der Teufel kam aus Akasava (1971) r. Jesus Franco ( Fred Williams ) v koprodukcii so Španielskom
Die Tote aus der Themse (1971) r. Harald Philipp ( Hansjorg Felmy ) 
Solange: Terror v dievčenskej škole, Das Geheimnis der grünen Stecknadel (1972) r. Massimo Dallamono ( Joachim Fuchsberger ) koprodukcia s Talianskom
Das Rätsel des silbernen Halbmonds (1972) r. Umberto Lenzi ( Pier Paolo Capponi ) koprodukcia s Talianskom

  Viac-menej všetky filmy mali osvedčenú schému aké sú typické pre kriminálne príbehy, ale od ostatných sa odlišovali tajomnou atmosférou, mysterióznosťou a mnohokrát sa prikľáňali k horrorovému žánru. Aj prostredie bolo atraktívne, lebo nás sprevádzali po zahmlených londýnskych uličkách, kanáloch, skladoch, starých zámkoch, parkoch… V krajine zrodu sa veľkej popularite teší “Der Hexer”. V 70-tych rokoch sa v našej distribúcii objavili filmy “Tajomný mních”, “Zelený lukostrelec”, “Mŕtve oči Londýna”, “Hostinec na Temži” a “Indická šatka” a všetky zaznamenali veľký úspech. Predpokladám, že je len otázkou času, kedy sa aj u nás na DVD či BLU-RAY nosičoch objavia ostatné filmy. 
