
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Vrátny
                                        &gt;
                U nás na Slovensku
                     
                 Na trase Hamuliakovo-Čilistov-Šamorín-Kvetoslavov 

        
            
                                    18.4.2010
            o
            0:19
                        (upravené
                2.5.2011
                o
                5:48)
                        |
            Karma článku:
                6.25
            |
            Prečítané 
            1707-krát
                    
         
     
         
             

                 
                    V sobotu ráno v rádiu hovorili čosi o možnosti plavby z Hamuliakova cez gabčíkovskú priehradu do Čuňova ku galérii Danubiana a tak som sadol na bicykel, aby som si to všetko overil. Počasie bolo krásne, vietor dul, ale nie tak veľmi, aby vadil a výlet vydaril. Aj keď som prevoz cez priehradu nenašiel. Možno som dosť nehľadal.
                 

                                                                          Obec Hamuliakovo stojí za návštevu kvôli svojmu románskemu kostolu sv. Kríža z 13. stor. Je tehlový, čo vraj vyplývalo z neprístupnosti iných materiálov, hlavne stavebného kameňa. Vo vnútri sú mnohostoročné nástenné maľby, ktoré som pri dnešnej návšteve nevidel, lebo kostol bol zavretý, ale z predchádzajúcich výletov si ich pamätám. Sú tam určite. V dedine sú aj nejaké kúrie a sochy, kto má hlbší záujem, informácie sú na internete, tu ich radšej vynechám.          Jazda na priehradnej hrádzi, či už na bicykli alebo kolieskových korčuliach, je zážitkom sám o sebe a množstvo ľudí sa v ňom vyžíva. Mne bolo ľúto hlavne dreva, ktoré sa povaľovalo na brehoch priehrady. Možno pochádza až z Čierneho lesa v Nemecku, odkiaľ si k nám našlo cestu po Dunaji, je teda vzácne, pritom nevyužité a slúži ledaže na dotvorenie krajiny. „Keby sa nejakým zázrakom popílilo a prenieslo do mojej záhrady. To by som mal kurivo zabezpečené na ... ani neskúšam hádať dokedy,“ tak som si nejako povzdychol a spomenul na hubárske výlety z detských liet, keď som si podobne predstavoval, ako mi všetky huby lesa naskáču do košíka. Zázrak sa neudial, žiadne drevo od priehrady som po návrate domov v záhrade nenašiel.         Čilistov má 1343 odkazov na internete. To nie je málo. Pre (cyklo)turistu sú vzácne jeho zákutia a riečna loď s možnosťou občerstvenia.                              Nedá sa nič robiť, som zaťažený na historické pamiatky a tak som sa poponáhľal do Šamorína. Samotný názov mesta má zaujímavú históriu. Vraj v dobách,  keď sme ešte mali latinu ako úradný jazyk, sa mesto volalo Sancta Maria, z čoho vraj všelijakými prešmyčkami vznikol dnešný názov Šamorín. Z budov spomeniem stavbu najvzácnejšiu, reformovaný kostol, ktorý tiež patrí do skupiny románskych kostolov Podunajska, len tento má odkryté rozsiahle nástenné maľby a tie z kostola urobili skutočný skvost. Fotky neprikladám, lebo dnes som sa dovnútra nedostal. Choďte a presvedčite sa, či preháňam, keď hovorím o skvoste.          Ešte skok do šamorínskeho cintorína, kde majú pomníky ruskí a talianski vojaci z čias prvej svetovej vojny. Vraj pri Šamoríne stál zajatecký tábor a pochovaní vojaci sú hlavne obete chorôb.                                        Na ceste domov som sa zastavil v Kvetoslavove kvôli fotke pomníka padlým z 1. a 2. svetovej vojny. Sú tam lavičky, na ktorých sa veľmi dobre sedí a číta. To asi kvôli tým mohutným dubom, ktoré šíria okolo seba pocit istoty.               To je všetko o mojom sobotňajšom výlete. Bolo pekne, však fotky to azda potvrdzujú. Len informácie, by mali byť obšírnejšie. Vraj všetko je na internete a tak si azda každý nájde to, čo ho zaujíma. Kto však chce zažiť tú správnu atmosféru a nefalšované to, čomu sa hovorí genius loci, musí sa tam vybrať osobne. To na internete nájsť nemožno.       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Prechádzka po Lutherovom meste Wittenberg
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Slnečný babioletný deň v Drážďanoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Nagymaros-Visegrád v neskoré leto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Na tému Ukrajiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Spomienka na Veľkú  vojnu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Vrátny
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Vrátny
            
         
        vratny.blog.sme.sk (rss)
         
                        VIP
                             
     
        V živote som vystriedal viacero bydlísk i povolaní a som teda z každého rožku trošku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    211
                
                
                    Celková karma
                    
                                                5.73
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inde v Európe
                        
                     
                                     
                        
                            Ázia
                        
                     
                                     
                        
                            Nepriateľská osoba
                        
                     
                                     
                        
                            Britské ostrovy
                        
                     
                                     
                        
                            Osudy
                        
                     
                                     
                        
                            U nás na Slovensku
                        
                     
                                     
                        
                            U susedov
                        
                     
                                     
                        
                            USA, Kanada
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na tému Ukrajiny
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Migranti v mojej obývačke
                     
                                                         
                       Sedem dobrých rokov skončilo
                     
                                                         
                       Fragmenty týždňa
                     
                                                         
                       O alkoholikovi, ktorý namaľoval obraz za $ 140 miliónov (a jeho múzeu).
                     
                                                         
                       Slovensko sa opäť zviditelnilo
                     
                                                         
                       Nie, nemusíte jesť špekáčiky zo separátu, vy ich jesť chcete
                     
                                                         
                       Najkrajší primátor najkrajšieho mesta
                     
                                                         
                       Letný pozdrav Sociálnej poisťovni
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




