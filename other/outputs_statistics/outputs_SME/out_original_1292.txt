




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 Vojna v Afganistane 
 
 Vydan? 29. 12. 2004 o 0:00 Autor: PETRA PROCH?ZKOV?, agent?ra Epicentrum pre SME
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (2)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 Vodca le?al na zemi pri bare, mal na sebe tri?ko a tren?rky s n?pisom ADIDAS. Bol m?tvy. "Jednoducho sme ho zastrelili a dodnes nikto nevie, ?ia gu?ka ho zasiahla," spom?na nemenovan? ??astn?k jednej z najz?hadnej??ch a najsl?vnej??ch oper?ci? ?peci?lnej skupiny sovietskej KGB, zn?mej pod n?zvom ALFA. Rovnako ako  ostatn?, ktor? sa 27. decembra 1979 z??astnili na ?toku na prezidentsk? pal?c v K?bule, je na to, ako vtedy splnil bojov? ?lohu, patri?ne hrd?. Inv?ziu do Afganistanu, ktor? sa za?ala presne pred 25 rokmi, hodnotia dnes Rusi rozporuplne. 
 Pokus urobi? z feud?lneho Afganistanu ?al?? komunistick? satelit Sovietsk?ho zv?zu bol vopred  ods?den? na ne?spech, ale k?m na to v Moskve pri?li, uplynulo desa? rokov. Od decembra roku 1979 a? do febru?ra 1989, ke? sovietska arm?da z Afganistanu musela od?s?, zahynulo asi mili?n Afgancov. 
 Pod?a ofici?lnych rusk?ch zdrojov pri?la sovietska arm?da v hor?ch Hinduk??a o 15-tis?c vojakov (neofici?lne ?daje hovoria o 40-tis?coch), 54-tis?c Rusov bolo  zranen?ch. 
 Geopolitick? d?sledky fat?lnej chyby sovietskeho vedenia ?neme dodnes. Vojna porodila neskrotn?ch mud?ahed?nov, krut?ch talibov, pomohla zrodu a rozkvetu hnutia al-K?ida i jeho vodcu Us?ma bin L?dina a t?borom, kde sa cvi?ili profesion?lni teroristi i m?nov?m poliam, na ktor?ch dnes zomieraj? ?udia. Vyu?ili ju narkobar?ni, ktor? sa v povojnovom  chaose c?tia ako ryby vo vode. Vojensk? dobrodru?stvo Sovietov v Afganistane pravdepodobne ur?chlilo aj rozpad Sovietskeho zv?zu. 
 
 
 

 V?etko sa za?alo 27. decembra z vojensk?ho h?adiska ?ialen?m ?tokom na s?dlo afgansk?ho prezidenta Amina. O udalosti bolo nap?san?ch nieko?ko kn?h a stovky ?l?nkov, zost?va ale tajomstvom. Po ?t?tnom prevrate v Afganistane v  apr?li 1979 sa v Kremli za?ali ob?va?, ?e by sa im situ?cia u ju?n?ch susedov mohla vymkn?? z r?k. 
 To?ko pritom do zaostalej krajiny investovali, poslali tam negramotn?ch vojensk?ch poradcov i sovietsk?ch in?inierov, ktor? strkali pastierov do panel?kov a sna?ili sa zo ?ien zhodi? burky zaha?uj?ce ich tv?r i telo. To?ko pe?az? poslali na  v?stavbu ciest, z?vodov a elektr?rn?. Pusti? hornat? krajinu, o ktor? sa v minulosti m?rne ve?akr?t pok??ali Angli?ania, nebolo mo?n?. 
 V apr?li 1979 sa moci v K?bule ujal py?n? Amin. Nepo??val tak, ako by si v Moskve ?elali, paktoval tajne s Ameri?anmi. V Kremli sa ??rili zvesti o americk?ch ?peci?lnych jednotk?ch,  ktor? s? pripraven? obsadi? krajinu. Leonid Bre?nev prepadol panike. O inv?zii ho ?dajne presved?il minister obrany Dmitrij Ustinov a ??f KGB Jurij Andropov. 
 Oper?cia sa pripravovala v ?plnej tajnosti. V noci z 24. na 25. decembra 1979 sa 40. arm?da dala do pohybu a za?ala prekra?ova? hranice. Medzit?m sa v K?bule chystala akcia, ktorej cie?om bolo zbavi? krajinu jej vodcu.  Sovietski poradcovia ani po nieko?koro?nom p?soben? v krajine nepochopili, ?e v Afganistane m? svojho vodcu ka?d? ?dolie. 
 Amin bol len fig?rkou, ktor? ?a?ko ovl?dala ?as? metropoly. Ako fig?rka aj zomrel. V spodnej bielizni a s inf?ziou zavedenou do ?ily. De? pred ?tokom na pal?c sa ho toti? ne?spe?ne pok?sil otr?vi? sovietsky  ?pi?n, ktor? sa votrel medzi Aminove slu?obn?ctvo ako kuch?r. Sovietski lek?ri, ktor? sa starali o zdravie Aminovej rodiny, ale o pl?ne na likvid?ciu nevedeli, a tak prezidenta zachr?nili. Pred?ili mu ?ivot o 24 hod?n. Zabili ich krajania spolu s Aminom. 
 Sovietske ?peci?lne skupiny KGB "Grom" a "Zenit" sa prezliekli do afgansk?ch uniforiem a aby sa odl?ili v boji od  protivn?ka, navliekli si na ruk?vy biele p?sky. Amin pochopil, ?e nejde o povstanie vlastn?ch a? vo chv?li, ke? pod oknami po?ul rusk? nad?vky. 
 Strata nemilovan?ho vodcu obyvate?ov ve?mi nebolela. Niektor? dokonca, ke? do K?bulu vo?li sovietske tanky, m?vali na vojakov. Namiesto Amina dosadila Moskva pokorn?ho Babraka Karmala a vojaci rozd?vali ?u?om  su?ienky. ?udia im ale m?va? ?oskoro prestali. V hor?ch sa formovali oddiely afgansk?ch mud?ahed?nov, v susednom Pakistane vznikala arm?da f?zat?ch mu?ov, podporovan?ch USA, Ir?nom, Saudskou Ar?biou i ??nou. 
 Po ?iestich rokoch bojov opustila pribli?ne polovica afgansk?ho obyvate?stva svoje domovy. A sl?vny poradca Bieleho domu Zbigniew Brzezinski upokojoval  americk? vedenie: "M?me historick? ?ancu dopria? Sovietom ich vlastn? Vietnam." Jeho slov? sa naplnili. 

 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (2)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 21:30  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v utorok st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
KOMENT?R PETRA SCHUTZA  
Len korup?n? ?kand?ly preferencie Smeru nepotopia
 
 Ak chce opoz?cia vo vo?b?ch zvrhn?? Smer, bez roz??renia reperto?ru si neporad?. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 23:50  
Taliban zabil v ?kole v Pakistane 132 det?
 
 Pakistan a? teraz poriadne zatla?il na islamistov. Odplatou je ?tok na deti vojakov. 
 
 
 
 
 




 
 

 
ZAHRANI?IE  
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi
 
 Na demon?tr?cii proti Orb?novej vl?de bolo v utorok u? len nieko?ko tis?c ?ud?. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






