
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Libor Lukáč
                                        &gt;
                Nezaradené
                     
                 Našiel sa vinník zodpovedný za povodne! 

        
            
                                    10.6.2010
            o
            16:35
                        (upravené
                10.6.2010
                o
                16:45)
                        |
            Karma článku:
                2.24
            |
            Prečítané 
            484-krát
                    
         
     
         
             

                 
                       Slovensko, ale aj celú Európu zasiahli povodne! Hovorí sa nie o storočnej, ale o tisícročnej vode. Materiálne škody sú obrovské. Vo svojom zúfalstve, bezmocnosti, hneve a niekedy až agresivite cítia ľudia, postihnutí povodňami, potrebu nájsť nejakého vinníka, na ktorom by ventilovali svoje vnútorné napätie. A tak sa týmto vinníkom stal výbuch sopky na Islande, či globálne otepľovanie, sú ním nedostatočné protipovodňové opatrenia štátu, neschopnosť obecných samospráv a vlády a tak ďalej a tak ďalej.
                 

                   
         V protiklade so všetkými týmito ľuďmi, snažiacimi sa hľadať vinu len a len vo svojom okolí si uveďme slová jedného starého človeka, ktorý pri pohľade na vlastné zatopené pole skonštatoval: Sme zlí a preto nás Boh tresce!          Tieto slová prostého a jednoduchého človeka sú omnoho bližšie k pochopeniu skutočných príčin záplav, ako erudované formulácie meteorológov, vodohospodárov, krízových protipovodňových štábov, primátorov, starostov, a podobne, ktoré vždy hovoria iba dôsledkoch, ale nikdy nie o skutočných príčinách. Tieto slová totiž nehľadajú vinu v niekom, alebo v niečom inom okolo nás tak, ako sú na to ľudia vo všeobecnosti zvyknutí, ale hľadajú ju tam, kde naozaj je. V nás samotných! Áno, jedine my, ľudia, sme vinní za to, čo sa deje! Podobný uzáver však bude s veľkou pravdepodobnosťou niečím, čo človek dneška asi veľmi ťažko pochopí a to z jednoduchého dôvodu – z nedostatku pokory a s ňou spojenej, zdravej sebareflexie.           Určite sa teraz mnohí spýtajú: Ako môžem ja osobne za to, že došlo k záplavám? Odpoveď už raz zaznela: Sme zlí a preto nás Boh tresce! V týchto jednoduchých slovách je naozaj vystihnutá podstata pravdy i keď to bude treba predsa len treba trochu upresniť.           V skutočnosti nás Boh vôbec netresce, pretože na to je príliš vznešený. Tvorcovi univerza stačilo vložiť do fungovania stvorenia jeden jediný, dokonalý Zákon, na základe ktorého sa my sami trestáme, alebo odmeňujeme. Je to Zákon spätného účinku – zákon akcie a reakcie!          Aká je akcia, taká je reakcia!  Čo kto zaseje, to aj zožne! Takto jednoducho, prosto a dokonalo to funguje.          Ak konáme dobro, vráti sa nám dobro a ak konáme zlo, vráti sa nám zlo. V účinkoch jedného a toho istého Zákona je teda zahrnutá všetka odmena i trest, je v ňom zahrnutá veľká a dokonalá Spravodlivosť Božia.           A teraz sa vráťme k povodniam, ktoré sú tiež iba dôsledkom spomínaného Zákona. Z poznania jeho účinkov možno veľmi jednoducho vydedukovať, že ak ľudí postihlo niečo mimoriadne negatívneho, museli tomu oni sami, zavdať príčinu svojou vlastnou negativitou. Čo kto totiž zasej, to aj zožne! Ak ľudia sejú zlo, musia byť zlí, lebo podľa Zákona spätného účinku to jednoducho inak nie je možné!           V čom konkrétne sú však ľudia takí zlí, keď ich trestajú rôzne povodne, zemetrasenia, veterné smršte, či iné katastrofy?           Naozaj iba nedostatok pokory a zdravej sebareflexie nám znemožňuje jasne vnímať, v čom je ľudstvo zlé, ba čo viac, doslova zvrhlé a skazené.           Poďme teda rad za radom a pozrime sa napríklad na internet. Je plný zmyselnosti, zvrhlosti, nízkosti, špiny a duševnej prázdnoty. Pozrime sa na filmy, plné násilia, hrubosti, vulgárností a sexuálnych zvráteností. Pozrime sa na najčítanejšie bulvárne noviny a časopisy, plné ohovárania, vyzývavej telesnosti a prázdnych senzácií. Pozrime sa na ženskú módu, ktorej ideálom je byť sexy a ktorá je vytváraná s úmyslom čo najrafinovanejšieho dráždenia zmyslov. Pozrime sa na už úplne „normálne“ používanie vulgarizmov v bežnej, hovorovej reči, na nevraživosť, ohováranie a neprajnosť, tak veľmi rozšírené medzi ľuďmi. Ďalej na duchovnú prázdnotu a tupú honbu za hmotou, na egoistickú snahu získať pre seba čo najviac i na úkor druhých, na neprekonateľnú nenávisť, ústiacu do terorizmu, na snahu ovládať a zneužívať iné národy a tak ďalej a tak ďalej.           Toto je prostredie, v ktorom žijú ľudia na zemi! V jeho hraniciach sa pohybuje všetko ich myslenie a cítenie! No týmto nečistým myslením a cítením, týmto nízkym a nečistým vnútorným životom väčšiny sú neustále a každodenne produkované obrovské mračná negatívnej mentálnej a citovej energie, ktoré obklopujú našu planétu. Je to doslova oceán neuveriteľnej nízkosti, špiny, malosti a úbohosti. Oceán, do ktorého sa stále, dňom i nocou valia nové negatívne prúdy ľudského cítenia a myslenia! Desivý oceán zla, vytvoreného ľuďmi, ktorého hladina už kulminuje a v spätnom účinku, prostredníctvom rôzneho negatívneho živelného diania zasahuje skazené ľudstvo. Živly zeme, vedené inteligenciu prírody, vrhajú ľuďom naspäť do tváre všetko, nimi samotnými vytvorené zlo! Toto je hlavná príčiny všetkých súčasných i budúcich prírodných katastrof!           Ak im budú chcieť ľudia zabrániť, pomôže iba jediné: Radikálny obrat v spôsobe myslenia a cítenia! Ak totiž chceme, aby sme dobro žali, musíme ho aj siať! Tento Zákon nepustí!           Ako na to? Každý z nás by mal v prvom rade očistiť svoj vlastný, osobný vnútorný život! Mal by vedome dbať na čistotu a ušľachtilosť svojho myslenia a cítenia! Mal by od seba nekompromisne odvrhnúť všetku špinu, nízkosť a úbohosť, ktorá naňho dolieha z filmov, časopisov, internetu, módy, atď.            Honbu len a len za čisto svojim osobným prospechom by mali ľudia nahradiť snahou po pomoci iným – snahou po prospechu všeobecnom a to v duchu zásady: Nikdy nerob iným to, čo nechceš, aby iní robili tebe! Alebo inak: Ako chceš, aby sa iní chovali k tebe, tak sa chovaj aj ty k nim!          Nie je to nič nového! Sú to princípy, ktoré poznáme už tisícročia! Princípy, ktoré sme však nikdy nezrealizovali, ale naopak, vždy sme kráčali a doteraz kráčame proti ním. Práve preto dnes žneme to, čo v prenesenom slova zmysle možno nazvať Božím trestom.           Nie je to však nijaký trest Boží! Je to iba žatva toho, čo sme ako ľudia vždy siali a čo ešte doteraz sejeme! Žatva, ktorej trpké plody budeme požívať dovtedy, kým sa nezmeníme!           A ak pozemské ľudstvo, vo svojej slepote nebude schopné spoznať skutočné príčiny, zodpovedné za všetko, čo ho stretá, príčiny, skrývajúce sa v ňom samotnom, živly prírody budú čoraz viacej stupňovať silu spätných účinkov a to až k neznesiteľnosti, aby nakoniec zmietli zo zemského povrchu všetkých tých, ktorí i napriek mnohým upozorneniam a výstrahám zostali do poslednej chvíle iba nepoučiteľnými škodcami, vytvárajúcimi vo svojom myslení a cítení len a len zlo.           L.L., sympatizant Slovenského občianskeho združenia pre posilňovanie mravov a ľudskosti    http://www.pre-ludskost.sk/                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Televízia – hriech náš každodenný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Iný pohľad na katastrofu v Japonsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            O radosti a bolesti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Zahynieme na devalváciu slova
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Libor Lukáč 
                                        
                                            Katastrofa v Japonsku – hlas Matky prírody!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Libor Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Libor Lukáč
            
         
        liborlukac.blog.sme.sk (rss)
         
                                     
     
        Snažím sa byť človekom s vlastným pohľadom na realitu. Emailový kontakt: libor.lukac@atlas.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    504
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




