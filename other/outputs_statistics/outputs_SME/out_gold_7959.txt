

 
 
 Táto nemecká strana vznikla v roku 1920 a predstavila plán (25.bodov) ktorý mal viesť k veľkým zmenám. 
 Týchto 25 bodov strany aj k veľkej zmene krajinu aj doviedli, no veľa ľudí asi neočakávalo až takúto zmenu. Istotne viete o čom píšem. Pri čítaní tých dvadsiatich bodov som našiel podobnosť s niektorými bodmi našej strany. 
 Tie body, nazývajme ich „sľuby“, niesu zlé, ale v celkovom ponímaní tvoria hrozbu. Na jednej strane pomôžu vzchopiť sa a nebáť sa zasiahnuť v prípade konania zlého z tretej strany, no na strane druhej si to každý môže vysvetliť po svojom a tak rozpútať peklo. Nemám obavy zo strany, ale zo sympatizantov. Z ich komentárov, diskusných fór sa dá vyčítať mnoho a niektoré komentáre sú hrôzostrašné, nenávistné a pod... 
 Je to len názor laika, ktorý sa nevyzná do politiky skoro vôbec a nájsť niečo zlé sa dá na každú stranu. No každý má právo na názor, na slobodu vyjadrovania sa (myslenia), čo je v podstate aj zakotvené v Ústave SR (Druhá hlava, Druhý oddiel čl. 24/1) 

