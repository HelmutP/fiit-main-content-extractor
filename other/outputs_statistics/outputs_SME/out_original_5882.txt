
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kotrčka
                                        &gt;
                OpenSource
                     
                 Len krátko 

        
            
                                    5.5.2010
            o
            9:40
                        (upravené
                5.5.2010
                o
                10:37)
                        |
            Karma článku:
                5.81
            |
            Prečítané 
            394-krát
                    
         
     
         
             

                 
                    O SNS totiž dlho ani nemá zmysel písať. No tentokrát sa ich paradoxne trošku zastanem.
                 

                 Iste ste si všimli novú malú kauzu okolo predvolebného plagátu SNS s textom "Aby sme nekŕmili tých, čo nechcú pracovať.".   Že čo je na ňom zlé? Veď je to pravda, súhlasili by mnohí. No podľa "konkurenčných" politikov je to podnecovanie k raovej neznášanlivosti a "na hranici trestného činu". Len preto, že je na obrázku nad textom zobrazený Cigán/Róm (ako chcete). Ak by tam bol biely, tak to už nie je na tej istej hranici? Neochraňujme týchto ľudí prehnane len preto, že sú menšina (teda ako kde, všakže?).   Ja nemám nič proti nim - poznám dosť Rómov, ktorí sa snažia, pracujú, študujú. No v jednej veci má SNS pravdu. Ak postavíme vedľa seba 100 Rómov a 100 neRómov, kde bude väčšie percento tých, ktorí sa priživujú (a to nemyslím obrazne) na systéme? Stavil by som majetok na to, že väčšia skupina bude medzi Rómami - ba dokonca by som sa nebál povedať, že aj v absolútnych číslach na Slovensku to bude viac aj napriek nepomeru veľkosti oboch skupín.   Podporujme (nediskriminujme) rómsku kultúru, zvyky, atď. Ale nehrajme sa na svätých v prípade, že sa poukáže (aj keď drsnejšie, to uznávam) na pravdivú skutočnosť. A to píšem ako zásadný odporca SNS (najmä vďaka jej predstaviteľom). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Správy STV/RTV:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Červená tabuľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            02 je drahé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Dlhodobý problém 02
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Prečo nie je 02 môj hlavný operátor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kotrčka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kotrčka
            
         
        kotrcka.blog.sme.sk (rss)
         
                                     
     
        25/184/72 - narodený v deň, keď má meniny Adolf, IT, hamradio a cyklofanatik...

 
  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    63
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1383
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            OpenSource
                        
                     
                                     
                        
                            Fotografovanie
                        
                     
                                     
                        
                            Cestovanie a iné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sex na pracovisku
                     
                                                         
                       Má Ficova vláda prepojenie na scientológov?
                     
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Privatizácia Smeru
                     
                                                         
                       Cargo – obrovské dlhy, vysoké platy a pokútne spory
                     
                                                         
                       Na ceste po juhovýchodnom Poľsku
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Počiatkov pochabý nápad: zadržiavanie turistov za 30 EUR
                     
                                                         
                       "Rómsky problém"
                     
                                                         
                       Rómska otázka – aké sú vlastne fakty?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




