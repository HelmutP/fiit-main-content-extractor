
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Koiš
                                        &gt;
                Spoločnosť a politika
                     
                 Pomohli by nám dvojkolové parlamentné voľby? 

        
            
                                    5.4.2010
            o
            15:00
                        |
            Karma článku:
                7.60
            |
            Prečítané 
            1900-krát
                    
         
     
         
             

                 
                    Ak môžu byť dvojkolové voľby prezidenta, prípadne dvojkolové voľby do VÚC, prečo by sme nemohli mať aj dvojkolové voľby do parlamentu, keďže v nich náš život ovplyvňujeme najviac? Pritom práve dvojkolová voľba strán do parlamentu by mohla vyriešiť súčasný problém, keď ľudia sa boja dať hlas menším stranám iba preto, že ich hlas prepadne. Pripájam ďalší zo svojich utopistických nápadov, ktorý síce nemá šancu vstúpiť do platnosti, ale zaujíma ma diskusia k nemu. Čiže ako by som si dvojkolovú voľbu do parlamentu predstavoval?
                 

                 
Ilustracne foto - autor 
   1. kolo - voľte, koho chcete  V prvom kole by mal občan absolútne slobodnú voľbu strany, ktorá ho chce reprezentovať. Ale iba strany - nie poslancov. Z organizačných dôvodov by mal na hlasovacom lístku názvy všetkých kandidujúcich strán, z ktorých by vybral jednu. Kandidátov by nekrúžkoval, tí by ani neboli vyobrazení - ušetrili by sa vďaka tomu tony papiera a hektolitre tlačiarenskej farby :-) Prvé kolo parlamentných volieb by jednoducho bolo selekciou strán, ktoré voliči najviac preferujú. Nič viac, nič menej.  Do druhého kola voľby by následne postúpili strany, ktoré v prvom kole dosiahli aspoň 5% hlasov oprávnených zúčastnených voličov.   2. kolo - rozdelujte mandáty  A v druhom kole by sa začala rozhodujúca fáza súťaže. Druhé kolo by sa odohrávalo už klasickým spôsobom ako dnešné parlamentné voľby - občania by už vyberali zo všetkých postupujúcich strán, a mali by k dispozícii už aj konkrétne kandidátky. Mohli by voliť bez krúžkovania, alebo preferenčne krúžkovať jednotlivcov, tak ako je tomu dnes. Do parlamentu by postúpili všetky strany v druhom kole bez rozdielu, občania by však v tomto kole určili pomer mandátov medzi nimi.  Druhé kolo by teda bolo reálnym súbojom strán a programov, ktoré budú voliča zastupovať. Neboli by v ňom tie malé strany, o ktoré nie je žiadny záujem, a občan by si slobodne mohol vybrať poslancov, ktorí ho budú zastupovať, pričom by už poznal skutočné šance zúčastnených strán a mohol by kalkulovať aj s prípadnými koalíciami. Volebná kampaň v dvojtýždňovej prestávke medzi prvým a druhým kolom by tak mohla byť veľmi zaujímavá. A voľba občana by zároveň bola zodpovednejšia, pretože by mal oveľa viac vstupných informácií k svojej voľbe než má dnes.  Prieskumy sú nepresné, potrebujeme reálne voľby  Dnes sú pre nás orientáciou iba veľmi nepresné prieskumy verejnej mienky. Navyše, čo agentúra, to iný prieskum (viď napríklad priepastný rozdiel v preferenciách SaS a Most-Híd v prieskumoch Medianu a ostatných agentúr). Prvé kolo volieb by nám teda umožnilo odkryť karty, a každý by mohol dať hlas tej strane, ktorú naozaj chce. Odpadol by strach z prepadnutia hlasu, a voliči by sa rozhodovali oveľa slobodnejšie.  A zároveň by existovala šanca na opravu. Ak by volič cítil, že jeho voľba v prvom kole ho neuspokojila, mohol by v druhom kole dať hlas inej strane, a mal by oveľa silnejší pocit, že jeho voľba mala zmysel.    PS: Uvedomujem si, že tento nápad je utópia, a že vôbec nie je dokonalý. Ale zaujímal by ma váš názor, a určité rozvinutie v diskusii. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (61)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Malé desatoro SaS: Koncesionárske poplatky (už) nezrušíme!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Referendum podľa Fica: občianska povinnosť len keď sa mi to hodí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Pán Kmotrík postaví štadión, prispejeme mu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Do tankoch a na živnostníkov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Omeškal som sa so splátkou úveru. O mínus jeden deň (aktualizácia)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Koiš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Koiš
            
         
        kois.blog.sme.sk (rss)
         
                                     
     
        Rád píšem o všetkom, čo ma zaujme. A rád dávam najavo aj pozitívne skúsenosti, ktoré nevnímam ako samozrejmé.


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    91
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Letecká doprava
                        
                     
                                     
                        
                            Cestovanie vlakom
                        
                     
                                     
                        
                            Spoločnosť a politika
                        
                     
                                     
                        
                            Médiá @ komunikácia
                        
                     
                                     
                        
                            Hokej a futbal
                        
                     
                                     
                        
                            Život v Bratislave
                        
                     
                                     
                        
                            Život v Prahe
                        
                     
                                     
                        
                            Ostatné články
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Gabin - La Maison
                                     
                                                                             
                                            Tom Waits - What's he building in there?
                                     
                                                                             
                                            Kings of Convenience - I'd rather dance with you
                                     
                                                                             
                                            One Night Only - Just for tonight
                                     
                                                                             
                                            Counting Crows - A Murder of One
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Ščobák
                                     
                                                                             
                                            Jozef Havrilla
                                     
                                                                             
                                            Jan Potůček
                                     
                                                                             
                                            Sergej Danilov
                                     
                                                                             
                                            Lukáš Polák
                                     
                                                                             
                                            Zdeno Jašek
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Richard Sulík
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Digitálně.tv
                                     
                                                                             
                                            Digizone.cz
                                     
                                                                             
                                            RadioTV.sk
                                     
                                                                             
                                            RadioTV.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chrániť ľudí pred vrahom (a naopak)
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




