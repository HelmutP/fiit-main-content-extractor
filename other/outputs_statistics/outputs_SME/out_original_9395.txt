
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Manžel novinár, manželka poslankyňa – čo s konfliktom záujmov? 

        
            
                                    22.6.2010
            o
            18:53
                        |
            Karma článku:
                12.26
            |
            Prečítané 
            18667-krát
                    
         
     
         
             

                 
                    Tom Nicholson zo SME prestane robiť investigatívu a písať komentáre
                 

                 
Nicholson v komentári z roku 2007 volá po zmene vo fungovaní Pozemkového fondu.tv.sme.sk
   Dva dni pred voľbami premiér Robert Fico namietal objektívnosť predvolebných diskusií politikov v Markíze so Zlaticou Puškárovou, keďže dramaturgičkou relácie bola manželka jedného z lídrov KDH Beáta Lipšicová. Tá mala na starosť komunikáciu s hosťami/stranami ako aj prípravu informačných podkladov pre diskusné témy.   Hoci premiér tú sťažnosť vytiahol spolu s mnohými inými zjavne s cieľom mobilizovať svojich voličov, jeho útok na dôveryhodnosť predvolebnej diskusie nebol bez vecnej podstaty. Lipšicová ako novinárka stojí v konflikte záujmov, keď má za manžela politika (a za chvíľu zrejme ministra). Podľa šéfredaktora spravodajstva Markízy Lukáša Dika sa nakoniec na príprave záverečnej predvolebnej debaty, kde vystupoval aj Fico a Figeľ, po výčitkách premiéra samotná Lipšicová nepodieľala:   Televízia Markíza vyvíjala formát Parlamentné voľby 2010 viac ako pol roka. Beata Lipšicová sa do prípravy diskusií zapojila až v poslednej fáze spoluprácou na príprave podkladov pre moderátorku a komunikáciou s jednotlivými straníckymi centrálami. Ani jedna zo strán počas niekoľkotýždňovej komunikácie až do včerajšieho dňa (štvrtka 10.6.2010) nevzniesla voči nej žiadnu námietku. Aj napriek tomu sa Beata Lipšicová rozhodla nepodieľať sa na včerajšej diskusii, aby obsahom vrcholnej predvolebnej diskusie boli témy, ktoré sú pre občanov dôležité, a nie jej osoba.   Podobný konflikt záujmov riešia aj v SME. Manželka ich investigatívneho reportéra Toma Nicholsona Lucia bola totiž za SaS zvolená do parlamentu. Nicholson väčšinou písal o pochybných kšeftoch vládnej koalície, vrátane pozemkového škandálu. Vedenie SME rieši konflikt prísne. Šéfredaktor SME Matúš Kostolný mi dnes napísal:   S Tomom Nicholsonom sme sa dohodli, že prestáva písať politické spravodajstvo, komentáre aj investigatívne kauzy. Spoločne hľadáme novú pozíciu v rámci redakcie či vydavateľstva, kde by bol Tom pre SME prospešný. Štyri roky, ktoré sa Tom venoval v SME investigatíve boli mimoriadne úspešné. Dôkazom sú početné kauzy, ktoré Tom čitateľom priniesol. Preto som presvedčený, že budeme v spolupráci pokračovať.   "Konfliktná"situácia nastala u moderátora ekonomickej publicistiky v TA3 Roberta Žitňanského, ktorého manželku Janu voľby posunuli do parlamentu za KDH (a v rodine môže mať aj budúcu ministerku Luciu Žitňanskú). Žitňanský mi odpovedal takto:   Potencialnemu konfliktu zaujmov budem predchadzat tak ako doteraz. To znamena, ze do diskusii - tak ako uz niekolko rokov - nebudem pozyvat politikov, ale vyhradne expertov a analytikov. Ak sa niektory z nich rozhodne pre politicku karieru (a napriklad kandiduje za niektoru politicku stranu), nepozyvam ho. Ak by nastala situacia, ze by sa ziadala debata politikov-ekonomov, potom by prebehla v striktne "vyvazenej" podobe - teda dve strany sporu a neutralny moderator.    Robert M.Steele z Poynter Institute, jeden z najcitovanejších odborníkov na novinársku etiku v USA mi napísal, že v takýchto vzťahoch sa stáva otáznou nezávislosť novinára, keďže jednak má byť lojálny k svojmu manželovi či manželke a jednak ku svojim divákom či čitateľom. „Čo ak sa novinár počas jeho „súkromnej“ časti dňa dozvie niečo z pohľadu verejnosti skutočne dôležité? Čo ak si náhodou vypočuje hádku medzi manželkou a iným politikom o veciach, ktoré by mala verejnosť vedieť, ale ktoré politici utajujú?“  Riešenia podľa Steela závisia od okolností. Prvým, ale nie vždy dostatočným krokom je tzv. disclosure, teda uvedenie svojho konfliktu záujmov (ako to urobil Žitňanský v článku pre LN, či Nicholson vo svojej povolebnej relácii). Otázna je forma - uviesť to v každom článku novinára na politickú tému, či len pri zmienke o dotyčnej politickej strane, alebo stačí mať to niekde na webovom profile reportéra? Zvážiť podľa Steela treba aj obmedzenia pre novinárov v témach, ktoré by mohli byť príliš blízke záujmom ich manželov.   Asi najhoršie teda bude tváriť sa, že konflikt neexistuje a nerobiť nič. Vtedy totiž hrozí, že bude médium legitímne spochybňované aj v prípadoch, kde si možno editori dali naozaj veľký pozor na možné nežiadúce osobné vplyvy. Mimochodom,  na podobných konfliktoch záujmov stoja mnohé novinárske články, často napríklad v udeľovaní zákaziek či dotácií. Ak je napr. rodinný vzťah medzi človekom z víťazného tendra či prijímateľa dotácie a koaličnou stranou alebo vysokým úradníkom z ministerstva, ktoré tender či dotačnú schému organizujú, médiá to automaticky berú ako legitímny základ pre podozrenie. Preto by sa teraz aj novinári mali pozerať na vlastné konflikty záujmov rovnako prísnym merítkom.       PS Nicholson bol pred 10 rokmi počas môjho krátkeho pobytu v týždenníku The Slovak Spectator mojim šéfredaktorom.     Titulok dňa: „Lídri sa chcú dohodnúť aj za cenu ústupkov,“ hlásila v sobotu Pravda o rokovaniach budúcich koaličných strán. Čo nové chceli noviny titulkom povedať? Ktorá koalícia na svete sa kedy dohodla bez ústupkov?   Titulok dňa 2: „Najmenej ľudí na cestách zomiera u nás“, znie titulok k správe ČTK na hnonline.sk. Ako ma upozornil čitateľ, článok ale nie je o počte úmrtí, ale o medziročnom poklese 2008-9, kde sme najlepší.   Navyše, už len z pohľadu na tabuľku vidno, že škandinávske krajiny s podobnou populáciou (Dánsko, Nórsko) majú počet obetí nehôd určite nižší ako Slovensko, a teda titulok je určite nepravdivý.  Samotný server ČTK v češtine má titulok vecne správny: „Slovensko snížilo množství obětí na silnicích nejvíc v EU.“  Prírodné pozorovanie dňa: V nedeľných správach Markízy Dominika Lukáčová v reportáži o zaplavenom kostole povedala:   Po záplavách v kostole všetko plávalo. Vode nebol svätý ani oltár.   Žeby bola povodňová voda iného vierovyznania ako ten oltár? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (64)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




