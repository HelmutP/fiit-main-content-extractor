
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Anna Mezovská
                                        &gt;
                Short stories
                     
                 Kletko 

        
            
                                    3.4.2010
            o
            11:37
                        (upravené
                31.5.2010
                o
                21:04)
                        |
            Karma článku:
                1.64
            |
            Prečítané 
            615-krát
                    
         
     
         
             

                 
                    Bol krásny deň. Jeden z tých, keď býva ešte skoro ráno a neskoro večer veľmi chladno, no okolo poludnia sa už dá na slniečku príjemne vyhrievať. Vo vzduchu lietali prvé muchy. Boli po zime také nemotorné, že neustále narážali do predmetov, ktoré im stáli v ceste, a tak omráčené padali na zem. Po chvíľke opäť vzlietli, ale ich let bol ešte katastrofálnejší.
                 

                 
  
   Zo stromov s púčikmi - budúcimi listami, sa ozýval veselý spev vtáčikov. Pridal sa k nim aj zurčiaci potôčik, tečúci priamo cez dedinu. Na okolitých kopcoch sa ešte kde-tu beleli fliačiky posledného snehu, ktorý sa však veľmi rýchlo topil. V záhradkách pred učupenými domčekmi kvitli snežienky a iné drobné kvietky. Aj trávička sa začala pomaly zelenať, pretože prišla jar!   „Vidíš to krásne autíčko?" spýtala som sa dvojročného Timka, ktorý ma pevne držal za ruku. Stáli sme na jednej z dedinských uličiek a pozerali cez plot na akýsi dvor, kde sa povaľovalo veľa hračiek.   „Timko si ho chce požičať!" rázne odvetil.   „No, to asi tak ľahké nebude, lebo si ho nemáme od koho vypýtať, vieš? Nevadí, Timi. Nabudúce, dobre?"   „Dobe, nabudúce." A už sa chcel rozbehnúť preč, keď som ho v poslednej chvíli zastavila.   „Aha, Timi! Pozri aký veľký havo! Asi ide ku nám!" Jemne som pootočila Timkovu hlávku, aby uvidel čuvača, idúceho po ulici.   Veľký biely pes nebol sám. Viedol ho muž, ktorý pred sebou tlačil detský kočík. Popri ňom cupitalo asi tak štvorročné  dievčatko. Pes ťahal pána z jednej strany ulice na druhú. Všade mu niečo voňalo, preto to chcel oňuchať. Muž však strácal trpezlivosť. V jedenej ruke kočík a v druhej pes, akosi to nešlo dohromady. Majiteľ psa zrazu zastal. Strhol deku, ktorá bola prehodená cez kočík a začal surovo biť drobčeka, ktorý v ňom nechcel spinkať. Keď sa vyzúril, dal deku na pôvodné miesto a pokračoval v ceste aj so svojím psom.   Ako tak prechádzal okolo nás, zdvihol zrak a naše pohľady sa na okamih stretli. Neviem, čo vtedy tento muž prežíval, ale z jeho očí bolo cítiť smútok, bolesť, beznádej, neistotu a sklamanie. Nemal veľa rokov. Možno tridsať alebo ešte menej, ale zažil toho už veľa...   Večer sme išli s Timkom pozrieť vláčik na malom dedinskom námestíčku. Timinko dúfal, že ho už niekto opravil, ale nestalo sa tak. Starý banský vláčik, odstavený na kúsku koľajnice, čakal na svojich obdivovateľov tak isto, ako aj pred niekoľkými dňami. A nielen dňami, už mnoho rokov nehybne stál na tom istom mieste ako pozornosť pre turistov. To však Timko ešte nevedel pochopiť, a tak stále tajne dúfal, že ho raz niekto opraví a bude sa dať na ňom previezť.   Keď sme prišli na námestie, privítali nás dve krásne dievčatká, ktoré sa tam hrali. Náš príchod ich veľmi potešil. Veselo okolo nás pobehovali a chceli, aby sme sa k nim pridali.   „Ako sa voláš?" spýtala som sa tej staršej.   „Mimi a toto je moja sestrička Ester," pričom ukázala na drobčeka v žltej bundičke.   „Máte krásne mená. Kto vám ich dal?"   „Maminka," znela odpoveď.   „A kde máte maminku?" vyzvedala som.   „Pracuje v obchode."   „A ocka?"   „Ocko je tamto, ale asi ho nevidíte," odpovedala, zatiaľ čo ukazovala na muža, ktorý pri parku, vzdialenom asi sto metrov, opravoval auto. Vôbec si nás nevšímal, svoju pozornosť úplne venoval starej rachotinke. Pri zadných kolesách auta ležal biely pes. A vtedy mi došlo, bol to ten muž, s ktorým sme sa už raz stretli!   „Miriam, koľko máš rôčkov?" spýtala som sa.   „Štyri a Esterka má dva."   „No, tak to má tak isto ako Timko. Však Timi?" Pozrela som sa na môjho kamoša, ktorý stál vedľa mňa.   „Ja mám dua," zahlásil Timko, pričom hrdo zdvihol rúčku a ukazoval nám koľko je to na prstoch.   „Mimi, chodíš už do škôlky?"   „Nie, nemôžem. Musím sa starať o sestričku, keď je ocko aj s maminkou v robote." Jej odpoveď ma veľmi zabolela.   „Zostávate teda doma samé?!" zdesene som vykríkla.   „Nie, ešte je s nami Alan, náš pes."   Zahľadela som sa na obe dievčatká. Boli sirôtkami aj napriek tomu, že mali rodičov. Ich oblečenie bolo pekné, ale veľmi otrhané. Akoby ich maminke na nich aj záležalo, no na druhej strane, nemohla sa im venovať.   Po chvíli sa Miriam rozbehla ku vláčiku. Timko ju nasledoval. Zostala pri mne len malá Esterka, ktorá dovtedy nepovedala ani slovko.   „To je kletko, však?" zrazu prehovorila. „Klétuško kletko!" Jej slová boli adresované na malý Timkov ruksačik - plyšového krtka, ktorý som držala v rukách.   „Cem si vyskúšať!" zahlásila.   „Pravdaže, len z neho vytiahnem čajík, dobre?" A už som rozopínala čierny krtkov zips. Potom som dala ruksačik Esterke na chrbátik, ako som jej sľúbila.   „Dobý, dobý mi je!" radostne zvolala, pričom tlieskala drobnými ručičkami.   ‚Taká malá vec a taká veľká radosť!‛ tešila som sa s Esterkou. ‚Aspoň iskierka nádeje v jej ešte krátkom živote!‛   Potom sme sa chvíľku spolu hrali. Rýchlo sa však začalo stmievať, preto sme sa museli rozlúčiť. Dievčatká nám smutne mávali na rozlúčku a chceli vedieť, kedy znovu prídeme. Ale pre nás to bol posledný deň, čo sme mohli zostať v tej tichej zabudnutej baníckej dedinke.   Keď sme s Timkom prechádzali okolo parku, ocino Miriam a Ester ešte stále opravoval auto. Jeho veľký biely čuvač Alan sa schúlil do klbka a tvrdo zaspal.       annieMERCY©2010     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Nehodná lásky? (Skutočný príbeh)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            S deťmi na nákupe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Horehronské babky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Anna Mezovská 
                                        
                                            Mačacia kráľovná
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Anna Mezovská
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Anna Mezovská
            
         
        mezovska.blog.sme.sk (rss)
         
                                     
     
        :-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1039
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




