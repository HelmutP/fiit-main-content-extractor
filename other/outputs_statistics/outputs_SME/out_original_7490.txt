
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Nezaradené
                     
                 Vážená pani redaktorka 

        
            
                                    28.5.2010
            o
            16:15
                        (upravené
                28.5.2010
                o
                16:22)
                        |
            Karma článku:
                10.21
            |
            Prečítané 
            1758-krát
                    
         
     
         
             

                 
                    Vašu prácu v Markíze, vážená pani Zlatica Puškárová, si už naozaj dlho vážim, vaša relácia je solídnym príspevkom prezentácie práce jedného rozmeru investigatívneho novinárstva a som si istý, že to nie je iba môj názor. Bol som naozaj rád, keď ste sa vrátili do tejto neľahkej sféry priamych stretov predovšetkým s politikmi, priamo pred televíznymi kamerami a bol som si istý, že aj keď sa hovorí, že dvakrát do tej istej rieky nevkročíš, vám sa to podarí. A spočiatku to bolo naozaj pravdivé očakávanie, vlastne až do chvíle, kým si na stoličku pred vami nezasadol Robert Kaliňák. Vraj vzdelaním právnik, minister vnútra našej republiky, podpredseda strany SMER, človek samoľúby a bez zábran pyšný, ktorého nielenže nezvládate – čím avizujem všetky „poruchy“ vášho reportérskeho zámeru, alebo ak chcete, tak scenára polhodinky Na telo- ale naviac, podliehate „bacilonosičstvu“ jeho nekultúrnosti vo verbálnych kontaktoch
                 

                     Žena vraj môže byť aj škriepna, aspoň to tvrdia staré príručky pravidiel bontónu veľkej dámy, ale tam, kde je vo veci naozaj viac než iba móda, osobný vzhľad či preferencia určitého druhu kozmetiky, tam sa vyžaduje celkom zákonite oveľa viac než troška nekultúry, frivolnosti a neskroteného temperamentu, lebo akurát v osobnom styku s ministrom Kaliňákom je to celkom kontraproduktívne. Naňho jednoducho nemáte,  to už v televízii predviedol spoľahlivo a opakovane.   Vás mi je ľúto ako profesionála a tak ako sa môžete nakaziť vírusom či bacilom akéhokoľvek neduhu, je to už v krátkom čase druhá nákaza šírená Kaliňákom. Je  vašim nešťastím, chýbajú vám ochranné mechanizmy, nie ste voči jeho chovaniu imúnna a po chvíli rozhovoru ste z ničoho nič v škriepke, ste stále viac a viac agresívna, rezolútna, skáčete do reči, najradšej rozprávate vtedy kedy aj on a pre nás, čo sedíme pred televízormi, Na telo stráca čaro rôznosti či hľadania pravdy. Voľakedy ste to zvládali a som si istý, že vlastne ešte aj teraz, nebyť toho Kaliňáka! Skúste si to prebrať v pokoji doma, poraďte sa s majstrami profesie či psychológmi, alebo aj s psychiatrami, lebo občas to vyzerá byť až v takomto rozmere a určite si pomôžete. Keby som vám mal radiť ja, tak mám jeden recept!   V tej chvíli, keď sa Kaliňák nedá  zastaviť, keď na nič neberie ohľad a chce za každú cenu dokončiť svoje nekonečné frázovanie, namiesto skoku do reči využite to starodávne, dostatočne razantné – kuš! Buď už ticho! Kuš! Všimnite si, je to vždy vtedy, keď po dokončení nedokončenej myšlienky použije niektorú zo spojok, najčastejšie je to -a- a môžete si byť istá, že na vašu otázku aj tak neodpovedal a to čo má prísť, to je z celkom  iného súdka. Ten, s kým sa rozpráva, je v zajatí bohapustej demagógie a populizmu, čo je ukryté v slovnej sprche vyhŕknutej s neznalosťou zásad dýchania pri reči a bolo mi ľúto, v tomto poslednom prípade Jána Figeľa, ktorého priestor na reakcie bol nedôstojný. Ba celkom symptomaticky nákaza preskakovala už aj k nemu, začal byť nervózny, nadychoval sa, chcel reagovať a jeho výzor kariérneho diplomata, ktorého nerozhádže vlastne vôbec nič, sa celkom strácal.   Je to logické, lebo len letmá spomienka na počuté je neuveriteľná. Na otázku, ako je to s financovaním SMER-u, prečo je trestné oznámenie anonymné, keď oficiálne poznáme autorstvo, prečo je premiér ticho a prečo sa nevyvodzujú dôsledky prišla odpoveď.   „Aha, tak prečo ste vy neurobili to a to, prečo ste nežiadali vyšetrovanie, prečo ignorujete, že Slovensko už teraz obdivujú za jeho budúce ekonomické úspechy, prečo závidíte...??? Tak!“   Nuž a vy ste pani redaktorka radšej „zbalili krám!“ Pred časovým limitom, avizovaným v programe, celkom iste vnútorne roztrpčená, v citoch zranená, profesionálne zneistená, lebo vydržať polhodinku pri takom agresívnom, bigotne samoľúbom indivíduu, naviac, ak je to minister vo vláde silového rezortu a podpredseda „rodnej strany SMER“, ktorá bez zábran prezentuje svoju moc na tisíc spôsobov, to naozaj nie je pobyt v sanatóriu či v kúpeľoch. Určite ste si spomenuli na úvod vašej show, kde minister vysvetľoval, ako dokonale príslušníci jeho policajného korpusu ochraňovali chudákov neheterosexuálov na ružovom pride a malo vám byť všetko jasné. Za starých čias sa hovorilo, že ak sa nechceš ocikať, tak to nerob proti vetru! Howgh! Je to ako otvorený list, ale naozaj vás mám rád a z ničoho neobviňujem. Život je často aj pes!         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




