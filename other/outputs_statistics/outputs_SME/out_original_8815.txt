
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Inocent Szaniszló
                                        &gt;
                Verejný život
                     
                 Štvorlístok a mediálna etika 

        
            
                                    14.6.2010
            o
            17:13
                        (upravené
                14.6.2010
                o
                21:28)
                        |
            Karma článku:
                7.91
            |
            Prečítané 
            1801-krát
                    
         
     
         
             

                 
                    V podstate už asi bolo povedané všetko, čo sa dalo o našej predčasnej noci zázrakov zo soboty na nedeľu. Napriek tomu, že mám na stole dosť roboty a radšej nechám miesto iným a múdrejším, neodolal som sa pozrieť na našu možnú budúcnosť. Kiežby štvorlístok vydržal- to je jedno prianie a nie je ideologické. Skôr mi ide o kultúru našej spoločnosti. Druhé prianie- kiežby aj média pochopili, že vrámci menšieho zla už nemôžu dopustiť toľko priestoru amatérskej a nezrelej kritike bez rozumného komentára ako pred voľbami v roku 2006. Ktorý štát si dovolí akokoľvek komplikovaných ľudí, ktorí ho konečne dobre spravujú, takto nahradiť niekym, kto denne má toľko mediálneho priestoru až presvedčí ľudí (a aj Cirkev), že sme ohrození všetkým možným a zo všetkých strán?
                 

                 
Volebné výsledkywww.pravda.sk
  Za posledné štyri roky sme sa naučili hodne surrealisticky sa pozerať na verejný slovenský život. Klamstvo ako pracovná metóda, oddeľovanie verejného života od etiky, hrubé osočovanie a nadávanie, hrdinovia ako Jánošík a iní brutálni vládcovia (a označovateľa tvorcu pokoja strednej Európy za šaša na koni). Celý náš život znova zhrubol a zvulgárnel ako som to prežíval v rokoch 1995-1996. Najlepšie to vidíte, keď sa vraciate mesačne zo zahraničia: služby, tváre ľudí dokonca aj správanie sa v cestnej premávke a pod. Takisto žurnalisti a ich stále slabšia kvalifikácia... Čo ma najviac mrzelo v roku 2006 neboli ani tak hádky mocných. Našinec ešte celkom nechápe ako moc ničí a opíja a ako jej držitelia si musia dať pozor, aby nepodľahli jej vábeniu a ako im treba nastaviť zrkadlo (teda ešte neverí, že to môže byť inak: bez okázalého bohatstva- spomínam si na nášho profesora vo Fribourgu, ktorý keď sa stal prezidentom denne chodil vlakom s jedným nenápadným ochrankárom do práca ako predseda vlády do Bernu). Horšie boli ideály, ktoré rôzni aj mediálni intelektuáli vkladali do svojej predstavy o politike a snoch ako tých politikov budú vychovávať! Skúste sa milí novinári pozrieť na kampaň proti tým, čo nám zachránili Európu pred voľbami v roku 2006. Ja viem, aj ja mám veľké problémy vidieť prehrešky mocných, ale skúste si uvedomiť výsledok ku ktorému sme tou stálou účelovou kritikou došli. Kritika je totiž super vec, ale musí byť nad osobou. A osobná nenávisť mnohých novinárov k mocným presiahla vtedy všetky medze. Aj nádej, že my tých nám neprijateľných vyženieme a prídu lepší /skúste si prečítať český týždenník Reflex po voľbách, kde sa analyzuje neschopnosť zabrániť korupcii verejnej sféry v celej talianskej povojnovej histórii aj s následkami- keby sme my takto vedeli analyzovať politiku, tak by sme asi boli v stálej depke/. Hoci... musím sa priznať, že je to dosť normálne mať vyrážky z premiéra, ktorému nerozumiem ani ľudsky. Môj osobný kontakt počas troch mesiacov práce s politikmi ohľadne politickej etiky bol rovnako rozčarujúci. Ak by išlo o osobné sympatie, tak by som mal tiež čo robiť, aby som písal objektívne. Nakoniec to malo len vplyv na moje volebné rozhodnutie. Ale aby som nerozťahoval: Ak táto štvorkoalícia nebude schopná prísnej disciplíne (to znamená, čo sa dohodne sa musí dodržať a spoločne prezentovať bez bočnej protimediálnej kampane- viz. problémy nemeckej vládnej koalície, ktorá nie je zvyknutá na vládnutie a už lezie veľkej časti národa na nervy aj keď vo veľa veciach má pravdu) tak nám vracajúca sa denná kritika malého Muftiho (ako sme ju počúvali v kuse aj 20x denne vo všetkých médiach v roku 2002-2006) znova prinesie surrealizmus v praxi a vieru, že bez hodnôt, ale s revom môžeme spolu existovať vo verejnom priestore. Kiežby štvorlístok tak vedel komunikovať s opozíciou a novinári tak rozumeli politike, aby sme naozaj ukázali, ktorá cesta nás môže vyviesť z (aj duchovnej) biedy. Preto nech radšej mesiac rokujú ako češi a poriadne zazmluvnia čo sa dá a na čom sa dohodnú, nech tvrdo a bez pokušenia moci príjmu za svoje a ukážu pravdu, ktorú nám veľmi ani médiá za posledné obdobie neukázali: ako biedne vyzerajú naše služby, zdravotníctvo, polícia a súdnictvo aby ľudia dokázali nielen utopisticky, ale aj reálne veriť, že sa to všetko dá zmeniť. Nuž a nakoniec moja radosť: že slovenskí občania maďarskej národnosti sú naši maďari (už skoro ako vo Švajčiarsku) a nie lobby nejakých zaslepencov z 19. storočia

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Zbytočná Európska Únia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Základné právo na náboženskú slobodu a nezávislosť od štátu (IV.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Pápež a kondómy?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Inocent Szaniszló
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Inocent Szaniszló
            
         
        szaniszlo.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ, večný hľadač pravdy, dialógu a vzájomnej výmeny ideí. Spolu s mladšími a staršími kolegami a študentmi rozvíjame obzory nášho poznania./
L´enseignant academique, le chercheur eternel de la vérité, du dialogue et du changement des idées. Avec ses collegues et étudiants nous élargisons l´horisont de notre conaisance.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    849
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politická filozofia
                        
                     
                                     
                        
                            Verejný život
                        
                     
                                     
                        
                            Hľadanie koreňov
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Klaus Berger: Die Wahrheit ist Partei
                                     
                                                                             
                                            Notker Wolf-Matthias Drobinski: Regeln zum Leben
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Laura Passini
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Primitívne nedeľné politické debaty na RTVS a Markíze
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




