
 že každý deň 
túžobné myšlienky 
do vankúša vtláčam
so sebou ich vláčim 

Moje telo už dozrelo
ako hrejivé chute vína 
a predsa 
tak málo o mne vieš... 

Nepadla som tvárou
na zlatom zaprášenú zem 
ale oči smerom k nebu 
a bez toho žeby to chceli 
do budúcnosti odleteli 

Milujem ťa 
viem čo je riziko pádu

Čo hovoríš...? 

že v lásku večnú veríš
a predsa neveríš 
že na záverečnú 
vždy stiahnem oponu
a odovzdávam sa 
len nebeskému parketu  

Zakry ma telom  
som posledná pre lásku 
a keď ráno vyjde slnko  
môžeš ma odokryť 

Vtedy uveríš
že tvoja duša a láska
tu nie je zbytočná 
zostala som jej verná 
aj tvojej slze v tvári  
 
Moje oči a bozky 
sa volajú nezábudky 

Milujem ťa
čo je dôležité už vieš

- 
