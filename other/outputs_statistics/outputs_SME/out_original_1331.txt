
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Etela Kováčová
                                        &gt;
                zvieratká
                     
                 Kajmanka dravá 

        
            
                                    9.4.2009
            o
            11:43
                        |
            Karma článku:
                6.35
            |
            Prečítané 
            2441-krát
                    
         
     
         
             

                 
                    Kajmanka dravá robí česť svojmu menu: útočí a chňape po všetkom, čo leži v dosahu jej silných čeľustí, nezastaví sa dokonca ani pred mláďatami aligátora....
                 

                 
  
   Spôsob života: Kajmanky dravé trávia väčšinu času pod vodou, ležiac na dne plytkých sladkovodných jazier, rybníkov alebo pomaly tečúcich riek. Vo vode sa pohybujú pomaly, na zemi sú ťažkopadné a neobrátne. V chladnom podnebí severnej časti územia výskytu prezimujú pod vodou, avšak aj pod zamrznutou hladinou vody zostavajú často značne aktívne. Kajmanky dravé sú na zemi veľmi agresívne, hlavne v stave ohrozenia. V nebezpečí sa prudko vrhajú na útočníka, pričom ich hákovité čeľuste sa hrozivo roztiahnú. Majú veľkú hlavu s ostrými čeľusťami, silné nohy s mohutnými pazúrmi a plávacími blanami medzi prstami.   Rozmnožovanie: Kajmanka dravá sa parí spravidla vo vode, a to tak, že menšie samčeky vyliezajú na samičky. Samička si potom na okraji brehu vyhrabe jamku, do ktorej od začiatku leta nakladie až dvadcaťštyri vajec. Vývin trvá dva až tri mesiace. Z vajec, ktoré boli nakladené na konci leta, sa mláďatá liahnu najčastejšie až budúcu jar za teplého počasia. Sfarbenie mláďat je oveľa svetlejšie ako u dospelých jedincov. Ihneď po vyliahnutí sa mláďatá škriabu do vody, kde sú počas prvého roka života dobre ukryté. Rýchle rastú a ich pancier vo veku jedného roka obyčajne meria pätnásť centimetrov.   Potrava a lov: Kajmanka dravá loví každé zviera, na ktoré si vzhľadom na jeho veľkosť trúfne. Loví ryby, malé korytnačky, žaby, mloky, vodné hady, mláďatá aligátorov a lmáďatá vtákov, tak isto ako cicavce žijúce vo vode. Ak Kajmanka zbadá vo vode živú rybu, zostane nehybná, ale pozorne sleduje každý pohyb koristi. Väčšie zvieratá zachytáva svojími čeľusťami a prednými pazúrmi ich trhá na kúsky. Menšiu korisť prehĺta celú. Kajmanky dravé sú aj zdochlinožravce, zožerú zdochlinu akejkoľvek veľkosti, ktorú stiahnu pod vodu. Mláďatá Kajmanky dravej sa živia mladými rybami, žubrienkami, kôrovcami a vodným hmyzom.   - Kedysi boli Kajmanky používané na vyhľadávanie tiel obetí nehôd, vrážd alebo samovrážd v jazerách. Do vody ich púšťali priviazané na vlákno a keď Kajmanka zatiahla, bol to signál, že práve našla hľadané telo.   - Kajmanka supia má malý červovitý výrastok na jazyku, ktorým pravidelne pohybuje. Sedí s otvorenou papuľou na dne jazera a striehne na malé rybky, ktoré na tento červovitý výrastok naláka. Sotva sú dostatočne blízko, korytnačka zaklapne papuľu a prehĺtne ich.   Tento druh korytnačky je pre odchov a akvateráriu zaujímavý aj svojím spôsobom života. Korytnačky chované v akvateráriu s hĺbkou vody 30 centimetrov, v lete ich môžeme umiestniť v jazierku s hĺbkou približne 40 centimetrov. Jednotlivé korytnačky chováme oddelene, len v čase párenia umiestníme pár spoločne.   Výskyt- v plytkých sladkovodných jazerách, rybníkoch a riekach na východnej strane Severnej Ameriky, od južnej Kanady až po Strednú Ameriku a severozápad Južnej Ameriky.   Ochrana druhu- Hlavným nepriateľom Kajmanky dravej je človek: Zabíja ju, aby si mohol pripraviť korytnačiu polievku, alebo ju loví len tak zo športu. Napriek tomu sú stavy početnosti konštantné.   Telesné rozmery - pancier až 40 cm, celková dĺžka až 80 cm, samce sú o niečo menšie.   Rozmnožovanie a pohlavná dospelosť- samce v 3 - 5 rokoch, samice v 4 - 6 rokoch.   Obdobie kladenia vajec - koniec leta.   Počet vajec - asi 24.   Inkubačná doba - 2 - 3 mesiace.   Spôsob života - samotár.   Potrava - ryby, mláďatá vtákov a cicavcov, žaby, mloky, vodné hady, mladé korytnačky.   Dĺžka života - v zajatí dokonca až 60 rokov.   Príbuzné druhy - v Severnej Amerike žije Kajmanka supia Macroclemys temminckii.   Trieda - Plazy   Rad- Korytnačky   Čeľaď - Kajmankovité   Rod a Druh - Chelydra serpentina 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Etela Kováčová 
                                        
                                            Veľryba čierna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Etela Kováčová 
                                        
                                            Kapor obycajný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Etela Kováčová 
                                        
                                            Včela medonosná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Etela Kováčová 
                                        
                                            Modlivky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Etela Kováčová 
                                        
                                            Miesto odpočinku "môj domov"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Etela Kováčová
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Etela Kováčová
            
         
        etelakovacova.blog.sme.sk (rss)
         
                                     
     
        Už v detstve som rada čítavala knihy, čo ma inšpirovalo a sama som si začala tvoriť vlastnú tvorbu.Som človek, ktorý miluje hudbu kdekoľvek a kedykoľvek,pretože vystihuje náladu v dannej chvíli.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    230
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1063
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            basne
                        
                     
                                     
                        
                            pribehy
                        
                     
                                     
                        
                            Recepty
                        
                     
                                     
                        
                            zvieratká
                        
                     
                                     
                        
                            myšlienky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Veľryba čierna
                     
                                                         
                       Kapor obycajný
                     
                                                         
                       Včela medonosná
                     
                                                         
                       Modlivky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




