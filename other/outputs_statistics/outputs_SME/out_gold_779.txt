

 
Večer preletím strohé agentúrne správy:
 
 
 
 
 
 
 
"Z dôvodu strhnutého trolejového vedenia došlo včera k značnému omeškaniu vlakových spojení na úseku medzi Žiarom nad Hronom a Novou Baňou. Cestujúci zostali 
uväznení na trati bez vody, jedla a možnosti náhradnej dopravy po dobu viac ako 6 hodín."
 
 
 
 
 
 
 
A ako to vyzeralo naživo? Čítajme ďalej.  
 
 
Najskôr fakty: 
 
 
 
 
	 
	17:44, Nová Baňa, Voznica - R805 Sitno - rozpumpovaný rušeň rýchlika na trati BA-BB strháva trakčné vedenie na jednokoľajovom úseku 3 x 50 metrov; 
	 
	18:10, Brezno - R810 Horehronec - s nepatrným meškaním prichádza vlak a ja naskakujem do posledného vozňa - dvojka, hranaté čalúnené sedačky a zásuvky bez šťavy; 
	 
	18:25, Levice - Ex531 Hron - do expresu nastupujú pasažieri, smerujúci do stanice Banská Bystrica; 
	 
	18:32, vlaky.net  - na internetovom fóre sa prvýkrát objavuje zmienka o strhnutej troleji; 
	 
	18:55, Banská Bystrica - R810 Horehronec - sme tu načas, ľudia vystupujú a najmä nastupujú; 
	 
	18:55, Žiar nad Hronom - R1530 Žitava - do stanice prichádza nedeľný rýchlik zo Zvolena a berie cestujúcich na Bratislavu; 
	 
	19:30, Zvolen - R810 Horehronec - stále bez meškania, pristupujú ďalší a ďalší pasažieri 
	 
	 
	19:55, Žiar nad Hronom - zadriemem na okne, pohyb na peróne si už nevšímam.  
 
 
 
 
Všimli ste si? Napriek evidentne rozsiahlej nehode sa z pohľadu cestujúcich celé hodiny nič nedeje, vlaky fičia ďalej z oboch smerov, v ústrety beznádejnej výluke. Žiadne pardon, odstavenie mašín a pristavenie náhradnej dopravy z Bystrice, Zvolena, či Žiaru. Žiadne upozornenie cestujúcich s nástupom v staniciach v čase, keď muselo byť jasné, že sa povezú len o pár kilometrov ďalej. Opravárenské čaty, výpravcovia, dispečeri a vlakové čaty v dobe mobilov a internetu spolu nekomunikujú. Alebo jednoducho (a krátkozrako) - biznis je biznis a rukojemníci to strpia? 
 
Poďme sa pozrieť do vlaku. Za oknom tabuľa Hliník nad Hronom a stojíme. Tí bdelší spozornejú hneď, ostatní po pár minútach, keď si uvedomia, že sa vlak nehýbe. Po 20 minútach sa začnú trúsiť sprievodkyne: "fajčiarska prestávka, môžte sa prejsť vonká". Na otázky, čo sa stalo a koľko to bude trvať krčia plecami - klasika. Po hodine je už vonku na perónoch celý vlak, vrátane čaty. Tvoria sa skupinky, kolujú dohady, nič nie je isté. Kým sa pre pár šťastivcov otáčajú autá, vbehnem do chodbičky pre foťák. Ledva ho vyberiem, už ma do vedľajšieho vagóna vtiahne dunivý prerývaný rev. Neverím vlastným očiam a ušiam. Na vystrašeného mladíka vyslovene ZIAPE postarši sprievodca (či vlakvedúci?) čosi v zmysle, že on nič nemusí, s meškaním treba rátať a že prečo si to všetko odse*e práve on. Napokon s 
odseknutím, že práve večeria plesne dverami na svojej "kajute" a my dvaja sa len zarazene pozrieme na seba. Chlapec chcel vedieť, čo s lístkom na nadväzujúcu Pannoniu do Prahy. 
 
 
Ako obarený sa vymotám na vzduch a sedím na múriku ako ostatní. Apatia, nervozita, naliehanie, sarkazmus - emócie sa striedajú podľa toho, či sa "len" nevyspíte pred robotou, alebo zmeškáte nadväzujúci spoj za prácou, či odlet chartra na dlho odriekanú dovolenku.  Po 127 minútach sa ozve hvizd a hurá, konečne sa hýbeme. 
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
Áno, tušíme správne - potiahli nás akurát do ďalšej stanice. V Žarnovici už stojí poloopustená Žitava do Levíc s rovnakými zúfalcami, ako sme my. Akákoľvek šanca na alternatívnu dopravu ďalej už pominula. Poflakujeme sa medzi vlakmi, poniektorí si  ustlali v zavretých kupéčkach, kde-tu sa niekto vykradne do lesa na toaletu. Z kohútika pred budovou stanice sa ovlažujem vodou - pivo po 60 korún, čo nejaký podnikavec dotiahol v debničke považujem za provokáciu. 
 
 
Po troch hodinách meškania začne sprievodkyňa a vyššie spomínaný hulvát vypisovať návratky cestovného. Po pár minútach zahlási stop, nemáme dosť tlačív, mám byť rád, že som bol medzi prvými. Vypisujú sa aj potvrdenky o tom, že v rýchliku nie je radený vozeň prvej triedy. To pre cestujúcich, ktorý si do prvej kúpili lístok a vlak prišiel čisto druhotriedny. Áno, aj toto je možné! Ako ináč - treba si vyrobiť problém. Sprievodcovia sa menili v Hronskej Dúbrave a tak spisujú nárok na vrátenie cenového rozdielu medzi prvou a druhou triedou len od Dúbravy. "Ja nemám skade vedieť, že prvá trieda nešla už z Košíc. To ste si mali nárokovať u predošlého sprievodcu!" Pravda, pravdúca - čo ak sme tú jednotku len stratili niekde cestou?!?  
 
 
Za celú tú dlhú noc nepočuť zo strany posádky jediné ospravedlnenie, prepáčte, mrzí nás to, návrh riešenia vzniknutých nepríjemností, nič. Len prízemné ponosy typu "končím.. za taký plat toto nemienim.. nech si to príde Čikovský vyskúšať sám.. pani, ja som tiež v robote.. skúste si to sama skúsiť na takej Poľane, čo to je..". Znenazdajky, tesne pred jednou nás naženie do vlaku hvizd a po pár sekundách už rachotíme preč. Sme všetci? Snáď. Prázdnu Žitavu nechávame v Žarnovici. Zmorený sa poskladám do sedačky a zvyšok už poznáte. 
 
 
 
 
 
Suma sumárum: 
 
 
 
	 Výluka si vyžiadala meškanie siedmich a odrieknutie piatich vlakových súprav; 
	 tri hodiny zmeškaný R805 Sitno ešte pred príchodom do BB stihol zraziť motocyklistu a nabrať ďalšie minúty; 
	 Ex531 Hron dorazil do Zvolena po druhej hodine v noci s omeškaním viac ako 6 a pol hodiny a do cieľovej stanice už nepokračoval; 
	 
	R810 Horehronec ukončil svoju nočnú odyseu v Bratislave s meškaním 4:40; 
	 
	R1530 Žitava zostal v Žarnovici, keď jeho cestujúcich zobral Horehronec; 
	 
	Os6115 s pravidelným odchodom 22:05 do Banskej Štiavnice absolútne zbytočne čakal na svoje prípoje do polnoci. Po 110 minútach údajne vyrazil prázdny - všetci cestujúci si už sami zohnali inú dopravu; 
	 
	výluka ovplyvnila grafikon mnohých ďalších prípojov, o čom neboli cestujúci včas a dostatočne informovaní. 
 
 
 
 
Zhrnutie: 
 
 
 
	 To, že na trati sem-tam dojde k nehode je štatisticky bežné a pochopiteľné; 
	 nepochopiteľná, až šokujúca je ignorancia dôsledkov nehody pracovníkmi ŽSSK a absencia manažmentu mimoriadnej situácie; 
	 
	nepochopiteľné sú nulové opatrenia v ústrety cestujúcim, ba až ich zámerné zavádzanie; 
	 
	nepochopiteľný je aj vrcholne neprofesionálny, alibistický, miestami hrubý, inde zmätočný prístup vlakovej čaty. 
 
 
 
 
Výsledok? Opäť raz potvrdená nespoľahlivosť železničnej prepravy a klesajúci počet cestujúcich aj napriek komparatívnej výhode čoraz drahšej nafty.  
 
Mimochodom, čo myslíte, dostal som cestovné späť? Ale kdeže, spoza okienka som sa dočkal len sústrastných úsmevov. A poučenia, že "Návratok cestovného" (ani spolu s necvaknutým lístkom!) nie je  doklad, potrebný na vrátenie cestovného. Tým má byť vraj záhadné potvrdenie o "Vzdaní sa prepravy" (ktoré na mieste nikto nevydával) a malo byť uplatnené priamo v polnočnej Žarnovici. Načo je potom Návratok? Nevie nik. Len placebo na upokojenie rukojemníkov? Pravda je, že prepravy som sa nevzdal - nedostal som šancu. Podobne ako stovky spolucestujúcich bez informácií, aké komplikácie ich čakajú.
 
 
 
 
 
 
 
Milý monopolný moloch ŽSSK, my si to vynahradíme. Nabudúce sa radi vašej prepravy vzdáme už v zárodku, najmä keď bude treba stihnúť niečo dôležité. Ekologické cítenie bokom, spoľahnúť sa na Vás je iracionálne. Čakáme na sľubované opatrenia. 
 
 
 
 
 
 
 
 
 
 
 
Pozn: Pokiaľ sa mi miestami podarilo načrieť do nesprávnych zdrojov a načerpať mylné informácie, opravte ma, prosím v diskusií.   
 
 
 
 

