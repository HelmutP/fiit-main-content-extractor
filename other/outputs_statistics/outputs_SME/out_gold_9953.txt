

 A tá pochádza ešte z čias, keď som sa prisťahoval do medvedej rodinky, čo je už pekný pár rokov... 
 V tých dobách prvej polovice 90.rokov fičala medvedia rodina na sójových plátkoch takpovediac programovo. Bolo to zdravšie, ale aj oveľa lacnejšie ako mäso. A Medveďova mama zvykla sójové plátky obaľovať v bežnom trojobale (múka, vajíčko, prézle..) a vysmážať viac menej klasické rezne len zo sóje. 
 S nápadom urobiť čiernohorské rezne som prišiel ja, keďže práve 'čiernohory' boli odjakživa moje obľúbené. A odvtedy, ich robievam vždy s veľkým nadšením. Za tie roky som si technológiu ich výroby zdokonalil a cesto na ne je skvele aplikovateľné ako na sóju, tak aj na mäso... 
 Potrebujeme: 
 Na rezne: 1 balík sójových plátkov 90g (10 plátkov na dve dospelácke a dve detské porcie 0,60 EUR), 3 zemiaky (0,20 EUR, 4 ks na obrázku bolo priveľa), 2 strúčiky cesnaku (0,10 eur), 1 - 2 vajíčka (0,25 EUR), 3 lyžice múky, trochu sójovej omáčky, 1 kocku zeleninového bujónu (0,18 EUR), pol kávovej lyžičky sódy bikarbóny, trochu mletého čierneho korenia a mletej rasce, olej na vysmážanie a soľ. 
 Na prílohu: 1 kg nových zemiakov (0,80 EUR), mletá rasca, soľ a mleté čierne korenie. 
 Na šalát: 1 veľkú šalátovú uhorku (0,30 EUR), 1 kyslú smotanu (0,60 EUR), 1 strúčik cesnaku, prípadne trochu kôpru a štipku soli. 
  
 Na niektoré drobné ingrediencie nemôžeme urobiť cenu, ale do 3,5 - 4 EUR sme sa zmestili na 100%.:) 
 A ideme variť: 
 Sójové plátky nasypeme do hrnca s vodou, pridáme kocku zeleninového bujónu, necháme zovrieť a chvíľu povaríme. Plátky následne precedíme cez sitko a necháme vychladnúť. 
  
 Kým mám sójové plátky chladnú, pripravíme si zemiakové cestíčko. Zemiaky ošúpeme a jemne nastrúhame, pridáme 2, ale skutočne stačí aj 1 vajíčko, 3 kopcovité lyžice hladkej múky, trochu soli, čierneho korenia a rasce. Nezabudneme na pol lyžičky sódy bikarbonátu, cestíčko bude po vysmažení oveľa nadýchanejšie. 
  
 Zmes dobre vymiešame a necháme postáť. 
  
 Vychladnuté mäkké plátky medzi prstami jemne postláčame, čím necháme nadbytočnú tekutnu odkvapkať.  Plátky pokvapkáme sójovou omáčkou. 
  
 Tak a sme pripravení na vysmážanie. 
  
 Sójové plátky smažíme na horúcom oleji (pozor, čím studenší olej, tým mastnejší rezeň)... 
  
 ...dobre z každej strany namočené v cestíčku asi 3 minúty na každej strane,  či dohneda. 
  
 Sójové 'čiernohory' pekne voňajú, cestíčko drží perfektne. 
  
 K rezňom sme podávali nové zemiaky len prekrojene na polovice bez šúpania, posolené, posypané čiernym korením a rascou, upečené v olejom vymastenom pekači v rúre zohriatej na 170 stupňov. 
 Skvelý doplnok bol aj uhorkový šalát so smotanou s pretlačeným strúčikom cesnaku, štipkou kôpru a soli. 
  
 Dobrú chuť. Po splave Hrona sa vám ozveme... 
  
   
 tenjeho 

