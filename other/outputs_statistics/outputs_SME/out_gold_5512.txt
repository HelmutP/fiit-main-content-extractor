

 Nezahodila som moje obľúbené botasky, triko, rifle ani mikinu, nezmenila som svoje oblečenie ani svoje správanie, ani svoje pripravovanie sa na vyučovanie. 
 Zmenili sa mi priority, životné ciele, organizácia práce a voľného času, spoznala som za tie 2 roky nových ľudí, preskákala som si nové pracovné pozície. Začala som myslieť na to, čo chcem v živote dosiahnuť, akej profesii sa chcem venovať. 
 Vyskúšala som si brigádnicky predavačku kvetov na trhu i v obchode, poštárku pracujúcu na nočné zmeny, geodetku v kancelárii i v teréne, pomocnú silu v pekárni, prácu v upratovacom servise, aj roznos letákov na ,,nezaplatenie." Byť študentkou je v dnešnej dobe výhodné, usilovný človiečik si čo-to zarobí. Ale študentský život nie je lacný - škola, cestovanie, ponocovanie, diskotékovanie. 
 Čas strávený s pohodovými ľuďmi sa oceniť nedá. Je to najlepší pocit, ak Vám niekto povie: ,,Urobil si to dobre, je to správne, mám Ťa rád, bez Teba by sme to tu nezvládali..." Ale chvíle sú aj oveľa ťažšie každodenné stesy na nás číhajú za každým rohom:-) 
 Mala som len pocit dnes vysloviť jedno prosté ĎAKUJEM ľuďom, ktorí ma vychovali, pretože dnes, ako 20 - ročná si uvedomujem, že najdôležitejšia vec je robiť čo ma baví, pracovať s radosťou vo dne i v noci, byť s ľuďmi, ktorí sú spoľahliví a pravdovravní... 

