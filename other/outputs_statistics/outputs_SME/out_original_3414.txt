
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Iveta Rajtáková
                                        &gt;
                ja tomu nerozumiem
                     
                 Kto nevie konzervovať, nech sa učí gramatiku 

        
            
                                    27.3.2010
            o
            7:59
                        (upravené
                27.3.2010
                o
                13:38)
                        |
            Karma článku:
                10.24
            |
            Prečítané 
            1390-krát
                    
         
     
         
             

                 
                                                       Toto ma zasa presvedčilo, že pozerať správy v telke sa oplatí. Veď nebyť toho, nevidela by som Jureňu  búchajúceho v kravíne päsťou do stola, a prišla by som o ten úžasný zážitok, keď predseda vlády odmietal čosi, čo mu nikto neponúkal a minister obrany to potom ťarbavo vysvetľoval tým, že on predsa ešte včera nemohol vedieť, že mu dnes nikto nič neponúkne. (Prečo to pán predseda vlády robí? Blog - Iveta Rajtáková (blog.sme.sk)) Asi je to trochu zvrátené, keď vážna spravodajská relácia konkuruje zábavným, ale taká je už doba. Naposledy predvčerom. Správy STV a v hlavnej úlohe minister školstva.                                               
                 

                                               Spravodajský príspevok o slovenčine na školách pre menšiny.                                  Viac gramatiky, či viac konverzácie ?                                  Názory sa rôznia, podľa kľúča  - pozri, kto to hovorí. Dvaja žiaci, jedna učiteľka a László Szigeti unisono tvrdia -  gramatiky menej, chceme viac konverzovať. Hovoria po slovensky a až na jednu chybičku ( niet sa čo čudovať, mladá slečna v telke ešte zrejme nebola a tréma vie urobiť aj horšie veci ako pomýliť jednotné a množné číslo) aj GRAMATICKY správne.                                   No a kto je na druhej strane ? Minister školstva. Protivník najťažšieho kalibru. Teraz zaujme ministerský, odborný a pretože sa zrejme neučil slovenčinu v škole pre národnostné menšiny, aj brilantnou slovenčinou vyjadrený názor. Všetci pochopia, ako sa dá svedomitým štúdiom slovenskej gramatiky dopracovať ku kultivovanému a bezchybnému jazykovému prejavu. Určite to deti zo škôl pre národnostné menšiny zaujme, a ak sa doteraz učili gramatiku s odporom, všetko sa zmení, pretože budú chcieť rozprávať po slovensky tak dokonale, ako pán minister ŠKOLSTVA.                                   Minister školstva má na celú vec opačný názor. Všetkým to jasne a zrozumiteľne vysvetlí:                                                      „ Nemôžte KONZERVOVAŤ,  pokiaľ nepoznáte gramatiku.                                                      Šok. Všetci čakali, že to bude o slovenčine, a on hovorí o zaváraní.                                Keď deti zo škôl pre národnostné menšiny pochopia , že slovo  „môžte" je podivným patvarom slovesa „ môžete ", budú sa musieť vysporiadať so zásadnou otázkou:                                                               Ako môže dobrá znalosť slovenskej gramatiky ovplyvniť zaváranie?                                                                 Keďže školy pre národnostné menšiny a osobitne tie, pre tú maďarskú sa nachádzajú predovšetkým na juhu Slovenska, známeho svojou hojnou produkciou zeleniny a ovocia, pre mnohé z nich je to perspektívne otázka existenčná.                                 Veď uznajte, ako sa môže Maďar z juhu Slovenska zmieriť s osudom, že si tie uhorky, čo vypestuje nebude môcť ani len naložiť, ak nebude vedieť správne použiť prechodník slovesa zavárať a príčastie minulé trpné slova nakladať ?                                  ( Je síce pravdou, že moja mama, ktorá v gramatike tiež nevyniká, robí nakladané uhorky, ktoré ju preslávili široko-ďaleko, ale ona chodila do slovenskej školy a tam gramatika pre zaváranie až taká potrebná nie je. Vo všeobecnosti my, štátotvorný národ, tú gramatiku až tak vážne neberieme, čoho dôkazom je práve minister školstva, ale reč bola o menšinách , nie ? )                                 Ak by som bola školáčkou v škole pre národnostné menšiny, lúskala by som tú gramatiku dňom i nocou. A keď by som ju dolúskala, konzervovala by som a konzervovala a.....................                     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ženy nevedia čo chcú
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            V Slovenskej sporiteľni alebo v Kocúrkove?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Ad: Rodič a rodič alebo ukážka hrubozrnnej demagógie par excellence
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Súmrak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Iveta Rajtáková 
                                        
                                            Buďte ako deti. Apdejt
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Iveta Rajtáková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Iveta Rajtáková
            
         
        rajtakova.blog.sme.sk (rss)
         
                                     
     
        Som Iveta Rajtáková.Asi to podstatné, čo sa dá o mne povedať.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    132
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2336
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Služby
                        
                     
                                     
                        
                            Poézia alebo pocta Sufenovi
                        
                     
                                     
                        
                            Svet nie je children friendly
                        
                     
                                     
                        
                            ja tomu nerozumiem
                        
                     
                                     
                        
                            Videli sme
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                                     
                        
                            Boli sme v...
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Z iného sveta
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog Iveta Rajtáková
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




