




 
	 
	 
 SME.sk 
 Z?avy 
 Zozn?menie 
 Reality 
 Dovolenky 
 Pizza 
 Re?taur?cie 
 Recenzie 
 Inzer?ty 
 Nani?mama 	 
N?kupy 
		 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 Poistenie 
 SME knihy a DVD 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 Predplatn? SME 		 
	 
	 
 
	 
 
 
 
  
 
 
 
 

 
	 
		
	 
	 
  	 
 







 
 
   
 

 
 
  SME.sk 
  DOMOV 
  
REGI?NY
 	
		Vybra? svoj regi?n

Tip: Spr?vy z v?ho regi?nu si teraz m??ete nastavi? priamo na titulke 
Chcem vysk??a?

 	Z?padBratislavaLeviceNov? Z?mkyNitraPezinokSenecTopo??anyTren??nTrnavaZ?horie
		V?chodKorz?rHumenn?Ko?iceMichalovcePopradPre?ovStar? ?ubov?aSpi?
		StredBansk? BystricaKysuceLiptovNovohradOravaPova?sk? BystricaPrievidzaTuriecZvolen?iar?ilina
 	
  
  EKONOMIKA 
  SVET 
  KOMENT?RE 
  KULT?RA 
  ?PORT 
  TV 
  AUTO 
  TECH 
  PORAD?A 
  ?ENA 
  B?VANIE 
  ZDRAVIE 
  BLOG 
 
 
 
 
 Encyklop?dia: 
 ?udia 
 Udalosti 
 Miesta 
 In?tit?cie 
 Vyn?lezy 
 Diela 
 
 
 
  
   
    
     
      
       



 
 Pompeje 
 
 Vydan? 25. 8. 2004 o 0:00 Autor: MATEJ HANULA
 
 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 "Bolo po?u? n?rek ?ien, pla? det? a v?kriky mu?ov. Niektor? sa b?li smrti tak ve?mi, ?e sa modlili, aby pri?la. Mnoh? sa up?nali k bohom, ale e?te viac bolo t?ch, ?o boli  presved?en?, ?e bohov u? niet a t?to noc je poslednou nocou sveta." O?it? svedok Pl?nius Mlad?? takto op?sal po rokoch katastrofu Pompej?. V?buch bl?zkej sopky Vezuv pochoval toto mesto pod vrstvu l?vy a vulkanick?ho kamenia. Zo zemsk?ho povrchu zmizli aj men?ie mest? Herkulaneum a pr?stav Stabiae. Stalo sa to pred 1925 rokmi, 24. augusta 79 n?ho letopo?tu. 
 U? v  ?asoch R?mskej r?e bola oblas? pri Neapolskom z?live zn?ma svojou kr?sou a v??dnym podneb?m. Bohat? Rimania si v Pompejach, asi dvadsa? kilometrov od Neapolu, stavali letn? vily. 20-tis?cov? Pompeje prosperovali z obchodu s v?nom, olivov?m olejom a rybami. Presl?vili sa v?robou mlynsk?ch kame?ov, parfumov a oble?enia. Medzi obyvate?mi boli bohat?  vlastn?ci p?dy, obchodn?ci, remeseln?ci a umelci. Vezuv prin?al ?rodn? p?du, v?aka ktorej sa obilie rodilo tri razy do roka. Nakoniec v?ak priniesol mestu skazu. 
 V?buch z roku 79 nebol prvou pohromou Pompej?. Sedemn?s? rokov predt?m zasiahlo mesto zemetrasenie, ktor? zni?ilo ve?a stavieb. Mnoh? z nich e?te rekon?truovali. Otrasy p?dy neboli  v?nimo?n?, obyvatelia boli na ne zvyknut?. Ke? sa zem znovu zatriasla 20. augusta, nikto nevedel, ak? katastrofu predznamen?va. 
 
 
 

 Skaza sa za?ala 24. augusta popoludn?. Na oblohe sa objavil obrovsk? nezvy?ajn? oblak. Vych?dzal z vrcholu Vezuvu a postupne sa rozvetvoval. Pl?nius ho op?sal ako oblak v tvare borovice. ?plne zatienil slnko, tak?e nastalo ?ero. Bol to sope?n? dym, ktor?  prin?al ?lomky vulkanick?ch horn?n a popol. D?? padal nieko?ko hod?n. Ke?e vietor f?kal zo severov?chodu, dopadal smerom na juhoz?pad na Pompeje. Neapol bol u?etren?. 
 Sope?n? d?? e?te skazu miest nesp?sobil. Za obe? mu padlo nieko?ko striech. Medzi ?u?mi sa ??rila panika, ale nikto netu?il, ?e e?te len  pr?de k v?buchu, ktor? vyvrhne na zemsk? povrch magmu z ?trob sopky. Z?chranu h?adali tam, kde sa c?tili najbezpe?nej?ie - vo svojich domovoch. Dnes u? m?me aj d?kazy o tom, ?e mnoh? sa po pokuse o ?tek do svojich pr?bytkov vr?tili. 
 Definit?vne zni?enie pri?lo kr?tko po polnoci. V z?vere?nej f?ze u? sopka str?cala energiu, a tak  namiesto chrlenia kame?ov do vzduchu sa l?va vyliala von. Lav?na hor?ceho kamenia a roztaven?ch horn?n sa zos?vala po svahoch Vezuvu r?chlos?ou pribli?ne 100 kilometrov za hodinu. Po ?tyroch min?tach zasiahla Herkulaneum na ?p?t? hory. Obyvatelia utiekli na bl?zku pl?, kde pl?novali pre?ka? noc. Zadusil ich hor?ci vzduch naplnen? s?rou. 
 V  Pompej?ch sa l?va zastavila na severnom m?re mestsk?ch hradieb. Ten v?ak dlho neodolal, l?vy bolo po chv?li to?ko, ?e sa preliala cez hradby a mesto pochovala. Vn?tri hradieb zomrelo asi  
2-tis?c ?ud?, ?al?ie tis?ce zahynuli pri z?falom pokuse o ?tek. Spolu s dvoma men??mi mestami pochoval Vezuv pribli?ne  
16-tis?c obyvate?ov r?e. 
 Pl?nius  Mlad?? op?sal vo svojich listoch historikovi Tacitovi ?iny svojho str?ka, r?mskeho spisovate?a Pl?nia Star?ieho. Ten bol ?radn?kom a velite?om flotily v bl?zkej Misene. Popoludn? 24. augusta sa vydal lo?ou na pomoc ohrozen?m mest?m. Flotila stihla odviez? z Pompej? mno?stvo ?ud?. Pl?nia priviedli okolnosti do Stabi?. Tu pl?noval nalodi?  nieko?ko ob?anov. Pri sch?dzan? do pr?stavu si chr?nili hlavy pred sope?n?m da??om vank??mi. Na mori sa v?ak u? vtedy vzd?vali obrovsk? vlny - plavba bola nemo?n?. Vr?tili sa do mesta. Pl?nius chcel ?ud? upokoji?, preto si prv? ?ahol spa? v n?deji, ?e do r?na sa v?etko skon??. V noci ich v?ak  v?etk?ch na?la smr?. 
 Katastrofa bola darom pre archeol?gov. L?va mesto dokonale zakonzervovala, tak?e sa takmer kompletne zachovalo v stave z roku 79. Odha?ovanie Pompej? sa za?alo u? v 16. storo??, systematick? v?skum od roku 1806. Pred svetom sa tak op? zjavili pal?ce s n?dhern?mi freskami, chr?my, amfite?ter pre 20-tis?c div?kov, obchody, ulice a  n?mestia. Mesto malo prepracovan? akvadukt, pitn? voda prich?dzala do font?n a mestsk?mu patrici?tu aj priamo do jeho pal?cov. Na?li sa aj odtla?ky ?udsk?ch tiel zrkadliace posledn? okamihy ?ivota niektor?ch obyvate?ov. Pompeje s? dnes zrejme najnav?tevovanej?ou archeologickou lokalitou na svete. 
 
 


 
 
 
	
		Zdie?a?
		
			Zdie?a? na
			 

Tweet


      	
			 
		
	
	
		Diskusia (0)
		Nahl?si? chybu
		Tla?
	
 
 
 
 
 
 
  
 
 
 
 Hlavn? spr?vy 
 


 
 

 
AKTUALIZOVAN? O 20:00  Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 
 Dol?r v?era st?l u? takmer 80 rub?ov, pod hladinu ho tla?ia najm? n?zke ceny ropy. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 20:22  Slovenky v hlavnej skupine nebodovali, ?v?dky vyhrali o 9 g?lov 
 Na?e h?dzan?rky hrali na ME piaty z?pas. V Z?hrebe proti ?v?dkam. 
 
 
 
 
 




 
 

 
AKTUALIZOVAN? 18:45  
Taliban za?to?il na citliv? miesto Pakistanu. Na deti
 
 Taliban zabil v ?kole v Pakistane najmenej 141 ?ud?, v??inou deti. 
 
 
 
 
 




 
 

 
KOMENTARE.SME.SK  Cynick? obluda: Medzi?asom na opustenom ostrove 
 Stroskotanci maj? ve?k? ??astie, ?e s? dvaja. Lacn? vtipy na ?rovni ich autora. 
 
 
 
 
 


 
 
 
 

				 
        
         
 
 
  
 
 
          

           
     
       
 
 24h 
 3dni 
 7dn? 
 
       
 
	 
	 
 
 
 
 
 
 
 
 
 
     

           

          
 
 
  
 
 
  
 


  
  
 


  
         
        
        



       
      
     
    
   
   
  

 
 
 
 U? ste ??tali? 
 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	 
 
 



 
 
 
	 

		Kontakty
		Predplatn?
		Etick? k?dex
		Pomoc
		Mapa str?nky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Telev?zor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		?al?ie weby skupiny: Prihl?senie do Post.sk
		?j Sz? Slovak Spectator
		Agent?rne spr?vy 
		Vydavate?stvo
		Inzercia
		Osobn? ?daje
		N?v?tevnos? webu
		Predajnos? tla?e
		Petit Academy
		SME v ?kole 		? Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia N?v?tevnos? webu Predajnos? tla?e Petit Academy © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  


 



 

 

 


 











 
 
 
 
 






