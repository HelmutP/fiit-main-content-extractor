
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Baláž
                                        &gt;
                Nezaradené
                     
                 Moja pocta Petrovi Dubovskému 

        
            
                                    23.6.2010
            o
            7:24
                        (upravené
                23.6.2010
                o
                3:57)
                        |
            Karma článku:
                13.11
            |
            Prečítané 
            3859-krát
                    
         
     
         
             

                 
                    23.júna 2000 zahynul počas dovolenky v Thajsku jeden z najlepších futbalistov v histórii Slovenska Peter Dubovský. Presne o 10 rokov a jeden deň odohrá Slovensko proti Taliansku zápas o postup do osemfinále MS.
                 

                 Predpokladám, že v tento deň bude o ňom viacero článkov. Ja si však neodpustím napísať aspoň pár mojich spomienok na tohto výnimočného futbalistu. Čo ma potešilo, našiel som aj viaceré videá.   Peter Dubovský v mojej pamäti   Pamätám, ako v jednom z bratislavských derby Slovan - Inter nastúpil na trávnik mladý dorastenec, vtedy 18 ročný Peter Dubovský. Slovan viedol o 3 góly a Peter pridal zakrátko ďalší gól.   http://www.youtube.com/watch?v=eloh-2O6vLM   (Aj s prognózou Miroslava Michalecha "Možno sa rodí nový ligový strelec.")   Ako viedla Slavia na Slovane tesne pred koncom zápasu 2:1 a nakoniec prehrala 2:3. Hetrik strelil Dubovský (video od času cca 1:00).   http://www.youtube.com/watch?v=jD0RkQzt0ag   Gól hlavou proti Realu Madrid v Pohári UEFA, na ktorý mu centroval dvorný nahrávač Ondrej Krištofík.   http://www.youtube.com/watch?v=Bp1bY63UwX0   Dva góly proti Ferencvárosu Budapest pri výhre 4:1 v 1.kole Pohára európskych majstrov.   http://www.youtube.com/watch?v=jw1ly7uee2g   http://www.youtube.com/watch?v=YiA5WydccgY   http://www.youtube.com/watch?v=HsjhSElWOL8   Zápas v Košiciach v drese Reprezentácie Česka a Slovenska proti Hagiho Rumunsku. Za stavu 2:2 dostal od Němečka prihrávku do uličky a netradične pravačkou prekonal krížnou strelou k vzdialenejšej žrdi Lunga. V závere zápasu pri dvojnásobnej presile Rumunska - priamy kop zahrával z hranice 16-ky Skuhravý, napálil to tvrdou strelou rovno do múru. Dubovský si odrazenú loptu ešte vo vzduchu stlmil na hrudi a ľavačkou poslal pod brvno. O chvíľu už exekúciu priameho kopu nechali na Petra a ten vymietol horný roh. ČSFR vyhralo s 9 hráčmi 5:2, Dubovský strelil hetrik.   RČS - RUMUNSKO 5:2 (2:1)    Košice, 02.06.1993. Kvalifikácia MS 1994. Štadión: Všešportový areál. Divákov: 15 000. Rozhodca: Kim-Milton Nielsen (Dánsko). Góly: 13. Vrabec, 37. Látal, 59., 83. a 90. Dubovský - 26. a 56. Raducioiu. Vylúčení: 71. Němec, 77. Vrabec. RČS: Petr KOUBA - Radoslav LÁTAL, Jan SUCHOPÁREK, Jiří NOVOTNÝ, Petr VRABEC - Václav NĚMEČEK, Ľubomír MORAVČÍK, Peter DUBOVSKÝ, Luboš KUBÍK (46. Jiří NĚMEC) - Tomáš SKUHRAVÝ, Pavel KUKA (80. Miloš GLONEK). Tréner: Václav Ježek.  Rumunsko: Silviu LUNG - Ioan SABAU, Daniel Claudiu PRODAN (75.Ovidiu HANGANU), Miodrag BELODEDICI, Dorinel MUNTEANU - Ionut LUPESCU, Gica POPESCU, Gheorghe HAGI, Ilie DUMITRESCU - Marius LACATUS (60. Basarab PANDURU) Florin RADUCIOIU. Tréner: Cornel Dinu.   Kvalifikácia nakoniec pre dosluhujúci spoločný tím nedopadla dobre. To bolo asi najväčšie futbalové sklamanie Petra Dubovského. Hádam každý, kto videl tento zápas, si pamätá Dubovského sediaceho na trávniku dlho po záverečnom hvizde s hlavou sklonenou medzi kolenami. Vedel, že novotvoriaci sa tím Slovenska nebude mať veľa šancí v kvalifikáciach o postup na MS a ME. Video z posledného zápasu o postup v Belgicku, kde potrebovala RČS vyhrať:   http://www.youtube.com/watch?v=BdTzrjy6_nQ   Pamätám aj na gól v drese Slovana proti dánskemu Aarhusu. V zápase Interpohára v 1992 (hralo sa v Zlatých Moravciach) zobral Dubovský loptu na polovici ihriska a skončil až v bránke. Postupne oklamal 4 súperov vrátane brankára, pričom minimálne jeden dostal "jasličky". Tento gól súťažil aj o Gól rok v TV-ankete a zdá sa mi, že aj vyhral.   Alebo potom v drese Realu Madrid. V Pohári UEFA proti Luganu čakal brankár súpera na center, ale Peter to dal po zemi do bránky z dosť ostrého uhla.   Keď zahodil už v drese Slovenska penaltu na Faerských ostrovoch. Predtým síce prihral na gól Moravčíkovi, ale potom to bolo dlho 1:1. Až do 90.minúty, kedy trafil z priameho kopu ľavačkou k ľavej žrdi.   http://www.youtube.com/watch?v=QCu6EfZqExU   Ešte si spomínam, ako v niekorej z ankiet o Najlepšieho mladého hráča roku 1992 či 1993 obsadil druhé miesto za Ryanom Giggsom. A ako nastúpil v exhibičnom zápase na Vianoce 1992 za výber legionárov talianskej ligy proti AC Milan, hoci bol hráčom Slovana Bratislava (v tom zápase nastúpil aj Miloš Glonek, vtedy už hráč Ancony, predtým Dubovského spoluhráč v Slovane).   Mohol by som pokračovať ďalej, napríklad aj o tom ako sa hneval a na novinárov a nekomunikoval s nimi nielen kvôli prezývke Dubák, ktorú používali v článkoch. Jeho prezývka bola Bobo.   Peter Dubovský v Reale Madrid    O prestupe do Realu Madrid sa popísalo veľa. Mal ísť do Ajaxu, ktorý ho mal ako transferovú prioritu číslo 1 na pozíciu ofenzívneho záložníka. Už tam možno aj jednou nohou bol, ale ponuku prebil vyše 100 000 000 čiastkou (v slovenských korunách) Real Madrid. Ajax potom kúpil Litmanena (ktorý bol až "prestupová trojka"). Urobil z neho hviezdu svetového futbalu a vyhrali všetko čo sa dalo, vrátane Ligy majstrov. Holandská liga vtedy slúžila ako prestupná stanica pre viaceré hviezdy ako boli napríklad Romário či Ronaldo.   V Reale Madrid to mal ťažké. Real bol vtedy v kríze a do Dubovského vkladali veľké nádeje. Medzi Španielskom a vtedajším postkomunistickým a čerstvo rozdeleným Slovenskom však bol obrovský rozdiel. V kultúre, v spôsobe života. Zvlášť pre 21-ročného mladého muža, ktorý bol viac introvertom. V prvej sezóne hrával takmer pravidelne. Nástup mal bleskový. Vo vôbec prvom zápase za Real Madrid (prípravnom zápase proti Herculesu) skóroval po deviatich minútach pobytu na ihrisku. Prvý súťažný strelil už v spomínanom zápase proti Luganu, dňa 15.9.1993:   REAL MADRID CF - FC LUGANO (SUI)		3-0   Góly:	1-0 44. Dubovský,  2-0 66. Michel,  3-0 70. Fernández (vlastný).   Real Madrid: Buyo, Lasa, Luis Enrique, Alkorta, Sanchís, Michel, Hierro, Martín Vázquez, Dubovský, Zamorano (61. Alfonso), Butrageňo.    Počas sezóny prišlo k zmene trénera a Benita Flora vystriedal Vincente del Bosque. Dubovský hrával aj naďalej. Buď na pozícii ľavého záložníka alebo ako útočník. Celkovo odohral 35 súťažných zápasov (vrátane pohárových a európskych), strelil 2 góly. V 11 prípravných zápasoch skóroval trikrát.   V druhej sezóne nastúpil tréner Jorge Valdano a ten mal iných obľúbencov. Vtedy mohli hrávať maximálne traja legionári a Peter bol u Valdana často tým štvrtým. Spočiatku síce nastupoval, ale hneď v 3.kole prišlo zranenie a potom dostávali na legionárskych pozíciach šance Redondo, Laudrup a Zamorano. Navyše sa začal v útoku presadzovať mladý Raúl. V prípravných zápasoch to Petrovi sypalo. Počas sezóny nastúpil v šestnástich a zaznamenal 9 gólov. V súťažných dal v deviatich zápasoch jeden gól. Asi najlepšie obdobie zažil pred Vianocami 1994. 15.12. strelil v zápase o "Trofeo de la Línea" proti PSV Eindhoven (3:0) dva góly. O 8 dní pridal ďalšie dva Atleticu Madrid (2:1) a získal pre Real "Trofeo de Navidad C.A.M." Ani to Valdana nepresvedčilo a na ďalší ligový zápas nastúpil až 9.4.1995. Dubovský vystriedal v 78.minúte Zamorana a v 79.minúte zvýšil na 3:0...   Po dvoch sezónach prišiel prestup do Realu Oviedo. Ked som si prečítal niektoré vyjadrenia fanúšikov, ktorí na neho ani po 10 rokoch nezabudli, zhodujú sa, že to bol hráč dvoch tvárí. Keď mal svoj deň, tak bol na neudržanie. Keď ho nemal, tak na ihrisku neexistoval. Klub z Astúrie a jeho fanúšikovia si Petra Dubovského na konci tejto sezóny uctili krátkou predzápasovou slávnosťou (koniec prvého videa).   http://www.youtube.com/watch?v=4N1WjXZivG4   Góly Petra Dubovského v drese Ovieda:   http://www.youtube.com/watch?v=5wb449NlbDw   Peter Dubovský dnes   Zdá sa mi, že na Petra Dubovského sa na futbalovom Slovensku zabúda. Máme po ňom síce pomenovanú cenu pre najlepšieho mladého hráča, ale to asi tak všetko. Viem, že slovenská futbalová história má mnoho osobností, ktoré dokázali na ihrisku viac (napríklad víťazstvo Slovana v PVP 1969 alebo striebro na MS 1962), ale Dubovský bol v niečom výnimočný. Bol to asi posledný hráč, na ktorého sa ľudia chodili pozerať. Slovan mal vtedy v kádri viacero výborných hráčov ako boli Vencel, Stúpala, Glonek, Zeman, Kinder, Krištofík, Pecko, Timko. Dubovský bol niečo špeciálne.  Ja si pamätám zápasy napríklad v Nitre, kde bolo "na Dubovského" viac ako 10 000 ľudí. Zaslúžil by si, aby bol po ňom pomenovaný niektorý štadión. Je však problém, ktorý. Mám pochybnosti, či ešte vôbec existuje ihrisko klubu Vinohrady, kde začínal. Z Tehelného poľa je ruina, na ktorej chcú zarobiť určité typy futbalových podnikateľov. Ten nový národný (ak náhodou bude), to bude najskôr nejaká XXX-aréna.   Neviem, čo by robil Peter Dubovský dnes. Možno by ako 38-ročný ešte aktívne hrával na vysokej úrovni alebo by bol asistentom trénera Weissa, či priamo trénerom reprezentácie. Som však presvedčený, že nejakým spôsobom by slovenskej reprezentácii pomáhal. Bolo by pekné, keby si ho naša výprava v JAR nejakým spôsobom uctila pred zápasom s Talianskom. Symbolickým dresom na lavičke s čislom 10 a jeho menom (možno si niekto v SFZ na Petra Dubovského pred odchodom na MS spomenul)... Alebo aspoň hráči - výkonom na ihrisku, za ktorý sa Peter Dubovský nebude musieť hanbiť.   http://www.youtube.com/watch?v=MuDtlPUb3n0 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Baláž 
                                        
                                            Jaroslav Halák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Baláž 
                                        
                                            Prvý ťah Glena Hanlona
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Baláž 
                                        
                                            MS v hokeji divízia IIB - Estónci splnili úlohu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Baláž 
                                        
                                            Historický úspech španielskeho hokeja
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Baláž 
                                        
                                            Pavol Demitra: "Prehrali sme si to sami..."
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Baláž
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Baláž
            
         
        pavolbalaz.blog.sme.sk (rss)
         
                                     
     
        Nemám rád komunistov a im podobných obmedzených tupcov a vlastne celú tú bandu budovateľov svetlých zajtrajškov.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1818
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




