
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Petronela Ľachová
                                        &gt;
                Nezaradené
                     
                 Sme len ľudia :) 

        
            
                                    12.4.2010
            o
            18:47
                        |
            Karma článku:
                4.50
            |
            Prečítané 
            705-krát
                    
         
     
         
             

                 
                    Na svete sú miliardy ľudí a pri tom by sme nenašli dvoch na vlas rovnakých. Všeobecne môžeme ľudí rozdeliť na introvertov a extrovertov. Dve povahy, diametrálne odlišné. Zhovorčiví a tichí, roztržití a vnímaví, otvorení a uzavretí, optimistickí a pesimistickí a pri tom sa navzájom nevylučujú ani tieto protiklady.
                 

                     Nie je ani tak veľa typov pováh, ako sú rôzne kombinácie vlastností, ktoré máme. Kto si to všíma a vníma, vie po pár vetách zaradiť človeka, no samozrejme, je to málo na to, aby poznal ako bude reagovať v určitých situáciách. Preto sa radi spoznávame a rozprávame. A lákajú nás ľudia, ktorí sú iní, záhadní, ktorých je ťažké prečítať. Niekedy ich odsúdime s tým, že on je divný, pri tom len nemusí zapadať do konzervatívneho vzoru, do predstavy, ktorú sme si ako spoločnosť vytvorili. Len pri porovnaní ľudí v Európe a v Amerike. Je tam taká sloboda, až je nákazlivá. Môžete si začať v metre spievať a nikto ani len neotočí hlavou, pri čom tu by ste si vyslúžili pohoršujúce pohľady. Ťukáme si prstom na čelo, ak nás niekto nechápe a ostávame nešťastní, ak niekto nepochopí nás. Potom sú medzi nami herci, ktorí hrajú nie len na plátne, ale aj v skutočnom živote. Niektorí lepšie ako na oskara. A potom sú úprimní, ktorí hrajú, ale otvorenú hru. Niekedy vedia úprimnosťou raniť a to sa nám nepáči. Ale lepšie je povedať veci rovno do očí.   Každý človek má pre niečo nadšenie, dôvod, pre ktorý žije. Svoje priority. A do života si hľadá niekoho, kto pôjde jeho cestou, niekoho, v kom nájde sám seba, svoju druhú polovicu, ktorá mu chýba. A tak sú ženy a muži. Niekto raz povedal: "Ženy a muži sa nikdy nemôžu pochopiť, lebo žena chce muža a muž ženu."Raz za život môžeme stretnúť človeka, s ktorým sme ako dokončený obraz z puzzlí, dokonale do seba zapadajúci. Tvoria potom jeden tím, spolu, sami proti svetu..   Chodíme okolo ľudí dennodenne a každý jeden má svoj osud, svoje myšlienky, svoje problémy a svoje radosti. Každý iný, každý výnimočný. Každý by vedel vyrozprávať svoj príbeh, každý jeden z nás tvorí samostatnú kapitolu v knihe s názvom Svet :)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petronela Ľachová 
                                        
                                            Počúvajme
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Petronela Ľachová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Petronela Ľachová
            
         
        lachova.blog.sme.sk (rss)
         
                                     
     
        Učím sa byť realista, ale ešte stále verím v dobro ľudí.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    701
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




