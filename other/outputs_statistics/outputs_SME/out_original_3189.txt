
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radka Mydlarčíková
                                        &gt;
                Nezaradené
                     
                 Štockholm mojimi očami 

        
            
                                    23.3.2010
            o
            22:07
                        (upravené
                23.3.2010
                o
                23:25)
                        |
            Karma článku:
                9.60
            |
            Prečítané 
            3258-krát
                    
         
     
         
             

                 
                    Som doma ani nie hodinu a už sa mi za týmto milým mestom cnie. Priznám sa, že doteraz som veľmi k severským krajinám príliš neinklinovala. Povedzme si však úprimne, lacné letenky, ubytovanie zdarma, mňa a moju kamarátku presvedčili.
                 

                 Dvojhodinový let z Bratislavy nám zbehol rýchlo. Pristáli sme na letisku s názvom Skavsta odkiaľ sme si museli kúpiť lístky na autobus do centra. Na letisku sa dajú kúpiť lístky od viacerých spoločností. Jednosmerný lístok vás vyjde v prepočte na našu menu na sedem eur. Autobus vás odvezie na gigantickú stanicu s názvom T-Centralen. Samotná cesta trvá približne osemdesiat minút.    V momente ako sme vystúpili som mala pocit ako keby som sa ocitla v nejakom futuristickom sne. Supermoderná stanica nás úplne ochromila a dovolím si tvrdiť, že tá londýnska je v tomto ohľade o čosi „chudobnejšia.“ Švédi sú veľkí systematici a všetko je dokonale zorganizované. Čokoľvek sa snažíte nájsť, nebude vám to trvať viac než pár minút. A to je už čo povedať aj na mňa – lovkyňu trapasov a nositeľku nulovej orientácie.    „Lístky na všetko“ (v prípade Štockholmu rozumej na metro, autobus a loď) si môžete kúpiť v novinovom stánku. Tu by som chcela upozorniť na ceny, ktoré ma dosť zaskočili. Vedela som, že to bude drahý špás, ale toto ma úplne dorazilo. Keď sme sa pýtali koľko by nás vyšiel štvordňový lístok, predavačka nám odpovedala, že vraj v prepočte 70 eur. „Čo blázniš?“, povedali sme si a kúpili sme si „Pre paid strips“(1 osoba/8 lístkov), ktorý si môžete zaobstarať za necelých 20 eur. My sme šli napoly a tak každá z nás mohla daný lístok využiť štyrikrát. Jeden lístok však platí iba jednu hodinu, takže sme dosť šetrili. Ak však vyzeráte pod dvadsať rokov, môžete si kúpiť zľavnenú verziu týchto lístok, čo vás vyjde na 11 eur. My sme po druhý raz využili túto možnosť a žiaden kontrolór si od nás naše ID nepýtal.    Po Budapešti som veľmi uvítala zmenu týkajúci sa dorozumievania. 90% obyvateľstva tu rozpráva perfektnou angličtinou. Od manažérov, cez predavačov až smetiarov. Neviem čím som si to vyslúžila, ale od prvého momentu ako som vystúpila na T-Centralen na mňa ľudia rozprávali po švédsky. Na jednej strane ma to zaskočilo, na druhej strane mi to pripadalo  neuveriteľne milé. Asi som švédsky typ. J Ľudia sú tu veľmi milí. Pôsobili na mňa veľmi upokojujúco. Žiadne stresy, celková spokojnosť, ktorá im vyžarovala z tváre mi pomohla splynúť s jedným z najkrajších európskych miest. Ak ste sa stratili a opýtate sa ich na cestu, ochotne s vami pôjdu a ukážu vám smer, respektíve vás na to miesto priamo odprevadia.    Čo sa týka mužov, nemožno ich nespomenúť, zostala som mierne prekvapená. Švédski muži sú neuveriteľne štýloví a pekní. Svetlovlasý, vysoký so strniskom a samozrejme dobre oblečený – to je prototyp Švéda. Od  začiatku som od nich nemohla odtrhnúť oči. Často krát sa hovorí o Francúzoch, Talianoch v rámci módy a šarmu. Ľudia, ktorí preferujú spomínané krajiny zrejme neboli vo Švédsku. U mňa sa odteraz budú Švédi nachádzať v prvej pätici.   Ak by som mala hovoriť o ženách, nebojte sa. Žiadne „Pippi dlhé pančuchy“ tam nechodia. Švédky vedia ako na to. Niektoré z nich sú veľké extrémistky a v móde nepoznajú žiadne obmedzenia. Občas mi mnohé pripadali, že si súdnosť nechali doma. Išlo hlavne o oblejšie ženy, ktoré nemali problém navliecť sa do siloniek a minišiat. Tiež nepatrím medzi najštíhlejšie, a preto som obdivovala ich odvahu vyjsť do ulíc. Na druhej strane, v Štockholme nikto nikoho nerieši, takže sa im príliš nečudujem.    Ak už spomínam módu a oblečenie, Švédi mi v mnohom pripomínali Francúzov. Vonku mínusové teploty a 80% mladých ľudí si veselo vykračovalo v rozopnutých kabátoch alebo tenkých kožených bundách. Pri teplote plus desať však niektorí šli do extrémov a po meste behali v krátkych šortkách. Taktiež sme zhodnotili, že muža Švéda spoznáš nie podľa vlasov, ale podľa riflí vyhrnutých tak, že im vidieť ponožky. Alebo varianta číslo dva, rifle vsúkané do úzkych topánok „a la Pippi.“   V prípade jedla, ak nechcete minúť príliš veľa, oplatí sa navštíviť známe fast foody, kde sa ceny za menu pohybujú v rovnakých cenových reláciách, plus mínus euro rozdiel. Čo sme si však v Štockholme vychutnali boli coffee posedenia. Skoro všetky kaviarne sú presklené a všetci Švédi uprednostňujú sedenie pri oknách. Chcú vidieť a chcú byť videní. V starom meste odporúčam navštíviť kaviareň Espresso coffee, kde za latte zaplatíte okolo troch eur a naservírujú vám to skutočne štýlovo. Taktiež priamo v centre nás očarila kaviareň John Chris coffee, kde sme cez sklo mohli rozjímať a sledovať pulzujúci dav za oknom. Ľudia sa na vás usmievajú a sú radi, že sa na nich pozeráte. Jeden fotograf si nás takto chcel dokonca odfotiť. J   Alkohol je vo Švédsku veľmi drahý. A keď píšem drahý, tak to tak aj myslím. Je drahý dokonca aj pre samotných Švédov. Najlacnejšia fľaša vodky ich vyjde na dvadsať eur. Na diskotéky, koncerty alebo bary si riadne priplatíte. Len samotný vstup vás bude stáť desať eur  a odloženie kabáta ďalšie dve eurá. V samotnom vstupe nemáte zahrnutý žiaden drink. Pivo sa tu pohybuje v cenových reláciách okolo štyroch eur a s tvrdším alkoholom sa ceny pohybujú smerom nahor. Avšak Švédi vedia ako oklamať systém. Často pašujú alkohol do podnikov a na toaletách si potom dopĺňajú svoje poháre tak, aby ich nechytil nejaký ten pán vyhadzovač. Ak však nechcete riskovať, aj na to majú naši švédski kamaráti odpoveď. Je ňou Snuss. Číry nikotín, ktorý sa používa v prípade ak už na alkohol nezostala ani jedna švédska koruna. Bežne to predávajú v trafikách, najkvalitnejší je čierny s názvom General. Chlapci nám tvrdili, že ak si to dáme a necháme pôsobiť v ústach, budeme mať z toho riadny flesh. Vo Švédsku to má snáď každý človek a ak vám váš dôjde, nemajú problém sa s vami oň podeliť. Snuss si môžete zaobstarať v prepočte za 4 eurá. Bary, diskotéky a samotný nočný život funguje od piatka do soboty, prípadne v nedeľu. Zvyšok týždňa Švédi oddychujú a šetria na ďalší výjazd.    Doprava je tu ukážková. Nadzemné metro ma pravdepodobne nikdy neprestane fascinovať a veľmi som si ho užila. Žiadna špina ani posprejované vlaky. Švédi si potrpia na čistotu. Taktiež sa všetci chodci radi púšťajú na červenú a na prechode pre chodcov vám zastaví každé auto. Skutočne každé. Vodiči tu dodržiavajú pravidlá cestnej premávky, čo je po Grécku veľmi príjemná zmena. Obrovskému počtu automobilov tu konkuruje bezpočet bicyklov, na ktoré natrafíte všade.    Švédsko vždy dokáže prekvapiť. Dnes, keď sme nastupovali na autobus na letisko, sa mne a kamarátke ušli dve posledné miesta. Ja som sedela hneď vedľa vodiča a kamarátka o pár radov ďalej. V tom ku mne prišiel švédsky krásavec, poklepal ma po pleci a opýtal sa ma či si nechcem presadnúť ku svojej friend. Zostala som šokovaná, pretože na Slovensku by sa vám to iste nestalo. A takto dojemne sa so mnou rozlúčil celý Štockholm a ja naň budem spomínať do konca svojho života.      

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            what goes around comes around
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Odsudzovanie = delete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Princ na bielom koni alebo aj rytieri sú muži
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Grécko mojimi očami (časť druhá)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radka Mydlarčíková 
                                        
                                            Grécko mojimi očami (časť prvá)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radka Mydlarčíková
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radka Mydlarčíková
            
         
        mydlarcikova.blog.sme.sk (rss)
         
                                     
     
        Som veľkou neznámou aj pre seba samotnú.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1954
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




