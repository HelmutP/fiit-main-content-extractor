

 



 
 



 



 



 



 



 





 



 



 
Madridské
múzeum Prado ponúka veľa lahôdok.
 
 

 
 
Niektoré ma mimoriadne očarili. 
 
Hrou farieb. 
 
Rafinovanou kompozíciou. 
 
Skrytými odkazmi, ktorými sa autor s nami pohráva s očividným potešením ako
prešibaný kocúr so šeroslepými myškami.
 
 

 





 
Obraz
Rodina kráľa Filipa IV. namaľoval Diego Rodríguez de Silva y Velázquez v roku
1656.  
 
Je považovaný nielen za jedno z jeho najlepších diel, ale i celého baroka. 
 
Každý si však naň s úsmevom spomína ako na Las Meninas - Dvorné dámy.  
 
Chutnú päťročnú plavovlasú infantku Margaritu, budúcu rakúsku cisárovnú,
manželku Leopolda I. pri nekonečných hodinách pózovania zabávajú dve, len o pár
rokov staršie dvorné dámy, María Agustina Sarmiento de Sotomayor a Isabel de
Velasco. 
 
 
Dvorná
šaša, trpaslíčka Nemka Maribarbola ešte vládze podskakovať. 
 
V očiach piadimužíka Taliana Nicolasa Pertusata sa už zrkadlí únava, no
premáha sa.  
 
Kráľovský mastifský hafan to má najjednoduchšie, už odkväcol a podriemkáva.  
 
Maliar Diego sa nonšalantne tvári, že mu vôôôbec nevadí, že to nádherné dieťa
portrétuje odzadu.   
 
Jasné, že mu to nevadí.  
 
Malé zrkadlo nám prezrádza, že mama kráľovná Mariana a oco kráľ Filip IV. sa
radšej vykašľali na kráľovské povinnosti a pre istotu prišli skontrolovať, čo
vyvádza ich zbožňovaný jedináčik, neposedne živá rozprávková princeznička. 
 
 
To, že kráľovský pár nie je zobrazený v strede obrazu, ale v malom zrkadle a pološere jeho pozadia považujú niektorí znalci umenia na symbol, že Diego si veľmi dobre uvedomoval, že svetovláda španielskeho kráľovstva nenávratne zhasína ...
 

