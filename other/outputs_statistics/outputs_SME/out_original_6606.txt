
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Rôzne
                     
                 Pokladňa číslo 13 

        
            
                                    16.5.2010
            o
            15:48
                        |
            Karma článku:
                8.65
            |
            Prečítané 
            2572-krát
                    
         
     
         
             

                 
                    Doteraz som netušil ako dobre padne, keď je veľa ľudí poverčivých. Dnes je nedeľa a už odvčera leje  ako z dvoch krhiel. Preto na stredne veľký, operatívny nákup, som si vybral nemenovaný hypermarket v bratislavskom Lamači, ktorý má parkovisko pod strechou.
                 

                 
 foto: Jozef Drahovský 16.5.2010 niečo po 13:30
    Lenže rovnako zmýšľalo veľké množstvo nakupujúcich, ktorí radšej prešli autom aj niekoľko kilometrov navyše, len aby nemuseli stáť na vytrvalom daždi pri nakladaní tovaru do auta.     V nedeľu býva parkovisko obsadené tak na 1/4, ale dnes bolo takmer plné. Tak isto to vyzeralo v predajni, ako keby bol výpredaj všetkého za symbolickú cenu.     Hoci fungovala takmer polovica pokladní  (bežnú nedeľu stačí 1/4), tvorili sa pred nimi dlhé rady, preto som si na záver nákupov kúpil časopis na čítanie, zhodnotil šikovnosť ľudí v pokladniach, plnosť nákupných vozíkov v jednotlivých radoch a postavil sa do výrazne najkratšieho.     Trochu som sa aj čudoval, prečo v ňom nie je veľa ľudí. Bol asi polovičnej dĺžky voči ostatným. Potom som sa pozrel na mobilhodiny, ktoré ukazovali 13:13 a samozrejme som stál v pokladni 13, čim sa vysvetlila dĺžka radu.     Pustil sa do čítania nemenovaného časopisu a 25 minút čakania ubehlo ako voda.  Aby boli spokojní aj poverčiví, nepodarilo sa mi v peňaženke nájsť klubovú kartu, hoci som sa na ňu pozeral.     Takže vďaka kombinácii poverčivosti iných a veľmi šikovnej  pokladníčky v pokladni číslo 13, som ušetril aspoň štvrť hodinu času.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




