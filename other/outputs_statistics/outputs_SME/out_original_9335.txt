
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Klačková
                                        &gt;
                Moje aktivity
                     
                 Z Ružomberka okolo Mary a do Liptovských Matiašoviec 

        
            
                                    21.6.2010
            o
            19:00
                        (upravené
                21.6.2010
                o
                19:18)
                        |
            Karma článku:
                4.79
            |
            Prečítané 
            669-krát
                    
         
     
         
             

                 
                    Tento rok začína tak poskromnejšie. Na turistické výlety nie je toľko času a pravdu povediac aj to počasie nie je ideálne a tak skĺbiť čas a vhodné, keď už nie dobré počasie je dosť náročné. Aby to však nebolo pesimisticky zamerané, chcem Vás predsa inšpirovať na výlet, tak sa nám - mne a kamarátke Eni niečo také predsa len podarilo. Tento krát však na bicykloch.Náš cyklistický výlet sme začali s cieľom - pozrieť stav vody v priehrade Liptovská Mara. Túto trasu už poznáme a tak sme sa ani moc nezastavovali pri zaujímavostiach, ale spomeniem ich tu. Začiatok našej cesty je samozrejme v Ružomberku. Fotky som použila aktuálne aj rokmi nazbierané.
                 

                 
  
   Na smer Mara chodievame po tzv. starej ceste, kadiaľ premáva predsa len menej áut a dá sa toho aj viac vidieť. Cesta začína za vlakovou stanicou v Ružomberku  pod vrchom Mních a pokračuje cez obce Lisková, križuje sa s cestičkami do Turíka či Lúčiek a ďalej smeruje cez Liptovskú Teplú do Bešeňovej, kde mávame vždy kratšiu prestávku. Táto obec je známa vodným areálom a pre nás najmä zdrojom pitnej minerálnej  železitej vody. Prameň nájdete asi po 200 metrovom stúpaní za hotelom Summit a je na pravej strane cesty, často tu stoja nejaké autá, tak sa dá ľahko nájsť.  Na opačnej strane je aj zaujímavý skalný útvar s krížom, odkiaľ je pekný výhľad najmä na Nízke Tatry.         Keď už sme pri pohoriach, takmer celou cestou je na pravo dobre viditeľné pohorie Chočské vrchy a známy Veľký Choč. Za  Bešeňovou je krátky zjazd a potom sú dve možnosti cesty. My zvykneme hneď za týmto zjazdom zabočiť doprava na vedľajšiu cestu, ale dá sa pokračovať aj rovno cez obec Potok. Po spomínanom zabočení na cestičku, ktorá je v zime neudržiavaná, čo sa spomína na tabuli je náročné stúpanie, ale na jeho konci je perfektný výhľad na časť Mary, ktorý si zvykneme vychutnať, vlastne oddýchnuť po tom stúpaní J . Ďalej nasleduje nebezpečný zjazd po rozbitej ceste, takže neodporúčam sa uniesť a popustiť brzdy, je tu aj dosť možné, že tu stretnete nejaké auto a zákruta na rozbitej ceste s autom zrazu pred očami neveští nič dobré. Tu dole už človek obchádza cestu popri Mare a dostanete sa tu až na križovatku. My sme išli cez ňu doprava cez obce Vlachy a Vlašky. Za nimi sa vyskytne strmé stúpanie č. 2 a je to stúpanie po hrádzi, takže na jeho konci sa človek ocitne na priehradnom múre.      Asi prvý krát sme si tu všimli, že akýmsi bočným otvorom aj odtiaľto vypúšťali vodu. Nuž bolo obdobie povodní, tento výlet sa totiž odohráva v prvý slnečný krajší víkend po povodniach. Naša cesta po zhodnotení stavu vody a debate pokračovala ďalej.       Rozhodli sme sa totiž, že si ešte pozrieme obec Matiašovce, kamarátka chcela vidieť nejaký kostolík a tak ma prehovorila. Míňali sme archeologické múzeum v prírode Havránok  a známy kostolík pri Mare a pokračovali sme cez Bobrovník, kadiaľ sa cesta naozaj vychutnáva.             Pred nami výhľad, klesanie,.... takto nám zľahka pribúdali kilometre. Za Bobrovníkom sme pokračovali doprava, smerom na Mikuláš až sme sa dostali na križovatku, ktorá nás zaviedla na opačnú stranu a odtiaľ asi 4 km do tých Matiašoviec. Presnejšie Liptovské Matiašovce. Tu sa mi už moc nechcelo. Začala ma zmáhať akási únava a kamarátkine nadšené rozprávanie o tom, ako kostolík v tejto obci dobre vyzerá ma moc nepovzbudilo, tak som hundrajúc šlapala do pedálov. Ich každým otočením sme však boli bližšie až sme sa tu naozaj dostali.                Pred kostolíkom zo začiatku 16. stor., ktorý je údajne renesančno - barokový a zasvätený sv. Ladislavovi je ešte socha svätého za dedinou, ako to Eňa volala. Opevnenie okolo kostolíka je zo 17. Storočia a je naozaj na pohľad zaujímavé. Okrem iného je kostolík zapísaný aj na zozname kultúrnych pamiatok pod číslom. 331. Obec obklopujú nielen Chočské vrchy, ale aj Západné Tatry, tak výhľad je akousi odmenou, pre cyklo - turistov.      Cesta naspäť bola ešte dlhá, ale krátky oddych s výhľadom prospel. Aby sme nešli rovnakou cestou, tak sme z Matiašoviec išli cez Liptovskú Sielnicu a nevracali sme sa cez Bobrovník, ale pokračovali sme miernym dlhším stúpaním a potom vyše 10 % klesaním do obce Potok už po známej ceste do Bešeňovej a do Ružomberka. Toto klesanie je za odmenu. Po stúpaní s tu rada nechám uniesť a skúšam maximálnu rýchlosť, ktorá sa tu dá vyšlapať. Je dobre počkať si však, kým človek prejde prvú zákrutu, potom je rozhľad dopredu dobrý a ak nejde auto, tak to stojí za to, 50 km/ hod nie je problém tu dosiahnuť, kto má odvahu môže aj viac, ale naozaj pozor a predtým si to radšej prejsť aj menej riskantne, predsa len najlepšie je cestu pred tým poznať.   Celý tento náš výlet mal dĺžku približne 60 km a inšpiroval nás na ďalšie kilometre 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Klačková 
                                        
                                            Veľká Svišťovka 2038 m.n.m.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Klačková 
                                        
                                            Sedlo Prielom 2288 m.n.m.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Klačková 
                                        
                                            Ostrý Roháč, tam kde kamzíky ešte nestratili svoju plachosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Klačková 
                                        
                                            Jakubiná (2194 m.n.m.) - strážca Západných Tatier
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Klačková 
                                        
                                            Aj svište chodia na Východnú Vysokú (2429 m.n.m.)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Klačková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Klačková
            
         
        klackova.blog.sme.sk (rss)
         
                                     
     
        Myslim si, že som taká normálna osoba. Mám rada, keď sa okolo mňa niečo deje,aktívny život a k tomu mi pomáhajú aj "výlety" do prírody - človek sa vyčerpá a zároveň si aj neskutočne oddýchne. A viac,... no asi najlepšie - spoznať sa osobne, sama seba pár slovami asi neopíšem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    48
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1012
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje aktivity
                        
                     
                                     
                        
                            Turistika, príroda a výlety
                        
                     
                                     
                        
                            Svet naokolo - môj pohľad
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            E.M.Remarque - Zasľúbená krajina
                                     
                                                                             
                                            Království faraonu - Zahi Hawass
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            HIM - Screamworks - Love in Theory and Practice
                                     
                                                                             
                                            I.M.T. Smile - 2010 Odysea 2
                                     
                                                                             
                                            I.M.T.Smile - Hlava má sedem otvorov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Cez kopce to pešobusom trvá z Donoval do Telgártu štyri dni
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




