

 Každé ráno v autobuse čítam dennú tlač. Noviny a ich palcové titulky, už zrána, to je moje. V autobuse čítam i knihu. Je hrubá, vytlačená na kvalitnom papieri. Je napísaná veľmi pútavo a zaujímavo. No neviem ako sa volá. Drží ju v rukách veľký chlap. Text je popretkávaný vtipnými pointami. Vtedy chlap knihu na chvíľu privrie, nechá svoj ozrutný palec ako záložku, zasmeje sa a pretrie si oči. Napriek tomu sa text číta ľahko, hoci je celá napísaná, tuším v španielčine, či čo to je za jazyk. 
 V kníhkupectve mám rozčítané dve ďalšie knihy. Rád by som spomenul tituly, no moja úcta ku knihám mi to nedovolí. Keby som spomenul ich názvy, prerušil by som to puto čo medzi nami vzniklo, a odtajená identita by mohla mať negatívny dopad na ich referencie. A tiež by som nerád, keby ma zajtra, špičkovo utajený detektív, ktorý jediný chodí v zime po predajni v bunde a v šľapkách, odhalil. 
 Musím spomenúť  raj časopisov. Panel s dennou tlačou, periodikami v našom obchode. Beriem do rúk tie ochytané strany a absorbujem všetko čo je tam napísané. Znehodnotené okraje strán sa mi nepriečia. Moja myseľ potrebuje veľa textu, mnoho informácii a nezaujíma ju odkiaľ. 
 S hrôzou spomínam na týždeň doma s chrípkou. Tá prázdnota bola neopísateľná. Odkázaný na reklamné letáky zo schránky som ťažko prežil túto skúšku. Čítam všetko. Aj túto moju spoveď si prečítam zajtra v práci, ponad rameno niektorého kolegu. 
   
 Tvoj čitateľ 
   
   
   

