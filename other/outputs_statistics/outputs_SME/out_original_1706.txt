
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Andrej Osuský
                                        &gt;
                A je to!
                     
                 Domáca hrazda na cvičenie 

        
            
                                    20.9.2009
            o
            17:47
                        |
            Karma článku:
                10.65
            |
            Prečítané 
            20218-krát
                    
         
     
         
             

                 
                    Napadlo ma vyrobiť si jednoduchú hrazdu s upevnením na stenu. Hrazda do stropu je problém, keď máte strop zo sadrokartónu a populárna hrazda do dverí je síce skvelá vec, ktorá zázračne drží v zárubni, no neumožňuje širší úchop, čo je dosť podstatné obmedzenie. Zhyby na hrazde so širokým úchopom je najúčinnejšie cvičenie pre rozvoj širokých chrbtových svalov, ktoré formujú postavu do tvaru písmena "V" ako Valibuk.
                 

                 Podobné hrazdy, ktoré som našiel v obchodoch, sa mi zdali nenormálne predražené na jednoliaty kus železa jednoduchého tvaru, ktoré Číňania vyrobili za pár centov. Ľudia si však zvyknú hrazdu vyrábať sami - na internete je množstvo nápadov. Išiel som teda do železiarstva a obracal železá rôznych tvarov, z ktorých by sa dala vyrobiť hrazda. Po prehrabávaní sa vo vodovodných kohútikoch, vešiakoch a trúbkach som nakoniec kúpil toto:      Dva veľmi pevné trojuholníky slúžiace pravdepodobne na zavesenie police a dutú hliníkovú tyč, o ktorej som sa spočiatku obával, že sa ohne, no železnú som nemal po ruke. Každý trojuholník som  priskrutkoval na stenu s pomocou troch dlhých hmoždiniek. Hore som dal dve, aby som sa cítil bezpečnejšie.      Pôvodne som chcel využiť diery v trojuholníkoch, aby som cez ne prevliekol tyč, ale to by bolo príliš blízko steny. Tyč som položil zvrchu a upevnil hrubým drôtom. Ohnutie na koncoch za účelom šetrenia zápästia sa mi podarilo docieliť tak, že som tyč nahrial na plynovom sporáku a buchol po nej kladivom.      Aby som sa náhodou neoškrel o drôt, obalil som ho molitanom a následne celú tyč oblepil čiernou izolačnou páskou pre príjemnejší dotyk. Lepšie sa mi však osvedčili tréningové rukavice.         A je to. Občas som zabehol do lekárničky a pozorujúci kamarát mal o zábavu postarané. Výsledok úspešne drží a dá sa použiť na úzke zhyby, široké zhyby, bicepsové zhyby a sušenie vypratej mikiny. :-)                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Andrej Osuský 
                                        
                                            Nový volebný systém nás zbaví populistov a extrémistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Osuský 
                                        
                                            Týždeň v Grónsku akoby na inej planéte
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Osuský 
                                        
                                            Čo zažije exot na Mauríciu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Osuský 
                                        
                                            Ďaleko na juhu: život na Mauríciu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Andrej Osuský 
                                        
                                            Hypotéza odoláva 153 rokov, odmena je nevyčísliteľná
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Andrej Osuský
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Andrej Osuský
            
         
        osusky.blog.sme.sk (rss)
         
                                     
     
        Žijem na ostrove Lanzarote na Kanárskych ostrovoch. Pracujem cez internet, čo mi dáva slobodu pohybu a čas. Vyštudoval som teoretickú fyziku, ale zatiaľ mi nič nenapadlo. Rád plávam vo vlnách oceánu a potápam sa.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5416
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            A je to!
                        
                     
                                     
                        
                            Lanzarote
                        
                     
                                     
                        
                            Čudný svet
                        
                     
                                     
                        
                            Vážne veci
                        
                     
                                     
                        
                            Aj vážne veci sú čudné!
                        
                     
                                     
                        
                            Zábava graduje
                        
                     
                                     
                        
                            Kapitál
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rap ca ca, da ribi ribi de lando...
                                     
                                                                             
                                            Meliško
                                     
                                                                             
                                            Drncanie harddisku
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




