

 Vyškolený personál vášmu Chrobáčikovi kvalifikovane zmeria veľkosť i šírku chodidiel. Pre menších majú také tie klasické „posuvné pravítka" a pre tých väčších digitálne zariadenie pripomínajúce osobnú váhu. Poradia vám, aký typ obuvi je v danom veku pre Chrobáčikov vhodný, vyskúšajú zvolenú obuv, skontrolujú, či niekde netlačí, pri kúpe čižmičiek skontrolujú, či dobre sedia aj na lýtkach - radosť pozerať. 
 Ďalšia spoločnosť tiež zameraná na detskú obuv vám zase po registrácii posiela na mail informácie, z ktorých sa dozviete, že detské nožičky je potrebné premerať každých 6 až 8 týždňov, pretože veľmi rýchlo rastú. Nemusí to znamenať, že budete musieť zakaždým urobiť nový nákup, kontrola je však dôležitá. Detské kosti sú veľmi mäkké a môže ich zdeformovať aj primalá ponožka, nieto ešte primalé topánočky. Chrobáčikovia necítia, že ich topánka tlačí, ale ak si ich odmietajú obuť, mal by to byť pre rodiča signál, aby skontroloval, či topánka nie je primalá. 
 Na Slovensku som sa pýtala u malých predajcov obuvi na posuvné pravítka, aby som mohla Chrobáčikovi zmerať veľkosť nožičky. Jedna predavačka mi ako dôvod, prečo také meradlo nemá, uviedla, že aj tak sa číslovanie rôznych výrobcov veľmi odlišuje, a tak to nemá veľmi zmysel. Ja si skôr myslím, že práve preto by také meradlo mala mať. 
 Ale aby som nekrivdila, posuvné pravítka som našla v Deichmanne. Obsluhujúca predavačka mi však nevedela odpovedať na otázky a bola rovnako bezradná ako ja. 
 Ak poznáte na Slovensku predajcov detskej obuvi s kvalifikovaným personálom a potrebnými meradlami, dajte mi vedieť alebo si urobte reklamu v diskusii. A ak ste predajcom detskej obuvi a takéto služby neposkytujete, nechceli by ste o tom pouvažovať? 
   

