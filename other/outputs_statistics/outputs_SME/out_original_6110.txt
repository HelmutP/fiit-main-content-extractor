
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Kačmár
                                        &gt;
                Recenzie filmov
                     
                 Recenzia filmu: Svitanie 

        
            
                                    8.5.2010
            o
            12:30
                        |
            Karma článku:
                2.83
            |
            Prečítané 
            1013-krát
                    
         
     
         
             

                 
                    Úpíri ovládli svet. S megaúspešnou filmovou a knižnou sériou Súmrak, seriálom HBO True blood, a stále pribúdajúcim množstvom knižných titulov (napr. Upírske denníky, či Úpírska trilógia) sa zdá, že vo filmoch, televízii a literatúre sa im momentálne darí lepšie než kedykoľvek predtým. Aj nový film Svitanie (Daybreakers) je prírastkom k tejto téme, a zaujímavé je, že sa mu úspešne darí žáner inovovať.
                 

                 
Ethan Hawke, Claudia Karvan a Willem Dafoe vo filme Svitaniewww.moviesonline.com
   Vo Svitaní upíri naozaj ovládli svet. Jedno uhryznutie netopiera na začiatku sa zmenilo na rýchlo šíriacu sa epidémiu upírstva. Viedlo to k tomu, že v roku 2019 je svet ovládaný upírmi. Až tak sa však spoločnosť nezmenila.  Stále existujú vlády, stále sa chodí do práce. Akurát, že sa chodí pracovať v noci, do kávy sa namiesto mlieka pridáva krv, a zo zostávajúcich piatich percent ľudskej populácie sa stala lovná zver upírskej armády. Zajatí jedinci sa stávajú zdrojom krvi vo farmách firmy Bromley Marks. Tá je totiž najväčším dodávateľom krvi na trh. Krv sa však vďaka prudko sa zmenšujúcemu počtu ľudí stáva nedostatkovým tovarov, čo má pre upírov zničujúce následky. Preto sa Bromley Marks snaží vo svojich laboratóriach pod vedením hematológa Edwarda Daltona (Ethan Hawke) vytvoriť syntetickú náhradu krvi. Edward je upír, no odmieta piť ľudskú krv, a umelú krv berie ako spôsob, akým oslobodiť zvyšok ľudskej populácie. Naproti tomu Walter Bromley (Sam Neill), šéf firmy, považuje upírstvo za evolúciu. Umelú krv považuje za riešnie pre vačšinu upírov a za prostriedok, ako získať dostatok času na regeneráciu ľudí, ktorých by potom opäť využíval pre dostatočne bohatých záujemcov o ľudskú krv.   Edward sa náhodne zoznamuje z unikajúcou skupinou ľudí, ktorým pomôže. Vďaka jednej z nich - Audrey (Claudia Karvan) sa stretáva aj z Elvisom (Willem Dafoe), ktorý pozná lepšie riešenie pre ľudstvo ako umelú krv - stať sa opäť človekom. Vzdať sa nesmrteľnosti, a stratiť moc, je však pre Waltera Bromleyho a pre mnohyých ďalších upírov neprípustné.   Svitanie je béčko, a nehanbí sa za to. Je veľmi dobre vystavané. Nie žeby sa nenašla nejaká ta diera v scenári, ale príbeh a svet ovládaný upírmi mi prišiel ako veľmi premyslený. V žánri, ktorý sa zdal byť pred časom vyčerpaný, prišlo scenáristicko-režisérske duo bratov Spierigovcov s osviežujúcou inováciou. Zároveň si film po celý čas udržiava veľmi slušné tempo, nie je v ňom ani scéna naviac, a okrem príjemne temnej atmosféry sa môže pochváliť aj hereckým obsadením, ktoré poteší - Ethan Hawke (Training Day, 2001), Sam Neill (Jurský park III, 2001) a samozrejme Willem Dafoe (V tieni upíra, 2000; Spider-man, 2002).   Hodnotenie: ●●●○○ 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Kúpili sme ZOO
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Princ z Perzie:Piesky času
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Robin Hood (2010)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Na hrane temnoty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Súboj titanov (2010)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Kačmár
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Kačmár
            
         
        kacmar.blog.sme.sk (rss)
         
                                     
     
        31-ročný pozerač filmov, čitateľ kníh, počúvač hudby. Tiež syn, kamarát, kolega, zamestananec, a všeličo iné
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    62
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1804
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Recenzie filmov
                        
                     
                                     
                        
                            Filmové všeličo
                        
                     
                                     
                        
                            Off Topic
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Anthony Hopkins odpovedá na otázky študentov
                                     
                                                                             
                                            Zaujimavé interview s Ridleym Scottom a Russellom Croweom
                                     
                                                                             
                                            Roger Ebert - The golden age of movie critics
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jonathan Franzen - Freedom
                                     
                                                                             
                                            Philip Roth - Lidská skvrna
                                     
                                                                             
                                            Pavel Vilikovský - Vlastný životopis zla
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            IMT Smile - Rodina
                                     
                                                                             
                                            Bruce Springsteen - Wrecking Ball
                                     
                                                                             
                                            Bob Seger &amp; Silver Bullet band - Greatest Hits Vol.1
                                     
                                                                             
                                            Ben Lee - Awake Is The New Sleep
                                     
                                                                             
                                            Stephen Kellogg &amp; the Sixers - Glassjaw Boxer
                                     
                                                                             
                                            Simple Minds - Graffiti Soul
                                     
                                                                             
                                            U2 - No Line On A Horizon
                                     
                                                                             
                                            Bryan Ferry - Dylanesque
                                     
                                                                             
                                            Bruce Springsteen - Working On A Dream
                                     
                                                                             
                                            Kings of Leon - Only by the Night
                                     
                                                                             
                                            Jason Mraz - We Sing We Dance We Steal Things
                                     
                                                                             
                                            Last.fm
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Roger Ebert
                                     
                                                                             
                                            Vlado Schwandtner
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Unie filmových distributorů
                                     
                                                                             
                                            Česko-Slovenská filmová databáze
                                     
                                                                             
                                            medialne.sk
                                     
                                                                             
                                            Box Office USA
                                     
                                                                             
                                            Internet Movie Database
                                     
                                                                             
                                            Roger Ebert
                                     
                                                                             
                                            www.movieweb.com
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ohýbaj ma, mamko...
                     
                                                         
                       objatie prostitútky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




