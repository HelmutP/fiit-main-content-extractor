

 Slovenská konvenčnosť nemá hranice takisto ako ľudská hlúposť. Intolerancia voči odlišným ľudským bytostiam je v tomto štáte hlboko zakorenená a je v podstate nepodstatné v ktorej z ľudských charakteristík daná odlišnosť spočíva, či je to vek, pohlavie, sexuálna orientácia, vierovyznanie, farba pleti a podobne. 
 Podmienkou fungovania zdravej, spravodlivej, slušnej a tolerantnej demokratickej spoločnosti je rešpektovanie a uznávanie ľudských práv predstaviteľov minorít a ich integrácia do majoritnej spoločnosti. Slovenský recept v tejto oblasti (narozdiel od vyspelých krajín) však ponúka odlišné, značne primitívne riešenie ~ urážanie, zosmiešňovanie, nadávanie a dehonestácia každého, kto sa odlišuje od noriem, ktoré stanovila väčšina spoločnosti. 
 Typickým predstaviteľom netolerantnej skupiny slovenských homofóbov je jeden banský inžinier zo Žiliny, ktorý svoje názory často prezentuje pod vplyvom alkoholu. O intelektuálnej úrovni tejto ikony slovenskej politickej scény dnes písať nebudem. Primitívne názory, že homosexualita je choroba a „buzeranti“ patria do pekla nie je potrebné komentovať. 
 Problém nenávistných pravicových extrémistov (ktorých považujem za hlavné jadro hnutia proti „dúhovému“ pochodu) je v ich nízkej inteligencii a ešte nižšej vzdelanosti, ale ani o tom dnes nebudem... 
 Podstatou tohto článku je volanie po tolerancii a rovnosti, volanie po slušnej, slobodnej a tolerantnej spoločnosti, v ktorej si budeme všetci rovní, volanie po spoločnosti, v ktorej nebude dôležité kto a s kým... 
 Láska je zmyslom nášho života a naším jediným cieľom. Láska je dôležitejšia ako spoločenské predsudky. Otvorme si oči a buďme tolerantní. 

