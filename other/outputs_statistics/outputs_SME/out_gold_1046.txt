





 
  
 V troskách školy sa kopia kvety, hračky, fotografie detí a fľaše vody, ktoré majú pripomenúť, že rukojemníci v Beslane tri dni nemohli ani piť. Ruská televízia včera ukázala tvár jedného z údajných únoscov. FOTO - REUTERS 
 




 Masaker v Beslane je stále veľkou záhadou. Čo sa presne stalo? Kto začal útok? Kto vlastne školu obsadil? Ani jedna z týchto otázok nemá zatiaľ jasnú odpoveď. Z niekoľkých možných verzií sa však dá poskladať obraz, ako to asi vyzeralo. 
 Každý deň sa objaví nová informácia. Včera spravodajcovia denníka Izvestija napísali, že ako prví začali strieľať otcovia detí - rukojemníkov. Až potom sa vraj ozval výbuch, možno ako reakcia na "divoký útok" zúfalých a samostatne vyzbrojených rodičov. 
 Kto to bol 
 Ruské bezpečnostné jednotky už v nedeľu začali zatýkať v Čečensku. Podľa Federálnej bezpečnostnej služby boli totiž v skupine, ktorá školu obsadila, predovšetkým Čečeni a Inguši. Okrem nich tam vraj boli aj niekoľkí Arabi, jeden Afričan, Oset a Ukrajinec. 
 Pátraniu môže veľmi pomôcť zadržanie jedného z členov skupiny. Ruská televízia ukázala mladého muža hovoriaceho po rusky s kaukazským prízvukom, ktorý údajne aktívne spolupracuje s vyšetrovateľmi a vďaka nemu už identifikovali veľa členov skupiny. 
 "Pri Alahovi, prisahám, že ja som nestrieľal. Hovorili mi, aby som strieľal, ale ja som nestrieľal, len do vzduchu, pri Alahovi, prisahám," opakoval dokola mladík, ktorého sa pri zatýkaní pokúsil policajtom vytrhnúť rozzúrený dav osetských mužov. 
 Komentátor ruskej televízie podotkol, že i keď v Rusku platí moratórium na trest smrti, ak mladíka odsúdia, "nečakajú ho v base ako vraha detí pekné veci". 
 Čo chceli 
 Teroristi požadovali nezávislosť Čečenska, odchod ruskej armády z krajiny a slobodu pre svojich väznených spolubojovníkov. Údajne tieto požiadavky odovzdali písomne exprezidentovi Ingušska Ruslanovi Auševovi, ktorého ako jediného pustili do školy a odovzdali mu 26 rukojemníkov. 
 Aušev zoznam odovzdal operatívnemu štábu a ten ho údajne ihneď zaslal ruskému prezidentovi Vladimirovi Putinovi. Kremeľ dokument zakázal zverejniť. 
 Ako sa pripravovali 
 Už je takmer isté, že banda sa na útok pripravovala dlho, možno niekoľko mesiacov, a že s nimi spolupracoval niekto z miestnej polície. V budove našli toľko zbraní a výbušnín, že ich so sebou nemohli útočníci priviezť. Pravdepodobne ich ukryli v škole v júli, keď tu opravovali robotníci z Čečenska, údajne "iba za kopejky". Do múru telocvične dokonca zamurovali drôty, ktoré neskôr využili na prepojenie jednotlivých bômb. 
 Všetci zo skupiny sa v budove skvele orientovali. Pri rozmiestňovaní mín a bômb sa pridržiavali zjavne dopredu pripraveného plánu, nechali nakoniec iba úzky priechod medzi sediacimi ľuďmi, ktorí by pri pokuse o útek nutne museli spôsobiť rad explózií. 
 Ako sa dostali do Beslanu 
 K budove školy prišli útočníci nákladným autom GAZ-66 a hranicu s Ingušskom do Osetska zrejme prekročili tam, čo miestni nazývajú "zlatý trojuholník". Aj keď sú hlavné trasy strážené, tento nelegálny priechod pri osetskej obci Churikau strážený nie je a spravidla po ňom prevážajú z Čečenska nelegálne dobývanú ropu do južnej oblasti Ruska. Obrovská korupcia medzi miestnou políciou umožňuje, že tade môže prejsť prakticky čokoľvek. 
 Nato, ako vyšla ozbrojená skupina na hlavnú trasu Rostov-Baku, zajala policajta, ktorý stál vedľa svojho služobného auta a telefonoval. Práve tento vynútený policajný sprievod im umožnil bez prehliadok dôjsť až do Beslanu. 
 Ako sa začal útok 
 Stále však nie je jasné, čo spôsobilo začiatok útoku. Buď sa situácia vymkla kontrole náhodou, keď samovoľne vybuchla v škole jedna z bômb, ako tvrdí jedna z rukojemníčok, ktorá videla, že sa odlepila izolepa zo steny a uvoľnila nálož, alebo teroristov naozaj napadli rozzúrení rodičia či zle zorganizovaní vojaci špeciálneho komanda.  
 Robert plače, nechce otvoriť oči a čaká na mamu 
 Šesťročný Robert sa hádže na nemocničnom lôžku, chce utiecť. Počuje a chápe, čo mu hovoria, ale nechce otvoriť oči. Nevie, že jeho matka je už dva dni po smrti, stále ju volá. 
 Kvôli šoku a veľkému stresu bude rovnako ako stovky ďalších detí, ktoré boli tri dni zavreté v beslanskej škole, potrebovať dlhodobú terapiu. 
 O deti na chirurgickom oddelení nemocnice vo Vladikaukaze, kam previezli väčšinu zranených, sa starajú dve detské psychiatričky, Zara Arbijevová a Diana Gulujevová. 
 Pre Roberta to mal byť prvý školský deň, veľký sviatok. Chlapca zachránila matka, keď ho vyhodila z okna. Jeho príbuzní stoja okolo lôžka. Niektorí z nich nedokážu zadržať slzy, keď Robert volá mamičku. Vedia, že už nepríde. 
 Po prvom období rozrušenia, ktoré utlmia sedatíva, bude nervový systém dlhší čas oslabený a Robertovi bude hroziť neuróza. Potom bude nasledovať najmenej ročná terapia, vysvetľuje doktorka Arbijevová. 
 Z psychologického hľadiska hrozí najväčšie nebezpečenstvo deťom, ktoré sú navonok pokojné. Ako štrnásťročný Aslan, ktorý je ešte na infúziách. Nepohne sa ani centimeter a jeho pohľad je naprosto bezvýrazný. 
 Deti, ktoré sa uzavrú, odmietajú akýkoľvek kontakt a upadnú do stavu podobnému autizmu, môžu trpieť dlhodobými následkami, od psychózy až po negativizmus, hovorí doktorka Gulujevová. 
 Liečia sa najskôr sedatívami, potom sa podrobujú dlhodobej psychoterapii. Veľa záleží aj na podpore rodiny. "Veľa detí však stratilo matku, o to dlhšie sa budú liečiť," hovorí Arbijevová. (čtk/afp) 

