
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Putra
                                        &gt;
                Nezaradené
                     
                 Rodina, homosexuáli a katolíci 

        
            
                                    17.5.2010
            o
            19:28
                        (upravené
                17.5.2010
                o
                20:17)
                        |
            Karma článku:
                11.36
            |
            Prečítané 
            2424-krát
                    
         
     
         
             

                 
                    V sobotu 15.mája sa zo svätej omše v kostole na bratislavskom námestí SNP vydal sprievod asi 200 ľudí, ktorí protestovali proti plánovanému pochodu homosexuálov Pride Bratislava 2010. Proti čomu však títo ľudia protestovali? Proti právu vyjadriť svoj názor, alebo mali aj nejaké vecné argumenty proti konkrétnym záležitostiam?
                 

                 Títo ľudia si vzali ako rekvizity svoje deti, ktorým dali do rúk transparenty ako „My chceme ocka aj mamu" a celú akciu pomenovali „Pochod za tradičnú rodinu". Kde však prišli k záveru, že je tradičná rodina ohrozená, alebo že chce deťom niekto ocka a mamu vziať? Celý problém protestov proti homosexuálom a ich zväzkom je výsledkom nekonečného radu dezinformácií, dezinterpretácií a využívania nálad spoločnosti konkrétnymi záujmovými skupinami, najmä z radov cirkvi a časti politického spektra.   Za strašiaka číslo jeden slúžia týmto skupinám adopcie detí homosexuálnymi pármi. Vravia, že aká katastrofa a psychická ujma hrozí tomu dieťaťu atď.. Po prvé, drvivá väčšina homosexuálov v SR o adopcie buď nestojí, alebo je dokonca proti. Je to skrátka strašenie strašiakom v poli, ktorého sa menej inteligentné škorce možno zľaknú a budú chodiť častejšie do kostola, homosexuálom sa vyhýbať (čím strach ešte rastie, lebo najväčší strach mávame z nepoznaného) a budú voliť, správne, KDH a SNS, ktoré obhajujú „tradičnú" rodinu. Keď už by aj na to malo prísť, bude sa mať dieťa lepšie v detskom domove, alebo u homosexuálnych „rodičov"? Odpoveď je asi jasná: v detskom domove nezažije žiadnu rodičovskú lásku, fungovanie bežnej domácnosti spozná len čiastočne, prípadne vôbec nebude pripravené na riešenie vzťahov a konfliktov medzi dospelými .   Strašiak č.2.: Ohrozenie manželstva. Tak toto je podobný nezmysel ako strašenie adopciami. Žiada nebodaj niekto zo zástupcov homosexuálov zakazovať manželstvá heterosexuálom? Požiadavku na to, aby sa homosexuálne zväzky nazývali manželstvami vyslovilo len niekoľko aktivistov združenia QLF bez väčšej podpory minority ako takej. Renomované občianske združenia Ganymedes a Iniciatíva Inakosť, ktoré ako prvé začali presadzovať  uzákonenie Registrovaných či Životných partnerstiev túto iniciatívu nepodporujú a keďže žijem zhruba 6 rokov medzi komunitou, viem, že podstatná časť komunity je vyslovene proti takémuto názvu. Dokonca aj v doteraz predkladanom návrhu zákona o životnom/registrovanom partnerstve sa uvádza, že tieto partnerstvá by mohli uzatvárať len osoby rovnakého pohlavia. Vplyv na klasické manželstvá by sa teda logicky rovnal nule. Ak chce niekto chrániť manželstvá, nech zistí, prečo stúpa rozvodovosť, klesá počet uzatvorených manželstiev a prečo manželské páry váhajú s rodičovstvom. Ale nech príčiny lovia v iných vodách, lebo homosexualita medzi ne nepatrí.   Strašiak č.3.: Náboženstvo. Ak nerátame islam (v Iráne idete za homosexualitu rovno do mučiarne, skadiaľ vás vykúpi popravisko, keď z vás dostanú mená všetkých homosexuálov ktorých poznáte), u nás proti homosexuálom najviac brojí katolícka cirkev. Z čoho pramení ich odpor voči homosexuálom? Homosexualita sa uvádza ako hriech sodomie, čo je ďalší príklad dezinterpretácie. V Starom zákone sa totiž uvádza, že sodomiti po dobytí mesta znásilnili mužov aj ženy, za čo ich mesto, Sodomu, stihol boží hnev. Problém je v tom, že historické obdobie, do ktorého je Starý zákon zasadený je niekde v staršom staroveku, v čase prvých mestských štátov na blízkom a strednom východe a v tej dobe bolo znásilnenie protivníka po porážke vnímané ako akt poníženia, bez ohľadu na pohlavie. Len historická neskúsenosť katolíckych vykladačov Biblie z toho urobila hriech homosexuality. Po tom, čo katolícka cirkev na prelome prvého a druhého tisícročia prestala(!) udeľovať sviatosť manželstva homosexuálnym párom, potrebovala si uchovať monopol na sviatosti a spásu. Medzi ich hlavné sviatosti pochopiteľne patril krst, manželstvo a sväté prijímania, ktoré by však homosexuálne dvojice nepotrebovali, čím hrozilo, že istú, hoci malú časť populácie by nemali pod kontrolou, čo nemohli pripustiť, tak z homosexuality urobili hriech najhrubšieho zrna, podobne ako postupovali pri celibáte pre klérus, aby sa majetok klerikov nedelil medzi potomkov, prípadne manželky, ale aby zostal celý cirkvi.   Tak vážení, koho a pred čím strašíte? Ale kľúčové je tu ešte niečo... čo vlastne chcete dosiahnuť?   Ako by asi mala vyzerať táto „ochrana tradičnej rodiny" podľa odporcov homosexuálnych zväzkov? Asi chcú zákon, ktorý zakáže prijať zákon, ktorý ešte ani neexistuje. Absurdné, ale je to asi to prvé, čo ma napadne. Aha, najlepšie by bol zákon ústavný, aby sa nedal prelomiť tak ľahko. Ešte absurdnejšie? Áno, ale o kúsok vyššie som rozpísal absurdnosť ich argumentov, takže je to podľa nich asi úplne normálny postup.   Často nás strašia tým, že registrované partnerstvá, ktorých prvoradou úlohou je upraviť majetkoprávne vzťahy rovnakopohlavných párov, sú otvorením akejsi Pandorinej skrinky, že nevyhnutne budú nasledovať manželstvá, adopcie a podľa niektorých radikálov Slotovho formátu to bude pokračovať legalizáciou polygamie, pedofílie a iných deviácií, ktoré nemajú s homosexualitou absolútne nič spoločné.   Skúsme si to v obrátenom garde:   Prešiel by (možno ústavný) „zákon na ochranu tradičnej rodiny", ktorý by zamedzoval prijatiu registrovaných partnerstiev. Čo by mohlo nasledovať:  Zákon, ktorý zakáže informovať o homosexualite najskôr v tlačenej, neskôr aj verbálnej forme. Nie, nerobím si srandu. Takáto volovina prešla minulý rok v Litve, a síce obsahuje formuláciu, že sa to týka len prejavov(písomných aj verbálnych), ktoré sú prístupné deťom a mládeži, lebo ide o „mládeži škodlivé témy", ukážte mi médium, ku ktorému sa normálny 16-17 ročný študent strednej školy nedostane. Výsledkom by bolo zhabávanie periodík, šikana médií, záznamy v registri trestov u ľudí, ktorí by nemali ešte ani len maturitu atď.. Podotýkam, že Litva je členskou krajinou EÚ, ale tá je vďaka svojej neohrabanosti a byrokracii neschopná adekvátne a včas zasiahnuť, čo však znamená, že niekoľko populačných ročníkov môže byť trvalo poškodených touto katolíckou demagógiou v najkritickejšom období svojho života.   Nestačí? Vraví sa, že keď sa v jeden deň pália knihy, treba utekať, lebo na ten ďalší sa budú páliť ľudia. Znie to prehnane, ale viem si celkom reálne predstaviť, že ak máme na Slovensku psychiatrov, ktorí tvrdia, že vedia liečiť homosexualitu (a zúfalí rodičia detí, ktorí nemajú dosť informácií im to zbaštia bez ohľadu na následky), tak dokážu pri konštelácii moci, ktorá by odklepla vyššie spomínané nezmysly, zalobovať a vrátiť homosexualitu do zoznamu chorôb. A bolo by im úplne jedno, že WHO homosexualitu vyškrtla zo zoznamu diagnóz pred 20 rokmi. Hlavné je, že oni by mali plné ambulancie, pripadne celé psychiatrické oddelenia (7% populácie naozaj nie je málo), nedobrovoľné platby poisťovní a spokojných polovzdelaných xnenofóbnych voličov pre svojich patrónov na hradnom vrchu.   Vážení protestujúci spasitelia rodiny: Ak nebojujete za vyššie uvedené nezmysly, tak si už raz dajte vysvetliť, že Zákon o životnom partnerstve neznamená nič viac a nič menej ako právnu úpravu  najmä majetkoprávnych vzťahov v spoločnom živote dvoch osôb rovnakého pohlavia. Ide konkrétne o právo vzájomne po sebe dediť, právo na vdovský/vdovecký dôchodok, informácie o zdravotnom stave partnera, vzájomná vyživovacia povinnosť partnerov, možnosť bezpodielového vlastníctva alebo možnosť dostať u lekára voľno na ošetrovanie člena rodiny (známa OČR-ka), či daňové úľavy obdobné ako pri manželstve, teda že sa základ dane vypočítava z príjmov oboch partnerov. Dnes by sa to dalo rozšíriť aj o mladomanželské pôžičky, keďže zo skúseností mojich známych viem, že dvaja mladí homosexuáli, ktorí v nejednom prípade odídu do Bratislavy pred nepochopením a intoleranciou vo svojich rodinách a regiónoch, majú prakticky rovnaký problém získať spoločné bývanie ako mladomanželský pár zakladajúci rodinu. Povedzte mi prosím, čím to ohrozuje tradičnú rodinu, výchovu detí alebo manželstvá ako také.       Ondrej Putra - predseda OZ Ganymedes - Hnutie za rovnoprávnosť homosexuálnych občanov SR   Marián Vojtek - dlhoročný predseda OZ Ganymedes a v súčasnosti jeho čestný predseda 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (141)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            „ROH už nie je čo bývalo“ alebo „Ako úplatné odbory Gorila zožrala“
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Sú skutočne protinožcami?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            „Rómska otázka“ nám podpaľuje podprdelník
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Nie som fašista ani Kotlebovec!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ondrej Putra 
                                        
                                            Naštartujme štartovacie bývanie!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Putra
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Putra
            
         
        putra.blog.sme.sk (rss)
         
                                     
     
        Presvedčený pravicový liberál s úctou k životnému prostrediu. Som predseda OZ Hnutie Ganymedes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2162
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Oznam riaditeľa školy
                     
                                                         
                       Od neúspešného štrajku k štrajkovému prasiatku
                     
                                                         
                       My všetci sme prehrali
                     
                                                         
                       Rozdať si to s Robertom Ficom
                     
                                                         
                       Družstevná Malacky - ako Reportéri zavádzali verejnosť.
                     
                                                         
                       Kým sa pokonajú
                     
                                                         
                       Asistent z Gorily sa stal majetným
                     
                                                         
                       Vyšetrovanie Gorily – výnimka alebo pravidlo?
                     
                                                         
                       „ROH už nie je čo bývalo“ alebo „Ako úplatné odbory Gorila zožrala“
                     
                                                         
                       O tom, ako úradníkov v Bruseli zneužívajú lobisti zo Slovenska
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




