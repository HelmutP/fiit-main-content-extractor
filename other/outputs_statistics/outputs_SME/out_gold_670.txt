

 Tíško tu ležia  skrehnutí na snehu  tak ako milenci  čakajú na nehu  Ich zlomené krídla  chvejú sa strachom  pred ľudmi pred dušou  zapadlou prachom  Tou čo nič necíti  nezúfa nekričí  už dávno neprahne  netúži po žití  V tých dušiach prázdnych  smútok a chlad  už dávno zabudli   čo je mať rád  Pár krídiel zlomených   Kto im ich pofúka  Niekto kto zahojí  odnesie na rukách  Skúsi prísť na miesta  do tepla, bezpečia  kde zlo a nenávisť   nikomu nesvedčia  Tam kde si skrývam  tie tiché poklady  Kde jedno srdce 
 to druhé nezradí         

