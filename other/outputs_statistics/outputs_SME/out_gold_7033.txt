

 Andrej bol tvrďas. Len tak si nepripustil kadekoho k telu a dokonca mal pocit, že si túto vlastnosť zámerne pestoval. Aj ľudia ho tak vnímali. Len ona... Len ona vedela, že vie byť aj iný. Nežný. Že vie byť blázon a že má slabé miesta. 
 Jedného dňa si ju posadil na kolená a len tak jej hľadel do očí. Nič nemusel hovoriť. Ona vedela... Poznala chvíle, keď mu zmäkli vrásky a bol pred ňou ako dieťa. Videla ho pri robote, keď rúbal drevo a leskol sa od potu. Keď sa zahľadel na oblohu a povedal: „Príde búrka." Videla ho v horúčavách. Napil sa z krčahu, ktorý mu doniesla, a potom jej pritisol svoje mokré pery na čelo. 
 Občas ale potreboval byť sám. Ako aj teraz, keď celý čas pršalo. Akoby si chcel zoradiť myšlienky a pozhovárať sa iba so životom. Túto trinástu komnatu sa jej nikdy nepodarilo otvoriť. A vlastne to ani nechcela. Veď každý má právo na svoje. 
 Toto všetko mu v sekunde prebehlo mysľou. Precitol, keď započul jej hlas. Bežala dole závozom a kývala mu na privítanie. Vyšiel jej oproti. Stúpal po kameňoch, ktoré vyčnievali z mokrej zeme. 
 Vbehla mu do narúčia. Rovno na moste. Bol šťastný, že ju má pri sebe. Uvedomil si, že ten starý most, ktorý postavil ešte jeho starý otec, vydržal. Nezobrala ho ani tá storočná voda, na ktorú hľadel spoza okna len pred tromi dňami. „Ďakujem," povedal a ona presne vedela, čo všetko v sebe skrýva to jednoduché slovo. Zobral ju do náručia a vôbec nedbal na kamene, čo vytŕčali z blatistého dvora. Hľadel na ňu. A ona mu rapotala o tom, že mu doniesla makovník a že dá do vázy kvety, lebo konečne vyšlo slnko... 
   
   
   
   
   
   
   
   

