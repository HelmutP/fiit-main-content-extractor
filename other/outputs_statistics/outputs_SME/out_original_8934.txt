
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Gogola
                                        &gt;
                Ľudia a lesy
                     
                 Otvorený list budúcemu ministrovi pôdohospodárstva 

        
            
                                    16.6.2010
            o
            2:34
                        (upravené
                16.6.2010
                o
                3:48)
                        |
            Karma článku:
                12.34
            |
            Prečítané 
            2844-krát
                    
         
     
         
             

                 
                    Vážený pán minister, dovoľujem si Vás osloviť ako člen občianskej iniciatívy ĽUDIA PRE LESY. Čoskoro uplynie rok od chvíle, čo hŕstka zamestnancov, jedným z Vašich predchodcov eufemisticky označená ako „skupinka proti neporiadku", podala trestné oznámenie vo veci nehospodárneho nakladania s majetkom štátu. Naše podozrenie, hraničiace s istotou, že štátne lesy sú tunelované, sme spísali do 27 bodov. Dôsledkom trestného oznámenia bol pokyn premiéra Fica vymeniť vedenie štátneho podniku LESY SR. Ako sa ukázalo, presne v duchu príslovia „psy štekajú, karavána ide ďalej". Nič sa touto zmenou nevyriešilo. Všetci kompetentní, ktorí niesli a stále nesú zodpovednosť za riadenie štátneho podniku LESY SR, sa jej vehementne zbavili. Generálny riaditeľ, vedenie podniku, dozorná rada, ale i generálny riaditeľ Sekcie lesníckej na Ministerstve pôdohospodárstva SR alibisticky tvrdia, že kým má veci v rukách vyšetrovateľ, nemôžu a nechcú do ničoho zasahovať. Takže jediné, do čoho sa nebáli zasiahnuť, bola naša „skupinka proti neporiadku". Skončili sme na ulici.
                 

                 Vážený pán minister,   nepíšem Vám preto, aby som si sťažoval. Stále čakáme odpoveď na otázku, na ktorú Váš predchodca nemohol, nechcel alebo nevedel odpovedať: či sme konali správne, alebo nie. Ak bolo poukázanie na šafárenie v štátnych lesoch skutkom, ktorý protirečí dobrým mravom, odsúdeniahodným činom, protizákonným aktom alebo „bomzáctvom", ktoré poškodilo dobré meno štátneho podniku LESY SR, tak Vás žiadam, aby ste naše konanie jasne a zreteľne odsúdili.   Ale ak sa domnievate, že naše konanie bolo správne, že to bol prejav zamestnaneckej lojality, občianskej statočnosti alebo skutok v súlade so svedomím, POVEDZTE TO NAHLAS. Nie kvôli nám. Kvôli všetkým slušným lesníkom. Kvôli ľuďom, ktorí potrebujú vedieť, či sa oplatí „nasadzovať krk" za veci verejné. Lebo LESY sú vecou VEREJNOU.   Vopred Vám ďakujem za odpoveď. Budem na ňu čakať so zatajeným dychom.                                                S úctou                                                                                       Peter Gogola       V Banskej Bystrici 16.6.2010 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Referendum–dum
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Bubeníkove osudy III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Stopárov sprievodca Banskou Bystricou (1. časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Včera som stretol dôchodcu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Gogola 
                                        
                                            Nežná zelená
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Gogola
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Gogola
            
         
        petergogola.blog.sme.sk (rss)
         
                                     
     
        Komárňan, domestifikovaný v Banskej Bystrici. Milovník prírody a dobrého vína. Fanúšik rockovej hudby, bubnov, bežiek a Monthyho Pythona.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Banská Bystrica
                        
                     
                                     
                        
                            Ľudia a lesy
                        
                     
                                     
                        
                            Zbrane
                        
                     
                                     
                        
                            Bubny a bubeníci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




