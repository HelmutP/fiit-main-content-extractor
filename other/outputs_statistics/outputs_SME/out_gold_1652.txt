

 Možno to robíte každé ráno. Pozriete sa von oknom čo majú oblečené ostatní, vyjdete na balkón aby ste zistili aké počasie je za oknami a pozriete sa na teplomer na okennom ráme. I tak možno cestou do práce a z práce zmoknete, alebo zistíte že ste sa obliekli viac ako treba. O výletoch zmarených zlým počasím ani nehovoriac. Viete o tom že domáca meteorologická stanica dnes už nie je finančne nedostupným doplnkom a je užitočnejšia ako by ste si myslieli? 


 FOTO - MILAN GIGEL


 Meteorologická stanica WS680HLR je domácim informačným centrom, ktoré vám prezradí nielen aktuálne informácie týkajúce sa času, ale i počasia a fázy mesiaca. Panelové zariadenie, ktorému dominuje majestátny segmentový displej je navrhnuté tak, aby ste dokázali informácie prečítať i s väčšej vzdialenosti a to i bez toho, aby bolo potrebné preklikávať sa tlačidlami k želaným informáciám. Ak by sme chceli v krátkosti charakterizovať o čo vlastne ide, mohli by sme povedať že ide o inteligentnú kombináciu dvojice hodín riadených signálom DCF-77 vybavené dvojicou budíkov, zabudovaným teplomerom, senzorom vlhkosti a meračom atmosferického tlaku, s možnosťou bezdrôtovej komunikácie s externými snezorovými jednotkami. Variabilita montáže a podsvietenie dipsleja sú samozrejmosťou. 


 FOTO - MILAN GIGEL


 Centrálna jednotka poskytuje používateľovi dve možnosti umiestnenia v interiéri. Prvou z nich je použitie dodávaného trojramenného plastového stojana, ktorý sa zasúva do otvoru v zadnej časti jednotk.y umiestnenie na pracovnom stole nie je prekážkou v prenášaní medzi pracovňou, nočným stolíkom či kuchyňou. Keďže napájanie zabezpečuje dvojica alkalických monočlánkov, v pohybe nebránia žiadne káble a rozmery s hmotnosťou zodpovedajú požiadavkám na mobilitu. Druhou alternatívou je využitie závesného bodu pre skrutku, čo umožňuje tradičné zavesenie na stenu.  Vzhľadom na displej rozmerov 8,5×6 centimetrov sa však asi väčšina používateľov rozhodne pre použitie stojančeka. Nástenná montáž má opodstatnenie pri uhladenom nečlenitom teréne, alebo napríklad pri jachte, kde je každý priestor vzácny. 


 FOTO - MILAN GIGEL


 Z pohľadu dizajnu a vyhotovenia možno povedať, že ide o zariadenie nesúce sa v súlade s dnešnými trendami a požiadavkami používateľov. Zaujímavé tvarové vyhotovenie s atraktívne členeným povrchom dopĺňa metalická povrchová úprava, ladiaca kombinácia farieb a hlavne tvarová účelnosť, ktorá vám poskytne pevný úchop z každej strany. Z produktu cítiť viac invenčnosť západného trhu ako gýčovité dizajnérske sklony masovo produkujúceho východu. Základňa tak pôsobí vznešeným a finančne náročným dojmom z nablýskaného katalógu. V rovnakom tóne sa však nesie i samotná funkcionalita. V dokonalej obálke sa skrýva i zaujímavé vnútro. 


 FOTO - MILAN GIGEL


 Hlavným ovládacím prvkom je štvorica mikrospínačových tlačidiel s textovými popiskami, ktorá je umiestnená pod spodnou časťou rámu LCD displeja. Chod tlačidiel je plynulý so znateľným vzopnutím a vzhľadom na odpor ktoré kladú pri manipulácii sa dá predpokladať, že aj ich trvácnosť bude dostatočná. I keď sa po zoznámení so systémom taker zaobídete i bez manipulácie s tlačidlami, zdá sa že povrchová úprava uskutočnená nástrekom plastu je dostatočne fixovaná, aby s a pri častom fyzickom kontakte s prstami neznehodnotila. Funkcionalita tlačidiel je kombinovaná a tak v každom pracovnom režime sa ich význam mení. Keďže je k systému dodávaná i podrobná lokalizovaná dokumentácia, ich význam zistíte úplne jednoducho. Ak patríte medzi zručnejších používateľov elektroniky, intuícia vás posunie vpred i bez návodu. 


 FOTO - MILAN GIGEL


 Druhým ovládacím prvkom je rozmerné tlačidlo s membránovým spínaním, ktoré je umiestnené nad LCD displejom. To slúži pre prepínanie režimu zobrazenia časových údajov, prístup k nastaveniam, avšak hlavne pre vypnutie dvojice nastaviteľných budíkov. To ocenia hlavne tí, ktorí sa bez rozmyslu nechali nalákať na novú generáciu budíkov, ktoré vrchný vypínač nahradili posuvným prepínačom umiestneným na zadnom paneli. Ako je teda zrejmé, výrobcovi pri návrhu záležalo i na tom, aby bolo ovládanie čo najjednoduchšie. 


 FOTO - MILAN GIGEL


 Keď hovoríme o budíku, s nocou sa spája samozrejme i potreba osvetlenia. To zabezpečuje štvorica modrých LEDiek umiestnená na stranách LCD panelu. I keď je úroveň podsvietenia dostatočná, svetlo je do istej miery nerovnomerne rozložené (digitálna fotografia tento defekt zosilňuje, v reále je to menej badateľné). Fóliová distribúcia tradičným podsvecovaním by bola zaručene dobrým vylepšením. Ak sa však zamyslíte nad tým, že nočné používanie je iba zlomkom všetkých prípadov, takýto detail možno oželiete. 


 FOTO - MILAN GIGEL


 Všestrannosť meteorologických staníc tejto triedy však spočíva v tom, že dokážu zbierať údaje z viacerých senzorov, aby ich merania prinášali všestrannejšie výsledky a vyššiu presnosť. Väčšina bytov je otočená tak, že máte prístup k čelnej i zadnej strane panelového domu, pričom jedna z nich je slnečná a druhá naopak v tieni. Ak si umiestnite senzorové stanice na obe strany, získate lepší prehľad o situácii vonku. Komunikácia prebieha vo frekvenčom pásme 433 MHz a priemerný dosah bez panelových prekážok dosahuje 40 metrov. Celkovo je možné základňu spárovať s trojicou senzorových modulov, z ktorých každý monitoruje vlhkosť vzduchu a teplotu okolitého prostredia. Niektoré z nich je možné vybaviť i tepelnou sondou, ktorú možno umiestniť do vody, či do pôdy. V tomto prípade je súčasťou balenia senzor WT260H v kompaktnom vyhotovení umožňujúcom pohodlnú montáž. LCD displej vám sprístupní informáciu i vizuálne, takže sa môžete presvedčiť o správnosti jeho funkcionality. 


 FOTO - MILAN GIGEL


 Na rozdiel od základne, ktorú poháňa dvojica aklalických monočlánkov formátu AAA, v tomto prípade vystačíte s ich menšou AAA verziou. Energetická náročnosť je vďaka úspornému režimu dátovej komunikácie a iba základnému zobrazovaniu pomerne malá, takže sa nemusíte báť, že by bolo potrebné batérie meniť neustále. 


 FOTO - MILAN GIGEL


 Senzorový modul na segmentovom LCD displeji cykluje informáciu o teplote a vlhkosti. Teplotný rozsah pokrýva rozmedzie od - 30°C do +70°C, treba však mať na vedomí, že všetko v mnohom závisí i na druhu použitých batérií. Čím lacnejšie a obyčajnejšie, tým menej toho znesú. Vzdušná vlhkosť je monitorovaná v rozmedzí od 15 do 95%. V prípade teplôt mimo rámca sa zobrazí informácia o jeho prekročení. Pri testoch sa jeho funkcionalita preukázala i pri použití v chladničke a mrazničke. V kombinácii s možnosťou nastavenia limitných teplôt na základni tak môžete získať akustické upozornenie ak teplota minie preddefinovaný rozsah. Je iba na vás, kam si senzory umiestnite a čo od nich očakávate. 


 FOTO - MILAN GIGEL


 Rádiovú jednotku senzoru môžete pomocou dvojice tlačidiel "naladiť" na požadovanú frekvenciu. Ak máte senzor iba jeden, nemusíte sa o nič starať, so základňou sa spárujú akosi samé bez vašej pomoci. V prípade násobných inštalácií je nastavovanie pravdepodobne nevyhnutné. Tlačidlá sú jednoducho prístupné a vyhotovené sú tak, aby zamedzili vnikaniu vlhkosti k elektronike. Celé vyhotovenie je "šplechotesné" a zaisťuje spoľahlivý chod v exteriéroch. 


 FOTO - MILAN GIGEL


 Meteorologická základňa vám v rámci možností zaistí presný chod zabudovanej dvojice hodín. Súčasťou riadiaceho čipu je prijímací modul signálu DCF-77, ktorý zaistí ich presné nastavenie. Podmienkou však je, aby ste mali dostatočný signál pre príjem, čo by v našich končinách nemalo byť problémom. Zabudovaná je trojica senzorov. Teplotný má citlivosť od -9,9°C do 50°C, mečač vzdušnej vlhkosti obslúži roszah od 25 do 95% a zákonite nesmie chýbať ani senzor atmosferického tlaku. Čo sa týka presnosti meraní, pri teplote je tolerancia +/- 1°C, pri ostatných veličinách je presnosť ešte vyššia. Zvyšok je otázkou inteligencie procesora a vyhodnotenia údajov zozbieraných v časovom rade.  Pomocou dát pokrývajúcich posledných 24 hodín systém zostaví predpoveď počasia a prehľadne zobrazí na displeji aktuálnu situáciu a trendy. 


 FOTO - MILAN GIGEL


 Pre zobrazenie informácií o počasí sa používa grafická symbolika a je potrebné mať na pamäti, že ide o vyhodnotenie tendenčných údajov vopred. Ak za oknami praží slnko a na displeji je dážď, neznamená to poruchu ale prichádzajúcu zmenu počasia zo zrážkami. Údaje sú s pomerne značnou úspešnosťou v odhade zobrazované približne s 8-12 hodinovým predstihom. Viete teda či náhodou nesp=rchne a či má cenu vyvesiť von na sušiak bielizeň alebo ísť na výlet do lesa. Symboly jú pomerne jasné a zodpovedajú zaužívaným značkám. Tie sú navyše animované. 


 FOTO - MILAN GIGEL


 Informácia o čase je vďaka kalendáru a jednoduchej aritmetike doplnená i o zobrazenie aktuálnej fázy mesiaca. Možno vás tento údaj ničím neočarí, avšak ak máte spálňové okno situované tak, že vám cez žalúžie prekúka mesiac priamo do zaspávajúcich očí, viete vopred na čo sa máte tešiť. 


 FOTO - MILAN GIGEL


 Vo vrchnej časdti displeja sa zobrazuje i aktuálny časový údaj jedných z dvoch zabudovaných hodín, dvojicu nastavených budíkov, dátum (všetky hodnoty pri prepínaní), symbol dostupnosti signálu DCF-77 a aktuálny atmosferický tlak. Čoskoro zistíte že predsa len tie "hektopaskaly" čosi znamenajú a sami odhadnete kedy znamená číselná hodnota tlakovú níž alebo výš. Samotnému číslu však pozornosť venovať nemusíte, v spodnej časti displeja sa nachádzajú preibežné grafy hodnôť a tak je všekto názornejšie ako by ste si mysleli. 


 FOTO - MILAN GIGEL


 V strednej časti sú cyklované informácie o vlhkosti vzduchu a teplote, a to vo vnútri a pri jednotlivých senzorových jednotkách. Čo je na zobrazovaní skvelé je skutočnosť, že nechýba ani zobrazenie tendenčnej zmeny. Symboly so šipkou naznačia či je hodnota vyrovnaná, má tendenciu stúpať alebo klesať. 


  



 FOTO - MILAN GIGEL


 K monitorovaným teplotám je možné zadať i rozsah spodnej a vrchnej kritickej hodnoty. To vás upozorní napríklad na to, aby ste na fóliovníku otvorili okno alebo šli vyvetrať dielňu či kuchyňu. Hodnoty sú nastavované numericky ako alarmové. 


 FOTO - MILAN GIGEL


 Veľkým prínosom je grafické sledovanie trendov a priebehu meraných veličín v posledných 24 hodinách. Priebeh teploty v interiéri i na vonkajších senzoroch prezradí čosi o teplených výkyvoch počas dňa. Pri exteriéri získate prehľad o častiach dňa kedy sa nachádzate vo vnútri a budete vedieť i spätne povedať, aká bola teplota včera naobed alebo v noci. Pri interiéri viete lepšie nastaviť klimatizáciu či vykurovanie. Najzaujímavejšie sú však priebehy atmosferického tlaku, ktoré vám prezradia, že prichádza čas ďažďov alebo naopak približuje sa vyjasňovanie. Maximálne a minimálne hodnoty jednotlivých veličín možno však stlačením jediného tlačidla prečítať i pri numerickom strednom bloku. 


  



 FOTO - MILAN GIGEL


 Celkovo je možné tento malý domáci meteorologický systém hodnotiť ako veľmi účelného a praktického pomocníka pre tých, ktorí žijú aktívnym dynamickým životom a radi trávia svoj voľný čas i mimo panelových múrov. Hodnotné použitie je i na chatách a v rodinných domoch, kde sa rôzne činnosti spájajú s nárokmi na počasie. Ak uvážite že systém v plnej miere nahradí i budík nastavovaný DCF signálom a kombinovaný interný/externý teplomer, pomer cena/výkon je skutočne na veľmi dobrej úrovni. Ak túžite po informovanosti a máte radi techniku, potom budete so zariadením skutočne spokojní. Jediné čo vám možno bude chýbať je možnosť pripojenia k PC pre ďalšie spracovanie údajov v počítači - ale to už by sme sa dostali k úplne inej triede zariadení. 

 Dostupnosť: http://www.emos.cz, http://www.emos.sk 
Orientačná cena: 1950 Kč s DPH 

