

  Deň štvrtý - kroměřížsky 

 Zatiaľ balíček záchrany nepotrebujem, je opäť nádherný deň a my ideme spoznávať ďalšie krásy Moravy. Našim dnešným celodenným programom je Kroměříž. 
 

 Znám já zahrádečku, roste v ní fialka 
[:zalévá ju švarné dívča, moja frajárka:] (moravská ľudová)  

 Prvou zastávkou je Květná záhrada, ktorá je zapísaná na zozname svetových kultúrnych a prírodných pamiatok UNESCO. Jej základy aj s kolonádou vznikli už v 17. storočí,  o dvesto rokov neskôr bola rozšírená. Ako si môžete všimnúť na fotkách, je typická svojou symetriou geometrických tvarov a kvetinovými ornamentami.  


 Fotila som to z terasy tejto kolonády, ktorú zdobí 44 sôch antických hrdinov a cisárov.  

 Ešte jedno súsošie. 


 Za fontánou vidieť rotundu. Plagát s obrýlenými ľudskými tvárami pri vchode pozýval na výstavu Orbis Pictus, ktorá nás spolu s vysokým vstupným akosi neoslovila, a tak sme dovnútra nevstúpili. Mali sme iné priority - tešili sme sa  na slávne Foucaultovo kyvadlo, ktoré dokazuje otáčanie Zeme okolo vlastnej osi.  Preto sme sa naň neskôr počas prehliadky zámku opýtali sprievodkyne. Možno teraz tušíte - áno, kyvadlo je presne v tejto rotunde. Už sme sa tam nevrátili.  
Asi si budem musieť za trest prečítať Eca... 


 Návštevou palmária prehliadku Květnej záhrady ukončujeme. 



 Na tých panských lúkách, našel jsem já dukát, 
kdo mně ho promění, milá doma není. (moravská ľudová)  

 A vrháme sa na ďalšie ušľachtilé ciele. Prezrieme si arcibiskupskú mincovňu, ktorá mňa osobne nejako zvlášť nezaujala, pretože  nie som moc na prachy. Navyše neskôr zistíme, že patrí k arcibiskupskému zámku a platia tu rovnaké vstupenky, takže tie, ktoré máme, sme kúpili zbytočne. Ale viem si predstaviť, že skutoční znalci krásu toliarov ocenia.  


 Kdo vínko pije, to vínko bílé, ten je veselý, 
kdo vínko chutná, frajárko smutná, ten Ťa potěší. (moravská ľudová) 

 Zato prehliadka arcibiskupských vínnych pivníc bola zaujímavá dosť. Nachádzajú sa šesť metrov pod zemou, preto je tu príjemný chládok - teplota sa tu pohybuje od 9°C do 11°C. A veľmi pekne to tu vonia. 

 Založil ich biskup Bruno v 13. storočí. Toto je on, teda jeho socha, a dole za ním sú uložené zásoby. 



 Pijme vínečko, dobré, dobré je, a kdo že nám ho, naleje, naleje, 
[:naleje nám ho, svatý Jan, svatý Jan, požehná nám ho, Kristus Pán, Kristus Pán.:] 
My sme vínečko, píjali, píjali, Pannu Mariu, vzývali, vzývali, 
[:Pannu Mariu, Žarošskú, Žarošskú, našu patronku, slováckú, slováckú.:](moravská ľudová)  

 Od roku 1345 sa tu vyrába  omšové víno, toto právo udelil pivniciam kráľ Karel IV. Najstarší sud je zo začiatku 19. storočia.  

  Toto je zas najväčší sud, ktorý tam majú. Je z dubového dreva a má objem 19 220 litrov. Keby z neho človek pil liter denne, mal by čo piť  vyše 52 rokov (pokiaľ by ho medzitým nesklátila cirhóza). Ja by som ho už asi nestihla vyprázdniť ani bez cirhózy, jedine že by som si zvýšila dennú dávku. 
Najväčší sud v Českej republike sa nachádza v Mikulove  a je ešte 5-x väčší. 
Ak podržíte nad obrázkom kurzor myši, uvidíte, aký veľký je sud v porovnaní s človekom. Otvorom v spodnej časti   sa sud čistí. 


 Fotky nie sú príliš kvalitné, v pivniciach bola poriadna tma. Steny sú pokryté čiernou ušľachtilou plesňou, ktorá pomáha kontrolovať stabilnú teplotu a vlhkosť potrebnú pre dozrievanie vína. 





 Táto pivnica  hrala dôležitú úlohu aj v nedávnej minulosti - práve tu vraj Mečiar s Klausom pri rozhovoroch o delení Československa prekonávali rozdielne stanoviská. 

 Vyliezame z podzemia, aby sme sa vrhli na ďalšiu kultúru a históriu. Po hodinovej prehliadke Arcibiskupského zámku ideme do Podzámeckých záhrad. Oba tieto objekty  sú tiež zapísané na zozname svetových kultúrnych a prírodných pamiatok UNESCO. 




 Když jsem já šel kolem panskej zahrady 
   zavoňal mě rozmarýnek zelený. (moravská ľudová)  




 Táto záhrada sa mi páčila ešte viac ako Květná. Je to vlastne akýsi park, bezplatne dostupná odpočinková zóna s množstvom vodných plôch,  rôznych stavieb a sôch a starých dôstojných stromov. 










 A všade množstvo lavičiek, veveričiek, kačičiek, srniek a všeličoho iného. 





 V záhrade sa nachádza aj kútik živej prírody, ktorý slúži ako rehabilitačné stredisko pre zranené a choré zvieratá. Veru, táto pávica moc zdravo nevyzerala. Hneď mi napadlo pár písmenok a čísiel - H5N1. 

 Ani  tento  sa netváril veľmi šťastne. Ale možno sa len nudil a robiť opičky alebo vrieskať ako pavián ho už nebavilo. 



 Aby som nezabudla, aj mesto sme si pozreli.  Vraj má titul "Najkrajšie mesto ČR v roku 1997". Neviem, vtedy som tu nebola. Ale ani po desiatich rokoch nevyzerá zle.  Námestie s podlubím aj bez neho. 










 Gotický kostol sv. Mórica. Vybudovať ho dal náš starý známy z vínnych pivníc, biskup Bruno. 


 Barokový kostol sv. Jána Krstiteľa na Masarykovom námestí - socha prvého prezidenta ČSR je v spodnej časti obrázka, niekde tam v strede medzi dopravnými značkami. 



 Rodný dom maliara Maxa Švabinského -  spoluautora prvých československých bankoviek. 




 A už sme z toľkej krásy unavení. Aby som neunavila aj vás, s Kroměřížom, ktorý nazývajú aj "Hanáckymi Aténami", sa radšej rozlúčime.  

 V penzióne si potom prezeráme hŕbu materiálu, ktorý sme dnes nazbierali, a zisťujeme, že autori propagačných letáčikov neklamú. Všetko v skutočnosti vyzerá presne tak, ako na fotkách. Dokonca aj moje fotky. 

 Nezanedbávam ani vzdelávanie. Plzáka som pochopila takto - čokoľvek dobré urobím pre niekoho iného, robím to v konečnom dôsledku kvôli sebe, pretože tým získavam emočný zisk. Takže som vlastne obyčajný hnusný sebec a teraz rozmýšľam, či nemám radšej uspokojovať svoje emočné potreby iným spôsobom. Nie že by som mala zrovna chuť trhať muchám krídla, ale zas by som nebola taký egoista...  
Aj keď je dosť pravdepodobné, že som to nepochopila správne a vôbec nie som sebecká, iba blbá. 
 Deň piaty - mokrý 

 Vonku prší, tak tomu prispôsobujeme svoj program. Schovať sa môžeme napríklad v jaskyni. Nasadáme do auta a ideme smer Teplice nad Bečvou. 

 Okolo mlýna, červená hlína, 
   [: daj mně mlynárko, daj mně mlynárko svojeho syna.:] (moravská ľudová)  

 Cestou sa zastavujeme v Skaličke, kde sa nachádza Červekov veterný mlyn. Bol postavený začiatkom 19. storočia v Dřevohosticiach a do Skaličky prevezený v roku 1850 na 12 konských povozoch. Je vysoký 11 metrov a do dneška je prevádzkyschopný. 


 A už sme v Zbrašovských aragonitových jaskyniach. Je to tam ako v každej podobnej jaskyni, akurát teplejšie - sú to najteplejšie jaskyne v ČR, teplota sa tu pohybuje okolo 14°C:


 
 Tento stalagmit sa volá sv. Antoníček. Je jediný, na ktorý si môžeme siahnuť. Ak si pri tom budeme niečo priať, vyplní sa nám to. Stihla som ho len odfotiť, na osahávanie a prianie neostal čas. 



 Z jaskyne ideme pol hodiny peši k najhlbšej priepasti v ČR - k Hranickej priepasti. Predpokladaná hĺbka je 700 - 800 metrov, zatiaľ potvrdená je 274 metrov. Nad hladinou je 69,5 m, zvyšok je pod vodou. 



 Pokračujeme autom do obce Bělotín, kde sa nachádza pamätník európskeho rozvodia.   



 Vnútri pamätníka sú slová básnika Jána Skácela: 
"Podivuhodné místo, kde se rozcházejí vody a kde se rozděluje déšť. Potoky spějí odtud na sever a na jih.  
Ta voda se už nikdy více nepotká a nenávratné býva loučení lidí a vod.  

Stojíte na předělu moří." 

 A tu je ten predel - časť vodstva putuje do Baltického mora, časť do mora Čierneho. 



 Pretože sa počasie celkom umúdrilo, vyberáme sa na hrad Helfštýn.  




 Máme dobrého sprievodcu, ktorého cesta dejinami evidentne baví, no ja sa  už tradične strácam po prvých dvoch storočiach, troch prestavbách a šiestich majiteľoch. Takže aspoň začiatok  - 13. storočie a Friduš z Linavy. Koniec sa nekoná, hrad je v súčasnosti zakonzervovaný a každoročne v auguste býva dejiskom medzinárodnej súťažnej prehliadky umeleckého kováčstva Hefaiston. Diela, ktoré účastníci musia  vytvoriť v časovom limite dvoch hodín, sú potom ponechané väčšinou na nádvoriach hradu alebo v jeho múzeu. 





 No a už nás to ťahá do civilizácie, preto si ešte odbehneme pozrieť neďaleký Lipník nad Bečvou. Podotýkam, že sa stále pohybujeme v priestore vymedzenom fiktívnou kružnicou s priemerom 40 km, ktorú som spomínala v prvej časti moravskej reportáže. 



 Na námesti práve hrajú skupiny, ktoré budú cez víkend vystupovať na folkovom festivale Zahrada v Náměšti na Hané. Nakukneme do  zámockého parku s obrovskými štyristoročnýni stromami a zámkom, v ktorom je mestský úrad. Prejdeme sa okolo piaristickej školy, na ktorej študoval zakladateľ genetiky Gregor Mendel. Možno práve tu mu začalo vŕtať v hlave, čo všetko by sa dalo urobiť s hrachom.  



 Večer dočítam Plzáka (ešte raz veľké ďakujem, bolo mi potešením, majstre aj milá diskutérka) a pobalím. Dnes tu spíme naposledy. 
 Deň šiesty - posledný 

 Posledné hemendexy a potom z Rajnochovíc odchádzame. Ale aj na dnes máme toho naplánovaného dosť. 

 Naša cesta do Holešova vedie cez Rymice. Nachádza sa tu miniskanzen hanáckych stavieb - pár bielych domčekov s doškovými (slamenými) strechami. Nakukujeme cez okienka dovnútra, ale zdá sa to byť ešte nedokončené. Ujo, čo v susednej záhradke kosí trávu, nás posiela kamsi na most, kde by mali sedieť ľudia, ktorí nám múzeum sprístupnia. 




 Obec je známa aj veterným mlynom, ktorá sem bol prevezený z neďalekých Bořenovíc. Mlel až do konca 2. svetovej vojny. 



 Ľudí s kľúčmi od skanzenu sme nenašli, ale beztak máme dosť nabitý program. Ujo s kosou v záhradke je sklamaný, že sme neuspeli, preto ho utešíme, že sa nám tu páčilo aj tak, a smerujeme do Holešova. 



 Navštívime múzeum, kde nás najviac zaujme miestny rodák Josef Drásal, ktorého nazývajú hanáckym obrom. Narodil sa v roku 1841 a podľa literatúry meral 232 alebo 241 cm. Živil sa tým, že putoval ako atrakcia po Európe. Neoženil sa a zomrel vo veku 44 rokov. V múzeu je jeho portrét v skutočnej veľkosti,  topánky a pár fotografií.  

  V Holešove žila v minulosti početná židovská komunita. V roku 1848 tvorili 32 % obyvateľstva.  Navštívime Šachovu synagógu z roku 1560, v ktorej je v súčasnosti múzeum. Je pomenovaná na počesť  litovského rabína  Sabbatai  ben Meir Kohena, prezývaného Šachom, ktorý tu v 17. storočí pôsobil až do smrti.  



 Interiér synagógy fotený z prvého poschodia, ktoré bolo určené pre ženy. 


 Na druhom poschodí sa nachádzala chlapčenská učebňa.  



 Tóra, v ktorej je na pergamene zapísaných prvých 5 kníh Mojžišových. Nemožno sa jej dotýkať holou rukou, na čítanie sa preto používa špeciálne ukazovátko. 




 Synagóga fungovala do 20. rokov minulého storočia, pretože koncom 19. storočia si Židia postavili novú. Tá však nemala dlhé trvanie, v roku 1941 ju nacisti vypálili. Židovské rodiny boli deportované do Terezína a odtiaľ do Osvienčimu, išlo asi o 200 ľudí. Po vojne sa vrátili deviati. Osudy mnohých ostávajú neznáme. 




 Zo synagógy mierime na židovský cintorín. Najstaršie náhrobky sú  z roku 1647. Na niektorých sú poukladané kamienky. 



 Hrob rabína Šacha. Na jeho náhrobku bolo kamienkov najviac.  


 Chvíľu sa ešte poprechádzame po zámockom parku s vodným kanálom. Zámok sa momentálne opravuje. Je štvorkrídlový a  má štyri vežičky.  Trochu mi tým pripomína Bratislavský hrad, akurát ako keby mu chýbalo nejaké poschodie. 



 Je čas obeda, preto zbehneme na halušky (skutočne, bryndzové halušky aj strapáčky majú v tomto kraji v každej reštaurácii a chutia výborne) do neďalekého Brusna, kde sme už raz večerali a veľmi sa nám tam páčilo. Reštaurácia je v penzióne prebudovanom zo starého mlyna z  15. storočia  a z obrázkov na stene sa dozvedáme, že tu raz prespal aj tatíček Masaryk, keď sa na kočiari, ktorým tadiaľto prechádzal, polámalo koleso. A tiež sa mu tu páčilo. 





 Našu moravskú dovolenku ukončíme prehliadkou Bystřice pod Hostýnem. Chceli sme navštíviť múzeum ohýbaného nábytku firmy Thonetovcov, ale nefunguje. Tak si aspoň pozrieme na námestí vystúpenie nemeckého folklórneho súboru, ktorý však svoje tančeky musí čoskoro prerušiť kvôli lejáku. 



 

 
 A už je čas odchodu. Nedokázala by som sa s týmto krajom rozlúčiť lepšie než slovami básne Valašsko od Metoděja Jahna. Visela zarámovaná na stene reštaurácie v Rajnochoviciach, v ktorej sme si dávali svoju pravidelnú dávku valašskej kyselice.  


 Na chlumu kopce z rána stoje 
přehlížím dálné pásmo hor, 
naslouchám, jak z lesní chvoje 
tajemný vane rozhovor. 
 
Údolí, hory v dálku běží, 
rubiska vonná, paseky, 
pryskyřicí a rosou svěží 
dýchá ten prostor daleký. 
 
Ej, to je kraj můj milovaný, 
krása je v něm a boží klid, 
kde má své rolky, chudé stany 
dobrý můj a tak bystrý lid. 
 
Sotva tě shlédnu země drahá, 
radostí se mi zvlní hruď, 
po klobouku že ruka sahá - 
Valašsko moje, zdrávo buď!...


 

 Opúšťame kraj, kde sa som sa cítila bezstarostná, voľná a mladá. 



 Dedinky, kde sa vedľa seba ocitli  Černotín i Bělotín a kde sa za pár minút môžete dostať z Dobrotíc do Zlobic a naopak. 

 Lesy, v ktorých  na nás čakala nejedna studnička... 



 ... i nejedno prekvapenie. 




 Miesta, kde  história nie je dávno zabudnutou minulosťou, 


 ... aj keď to občas vyzerá, akoby niektoré spomienky odvial čas. 


  Preto tu oddávna až do dneška svojich ľudí strážia pomocnice a orodovnice na návsiach... 



 ... aj tam, kde sa stretáva pole s horou, 



 ...na mostoch na veky vekov mlčia Nepomuckí Jánovia... 




 ... a  na krížnych cestách stojí kríž. 






