

 Boli sme zvedaví, čo sa pred nami vynorí. Keďže nás cestou chytila prietrž mračien a všade bolo more vody, farma, ktorá bola našim cieľom, zívala prázdnotou. Okrem akýchsi chlpatých potvor a pár kengúr za plotom sme nevideli živej duše. Rampa spustená, pokladňa zavretá, informácia k danému dňu žiadna. Predsa len, sme na Slovensku. 
 Vrátili sme sa teda do Piešťan s vidinou teplého obeda a nádejou, že sa snáď vyčasí a tá záplava kvapiek, padajúca na nás z neba, si bude predsa len potrebovať oddýchnuť a spraví si prestávku. Naše prosby boli vypočuté. Žalúdky sme mali naplnené, náladu dobrú a nič nám nebránilo trošku sa pomotať po piešťanskom parku a kolonádnom moste vedľa neho. Mimochodom, ak by mal niekto chuť fotografovať veveričky, hmyz, či vodné vtáctvo – labute a čajky, príde si tu na svoje. Môže sa vyhrať s makroobjektívom, ale aj dlhým ohniskom. 
  
  
  
  
 Keď sa ukázal prvý slnečný lúč, naskákali sme rýchlo opäť do auta a poď ho do Modrovej. Zvláštne, ako úplne inak vyzerala cesta za pekného slnečného dňa, oproti rannému vodnému dopusteniu. Slnko vylákalo do týchto končín viacero nadšencov, takže sme to zapichli na jedno z posledných voľných miest na parkovanie na miestnom miniparkovisku. Chvíľku sme sa zdržali pri platení, dať lístky 4 osobám na prehliadku zvierat a aj na vystúpenie pravých nefalšovaných Maorov, robilo tete biletárke mierny problém. Najprv nám sľúbila zvlášť 4 lístky na ZOO, zvlášť na novozélandských domorodcov, nakoniec sme obdržali lístok jeden - na všetko a pre všetkých. Trochu rozpačite sme sa s Ľubkou na seba pozreli, ale potom sme tento úvodný ceremoniál hodili za hlavu, vybrali fotoaparáty z tašiek a tešili sa na zaujímavé momenty. 
 Za 66 eur sa dá priamo v areáli zaplatiť výhliadkový let vrtuľníkom, čoho svedkom sme sa takmer okamžite stali. Vrtuľník nám prelietaval ponad hlavy a temer nad rohami akéhosi dlhosrstého dobytka a oviec sa pilot snažil nahradiť rolu honelníka a dostať stáda z jednej strany ohrady na druhú. Čiastočne sa mu to podarilo. 
  
  
  
  
 Vyššie v kopci, za heliportom, sme vzhliadli ohradu s nápisom AUSTRÁLIA, v ktorej sa nachádzajú 4 kengury – pozor - jedna je čerstvá mamička, takže pri troche šťastia môžete vidieť malú ružovú hlávku vykúkajúcu z maminho teplého brušného vaku. Keď však mama zavetrí akýkoľvek náznak nepokoja, už aj labami capne krpcovi po hlave a ten okamžite mizne v spleti kengurých chlpov a Vy dostanete pocit, akoby ste pred chvíľkou videli len akúsi fatamorgánu. 
  
  
  
 Za kengurami narazíte na 2 pštrosov emu a pod ich životným priestorom sa v tráve plazí krokodíl. Na toho som sa tešila najviac. Zbadajúc jeho mohutný šupinatý chrbát, zrýchleným tempom som sa šmýkala po rozbahnenej cestičke s cieľom zachytiť jeho pohľad. A ako som si tak „štelovala“ svoj nový objektív /týmto Vám všetkým dávam na známosť, že som vymenila fototechniku – telo aj objektívy a mnou tak často ospevované dlhé ohnisko sa už nachádza v mojej fototaške :-)), zaostriac na oko, dumám čo za druh krokodíla to je, že má až také oranžové oči. A až vtedy zisťujem, že ide len o umelú atrapu a žiaden „origoš“ krokodílí portrét nebude. Nuž čo ... niekedy je naozaj lepšie nič neočakávať, nemám byť z čoho potom sklamaná. 
  
  
 Mierime k dobytku, ja len tak z diaľky, neviem prečo, ale predsa len mám rešpekt pred týmito rohatými príšerami.  Avšak Ľubka, Karol aj Michal stáli od tých rohov len niekoľko centimetrov. 
  
  
  
  
 Medzitým sa opäť zamračilo a začalo pršať, takže sme zbehli dole stráňou do auta. Našťastie, to bola kratučká sprcha, trvajúca zhruba 10 minút. 
 Zlatý klinec programu sa konal za pekného letného slnečného svetla. Bojovníci pôvodného novozélandského kmeňa nabehli vo svojich zelených kostýmoch so zablatenými krkmi a oštepmi v ruke. Spočiatku som sa zľakla ich výrazov, lebo pučili oči, vyplazovali jazyky a zatínali päste. Avšak pre fotografa to znamená priam rajský okamih. Výzva – zachytím, nezachytím – je tu a tak sme sa všetci štyria rozpŕchli po okolí a každý cvakal z iného uhla, s iným aparátom a iným objektívom. Po prvý krát som nasadila svoje nové 300 mm ohnisko a len sa pomodlila, nech ho udržím bez chvenia ruky. Musím pripustiť, že pár záberov som pokazila, ale ako sa vraví, všetko zlé je na niečo dobré. Aspoň som sa od Karola dozvedela, nech si strážim čas na minimálne 1/200s, najlepšie 1/300s a vyššie. Vtedy by som mohla obraz zachytiť ostrý bez záchvevu, k čomu mi dopomáhaj ... stabilizátor :-). 
 Okrem hrôzostrašných bojovníkov sa na pódium dostavili aj miestne maorské krásky /u nás by sme ich mohli definovať ako dievčence krv a mlieko/ a tie trošku zjemnili drsnosť mužského plemena. Aj keď v niektorých piesňach, ktorých text neviem, sa tvárili rovnako hrôzostrašne ako ich kolegovia. Na ukážku Vás teda pozývam medzi nich, môžete si s nimi kľudne zaskákať a zaspievať, ako to urobili niektorí návštevníci. My sme sa s nimi na záver odfotili, nech máme pamiatku na tento vskutku príjemný deň. 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
   

