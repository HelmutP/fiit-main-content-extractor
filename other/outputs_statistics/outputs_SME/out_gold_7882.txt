

    
  
  
 Svedomie... Ľudia pretvárajúc sa sami pred sebou. Tí, ktorí klamú dokonca pri pohľade do zrkadla. Slepé sny a zvrátené túžby mať to a za každú cenu! A byť rovnaký ako oni, aj keď v skutočnosti sa nedá inak. Nedá sa zabudnúť na to, čo spravili, na tú bolesť, ktorú spôsobili. Iba byť nimi. Tými falošnými, ktorí nám vzali všetko! Celý svet. Náš život. Našu rodinu L. Ale rozhodli sa, a ako som na začiatku spomínala, nakoniec sa rozhodneme podľa toho,  na čom skutočne záleží. Mať posledné slovo v boji, neznamená vyhrať vojnu. Sme ako slovenskí junáci v básni Mor ho, porazení víťazi.  
 Niekedy je morálka prekliate zlo, ktoré vám nedovolí konať inak. Buď to v sebe máte, alebo sa to svet dozvie. Skôr či neskôr, ako sa vraví... Neskoro je plakať, keď sa necháme oklamať, neskoro je prebdieť noci a dúfať, že je to rovnaké aj na druhej strane. Už je neskoro. Je čas odísť a nepozerať sa späť. Ale svedomie... Niektorí ho majú... a srdce. Odísť znamená nechať tu srdce, orgán zodpovedný za náš život. Odísť znamená umrieť a žiť odznova. Ale odznova znamená byť sám.  
 A tak len odpočítavame dni, ktoré ešte zostávajú a pomaličky odchádzame. Pomaličky umierame. Je to akoby sme sa mali tešiť na smrť. Keď nevieme, čo bude potom, kam pôjdeme, a čo sa s nami stane. Je to, ako čakanie na Godota.  
   

