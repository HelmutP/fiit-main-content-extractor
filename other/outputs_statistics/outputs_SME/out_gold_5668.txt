

 Vôbec nevyzeral na človeka, ktorý bol kedysi aktívnym vojakom. Možno až na rovné držanie tela. Tvár mu prekrývala neostrihaná brada, chýbali mu horné zuby a mal mäkké oči, z ktorých nikdy nemohol vystreliť. 
 Pochádzal od Nitry a na štyridsať rokov uviazol na Spiši. Mám dojem, že tu už zostane do konečnej stanice. Ako dvadsaťdeväťročný sa oženil. Vzal si ženu s piatimi deťmi. 
 Slová mu z tváre odkryli závoj. Takto mal oči ešte mäkšie a nežný úsmev mu visel na sivom strnisku. Rozprával dychtivo, v úryvkoch, no pravdepodobne nikdy neoľutoval ani jediný deň strávený s tou ženou a jej deťmi, o ktoré sa staral ako o vlastné. 
 Postupne sa mu reč zadrhávala. Slová zo seba vypľúval jednotlivo, niektoré musel aj na dvakrát. Pred dvoma rokmi mu žena zomrela. Zahanbene sa priznal, že to nezvládol. Päť detí, o ktoré sa staral, sa od neho zo dňa na deň odvrátilo a nech robil čo robil, vodu na krv nedokázal premeniť. 
 Zostal sám a v kraji, do ktorého zapustil jediný koreň, svoju ženu a ten sa po jej smrti začal kývať. Celé dni presedel v krčme a s hanbou mi pošepkal, že iba pil a neustále telefonoval. Na jediné číslo. Zakaždým sa mu ozval neosobný hlas, ktorý tvrdil, že číslo je nedostupné, aby to skúsil neskôr. A on aj keď vedel, že číslo už zostane naveky nemé, telefón pokúšal znovu a znovu. 
 Prestal s tým, keď sa jednej noci vracal domov popri železničnej trati. Sedela vedľa koľají. Nedalo mu, zastavil sa a prihovoril. I keď nebol psychológ, prehovoril ju a odviedol domov. Bola v jeho veku a synovým pričinením na ňu vycierala zuby exekúcia. Tak ju prichýlil. Staral sa o ňu. Do krčmy už nemohol, iba z času na čas tajne zavolal na číslo, ktoré zostávalo nedostupné. Pre istotu, aby naň nezabudol. 
 Mohutný chlap, ktorý rozprával, ako keď sa prelieva med, chlap, ktoré z armády odsunuli do výslužby. Našťastie v aktívnej službe zostal ako človek. 
   

