
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Vrátny
                                        &gt;
                U nás na Slovensku
                     
                 Spomienky na nepedofilné chlapčenstvo 

        
            
                                    25.6.2010
            o
            11:01
                        (upravené
                2.5.2011
                o
                5:45)
                        |
            Karma článku:
                10.25
            |
            Prečítané 
            1454-krát
                    
         
     
         
             

                 
                    Znovu som po mnohých rokoch zobral do rúk Rázusovho Maroška. Pekný popis mladosti na slovenskom vidieku koncom 19. storočia. Uvedomil som si, že aj moja mladosť prebiehala ešte podobne, ako Maroškova, aj keď niekoľko generácií neskôr.
                 

                 Tiež sme chovali doma zajacov, sliepky, prasce, aj požiar maštale som zažil (aj keď nie našej, lebo maštaľ sme už nemali), i v kostole sme nezbedu vyvádzali, chodil som zvoniť na katedrálnu zvonicu, miništrovať, kvôli peniažkom na Veľkú Noc polievať a na Troch kráľov vinšovať, dobytok sme pásli, v rieke sa kúpali, ryby a raky chytali, všetko ako za čias Maroška. Zato dnešná mládež už má hodne iný život, aj starosti. Nechcem povedať či lepšie, či horšie, ale iné určite. A poriadne.      Na veži katedrálneho kostola v pozadí som ako chlapec zvonil. Vyžadovalo si to na môj vek značnú silu, výdrž a aj opatrnosť. Povraz musel byť neustále napätý, lebo ináč sa rozkmital do zákerných slučiek. To sa podarilo spolužiakovi, ktorému sa z nepozornosti povraz vyšmykol z rúk a hneď na to sa mu okrútil okolo krku, nadvihol ho a poriadne šmaril o zem. Našťastie okrem červeného odtlačku povrazu, ktorý mu ako náhrdelník niekoľko týždňov krášlil krk, prípad nemal nijaké následky. Okrem pocitu dôležitosti mi zvonenie pomáhalo dostať sa aj k peniažkom. Za zvonenie na malom zvone som dostal dve koruny, tri koruny na strednom zvone a štyri koruny na najväčšom zvone. (Štvrtý zvon sme už nemali, z toho uliali kanóny ešte v časoch 1. svetovej vojny.) Pripomeniem, že pohár sódy stál vtedy 10 halierov, polguľka vanilkovej zmrzliny sedemdesiat halierov a kino bolo za jednu korunu.      A potom tu bolo miništrovanie. Lial som opatrne kňazom víno a vodu na prsty nad kalichom, usilovne triasol zvončekom na pozdvihovanie, odpovedal, vtedy ešte po latinsky, na mne nie veľmi zrozumiteľné vety, sprevádzal som ich k ťažko chorým prijímajúcim posledné pomazanie, na pohreboch a pri vysväcovaní príbytkov veriacich farnosti na Troch kráľov. (Za pohreb som dostal dve koruny a z úspor trojkráľovej výslužky v roku 1953, keď sme mali tú nešťastnú menu peňazí, mi v triednej vkladnej knižke zostalo stosedemnásť nových korún aj štyridsať halierov. Toľko peňazí nedokázal zarobiť za jedno popoludnie a večer ani môj otec!) Za prízemnými oknami rožnej budovy fary v pozadí sme mali sobotné popoludňajšie sedenia pri hrách a rozhovoroch s našimi kaplánmi (v sobotu ráno sa ešte muselo do školy.)   Čo ma viedlo k takémuto spomínaniu?   Mal som tu pred nedávnom jedného Austrálčana. Potreboval si u nás niečo vybaviť a mňa si vybral za tlmočníka a sprievodcu v labyrinte našich organizácií. Mali sme času aj na rozhovor o živote v našich krajinách a ja som sa vrátil do svojich školských liet, lebo chcel počuť, ako som vnímal politicky ťažké 1950. roky. A v takomto duchu som aj odpovedal. Pochválil som sa miništrovaním, že som si udržiaval priateľské vzťahy s kaplánmi a že mám na nich lepšie spomienky, než na rôznych mládežníckych vedúcich, ktorých sme mali v rámci povinnej, marx-leninsky spolitizovanej pionierskej organizácie.   „No u nás by ti to len tak neprešlo. Hneď by sa za tým hľadal nejaký pedofilný škandál!", vyšlo z môjho austrálskeho hosťa.   Zarazilo ma to, lebo v mojom prípade sa určite o nič podobné nejednalo. A zároveň som sa aj pobavil, lebo som si uvedomil, ako nás média občas temperujú, aby sme reagovali po vzore Pavlovových psov. Nielen u nás, ale aj u ďalekých protinožcov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Prechádzka po Lutherovom meste Wittenberg
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Slnečný babioletný deň v Drážďanoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Nagymaros-Visegrád v neskoré leto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Na tému Ukrajiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Spomienka na Veľkú  vojnu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Vrátny
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Vrátny
            
         
        vratny.blog.sme.sk (rss)
         
                        VIP
                             
     
        V živote som vystriedal viacero bydlísk i povolaní a som teda z každého rožku trošku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    211
                
                
                    Celková karma
                    
                                                5.73
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inde v Európe
                        
                     
                                     
                        
                            Ázia
                        
                     
                                     
                        
                            Nepriateľská osoba
                        
                     
                                     
                        
                            Britské ostrovy
                        
                     
                                     
                        
                            Osudy
                        
                     
                                     
                        
                            U nás na Slovensku
                        
                     
                                     
                        
                            U susedov
                        
                     
                                     
                        
                            USA, Kanada
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na tému Ukrajiny
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Migranti v mojej obývačke
                     
                                                         
                       Sedem dobrých rokov skončilo
                     
                                                         
                       Fragmenty týždňa
                     
                                                         
                       O alkoholikovi, ktorý namaľoval obraz za $ 140 miliónov (a jeho múzeu).
                     
                                                         
                       Slovensko sa opäť zviditelnilo
                     
                                                         
                       Nie, nemusíte jesť špekáčiky zo separátu, vy ich jesť chcete
                     
                                                         
                       Najkrajší primátor najkrajšieho mesta
                     
                                                         
                       Letný pozdrav Sociálnej poisťovni
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




