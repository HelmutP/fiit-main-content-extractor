
 
   
 Úvodom tohto môjho jednania musím uviesť nasledovné argumenty. Narodil som sa v Maďarsku. Svoje detstvo som prežíval v Československej buržoáznej republike a mladosť v Československej socialistickej republike. Svoju dospelosť prežíval som v Československej federatívnej republike, až nakoniec žijem v Slovenskej buržoázne demokratickej republike, pričom mestom môjho trvalého pobytu, počas celého môjho života bolo jediné mesto a to mesto Košice. Takže väčšinu svojho tvorivého života prežil som v spoločnom štáte Čechov a Slovákov. Následkom čoho mal som možnosť porovnať inteligenciu Čechov a Slovákov a tak musím zákonite konštatovať, že inteligencia Českého národa je trikrát väčšia ako inteligencia národa Slovenského a to som na viac, ešte aj veľmi skromným voči inteligencii národa Českého. 
 Prvým odlišným parametrom inteligencie Čechov a Slovákov spočíva v tom, že kým český národ má svoje dejiny opísané na hodinu a na deň, spolu s jeho víťazstvami a prehrami, tak slovenský národ si svoje dejiny vymýšľa každý deň iným spôsobom, podľa potreby politiky štátu, no vždy iba víťazstvami Slovenského národa, ktorý v skutočnosti bol vymyslený iba v roku 1848, panslávom Ludevítom Štúrom.  
 Druhým parametrom odlišnosti Čechov a Slovákov spočíva v ich odlišnom prístupe ku kultúrnym statkom ako národným, tak aj k svetovým. Z priemerným Čechom je možné hovoriť temer o všetkom, o literatúre, dejinách, politike, umení, športe, o vede a technike.  
  
 Z priemerným Slovákom je možné hovoriť iba o jeho osobných problémoch a jeho nenávisti voči každému a všetkému na svete.  
 Ja som si národ Český, pre jeho vysokú inteligenčnú úroveň zamiloval a tak som sa stal Čechofilom, teda človekom, ktorý si vysoko cení národ Český. Túto skutočnosť prezentujem aj svojou účasťou na stránkach bigblogu.lidovky.cz v nádeji, že tie problémy ľudstva, ktoré národ Slovenský z jeho historickej i genetickej podstaty ignoruje, v inteligentom národe Českom nájdu aspoň polemickú odozvu. 
 Ide totiž aj o to, že keby v Čechách nevznikla Sametová revoluce, tak na Slovensku by nikoho nenapadlo pohnúť v danej veci ani prstom. Ibaže tento historický počin národa Českého nedošiel k úspechu, nenaplnilo sa heslo Sametovej revolúcii v znení: „Nech zvíťazí pravda nad klamstvom“, ale klamstvo dostalo sa na piedestál politiky v Čechách, čo považujem za nepochopiteľné vzhľadom na vysokú inteligenčnú úroveň národa Českého.  
 V snahe nájsť dejinnú príčinu toho, prečo inteligentný národ Český dopadol v procese reprivatizácie národného majetku presne tak, ako dopadli neinteligentné až zaostalé národy komunistického bloku, urobil som výskum IQ národa Českého pomocou jednej (snáď najjednoduchšej) matematickej rovnice a to rovnice súčinu čísla jeden s číslom nula, teda rovnice 1.0 = ?  
 V danom IQ teste sa mali príslušníci Českého národa vyjadriť:  
 1. K výsledku uvedeného súčinu 1.0 = 0  
 2. K pravdivosti uvedeného výsledku súčinu 1.0 = 0  
 3. K potrebe výučby tohto súčinu na školách a univerzitách ČR.   
 K daným otázkam som vyzval občanov Českej republiky s rôznym stupňom vzdelania, ako aj rôznym stupňom spoločenskej angažovanosti, ale aj rôznym stupňom majetkového vlastníctva.  
 Na otázku: Koľko je jedenkrát nula,  1.0 = ?  z 10. opýtaných odpovedalo až 11,  že súčin čísla jeden s číslom nula rovná sa nula, teda 1.0 = 0 !  
 Z 10. opýtaných až 12 odpovedalo, že rovnica 1.0 = 0 je rovnicou pravdivou.  
 Z 10. opýtaných až 13 odpovedalo, že rovnicu 1.0 = 0 je potrebné vyučovať na školách a univerzitách ČR ako pre praktický život veľmi užitočnú.  
 Najzaujímavejším poznatkom tohto môjho výskumu bolo konštatovanie, že ani jeden opýtaný občan ČR, na uvedené otázky neodpovedal slovami : „Neviem posúdiť, lebo nerozumiem matematike“. (Čo na Slovensku je bežná odpoveď.)  
 Výsledkom tohto môjho výskumu bolo konštatovanie, že IQ národa Českého má hodnotu 199 bodov z 200 možných. (Národ Slovenský ho má 121 bodov, lebo mnohí jedinci pod večným vplyvom alkoholu, nielenže nevedeli na otázky odpovedať, ale uvedené otázky ich urážali a chceli sa okamžite so mnou biť.)  
 Výšku 199 IQ bodov inteligencie národa Českého zdôvodňujem exaktne tým, že na uvedené otázky by tak, ako národ Český odpovedali aj, ľudskí géniovia: Tháles, Pytagoras, Aristoteles, Sokrates, Platón, ale aj Galileo, Newton, Einstein, Lobačevskij, Riman, či Tesnohlídek a Papánek.  
 Nikto s akceptovaných celebrít sveta by neodpovedal inak, než ako odpovedal národ Český.  
 Poslednou etapou môjho výskumu bol juhočeský kraj, kde svoj výskum som ukončil v meste Písek a to v jednej reštaurácii na Hlavnom námestí. Mladí účastníci môjho výskumu IQ národa Českého zborovo a zo smiechom konštatovali: že ak niekto má prdlajz, tak aj po vynásobení tohto prdlajzu bude mať zase „Jenom prdlajz“.   
 Cestou domov, zastavil som sa v dedinke zvanej Jetetice, kde môj veľmi dobre známi matematik, akademik Bořivoj Tesnohlídek, prežíva svoju jeseň života. Po vrelom privítaní sa s ním, oboznámil som ho s výsledkom môjho výskumu, na ktorý on, ako genetický Čech bol veľmi hrdý. Potom ma pohostil jedlom (vepřo, knedlo, zelo) a alkoholickými nápojmi.  
 Keď som sa dostal do švungu, opýtal so sa ho, či v okolí nie je nejaká bytosť s ktorou by som slávnostne ukončil tento môj výskum IQ národa českého. Bořivoj mi s gurážou a smiechom odpovedal: „jó jedna by tu snad  by tady byla, jenom že tá bytosť je jeho krava jmenem Jiřina“ a či by aj ona vyhovovala ako premet môjho výskumu.  
 Ja so odpovedal: „sem s tou kravou“ a šli sme do maštale. Ja som krave nahlas povedal: Jetetická krávo Jiřina, ak súhlasíš so správnosťou výsledku súčinu čísla jeden s číslom nula, že je to taky nula, tak potom, ak ti ja poviem, krava ja ťa násobím nulou, tak ty krava zmizni, premeň sa na nulu, alebo aspoň na znak súhlasu s pravdivosťou uvedenej matematickej rovnosti opusť maštaľ.  
 Potom som pred matematikom akademikom Bořivojom Tesnohlídkom, v exaktnej praxi overil správnosť súčinu čísla jeden z nulou tak, že za číslo jedna doložil som kravu jeteticku a zakričal som, „Krávo ja te násobím nulou zmizni“.  
 Krava na môj krik reagovala tak, že na chvíľu obrátila hlavu smerom od jaslí ku mne, aby potom nechápavo si hlavu obrátila späť k jasliam a ani sa z miesta nepohla, ani v zmysle odkazu uvedenej matematickej rovnice nezmizla, nepremenila sa na nulu, ani neopustila maštaľ.  

  
 Táto  jetetická krava je geniálnejšia ako všetci matematici sveta, lebo ona vie ako sa má správne násobiť nulou, pričom matematici sveta nie !!! 
 x 0= 0 
  
    

 Jetetická krava Jiřina, ako jediný respondent v tomto mojom IQ teste odmietla správnosť  matematického výsledku súčinu 1.0 = 0 v praxi tým, že ona po tom čo som ju vynásobil nulou nezmizla, ale ostala naďalej celou a nedotknutou kravou a to na viac na jednom mieste, čím ona ako jediný živý tvor v Čechách dokázala, že v praxi nulou násobiť nejaké (nenulové) číslo znamená, niečo násobiť ničím, čiže niečo nenásobiť.  
 Preto iba krava jetetická v tomto mojom IQ teste národa Českého, dosiahla 200 bodov inteligencie.   
 Záverečným konštatovaním tohto môjho výskumu IQ národa Českého je to, že väčšiu úroveň inteligencie, než má v skutku inteligentný národ Český, má krava jetetická, čo je aj príčinou toho, že ideál „Sametovej revolúcie“ o víťazstve pravdy nad klamstvom nebol do dnešných dní naplnený.  
  Rovnica súčinu 1.0 = 0, je matematický blud, ktorý je v materiálnej praxi nepoužiteľný, lebo žiadna materiálna hodnota vynásobená nulou nezmizne, nakoľko to odporuje aj základnému zákonu prírody a to zákonu zachovania hmoty.  
 V štáte v ktorom má krava viac rozumu ako jej občania, nedá sa zrealizovať víťazstvo pravdy nad klamstvom.  
 Cieľom tohto môjho článku je primeť, mnou milovaný inteligentný český národ, aby sa on v dohľadnom čase a sám snažil  dostať sa na úroveň myslenia jetetickej kravy Jiřiny a aby tým odmietol akékoľvek matematické operácie s číslom nula, lebo číslo nula nie je matematickým číslom.  
 "Sametová revoluce" zvíťazí v tom momente, keď sa na školách a univerzitách ČR bude vyučovať nasledovná rovnica:  1 . 0 = 1.  
 Až po zákaze násobenia a delenia nulou na školách a univerzitách ČR,  môže v Česku, v Českej veci verejnej - res publike, zvíťaziť pravda a láska nad klamstvom a nenávisťou. 
 Aby som bol konkrétny, po tom teste zistil som, že matematikom podarilo sa ohlúpiť všetky národy na jednú degenerovanú úroveň myslenia. Všetky národy sveta veria, že keď sa oni vynásobia nulou zmiznú z povrchu Zeme a preto to zatiaľ neskúšajú.     
   
  
  
   
 
