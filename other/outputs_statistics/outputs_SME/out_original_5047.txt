
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katy Suchovská
                                        &gt;
                Kynológia
                     
                 Po dlhej dobe: "Predstavujeme" 

        
            
                                    22.4.2010
            o
            16:50
                        (upravené
                23.4.2010
                o
                0:53)
                        |
            Karma článku:
                6.97
            |
            Prečítané 
            199-krát
                    
         
     
         
             

                 
                    Kedysi dávno mi tu zostal pozastavený blog bez činnosti. Po čase som si naň spomenula a rozhodla som sa opäť začať s písaním. No tentoraz s písaním o niečom, čo v posledných troch rokoch hrá v mojom živote veľkú úlohu – kynológia. (Prosím nepozastavujte sa nad chybami, píšem po dlhej dobe, takže som už možno trochu pozabudla, ako sa to robí.. :o))
                 

                 
Intenzívny trénink, Krumvíř (CZ)Terka Smolová
   Keď som bola malá, boli časy, kedy som prosila rodičov o to, aby mi kúpili šteniatko. Vždy mi odvetili, že sa nebudem o psíka starať a že zostane starostlivosť na nich, tiež boli vety ako „..Za dobré známky dostaneš psa!“... V neskoršom veku, mi povolili môjho prvého psa – maltézáčik. Vraj je to skladné, hodné, určite poslušné. S údajne malého, 26 cm maltézáčika s váhou max. 4-5 kg, vyrástol maltézák o výške 32 cm a váhe 7 kg. :o) Je pravda, že jeho maminka bola tiež väčšia.. a nie o trošku, ale o veľa. To si ešte hovorím, že Charlie je malý.    No a keďže to bol môj prvý pes, všetko mu bolo odpúšťané, všetko sa tolerovalo, naučil sa spávať v posteli, no proste základné chyby majiteľa psa. Lenže aby nezostalo len pri tom, že sme mali konečne doma nejakého psa. Chcela som sa s tým prvým vyvoleným psom venovať rôznym športom, chcela som ho naučiť výchove a poslušnosti a tak som narazila na kynologický klub v Hodoníne (CZ). Keď som si pozrela ich stránky, vôbec to nebolo cvičisko, kde cvičili nemeckých ovčiakov a iných veľkých psov.  Bolo to cvičisko, kde boli rôzne psie plemená, ktoré behali cez rôzne prekážky. Videla som to prvý krát, ale veľmi sa mi to zapáčilo. Do klubu som sa prihlásila a začali sme pravidelne chodiť na tréningy. Bavilo ma to veľmi, pokročili sme tak ďaleko, lenže prišla jeseň, obdobie kedy fenky hárajú a ja som u Charlieka poznala, čo je to byť v puberte a mať v hlave len fenky! Mohla som sa stavať na hlavu, mohla som mu dávať akékoľvek odmeny a dobroty, nepomáhalo. Prišla zima a snažili sme sa túto Charlieho vlastnosť odbúrať a znovu ho rozbehať. Rozbehal sa, no nemohla som povedať, že by som bola natoľko zaujímavá ako fenečky opäť na jaro. A tak prišlo veľké rozhodnutie v živote Charliekovom, kedy sa z Charlieho stala Charlienka. Vykastrovali sme ho. Verte alebo nie, tento krok urobil rapídnu zmenu v našej spoločnej tímovej práci. Charlie chcel, prácu a behanie vyžadoval, behal rýchlejšie a radostnejšie. Vyrazili sme na naše prvé závody, kde sme splnili prvú skúšku na výbornú. Bola som šťastná - prešťastná, domov som si priviezla prvú medailu a nadšenie, že sa určite musíme zúčastniť aj ďalších závodov..    Agility ako šport nadobudol v mojom živote veľkú „závislosť“. Keď som bola chorá, vždy som sa musela čo najrýchlejšie vyliečiť, aby som mohla ísť trénovať. Bola to ako droga.    V zime 2007 sa narodili kamarátke zo cvičiska, ktorá vlastnila tri fenky border kólií šteniatka. Na šteniatka som sa bola pozrieť a neustále tam zostával „Bezdomoveček“, ako mu hovorila. Bol to posledný psík, ktorý si nemohol nájsť nových majiteľov. A tak som prehovorila maminu, či by nemohlo byť nové šteniatko dočasne u nej, mamina povolila a ja som si šteniatko asi za 14 dní na to priviezla domov. Šteniatku som sa začala venovať, keď mal všetky povinné očkovania za sebou a mohol navštíviť cvičisko. Venovala som sa mu tak, že som chodila aj mimo tréningové dni, trénovala si sama, pomaly ale isto nové a nové veci. Chcela som mať u druhého psa všetko 100%. To čo som u Charlieka zanedbala, pretože to bol prvý pes, chcela som u druhého mať všetko. A podarilo sa. Crashík je najšikovnejšie zvieratko, ktoré agility miluje, pre obyčajnú loptičku urobí prvé posledné...    A na záver? Milujem moje dva psíky, milujem agility, je to jediný šport, ktorému sa venujem a vďaka ktorému som zaktivizovala svoj „lenivý život“. A viete čo sa o nás „agiliťákoch“ hovorí? „Agiliťáci sú blázni, ale vôbec im to neprekáža!!“ :o)        V článkoch, ktoré budem písať, by som vás chcela oboznámiť s tréningovými metódami, ako sme pokračovali v jednotlivom učení prekážok, o čom agility vôbec je, od samého začiatku až po koniec..              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            European Open, medzinárodný závod agility
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Náš aktívny agilitný život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Muffiny s Marsu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Keď kynológ, tak poriadny..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katy Suchovská 
                                        
                                            Veľká voda
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katy Suchovská
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katy Suchovská
            
         
        suchovska.blog.sme.sk (rss)
         
                                     
     
        Študentka VŠ, ktorá venuje všetok svoj voľný čas svojim dvom chlpatým miláčikom..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    27
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    662
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Kuchyňské pokusy
                        
                     
                                     
                        
                            Kynológia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            My chemical romance
                                     
                                                                             
                                            Evanescence
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




