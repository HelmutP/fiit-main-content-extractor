
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ondrej Dostál
                                        &gt;
                Politika
                     
                 Ficov podraz na voličoch 

        
            
                                    27.6.2010
            o
            16:04
                        (upravené
                27.6.2010
                o
                16:40)
                        |
            Karma článku:
                20.67
            |
            Prečítané 
            14008-krát
                    
         
     
         
             

                 
                    Stále mi nie je celkom jasné, prečo sa taký schopný demagóg ako Fico štyri roky vyhýbal televíznym diskusiám s politickými oponentmi. Po tom, čo dnes predviedol v STV, to naozaj úprimne nechápem.
                 

                 
  
   Programové tézy vznikajúcej pravicovej koalície označil za najväčší generálny podraz na voličoch, aký tu ešte nebol. Argumentoval najmä tým, že do spoločných programových téz budúcej koalície sa nedostali niektoré dôležité body z volebných programov koaličných strán. Keby Robert Fico práve priletel z Marsu a nikdy sa nepodieľal na žiadnom zostavovaní koaličnej vlády, ani o ničom takom v živote nepočul, mohli by sme veriť, že jeho rozhorčenie by teoreticky mohlo byť nestrojené a úprimné. Lebo by sa mohol mýliť, keďže by netušil, o čom hovorí. Takto je evidentné, že klame a zavádza.   Dosť pochybujem, že niekde na svete niekedy existovala koaličná vláda, ktorej vládny program by obsahoval všetky body predvolebných programov koaličných strán. Keďže politické strany majú rôzne programy a na niektoré otázky majú rozdielne názory, inak sa koaličná vláda ani zostaviť nedá. Aby sme len neteoretizovali, povedzme si aj reálny príklad z najbližšej možnej minulosti. Ficov SMER mal v roku 2006 vo svojom volebnom programe zavedenie progresívneho zdanenia pri dani z príjmov fyzických osôb. Do programového vyhlásenia Ficovej vlády sa po voľbách 2006 požiadavka na progresívne zdaňovanie príjmov fyzických osôb nedostala a rovná daň ostala zachovaná (úprava nezdaniteľného minima je síce jej deformáciou, ale nie zavedením progresívnej dane). Ak je nepresadenie všetkých dôležitých bodov volebného programu do vládneho programu generálnym podrazom na voličoch, aký tu ešte nebol, dopustil sa ho sám Fico v roku 2006. A to si pritom Fico v roku 2006 nemusel so svojimi koaličnými partnermi nič extra zložito dohadovať, lebo so Slotom a Mečiarom mohol v tom čas pokojne utierať dlážku a ani by necekli, aby sa dostali k vládnym korytám.   Rovnako veľkú mieru demagogickej geniality predviedol Fico, keď tvrdil, že sekera miliarda eur, ktorú po voľbách priznal jeho Počiatek, vlastne nie je jeho sekera, lebo vláda míňala presne tak, ako si naplánovala. To akurát tí zlí podnikatelia odviedli na daniach do štátneho rozpočtu menej, ako bolo plánované. A to predsa nie je chyba jeho, ani jeho vlády, ale tých zlých podnikateľov, však? Že si Fico s Počiatkom v štátnom rozpočte naplánovali nereálne príjmy, zjavne nie je pre Fica dôležité.   Maďarič vzápätí na Markíze spieval zjavne podľa tých istých nôt ako Fico v STV. Oddelenie propagandy zo Súmračnej pracuje dôkladne a spustilo na plné obrátky predvolebnú kampaň do ďalších parlamentných volieb. A Fico si ešte medzi chrlením predvolebnej propagandy dovolí moderátora uzemniť tvrdením, že „nie sme pred voľbami". Veď nežartujte už toľko, pán odchádzajúci premiér. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (160)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Konečne sa dozvieme mená všetkých členov Smeru
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Niekoľko viet o Táni Kratochvílovej
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Smeráčina á la Brixi: Len si tak niečo vybaviť
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ondrej Dostál 
                                        
                                            Fico nám berie peniaze a dáva ich svojim kamarátom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ondrej Dostál
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ondrej Dostál
            
         
        dostal.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som predsedom OKS. V novembrových voľbách kandidujem v bratislavskom Starom Meste do miestneho i mestského zastupiteľstva. Ako jeden z členov
Staromestskej päťky. Viac na www.sm5.sk. 

  

 Ondrej Dostál on Facebook 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    202
                
                
                    Celková karma
                    
                                                15.07
                    
                
                
                    Priemerná čítanosť
                    5758
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Smrteľne vážne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Radovan Kazda
                                     
                                                                             
                                            Ivan Kuhn
                                     
                                                                             
                                            Juraj Petrovič
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Michal Novota
                                     
                                                                             
                                            Ondrej Schutz
                                     
                                                                             
                                            Tomáš Krištofóry
                                     
                                                                             
                                            Peter Spáč
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            INLAND
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Konzervatívny inštitút M.R.Štefánika
                                     
                                                                             
                                            Peter Gonda
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       TA3 robí Ficovi reklamu
                     
                                                         
                       V TA3 Fica hrýzkajú slabúčko
                     
                                                         
                       O tom, čo platí "štát"
                     
                                                         
                       Konečne sa dozvieme mená všetkých členov Smeru
                     
                                                         
                       My, národ slovenský, spoločne s potrubiami a spotrebiteľskými obalmi
                     
                                                         
                       Smeráčina á la Brixi: Len si tak niečo vybaviť
                     
                                                         
                       Potemkinov most v Bratislave
                     
                                                         
                       Fico nám berie peniaze a dáva ich svojim kamarátom
                     
                                                         
                       Výsmech občanom v priamom prenose alebo .... Mišíková v akcii ...
                     
                                                         
                       Ficov podrazík na voličoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




