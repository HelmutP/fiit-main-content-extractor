

 Najprv zažartuje o kukátku. Vraj ako som si ho prezrela. Potom ma uisťuje - neprišiel ma vykradnúť. Fú, vydýchla som si, ešte že ma takto upokojil. A hneď to zabil ďalšou otázkou: 
 "Rodičia sú doma, slečna?" pýta sa ďalej. rozmýšľam či mi chcel zalichotiť, že vyzerám mlado. Odpovedám nie, nebývajú tu. 
 "Aha, a priateľa máte doma?" pýta sa ďalej priblblým tónom, ako keby som bola nesvojprávna. 
 "Nie je. Čo potrebujete, prosím vás? Som chorá a rada by som si ľahla naspäť do postele." hovorím už trocha rýchlejšie a demonštratívne zakašlem. 
 Chlapík vraví, že je z T-mobile. To mi bol čert dlžen, vážne. Stále sa viac a viac ku mne približuje, asi ich na školení učili, že to pôsobí dôveryhodne... a asi im nepovedali, že na niektoré typy to pôsobí presne opačne, vrátane mna. 
 Vlezlým hláskom a ešte vlezlejšou rukou mi podal obálku do ruky a hovorí: 
 "Rozdávam tieto simkarty iba vybraným ľuďom, a teda aj vám. Akého máte operátora? Aké vysoké máte účty?" 
 "Nehnevajte sa, ale to je moja vec."  vraciam mu obálku a chcem zatvoriť dvere. 
 "Nie, nie, prosím vás, toto je veľmi výhodná ponuka. A mne veľa ľudí hovorí o účtoch, myslíte si, že si to pamätám??? Aj vaša suseda nad vami mi povedala, tá má 130 € za mesiac!" povedal a slizko sa zasmial. Informácie u neho boli zjavne ako v hrobe. Tak som mu povedala fiktívnu sumu. 
 Spustil informácie o paušáloch, začal mi to všetko ukazovať na pripravenej tabulečke, ako úžasne sa mi to oplatí. Ušetrím neskutočné peniaze...všetko len s jednou simkartou navyše, a iba za 300 korún mesačne. Chlapík ešte stále rozprával v korunách, neustále mi všetko vysvetľoval ako idiotovi, a za každým porovnaním dodal: "To je jednoduchá matika, tá nepustí!" 
 Hovorím mu: "Viete čo, ja fakt o toto nemám záujem. Ja nebudem mať dve simkarty, nebudem si ich meniť, a aj tak chcem od T-mobile odísť, hneď ako sa bude dať." 
 Mladík sa opäť nedal... "O2 sa neoplatí! Oni majú hovory drahšie ako na tejto simkarte!!!" 
 "A vy ako viete, že chcem ísť do O2? Nič také som nepovedala. A vážne si teraz už idem ľahnúť." hovorím a vraciam mu tú simkartu asi po 5 krát. 
 Chlapec však stále dúfal, že z toho niečo bude a vyšla z neho posledná perla: "Vy si asi neplatíte ten telefón sama, že? Priateľ sponzoruje? Alebo rodičia? Nechcete im ušetriť trocha peňazí?" pokračuje a netuší, že práve naozaj skončil. 
 Pri zatváraní dverí už len rýchlo chytil letiacu obálku so simkartou a stihol ešte rýchlu otázku, že či by si nemohol u mňa odskočiť. Nemohol. Vodu treba platiť, ušetríme jedno splachovanie. 
   

