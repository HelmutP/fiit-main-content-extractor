

   
 "Je to strašne ťažkééé..." 
 Kamarátka zamyslene prikývla. 
 "A je toho tak veľa..." 
 Ďalšie prikývnutie. 
 "Ale vieš čo?" 
 Spolužiačka odpovedala pokrútením hlavou. 
 "Môžu nás prinútiť prečítať si tú hlúpu knihu! 
 Aj naučiť všetky teórie a definície! 
 Môžu nám prikázať pozerať správy a čítať noviny! 
 Ale nemôžu nás prinútiť logicky myslieť!" 
 Nie, to naozaj nemôžu... 
   

