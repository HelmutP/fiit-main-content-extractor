
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dana Janebová
                                        &gt;
                Poézia
                     
                 Poetický deň 

        
            
                                    31.7.2010
            o
            8:20
                        (upravené
                28.7.2010
                o
                23:05)
                        |
            Karma článku:
                11.43
            |
            Prečítané 
            500-krát
                    
         
     
         
             

                 
                    
                 

                Svieže ráno tráva v pozore rosou si pripila na dnešný deň brezové listy vytiahli mimikry a v poznačení slnka cvengá ich ligot ako zlaté dukáty Deň si berie do úst zvony a na zvonici zvoní že chce pokoj a mier žiadne výstrednosti pod pás Slnko je zlatý topás a šteklí moje oči až ich privieram Taká bezstarostná nedeľa... Ticho plávam poludním aj vánok sa ukľudní Je čas zapískať na zore nech sa tlačia hore a večeru dať svoj hlas aby večernica zasadla do parkov a na lavičkách tajne vypočula milencov v podvečerný čas... *

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dana Janebová 
                                        
                                            Svetlo 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dana Janebová 
                                        
                                            Svetlo 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dana Janebová 
                                        
                                            Svetlo 1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dana Janebová 
                                        
                                            Som človek - kto je viac?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dana Janebová 
                                        
                                            Ako smädný pútnik tohto sveta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dana Janebová
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Do mňa sa omáčaj
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Nežná
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dana Janebová
            
         
        janebova.blog.sme.sk (rss)
         
                        VIP
                             
     
         ...MOJE RUKY SÚ LÁSKA... http://www.artforum.sk/catalog/janebova-dana:moje-ruky-su-laska-/?mod=catalog&amp;detail=32019 -...ODSÚDENÁ NA LÁSKU... http://www.perfekt.sk/knihy-odsudena-na-lasku....................................... TVOJE OČI SÚ CESTA... http://www.perfekt.sk/knihy-tvoje-oci-su-cesta 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    562
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    380
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Prehlasované smútky
                        
                     
                                     
                        
                            Osudova je vždy piata
                        
                     
                                     
                        
                            Elégia misterioso
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Listy dôverné
                        
                     
                                     
                        
                            Mojim priateľom
                        
                     
                                     
                        
                            Asteroidy
                        
                     
                                     
                        
                            Impresie
                        
                     
                                     
                        
                            Fragmenty
                        
                     
                                     
                        
                            Pieseň pre lásku
                        
                     
                                     
                        
                            Tretí rozmer
                        
                     
                                     
                        
                            Iný rozmer
                        
                     
                                     
                        
                            Rozhovory s Bohom
                        
                     
                                     
                        
                            Vibrácie
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




