

 Telefonát prebiehal približne takto: 
 „Pozdravujem ťa, pán vikár, potrebujem s tebou prebrať jednu vec." 
 „Aj ja ťa pozdravujem, Štefan, dlho som ťa nepočul, ako sa ti darí?" 
 „Ďakujem, darí sa. Len keby tých problémov bolo  menej. Počúvaj, Jozef, potrebujem s tebou dohodnúť opravu kotla, ktorý začína byť v havarijnom stave. Myslíš, že sa môžem do toho pustiť ešte v tomto roku?" 
 „Vieš čo, Števo, pozriem sa ako na to ako stojíme s peniazmi a dám ti vedieť." 
 „Jozef, ale bol by som rád, keby si si švihol, lebo sa mi rysuje ešte jedno provizórne riešenie. Radšej by som však bol, keby sa to vyriešilo v tomto roku. Zima je ešte dlhá." 
 „Budeme sa modliť, aby bola krátka:-)  Neboj sa, dačo vymyslíme, aby deťom nebolo zima. Požehnaný čas tebe i celému detskému domovu." 
 „Budem ti veľmi vďačný, Jozef, keby si nám pomohol. Spadol by mi veľký balvan zo srdca. Mohli by sme sa už niekedy stretnúť." 
 „Neboj, stretneme sa na jar. S Pánom Bohom." 
  Toľko telefonický rozhovor. Števo, trošku zmätený, sa pýtal kolegyne: „Aký je ten Janko čudný, povedal mi, že sa na jar stretneme. Nechápem, ako to myslel. Ako by si zo mňa strieľal. Nevieš, čo to malo znamenať?"  Kolegyňa sa usmievala:  „Ty fakt nevieš? Celý čas si ho oslovoval Jozef. To čo sa ti porobilo?" 
 „Ja že som ho oslovoval Jozef?  No to som si vyrobil riadnu hanbu."  Ešte chvíľu sa sa na tom smiali. Časom na veselú príhodu  zabudli. 
  19. marec 2009. Stará Ľubovňa praská vo švíkoch, dopravu regulujú policajti. Pochovávajú biskupského vikára, Jána Zentka. V dave ľudí kráča aj môj kamarát Števo. 
   

