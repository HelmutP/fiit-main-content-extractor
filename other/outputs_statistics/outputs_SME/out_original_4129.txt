
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Kristian Takac
                                        &gt;
                Nezaradené
                     
                 Dve štátne návštevy a jedna fraška v priamom prenose. 

        
            
                                    7.4.2010
            o
            21:44
                        (upravené
                7.4.2010
                o
                21:50)
                        |
            Karma článku:
                8.85
            |
            Prečítané 
            1362-krát
                    
         
     
         
             

                 
                    Je ťažké byť Slovanom a mať neutrálny vzťah k Rusku. Ten veľký sused na východe svojou kultúrnou blízkosťou a mocenským postavením vo svete na jednej strane priťahuje a na druhej strane svojou brutalitou, čudnými aziatskými praktikami odpudzuje a straší. Pred pár rokmi to vyzeralo, že si z touto femme fatale z východu už viac krát do spoločnej postele neľahneme. V 89 sme sa síce so zlomeným srdcom ale s úľavou vymanili z jej prehnane tuhého objatia.
                 

                 Lenže osudová je osudová. Prešlo pár rokov, a máme ju tu znova. Botox, silikón a šikovné PR v podobe „historických očistných kúr" spravili svoje. Jazvy na srdci sa šmahom zahojili. Predseda vlády zašiel dokonca ešte ďalej. Podľa neho je slobodné pestovanie slovanskej vzájomnosti priam právom a povinnosťou tejto vlády.   Aby sme sa rozumeli správne. Návšteva najvyššieho predstaviteľa Ruska je z diplomatického hľadiska dobrá vec. Zviditeľňuje to našu krajinu a dáva priestor pre pestovanie obchodných vzťahov, ktoré sú pre expanziu našich firiem potrebné. Kontrakty, ktoré Tatravagonka a I.Tran podpísali nech sú pozitívnym príkladom. Nič proti, naopak. Z hľadiska energetickej diverzifikácie rozpačito pôsobí zmluva o dodávkach jadrového paliva pre Mochovce. Ale prižmúrme oko a povedzme, že ani to nie je až taká tragédia. Sme predsa v NATO a EU, či nie?   To, čo tragédiou bezpochyby je, sú znovu tie náznaky servilnosti súčasnej vlády a prezidenta voči veľkému bratovi z východu. Možno to nie je nič nenormálneho, možno tú servilnosť bývali komunisti Fico a Gašparovič jednoducho majú v krvi. Ale bolo by viac než milé, keby si v dôležitých okamihoch uvedomili, že nereprezentujú len seba a svoje stranícke farby ale predovšetkým Slovensko. A Slovensko, cez ktoré nie tak dávno prešli sovietske tanky a pošliapali ideály, nádeje aj ľudské životy si ospravedlnenie z úst ruského predstaviteľa zaslúži.   Mnohí iste namietnu, že ospravedlnenie za 68, to je vysoká méta. Kvitujem. Ale v takomto prípade by Gašparovič nemal robiť (ruského) gašparka a osobne ospravedlňovať ruské neospravedlenie sa. Stačilo by nebyť pokrytcom a nepodpisovať deklaráciu podľa ktorej je „objektívny vzťah k histórii jej rešpektovanie je meradlom zodpovedných a civilizovaných medzištátnych vzťahov v rámci celej Europy". Stačilo by povedať, že ruská strana na otázky týkajúce sa úlohy „červenej armády" v československej okupácii v auguste 1968 nie je (ešte) pripravená odpovedať. Stačilo by nechať Rusov, aby si svoje svedomie spytovali sami.   Takto nejako to dnes šikovne a dôstojne spravili Poliaci. Patrične vzdali hold premiérovi Putinovi, ktorí si dnes spolu s poľským premiérom Tuskom uctil pamiatku obetí Katyňského masakru. To gesto je jednoznačne prelomovým krokom v novodobých poľsko-ruských vzťahoch. Donedávna Rusko akýkoľvek podiel sovietskeho NKVD na vražde poľských dôstojníkov vytrvalo odmietalo a držalo sa verzie sovietskej histografie obviňujúcej z vraždy nacistické Nemecko. Ale nech bol tento krok (o ktorom sa slovenské média doteraz ani len slovkom nezmienili) akokoľvek veľkorysý, toho hlavného sa Poliaci nedočkali. V prejave Putina ani raz nepadlo to jednoduché ale bolestivé slovíčko „prepáčte".  V prejave sa mimochodom neobjavili ani slová „NKVD" či „Stalin". Poliaci si túto polovičatosť ruského prístupu všimli, pán Gašparovič a Fico zjavne nie.   Možno je to normálne. Medvedeva i Putina budú voliť Rusi, nie my. Je to Putin kto stojí za oživovaním kultu „sovietizmu", ktorým od svojho nástupu k moci masíruje egá a mozgy bežných Rusov. Je to Putin, ktorý sa nezdráha zaradiť Stalina do panteónu „veľkých Rusov". Je to smutné, ale je to jeho sväté (populistické) právo.   Našou svätou povinnosťou by malo byť držať si od Rusov v zdravej vzdialenosti od tela a dešifrovať dymové clony rafinovanej ruskej diplomacie. Kladenie vencov na Slavíne je pekná vec ale pokiaľ Rusi neprejavia aspoň trochu úprimnej snahy vniesť malé zrnko nefalšovanej úprimnosti do vzťahu so svojimi bývalými satelitmi bude treba podobné vábivé prelúdiá zo strany Ruska brať skutočne len ako PR a snahu o oživenie starej osudovej lásky.  Pri šikovnosti prezidenta a predsedu vlády však zostáva len dúfať, že aj stará láska raz zhrdzavie. Obzvlášť ak jeden z partnerov tú lásku väčšinou len predstiera.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (61)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristian Takac 
                                        
                                            Slovenský pravicový volič - ruská voľba?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristian Takac 
                                        
                                            Ponaučenie z poľskej Gorily.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristian Takac 
                                        
                                            O farizejoch a zabávačoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristian Takac 
                                        
                                            O solidarite a demokracii. Tej európskej.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kristian Takac 
                                        
                                            Tí druhí, tí zlí, systém, whatever. Ja ťažko.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Kristian Takac
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Kristian Takac
            
         
        kristiantakac.blog.sme.sk (rss)
         
                                     
     
        Patrim do kategorie tych, co ked rano otvoria noviny, nevedia ci sa maju smiat a ci plakat...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1275
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




