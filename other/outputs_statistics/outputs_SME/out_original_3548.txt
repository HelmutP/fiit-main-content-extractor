
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Kuľhavý
                                        &gt;
                Moja teória - POLITIKA
                     
                 Len idiot môže napísať...alebo nádych liberálnej politiky... 

        
            
                                    29.3.2010
            o
            9:57
                        (upravené
                29.3.2010
                o
                12:20)
                        |
            Karma článku:
                5.48
            |
            Prečítané 
            1222-krát
                    
         
     
         
             

                 
                    Otázkou je určite pritiahnuť k volebným urnám mladých ľudí, v tomto spektre voličov môže byť veľká sila, tá sa dá využit atraktívnym volebným programom, to sa darí strane SaS, otvorila kontroverzné témy ako: marihuana, generačná výmena, kandidáti bez korupčných škandálov. Voliči generácie našich starých rodičov su stabilnými aktívnymi voličmi, o ich podporu sa strany môžu pevne opierať, ale práve mladí ľudia zohrajú v týchto voľbách dôležitú úlohu...
                 

                 VOĽBY 2010:    Strana SaS sa pustila do veľmi efektívneho využitia sociálnej siete, informuje svojich sympatizantov a fanúšikov o ich vyjadreniach, novinkách... Dôležite je aj to, že strana získala poprednú známu osobnosť, ktorá tým, že sa stáva jedným zo štyroch lídrov citlivo a potichu v očiach verejnosti upevnila strane svoju pozíciu na politickej scéne "kvázidôveryhodnosťou" a volebné preferencie naznačujú, že SaS svoj potenciál využíva veľmi efektne a dobre. Richard Sulík mi nepripadá ako typ - "od škriepky záleží". To, že strana je populistická je snáď jasné každému, ale ktorá nie je? Populizmus je choroba, ktorá prirodzene patrí k politike,(neospravedlňujem ju) patrí tam preto, lebo je účinná. Prirovnal by som to k bulváru, väčšina ľudí vedome vie o aké informácie tam ide, z titulky sa dozviete vlastne všetko a to čo je napísané pod ňou je niekedy naozaj len chabým rozvitím, ktoré nič nové neprinesie. Ale je najčitanejší. Nech už si povieme: "Sú to blbosti" - určitým spôsobom ste sa určite všetci presvedčili, že vás to viac či menej oplyvnilo a na "celebritu" sa pozeráte "trošičku" inak.   Moje presvedčenie je v tom, že SaS sa parlemantnou stranou stane, možno s lepšim volebným výsledkom, ako napovedajú preferencie. (Mňa a mojich známych, kamarátov, priateľov sa nikdy žiadny prieskum nič nepýtal). Vsadiť môže na naivitu mladých prvovoličov. Stále som presvedčený, že 18 rokov je málo na názorové vyzretie a je aj málo na to, aby si "18+ dospelák" uvedomil, akú veľku zodpovednosť vo volebnej miestnosti má vo svojich rukách. Práve takýto človek je ľahko ovplyvnitelný len pocitom, žiadnou skúsenosťou.   KOALÍCIA / OPOZÍCIA:   Každej novej strane, môže uškodiť okamžitý vstup do koalície, tak ako prípad liberálnej strany Aliancia Nového Občana, naopak môže pomôcť, byť pár rokov v opozícii, ako prípad strany Smer. Vyprofilovať si program, stabilizovať voličov, členskú základňu, na to už asi dnešná pravica čas mať nebude, lebo zmena je určite z môjho pohľadu potrebná, priam nevyhnutná.   VETA NA ZÁVER:   Tento článok je len MOJA TEÓRIA, preto budem rád, ak sa vyhne osobnému napádaniu, ale naopak bude článok podrobený konštruktívnej kritike,nepíšem pre každého, píšem pre voľnomyšlienkárov a otvorených ľudí, čo sa snažia vidieť svet v súvislostiach, nie v politických farbách.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kuľhavý 
                                        
                                            Dávajú nám to, čo chceme - informačný smog
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kuľhavý 
                                        
                                            Simulácia voľného pádu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kuľhavý 
                                        
                                            Slovenské more - 3 časť - Zadar a Vrgada 3/3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kuľhavý 
                                        
                                            Slovenské more - 2 časť - No limit, Kornati 2/3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kuľhavý 
                                        
                                            Slovenské more - 1 časť - Dobro došli  1/3
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Kuľhavý
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Kuľhavý
            
         
        martinkulhavy.blog.sme.sk (rss)
         
                                     
     
        basgitarista, bloger  a hobby fotodizajnér  

Osobný blog: 
mkkoment blog

 
Návštevnosť: 




 
 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    41
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    548
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Popísané
                        
                     
                                     
                        
                            Poezia - texty
                        
                     
                                     
                        
                            Tip pre android
                        
                     
                                     
                        
                            Moja teória - POLITIKA
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




