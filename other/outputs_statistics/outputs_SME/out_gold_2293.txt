

 Podobenstvo o ľudskej spravodlivosti 
 Zlodej sa vybral uprostred noci vylúpiť dom. Keď sa vyškrabal, na parapetu okna, tá pod ním povolila, zlodej spadol a zlomil si nohu. 
 Ráno  s nohou v dlahách udal majiteľa domu za ublíženie na zdraví, lebo tak neporiadne dal urobiť okno, že  mu spôsobil ujmu na zdraví. Sudca si teda predvolal majiteľa domu ako obžalovaného. Majiteľ domu sa ale necítil byť vinným, tvrdil že na vine je tesár ktorý vyrábal okno. Určite on ho urobil neporiadne, a tak si chudák zlodej zlomil pre jeho lajdáctvo nohu. 
 Poslali teda pre tesára. Tesár sa od ľaku triasol, že čo s ním bude, ale spomenul si ako to bolo keď robil okno na ktorom si zlodej zlomil nohu. " Ja som nevinný, pán sudca. Keď som robil to okno, išla okolo pani v červených šatách a tie boli tak krásne, že som musel za ňou hľadieť a tak som neporiadne urobil to okno." 
 Dali zohnať pani v krásnych červených šatách. Nebolo to ťažké, také šaty si všimne každý. Pani  pred súdom hneď vedela čo má povedať: " Ja nemôžem za to že sa za mnou pozeral tesár a tak zle urobil okno. Vinníkom je farbiar ktorý namiešal tak krásnu červenú farbu, že sa musel každý za mnou otáčať. 
 Takže farbiar ... Taký šikovný farbiar bol v meste len jeden, a toho predvolali na súd. Sudca ho obvinil z toho že je vinný za zlomenú nohu  zlodeja, lebo ten si zlomil nohu na ráme okna, ktorý vyrobil tesár hľadiaci na šaty ktoré on farbil a preto to okno urobil zle. 
 Rozsudok znel: "Smrť obesením vo verajách dverí súdu." Farbiar bol ale taký vysoký že ho vo dverách súdu obesiť nemohli. Vyhľadali teda nízkeho farbiara a toho obesili. Rozsudok bol tým naplnený a spravodlivosti učinené ... 

