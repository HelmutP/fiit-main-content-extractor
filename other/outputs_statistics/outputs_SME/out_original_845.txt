
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Aneta Čižmáriková
                                        &gt;
                Kreatívec notorik
                     
                 Swarovski v striebre - prívesky a náhrdelníky 

        
            
                                    15.9.2008
            o
            15:00
                        |
            Karma článku:
                10.56
            |
            Prečítané 
            11219-krát
                    
         
     
         
             

                 
                    V dvoch predchádzajúcich článkoch som predviedla ukážky ručne vyrábanej bižutérie z komponentov značky Swarovski. Ako som spomínala, okrem korálok a príveskov Swarovski sa v obchodoch pre šikovné ruky dá kúpiť aj striebro.   Výsledkom môžu byť šperky z pravého striebra so Swarovski krištáľmi... vyrobené jednoducho a rýchlo, hoci pri kuchynskom stole.   A opäť vám poradím, ako na to.
                 

                  Strieborných komponentov, ktoré dostať kúpiť (najlepšie v internetových obchodoch), je taký výber, že strieborné šperky vlastne ani netreba vyrábať... stačí ich skladať.  Napísala by som, že začnem ukážkou najľahších postupov, ale v duchu predchádzajúcej vety, ktorá verím že platí, vlastne ani nemám žiadne zložité Ag+SW šperky. Všetko je to len o fantázii a možnostiach - prezerám si korálky a strieborné komponenty a predstavujem si, ako sa dajú skombinovať.   Výsledok sa často podobá - a pokiaľ sa to netýka originál Swarovski dizajnu, vlepovaných krišťáľov či teplom opracovaného striebra - aj vyrovná tovaru ponúkanému v obchodoch. Veď je to vlastne rovnaký postup výroby, ibaže ak vás to baví a máte záujem, spravíte si za zlomok ceny doma to, čo v obchode  stojí štvornásobok, lebo to za vás poskladal niekto iný...      ♪♫♪♫♫♪      Úplne najjednoduchšie je kúpiť si korálku, šlupňu správnej veľkosti a spraviť si prívesok. Šlupňa sa kliešťami roztiahne, jeden kolíček sa vloží do dierky prívesku a opatrne kliešťami (alebo prstami) zase stlačí.        Základná strieborná šlupňa vyzerá takto →      Výsledok takto:        použité Swarovski srdiečka, vľavo 10,3 x 10 mm AB, vpravo 14,4 x 14 mm Light Siam       odtieň Crystal Vitrail Medium má rovnako ako Crystal AB na zadnej strane pokov   a na Korálky.sk pribudol len nedávno        Mno, ak nejde práve o prívesok, ja osobne sa šlupniam radšej vyhýbam. Sú predsa len trochu masívnejšie než napríklad spojovacie krúžky a treba s nimi zaobchádzať silou a zároveň jemne - pri troške nepozornosti či zlom odhade sily hrozí buď poškodenie krištáľu (avšak nič tragického - plôšky po odštiepených kúskoch vidieť iba veľmi zblízka) alebo stlačenie či poškriabanie striebra, ktoré je pomerne mäkké.     TIP: Na kapitalistickom Západe samozrejme vyriešili aj toto - majú kliešte s nylonovými vložkami na čeľustiach. V ČR ich predáva napr. Rooya, na Slovensku zatiaľ asi nikto. My tu na Východe to riešime tak, že si čeľuste klieští oblepíme leukoplastom ;-)          ♪♫♪♫♫♪       Rovnako ľahko a rýchlo sa dajú vyrobiť náušnice.   Samotná šlupňa je vhodná len na zavesenie na retiazku, inak by vyzerala čudne, ale dostať kúpiť aj náušnice s už pripravenou šlupňou ;-)  Šlupňa sa roztiahne, korálka nasadí, zatlačí a náušnica je hotová.    (Môžete však použiť aj šlupňu s očkom + bežný náušnicový háčik.)    Tak vznikla moja prvá, ešte pomerne skromná súpravička z 10,3 mm srdiečkami v odtieni AB:           Neskôr s vymeneným väčším príveskom:           ♪♫♪♫♫♪                 Asi sa vám zdá, že je tu už presrdiečkované... no malé srdiečka patria medzi cenovo najdostupnejšie Swarovski prívesky, preto som práve na nich robila prvé krôčky s krišťáľmi. Keď sa mi nazbierali vo viacerých farbách, dostala som chuť spraviť si niečo zaujímavejšie, než len jeden prívesok na retiazke. Čo tak dať ich tam viac? Zostavila som retiazku, špendlíkmi ju napichala na kus polystyrénu a prepočítala polohu príveskov. Sú celkom maličké, takže vyzerajú jemne. Možno až príliš na retiazke s väčšími očkami :-)                srdiečka sú v odtieňoch Olivine, Aquamarine, Light Siam, Blue Zircon a Light Rose       Na pripojenie som použila moje obľúbené 6 mm strieborné krúžky. Pre drobné korálky ich priemer vyzerá síce nevhodne veľký, ale majú inú výhodu - menšiu hrúbku, ktorá lepšie ladí s ostatnými striebornými komponentami a retiazkami. Drobnejšie krúžky hrubé 1 mm sa dajú síce kúpiť vo viacerých priemeroch (4-5 mm), ale horšie sa s nimi manipuluje, vyzerajú masívne a namiesto šlupní sú úplne nepoužiteľné, lebo sa do korálkových dierok nedajú vložiť. Používam ich na konci retiazok a na všetko ostatné mám radšej tieto šesťmilimetrové.    ♪♫♪♫♫♪           Ale pretože som straka milujúca trblietavé vecičky, neskončila som pri centimetrových srdiečkach... Vymyslela som si veselý letný náhrdelník ovešaný swarovským a začala prezerať ponuku väčších príveskov - farby, presné veľkosti i vhodnosť tvarov. Po dlhom vymýšľaní a kombinovaní som ešte doplnila k materiálu striebornú ROLO retiazku, ktorá sa mi páčila svojím moderným vzhľadom.         Zľava v smere hodinových ručiek náhrdelník obsahuje prívesky Snowflake 20 mm   Crystal AB, Cosmic 20 mm Violet, Starfish 20 mm Peridot, 2 Sprig Coral 25 mm   Aquamarine a Cross 20 mm Light Rose                ♪♫♪♫♫♪       Mimochodom, retiazky... Ani s tými nie je žiaden problém s domácou výrobou. Vyberiem si v e-obchode aká sa mi páči (očká musia mať aspoň 2 mm), asi tak 40 centimetrov, pridám k nej tri krúžky, rýdzostné očko a karabínku, klieštikmi pripevním na konce a retiazka je hotová. Stojí ma to podľa druhu retiazky 140-240 SK. Absolútne ju nerozoznáte od kupovanej a že nie je zvarená? Nedávno mi kamarátka ukazovala striebornú retiazku z obchodu a skoro mi zabehlo, keď som videla, že ju niekto vyrobil presne tak, ako ja doma - iba kliešťami :-) Koniec-koncov je to úplne jedno, oba druhy sú rovnako pevné pri bežnom zaobchádzaní a rovnako ľahko sa roztrhnú, ak ich silno potiahnete.           V nedávnej dobe som dostala zásielku nových Swarovski príveskov (napr. vyššie odfotené srdce Vitrail Medium) i striebra a hneď som realizovala navrhnuté náčrtky. Už som ich stihla nafotiť a čoskoro ich ukážem v ďalších príspevkoch, spolu so skôr vyrobenými súpravami a náušnicami.            Obrázky strieborných komponentov © Korálky.sk, použitý materiál z toho istého zdroja :-)         P.S. Ak ste náhodou nerozumeli niektorým výrazom (napr. AB), odporúčam prečítať si úvodný článok   o krištále Swarovski alebo ďalšie články o výrobe bižutérie v rubrike Kreatívec notorik (linky sú aj tu v súvisiacich článkoch).         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Zmena je život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 4. časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 3. časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Za čo ma buzeruje anonymný admin SME blogov?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 2. časť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Aneta Čižmáriková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Aneta Čižmáriková
            
         
        cizmarikova.blog.sme.sk (rss)
         
                                     
     
        Tento blog je od 16.4.2009 uložený k spánku, nájdete ma tu:    ~~~ Riddick's Realm ~~~   
  
aj tu:   


        
    
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    199
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Knihy
                        
                     
                                     
                        
                            Z knihovničky knihovníčky
                        
                     
                                     
                        
                            Moje knižné lásky
                        
                     
                                     
                        
                            Sci-fi a Fantasy
                        
                     
                                     
                        
                            SF&amp;F cony
                        
                     
                                     
                        
                            • Dialógy o fantastike •
                        
                     
                                     
                        
                            Fanovinky
                        
                     
                                     
                        
                            Blogový nedenník
                        
                     
                                     
                        
                            Rozpravy o Islande
                        
                     
                                     
                        
                            Všeli-len-čo
                        
                     
                                     
                        
                            Kreatívec notorik
                        
                     
                                     
                        
                            Bavíme sa s Tubou
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            čitateľka, mama, Milenka
                                     
                                                                             
                                            Oľga Pietruchová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            - od roku 2005 hodnotím filmy
                                     
                                                                             
                                            - ďalej píšem o knihách
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  




     






            
    
        
    
  
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 


 


