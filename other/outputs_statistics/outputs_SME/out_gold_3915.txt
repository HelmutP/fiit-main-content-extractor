

  Moja odpoveď , že som občan, občan a ešte aj trochu sused od vedľa mi v ničom nepomohla. Zdá sa, že byť občanom, špeciálne byť občanom v našom parlamente nič veľké neznamená. Hľadať Slotu alebo iného poslanca v parlamente môže v dnešnej dobe len iná „kapacita", a nie len nejaký občan. 
 Treba sa pokúsiť  aspoň čiastočne vrátiť starú zašlú vážnosť občanovi. Vážnosť, ktorej sa občan tešil už v starovekom Grécku. Vážnosť občanovi, ktorému dnes už ruže nekvitnú a stáva sa pomaly existenčne ohrozeným druhom. 
 Existenčne ohrozeným nie v tom zmysle, že na to, aby niekto mohol byť občanom, musí byť aj agentom niekoho alebo niečoho, a zákonite musí byť niekým platený, ale existenčne ohrozený v tom zmysle, že je ohrozeným druhom zapísaným v červenej knihe, druhom  odsúdeným na vymretie a špeciálne práve na Slovensku. 
 Skúsime v sobotu robiť Občianske senáty na zaujímavú tému, so zaujímavými hosťami, kde občania, ktorí sa budú chcieť zúčastniť senátu, budú srdečne vítaní. 
 V sobotu 10.4.2010 od 16,00 bude Občiansky senát v priestoroch bývalého kina Praha na námestí SNP 8 / v súčasnosti to je veľká bratislavská reštaurácia, názov uvádzať nebudem, aby to nebolo posudzované ako reklama/. 
 Mám pocit, že ako hosť by mohol byť zaujímavý spisovateľ Hvorecký, ktorého týmto pozývam a verím, že pozvanie prijme. 
 Aká bude téma a koho ďalšieho pozveme bude závisieť od toho, či spisovateľ Hvorecký prijme pozvanie. V prípade, že áno, budeme informovať. 
   
   

