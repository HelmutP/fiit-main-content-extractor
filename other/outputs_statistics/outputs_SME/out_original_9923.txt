
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Panelák ako domáci poklad 

        
            
                                    1.7.2010
            o
            5:41
                        (upravené
                1.7.2010
                o
                5:47)
                        |
            Karma článku:
                12.45
            |
            Prečítané 
            3025-krát
                    
         
     
         
             

                 
                      Aby ste mi dobre porozumeli, tak celkom presne bude reč o televíznom seriáli Panelák na obrazovke televízie JOJ a samozrejme predovšetkým o tom, aká je naozajstná hodnota všetkého, čo nám v svojej dramatickej činorodosti ponúka. Tým mám na zreteli formát, herecké obsadenie, jednotlivé scenáre epizód, réžiu a čo považujem za najdôležitejšie, myšlienkové posolstvo. Lebo raz darmo, ak je niečo v ponuke naozaj denne, ak to má ten najlukratívnejší vysielací čas a ak sa nám to prezentuje sloganom – najlepšie seriály sú na JOJ –ke, naša pozornosť by mala byť seriózne aktivizovaná.
                 

                     Priznám sa, mám už naozaj dosť rokov i skúseností, aby som mohol svoje tvrdenie, že viem o živote skutočne veľa - ba naviac, kultúra vo všetkých podobách je mojim veľkým koníčkom, takže si trúfam aj v tomto smere zaujať stanovisko - povedať niečo celkom nahlas. Ak sa náhodou nestotožním s vašim vkusom, prepáčte, ale presne v intenciách tvrdenia, že čoho je moc, toho už je priveľa, mlčať nemôžem,   Seriál Panelák  je na obrazovkách televízorov už dlho, momentálne je za nami už jeho V. séria. Rád priznávam, že som príbehy hrdinov z menšej bytovky sledoval takmer od začiatku, objavoval som v ich osudoch presne to, s čím sa boria ľudia v podobných podmienkach všade okolo mňa a tak trošku so zadosťučinením som sa presviedčal, že máme rovnaké danosti i starosti, že nás trápia rovnaké denné i nočné mory, že nežijeme v inom vesmíre a takmer všetkému v jednotlivých epizódach sa dalo veriť. Žiaľ, tomu všetkému je takpovediac amen!   Z ničoho nič sa energia vývoja všetkých postáv preniesla do roviny „bujarého sexu“ a krédom všetkého diania je odpoveď na otázku, kto koho, kto s kým, kto ako a kedy a čo za to, alebo, čo je tiež celkom ľudské, nech sa na to nepríde!   Zdá sa vám, že preháňam? Prisahám, že vôbec v ničom, ba celkom úmyselne sa držím poriadne na uzde, ale žiaľ, tam kde bola človečina, kde bol ľudský osud hodný pozornosti, tam dominuje živočíšny sex. Švehlová podvádza svojho Jakuba s Bajzom, na tohto primára gynekológie si brúsi zuby vlastne taká menšia armáda žien širokej vekovej škály, Agátkin tatko celý zmysel svojho života nasmeroval k dobrodružstvám s mladými a radodajnými, barmanka s úžasným citom pre deti z ničoho nič bojuje s dilemou, či v jej živote náhodou neprevládajú doteraz dobre utajené lesbické danosti, ťuťko Jakub si tajne brúsi zuby na svoju šikovnú zamestnankyňu Niku a z tej sa zasa z ničoho nič vyprofilovala snáď ešte horšia nymfomanka i podvodníčka, než je všetkých naokolo preťahujúca Zdenka. Našťastie, väčšinou iba v rovine verbálnej, lebo potom by sa to na normálnom vysielacom kanáli nedalo ponúknuť verejnosti, ale jasnejšie prognózy divák dostať nemohol! A keď už je to nezvládnuteľné, potom sa voľačo prezradí, objavia sa nové sexuálne príhody s tým najneuveriteľnejším podtextom, ba padne aj facka a blesková emigrácia ako útek pred zákonom, takže sa divák nenudí. Žiaľ, už naozaj iba „čumí“, lebo takmer ničomu dávno neverí.   Pravdaže, všetko v sexuálnej rovine, čo seriál dennodenne ponúkal, som nemal šancu vymenovať, ba už som sa aj ako divák ulieval. Je toho viac ako veľa a ak to má byť obraz toho, aký sme národ, aké sú naše kvality a danosti a čo svetu ponúkame, stáva sa zo mňa strašný skeptik a človiečik, čo má chuť voľakde sa dobre ukryť a preventívne vykrikovať, že ja nie som z tej peknej krajiny od Váhu a Tatier, kde takýto seriál prišiel k rodnému listu.   No nič, istotne oceňujete, že v hodnotení Paneláku som vynechal mená tvorcov. Len im všetkým pripomínam, že žiadna guma sa nedá naťahovať donekonečna a presne tak je to aj s každým príbehom, čo ako dobrým a zaujímavým. Potom už všetkému dominuje klišé, túžba vyhovieť vkusu väčšiny na konto umeleckých ambícií i serióznosti a tvorcovia na celom svete dobre vedia, že veľa naozaj neznamená, že stále dobre. Panelák je teda domáci poklad s čoraz menšou hodnotou a ak mu má zostať aký - taký valór, nech má čím skôr derniéru, aby sa jeho postavičky nestali obyčajnými kreatúrami.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (92)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




