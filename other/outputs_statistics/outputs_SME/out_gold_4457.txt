

 
 
 Zo Štokholmu sme vyplávali asi pred hodinou. Parník sa kolíše na vlnách Baltického mora, z ktorého sa nad hladinu vynára vyše dvadsaťtisíc ostrovov. Na niektorých stojí iba jeden dom a jedno mólo, pre jednu plachetnicu. Aj starý muž pôsobí osamelo. Spolu s ním sa pomedzi ostrovy plaví možno dvesto ľudí, ale on cestuje sám. S nikým nenadväzuje rozhovor, ani očný kontakt. Sedí v salóniku tohto parníka, podobne, ako sa pred storočím plavil na ostrov Hemsö spisovateľ August Strindberg, aby opustil svoj štokholmský byt a v letnom dome nasal silu z prírody. 
 Starý muž zjedol svoju žemľu a dopil svoje pivo, ale noviny ešte nedočítal. Sú hrubé, víkendové. Škandinávci sú najvášnivejší čitatelia denníkov v Európe. Starý muž vytrvalo, so zaujatím stále číta. Nezdvihne zrak, ani keď cvakne uzávierka fotoaparátu. Má zvráskavené ruky, tvár, peknú bielu košeľu a pekné biele vlasy. Vyzerá ako postava z Bergmanovho filmu. 

 Nepostrehol som na ktorom z ostrovov vystúpil a dnes sa to už nedozviem. Akoby sa v parníku mihol iba na okamih a väčšina cestujúcich si ho zrejme ani nevšimla. Napriek tomu bol svojim spôsobom neprehliadnuteľný. 
   
   

