

 Takmer všetky vypredané koncerty v dvanástich slovenských mestách zvestovali, že nitrianska skupina Horkýže slíže opäť raz zabodovala a potvrdila post našej najlepšej koncertnej skupiny aj pri prezentácii svojho najnovšieho albumu 54 dole hlavou.  Vynikajúco im sekundovala punková legenda Konflikt, predkapelou boli Fuera Fondo a celé turné prebehlo pod ochranou najväčšieho slovenského festivalu Topfest a pudu južného. Turné odohrávajúce sa v halách odštartovalo v bratislavskom PKO za prítomnosti kamier a skončilo v Ružomberku. Počuť Kukove "perličky", ktoré sa nacvičiť nedajú a ani sám Kuko nevie, kedy ho čo napadne, je naozajstným zážitkom. Boli to dve hodiny hrania, zábavy, odznelo 28 skladieb. Úvodná pieseň Bernardín rozhýbala všetkých a vrenie sa stupňovalo každou ďalšou piesňou. Odzneli notoricky známe hity ako Maštaľ, Vlak, Náboženské zvyky, Brďokoky, z nového albumu napríklad Eva je v sádre. V každom meste mal jeden šťastlivec šancu zahrať si pesničku Líza a Wendy priamo na pódiu a zároveň si odniesol domov novučičkú gitaru. Počas piesne Shanghaj Cola sa rozdávala špeciálna edícia koly. Holandská začínala paródiou na jednu z prvých tanečných show. Song Ukáž tú tvoju Zoo bol venovaný víťazke Eurosongu slovami "Kristína, ukáž tú tvoju zoo". K Slížom sa v závere koncertu pridali aj predkapely - s Fuera Fondo odspievali Trápny vietor a Konflikťáci boli "perličkou" v piesni Silný refrén. Mesiac, počas ktorého brázdili títo hudobníci naše mestá, priniesol fantastické zážitky - ponúkame vám možnosť pripomenúť si ich prostredníctvom záberov objektívu. 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 foto : Daniel Lipár text : Sisa Rákociová 
   

