

 Na jeho vykrúcanie sa sme už vcelku zvyknutí a to, že čokoľvek prizná určite nikto neočakával. Týmto neprekvapil. Ani tým, že redaktorovi nadával do hlúpych, nenormálnych, nekompletných a jeho prácu označil za hrubý primitivizmus. Dokonca aj na takéto vyjadrovanie sme si za uplynulé štyri roky bohužiaľ zvykli. To, čo priam šokovalo bola táto reakcia na adresu redaktora Vagoviča zo SME: "Keby som nebol v politike, tak by som si to asi s vami vybavil inak. Ale som v politike." 
 Zaujíma ma, ako by si to pán premiér s pánom Vagovičom chcel "vybaviť". Zbil by ho? Dal by ho zbiť? Prepichol by mu pneumatiky? Alebo niečo horšie? 
 Našťastie to nezistíme, lebo ako sám hovorí, bráni mu v tom to, že je v politike. Slušnému človeku obyčajne bráni svedomie. 
 Máme sa teda začať vážne báť toho času, keď pán premiér v politike nebude a začne si účty vybavovať "inak", než zákonnou formou? 
   
   

