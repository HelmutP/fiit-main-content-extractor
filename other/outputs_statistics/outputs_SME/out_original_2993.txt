
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Darina Paprčková
                                        &gt;
                Nezaradené
                     
                 Goodbye Greenery ! 

        
            
                                    20.3.2010
            o
            20:05
                        (upravené
                20.3.2010
                o
                20:53)
                        |
            Karma článku:
                7.41
            |
            Prečítané 
            802-krát
                    
         
     
         
             

                 
                    Ulica, ktorá vedie k môjmu domu, sa dlhé roky ani o kúsok nezmenila. Na to, že bývam v Bratislave, iba chvíľku od centra mesta, môžem byť spokojná, že moja štvrť sa ako jedna z mála  môže popýšiť zeleňou. Teda, až do dnešného dňa ....
                 

                 
  
   Ešte včera večer ulicu lemovali desaťročia staré stromy, ktoré sú tu dlhšie ako mnohí obyvatelia tejto mestskej časti. Dnes na obed sa mi však naskytol pohľad, ktorý ma doslova na pár sekúnd zmrazil. Časti stromov ležali spílené na zemi, chodník odrazu pôsobil smutne, akoby bež života.   Možno si poviete - "No a čo ? Zopár stromov predsa nikomu chýbať nebude." Ale tu predsa nejde iba o jednu ulicu. Všetci dobre vieme, že už nielen v mestách platí nové pravidlo - čím menej zelene, tým lepšie. Aj tak mi zatiaľ nie je jasné, prečo práve na tomto mieste museli byť odstránené. Nikomu nezavadzali, na okraji chodníka sa dá asi len ťažko niečo stavať, takže dôvod ostáva pre mňa záhadou.   Nepatrím k žiadnej fanatickej organizácii za záchranu zelene, to nie, som iba obyčajný človek, ktorému sa už veľmi dlho prieči tento spôsob budovania miest. Vyrubujú sa parky, ničia sa lúky, len preto, aby sa mohlo stavať, stavať a stavať. Prestavajte sa všetci, žite hlava na hlave v betónovej džungli, veď to je predsa IN ! Už iba blázon by mohol vedieť oceniť kúsok zelene uprostred rušných križovatiek a panelákov...   Ničíme aj posledné kúsky prírody a potom ochkáme a jojkáme, ako nám je zle, aké prírodné katastrofy sa na nás rútia a nevieme si ich ani poriadne vysvetliť, no to, že sme iba obeťami samých seba, to si prizná iba málo ľudí. Globálne otepľovanie, skleníkové efekty, to nie sú bubáci vyslaní Matkou Prírodou, ako si to mnohí slepo namýšľajú ( áno, existujú aj takí..), to sú výsledky vedomého, niekedy aj nevedomého devastovania Zeme..   Nepotrebujeme apokalyptické predpovede na rok 2012, veď rovnaké sa viazali aj na rok 2000 a vidíte, ešte stále tu sme, no hádam skoro každý si vie spočítať 1+1 a prísť na to, že takýmto tempom ničenia a rozrušovania tejto planéty budeme aj bez obávaného roku čoskoro, slušne povedané, v análnom otvore.. Keby sa aspoň polovica svetovej populácie na chvíľu zastavila a zamyslela, možno by sme to všetko mohli zmierniť.. ostáva dúfať... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Darina Paprčková 
                                        
                                            Vianoce, sviatky pokoja a radosti ?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Darina Paprčková 
                                        
                                            Anjel na D1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Darina Paprčková 
                                        
                                            Slamka moja milovaná...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Darina Paprčková 
                                        
                                            Deň ako každý iný
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Darina Paprčková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Darina Paprčková
            
         
        darinapaprckova.blog.sme.sk (rss)
         
                                     
     
        A dreamer is one who can only find his way by moonlight, and his punishment is that he sees the dawn before the rest of the world.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1078
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




