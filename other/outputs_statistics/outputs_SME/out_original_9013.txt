
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Matuška
                                        &gt;
                Nezaradené
                     
                 Utópia, či? 

        
            
                                    16.6.2010
            o
            23:13
                        |
            Karma článku:
                4.37
            |
            Prečítané 
            741-krát
                    
         
     
         
             

                 
                    Pravidlá sú údajne na to, aby sa porušovali. Ja to tak „rebelantsky“ nevidím. Na to slovné spojenie som doslova alergický a keď to dokonca vyhlási niekto za volantom, tak si veru odo mňa auto nepožičia. 
                 

                 Ja by som sa teraz ale rád venoval inému „trošku“ staršiemu zákonu. Desatoru božích prikázaní.   Zákon daný Bohom Mojžišovi, na hore Sinaj, je síce základným pilierom niekoľkých náboženstiev, no keby ho dodržiava každý, svet by asi vyzeral inak.    Pojmem to rečou peňazí, tej vraj rozumie každý. Chcem sa venovať len dvom príkladom ktoré mi padli do oka.   1. Nezabiješ. Tu je každému jasné že ľudský život nik nemôže dať, preto ho ani nemôže vziať. No tá formulácia sa mi zdá odjakživa relativizovaná. Tam nie je nezabiješ, okrem vojnového konfliktu, okrem... Keby každý človek rešpektoval toto prikázanie neboli by napr. vojny. Viete si predstaviť ten ohromný kapitál ktorý dnes vo svete ide na zbrojenie, presunutý napr. do sociálnej starostlivosti? Ale poďme ďalej.  2. Nepokradneš. Aký by bol asi svet, keby nik nekradne? Nie len ten prvý efekt, že by niekomu kto poctivo drie, nik niečo iba tak nezobral.  Poisťovacie podvody podľa odhadov odborníkov zvyšujú cenu poistenia približne o dvadsať percent. Taký je európsky priemer. Čiže lepšie by bolo i tím ktorých sa zlodejčina „netýka“ . Ďalej nebolo by potreba kapitálu na vývoj rôznych zabezpečovacích prostriedkov, SBS služieb atď.   Viete si predstaviť niečo také? Že by minimálne tieto dva zákony dodržiaval úplne každý? Že je to nemožné? Že žijem v inom tisícročí?? Nemyslím.  Stačí keď sa tím desatoro začne riadiť aspoň jeden človek ktorý toto číta. Stačí keď ten človek o tom že sa desatoro oplatí, presvedčí ďalšieho, aspoň jedného človeka. A ten zas aspoň jedného a ten aspoň... A kto vie, možno raz...  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Prežitie v chotári. Pavol Satko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Znechutenie z blogov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Tridsať metrov pod morom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Dekriminalizácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Loď do neznáma
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Matuška
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Matuška
            
         
        matejmatuska.blog.sme.sk (rss)
         
                                     
     
        Som človek ktorého zaujíma všetko a nič.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    709
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Slovensko sa opäť blyslo neúčasťou na veľtrhu IHM
                     
                                                         
                       Slovensko už nemá žiadne remeslá?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




