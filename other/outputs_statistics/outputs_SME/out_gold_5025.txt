

   
 Tak aj otvorením nového domu komunity Jeruzalem v srdci mesta Varšavy pre Poľsko sa otvára nová etapa tejto komunity – prvého domu v postkomunistických krajinách. Komunite sa podľa liturgických zásad II. Vatikánskeho koncilu podarilo dobudovať aj nový kostol, ktorý bol počas II. svetovej vojny zničený a liturgie sa budú v ňom slúžiť každý deň ráno o 7.00, na obed o 12.30 a večer o 18.00,, povedal Irenej-Mária, jeden z bratov komunity. Predstavený komunity bratov vo Varšave brat Benedikt, pôvodom z Moravy, po 10 rokoch formácie v Paríži a Kanade po stopách sv. Vojtecha prišiel do Poľska kde spolu s bratmi a sestrami komunity Jeruzalém vpíše tak novú etapu histórie komunity v Poľsku. Kniha zakladateľa o spiritualite bratstva Jeruzalém - Kniha života sa pripravuje na vydanie v slovenčine v najbližších mesiacoch. 
   
   
   
   
 Mníšske bratstva Jeruzalem, 
 sa skladajú z dvoch rehoľných inštitútov bratov a sestier, majú za svoje povolanie hĺbiť na „púšti veľkomiest“ oázy modlitby, mlčania a pokoja. 
 Túžia predovšetkým  ,,rozvinúť koberec modlitby na uliciach veľkomiest". 
 Založená bola cez slávnosť  Všetkých Svätých v roku 1975 pri  parížskom kostole Saint-Gervais-Saint-Protais, ako odpoveď na túžbu kardinála François Marty vtedajšieho arcibiskupa Paríža, a ponuku brata Pierra-Marie Delfieux, vtedajšieho  študentského kaplána na Univerzite Sorbone a zakladateľa Mníšskych bratstiev Jeruzalem. 
 Dnes sa Bratstva okrem Varšavy nachádzajú v Paríži, Vézelay, Štrasburku, na Mont-Saint-Michel, v Bruseli, talianskej Florencii, kanadskom Montrealu, v Ríme, v nemeckom Kolíne nad Rýnom a v dvoch dedinských lokalitách určených k duchovnému cvičeniu: Magdala blízko Orléansu vo Francúzsku a Gamogna v Toskánsku/Taliansku. Pripravuje sa Dom bratstva Jeruzalém v africkom Togu. 
 Tie dve  rehoľné rodiny žijú na týchto miestach zo základnej charizmy spoločenstva, modlitby, práce a pohostinnosti „v srdci miest“ alebo „v srdci sveta“ hľadajúc súčasne život „v srdci Boha“. 
 Ide tiež o zvláštne povolanie ku kráse liturgie, oáze pozývajúca trikrát cez deň, ráno, na poludnie a večer, pre nájdenie pokoja a občerstvenia, k odpočinku uprostred mestského zhonu. 
 
  
 
 Vedľa mníšskych bratstiev existujú  tiež bratstva apoštolské a laické bratstvá. Tie spolu vytvárajú ,,Rodinu Jeruzalém,,.  
   

