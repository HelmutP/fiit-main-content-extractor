
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Monošová
                                        &gt;
                Recenzie
                     
                 Sex v meste 2 - recenzia 

        
            
                                    28.5.2010
            o
            13:29
                        (upravené
                28.5.2010
                o
                14:06)
                        |
            Karma článku:
                4.44
            |
            Prečítané 
            950-krát
                    
         
     
         
             

                 
                    Nakrútiť film, ktorý je voľným pokračovaním kultového seriálu, je výzva. Nakrútiť pokračovanie úspešného pokračovania, ktoré je ešte o pár minút dlhšie, než už i tak dlhý prvý film, je možno až šialený nápad. Po stoštyridsiatich šiestich minútach v tradične zle vetranej kinosále v Poluse som vychádzala s mierne zmiešanými pocitmi. Nepomáhalo mi ani vedomie, že v Amerike si kvôli časovému posunu na svoju premiéru ešte zopár hodín počkajú. Asi som čakala viac.
                 

                 Nechcem tvrdiť, že Sex and the city 2 je zlý film. Nie je. Keby sa jednalo o film úplne nezávislý na nejakom seriáli, skrátka niečo ako drvivá väčšina ostatnej americkej filmovej tvorby, potom by som mohla s čistým svedomím povedať: Choďte na to, je to zábavné a veselé. V čom teda vidím hlavné negatíva?   Nemôžem si pomôcť, ale tento film akoby zabudol na svoje tradičné diváčky. Úrovňou ďaleko zaostával oproti prvému filmu a napríklad vôbec nerozvíja postavu Charlotte, či Mirandy, ktorá je v celom filme úplne iba do počtu. Seriál bol hlavne o Carrie a potom až o všetkých troch, ale tento film je najviac o Samanthe.   Deju žiaľ nepomáha, že Carrie znovu strčili do cesty Aidana - osvedčený spôsob, ako skomplikovať inak vlastne neexistujúcu zápletku! V romantickom prostredí orientálneho trhu by sa nechala zblbnúť aj menej pragmatická žena ako je Carrie - a toto je jeden z prvkov, ktorý mi vo filme zaznel falošne. Veď ona má nielenže Aidana už dávno-pradávno vyriešeného, ale spomeňme si, ako Carrie nedokázala prijať romantické dvorenie na ruský spôsob od Alexandra Petrovského! Dobre, beriem, že stereotypnosť manželstva môže v žene vyvolávať falošné prehodnocovanie vlastných spomienok a citov. Dá sa tiež uveriť, že svoju rolu zohrala aj romantika prostredia. Preto vyznievalo logicky, že sa napokon navliekla do tých kolosálne dokonalých šiat, keď šla s Aidanom na večeru, ktoré by si s ňou vymenila aj vodná víla z reklamy na mattonku. Je pochopiteľné, že sa chcela páčiť a po dvoch rokoch sivosti manželstva sa potrebovala cítiť žiadúca!   Z tohto uhla pohľadu je logický aj ten bozk, ktorý medzi nimi nastal - ale, prepánajána, prečo Carrie tak zblbla, že o tom musela volať Božskému? A prečo sa napokon Božský zachoval ako ťuťko a obaja tak klesli na úroveň postáv ako vyšitých z priemernej mexickej telenovely?! Prečo z nás fanúšikov robia úplných hlupákov? Božský by nikdy nič také neurobil a už vôbec by nekupoval prsteň s čiernym diamantom ako „pamätanie toho, že si vydatá"! Už v prvom dieli bolo to jeho predsvadobné váhanie mierne prehnané, ale tam sa to ešte dalo zniesť, ale: kedy sa z Božského stala papuča? Veď on bol vždy synonymom mužskej citovej nepolapiteľnosti!   Podľa toho, ako bol postavený dej, divák akosi automaticky čakal, že aj Samanthu dostaneme pod čepiec. Rôzne internetové fámy, že Samantha sa objavila pri natáčaní v svadobných šatách a teda ju uvidíme pred oltárom, sa ale nepotvrdili. Bola to možno súčasť reklamnej kampane alebo filmári napokon vytvorili niekoľko variácií jej „konca", ktoré sa možno exkluzívne objavia až na DVDčku. Faktom však zostáva, že Samantha je nositeľkou podstatnej časti deja v druhej polovici filmu, stala sa hlavnou hrdinku a ostatné tri dámy jej iba sekundujú.   Je škoda, že tvorbe scenára nevenovali tvorcovia toľko pozornosti ako príprave prostredia a stavili na osvedčené podvedomé klasiky, ktoré sa vo „filme pre ženy" skrátka museli objaviť. Napríklad krásna svadba hneď v úvode filmu ponúka veľkolepý (akýsi zahrievací) hepening na tému gejovia sa berú. (Liza má stále skutočne kolosálne nohy a to ani nehovorím o jej šarme, ktorému ani čas neubral iskru!)   Ďalším momentom, na ktorý sa nás tvorcovia snažili nalákať bolo, že skoro každá žena (a možno aj niektorí muži) túži po romantickom rozmaznávaní. A tak sú naše štyri hrdinky vrhnuté do exotického prostredia Abu Dhabi, kde ich osud v podobe podnikavého šejka zahrnie hádam všetkým materiálnym luxusom. Každý romantik by si mal prísť na svoje - lenže my nie sme Američania! Podobne falošne mi zaznieva moment, kedy idú naše hrdinky spievať karaoke: dobrý nápad, ale priateľstvo a energia, ktorá z nich mala sršať sa nekonala. Nehovoriac o tom, že Samantha to celé posúva do úplne inej roviny. Jej vyznanie, keď dala prednosť kamarátkam pred novým dobrodružstvom s krásnym neznámym, vyznieva po jej sexuálnych narážkach prehnane premúdrelo.   Podobne, keď sa Carrie rozhoduje volať Božskému a priznať sa, že sa bozkávala s Aidanom, mala ju upozorniť na chybu Miranda, nie Samantha. Ibaže Miranda v celom dlhom filme má iba málo činnosti, a tak je v tomto kľúčovom momente práve opitá, ako sa so Charlotte ľutovali nad tým, že sú matky - nuž: mnohú diváčku-matku táto scéna upokojí a zbaví vlastných výčitiek. Nuž ale, aj tento moment evokuje niečo veľmi podobné telenovele...   Nový Sex v meste neprináša veľa skutočných zápletiek. Hrdinky sú pomerne statické a nezachráni to ani exotické prostredie orientu, ani úžasné módne lahôdky, v ktorých sa naše hrdinky neprestajne objavujú a nad ktorými nejedno oko i srdce zapiští blahom. Nezachráni to ani mierne nasilu nastolená otázka utláčania žien, ktoré sú pod burkami úplne rovnaké ako „my" - neviem, koľko odevov od skutočných módnych tvorcov denne nosí priemerne zarábajúca Európanka. Teda zasa sme raz svedkami, kedy sa s našimi mysľami idú zahrávať na spôsob masírovania amero-ega. Aj tento moment sa vo filme objavuje niekoľkokrát a zaváňa nefalšovanou nadradenosťou.   Film tak skrátka zapadá medzi ostatné americké filmy. Okrem toho, mnohé scény by si zaslúžili poskracovať, lebo im chýba ostrie pointy. Jednotlivé zápletky sa už nedajú veľmi posúvať ďalej, pokiaľ nechceme, aby sa naše páriky porozchádzali. Všetky dobré nápady si vystrieľali už v prvom filme a zrejme aj preto mňa osobne film dostatočne neuspokojil.   Z Carrie robia priam nerozhodnú a zanovitú pubertiačku, akoby hneď od začiatku seriálu neprejavovala dostatok filozofickosti, hĺbavosti a schopnosti nad vecami premýšľať. Iste, občas sa v seriáli aj ona necháva strhnúť emóciami k hlúpostiam, no včera v kine som svoju hrdinku nespoznávala. Dokonca celkom zabudli na klasiku, tj. nechať Carrie napísať nejaké to slovko či vetičku na počítači alebo aspoň na papier, čo má divákovi evokovať jej zbehlosť v písaní. Tu na to akosi pozabudli, čím porušili nepísanú tradíciu každej časti - veď Carriein list sluhovi sa nedá brať ako tvorba spisovateľky.   Samantha, ktorej láska k životu sa prejavuje radosťou zo sexu, rieši vo filme netradičným spôsobom svoj prechod. No nezdá sa mi pravdepodobné, že by v skutočnom živote žena jej inteligencie a obchodných schopností všetko až natoľko podriaďovala závislosti na sexe. „Sex" z názvu seriálu teda „dodržiava" už iba ona - niežeby sme na film chodili kvôli tomu, veď všetci sme si na seriáli a prvom filme obľúbili hlavne to hlboké priateľstvo týchto štyroch žien. No asi aby sa zachovalo dekórum v súvislosti s názvom, tak to táto postava so sexom tak neprimerane a nepravdepodobne preháňa.   Charolotte je matka na plný úväzok a jej postava sa scvrkla na permanentné cupitanie, ktoré ale v seriáli nebolo. Kam zmizla rozhodnosť bývalej riaditeľky galérie a jej schopnosť riešiť problémy?   O Mirande a jej skoro absencii v tomto pokračovaní som písala už vyššie. Hoci má akože problémy v práci a počas pobytu v Abu Dhabi sa snaží učiť ich jazyk, aby napokon zachraňovala situáciu, keď sa všetok luxus a pompa zrúti kvôli Samanthiným avantúrkam, Miranda je v celkom príbehu skoro zbytočná. Klesla takmer na úroveň vedľajšej postavy.   Z našich ikôn, ktoré manifestovali istý postoj k životu, ktoré postupom deja v seriáli a v prvom filme dozreli a dospeli, zo štyroch priateliek, ktoré všetko riešia spoločne ako sestry a s ktorými sme sa mohli do veľkej miery stotožniť, sa v najnovšom pokračovaní príbehu stávajú postavičky, ktoré sú síce vždy skvelo oblečené a upravené, ale zrazu sú nám akési cudzie.   Rozprávanie príbehu Sexu v meste 2 je nepochybne stále štandardne vtipné. Občas sa musíme nad výpoveďami hrdiniek aj zamyslieť či súhlasne prikývnuť nad životnou pravdou. Tento film (až na nepravdepodobnú bohatosť manifestácie luxusnej módy) do veľkej miery odráža skutočný život. Pri každom inom americkom filme by to bolo dostatočné kritérium na jeho obĺúbenosť. No pri Sexe v meste 2 som očakávala omnoho viac.   PS: Ako sa mi vždy páčilo, že "Mr. Big" prekladali v čeština ako "pán Božský", tak sa mi na titulkoch nepáčilo, keď niektoré jemné narážky v angličtine prekladateľka Anna Karaninová (je to skutočné meno?) prekladala viac ordinárne, než bolo treba. Samanthinu narážku, ktorá v angličtine znela: "Tohto Lawrencea by som do svojej Arábie vpustila okamžite!" by takto pochopil aj slovenský divák. Slovo "genitálie" vôbec nebolo potrebné. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nebuďte ovce a poďte voliť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nielen za zvukov hudby, no aj popri jej videoklipe...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Zuhair Murad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Ellie Saab
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Jean Paul Gaultier
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Monošová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Monošová
            
         
        monosova.blog.sme.sk (rss)
         
                                     
     
         O všetkom krásnom alebo šokujúcom, čo ma prinútilo napísať o tom aj čosi iné ako román:-) Som autorka románov (nielen pre ženy): Lásky o piatej (2005, 2012) Anglické prebúdzania (2006) Klišé (2010) Zlodeji bozkov (2012) Sladké sny (2013) Lekcie z nenávisti (2013) Miluje-nemiluje, idem! (2014) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    119
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1068
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Celkom mimo
                        
                     
                                     
                        
                            Mixtúra
                        
                     
                                     
                        
                            Odraz spoločnosti
                        
                     
                                     
                        
                            Móda
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O vzťahoch
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Etela Farkašová: Čas na ticho
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Caro Emerald
                                     
                                                                             
                                            Tiesto
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Súčasní slovenskí spisovatelia
                                     
                                                                             
                                            Iné názory
                                     
                                                                             
                                            Big girl likes fashion too
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            moja osobná stránka
                                     
                                                                             
                                            moda a tak
                                     
                                                                             
                                            super móda
                                     
                                                                             
                                            klasický interiér
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Haute couture jar-leto 2014: Ellie Saab
                     
                                                         
                       Haute couture jar-leto 2014: Jean Paul Gaultier
                     
                                                         
                       Haute couture jar-leto 2014: Chanel
                     
                                                         
                       Haute couture jar-leto 2014: Dior
                     
                                                         
                       Haute couture jar-leto 2014: Valentino
                     
                                                         
                       Akú cenu pre nás má Zlatý slávik?
                     
                                                         
                       Každý deň stretnúť človeka...
                     
                                                         
                       Robert Fico, Vy úbohý frajer, teraz zaplatíte 30 mega z Vašich?
                     
                                                         
                       Ako stratiť lásku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




