

 "Ide o poškodzovanie bilbordov na takej profesionálnej úrovni, že nemôže ísť o obvyklú ľudovú tvorivosť. Nečudovali by sme sa, keby za týmito útokmi boli tí opoziční politici, ktorí sa tvária, že sú najslušnejší na svete." 
 Bravo! Ale čo, súdružka! Čo to tu preboha trepete? Už aj ožratý Jano, ktorý sa po ôsmich pivách vracia večer z krčmy domov a skoro dostane infarkt pri pohľade na tých usmiatych vyretušovaných pajácov na vašich bilbordoch, je tajným agentom opozičných politikov, keď im trocha ponapráva fazónu, aby ušetril svojim kamarátom podobný zážitok? 
 Veď človeka tak pekne sa usmievať na svet už chudák Jano nevidel najmenej štyri roky. Všade sa stretáva len s aroganciou moci, falošnými prázdnymi sľubmi, korupciou, amorálnosťou a kleptomániou súdruhov z vašej sociálnej firmy na nesplnené sny. 
 A vy ho tu obviňujete, že sa zámerne spolčil s opozíciou a zjavne za vysoké provízie nemá po večeroch nič lepšie na robote, ako naprávať úsmevy vašim paranoidným súdruhom. 
 Len im to dajte pekne naservírované na podnose. Všetko oni, to my nie. My sme tí dobrí, oni sú tí zlí. Nepripomína vám to cielenú agitáciu z nejakej eštebáckej príručky? Súdruh predseda vidí za všetkým ideovo odlišné opozičné strany a ich tajných agentov. 
 Janovi odporúčam, aby na tie plagáty nekreslil masky alebo klaunove nosy, ale radšej psychologicky účinnejšie slogany typu: 
 - prečo rastie korupcia? 
 - kde sa stratil právny štát? 
 - koľko generácií bude splácať predražené diaľnice a takmer zdvojnásobený štátny dlh? 
 - prečo je hlavou súdnictva človek podozrivý kontaktmi s mafiou? 
 - kde zmizlo transparentné podnikateľské prostredie? 
 Apropo, kde žijú ľudia, ktorí považujú za najväčší problém v tejto krajine rozdiely v odmeňovaní žien a mužov, keďže na svojom bilborde hlásajú "Za rovné šance?" 
 Priatelia, niečo nie je s touto krajinou v poriadku... 
   

