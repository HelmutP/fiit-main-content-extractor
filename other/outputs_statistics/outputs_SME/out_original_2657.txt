
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Urda
                                        &gt;
                Cestovanie
                     
                 Dôchodcov vlaňajší letný mesiac - 4 

        
            
                                    24.3.2010
            o
            10:02
                        (upravené
                26.3.2010
                o
                7:43)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            4015-krát
                    
         
     
         
             

                 
                    Záverečné pokračovanie prechádzok dôchodcu bez ladu a skladu krížom-krážom po Slovensku.
                 

                 ***DEŇ DVADSIATY TRETÍ***   RAJECKÁ LESNÁ a RAJECKÉ TEPLICE   Doprava vlakom a autobusom RAJECKÁ LESNÁ - Do Rajeckej lesnej chodievame s manželkou a vnučkami každoročne pozrieť nádherný betlehem.       Nádherná práca ľudového rezbára, veľká väčšina postáv je v neustálom pohybe   RAJECKÉ TEPLICE - pekné tiché kúpeľné mestečo zasadené do krásneho prírodného prostredia   Vnučky pri kŕmení labute v kúpeľnom jazierku   Idyla pri kúpeľnom jazierku   ***DEŇ DVADSIATY ŠTRTÝ***  PREŠOV a BARDEJOV   Doprava: ta i späť vlakom   PREŠOV - krásne mesto s mnohými historickými pamiatkami.  Onedlho bude svetovo preslávené vďaka istej soche, ktorá zatieni všetko doterajšie,  čo v meste je, a návštevníci z celého sveta sa sem budú hrnúť viac ako do Ria de Janeiro.  Malá socha v meste už stojí.   Budova divadla, ktoré nesie meno slovenského prozaika, básnika, dramatika, historika, novinára, teológa, kňaza a profesora gréčtiny, rodáka zo Záboria v Turci, prvého slovenského ekumenistu nielen rečami, ale aj skutkami. Rodený evanjelik vyštudoval za evanjelického farára, pôsobil ako evanjelický farár, prestúpil ku katolíkom, pôsobil i zomrel ako katolícky kňaz.   Kópia Turínskeho plátna, ktorá je uložená v krásnom stánku grécko-katolíckej cirkvi v Prešove.  Turínske plátno je kus ľanovej plachty rozmerov približne 4,4x1,1m,  do ktorej bolo zabalené telo Ježiša po jeho sňatí z kríža.  Na plátne sú ostatky krvi od rán na hlave od tŕňovej koruny a od bičovania na tele.  Originál plátna je uložený v Turíne a verejnosti nie je prístupný. Na svete je zopár jeho kópií.   Socha Ježiša v centre mesta. Onedlho má stáť podobná, ale podstastne väčšia železo-betónová nad mestom,  ktorá má prilákať do Prešova viac návštevníkov, ako tá nad Riom v Brazílii. Nádherná socha ako spomienka pre budúce generácie  na nesmrteľného primátora Prešova.   BARDEJOV - Krásne starobylé mesto, zapísané v Zozname kultúrneho a prírodného dedičstva UNESCO. Oproti mojej ostatnej návšteve mesta značne pokročili úpravy priľahlých ulíc.    Mestské opevnenie, na ktorom práce ešte stále trvali   Známe bardejovsné námestie s kostolom a starou radnicou   Krásne pestrofarebné domy, obklopujúce námestie zo všetkých strán i priľahlé ulice v centre mesta   ***DEŇ DVADSIATY PIATY***   BRATISLAVA ODDYCHOVÁ   Doprava: Do ž. st. Bratislava-Vinohrady vlakom, odtiaľ na hl.st. peši, späť vlakom  Bratislava - nostalgická cesta po miestach, ktoré som kedysi denne absolvoval.  Tentoraz som ju absolvoval peši, aby som si to lepšie vychutnal   Bývalá chemická priemyslovka s celoslovenskou pôsobnoťou, na ktorej študovala aj manželka, neďaleko konečnej električkiek č. 1, 3 a 5 v Gaštanovom hájiku. Teraz som tam našiel tabuľu Gymnázium Grosslingová ul.  Asi v Bratislave už technikov netreba, Bratislava si vystačí s úradníkmi   Fabrický komín naproti ŠD Mladá Garda, pamiatka na časy, keď na týchto miestach boli ešte fabriky na výrobu parkiet a Tatrovka,  trochu vyššie Palma, trochu nižšie Stolwerck a za štrekou, ak sa dobre pamätám, Tesla. Cez štreku sme chodili na Tehelné pole na Slovan a raz v týždni na Pasienky na vojenské cvičenia. Pasienky vtedy boli len také hrboľaté zarastené polia, plné jám a kríkov,  akurát vhodné na nácvik útočiacej čaty so samopalmi a slepými nábojmi  a na budovanie okopov jednotlivca. Veru, "boli časy, boli, ale sa minuli, po maličkej chvíli minieme sa i my..." (J.Botto)    A ŠD Mladá garda so svojimi mládežníckymi čajmi o piatej: stále mladá a krásna, oproti ostatnej návšteve veľmi opeknela.    ***DEŇ DVADSIATY ŠIESTY***   LEVICE a GALANTA  Doprava: vlakom ta i späť  LEVICE - Levice, staré historické mesto, ktorého osud je nerozlučne spojený s tunajším hradom   Levický hrad, ktorého majiteľom bol o. i. aj slávny pán Váhu a Tatier - Matúš  Čák Trenčiansky   Pomerne zachovalé zrúcaniny hradu   Na nádvorí hradu je Tekovské múzeum, pod ktoré hrad patrí,  známe aj veľkou krádežou obrazov spred pár rokov   Nové evanjelické centrum v meste   GALANTA - staré okresné mesto   Jednoduché a krásne:  "...a diamant v hrude nezhnije" - úryvok zo Sládkovičovej Maríny   Krásne zreštavrovaná budova   Temto ošarpaný kaštieľ ešte len čaká na svoju reštauráciu    ***DEŇ DVADSIATY SIEDMY***   MYJAVA - nazývaná aj "Metropola pod slamou", ako ju nazvala vo svojom románe Zuzka Zguriška. Zohrala významnú úlohu v národnom povedomí Slovákov, keď tu bola vytvorená a na verejnom zhromaždení 19. sept. 1848 potvrdená Slovenská národná rada, prvá oficiálna predstaviteľka Slovákov.   Doprava: Tam i späť vlakom   Hoci Myjava bola čisto evanjelická, evanjelický kostol - najstaršia budova v meste (1711) -  im bol na príkaz vladára vzatý a pridelený katolíkom. Evanjelici si mohli o niekoľko desaťročí (1786) postaviť nový kostol, ale von z mesta a bez veže, ktorá mohla byť pristavená až o ďalších 70 rokov neskoršie (1856). Dnes je novší evanjelický kostol dominantou mesta,  starší, pôvodne evanjelický kostol patrí rímsko-katolíkom.    Myjava sa zapísala do slovenských dejín najmä ako mestečko,  v ktorom vznikla a sídlila prvá Slovenská národná rada v r. 1848. Dom pani Koléniovej, v ktorom zasadala Slovenská národná rada, je v súčasnosti súčasťou Múzea Slovenských národných rád   Súčasná slovenská hymna od Jána Matúšku ešte ako Dobrovoľnícka ako exponát  Múzea Slovenských národných rád na Myjave.  V r. 1939-1945 slovenskou hymnou bola Hej Slováci od Sama Tomášika, ktorá bola aj národnou hymnou v Juhoslávii, neskôr v Srbsku a Čiernej Hore pod názvom Hej Slovania.  Ja sa pamätám, že po oslobodení sa v našej dedine na verejných zhromaždeniach spievala  hymnická pieseň od Karola Kuzmányko Kto za pravdu horí.  A myslím si, že by bola najvhodnejšou národnou hymnou aj pre dnešok     Slovenská zástava a vlajka z meruôsmych rokov z Múzea SNR:  zaujímavé, že každá z nich má iné poradie farieb   ***DEŇ DVADSIATY ÔSMY***   FIĽAKOVO, RIMAVSKÁ SOBOTA a TISOVEC  Doprava: cesta ta i späť vlakom, späť cez Brezno a Banskú Bastricu  FIĽAKOVO - Mestečko pri maďarských hraniciach, najmä jeho hrad,  zohrali významnú úlohu v pritureckých bojoch   Centrum mesta   Fiľakovský hrad sa čnie na nevysokom návrší nad mestom   Pohľady z hradu na mesto a okolie   Zreštavrovaná rohová bašta   RIMAVSKÁ SOBOTA - centrum Gemera a Malohontu   V centre mesta   Socha Sándora Petöfiho z r. 2004   V meste   TISOVEC - slovenské mestečko, z ktorého ešte v nedávnej minulosti viedla ozubová železnica smerom na Brezno   Rodný dom Vladimíra Clementisa   Busta V. Clementisa na jeho rodnom dome   ***DEŇ DVADSIATY DEVIATY***   TRENČIANSKE TEPLICE - známe najmä Elektrou, vpádom kukláčov aj s granátmi do nej  a Mostom slávy, na ktorom sú umiestené tabuľky s menami  najvýznamnejších svetových filmových umelcov.  V súčasnosti sa už tabuľky umiesťujú v miestnom parku  Doprava: cesta tam i späť vlakom    Hotel Most slávy   Socha Ch. Chaplina na Moste slávy   Kúpeľné mestečko     ***DEŇ TRIDSIATY***   VYSOKÉ TATRY ODDYCHOVÉ  Len tak oddychovo sa previezť po Vysokých Tatrách, posledný raz toho roku, popozerať tie krásne kopce a nadýchať sa čerstvého tatranského vzduchu  Doprava: cesta tam i späť vlakom   Cesta ozubovou železnicou zo Štrby na Štrbské pleso   Prechádzka okolo Štrbského plesa   Železničná stanica Starý Smokovec   Pohľad na najvyšší vrch Vysokých Tatier - Gerlach  a spustošené pomaličky ožívajúce lesy   ***DEŇ TRIDSIATY PRVÝ***  SLOVENSKÝ OKRUH   Doprava vlakom  MARTIN - BANSKÁ BYSTRICA - MARGECANY - MARTIN V tento deň som celú cestu absolvoval vlskom s prestupom v Banskej Bystrici a Margecanoch. V Margecanoch som vynechal najbližší spoj a šiel som sa prejsť okolo nádrže, o ktorej som ani nevedel, až keď som ju videl z vlaku   Niekde na Horehroní   Dedinky   Ak si ešte dobre pamätám, tak to je vodná nádrž pri Margecanoch    * * *  SLOVO NA KONIEC   V tomto roku dovŕšim sedemdesiatku. Je to veľa, či málo? Aj v takom veku sa dá toho hodne skúsiť a vidieť a pritom s dôchodcovským poloprázdnym vačkom. Potešilo by ma, ak som týmto mojím putovaním upozornil niekoho na niečo, čo aj on v tomto roku navštívi. Veď naše Slovensko je nádherné, len si ho nevieme dostatočne ceniť.  Takto som vlani pochodil a popozeral kus nášho krásneho Slovenska.  Tridsaťjeden dní vkuse okrem jedného, keď som bol len na na bicykli na Bystričke kvôli rodinnej oslave.  Poobdivoval som prírodu i mestá, hrady i kopce, múzeá... Podobné niečo plánujem aj na tento rok. Niečo už mám vyhliadnuté, niečo sa ešte pritrafí... Nebol som ešte na Vršatci, zďaleka vyzerá impozantne. Neprešiel som z Vrátnej do Trusalovej, aj to je pekná prechádzka. Aj z Lietavskej Lúčky do Hrabového cez Súľovské skaly vyzerá vábne. Niečo by som si chcel aj zopakovať :  Prosiecku dolinu a Kvačiansku dolinu, z Čremošného Žarnovickou dolinou na Kráľovu studňu a odtiaľ do Blatnice, z Necpál na Ploskú a Borišov a do Belej-Dulíc... A popretkávať to niektorými ďalšími mestami a dedinami  Takže plánov na leto dosť   Budem Vám povďačný za každý ďalší návrh na leto. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (67)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Z Detvy do Hriňovej...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Znova z Handlovej do Prievidze
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Králická tiesňava a vodopád
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Zákopčianskymi kopanicami
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Hmly a spomienky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Urda
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Urda
            
         
        urda.blog.sme.sk (rss)
         
                        VIP
                             
     
         jednoducho dôchodca, jednou rukou na blogu, druhou nohou v hrobe so životnou zásadou podľa Diderota: 
 "Lepšie je opotrebovať sa, ako zhrdzavieť" 

                                    * * * 
Krásy Slovenska som propagoval i tu: 
http://fotki.yandex.ru/users/jan-urda/ stále živá stránka - http://fotki.yandex.ru/users/jujuju40/ 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    517
                
                
                    Celková karma
                    
                                                7.56
                    
                
                
                    Priemerná čítanosť
                    2324
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Detstvo - spomienky
                        
                     
                                     
                        
                            Škola - spomienky
                        
                     
                                     
                        
                            Práca - spomienky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Z Turca
                        
                     
                                     
                        
                            Staroba - spomienky
                        
                     
                                     
                        
                            Filatelia - svet poznania
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Nezamestnanosť - najzávažnejší problém na Slovensku
                     
                                                         
                       Môj 4% ný kamarát.
                     
                                                         
                       Straka
                     
                                                         
                       Zadarmo do Žiliny
                     
                                                         
                       Oneskorene k 17. Novembru ....
                     
                                                         
                       Komunálne voľby prerušené na 38 minút.
                     
                                                         
                       Inzerát
                     
                                                         
                       Koľko je reálne hodné euro?
                     
                                                         
                       Gruzínsko - cesta do kraja bez ciest
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




