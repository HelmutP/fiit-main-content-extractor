
 Aktuálny vývoj:  
prijatá SMS, 6.11.2007, 17:41, odosielatel: Sedlak Laviciari 
Pripravuje sa zaloba za ohovaranie tak si rozmysli ci to podpisem a odovzdam-o 15 minut si pozrem sme.dufam ze to bude dolu.  

Róbert Sedlák reaguje: 

  Predseda Mladých ľavičiarov podá trestné oznámenie na Juliána Bosáka - na jeho stránke 
 Predseda Mladých ľavičiarov podá trestné oznámenie na Juliána Bosáka - na blogu sme 
(Autor to už vymazal, tak uverejňujem uložené podoby)  
18:28 Ja budem reagovať neskôr, lebo idem von, pekne sneží :)   

Róbert Sedlák o tom ako chápe princípy trhovej ekonomiky 
((video))
  

Od začiatku mi je celá publicita Róberta Sedláka, predsedu Mladých ľavičiarov podozrivá. Preto som prijal jeho ponuku pre neho pracovať. Zaujímalo ma, ako to bude vyzerať. Ako sám hovorí, chápe trhovú ekonomiku a jej princípy (video). Je moderný ľavičiar a podniká. Založil si internetovú televíziu. SME.sk napríklad uverejnilo správu o jej vzniku v sekcii "Z domova" a "Počítače". Neviem, ako to bolo s inými médiami, ale na stránke IC culture train, alternetívneho klubu v Košiciach, taktiež uviedli na nich avízo na titulke a prvá reakcia v diskusii bola takáto:
  
„toto uz fakt nemyslite vazne... robit reklammu nejakej stranke ktora linkuje videa z Youtube a zaraba na nich pomocou Etargetu?? a napisete tu ze Nova Internetova Televizia ??? tie videa ste si mohli na Youtube dat sami a nalinkovat si ich pekne tuna na Vasej stranke pripadne im vytvorit osobitnu sekciu a netrepat nas na nejaku komercnu kravinu....“
  
Moja práca spočívala v natáčaní a zostrihaní reportáží z alternatívnych akcií. Idea v celku sympatická. Dohodli sme sa na pláci. Dohodli sme sa na termíne výplaty. Ústne. Natočili sme 4 reportáže (moja obľúbená), ktoré boli aj uverejnené + 1 prezentačný rozhovor. Potom jedného dňa mi volal, že dnes netočíme, neskôr som zistil, že bol točiť s niekým iným, moderátorku taktiež vymenil za svoje „zlatíčko“. Odvtedy sa neozval. Aj celá jeho televízia podľa čísel čítanosti mala nízku sledovanosť. Napísal som mu teda mail, ako to bude s plácou.
  e-mail prijatý dňa 2.10.2007 od Robert Sedlak: 
„tak ako som Ti slubil, honorar Ti viem vyplatit po mesiaci od zaciatku spoluprace, najblizsie to vychadza niekedy na buduci tyzden, kedze vtedy budem inkasovat prvu platbu za reklamnu kampan, takze urcite budeme v kontakte a budem ti volat.“ 
  
Nič sa neudialo, nevolal, tak som mu zavolal ja a mal som čakať do konca týždňa, opäť som sa nedočkal, nasledujúci termín bol pondelok večer, že sa mi ozve, opäť nič, ďalší deň som dostal odpoveď na mail.
  e-mail prijatý dňa 16.10.2007 od Robert Sedlak: 
„Pohoda, 
neviem kde mi hlava stoji, nemam cas ani sa napit, vcera som sa dokonca mierne dehydroval :(
   
Posli mi pls cislo uctu, lebo cez netbanking ti viem poslat peniaze kedykolvek, ale teraz nemam cas ani pockat na autobus.. je to des :(„
  
Poslal som mu svoje číslo účtu a čakal som. V piatok mi zdvihol telefón a na moju otázku, kedy pošle peniaze mi povedal, že čakal na peniaze za reklamu a údaje aj od moderátorky, aby to mohol celé naraz poslať, že to urobí poobede.
  
Čakal som tri pracovné dni, či mi nabehnú peniaze a nič. Tak som mu volal, niekoľkokrát, nezdvíhal ani jeden z dvoch svojich telefónov. Celý deň. Z adresy. ktorú uvádza ako sídlo svojej firmy sa asi pred mesiacom odsťahoval. Tak som našiel firmu, kde je zamestnaný. Jeho údajný nadriadený, ktorého som poprosil, že sa chcem stretnúť s Róbertom Sedlákom bol za ním a vrátil sa s tým, že je zaneprázdnený, že sa mi poobede ozve. Pýtal sa ma či mi dlží peniaze.
  
Poobede samozrejme ani ťuk. Volal som mu, nezdvíhal. 11ty deň od konca mesiaca po prvej reportáži, od dohodnutého termínu výplaty som zašiel za ním priamo do firmy, kde robí, najprv ma vyviedol von a upozornil ma, že tam len tak chodiť nemôžem. Potom sa vyhováral na to, že sme sa vraj dohodli, že mi zavolá. Ďalšia výhovorka bola, že sám nemá peniaze, že mu ešte neprišli, že nemal DIČ. Že mi do konca týždňa zavolá. Týždeň prešiel, neozval sa, telefóny mal v nedeľu nedostupné. 
  

Dnes je utorok, bol som zase za ním v práci. Opäť na mňa vytasil argument, že sme sa dohodli, že mi zavolá. Ja som mu len pripomenul, že dohoda znela - do konca týždňa. Pokračoval s tým, že práve vypláca moderátorku a ja prídem na rad po nej, lebo sa jedná o väčší obnos. Tak som mu navrhol, že chcem podpísať dohodu o vykonaní práce, on že či ju mám so sebou, tak som mu ju dal, pozrel ju, šiel si ju ofotiť, vrátil sa s tým, že ju dá právnikovi preštudovať a v sobotu mám prísť za ním. Pýtal som sa ho, či má svoju dohodu, že ako to chcel zaúčtovať. On, že nemá, že on to nepotreboval, že asi ja to potrebujem, keď som ju doniesol. Ja mu hovorím, že mne ide o tie peniaze, on mi na to, že je vo finančnej tiesni a nepovedal mi, kedy nebude. Takže opäť vidím, že sa nepohneme, tak som chcel mať v ruke papier. Takže v sobotu mám prísť. 
  
Doma som si dal dve a dve dokopy, ďalej už som nemienil šaškovať, tak som mu z iného telefónu zavolal, že buď podpíšeme dohodu dnes, alebo mi vyplatí peniaze, alebo ho to bude mrzieť. Obrátil to proti mne, že ho vydieram, a že to môže mrzieť mňa. Tak tu je článoček, ktorý som chcel zverejniť už v pondelok, ale chcel som mu ešte dať šancu. Netajím sa tým, že by som ho zverejnil tak či tak, pretože si myslím, že by malo byť jasné, kto sa skrýva za označením predseda Mladých ľavičiarov. Ale šlo mi aj o tých pred zdanením 4000 korún, pretože ja tiež nežijem zo vzduchu (za zub budem platiť toľko aj s tou daňou). 
  
Asi pred týždňom som sa spojil aj s bývalou moderátorkou, na ktorú mi Róbert nevedel dať kontakt a zistil som, že ani ona nedostala peniaze ani nevie, čo sa vlastne deje. Dnes som jej volal, tak potom, čo sa mu ona ozvala, že či na ňu nezabudol, aj jej sľúbil časť dohodnutých peňazí.
  
Pozrel som si aj blog bývalého člena Mladých ľavičiarov a je takisto zaujímavý aj v súvislosti s mojou skúsenosťou.
  

V rozhovore so staršími pánmi, ktorí uznanlivo oceňovali jeho progresívne myslenie podotkol, že ma platí dobre.
  

A tento človek veselo kritizuje prácu iných ľudí a teší sa pozornosti médií.
 


 
